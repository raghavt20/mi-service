.class public Lcom/android/server/am/MemoryStandardProcessControl;
.super Ljava/lang/Object;
.source "MemoryStandardProcessControl.java"

# interfaces
.implements Lcom/android/server/am/MemoryStandardProcessControlStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;,
        Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;,
        Lcom/android/server/am/MemoryStandardProcessControl$BinderService;,
        Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;,
        Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;,
        Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo;,
        Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;
    }
.end annotation


# static fields
.field private static final ADJ_WATERLINE:I = 0xfa

.field public static DEBUG:Z = false

.field private static final DEFAULT_CONFIG_FILE_PATH:Ljava/lang/String; = "/product/etc/mspc.default.json"

.field public static final EVENT_TAGS:I = 0x13c04

.field private static final HANDLE_MEM_PRESSURE_INTERVAL:J = 0x1d4c0L

.field private static final KILLING_DURATION_THRESHOLD:J = 0x5265c00L

.field private static final MAX_CONTINUE_DETECT_EXCEPTION:I = 0x3

.field private static final MAX_PROCESS_KILL_TIMES:I = 0x3

.field private static final MEMORY_SCREENOFF_STRATEGY:Ljava/lang/String; = "persist.sys.memory_standard.strategy"

.field private static final MEMORY_STANDARD_APPHEAP_ENABLE_REOP:Ljava/lang/String; = "persist.sys.memory_standard.appheap.enable"

.field private static final MEMORY_STANDARD_DEBUG_PROP:Ljava/lang/String; = "persist.sys.memory_standard.debug"

.field private static final MEMORY_STANDARD_ENABLE_PROP:Ljava/lang/String; = "persist.sys.memory_standard.enable"

.field private static final MEM_NO_PRESSURE:I = -0x1

.field private static final MEM_PRESSURE_COUNT:I = 0x3

.field private static final MEM_PRESSURE_CRITICAL:I = 0x2

.field private static final MEM_PRESSURE_LOW:I = 0x0

.field private static final MEM_PRESSURE_MIN:I = 0x1

.field private static final MSG_DUMP_STANDARD:I = 0x6

.field private static final MSG_DUMP_WARNED_APP:I = 0x5

.field private static final MSG_KILL_HIGH_PRIORITY_PROCESSES:I = 0x3

.field private static final MSG_PERIODIC_DETECTION:I = 0x1

.field private static final MSG_PERIODIC_KILL:I = 0x2

.field private static final MSG_REPORT_APP_HEAP:I = 0x7

.field private static final MSG_REPORT_PRESSURE:I = 0x4

.field private static final SCREEN_STATE_OFF:I = 0x2

.field private static final SCREEN_STATE_ON:I = 0x1

.field private static final SCREEN_STATE_UNKOWN:I = 0x3

.field private static final TAG:Ljava/lang/String; = "MemoryStandardProcessControl"

.field private static final sDefaultCacheLevel:[[I

.field private static sDefaultMemoryStandardArray:[J


# instance fields
.field private mAMS:Lcom/android/server/am/ActivityManagerService;

.field private mATMS:Lcom/android/server/wm/ActivityTaskManagerService;

.field public volatile mAppHeapEnable:Z

.field public volatile mAppHeapHandleTime:J

.field public mAppHeapStandard:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mAudioService:Lcom/android/server/audio/AudioService;

.field private mContext:Landroid/content/Context;

.field private mDefaultMemoryStandard:J

.field public volatile mEnable:Z

.field public mHandler:Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mIsInit:Z

.field private mLastMemDetectionHasKilled:Z

.field private mLastMemPressureTime:J

.field public mMemoryStandardMap:Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;

.field private mNeedReleaseMemSize:J

.field private mPMS:Lcom/android/server/am/ProcessManagerService;

.field private mPackageManager:Landroid/app/ApplicationPackageManager;

.field private mPressureCacheThreshold:[I

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private volatile mScreenDetection:Z

.field public volatile mScreenOffStrategy:I

.field public volatile mScreenState:I

.field private mTriggerKill:Z

.field private mWMS:Lcom/android/server/wm/WindowManagerInternal;

.field public mWarnedAppMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$mdebugModifyKillStrategy(Lcom/android/server/am/MemoryStandardProcessControl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/MemoryStandardProcessControl;->debugModifyKillStrategy()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleAppHeapWhenBackground(Lcom/android/server/am/MemoryStandardProcessControl;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/am/MemoryStandardProcessControl;->handleAppHeapWhenBackground(ILjava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleMemPressure(Lcom/android/server/am/MemoryStandardProcessControl;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/MemoryStandardProcessControl;->handleMemPressure(I)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 10

    .line 115
    const/4 v0, 0x7

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/server/am/MemoryStandardProcessControl;->sDefaultMemoryStandardArray:[J

    .line 117
    const v0, 0x5a550

    const v1, 0x46cd0

    const v2, 0x6ddd0

    filled-new-array {v2, v0, v1}, [I

    move-result-object v3

    const v0, 0x72bf0

    const v1, 0x5f370

    const v2, 0x86470

    filled-new-array {v2, v0, v1}, [I

    move-result-object v4

    const v0, 0x8b290

    const v1, 0x77a10

    const v2, 0x9eb10

    filled-new-array {v2, v0, v1}, [I

    move-result-object v5

    const v0, 0x11da50

    const v1, 0x10a1d0

    const v2, 0x1312d0

    filled-new-array {v2, v0, v1}, [I

    move-result-object v6

    const v0, 0x17f4d0

    const v1, 0x16bc50

    const v2, 0x192d50

    filled-new-array {v2, v0, v1}, [I

    move-result-object v7

    const v0, 0x1e0f50

    const v1, 0x1cd6d0

    const v2, 0x1f47d0

    filled-new-array {v2, v0, v1}, [I

    move-result-object v8

    const v0, 0x2429d0

    const v1, 0x22f150

    const v2, 0x256250

    filled-new-array {v2, v0, v1}, [I

    move-result-object v9

    filled-new-array/range {v3 .. v9}, [[I

    move-result-object v0

    sput-object v0, Lcom/android/server/am/MemoryStandardProcessControl;->sDefaultCacheLevel:[[I

    .line 125
    const-string v0, "persist.sys.memory_standard.debug"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z

    return-void

    nop

    :array_0
    .array-data 8
        0x100000
        0x100000
        0x180000
        0x180000
        0x200000
        0x200000
        0x200000
    .end array-data
.end method

.method public constructor <init>()V
    .locals 6

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    const-string v0, "persist.sys.memory_standard.enable"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mEnable:Z

    .line 121
    const-string v0, "persist.sys.memory_standard.appheap.enable"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapEnable:Z

    .line 122
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenDetection:Z

    .line 123
    const-string v0, "persist.sys.memory_standard.strategy"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenOffStrategy:I

    .line 127
    new-instance v0, Landroid/os/HandlerThread;

    const-string v2, "MemoryStandardProcessControl"

    invoke-direct {v0, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mHandlerThread:Landroid/os/HandlerThread;

    .line 128
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenState:I

    .line 130
    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mPressureCacheThreshold:[I

    .line 131
    iput-boolean v1, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mIsInit:Z

    .line 132
    iput-boolean v1, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mTriggerKill:Z

    .line 133
    const-wide/32 v2, 0x200000

    iput-wide v2, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mDefaultMemoryStandard:J

    .line 134
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mNeedReleaseMemSize:J

    .line 136
    const-wide/32 v4, 0x124f80

    iput-wide v4, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapHandleTime:J

    .line 138
    iput-wide v2, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mLastMemPressureTime:J

    .line 140
    iput-boolean v1, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mLastMemDetectionHasKilled:Z

    .line 142
    new-instance v0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;

    invoke-direct {v0, p0}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;-><init>(Lcom/android/server/am/MemoryStandardProcessControl;)V

    iput-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mMemoryStandardMap:Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;

    .line 143
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mWarnedAppMap:Ljava/util/HashMap;

    .line 145
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapStandard:Ljava/util/HashMap;

    .line 147
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mContext:Landroid/content/Context;

    .line 148
    iput-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mHandler:Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;

    .line 149
    iput-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mAMS:Lcom/android/server/am/ActivityManagerService;

    .line 150
    iput-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mATMS:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 151
    iput-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mPMS:Lcom/android/server/am/ProcessManagerService;

    .line 152
    iput-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mAudioService:Lcom/android/server/audio/AudioService;

    .line 153
    iput-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mPackageManager:Landroid/app/ApplicationPackageManager;

    .line 154
    iput-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mWMS:Lcom/android/server/wm/WindowManagerInternal;

    .line 794
    new-instance v0, Lcom/android/server/am/MemoryStandardProcessControl$1;

    invoke-direct {v0, p0}, Lcom/android/server/am/MemoryStandardProcessControl$1;-><init>(Lcom/android/server/am/MemoryStandardProcessControl;)V

    iput-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private canKill(ZLcom/android/server/am/MemoryStandardProcessControl$AppInfo;Ljava/util/List;Ljava/util/List;)Z
    .locals 11
    .param p1, "canKillhighPriorityProcess"    # Z
    .param p2, "info"    # Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    .line 611
    .local p3, "activeUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .local p4, "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iget v0, p2, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mAdj:I

    .line 612
    .local v0, "adj":I
    const/16 v1, 0xfa

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    .line 614
    if-le v0, v1, :cond_1

    .line 615
    return v2

    .line 619
    :cond_0
    if-gt v0, v1, :cond_1

    .line 620
    return v2

    .line 623
    :cond_1
    iget-object v1, p2, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mPackageName:Ljava/lang/String;

    .line 625
    .local v1, "packageName":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mMemoryStandardMap:Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;

    invoke-static {v3}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->-$$Nest$fgetmNativeProcessCmds(Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_3

    .line 627
    iget-object v2, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mMemoryStandardMap:Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;

    invoke-virtual {v2, v1}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->getStandard(Ljava/lang/String;)Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;

    move-result-object v2

    .line 628
    .local v2, "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
    if-eqz v2, :cond_2

    iget-object v3, v2, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;->mParent:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 630
    iget-object v3, v2, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;->mParent:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/android/server/am/MemoryStandardProcessControl;->isForegroundApp(Ljava/lang/String;)Z

    move-result v3

    xor-int/2addr v3, v4

    return v3

    .line 632
    :cond_2
    return v4

    .line 635
    .end local v2    # "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
    :cond_3
    iget v3, p2, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mUid:I

    .line 636
    .local v3, "uid":I
    invoke-virtual {p0, v1}, Lcom/android/server/am/MemoryStandardProcessControl;->isForegroundApp(Ljava/lang/String;)Z

    move-result v5

    .line 637
    .local v5, "foreground":Z
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {p3, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    .line 638
    .local v6, "active":Z
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {p4, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    .line 639
    .local v7, "visible":Z
    sget-boolean v8, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z

    if-eqz v8, :cond_4

    .line 640
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "packageName: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "MemoryStandardProcessControl"

    invoke-static {v9, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 641
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "isForegroundApp(packageName): "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v9, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 642
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "activeUids.contains(uid): "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v9, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 643
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "visibleUids.contains(uid): "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v9, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 645
    :cond_4
    if-nez v5, :cond_6

    if-nez v6, :cond_6

    if-eqz v7, :cond_5

    goto :goto_0

    .line 648
    :cond_5
    return v4

    .line 646
    :cond_6
    :goto_0
    return v2
.end method

.method private closeIO(Ljava/io/Closeable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/io/Closeable;",
            ">(TT;)V"
        }
    .end annotation

    .line 1119
    .local p1, "io":Ljava/io/Closeable;, "TT;"
    if-eqz p1, :cond_0

    .line 1121
    :try_start_0
    invoke-interface {p1}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1124
    goto :goto_0

    .line 1122
    :catch_0
    move-exception v0

    .line 1123
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "close io failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MemoryStandardProcessControl"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1126
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    :goto_0
    return-void
.end method

.method private debugModifyKillStrategy()V
    .locals 1

    .line 266
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mEnable:Z

    .line 267
    iput v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenOffStrategy:I

    .line 268
    return-void
.end method

.method private doClean(Ljava/util/Set;Z)V
    .locals 35
    .param p2, "canKillhighPriorityProcess"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .line 437
    .local p1, "deletePackageNameSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct/range {p0 .. p0}, Lcom/android/server/am/MemoryStandardProcessControl;->getMemPressureLevel()I

    move-result v0

    const/4 v3, 0x2

    if-ge v0, v3, :cond_0

    sget-boolean v0, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z

    if-nez v0, :cond_0

    .line 438
    return-void

    .line 441
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v3, v0

    .line 442
    .local v3, "recordRestartProcs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo;>;"
    const-wide/16 v4, 0x0

    .line 445
    .local v4, "memoryReleased":J
    :try_start_0
    iget-object v6, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mAMS:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v6
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 446
    :try_start_1
    iget-object v0, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessList;->getLruProcessesLOSP()Ljava/util/ArrayList;

    move-result-object v0

    .line 447
    .local v0, "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    .line 448
    .local v7, "N":I
    add-int/lit8 v8, v7, -0x1

    .local v8, "i":I
    :goto_0
    const/4 v12, 0x0

    if-ltz v8, :cond_8

    .line 449
    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/android/server/am/ProcessRecord;

    .line 450
    .local v14, "app":Lcom/android/server/am/ProcessRecord;
    iget-object v15, v14, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v15}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I

    move-result v15

    .line 451
    .local v15, "adj":I
    iget v9, v14, Lcom/android/server/am/ProcessRecord;->mPid:I

    .line 452
    .local v9, "pid":I
    iget-object v13, v14, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 453
    .local v13, "processName":Ljava/lang/String;
    iget-object v10, v14, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v10, v10, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 455
    .local v10, "packageName":Ljava/lang/String;
    if-eqz v10, :cond_6

    invoke-interface {v2, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    invoke-virtual {v14}, Lcom/android/server/am/ProcessRecord;->isKilledByAm()Z

    move-result v11

    if-nez v11, :cond_6

    .line 456
    invoke-virtual {v14}, Lcom/android/server/am/ProcessRecord;->isKilled()Z

    move-result v11

    if-nez v11, :cond_5

    invoke-virtual {v14}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v11

    if-eqz v11, :cond_5

    iget-boolean v11, v14, Lcom/android/server/am/ProcessRecord;->isolated:Z

    if-eqz v11, :cond_1

    .line 457
    move-object/from16 v22, v0

    goto/16 :goto_1

    .line 459
    :cond_1
    const/16 v11, 0xfa

    if-ge v15, v11, :cond_2

    if-nez p2, :cond_2

    .line 460
    const-string v11, "MemoryStandardProcessControl"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v22, v0

    .end local v0    # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    .local v22, "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    const-string v0, "Process adj has a high priority and cannot be killed:"

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v12, "; adj:"

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v11, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 461
    goto/16 :goto_1

    .line 459
    .end local v22    # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    .restart local v0    # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    :cond_2
    move-object/from16 v22, v0

    .line 463
    .end local v0    # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    .restart local v22    # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    iget-object v0, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mMemoryStandardMap:Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;

    invoke-virtual {v0, v10}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->getStandard(Ljava/lang/String;)Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;

    move-result-object v0

    .line 465
    .local v0, "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
    if-eqz v0, :cond_3

    iget-object v11, v0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;->mProcesses:Ljava/util/Set;

    invoke-interface {v11, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_3

    .line 466
    goto/16 :goto_1

    .line 468
    :cond_3
    iget v11, v14, Lcom/android/server/am/ProcessRecord;->mPid:I

    invoke-static {v11, v12, v12}, Landroid/os/Debug;->getPss(I[J[J)J

    move-result-wide v16

    const-wide/16 v18, 0x400

    div-long v16, v16, v18

    move-wide/from16 v23, v16

    .line 469
    .local v23, "procPss":J
    add-long v4, v4, v23

    .line 471
    invoke-static {}, Lcom/android/server/am/AppProfilerStub;->getInstance()Lcom/android/server/am/AppProfilerStub;

    move-result-object v16

    iget v11, v14, Lcom/android/server/am/ProcessRecord;->mPid:I

    iget v12, v14, Lcom/android/server/am/ProcessRecord;->uid:I

    move-object/from16 v17, v13

    move/from16 v18, v11

    move/from16 v19, v12

    move-wide/from16 v20, v23

    invoke-interface/range {v16 .. v21}, Lcom/android/server/am/AppProfilerStub;->reportMemoryStandardProcessControlKillMessage(Ljava/lang/String;IIJ)V

    .line 473
    const/16 v11, 0xfa

    if-ge v15, v11, :cond_4

    .line 474
    new-instance v11, Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo;

    iget v12, v14, Lcom/android/server/am/ProcessRecord;->userId:I

    invoke-direct {v11, v1, v10, v13, v12}, Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo;-><init>(Lcom/android/server/am/MemoryStandardProcessControl;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v3, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 476
    :cond_4
    const/4 v11, 0x1

    iput-boolean v11, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mLastMemDetectionHasKilled:Z

    .line 477
    const-string v12, "MemoryStandardProcessContral"

    move-object/from16 v16, v0

    .end local v0    # "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
    .local v16, "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
    const/16 v0, 0xd

    invoke-virtual {v14, v12, v0, v11}, Lcom/android/server/am/ProcessRecord;->killLocked(Ljava/lang/String;IZ)V

    .line 478
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "standardkill:"

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {v1, v14}, Lcom/android/server/am/MemoryStandardProcessControl;->killedReason(Lcom/android/server/am/ProcessRecord;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const v11, 0x13c04

    invoke-static {v11, v0}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 479
    const-string v0, "MemoryStandardProcessControl"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "kill process, processName:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "; pid:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v14, Lcom/android/server/am/ProcessRecord;->mPid:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v0, v11}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 481
    invoke-interface {v2, v10}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 483
    iget-object v0, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mWarnedAppMap:Ljava/util/HashMap;

    invoke-virtual {v0, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;

    .line 484
    .local v0, "killedAppInfo":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
    const/4 v11, 0x0

    iput v11, v0, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mMemExcelTime:I

    .line 485
    const/16 v11, 0x3e9

    iput v11, v0, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mAdj:I

    .line 486
    iget-wide v11, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mNeedReleaseMemSize:J

    cmp-long v11, v4, v11

    if-lez v11, :cond_7

    sget-boolean v11, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z

    if-nez v11, :cond_7

    .line 487
    goto :goto_2

    .line 456
    .end local v16    # "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
    .end local v22    # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    .end local v23    # "procPss":J
    .local v0, "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    :cond_5
    move-object/from16 v22, v0

    .end local v0    # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    .restart local v22    # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    goto :goto_1

    .line 455
    .end local v22    # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    .restart local v0    # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    :cond_6
    move-object/from16 v22, v0

    .line 448
    .end local v0    # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    .end local v9    # "pid":I
    .end local v10    # "packageName":Ljava/lang/String;
    .end local v13    # "processName":Ljava/lang/String;
    .end local v14    # "app":Lcom/android/server/am/ProcessRecord;
    .end local v15    # "adj":I
    .restart local v22    # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    :cond_7
    :goto_1
    add-int/lit8 v8, v8, -0x1

    move-object/from16 v0, v22

    goto/16 :goto_0

    .end local v22    # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    .restart local v0    # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    :cond_8
    move-object/from16 v22, v0

    .line 490
    .end local v0    # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    .end local v7    # "N":I
    .end local v8    # "i":I
    :goto_2
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 492
    const/4 v0, 0x0

    move v6, v0

    .local v6, "i":I
    :goto_3
    :try_start_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v6, v0, :cond_9

    .line 493
    const-string v0, "MemoryStandardPullUp"

    move-object v7, v0

    .line 494
    .local v7, "hostingType":Ljava/lang/String;
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo;

    move-object v8, v0

    .line 495
    .local v8, "procInfo":Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo;
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v0

    iget-object v9, v8, Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo;->mPackageName:Ljava/lang/String;

    iget v10, v8, Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo;->mUserId:I

    const-wide/16 v11, 0x400

    invoke-interface {v0, v9, v11, v12, v10}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;JI)Landroid/content/pm/ApplicationInfo;

    move-result-object v27

    .line 497
    .local v27, "info":Landroid/content/pm/ApplicationInfo;
    iget-object v9, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mAMS:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v9
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 498
    :try_start_3
    iget-object v0, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget-object v10, v8, Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo;->mProcessName:Ljava/lang/String;

    const/16 v28, 0x0

    const/16 v29, 0x0

    new-instance v11, Lcom/android/server/am/HostingRecord;

    iget-object v12, v8, Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo;->mProcessName:Ljava/lang/String;

    invoke-direct {v11, v7, v12}, Lcom/android/server/am/HostingRecord;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v31, 0x1

    const/16 v32, 0x0

    const/16 v33, 0x0

    const-string v34, "android"

    move-object/from16 v25, v0

    move-object/from16 v26, v10

    move-object/from16 v30, v11

    invoke-virtual/range {v25 .. v34}, Lcom/android/server/am/ActivityManagerService;->startProcessLocked(Ljava/lang/String;Landroid/content/pm/ApplicationInfo;ZILcom/android/server/am/HostingRecord;IZZLjava/lang/String;)Lcom/android/server/am/ProcessRecord;

    .line 502
    monitor-exit v9

    .line 492
    .end local v7    # "hostingType":Ljava/lang/String;
    .end local v8    # "procInfo":Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo;
    .end local v27    # "info":Landroid/content/pm/ApplicationInfo;
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 502
    .restart local v7    # "hostingType":Ljava/lang/String;
    .restart local v8    # "procInfo":Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo;
    .restart local v27    # "info":Landroid/content/pm/ApplicationInfo;
    :catchall_0
    move-exception v0

    monitor-exit v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .end local v3    # "recordRestartProcs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo;>;"
    .end local v4    # "memoryReleased":J
    .end local p0    # "this":Lcom/android/server/am/MemoryStandardProcessControl;
    .end local p1    # "deletePackageNameSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local p2    # "canKillhighPriorityProcess":Z
    :try_start_4
    throw v0

    .line 506
    .end local v6    # "i":I
    .end local v7    # "hostingType":Ljava/lang/String;
    .end local v8    # "procInfo":Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo;
    .end local v27    # "info":Landroid/content/pm/ApplicationInfo;
    .restart local v3    # "recordRestartProcs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo;>;"
    .restart local v4    # "memoryReleased":J
    .restart local p0    # "this":Lcom/android/server/am/MemoryStandardProcessControl;
    .restart local p1    # "deletePackageNameSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local p2    # "canKillhighPriorityProcess":Z
    :cond_9
    iget-object v0, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mMemoryStandardMap:Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;

    invoke-virtual {v0}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->getNativeProcessCmds()Ljava/util/Set;

    move-result-object v0

    .line 507
    .local v0, "nativeCmds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v6

    if-nez v6, :cond_a

    .line 508
    return-void

    .line 510
    :cond_a
    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 512
    .local v6, "procs":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_e

    iget-wide v7, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mNeedReleaseMemSize:J

    cmp-long v7, v4, v7

    if-lez v7, :cond_b

    sget-boolean v7, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z

    if-eqz v7, :cond_e

    .line 513
    :cond_b
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 514
    .local v7, "proc":Ljava/lang/String;
    invoke-interface {v0, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_c

    .line 515
    goto :goto_4

    .line 517
    :cond_c
    filled-new-array {v7}, [Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/os/Process;->getPidsForCommands([Ljava/lang/String;)[I

    move-result-object v8

    .line 518
    .local v8, "pids":[I
    if-eqz v8, :cond_d

    array-length v9, v8

    if-lez v9, :cond_d

    .line 519
    const/4 v9, 0x0

    aget v10, v8, v9

    move v9, v10

    .line 520
    .restart local v9    # "pid":I
    const/4 v10, 0x0

    invoke-static {v9, v10, v10}, Landroid/os/Debug;->getPss(I[J[J)J

    move-result-wide v11

    const-wide/16 v13, 0x400

    div-long/2addr v11, v13

    .line 521
    .local v11, "procPss":J
    const/4 v15, 0x1

    iput-boolean v15, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mLastMemDetectionHasKilled:Z

    .line 522
    invoke-static {v9}, Landroid/os/Process;->killProcess(I)V

    .line 523
    add-long/2addr v4, v11

    .line 524
    const-string v10, "MemoryStandardProcessControl"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "kill native process, processName:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "; pid:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v10, v13}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 526
    iget-object v10, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mWarnedAppMap:Ljava/util/HashMap;

    invoke-virtual {v10, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;

    .line 527
    .local v10, "killedAppInfo":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
    const/4 v13, 0x0

    iput v13, v10, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mMemExcelTime:I

    .line 528
    const/16 v14, -0x3e8

    iput v14, v10, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mAdj:I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_5

    .line 518
    .end local v9    # "pid":I
    .end local v10    # "killedAppInfo":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
    .end local v11    # "procPss":J
    :cond_d
    const/4 v13, 0x0

    const/4 v15, 0x1

    .line 530
    .end local v7    # "proc":Ljava/lang/String;
    .end local v8    # "pids":[I
    :goto_5
    goto :goto_4

    .line 533
    .end local v0    # "nativeCmds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v6    # "procs":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_e
    goto :goto_6

    .line 490
    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .end local v3    # "recordRestartProcs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo;>;"
    .end local v4    # "memoryReleased":J
    .end local p0    # "this":Lcom/android/server/am/MemoryStandardProcessControl;
    .end local p1    # "deletePackageNameSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local p2    # "canKillhighPriorityProcess":Z
    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    .line 531
    .restart local v3    # "recordRestartProcs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo;>;"
    .restart local v4    # "memoryReleased":J
    .restart local p0    # "this":Lcom/android/server/am/MemoryStandardProcessControl;
    .restart local p1    # "deletePackageNameSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local p2    # "canKillhighPriorityProcess":Z
    :catch_0
    move-exception v0

    .line 532
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 534
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_6
    return-void
.end method

.method private getActiveUids()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 582
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 583
    .local v0, "uids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iget-object v1, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mPMS:Lcom/android/server/am/ProcessManagerService;

    .line 584
    invoke-virtual {v1}, Lcom/android/server/am/ProcessManagerService;->getProcessPolicy()Lcom/android/server/am/ProcessPolicy;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessPolicy;->getActiveUidRecordList(I)Ljava/util/List;

    move-result-object v1

    .line 585
    .local v1, "activeUids":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;>;"
    if-eqz v1, :cond_1

    .line 586
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;

    .line 587
    .local v3, "r":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;
    iget v4, v3, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->uid:I

    .line 588
    .local v4, "uid":I
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 589
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 591
    .end local v3    # "r":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;
    .end local v4    # "uid":I
    :cond_0
    goto :goto_0

    .line 593
    :cond_1
    sget-boolean v2, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_3

    .line 594
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [I

    .line 595
    .local v2, "debugUids":[I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 596
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    aput v4, v2, v3

    .line 595
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 598
    .end local v3    # "i":I
    :cond_2
    iget-object v3, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mPackageManager:Landroid/app/ApplicationPackageManager;

    invoke-virtual {v3, v2}, Landroid/app/ApplicationPackageManager;->getNamesForUids([I)[Ljava/lang/String;

    move-result-object v3

    .line 599
    .local v3, "pkgs":[Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "LocationActivePkgs: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "MemoryStandardProcessControl"

    invoke-static {v5, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 601
    .end local v2    # "debugUids":[I
    .end local v3    # "pkgs":[Ljava/lang/String;
    :cond_3
    return-object v0
.end method

.method private getMemPressureLevel()I
    .locals 10

    .line 235
    new-instance v0, Lcom/android/internal/util/MemInfoReader;

    invoke-direct {v0}, Lcom/android/internal/util/MemInfoReader;-><init>()V

    .line 236
    .local v0, "minfo":Lcom/android/internal/util/MemInfoReader;
    invoke-virtual {v0}, Lcom/android/internal/util/MemInfoReader;->readMemInfo()V

    .line 237
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mNeedReleaseMemSize:J

    .line 238
    invoke-virtual {v0}, Lcom/android/internal/util/MemInfoReader;->getRawInfo()[J

    move-result-object v1

    .line 239
    .local v1, "rawInfo":[J
    const/4 v2, 0x3

    aget-wide v2, v1, v2

    const/16 v4, 0x1a

    aget-wide v5, v1, v4

    add-long/2addr v2, v5

    const/4 v5, 0x2

    aget-wide v6, v1, v5

    add-long/2addr v2, v6

    .line 242
    .local v2, "otherFile":J
    const/4 v6, 0x4

    aget-wide v6, v1, v6

    const/16 v8, 0x12

    aget-wide v8, v1, v8

    add-long/2addr v6, v8

    aget-wide v8, v1, v4

    add-long/2addr v6, v8

    .line 245
    .local v6, "needRemovedFile":J
    cmp-long v4, v2, v6

    if-lez v4, :cond_0

    .line 246
    sub-long/2addr v2, v6

    .line 248
    :cond_0
    iget-object v4, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mPressureCacheThreshold:[I

    const/4 v8, 0x0

    aget v8, v4, v8

    int-to-long v8, v8

    cmp-long v8, v2, v8

    if-ltz v8, :cond_1

    .line 249
    const/4 v4, -0x1

    return v4

    .line 250
    :cond_1
    const/4 v8, 0x1

    aget v8, v4, v8

    int-to-long v8, v8

    cmp-long v8, v2, v8

    if-ltz v8, :cond_2

    .line 251
    const/4 v4, 0x0

    .local v4, "level":I
    goto :goto_0

    .line 252
    .end local v4    # "level":I
    :cond_2
    aget v4, v4, v5

    int-to-long v8, v4

    cmp-long v5, v2, v8

    if-ltz v5, :cond_3

    .line 253
    const/4 v4, 0x1

    .restart local v4    # "level":I
    goto :goto_0

    .line 255
    .end local v4    # "level":I
    :cond_3
    int-to-long v4, v4

    sub-long/2addr v4, v2

    iput-wide v4, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mNeedReleaseMemSize:J

    .line 256
    const/4 v4, 0x2

    .line 258
    .restart local v4    # "level":I
    :goto_0
    sget-boolean v5, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z

    if-eqz v5, :cond_4

    .line 259
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Other File: "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "KB. Mem Pressure Level: "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v8, "MemoryStandardProcessControl"

    invoke-static {v8, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    :cond_4
    return v4
.end method

.method private getVisibleWindowOwner()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 568
    iget-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mWMS:Lcom/android/server/wm/WindowManagerInternal;

    invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerInternal;->getVisibleWindowOwner()Ljava/util/List;

    move-result-object v0

    .line 570
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    sget-boolean v1, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 571
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [I

    .line 572
    .local v1, "debugUids":[I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 573
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aput v3, v1, v2

    .line 572
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 575
    .end local v2    # "i":I
    :cond_0
    iget-object v2, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mPackageManager:Landroid/app/ApplicationPackageManager;

    invoke-virtual {v2, v1}, Landroid/app/ApplicationPackageManager;->getNamesForUids([I)[Ljava/lang/String;

    move-result-object v2

    .line 576
    .local v2, "pkgs":[Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "VisibleWindowPkgs: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "MemoryStandardProcessControl"

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 578
    .end local v1    # "debugUids":[I
    .end local v2    # "pkgs":[Ljava/lang/String;
    :cond_1
    return-object v0
.end method

.method private handleAppHeapWhenBackground(ILjava/lang/String;)V
    .locals 18
    .param p1, "pid"    # I
    .param p2, "pkgName"    # Ljava/lang/String;

    .line 676
    move-object/from16 v1, p0

    move/from16 v2, p1

    move-object/from16 v9, p2

    const/4 v0, 0x0

    .line 677
    .local v0, "appHeap":I
    new-instance v3, Landroid/os/Debug$MemoryInfo;

    invoke-direct {v3}, Landroid/os/Debug$MemoryInfo;-><init>()V

    move-object v10, v3

    .line 678
    .local v10, "memInfo":Landroid/os/Debug$MemoryInfo;
    invoke-static {v2, v10}, Landroid/os/Debug;->getMemoryInfo(ILandroid/os/Debug$MemoryInfo;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 679
    return-void

    .line 681
    :cond_0
    invoke-virtual {v10}, Landroid/os/Debug$MemoryInfo;->getSummaryJavaHeap()I

    move-result v3

    div-int/lit16 v11, v3, 0x400

    .line 682
    .end local v0    # "appHeap":I
    .local v11, "appHeap":I
    const-string v0, "MemoryStandardProcessControl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handle app heap when background, appHeap is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 685
    const/4 v3, -0x1

    .line 686
    .local v3, "uid":I
    const/4 v4, -0x1

    .line 687
    .local v4, "userId":I
    const/4 v5, 0x0

    .line 688
    .local v5, "finded":Z
    iget-object v6, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mAMS:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v6

    .line 689
    :try_start_0
    iget-object v0, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessList;->getLruProcessesLOSP()Ljava/util/ArrayList;

    move-result-object v0

    .line 690
    .local v0, "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    .line 691
    .local v7, "N":I
    const/4 v8, 0x0

    .line 692
    .local v8, "app":Lcom/android/server/am/ProcessRecord;
    add-int/lit8 v12, v7, -0x1

    .local v12, "i":I
    :goto_0
    if-ltz v12, :cond_4

    .line 693
    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/android/server/am/ProcessRecord;

    move-object v8, v13

    .line 694
    iget-object v13, v8, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 695
    .local v13, "processName":Ljava/lang/String;
    invoke-virtual {v9, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_1

    .line 697
    nop

    .line 692
    .end local v13    # "processName":Ljava/lang/String;
    add-int/lit8 v12, v12, -0x1

    goto :goto_0

    .line 699
    .restart local v13    # "processName":Ljava/lang/String;
    :cond_1
    iget v14, v8, Lcom/android/server/am/ProcessRecord;->uid:I

    move v3, v14

    .line 700
    iget v14, v8, Lcom/android/server/am/ProcessRecord;->userId:I

    move v4, v14

    .line 701
    const/4 v5, 0x1

    .line 702
    iget v14, v8, Lcom/android/server/am/ProcessRecord;->mPid:I

    if-ne v14, v2, :cond_3

    invoke-virtual {v8}, Lcom/android/server/am/ProcessRecord;->isKilledByAm()Z

    move-result v14

    if-nez v14, :cond_3

    invoke-virtual {v8}, Lcom/android/server/am/ProcessRecord;->isKilled()Z

    move-result v14

    if-nez v14, :cond_3

    .line 703
    invoke-virtual {v8}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v14

    if-eqz v14, :cond_3

    iget-boolean v14, v8, Lcom/android/server/am/ProcessRecord;->isolated:Z

    if-eqz v14, :cond_2

    goto :goto_1

    :cond_2
    move v12, v3

    move v13, v4

    move v14, v5

    goto :goto_2

    .line 704
    :cond_3
    :goto_1
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    return-void

    .line 692
    .end local v13    # "processName":Ljava/lang/String;
    :cond_4
    move v12, v3

    move v13, v4

    move v14, v5

    .line 708
    .end local v0    # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    .end local v3    # "uid":I
    .end local v4    # "userId":I
    .end local v5    # "finded":Z
    .end local v7    # "N":I
    .end local v8    # "app":Lcom/android/server/am/ProcessRecord;
    .local v12, "uid":I
    .local v13, "userId":I
    .local v14, "finded":Z
    :goto_2
    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 712
    invoke-direct/range {p0 .. p0}, Lcom/android/server/am/MemoryStandardProcessControl;->getVisibleWindowOwner()Ljava/util/List;

    move-result-object v0

    .line 713
    .local v0, "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-direct/range {p0 .. p0}, Lcom/android/server/am/MemoryStandardProcessControl;->getActiveUids()Ljava/util/List;

    move-result-object v15

    .line 714
    .local v15, "activeUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    if-eqz v14, :cond_8

    iget-object v3, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapStandard:Ljava/util/HashMap;

    invoke-virtual {v3, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-le v11, v3, :cond_8

    .line 715
    invoke-virtual {v1, v9}, Lcom/android/server/am/MemoryStandardProcessControl;->isForegroundApp(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 716
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 717
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v15, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    move-object/from16 v17, v0

    goto :goto_3

    .line 720
    :cond_5
    iget-object v3, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mAMS:Lcom/android/server/am/ActivityManagerService;

    const/4 v4, 0x0

    const-string v5, "MemoryStandardProcessControl: OutOfHeapStandard"

    invoke-virtual {v3, v9, v13, v4, v5}, Lcom/android/server/am/ActivityManagerService;->forceStopPackage(Ljava/lang/String;IILjava/lang/String;)V

    .line 722
    const-string v8, "#%s n:%s(%d) u:%d pss:%d r:%s"

    const-string v3, "KillProc"

    .line 723
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const-string v16, "OutOfHeapStandard"

    move-object/from16 v4, p2

    move-object/from16 v17, v0

    move-object v0, v8

    .end local v0    # "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .local v17, "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    move-object/from16 v8, v16

    filled-new-array/range {v3 .. v8}, [Ljava/lang/Object;

    move-result-object v3

    .line 722
    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 724
    .local v0, "killReason":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "overHeapkill:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const v4, 0x13c04

    invoke-static {v4, v3}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 725
    const-string v3, "MemoryStandardProcessControl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "over heap kill, processName:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; pid:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 726
    return-void

    .line 716
    .end local v17    # "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .local v0, "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_6
    move-object/from16 v17, v0

    .end local v0    # "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v17    # "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    goto :goto_3

    .line 715
    .end local v17    # "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v0    # "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_7
    move-object/from16 v17, v0

    .end local v0    # "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v17    # "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    goto :goto_3

    .line 714
    .end local v17    # "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v0    # "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_8
    move-object/from16 v17, v0

    .line 718
    .end local v0    # "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v17    # "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :goto_3
    return-void

    .line 708
    .end local v15    # "activeUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v17    # "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :catchall_0
    move-exception v0

    move v3, v12

    move v4, v13

    move v5, v14

    goto :goto_4

    .end local v12    # "uid":I
    .end local v13    # "userId":I
    .end local v14    # "finded":Z
    .restart local v3    # "uid":I
    .restart local v4    # "userId":I
    .restart local v5    # "finded":Z
    :catchall_1
    move-exception v0

    :goto_4
    :try_start_2
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private handleMemPressure(I)V
    .locals 3
    .param p1, "pressureState"    # I

    .line 657
    sget-boolean v0, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z

    const-string v1, "MemoryStandardProcessControl"

    if-eqz v0, :cond_0

    .line 658
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "current pressure is "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 661
    :cond_0
    invoke-direct {p0}, Lcom/android/server/am/MemoryStandardProcessControl;->getMemPressureLevel()I

    move-result v0

    const/4 v2, 0x2

    if-ge v0, v2, :cond_1

    sget-boolean v0, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z

    if-nez v0, :cond_1

    .line 662
    return-void

    .line 664
    :cond_1
    invoke-virtual {p0}, Lcom/android/server/am/MemoryStandardProcessControl;->periodicMemoryDetection()V

    .line 666
    iget-boolean v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mLastMemDetectionHasKilled:Z

    if-eqz v0, :cond_2

    .line 667
    iget-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/am/MemoryControlJobService;->detectionSchedule(Landroid/content/Context;)V

    .line 668
    sget-boolean v0, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z

    if-eqz v0, :cond_2

    .line 669
    const-string v0, "app killed by memory pressure, reschedule detection job"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 672
    :cond_2
    return-void
.end method

.method private initGlobalParams(Landroid/content/Context;Lcom/android/server/am/ActivityManagerService;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ams"    # Lcom/android/server/am/ActivityManagerService;
    .param p3, "index"    # I

    .line 213
    sget-object v0, Lcom/android/server/am/MemoryStandardProcessControl;->sDefaultMemoryStandardArray:[J

    aget-wide v0, v0, p3

    iput-wide v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mDefaultMemoryStandard:J

    .line 214
    invoke-direct {p0, p3}, Lcom/android/server/am/MemoryStandardProcessControl;->loadDefCacheLevelConfig(I)V

    .line 215
    iget-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 216
    new-instance v0, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;

    iget-object v1, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;-><init>(Lcom/android/server/am/MemoryStandardProcessControl;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mHandler:Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;

    .line 217
    iput-object p2, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mAMS:Lcom/android/server/am/ActivityManagerService;

    .line 218
    iget-object v0, p2, Lcom/android/server/am/ActivityManagerService;->mActivityTaskManager:Lcom/android/server/wm/ActivityTaskManagerService;

    iput-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mATMS:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 219
    const-string v0, "ProcessManager"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/ProcessManagerService;

    iput-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mPMS:Lcom/android/server/am/ProcessManagerService;

    .line 220
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    check-cast v0, Landroid/app/ApplicationPackageManager;

    iput-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mPackageManager:Landroid/app/ApplicationPackageManager;

    .line 221
    const-class v0, Lcom/android/server/wm/WindowManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/WindowManagerInternal;

    iput-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mWMS:Lcom/android/server/wm/WindowManagerInternal;

    .line 222
    const-string v0, "audio"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/audio/AudioService;

    iput-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mAudioService:Lcom/android/server/audio/AudioService;

    .line 223
    return-void
.end method

.method private killedReason(Lcom/android/server/am/ProcessRecord;)Ljava/lang/String;
    .locals 6
    .param p1, "pr"    # Lcom/android/server/am/ProcessRecord;

    .line 652
    const-string v0, "KillProc"

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    iget v2, p1, Lcom/android/server/am/ProcessRecord;->mPid:I

    .line 653
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p1, Lcom/android/server/am/ProcessRecord;->mProfile:Lcom/android/server/am/ProcessProfileRecord;

    invoke-virtual {v4}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const-string v5, "OutOfMemoryStandard"

    filled-new-array/range {v0 .. v5}, [Ljava/lang/Object;

    move-result-object v0

    .line 652
    const-string v1, "#%s n:%s(%d) u:%d pss:%d r:%s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private loadDefCacheLevelConfig(I)V
    .locals 4
    .param p1, "memIndex"    # I

    .line 227
    iget-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mPressureCacheThreshold:[I

    sget-object v1, Lcom/android/server/am/MemoryStandardProcessControl;->sDefaultCacheLevel:[[I

    aget-object v1, v1, p1

    const/4 v2, 0x0

    aget v3, v1, v2

    aput v3, v0, v2

    .line 228
    const/4 v2, 0x1

    aget v3, v1, v2

    aput v3, v0, v2

    .line 229
    const/4 v2, 0x2

    aget v1, v1, v2

    aput v1, v0, v2

    .line 230
    return-void
.end method

.method private loadDefaultConfigFromFile()Z
    .locals 9

    .line 1004
    new-instance v0, Ljava/io/File;

    const-string v1, "/product/etc/mspc.default.json"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1005
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 1006
    return v2

    .line 1008
    :cond_0
    const/4 v1, 0x0

    .line 1009
    .local v1, "inputStream":Ljava/io/InputStream;
    const/4 v3, 0x0

    .line 1010
    .local v3, "inputStreamReader":Ljava/io/InputStreamReader;
    const/4 v4, 0x0

    .line 1012
    .local v4, "jsonReader":Landroid/util/JsonReader;
    iget-object v5, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mMemoryStandardMap:Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;

    invoke-virtual {v5}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->clear()V

    .line 1014
    sget-boolean v5, Lmiui/util/DeviceLevel;->IS_MIUI_LITE_VERSION:Z

    if-eqz v5, :cond_1

    const-string v5, "lite"

    goto :goto_0

    :cond_1
    const-string v5, "miui"

    .line 1016
    .local v5, "systemType":Ljava/lang/String;
    :goto_0
    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    move-object v1, v6

    .line 1017
    new-instance v6, Ljava/io/InputStreamReader;

    invoke-direct {v6, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object v3, v6

    .line 1018
    new-instance v6, Landroid/util/JsonReader;

    invoke-direct {v6, v3}, Landroid/util/JsonReader;-><init>(Ljava/io/Reader;)V

    move-object v4, v6

    .line 1020
    invoke-virtual {v4}, Landroid/util/JsonReader;->beginObject()V

    .line 1021
    :goto_1
    invoke-virtual {v4}, Landroid/util/JsonReader;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1022
    invoke-virtual {v4}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v6

    .line 1024
    .local v6, "name":Ljava/lang/String;
    const-string v7, "aging"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1025
    invoke-direct {p0, v4, v5}, Lcom/android/server/am/MemoryStandardProcessControl;->parseConfigAging(Landroid/util/JsonReader;Ljava/lang/String;)V

    goto :goto_2

    .line 1026
    :cond_2
    const-string v7, "common"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1027
    invoke-direct {p0, v4, v5}, Lcom/android/server/am/MemoryStandardProcessControl;->parseConfigCommon(Landroid/util/JsonReader;Ljava/lang/String;)V

    goto :goto_2

    .line 1028
    :cond_3
    const-string v7, "heap"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1029
    invoke-direct {p0, v4, v5}, Lcom/android/server/am/MemoryStandardProcessControl;->parseConfigHeap(Landroid/util/JsonReader;Ljava/lang/String;)V

    goto :goto_2

    .line 1031
    :cond_4
    invoke-virtual {v4}, Landroid/util/JsonReader;->skipValue()V

    .line 1033
    .end local v6    # "name":Ljava/lang/String;
    :goto_2
    goto :goto_1

    .line 1034
    :cond_5
    invoke-virtual {v4}, Landroid/util/JsonReader;->endObject()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1041
    invoke-direct {p0, v4}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V

    .line 1042
    invoke-direct {p0, v3}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V

    .line 1043
    invoke-direct {p0, v1}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V

    .line 1044
    nop

    .line 1045
    const/4 v2, 0x1

    return v2

    .line 1041
    :catchall_0
    move-exception v2

    goto :goto_3

    .line 1035
    :catch_0
    move-exception v6

    .line 1036
    .local v6, "e":Ljava/io/IOException;
    :try_start_1
    sget-boolean v7, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z

    if-eqz v7, :cond_6

    .line 1037
    const-string v7, "MemoryStandardProcessControl"

    const-string v8, "load config from default config file failed, invalid config"

    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1039
    :cond_6
    nop

    .line 1041
    invoke-direct {p0, v4}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V

    .line 1042
    invoke-direct {p0, v3}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V

    .line 1043
    invoke-direct {p0, v1}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V

    .line 1039
    return v2

    .line 1041
    .end local v6    # "e":Ljava/io/IOException;
    :goto_3
    invoke-direct {p0, v4}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V

    .line 1042
    invoke-direct {p0, v3}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V

    .line 1043
    invoke-direct {p0, v1}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V

    .line 1044
    throw v2
.end method

.method private parseConfigAging(Landroid/util/JsonReader;Ljava/lang/String;)V
    .locals 12
    .param p1, "jsonReader"    # Landroid/util/JsonReader;
    .param p2, "systemType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 851
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginArray()V

    .line 852
    :cond_0
    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 853
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V

    .line 854
    const/4 v0, 0x0

    .line 855
    .local v0, "packageName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 856
    .local v1, "parent":Ljava/lang/String;
    const-wide/16 v2, 0x0

    .line 857
    .local v2, "pss":J
    const/4 v4, 0x0

    .line 858
    .local v4, "isNative":Z
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 859
    .local v5, "processes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_1
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 860
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v6

    .line 861
    .local v6, "key":Ljava/lang/String;
    const-string v7, "name"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 863
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 864
    :cond_1
    const-string v7, "processes"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 866
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginArray()V

    .line 867
    :goto_2
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 868
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 870
    :cond_2
    invoke-virtual {p1}, Landroid/util/JsonReader;->endArray()V

    goto :goto_3

    .line 871
    :cond_3
    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 873
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextLong()J

    move-result-wide v2

    goto :goto_3

    .line 874
    :cond_4
    const-string v7, "parent"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 876
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 877
    :cond_5
    const-string v7, "native"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 879
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextBoolean()Z

    move-result v4

    goto :goto_3

    .line 882
    :cond_6
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 884
    .end local v6    # "key":Ljava/lang/String;
    :goto_3
    goto :goto_1

    .line 885
    :cond_7
    invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V

    .line 887
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-eqz v6, :cond_0

    if-nez v0, :cond_8

    .line 888
    goto :goto_0

    .line 890
    :cond_8
    if-eqz v4, :cond_9

    .line 892
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 893
    iget-object v6, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mMemoryStandardMap:Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;

    move-wide v7, v2

    move-object v9, v0

    move-object v10, v5

    move-object v11, v1

    invoke-virtual/range {v6 .. v11}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->updateNativeMemoryStandard(JLjava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    goto :goto_4

    .line 895
    :cond_9
    iget-object v6, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mMemoryStandardMap:Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;

    move-wide v7, v2

    move-object v9, v0

    move-object v10, v5

    move-object v11, v1

    invoke-virtual/range {v6 .. v11}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->updateMemoryStandard(JLjava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    .line 897
    .end local v0    # "packageName":Ljava/lang/String;
    .end local v1    # "parent":Ljava/lang/String;
    .end local v2    # "pss":J
    .end local v4    # "isNative":Z
    .end local v5    # "processes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_4
    goto/16 :goto_0

    .line 898
    :cond_a
    invoke-virtual {p1}, Landroid/util/JsonReader;->endArray()V

    .line 899
    return-void
.end method

.method private parseConfigCommon(Landroid/util/JsonReader;Ljava/lang/String;)V
    .locals 13
    .param p1, "jsonReader"    # Landroid/util/JsonReader;
    .param p2, "systemType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 903
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V

    .line 904
    const-wide/16 v0, 0x0

    .line 905
    .local v0, "pss":J
    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 906
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v8

    .line 907
    .local v8, "key":Ljava/lang/String;
    invoke-virtual {v8, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 909
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextLong()J

    move-result-wide v0

    goto/16 :goto_6

    .line 910
    :cond_0
    const-string v2, "apps"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 911
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginArray()V

    .line 912
    :cond_1
    :goto_1
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 913
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V

    .line 914
    const/4 v2, 0x0

    .line 915
    .local v2, "packageName":Ljava/lang/String;
    const/4 v3, 0x0

    .line 916
    .local v3, "parent":Ljava/lang/String;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object v9, v4

    .line 917
    .local v9, "processes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v4, 0x0

    move-object v10, v2

    move-object v11, v3

    move v12, v4

    .line 918
    .end local v2    # "packageName":Ljava/lang/String;
    .end local v3    # "parent":Ljava/lang/String;
    .local v10, "packageName":Ljava/lang/String;
    .local v11, "parent":Ljava/lang/String;
    .local v12, "isNative":Z
    :goto_2
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 919
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v2

    .line 920
    .local v2, "pKey":Ljava/lang/String;
    const-string v3, "name"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 922
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v3

    move-object v10, v3

    .end local v10    # "packageName":Ljava/lang/String;
    .local v3, "packageName":Ljava/lang/String;
    goto :goto_4

    .line 923
    .end local v3    # "packageName":Ljava/lang/String;
    .restart local v10    # "packageName":Ljava/lang/String;
    :cond_2
    const-string v3, "processes"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 925
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginArray()V

    .line 926
    :goto_3
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 927
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v9, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 929
    :cond_3
    invoke-virtual {p1}, Landroid/util/JsonReader;->endArray()V

    goto :goto_4

    .line 930
    :cond_4
    const-string v3, "native"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 931
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextBoolean()Z

    move-result v3

    move v12, v3

    .end local v12    # "isNative":Z
    .local v3, "isNative":Z
    goto :goto_4

    .line 932
    .end local v3    # "isNative":Z
    .restart local v12    # "isNative":Z
    :cond_5
    const-string v3, "parent"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 933
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v3

    move-object v11, v3

    .end local v11    # "parent":Ljava/lang/String;
    .local v3, "parent":Ljava/lang/String;
    goto :goto_4

    .line 935
    .end local v3    # "parent":Ljava/lang/String;
    .restart local v11    # "parent":Ljava/lang/String;
    :cond_6
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 937
    .end local v2    # "pKey":Ljava/lang/String;
    :goto_4
    goto :goto_2

    .line 939
    :cond_7
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    if-nez v10, :cond_8

    .line 940
    goto :goto_1

    .line 942
    :cond_8
    if-eqz v12, :cond_9

    .line 944
    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 945
    iget-object v2, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mMemoryStandardMap:Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;

    move-wide v3, v0

    move-object v5, v10

    move-object v6, v9

    move-object v7, v11

    invoke-virtual/range {v2 .. v7}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->updateNativeMemoryStandard(JLjava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    goto :goto_5

    .line 947
    :cond_9
    iget-object v2, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mMemoryStandardMap:Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;

    move-wide v3, v0

    move-object v5, v10

    move-object v6, v9

    move-object v7, v11

    invoke-virtual/range {v2 .. v7}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->updateMemoryStandard(JLjava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    .line 949
    :goto_5
    invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V

    .line 950
    .end local v9    # "processes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v10    # "packageName":Ljava/lang/String;
    .end local v11    # "parent":Ljava/lang/String;
    .end local v12    # "isNative":Z
    goto/16 :goto_1

    .line 951
    :cond_a
    invoke-virtual {p1}, Landroid/util/JsonReader;->endArray()V

    goto :goto_6

    .line 953
    :cond_b
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 955
    .end local v8    # "key":Ljava/lang/String;
    :goto_6
    goto/16 :goto_0

    .line 956
    :cond_c
    invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V

    .line 957
    return-void
.end method

.method private parseConfigHeap(Landroid/util/JsonReader;Ljava/lang/String;)V
    .locals 4
    .param p1, "jsonReader"    # Landroid/util/JsonReader;
    .param p2, "systemType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 960
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginArray()V

    .line 961
    :cond_0
    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 962
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V

    .line 963
    const/4 v0, 0x0

    .line 964
    .local v0, "packageName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 965
    .local v1, "appHeap":I
    :goto_1
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 966
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v2

    .line 967
    .local v2, "key":Ljava/lang/String;
    const-string v3, "name"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 969
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 970
    :cond_1
    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 972
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v1

    goto :goto_2

    .line 975
    :cond_2
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 977
    .end local v2    # "key":Ljava/lang/String;
    :goto_2
    goto :goto_1

    .line 978
    :cond_3
    invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V

    .line 980
    if-eqz v1, :cond_0

    if-nez v0, :cond_4

    .line 981
    goto :goto_0

    .line 983
    :cond_4
    iget-object v2, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapStandard:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 984
    .end local v0    # "packageName":Ljava/lang/String;
    .end local v1    # "appHeap":I
    goto :goto_0

    .line 985
    :cond_5
    invoke-virtual {p1}, Landroid/util/JsonReader;->endArray()V

    .line 986
    return-void
.end method

.method private parseConfigRemove(Landroid/util/JsonReader;)V
    .locals 2
    .param p1, "jsonReader"    # Landroid/util/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 991
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginArray()V

    .line 992
    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 993
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v0

    .line 995
    .local v0, "packageName":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mMemoryStandardMap:Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;

    invoke-virtual {v1, v0}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->removeMemoryStandard(Ljava/lang/String;)V

    .line 997
    iget-object v1, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mWarnedAppMap:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 998
    .end local v0    # "packageName":Ljava/lang/String;
    goto :goto_0

    .line 999
    :cond_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->endArray()V

    .line 1000
    return-void
.end method

.method private resetRecordDataIfNeeded()V
    .locals 8

    .line 553
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 555
    .local v0, "now":J
    iget-object v2, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mWarnedAppMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;

    .line 556
    .local v3, "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
    iget-wide v4, v3, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mLastKillTime:J

    sub-long v4, v0, v4

    const-wide/32 v6, 0x5265c00

    cmp-long v4, v4, v6

    if-lez v4, :cond_0

    iget v4, v3, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mKillTimes:I

    const/4 v5, 0x3

    if-le v4, v5, :cond_0

    .line 558
    const/4 v4, 0x0

    iput v4, v3, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mKillTimes:I

    .line 559
    sget-boolean v4, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z

    if-eqz v4, :cond_0

    .line 560
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v3, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " long time never killed, reset kill times"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "MemoryStandardProcessControl"

    invoke-static {v5, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 563
    .end local v3    # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
    :cond_0
    goto :goto_0

    .line 564
    :cond_1
    return-void
.end method


# virtual methods
.method public callKillProcess(Z)V
    .locals 2
    .param p1, "killHighPriority"    # Z

    .line 774
    if-eqz p1, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :cond_0
    const/4 v0, 0x2

    .line 775
    .local v0, "msgId":I
    :goto_0
    iget-object v1, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mHandler:Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;

    invoke-virtual {v1, v0}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->removeMessages(I)V

    .line 776
    iget-object v1, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mHandler:Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;

    invoke-virtual {v1, v0}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 777
    return-void
.end method

.method public callPeriodicDetection()V
    .locals 2

    .line 768
    iget-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mHandler:Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->removeMessages(I)V

    .line 769
    iget-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mHandler:Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;

    invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 770
    return-void
.end method

.method public callRunnable(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "callback"    # Ljava/lang/Runnable;

    .line 790
    iget-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mHandler:Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;

    invoke-virtual {v0, p1}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->post(Ljava/lang/Runnable;)Z

    .line 791
    return-void
.end method

.method public callUpdateCloudAppHeapConfig(Ljava/lang/String;)V
    .locals 0
    .param p1, "cloudStr"    # Ljava/lang/String;

    .line 786
    invoke-virtual {p0, p1}, Lcom/android/server/am/MemoryStandardProcessControl;->loadAppHeapConfigFromCloud(Ljava/lang/String;)V

    .line 787
    return-void
.end method

.method public callUpdateCloudConfig(Ljava/lang/String;)V
    .locals 0
    .param p1, "cloudStr"    # Ljava/lang/String;

    .line 781
    invoke-virtual {p0, p1}, Lcom/android/server/am/MemoryStandardProcessControl;->loadConfigFromCloud(Ljava/lang/String;)V

    .line 782
    return-void
.end method

.method public init(Landroid/content/Context;Lcom/android/server/am/ActivityManagerService;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ams"    # Lcom/android/server/am/ActivityManagerService;

    .line 162
    iget-boolean v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mEnable:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mIsInit:Z

    if-eqz v0, :cond_0

    goto/16 :goto_1

    .line 166
    :cond_0
    invoke-direct {p0}, Lcom/android/server/am/MemoryStandardProcessControl;->loadDefaultConfigFromFile()Z

    move-result v0

    if-nez v0, :cond_1

    .line 167
    iput-boolean v1, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mEnable:Z

    .line 168
    return v1

    .line 170
    :cond_1
    iput-object p1, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mContext:Landroid/content/Context;

    .line 171
    invoke-static {}, Landroid/os/Process;->getTotalMemory()J

    move-result-wide v0

    const-wide/32 v2, 0x40000000

    div-long/2addr v0, v2

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    long-to-int v0, v0

    .line 172
    .local v0, "totalMemGB":I
    const/4 v1, 0x0

    .line 173
    .local v1, "index":I
    const/16 v2, 0xc

    const/4 v3, 0x1

    if-le v0, v2, :cond_2

    .line 175
    sget-object v2, Lcom/android/server/am/MemoryStandardProcessControl;->sDefaultMemoryStandardArray:[J

    array-length v2, v2

    sub-int/2addr v2, v3

    .end local v1    # "index":I
    .local v2, "index":I
    goto :goto_0

    .line 176
    .end local v2    # "index":I
    .restart local v1    # "index":I
    :cond_2
    const/4 v2, 0x4

    if-le v0, v2, :cond_3

    .line 178
    div-int/lit8 v2, v0, 0x2

    .end local v1    # "index":I
    .restart local v2    # "index":I
    goto :goto_0

    .line 181
    .end local v2    # "index":I
    .restart local v1    # "index":I
    :cond_3
    add-int/lit8 v2, v0, -0x2

    .line 183
    .end local v1    # "index":I
    .restart local v2    # "index":I
    :goto_0
    invoke-direct {p0, p1, p2, v2}, Lcom/android/server/am/MemoryStandardProcessControl;->initGlobalParams(Landroid/content/Context;Lcom/android/server/am/ActivityManagerService;I)V

    .line 184
    new-instance v1, Lcom/android/server/am/MemoryControlCloud;

    invoke-direct {v1}, Lcom/android/server/am/MemoryControlCloud;-><init>()V

    .line 185
    .local v1, "cloudControl":Lcom/android/server/am/MemoryControlCloud;
    invoke-virtual {v1, p1, p0}, Lcom/android/server/am/MemoryControlCloud;->initCloud(Landroid/content/Context;Lcom/android/server/am/MemoryStandardProcessControl;)Z

    .line 188
    iget-boolean v4, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenDetection:Z

    if-eqz v4, :cond_4

    .line 189
    new-instance v4, Landroid/content/IntentFilter;

    invoke-direct {v4}, Landroid/content/IntentFilter;-><init>()V

    .line 190
    .local v4, "filter":Landroid/content/IntentFilter;
    const-string v5, "android.intent.action.SCREEN_ON"

    invoke-virtual {v4, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 191
    const-string v5, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v4, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 192
    iget-object v5, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v5, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 194
    .end local v4    # "filter":Landroid/content/IntentFilter;
    :cond_4
    sget-boolean v4, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z

    if-eqz v4, :cond_5

    .line 195
    invoke-direct {p0}, Lcom/android/server/am/MemoryStandardProcessControl;->debugModifyKillStrategy()V

    .line 199
    :cond_5
    iget-object v4, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/android/server/am/MemoryControlJobService;->detectionSchedule(Landroid/content/Context;)V

    .line 200
    iget-object v4, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/android/server/am/MemoryControlJobService;->killSchedule(Landroid/content/Context;)V

    .line 201
    new-instance v4, Lcom/android/server/am/MemoryStandardProcessControl$BinderService;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/android/server/am/MemoryStandardProcessControl$BinderService;-><init>(Lcom/android/server/am/MemoryStandardProcessControl;Lcom/android/server/am/MemoryStandardProcessControl$BinderService-IA;)V

    const-string v5, "mspc"

    invoke-static {v5, v4}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 203
    sget-boolean v4, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z

    if-eqz v4, :cond_6

    .line 204
    const-string v4, "MemoryStandardProcessControl"

    const-string v5, "init mspc2.0 success"

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    :cond_6
    iput-boolean v3, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mIsInit:Z

    .line 208
    return v3

    .line 163
    .end local v0    # "totalMemGB":I
    .end local v1    # "cloudControl":Lcom/android/server/am/MemoryControlCloud;
    .end local v2    # "index":I
    :cond_7
    :goto_1
    return v1
.end method

.method public isEnable()Z
    .locals 1

    .line 157
    iget-boolean v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mEnable:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mIsInit:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isForegroundApp(Ljava/lang/String;)Z
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .line 539
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mATMS:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskManagerService;->getFocusedRootTaskInfo()Landroid/app/ActivityTaskManager$RootTaskInfo;

    move-result-object v1

    .line 540
    .local v1, "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    if-eqz v1, :cond_2

    iget-object v2, v1, Landroid/app/ActivityTaskManager$RootTaskInfo;->topActivity:Landroid/content/ComponentName;

    if-nez v2, :cond_0

    goto :goto_0

    .line 544
    :cond_0
    iget-object v2, v1, Landroid/app/ActivityTaskManager$RootTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 545
    .local v2, "topPackageName":Ljava/lang/String;
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v0, 0x1

    return v0

    .line 548
    .end local v1    # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    .end local v2    # "topPackageName":Ljava/lang/String;
    :cond_1
    goto :goto_1

    .line 541
    .restart local v1    # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    :cond_2
    :goto_0
    const-string v2, "MemoryStandardProcessControl"

    const-string v3, "get getFocusedStackInfo error."

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 542
    return v0

    .line 546
    .end local v1    # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    :catch_0
    move-exception v1

    .line 549
    :goto_1
    return v0
.end method

.method public killProcess(Z)V
    .locals 9
    .param p1, "canKillhighPriorityProcess"    # Z

    .line 396
    invoke-virtual {p0}, Lcom/android/server/am/MemoryStandardProcessControl;->isEnable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 397
    return-void

    .line 399
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "canKillhighPriorityProcess: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MemoryStandardProcessControl"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 400
    if-eqz p1, :cond_1

    .line 401
    iget v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenState:I

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    .line 402
    const-string v0, "screen on when detect, skip"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    return-void

    .line 406
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mTriggerKill:Z

    .line 408
    :cond_2
    invoke-direct {p0}, Lcom/android/server/am/MemoryStandardProcessControl;->getActiveUids()Ljava/util/List;

    move-result-object v0

    .line 409
    .local v0, "activeUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-direct {p0}, Lcom/android/server/am/MemoryStandardProcessControl;->getVisibleWindowOwner()Ljava/util/List;

    move-result-object v2

    .line 411
    .local v2, "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 412
    .local v3, "deletePackageNameSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mWarnedAppMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;

    .line 413
    .local v5, "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
    iget v6, v5, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mMemExcelTime:I

    const/4 v7, 0x3

    if-ge v6, v7, :cond_3

    .line 414
    goto :goto_0

    .line 416
    :cond_3
    iget-object v6, v5, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mPackageName:Ljava/lang/String;

    .line 418
    .local v6, "packageName":Ljava/lang/String;
    invoke-direct {p0, p1, v5, v0, v2}, Lcom/android/server/am/MemoryStandardProcessControl;->canKill(ZLcom/android/server/am/MemoryStandardProcessControl$AppInfo;Ljava/util/List;Ljava/util/List;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 419
    goto :goto_0

    .line 421
    :cond_4
    sget-boolean v7, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z

    if-eqz v7, :cond_5

    .line 422
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "add to the kill list, packageName:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v5, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "; mMemExcelTime:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v5, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mMemExcelTime:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    :cond_5
    invoke-interface {v3, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 426
    .end local v5    # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
    .end local v6    # "packageName":Ljava/lang/String;
    goto :goto_0

    .line 428
    :cond_6
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v1

    if-lez v1, :cond_7

    .line 429
    invoke-direct {p0, v3, p1}, Lcom/android/server/am/MemoryStandardProcessControl;->doClean(Ljava/util/Set;Z)V

    .line 432
    :cond_7
    invoke-direct {p0}, Lcom/android/server/am/MemoryStandardProcessControl;->resetRecordDataIfNeeded()V

    .line 433
    return-void
.end method

.method public loadAppHeapConfigFromCloud(Ljava/lang/String;)V
    .locals 6
    .param p1, "str"    # Ljava/lang/String;

    .line 1085
    const/4 v0, 0x0

    .line 1086
    .local v0, "stringReader":Ljava/io/StringReader;
    const/4 v1, 0x0

    .line 1087
    .local v1, "jsonReader":Landroid/util/JsonReader;
    sget-boolean v2, Lmiui/util/DeviceLevel;->IS_MIUI_LITE_VERSION:Z

    if-eqz v2, :cond_0

    const-string v2, "lite"

    goto :goto_0

    :cond_0
    const-string v2, "miui"

    .line 1089
    .local v2, "systemType":Ljava/lang/String;
    :goto_0
    iget-object v3, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapStandard:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    .line 1091
    iget-object v3, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mHandler:Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;

    const/4 v4, 0x7

    invoke-virtual {v3, v4}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->removeMessages(I)V

    .line 1093
    :try_start_0
    new-instance v3, Ljava/io/StringReader;

    invoke-direct {v3, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    move-object v0, v3

    .line 1094
    new-instance v3, Landroid/util/JsonReader;

    invoke-direct {v3, v0}, Landroid/util/JsonReader;-><init>(Ljava/io/Reader;)V

    move-object v1, v3

    .line 1095
    invoke-virtual {v1}, Landroid/util/JsonReader;->beginObject()V

    .line 1096
    :goto_1
    invoke-virtual {v1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1097
    invoke-virtual {v1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v3

    .line 1098
    .local v3, "name":Ljava/lang/String;
    const-string v4, "heap"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1100
    invoke-direct {p0, v1, v2}, Lcom/android/server/am/MemoryStandardProcessControl;->parseConfigHeap(Landroid/util/JsonReader;Ljava/lang/String;)V

    goto :goto_2

    .line 1102
    :cond_1
    invoke-virtual {v1}, Landroid/util/JsonReader;->skipValue()V

    .line 1104
    .end local v3    # "name":Ljava/lang/String;
    :goto_2
    goto :goto_1

    .line 1105
    :cond_2
    invoke-virtual {v1}, Landroid/util/JsonReader;->endObject()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 1113
    :catchall_0
    move-exception v3

    goto :goto_4

    .line 1106
    :catch_0
    move-exception v3

    .line 1108
    .local v3, "e":Ljava/io/IOException;
    :try_start_1
    sget-boolean v4, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z

    if-eqz v4, :cond_3

    .line 1109
    const-string v4, "MemoryStandardProcessControl"

    const-string v5, "load app heap config from cloud or database failed, invalid config"

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1111
    :cond_3
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapEnable:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1113
    .end local v3    # "e":Ljava/io/IOException;
    :goto_3
    invoke-direct {p0, v1}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V

    .line 1114
    invoke-direct {p0, v0}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V

    .line 1115
    nop

    .line 1116
    return-void

    .line 1113
    :goto_4
    invoke-direct {p0, v1}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V

    .line 1114
    invoke-direct {p0, v0}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V

    .line 1115
    throw v3
.end method

.method public loadConfigFromCloud(Ljava/lang/String;)V
    .locals 6
    .param p1, "str"    # Ljava/lang/String;

    .line 1049
    const/4 v0, 0x0

    .line 1050
    .local v0, "stringReader":Ljava/io/StringReader;
    const/4 v1, 0x0

    .line 1051
    .local v1, "jsonReader":Landroid/util/JsonReader;
    sget-boolean v2, Lmiui/util/DeviceLevel;->IS_MIUI_LITE_VERSION:Z

    if-eqz v2, :cond_0

    const-string v2, "lite"

    goto :goto_0

    :cond_0
    const-string v2, "miui"

    .line 1053
    .local v2, "systemType":Ljava/lang/String;
    :goto_0
    :try_start_0
    new-instance v3, Ljava/io/StringReader;

    invoke-direct {v3, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    move-object v0, v3

    .line 1054
    new-instance v3, Landroid/util/JsonReader;

    invoke-direct {v3, v0}, Landroid/util/JsonReader;-><init>(Ljava/io/Reader;)V

    move-object v1, v3

    .line 1055
    invoke-virtual {v1}, Landroid/util/JsonReader;->beginObject()V

    .line 1056
    :goto_1
    invoke-virtual {v1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1057
    invoke-virtual {v1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v3

    .line 1058
    .local v3, "name":Ljava/lang/String;
    const-string v4, "aging"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1060
    invoke-direct {p0, v1, v2}, Lcom/android/server/am/MemoryStandardProcessControl;->parseConfigAging(Landroid/util/JsonReader;Ljava/lang/String;)V

    goto :goto_2

    .line 1061
    :cond_1
    const-string v4, "common"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1063
    invoke-direct {p0, v1, v2}, Lcom/android/server/am/MemoryStandardProcessControl;->parseConfigCommon(Landroid/util/JsonReader;Ljava/lang/String;)V

    goto :goto_2

    .line 1064
    :cond_2
    const-string v4, "remove"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1066
    invoke-direct {p0, v1}, Lcom/android/server/am/MemoryStandardProcessControl;->parseConfigRemove(Landroid/util/JsonReader;)V

    goto :goto_2

    .line 1068
    :cond_3
    invoke-virtual {v1}, Landroid/util/JsonReader;->skipValue()V

    .line 1070
    .end local v3    # "name":Ljava/lang/String;
    :goto_2
    goto :goto_1

    .line 1071
    :cond_4
    invoke-virtual {v1}, Landroid/util/JsonReader;->endObject()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 1079
    :catchall_0
    move-exception v3

    goto :goto_4

    .line 1072
    :catch_0
    move-exception v3

    .line 1074
    .local v3, "e":Ljava/io/IOException;
    :try_start_1
    sget-boolean v4, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z

    if-eqz v4, :cond_5

    .line 1075
    const-string v4, "MemoryStandardProcessControl"

    const-string v5, "load config from cloud or database failed, invalid config"

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1077
    :cond_5
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mEnable:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1079
    .end local v3    # "e":Ljava/io/IOException;
    :goto_3
    invoke-direct {p0, v1}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V

    .line 1080
    invoke-direct {p0, v0}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V

    .line 1081
    nop

    .line 1082
    return-void

    .line 1079
    :goto_4
    invoke-direct {p0, v1}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V

    .line 1080
    invoke-direct {p0, v0}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V

    .line 1081
    throw v3
.end method

.method public periodicMemoryDetection()V
    .locals 20

    .line 272
    move-object/from16 v7, p0

    invoke-virtual/range {p0 .. p0}, Lcom/android/server/am/MemoryStandardProcessControl;->isEnable()Z

    move-result v0

    if-eqz v0, :cond_15

    iget-object v0, v7, Lcom/android/server/am/MemoryStandardProcessControl;->mMemoryStandardMap:Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;

    invoke-virtual {v0}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_a

    .line 275
    :cond_0
    sget-boolean v0, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 276
    const-string v0, "MemoryStandardProcessControl"

    const-string/jumbo v1, "start memory detection"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, v7, Lcom/android/server/am/MemoryStandardProcessControl;->mLastMemDetectionHasKilled:Z

    .line 280
    iget-object v1, v7, Lcom/android/server/am/MemoryStandardProcessControl;->mWarnedAppMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;

    .line 282
    .local v2, "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
    iget-object v3, v2, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mProcessMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 283
    const/16 v3, 0x3e9

    iput v3, v2, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mAdj:I

    .line 284
    .end local v2    # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
    goto :goto_0

    .line 286
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object v8, v1

    .line 287
    .local v8, "procs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;"
    const/4 v1, 0x0

    .line 288
    .local v1, "N":I
    iget-object v2, v7, Lcom/android/server/am/MemoryStandardProcessControl;->mAMS:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v2

    .line 289
    :try_start_0
    iget-object v3, v7, Lcom/android/server/am/MemoryStandardProcessControl;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    invoke-virtual {v3}, Lcom/android/server/am/ProcessList;->getLruProcessesLOSP()Ljava/util/ArrayList;

    move-result-object v3

    .line 290
    .local v3, "lruProcs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move v9, v4

    .line 291
    .end local v1    # "N":I
    .local v9, "N":I
    add-int/lit8 v1, v9, -0x1

    .local v1, "i":I
    :goto_1
    if-ltz v1, :cond_3

    .line 292
    :try_start_1
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/am/ProcessRecord;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 291
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 294
    .end local v1    # "i":I
    .end local v3    # "lruProcs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;"
    :cond_3
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 297
    add-int/lit8 v1, v9, -0x1

    move v10, v1

    .local v10, "i":I
    :goto_2
    const-wide/16 v11, 0x400

    const/4 v13, 0x0

    if-ltz v10, :cond_b

    .line 298
    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/android/server/am/ProcessRecord;

    .line 299
    .local v14, "app":Lcom/android/server/am/ProcessRecord;
    iget-object v1, v14, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I

    move-result v1

    if-gtz v1, :cond_4

    .line 300
    goto/16 :goto_4

    .line 302
    :cond_4
    invoke-virtual {v14}, Lcom/android/server/am/ProcessRecord;->isKilledByAm()Z

    move-result v1

    if-nez v1, :cond_a

    invoke-virtual {v14}, Lcom/android/server/am/ProcessRecord;->isKilled()Z

    move-result v1

    if-nez v1, :cond_a

    .line 303
    invoke-virtual {v14}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v1

    if-eqz v1, :cond_a

    iget-boolean v1, v14, Lcom/android/server/am/ProcessRecord;->isolated:Z

    if-eqz v1, :cond_5

    .line 304
    goto/16 :goto_4

    .line 306
    :cond_5
    iget-object v1, v14, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v15, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 307
    .local v15, "packageName":Ljava/lang/String;
    iget-object v1, v7, Lcom/android/server/am/MemoryStandardProcessControl;->mMemoryStandardMap:Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;

    invoke-virtual {v1, v15}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->getStandard(Ljava/lang/String;)Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;

    move-result-object v6

    .line 309
    .local v6, "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
    if-eqz v6, :cond_9

    .line 310
    iget-object v5, v14, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 312
    .local v5, "processName":Ljava/lang/String;
    iget-object v1, v6, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;->mProcesses:Ljava/util/Set;

    invoke-interface {v1, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 313
    goto/16 :goto_4

    .line 315
    :cond_6
    iget-object v1, v7, Lcom/android/server/am/MemoryStandardProcessControl;->mWarnedAppMap:Ljava/util/HashMap;

    invoke-virtual {v1, v15}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;

    .line 316
    .local v16, "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
    if-nez v16, :cond_7

    .line 317
    new-instance v17, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;

    iget v4, v14, Lcom/android/server/am/ProcessRecord;->uid:I

    iget v3, v14, Lcom/android/server/am/ProcessRecord;->userId:I

    iget-object v1, v14, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    .line 318
    invoke-virtual {v1}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I

    move-result v18

    move-object/from16 v1, v17

    move-object/from16 v2, p0

    move/from16 v19, v3

    move-object v3, v15

    move-object v0, v5

    .end local v5    # "processName":Ljava/lang/String;
    .local v0, "processName":Ljava/lang/String;
    move/from16 v5, v19

    move-object/from16 v19, v6

    .end local v6    # "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
    .local v19, "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
    move/from16 v6, v18

    invoke-direct/range {v1 .. v6}, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;-><init>(Lcom/android/server/am/MemoryStandardProcessControl;Ljava/lang/String;III)V

    .line 319
    .end local v16    # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
    .local v1, "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
    iget-object v2, v7, Lcom/android/server/am/MemoryStandardProcessControl;->mWarnedAppMap:Ljava/util/HashMap;

    invoke-virtual {v2, v15, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 316
    .end local v0    # "processName":Ljava/lang/String;
    .end local v1    # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
    .end local v19    # "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
    .restart local v5    # "processName":Ljava/lang/String;
    .restart local v6    # "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
    .restart local v16    # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
    :cond_7
    move-object v0, v5

    move-object/from16 v19, v6

    .end local v5    # "processName":Ljava/lang/String;
    .end local v6    # "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
    .restart local v0    # "processName":Ljava/lang/String;
    .restart local v19    # "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
    move-object/from16 v1, v16

    .line 321
    .end local v16    # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
    .restart local v1    # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
    :goto_3
    iget v2, v14, Lcom/android/server/am/ProcessRecord;->mPid:I

    invoke-static {v2, v13, v13}, Landroid/os/Debug;->getPss(I[J[J)J

    move-result-wide v2

    div-long/2addr v2, v11

    .line 322
    .local v2, "appPss":J
    iget-object v4, v1, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mProcessMap:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 324
    iget-object v4, v14, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v4}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I

    move-result v4

    .line 325
    .local v4, "newAdj":I
    iget v5, v1, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mAdj:I

    if-le v5, v4, :cond_8

    .line 326
    iput v4, v1, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mAdj:I

    .line 328
    :cond_8
    sget-boolean v5, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z

    if-eqz v5, :cond_a

    .line 329
    const-string v5, "MemoryStandardProcessControl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "package = "

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v11, ", process = "

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v11, ", adj = "

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v11, v1, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mAdj:I

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 309
    .end local v0    # "processName":Ljava/lang/String;
    .end local v1    # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
    .end local v2    # "appPss":J
    .end local v4    # "newAdj":I
    .end local v19    # "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
    .restart local v6    # "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
    :cond_9
    move-object/from16 v19, v6

    .line 297
    .end local v6    # "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
    .end local v14    # "app":Lcom/android/server/am/ProcessRecord;
    .end local v15    # "packageName":Ljava/lang/String;
    :cond_a
    :goto_4
    add-int/lit8 v10, v10, -0x1

    const/4 v0, 0x0

    goto/16 :goto_2

    .line 336
    .end local v10    # "i":I
    :cond_b
    iget-object v0, v7, Lcom/android/server/am/MemoryStandardProcessControl;->mMemoryStandardMap:Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;

    invoke-virtual {v0}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->getNativeProcessCmds()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 337
    .local v0, "nativeProcIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_e

    .line 338
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Ljava/lang/String;

    .line 339
    .local v10, "cmd":Ljava/lang/String;
    filled-new-array {v10}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/os/Process;->getPidsForCommands([Ljava/lang/String;)[I

    move-result-object v14

    .line 340
    .local v14, "pids":[I
    if-eqz v14, :cond_d

    array-length v1, v14

    if-lez v1, :cond_d

    .line 342
    const/4 v1, 0x0

    aget v15, v14, v1

    .line 344
    .local v15, "pid":I
    invoke-static {v15, v13, v13}, Landroid/os/Debug;->getPss(I[J[J)J

    move-result-wide v1

    div-long v16, v1, v11

    .line 345
    .local v16, "appPss":J
    iget-object v1, v7, Lcom/android/server/am/MemoryStandardProcessControl;->mWarnedAppMap:Ljava/util/HashMap;

    invoke-virtual {v1, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;

    .line 346
    .local v18, "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
    if-nez v18, :cond_c

    .line 347
    new-instance v19, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;

    const/16 v4, 0x3e8

    const/4 v5, 0x0

    const/16 v6, -0x3e8

    move-object/from16 v1, v19

    move-object/from16 v2, p0

    move-object v3, v10

    invoke-direct/range {v1 .. v6}, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;-><init>(Lcom/android/server/am/MemoryStandardProcessControl;Ljava/lang/String;III)V

    .line 348
    .end local v18    # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
    .restart local v1    # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
    iget-object v2, v7, Lcom/android/server/am/MemoryStandardProcessControl;->mWarnedAppMap:Ljava/util/HashMap;

    invoke-virtual {v2, v10, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_6

    .line 346
    .end local v1    # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
    .restart local v18    # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
    :cond_c
    move-object/from16 v1, v18

    .line 350
    .end local v18    # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
    .restart local v1    # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
    :goto_6
    const/16 v2, -0x3e8

    iput v2, v1, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mAdj:I

    .line 351
    iget-object v2, v1, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mProcessMap:Ljava/util/Map;

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v10, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 352
    sget-boolean v2, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z

    if-eqz v2, :cond_d

    .line 353
    const-string v2, "MemoryStandardProcessControl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "package = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", process = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", adj = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mAdj:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    .end local v1    # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
    .end local v10    # "cmd":Ljava/lang/String;
    .end local v14    # "pids":[I
    .end local v15    # "pid":I
    .end local v16    # "appPss":J
    :cond_d
    goto/16 :goto_5

    .line 360
    :cond_e
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 361
    .local v1, "now":J
    iget-object v3, v7, Lcom/android/server/am/MemoryStandardProcessControl;->mWarnedAppMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_13

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;

    .line 362
    .local v4, "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
    iget-object v5, v4, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mPackageName:Ljava/lang/String;

    .line 363
    .local v5, "packageName":Ljava/lang/String;
    iget v6, v4, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mKillTimes:I

    const/4 v10, 0x3

    if-le v6, v10, :cond_f

    sget-boolean v6, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z

    if-nez v6, :cond_f

    .line 365
    const-string v6, "MemoryStandardProcessControl"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " frequently killed, skip"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v6, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    goto :goto_7

    .line 368
    :cond_f
    invoke-virtual {v4}, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->getTotalPss()I

    move-result v6

    int-to-long v11, v6

    .line 369
    .local v11, "totalPss":J
    iget-object v6, v7, Lcom/android/server/am/MemoryStandardProcessControl;->mMemoryStandardMap:Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;

    invoke-virtual {v6, v5}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->getPss(Ljava/lang/String;)J

    move-result-wide v13

    .line 370
    .local v13, "standardPss":J
    cmp-long v6, v11, v13

    const/4 v15, 0x1

    if-lez v6, :cond_11

    .line 371
    iget v6, v4, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mMemExcelTime:I

    add-int/2addr v6, v15

    iput v6, v4, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mMemExcelTime:I

    .line 372
    iget v6, v4, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mMemExcelTime:I

    if-lt v6, v10, :cond_10

    .line 373
    iput-boolean v15, v7, Lcom/android/server/am/MemoryStandardProcessControl;->mTriggerKill:Z

    .line 376
    iget v6, v4, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mKillTimes:I

    add-int/2addr v6, v15

    iput v6, v4, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mKillTimes:I

    .line 377
    iput-wide v1, v4, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mLastKillTime:J

    .line 379
    :cond_10
    const-string v6, "MemoryStandardProcessControl"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v15, " is out of standard, pss is "

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v6, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    .line 380
    :cond_11
    iget v6, v4, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mMemExcelTime:I

    if-lez v6, :cond_12

    .line 382
    iget v6, v4, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mMemExcelTime:I

    sub-int/2addr v6, v15

    iput v6, v4, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mMemExcelTime:I

    .line 384
    .end local v4    # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
    .end local v5    # "packageName":Ljava/lang/String;
    .end local v11    # "totalPss":J
    .end local v13    # "standardPss":J
    :cond_12
    :goto_8
    goto :goto_7

    .line 386
    :cond_13
    iget-boolean v3, v7, Lcom/android/server/am/MemoryStandardProcessControl;->mTriggerKill:Z

    if-eqz v3, :cond_14

    .line 387
    const/4 v3, 0x0

    invoke-virtual {v7, v3}, Lcom/android/server/am/MemoryStandardProcessControl;->killProcess(Z)V

    .line 389
    :cond_14
    return-void

    .line 294
    .end local v0    # "nativeProcIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v1    # "now":J
    :catchall_0
    move-exception v0

    move v1, v9

    goto :goto_9

    .end local v9    # "N":I
    .local v1, "N":I
    :catchall_1
    move-exception v0

    :goto_9
    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 273
    .end local v1    # "N":I
    .end local v8    # "procs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;"
    :cond_15
    :goto_a
    return-void
.end method

.method public reportAppResumed(ILjava/lang/String;)V
    .locals 2
    .param p1, "pid"    # I
    .param p2, "packageName"    # Ljava/lang/String;

    .line 758
    invoke-virtual {p0}, Lcom/android/server/am/MemoryStandardProcessControl;->isEnable()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapEnable:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 761
    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapStandard:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 762
    iget-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mHandler:Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1, p2}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->removeEqualMessages(ILjava/lang/Object;)V

    .line 764
    :cond_1
    return-void

    .line 759
    :cond_2
    :goto_0
    return-void
.end method

.method public reportAppStopped(ILjava/lang/String;)V
    .locals 4
    .param p1, "pid"    # I
    .param p2, "packageName"    # Ljava/lang/String;

    .line 746
    invoke-virtual {p0}, Lcom/android/server/am/MemoryStandardProcessControl;->isEnable()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapEnable:Z

    if-nez v0, :cond_0

    goto :goto_1

    .line 749
    :cond_0
    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapStandard:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 750
    iget-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mHandler:Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1, p2}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->removeEqualMessages(ILjava/lang/Object;)V

    .line 751
    iget-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mHandler:Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2, p2}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 752
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mHandler:Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;

    sget-boolean v2, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z

    if-eqz v2, :cond_1

    const-wide/16 v2, 0x1388

    goto :goto_0

    :cond_1
    iget-wide v2, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapHandleTime:J

    :goto_0
    invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 754
    .end local v0    # "msg":Landroid/os/Message;
    :cond_2
    return-void

    .line 747
    :cond_3
    :goto_1
    return-void
.end method

.method public reportMemPressure(I)V
    .locals 6
    .param p1, "pressureState"    # I

    .line 730
    invoke-virtual {p0}, Lcom/android/server/am/MemoryStandardProcessControl;->isEnable()Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenState:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 733
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 735
    .local v0, "now":J
    iget-wide v2, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mLastMemPressureTime:J

    sub-long v2, v0, v2

    const-wide/32 v4, 0x1d4c0

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    const/4 v2, 0x3

    if-ge p1, v2, :cond_2

    :cond_1
    sget-boolean v2, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z

    if-nez v2, :cond_2

    .line 737
    return-void

    .line 739
    :cond_2
    iput-wide v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mLastMemPressureTime:J

    .line 740
    iget-object v2, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mHandler:Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;

    const/4 v3, 0x4

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p1, v4}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v2

    .line 741
    .local v2, "msg":Landroid/os/Message;
    iget-object v3, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mHandler:Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;

    invoke-virtual {v3, v2}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 742
    return-void

    .line 731
    .end local v0    # "now":J
    .end local v2    # "msg":Landroid/os/Message;
    :cond_3
    :goto_0
    return-void
.end method
