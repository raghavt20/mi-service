.class public Lcom/android/server/am/PeriodicCleanerService;
.super Lcom/android/server/SystemService;
.source "PeriodicCleanerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/PeriodicCleanerService$BinderService;,
        Lcom/android/server/am/PeriodicCleanerService$LocalService;,
        Lcom/android/server/am/PeriodicCleanerService$MyHandler;,
        Lcom/android/server/am/PeriodicCleanerService$CleanInfo;,
        Lcom/android/server/am/PeriodicCleanerService$MyEvent;,
        Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;,
        Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;
    }
.end annotation


# static fields
.field private static final AGGREGATE_HOURS:I = 0x18

.field private static final CACHED_APP_MIN_ADJ:I = 0x384

.field private static final CLOUD_PERIODIC_ENABLE:Ljava/lang/String; = "cloud_periodic_enable"

.field private static final CLOUD_PERIODIC_GAME_ENABLE:Ljava/lang/String; = "cloud_periodic_game_enable"

.field private static final CLOUD_PERIODIC_WHITE_LIST:Ljava/lang/String; = "cloud_periodic_white_list"

.field private static DEBUG:Z = false

.field private static final DEVICE_MEM_TYPE_COUNT:I = 0x7

.field private static final HISTORY_SIZE:I

.field private static final KILL_LEVEL_FORCE_STOP:I = 0x68

.field private static final KILL_LEVEL_UNKOWN:I = 0x64

.field private static final MAX_MEMORY_VALUE:J = 0x64000L

.field private static final MAX_PREVIOUS_TIME:I = 0x493e0

.field private static final MEM_NO_PRESSURE:I = -0x1

.field private static final MEM_PRESSURE_COUNT:I = 0x3

.field private static final MEM_PRESSURE_CRITICAL:I = 0x2

.field private static final MEM_PRESSURE_LOW:I = 0x0

.field private static final MEM_PRESSURE_MIN:I = 0x1

.field private static final MININUM_AGING_THRESHOLD:I = 0x3

.field private static final MSG_REPORT_CLEAN_PROCESS:I = 0x6

.field private static final MSG_REPORT_EVENT:I = 0x1

.field private static final MSG_REPORT_PRESSURE:I = 0x2

.field private static final MSG_REPORT_START_PROCESS:I = 0x5

.field private static final MSG_SCREEN_OFF:I = 0x3

.field private static final PACKAGE_NAME_CAMERA:Ljava/lang/String; = "com.android.camera"

.field private static final PERIODIC_DEBUG_PROP:Ljava/lang/String; = "persist.sys.periodic.debug"

.field private static final PERIODIC_ENABLE_PROP:Ljava/lang/String; = "persist.sys.periodic.u.enable"

.field private static final PERIODIC_MEM_THRES_PROP:Ljava/lang/String; = "persist.sys.periodic.mem_threshold"

.field private static final PERIODIC_START_PROCESS_ENABLE_PROP:Ljava/lang/String; = "persist.sys.periodic.u.startprocess.enable"

.field private static final PERIODIC_TIME_THRES_PROP:Ljava/lang/String; = "persist.sys.periodic.time_threshold"

.field private static final SCREEN_STATE_OFF:I = 0x2

.field private static final SCREEN_STATE_ON:I = 0x1

.field private static final SCREEN_STATE_UNKOWN:I = 0x3

.field private static final SYSTEM_UID_OBJ:Ljava/lang/Integer;

.field private static final TAG:Ljava/lang/String; = "PeriodicCleaner"

.field private static final TIME_FORMAT_PATTERN:Ljava/lang/String; = "HH:mm:ss.SSS"

.field private static final UPDATE_PROCSTATS_PERIOD:J = 0x2932e00L

.field private static sCleanWhiteList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sDefaultActiveLength:[[I

.field private static final sDefaultCacheLevel:[[I

.field private static sDefaultTimeLevel:[I

.field private static sGameApp:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sGamePlayAppNumArray:[I

.field private static sHighFrequencyApp:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sHighMemoryApp:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sHomeOrRecents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sOverTimeAppNumArray:[I

.field private static sSystemRelatedPkgs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAMS:Lcom/android/server/am/ActivityManagerService;

.field private mATMS:Lcom/android/server/wm/ActivityTaskManagerService;

.field private mAndroidOsDebugClz:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field private mAppMemThreshold:I

.field private mAudioService:Lcom/android/server/audio/AudioService;

.field private mAudioServiceClz:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field private mBinderService:Lcom/android/server/am/PeriodicCleanerService$BinderService;

.field private mClassProcessState:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field final mCleanHistory:[Lcom/android/server/am/PeriodicCleanerService$CleanInfo;

.field private mCleanHistoryIndex:I

.field private mContext:Landroid/content/Context;

.field private mDebugClz_Field_MEMINFO_BUFFERS:Ljava/lang/reflect/Field;

.field private mDebugClz_Field_MEMINFO_CACHED:Ljava/lang/reflect/Field;

.field private mDebugClz_Field_MEMINFO_SHMEM:Ljava/lang/reflect/Field;

.field private mDebugClz_Field_MEMINFO_SWAPCACHED:Ljava/lang/reflect/Field;

.field private mDebugClz_Field_MEMINFO_UNEVICTABLE:Ljava/lang/reflect/Field;

.field private mDebugClz_MEMINFO_BUFFERS:I

.field private mDebugClz_MEMINFO_CACHED:I

.field private mDebugClz_MEMINFO_SHMEM:I

.field private mDebugClz_MEMINFO_SWAPCACHED:I

.field private mDebugClz_MEMINFO_UNEVICTABLE:I

.field private volatile mEnable:Z

.field private volatile mEnableFgTrim:Z

.field private volatile mEnableGameClean:Z

.field private volatile mEnableStartProcess:Z

.field private mFgTrimTheshold:I

.field private mFieldProcessState:Ljava/lang/reflect/Field;

.field private mGamePlayAppNum:I

.field private mGetAllAudioFocusMethod:Ljava/lang/reflect/Method;

.field private mGetVisibleWindowOwnerMethod:Ljava/lang/reflect/Method;

.field private mHandler:Lcom/android/server/am/PeriodicCleanerService$MyHandler;

.field private volatile mHighDevice:Z

.field private mIApplicationThreadClz:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field private mLastCleanByPressure:J

.field private mLastNonSystemFgPkg:Ljava/lang/String;

.field private mLastNonSystemFgUid:I

.field private mLastUpdateTime:J

.field private mLastVisibleUids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mLocalService:Lcom/android/server/am/PeriodicCleanerInternalStub;

.field private final mLock:Ljava/lang/Object;

.field private mLruActiveLength:I

.field final mLruPackages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mOverTimeAppNum:I

.field private mPKMS:Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

.field private mPMS:Lcom/android/server/am/ProcessManagerService;

.field private mPressureAgingThreshold:[I

.field private mPressureCacheThreshold:[I

.field private mPressureTimeThreshold:[I

.field private mProcessStats:Lcom/android/internal/app/procstats/IProcessStats;

.field private volatile mReady:Z

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mScheduleAggressiveTrimMethod:Ljava/lang/reflect/Method;

.field private volatile mScreenState:I

.field private mThread:Landroid/os/HandlerThread;

.field private mUSM:Landroid/app/usage/UsageStatsManager;

.field private mWMS:Lcom/android/server/wm/WindowManagerInternal;

.field private mWindowManagerInternalClz:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/am/PeriodicCleanerService;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/PeriodicCleanerService;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmEnable(Lcom/android/server/am/PeriodicCleanerService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnable:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmEnableGameClean(Lcom/android/server/am/PeriodicCleanerService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnableGameClean:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/am/PeriodicCleanerService;)Lcom/android/server/am/PeriodicCleanerService$MyHandler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/PeriodicCleanerService;->mHandler:Lcom/android/server/am/PeriodicCleanerService$MyHandler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmEnable(Lcom/android/server/am/PeriodicCleanerService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnable:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmEnableGameClean(Lcom/android/server/am/PeriodicCleanerService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnableGameClean:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmReady(Lcom/android/server/am/PeriodicCleanerService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/am/PeriodicCleanerService;->mReady:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmScreenState(Lcom/android/server/am/PeriodicCleanerService;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/am/PeriodicCleanerService;->mScreenState:I

    return-void
.end method

.method static bridge synthetic -$$Nest$mcleanPackageByPeriodic(Lcom/android/server/am/PeriodicCleanerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->cleanPackageByPeriodic()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mcleanPackageByPressure(Lcom/android/server/am/PeriodicCleanerService;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/am/PeriodicCleanerService;->cleanPackageByPressure(ILjava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mcleanPackageByTime(Lcom/android/server/am/PeriodicCleanerService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/PeriodicCleanerService;->cleanPackageByTime(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mdumpCleanHistory(Lcom/android/server/am/PeriodicCleanerService;Ljava/io/PrintWriter;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/PeriodicCleanerService;->dumpCleanHistory(Ljava/io/PrintWriter;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mdumpFgLru(Lcom/android/server/am/PeriodicCleanerService;Ljava/io/PrintWriter;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/PeriodicCleanerService;->dumpFgLru(Ljava/io/PrintWriter;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleScreenOff(Lcom/android/server/am/PeriodicCleanerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->handleScreenOff()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mreportCleanProcess(Lcom/android/server/am/PeriodicCleanerService;IILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/am/PeriodicCleanerService;->reportCleanProcess(IILjava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mreportEvent(Lcom/android/server/am/PeriodicCleanerService;Lcom/android/server/am/PeriodicCleanerService$MyEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/PeriodicCleanerService;->reportEvent(Lcom/android/server/am/PeriodicCleanerService$MyEvent;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mreportMemPressure(Lcom/android/server/am/PeriodicCleanerService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/PeriodicCleanerService;->reportMemPressure(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mreportStartProcess(Lcom/android/server/am/PeriodicCleanerService;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/PeriodicCleanerService;->reportStartProcess(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateProcStatsList(Lcom/android/server/am/PeriodicCleanerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->updateProcStatsList()V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetDEBUG()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetsCleanWhiteList()Ljava/util/List;
    .locals 1

    sget-object v0, Lcom/android/server/am/PeriodicCleanerService;->sCleanWhiteList:Ljava/util/List;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfputDEBUG(Z)V
    .locals 0

    sput-boolean p0, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z

    return-void
.end method

.method static constructor <clinit>()V
    .locals 10

    .line 138
    const v0, 0x927c0

    const v1, 0x493e0

    const v2, 0xdbba0

    filled-new-array {v2, v0, v1}, [I

    move-result-object v0

    sput-object v0, Lcom/android/server/am/PeriodicCleanerService;->sDefaultTimeLevel:[I

    .line 139
    const/4 v0, 0x7

    new-array v1, v0, [I

    fill-array-data v1, :array_0

    sput-object v1, Lcom/android/server/am/PeriodicCleanerService;->sOverTimeAppNumArray:[I

    .line 140
    new-array v1, v0, [I

    fill-array-data v1, :array_1

    sput-object v1, Lcom/android/server/am/PeriodicCleanerService;->sGamePlayAppNumArray:[I

    .line 142
    const/4 v1, 0x3

    const/4 v2, 0x2

    filled-new-array {v1, v2, v2}, [I

    move-result-object v3

    filled-new-array {v1, v2, v2}, [I

    move-result-object v4

    const/4 v2, 0x4

    const/4 v5, 0x5

    filled-new-array {v5, v2, v1}, [I

    move-result-object v1

    const/4 v2, 0x6

    filled-new-array {v0, v2, v5}, [I

    move-result-object v6

    const/16 v2, 0x9

    const/16 v5, 0x8

    filled-new-array {v2, v5, v0}, [I

    move-result-object v7

    const/16 v0, 0xa

    filled-new-array {v0, v2, v5}, [I

    move-result-object v8

    const/16 v0, 0xd

    const/16 v2, 0xc

    const/16 v5, 0xe

    filled-new-array {v5, v0, v2}, [I

    move-result-object v9

    move-object v5, v1

    filled-new-array/range {v3 .. v9}, [[I

    move-result-object v0

    sput-object v0, Lcom/android/server/am/PeriodicCleanerService;->sDefaultActiveLength:[[I

    .line 144
    const v0, 0x5a550

    const v1, 0x46cd0

    const v2, 0x6ddd0

    filled-new-array {v2, v0, v1}, [I

    move-result-object v3

    const v0, 0x72bf0

    const v1, 0x5f370

    const v2, 0x86470

    filled-new-array {v2, v0, v1}, [I

    move-result-object v4

    const v0, 0xd9490

    const v1, 0xc5c10

    const v2, 0xecd10

    filled-new-array {v2, v0, v1}, [I

    move-result-object v5

    const v0, 0x11da50

    const v1, 0x10a1d0

    const v2, 0x1312d0

    filled-new-array {v2, v0, v1}, [I

    move-result-object v6

    const v0, 0x17f4d0

    const v1, 0x16bc50

    const v2, 0x192d50

    filled-new-array {v2, v0, v1}, [I

    move-result-object v7

    const v0, 0x1e0f50

    const v1, 0x1cd6d0

    const v2, 0x1f47d0

    filled-new-array {v2, v0, v1}, [I

    move-result-object v8

    const v0, 0x2429d0

    const v1, 0x22f150

    const v2, 0x256250

    filled-new-array {v2, v0, v1}, [I

    move-result-object v9

    filled-new-array/range {v3 .. v9}, [[I

    move-result-object v0

    sput-object v0, Lcom/android/server/am/PeriodicCleanerService;->sDefaultCacheLevel:[[I

    .line 148
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/am/PeriodicCleanerService;->sSystemRelatedPkgs:Ljava/util/List;

    .line 149
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/am/PeriodicCleanerService;->sCleanWhiteList:Ljava/util/List;

    .line 150
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/am/PeriodicCleanerService;->sHomeOrRecents:Ljava/util/List;

    .line 151
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    sput-object v0, Lcom/android/server/am/PeriodicCleanerService;->sHighFrequencyApp:Ljava/util/Set;

    .line 152
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/am/PeriodicCleanerService;->sHighMemoryApp:Ljava/util/List;

    .line 153
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/am/PeriodicCleanerService;->sGameApp:Ljava/util/List;

    .line 155
    const-string v0, "persist.sys.periodic.debug"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z

    .line 156
    if-eqz v0, :cond_0

    const/16 v0, 0x1f4

    goto :goto_0

    :cond_0
    const/16 v0, 0x64

    :goto_0
    sput v0, Lcom/android/server/am/PeriodicCleanerService;->HISTORY_SIZE:I

    .line 157
    new-instance v0, Ljava/lang/Integer;

    const/16 v1, 0x3e8

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    sput-object v0, Lcom/android/server/am/PeriodicCleanerService;->SYSTEM_UID_OBJ:Ljava/lang/Integer;

    return-void

    nop

    :array_0
    .array-data 4
        0x2
        0x3
        0x3
        0x4
        0x6
        0x6
        0x6
    .end array-data

    :array_1
    .array-data 4
        0x2
        0x2
        0x2
        0x4
        0x5
        0x6
        0x7
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 323
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 165
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mReady:Z

    .line 166
    iput-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mHighDevice:Z

    .line 167
    iput-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnableFgTrim:Z

    .line 168
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnableGameClean:Z

    .line 169
    const-string v2, "persist.sys.periodic.u.enable"

    invoke-static {v2, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnable:Z

    .line 170
    nop

    .line 171
    const-string v2, "persist.sys.periodic.u.startprocess.enable"

    invoke-static {v2, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnableStartProcess:Z

    .line 172
    const-string v2, "persist.sys.periodic.mem_threshold"

    const/16 v3, 0x258

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mAppMemThreshold:I

    .line 174
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mLock:Ljava/lang/Object;

    .line 175
    new-instance v2, Landroid/os/HandlerThread;

    const-string v3, "PeriodicCleaner"

    invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mThread:Landroid/os/HandlerThread;

    .line 176
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mContext:Landroid/content/Context;

    .line 177
    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mBinderService:Lcom/android/server/am/PeriodicCleanerService$BinderService;

    .line 178
    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mLocalService:Lcom/android/server/am/PeriodicCleanerInternalStub;

    .line 179
    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mHandler:Lcom/android/server/am/PeriodicCleanerService$MyHandler;

    .line 180
    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mPMS:Lcom/android/server/am/ProcessManagerService;

    .line 181
    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mAMS:Lcom/android/server/am/ActivityManagerService;

    .line 182
    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mATMS:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 183
    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mWMS:Lcom/android/server/wm/WindowManagerInternal;

    .line 184
    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mAudioService:Lcom/android/server/audio/AudioService;

    .line 185
    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mPKMS:Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

    .line 186
    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mUSM:Landroid/app/usage/UsageStatsManager;

    .line 189
    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mAudioServiceClz:Ljava/lang/Class;

    .line 190
    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mWindowManagerInternalClz:Ljava/lang/Class;

    .line 191
    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mGetAllAudioFocusMethod:Ljava/lang/reflect/Method;

    .line 192
    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mGetVisibleWindowOwnerMethod:Ljava/lang/reflect/Method;

    .line 194
    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mAndroidOsDebugClz:Ljava/lang/Class;

    .line 195
    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mClassProcessState:Ljava/lang/Class;

    .line 196
    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mFieldProcessState:Ljava/lang/reflect/Field;

    .line 197
    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_Field_MEMINFO_CACHED:Ljava/lang/reflect/Field;

    .line 198
    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_Field_MEMINFO_SWAPCACHED:Ljava/lang/reflect/Field;

    .line 199
    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_Field_MEMINFO_BUFFERS:Ljava/lang/reflect/Field;

    .line 200
    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_Field_MEMINFO_SHMEM:Ljava/lang/reflect/Field;

    .line 201
    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_Field_MEMINFO_UNEVICTABLE:Ljava/lang/reflect/Field;

    .line 202
    iput v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_MEMINFO_CACHED:I

    .line 203
    iput v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_MEMINFO_SWAPCACHED:I

    .line 204
    iput v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_MEMINFO_BUFFERS:I

    .line 205
    iput v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_MEMINFO_SHMEM:I

    .line 206
    iput v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_MEMINFO_UNEVICTABLE:I

    .line 208
    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mIApplicationThreadClz:Ljava/lang/Class;

    .line 209
    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mScheduleAggressiveTrimMethod:Ljava/lang/reflect/Method;

    .line 211
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruPackages:Ljava/util/ArrayList;

    .line 213
    sget v3, Lcom/android/server/am/PeriodicCleanerService;->HISTORY_SIZE:I

    new-array v3, v3, [Lcom/android/server/am/PeriodicCleanerService$CleanInfo;

    iput-object v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mCleanHistory:[Lcom/android/server/am/PeriodicCleanerService$CleanInfo;

    .line 214
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mLastVisibleUids:Ljava/util/List;

    .line 216
    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mLastNonSystemFgPkg:Ljava/lang/String;

    .line 217
    iput v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mLastNonSystemFgUid:I

    .line 218
    iput v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruActiveLength:I

    .line 219
    const/4 v2, 0x3

    new-array v3, v2, [I

    iput-object v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mPressureTimeThreshold:[I

    .line 220
    new-array v3, v2, [I

    iput-object v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mPressureAgingThreshold:[I

    .line 221
    new-array v3, v2, [I

    iput-object v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mPressureCacheThreshold:[I

    .line 222
    const-wide/16 v3, 0x0

    iput-wide v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mLastCleanByPressure:J

    .line 223
    iput v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mCleanHistoryIndex:I

    .line 224
    iput v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mScreenState:I

    .line 225
    iput v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mFgTrimTheshold:I

    .line 226
    iput-wide v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mLastUpdateTime:J

    .line 227
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mOverTimeAppNum:I

    .line 228
    iput v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mGamePlayAppNum:I

    .line 1731
    new-instance v0, Lcom/android/server/am/PeriodicCleanerService$6;

    invoke-direct {v0, p0}, Lcom/android/server/am/PeriodicCleanerService$6;-><init>(Lcom/android/server/am/PeriodicCleanerService;)V

    iput-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 324
    iput-object p1, p0, Lcom/android/server/am/PeriodicCleanerService;->mContext:Landroid/content/Context;

    .line 325
    iget-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 326
    new-instance v0, Lcom/android/server/am/PeriodicCleanerService$MyHandler;

    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/am/PeriodicCleanerService$MyHandler;-><init>(Lcom/android/server/am/PeriodicCleanerService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mHandler:Lcom/android/server/am/PeriodicCleanerService$MyHandler;

    .line 327
    invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->init()V

    .line 328
    return-void
.end method

.method private addCleanHistory(Lcom/android/server/am/PeriodicCleanerService$CleanInfo;)V
    .locals 3
    .param p1, "cInfo"    # Lcom/android/server/am/PeriodicCleanerService$CleanInfo;

    .line 612
    invoke-virtual {p1}, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613
    iget-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mCleanHistory:[Lcom/android/server/am/PeriodicCleanerService$CleanInfo;

    monitor-enter v0

    .line 614
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mCleanHistory:[Lcom/android/server/am/PeriodicCleanerService$CleanInfo;

    iget v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mCleanHistoryIndex:I

    aput-object p1, v1, v2

    .line 615
    add-int/lit8 v2, v2, 0x1

    sget v1, Lcom/android/server/am/PeriodicCleanerService;->HISTORY_SIZE:I

    rem-int/2addr v2, v1

    iput v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mCleanHistoryIndex:I

    .line 616
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 618
    :cond_0
    :goto_0
    return-void
.end method

.method private canCleanPackage(Lcom/android/server/am/ProcessRecord;ILjava/util/HashMap;Ljava/util/List;Ljava/util/List;Ljava/util/List;Z)Z
    .locals 3
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "pressure"    # I
    .param p7, "isCameraForeground"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/server/am/ProcessRecord;",
            "I",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;Z)Z"
        }
    .end annotation

    .line 1462
    .local p3, "dynWhitelist":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    .local p4, "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .local p5, "activeAudioUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .local p6, "locationActiveUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 1463
    .local v0, "packageName":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/android/server/am/PeriodicCleanerService;->isSystemRelated(Lcom/android/server/am/ProcessRecord;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 1464
    return v2

    .line 1466
    :cond_0
    invoke-direct {p0, v0, p3}, Lcom/android/server/am/PeriodicCleanerService;->isDynWhitelist(Ljava/lang/String;Ljava/util/HashMap;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1467
    return v2

    .line 1469
    :cond_1
    invoke-direct {p0, p1, p4, p5, p6}, Lcom/android/server/am/PeriodicCleanerService;->isActive(Lcom/android/server/am/ProcessRecord;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1470
    return v2

    .line 1472
    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/android/server/am/PeriodicCleanerService;->isImportant(Lcom/android/server/am/ProcessRecord;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1473
    return v2

    .line 1475
    :cond_3
    if-eqz p7, :cond_4

    invoke-direct {p0, p1, p2}, Lcom/android/server/am/PeriodicCleanerService;->isSkipClean(Lcom/android/server/am/ProcessRecord;I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1476
    return v2

    .line 1478
    :cond_4
    const/4 v1, 0x1

    return v1
.end method

.method private checkEnableFgTrim()Z
    .locals 5

    .line 491
    const-string v0, "PeriodicCleaner"

    const/4 v1, 0x0

    :try_start_0
    const-string v2, "android.app.IApplicationThread"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mIApplicationThreadClz:Ljava/lang/Class;

    .line 492
    const-string v3, "scheduleAggressiveMemoryTrim"

    new-array v4, v1, [Ljava/lang/Class;

    .line 493
    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mScheduleAggressiveTrimMethod:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 494
    const/4 v0, 0x1

    return v0

    .line 498
    :catch_0
    move-exception v2

    .line 499
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "check failed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    return v1

    .line 495
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v2

    .line 496
    .local v2, "nme":Ljava/lang/NoSuchMethodException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "interface missing: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 497
    return v1
.end method

.method private checkPressureAndClean(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .line 1100
    invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->getMemPressureLevel()I

    move-result v0

    .line 1101
    .local v0, "pressure":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1102
    invoke-direct {p0, v0, p1}, Lcom/android/server/am/PeriodicCleanerService;->cleanPackageByPressure(ILjava/lang/String;)V

    .line 1103
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mLastCleanByPressure:J

    .line 1105
    :cond_0
    return-void
.end method

.method private static checkTime(JLjava/lang/String;)V
    .locals 6
    .param p0, "startTime"    # J
    .param p2, "where"    # Ljava/lang/String;

    .line 1594
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1595
    .local v0, "now":J
    sub-long v2, v0, p0

    sget-boolean v4, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z

    if-eqz v4, :cond_0

    const/16 v4, 0x32

    goto :goto_0

    :cond_0
    const/16 v4, 0xc8

    :goto_0
    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 1596
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Slow operation: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sub-long v3, v0, p0

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms so far, now at "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "PeriodicCleaner"

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1598
    :cond_1
    return-void
.end method

.method private cleanPackageByGamePlay(ILjava/lang/String;)V
    .locals 15
    .param p1, "activeNum"    # I
    .param p2, "startProcessName"    # Ljava/lang/String;

    .line 1053
    move-object v7, p0

    move/from16 v8, p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "game:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object/from16 v9, p2

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1054
    .local v10, "reason":Ljava/lang/String;
    const/16 v0, 0x64

    const/4 v1, -0x1

    invoke-direct {p0, v8, v0, v1, v10}, Lcom/android/server/am/PeriodicCleanerService;->doClean(IIILjava/lang/String;)V

    .line 1057
    invoke-direct/range {p0 .. p2}, Lcom/android/server/am/PeriodicCleanerService;->findOtherGamePackageLocked(ILjava/lang/String;)Ljava/util/List;

    move-result-object v11

    .line 1058
    .local v11, "gamePackagesUid":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 1059
    return-void

    .line 1060
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PeriodicCleaner("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "|"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "|muti_game)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 1061
    .local v12, "longReason":Ljava/lang/String;
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    move-object v13, v0

    .line 1062
    .local v13, "killedArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1063
    .local v1, "uid":I
    const/4 v2, 0x0

    .line 1064
    .local v2, "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    iget-object v3, v7, Lcom/android/server/am/PeriodicCleanerService;->mPMS:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v3, v1}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordByUid(I)Ljava/util/List;

    move-result-object v2

    .line 1065
    if-nez v2, :cond_1

    goto :goto_0

    .line 1066
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/am/ProcessRecord;

    .line 1067
    .local v4, "app":Lcom/android/server/am/ProcessRecord;
    invoke-virtual {v4}, Lcom/android/server/am/ProcessRecord;->isKilledByAm()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v4}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v5

    if-eqz v5, :cond_2

    iget-boolean v5, v4, Lcom/android/server/am/ProcessRecord;->isolated:Z

    if-nez v5, :cond_2

    .line 1068
    invoke-virtual {p0, v4}, Lcom/android/server/am/PeriodicCleanerService;->isSystemRelated(Lcom/android/server/am/ProcessRecord;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1069
    goto :goto_1

    .line 1071
    :cond_3
    iget-object v5, v7, Lcom/android/server/am/PeriodicCleanerService;->mPMS:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v5}, Lcom/android/server/am/ProcessManagerService;->getProcessKiller()Lcom/android/server/am/ProcessKiller;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v4, v12, v6}, Lcom/android/server/am/ProcessKiller;->killApplication(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Z)Z

    .line 1072
    iget v5, v4, Lcom/android/server/am/ProcessRecord;->userId:I

    invoke-virtual {v13, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    .line 1073
    .local v5, "userTargets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v5, :cond_4

    .line 1074
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move-object v5, v6

    .line 1075
    iget v6, v4, Lcom/android/server/am/ProcessRecord;->userId:I

    invoke-virtual {v13, v6, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1077
    :cond_4
    iget-object v6, v4, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1078
    .end local v4    # "app":Lcom/android/server/am/ProcessRecord;
    .end local v5    # "userTargets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    goto :goto_1

    .line 1079
    .end local v1    # "uid":I
    .end local v2    # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    :cond_5
    goto :goto_0

    .line 1080
    :cond_6
    new-instance v14, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const/4 v4, -0x1

    const/4 v5, -0x1

    const-string v6, "muti_game"

    move-object v0, v14

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;-><init>(Lcom/android/server/am/PeriodicCleanerService;JIILjava/lang/String;)V

    .line 1081
    .local v0, "cInfo":Lcom/android/server/am/PeriodicCleanerService$CleanInfo;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    invoke-virtual {v13}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_7

    .line 1082
    invoke-virtual {v13, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    invoke-virtual {v13, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-virtual {v0, v2, v3}, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->addCleanList(ILjava/util/List;)V

    .line 1081
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1084
    .end local v1    # "i":I
    :cond_7
    invoke-direct {p0, v0}, Lcom/android/server/am/PeriodicCleanerService;->addCleanHistory(Lcom/android/server/am/PeriodicCleanerService$CleanInfo;)V

    .line 1085
    return-void
.end method

.method private cleanPackageByPeriodic()V
    .locals 4

    .line 1048
    iget v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruActiveLength:I

    const/4 v1, -0x1

    const-string v2, "cycle"

    const/16 v3, 0x64

    invoke-direct {p0, v0, v3, v1, v2}, Lcom/android/server/am/PeriodicCleanerService;->doClean(IIILjava/lang/String;)V

    .line 1049
    return-void
.end method

.method private cleanPackageByPressure(ILjava/lang/String;)V
    .locals 2
    .param p1, "pressure"    # I
    .param p2, "reason"    # Ljava/lang/String;

    .line 1088
    iget-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mPressureAgingThreshold:[I

    aget v0, v0, p1

    const/16 v1, 0x64

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/android/server/am/PeriodicCleanerService;->doClean(IIILjava/lang/String;)V

    .line 1089
    invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->reCheckPressureAndClean()V

    .line 1090
    return-void
.end method

.method private cleanPackageByTime(I)V
    .locals 3
    .param p1, "pressure"    # I

    .line 1044
    iget v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruActiveLength:I

    const/16 v1, 0x64

    const-string/jumbo v2, "time"

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/android/server/am/PeriodicCleanerService;->doClean(IIILjava/lang/String;)V

    .line 1045
    return-void
.end method

.method private dependencyCheck()Z
    .locals 5

    .line 448
    const-string v0, "PeriodicCleaner"

    const/4 v1, 0x0

    :try_start_0
    const-string v2, "com.android.server.audio.AudioService"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mAudioServiceClz:Ljava/lang/Class;

    .line 449
    const-string v3, "getAllAudioFocus"

    new-array v4, v1, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mGetAllAudioFocusMethod:Ljava/lang/reflect/Method;

    .line 452
    const-string v2, "com.android.server.wm.WindowManagerInternal"

    .line 453
    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mWindowManagerInternalClz:Ljava/lang/Class;

    .line 454
    const-string v3, "getVisibleWindowOwner"

    new-array v4, v1, [Ljava/lang/Class;

    .line 455
    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mGetVisibleWindowOwnerMethod:Ljava/lang/reflect/Method;

    .line 458
    const-string v2, "android.os.Debug"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mAndroidOsDebugClz:Ljava/lang/Class;

    .line 459
    const-string v3, "MEMINFO_CACHED"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_Field_MEMINFO_CACHED:Ljava/lang/reflect/Field;

    .line 460
    iget-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mAndroidOsDebugClz:Ljava/lang/Class;

    const-string v3, "MEMINFO_SWAPCACHED"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_Field_MEMINFO_SWAPCACHED:Ljava/lang/reflect/Field;

    .line 461
    iget-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mAndroidOsDebugClz:Ljava/lang/Class;

    const-string v3, "MEMINFO_BUFFERS"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_Field_MEMINFO_BUFFERS:Ljava/lang/reflect/Field;

    .line 462
    iget-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mAndroidOsDebugClz:Ljava/lang/Class;

    const-string v3, "MEMINFO_SHMEM"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_Field_MEMINFO_SHMEM:Ljava/lang/reflect/Field;

    .line 463
    iget-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mAndroidOsDebugClz:Ljava/lang/Class;

    const-string v3, "MEMINFO_UNEVICTABLE"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_Field_MEMINFO_UNEVICTABLE:Ljava/lang/reflect/Field;

    .line 465
    iget-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_Field_MEMINFO_CACHED:Ljava/lang/reflect/Field;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v2

    iput v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_MEMINFO_CACHED:I

    .line 466
    iget-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_Field_MEMINFO_SWAPCACHED:Ljava/lang/reflect/Field;

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v2

    iput v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_MEMINFO_SWAPCACHED:I

    .line 467
    iget-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_Field_MEMINFO_BUFFERS:Ljava/lang/reflect/Field;

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v2

    iput v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_MEMINFO_BUFFERS:I

    .line 468
    iget-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_Field_MEMINFO_SHMEM:Ljava/lang/reflect/Field;

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v2

    iput v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_MEMINFO_SHMEM:I

    .line 469
    iget-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_Field_MEMINFO_UNEVICTABLE:Ljava/lang/reflect/Field;

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v2

    iput v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_MEMINFO_UNEVICTABLE:I

    .line 472
    const-string v2, "com.android.internal.app.procstats.ProcessState"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mClassProcessState:Ljava/lang/Class;

    .line 473
    const-string v3, "mPssTable"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mFieldProcessState:Ljava/lang/reflect/Field;

    .line 474
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 476
    return v3

    .line 483
    :catch_0
    move-exception v2

    .line 484
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dependency check failed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 485
    return v1

    .line 480
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v2

    .line 481
    .local v2, "nfe":Ljava/lang/NoSuchFieldException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dependent field missing: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    return v1

    .line 477
    .end local v2    # "nfe":Ljava/lang/NoSuchFieldException;
    :catch_2
    move-exception v2

    .line 478
    .local v2, "nme":Ljava/lang/NoSuchMethodException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dependent interface missing: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    return v1
.end method

.method private doClean(IIILjava/lang/String;)V
    .locals 22
    .param p1, "thresHold"    # I
    .param p2, "killLevel"    # I
    .param p3, "pressure"    # I
    .param p4, "reason"    # Ljava/lang/String;

    .line 1187
    move-object/from16 v7, p0

    move/from16 v8, p3

    move-object/from16 v0, p4

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v9

    .line 1188
    .local v9, "startTime":J
    const/4 v1, 0x0

    .line 1189
    .local v1, "agingPkgs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    const-string/jumbo v2, "time"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1190
    invoke-direct {v7, v8}, Lcom/android/server/am/PeriodicCleanerService;->findOverTimePackageLocked(I)Landroid/util/SparseArray;

    move-result-object v1

    move-object v11, v1

    goto :goto_0

    .line 1192
    :cond_0
    invoke-direct/range {p0 .. p1}, Lcom/android/server/am/PeriodicCleanerService;->findAgingPackageLocked(I)Landroid/util/SparseArray;

    move-result-object v1

    move-object v11, v1

    .line 1193
    .end local v1    # "agingPkgs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    .local v11, "agingPkgs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    :goto_0
    const/4 v1, -0x1

    .line 1194
    .local v1, "processIndex":I
    const-string v2, "game:"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x4

    .line 1195
    :cond_1
    const-string v2, "pressure:startprocess:"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v1, 0x15

    :cond_2
    move v12, v1

    .line 1196
    .end local v1    # "processIndex":I
    .local v12, "processIndex":I
    const/4 v1, -0x1

    if-le v12, v1, :cond_3

    .line 1197
    add-int/lit8 v1, v12, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 1198
    .local v1, "currentPackage":Ljava/lang/String;
    invoke-direct {v7, v11, v1}, Lcom/android/server/am/PeriodicCleanerService;->excludeCurrentProcess(Landroid/util/SparseArray;Ljava/lang/String;)V

    .line 1199
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    move-object v13, v0

    .end local p4    # "reason":Ljava/lang/String;
    .local v0, "reason":Ljava/lang/String;
    goto :goto_1

    .line 1196
    .end local v0    # "reason":Ljava/lang/String;
    .end local v1    # "currentPackage":Ljava/lang/String;
    .restart local p4    # "reason":Ljava/lang/String;
    :cond_3
    move-object v13, v0

    .line 1201
    .end local p4    # "reason":Ljava/lang/String;
    .local v13, "reason":Ljava/lang/String;
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PeriodicCleaner("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move/from16 v14, p1

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 1202
    .local v15, "longReason":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/android/server/am/PeriodicCleanerService;->getVisibleWindowOwner()Ljava/util/List;

    move-result-object v6

    .line 1203
    .local v6, "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-direct/range {p0 .. p0}, Lcom/android/server/am/PeriodicCleanerService;->getAudioActiveUids()Ljava/util/List;

    move-result-object v5

    .line 1204
    .local v5, "audioActiveUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-direct/range {p0 .. p0}, Lcom/android/server/am/PeriodicCleanerService;->getLocationActiveUids()Ljava/util/List;

    move-result-object v4

    .line 1205
    .local v4, "locationActiveUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const-string v0, "finish get active and visible uids"

    invoke-static {v9, v10, v0}, Lcom/android/server/am/PeriodicCleanerService;->checkTime(JLjava/lang/String;)V

    .line 1206
    sget-boolean v0, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z

    if-eqz v0, :cond_4

    .line 1207
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AgingPacakges: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", LocationActiveUids: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", VisibleUids: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", AudioActiveUids: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PeriodicCleaner"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1212
    :cond_4
    new-instance v16, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v17, v4

    .end local v4    # "locationActiveUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .local v17, "locationActiveUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    move/from16 v4, p3

    move-object/from16 v18, v5

    .end local v5    # "audioActiveUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .local v18, "audioActiveUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    move/from16 v5, p1

    move-object/from16 v19, v6

    .end local v6    # "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .local v19, "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    move-object v6, v13

    invoke-direct/range {v0 .. v6}, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;-><init>(Lcom/android/server/am/PeriodicCleanerService;JIILjava/lang/String;)V

    move-object/from16 v6, v16

    .line 1213
    .local v6, "cInfo":Lcom/android/server/am/PeriodicCleanerService$CleanInfo;
    const/4 v0, 0x0

    move v5, v0

    .local v5, "i":I
    :goto_2
    invoke-virtual {v11}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v5, v0, :cond_6

    .line 1214
    invoke-virtual {v11, v5}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    .line 1215
    .local v4, "userId":I
    nop

    .line 1216
    invoke-virtual {v11, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/util/ArrayList;

    .line 1215
    move-object/from16 v0, p0

    move v1, v4

    move/from16 v2, p3

    move v8, v4

    .end local v4    # "userId":I
    .local v8, "userId":I
    move-object/from16 v4, v19

    move/from16 v16, v5

    .end local v5    # "i":I
    .local v16, "i":I
    move-object/from16 v5, v18

    move-wide/from16 v20, v9

    move-object v9, v6

    .end local v6    # "cInfo":Lcom/android/server/am/PeriodicCleanerService$CleanInfo;
    .local v9, "cInfo":Lcom/android/server/am/PeriodicCleanerService$CleanInfo;
    .local v20, "startTime":J
    move-object/from16 v6, v17

    invoke-direct/range {v0 .. v6}, Lcom/android/server/am/PeriodicCleanerService;->filterOutKillablePackages(IILjava/util/ArrayList;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1217
    .local v0, "userTargets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_5

    .line 1218
    move/from16 v1, p2

    invoke-direct {v7, v8, v0, v1, v15}, Lcom/android/server/am/PeriodicCleanerService;->killTargets(ILjava/util/ArrayList;ILjava/lang/String;)V

    .line 1219
    invoke-virtual {v9, v8, v0}, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->addCleanList(ILjava/util/List;)V

    goto :goto_3

    .line 1217
    :cond_5
    move/from16 v1, p2

    .line 1213
    .end local v0    # "userTargets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v8    # "userId":I
    :goto_3
    add-int/lit8 v5, v16, 0x1

    move/from16 v8, p3

    move-object v6, v9

    move-wide/from16 v9, v20

    .end local v16    # "i":I
    .restart local v5    # "i":I
    goto :goto_2

    .end local v20    # "startTime":J
    .restart local v6    # "cInfo":Lcom/android/server/am/PeriodicCleanerService$CleanInfo;
    .local v9, "startTime":J
    :cond_6
    move-wide/from16 v20, v9

    move-object v9, v6

    .line 1222
    .end local v5    # "i":I
    .end local v6    # "cInfo":Lcom/android/server/am/PeriodicCleanerService$CleanInfo;
    .local v9, "cInfo":Lcom/android/server/am/PeriodicCleanerService$CleanInfo;
    .restart local v20    # "startTime":J
    invoke-direct {v7, v9}, Lcom/android/server/am/PeriodicCleanerService;->addCleanHistory(Lcom/android/server/am/PeriodicCleanerService$CleanInfo;)V

    .line 1223
    return-void
.end method

.method private doFgTrim(I)V
    .locals 13
    .param p1, "threshold"    # I

    .line 1108
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1109
    .local v0, "startTime":J
    invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->getVisibleWindowOwner()Ljava/util/List;

    move-result-object v2

    .line 1110
    .local v2, "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const-string v3, "finish doFgTrim#getVisibleWindowOwner"

    invoke-static {v0, v1, v3}, Lcom/android/server/am/PeriodicCleanerService;->checkTime(JLjava/lang/String;)V

    .line 1112
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1113
    .local v3, "trimTargets":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;>;"
    iget-object v4, p0, Lcom/android/server/am/PeriodicCleanerService;->mLock:Ljava/lang/Object;

    monitor-enter v4

    .line 1114
    :try_start_0
    iget-object v5, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruPackages:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 1115
    .local v5, "N":I
    const/4 v6, 0x0

    .line 1116
    .local v6, "info":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;
    add-int/lit8 v7, v5, -0x1

    sub-int/2addr v7, p1

    .local v7, "i":I
    :goto_0
    if-ltz v7, :cond_1

    .line 1117
    iget-object v8, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruPackages:Ljava/util/ArrayList;

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;

    move-object v6, v8

    .line 1118
    invoke-static {v6}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->-$$Nest$fgetmFgTrimDone(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 1119
    invoke-static {v6}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->-$$Nest$fgetmUid(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v3, v8, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1116
    :cond_0
    add-int/lit8 v7, v7, -0x1

    goto :goto_0

    .line 1122
    .end local v5    # "N":I
    .end local v6    # "info":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;
    .end local v7    # "i":I
    :cond_1
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1123
    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v4

    if-gtz v4, :cond_2

    return-void

    .line 1124
    :cond_2
    const-string v4, "finish doFgTrim#getTrimTargets"

    invoke-static {v0, v1, v4}, Lcom/android/server/am/PeriodicCleanerService;->checkTime(JLjava/lang/String;)V

    .line 1126
    iget-object v5, p0, Lcom/android/server/am/PeriodicCleanerService;->mAMS:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v5

    .line 1127
    :try_start_1
    iget-object v4, p0, Lcom/android/server/am/PeriodicCleanerService;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget-object v4, v4, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    invoke-virtual {v4}, Lcom/android/server/am/ProcessList;->getLruSizeLOSP()I

    move-result v4

    .line 1128
    .local v4, "N":I
    const/4 v6, 0x0

    .line 1129
    .local v6, "target":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;
    add-int/lit8 v7, v4, -0x1

    .restart local v7    # "i":I
    :goto_1
    if-ltz v7, :cond_8

    .line 1130
    iget-object v8, p0, Lcom/android/server/am/PeriodicCleanerService;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget-object v8, v8, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    invoke-virtual {v8}, Lcom/android/server/am/ProcessList;->getLruProcessesLOSP()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/server/am/ProcessRecord;

    .line 1131
    .local v8, "app":Lcom/android/server/am/ProcessRecord;
    invoke-virtual {v8}, Lcom/android/server/am/ProcessRecord;->isKilledByAm()Z

    move-result v9

    if-nez v9, :cond_7

    invoke-virtual {v8}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v9

    if-nez v9, :cond_3

    .line 1132
    goto/16 :goto_3

    .line 1134
    :cond_3
    iget-boolean v9, v8, Lcom/android/server/am/ProcessRecord;->isolated:Z

    if-nez v9, :cond_6

    iget v9, v8, Lcom/android/server/am/ProcessRecord;->uid:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v2, v9}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    goto :goto_2

    .line 1140
    :cond_4
    iget v9, v8, Lcom/android/server/am/ProcessRecord;->uid:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v6, v9

    .line 1141
    if-nez v6, :cond_5

    .line 1142
    goto :goto_3

    .line 1146
    :cond_5
    :try_start_2
    iget-object v9, p0, Lcom/android/server/am/PeriodicCleanerService;->mScheduleAggressiveTrimMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v8}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v10

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-virtual {v9, v10, v11}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 1147
    const/4 v9, 0x1

    invoke-static {v6, v9}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->-$$Nest$fputmFgTrimDone(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;Z)V

    .line 1148
    const-string v9, "PeriodicCleaner"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "send aggressive trim to "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1151
    goto :goto_3

    .line 1149
    :catch_0
    move-exception v9

    .line 1150
    .local v9, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v10, "PeriodicCleaner"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "doFgTrim#scheduleAggressiveMemoryTrim: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 1135
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_6
    :goto_2
    sget-boolean v9, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z

    if-eqz v9, :cond_7

    .line 1136
    const-string v9, "PeriodicCleaner"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " is isolated or uid has visible window."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1129
    .end local v8    # "app":Lcom/android/server/am/ProcessRecord;
    :cond_7
    :goto_3
    add-int/lit8 v7, v7, -0x1

    goto/16 :goto_1

    .line 1153
    .end local v7    # "i":I
    :cond_8
    const-string v7, "finish doFgTrim"

    invoke-static {v0, v1, v7}, Lcom/android/server/am/PeriodicCleanerService;->checkTime(JLjava/lang/String;)V

    .line 1154
    .end local v4    # "N":I
    .end local v6    # "target":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;
    monitor-exit v5

    .line 1155
    return-void

    .line 1154
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v4

    .line 1122
    :catchall_1
    move-exception v5

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v5
.end method

.method private dumpCleanHistory(Ljava/io/PrintWriter;)V
    .locals 8
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 1640
    const-string v0, "\n---- CleanHistory ----"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1641
    iget-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mCleanHistory:[Lcom/android/server/am/PeriodicCleanerService$CleanInfo;

    monitor-enter v0

    .line 1642
    :try_start_0
    iget v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mCleanHistoryIndex:I

    add-int/lit8 v1, v1, -0x1

    .line 1643
    .local v1, "index":I
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "HH:mm:ss.SSS"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1644
    .local v2, "formater":Ljava/text/SimpleDateFormat;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    sget v4, Lcom/android/server/am/PeriodicCleanerService;->HISTORY_SIZE:I

    if-ge v3, v4, :cond_1

    .line 1645
    add-int v5, v1, v4

    rem-int/2addr v5, v4

    move v1, v5

    .line 1646
    iget-object v4, p0, Lcom/android/server/am/PeriodicCleanerService;->mCleanHistory:[Lcom/android/server/am/PeriodicCleanerService$CleanInfo;

    aget-object v4, v4, v1

    if-nez v4, :cond_0

    goto/16 :goto_1

    .line 1647
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "#"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1648
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/util/Date;

    iget-object v6, p0, Lcom/android/server/am/PeriodicCleanerService;->mCleanHistory:[Lcom/android/server/am/PeriodicCleanerService$CleanInfo;

    aget-object v6, v6, v1

    invoke-static {v6}, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->-$$Nest$fgetmCleanTime(Lcom/android/server/am/PeriodicCleanerService$CleanInfo;)J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1649
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/am/PeriodicCleanerService;->mCleanHistory:[Lcom/android/server/am/PeriodicCleanerService$CleanInfo;

    aget-object v5, v5, v1

    invoke-static {v5}, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->-$$Nest$fgetmPressure(Lcom/android/server/am/PeriodicCleanerService$CleanInfo;)I

    move-result v5

    invoke-direct {p0, v5}, Lcom/android/server/am/PeriodicCleanerService;->pressureToString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1650
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/am/PeriodicCleanerService;->mCleanHistory:[Lcom/android/server/am/PeriodicCleanerService$CleanInfo;

    aget-object v5, v5, v1

    invoke-static {v5}, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->-$$Nest$fgetmAgingThresHold(Lcom/android/server/am/PeriodicCleanerService$CleanInfo;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1651
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/am/PeriodicCleanerService;->mCleanHistory:[Lcom/android/server/am/PeriodicCleanerService$CleanInfo;

    aget-object v5, v5, v1

    invoke-static {v5}, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->-$$Nest$fgetmReason(Lcom/android/server/am/PeriodicCleanerService$CleanInfo;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1652
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/am/PeriodicCleanerService;->mCleanHistory:[Lcom/android/server/am/PeriodicCleanerService$CleanInfo;

    aget-object v5, v5, v1

    invoke-static {v5}, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->-$$Nest$fgetmCleanList(Lcom/android/server/am/PeriodicCleanerService$CleanInfo;)Landroid/util/SparseArray;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1653
    add-int/lit8 v1, v1, -0x1

    .line 1644
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 1655
    .end local v1    # "index":I
    .end local v2    # "formater":Ljava/text/SimpleDateFormat;
    .end local v3    # "i":I
    :cond_1
    :goto_1
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1656
    const-string v0, "---- End of CleanHistory ----"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1657
    return-void

    .line 1655
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private dumpFgLru(Ljava/io/PrintWriter;)V
    .locals 6
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 1601
    const-string v0, "\n---- Foreground-LRU ----"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1602
    invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->getMemPressureLevel()I

    move-result v0

    .line 1603
    .local v0, "curPressure":I
    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1604
    :try_start_0
    const-string v2, "Settings:"

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1606
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "  Enable="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnable:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1607
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " Ready="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mReady:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1608
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " FgTrim="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnableFgTrim:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1609
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " Debug="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1610
    iget-boolean v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnableFgTrim:Z

    if-eqz v2, :cond_0

    .line 1611
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " FgTrimThreshold="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mFgTrimTheshold:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1613
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " mOverTimeAppNum="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mOverTimeAppNum:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1614
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " mGamePlayAppNum="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mGamePlayAppNum:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1615
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " LruLengthLimit="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruActiveLength:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1617
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "  AvailableLow="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mPressureCacheThreshold:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Kb"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1618
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " AvailableMin="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mPressureCacheThreshold:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Kb"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1619
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " AvailableCritical="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mPressureCacheThreshold:[I

    const/4 v5, 0x2

    aget v3, v3, v5

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Kb"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1621
    const-string v2, "Package Usage LRU:"

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1622
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "  CurLength:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruPackages:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " CurrentPressure:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0, v0}, Lcom/android/server/am/PeriodicCleanerService;->pressureToString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1623
    iget-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruPackages:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    sub-int/2addr v2, v4

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_1

    .line 1624
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruPackages:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;

    invoke-virtual {v4}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1623
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 1626
    .end local v2    # "i":I
    :cond_1
    const-string v2, "High Memory Usage List:"

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1627
    sget-object v2, Lcom/android/server/am/PeriodicCleanerService;->sHighMemoryApp:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1628
    .local v3, "str":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1629
    .end local v3    # "str":Ljava/lang/String;
    goto :goto_1

    .line 1630
    :cond_2
    const-string v2, "High Frequency Usage List:"

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1631
    sget-object v2, Lcom/android/server/am/PeriodicCleanerService;->sHighFrequencyApp:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1632
    .restart local v3    # "str":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1633
    .end local v3    # "str":Ljava/lang/String;
    goto :goto_2

    .line 1634
    :cond_3
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1635
    const-string v1, "---- End of Foreground-LRU ----"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1637
    return-void

    .line 1634
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method private excludeCurrentProcess(Landroid/util/SparseArray;Ljava/lang/String;)V
    .locals 2
    .param p2, "currentPackage"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray<",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1317
    .local p1, "agingPkgs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    if-eqz p1, :cond_3

    if-nez p2, :cond_0

    goto :goto_2

    .line 1318
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 1319
    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 1320
    .local v1, "pkgs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v1, :cond_1

    goto :goto_1

    .line 1321
    :cond_1
    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1318
    .end local v1    # "pkgs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1323
    .end local v0    # "i":I
    :cond_2
    return-void

    .line 1317
    :cond_3
    :goto_2
    return-void
.end method

.method private filterOutKillablePackages(IILjava/util/ArrayList;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Ljava/util/ArrayList;
    .locals 20
    .param p1, "userId"    # I
    .param p2, "pressure"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1399
    .local p3, "agingPkgs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p4, "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .local p5, "audioActiveUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .local p6, "locationActiveUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    move-object/from16 v9, p0

    move/from16 v10, p1

    move-object/from16 v11, p3

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v12

    .line 1400
    .local v12, "startTime":J
    iget-object v0, v9, Lcom/android/server/am/PeriodicCleanerService;->mPMS:Lcom/android/server/am/ProcessManagerService;

    .line 1401
    invoke-virtual {v0}, Lcom/android/server/am/ProcessManagerService;->getProcessPolicy()Lcom/android/server/am/ProcessPolicy;

    move-result-object v0

    iget-object v1, v9, Lcom/android/server/am/PeriodicCleanerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, v10}, Lcom/android/server/am/ProcessPolicy;->updateDynamicWhiteList(Landroid/content/Context;I)Ljava/util/HashMap;

    move-result-object v14

    .line 1402
    .local v14, "dynWhitelist":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    const-string v0, "finish updateDynamicWhiteList"

    invoke-static {v12, v13, v0}, Lcom/android/server/am/PeriodicCleanerService;->checkTime(JLjava/lang/String;)V

    .line 1404
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v15, v0

    .line 1405
    .local v15, "killList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct/range {p0 .. p0}, Lcom/android/server/am/PeriodicCleanerService;->isCameraForegroundCase()Z

    move-result v16

    .line 1406
    .local v16, "isCameraForeground":Z
    iget-object v8, v9, Lcom/android/server/am/PeriodicCleanerService;->mAMS:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v8

    .line 1407
    :try_start_0
    iget-object v0, v9, Lcom/android/server/am/PeriodicCleanerService;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessList;->getLruSizeLOSP()I

    move-result v0

    .line 1408
    .local v0, "N":I
    add-int/lit8 v1, v0, -0x1

    move v7, v1

    .local v7, "i":I
    :goto_0
    if-ltz v7, :cond_5

    .line 1409
    iget-object v1, v9, Lcom/android/server/am/PeriodicCleanerService;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget-object v1, v1, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessList;->getLruProcessesLOSP()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/am/ProcessRecord;

    move-object v6, v1

    .line 1410
    .local v6, "app":Lcom/android/server/am/ProcessRecord;
    invoke-virtual {v6}, Lcom/android/server/am/ProcessRecord;->isKilledByAm()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v6}, Lcom/android/server/am/ProcessRecord;->isKilled()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v6}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v1

    if-eqz v1, :cond_3

    iget v1, v6, Lcom/android/server/am/ProcessRecord;->userId:I

    if-ne v1, v10, :cond_3

    iget-object v1, v6, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 1411
    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v1, v6, Lcom/android/server/am/ProcessRecord;->isolated:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 1412
    move/from16 v17, v0

    move/from16 v18, v7

    move-object/from16 v19, v8

    goto :goto_1

    .line 1414
    :cond_0
    move-object/from16 v1, p0

    move-object v2, v6

    move/from16 v3, p2

    move-object v4, v14

    move-object/from16 v5, p4

    move/from16 v17, v0

    move-object v0, v6

    .end local v6    # "app":Lcom/android/server/am/ProcessRecord;
    .local v0, "app":Lcom/android/server/am/ProcessRecord;
    .local v17, "N":I
    move-object/from16 v6, p5

    move/from16 v18, v7

    .end local v7    # "i":I
    .local v18, "i":I
    move-object/from16 v7, p6

    move-object/from16 v19, v8

    move/from16 v8, v16

    :try_start_1
    invoke-direct/range {v1 .. v8}, Lcom/android/server/am/PeriodicCleanerService;->canCleanPackage(Lcom/android/server/am/ProcessRecord;ILjava/util/HashMap;Ljava/util/List;Ljava/util/List;Ljava/util/List;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1417
    iget-object v1, v0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1418
    iget-object v1, v0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1422
    :cond_1
    iget-object v1, v0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1423
    iget-object v1, v0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1411
    .end local v17    # "N":I
    .end local v18    # "i":I
    .local v0, "N":I
    .restart local v6    # "app":Lcom/android/server/am/ProcessRecord;
    .restart local v7    # "i":I
    :cond_2
    move/from16 v17, v0

    move-object v0, v6

    move/from16 v18, v7

    move-object/from16 v19, v8

    .end local v6    # "app":Lcom/android/server/am/ProcessRecord;
    .end local v7    # "i":I
    .local v0, "app":Lcom/android/server/am/ProcessRecord;
    .restart local v17    # "N":I
    .restart local v18    # "i":I
    goto :goto_1

    .line 1410
    .end local v17    # "N":I
    .end local v18    # "i":I
    .local v0, "N":I
    .restart local v6    # "app":Lcom/android/server/am/ProcessRecord;
    .restart local v7    # "i":I
    :cond_3
    move/from16 v17, v0

    move-object v0, v6

    move/from16 v18, v7

    move-object/from16 v19, v8

    .line 1408
    .end local v0    # "N":I
    .end local v6    # "app":Lcom/android/server/am/ProcessRecord;
    .end local v7    # "i":I
    .restart local v17    # "N":I
    .restart local v18    # "i":I
    :cond_4
    :goto_1
    add-int/lit8 v7, v18, -0x1

    move/from16 v0, v17

    move-object/from16 v8, v19

    .end local v18    # "i":I
    .restart local v7    # "i":I
    goto/16 :goto_0

    .end local v17    # "N":I
    .restart local v0    # "N":I
    :cond_5
    move/from16 v17, v0

    move/from16 v18, v7

    move-object/from16 v19, v8

    .line 1426
    .end local v0    # "N":I
    .end local v7    # "i":I
    .restart local v17    # "N":I
    const-string v0, "finish filterOutKillablePackages"

    invoke-static {v12, v13, v0}, Lcom/android/server/am/PeriodicCleanerService;->checkTime(JLjava/lang/String;)V

    .line 1427
    .end local v17    # "N":I
    monitor-exit v19

    .line 1428
    return-object v15

    .line 1427
    :catchall_0
    move-exception v0

    move-object/from16 v19, v8

    :goto_2
    monitor-exit v19
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2
.end method

.method private finalLruActiveLength(I)I
    .locals 1
    .param p1, "length"    # I

    .line 505
    const/4 v0, 0x3

    if-le p1, v0, :cond_0

    .line 506
    return p1

    .line 508
    :cond_0
    return v0
.end method

.method private findAgingPackageLocked(I)Landroid/util/SparseArray;
    .locals 9
    .param p1, "threshold"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Landroid/util/SparseArray<",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 1265
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 1266
    .local v0, "agingPkgs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1267
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 1268
    .local v2, "startTime":J
    iget-object v4, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruPackages:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    sub-int/2addr v4, p1

    .line 1269
    .local v4, "size":I
    if-lez v4, :cond_1

    .line 1270
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v4, :cond_1

    .line 1271
    iget-object v6, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruPackages:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;

    .line 1272
    .local v6, "info":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;
    invoke-static {v6}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->-$$Nest$fgetmUserId(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;)I

    move-result v7

    invoke-virtual {v0, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/ArrayList;

    .line 1273
    .local v7, "userTargets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v7, :cond_0

    .line 1274
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    move-object v7, v8

    .line 1275
    invoke-static {v6}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->-$$Nest$fgetmUserId(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;)I

    move-result v8

    invoke-virtual {v0, v8, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1277
    :cond_0
    invoke-static {v6}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->-$$Nest$fgetmPackageName(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1270
    nop

    .end local v6    # "info":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;
    .end local v7    # "userTargets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 1280
    .end local v5    # "i":I
    :cond_1
    const-string v5, "finish findAgingPackageLocked"

    invoke-static {v2, v3, v5}, Lcom/android/server/am/PeriodicCleanerService;->checkTime(JLjava/lang/String;)V

    .line 1281
    .end local v2    # "startTime":J
    .end local v4    # "size":I
    monitor-exit v1

    .line 1282
    return-object v0

    .line 1281
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private findOtherGamePackageLocked(ILjava/lang/String;)Ljava/util/List;
    .locals 9
    .param p1, "threshold"    # I
    .param p2, "startProcessName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1289
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1291
    .local v0, "startTime":J
    const/4 v2, 0x0

    .line 1292
    .local v2, "lruPrevPackages":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1293
    .local v3, "gamePackagesUid":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iget-object v4, p0, Lcom/android/server/am/PeriodicCleanerService;->mLock:Ljava/lang/Object;

    monitor-enter v4

    .line 1294
    :try_start_0
    iget-object v5, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruPackages:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 1295
    .local v5, "lruSize":I
    if-lez v5, :cond_1

    .line 1296
    iget-object v6, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruPackages:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 1297
    .local v6, "end":I
    iget-object v7, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruPackages:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    sub-int/2addr v7, p1

    .line 1298
    .local v7, "start":I
    if-gez v7, :cond_0

    const/4 v7, 0x0

    .line 1299
    :cond_0
    iget-object v8, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruPackages:Ljava/util/ArrayList;

    invoke-virtual {v8, v7, v6}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v8

    move-object v2, v8

    .line 1301
    .end local v5    # "lruSize":I
    .end local v6    # "end":I
    .end local v7    # "start":I
    :cond_1
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1304
    if-eqz v2, :cond_3

    .line 1305
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;

    .line 1306
    .local v5, "info":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;
    sget-object v6, Lcom/android/server/am/PeriodicCleanerService;->sGameApp:Ljava/util/List;

    invoke-static {v5}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->-$$Nest$fgetmPackageName(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-static {v5}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->-$$Nest$fgetmPackageName(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;)Ljava/lang/String;

    move-result-object v6

    .line 1307
    invoke-virtual {p2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 1308
    invoke-static {v5}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->-$$Nest$fgetmUid(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1310
    .end local v5    # "info":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;
    :cond_2
    goto :goto_0

    .line 1312
    :cond_3
    const-string v4, "finish findOtherGamePackageLocked"

    invoke-static {v0, v1, v4}, Lcom/android/server/am/PeriodicCleanerService;->checkTime(JLjava/lang/String;)V

    .line 1313
    return-object v3

    .line 1301
    :catchall_0
    move-exception v5

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5
.end method

.method private findOverTimePackageLocked(I)Landroid/util/SparseArray;
    .locals 12
    .param p1, "pressure"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Landroid/util/SparseArray<",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 1230
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 1231
    .local v0, "agingPkgs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1233
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruPackages:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    iget v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mOverTimeAppNum:I

    sub-int/2addr v2, v3

    .line 1234
    .local v2, "size":I
    if-gez v2, :cond_0

    monitor-exit v1

    return-object v0

    .line 1235
    :cond_0
    iget-object v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruPackages:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;

    .line 1236
    .local v3, "tmpInfo":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v2, :cond_3

    .line 1237
    iget-object v5, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruPackages:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;

    .line 1238
    .local v5, "histroyInfo":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;
    invoke-static {v3}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->-$$Nest$fgetmBackgroundTime(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;)J

    move-result-wide v6

    invoke-static {v5}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->-$$Nest$fgetmBackgroundTime(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;)J

    move-result-wide v8

    sub-long/2addr v6, v8

    .line 1239
    .local v6, "diffTime":J
    sget-boolean v8, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z

    if-eqz v8, :cond_1

    .line 1240
    const-string v8, "PeriodicCleaner"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "OverTimeValue:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v3}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->-$$Nest$fgetmBackgroundTime(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;)J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "-"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v5}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->-$$Nest$fgetmBackgroundTime(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;)J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "?"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Lcom/android/server/am/PeriodicCleanerService;->sDefaultTimeLevel:[I

    aget v10, v10, p1

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1243
    const-string v8, "PeriodicCleaner"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "OverTimePackage:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v3}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->-$$Nest$fgetmPackageName(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ":"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v5}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->-$$Nest$fgetmPackageName(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1246
    :cond_1
    sget-object v8, Lcom/android/server/am/PeriodicCleanerService;->sDefaultTimeLevel:[I

    aget v8, v8, p1

    int-to-long v8, v8

    cmp-long v8, v6, v8

    if-ltz v8, :cond_3

    .line 1247
    invoke-static {v5}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->-$$Nest$fgetmUserId(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;)I

    move-result v8

    invoke-virtual {v0, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/ArrayList;

    .line 1248
    .local v8, "userTargets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v8, :cond_2

    .line 1249
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    move-object v8, v9

    .line 1250
    invoke-static {v5}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->-$$Nest$fgetmUserId(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;)I

    move-result v9

    invoke-virtual {v0, v9, v8}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1252
    :cond_2
    invoke-static {v5}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->-$$Nest$fgetmPackageName(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1236
    nop

    .end local v5    # "histroyInfo":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;
    .end local v6    # "diffTime":J
    .end local v8    # "userTargets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 1257
    .end local v2    # "size":I
    .end local v3    # "tmpInfo":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;
    .end local v4    # "i":I
    :cond_3
    monitor-exit v1

    .line 1258
    return-object v0

    .line 1257
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private getAllAudioFocus()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1357
    :try_start_0
    iget-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mGetAllAudioFocusMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mAudioService:Lcom/android/server/audio/AudioService;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 1358
    :catch_0
    move-exception v0

    .line 1359
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAllAudioFocus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PeriodicCleaner"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1361
    .end local v0    # "e":Ljava/lang/Exception;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method private getAllPackagePss()Ljava/util/HashMap;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 765
    const-string v0, "PeriodicCleaner"

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 767
    .local v1, "packagePss":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mProcessStats:Lcom/android/internal/app/procstats/IProcessStats;

    sget-wide v3, Lcom/android/internal/app/procstats/ProcessStats;->COMMIT_PERIOD:J

    const-wide/16 v5, 0x2

    div-long/2addr v3, v5

    const-wide/32 v5, 0x5265c00

    sub-long/2addr v5, v3

    invoke-interface {v2, v5, v6}, Lcom/android/internal/app/procstats/IProcessStats;->getStatsOverTime(J)Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    .line 769
    .local v2, "pfd":Landroid/os/ParcelFileDescriptor;
    const/4 v3, 0x0

    if-nez v2, :cond_0

    .line 770
    const-string v4, "open file error."

    invoke-static {v0, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 771
    return-object v3

    .line 773
    :cond_0
    new-instance v4, Lcom/android/internal/app/procstats/ProcessStats;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Lcom/android/internal/app/procstats/ProcessStats;-><init>(Z)V

    .line 774
    .local v4, "stats":Lcom/android/internal/app/procstats/ProcessStats;
    new-instance v5, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;

    invoke-direct {v5, v2}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    .line 775
    .local v5, "stream":Ljava/io/InputStream;
    invoke-virtual {v4, v5}, Lcom/android/internal/app/procstats/ProcessStats;->read(Ljava/io/InputStream;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 777
    :try_start_1
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 779
    goto :goto_0

    .line 778
    :catch_0
    move-exception v6

    .line 780
    :goto_0
    :try_start_2
    iget-object v6, v4, Lcom/android/internal/app/procstats/ProcessStats;->mReadError:Ljava/lang/String;

    if-eqz v6, :cond_1

    .line 781
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failure reading process stats: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v4, Lcom/android/internal/app/procstats/ProcessStats;->mReadError:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 782
    return-object v3

    .line 784
    :cond_1
    invoke-direct {p0, v4}, Lcom/android/server/am/PeriodicCleanerService;->getAllPackagePssDetail(Lcom/android/internal/app/procstats/ProcessStats;)Ljava/util/HashMap;

    move-result-object v0
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    move-object v1, v0

    .line 787
    .end local v2    # "pfd":Landroid/os/ParcelFileDescriptor;
    .end local v4    # "stats":Lcom/android/internal/app/procstats/ProcessStats;
    .end local v5    # "stream":Ljava/io/InputStream;
    goto :goto_1

    .line 785
    :catch_1
    move-exception v2

    .line 786
    .local v2, "e":Landroid/os/RemoteException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "RemoteException:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 788
    .end local v2    # "e":Landroid/os/RemoteException;
    :goto_1
    return-object v1
.end method

.method private getAllPackagePssDetail(Lcom/android/internal/app/procstats/ProcessStats;)Ljava/util/HashMap;
    .locals 34
    .param p1, "stats"    # Lcom/android/internal/app/procstats/ProcessStats;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/app/procstats/ProcessStats;",
            ")",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 792
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    move-object v1, v0

    .line 793
    .local v1, "packagePss":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    move-object/from16 v2, p1

    iget-object v0, v2, Lcom/android/internal/app/procstats/ProcessStats;->mPackages:Lcom/android/internal/app/ProcessMap;

    .line 794
    invoke-virtual {v0}, Lcom/android/internal/app/ProcessMap;->getMap()Landroid/util/ArrayMap;

    move-result-object v3

    .line 795
    .local v3, "pkgMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;>;"
    invoke-virtual {v3}, Landroid/util/ArrayMap;->size()I

    move-result v4

    .line 797
    .local v4, "NPKG":I
    const/4 v0, 0x0

    move v5, v0

    .local v5, "ip":I
    :goto_0
    if-ge v5, v4, :cond_7

    .line 798
    invoke-virtual {v3, v5}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/lang/String;

    .line 799
    .local v6, "pkgName":Ljava/lang/String;
    invoke-virtual {v3, v5}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/util/SparseArray;

    .line 800
    .local v7, "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
    invoke-virtual {v7}, Landroid/util/SparseArray;->size()I

    move-result v8

    .line 801
    .local v8, "NUID":I
    const-wide/16 v9, 0x0

    .line 803
    .local v9, "maxPackagePssAve":J
    const/4 v0, 0x0

    move-wide v10, v9

    move v9, v0

    .local v9, "iu":I
    .local v10, "maxPackagePssAve":J
    :goto_1
    if-ge v9, v8, :cond_6

    .line 804
    invoke-virtual {v7, v9}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v12

    .line 805
    .local v12, "uid":I
    invoke-virtual {v7, v9}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Landroid/util/LongSparseArray;

    .line 806
    .local v13, "vpkgs":Landroid/util/LongSparseArray;, "Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;"
    invoke-virtual {v13}, Landroid/util/LongSparseArray;->size()I

    move-result v14

    .line 807
    .local v14, "NVERS":I
    const-wide/16 v15, 0x0

    .line 809
    .local v15, "tmpMaxPackagePssAve":J
    const/4 v0, 0x0

    move-wide/from16 v16, v15

    move v15, v0

    .local v15, "iv":I
    .local v16, "tmpMaxPackagePssAve":J
    :goto_2
    if-ge v15, v14, :cond_4

    .line 810
    invoke-virtual {v13, v15}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v18

    .line 811
    .local v18, "vers":J
    invoke-virtual {v13, v15}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/android/internal/app/procstats/ProcessStats$PackageState;

    .line 812
    .local v2, "pkgState":Lcom/android/internal/app/procstats/ProcessStats$PackageState;
    iget-object v0, v2, Lcom/android/internal/app/procstats/ProcessStats$PackageState;->mProcesses:Landroid/util/ArrayMap;

    move-object/from16 v20, v3

    .end local v3    # "pkgMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;>;"
    .local v20, "pkgMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;>;"
    invoke-virtual {v0}, Landroid/util/ArrayMap;->size()I

    move-result v3

    .line 813
    .local v3, "NPROCS":I
    const-wide/16 v21, 0x0

    .line 815
    .local v21, "tmpMaxPackagePssAveVer":J
    const/4 v0, 0x0

    move/from16 v23, v4

    move v4, v0

    .local v4, "iproc":I
    .local v23, "NPKG":I
    :goto_3
    if-ge v4, v3, :cond_2

    .line 817
    :try_start_0
    iget-object v0, v2, Lcom/android/internal/app/procstats/ProcessStats$PackageState;->mProcesses:Landroid/util/ArrayMap;

    invoke-virtual {v0, v4}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/app/procstats/ProcessState;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 818
    .local v0, "proc":Lcom/android/internal/app/procstats/ProcessState;
    move-object/from16 v24, v2

    move/from16 v25, v3

    move-object/from16 v2, p0

    .end local v2    # "pkgState":Lcom/android/internal/app/procstats/ProcessStats$PackageState;
    .end local v3    # "NPROCS":I
    .local v24, "pkgState":Lcom/android/internal/app/procstats/ProcessStats$PackageState;
    .local v25, "NPROCS":I
    :try_start_1
    iget-object v3, v2, Lcom/android/server/am/PeriodicCleanerService;->mFieldProcessState:Ljava/lang/reflect/Field;

    invoke-virtual {v3, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/internal/app/procstats/PssTable;

    .line 820
    .local v3, "mPssTable":Lcom/android/internal/app/procstats/PssTable;
    invoke-virtual {v3}, Lcom/android/internal/app/procstats/PssTable;->getKeyCount()I

    move-result v26

    move/from16 v27, v26

    .line 821
    .local v27, "Num":I
    const-wide/16 v28, 0x0

    .line 827
    .local v28, "maxSingleProcPssAve":J
    const/16 v26, 0x0

    move-object/from16 v30, v0

    move/from16 v0, v26

    .local v0, "ivalue":I
    .local v30, "proc":Lcom/android/internal/app/procstats/ProcessState;
    :goto_4
    move/from16 v2, v27

    .end local v27    # "Num":I
    .local v2, "Num":I
    if-ge v0, v2, :cond_1

    .line 828
    invoke-virtual {v3, v0}, Lcom/android/internal/app/procstats/PssTable;->getKeyAt(I)I

    move-result v26
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move/from16 v27, v26

    .line 829
    .local v27, "key":I
    move/from16 v26, v2

    .end local v2    # "Num":I
    .local v26, "Num":I
    const/4 v2, 0x2

    move-object/from16 v31, v7

    move/from16 v7, v27

    .end local v27    # "key":I
    .local v7, "key":I
    .local v31, "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
    :try_start_2
    invoke-virtual {v3, v7, v2}, Lcom/android/internal/app/procstats/PssTable;->getValue(II)J

    move-result-wide v32

    cmp-long v27, v32, v28

    if-ltz v27, :cond_0

    .line 830
    invoke-virtual {v3, v7, v2}, Lcom/android/internal/app/procstats/PssTable;->getValue(II)J

    move-result-wide v32
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-wide/from16 v27, v32

    move-wide/from16 v28, v27

    .line 827
    .end local v7    # "key":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    move-object/from16 v2, p0

    move/from16 v27, v26

    move-object/from16 v7, v31

    goto :goto_4

    .line 834
    .end local v0    # "ivalue":I
    .end local v3    # "mPssTable":Lcom/android/internal/app/procstats/PssTable;
    .end local v26    # "Num":I
    .end local v28    # "maxSingleProcPssAve":J
    .end local v30    # "proc":Lcom/android/internal/app/procstats/ProcessState;
    :catch_0
    move-exception v0

    goto :goto_5

    .line 827
    .end local v31    # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
    .restart local v0    # "ivalue":I
    .restart local v2    # "Num":I
    .restart local v3    # "mPssTable":Lcom/android/internal/app/procstats/PssTable;
    .local v7, "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
    .restart local v28    # "maxSingleProcPssAve":J
    .restart local v30    # "proc":Lcom/android/internal/app/procstats/ProcessState;
    :cond_1
    move/from16 v26, v2

    move-object/from16 v31, v7

    .line 833
    .end local v0    # "ivalue":I
    .end local v2    # "Num":I
    .end local v7    # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
    .restart local v26    # "Num":I
    .restart local v31    # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
    add-long v21, v21, v28

    .line 836
    .end local v3    # "mPssTable":Lcom/android/internal/app/procstats/PssTable;
    .end local v26    # "Num":I
    .end local v28    # "maxSingleProcPssAve":J
    .end local v30    # "proc":Lcom/android/internal/app/procstats/ProcessState;
    goto :goto_6

    .line 834
    .end local v31    # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
    .restart local v7    # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
    :catch_1
    move-exception v0

    move-object/from16 v31, v7

    .end local v7    # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
    .restart local v31    # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
    goto :goto_5

    .end local v24    # "pkgState":Lcom/android/internal/app/procstats/ProcessStats$PackageState;
    .end local v25    # "NPROCS":I
    .end local v31    # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
    .local v2, "pkgState":Lcom/android/internal/app/procstats/ProcessStats$PackageState;
    .local v3, "NPROCS":I
    .restart local v7    # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
    :catch_2
    move-exception v0

    move-object/from16 v24, v2

    move/from16 v25, v3

    move-object/from16 v31, v7

    .line 835
    .end local v2    # "pkgState":Lcom/android/internal/app/procstats/ProcessStats$PackageState;
    .end local v3    # "NPROCS":I
    .end local v7    # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
    .local v0, "e":Ljava/lang/Exception;
    .restart local v24    # "pkgState":Lcom/android/internal/app/procstats/ProcessStats$PackageState;
    .restart local v25    # "NPROCS":I
    .restart local v31    # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
    :goto_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ProcessState#mPssTable error "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "PeriodicCleaner"

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 815
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_6
    add-int/lit8 v4, v4, 0x1

    move-object/from16 v2, v24

    move/from16 v3, v25

    move-object/from16 v7, v31

    goto/16 :goto_3

    .end local v24    # "pkgState":Lcom/android/internal/app/procstats/ProcessStats$PackageState;
    .end local v25    # "NPROCS":I
    .end local v31    # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
    .restart local v2    # "pkgState":Lcom/android/internal/app/procstats/ProcessStats$PackageState;
    .restart local v3    # "NPROCS":I
    .restart local v7    # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
    :cond_2
    move-object/from16 v24, v2

    move/from16 v25, v3

    move-object/from16 v31, v7

    .line 838
    .end local v2    # "pkgState":Lcom/android/internal/app/procstats/ProcessStats$PackageState;
    .end local v3    # "NPROCS":I
    .end local v4    # "iproc":I
    .end local v7    # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
    .restart local v24    # "pkgState":Lcom/android/internal/app/procstats/ProcessStats$PackageState;
    .restart local v25    # "NPROCS":I
    .restart local v31    # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
    cmp-long v0, v21, v16

    if-ltz v0, :cond_3

    .line 839
    move-wide/from16 v2, v21

    move-wide/from16 v16, v2

    .line 809
    .end local v18    # "vers":J
    .end local v21    # "tmpMaxPackagePssAveVer":J
    .end local v24    # "pkgState":Lcom/android/internal/app/procstats/ProcessStats$PackageState;
    .end local v25    # "NPROCS":I
    :cond_3
    add-int/lit8 v15, v15, 0x1

    move-object/from16 v2, p1

    move-object/from16 v3, v20

    move/from16 v4, v23

    move-object/from16 v7, v31

    goto/16 :goto_2

    .end local v20    # "pkgMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;>;"
    .end local v23    # "NPKG":I
    .end local v31    # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
    .local v3, "pkgMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;>;"
    .local v4, "NPKG":I
    .restart local v7    # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
    :cond_4
    move-object/from16 v20, v3

    move/from16 v23, v4

    move-object/from16 v31, v7

    .line 842
    .end local v3    # "pkgMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;>;"
    .end local v4    # "NPKG":I
    .end local v7    # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
    .end local v15    # "iv":I
    .restart local v20    # "pkgMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;>;"
    .restart local v23    # "NPKG":I
    .restart local v31    # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
    cmp-long v0, v16, v10

    if-ltz v0, :cond_5

    .line 843
    move-wide/from16 v2, v16

    move-wide v10, v2

    .line 803
    .end local v12    # "uid":I
    .end local v13    # "vpkgs":Landroid/util/LongSparseArray;, "Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;"
    .end local v14    # "NVERS":I
    .end local v16    # "tmpMaxPackagePssAve":J
    :cond_5
    add-int/lit8 v9, v9, 0x1

    move-object/from16 v2, p1

    move-object/from16 v3, v20

    move/from16 v4, v23

    move-object/from16 v7, v31

    goto/16 :goto_1

    .end local v20    # "pkgMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;>;"
    .end local v23    # "NPKG":I
    .end local v31    # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
    .restart local v3    # "pkgMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;>;"
    .restart local v4    # "NPKG":I
    .restart local v7    # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
    :cond_6
    move-object/from16 v20, v3

    move/from16 v23, v4

    move-object/from16 v31, v7

    .line 846
    .end local v3    # "pkgMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;>;"
    .end local v4    # "NPKG":I
    .end local v7    # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
    .end local v9    # "iu":I
    .restart local v20    # "pkgMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;>;"
    .restart local v23    # "NPKG":I
    .restart local v31    # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 797
    .end local v6    # "pkgName":Ljava/lang/String;
    .end local v8    # "NUID":I
    .end local v10    # "maxPackagePssAve":J
    .end local v31    # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
    add-int/lit8 v5, v5, 0x1

    move-object/from16 v2, p1

    goto/16 :goto_0

    .line 848
    .end local v5    # "ip":I
    .end local v20    # "pkgMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;>;"
    .end local v23    # "NPKG":I
    .restart local v3    # "pkgMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;>;"
    .restart local v4    # "NPKG":I
    :cond_7
    return-object v1
.end method

.method private getAudioActiveUids()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1330
    iget-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mAudioService:Lcom/android/server/audio/AudioService;

    .line 1331
    invoke-virtual {v0}, Lcom/android/server/audio/AudioService;->getActivePlaybackConfigurations()Ljava/util/List;

    move-result-object v0

    .line 1332
    .local v0, "activePlayers":Ljava/util/List;, "Ljava/util/List<Landroid/media/AudioPlaybackConfiguration;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1333
    .local v1, "activeUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/media/AudioPlaybackConfiguration;

    .line 1334
    .local v3, "conf":Landroid/media/AudioPlaybackConfiguration;
    invoke-virtual {v3}, Landroid/media/AudioPlaybackConfiguration;->getPlayerState()I

    move-result v4

    .line 1335
    .local v4, "state":I
    const/4 v5, 0x2

    if-eq v4, v5, :cond_0

    const/4 v5, 0x3

    if-ne v4, v5, :cond_1

    .line 1336
    :cond_0
    invoke-virtual {v3}, Landroid/media/AudioPlaybackConfiguration;->getClientUid()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1338
    .end local v3    # "conf":Landroid/media/AudioPlaybackConfiguration;
    .end local v4    # "state":I
    :cond_1
    goto :goto_0

    .line 1340
    :cond_2
    invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->getAllAudioFocus()Ljava/util/List;

    move-result-object v2

    .line 1341
    .local v2, "focusUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 1342
    .local v4, "uid":Ljava/lang/Integer;
    invoke-interface {v1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 1343
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1345
    .end local v4    # "uid":Ljava/lang/Integer;
    :cond_3
    goto :goto_1

    .line 1346
    :cond_4
    sget-boolean v3, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_5

    .line 1347
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [I

    .line 1348
    .local v3, "uids":[I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_5

    .line 1349
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    aput v5, v3, v4

    .line 1348
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1352
    .end local v3    # "uids":[I
    .end local v4    # "i":I
    :cond_5
    return-object v1
.end method

.method private getLocationActiveUids()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1377
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1378
    .local v0, "uids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mPMS:Lcom/android/server/am/ProcessManagerService;

    .line 1379
    invoke-virtual {v1}, Lcom/android/server/am/ProcessManagerService;->getProcessPolicy()Lcom/android/server/am/ProcessPolicy;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessPolicy;->getActiveUidRecordList(I)Ljava/util/List;

    move-result-object v1

    .line 1380
    .local v1, "activeUids":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;>;"
    if-eqz v1, :cond_1

    .line 1381
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;

    .line 1382
    .local v3, "r":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;
    iget v4, v3, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->uid:I

    .line 1383
    .local v4, "uid":I
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1384
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1386
    .end local v3    # "r":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;
    .end local v4    # "uid":I
    :cond_0
    goto :goto_0

    .line 1388
    :cond_1
    return-object v0
.end method

.method private getMemPressureLevel()I
    .locals 9

    .line 1159
    new-instance v0, Lcom/android/internal/util/MemInfoReader;

    invoke-direct {v0}, Lcom/android/internal/util/MemInfoReader;-><init>()V

    .line 1160
    .local v0, "minfo":Lcom/android/internal/util/MemInfoReader;
    invoke-virtual {v0}, Lcom/android/internal/util/MemInfoReader;->readMemInfo()V

    .line 1161
    invoke-virtual {v0}, Lcom/android/internal/util/MemInfoReader;->getRawInfo()[J

    move-result-object v1

    .line 1162
    .local v1, "rawInfo":[J
    iget v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_MEMINFO_CACHED:I

    aget-wide v2, v1, v2

    iget v4, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_MEMINFO_SWAPCACHED:I

    aget-wide v5, v1, v4

    add-long/2addr v2, v5

    iget v5, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_MEMINFO_BUFFERS:I

    aget-wide v5, v1, v5

    add-long/2addr v2, v5

    .line 1165
    .local v2, "otherFile":J
    iget v5, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_MEMINFO_SHMEM:I

    aget-wide v5, v1, v5

    iget v7, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_MEMINFO_UNEVICTABLE:I

    aget-wide v7, v1, v7

    add-long/2addr v5, v7

    aget-wide v7, v1, v4

    add-long/2addr v5, v7

    .line 1168
    .local v5, "needRemovedFile":J
    cmp-long v4, v2, v5

    if-lez v4, :cond_0

    .line 1169
    sub-long/2addr v2, v5

    .line 1171
    :cond_0
    iget-object v4, p0, Lcom/android/server/am/PeriodicCleanerService;->mPressureCacheThreshold:[I

    const/4 v7, 0x0

    aget v7, v4, v7

    int-to-long v7, v7

    cmp-long v7, v2, v7

    if-ltz v7, :cond_1

    .line 1172
    const/4 v4, -0x1

    .local v4, "level":I
    goto :goto_0

    .line 1173
    .end local v4    # "level":I
    :cond_1
    const/4 v7, 0x1

    aget v7, v4, v7

    int-to-long v7, v7

    cmp-long v7, v2, v7

    if-ltz v7, :cond_2

    .line 1174
    const/4 v4, 0x0

    .restart local v4    # "level":I
    goto :goto_0

    .line 1175
    .end local v4    # "level":I
    :cond_2
    const/4 v7, 0x2

    aget v4, v4, v7

    int-to-long v7, v4

    cmp-long v4, v2, v7

    if-ltz v4, :cond_3

    .line 1176
    const/4 v4, 0x1

    .restart local v4    # "level":I
    goto :goto_0

    .line 1178
    .end local v4    # "level":I
    :cond_3
    const/4 v4, 0x2

    .line 1180
    .restart local v4    # "level":I
    :goto_0
    sget-boolean v7, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z

    if-eqz v7, :cond_4

    .line 1181
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Other File: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "KB. Mem Pressure Level: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "PeriodicCleaner"

    invoke-static {v8, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1183
    :cond_4
    return v4
.end method

.method private getPackageForegroundTime()Ljava/util/HashMap;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 878
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 879
    .local v6, "currentTime":J
    const-wide/32 v0, 0x5265c00

    sub-long v0, v6, v0

    .line 880
    .local v0, "startTime":J
    const-wide/16 v8, 0x0

    cmp-long v2, v0, v8

    if-gez v2, :cond_0

    const-wide/16 v0, 0x0

    :cond_0
    move-wide v10, v0

    .line 881
    .end local v0    # "startTime":J
    .local v10, "startTime":J
    move-object/from16 v12, p0

    iget-object v0, v12, Lcom/android/server/am/PeriodicCleanerService;->mUSM:Landroid/app/usage/UsageStatsManager;

    const/4 v1, 0x4

    move-wide v2, v10

    move-wide v4, v6

    invoke-virtual/range {v0 .. v5}, Landroid/app/usage/UsageStatsManager;->queryUsageStats(IJJ)Ljava/util/List;

    move-result-object v0

    .line 884
    .local v0, "stats":Ljava/util/List;, "Ljava/util/List<Landroid/app/usage/UsageStats;>;"
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 885
    .local v1, "packageForegroundTime":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_5

    .line 886
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/usage/UsageStats;

    .line 887
    .local v3, "us":Landroid/app/usage/UsageStats;
    if-nez v3, :cond_1

    goto :goto_1

    .line 889
    :cond_1
    invoke-virtual {v3}, Landroid/app/usage/UsageStats;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 890
    .local v4, "packageName":Ljava/lang/String;
    if-nez v4, :cond_2

    goto :goto_1

    .line 892
    :cond_2
    invoke-virtual {v3}, Landroid/app/usage/UsageStats;->getTotalTimeInForeground()J

    move-result-wide v13

    .line 893
    .local v13, "totalTime":J
    cmp-long v5, v13, v8

    if-gtz v5, :cond_3

    goto :goto_1

    .line 895
    :cond_3
    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    .line 896
    .local v5, "foregroundTime":Ljava/lang/Long;
    if-nez v5, :cond_4

    .line 897
    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v1, v4, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 899
    :cond_4
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v15

    add-long/2addr v15, v13

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v1, v4, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 885
    .end local v3    # "us":Landroid/app/usage/UsageStats;
    .end local v4    # "packageName":Ljava/lang/String;
    .end local v5    # "foregroundTime":Ljava/lang/Long;
    .end local v13    # "totalTime":J
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 903
    .end local v2    # "i":I
    :cond_5
    return-object v1
.end method

.method private getVisibleWindowOwner()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1366
    :try_start_0
    iget-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mGetVisibleWindowOwnerMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mWMS:Lcom/android/server/wm/WindowManagerInternal;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1368
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iput-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mLastVisibleUids:Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1369
    return-object v0

    .line 1370
    .end local v0    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :catch_0
    move-exception v0

    .line 1371
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getVisibleWindowOwner: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PeriodicCleaner"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1373
    .end local v0    # "e":Ljava/lang/Exception;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method private handleScreenOff()V
    .locals 18

    .line 907
    move-object/from16 v8, p0

    iget v0, v8, Lcom/android/server/am/PeriodicCleanerService;->mScreenState:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 908
    const-string v0, "PeriodicCleaner"

    const-string v1, "screen on when deap clean, skip"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 909
    return-void

    .line 911
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v9

    .line 912
    .local v9, "startTime":J
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v11, v0

    .line 913
    .local v11, "victimList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v12, v0

    .line 914
    .local v12, "killed":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v13, v0

    .line 915
    .local v13, "topUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iget-object v2, v8, Lcom/android/server/am/PeriodicCleanerService;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 916
    :try_start_0
    iget-object v0, v8, Lcom/android/server/am/PeriodicCleanerService;->mLruPackages:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 917
    .local v0, "N":I
    add-int/lit8 v3, v0, -0x1

    .local v3, "i":I
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_0
    if-ltz v3, :cond_1

    if-ge v4, v1, :cond_1

    .line 918
    iget-object v5, v8, Lcom/android/server/am/PeriodicCleanerService;->mLruPackages:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;

    invoke-static {v5}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->-$$Nest$fgetmUid(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v13, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 917
    add-int/lit8 v3, v3, -0x1

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 920
    .end local v0    # "N":I
    .end local v3    # "i":I
    .end local v4    # "j":I
    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 921
    iget-object v0, v8, Lcom/android/server/am/PeriodicCleanerService;->mLastVisibleUids:Ljava/util/List;

    sget-object v2, Lcom/android/server/am/PeriodicCleanerService;->SYSTEM_UID_OBJ:Ljava/lang/Integer;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 922
    invoke-direct/range {p0 .. p0}, Lcom/android/server/am/PeriodicCleanerService;->getAudioActiveUids()Ljava/util/List;

    move-result-object v14

    .line 923
    .local v14, "audioActiveUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-direct/range {p0 .. p0}, Lcom/android/server/am/PeriodicCleanerService;->getLocationActiveUids()Ljava/util/List;

    move-result-object v15

    .line 924
    .local v15, "locationActiveUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    sget-boolean v0, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z

    if-eqz v0, :cond_2

    .line 925
    const-string v0, "PeriodicCleaner"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TopPkg="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", AudioActive="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", LocationActive="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", LastVisible="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v8, Lcom/android/server/am/PeriodicCleanerService;->mLastVisibleUids:Ljava/util/List;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 930
    :cond_2
    iget-object v3, v8, Lcom/android/server/am/PeriodicCleanerService;->mAMS:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v3

    .line 931
    :try_start_1
    iget-object v0, v8, Lcom/android/server/am/PeriodicCleanerService;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessList;->getLruSizeLOSP()I

    move-result v0

    .line 932
    .restart local v0    # "N":I
    add-int/lit8 v2, v0, -0x1

    .local v2, "i":I
    :goto_1
    if-ltz v2, :cond_4

    .line 933
    iget-object v4, v8, Lcom/android/server/am/PeriodicCleanerService;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget-object v4, v4, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    invoke-virtual {v4}, Lcom/android/server/am/ProcessList;->getLruProcessesLOSP()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/am/ProcessRecord;

    .line 935
    .local v4, "app":Lcom/android/server/am/ProcessRecord;
    iget-object v5, v4, Lcom/android/server/am/ProcessRecord;->mProfile:Lcom/android/server/am/ProcessProfileRecord;

    invoke-virtual {v5}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J

    move-result-wide v5

    const-wide/16 v16, 0x400

    div-long v5, v5, v16

    iget v7, v8, Lcom/android/server/am/PeriodicCleanerService;->mAppMemThreshold:I

    move/from16 v17, v2

    .end local v2    # "i":I
    .local v17, "i":I
    int-to-long v1, v7

    cmp-long v1, v5, v1

    if-lez v1, :cond_3

    iget-object v1, v4, Lcom/android/server/am/ProcessRecord;->mProfile:Lcom/android/server/am/ProcessProfileRecord;

    .line 936
    invoke-virtual {v1}, Lcom/android/server/am/ProcessProfileRecord;->getPid()I

    move-result v1

    sget v2, Lcom/android/server/am/ActivityManagerService;->MY_PID:I

    if-eq v1, v2, :cond_3

    iget v1, v4, Lcom/android/server/am/ProcessRecord;->uid:I

    .line 938
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v14, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    iget v1, v4, Lcom/android/server/am/ProcessRecord;->uid:I

    .line 939
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v15, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    iget v1, v4, Lcom/android/server/am/ProcessRecord;->uid:I

    .line 940
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v13, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 941
    invoke-interface {v11, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 932
    .end local v4    # "app":Lcom/android/server/am/ProcessRecord;
    :cond_3
    add-int/lit8 v2, v17, -0x1

    const/4 v1, 0x2

    .end local v17    # "i":I
    .restart local v2    # "i":I
    goto :goto_1

    :cond_4
    move/from16 v17, v2

    .line 944
    .end local v2    # "i":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_6

    .line 945
    iget v2, v8, Lcom/android/server/am/PeriodicCleanerService;->mScreenState:I

    const/4 v4, 0x2

    if-eq v2, v4, :cond_5

    .line 946
    const-string v2, "PeriodicCleaner"

    const-string v4, "screen on when deap clean, abort"

    invoke-static {v2, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 947
    goto :goto_3

    .line 949
    :cond_5
    invoke-interface {v11, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/ProcessRecord;

    .line 950
    .local v2, "app":Lcom/android/server/am/ProcessRecord;
    const-string v5, "PeriodicCleaner"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Kill process "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v2, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", pid "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v2, Lcom/android/server/am/ProcessRecord;->mProfile:Lcom/android/server/am/ProcessProfileRecord;

    .line 951
    invoke-virtual {v7}, Lcom/android/server/am/ProcessProfileRecord;->getPid()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", pss "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v2, Lcom/android/server/am/ProcessRecord;->mProfile:Lcom/android/server/am/ProcessProfileRecord;

    move-object/from16 v17, v5

    invoke-virtual {v7}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J

    move-result-wide v4

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " for abnormal mem usage."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 950
    move-object/from16 v5, v17

    invoke-static {v5, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 953
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deap clean "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v2, Lcom/android/server/am/ProcessRecord;->mProfile:Lcom/android/server/am/ProcessProfileRecord;

    invoke-virtual {v5}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x5

    const/4 v6, 0x1

    invoke-virtual {v2, v4, v5, v6}, Lcom/android/server/am/ProcessRecord;->killLocked(Ljava/lang/String;IZ)V

    .line 954
    iget-object v4, v2, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-interface {v12, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 944
    nop

    .end local v2    # "app":Lcom/android/server/am/ProcessRecord;
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_2

    .line 956
    .end local v0    # "N":I
    .end local v1    # "i":I
    :cond_6
    :goto_3
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 957
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_7

    .line 958
    new-instance v0, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;

    .line 959
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const/4 v5, -0x1

    const/4 v6, 0x2

    const-string v7, "deap"

    move-object v1, v0

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v7}, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;-><init>(Lcom/android/server/am/PeriodicCleanerService;JIILjava/lang/String;)V

    .line 960
    .local v0, "cInfo":Lcom/android/server/am/PeriodicCleanerService$CleanInfo;
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v12}, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->addCleanList(ILjava/util/List;)V

    .line 961
    invoke-direct {v8, v0}, Lcom/android/server/am/PeriodicCleanerService;->addCleanHistory(Lcom/android/server/am/PeriodicCleanerService$CleanInfo;)V

    .line 963
    .end local v0    # "cInfo":Lcom/android/server/am/PeriodicCleanerService$CleanInfo;
    :cond_7
    const-string v0, "finish deap clean"

    invoke-static {v9, v10, v0}, Lcom/android/server/am/PeriodicCleanerService;->checkTime(JLjava/lang/String;)V

    .line 964
    return-void

    .line 956
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 920
    .end local v14    # "audioActiveUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v15    # "locationActiveUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private init()V
    .locals 9

    .line 388
    iget-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnable:Z

    if-eqz v0, :cond_6

    .line 389
    invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->dependencyCheck()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 390
    iput-boolean v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnable:Z

    .line 391
    return-void

    .line 393
    :cond_0
    invoke-static {}, Landroid/os/Process;->getTotalMemory()J

    move-result-wide v2

    const-wide/32 v4, 0x40000000

    div-long/2addr v2, v4

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    long-to-int v0, v2

    .line 394
    .local v0, "totalMemGB":I
    const/4 v2, 0x0

    .line 395
    .local v2, "index":I
    const/4 v3, 0x4

    const/4 v4, 0x1

    if-le v0, v3, :cond_1

    .line 396
    iput-boolean v4, p0, Lcom/android/server/am/PeriodicCleanerService;->mHighDevice:Z

    .line 398
    div-int/lit8 v2, v0, 0x2

    .line 399
    const/4 v3, 0x2

    iput v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mFgTrimTheshold:I

    goto :goto_0

    .line 402
    :cond_1
    add-int/lit8 v2, v0, -0x2

    .line 403
    iput v4, p0, Lcom/android/server/am/PeriodicCleanerService;->mFgTrimTheshold:I

    .line 406
    :goto_0
    const/4 v3, 0x6

    if-le v2, v3, :cond_2

    const/4 v2, 0x6

    .line 407
    :cond_2
    invoke-direct {p0, v2}, Lcom/android/server/am/PeriodicCleanerService;->loadLruLengthConfig(I)Z

    move-result v3

    if-nez v3, :cond_3

    .line 408
    invoke-direct {p0, v2}, Lcom/android/server/am/PeriodicCleanerService;->loadDefLruLengthConfig(I)V

    .line 410
    :cond_3
    invoke-direct {p0, v2}, Lcom/android/server/am/PeriodicCleanerService;->loadCacheLevelConfig(I)Z

    move-result v3

    if-nez v3, :cond_4

    .line 411
    invoke-direct {p0, v2}, Lcom/android/server/am/PeriodicCleanerService;->loadDefCacheLevelConfig(I)V

    .line 413
    :cond_4
    invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->loadDefTimeLevelConfig()V

    .line 414
    sget-object v3, Lcom/android/server/am/PeriodicCleanerService;->sOverTimeAppNumArray:[I

    aget v3, v3, v2

    iput v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mOverTimeAppNum:I

    .line 415
    sget-object v3, Lcom/android/server/am/PeriodicCleanerService;->sGamePlayAppNumArray:[I

    aget v3, v3, v2

    iput v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mGamePlayAppNum:I

    .line 417
    const-string v3, "persist.sys.periodic.u.fgtrim"

    invoke-static {v3, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 418
    invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->checkEnableFgTrim()Z

    move-result v3

    if-eqz v3, :cond_5

    const/16 v3, 0x8

    if-ge v0, v3, :cond_5

    move v1, v4

    goto :goto_1

    :cond_5
    nop

    :goto_1
    iput-boolean v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnableFgTrim:Z

    .line 421
    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 422
    .local v1, "r":Landroid/content/res/Resources;
    nop

    .line 423
    const v3, 0x110300b2

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    .line 424
    .local v3, "homeOrRecentsList":[Ljava/lang/String;
    nop

    .line 425
    const v4, 0x110300b3

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 426
    .local v4, "systemRelatedPkgList":[Ljava/lang/String;
    nop

    .line 427
    const v5, 0x110300b0

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    .line 428
    .local v5, "cleanWhiteList":[Ljava/lang/String;
    nop

    .line 429
    const v6, 0x110300b1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    .line 431
    .local v6, "gameList":[Ljava/lang/String;
    sget-object v7, Lcom/android/server/am/PeriodicCleanerService;->sHomeOrRecents:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 432
    sget-object v7, Lcom/android/server/am/PeriodicCleanerService;->sSystemRelatedPkgs:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 433
    sget-object v7, Lcom/android/server/am/PeriodicCleanerService;->sCleanWhiteList:Ljava/util/List;

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 434
    sget-object v7, Lcom/android/server/am/PeriodicCleanerService;->sGameApp:Ljava/util/List;

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 436
    sget-boolean v7, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z

    if-eqz v7, :cond_6

    .line 437
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "HomeOrRecents pkgs: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/android/server/am/PeriodicCleanerService;->sHomeOrRecents:Ljava/util/List;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\nSystemRelated pkgs: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/android/server/am/PeriodicCleanerService;->sSystemRelatedPkgs:Ljava/util/List;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\nCleanWhiteList pkgs: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/android/server/am/PeriodicCleanerService;->sCleanWhiteList:Ljava/util/List;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\nGame pkgs: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/android/server/am/PeriodicCleanerService;->sGameApp:Ljava/util/List;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "PeriodicCleaner"

    invoke-static {v8, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    .end local v0    # "totalMemGB":I
    .end local v1    # "r":Landroid/content/res/Resources;
    .end local v2    # "index":I
    .end local v3    # "homeOrRecentsList":[Ljava/lang/String;
    .end local v4    # "systemRelatedPkgList":[Ljava/lang/String;
    .end local v5    # "cleanWhiteList":[Ljava/lang/String;
    .end local v6    # "gameList":[Ljava/lang/String;
    :cond_6
    return-void
.end method

.method private isActive(Lcom/android/server/am/ProcessRecord;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Z
    .locals 5
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/server/am/ProcessRecord;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    .line 1516
    .local p2, "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .local p3, "activeAudioUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .local p4, "locationActiveUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 1517
    .local v0, "packageName":Ljava/lang/String;
    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I

    move-result v1

    const-string v2, "PeriodicCleaner"

    const/4 v3, 0x1

    if-ltz v1, :cond_4

    iget v1, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    .line 1523
    :cond_0
    iget v1, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p4, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget v1, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p3, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    .line 1529
    :cond_1
    const/4 v1, 0x0

    return v1

    .line 1524
    :cond_2
    :goto_0
    sget-boolean v1, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z

    if-eqz v1, :cond_3

    .line 1525
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " is audio or gps active."

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1527
    :cond_3
    return v3

    .line 1518
    :cond_4
    :goto_1
    sget-boolean v1, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z

    if-eqz v1, :cond_5

    .line 1519
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " is persistent or owning visible window."

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1521
    :cond_5
    return v3
.end method

.method private isCameraForegroundCase()Z
    .locals 6

    .line 1437
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mATMS:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskManagerService;->getFocusedRootTaskInfo()Landroid/app/ActivityTaskManager$RootTaskInfo;

    move-result-object v1
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1438
    .local v1, "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    const-string v2, "PeriodicCleaner"

    if-eqz v1, :cond_3

    :try_start_1
    iget-object v3, v1, Landroid/app/ActivityTaskManager$RootTaskInfo;->topActivity:Landroid/content/ComponentName;

    if-nez v3, :cond_0

    goto :goto_0

    .line 1442
    :cond_0
    iget-object v3, v1, Landroid/app/ActivityTaskManager$RootTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 1443
    .local v3, "packageName":Ljava/lang/String;
    sget-boolean v4, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z

    if-eqz v4, :cond_1

    .line 1444
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "current focused package: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1446
    :cond_1
    const-string v2, "com.android.camera"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1447
    const/4 v0, 0x1

    return v0

    .line 1451
    .end local v1    # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    .end local v3    # "packageName":Ljava/lang/String;
    :cond_2
    goto :goto_1

    .line 1439
    .restart local v1    # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    :cond_3
    :goto_0
    const-string v3, "get getFocusedStackInfo error."

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1440
    return v0

    .line 1449
    .end local v1    # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    :catch_0
    move-exception v1

    .line 1452
    :goto_1
    return v0
.end method

.method private isDynWhitelist(Ljava/lang/String;Ljava/util/HashMap;)Z
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;)Z"
        }
    .end annotation

    .line 1533
    .local p2, "dynWhitelist":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    invoke-virtual {p2, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1534
    sget-boolean v0, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 1535
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is dynamic whitelist."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PeriodicCleaner"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1537
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 1539
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method private static isHomeOrRecents(Ljava/lang/String;)Z
    .locals 1
    .param p0, "packageName"    # Ljava/lang/String;

    .line 1681
    sget-object v0, Lcom/android/server/am/PeriodicCleanerService;->sHomeOrRecents:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isHomeOrRecentsToKeepAlive(Ljava/lang/String;)Z
    .locals 1
    .param p0, "packageName"    # Ljava/lang/String;

    .line 2006
    sget-object v0, Lcom/android/server/am/PeriodicCleanerService;->sHomeOrRecents:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private isImportant(Lcom/android/server/am/ProcessRecord;I)Z
    .locals 4
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "pressure"    # I

    .line 1547
    const-string v0, "PeriodicCleaner"

    const/4 v1, 0x1

    if-gtz p2, :cond_1

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->mServices:Lcom/android/server/am/ProcessServiceRecord;

    invoke-virtual {v2}, Lcom/android/server/am/ProcessServiceRecord;->hasForegroundServices()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1548
    sget-boolean v2, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 1549
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " has ForegroundService."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1551
    :cond_0
    return v1

    .line 1553
    :cond_1
    if-gt p2, v1, :cond_3

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget v3, p1, Lcom/android/server/am/ProcessRecord;->userId:I

    invoke-static {v2, v3}, Lmiui/process/ProcessManager;->isLockedApplication(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1555
    sget-boolean v2, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z

    if-eqz v2, :cond_2

    .line 1556
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " isLocked."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1558
    :cond_2
    return v1

    .line 1560
    :cond_3
    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v2}, Lcom/android/server/am/ProcessStateRecord;->getAdjSource()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lcom/android/server/am/ProcessRecord;

    if-eqz v2, :cond_5

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    .line 1561
    invoke-virtual {v2}, Lcom/android/server/am/ProcessStateRecord;->getAdjSource()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/ProcessRecord;

    iget v2, v2, Lcom/android/server/am/ProcessRecord;->uid:I

    iget v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mLastNonSystemFgUid:I

    if-ne v2, v3, :cond_5

    .line 1562
    sget-boolean v2, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z

    if-eqz v2, :cond_4

    .line 1563
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Last top pkg "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mLastNonSystemFgPkg:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " depend "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1565
    :cond_4
    return v1

    .line 1567
    :cond_5
    const/4 v0, 0x0

    return v0
.end method

.method private isSkipClean(Lcom/android/server/am/ProcessRecord;I)Z
    .locals 2
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "pressure"    # I

    .line 1505
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I

    move-result v0

    const/16 v1, 0x384

    if-lt v0, v1, :cond_0

    .line 1506
    const/4 v0, 0x0

    return v0

    .line 1508
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method private static isSystemPackage(Ljava/lang/String;)Z
    .locals 1
    .param p0, "packageName"    # Ljava/lang/String;

    .line 1685
    sget-object v0, Lcom/android/server/am/PeriodicCleanerService;->sSystemRelatedPkgs:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p0}, Lcom/android/server/am/PeriodicCleanerService;->isHomeOrRecents(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private static isWhiteListPackage(Ljava/lang/String;)Z
    .locals 1
    .param p0, "packageName"    # Ljava/lang/String;

    .line 1689
    sget-object v0, Lcom/android/server/am/PeriodicCleanerService;->sCleanWhiteList:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private killTargets(ILjava/util/ArrayList;ILjava/lang/String;)V
    .locals 7
    .param p1, "userId"    # I
    .param p3, "killLevel"    # I
    .param p4, "reason"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1574
    .local p2, "targets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_0

    .line 1575
    return-void

    .line 1578
    :cond_0
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1579
    .local v0, "startTime":J
    new-instance v2, Landroid/util/ArrayMap;

    invoke-direct {v2}, Landroid/util/ArrayMap;-><init>()V

    .line 1580
    .local v2, "killList":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/Integer;Ljava/util/List<Ljava/lang/String;>;>;"
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, p2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1581
    new-instance v3, Lmiui/process/ProcessConfig;

    const/16 v4, 0xa

    invoke-direct {v3, v4, p1, v2, p4}, Lmiui/process/ProcessConfig;-><init>(IILandroid/util/ArrayMap;Ljava/lang/String;)V

    .line 1583
    .local v3, "config":Lmiui/process/ProcessConfig;
    iget-object v4, p0, Lcom/android/server/am/PeriodicCleanerService;->mPMS:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v4, v3}, Lcom/android/server/am/ProcessManagerService;->kill(Lmiui/process/ProcessConfig;)Z

    .line 1584
    sget-boolean v4, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z

    if-eqz v4, :cond_1

    .line 1585
    const-string v4, "PeriodicCleaner"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "User"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": Clean "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1587
    :cond_1
    const-string v4, "finish clean victim packages"

    invoke-static {v0, v1, v4}, Lcom/android/server/am/PeriodicCleanerService;->checkTime(JLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1590
    .end local v0    # "startTime":J
    .end local v2    # "killList":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/Integer;Ljava/util/List<Ljava/lang/String;>;>;"
    .end local v3    # "config":Lmiui/process/ProcessConfig;
    goto :goto_0

    .line 1588
    :catch_0
    move-exception v0

    .line 1589
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1591
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private loadCacheLevelConfig(I)Z
    .locals 10
    .param p1, "memIndex"    # I

    .line 539
    const-string v0, "persist.sys.periodic.cache_level_config"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 540
    .local v0, "value":Ljava/lang/String;
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 541
    .local v1, "memArray":[Ljava/lang/String;
    array-length v2, v1

    const/4 v3, 0x7

    const/4 v4, 0x0

    if-ne v2, v3, :cond_4

    aget-object v2, v1, p1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_0

    goto :goto_1

    .line 545
    :cond_0
    aget-object v2, v1, p1

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 546
    .local v2, "pressureArray":[Ljava/lang/String;
    array-length v3, v2

    const/4 v5, 0x3

    if-eq v3, v5, :cond_1

    .line 547
    return v4

    .line 550
    :cond_1
    const/4 v3, 0x0

    .line 551
    .local v3, "cache":I
    const/4 v5, 0x0

    .line 552
    .local v5, "index":I
    array-length v6, v2

    move v7, v4

    :goto_0
    if-ge v7, v6, :cond_3

    aget-object v8, v2, v7

    .line 553
    .local v8, "str":Ljava/lang/String;
    const/4 v3, 0x0

    .line 555
    :try_start_0
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v3, v9

    .line 558
    nop

    .line 559
    if-gtz v3, :cond_2

    return v4

    .line 560
    :cond_2
    iget-object v9, p0, Lcom/android/server/am/PeriodicCleanerService;->mPressureCacheThreshold:[I

    aput v3, v9, v5

    .line 561
    nop

    .end local v8    # "str":Ljava/lang/String;
    add-int/lit8 v5, v5, 0x1

    .line 552
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 556
    .restart local v8    # "str":Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 557
    .local v6, "e":Ljava/lang/Exception;
    return v4

    .line 563
    .end local v6    # "e":Ljava/lang/Exception;
    .end local v8    # "str":Ljava/lang/String;
    :cond_3
    const/4 v4, 0x1

    return v4

    .line 542
    .end local v2    # "pressureArray":[Ljava/lang/String;
    .end local v3    # "cache":I
    .end local v5    # "index":I
    :cond_4
    :goto_1
    return v4
.end method

.method private loadDefCacheLevelConfig(I)V
    .locals 4
    .param p1, "memIndex"    # I

    .line 574
    iget-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mPressureCacheThreshold:[I

    sget-object v1, Lcom/android/server/am/PeriodicCleanerService;->sDefaultCacheLevel:[[I

    aget-object v1, v1, p1

    const/4 v2, 0x0

    aget v3, v1, v2

    aput v3, v0, v2

    .line 575
    const/4 v2, 0x1

    aget v3, v1, v2

    aput v3, v0, v2

    .line 576
    const/4 v2, 0x2

    aget v1, v1, v2

    aput v1, v0, v2

    .line 577
    return-void
.end method

.method private loadDefLruLengthConfig(I)V
    .locals 4
    .param p1, "memIndex"    # I

    .line 567
    sget-object v0, Lcom/android/server/am/PeriodicCleanerService;->sDefaultActiveLength:[[I

    aget-object v0, v0, p1

    const/4 v1, 0x0

    aget v2, v0, v1

    iput v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruActiveLength:I

    .line 568
    iget-object v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mPressureAgingThreshold:[I

    aput v2, v3, v1

    .line 569
    const/4 v1, 0x1

    aget v2, v0, v1

    aput v2, v3, v1

    .line 570
    const/4 v1, 0x2

    aget v0, v0, v1

    aput v0, v3, v1

    .line 571
    return-void
.end method

.method private loadDefTimeLevelConfig()V
    .locals 4

    .line 580
    invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->updateTimeLevel()V

    .line 581
    iget-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mPressureTimeThreshold:[I

    sget-object v1, Lcom/android/server/am/PeriodicCleanerService;->sDefaultTimeLevel:[I

    const/4 v2, 0x0

    aget v3, v1, v2

    aput v3, v0, v2

    .line 582
    const/4 v2, 0x1

    aget v3, v1, v2

    aput v3, v0, v2

    .line 583
    const/4 v2, 0x2

    aget v1, v1, v2

    aput v1, v0, v2

    .line 584
    return-void
.end method

.method private loadLruLengthConfig(I)Z
    .locals 7
    .param p1, "memIndex"    # I

    .line 514
    const-string v0, "persist.sys.periodic.lru_active_length"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 515
    .local v0, "value":Ljava/lang/String;
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 516
    .local v1, "memArray":[Ljava/lang/String;
    array-length v2, v1

    const/4 v3, 0x7

    const/4 v4, 0x0

    if-eq v2, v3, :cond_0

    .line 517
    return v4

    .line 520
    :cond_0
    const/4 v2, 0x0

    .line 522
    .local v2, "len":I
    :try_start_0
    aget-object v3, v1, p1

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v2, v3

    .line 525
    nop

    .line 526
    if-gtz v2, :cond_1

    .line 527
    return v4

    .line 530
    :cond_1
    iput v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruActiveLength:I

    .line 531
    iget-object v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mPressureAgingThreshold:[I

    invoke-direct {p0, v2}, Lcom/android/server/am/PeriodicCleanerService;->finalLruActiveLength(I)I

    move-result v5

    aput v5, v3, v4

    .line 532
    iget-object v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mPressureAgingThreshold:[I

    iget v4, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruActiveLength:I

    mul-int/lit8 v4, v4, 0x3

    div-int/lit8 v4, v4, 0x4

    invoke-direct {p0, v4}, Lcom/android/server/am/PeriodicCleanerService;->finalLruActiveLength(I)I

    move-result v4

    const/4 v5, 0x1

    aput v4, v3, v5

    .line 533
    iget-object v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mPressureAgingThreshold:[I

    iget v4, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruActiveLength:I

    const/4 v6, 0x2

    div-int/2addr v4, v6

    invoke-direct {p0, v4}, Lcom/android/server/am/PeriodicCleanerService;->finalLruActiveLength(I)I

    move-result v4

    aput v4, v3, v6

    .line 534
    return v5

    .line 523
    :catch_0
    move-exception v3

    .line 524
    .local v3, "e":Ljava/lang/Exception;
    return v4
.end method

.method private obtainPackageUseInfo(ILjava/lang/String;J)Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;
    .locals 7
    .param p1, "uid"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "backgroundTime"    # J

    .line 1036
    new-instance v6, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;

    move-object v0, v6

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;-><init>(Lcom/android/server/am/PeriodicCleanerService;ILjava/lang/String;J)V

    return-object v6
.end method

.method private onActivityManagerReady()V
    .locals 2

    .line 251
    const-string v0, "ProcessManager"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/ProcessManagerService;

    iput-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mPMS:Lcom/android/server/am/ProcessManagerService;

    .line 252
    const-string v0, "activity"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/ActivityManagerService;

    iput-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mAMS:Lcom/android/server/am/ActivityManagerService;

    .line 253
    invoke-static {}, Landroid/app/ActivityTaskManager;->getService()Landroid/app/IActivityTaskManager;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/ActivityTaskManagerService;

    iput-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mATMS:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 254
    const-class v0, Lcom/android/server/wm/WindowManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/WindowManagerInternal;

    iput-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mWMS:Lcom/android/server/wm/WindowManagerInternal;

    .line 255
    const-string v0, "package"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

    iput-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mPKMS:Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

    .line 256
    const-string v0, "audio"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/audio/AudioService;

    iput-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mAudioService:Lcom/android/server/audio/AudioService;

    .line 257
    nop

    .line 258
    const-string v0, "procstats"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 257
    invoke-static {v0}, Lcom/android/internal/app/procstats/IProcessStats$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/app/procstats/IProcessStats;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mProcessStats:Lcom/android/internal/app/procstats/IProcessStats;

    .line 259
    iget-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "usagestats"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/usage/UsageStatsManager;

    iput-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mUSM:Landroid/app/usage/UsageStatsManager;

    .line 260
    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mPMS:Lcom/android/server/am/ProcessManagerService;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mAMS:Lcom/android/server/am/ActivityManagerService;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mATMS:Lcom/android/server/wm/ActivityTaskManagerService;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mWMS:Lcom/android/server/wm/WindowManagerInternal;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mProcessStats:Lcom/android/internal/app/procstats/IProcessStats;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mPKMS:Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mAudioService:Lcom/android/server/audio/AudioService;

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 262
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnable:Z

    .line 263
    const-string v0, "PeriodicCleaner"

    const-string v1, "disable periodic for dependencies service not available"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    :cond_1
    return-void
.end method

.method private onBootComplete()V
    .locals 7

    .line 274
    iget-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnable:Z

    if-eqz v0, :cond_4

    .line 275
    iget-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/android/server/am/PeriodicCleanerService;->registerCloudObserver(Landroid/content/Context;)V

    .line 276
    iget-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "cloud_periodic_enable"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    const-string v3, "PeriodicCleaner"

    if-eqz v0, :cond_0

    .line 278
    iget-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mContext:Landroid/content/Context;

    .line 280
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 279
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 278
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnable:Z

    .line 283
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "set enable state from database: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnable:Z

    iput-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mReady:Z

    .line 288
    iget-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/android/server/am/PeriodicCleanerService;->registerCloudWhiteListObserver(Landroid/content/Context;)V

    .line 289
    iget-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "cloud_periodic_white_list"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 291
    iget-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mContext:Landroid/content/Context;

    .line 292
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 291
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 295
    .local v0, "whiteListStr":Ljava/lang/String;
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 296
    .local v1, "whiteList":[Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v5, v1

    if-ge v4, v5, :cond_2

    .line 297
    sget-object v5, Lcom/android/server/am/PeriodicCleanerService;->sCleanWhiteList:Ljava/util/List;

    aget-object v6, v1, v4

    invoke-interface {v5, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    goto :goto_1

    .line 298
    :cond_1
    sget-object v5, Lcom/android/server/am/PeriodicCleanerService;->sCleanWhiteList:Ljava/util/List;

    aget-object v6, v1, v4

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 296
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 300
    .end local v4    # "i":I
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "set white list from database, current list: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/android/server/am/PeriodicCleanerService;->sCleanWhiteList:Ljava/util/List;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    .end local v0    # "whiteListStr":Ljava/lang/String;
    .end local v1    # "whiteList":[Ljava/lang/String;
    :cond_3
    iget-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/android/server/am/PeriodicCleanerService;->registerCloudGameObserver(Landroid/content/Context;)V

    .line 305
    iget-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "cloud_periodic_game_enable"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 307
    iget-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mContext:Landroid/content/Context;

    .line 309
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 308
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 307
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnableGameClean:Z

    .line 312
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "set game enable state from database: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnableGameClean:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    :cond_4
    iget-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mHighDevice:Z

    if-nez v0, :cond_5

    .line 316
    iget-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mHandler:Lcom/android/server/am/PeriodicCleanerService$MyHandler;

    const/16 v1, 0x13

    const-string v2, "cch-empty"

    const/4 v3, 0x6

    const/16 v4, 0x384

    invoke-virtual {v0, v3, v4, v1, v2}, Lcom/android/server/am/PeriodicCleanerService$MyHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 318
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mHandler:Lcom/android/server/am/PeriodicCleanerService$MyHandler;

    const-wide/16 v2, 0x2710

    invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/am/PeriodicCleanerService$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 320
    .end local v0    # "msg":Landroid/os/Message;
    :cond_5
    return-void
.end method

.method private pressureToString(I)Ljava/lang/String;
    .locals 1
    .param p1, "pressure"    # I

    .line 1660
    const/4 v0, 0x0

    .line 1661
    .local v0, "ret":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 1675
    const-string/jumbo v0, "unkown"

    goto :goto_0

    .line 1672
    :pswitch_0
    const-string v0, "critical"

    .line 1673
    goto :goto_0

    .line 1669
    :pswitch_1
    const-string v0, "min"

    .line 1670
    goto :goto_0

    .line 1666
    :pswitch_2
    const-string v0, "low"

    .line 1667
    goto :goto_0

    .line 1663
    :pswitch_3
    const-string v0, "normal"

    .line 1664
    nop

    .line 1677
    :goto_0
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private reCheckPressureAndClean()V
    .locals 2

    .line 1093
    invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->getMemPressureLevel()I

    move-result v0

    .line 1094
    .local v0, "pressure":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1095
    invoke-direct {p0, v0}, Lcom/android/server/am/PeriodicCleanerService;->cleanPackageByTime(I)V

    .line 1097
    :cond_0
    return-void
.end method

.method private registerCloudGameObserver(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 371
    new-instance v0, Lcom/android/server/am/PeriodicCleanerService$3;

    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mHandler:Lcom/android/server/am/PeriodicCleanerService$MyHandler;

    invoke-direct {v0, p0, v1, p1}, Lcom/android/server/am/PeriodicCleanerService$3;-><init>(Lcom/android/server/am/PeriodicCleanerService;Landroid/os/Handler;Landroid/content/Context;)V

    .line 382
    .local v0, "observer":Landroid/database/ContentObserver;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 383
    const-string v2, "cloud_periodic_game_enable"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 382
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 385
    return-void
.end method

.method private registerCloudObserver(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 331
    new-instance v0, Lcom/android/server/am/PeriodicCleanerService$1;

    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mHandler:Lcom/android/server/am/PeriodicCleanerService$MyHandler;

    invoke-direct {v0, p0, v1, p1}, Lcom/android/server/am/PeriodicCleanerService$1;-><init>(Lcom/android/server/am/PeriodicCleanerService;Landroid/os/Handler;Landroid/content/Context;)V

    .line 343
    .local v0, "observer":Landroid/database/ContentObserver;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 344
    const-string v2, "cloud_periodic_enable"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 343
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 346
    return-void
.end method

.method private registerCloudWhiteListObserver(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 349
    new-instance v0, Lcom/android/server/am/PeriodicCleanerService$2;

    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mHandler:Lcom/android/server/am/PeriodicCleanerService$MyHandler;

    invoke-direct {v0, p0, v1, p1}, Lcom/android/server/am/PeriodicCleanerService$2;-><init>(Lcom/android/server/am/PeriodicCleanerService;Landroid/os/Handler;Landroid/content/Context;)V

    .line 365
    .local v0, "observer":Landroid/database/ContentObserver;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 366
    const-string v2, "cloud_periodic_white_list"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 365
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 368
    return-void
.end method

.method private reportCleanProcess(IILjava/lang/String;)V
    .locals 11
    .param p1, "minAdj"    # I
    .param p2, "procState"    # I
    .param p3, "reason"    # Ljava/lang/String;

    .line 653
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PeriodicCleaner("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 654
    .local v0, "longReason":Ljava/lang/String;
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    .line 655
    .local v1, "killedArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    iget-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mAMS:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v2

    .line 656
    :try_start_0
    iget-object v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    invoke-virtual {v3}, Lcom/android/server/am/ProcessList;->getLruSizeLOSP()I

    move-result v3

    .line 657
    .local v3, "N":I
    add-int/lit8 v4, v3, -0x2

    .local v4, "i":I
    :goto_0
    if-ltz v4, :cond_3

    .line 658
    iget-object v5, p0, Lcom/android/server/am/PeriodicCleanerService;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget-object v5, v5, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    invoke-virtual {v5}, Lcom/android/server/am/ProcessList;->getLruProcessesLOSP()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/am/ProcessRecord;

    .line 659
    .local v5, "app":Lcom/android/server/am/ProcessRecord;
    invoke-virtual {v5}, Lcom/android/server/am/ProcessRecord;->isKilledByAm()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v5}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v6

    if-eqz v6, :cond_2

    iget-boolean v6, v5, Lcom/android/server/am/ProcessRecord;->isolated:Z

    if-nez v6, :cond_2

    .line 660
    invoke-virtual {v5}, Lcom/android/server/am/ProcessRecord;->hasActivities()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {p0, v5}, Lcom/android/server/am/PeriodicCleanerService;->isSystemRelated(Lcom/android/server/am/ProcessRecord;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 661
    goto :goto_1

    .line 663
    :cond_0
    iget-object v6, v5, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    .line 664
    .local v6, "state":Lcom/android/server/am/ProcessStateRecord;
    invoke-virtual {v6}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I

    move-result v7

    if-lt v7, p1, :cond_2

    invoke-virtual {v6}, Lcom/android/server/am/ProcessStateRecord;->getCurProcState()I

    move-result v7

    if-lt v7, p2, :cond_2

    .line 665
    const/16 v7, 0xd

    const/4 v8, 0x1

    invoke-virtual {v5, v0, v7, v8}, Lcom/android/server/am/ProcessRecord;->killLocked(Ljava/lang/String;IZ)V

    .line 666
    iget v7, v5, Lcom/android/server/am/ProcessRecord;->userId:I

    invoke-virtual {v1, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/ArrayList;

    .line 667
    .local v7, "userTargets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v7, :cond_1

    .line 668
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    move-object v7, v8

    .line 669
    iget v8, v5, Lcom/android/server/am/ProcessRecord;->userId:I

    invoke-virtual {v1, v8, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 671
    :cond_1
    iget-object v8, v5, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 657
    .end local v5    # "app":Lcom/android/server/am/ProcessRecord;
    .end local v6    # "state":Lcom/android/server/am/ProcessStateRecord;
    .end local v7    # "userTargets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    :goto_1
    add-int/lit8 v4, v4, -0x1

    goto :goto_0

    .line 674
    .end local v3    # "N":I
    .end local v4    # "i":I
    :cond_3
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 675
    new-instance v2, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const/4 v8, -0x1

    const/4 v9, -0x1

    move-object v4, v2

    move-object v5, p0

    move-object v10, p3

    invoke-direct/range {v4 .. v10}, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;-><init>(Lcom/android/server/am/PeriodicCleanerService;JIILjava/lang/String;)V

    .line 676
    .local v2, "cInfo":Lcom/android/server/am/PeriodicCleanerService$CleanInfo;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v4

    if-ge v3, v4, :cond_4

    .line 677
    invoke-virtual {v1, v3}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    invoke-virtual {v1, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    invoke-virtual {v2, v4, v5}, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->addCleanList(ILjava/util/List;)V

    .line 676
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 679
    .end local v3    # "i":I
    :cond_4
    invoke-direct {p0, v2}, Lcom/android/server/am/PeriodicCleanerService;->addCleanHistory(Lcom/android/server/am/PeriodicCleanerService$CleanInfo;)V

    .line 680
    return-void

    .line 674
    .end local v2    # "cInfo":Lcom/android/server/am/PeriodicCleanerService$CleanInfo;
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method

.method private reportEvent(Lcom/android/server/am/PeriodicCleanerService$MyEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/android/server/am/PeriodicCleanerService$MyEvent;

    .line 622
    iget-object v0, p1, Lcom/android/server/am/PeriodicCleanerService$MyEvent;->mPackage:Ljava/lang/String;

    .line 623
    .local v0, "packageName":Ljava/lang/String;
    iget-boolean v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mReady:Z

    if-eqz v1, :cond_6

    if-eqz v0, :cond_6

    iget v1, p1, Lcom/android/server/am/PeriodicCleanerService$MyEvent;->mUid:I

    const/16 v2, 0x3e8

    if-eq v1, v2, :cond_6

    .line 625
    invoke-static {v0}, Lcom/android/server/am/PeriodicCleanerService;->isSystemPackage(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    iget v1, p1, Lcom/android/server/am/PeriodicCleanerService$MyEvent;->mEventType:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    goto :goto_1

    .line 629
    :cond_0
    iget-boolean v1, p1, Lcom/android/server/am/PeriodicCleanerService$MyEvent;->mFullScreen:Z

    if-nez v1, :cond_2

    .line 630
    sget-boolean v1, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 631
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/android/server/am/PeriodicCleanerService$MyEvent;->mClass:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " isn\'t fullscreen, skip."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PeriodicCleaner"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 633
    :cond_1
    return-void

    .line 635
    :cond_2
    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mLastNonSystemFgPkg:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget v1, p1, Lcom/android/server/am/PeriodicCleanerService$MyEvent;->mUid:I

    iget v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mLastNonSystemFgUid:I

    if-eq v1, v2, :cond_5

    .line 636
    :cond_3
    iput-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mLastNonSystemFgPkg:Ljava/lang/String;

    .line 637
    iget v1, p1, Lcom/android/server/am/PeriodicCleanerService$MyEvent;->mUid:I

    iput v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mLastNonSystemFgUid:I

    .line 638
    iget v1, p1, Lcom/android/server/am/PeriodicCleanerService$MyEvent;->mUid:I

    invoke-direct {p0, v1, v0}, Lcom/android/server/am/PeriodicCleanerService;->updateLruPackageLocked(ILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-boolean v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mHighDevice:Z

    if-nez v1, :cond_4

    .line 639
    invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->cleanPackageByPeriodic()V

    goto :goto_0

    .line 642
    :cond_4
    const-string v1, "pressure"

    invoke-direct {p0, v1}, Lcom/android/server/am/PeriodicCleanerService;->checkPressureAndClean(Ljava/lang/String;)V

    .line 649
    :cond_5
    :goto_0
    return-void

    .line 627
    :cond_6
    :goto_1
    return-void
.end method

.method private reportMemPressure(I)V
    .locals 6
    .param p1, "pressureState"    # I

    .line 684
    iget-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mReady:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 685
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 686
    .local v0, "now":J
    iget-wide v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mLastCleanByPressure:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x2710

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 687
    sget-boolean v2, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 688
    const-string v2, "PeriodicCleaner"

    const-string/jumbo v3, "try to clean for MEM_PRESSURE_HIGH."

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 690
    :cond_0
    const-string v2, "pressure"

    invoke-direct {p0, v2}, Lcom/android/server/am/PeriodicCleanerService;->checkPressureAndClean(Ljava/lang/String;)V

    .line 693
    .end local v0    # "now":J
    :cond_1
    return-void
.end method

.method private reportStartProcess(Ljava/lang/String;)V
    .locals 8
    .param p1, "processInfo"    # Ljava/lang/String;

    .line 696
    iget-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mReady:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnableStartProcess:Z

    if-eqz v0, :cond_5

    .line 697
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 698
    .local v0, "now":J
    const-string v2, ":"

    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    .line 699
    .local v2, "lastIndex":I
    add-int/lit8 v3, v2, 0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 700
    .local v3, "hostType":Ljava/lang/String;
    const-string v4, "activity"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 701
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 702
    .local v4, "processName":Ljava/lang/String;
    iget-boolean v5, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnableGameClean:Z

    const-string v6, "PeriodicCleaner"

    if-eqz v5, :cond_1

    sget-object v5, Lcom/android/server/am/PeriodicCleanerService;->sGameApp:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 703
    sget-boolean v5, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z

    if-eqz v5, :cond_0

    .line 704
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "reportStartGame "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 706
    :cond_0
    iget v5, p0, Lcom/android/server/am/PeriodicCleanerService;->mGamePlayAppNum:I

    invoke-direct {p0, v5, v4}, Lcom/android/server/am/PeriodicCleanerService;->cleanPackageByGamePlay(ILjava/lang/String;)V

    goto :goto_0

    .line 707
    :cond_1
    sget-object v5, Lcom/android/server/am/PeriodicCleanerService;->sHighFrequencyApp:Ljava/util/Set;

    invoke-interface {v5, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    sget-object v5, Lcom/android/server/am/PeriodicCleanerService;->sHighMemoryApp:Ljava/util/List;

    .line 708
    invoke-interface {v5, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 709
    :cond_2
    sget-boolean v5, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z

    if-eqz v5, :cond_3

    .line 710
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "reportStartProcess "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 712
    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "pressure:startprocess:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 713
    .local v5, "reason":Ljava/lang/String;
    invoke-direct {p0, v5}, Lcom/android/server/am/PeriodicCleanerService;->checkPressureAndClean(Ljava/lang/String;)V

    .line 716
    .end local v4    # "processName":Ljava/lang/String;
    .end local v5    # "reason":Ljava/lang/String;
    :cond_4
    :goto_0
    iget-wide v4, p0, Lcom/android/server/am/PeriodicCleanerService;->mLastUpdateTime:J

    sub-long v4, v0, v4

    const-wide/32 v6, 0x2932e00

    cmp-long v4, v4, v6

    if-ltz v4, :cond_5

    .line 717
    invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->updateProcStatsList()V

    .line 718
    iput-wide v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mLastUpdateTime:J

    .line 721
    .end local v0    # "now":J
    .end local v2    # "lastIndex":I
    .end local v3    # "hostType":Ljava/lang/String;
    :cond_5
    return-void
.end method

.method private updateHighFrequencyAppList()V
    .locals 7

    .line 852
    invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->getPackageForegroundTime()Ljava/util/HashMap;

    move-result-object v0

    .line 853
    .local v0, "packageForegroundTime":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    new-instance v1, Ljava/util/ArrayList;

    .line 854
    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 856
    .local v1, "packages":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;>;"
    new-instance v2, Lcom/android/server/am/PeriodicCleanerService$5;

    invoke-direct {v2, p0}, Lcom/android/server/am/PeriodicCleanerService$5;-><init>(Lcom/android/server/am/PeriodicCleanerService;)V

    invoke-interface {v1, v2}, Ljava/util/List;->sort(Ljava/util/Comparator;)V

    .line 863
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    .line 865
    .local v2, "maxSize":I
    const/4 v3, 0x5

    if-le v2, v3, :cond_0

    goto :goto_0

    :cond_0
    move v3, v2

    :goto_0
    move v2, v3

    .line 866
    sget-object v3, Lcom/android/server/am/PeriodicCleanerService;->sHighFrequencyApp:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->clear()V

    .line 867
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v2, :cond_2

    .line 868
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 869
    .local v4, "packageName":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
    sget-object v5, Lcom/android/server/am/PeriodicCleanerService;->sHighFrequencyApp:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 870
    sget-boolean v5, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z

    if-eqz v5, :cond_1

    .line 871
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "update highFrequencyApp: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 872
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 871
    const-string v6, "PeriodicCleaner"

    invoke-static {v6, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 867
    .end local v4    # "packageName":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 875
    .end local v3    # "i":I
    :cond_2
    return-void
.end method

.method private updateHighMemoryAppList()V
    .locals 13

    .line 730
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 731
    .local v0, "startTime":J
    invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->getAllPackagePss()Ljava/util/HashMap;

    move-result-object v2

    .line 732
    .local v2, "packagePss":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    const-string v3, "finish get all package pss."

    invoke-static {v0, v1, v3}, Lcom/android/server/am/PeriodicCleanerService;->checkTime(JLjava/lang/String;)V

    .line 733
    if-nez v2, :cond_0

    return-void

    .line 734
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    .line 735
    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 737
    .local v3, "listPkgPss":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;>;"
    new-instance v4, Lcom/android/server/am/PeriodicCleanerService$4;

    invoke-direct {v4, p0}, Lcom/android/server/am/PeriodicCleanerService$4;-><init>(Lcom/android/server/am/PeriodicCleanerService;)V

    invoke-interface {v3, v4}, Ljava/util/List;->sort(Ljava/util/Comparator;)V

    .line 743
    const/4 v4, 0x0

    .line 744
    .local v4, "index":I
    const/4 v5, 0x1

    .line 746
    .local v5, "isNeedClear":Z
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    const-string v8, "PeriodicCleaner"

    if-eqz v7, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map$Entry;

    .line 747
    .local v7, "mapping":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    const-wide/32 v11, 0x64000

    cmp-long v9, v9, v11

    if-ltz v9, :cond_3

    const/4 v9, 0x5

    if-ge v4, v9, :cond_3

    .line 748
    if-eqz v5, :cond_1

    .line 749
    sget-object v9, Lcom/android/server/am/PeriodicCleanerService;->sHighMemoryApp:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->clear()V

    .line 750
    const/4 v5, 0x0

    .line 752
    :cond_1
    sget-boolean v9, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z

    if-eqz v9, :cond_2

    .line 753
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "update highMemoryApp: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ":"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 754
    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 753
    invoke-static {v8, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 756
    :cond_2
    sget-object v8, Lcom/android/server/am/PeriodicCleanerService;->sHighMemoryApp:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 759
    nop

    .end local v7    # "mapping":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
    add-int/lit8 v4, v4, 0x1

    .line 760
    goto :goto_0

    .line 761
    :cond_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "highmemoryapp:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/android/server/am/PeriodicCleanerService;->sHighMemoryApp:Ljava/util/List;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v8, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 762
    return-void
.end method

.method private updateLruPackageLocked(ILjava/lang/String;)Z
    .locals 16
    .param p1, "uid"    # I
    .param p2, "packageName"    # Ljava/lang/String;

    .line 989
    move-object/from16 v1, p0

    move/from16 v2, p1

    move-object/from16 v3, p2

    const/4 v4, -0x1

    .line 990
    .local v4, "newFgIndex":I
    invoke-static/range {p1 .. p1}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v5

    .line 991
    .local v5, "userId":I
    const/4 v6, 0x0

    .line 992
    .local v6, "N":I
    const/4 v7, 0x0

    .line 993
    .local v7, "info":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;
    const/4 v8, 0x0

    .line 994
    .local v8, "newInfo":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;
    const/4 v9, 0x0

    .line 995
    .local v9, "modifyInfo":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 996
    .local v10, "backgroundTime":J
    iget-object v12, v1, Lcom/android/server/am/PeriodicCleanerService;->mLock:Ljava/lang/Object;

    monitor-enter v12

    .line 997
    :try_start_0
    iget-object v0, v1, Lcom/android/server/am/PeriodicCleanerService;->mLruPackages:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v6, v0

    .line 999
    add-int/lit8 v0, v6, -0x2

    .local v0, "i":I
    :goto_0
    const-wide/16 v14, 0x0

    if-ltz v0, :cond_1

    .line 1000
    iget-object v13, v1, Lcom/android/server/am/PeriodicCleanerService;->mLruPackages:Ljava/util/ArrayList;

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;

    move-object v7, v13

    .line 1002
    invoke-static {v7}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->-$$Nest$fgetmUserId(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;)I

    move-result v13

    if-ne v13, v5, :cond_0

    invoke-static {v7}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->-$$Nest$fgetmPackageName(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1003
    iget-object v13, v1, Lcom/android/server/am/PeriodicCleanerService;->mLruPackages:Ljava/util/ArrayList;

    invoke-virtual {v13, v7}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1004
    invoke-static {v7, v14, v15}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->-$$Nest$fputmBackgroundTime(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;J)V

    .line 1005
    const/4 v13, 0x0

    invoke-static {v7, v13}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->-$$Nest$fputmFgTrimDone(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;Z)V

    .line 1006
    iget-object v13, v1, Lcom/android/server/am/PeriodicCleanerService;->mLruPackages:Ljava/util/ArrayList;

    invoke-virtual {v13, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1007
    move v4, v0

    .line 1010
    iget-object v13, v1, Lcom/android/server/am/PeriodicCleanerService;->mLruPackages:Ljava/util/ArrayList;

    add-int/lit8 v14, v6, -0x2

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;

    move-object v9, v13

    .line 1011
    invoke-static {v9, v10, v11}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->-$$Nest$fputmBackgroundTime(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;J)V

    .line 1012
    goto :goto_1

    .line 999
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1015
    .end local v0    # "i":I
    :cond_1
    :goto_1
    const/4 v0, 0x1

    const/4 v13, -0x1

    if-ne v4, v13, :cond_3

    .line 1016
    const-wide/16 v13, 0x0

    invoke-direct {v1, v2, v3, v13, v14}, Lcom/android/server/am/PeriodicCleanerService;->obtainPackageUseInfo(ILjava/lang/String;J)Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;

    move-result-object v13

    move-object v8, v13

    .line 1017
    iget-object v13, v1, Lcom/android/server/am/PeriodicCleanerService;->mLruPackages:Ljava/util/ArrayList;

    invoke-virtual {v13, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1018
    iget-object v13, v1, Lcom/android/server/am/PeriodicCleanerService;->mLruPackages:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    move v6, v13

    .line 1019
    const/4 v13, 0x2

    if-lt v6, v13, :cond_2

    .line 1020
    iget-object v13, v1, Lcom/android/server/am/PeriodicCleanerService;->mLruPackages:Ljava/util/ArrayList;

    add-int/lit8 v14, v6, -0x2

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;

    move-object v9, v13

    .line 1021
    invoke-static {v9, v10, v11}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->-$$Nest$fputmBackgroundTime(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;J)V

    .line 1023
    :cond_2
    monitor-exit v12

    return v0

    .line 1025
    :cond_3
    monitor-exit v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1026
    if-eqz v7, :cond_4

    invoke-static {v7}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->-$$Nest$fgetmUid(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;)I

    move-result v12

    if-eq v12, v2, :cond_4

    .line 1027
    const-string v12, "PeriodicCleaner"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " re-installed, NewAppId="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 1028
    invoke-static/range {p1 .. p1}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", OldAppId="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {v7}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->-$$Nest$fgetmUid(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;)I

    move-result v14

    .line 1029
    invoke-static {v14}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 1027
    invoke-static {v12, v13}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1030
    invoke-virtual {v7, v2}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->updateUid(I)V

    .line 1032
    :cond_4
    sub-int v12, v6, v4

    iget v13, v1, Lcom/android/server/am/PeriodicCleanerService;->mLruActiveLength:I

    if-le v12, v13, :cond_5

    move v13, v0

    goto :goto_2

    :cond_5
    const/4 v13, 0x0

    :goto_2
    return v13

    .line 1025
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private updateProcStatsList()V
    .locals 2

    .line 724
    invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->updateHighMemoryAppList()V

    .line 725
    invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->updateHighFrequencyAppList()V

    .line 726
    const-string v0, "PeriodicCleaner"

    const-string/jumbo v1, "update high memory and frequency app list success"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 727
    return-void
.end method

.method private updateTimeLevel()V
    .locals 11

    .line 587
    const-string v0, "persist.sys.periodic.time_threshold"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 588
    .local v0, "prop":Ljava/lang/String;
    if-nez v0, :cond_0

    return-void

    .line 589
    :cond_0
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 590
    .local v1, "timeLevelString":[Ljava/lang/String;
    array-length v2, v1

    const/4 v3, 0x3

    if-eq v2, v3, :cond_1

    return-void

    .line 592
    :cond_1
    array-length v2, v1

    new-array v2, v2, [I

    .line 593
    .local v2, "timeLevelInt":[I
    const/4 v4, 0x0

    .line 594
    .local v4, "time_threshold":I
    const/4 v5, 0x0

    .line 595
    .local v5, "index":I
    array-length v6, v1

    const/4 v7, 0x0

    move v8, v7

    :goto_0
    if-ge v8, v6, :cond_3

    aget-object v9, v1, v8

    .line 596
    .local v9, "str":Ljava/lang/String;
    const/4 v4, 0x0

    .line 598
    :try_start_0
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v4, v10

    .line 602
    nop

    .line 603
    if-gtz v4, :cond_2

    return-void

    .line 604
    :cond_2
    aput v4, v2, v5

    .line 605
    nop

    .end local v9    # "str":Ljava/lang/String;
    add-int/lit8 v5, v5, 0x1

    .line 595
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 599
    .restart local v9    # "str":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 601
    .local v3, "e":Ljava/lang/Exception;
    return-void

    .line 607
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v9    # "str":Ljava/lang/String;
    :cond_3
    sget-object v6, Lcom/android/server/am/PeriodicCleanerService;->sDefaultTimeLevel:[I

    invoke-static {v2, v7, v6, v7, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 608
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Override mPressureTimeThreshold with: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v6, "PeriodicCleaner"

    invoke-static {v6, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 609
    return-void
.end method


# virtual methods
.method public isPreviousApp(Ljava/lang/String;)Z
    .locals 9
    .param p1, "packageName"    # Ljava/lang/String;

    .line 970
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 971
    .local v0, "curTime":J
    iget-object v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 972
    :try_start_0
    iget-object v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruPackages:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 973
    .local v3, "N":I
    const/4 v4, 0x2

    if-lt v3, v4, :cond_0

    .line 974
    iget-object v4, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruPackages:Ljava/util/ArrayList;

    add-int/lit8 v5, v3, -0x2

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;

    .line 975
    .local v4, "previousApp":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;
    invoke-static {v4}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->-$$Nest$fgetmPackageName(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 976
    invoke-static {v4}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->-$$Nest$fgetmBackgroundTime(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;)J

    move-result-wide v5

    sub-long v5, v0, v5

    const-wide/32 v7, 0x493e0

    cmp-long v5, v5, v7

    if-gtz v5, :cond_0

    .line 977
    monitor-exit v2

    const/4 v2, 0x1

    return v2

    .line 980
    .end local v3    # "N":I
    .end local v4    # "previousApp":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;
    :cond_0
    monitor-exit v2

    .line 981
    const/4 v2, 0x0

    return v2

    .line 980
    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public isSystemRelated(Lcom/android/server/am/ProcessRecord;)Z
    .locals 5
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 1482
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 1483
    .local v0, "packageName":Ljava/lang/String;
    invoke-static {v0}, Lcom/android/server/am/PeriodicCleanerService;->isSystemPackage(Ljava/lang/String;)Z

    move-result v1

    const-string v2, "PeriodicCleaner"

    const/4 v3, 0x1

    if-nez v1, :cond_3

    invoke-static {v0}, Lcom/android/server/am/PeriodicCleanerService;->isWhiteListPackage(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 1484
    invoke-static {v1}, Landroid/os/Process;->isApplicationUid(I)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 1490
    :cond_0
    if-eqz v0, :cond_2

    const-string v1, "com.google.android"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1491
    sget-boolean v1, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 1492
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", skip for google."

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1494
    :cond_1
    return v3

    .line 1496
    :cond_2
    const/4 v1, 0x0

    return v1

    .line 1485
    :cond_3
    :goto_0
    sget-boolean v1, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z

    if-eqz v1, :cond_4

    .line 1486
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " is in exclude list."

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1488
    :cond_4
    return v3
.end method

.method public onBootPhase(I)V
    .locals 1
    .param p1, "phase"    # I

    .line 243
    const/16 v0, 0x226

    if-ne p1, v0, :cond_0

    .line 244
    invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->onActivityManagerReady()V

    goto :goto_0

    .line 245
    :cond_0
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_1

    .line 246
    invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->onBootComplete()V

    .line 248
    :cond_1
    :goto_0
    return-void
.end method

.method public onStart()V
    .locals 2

    .line 232
    sget-boolean v0, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 233
    const-string v0, "PeriodicCleaner"

    const-string v1, "Starting PeriodicCleaner"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    :cond_0
    new-instance v0, Lcom/android/server/am/PeriodicCleanerService$BinderService;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/am/PeriodicCleanerService$BinderService;-><init>(Lcom/android/server/am/PeriodicCleanerService;Lcom/android/server/am/PeriodicCleanerService$BinderService-IA;)V

    iput-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mBinderService:Lcom/android/server/am/PeriodicCleanerService$BinderService;

    .line 236
    new-instance v0, Lcom/android/server/am/PeriodicCleanerService$LocalService;

    invoke-direct {v0, p0, v1}, Lcom/android/server/am/PeriodicCleanerService$LocalService;-><init>(Lcom/android/server/am/PeriodicCleanerService;Lcom/android/server/am/PeriodicCleanerService$LocalService-IA;)V

    iput-object v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mLocalService:Lcom/android/server/am/PeriodicCleanerInternalStub;

    .line 237
    const-class v1, Lcom/android/server/am/PeriodicCleanerInternalStub;

    invoke-virtual {p0, v1, v0}, Lcom/android/server/am/PeriodicCleanerService;->publishLocalService(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 238
    const-string v0, "periodic"

    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mBinderService:Lcom/android/server/am/PeriodicCleanerService$BinderService;

    invoke-virtual {p0, v0, v1}, Lcom/android/server/am/PeriodicCleanerService;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 239
    return-void
.end method
