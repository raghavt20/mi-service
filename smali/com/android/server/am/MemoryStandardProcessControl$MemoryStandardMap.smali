.class public Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;
.super Ljava/lang/Object;
.source "MemoryStandardProcessControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/MemoryStandardProcessControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MemoryStandardMap"
.end annotation


# instance fields
.field private mNativeProcessCmds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mStandardMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/server/am/MemoryStandardProcessControl;


# direct methods
.method static bridge synthetic -$$Nest$fgetmNativeProcessCmds(Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->mNativeProcessCmds:Ljava/util/Set;

    return-object p0
.end method

.method public constructor <init>(Lcom/android/server/am/MemoryStandardProcessControl;)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/server/am/MemoryStandardProcessControl;

    .line 1299
    iput-object p1, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1301
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->mStandardMap:Ljava/util/Map;

    .line 1303
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->mNativeProcessCmds:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .line 1358
    iget-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->mStandardMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1359
    iget-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->mNativeProcessCmds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1360
    return-void
.end method

.method public containsPackage(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1354
    iget-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->mStandardMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getNativeProcessCmds()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1334
    iget-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->mNativeProcessCmds:Ljava/util/Set;

    return-object v0
.end method

.method public getPss(Ljava/lang/String;)J
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1342
    iget-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->mStandardMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;

    .line 1343
    .local v0, "memoryStandard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
    if-eqz v0, :cond_0

    .line 1344
    iget-wide v1, v0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;->mPss:J

    return-wide v1

    .line 1346
    :cond_0
    const-wide/16 v1, 0x0

    return-wide v1
.end method

.method public getStandard(Ljava/lang/String;)Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1338
    iget-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->mStandardMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .line 1350
    iget-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->mStandardMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public removeMemoryStandard(Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1321
    iget-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->mStandardMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;

    .line 1322
    .local v0, "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
    if-nez v0, :cond_0

    .line 1323
    return-void

    .line 1326
    :cond_0
    iget-object v1, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->mNativeProcessCmds:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1327
    iget-object v1, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->mNativeProcessCmds:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1330
    :cond_1
    iget-object v1, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->mStandardMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1331
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .line 1364
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1365
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "Memory Standard:\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1366
    iget-object v1, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->mStandardMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const-string v3, "\n"

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1367
    .local v2, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;>;"
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;

    invoke-virtual {v5}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1368
    .end local v2    # "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;>;"
    goto :goto_0

    .line 1369
    :cond_0
    const-string v1, "Native Procs:\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1370
    iget-object v1, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->mNativeProcessCmds:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1371
    .local v1, "nativeProcIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1372
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1374
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public updateMemoryStandard(JLjava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .locals 7
    .param p1, "pss"    # J
    .param p3, "packageName"    # Ljava/lang/String;
    .param p5, "parent"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1307
    .local p4, "processes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v6, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;

    iget-object v1, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    move-object v0, v6

    move-wide v2, p1

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;-><init>(Lcom/android/server/am/MemoryStandardProcessControl;JLjava/util/List;Ljava/lang/String;)V

    .line 1308
    .local v0, "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
    iget-object v1, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->mStandardMap:Ljava/util/Map;

    invoke-interface {v1, p3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1309
    return-void
.end method

.method public updateNativeMemoryStandard(JLjava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .param p1, "pss"    # J
    .param p3, "packageName"    # Ljava/lang/String;
    .param p5, "parent"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1313
    .local p4, "processes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p0 .. p5}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->updateMemoryStandard(JLjava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    .line 1315
    iget-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->mNativeProcessCmds:Ljava/util/Set;

    invoke-interface {v0, p3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1316
    iget-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->mNativeProcessCmds:Ljava/util/Set;

    invoke-interface {v0, p3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1318
    :cond_0
    return-void
.end method
