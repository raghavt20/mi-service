class com.android.server.am.AppProfilerImpl$1 implements java.util.concurrent.Callable {
	 /* .source "AppProfilerImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/am/AppProfilerImpl;->dumpCpuInfo(Lcom/android/server/utils/PriorityDump$PriorityDumper;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)Z */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Object;", */
/* "Ljava/util/concurrent/Callable<", */
/* "Ljava/lang/Void;", */
/* ">;" */
/* } */
} // .end annotation
/* # instance fields */
final com.android.server.am.AppProfilerImpl this$0; //synthetic
final java.lang.String val$args; //synthetic
final com.android.server.utils.PriorityDump$PriorityDumper val$dumper; //synthetic
final java.io.FileDescriptor val$fd; //synthetic
final java.io.PrintWriter val$pw; //synthetic
/* # direct methods */
 com.android.server.am.AppProfilerImpl$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/AppProfilerImpl; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 445 */
this.this$0 = p1;
this.val$dumper = p2;
this.val$fd = p3;
this.val$pw = p4;
this.val$args = p5;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public java.lang.Object call ( ) { //bridge//synthethic
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 445 */
(( com.android.server.am.AppProfilerImpl$1 ) p0 ).call ( ); // invoke-virtual {p0}, Lcom/android/server/am/AppProfilerImpl$1;->call()Ljava/lang/Void;
} // .end method
public java.lang.Void call ( ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 448 */
v0 = this.val$dumper;
v1 = this.val$fd;
v2 = this.val$pw;
v3 = this.val$args;
com.android.server.utils.PriorityDump .dump ( v0,v1,v2,v3 );
/* .line 449 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
