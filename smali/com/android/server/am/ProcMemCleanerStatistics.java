public class com.android.server.am.ProcMemCleanerStatistics {
	 /* .source "ProcMemCleanerStatistics.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Boolean DEBUG;
public static final Integer EVENT_TAGS;
private static final Integer MAX_KILL_REPORT_COUNTS;
public static final Long MAX_RECENTLY_KILL_REPORT_TIME;
public static final java.lang.String REASON_CLEAN_UP_MEM;
public static final java.lang.String REASON_KILL_BG_PROC;
private static final java.lang.String TAG;
public static final Integer TYPE_FORCE_STOP;
public static final Integer TYPE_KILL_NONE;
public static final Integer TYPE_KILL_PROC;
private static java.util.List sCommonUsedPackageList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static com.android.server.am.ProcMemCleanerStatistics sInstance;
/* # instance fields */
private java.text.SimpleDateFormat mDateformat;
private final java.util.List mKillInfos;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.util.ArrayMap mLastKillProc;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Long mPreCleanUpTime;
private Integer mTotalKillsProc;
/* # direct methods */
static com.android.server.am.ProcMemCleanerStatistics ( ) {
/* .locals 2 */
/* .line 21 */
/* sget-boolean v0, Landroid/os/spc/PressureStateSettings;->DEBUG_ALL:Z */
com.android.server.am.ProcMemCleanerStatistics.DEBUG = (v0!= 0);
/* .line 24 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x3e8 */
} // :cond_0
/* const/16 v0, 0x12c */
} // :goto_0
/* .line 43 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 45 */
final String v1 = "com.tencent.mm"; // const-string v1, "com.tencent.mm"
/* .line 46 */
return;
} // .end method
private com.android.server.am.ProcMemCleanerStatistics ( ) {
/* .locals 2 */
/* .line 48 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 35 */
/* new-instance v0, Ljava/util/LinkedList; */
/* invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V */
this.mKillInfos = v0;
/* .line 37 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/am/ProcMemCleanerStatistics;->mPreCleanUpTime:J */
/* .line 38 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mLastKillProc = v0;
/* .line 40 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/am/ProcMemCleanerStatistics;->mTotalKillsProc:I */
/* .line 49 */
/* new-instance v0, Ljava/text/SimpleDateFormat; */
final String v1 = "HH:mm:ss"; // const-string v1, "HH:mm:ss"
/* invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V */
this.mDateformat = v0;
/* .line 50 */
return;
} // .end method
private void debugAppGroupToString ( ) {
/* .locals 5 */
/* .line 128 */
v0 = this.mLastKillProc;
(( android.util.ArrayMap ) v0 ).entrySet ( ); // invoke-virtual {v0}, Landroid/util/ArrayMap;->entrySet()Ljava/util/Set;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Ljava/util/Map$Entry; */
/* .line 129 */
/* .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;" */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "key:"; // const-string v3, "key:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v3, Ljava/lang/String; */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " value:"; // const-string v3, " value:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mDateformat;
(( java.text.SimpleDateFormat ) v3 ).format ( v4 ); // invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "ProcessMemCleanerStatistics"; // const-string v3, "ProcessMemCleanerStatistics"
android.util.Slog .d ( v3,v2 );
/* .line 130 */
} // .end local v1 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
/* .line 131 */
} // :cond_0
return;
} // .end method
private java.lang.String genLastKillKey ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "procName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 64 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public static com.android.server.am.ProcMemCleanerStatistics getInstance ( ) {
/* .locals 1 */
/* .line 53 */
v0 = com.android.server.am.ProcMemCleanerStatistics.sInstance;
/* if-nez v0, :cond_0 */
/* .line 54 */
/* new-instance v0, Lcom/android/server/am/ProcMemCleanerStatistics; */
/* invoke-direct {v0}, Lcom/android/server/am/ProcMemCleanerStatistics;-><init>()V */
/* .line 56 */
} // :cond_0
v0 = com.android.server.am.ProcMemCleanerStatistics.sInstance;
} // .end method
public static Boolean isCommonUsedApp ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "pckName" # Ljava/lang/String; */
/* .line 60 */
v0 = v0 = com.android.server.am.ProcMemCleanerStatistics.sCommonUsedPackageList;
} // .end method
/* # virtual methods */
public void checkLastKillTime ( Long p0 ) {
/* .locals 4 */
/* .param p1, "nowTime" # J */
/* .line 80 */
/* sget-boolean v0, Lcom/android/server/am/ProcMemCleanerStatistics;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* invoke-direct {p0}, Lcom/android/server/am/ProcMemCleanerStatistics;->debugAppGroupToString()V */
/* .line 82 */
} // :cond_0
/* iget-wide v0, p0, Lcom/android/server/am/ProcMemCleanerStatistics;->mPreCleanUpTime:J */
/* sub-long v0, p1, v0 */
/* const-wide/16 v2, 0x2710 */
/* cmp-long v0, v0, v2 */
/* if-lez v0, :cond_1 */
/* .line 83 */
v0 = this.mLastKillProc;
(( android.util.ArrayMap ) v0 ).clear ( ); // invoke-virtual {v0}, Landroid/util/ArrayMap;->clear()V
/* .line 85 */
} // :cond_1
/* iput-wide p1, p0, Lcom/android/server/am/ProcMemCleanerStatistics;->mPreCleanUpTime:J */
/* .line 86 */
return;
} // .end method
public void dumpKillInfo ( java.io.PrintWriter p0 ) {
/* .locals 5 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 118 */
final String v0 = "ProcessMemCleanerStatistics KillInfo"; // const-string v0, "ProcessMemCleanerStatistics KillInfo"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 119 */
v0 = this.mKillInfos;
/* monitor-enter v0 */
/* .line 120 */
try { // :try_start_0
/* const-string/jumbo v1, "total=%s last=%s" */
int v2 = 2; // const/4 v2, 0x2
/* new-array v2, v2, [Ljava/lang/Object; */
/* iget v3, p0, Lcom/android/server/am/ProcMemCleanerStatistics;->mTotalKillsProc:I */
java.lang.Integer .valueOf ( v3 );
int v4 = 0; // const/4 v4, 0x0
/* aput-object v3, v2, v4 */
v3 = v3 = this.mKillInfos;
java.lang.Integer .valueOf ( v3 );
int v4 = 1; // const/4 v4, 0x1
/* aput-object v3, v2, v4 */
java.lang.String .format ( v1,v2 );
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 121 */
v1 = this.mKillInfos;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo; */
/* .line 122 */
/* .local v2, "i":Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo; */
(( com.android.server.am.ProcMemCleanerStatistics$KillInfo ) v2 ).toString ( ); // invoke-virtual {v2}, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 123 */
} // .end local v2 # "i":Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;
/* .line 124 */
} // :cond_0
/* monitor-exit v0 */
/* .line 125 */
return;
/* .line 124 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isLastKillProcess ( java.lang.String p0, Integer p1, Long p2 ) {
/* .locals 7 */
/* .param p1, "procName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .param p3, "nowTime" # J */
/* .line 68 */
v0 = this.mLastKillProc;
/* invoke-direct {p0, p1, p2}, Lcom/android/server/am/ProcMemCleanerStatistics;->genLastKillKey(Ljava/lang/String;I)Ljava/lang/String; */
v0 = (( android.util.ArrayMap ) v0 ).indexOfKey ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/ArrayMap;->indexOfKey(Ljava/lang/Object;)I
/* .line 69 */
/* .local v0, "index":I */
/* if-ltz v0, :cond_1 */
/* .line 70 */
v1 = this.mLastKillProc;
(( android.util.ArrayMap ) v1 ).valueAt ( v0 ); // invoke-virtual {v1, v0}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Long; */
(( java.lang.Long ) v1 ).longValue ( ); // invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
/* move-result-wide v1 */
/* .line 71 */
/* .local v1, "value":J */
/* sub-long v3, v1, p3 */
/* const-wide/16 v5, 0x2710 */
/* cmp-long v3, v3, v5 */
/* if-gtz v3, :cond_0 */
/* .line 72 */
int v3 = 1; // const/4 v3, 0x1
/* .line 74 */
} // :cond_0
v3 = this.mLastKillProc;
/* invoke-direct {p0, p1, p2}, Lcom/android/server/am/ProcMemCleanerStatistics;->genLastKillKey(Ljava/lang/String;I)Ljava/lang/String; */
(( android.util.ArrayMap ) v3 ).remove ( v4 ); // invoke-virtual {v3, v4}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 76 */
} // .end local v1 # "value":J
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // .end method
public void reportEvent ( Integer p0, com.miui.server.smartpower.IAppState$IRunningProcess p1, Long p2, java.lang.String p3 ) {
/* .locals 6 */
/* .param p1, "eventType" # I */
/* .param p2, "app" # Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .param p3, "pss" # J */
/* .param p5, "killReason" # Ljava/lang/String; */
/* .line 90 */
/* new-instance v0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;-><init>(Lcom/android/server/am/ProcMemCleanerStatistics;Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo-IA;)V */
/* .line 91 */
/* .local v0, "info":Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo; */
v1 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) p2 ).getUid ( ); // invoke-virtual {p2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUid()I
com.android.server.am.ProcMemCleanerStatistics$KillInfo .-$$Nest$fputuid ( v0,v1 );
/* .line 92 */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) p2 ).getProcessName ( ); // invoke-virtual {p2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;
com.android.server.am.ProcMemCleanerStatistics$KillInfo .-$$Nest$fputname ( v0,v1 );
/* .line 93 */
v1 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) p2 ).getPid ( ); // invoke-virtual {p2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPid()I
com.android.server.am.ProcMemCleanerStatistics$KillInfo .-$$Nest$fputpid ( v0,v1 );
/* .line 94 */
com.android.server.am.ProcMemCleanerStatistics$KillInfo .-$$Nest$fputkillType ( v0,p1 );
/* .line 95 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v1 */
com.android.server.am.ProcMemCleanerStatistics$KillInfo .-$$Nest$fputtime ( v0,v1,v2 );
/* .line 96 */
v1 = com.android.server.am.ProcessMemoryCleaner .getProcPriority ( p2 );
com.android.server.am.ProcMemCleanerStatistics$KillInfo .-$$Nest$fputpriority ( v0,v1 );
/* .line 97 */
com.android.server.am.ProcMemCleanerStatistics$KillInfo .-$$Nest$fputpss ( v0,p3,p4 );
/* .line 98 */
com.android.server.am.ProcMemCleanerStatistics$KillInfo .-$$Nest$fputkillReason ( v0,p5 );
/* .line 100 */
int v1 = 1; // const/4 v1, 0x1
/* if-ne p1, v1, :cond_0 */
/* .line 101 */
v2 = this.mLastKillProc;
(( com.miui.server.smartpower.IAppState$IRunningProcess ) p2 ).getProcessName ( ); // invoke-virtual {p2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;
v4 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) p2 ).getUid ( ); // invoke-virtual {p2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUid()I
/* invoke-direct {p0, v3, v4}, Lcom/android/server/am/ProcMemCleanerStatistics;->genLastKillKey(Ljava/lang/String;I)Ljava/lang/String; */
com.android.server.am.ProcMemCleanerStatistics$KillInfo .-$$Nest$fgettime ( v0 );
/* move-result-wide v4 */
java.lang.Long .valueOf ( v4,v5 );
(( android.util.ArrayMap ) v2 ).put ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 103 */
} // :cond_0
v2 = this.mKillInfos;
/* monitor-enter v2 */
/* .line 104 */
try { // :try_start_0
v3 = this.mKillInfos;
/* .line 105 */
v3 = v3 = this.mKillInfos;
/* if-lt v3, v4, :cond_1 */
/* .line 106 */
v3 = this.mKillInfos;
int v4 = 0; // const/4 v4, 0x0
/* .line 108 */
} // :cond_1
/* iget v3, p0, Lcom/android/server/am/ProcMemCleanerStatistics;->mTotalKillsProc:I */
/* add-int/2addr v3, v1 */
/* iput v3, p0, Lcom/android/server/am/ProcMemCleanerStatistics;->mTotalKillsProc:I */
/* .line 109 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 111 */
/* sget-boolean v1, Lcom/android/server/am/ProcessMemoryCleaner;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 112 */
final String v1 = "ProcessMemoryCleaner"; // const-string v1, "ProcessMemoryCleaner"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "spckill:" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.am.ProcMemCleanerStatistics$KillInfo ) v0 ).toString ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 114 */
} // :cond_2
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "spckill:" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.am.ProcMemCleanerStatistics$KillInfo ) v0 ).toString ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const v2, 0x13ba0 */
android.util.EventLog .writeEvent ( v2,v1 );
/* .line 115 */
return;
/* .line 109 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
