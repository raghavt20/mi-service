.class Lcom/android/server/am/MiuiMemReclaimer$3;
.super Ljava/lang/Object;
.source "MiuiMemReclaimer.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/MiuiMemReclaimer;->sortProcsByRss(Ljava/util/List;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/MiuiMemReclaimer;

.field final synthetic val$action:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/server/am/MiuiMemReclaimer;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/MiuiMemReclaimer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 453
    iput-object p1, p0, Lcom/android/server/am/MiuiMemReclaimer$3;->this$0:Lcom/android/server/am/MiuiMemReclaimer;

    iput-object p2, p0, Lcom/android/server/am/MiuiMemReclaimer$3;->val$action:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)I
    .locals 5
    .param p1, "t1"    # Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;
    .param p2, "t2"    # Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;

    .line 456
    iget-object v0, p0, Lcom/android/server/am/MiuiMemReclaimer$3;->val$action:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    sparse-switch v1, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string v1, "file"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v3

    goto :goto_1

    :sswitch_1
    const-string v1, "anon"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    goto :goto_1

    :sswitch_2
    const-string v1, "all"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v4

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 464
    iget-object v0, p2, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rss:[J

    aget-wide v0, v0, v4

    iget-object v2, p1, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rss:[J

    aget-wide v2, v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Long;->compare(JJ)I

    move-result v0

    return v0

    .line 462
    :pswitch_0
    iget-object v0, p2, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rss:[J

    aget-wide v0, v0, v2

    iget-object v3, p1, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rss:[J

    aget-wide v2, v3, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Long;->compare(JJ)I

    move-result v0

    return v0

    .line 460
    :pswitch_1
    iget-object v0, p2, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rss:[J

    aget-wide v0, v0, v3

    iget-object v2, p1, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rss:[J

    aget-wide v2, v2, v3

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Long;->compare(JJ)I

    move-result v0

    return v0

    .line 458
    :pswitch_2
    iget-object v0, p2, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rss:[J

    aget-wide v0, v0, v4

    iget-object v2, p1, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rss:[J

    aget-wide v2, v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Long;->compare(JJ)I

    move-result v0

    return v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x179a1 -> :sswitch_2
        0x2dc2cc -> :sswitch_1
        0x2ff57c -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 453
    check-cast p1, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;

    check-cast p2, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;

    invoke-virtual {p0, p1, p2}, Lcom/android/server/am/MiuiMemReclaimer$3;->compare(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)I

    move-result p1

    return p1
.end method
