class com.android.server.am.ProcessRecordImpl implements com.android.server.am.ProcessRecordStub {
	 /* .source "ProcessRecordImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/am/ProcessRecordImpl$AppPss; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Long MEM_THRESHOLD_IN_WHITE_LIST;
private static final java.lang.String TAG;
private static final android.util.SparseArray sAppPssUserMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/am/ProcessRecordImpl$AppPss;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.Object sLock;
private static volatile com.miui.server.process.ProcessManagerInternal sProcessManagerInternal;
/* # instance fields */
private final java.lang.String APP_LOG_DIR;
private final java.lang.String LOG_DIR;
private final java.lang.String SYSTEM_LOG_DIR;
/* # direct methods */
static com.android.server.am.ProcessRecordImpl ( ) {
/* .locals 1 */
/* .line 68 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
/* .line 70 */
int v0 = 0; // const/4 v0, 0x0
/* .line 75 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
return;
} // .end method
 com.android.server.am.ProcessRecordImpl ( ) {
/* .locals 1 */
/* .line 63 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 77 */
final String v0 = "/data/miuilog/stability/scout"; // const-string v0, "/data/miuilog/stability/scout"
this.LOG_DIR = v0;
/* .line 78 */
final String v0 = "/data/miuilog/stability/scout/app"; // const-string v0, "/data/miuilog/stability/scout/app"
this.APP_LOG_DIR = v0;
/* .line 79 */
final String v0 = "/data/miuilog/stability/scout/sys"; // const-string v0, "/data/miuilog/stability/scout/sys"
this.SYSTEM_LOG_DIR = v0;
return;
} // .end method
public static void addAppPssIfNeeded ( com.miui.server.process.ProcessManagerInternal p0, com.android.server.am.ProcessRecord p1 ) {
/* .locals 13 */
/* .param p0, "pms" # Lcom/miui/server/process/ProcessManagerInternal; */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 100 */
v0 = this.info;
v0 = this.packageName;
/* .line 101 */
/* .local v0, "pkn":Ljava/lang/String; */
final String v1 = "android"; // const-string v1, "android"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 102 */
return;
/* .line 104 */
} // :cond_0
/* iget v1, p1, Lcom/android/server/am/ProcessRecord;->userId:I */
com.android.server.am.ProcessUtils .getPackageLastPss ( v0,v1 );
/* move-result-wide v7 */
/* .line 105 */
/* .local v7, "pss":J */
v9 = com.android.server.am.ProcessRecordImpl.sLock;
/* monitor-enter v9 */
/* .line 106 */
try { // :try_start_0
v1 = com.android.server.am.ProcessRecordImpl.sAppPssUserMap;
/* iget v2, p1, Lcom/android/server/am/ProcessRecord;->userId:I */
(( android.util.SparseArray ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Ljava/util/Map; */
/* move-object v10, v1 */
/* .line 107 */
/* .local v10, "appPssMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/am/ProcessRecordImpl$AppPss;>;" */
/* const-wide/32 v1, 0x11800 */
/* cmp-long v1, v7, v1 */
/* if-ltz v1, :cond_4 */
if ( v10 != null) { // if-eqz v10, :cond_1
	 /* .line 108 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* if-nez v1, :cond_4 */
	 /* .line 109 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
/* .line 111 */
/* .local v1, "pi":Landroid/content/pm/PackageInfo; */
try { // :try_start_1
	 android.app.AppGlobals .getPackageManager ( );
	 /* iget v3, p1, Lcom/android/server/am/ProcessRecord;->userId:I */
	 /* const-wide/16 v4, 0x0 */
	 /* :try_end_1 */
	 /* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
	 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
	 /* move-object v1, v2 */
	 /* .line 114 */
	 /* move-object v11, v1 */
	 /* .line 112 */
	 /* :catch_0 */
	 /* move-exception v2 */
	 /* .line 113 */
	 /* .local v2, "e":Landroid/os/RemoteException; */
	 try { // :try_start_2
		 (( android.os.RemoteException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V
		 /* move-object v11, v1 */
		 /* .line 115 */
	 } // .end local v1 # "pi":Landroid/content/pm/PackageInfo;
} // .end local v2 # "e":Landroid/os/RemoteException;
/* .local v11, "pi":Landroid/content/pm/PackageInfo; */
} // :goto_0
if ( v11 != null) { // if-eqz v11, :cond_2
v1 = this.versionName;
if ( v1 != null) { // if-eqz v1, :cond_2
	 v1 = this.versionName;
} // :cond_2
/* const-string/jumbo v1, "unknown" */
} // :goto_1
/* move-object v5, v1 */
/* .line 116 */
/* .local v5, "version":Ljava/lang/String; */
/* new-instance v12, Lcom/android/server/am/ProcessRecordImpl$AppPss; */
/* iget v6, p1, Lcom/android/server/am/ProcessRecord;->userId:I */
/* move-object v1, v12 */
/* move-object v2, v0 */
/* move-wide v3, v7 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/am/ProcessRecordImpl$AppPss;-><init>(Ljava/lang/String;JLjava/lang/String;I)V */
/* move-object v1, v12 */
/* .line 117 */
/* .local v1, "appPss":Lcom/android/server/am/ProcessRecordImpl$AppPss; */
/* if-nez v10, :cond_3 */
/* .line 118 */
/* new-instance v2, Ljava/util/HashMap; */
/* invoke-direct {v2}, Ljava/util/HashMap;-><init>()V */
/* move-object v10, v2 */
/* .line 119 */
v2 = com.android.server.am.ProcessRecordImpl.sAppPssUserMap;
/* iget v3, p1, Lcom/android/server/am/ProcessRecord;->userId:I */
(( android.util.SparseArray ) v2 ).put ( v3, v10 ); // invoke-virtual {v2, v3, v10}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 121 */
} // :cond_3
/* .line 123 */
} // .end local v1 # "appPss":Lcom/android/server/am/ProcessRecordImpl$AppPss;
} // .end local v5 # "version":Ljava/lang/String;
} // .end local v10 # "appPssMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/am/ProcessRecordImpl$AppPss;>;"
} // .end local v11 # "pi":Landroid/content/pm/PackageInfo;
} // :cond_4
/* monitor-exit v9 */
/* .line 124 */
return;
/* .line 123 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v9 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
private java.io.File createLastAnrFile ( miui.mqsas.sdk.event.AnrEvent p0, java.io.File p1 ) {
/* .locals 5 */
/* .param p1, "event" # Lmiui/mqsas/sdk/event/AnrEvent; */
/* .param p2, "logFile" # Ljava/io/File; */
/* .line 253 */
final String v0 = "-"; // const-string v0, "-"
/* if-nez p2, :cond_0 */
/* .line 254 */
int v0 = 0; // const/4 v0, 0x0
/* .line 256 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 258 */
/* .local v1, "lastAnrStateFile":Ljava/io/File; */
try { // :try_start_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.io.File ) p2 ).getName ( ); // invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( miui.mqsas.sdk.event.AnrEvent ) p1 ).getProcessName ( ); // invoke-virtual {p1}, Lmiui/mqsas/sdk/event/AnrEvent;->getProcessName()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 259 */
v2 = (( miui.mqsas.sdk.event.AnrEvent ) p1 ).getPid ( ); // invoke-virtual {p1}, Lmiui/mqsas/sdk/event/AnrEvent;->getPid()I
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = "-lastAnrState.txt"; // const-string v2, "-lastAnrState.txt"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 260 */
/* .local v0, "lastAnrFileName":Ljava/lang/String; */
/* const-string/jumbo v2, "system_server" */
(( miui.mqsas.sdk.event.AnrEvent ) p1 ).getProcessName ( ); // invoke-virtual {p1}, Lmiui/mqsas/sdk/event/AnrEvent;->getProcessName()Ljava/lang/String;
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 261 */
/* new-instance v2, Ljava/io/File; */
final String v3 = "/data/miuilog/stability/scout/sys"; // const-string v3, "/data/miuilog/stability/scout/sys"
/* invoke-direct {v2, v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* move-object v1, v2 */
/* .line 263 */
} // :cond_1
/* new-instance v2, Ljava/io/File; */
final String v3 = "/data/miuilog/stability/scout/app"; // const-string v3, "/data/miuilog/stability/scout/app"
/* invoke-direct {v2, v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* move-object v1, v2 */
/* .line 266 */
} // :goto_0
v2 = (( java.io.File ) v1 ).createNewFile ( ); // invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 267 */
(( java.io.File ) v1 ).getAbsolutePath ( ); // invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
/* const/16 v3, 0x1fd */
int v4 = -1; // const/4 v4, -0x1
android.os.FileUtils .setPermissions ( v2,v3,v4,v4 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 271 */
} // .end local v0 # "lastAnrFileName":Ljava/lang/String;
} // :cond_2
/* .line 269 */
/* :catch_0 */
/* move-exception v0 */
/* .line 270 */
/* .local v0, "e":Ljava/io/IOException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Exception creating lastAnrState dump file"; // const-string v3, "Exception creating lastAnrState dump file"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.IOException ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "ProcessRecordInjector"; // const-string v3, "ProcessRecordInjector"
android.util.Slog .w ( v3,v2 );
/* .line 272 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_1
} // .end method
private static com.miui.server.process.ProcessManagerInternal getProcessManagerInternal ( ) {
/* .locals 2 */
/* .line 82 */
v0 = com.android.server.am.ProcessRecordImpl.sProcessManagerInternal;
/* if-nez v0, :cond_1 */
/* .line 83 */
/* const-class v0, Lcom/android/server/am/ProcessRecordImpl; */
/* monitor-enter v0 */
/* .line 84 */
try { // :try_start_0
v1 = com.android.server.am.ProcessRecordImpl.sProcessManagerInternal;
/* if-nez v1, :cond_0 */
/* .line 85 */
/* const-class v1, Lcom/miui/server/process/ProcessManagerInternal; */
com.android.server.LocalServices .getService ( v1 );
/* check-cast v1, Lcom/miui/server/process/ProcessManagerInternal; */
/* .line 87 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 89 */
} // :cond_1
} // :goto_0
v0 = com.android.server.am.ProcessRecordImpl.sProcessManagerInternal;
} // .end method
private void reportANR ( com.android.server.am.ActivityManagerService p0, java.lang.String p1, com.android.server.am.ProcessRecord p2, java.lang.String p3, java.lang.String p4, java.lang.String p5, java.lang.String p6, java.lang.String p7, java.io.File p8, android.app.ApplicationErrorReport$CrashInfo p9, java.lang.String p10, com.android.server.am.ScoutAnrInfo p11 ) {
/* .locals 14 */
/* .param p1, "ams" # Lcom/android/server/am/ActivityManagerService; */
/* .param p2, "eventType" # Ljava/lang/String; */
/* .param p3, "process" # Lcom/android/server/am/ProcessRecord; */
/* .param p4, "processName" # Ljava/lang/String; */
/* .param p5, "activityShortComponentName" # Ljava/lang/String; */
/* .param p6, "parentShortCompontnName" # Ljava/lang/String; */
/* .param p7, "subject" # Ljava/lang/String; */
/* .param p8, "report" # Ljava/lang/String; */
/* .param p9, "logFile" # Ljava/io/File; */
/* .param p10, "crashInfo" # Landroid/app/ApplicationErrorReport$CrashInfo; */
/* .param p11, "headline" # Ljava/lang/String; */
/* .param p12, "anrInfo" # Lcom/android/server/am/ScoutAnrInfo; */
/* .line 215 */
/* move-object v0, p0 */
/* move-object/from16 v1, p3 */
/* move-object/from16 v2, p5 */
/* move-object/from16 v3, p6 */
/* move-object/from16 v4, p9 */
/* new-instance v5, Lmiui/mqsas/sdk/event/AnrEvent; */
/* invoke-direct {v5}, Lmiui/mqsas/sdk/event/AnrEvent;-><init>()V */
/* .line 216 */
/* .local v5, "event":Lmiui/mqsas/sdk/event/AnrEvent; */
v6 = /* invoke-virtual/range {p12 ..p12}, Lcom/android/server/am/ScoutAnrInfo;->getpid()I */
(( miui.mqsas.sdk.event.AnrEvent ) v5 ).setPid ( v6 ); // invoke-virtual {v5, v6}, Lmiui/mqsas/sdk/event/AnrEvent;->setPid(I)V
/* .line 217 */
v6 = /* invoke-virtual/range {p12 ..p12}, Lcom/android/server/am/ScoutAnrInfo;->getuid()I */
(( miui.mqsas.sdk.event.AnrEvent ) v5 ).setUid ( v6 ); // invoke-virtual {v5, v6}, Lmiui/mqsas/sdk/event/AnrEvent;->setUid(I)V
/* .line 218 */
/* iget v6, v1, Lcom/android/server/am/ProcessRecord;->mPid:I */
/* if-ne v6, v7, :cond_0 */
/* const-string/jumbo v6, "system_server" */
} // :cond_0
/* move-object/from16 v6, p4 */
} // :goto_0
(( miui.mqsas.sdk.event.AnrEvent ) v5 ).setProcessName ( v6 ); // invoke-virtual {v5, v6}, Lmiui/mqsas/sdk/event/AnrEvent;->setProcessName(Ljava/lang/String;)V
/* .line 219 */
/* const-string/jumbo v6, "system" */
/* move-object/from16 v7, p4 */
v6 = (( java.lang.String ) v6 ).equals ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_1
/* iget v6, v1, Lcom/android/server/am/ProcessRecord;->mPid:I */
/* if-eq v6, v8, :cond_1 */
/* .line 220 */
v6 = this.info;
v6 = this.packageName;
} // :cond_1
(( miui.mqsas.sdk.event.AnrEvent ) v5 ).getProcessName ( ); // invoke-virtual {v5}, Lmiui/mqsas/sdk/event/AnrEvent;->getProcessName()Ljava/lang/String;
/* .line 219 */
} // :goto_1
(( miui.mqsas.sdk.event.AnrEvent ) v5 ).setPackageName ( v6 ); // invoke-virtual {v5, v6}, Lmiui/mqsas/sdk/event/AnrEvent;->setPackageName(Ljava/lang/String;)V
/* .line 221 */
/* invoke-virtual/range {p12 ..p12}, Lcom/android/server/am/ScoutAnrInfo;->getTimeStamp()J */
/* move-result-wide v8 */
(( miui.mqsas.sdk.event.AnrEvent ) v5 ).setTimeStamp ( v8, v9 ); // invoke-virtual {v5, v8, v9}, Lmiui/mqsas/sdk/event/AnrEvent;->setTimeStamp(J)V
/* .line 222 */
/* move-object/from16 v6, p8 */
(( miui.mqsas.sdk.event.AnrEvent ) v5 ).setReason ( v6 ); // invoke-virtual {v5, v6}, Lmiui/mqsas/sdk/event/AnrEvent;->setReason(Ljava/lang/String;)V
/* .line 223 */
/* move-object/from16 v8, p7 */
(( miui.mqsas.sdk.event.AnrEvent ) v5 ).setCpuInfo ( v8 ); // invoke-virtual {v5, v8}, Lmiui/mqsas/sdk/event/AnrEvent;->setCpuInfo(Ljava/lang/String;)V
/* .line 224 */
v9 = /* invoke-virtual/range {p12 ..p12}, Lcom/android/server/am/ScoutAnrInfo;->getBgAnr()Z */
(( miui.mqsas.sdk.event.AnrEvent ) v5 ).setBgAnr ( v9 ); // invoke-virtual {v5, v9}, Lmiui/mqsas/sdk/event/AnrEvent;->setBgAnr(Z)V
/* .line 225 */
v9 = /* invoke-virtual/range {p12 ..p12}, Lcom/android/server/am/ScoutAnrInfo;->getBlockSystemState()Z */
(( miui.mqsas.sdk.event.AnrEvent ) v5 ).setBlockSystemState ( v9 ); // invoke-virtual {v5, v9}, Lmiui/mqsas/sdk/event/AnrEvent;->setBlockSystemState(Z)V
/* .line 226 */
/* invoke-virtual/range {p12 ..p12}, Lcom/android/server/am/ScoutAnrInfo;->getBinderTransInfo()Ljava/lang/String; */
(( miui.mqsas.sdk.event.AnrEvent ) v5 ).setBinderTransactionInfo ( v9 ); // invoke-virtual {v5, v9}, Lmiui/mqsas/sdk/event/AnrEvent;->setBinderTransactionInfo(Ljava/lang/String;)V
/* .line 227 */
if ( v4 != null) { // if-eqz v4, :cond_2
v9 = /* invoke-virtual/range {p9 ..p9}, Ljava/io/File;->exists()Z */
if ( v9 != null) { // if-eqz v9, :cond_2
/* .line 228 */
/* invoke-virtual/range {p9 ..p9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String; */
(( miui.mqsas.sdk.event.AnrEvent ) v5 ).setLogName ( v9 ); // invoke-virtual {v5, v9}, Lmiui/mqsas/sdk/event/AnrEvent;->setLogName(Ljava/lang/String;)V
/* .line 231 */
} // :cond_2
/* invoke-direct {p0, v5, v4}, Lcom/android/server/am/ProcessRecordImpl;->createLastAnrFile(Lmiui/mqsas/sdk/event/AnrEvent;Ljava/io/File;)Ljava/io/File; */
/* .line 233 */
/* .local v9, "lastAnrStateFile":Ljava/io/File; */
if ( v9 != null) { // if-eqz v9, :cond_3
v10 = (( java.io.File ) v9 ).exists ( ); // invoke-virtual {v9}, Ljava/io/File;->exists()Z
if ( v10 != null) { // if-eqz v10, :cond_3
/* .line 234 */
/* move-object v10, p1 */
/* invoke-direct {p0, p1, v9}, Lcom/android/server/am/ProcessRecordImpl;->saveLastAnrState(Lcom/android/server/am/ActivityManagerService;Ljava/io/File;)V */
/* .line 235 */
/* new-instance v11, Ljava/util/ArrayList; */
/* invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V */
/* .line 236 */
/* .local v11, "filesNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
(( java.io.File ) v9 ).getAbsolutePath ( ); // invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
/* .line 237 */
/* new-instance v12, Ljava/lang/StringBuilder; */
/* invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V */
final String v13 = " add extra file: "; // const-string v13, " add extra file: "
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v9 ).getAbsolutePath ( ); // invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).toString ( ); // invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v13 = "ProcessRecordInjector"; // const-string v13, "ProcessRecordInjector"
android.util.Slog .i ( v13,v12 );
/* .line 238 */
(( miui.mqsas.sdk.event.AnrEvent ) v5 ).setExtraFiles ( v11 ); // invoke-virtual {v5, v11}, Lmiui/mqsas/sdk/event/AnrEvent;->setExtraFiles(Ljava/util/List;)V
/* .line 233 */
} // .end local v11 # "filesNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_3
/* move-object v10, p1 */
/* .line 241 */
} // :goto_2
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 242 */
(( miui.mqsas.sdk.event.AnrEvent ) v5 ).setTargetActivity ( v2 ); // invoke-virtual {v5, v2}, Lmiui/mqsas/sdk/event/AnrEvent;->setTargetActivity(Ljava/lang/String;)V
/* .line 244 */
} // :cond_4
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 245 */
(( miui.mqsas.sdk.event.AnrEvent ) v5 ).setParent ( v3 ); // invoke-virtual {v5, v3}, Lmiui/mqsas/sdk/event/AnrEvent;->setParent(Ljava/lang/String;)V
/* .line 247 */
} // :cond_5
miui.mqsas.sdk.MQSEventManagerDelegate .getInstance ( );
(( miui.mqsas.sdk.MQSEventManagerDelegate ) v11 ).reportAnrEvent ( v5 ); // invoke-virtual {v11, v5}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportAnrEvent(Lmiui/mqsas/sdk/event/AnrEvent;)V
/* .line 248 */
return;
} // .end method
private void saveLastAnrState ( com.android.server.am.ActivityManagerService p0, java.io.File p1 ) {
/* .locals 8 */
/* .param p1, "ams" # Lcom/android/server/am/ActivityManagerService; */
/* .param p2, "file" # Ljava/io/File; */
/* .line 276 */
final String v0 = "lastanr"; // const-string v0, "lastanr"
final String v1 = "\n"; // const-string v1, "\n"
if ( p1 != null) { // if-eqz p1, :cond_1
v2 = (( java.io.File ) p2 ).exists ( ); // invoke-virtual {p2}, Ljava/io/File;->exists()Z
/* if-nez v2, :cond_0 */
/* .line 279 */
} // :cond_0
try { // :try_start_0
/* new-instance v2, Ljava/io/FileOutputStream; */
int v3 = 1; // const/4 v3, 0x1
/* invoke-direct {v2, p2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 280 */
/* .local v2, "fout":Ljava/io/FileOutputStream; */
try { // :try_start_1
/* new-instance v4, Lcom/android/internal/util/FastPrintWriter; */
/* invoke-direct {v4, v2}, Lcom/android/internal/util/FastPrintWriter;-><init>(Ljava/io/OutputStream;)V */
/* .line 281 */
/* .local v4, "pw":Lcom/android/internal/util/FastPrintWriter; */
(( com.android.internal.util.FastPrintWriter ) v4 ).println ( v1 ); // invoke-virtual {v4, v1}, Lcom/android/internal/util/FastPrintWriter;->println(Ljava/lang/String;)V
/* .line 282 */
/* new-array v5, v3, [Ljava/lang/String; */
int v6 = 0; // const/4 v6, 0x0
/* aput-object v0, v5, v6 */
int v7 = 0; // const/4 v7, 0x0
(( com.android.server.am.ActivityManagerService ) p1 ).dump ( v7, v4, v5 ); // invoke-virtual {p1, v7, v4, v5}, Lcom/android/server/am/ActivityManagerService;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
/* .line 283 */
(( com.android.internal.util.FastPrintWriter ) v4 ).println ( v1 ); // invoke-virtual {v4, v1}, Lcom/android/internal/util/FastPrintWriter;->println(Ljava/lang/String;)V
/* .line 284 */
v1 = this.mWindowManager;
/* new-array v3, v3, [Ljava/lang/String; */
/* aput-object v0, v3, v6 */
(( com.android.server.wm.WindowManagerService ) v1 ).dump ( v7, v4, v3 ); // invoke-virtual {v1, v7, v4, v3}, Lcom/android/server/wm/WindowManagerService;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
/* .line 285 */
(( com.android.internal.util.FastPrintWriter ) v4 ).flush ( ); // invoke-virtual {v4}, Lcom/android/internal/util/FastPrintWriter;->flush()V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 286 */
} // .end local v4 # "pw":Lcom/android/internal/util/FastPrintWriter;
try { // :try_start_2
(( java.io.FileOutputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 288 */
} // .end local v2 # "fout":Ljava/io/FileOutputStream;
/* .line 279 */
/* .restart local v2 # "fout":Ljava/io/FileOutputStream; */
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_3
(( java.io.FileOutputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v1 */
try { // :try_start_4
(( java.lang.Throwable ) v0 ).addSuppressed ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local p0 # "this":Lcom/android/server/am/ProcessRecordImpl;
} // .end local p1 # "ams":Lcom/android/server/am/ActivityManagerService;
} // .end local p2 # "file":Ljava/io/File;
} // :goto_0
/* throw v0 */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 286 */
} // .end local v2 # "fout":Ljava/io/FileOutputStream;
/* .restart local p0 # "this":Lcom/android/server/am/ProcessRecordImpl; */
/* .restart local p1 # "ams":Lcom/android/server/am/ActivityManagerService; */
/* .restart local p2 # "file":Ljava/io/File; */
/* :catch_0 */
/* move-exception v0 */
/* .line 287 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "ProcessRecordInjector"; // const-string v1, "ProcessRecordInjector"
final String v2 = "saveLastAnrState error "; // const-string v2, "saveLastAnrState error "
android.util.Slog .e ( v1,v2,v0 );
/* .line 289 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
return;
/* .line 277 */
} // :cond_1
} // :goto_2
return;
} // .end method
/* # virtual methods */
public void dumpPeriodHistoryMessage ( com.android.server.am.ProcessRecord p0, Long p1, Integer p2, Boolean p3 ) {
/* .locals 3 */
/* .param p1, "process" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "anrTime" # J */
/* .param p4, "duration" # I */
/* .param p5, "isSystemInputAnr" # Z */
/* .line 378 */
final String v0 = "ProcessRecordInjector"; // const-string v0, "ProcessRecordInjector"
try { // :try_start_0
(( com.android.server.am.ProcessRecord ) p1 ).getThread ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 379 */
/* if-nez p5, :cond_0 */
/* .line 380 */
(( com.android.server.am.ProcessRecord ) p1 ).getThread ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
/* .line 382 */
} // :cond_0
com.android.server.ScoutHelper .dumpUithreadPeriodHistoryMessage ( p2,p3,p4 );
/* .line 385 */
} // :cond_1
final String v1 = "Can\'t dumpPeriodHistoryMessage because of null IApplicationThread"; // const-string v1, "Can\'t dumpPeriodHistoryMessage because of null IApplicationThread"
android.util.Slog .w ( v0,v1 );
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 389 */
} // :goto_0
/* .line 387 */
/* :catch_0 */
/* move-exception v1 */
/* .line 388 */
/* .local v1, "e":Landroid/os/RemoteException; */
final String v2 = "Failed to dumpPeriodHistoryMessage after ANR"; // const-string v2, "Failed to dumpPeriodHistoryMessage after ANR"
android.util.Slog .w ( v0,v2,v1 );
/* .line 390 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_1
return;
} // .end method
public void onANR ( com.android.server.am.ActivityManagerService p0, com.android.server.am.ProcessRecord p1, java.lang.String p2, java.lang.String p3, java.lang.String p4, java.lang.String p5, java.io.File p6, android.app.ApplicationErrorReport$CrashInfo p7, java.lang.String p8, com.android.server.am.ScoutAnrInfo p9 ) {
/* .locals 15 */
/* .param p1, "ams" # Lcom/android/server/am/ActivityManagerService; */
/* .param p2, "process" # Lcom/android/server/am/ProcessRecord; */
/* .param p3, "activityShortComponentName" # Ljava/lang/String; */
/* .param p4, "parentShortCompontnName" # Ljava/lang/String; */
/* .param p5, "subject" # Ljava/lang/String; */
/* .param p6, "report" # Ljava/lang/String; */
/* .param p7, "logFile" # Ljava/io/File; */
/* .param p8, "crashInfo" # Landroid/app/ApplicationErrorReport$CrashInfo; */
/* .param p9, "headline" # Ljava/lang/String; */
/* .param p10, "anrInfo" # Lcom/android/server/am/ScoutAnrInfo; */
/* .line 202 */
/* move-object/from16 v13, p10 */
if ( v13 != null) { // if-eqz v13, :cond_0
/* .line 203 */
/* invoke-virtual/range {p10 ..p10}, Lcom/android/server/am/ScoutAnrInfo;->getBinderTransInfo()Ljava/lang/String; */
(( com.android.server.am.ScoutAnrInfo ) v13 ).setBinderTransInfo ( v0 ); // invoke-virtual {v13, v0}, Lcom/android/server/am/ScoutAnrInfo;->setBinderTransInfo(Ljava/lang/String;)V
/* .line 204 */
final String v2 = "anr"; // const-string v2, "anr"
/* move-object/from16 v14, p2 */
v4 = this.processName;
/* move-object v0, p0 */
/* move-object/from16 v1, p1 */
/* move-object/from16 v3, p2 */
/* move-object/from16 v5, p3 */
/* move-object/from16 v6, p4 */
/* move-object/from16 v7, p5 */
/* move-object/from16 v8, p6 */
/* move-object/from16 v9, p7 */
/* move-object/from16 v10, p8 */
/* move-object/from16 v11, p9 */
/* move-object/from16 v12, p10 */
/* invoke-direct/range {v0 ..v12}, Lcom/android/server/am/ProcessRecordImpl;->reportANR(Lcom/android/server/am/ActivityManagerService;Ljava/lang/String;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Landroid/app/ApplicationErrorReport$CrashInfo;Ljava/lang/String;Lcom/android/server/am/ScoutAnrInfo;)V */
/* .line 202 */
} // :cond_0
/* move-object/from16 v14, p2 */
/* .line 207 */
} // :goto_0
return;
} // .end method
public void reportAppPss ( ) {
/* .locals 5 */
/* .line 127 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 128 */
/* .local v0, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/am/ProcessRecordImpl$AppPss;>;" */
v1 = com.android.server.am.ProcessRecordImpl.sLock;
/* monitor-enter v1 */
/* .line 129 */
try { // :try_start_0
v2 = com.android.server.am.ProcessRecordImpl.sAppPssUserMap;
v3 = (( android.util.SparseArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/SparseArray;->size()I
/* if-lez v3, :cond_2 */
/* .line 130 */
v2 = (( android.util.SparseArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/SparseArray;->size()I
/* .line 131 */
/* .local v2, "size":I */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
/* if-ge v3, v2, :cond_1 */
/* .line 132 */
v4 = com.android.server.am.ProcessRecordImpl.sAppPssUserMap;
(( android.util.SparseArray ) v4 ).valueAt ( v3 ); // invoke-virtual {v4, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v4, Ljava/util/Map; */
/* .line 133 */
/* .local v4, "appPssMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/am/ProcessRecordImpl$AppPss;>;" */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 134 */
/* .line 131 */
} // .end local v4 # "appPssMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/am/ProcessRecordImpl$AppPss;>;"
} // :cond_0
/* add-int/lit8 v3, v3, 0x1 */
/* .line 137 */
} // .end local v3 # "i":I
} // :cond_1
v3 = com.android.server.am.ProcessRecordImpl.sAppPssUserMap;
(( android.util.SparseArray ) v3 ).clear ( ); // invoke-virtual {v3}, Landroid/util/SparseArray;->clear()V
/* .line 139 */
} // .end local v2 # "size":I
} // :cond_2
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
v1 = /* .line 140 */
/* if-nez v1, :cond_3 */
/* .line 141 */
return;
/* .line 143 */
} // :cond_3
com.android.internal.os.BackgroundThread .getHandler ( );
/* new-instance v2, Lcom/android/server/am/ProcessRecordImpl$1; */
/* invoke-direct {v2, p0, v0}, Lcom/android/server/am/ProcessRecordImpl$1;-><init>(Lcom/android/server/am/ProcessRecordImpl;Ljava/util/Map;)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 171 */
return;
/* .line 139 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
} // .end method
public void scoutAppAddBinderCallChainNativePids ( java.lang.String p0, java.util.ArrayList p1, java.util.ArrayList p2 ) {
/* .locals 3 */
/* .param p1, "tag" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 308 */
/* .local p2, "nativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* .local p3, "binderCallChainNativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
v0 = (( java.util.ArrayList ) p3 ).size ( ); // invoke-virtual {p3}, Ljava/util/ArrayList;->size()I
/* if-lez v0, :cond_1 */
/* .line 309 */
(( java.util.ArrayList ) p3 ).iterator ( ); // invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* .line 310 */
/* .local v1, "nativePid":I */
java.lang.Integer .valueOf ( v1 );
v2 = (( java.util.ArrayList ) p2 ).contains ( v2 ); // invoke-virtual {p2, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v2, :cond_0 */
/* .line 311 */
java.lang.Integer .valueOf ( v1 );
(( java.util.ArrayList ) p2 ).add ( v2 ); // invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 313 */
} // .end local v1 # "nativePid":I
} // :cond_0
/* .line 315 */
} // :cond_1
return;
} // .end method
public void scoutAppCheckBinderCallChain ( java.lang.String p0, Integer p1, Integer p2, java.lang.String p3, java.util.ArrayList p4, java.util.ArrayList p5, com.android.server.am.ScoutAnrInfo p6 ) {
/* .locals 8 */
/* .param p1, "tag" # Ljava/lang/String; */
/* .param p2, "Pid" # I */
/* .param p3, "systemPid" # I */
/* .param p4, "annotation" # Ljava/lang/String; */
/* .param p7, "anrInfo" # Lcom/android/server/am/ScoutAnrInfo; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "II", */
/* "Ljava/lang/String;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;", */
/* "Lcom/android/server/am/ScoutAnrInfo;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 321 */
/* .local p5, "firstPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* .local p6, "nativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* new-instance v0, Ljava/util/ArrayList; */
int v1 = 5; // const/4 v1, 0x5
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V */
/* .line 322 */
/* .local v0, "scoutJavaPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V */
/* move-object v1, v2 */
/* .line 323 */
/* .local v1, "scoutNativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* new-instance v2, Lcom/android/server/ScoutHelper$ScoutBinderInfo; */
int v3 = 1; // const/4 v3, 0x1
final String v4 = "MIUIScout ANR"; // const-string v4, "MIUIScout ANR"
/* invoke-direct {v2, p2, p3, v3, v4}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;-><init>(IIILjava/lang/String;)V */
/* .line 325 */
/* .local v2, "scoutBinderInfo":Lcom/android/server/ScoutHelper$ScoutBinderInfo; */
int v5 = 0; // const/4 v5, 0x0
/* .line 326 */
/* .local v5, "isBlockSystem":Z */
java.lang.Integer .valueOf ( p2 );
(( java.util.ArrayList ) v0 ).add ( v6 ); // invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 327 */
v5 = com.android.server.ScoutHelper .checkBinderCallPidList ( p2,v2,v0,v1 );
/* .line 329 */
java.lang.Integer .valueOf ( p3 );
v6 = (( java.util.ArrayList ) v0 ).contains ( v6 ); // invoke-virtual {v0, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v6, :cond_2 */
final String v6 = "Broadcast"; // const-string v6, "Broadcast"
v6 = (( java.lang.String ) p4 ).contains ( v6 ); // invoke-virtual {p4, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 330 */
v6 = com.android.server.ScoutHelper .checkAsyncBinderCallPidList ( p2,p3,v2,v0,v1 );
/* if-nez v6, :cond_1 */
if ( v5 != null) { // if-eqz v5, :cond_0
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
} // :cond_1
} // :goto_0
/* move v5, v3 */
/* .line 334 */
} // :cond_2
com.android.server.ScoutHelper .printfProcBinderInfo ( p2,v4 );
/* .line 335 */
com.android.server.ScoutHelper .printfProcBinderInfo ( p3,v4 );
/* .line 337 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( com.android.server.ScoutHelper$ScoutBinderInfo ) v2 ).getBinderTransInfo ( ); // invoke-virtual {v2}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->getBinderTransInfo()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "\n"; // const-string v4, "\n"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 338 */
(( com.android.server.ScoutHelper$ScoutBinderInfo ) v2 ).getProcInfo ( ); // invoke-virtual {v2}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->getProcInfo()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 337 */
(( com.android.server.am.ScoutAnrInfo ) p7 ).setBinderTransInfo ( v3 ); // invoke-virtual {p7, v3}, Lcom/android/server/am/ScoutAnrInfo;->setBinderTransInfo(Ljava/lang/String;)V
/* .line 339 */
v3 = (( com.android.server.ScoutHelper$ScoutBinderInfo ) v2 ).getDThreadState ( ); // invoke-virtual {v2}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->getDThreadState()Z
(( com.android.server.am.ScoutAnrInfo ) p7 ).setDThreadState ( v3 ); // invoke-virtual {p7, v3}, Lcom/android/server/am/ScoutAnrInfo;->setDThreadState(Z)V
/* .line 341 */
v3 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
final String v4 = "Dump Trace: add java proc "; // const-string v4, "Dump Trace: add java proc "
/* if-lez v3, :cond_4 */
/* .line 342 */
(( java.util.ArrayList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v6 = } // :goto_1
if ( v6 != null) { // if-eqz v6, :cond_4
/* check-cast v6, Ljava/lang/Integer; */
v6 = (( java.lang.Integer ) v6 ).intValue ( ); // invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I
/* .line 343 */
/* .local v6, "javaPid":I */
java.lang.Integer .valueOf ( v6 );
v7 = (( java.util.ArrayList ) p5 ).contains ( v7 ); // invoke-virtual {p5, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v7, :cond_3 */
/* .line 344 */
java.lang.Integer .valueOf ( v6 );
(( java.util.ArrayList ) p5 ).add ( v7 ); // invoke-virtual {p5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 345 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( p1,v7 );
/* .line 347 */
} // .end local v6 # "javaPid":I
} // :cond_3
/* .line 350 */
} // :cond_4
v3 = (( java.util.ArrayList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
/* if-lez v3, :cond_6 */
/* .line 351 */
(( java.util.ArrayList ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v6 = } // :goto_2
if ( v6 != null) { // if-eqz v6, :cond_6
/* check-cast v6, Ljava/lang/Integer; */
v6 = (( java.lang.Integer ) v6 ).intValue ( ); // invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I
/* .line 352 */
/* .local v6, "nativePid":I */
java.lang.Integer .valueOf ( v6 );
v7 = (( java.util.ArrayList ) p6 ).contains ( v7 ); // invoke-virtual {p6, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v7, :cond_5 */
/* .line 353 */
java.lang.Integer .valueOf ( v6 );
(( java.util.ArrayList ) p6 ).add ( v7 ); // invoke-virtual {p6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 354 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( p1,v7 );
/* .line 356 */
} // .end local v6 # "nativePid":I
} // :cond_5
/* .line 358 */
} // :cond_6
(( com.android.server.am.ScoutAnrInfo ) p7 ).setBlockSystemState ( v5 ); // invoke-virtual {p7, v5}, Lcom/android/server/am/ScoutAnrInfo;->setBlockSystemState(Z)V
/* .line 359 */
return;
} // .end method
public void scoutAppCheckDumpKernelTrace ( java.lang.String p0, com.android.server.am.ScoutAnrInfo p1 ) {
/* .locals 2 */
/* .param p1, "tag" # Ljava/lang/String; */
/* .param p2, "anrInfo" # Lcom/android/server/am/ScoutAnrInfo; */
/* .line 363 */
/* sget-boolean v0, Lcom/android/server/ScoutHelper;->SYSRQ_ANR_D_THREAD:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( com.android.server.am.ScoutAnrInfo ) p2 ).getDThreadState ( ); // invoke-virtual {p2}, Lcom/android/server/am/ScoutAnrInfo;->getDThreadState()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 364 */
/* const/16 v0, 0x77 */
com.android.server.ScoutHelper .doSysRqInterface ( v0 );
/* .line 365 */
/* const/16 v0, 0x6c */
com.android.server.ScoutHelper .doSysRqInterface ( v0 );
/* .line 366 */
/* sget-boolean v0, Lcom/android/server/ScoutHelper;->PANIC_ANR_D_THREAD:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = com.android.server.ScoutHelper .isDebugpolicyed ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 368 */
/* const-wide/16 v0, 0xbb8 */
android.os.SystemClock .sleep ( v0,v1 );
/* .line 369 */
final String v0 = "Trigge Panic Crash when anr Process has D state thread"; // const-string v0, "Trigge Panic Crash when anr Process has D state thread"
android.util.Slog .e ( p1,v0 );
/* .line 370 */
/* const/16 v0, 0x63 */
com.android.server.ScoutHelper .doSysRqInterface ( v0 );
/* .line 373 */
} // :cond_0
return;
} // .end method
public void scoutAppUpdateAnrInfo ( java.lang.String p0, com.android.server.am.ProcessRecord p1, com.android.server.am.ScoutAnrInfo p2 ) {
/* .locals 2 */
/* .param p1, "tag" # Ljava/lang/String; */
/* .param p2, "process" # Lcom/android/server/am/ProcessRecord; */
/* .param p3, "anrInfo" # Lcom/android/server/am/ScoutAnrInfo; */
/* .line 300 */
v0 = (( com.android.server.am.ProcessRecord ) p2 ).getPid ( ); // invoke-virtual {p2}, Lcom/android/server/am/ProcessRecord;->getPid()I
(( com.android.server.am.ScoutAnrInfo ) p3 ).setPid ( v0 ); // invoke-virtual {p3, v0}, Lcom/android/server/am/ScoutAnrInfo;->setPid(I)V
/* .line 301 */
v0 = (( com.android.server.am.ProcessRecord ) p2 ).getStartUid ( ); // invoke-virtual {p2}, Lcom/android/server/am/ProcessRecord;->getStartUid()I
(( com.android.server.am.ScoutAnrInfo ) p3 ).setuid ( v0 ); // invoke-virtual {p3, v0}, Lcom/android/server/am/ScoutAnrInfo;->setuid(I)V
/* .line 302 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
(( com.android.server.am.ScoutAnrInfo ) p3 ).setTimeStamp ( v0, v1 ); // invoke-virtual {p3, v0, v1}, Lcom/android/server/am/ScoutAnrInfo;->setTimeStamp(J)V
/* .line 303 */
return;
} // .end method
public Boolean skipAppErrorDialog ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 2 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 292 */
if ( p1 != null) { // if-eqz p1, :cond_2
v0 = this.info;
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = (( com.android.server.am.ProcessRecord ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I
v1 = android.os.Process .myPid ( );
/* if-eq v0, v1, :cond_2 */
/* sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.info;
v0 = this.packageName;
/* .line 294 */
final String v1 = "com.xiaomi.misubscreenui"; // const-string v1, "com.xiaomi.misubscreenui"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 295 */
} // :cond_0
v0 = miui.mqsas.scout.ScoutUtils .isLibraryTest ( );
if ( v0 != null) { // if-eqz v0, :cond_2
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
/* .line 292 */
} // :goto_0
} // .end method
