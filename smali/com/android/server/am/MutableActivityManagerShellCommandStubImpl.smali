.class public Lcom/android/server/am/MutableActivityManagerShellCommandStubImpl;
.super Ljava/lang/Object;
.source "MutableActivityManagerShellCommandStubImpl.java"

# interfaces
.implements Lcom/android/server/am/MutableActivityManagerShellCommandStub;


# static fields
.field private static final DEFAULT_MAX_SHELL_SYNC_BROADCAST:I = 0x5

.field private static final PROPERTIES_MAX_SHELL_SYNC_BROADCAST:Ljava/lang/String; = "persist.sys.max_shell_sync_broadcast"

.field private static mShellSyncBroadcast:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field activityManagerShellCommand:Lcom/android/server/am/ActivityManagerShellCommand;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 20
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/android/server/am/MutableActivityManagerShellCommandStubImpl;->mShellSyncBroadcast:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addLog(Ljava/io/PrintWriter;Ljava/lang/String;)I
    .locals 4
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "cmd"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 31
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, -0x1

    sparse-switch v0, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string v0, "app-logging"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_1

    :sswitch_1
    const-string v0, "move-to-privatecast"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    goto :goto_1

    :sswitch_2
    const-string v0, "logging"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    goto :goto_1

    :sswitch_3
    const-string v0, "move-to-cast"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_4
    const-string v0, "exit-cast"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_5
    const-string v0, "exit-privatecast"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    goto :goto_1

    :goto_0
    move v0, v3

    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 57
    return v3

    .line 53
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/am/MutableActivityManagerShellCommandStubImpl;->activityManagerShellCommand:Lcom/android/server/am/ActivityManagerShellCommand;

    iget-object v0, v0, Lcom/android/server/am/ActivityManagerShellCommand;->mWindowInterface:Landroid/view/IWindowManager;

    invoke-interface {v0, v1, v2}, Landroid/view/IWindowManager;->setScreenProjectionList(II)V

    .line 54
    return v2

    .line 49
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/am/MutableActivityManagerShellCommandStubImpl;->activityManagerShellCommand:Lcom/android/server/am/ActivityManagerShellCommand;

    iget-object v0, v0, Lcom/android/server/am/ActivityManagerShellCommand;->mWindowInterface:Landroid/view/IWindowManager;

    invoke-interface {v0, v1, v1}, Landroid/view/IWindowManager;->setScreenProjectionList(II)V

    .line 50
    return v2

    .line 45
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/am/MutableActivityManagerShellCommandStubImpl;->activityManagerShellCommand:Lcom/android/server/am/ActivityManagerShellCommand;

    iget-object v0, v0, Lcom/android/server/am/ActivityManagerShellCommand;->mTaskInterface:Landroid/app/IActivityTaskManager;

    invoke-interface {v0}, Landroid/app/IActivityTaskManager;->exitCastMode()V

    .line 46
    return v2

    .line 41
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/am/MutableActivityManagerShellCommandStubImpl;->activityManagerShellCommand:Lcom/android/server/am/ActivityManagerShellCommand;

    iget-object v0, v0, Lcom/android/server/am/ActivityManagerShellCommand;->mTaskInterface:Landroid/app/IActivityTaskManager;

    invoke-interface {v0}, Landroid/app/IActivityTaskManager;->moveTopActivityToCastMode()V

    .line 42
    return v2

    .line 36
    :pswitch_4
    invoke-virtual {p0, p1}, Lcom/android/server/am/MutableActivityManagerShellCommandStubImpl;->runAppLogging(Ljava/io/PrintWriter;)V

    .line 37
    return v2

    .line 33
    :pswitch_5
    invoke-virtual {p0, p1}, Lcom/android/server/am/MutableActivityManagerShellCommandStubImpl;->runLogging(Ljava/io/PrintWriter;)V

    .line 34
    return v2

    :sswitch_data_0
    .sparse-switch
        -0x5b7dcaed -> :sswitch_5
        -0x546a1972 -> :sswitch_4
        -0x165edeab -> :sswitch_3
        0x1466cb5f -> :sswitch_2
        0x584b9c6c -> :sswitch_1
        0x7fb577b3 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public init(Lcom/android/server/am/ActivityManagerShellCommand;)V
    .locals 0
    .param p1, "activityManagerShellCommand"    # Lcom/android/server/am/ActivityManagerShellCommand;

    .line 26
    iput-object p1, p0, Lcom/android/server/am/MutableActivityManagerShellCommandStubImpl;->activityManagerShellCommand:Lcom/android/server/am/ActivityManagerShellCommand;

    .line 27
    return-void
.end method

.method public runAppLogging(Ljava/io/PrintWriter;)V
    .locals 6
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 74
    iget-object v0, p0, Lcom/android/server/am/MutableActivityManagerShellCommandStubImpl;->activityManagerShellCommand:Lcom/android/server/am/ActivityManagerShellCommand;

    invoke-virtual {v0}, Lcom/android/server/am/ActivityManagerShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v0

    .line 75
    .local v0, "processName":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/am/MutableActivityManagerShellCommandStubImpl;->activityManagerShellCommand:Lcom/android/server/am/ActivityManagerShellCommand;

    invoke-virtual {v1}, Lcom/android/server/am/ActivityManagerShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 76
    .local v1, "uid":I
    iget-object v2, p0, Lcom/android/server/am/MutableActivityManagerShellCommandStubImpl;->activityManagerShellCommand:Lcom/android/server/am/ActivityManagerShellCommand;

    invoke-virtual {v2}, Lcom/android/server/am/ActivityManagerShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v2

    .line 77
    .local v2, "cmd":Ljava/lang/String;
    const-string v3, "enable-text"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "disable-text"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 78
    const-string v3, "Error: wrong args , must be enable-text or disable-text"

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 79
    return-void

    .line 81
    :cond_0
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 83
    .local v3, "enable":Z
    :goto_0
    iget-object v4, p0, Lcom/android/server/am/MutableActivityManagerShellCommandStubImpl;->activityManagerShellCommand:Lcom/android/server/am/ActivityManagerShellCommand;

    invoke-virtual {v4}, Lcom/android/server/am/ActivityManagerShellCommand;->getNextArg()Ljava/lang/String;

    move-result-object v4

    move-object v5, v4

    .local v5, "config":Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 84
    iget-object v4, p0, Lcom/android/server/am/MutableActivityManagerShellCommandStubImpl;->activityManagerShellCommand:Lcom/android/server/am/ActivityManagerShellCommand;

    iget-object v4, v4, Lcom/android/server/am/ActivityManagerShellCommand;->mInterface:Landroid/app/IActivityManager;

    invoke-interface {v4, v5, v3, v0, v1}, Landroid/app/IActivityManager;->enableAppDebugConfig(Ljava/lang/String;ZLjava/lang/String;I)V

    goto :goto_0

    .line 86
    :cond_1
    return-void
.end method

.method public runLogging(Ljava/io/PrintWriter;)V
    .locals 4
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 61
    iget-object v0, p0, Lcom/android/server/am/MutableActivityManagerShellCommandStubImpl;->activityManagerShellCommand:Lcom/android/server/am/ActivityManagerShellCommand;

    invoke-virtual {v0}, Lcom/android/server/am/ActivityManagerShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v0

    .line 62
    .local v0, "cmd":Ljava/lang/String;
    const-string v1, "enable-text"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "disable-text"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 63
    const-string v1, "Error: wrong args , must be enable-text or disable-text"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 64
    return-void

    .line 66
    :cond_0
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 68
    .local v1, "enable":Z
    :goto_0
    iget-object v2, p0, Lcom/android/server/am/MutableActivityManagerShellCommandStubImpl;->activityManagerShellCommand:Lcom/android/server/am/ActivityManagerShellCommand;

    invoke-virtual {v2}, Lcom/android/server/am/ActivityManagerShellCommand;->getNextArg()Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .local v3, "config":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 69
    iget-object v2, p0, Lcom/android/server/am/MutableActivityManagerShellCommandStubImpl;->activityManagerShellCommand:Lcom/android/server/am/ActivityManagerShellCommand;

    iget-object v2, v2, Lcom/android/server/am/ActivityManagerShellCommand;->mInterface:Landroid/app/IActivityManager;

    invoke-interface {v2, v3, v1}, Landroid/app/IActivityManager;->enableAmsDebugConfig(Ljava/lang/String;Z)V

    goto :goto_0

    .line 71
    :cond_1
    return-void
.end method

.method public waitForFinishIfNeeded(Lcom/android/server/am/ActivityManagerShellCommand$IntentReceiver;)Z
    .locals 3
    .param p1, "receiver"    # Lcom/android/server/am/ActivityManagerShellCommand$IntentReceiver;

    .line 90
    invoke-static {}, Landroid/os/Build;->isDebuggable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91
    sget-object v0, Lcom/android/server/am/MutableActivityManagerShellCommandStubImpl;->mShellSyncBroadcast:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    .line 92
    const-string v1, "persist.sys.max_shell_sync_broadcast"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-gt v0, v1, :cond_0

    .line 94
    invoke-virtual {p1}, Lcom/android/server/am/ActivityManagerShellCommand$IntentReceiver;->waitForFinish()V

    .line 96
    :cond_0
    sget-object v0, Lcom/android/server/am/MutableActivityManagerShellCommandStubImpl;->mShellSyncBroadcast:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    goto :goto_0

    .line 98
    :cond_1
    invoke-virtual {p1}, Lcom/android/server/am/ActivityManagerShellCommand$IntentReceiver;->waitForFinish()V

    .line 100
    :goto_0
    const/4 v0, 0x1

    return v0
.end method
