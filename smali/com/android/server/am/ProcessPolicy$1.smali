.class Lcom/android/server/am/ProcessPolicy$1;
.super Landroid/database/ContentObserver;
.source "ProcessPolicy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ProcessPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ProcessPolicy;


# direct methods
.method constructor <init>(Lcom/android/server/am/ProcessPolicy;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/ProcessPolicy;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 390
    iput-object p1, p0, Lcom/android/server/am/ProcessPolicy$1;->this$0:Lcom/android/server/am/ProcessPolicy;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 1
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 393
    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    .line 394
    invoke-static {}, Lcom/android/server/am/ProcessPolicy;->-$$Nest$sfgetSETTINGS_MIUI_VOICETRIGGER_ENABLE_URL()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 395
    iget-object v0, p0, Lcom/android/server/am/ProcessPolicy$1;->this$0:Lcom/android/server/am/ProcessPolicy;

    invoke-static {v0}, Lcom/android/server/am/ProcessPolicy;->-$$Nest$mupdateVoiceTriggerWhitelist(Lcom/android/server/am/ProcessPolicy;)V

    goto :goto_0

    .line 396
    :cond_0
    invoke-static {}, Lcom/android/server/am/ProcessPolicy;->-$$Nest$sfgetSETTINGS_MIUI_CCONTENT_CATCHER_URL()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 397
    iget-object v0, p0, Lcom/android/server/am/ProcessPolicy$1;->this$0:Lcom/android/server/am/ProcessPolicy;

    invoke-static {v0}, Lcom/android/server/am/ProcessPolicy;->-$$Nest$mupdateContentCatcherWhitelist(Lcom/android/server/am/ProcessPolicy;)V

    .line 399
    :cond_1
    :goto_0
    return-void
.end method
