class com.android.server.am.ProcessProphetImpl$2 implements android.content.ClipboardManager$OnPrimaryClipChangedListener {
	 /* .source "ProcessProphetImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/am/ProcessProphetImpl;->handlePostInit()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.am.ProcessProphetImpl this$0; //synthetic
final android.content.ClipboardManager val$clipboardManager; //synthetic
/* # direct methods */
 com.android.server.am.ProcessProphetImpl$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/ProcessProphetImpl; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 203 */
this.this$0 = p1;
this.val$clipboardManager = p2;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onPrimaryClipChanged ( ) {
/* .locals 4 */
/* .line 206 */
v0 = this.this$0;
v0 = (( com.android.server.am.ProcessProphetImpl ) v0 ).isEnable ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetImpl;->isEnable()Z
/* if-nez v0, :cond_0 */
return;
/* .line 207 */
} // :cond_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
v2 = this.this$0;
com.android.server.am.ProcessProphetImpl .-$$Nest$fgetmLastCopyUpTime ( v2 );
/* move-result-wide v2 */
/* sub-long/2addr v0, v2 */
/* const-wide/16 v2, 0x7d0 */
/* cmp-long v0, v0, v2 */
/* if-gez v0, :cond_1 */
return;
/* .line 208 */
} // :cond_1
v0 = this.this$0;
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
com.android.server.am.ProcessProphetImpl .-$$Nest$fputmLastCopyUpTime ( v0,v1,v2 );
/* .line 209 */
v0 = this.val$clipboardManager;
v0 = (( android.content.ClipboardManager ) v0 ).hasPrimaryClip ( ); // invoke-virtual {v0}, Landroid/content/ClipboardManager;->hasPrimaryClip()Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 210 */
v0 = this.val$clipboardManager;
(( android.content.ClipboardManager ) v0 ).getPrimaryClip ( ); // invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;
/* .line 211 */
/* .local v0, "clipData":Landroid/content/ClipData; */
if ( v0 != null) { // if-eqz v0, :cond_2
v1 = (( android.content.ClipData ) v0 ).getItemCount ( ); // invoke-virtual {v0}, Landroid/content/ClipData;->getItemCount()I
/* if-lez v1, :cond_2 */
/* .line 212 */
int v1 = 0; // const/4 v1, 0x0
(( android.content.ClipData ) v0 ).getItemAt ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;
(( android.content.ClipData$Item ) v1 ).getText ( ); // invoke-virtual {v1}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;
/* .line 213 */
/* .local v1, "text":Ljava/lang/CharSequence; */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 214 */
v2 = this.this$0;
v2 = this.mHandler;
int v3 = 6; // const/4 v3, 0x6
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v2 ).obtainMessage ( v3, v1 ); // invoke-virtual {v2, v3, v1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 215 */
/* .local v2, "msg":Landroid/os/Message; */
v3 = this.this$0;
v3 = this.mHandler;
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v3 ).sendMessage ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 219 */
} // .end local v0 # "clipData":Landroid/content/ClipData;
} // .end local v1 # "text":Ljava/lang/CharSequence;
} // .end local v2 # "msg":Landroid/os/Message;
} // :cond_2
return;
} // .end method
