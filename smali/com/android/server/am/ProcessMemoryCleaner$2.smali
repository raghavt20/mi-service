.class Lcom/android/server/am/ProcessMemoryCleaner$2;
.super Landroid/database/ContentObserver;
.source "ProcessMemoryCleaner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/ProcessMemoryCleaner;->registerCloudObserver(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ProcessMemoryCleaner;


# direct methods
.method constructor <init>(Lcom/android/server/am/ProcessMemoryCleaner;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/ProcessMemoryCleaner;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 315
    iput-object p1, p0, Lcom/android/server/am/ProcessMemoryCleaner$2;->this$0:Lcom/android/server/am/ProcessMemoryCleaner;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 1
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 318
    if-eqz p2, :cond_0

    .line 319
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner$2;->this$0:Lcom/android/server/am/ProcessMemoryCleaner;

    invoke-static {v0}, Lcom/android/server/am/ProcessMemoryCleaner;->-$$Nest$mupdateCloudControlData(Lcom/android/server/am/ProcessMemoryCleaner;)V

    .line 322
    :cond_0
    return-void
.end method
