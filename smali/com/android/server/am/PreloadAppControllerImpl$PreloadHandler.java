class com.android.server.am.PreloadAppControllerImpl$PreloadHandler extends android.os.Handler {
	 /* .source "PreloadAppControllerImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/PreloadAppControllerImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "PreloadHandler" */
} // .end annotation
/* # static fields */
static final Integer MSG_ENQUEUE_PRELOAD_APP_REQUEST;
static final Integer MSG_EXECUTE_PRELOAD_APP;
static final Integer MSG_PRELOAD_APP_START;
static final Integer MSG_REGIST_PRELOAD_CALLBACK;
static final Integer MSG_UNREGIST_PRELOAD_CALLBACK;
/* # instance fields */
private android.util.SparseArray mCallBacks;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lmiui/process/IPreloadCallback;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.LinkedList mPendingWork;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/LinkedList<", */
/* "Lcom/android/server/wm/PreloadLifecycle;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
final com.android.server.am.PreloadAppControllerImpl this$0; //synthetic
/* # direct methods */
public com.android.server.am.PreloadAppControllerImpl$PreloadHandler ( ) {
/* .locals 2 */
/* .param p1, "this$0" # Lcom/android/server/am/PreloadAppControllerImpl; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 534 */
this.this$0 = p1;
/* .line 535 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 525 */
/* new-instance v0, Ljava/util/LinkedList; */
/* invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V */
this.mPendingWork = v0;
/* .line 526 */
/* new-instance v0, Landroid/util/SparseArray; */
int v1 = 4; // const/4 v1, 0x4
/* invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V */
this.mCallBacks = v0;
/* .line 536 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 13 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 540 */
/* iget v0, p1, Landroid/os/Message;->what:I */
int v1 = 2; // const/4 v1, 0x2
final String v2 = "PreloadAppControllerImpl"; // const-string v2, "PreloadAppControllerImpl"
/* packed-switch v0, :pswitch_data_0 */
/* goto/16 :goto_5 */
/* .line 596 */
/* :pswitch_0 */
v0 = this.mCallBacks;
v0 = (( android.util.SparseArray ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/SparseArray;->size()I
/* .line 597 */
/* .local v0, "size":I */
v1 = this.obj;
/* check-cast v1, Ljava/lang/String; */
/* .line 598 */
/* .local v1, "packageName":Ljava/lang/String; */
/* iget v3, p1, Landroid/os/Message;->arg1:I */
/* .line 599 */
/* .local v3, "pid":I */
/* iget v4, p1, Landroid/os/Message;->arg2:I */
/* .line 600 */
/* .local v4, "type":I */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "i":I */
} // :goto_0
/* if-ge v5, v0, :cond_7 */
/* .line 601 */
v6 = this.mCallBacks;
v6 = (( android.util.SparseArray ) v6 ).keyAt ( v5 ); // invoke-virtual {v6, v5}, Landroid/util/SparseArray;->keyAt(I)I
/* if-eq v4, v6, :cond_0 */
/* .line 602 */
/* .line 606 */
} // :cond_0
try { // :try_start_0
v6 = this.mCallBacks;
(( android.util.SparseArray ) v6 ).valueAt ( v5 ); // invoke-virtual {v6, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v6, Lmiui/process/IPreloadCallback; */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 611 */
/* .line 607 */
/* :catch_0 */
/* move-exception v6 */
/* .line 608 */
/* .local v6, "e":Landroid/os/RemoteException; */
/* sget-boolean v7, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
if ( v7 != null) { // if-eqz v7, :cond_1
/* .line 609 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "onPreloadAppStarted callback fail, type "; // const-string v8, "onPreloadAppStarted callback fail, type "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v8 = this.mCallBacks;
v8 = (( android.util.SparseArray ) v8 ).keyAt ( v5 ); // invoke-virtual {v8, v5}, Landroid/util/SparseArray;->keyAt(I)I
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v7 );
/* .line 600 */
} // .end local v6 # "e":Landroid/os/RemoteException;
} // :cond_1
} // :goto_1
/* add-int/lit8 v5, v5, 0x1 */
/* .line 593 */
} // .end local v0 # "size":I
} // .end local v1 # "packageName":Ljava/lang/String;
} // .end local v3 # "pid":I
} // .end local v4 # "type":I
} // .end local v5 # "i":I
/* :pswitch_1 */
v0 = this.mCallBacks;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
(( android.util.SparseArray ) v0 ).delete ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/SparseArray;->delete(I)V
/* .line 594 */
/* goto/16 :goto_5 */
/* .line 590 */
/* :pswitch_2 */
v0 = this.mCallBacks;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
v2 = this.obj;
/* check-cast v2, Lmiui/process/IPreloadCallback; */
(( android.util.SparseArray ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 591 */
/* goto/16 :goto_5 */
/* .line 552 */
/* :pswitch_3 */
v0 = this.mPendingWork;
v0 = (( java.util.LinkedList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->size()I
/* if-nez v0, :cond_2 */
/* .line 553 */
return;
/* .line 555 */
} // :cond_2
v0 = this.mPendingWork;
(( java.util.LinkedList ) v0 ).remove ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/wm/PreloadLifecycle; */
/* .line 556 */
/* .local v0, "lifecycle":Lcom/android/server/wm/PreloadLifecycle; */
/* const-wide/16 v3, 0x0 */
/* .line 557 */
/* .local v3, "timeout":J */
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 558 */
v5 = this.this$0;
v5 = com.android.server.am.PreloadAppControllerImpl .-$$Nest$mstartPreloadApp ( v5,v0 );
/* .line 559 */
/* .local v5, "res":I */
v6 = android.app.ActivityManager .isStartResultSuccessful ( v5 );
final String v7 = " res="; // const-string v7, " res="
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 560 */
(( com.android.server.wm.PreloadLifecycle ) v0 ).getPreloadNextTimeout ( ); // invoke-virtual {v0}, Lcom/android/server/wm/PreloadLifecycle;->getPreloadNextTimeout()J
/* move-result-wide v3 */
/* .line 561 */
int v6 = 1; // const/4 v6, 0x1
(( com.android.server.wm.PreloadLifecycle ) v0 ).setAlreadyPreloaded ( v6 ); // invoke-virtual {v0, v6}, Lcom/android/server/wm/PreloadLifecycle;->setAlreadyPreloaded(Z)V
/* .line 562 */
/* sget-boolean v6, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
if ( v6 != null) { // if-eqz v6, :cond_4
/* .line 563 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "preloadApp SUCCESS preload:"; // const-string v8, "preloadApp SUCCESS preload:"
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 564 */
(( com.android.server.wm.PreloadLifecycle ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Lcom/android/server/wm/PreloadLifecycle;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 563 */
android.util.Slog .w ( v2,v6 );
/* .line 567 */
} // :cond_3
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "preloadApp failed preload:"; // const-string v8, "preloadApp failed preload:"
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 568 */
(( com.android.server.wm.PreloadLifecycle ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Lcom/android/server/wm/PreloadLifecycle;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 567 */
android.util.Slog .w ( v2,v6 );
/* .line 569 */
v6 = this.this$0;
(( com.android.server.wm.PreloadLifecycle ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Lcom/android/server/wm/PreloadLifecycle;->getPackageName()Ljava/lang/String;
com.android.server.am.PreloadAppControllerImpl .-$$Nest$mremovePreloadAppInfo ( v6,v7 );
/* .line 572 */
} // :cond_4
} // :goto_2
v6 = (( com.android.server.wm.PreloadLifecycle ) v0 ).getDisplayId ( ); // invoke-virtual {v0}, Lcom/android/server/wm/PreloadLifecycle;->getDisplayId()I
/* .line 573 */
/* .local v6, "displayId":I */
v7 = (( com.android.server.wm.PreloadLifecycle ) v0 ).getPreloadType ( ); // invoke-virtual {v0}, Lcom/android/server/wm/PreloadLifecycle;->getPreloadType()I
/* .line 574 */
/* .local v7, "type":I */
/* shl-int/lit8 v8, v6, 0x8 */
/* or-int/2addr v7, v8 */
/* .line 575 */
v8 = this.mCallBacks;
v8 = (( android.util.SparseArray ) v8 ).size ( ); // invoke-virtual {v8}, Landroid/util/SparseArray;->size()I
/* .line 576 */
/* .local v8, "size":I */
int v9 = 0; // const/4 v9, 0x0
/* .local v9, "i":I */
} // :goto_3
/* if-ge v9, v8, :cond_6 */
/* .line 578 */
try { // :try_start_1
v10 = this.mCallBacks;
(( android.util.SparseArray ) v10 ).valueAt ( v9 ); // invoke-virtual {v10, v9}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v10, Lmiui/process/IPreloadCallback; */
v11 = (( com.android.server.wm.PreloadLifecycle ) v0 ).getPreloadType ( ); // invoke-virtual {v0}, Lcom/android/server/wm/PreloadLifecycle;->getPreloadType()I
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 583 */
/* .line 579 */
/* :catch_1 */
/* move-exception v10 */
/* .line 580 */
/* .local v10, "e":Landroid/os/RemoteException; */
/* sget-boolean v11, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
if ( v11 != null) { // if-eqz v11, :cond_5
/* .line 581 */
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
final String v12 = "complete callback fail, type "; // const-string v12, "complete callback fail, type "
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v12 = this.mCallBacks;
v12 = (( android.util.SparseArray ) v12 ).keyAt ( v9 ); // invoke-virtual {v12, v9}, Landroid/util/SparseArray;->keyAt(I)I
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v11 );
/* .line 576 */
} // .end local v10 # "e":Landroid/os/RemoteException;
} // :cond_5
} // :goto_4
/* add-int/lit8 v9, v9, 0x1 */
/* .line 587 */
} // .end local v5 # "res":I
} // .end local v6 # "displayId":I
} // .end local v7 # "type":I
} // .end local v8 # "size":I
} // .end local v9 # "i":I
} // :cond_6
(( com.android.server.am.PreloadAppControllerImpl$PreloadHandler ) p0 ).sendEmptyMessageDelayed ( v1, v3, v4 ); // invoke-virtual {p0, v1, v3, v4}, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->sendEmptyMessageDelayed(IJ)Z
/* .line 588 */
/* .line 542 */
} // .end local v0 # "lifecycle":Lcom/android/server/wm/PreloadLifecycle;
} // .end local v3 # "timeout":J
/* :pswitch_4 */
v0 = this.obj;
/* check-cast v0, Lcom/android/server/wm/PreloadLifecycle; */
/* .line 543 */
/* .local v0, "preloadLifecycle":Lcom/android/server/wm/PreloadLifecycle; */
v2 = this.mPendingWork;
v2 = (( java.util.LinkedList ) v2 ).contains ( v0 ); // invoke-virtual {v2, v0}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z
/* if-nez v2, :cond_7 */
/* .line 544 */
v2 = this.mPendingWork;
(( java.util.LinkedList ) v2 ).add ( v0 ); // invoke-virtual {v2, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
/* .line 546 */
v2 = (( com.android.server.am.PreloadAppControllerImpl$PreloadHandler ) p0 ).hasMessages ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->hasMessages(I)Z
/* if-nez v2, :cond_7 */
/* .line 547 */
(( com.android.server.am.PreloadAppControllerImpl$PreloadHandler ) p0 ).sendEmptyMessage ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->sendEmptyMessage(I)Z
/* .line 615 */
} // .end local v0 # "preloadLifecycle":Lcom/android/server/wm/PreloadLifecycle;
} // :cond_7
} // :goto_5
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
