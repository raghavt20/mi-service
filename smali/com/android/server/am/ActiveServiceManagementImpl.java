class com.android.server.am.ActiveServiceManagementImpl implements com.android.server.am.ActiveServiceManagementStub {
	 /* .source "ActiveServiceManagementImpl.java" */
	 /* # interfaces */
	 /* # instance fields */
	 java.lang.String activeWallpaperPackageName;
	 /* # direct methods */
	 com.android.server.am.ActiveServiceManagementImpl ( ) {
		 /* .locals 0 */
		 /* .line 14 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Boolean canBindService ( android.content.Context p0, android.content.Intent p1, Integer p2 ) {
		 /* .locals 1 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "service" # Landroid/content/Intent; */
		 /* .param p3, "userId" # I */
		 /* .line 65 */
		 v0 = 		 com.android.server.am.AutoStartManagerServiceStub .getInstance ( );
	 } // .end method
	 public Boolean canRestartServiceLocked ( com.android.server.am.ServiceRecord p0 ) {
		 /* .locals 18 */
		 /* .param p1, "record" # Lcom/android/server/am/ServiceRecord; */
		 /* .line 20 */
		 /* move-object/from16 v0, p1 */
		 v1 = this.appInfo;
		 /* iget v1, v1, Landroid/content/pm/ApplicationInfo;->flags:I */
		 /* and-int/lit8 v1, v1, 0x8 */
		 int v2 = 1; // const/4 v2, 0x1
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 /* .line 21 */
			 /* .line 24 */
		 } // :cond_0
		 v1 = this.appInfo;
		 /* iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I */
		 v1 = 		 android.os.UserHandle .getAppId ( v1 );
		 /* const/16 v3, 0x7d0 */
		 /* if-gt v1, v3, :cond_1 */
		 /* .line 25 */
		 /* .line 27 */
	 } // :cond_1
	 v1 = this.packageName;
	 /* move-object/from16 v3, p0 */
	 v4 = this.activeWallpaperPackageName;
	 v1 = 	 (( java.lang.String ) v1 ).equals ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_2
		 /* .line 28 */
		 /* .line 30 */
	 } // :cond_2
	 com.android.server.am.AutoStartManagerServiceStub .getInstance ( );
	 v5 = this.packageName;
	 v1 = this.appInfo;
	 /* iget v6, v1, Landroid/content/pm/ApplicationInfo;->uid:I */
	 final String v7 = "ActiveServiceManagementImpl#canRestartServiceLocked"; // const-string v7, "ActiveServiceManagementImpl#canRestartServiceLocked"
	 /* .line 33 */
	 /* invoke-virtual/range {p1 ..p1}, Lcom/android/server/am/ServiceRecord;->getComponentName()Landroid/content/ComponentName; */
	 /* iget-boolean v1, v0, Lcom/android/server/am/ServiceRecord;->stopIfKilled:Z */
	 /* .line 30 */
	 /* xor-int/lit8 v9, v1, 0x1 */
	 v1 = 	 /* invoke-interface/range {v4 ..v9}, Lcom/android/server/am/AutoStartManagerServiceStub;->canRestartServiceLocked(Ljava/lang/String;ILjava/lang/String;Landroid/content/ComponentName;Z)Z */
	 int v4 = 0; // const/4 v4, 0x0
	 /* if-nez v1, :cond_3 */
	 /* .line 34 */
	 /* .line 38 */
} // :cond_3
com.miui.whetstone.server.WhetstoneActivityManagerService .getSingletonService ( );
final String v5 = ", UserId: "; // const-string v5, ", UserId: "
final String v6 = " in "; // const-string v6, " in "
final String v7 = "/"; // const-string v7, "/"
final String v8 = "WhetstonePackageState"; // const-string v8, "WhetstonePackageState"
final String v9 = ""; // const-string v9, ""
if ( v1 != null) { // if-eqz v1, :cond_7
	 /* .line 39 */
	 com.miui.whetstone.server.WhetstoneActivityManagerService .getSingletonService ( );
	 v11 = this.packageName;
	 final String v12 = "Restart: AMS"; // const-string v12, "Restart: AMS"
	 /* .line 43 */
	 v14 = 	 android.os.UserHandle .getCallingUserId ( );
	 /* .line 44 */
	 v1 = this.name;
	 if ( v1 != null) { // if-eqz v1, :cond_4
		 v1 = this.name;
		 (( android.content.ComponentName ) v1 ).getClassName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
		 /* move-object v15, v1 */
	 } // :cond_4
	 /* move-object v15, v9 */
} // :goto_0
v1 = this.processName;
/* new-array v13, v2, [Ljava/lang/Object; */
/* .line 46 */
v2 = this.intent;
if ( v2 != null) { // if-eqz v2, :cond_5
	 v2 = this.intent;
	 (( android.content.Intent$FilterComparison ) v2 ).getIntent ( ); // invoke-virtual {v2}, Landroid/content/Intent$FilterComparison;->getIntent()Landroid/content/Intent;
} // :cond_5
int v2 = 0; // const/4 v2, 0x0
} // :goto_1
/* aput-object v2, v13, v4 */
/* .line 39 */
/* move-object/from16 v17, v13 */
int v2 = 2; // const/4 v2, 0x2
/* move v13, v2 */
/* move-object/from16 v16, v1 */
v1 = /* invoke-virtual/range {v10 ..v17}, Lcom/miui/whetstone/server/WhetstoneActivityManagerService;->checkPackageState(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I */
int v2 = 1; // const/4 v2, 0x1
/* if-eq v1, v2, :cond_7 */
/* .line 47 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Permission denied by Whetstone, cannot re-start service from "; // const-string v2, "Permission denied by Whetstone, cannot re-start service from "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.packageName;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 49 */
v2 = this.name;
if ( v2 != null) { // if-eqz v2, :cond_6
v2 = this.name;
(( android.content.ComponentName ) v2 ).getClassName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
} // :cond_6
(( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.processName;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 50 */
v2 = android.os.UserHandle .getCallingUserId ( );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 47 */
android.util.Slog .w ( v8,v1 );
/* .line 51 */
/* .line 55 */
} // :cond_7
/* sget-boolean v1, Lcom/miui/whetstone/WhetstonePackageState;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_9
/* .line 56 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "restart service from "; // const-string v2, "restart service from "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.packageName;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 57 */
v2 = this.name;
if ( v2 != null) { // if-eqz v2, :cond_8
v2 = this.name;
(( android.content.ComponentName ) v2 ).getClassName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
} // :cond_8
(( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.processName;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 58 */
v2 = android.os.UserHandle .getCallingUserId ( );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 56 */
android.util.Slog .d ( v8,v1 );
/* .line 60 */
} // :cond_9
int v1 = 1; // const/4 v1, 0x1
} // .end method
public void updateWallPaperPackageName ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 69 */
this.activeWallpaperPackageName = p1;
/* .line 70 */
return;
} // .end method
