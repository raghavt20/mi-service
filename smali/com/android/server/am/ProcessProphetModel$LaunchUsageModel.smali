.class public final Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;
.super Ljava/lang/Object;
.source "ProcessProphetModel.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ProcessProphetModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LaunchUsageModel"
.end annotation


# instance fields
.field private dataList:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue<",
            "Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;",
            ">;"
        }
    .end annotation
.end field

.field launchUsageToday:Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;

.field private mLastLaunchTimeStamp:J

.field modelPredictData:[D


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 801
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 803
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->dataList:Ljava/util/Queue;

    .line 807
    const/4 v0, 0x6

    new-array v0, v0, [D

    iput-object v0, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->modelPredictData:[D

    .line 810
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->mLastLaunchTimeStamp:J

    return-void
.end method

.method private updateModelPredict(Ljava/lang/String;Ljava/util/Date;Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;)V
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "launchTime"    # Ljava/util/Date;
    .param p3, "mLUProbTab"    # Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;

    .line 899
    if-eqz p3, :cond_3

    .line 901
    invoke-virtual {p3}, Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;->getCurProbTab()Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->getPkgPos(Ljava/lang/String;)I

    move-result v0

    .line 902
    .local v0, "pos":I
    const/4 v1, -0x1

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-le v0, v1, :cond_0

    goto :goto_0

    .line 905
    :cond_0
    if-ltz v0, :cond_2

    if-gt v0, v1, :cond_2

    .line 907
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->modelPredictData:[D

    aget-wide v4, v1, v0

    add-double/2addr v4, v2

    aput-wide v4, v1, v0

    goto :goto_1

    .line 904
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->modelPredictData:[D

    const/4 v4, 0x5

    aget-wide v5, v1, v4

    add-double/2addr v5, v2

    aput-wide v5, v1, v4

    .line 909
    :cond_2
    :goto_1
    invoke-static {}, Lcom/android/server/am/ProcessProphetModel;->-$$Nest$sfgetDEBUG()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 910
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "update Model Predict "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", time="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ProcessProphetModel"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 913
    .end local v0    # "pos":I
    :cond_3
    return-void
.end method


# virtual methods
.method public conclude()Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;
    .locals 8

    .line 862
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 865
    .local v0, "concludeTimeStamp":J
    new-instance v2, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;

    const-wide/16 v3, 0x0

    invoke-direct {v2, v3, v4}, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;-><init>(J)V

    .line 866
    .local v2, "daySum":Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;
    new-instance v5, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;

    invoke-direct {v5, v3, v4}, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;-><init>(J)V

    move-object v3, v5

    .line 867
    .local v3, "weekendSum":Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;
    iget-object v4, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->dataList:Ljava/util/Queue;

    invoke-interface {v4}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;

    .line 868
    .local v5, "usage":Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;
    iget-boolean v6, v5, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;->isWeekend:Z

    if-eqz v6, :cond_0

    .line 869
    invoke-virtual {v3, v5}, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;->combine(Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;)V

    goto :goto_1

    .line 871
    :cond_0
    invoke-virtual {v2, v5}, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;->combine(Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;)V

    .line 873
    .end local v5    # "usage":Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;
    :goto_1
    goto :goto_0

    .line 876
    :cond_1
    new-instance v4, Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;

    invoke-direct {v4, v2, v3}, Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;-><init>(Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;)V

    .line 877
    .local v4, "probTabResult":Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "conclude consumed "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v0

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "ms"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "ProcessProphetModel"

    invoke-static {v6, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 878
    return-object v4
.end method

.method public dump()V
    .locals 2

    .line 854
    const-string v0, "ProcessProphetModel"

    const-string v1, "dumping Launch Usage Model"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 855
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->dataList:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;

    .line 856
    .local v1, "l":Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;
    invoke-virtual {v1}, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;->dump()V

    .line 857
    .end local v1    # "l":Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;
    goto :goto_0

    .line 858
    :cond_0
    return-void
.end method

.method public getAllDays()J
    .locals 2

    .line 883
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->dataList:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getAllLUUpdateTimes()J
    .locals 6

    .line 888
    const-wide/16 v0, 0x0

    .line 889
    .local v0, "allLaunchUpdateTimes":J
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->dataList:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;

    .line 890
    .local v3, "lid":Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;
    iget-wide v4, v3, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;->launchUpdateTimes:J

    add-long/2addr v0, v4

    .line 891
    .end local v3    # "lid":Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;
    goto :goto_0

    .line 892
    :cond_0
    return-wide v0
.end method

.method public getTopModelPredict()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .line 918
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 919
    .local v0, "uploadModPredList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Double;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->modelPredictData:[D

    array-length v3, v2

    if-ge v1, v3, :cond_0

    .line 920
    aget-wide v2, v2, v1

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 919
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 923
    .end local v1    # "i":I
    :cond_0
    const-wide/16 v3, 0x0

    invoke-static {v2, v3, v4}, Ljava/util/Arrays;->fill([DD)V

    .line 925
    return-object v0
.end method

.method public update(Ljava/lang/String;JLcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;)V
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "launchTimeStamp"    # J
    .param p4, "mLUProbTab"    # Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;

    .line 816
    iget-wide v0, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->mLastLaunchTimeStamp:J

    cmp-long v0, p2, v0

    if-gez v0, :cond_0

    .line 817
    return-void

    .line 821
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 822
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0, p2, p3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 824
    invoke-static {}, Lcom/android/server/am/ProcessProphetModel;->-$$Nest$sfgetDEBUG()Z

    move-result v1

    const-string v2, "ProcessProphetModel"

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "update launch "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", time="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 827
    :cond_1
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-direct {p0, p1, v1, p4}, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->updateModelPredict(Ljava/lang/String;Ljava/util/Date;Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;)V

    .line 829
    const/16 v1, 0xb

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 830
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 831
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 832
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 833
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->launchUsageToday:Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    iget-wide v5, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->mLastLaunchTimeStamp:J

    cmp-long v1, v3, v5

    if-lez v1, :cond_6

    .line 835
    :cond_2
    invoke-static {}, Lcom/android/server/am/ProcessProphetModel;->-$$Nest$sfgetDEBUG()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string/jumbo v1, "update a new day"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 837
    :cond_3
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->dataList:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->size()I

    move-result v1

    const/16 v3, 0x8

    if-lt v1, v3, :cond_4

    .line 838
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->dataList:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    .line 840
    :cond_4
    new-instance v1, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-direct {v1, v3, v4}, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;-><init>(J)V

    iput-object v1, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->launchUsageToday:Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;

    .line 841
    invoke-static {v0}, Lcom/android/server/am/ProcessProphetModel;->-$$Nest$smisWeekend(Ljava/util/Calendar;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 842
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->launchUsageToday:Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;

    const/4 v3, 0x1

    iput-boolean v3, v1, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;->isWeekend:Z

    .line 843
    invoke-static {}, Lcom/android/server/am/ProcessProphetModel;->-$$Nest$sfgetDEBUG()Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "Today is the weekend."

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 845
    :cond_5
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->dataList:Ljava/util/Queue;

    iget-object v2, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->launchUsageToday:Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;

    invoke-interface {v1, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 849
    :cond_6
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->launchUsageToday:Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;

    invoke-virtual {v1, p1, p2, p3}, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;->update(Ljava/lang/String;J)V

    .line 850
    iput-wide p2, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->mLastLaunchTimeStamp:J

    .line 851
    return-void
.end method
