.class public Lcom/android/server/am/AutoStartManagerServiceStubImpl;
.super Ljava/lang/Object;
.source "AutoStartManagerServiceStubImpl.java"

# interfaces
.implements Lcom/android/server/am/AutoStartManagerServiceStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;
    }
.end annotation


# static fields
.field private static final ENABLE_SIGSTOP_KILL:Z

.field private static final TAG:Ljava/lang/String; = "AutoStartManagerServiceStubImpl"

.field private static startServiceWhiteList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mSecurityInternal:Lmiui/security/SecurityManagerInternal;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 48
    nop

    .line 49
    const-string v0, "persist.proc.enable_sigstop"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/am/AutoStartManagerServiceStubImpl;->ENABLE_SIGSTOP_KILL:Z

    .line 51
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/server/am/AutoStartManagerServiceStubImpl;->startServiceWhiteList:Ljava/util/HashMap;

    .line 54
    const-string v1, "com.miui.huanji"

    const-string v2, "com.miui.huanji.parsebak.RestoreXSpaceService"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static isAllowStartServiceByUid(Landroid/content/Context;Landroid/content/Intent;I)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "service"    # Landroid/content/Intent;
    .param p2, "uid"    # I

    .line 300
    const-string v0, "appops"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AppOpsManager;

    .line 301
    .local v0, "aom":Landroid/app/AppOpsManager;
    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 302
    return v1

    .line 304
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x2718

    invoke-virtual {v0, v3, p2, v2}, Landroid/app/AppOpsManager;->checkOpNoThrow(IILjava/lang/String;)I

    move-result v2

    .line 305
    .local v2, "mode":I
    if-nez v2, :cond_1

    .line 306
    return v1

    .line 308
    :cond_1
    const/4 v1, 0x0

    return v1
.end method

.method private searchAllUncoverdProcsPid(Ljava/util/List;)Ljava/util/HashMap;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;",
            ">;)",
            "Ljava/util/HashMap<",
            "Ljava/lang/Integer;",
            "Landroid/util/Pair<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 132
    .local p1, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 133
    .local v0, "resPids":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/String;>;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;

    .line 134
    .local v2, "p":Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;
    new-instance v3, Landroid/util/Pair;

    iget v4, v2, Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;->uid:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-object v5, v2, Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;->processName:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 135
    .local v3, "procInfo":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/String;>;"
    iget v4, v2, Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;->uid:I

    iget v5, v2, Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;->pid:I

    invoke-static {v4, v5}, Lcom/android/server/am/AutoStartManagerServiceStubImpl;->searchNativeProc(II)Ljava/util/List;

    move-result-object v4

    .line 136
    .local v4, "natiiveProcs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    .line 137
    .local v6, "nPid":Ljava/lang/Integer;
    invoke-virtual {v0, v6, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    .end local v6    # "nPid":Ljava/lang/Integer;
    goto :goto_1

    .line 139
    .end local v2    # "p":Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;
    .end local v3    # "procInfo":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/String;>;"
    .end local v4    # "natiiveProcs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_0
    goto :goto_0

    .line 140
    :cond_1
    return-object v0
.end method

.method public static searchNativeProc(II)Ljava/util/List;
    .locals 6
    .param p0, "uid"    # I
    .param p1, "pid"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 144
    const/4 v0, 0x0

    .line 145
    .local v0, "reader":Ljava/io/BufferedReader;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 147
    .local v1, "nativeProcs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :try_start_0
    invoke-static {p0, p1}, Lmiui/process/ProcessManagerNative;->getCgroupFilePath(II)Ljava/lang/String;

    move-result-object v2

    .line 148
    .local v2, "fileName":Ljava/lang/String;
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v5}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object v0, v3

    .line 149
    const/4 v3, 0x0

    .line 150
    .local v3, "line":Ljava/lang/String;
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    if-eqz v4, :cond_1

    .line 151
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 152
    .local v4, "proc":I
    if-lez v4, :cond_0

    if-eq p1, v4, :cond_0

    .line 153
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155
    .end local v4    # "proc":I
    :cond_0
    goto :goto_0

    .line 150
    .end local v2    # "fileName":Ljava/lang/String;
    .end local v3    # "line":Ljava/lang/String;
    :cond_1
    goto :goto_2

    .line 160
    :catchall_0
    move-exception v2

    goto :goto_1

    .line 157
    :catch_0
    move-exception v2

    .line 158
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local v2    # "e":Ljava/lang/Exception;
    goto :goto_2

    .line 160
    :goto_1
    invoke-static {v0}, Lmiui/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 161
    throw v2

    .line 156
    :catch_1
    move-exception v2

    .line 160
    :goto_2
    invoke-static {v0}, Lmiui/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 161
    nop

    .line 162
    return-object v1
.end method

.method private trackAppBehavior(ILjava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "behavior"    # I
    .param p2, "pkg"    # Ljava/lang/String;
    .param p3, "info"    # Ljava/lang/String;

    .line 179
    iget-object v0, p0, Lcom/android/server/am/AutoStartManagerServiceStubImpl;->mSecurityInternal:Lmiui/security/SecurityManagerInternal;

    if-nez v0, :cond_0

    .line 180
    const-class v0, Lmiui/security/SecurityManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/security/SecurityManagerInternal;

    iput-object v0, p0, Lcom/android/server/am/AutoStartManagerServiceStubImpl;->mSecurityInternal:Lmiui/security/SecurityManagerInternal;

    .line 182
    :cond_0
    iget-object v1, p0, Lcom/android/server/am/AutoStartManagerServiceStubImpl;->mSecurityInternal:Lmiui/security/SecurityManagerInternal;

    if-eqz v1, :cond_1

    .line 183
    const-wide/16 v4, 0x1

    move v2, p1

    move-object v3, p2

    move-object v6, p3

    invoke-virtual/range {v1 .. v6}, Lmiui/security/SecurityManagerInternal;->recordAppBehaviorAsync(ILjava/lang/String;JLjava/lang/String;)V

    .line 185
    :cond_1
    return-void
.end method


# virtual methods
.method public canRestartServiceLocked(Ljava/lang/String;ILjava/lang/String;)Z
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "message"    # Ljava/lang/String;

    .line 257
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/android/server/am/AutoStartManagerServiceStubImpl;->canRestartServiceLocked(Ljava/lang/String;ILjava/lang/String;Landroid/content/ComponentName;Z)Z

    move-result v0

    return v0
.end method

.method public canRestartServiceLocked(Ljava/lang/String;ILjava/lang/String;Landroid/content/ComponentName;Z)Z
    .locals 8
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "message"    # Ljava/lang/String;
    .param p4, "component"    # Landroid/content/ComponentName;
    .param p5, "isNote"    # Z

    .line 262
    const-class v0, Lcom/miui/app/AppOpsServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/app/AppOpsServiceInternal;

    invoke-interface {v0, p1}, Lcom/miui/app/AppOpsServiceInternal;->isTestSuitSpecialIgnore(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 263
    return v1

    .line 265
    :cond_0
    if-eqz p4, :cond_1

    sget-object v0, Lcom/android/server/am/AutoStartManagerServiceStubImpl;->startServiceWhiteList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 266
    sget-object v0, Lcom/android/server/am/AutoStartManagerServiceStubImpl;->startServiceWhiteList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p4}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 267
    return v1

    .line 274
    :cond_1
    invoke-static {}, Landroid/app/ActivityThread;->currentApplication()Landroid/app/Application;

    move-result-object v0

    const-class v2, Landroid/app/AppOpsManager;

    invoke-virtual {v0, v2}, Landroid/app/Application;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AppOpsManager;

    .line 276
    .local v0, "appOpsManager":Landroid/app/AppOpsManager;
    if-eqz p5, :cond_2

    .line 277
    const/16 v3, 0x2718

    const/4 v6, 0x0

    move-object v2, v0

    move v4, p2

    move-object v5, p1

    move-object v7, p3

    invoke-virtual/range {v2 .. v7}, Landroid/app/AppOpsManager;->noteOpNoThrow(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 278
    .local v2, "mode":I
    if-eqz p4, :cond_3

    .line 279
    nop

    .line 280
    invoke-virtual {p4}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v3

    .line 279
    const/4 v4, 0x6

    invoke-direct {p0, v4, p1, v3}, Lcom/android/server/am/AutoStartManagerServiceStubImpl;->trackAppBehavior(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 283
    .end local v2    # "mode":I
    :cond_2
    const/16 v2, 0x2718

    invoke-virtual {v0, v2, p2, p1}, Landroid/app/AppOpsManager;->checkOpNoThrow(IILjava/lang/String;)I

    move-result v2

    .line 285
    .restart local v2    # "mode":I
    :cond_3
    :goto_0
    if-nez v2, :cond_4

    .line 286
    return v1

    .line 289
    :cond_4
    invoke-static {p1}, Lcom/miui/server/process/ProcessManagerInternal;->checkCtsProcess(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 290
    return v1

    .line 293
    :cond_5
    if-eqz p5, :cond_6

    if-eqz p4, :cond_6

    .line 294
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MIUILOG- Reject RestartService service :"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " uid : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "AutoStartManagerServiceStubImpl"

    invoke-static {v3, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    :cond_6
    const/4 v1, 0x0

    return v1
.end method

.method public isAllowStartService(Landroid/content/Context;Landroid/content/Intent;I)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "service"    # Landroid/content/Intent;
    .param p3, "userId"    # I

    .line 189
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 191
    .local v1, "packageName":Ljava/lang/String;
    invoke-static {}, Lcom/android/server/notification/BarrageListenerServiceStub;->getInstance()Lcom/android/server/notification/BarrageListenerServiceStub;

    move-result-object v2

    .line 192
    invoke-interface {v2, v1}, Lcom/android/server/notification/BarrageListenerServiceStub;->isAllowStartBarrage(Ljava/lang/String;)Z

    move-result v2

    .line 193
    .local v2, "isAllow":Z
    if-nez v2, :cond_0

    .line 194
    const-string v3, "AutoStartManagerServiceStubImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MiBarrage: Disallowing to start service {"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "}"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    const/4 v0, 0x0

    return v0

    .line 198
    :cond_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v3

    .line 199
    .local v3, "packageManager":Landroid/content/pm/IPackageManager;
    const-wide/16 v4, 0x0

    invoke-interface {v3, v1, v4, v5, p3}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;JI)Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    .line 200
    .local v4, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    if-nez v4, :cond_1

    .line 201
    return v0

    .line 203
    :cond_1
    iget v5, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 204
    .local v5, "uid":I
    invoke-virtual {p0, p1, p2, p3, v5}, Lcom/android/server/am/AutoStartManagerServiceStubImpl;->isAllowStartService(Landroid/content/Context;Landroid/content/Intent;II)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 205
    .end local v1    # "packageName":Ljava/lang/String;
    .end local v2    # "isAllow":Z
    .end local v3    # "packageManager":Landroid/content/pm/IPackageManager;
    .end local v4    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    .end local v5    # "uid":I
    :catch_0
    move-exception v1

    .line 206
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 208
    .end local v1    # "e":Ljava/lang/Exception;
    return v0
.end method

.method public isAllowStartService(Landroid/content/Context;Landroid/content/Intent;II)Z
    .locals 16
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "service"    # Landroid/content/Intent;
    .param p3, "userId"    # I
    .param p4, "uid"    # I

    .line 213
    move-object/from16 v1, p1

    move/from16 v8, p4

    const/4 v9, 0x1

    :try_start_0
    const-string v0, "appops"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AppOpsManager;

    .line 214
    .local v0, "aom":Landroid/app/AppOpsManager;
    if-nez v0, :cond_0

    .line 215
    return v9

    .line 217
    :cond_0
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/ActivityManagerService;

    move-object v10, v2

    .line 218
    .local v10, "ams":Lcom/android/server/am/ActivityManagerService;
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v2

    const/4 v4, 0x0

    const-wide/16 v5, 0x400

    move-object/from16 v3, p2

    move/from16 v7, p3

    invoke-interface/range {v2 .. v7}, Landroid/content/pm/IPackageManager;->resolveService(Landroid/content/Intent;Ljava/lang/String;JI)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    move-object v11, v2

    .line 220
    .local v11, "rInfo":Landroid/content/pm/ResolveInfo;
    if-eqz v11, :cond_1

    iget-object v2, v11, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    move-object v12, v2

    .line 221
    .local v12, "sInfo":Landroid/content/pm/ServiceInfo;
    if-nez v12, :cond_2

    .line 222
    return v9

    .line 224
    :cond_2
    iget-object v2, v11, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v2, v2, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/2addr v2, v9

    if-nez v2, :cond_6

    iget-object v2, v11, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v2, v2, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 225
    const/4 v13, 0x0

    invoke-static {v1, v2, v13}, Lmiui/content/pm/PreloadedAppPolicy;->isProtectedDataApp(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    move-object/from16 v5, p0

    move-object/from16 v6, p2

    move/from16 v7, p3

    goto/16 :goto_1

    .line 229
    :cond_3
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    move-object v14, v2

    .line 230
    .local v14, "packageName":Ljava/lang/String;
    iget-object v2, v10, Lcom/android/server/am/ActivityManagerService;->mActivityTaskManager:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v3, v12, Landroid/content/pm/ServiceInfo;->processName:Ljava/lang/String;

    invoke-static {v2, v14, v3, v8}, Lcom/android/server/wm/WindowProcessUtils;->isPackageRunning(Lcom/android/server/wm/ActivityTaskManagerService;Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v2

    move v15, v2

    .line 232
    .local v15, "isRunning":Z
    if-nez v15, :cond_5

    .line 237
    const/16 v3, 0x2718

    const/4 v6, 0x0

    const-string v7, "AutoStartManagerServiceStubImpl#isAllowStartService"

    move-object v2, v0

    move/from16 v4, p4

    move-object v5, v14

    invoke-virtual/range {v2 .. v7}, Landroid/app/AppOpsManager;->noteOpNoThrow(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 240
    .local v2, "mode":I
    nop

    .line 241
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    .line 240
    const/4 v4, 0x6

    move-object/from16 v5, p0

    :try_start_1
    invoke-direct {v5, v4, v14, v3}, Lcom/android/server/am/AutoStartManagerServiceStubImpl;->trackAppBehavior(ILjava/lang/String;Ljava/lang/String;)V

    .line 242
    if-nez v2, :cond_4

    .line 243
    return v9

    .line 245
    :cond_4
    const-string v3, "AutoStartManagerServiceStubImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MIUILOG- Reject service :"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-object/from16 v6, p2

    :try_start_2
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " userId : "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move/from16 v7, p3

    :try_start_3
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v9, " uid : "

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 246
    return v13

    .line 249
    .end local v0    # "aom":Landroid/app/AppOpsManager;
    .end local v2    # "mode":I
    .end local v10    # "ams":Lcom/android/server/am/ActivityManagerService;
    .end local v11    # "rInfo":Landroid/content/pm/ResolveInfo;
    .end local v12    # "sInfo":Landroid/content/pm/ServiceInfo;
    .end local v14    # "packageName":Ljava/lang/String;
    .end local v15    # "isRunning":Z
    :catch_0
    move-exception v0

    goto :goto_4

    :catch_1
    move-exception v0

    goto :goto_3

    :catch_2
    move-exception v0

    goto :goto_2

    .line 232
    .restart local v0    # "aom":Landroid/app/AppOpsManager;
    .restart local v10    # "ams":Lcom/android/server/am/ActivityManagerService;
    .restart local v11    # "rInfo":Landroid/content/pm/ResolveInfo;
    .restart local v12    # "sInfo":Landroid/content/pm/ServiceInfo;
    .restart local v14    # "packageName":Ljava/lang/String;
    .restart local v15    # "isRunning":Z
    :cond_5
    move-object/from16 v5, p0

    move-object/from16 v6, p2

    move/from16 v7, p3

    .line 251
    .end local v0    # "aom":Landroid/app/AppOpsManager;
    .end local v10    # "ams":Lcom/android/server/am/ActivityManagerService;
    .end local v11    # "rInfo":Landroid/content/pm/ResolveInfo;
    .end local v12    # "sInfo":Landroid/content/pm/ServiceInfo;
    .end local v14    # "packageName":Ljava/lang/String;
    .end local v15    # "isRunning":Z
    goto :goto_5

    .line 224
    .restart local v0    # "aom":Landroid/app/AppOpsManager;
    .restart local v10    # "ams":Lcom/android/server/am/ActivityManagerService;
    .restart local v11    # "rInfo":Landroid/content/pm/ResolveInfo;
    .restart local v12    # "sInfo":Landroid/content/pm/ServiceInfo;
    :cond_6
    move-object/from16 v5, p0

    move-object/from16 v6, p2

    move/from16 v7, p3

    .line 227
    :goto_1
    const/4 v2, 0x1

    return v2

    .line 249
    .end local v0    # "aom":Landroid/app/AppOpsManager;
    .end local v10    # "ams":Lcom/android/server/am/ActivityManagerService;
    .end local v11    # "rInfo":Landroid/content/pm/ResolveInfo;
    .end local v12    # "sInfo":Landroid/content/pm/ServiceInfo;
    :catch_3
    move-exception v0

    move-object/from16 v5, p0

    :goto_2
    move-object/from16 v6, p2

    :goto_3
    move/from16 v7, p3

    .line 250
    .local v0, "e":Ljava/lang/Exception;
    :goto_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 252
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_5
    const/4 v2, 0x1

    return v2
.end method

.method sendSignalToProcessLocked(Ljava/util/List;Ljava/lang/String;IZ)V
    .locals 10
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "signal"    # I
    .param p4, "needKillAgain"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;",
            ">;",
            "Ljava/lang/String;",
            "IZ)V"
        }
    .end annotation

    .line 89
    .local p1, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;>;"
    invoke-direct {p0, p1}, Lcom/android/server/am/AutoStartManagerServiceStubImpl;->searchAllUncoverdProcsPid(Ljava/util/List;)Ljava/util/HashMap;

    move-result-object v0

    .line 95
    .local v0, "allUncoverdPids":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/String;>;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const-string v3, " s: "

    const-string v4, "AutoStartManagerServiceStubImpl"

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;

    .line 96
    .local v2, "proc":Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "prepare force stop p:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v2, Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;->pid:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    iget v3, v2, Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;->pid:I

    invoke-static {v3, p3}, Landroid/os/Process;->sendSignal(II)V

    .line 98
    iget v3, v2, Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;->pid:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    .end local v2    # "proc":Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;
    goto :goto_0

    .line 104
    :cond_0
    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 105
    .local v2, "pid":Ljava/lang/Integer;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "prepare force stop native p:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5, p3}, Landroid/os/Process;->sendSignal(II)V

    .line 107
    if-nez p4, :cond_1

    .line 108
    nop

    .line 109
    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/util/Pair;

    iget-object v5, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Ljava/lang/String;

    .line 108
    const/16 v6, 0x22

    invoke-direct {p0, v6, p2, v5}, Lcom/android/server/am/AutoStartManagerServiceStubImpl;->trackAppBehavior(ILjava/lang/String;Ljava/lang/String;)V

    .line 111
    .end local v2    # "pid":Ljava/lang/Integer;
    :cond_1
    goto :goto_1

    .line 113
    :cond_2
    if-eqz p4, :cond_4

    .line 114
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;

    .line 115
    .local v2, "p":Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;
    iget v3, v2, Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;->pid:I

    .local v3, "pid":I
    iget v4, v2, Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;->uid:I

    .line 116
    .local v4, "uid":I
    if-lez v3, :cond_3

    int-to-long v5, v4

    int-to-long v7, v3

    invoke-static {v5, v6, v7, v8}, Lcom/android/server/am/ProcessUtils;->isDiedProcess(JJ)Z

    move-result v5

    if-nez v5, :cond_3

    .line 117
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iget-object v7, v2, Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;->processName:Ljava/lang/String;

    .line 118
    const/16 v8, 0x3e9

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const-string v9, "Kill Again"

    filled-new-array {v5, v6, v7, v8, v9}, [Ljava/lang/Object;

    move-result-object v5

    .line 117
    const/16 v6, 0x7547

    invoke-static {v6, v5}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    .line 119
    invoke-static {v3}, Landroid/os/Process;->killProcessQuiet(I)V

    .line 120
    invoke-static {v4, v3}, Landroid/os/Process;->killProcessGroup(II)I

    .line 122
    .end local v2    # "p":Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;
    .end local v3    # "pid":I
    .end local v4    # "uid":I
    :cond_3
    goto :goto_2

    .line 124
    :cond_4
    return-void
.end method

.method public signalStopProcessesLocked(Ljava/util/ArrayList;ZLjava/lang/String;I)V
    .locals 6
    .param p2, "allowRestart"    # Z
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "uid"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroid/util/Pair<",
            "Lcom/android/server/am/ProcessRecord;",
            "Ljava/lang/Boolean;",
            ">;>;Z",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .line 63
    .local p1, "procs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/util/Pair<Lcom/android/server/am/ProcessRecord;Ljava/lang/Boolean;>;>;"
    sget-boolean v0, Lcom/android/server/am/AutoStartManagerServiceStubImpl;->ENABLE_SIGSTOP_KILL:Z

    if-nez v0, :cond_0

    return-void

    .line 65
    :cond_0
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/ActivityManagerService;

    .line 66
    .local v0, "ams":Lcom/android/server/am/ActivityManagerService;
    if-eqz p2, :cond_1

    const-string v1, "AutoStartManagerServiceStubImpl#signalStopProcessesLocked"

    invoke-virtual {p0, p3, p4, v1}, Lcom/android/server/am/AutoStartManagerServiceStubImpl;->canRestartServiceLocked(Ljava/lang/String;ILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 68
    return-void

    .line 70
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 71
    .local v1, "tmpProcs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    .line 72
    .local v3, "proc":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/android/server/am/ProcessRecord;Ljava/lang/Boolean;>;"
    new-instance v4, Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;

    iget-object v5, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Lcom/android/server/am/ProcessRecord;

    invoke-direct {v4, v5}, Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;-><init>(Lcom/android/server/am/ProcessRecord;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 73
    .end local v3    # "proc":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/android/server/am/ProcessRecord;Ljava/lang/Boolean;>;"
    goto :goto_0

    .line 75
    :cond_2
    const/16 v2, 0x13

    const/4 v3, 0x0

    invoke-virtual {p0, v1, p3, v2, v3}, Lcom/android/server/am/AutoStartManagerServiceStubImpl;->sendSignalToProcessLocked(Ljava/util/List;Ljava/lang/String;IZ)V

    .line 79
    iget-object v2, v0, Lcom/android/server/am/ActivityManagerService;->mHandler:Lcom/android/server/am/ActivityManagerService$MainHandler;

    new-instance v3, Lcom/android/server/am/AutoStartManagerServiceStubImpl$1;

    invoke-direct {v3, p0, v1, p3}, Lcom/android/server/am/AutoStartManagerServiceStubImpl$1;-><init>(Lcom/android/server/am/AutoStartManagerServiceStubImpl;Ljava/util/ArrayList;Ljava/lang/String;)V

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v3, v4, v5}, Lcom/android/server/am/ActivityManagerService$MainHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 85
    return-void
.end method
