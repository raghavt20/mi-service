.class Lcom/android/server/am/ProcessProphetImpl$2;
.super Ljava/lang/Object;
.source "ProcessProphetImpl.java"

# interfaces
.implements Landroid/content/ClipboardManager$OnPrimaryClipChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/ProcessProphetImpl;->handlePostInit()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ProcessProphetImpl;

.field final synthetic val$clipboardManager:Landroid/content/ClipboardManager;


# direct methods
.method constructor <init>(Lcom/android/server/am/ProcessProphetImpl;Landroid/content/ClipboardManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/ProcessProphetImpl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 203
    iput-object p1, p0, Lcom/android/server/am/ProcessProphetImpl$2;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    iput-object p2, p0, Lcom/android/server/am/ProcessProphetImpl$2;->val$clipboardManager:Landroid/content/ClipboardManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrimaryClipChanged()V
    .locals 4

    .line 206
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl$2;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetImpl;->isEnable()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 207
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl$2;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    invoke-static {v2}, Lcom/android/server/am/ProcessProphetImpl;->-$$Nest$fgetmLastCopyUpTime(Lcom/android/server/am/ProcessProphetImpl;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x7d0

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    return-void

    .line 208
    :cond_1
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl$2;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/android/server/am/ProcessProphetImpl;->-$$Nest$fputmLastCopyUpTime(Lcom/android/server/am/ProcessProphetImpl;J)V

    .line 209
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl$2;->val$clipboardManager:Landroid/content/ClipboardManager;

    invoke-virtual {v0}, Landroid/content/ClipboardManager;->hasPrimaryClip()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 210
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl$2;->val$clipboardManager:Landroid/content/ClipboardManager;

    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v0

    .line 211
    .local v0, "clipData":Landroid/content/ClipData;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/content/ClipData;->getItemCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 212
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 213
    .local v1, "text":Ljava/lang/CharSequence;
    if-eqz v1, :cond_2

    .line 214
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl$2;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    iget-object v2, v2, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    const/4 v3, 0x6

    invoke-virtual {v2, v3, v1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 215
    .local v2, "msg":Landroid/os/Message;
    iget-object v3, p0, Lcom/android/server/am/ProcessProphetImpl$2;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    iget-object v3, v3, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    invoke-virtual {v3, v2}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z

    .line 219
    .end local v0    # "clipData":Landroid/content/ClipData;
    .end local v1    # "text":Ljava/lang/CharSequence;
    .end local v2    # "msg":Landroid/os/Message;
    :cond_2
    return-void
.end method
