public class com.android.server.am.MiuiWarningDialog extends com.android.server.am.BaseErrorDialog {
	 /* .source "MiuiWarningDialog.java" */
	 /* # static fields */
	 private static final Integer BUTTON_CANCEL;
	 private static final Integer BUTTON_OK;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private com.android.server.am.MiuiWarnings$WarningCallback mCallback;
	 private final android.os.Handler mHandler;
	 /* # direct methods */
	 static com.android.server.am.MiuiWarnings$WarningCallback -$$Nest$fgetmCallback ( com.android.server.am.MiuiWarningDialog p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.mCallback;
	 } // .end method
	 public com.android.server.am.MiuiWarningDialog ( ) {
		 /* .locals 6 */
		 /* .param p1, "packageLabel" # Ljava/lang/String; */
		 /* .param p2, "context" # Landroid/content/Context; */
		 /* .param p3, "callback" # Lcom/android/server/am/MiuiWarnings$WarningCallback; */
		 /* .line 24 */
		 /* invoke-direct {p0, p2}, Lcom/android/server/am/BaseErrorDialog;-><init>(Landroid/content/Context;)V */
		 /* .line 57 */
		 /* new-instance v0, Lcom/android/server/am/MiuiWarningDialog$1; */
		 /* invoke-direct {v0, p0}, Lcom/android/server/am/MiuiWarningDialog$1;-><init>(Lcom/android/server/am/MiuiWarningDialog;)V */
		 this.mHandler = v0;
		 /* .line 25 */
		 this.mCallback = p3;
		 /* .line 26 */
		 (( android.content.Context ) p2 ).getResources ( ); // invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
		 /* .line 27 */
		 /* .local v1, "res":Landroid/content/res/Resources; */
		 android.text.BidiFormatter .getInstance ( );
		 /* .line 29 */
		 /* .local v2, "bidi":Landroid/text/BidiFormatter; */
		 int v3 = 0; // const/4 v3, 0x0
		 (( com.android.server.am.MiuiWarningDialog ) p0 ).setCancelable ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/am/MiuiWarningDialog;->setCancelable(Z)V
		 /* .line 30 */
		 /* nop */
		 /* .line 31 */
		 (( android.text.BidiFormatter ) v2 ).unicodeWrap ( p1 ); // invoke-virtual {v2, p1}, Landroid/text/BidiFormatter;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;
		 /* filled-new-array {v3}, [Ljava/lang/Object; */
		 /* .line 30 */
		 /* const v4, 0x110f02a8 */
		 (( android.content.res.Resources ) v1 ).getString ( v4, v3 ); // invoke-virtual {v1, v4, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;
		 (( com.android.server.am.MiuiWarningDialog ) p0 ).setTitle ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/am/MiuiWarningDialog;->setTitle(Ljava/lang/CharSequence;)V
		 /* .line 32 */
		 /* nop */
		 /* .line 33 */
		 /* const v3, 0x110f02a7 */
		 (( android.content.res.Resources ) v1 ).getText ( v3 ); // invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;
		 /* .line 34 */
		 int v4 = 1; // const/4 v4, 0x1
		 (( android.os.Handler ) v0 ).obtainMessage ( v4 ); // invoke-virtual {v0, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
		 /* .line 32 */
		 int v5 = -1; // const/4 v5, -0x1
		 (( com.android.server.am.MiuiWarningDialog ) p0 ).setButton ( v5, v3, v4 ); // invoke-virtual {p0, v5, v3, v4}, Lcom/android/server/am/MiuiWarningDialog;->setButton(ILjava/lang/CharSequence;Landroid/os/Message;)V
		 /* .line 35 */
		 /* nop */
		 /* .line 36 */
		 /* const v3, 0x110f02a6 */
		 (( android.content.res.Resources ) v1 ).getText ( v3 ); // invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;
		 /* .line 37 */
		 int v4 = 2; // const/4 v4, 0x2
		 (( android.os.Handler ) v0 ).obtainMessage ( v4 ); // invoke-virtual {v0, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
		 /* .line 35 */
		 int v4 = -2; // const/4 v4, -0x2
		 (( com.android.server.am.MiuiWarningDialog ) p0 ).setButton ( v4, v3, v0 ); // invoke-virtual {p0, v4, v3, v0}, Lcom/android/server/am/MiuiWarningDialog;->setButton(ILjava/lang/CharSequence;Landroid/os/Message;)V
		 /* .line 39 */
		 (( com.android.server.am.MiuiWarningDialog ) p0 ).getWindow ( ); // invoke-virtual {p0}, Lcom/android/server/am/MiuiWarningDialog;->getWindow()Landroid/view/Window;
		 /* const/16 v3, 0x7da */
		 (( android.view.Window ) v0 ).setType ( v3 ); // invoke-virtual {v0, v3}, Landroid/view/Window;->setType(I)V
		 /* .line 40 */
		 (( com.android.server.am.MiuiWarningDialog ) p0 ).getWindow ( ); // invoke-virtual {p0}, Lcom/android/server/am/MiuiWarningDialog;->getWindow()Landroid/view/Window;
		 (( android.view.Window ) v0 ).getAttributes ( ); // invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;
		 /* .line 41 */
		 /* .local v0, "attrs":Landroid/view/WindowManager$LayoutParams; */
		 /* new-instance v3, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v4 = "MiuiWarning:"; // const-string v4, "MiuiWarning:"
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 (( android.view.WindowManager$LayoutParams ) v0 ).setTitle ( v3 ); // invoke-virtual {v0, v3}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V
		 /* .line 42 */
		 /* const/16 v3, 0x110 */
		 /* iput v3, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I */
		 /* .line 44 */
		 (( com.android.server.am.MiuiWarningDialog ) p0 ).getWindow ( ); // invoke-virtual {p0}, Lcom/android/server/am/MiuiWarningDialog;->getWindow()Landroid/view/Window;
		 (( android.view.Window ) v3 ).setAttributes ( v0 ); // invoke-virtual {v3, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
		 /* .line 45 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 protected void onCreate ( android.os.Bundle p0 ) {
		 /* .locals 4 */
		 /* .param p1, "savedInstanceState" # Landroid/os/Bundle; */
		 /* .line 49 */
		 /* invoke-super {p0, p1}, Lcom/android/server/am/BaseErrorDialog;->onCreate(Landroid/os/Bundle;)V */
		 /* .line 50 */
		 (( com.android.server.am.MiuiWarningDialog ) p0 ).getContext ( ); // invoke-virtual {p0}, Lcom/android/server/am/MiuiWarningDialog;->getContext()Landroid/content/Context;
		 (( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
		 final String v1 = "id"; // const-string v1, "id"
		 final String v2 = "miuix.stub"; // const-string v2, "miuix.stub"
		 final String v3 = "alertTitle"; // const-string v3, "alertTitle"
		 v0 = 		 (( android.content.res.Resources ) v0 ).getIdentifier ( v3, v1, v2 ); // invoke-virtual {v0, v3, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
		 /* .line 51 */
		 /* .local v0, "id":I */
		 (( com.android.server.am.MiuiWarningDialog ) p0 ).findViewById ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/am/MiuiWarningDialog;->findViewById(I)Landroid/view/View;
		 /* check-cast v1, Landroid/widget/TextView; */
		 /* .line 52 */
		 /* .local v1, "titleView":Landroid/widget/TextView; */
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 /* .line 53 */
			 int v2 = 0; // const/4 v2, 0x0
			 (( android.widget.TextView ) v1 ).setSingleLine ( v2 ); // invoke-virtual {v1, v2}, Landroid/widget/TextView;->setSingleLine(Z)V
			 /* .line 55 */
		 } // :cond_0
		 return;
	 } // .end method
