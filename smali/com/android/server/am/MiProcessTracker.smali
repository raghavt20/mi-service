.class public Lcom/android/server/am/MiProcessTracker;
.super Ljava/lang/Object;
.source "MiProcessTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/MiProcessTracker$AppStartRecordController;,
        Lcom/android/server/am/MiProcessTracker$ProcessRecordController;
    }
.end annotation


# static fields
.field private static final COMPACTION:I = 0x8

.field private static final CONT_LOW_WMARK:I = 0x3

.field private static final CRITICAL_KILL:I = 0x1

.field public static final DEBUG:Z

.field private static final DIRECT_RECL_AND_LOW_MEM:I = 0xa

.field private static final DIRECT_RECL_AND_THRASHING:I = 0x5

.field private static final DIRECT_RECL_AND_THROT:I = 0x9

.field private static final LMKD_COMPACTION:Ljava/lang/String; = "lmkdCompaction"

.field private static final LMKD_CONT_LOW_WMARK:Ljava/lang/String; = "lmkdContLowLwmark"

.field private static final LMKD_CRITICAL_KILL:Ljava/lang/String; = "lmkdCriticalKill"

.field private static final LMKD_DIRECT_RECL_AND_LOW_MEM:Ljava/lang/String; = "lmkdDirectReclAndLowMem"

.field private static final LMKD_DIRECT_RECL_AND_THRASHING:Ljava/lang/String; = "lmkdDirectReclAndThrashing"

.field private static final LMKD_DIRECT_RECL_AND_THROT:Ljava/lang/String; = "lmkdDirectReclAndThrot"

.field private static final LMKD_KILL_REASON_CAMERA:Ljava/lang/String; = "cameraKill"

.field private static final LMKD_KILL_REASON_UNKNOWN:Ljava/lang/String; = "lmkdUnknown"

.field private static final LMKD_LOW_FILECACHE_AFTER_THRASHING:Ljava/lang/String; = "lmkdLowFileCacheAfterThrashing"

.field private static final LMKD_LOW_MEM_AND_SWAP:Ljava/lang/String; = "lmkdLowMemAndSwap"

.field private static final LMKD_LOW_MEM_AND_SWAP_UTIL:Ljava/lang/String; = "lmkdLowMemAndSwapUtil"

.field private static final LMKD_LOW_MEM_AND_THRASHING:Ljava/lang/String; = "lmkdLowMemAndThrashing"

.field private static final LMKD_LOW_SWAP_AND_LOW_FILE:Ljava/lang/String; = "lmkdLowSwapAndLowFile"

.field private static final LMKD_LOW_SWAP_AND_THRASHING:Ljava/lang/String; = "lmkdLowSwapAndThrashing"

.field private static final LMKD_PERCEPTIBLE_PROTECT:Ljava/lang/String; = "lmkdPerceptibleProtect"

.field private static final LMKD_PRESSURE_AFTER_KILL:Ljava/lang/String; = "lmkdPressureAfterKill"

.field private static final LMKD_WILL_THROTTLED:Ljava/lang/String; = "lmkdWillThrottled"

.field private static final LOW_FILECACHE_AFTER_THRASHING:I = 0x7

.field private static final LOW_MEM_AND_SWAP:I = 0x3

.field private static final LOW_MEM_AND_SWAP_UTIL:I = 0x6

.field private static final LOW_MEM_AND_THRASHING:I = 0x4

.field private static final LOW_SWAP_AND_LOW_FILE:I = 0xb

.field private static final LOW_SWAP_AND_THRASHING:I = 0x2

.field private static final MINFREE_MODE:I = 0xd

.field private static final MI_NEW_KILL_REASON_BASE:I = 0x32

.field private static final MI_UPDATE_KILL_REASON_BASE:I = 0xc8

.field private static final PERCEPTIBLE_PROTECT:I = 0xc

.field private static final PRESSURE_AFTER_KILL:I = 0x0

.field public static final TAG:Ljava/lang/String; = "MiProcessTracker"

.field private static final WILL_THROTTLED:I = 0x4

.field public static sEnable:Z

.field public static sInstance:Lcom/android/server/am/MiProcessTracker;

.field public static trackerPkgWhiteList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAppStartRecordController:Lcom/android/server/am/MiProcessTracker$AppStartRecordController;

.field private mPerfShielder:Lcom/android/internal/app/IPerfShielder;

.field private final mProcessRecordController:Lcom/android/server/am/MiProcessTracker$ProcessRecordController;

.field private mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

.field private mSystemReady:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetmPerfShielder(Lcom/android/server/am/MiProcessTracker;)Lcom/android/internal/app/IPerfShielder;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/MiProcessTracker;->mPerfShielder:Lcom/android/internal/app/IPerfShielder;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmProcessRecordController(Lcom/android/server/am/MiProcessTracker;)Lcom/android/server/am/MiProcessTracker$ProcessRecordController;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/MiProcessTracker;->mProcessRecordController:Lcom/android/server/am/MiProcessTracker$ProcessRecordController;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSmartPowerService(Lcom/android/server/am/MiProcessTracker;)Lcom/miui/app/smartpower/SmartPowerServiceInternal;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/MiProcessTracker;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 28
    nop

    .line 29
    const-string v0, "persist.sys.process.tracker.enable"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/am/MiProcessTracker;->DEBUG:Z

    .line 31
    sget-boolean v0, Landroid/os/spc/PressureStateSettings;->PROCESS_TRACKER_ENABLE:Z

    sput-boolean v0, Lcom/android/server/am/MiProcessTracker;->sEnable:Z

    .line 33
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/server/am/MiProcessTracker;->trackerPkgWhiteList:Ljava/util/HashSet;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/am/MiProcessTracker;->mSystemReady:Z

    .line 118
    new-instance v0, Lcom/android/server/am/MiProcessTracker$AppStartRecordController;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/am/MiProcessTracker$AppStartRecordController;-><init>(Lcom/android/server/am/MiProcessTracker;Lcom/android/server/am/MiProcessTracker$AppStartRecordController-IA;)V

    iput-object v0, p0, Lcom/android/server/am/MiProcessTracker;->mAppStartRecordController:Lcom/android/server/am/MiProcessTracker$AppStartRecordController;

    .line 119
    new-instance v0, Lcom/android/server/am/MiProcessTracker$ProcessRecordController;

    invoke-direct {v0, p0, v1}, Lcom/android/server/am/MiProcessTracker$ProcessRecordController;-><init>(Lcom/android/server/am/MiProcessTracker;Lcom/android/server/am/MiProcessTracker$ProcessRecordController-IA;)V

    iput-object v0, p0, Lcom/android/server/am/MiProcessTracker;->mProcessRecordController:Lcom/android/server/am/MiProcessTracker$ProcessRecordController;

    .line 120
    return-void
.end method

.method public static getInstance()Lcom/android/server/am/MiProcessTracker;
    .locals 1

    .line 123
    sget-object v0, Lcom/android/server/am/MiProcessTracker;->sInstance:Lcom/android/server/am/MiProcessTracker;

    if-nez v0, :cond_0

    .line 124
    new-instance v0, Lcom/android/server/am/MiProcessTracker;

    invoke-direct {v0}, Lcom/android/server/am/MiProcessTracker;-><init>()V

    sput-object v0, Lcom/android/server/am/MiProcessTracker;->sInstance:Lcom/android/server/am/MiProcessTracker;

    .line 126
    :cond_0
    sget-object v0, Lcom/android/server/am/MiProcessTracker;->sInstance:Lcom/android/server/am/MiProcessTracker;

    return-object v0
.end method

.method private getLmkdKillReason(I)Ljava/lang/String;
    .locals 2
    .param p1, "reason"    # I

    .line 372
    const/16 v0, 0xfa

    const-string v1, "lmkdUnknown"

    if-le p1, v0, :cond_0

    .line 373
    add-int/lit16 v0, p1, -0xfa

    packed-switch v0, :pswitch_data_0

    .line 379
    return-object v1

    .line 377
    :pswitch_0
    const-string v0, "lmkdWillThrottled"

    return-object v0

    .line 375
    :pswitch_1
    const-string v0, "lmkdContLowLwmark"

    return-object v0

    .line 381
    :cond_0
    const/16 v0, 0xc8

    if-le p1, v0, :cond_1

    .line 382
    add-int/lit16 p1, p1, -0xc8

    .line 384
    :cond_1
    packed-switch p1, :pswitch_data_1

    .line 414
    return-object v1

    .line 412
    :pswitch_2
    const-string v0, "cameraKill"

    return-object v0

    .line 410
    :pswitch_3
    const-string v0, "lmkdPerceptibleProtect"

    return-object v0

    .line 408
    :pswitch_4
    const-string v0, "lmkdLowSwapAndLowFile"

    return-object v0

    .line 406
    :pswitch_5
    const-string v0, "lmkdDirectReclAndLowMem"

    return-object v0

    .line 404
    :pswitch_6
    const-string v0, "lmkdDirectReclAndThrot"

    return-object v0

    .line 402
    :pswitch_7
    const-string v0, "lmkdCompaction"

    return-object v0

    .line 400
    :pswitch_8
    const-string v0, "lmkdLowFileCacheAfterThrashing"

    return-object v0

    .line 398
    :pswitch_9
    const-string v0, "lmkdLowMemAndSwapUtil"

    return-object v0

    .line 396
    :pswitch_a
    const-string v0, "lmkdDirectReclAndThrashing"

    return-object v0

    .line 394
    :pswitch_b
    const-string v0, "lmkdLowMemAndThrashing"

    return-object v0

    .line 392
    :pswitch_c
    const-string v0, "lmkdLowMemAndSwap"

    return-object v0

    .line 390
    :pswitch_d
    const-string v0, "lmkdLowSwapAndThrashing"

    return-object v0

    .line 388
    :pswitch_e
    const-string v0, "lmkdCriticalKill"

    return-object v0

    .line 386
    :pswitch_f
    const-string v0, "lmkdPressureAfterKill"

    return-object v0

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public onActivityLaunched(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 1
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "uid"    # I
    .param p4, "launchState"    # I

    .line 149
    iget-boolean v0, p0, Lcom/android/server/am/MiProcessTracker;->mSystemReady:Z

    if-nez v0, :cond_0

    .line 150
    return-void

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/MiProcessTracker;->mAppStartRecordController:Lcom/android/server/am/MiProcessTracker$AppStartRecordController;

    invoke-static {v0, p1, p2, p3, p4}, Lcom/android/server/am/MiProcessTracker$AppStartRecordController;->-$$Nest$monActivityLaunched(Lcom/android/server/am/MiProcessTracker$AppStartRecordController;Ljava/lang/String;Ljava/lang/String;II)V

    .line 153
    return-void
.end method

.method public recordAmKillProcess(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
    .locals 18
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "reason"    # Ljava/lang/String;

    .line 156
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iget-boolean v2, v0, Lcom/android/server/am/MiProcessTracker;->mSystemReady:Z

    if-nez v2, :cond_0

    .line 157
    return-void

    .line 159
    :cond_0
    if-eqz v1, :cond_1

    .line 160
    iget-object v2, v1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 161
    .local v2, "processName":Ljava/lang/String;
    iget-object v3, v1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v13, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 162
    .local v13, "packageName":Ljava/lang/String;
    iget-object v3, v1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget v14, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 163
    .local v14, "uid":I
    iget-object v3, v1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v3}, Lcom/android/server/am/ProcessStateRecord;->getSetAdj()I

    move-result v15

    .line 164
    .local v15, "adj":I
    iget-object v3, v1, Lcom/android/server/am/ProcessRecord;->mProfile:Lcom/android/server/am/ProcessProfileRecord;

    invoke-virtual {v3}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J

    move-result-wide v16

    .line 165
    .local v16, "pss":J
    iget-object v3, v0, Lcom/android/server/am/MiProcessTracker;->mProcessRecordController:Lcom/android/server/am/MiProcessTracker$ProcessRecordController;

    const-wide/16 v11, 0x0

    move-object v4, v13

    move-object v5, v2

    move v6, v14

    move-object/from16 v7, p2

    move v8, v15

    move-wide/from16 v9, v16

    invoke-static/range {v3 .. v12}, Lcom/android/server/am/MiProcessTracker$ProcessRecordController;->-$$Nest$mrecordKillProcessIfNeed(Lcom/android/server/am/MiProcessTracker$ProcessRecordController;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IJJ)V

    .line 168
    .end local v2    # "processName":Ljava/lang/String;
    .end local v13    # "packageName":Ljava/lang/String;
    .end local v14    # "uid":I
    .end local v15    # "adj":I
    .end local v16    # "pss":J
    :cond_1
    return-void
.end method

.method public recordLmkKillProcess(IIJIIIILjava/lang/String;)V
    .locals 17
    .param p1, "uid"    # I
    .param p2, "oomScore"    # I
    .param p3, "rssInBytes"    # J
    .param p5, "freeMemKb"    # I
    .param p6, "freeSwapKb"    # I
    .param p7, "killReason"    # I
    .param p8, "thrashing"    # I
    .param p9, "processName"    # Ljava/lang/String;

    .line 173
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/server/am/MiProcessTracker;->mSystemReady:Z

    if-nez v1, :cond_0

    .line 174
    return-void

    .line 176
    :cond_0
    iget-object v1, v0, Lcom/android/server/am/MiProcessTracker;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    .line 177
    move/from16 v12, p1

    move-object/from16 v13, p9

    invoke-interface {v1, v12, v13}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->getRunningProcess(ILjava/lang/String;)Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    move-result-object v1

    .line 178
    .local v1, "runningProcess":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    if-eqz v1, :cond_1

    .line 179
    invoke-virtual {v1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v14

    .line 180
    .local v14, "packageName":Ljava/lang/String;
    move/from16 v15, p7

    invoke-direct {v0, v15}, Lcom/android/server/am/MiProcessTracker;->getLmkdKillReason(I)Ljava/lang/String;

    move-result-object v16

    .line 181
    .local v16, "reason":Ljava/lang/String;
    iget-object v2, v0, Lcom/android/server/am/MiProcessTracker;->mProcessRecordController:Lcom/android/server/am/MiProcessTracker$ProcessRecordController;

    const-wide/16 v8, 0x0

    const-wide/16 v3, 0x400

    div-long v10, p3, v3

    move-object v3, v14

    move-object/from16 v4, p9

    move/from16 v5, p1

    move-object/from16 v6, v16

    move/from16 v7, p2

    invoke-static/range {v2 .. v11}, Lcom/android/server/am/MiProcessTracker$ProcessRecordController;->-$$Nest$mrecordKillProcessIfNeed(Lcom/android/server/am/MiProcessTracker$ProcessRecordController;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IJJ)V

    goto :goto_0

    .line 178
    .end local v14    # "packageName":Ljava/lang/String;
    .end local v16    # "reason":Ljava/lang/String;
    :cond_1
    move/from16 v15, p7

    .line 184
    :goto_0
    return-void
.end method

.method public systemReady(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 130
    sget-boolean v0, Lcom/android/server/am/MiProcessTracker;->sEnable:Z

    if-nez v0, :cond_0

    .line 131
    return-void

    .line 133
    :cond_0
    invoke-static {}, Lcom/miui/daemon/performance/PerfShielderManager;->getService()Lcom/android/internal/app/IPerfShielder;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/MiProcessTracker;->mPerfShielder:Lcom/android/internal/app/IPerfShielder;

    .line 134
    const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    iput-object v0, p0, Lcom/android/server/am/MiProcessTracker;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    .line 135
    sget-object v0, Lcom/android/server/am/MiProcessTracker;->trackerPkgWhiteList:Ljava/util/HashSet;

    .line 136
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x110300ad

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 135
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 137
    sget-object v0, Lcom/android/server/am/MiProcessTracker;->trackerPkgWhiteList:Ljava/util/HashSet;

    .line 138
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x110300bb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 137
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 139
    sget-object v0, Lcom/android/server/am/MiProcessTracker;->trackerPkgWhiteList:Ljava/util/HashSet;

    .line 140
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x110300ac

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 139
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 141
    sget-object v0, Lcom/android/server/am/MiProcessTracker;->trackerPkgWhiteList:Ljava/util/HashSet;

    .line 142
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x110300ba

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 141
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 144
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/am/MiProcessTracker;->mSystemReady:Z

    .line 145
    return-void
.end method
