.class final Lcom/android/server/am/ProcessRecordImpl$AppPss;
.super Ljava/lang/Object;
.source "ProcessRecordImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ProcessRecordImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "AppPss"
.end annotation


# static fields
.field static final MODEL:Ljava/lang/String; = "model"

.field static final PACKAGE_NAME:Ljava/lang/String; = "packageName"

.field static final TOTAL_PSS:Ljava/lang/String; = "totalPss"

.field static final USER_ID:Ljava/lang/String; = "userId"

.field static final VERSION_NAME:Ljava/lang/String; = "versionName"


# instance fields
.field pkn:Ljava/lang/String;

.field pss:Ljava/lang/String;

.field user:Ljava/lang/String;

.field version:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;JLjava/lang/String;I)V
    .locals 1
    .param p1, "pkn"    # Ljava/lang/String;
    .param p2, "pss"    # J
    .param p4, "version"    # Ljava/lang/String;
    .param p5, "user"    # I

    .line 186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 187
    iput-object p1, p0, Lcom/android/server/am/ProcessRecordImpl$AppPss;->pkn:Ljava/lang/String;

    .line 188
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/ProcessRecordImpl$AppPss;->pss:Ljava/lang/String;

    .line 189
    iput-object p4, p0, Lcom/android/server/am/ProcessRecordImpl$AppPss;->version:Ljava/lang/String;

    .line 190
    invoke-static {p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/ProcessRecordImpl$AppPss;->user:Ljava/lang/String;

    .line 191
    return-void
.end method
