.class public Lcom/android/server/am/ScoutMemoryError;
.super Ljava/lang/Object;
.source "ScoutMemoryError.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ScoutMemoryError"

.field private static memoryError:Lcom/android/server/am/ScoutMemoryError;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mService:Lcom/android/server/am/ActivityManagerService;


# direct methods
.method public static synthetic $r8$lambda$8n2G-gTUp88IzYCZkG2H6HqO4Tw(Lcom/android/server/am/ScoutMemoryError;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/am/ScoutMemoryError;->lambda$showAppDisplayMemoryErrorDialog$1(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V

    return-void
.end method

.method public static synthetic $r8$lambda$g-8vQobdIi3vZa77d5NvdrS2wSM(Lcom/android/server/am/ScoutMemoryError;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/am/ScoutMemoryError;->lambda$showAppMemoryErrorDialog$0(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/android/server/am/ScoutMemoryError;
    .locals 1

    .line 22
    sget-object v0, Lcom/android/server/am/ScoutMemoryError;->memoryError:Lcom/android/server/am/ScoutMemoryError;

    if-nez v0, :cond_0

    .line 23
    new-instance v0, Lcom/android/server/am/ScoutMemoryError;

    invoke-direct {v0}, Lcom/android/server/am/ScoutMemoryError;-><init>()V

    sput-object v0, Lcom/android/server/am/ScoutMemoryError;->memoryError:Lcom/android/server/am/ScoutMemoryError;

    .line 25
    :cond_0
    sget-object v0, Lcom/android/server/am/ScoutMemoryError;->memoryError:Lcom/android/server/am/ScoutMemoryError;

    return-object v0
.end method

.method private getPackageLabelLocked(Lcom/android/server/am/ProcessRecord;)Ljava/lang/String;
    .locals 3
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 34
    const/4 v0, 0x0

    .line 35
    .local v0, "label":Ljava/lang/String;
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPkgList()Lcom/android/server/am/PackageList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/am/PackageList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 36
    iget-object v1, p0, Lcom/android/server/am/ScoutMemoryError;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 37
    .local v1, "labelChar":Ljava/lang/CharSequence;
    if-eqz v1, :cond_0

    .line 38
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 41
    .end local v1    # "labelChar":Ljava/lang/CharSequence;
    :cond_0
    return-object v0
.end method

.method private synthetic lambda$showAppDisplayMemoryErrorDialog$1(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V
    .locals 0
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "packageLabel"    # Ljava/lang/String;
    .param p3, "reason"    # Ljava/lang/String;
    .param p4, "errorInfo"    # Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;

    .line 61
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/android/server/am/ScoutMemoryError;->showDisplayMemoryErrorDialog(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V

    return-void
.end method

.method private synthetic lambda$showAppMemoryErrorDialog$0(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "packageLabel"    # Ljava/lang/String;
    .param p3, "reason"    # Ljava/lang/String;

    .line 50
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/server/am/ScoutMemoryError;->showMemoryErrorDialog(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public init(Lcom/android/server/am/ActivityManagerService;Landroid/content/Context;)V
    .locals 0
    .param p1, "ams"    # Lcom/android/server/am/ActivityManagerService;
    .param p2, "context"    # Landroid/content/Context;

    .line 29
    iput-object p1, p0, Lcom/android/server/am/ScoutMemoryError;->mService:Lcom/android/server/am/ActivityManagerService;

    .line 30
    iput-object p2, p0, Lcom/android/server/am/ScoutMemoryError;->mContext:Landroid/content/Context;

    .line 31
    return-void
.end method

.method public scheduleCrashApp(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)Z
    .locals 4
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "reason"    # Ljava/lang/String;

    .line 115
    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 116
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v1

    .line 117
    .local v1, "thread":Landroid/app/IApplicationThread;
    if-eqz v1, :cond_0

    .line 118
    iget-object v2, p0, Lcom/android/server/am/ScoutMemoryError;->mService:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v2

    .line 119
    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p1, p2, v0, v3}, Lcom/android/server/am/ProcessRecord;->scheduleCrashLocked(Ljava/lang/String;ILandroid/os/Bundle;)V

    .line 120
    monitor-exit v2

    const/4 v0, 0x1

    return v0

    .line 121
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 124
    .end local v1    # "thread":Landroid/app/IApplicationThread;
    :cond_0
    return v0
.end method

.method public showAppDisplayMemoryErrorDialog(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)Z
    .locals 9
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "reason"    # Ljava/lang/String;
    .param p3, "errorInfo"    # Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;

    .line 56
    invoke-direct {p0, p1}, Lcom/android/server/am/ScoutMemoryError;->getPackageLabelLocked(Lcom/android/server/am/ProcessRecord;)Ljava/lang/String;

    move-result-object v6

    .line 57
    .local v6, "packageLabel":Ljava/lang/String;
    if-nez v6, :cond_0

    .line 58
    const/4 v0, 0x0

    return v0

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ScoutMemoryError;->mService:Lcom/android/server/am/ActivityManagerService;

    iget-object v7, v0, Lcom/android/server/am/ActivityManagerService;->mUiHandler:Landroid/os/Handler;

    new-instance v8, Lcom/android/server/am/ScoutMemoryError$$ExternalSyntheticLambda0;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, v6

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/server/am/ScoutMemoryError$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/am/ScoutMemoryError;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V

    invoke-virtual {v7, v8}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    .line 62
    const/4 v0, 0x1

    return v0
.end method

.method public showAppMemoryErrorDialog(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)Z
    .locals 3
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "reason"    # Ljava/lang/String;

    .line 45
    invoke-direct {p0, p1}, Lcom/android/server/am/ScoutMemoryError;->getPackageLabelLocked(Lcom/android/server/am/ProcessRecord;)Ljava/lang/String;

    move-result-object v0

    .line 46
    .local v0, "packageLabel":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 47
    const/4 v1, 0x0

    return v1

    .line 49
    :cond_0
    iget-object v1, p0, Lcom/android/server/am/ScoutMemoryError;->mService:Lcom/android/server/am/ActivityManagerService;

    iget-object v1, v1, Lcom/android/server/am/ActivityManagerService;->mUiHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/server/am/ScoutMemoryError$$ExternalSyntheticLambda1;

    invoke-direct {v2, p0, p1, v0, p2}, Lcom/android/server/am/ScoutMemoryError$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/am/ScoutMemoryError;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    .line 51
    const/4 v1, 0x1

    return v1
.end method

.method public showDisplayMemoryErrorDialog(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V
    .locals 3
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "packageLabel"    # Ljava/lang/String;
    .param p3, "reason"    # Ljava/lang/String;
    .param p4, "errorInfo"    # Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;

    .line 87
    const/4 v0, 0x0

    .line 89
    .local v0, "showDialogSuccess":Z
    invoke-static {}, Lcom/android/server/am/MiuiWarnings;->getInstance()Lcom/android/server/am/MiuiWarnings;

    move-result-object v1

    new-instance v2, Lcom/android/server/am/ScoutMemoryError$2;

    invoke-direct {v2, p0, p1, p3, p4}, Lcom/android/server/am/ScoutMemoryError$2;-><init>(Lcom/android/server/am/ScoutMemoryError;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V

    invoke-virtual {v1, p2, v2}, Lcom/android/server/am/MiuiWarnings;->showWarningDialog(Ljava/lang/String;Lcom/android/server/am/MiuiWarnings$WarningCallback;)Z

    move-result v0

    .line 107
    if-eqz v0, :cond_0

    .line 108
    invoke-static {}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->getInstance()Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->setShowDialogState(Z)V

    goto :goto_0

    .line 110
    :cond_0
    const-string v1, "ScoutMemoryError"

    const-string v2, "occur memory leak, showWarningDialog fail"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    :goto_0
    return-void
.end method

.method public showMemoryErrorDialog(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "packageLabel"    # Ljava/lang/String;
    .param p3, "reason"    # Ljava/lang/String;

    .line 66
    const/4 v0, 0x0

    .line 68
    .local v0, "showDialogSuccess":Z
    invoke-static {}, Lcom/android/server/am/MiuiWarnings;->getInstance()Lcom/android/server/am/MiuiWarnings;

    move-result-object v1

    new-instance v2, Lcom/android/server/am/ScoutMemoryError$1;

    invoke-direct {v2, p0, p1, p3}, Lcom/android/server/am/ScoutMemoryError$1;-><init>(Lcom/android/server/am/ScoutMemoryError;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V

    invoke-virtual {v1, p2, v2}, Lcom/android/server/am/MiuiWarnings;->showWarningDialog(Ljava/lang/String;Lcom/android/server/am/MiuiWarnings$WarningCallback;)Z

    move-result v0

    .line 80
    if-nez v0, :cond_0

    .line 81
    const-string v1, "ScoutMemoryError"

    const-string v2, "occur memory leak, showWarningDialog fail"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    :cond_0
    return-void
.end method
