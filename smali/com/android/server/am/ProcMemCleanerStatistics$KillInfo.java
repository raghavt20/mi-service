class com.android.server.am.ProcMemCleanerStatistics$KillInfo {
	 /* .source "ProcMemCleanerStatistics.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/ProcMemCleanerStatistics; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "KillInfo" */
} // .end annotation
/* # instance fields */
private java.lang.String killReason;
private Integer killType;
private java.lang.String name;
private Integer pid;
private Integer priority;
private Long pss;
final com.android.server.am.ProcMemCleanerStatistics this$0; //synthetic
private Long time;
private Integer uid;
/* # direct methods */
static Long -$$Nest$fgettime ( com.android.server.am.ProcMemCleanerStatistics$KillInfo p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->time:J */
/* return-wide v0 */
} // .end method
static void -$$Nest$fputkillReason ( com.android.server.am.ProcMemCleanerStatistics$KillInfo p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.killReason = p1;
return;
} // .end method
static void -$$Nest$fputkillType ( com.android.server.am.ProcMemCleanerStatistics$KillInfo p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->killType:I */
return;
} // .end method
static void -$$Nest$fputname ( com.android.server.am.ProcMemCleanerStatistics$KillInfo p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.name = p1;
return;
} // .end method
static void -$$Nest$fputpid ( com.android.server.am.ProcMemCleanerStatistics$KillInfo p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->pid:I */
return;
} // .end method
static void -$$Nest$fputpriority ( com.android.server.am.ProcMemCleanerStatistics$KillInfo p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->priority:I */
return;
} // .end method
static void -$$Nest$fputpss ( com.android.server.am.ProcMemCleanerStatistics$KillInfo p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-wide p1, p0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->pss:J */
return;
} // .end method
static void -$$Nest$fputtime ( com.android.server.am.ProcMemCleanerStatistics$KillInfo p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-wide p1, p0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->time:J */
return;
} // .end method
static void -$$Nest$fputuid ( com.android.server.am.ProcMemCleanerStatistics$KillInfo p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->uid:I */
return;
} // .end method
private com.android.server.am.ProcMemCleanerStatistics$KillInfo ( ) {
/* .locals 0 */
/* .line 133 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
 com.android.server.am.ProcMemCleanerStatistics$KillInfo ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;-><init>(Lcom/android/server/am/ProcMemCleanerStatistics;)V */
return;
} // .end method
/* # virtual methods */
public java.lang.String getType ( ) {
/* .locals 1 */
/* .line 151 */
/* iget v0, p0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->killType:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 157 */
final String v0 = "Unknown"; // const-string v0, "Unknown"
/* .line 155 */
/* :pswitch_0 */
final String v0 = "ForceStop"; // const-string v0, "ForceStop"
/* .line 153 */
/* :pswitch_1 */
final String v0 = "KillProc"; // const-string v0, "KillProc"
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public java.lang.String toString ( ) {
/* .locals 8 */
/* .line 146 */
/* nop */
/* .line 147 */
(( com.android.server.am.ProcMemCleanerStatistics$KillInfo ) p0 ).getType ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->getType()Ljava/lang/String;
v1 = this.name;
/* iget v2, p0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->pid:I */
java.lang.Integer .valueOf ( v2 );
/* iget v3, p0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->uid:I */
java.lang.Integer .valueOf ( v3 );
/* iget-wide v4, p0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->time:J */
java.lang.Long .valueOf ( v4,v5 );
/* iget v5, p0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->priority:I */
java.lang.Integer .valueOf ( v5 );
/* iget-wide v6, p0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->pss:J */
java.lang.Long .valueOf ( v6,v7 );
v7 = this.killReason;
/* filled-new-array/range {v0 ..v7}, [Ljava/lang/Object; */
/* .line 146 */
final String v1 = "#%s n:%s(%d) u:%d t:%d pri:%s pss:%d r:%s"; // const-string v1, "#%s n:%s(%d) u:%d t:%d pri:%s pss:%d r:%s"
java.lang.String .format ( v1,v0 );
} // .end method
