.class Lcom/android/server/am/SystemPressureController$ScreenStateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SystemPressureController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/SystemPressureController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ScreenStateReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/SystemPressureController;


# direct methods
.method constructor <init>(Lcom/android/server/am/SystemPressureController;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/SystemPressureController;

    .line 729
    iput-object p1, p0, Lcom/android/server/am/SystemPressureController$ScreenStateReceiver;->this$0:Lcom/android/server/am/SystemPressureController;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 732
    const/4 v0, 0x0

    .line 733
    .local v0, "screenOn":Z
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string v2, "android.intent.action.SCREEN_ON"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_1

    :sswitch_1
    const-string v2, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_1

    :goto_0
    const/4 v1, -0x1

    :goto_1
    packed-switch v1, :pswitch_data_0

    .line 739
    const/4 v0, 0x0

    goto :goto_2

    .line 735
    :pswitch_0
    const/4 v0, 0x1

    .line 736
    nop

    .line 742
    :goto_2
    iget-object v1, p0, Lcom/android/server/am/SystemPressureController$ScreenStateReceiver;->this$0:Lcom/android/server/am/SystemPressureController;

    invoke-static {v1}, Lcom/android/server/am/SystemPressureController;->-$$Nest$fgetmHandler(Lcom/android/server/am/SystemPressureController;)Lcom/android/server/am/SystemPressureController$H;

    move-result-object v1

    const/4 v2, 0x4

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 743
    .local v1, "msg":Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 744
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x7ed8ea7f -> :sswitch_1
        -0x56ac2893 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
