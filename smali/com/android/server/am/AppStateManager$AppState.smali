.class public Lcom/android/server/am/AppStateManager$AppState;
.super Lcom/miui/server/smartpower/IAppState;
.source "AppStateManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/AppStateManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AppState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/AppStateManager$AppState$MyHandler;,
        Lcom/android/server/am/AppStateManager$AppState$RunningProcess;,
        Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;
    }
.end annotation


# static fields
.field private static final MSG_STEP_APP_STATE:I = 0x1

.field private static final MSG_UPDATE_APPSTATE:I = 0x2


# instance fields
.field private mAppState:I

.field private mAppSwitchFgCount:I

.field private mForeground:Z

.field private mFreeFormCount:I

.field private mHandler:Lcom/android/server/am/AppStateManager$AppState$MyHandler;

.field private mHasFgService:Z

.field private mHasProtectProcess:Z

.field private mInSplitMode:Z

.field private mIs64BitApp:Z

.field private mIsAutoStartApp:Z

.field private mIsBackupServiceApp:Z

.field private mIsLockedApp:Z

.field private mLastTopTime:J

.field private mLastUidInteractiveTimeMills:J

.field private mLastVideoPlayTimeStamp:J

.field private mLaunchCount:I

.field private mMainProcOomAdj:I

.field private mMainProcStartTime:J

.field private mMinAmsProcState:I

.field private mMinOomAdj:I

.field private mMinPriorityScore:I

.field private mMinUsageLevel:I

.field private mOverlayCount:I

.field private mPackageName:Ljava/lang/String;

.field private final mProcLock:Ljava/lang/Object;

.field private mResourceBehavier:I

.field private final mRunningProcs:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Lcom/android/server/am/AppStateManager$AppState$RunningProcess;",
            ">;"
        }
    .end annotation
.end field

.field private mScenarioActions:I

.field private mSystemApp:Z

.field private mSystemSignature:Z

.field private mTopAppCount:I

.field private mTotalTimeInForeground:J

.field private final mUid:I

.field private mWhiteListVideoPlaying:Z

.field final synthetic this$0:Lcom/android/server/am/AppStateManager;


# direct methods
.method static bridge synthetic -$$Nest$fgetmForeground(Lcom/android/server/am/AppStateManager$AppState;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/am/AppStateManager$AppState;->mForeground:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmFreeFormCount(Lcom/android/server/am/AppStateManager$AppState;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/AppStateManager$AppState;->mFreeFormCount:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/am/AppStateManager$AppState;)Lcom/android/server/am/AppStateManager$AppState$MyHandler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/AppStateManager$AppState;->mHandler:Lcom/android/server/am/AppStateManager$AppState$MyHandler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmInSplitMode(Lcom/android/server/am/AppStateManager$AppState;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/am/AppStateManager$AppState;->mInSplitMode:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsAutoStartApp(Lcom/android/server/am/AppStateManager$AppState;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/am/AppStateManager$AppState;->mIsAutoStartApp:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsBackupServiceApp(Lcom/android/server/am/AppStateManager$AppState;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/am/AppStateManager$AppState;->mIsBackupServiceApp:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsLockedApp(Lcom/android/server/am/AppStateManager$AppState;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/am/AppStateManager$AppState;->mIsLockedApp:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastUidInteractiveTimeMills(Lcom/android/server/am/AppStateManager$AppState;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mLastUidInteractiveTimeMills:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmOverlayCount(Lcom/android/server/am/AppStateManager$AppState;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/AppStateManager$AppState;->mOverlayCount:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmScenarioActions(Lcom/android/server/am/AppStateManager$AppState;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/AppStateManager$AppState;->mScenarioActions:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSystemApp(Lcom/android/server/am/AppStateManager$AppState;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/am/AppStateManager$AppState;->mSystemApp:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSystemSignature(Lcom/android/server/am/AppStateManager$AppState;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/am/AppStateManager$AppState;->mSystemSignature:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmTopAppCount(Lcom/android/server/am/AppStateManager$AppState;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/AppStateManager$AppState;->mTopAppCount:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmUid(Lcom/android/server/am/AppStateManager$AppState;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/AppStateManager$AppState;->mUid:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmAppSwitchFgCount(Lcom/android/server/am/AppStateManager$AppState;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppSwitchFgCount:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmFreeFormCount(Lcom/android/server/am/AppStateManager$AppState;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/am/AppStateManager$AppState;->mFreeFormCount:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmInSplitMode(Lcom/android/server/am/AppStateManager$AppState;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/am/AppStateManager$AppState;->mInSplitMode:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsAutoStartApp(Lcom/android/server/am/AppStateManager$AppState;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/am/AppStateManager$AppState;->mIsAutoStartApp:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsLockedApp(Lcom/android/server/am/AppStateManager$AppState;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/am/AppStateManager$AppState;->mIsLockedApp:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmMainProcStartTime(Lcom/android/server/am/AppStateManager$AppState;J)V
    .locals 0

    iput-wide p1, p0, Lcom/android/server/am/AppStateManager$AppState;->mMainProcStartTime:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmOverlayCount(Lcom/android/server/am/AppStateManager$AppState;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/am/AppStateManager$AppState;->mOverlayCount:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmTopAppCount(Lcom/android/server/am/AppStateManager$AppState;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/am/AppStateManager$AppState;->mTopAppCount:I

    return-void
.end method

.method static bridge synthetic -$$Nest$mcomponentEnd(Lcom/android/server/am/AppStateManager$AppState;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/am/AppStateManager$AppState;->componentEnd(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mcomponentEnd(Lcom/android/server/am/AppStateManager$AppState;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->componentEnd(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mcomponentStart(Lcom/android/server/am/AppStateManager$AppState;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/am/AppStateManager$AppState;->componentStart(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mcomponentStart(Lcom/android/server/am/AppStateManager$AppState;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->componentStart(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhibernateAllIfNeeded(Lcom/android/server/am/AppStateManager$AppState;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->hibernateAllIfNeeded(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhibernateAllInactive(Lcom/android/server/am/AppStateManager$AppState;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->hibernateAllInactive(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monActivityForegroundLocked(Lcom/android/server/am/AppStateManager$AppState;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->onActivityForegroundLocked(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monAddToWhiteList(Lcom/android/server/am/AppStateManager$AppState;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState;->onAddToWhiteList()V

    return-void
.end method

.method static bridge synthetic -$$Nest$monAddToWhiteList(Lcom/android/server/am/AppStateManager$AppState;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->onAddToWhiteList(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monApplyOomAdjLocked(Lcom/android/server/am/AppStateManager$AppState;Lcom/android/server/am/ProcessRecord;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->onApplyOomAdjLocked(Lcom/android/server/am/ProcessRecord;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monBackupChanged(Lcom/android/server/am/AppStateManager$AppState;ZLcom/android/server/am/ProcessRecord;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/am/AppStateManager$AppState;->onBackupChanged(ZLcom/android/server/am/ProcessRecord;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monBackupServiceAppChanged(Lcom/android/server/am/AppStateManager$AppState;ZI)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/am/AppStateManager$AppState;->onBackupServiceAppChanged(ZI)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monInputMethodShow(Lcom/android/server/am/AppStateManager$AppState;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState;->onInputMethodShow()V

    return-void
.end method

.method static bridge synthetic -$$Nest$monMediaKey(Lcom/android/server/am/AppStateManager$AppState;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState;->onMediaKey()V

    return-void
.end method

.method static bridge synthetic -$$Nest$monMediaKey(Lcom/android/server/am/AppStateManager$AppState;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->onMediaKey(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monRemoveFromWhiteList(Lcom/android/server/am/AppStateManager$AppState;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState;->onRemoveFromWhiteList()V

    return-void
.end method

.method static bridge synthetic -$$Nest$monRemoveFromWhiteList(Lcom/android/server/am/AppStateManager$AppState;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->onRemoveFromWhiteList(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monReportPowerFrozenSignal(Lcom/android/server/am/AppStateManager$AppState;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/am/AppStateManager$AppState;->onReportPowerFrozenSignal(ILjava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monReportPowerFrozenSignal(Lcom/android/server/am/AppStateManager$AppState;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->onReportPowerFrozenSignal(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monSendPendingIntent(Lcom/android/server/am/AppStateManager$AppState;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->onSendPendingIntent(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mprocessKilledLocked(Lcom/android/server/am/AppStateManager$AppState;Lcom/android/server/am/ProcessRecord;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->processKilledLocked(Lcom/android/server/am/ProcessRecord;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mprocessStartedLocked(Lcom/android/server/am/AppStateManager$AppState;Lcom/android/server/am/ProcessRecord;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->processStartedLocked(Lcom/android/server/am/ProcessRecord;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mrecordVideoPlayIfNeeded(Lcom/android/server/am/AppStateManager$AppState;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->recordVideoPlayIfNeeded(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetActivityVisible(Lcom/android/server/am/AppStateManager$AppState;Ljava/lang/String;Lcom/android/server/wm/ActivityRecord;Z)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/am/AppStateManager$AppState;->setActivityVisible(Ljava/lang/String;Lcom/android/server/wm/ActivityRecord;Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetWindowVisible(Lcom/android/server/am/AppStateManager$AppState;ILcom/android/server/policy/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;Z)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/am/AppStateManager$AppState;->setWindowVisible(ILcom/android/server/policy/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mstepAppState(Lcom/android/server/am/AppStateManager$AppState;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->stepAppState(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateAppState(Lcom/android/server/am/AppStateManager$AppState;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->updateAppState(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateCurrentResourceBehavier(Lcom/android/server/am/AppStateManager$AppState;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState;->updateCurrentResourceBehavier()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateProcessLocked(Lcom/android/server/am/AppStateManager$AppState;Lcom/android/server/am/ProcessRecord;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->updateProcessLocked(Lcom/android/server/am/ProcessRecord;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateProviderDepends(Lcom/android/server/am/AppStateManager$AppState;Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;Z)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/am/AppStateManager$AppState;->updateProviderDepends(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateServiceDepends(Lcom/android/server/am/AppStateManager$AppState;Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;ZJ)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/android/server/am/AppStateManager$AppState;->updateServiceDepends(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;ZJ)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateVisibility(Lcom/android/server/am/AppStateManager$AppState;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState;->updateVisibility()V

    return-void
.end method

.method private constructor <init>(Lcom/android/server/am/AppStateManager;ILjava/lang/String;Landroid/content/Context;)V
    .locals 4
    .param p1, "this$0"    # Lcom/android/server/am/AppStateManager;
    .param p2, "uid"    # I
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "context"    # Landroid/content/Context;

    .line 938
    iput-object p1, p0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-direct {p0}, Lcom/miui/server/smartpower/IAppState;-><init>()V

    .line 892
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mScenarioActions:I

    .line 893
    iput v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mResourceBehavier:I

    .line 894
    iput v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppSwitchFgCount:I

    .line 895
    iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mForeground:Z

    .line 896
    iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mIsBackupServiceApp:Z

    .line 897
    iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mHasProtectProcess:Z

    .line 898
    iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mIsAutoStartApp:Z

    .line 899
    iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mIsLockedApp:Z

    .line 900
    iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mHasFgService:Z

    .line 901
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mTotalTimeInForeground:J

    .line 902
    iput-wide v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mLastTopTime:J

    .line 903
    iput-wide v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mLastUidInteractiveTimeMills:J

    .line 904
    iput-wide v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mMainProcStartTime:J

    .line 905
    const/4 v3, -0x1

    iput v3, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppState:I

    .line 917
    iput v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mTopAppCount:I

    .line 918
    iput v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mFreeFormCount:I

    .line 919
    iput v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mOverlayCount:I

    .line 920
    iput v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mLaunchCount:I

    .line 921
    iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mInSplitMode:Z

    .line 923
    iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mWhiteListVideoPlaying:Z

    .line 924
    iput-wide v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mLastVideoPlayTimeStamp:J

    .line 930
    new-instance v1, Landroid/util/ArrayMap;

    invoke-direct {v1}, Landroid/util/ArrayMap;-><init>()V

    iput-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    .line 931
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    .line 933
    iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mSystemApp:Z

    .line 934
    iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mSystemSignature:Z

    .line 936
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mIs64BitApp:Z

    .line 939
    iput p2, p0, Lcom/android/server/am/AppStateManager$AppState;->mUid:I

    .line 940
    iput-object p3, p0, Lcom/android/server/am/AppStateManager$AppState;->mPackageName:Ljava/lang/String;

    .line 941
    new-instance v0, Lcom/android/server/am/AppStateManager$AppState$MyHandler;

    invoke-static {p1}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmLooper(Lcom/android/server/am/AppStateManager;)Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;-><init>(Lcom/android/server/am/AppStateManager$AppState;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mHandler:Lcom/android/server/am/AppStateManager$AppState$MyHandler;

    .line 942
    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState;->checkSystemSignature()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mSystemSignature:Z

    .line 943
    invoke-static {p1}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmPendingInteractiveUids(Lcom/android/server/am/AppStateManager;)Landroid/util/SparseArray;

    move-result-object v0

    monitor-enter v0

    .line 944
    :try_start_0
    invoke-static {p1}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmPendingInteractiveUids(Lcom/android/server/am/AppStateManager;)Landroid/util/SparseArray;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->contains(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 945
    invoke-static {p1}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmPendingInteractiveUids(Lcom/android/server/am/AppStateManager;)Landroid/util/SparseArray;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mLastUidInteractiveTimeMills:J

    .line 946
    invoke-static {p1}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmPendingInteractiveUids(Lcom/android/server/am/AppStateManager;)Landroid/util/SparseArray;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->remove(I)V

    .line 948
    :cond_0
    monitor-exit v0

    .line 949
    return-void

    .line 948
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method synthetic constructor <init>(Lcom/android/server/am/AppStateManager;ILjava/lang/String;Landroid/content/Context;Lcom/android/server/am/AppStateManager$AppState-IA;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/am/AppStateManager$AppState;-><init>(Lcom/android/server/am/AppStateManager;ILjava/lang/String;Landroid/content/Context;)V

    return-void
.end method

.method private changeAppState(IZLjava/lang/String;)V
    .locals 11
    .param p1, "state"    # I
    .param p2, "hasProtectProcess"    # Z
    .param p3, "reason"    # Ljava/lang/String;

    .line 1782
    const/4 v0, 0x1

    const/4 v1, 0x3

    if-gt p1, v1, :cond_0

    .line 1783
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mHandler:Lcom/android/server/am/AppStateManager$AppState$MyHandler;

    invoke-virtual {v2, v0}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->removeMessages(I)V

    .line 1785
    :cond_0
    const/4 v2, 0x6

    if-ne p1, v2, :cond_1

    iget v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppState:I

    if-eq v2, p1, :cond_1

    const-string v2, "hibernate all "

    .line 1786
    invoke-virtual {p3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1787
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mHandler:Lcom/android/server/am/AppStateManager$AppState$MyHandler;

    invoke-virtual {v2, v0}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->removeMessages(I)V

    .line 1788
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v2}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmSmartPowerPolicyManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->getHibernateDuration(Lcom/android/server/am/AppStateManager$AppState;)J

    move-result-wide v2

    .line 1789
    .local v2, "delay":J
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_1

    .line 1790
    iget-object v4, p0, Lcom/android/server/am/AppStateManager$AppState;->mHandler:Lcom/android/server/am/AppStateManager$AppState$MyHandler;

    const-string v5, "periodic active"

    invoke-virtual {v4, v0, v5}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    .line 1792
    .local v4, "msg":Landroid/os/Message;
    iget-object v5, p0, Lcom/android/server/am/AppStateManager$AppState;->mHandler:Lcom/android/server/am/AppStateManager$AppState$MyHandler;

    invoke-virtual {v5, v4, v2, v3}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1795
    .end local v2    # "delay":J
    .end local v4    # "msg":Landroid/os/Message;
    :cond_1
    if-ne p1, v1, :cond_2

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmSmartPowerPolicyManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    move-result-object v1

    .line 1796
    invoke-virtual {v1, p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isAppHibernationWhiteList(Lcom/android/server/am/AppStateManager$AppState;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1797
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mHandler:Lcom/android/server/am/AppStateManager$AppState$MyHandler;

    invoke-virtual {v1, v0, p3}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 1798
    .local v1, "msg":Landroid/os/Message;
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mHandler:Lcom/android/server/am/AppStateManager$AppState$MyHandler;

    iget-object v3, p0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v3}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmSmartPowerPolicyManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/am/AppStateManager$AppState;->mPackageName:Ljava/lang/String;

    iget v5, p0, Lcom/android/server/am/AppStateManager$AppState;->mUid:I

    .line 1799
    invoke-virtual {v3, v4, v5}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->getInactiveDuration(Ljava/lang/String;I)J

    move-result-wide v3

    .line 1798
    invoke-virtual {v2, v1, v3, v4}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1801
    .end local v1    # "msg":Landroid/os/Message;
    :cond_2
    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppState:I

    if-ne v1, v0, :cond_3

    if-le p1, v0, :cond_3

    .line 1802
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mLastTopTime:J

    .line 1804
    :cond_3
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmProcessStateCallbacks(Lcom/android/server/am/AppStateManager;)Ljava/util/ArrayList;

    move-result-object v1

    monitor-enter v1

    .line 1805
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v2}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmProcessStateCallbacks(Lcom/android/server/am/AppStateManager;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lcom/android/server/am/AppStateManager$IProcessStateCallback;

    .line 1806
    .local v4, "callback":Lcom/android/server/am/AppStateManager$IProcessStateCallback;
    iget v6, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppState:I

    iget-boolean v8, p0, Lcom/android/server/am/AppStateManager$AppState;->mHasProtectProcess:Z

    move-object v5, p0

    move v7, p1

    move v9, p2

    move-object v10, p3

    invoke-interface/range {v4 .. v10}, Lcom/android/server/am/AppStateManager$IProcessStateCallback;->onAppStateChanged(Lcom/android/server/am/AppStateManager$AppState;IIZZLjava/lang/String;)V

    .line 1808
    .end local v4    # "callback":Lcom/android/server/am/AppStateManager$IProcessStateCallback;
    goto :goto_0

    .line 1809
    :cond_4
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1810
    sget-boolean v1, Lcom/android/server/am/AppStateManager;->DEBUG:Z

    if-eqz v1, :cond_5

    .line 1811
    const-string v1, "SmartPower"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " move to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Lcom/android/server/am/AppStateManager;->appStateToString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1813
    :cond_5
    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppState:I

    if-eq v1, p1, :cond_6

    if-ne p1, v0, :cond_6

    .line 1814
    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppSwitchFgCount:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppSwitchFgCount:I

    .line 1816
    :cond_6
    iput p1, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppState:I

    .line 1817
    iput-boolean p2, p0, Lcom/android/server/am/AppStateManager$AppState;->mHasProtectProcess:Z

    .line 1818
    return-void

    .line 1809
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private changeProcessState(ILjava/lang/String;)V
    .locals 5
    .param p1, "state"    # I
    .param p2, "reason"    # Ljava/lang/String;

    .line 1821
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1822
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 1823
    .local v2, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    invoke-static {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmStateLock(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1824
    :try_start_1
    invoke-virtual {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I

    move-result v4

    if-lez v4, :cond_1

    invoke-virtual {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->canHibernate()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x5

    if-eq p1, v4, :cond_0

    .line 1826
    invoke-virtual {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I

    move-result v4

    if-ge v4, p1, :cond_1

    .line 1827
    :cond_0
    invoke-static {v2, p1, p2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$mmoveToStateLocked(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;ILjava/lang/String;)V

    .line 1829
    :cond_1
    monitor-exit v3

    .line 1830
    .end local v2    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    goto :goto_0

    .line 1829
    .restart local v2    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    :catchall_0
    move-exception v1

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local p0    # "this":Lcom/android/server/am/AppStateManager$AppState;
    .end local p1    # "state":I
    .end local p2    # "reason":Ljava/lang/String;
    :try_start_2
    throw v1

    .line 1831
    .end local v2    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    .restart local p0    # "this":Lcom/android/server/am/AppStateManager$AppState;
    .restart local p1    # "state":I
    .restart local p2    # "reason":Ljava/lang/String;
    :cond_2
    monitor-exit v0

    .line 1832
    return-void

    .line 1831
    :catchall_1
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1
.end method

.method private checkSystemSignature()Z
    .locals 4

    .line 952
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmPkms(Lcom/android/server/am/AppStateManager;)Landroid/content/pm/PackageManager;

    move-result-object v0

    if-nez v0, :cond_0

    .line 953
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmContext(Lcom/android/server/am/AppStateManager;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/am/AppStateManager;->-$$Nest$fputmPkms(Lcom/android/server/am/AppStateManager;Landroid/content/pm/PackageManager;)V

    .line 955
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmPkms(Lcom/android/server/am/AppStateManager;)Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 956
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmPkms(Lcom/android/server/am/AppStateManager;)Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v2, "android"

    iget-object v3, p0, Lcom/android/server/am/AppStateManager$AppState;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->checkSignatures(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1

    .line 959
    :cond_2
    return v1
.end method

.method private componentEnd(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
    .locals 1
    .param p1, "processRecord"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "content"    # Ljava/lang/String;

    .line 1629
    iget v0, p1, Lcom/android/server/am/ProcessRecord;->mPid:I

    invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v0

    .line 1630
    .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    if-eqz v0, :cond_0

    .line 1631
    invoke-static {v0, p2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$mcomponentEnd(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V

    .line 1633
    :cond_0
    return-void
.end method

.method private componentEnd(Ljava/lang/String;)V
    .locals 3
    .param p1, "content"    # Ljava/lang/String;

    .line 1614
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1615
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 1616
    .local v2, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    invoke-static {v2, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$mcomponentEnd(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V

    .line 1617
    .end local v2    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    goto :goto_0

    .line 1618
    :cond_0
    monitor-exit v0

    .line 1619
    return-void

    .line 1618
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private componentStart(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
    .locals 1
    .param p1, "processRecord"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "content"    # Ljava/lang/String;

    .line 1622
    iget v0, p1, Lcom/android/server/am/ProcessRecord;->mPid:I

    invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v0

    .line 1623
    .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    if-eqz v0, :cond_0

    .line 1624
    invoke-static {v0, p2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$mcomponentStart(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V

    .line 1626
    :cond_0
    return-void
.end method

.method private componentStart(Ljava/lang/String;)V
    .locals 3
    .param p1, "content"    # Ljava/lang/String;

    .line 1596
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1597
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 1598
    .local v2, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    invoke-static {v2, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$mcomponentStart(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V

    .line 1599
    .end local v2    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    goto :goto_0

    .line 1600
    :cond_0
    monitor-exit v0

    .line 1601
    return-void

    .line 1600
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private hibernateAllIfNeeded(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .line 1503
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1504
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 1505
    .local v2, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    invoke-static {v2, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$mbecomeInactiveIfNeeded(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V

    .line 1506
    .end local v2    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    goto :goto_0

    .line 1507
    :cond_0
    monitor-exit v0

    .line 1508
    return-void

    .line 1507
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private hibernateAllInactive(Ljava/lang/String;)V
    .locals 6
    .param p1, "reason"    # Ljava/lang/String;

    .line 1511
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1512
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 1513
    .local v2, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    invoke-static {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmStateLock(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1514
    :try_start_1
    invoke-virtual {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I

    move-result v4

    const/4 v5, 0x4

    if-lt v4, v5, :cond_0

    .line 1515
    invoke-virtual {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I

    move-result v4

    const/4 v5, 0x6

    if-gt v4, v5, :cond_0

    .line 1516
    invoke-virtual {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getAdj()I

    move-result v4

    if-lez v4, :cond_0

    .line 1517
    const/4 v4, 0x7

    invoke-static {v2, v4, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$mmoveToStateLocked(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;ILjava/lang/String;)V

    .line 1519
    :cond_0
    monitor-exit v3

    .line 1521
    .end local v2    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    goto :goto_0

    .line 1519
    .restart local v2    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    :catchall_0
    move-exception v1

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local p0    # "this":Lcom/android/server/am/AppStateManager$AppState;
    .end local p1    # "reason":Ljava/lang/String;
    :try_start_2
    throw v1

    .line 1522
    .end local v2    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    .restart local p0    # "this":Lcom/android/server/am/AppStateManager$AppState;
    .restart local p1    # "reason":Ljava/lang/String;
    :cond_1
    monitor-exit v0

    .line 1523
    return-void

    .line 1522
    :catchall_1
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1
.end method

.method private isAppRunningComponent()Z
    .locals 4

    .line 1840
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmAMS(Lcom/android/server/am/AppStateManager;)Lcom/android/server/am/ActivityManagerService;

    move-result-object v0

    monitor-enter v0

    .line 1841
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 1842
    .local v2, "r":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    invoke-virtual {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/server/am/AppStateManager$AppState;->isAppRunningComponentLock(Lcom/android/server/am/ProcessRecord;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1843
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 1845
    .end local v2    # "r":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    :cond_0
    goto :goto_0

    .line 1846
    :cond_1
    monitor-exit v0

    .line 1848
    const/4 v0, 0x0

    return v0

    .line 1846
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private isAppRunningComponentLock(Lcom/android/server/am/ProcessRecord;)Z
    .locals 1
    .param p1, "proc"    # Lcom/android/server/am/ProcessRecord;

    .line 1835
    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mServices:Lcom/android/server/am/ProcessServiceRecord;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessServiceRecord;->numberOfExecutingServices()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mReceivers:Lcom/android/server/am/ProcessReceiverRecord;

    .line 1836
    invoke-virtual {v0}, Lcom/android/server/am/ProcessReceiverRecord;->numberOfCurReceivers()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 1835
    :goto_0
    return v0
.end method

.method private onActivityForegroundLocked(Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .line 1682
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1683
    :try_start_0
    iget-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mForeground:Z

    if-nez v1, :cond_0

    .line 1684
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/android/server/am/AppStateManager$AppState;->setForegroundLocked(Z)V

    .line 1685
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 1686
    .local v2, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    invoke-static {v2, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$monActivityForegroundLocked(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V

    .line 1687
    .end local v2    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    goto :goto_0

    .line 1689
    :cond_0
    monitor-exit v0

    .line 1690
    return-void

    .line 1689
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private onAddToWhiteList()V
    .locals 3

    .line 1526
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1527
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 1528
    .local v2, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    invoke-static {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$monAddToWhiteList(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V

    .line 1529
    .end local v2    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    goto :goto_0

    .line 1530
    :cond_0
    monitor-exit v0

    .line 1531
    return-void

    .line 1530
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private onAddToWhiteList(I)V
    .locals 1
    .param p1, "pid"    # I

    .line 1534
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v0

    .line 1535
    .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    if-eqz v0, :cond_0

    .line 1536
    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$monAddToWhiteList(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V

    .line 1538
    :cond_0
    return-void
.end method

.method private onApplyOomAdjLocked(Lcom/android/server/am/ProcessRecord;)V
    .locals 2
    .param p1, "record"    # Lcom/android/server/am/ProcessRecord;

    .line 1385
    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->updateProcessLocked(Lcom/android/server/am/ProcessRecord;)V

    .line 1386
    invoke-virtual {p0}, Lcom/android/server/am/AppStateManager$AppState;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1387
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1388
    :try_start_0
    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState;->updatePkgInfoLocked()V

    .line 1389
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 1391
    :cond_0
    :goto_0
    return-void
.end method

.method private onBackupChanged(ZLcom/android/server/am/ProcessRecord;)V
    .locals 1
    .param p1, "active"    # Z
    .param p2, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 1580
    iget v0, p2, Lcom/android/server/am/ProcessRecord;->mPid:I

    invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v0

    .line 1581
    .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    if-eqz v0, :cond_0

    .line 1582
    invoke-static {v0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$monBackupChanged(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Z)V

    .line 1584
    :cond_0
    return-void
.end method

.method private onBackupServiceAppChanged(ZI)V
    .locals 3
    .param p1, "active"    # Z
    .param p2, "pid"    # I

    .line 1587
    iput-boolean p1, p0, Lcom/android/server/am/AppStateManager$AppState;->mIsBackupServiceApp:Z

    .line 1588
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1589
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 1590
    .local v2, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    invoke-static {v2, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$monBackupServiceAppChanged(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Z)V

    .line 1591
    .end local v2    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    goto :goto_0

    .line 1592
    :cond_0
    monitor-exit v0

    .line 1593
    return-void

    .line 1592
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private onInputMethodShow()V
    .locals 3

    .line 1572
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1573
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 1574
    .local v2, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    invoke-static {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$monInputMethodShow(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V

    .line 1575
    .end local v2    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    goto :goto_0

    .line 1576
    :cond_0
    monitor-exit v0

    .line 1577
    return-void

    .line 1576
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private onMediaKey()V
    .locals 5

    .line 1563
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mLastUidInteractiveTimeMills:J

    .line 1564
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1565
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 1566
    .local v2, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    iget-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState;->mLastUidInteractiveTimeMills:J

    invoke-static {v2, v3, v4}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$monMediaKey(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;J)V

    .line 1567
    .end local v2    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    goto :goto_0

    .line 1568
    :cond_0
    monitor-exit v0

    .line 1569
    return-void

    .line 1568
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private onMediaKey(I)V
    .locals 3
    .param p1, "pid"    # I

    .line 1556
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v0

    .line 1557
    .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    if-eqz v0, :cond_0

    .line 1558
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$monMediaKey(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;J)V

    .line 1560
    :cond_0
    return-void
.end method

.method private onRemoveFromWhiteList()V
    .locals 3

    .line 1541
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1542
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 1543
    .local v2, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    invoke-static {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$monRemoveFromWhiteList(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V

    .line 1544
    .end local v2    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    goto :goto_0

    .line 1545
    :cond_0
    monitor-exit v0

    .line 1546
    return-void

    .line 1545
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private onRemoveFromWhiteList(I)V
    .locals 1
    .param p1, "pid"    # I

    .line 1549
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v0

    .line 1550
    .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    if-eqz v0, :cond_0

    .line 1551
    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$monRemoveFromWhiteList(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V

    .line 1553
    :cond_0
    return-void
.end method

.method private onReportPowerFrozenSignal(ILjava/lang/String;)V
    .locals 1
    .param p1, "pid"    # I
    .param p2, "reason"    # Ljava/lang/String;

    .line 1667
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v0

    .line 1668
    .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    if-eqz v0, :cond_0

    .line 1669
    invoke-static {v0, p2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$monReportPowerFrozenSignal(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V

    .line 1671
    :cond_0
    return-void
.end method

.method private onReportPowerFrozenSignal(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .line 1674
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1675
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 1676
    .local v2, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    invoke-static {v2, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$monReportPowerFrozenSignal(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V

    .line 1677
    .end local v2    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    goto :goto_0

    .line 1678
    :cond_0
    monitor-exit v0

    .line 1679
    return-void

    .line 1678
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private onResourceBehavierChanged(I)V
    .locals 7
    .param p1, "resourceBehavier"    # I

    .line 1037
    invoke-static {p1}, Lcom/miui/server/smartpower/SmartScenarioManager;->calculateScenarioAction(I)I

    move-result v0

    .line 1038
    .local v0, "curAction":I
    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mResourceBehavier:I

    invoke-static {v1}, Lcom/miui/server/smartpower/SmartScenarioManager;->calculateScenarioAction(I)I

    move-result v1

    .line 1039
    .local v1, "preAction":I
    if-eq v0, v1, :cond_1

    .line 1040
    not-int v2, v0

    and-int/2addr v2, v1

    invoke-static {v2}, Lcom/android/server/am/AppStateManager;->-$$Nest$smparseScenatioActions(I)Ljava/util/List;

    move-result-object v2

    .line 1041
    .local v2, "removedActions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 1042
    .local v4, "action":I
    const/4 v5, 0x0

    invoke-direct {p0, v4, v5}, Lcom/android/server/am/AppStateManager$AppState;->updateCurrentScenarioAction(IZ)V

    .line 1043
    .end local v4    # "action":I
    goto :goto_0

    .line 1044
    :cond_0
    not-int v3, v1

    and-int/2addr v3, v0

    invoke-static {v3}, Lcom/android/server/am/AppStateManager;->-$$Nest$smparseScenatioActions(I)Ljava/util/List;

    move-result-object v3

    .line 1045
    .local v3, "addedActions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 1046
    .local v5, "action":I
    const/4 v6, 0x1

    invoke-direct {p0, v5, v6}, Lcom/android/server/am/AppStateManager$AppState;->updateCurrentScenarioAction(IZ)V

    .line 1047
    .end local v5    # "action":I
    goto :goto_1

    .line 1049
    .end local v2    # "removedActions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v3    # "addedActions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_1
    iput p1, p0, Lcom/android/server/am/AppStateManager$AppState;->mResourceBehavier:I

    .line 1050
    return-void
.end method

.method private onSendPendingIntent(Ljava/lang/String;)V
    .locals 4
    .param p1, "typeName"    # Ljava/lang/String;

    .line 1604
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "pending intent "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1605
    .local v0, "content":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1606
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v2}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 1607
    .local v3, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    invoke-static {v3, v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$monSendPendingIntent(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V

    .line 1608
    .end local v3    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    goto :goto_0

    .line 1609
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1610
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mLastUidInteractiveTimeMills:J

    .line 1611
    return-void

    .line 1609
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method private processKilledLocked(Lcom/android/server/am/ProcessRecord;)V
    .locals 3
    .param p1, "record"    # Lcom/android/server/am/ProcessRecord;

    .line 1336
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(Ljava/lang/String;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v0

    .line 1337
    .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    if-eqz v0, :cond_0

    .line 1338
    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$mprocessKilledLocked(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V

    .line 1339
    invoke-direct {p0, v0}, Lcom/android/server/am/AppStateManager$AppState;->removeProcessIfNeeded(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V

    .line 1340
    invoke-virtual {p0}, Lcom/android/server/am/AppStateManager$AppState;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1341
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1342
    :try_start_0
    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState;->updatePkgInfoLocked()V

    .line 1343
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 1346
    :cond_0
    :goto_0
    return-void
.end method

.method private processStartedLocked(Lcom/android/server/am/ProcessRecord;)V
    .locals 4
    .param p1, "record"    # Lcom/android/server/am/ProcessRecord;

    .line 1318
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(Ljava/lang/String;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v0

    .line 1319
    .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    if-nez v0, :cond_1

    .line 1320
    new-instance v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;-><init>(Lcom/android/server/am/AppStateManager$AppState;Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/AppStateManager$AppState$RunningProcess-IA;)V

    .line 1321
    .end local v0    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    .local v1, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v2

    .line 1322
    :try_start_0
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v0}, Landroid/util/ArrayMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1323
    invoke-static {p1}, Lcom/android/server/am/AppStateManager;->isSystemApp(Lcom/android/server/am/ProcessRecord;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mSystemApp:Z

    .line 1325
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v0, v3, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1326
    monitor-exit v2

    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1328
    .end local v1    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    .restart local v0    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    :cond_1
    invoke-static {v0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$mupdateProcessInfo(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Lcom/android/server/am/ProcessRecord;)V

    .line 1330
    :goto_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1331
    :try_start_1
    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState;->updatePkgInfoLocked()V

    .line 1332
    monitor-exit v1

    .line 1333
    return-void

    .line 1332
    :catchall_1
    move-exception v2

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v2
.end method

.method private recordVideoPlayIfNeeded(Z)V
    .locals 5
    .param p1, "active"    # Z

    .line 994
    if-eqz p1, :cond_1

    .line 995
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v0

    .line 996
    const/4 v1, 0x0

    .line 997
    .local v1, "whiteListVideoVisible":Z
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v2}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 998
    .local v3, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    invoke-static {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$mwhiteListVideoVisible(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Z

    move-result v4

    or-int/2addr v1, v4

    .line 999
    .end local v3    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    goto :goto_0

    .line 1000
    :cond_0
    iput-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mWhiteListVideoPlaying:Z

    .line 1001
    .end local v1    # "whiteListVideoVisible":Z
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1002
    sget-boolean v0, Lcom/android/server/am/AppStateManager;->DEBUG:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mWhiteListVideoPlaying:Z

    if-eqz v0, :cond_2

    .line 1003
    const-string v0, "SmartPower"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " WhiteListVideoPlaying "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mWhiteListVideoPlaying:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1001
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 1006
    :cond_1
    iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mWhiteListVideoPlaying:Z

    if-eqz v0, :cond_2

    .line 1007
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mLastVideoPlayTimeStamp:J

    .line 1008
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mWhiteListVideoPlaying:Z

    .line 1009
    sget-boolean v0, Lcom/android/server/am/AppStateManager;->DEBUG:Z

    if-eqz v0, :cond_2

    .line 1010
    const-string v0, "SmartPower"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " WhiteListVideoPlaying "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mWhiteListVideoPlaying:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1014
    :cond_2
    :goto_1
    return-void
.end method

.method private removeProcessIfNeeded(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V
    .locals 3
    .param p1, "proc"    # Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 1361
    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getUid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->isIsolated(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isKilled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1362
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1363
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getProcessName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1364
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 1366
    :cond_0
    :goto_0
    return-void
.end method

.method private setActivityVisible(Ljava/lang/String;Lcom/android/server/wm/ActivityRecord;Z)V
    .locals 1
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "record"    # Lcom/android/server/wm/ActivityRecord;
    .param p3, "visible"    # Z

    .line 1370
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(Ljava/lang/String;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v0

    .line 1371
    .local v0, "process":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    if-eqz v0, :cond_0

    .line 1372
    invoke-static {v0, p2, p3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$msetActivityVisible(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Lcom/android/server/wm/ActivityRecord;Z)V

    .line 1374
    :cond_0
    return-void
.end method

.method private setForegroundLocked(Z)V
    .locals 2
    .param p1, "foreground"    # Z

    .line 1496
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v0}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 1497
    .local v1, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    invoke-static {v1, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$msetForeground(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Z)V

    .line 1498
    .end local v1    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    goto :goto_0

    .line 1499
    :cond_0
    iput-boolean p1, p0, Lcom/android/server/am/AppStateManager$AppState;->mForeground:Z

    .line 1500
    return-void
.end method

.method private setWindowVisible(ILcom/android/server/policy/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;Z)V
    .locals 1
    .param p1, "pid"    # I
    .param p2, "win"    # Lcom/android/server/policy/WindowManagerPolicy$WindowState;
    .param p3, "attrs"    # Landroid/view/WindowManager$LayoutParams;
    .param p4, "visible"    # Z

    .line 1378
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v0

    .line 1379
    .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    if-eqz v0, :cond_0

    .line 1380
    invoke-static {v0, p2, p3, p4}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$msetWindowVisible(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Lcom/android/server/policy/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;Z)V

    .line 1382
    :cond_0
    return-void
.end method

.method private stepAppState(Ljava/lang/String;)V
    .locals 6
    .param p1, "reason"    # Ljava/lang/String;

    .line 1852
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mHandler:Lcom/android/server/am/AppStateManager$AppState$MyHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->removeMessages(I)V

    .line 1853
    const-wide/16 v2, 0x0

    .line 1854
    .local v2, "nextStepDuration":J
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mHandler:Lcom/android/server/am/AppStateManager$AppState$MyHandler;

    invoke-virtual {v0, v1, p1}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1855
    .local v0, "msg":Landroid/os/Message;
    iget v4, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppState:I

    const/4 v5, 0x6

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 1882
    :pswitch_0
    const/4 v1, 0x5

    invoke-direct {p0, v1, p1}, Lcom/android/server/am/AppStateManager$AppState;->changeProcessState(ILjava/lang/String;)V

    .line 1883
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmSmartPowerPolicyManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    move-result-object v1

    iget-object v4, p0, Lcom/android/server/am/AppStateManager$AppState;->mPackageName:Ljava/lang/String;

    .line 1884
    invoke-virtual {v1, v4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->getMaintenanceDur(Ljava/lang/String;)J

    move-result-wide v2

    goto :goto_0

    .line 1867
    :pswitch_1
    iget v4, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinOomAdj:I

    if-gtz v4, :cond_1

    iget v4, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinAmsProcState:I

    if-lt v4, v5, :cond_0

    .line 1869
    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState;->isAppRunningComponent()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1870
    :cond_0
    const-wide/16 v4, 0x3e8

    .line 1871
    .local v4, "delay":J
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mHandler:Lcom/android/server/am/AppStateManager$AppState$MyHandler;

    invoke-virtual {v1, v0, v4, v5}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1872
    return-void

    .line 1874
    .end local v4    # "delay":J
    :cond_1
    const/4 v4, 0x7

    invoke-direct {p0, v4, p1}, Lcom/android/server/am/AppStateManager$AppState;->changeProcessState(ILjava/lang/String;)V

    .line 1875
    iget-object v4, p0, Lcom/android/server/am/AppStateManager$AppState;->mHandler:Lcom/android/server/am/AppStateManager$AppState$MyHandler;

    const-string v5, "periodic active"

    invoke-virtual {v4, v1, v5}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1877
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmSmartPowerPolicyManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    move-result-object v1

    .line 1878
    invoke-virtual {v1, p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->getHibernateDuration(Lcom/android/server/am/AppStateManager$AppState;)J

    move-result-wide v2

    .line 1879
    goto :goto_0

    .line 1858
    :pswitch_2
    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinOomAdj:I

    if-gtz v1, :cond_2

    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState;->isAppRunningComponent()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1859
    const-wide/16 v4, 0x3e8

    .line 1860
    .restart local v4    # "delay":J
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mHandler:Lcom/android/server/am/AppStateManager$AppState$MyHandler;

    invoke-virtual {v1, v0, v4, v5}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1861
    return-void

    .line 1863
    .end local v4    # "delay":J
    :cond_2
    invoke-direct {p0, v5, p1}, Lcom/android/server/am/AppStateManager$AppState;->changeProcessState(ILjava/lang/String;)V

    .line 1864
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmSmartPowerPolicyManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    move-result-object v1

    iget-object v4, p0, Lcom/android/server/am/AppStateManager$AppState;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->getIdleDur(Ljava/lang/String;)J

    move-result-wide v2

    .line 1865
    nop

    .line 1887
    :goto_0
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_3

    .line 1888
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mHandler:Lcom/android/server/am/AppStateManager$AppState$MyHandler;

    invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1890
    :cond_3
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private updateAppState(Ljava/lang/String;)V
    .locals 9
    .param p1, "reason"    # Ljava/lang/String;

    .line 1723
    const/4 v0, -0x1

    .line 1724
    .local v0, "minProcessState":I
    const/4 v1, -0x1

    .line 1725
    .local v1, "minProcessStateExceptProtect":I
    const/4 v2, 0x0

    .line 1726
    .local v2, "hasProtectProcess":Z
    iget-object v3, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v3

    .line 1727
    :try_start_0
    iget-object v4, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v4}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 1728
    .local v5, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    invoke-virtual {v5}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->canHibernate()Z

    move-result v6

    .line 1729
    .local v6, "canHibernate":Z
    invoke-virtual {v5}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I

    move-result v7

    .line 1730
    .local v7, "state":I
    if-nez v6, :cond_0

    const/4 v8, 0x1

    goto :goto_1

    :cond_0
    const/4 v8, 0x0

    :goto_1
    or-int/2addr v2, v8

    .line 1731
    const/4 v8, -0x1

    if-lez v7, :cond_1

    if-lt v7, v0, :cond_2

    if-eqz v0, :cond_2

    :cond_1
    if-ne v0, v8, :cond_3

    .line 1734
    :cond_2
    move v0, v7

    .line 1736
    :cond_3
    if-eqz v6, :cond_5

    if-lt v7, v1, :cond_4

    if-eqz v1, :cond_4

    if-ne v1, v8, :cond_5

    .line 1740
    :cond_4
    move v1, v7

    .line 1741
    iget-boolean v8, p0, Lcom/android/server/am/AppStateManager$AppState;->mForeground:Z

    if-nez v8, :cond_5

    const/4 v8, 0x3

    if-ne v7, v8, :cond_5

    .line 1742
    invoke-static {v5, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$msendBecomeInactiveMsg(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V

    .line 1745
    .end local v5    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    .end local v6    # "canHibernate":Z
    .end local v7    # "state":I
    :cond_5
    goto :goto_0

    .line 1746
    :cond_6
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v4

    move v0, v4

    .line 1747
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1748
    const/4 v3, -0x1

    .line 1749
    .local v3, "appState":I
    packed-switch v0, :pswitch_data_0

    goto :goto_2

    .line 1773
    :pswitch_0
    const/4 v3, 0x6

    goto :goto_2

    .line 1770
    :pswitch_1
    const/4 v3, 0x5

    .line 1771
    goto :goto_2

    .line 1767
    :pswitch_2
    const/4 v3, 0x4

    .line 1768
    goto :goto_2

    .line 1764
    :pswitch_3
    const/4 v3, 0x3

    .line 1765
    goto :goto_2

    .line 1761
    :pswitch_4
    const/4 v3, 0x2

    .line 1762
    goto :goto_2

    .line 1757
    :pswitch_5
    const/4 v3, 0x1

    .line 1758
    goto :goto_2

    .line 1754
    :pswitch_6
    const/4 v3, 0x0

    .line 1755
    goto :goto_2

    .line 1751
    :pswitch_7
    const/4 v3, -0x1

    .line 1752
    nop

    .line 1776
    :goto_2
    iget v4, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppState:I

    if-ne v3, v4, :cond_7

    iget-boolean v4, p0, Lcom/android/server/am/AppStateManager$AppState;->mHasProtectProcess:Z

    if-eq v4, v2, :cond_8

    .line 1777
    :cond_7
    invoke-direct {p0, v3, v2, p1}, Lcom/android/server/am/AppStateManager$AppState;->changeAppState(IZLjava/lang/String;)V

    .line 1779
    :cond_8
    return-void

    .line 1747
    .end local v3    # "appState":I
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private updateCurrentResourceBehavier()V
    .locals 5

    .line 1017
    const/4 v0, 0x0

    .line 1018
    .local v0, "currentBehavier":I
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1019
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v2}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 1020
    .local v3, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    invoke-static {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmAudioBehavier(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v4

    or-int/2addr v0, v4

    .line 1021
    invoke-static {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmGpsBehavier(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v4

    or-int/2addr v0, v4

    .line 1022
    invoke-static {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmNetBehavier(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v4

    or-int/2addr v0, v4

    .line 1023
    invoke-static {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmCamBehavier(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v4

    or-int/2addr v0, v4

    .line 1024
    invoke-static {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmDisplayBehavier(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v4

    or-int/2addr v0, v4

    .line 1025
    .end local v3    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    goto :goto_0

    .line 1026
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1027
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmHoldScreenUid(Lcom/android/server/am/AppStateManager;)I

    move-result v1

    iget v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mUid:I

    if-ne v1, v2, :cond_1

    .line 1028
    or-int/lit16 v0, v0, 0x400

    .line 1031
    :cond_1
    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mResourceBehavier:I

    if-eq v0, v1, :cond_2

    .line 1032
    invoke-direct {p0, v0}, Lcom/android/server/am/AppStateManager$AppState;->onResourceBehavierChanged(I)V

    .line 1034
    :cond_2
    return-void

    .line 1026
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method private updateCurrentScenarioAction(IZ)V
    .locals 3
    .param p1, "action"    # I
    .param p2, "active"    # Z

    .line 968
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mScenarioActions:I

    .line 969
    .local v0, "currentActions":I
    if-eqz p2, :cond_0

    .line 970
    or-int/2addr v0, p1

    goto :goto_0

    .line 972
    :cond_0
    not-int v1, p1

    and-int/2addr v0, v1

    .line 974
    :goto_0
    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mScenarioActions:I

    if-eq v0, v1, :cond_1

    .line 975
    iput v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mScenarioActions:I

    .line 976
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mHandler:Lcom/android/server/am/AppStateManager$AppState$MyHandler;

    new-instance v2, Lcom/android/server/am/AppStateManager$AppState$1;

    invoke-direct {v2, p0, p1, p2}, Lcom/android/server/am/AppStateManager$AppState$1;-><init>(Lcom/android/server/am/AppStateManager$AppState;IZ)V

    invoke-virtual {v1, v2}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->post(Ljava/lang/Runnable;)Z

    .line 991
    :cond_1
    return-void
.end method

.method private updatePkgInfoLocked()V
    .locals 10

    .line 1394
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v0}, Landroid/util/ArrayMap;->size()I

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 1396
    :cond_0
    const/16 v0, -0x2710

    .line 1397
    .local v0, "minAdj":I
    const/16 v1, 0x14

    .line 1398
    .local v1, "minAmsProcState":I
    const/16 v2, 0x3e8

    .line 1400
    .local v2, "minPriorityScore":I
    const/16 v3, 0x9

    .line 1402
    .local v3, "minUsageLevel":I
    const/4 v4, 0x0

    .line 1403
    .local v4, "hasFgService":Z
    const/4 v5, 0x0

    .line 1404
    .local v5, "is32BitApp":Z
    iget-object v6, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v6}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    const/16 v8, -0x2710

    if-eqz v7, :cond_b

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 1405
    .local v7, "r":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    invoke-virtual {v7}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I

    move-result v9

    if-eqz v9, :cond_1

    invoke-static {v7}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmAdj(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v9

    if-ne v9, v8, :cond_2

    .line 1407
    goto :goto_0

    .line 1409
    :cond_2
    invoke-static {v7}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmAdj(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v9

    if-gt v0, v9, :cond_3

    if-ne v0, v8, :cond_4

    .line 1410
    :cond_3
    invoke-static {v7}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmAdj(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v0

    .line 1412
    :cond_4
    invoke-static {v7}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmAmsProcState(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v8

    if-le v1, v8, :cond_5

    .line 1413
    invoke-static {v7}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmAmsProcState(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v1

    .line 1415
    :cond_5
    invoke-virtual {v7}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPriorityScore()I

    move-result v8

    if-le v2, v8, :cond_6

    .line 1416
    invoke-virtual {v7}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPriorityScore()I

    move-result v2

    .line 1418
    :cond_6
    invoke-virtual {v7}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getUsageLevel()I

    move-result v8

    if-le v3, v8, :cond_7

    .line 1419
    invoke-virtual {v7}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getUsageLevel()I

    move-result v3

    .line 1421
    :cond_7
    invoke-virtual {v7}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->hasForegrundService()Z

    move-result v8

    if-eqz v8, :cond_8

    .line 1422
    const/4 v4, 0x1

    .line 1424
    :cond_8
    invoke-virtual {v7}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isMainProc()Z

    move-result v8

    if-eqz v8, :cond_9

    .line 1425
    invoke-static {v7}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmAdj(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v8

    iput v8, p0, Lcom/android/server/am/AppStateManager$AppState;->mMainProcOomAdj:I

    .line 1427
    :cond_9
    invoke-static {v7}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmIs64BitProcess(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Z

    move-result v8

    if-nez v8, :cond_a

    .line 1428
    const/4 v5, 0x1

    .line 1430
    .end local v7    # "r":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    :cond_a
    goto :goto_0

    .line 1431
    :cond_b
    iget v6, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinOomAdj:I

    if-eq v0, v6, :cond_c

    if-eq v0, v8, :cond_c

    .line 1432
    iput v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinOomAdj:I

    .line 1434
    :cond_c
    iget v6, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinPriorityScore:I

    if-eq v2, v6, :cond_d

    const/16 v6, 0x3e8

    if-eq v2, v6, :cond_d

    .line 1437
    iput v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinPriorityScore:I

    .line 1439
    :cond_d
    xor-int/lit8 v6, v5, 0x1

    iput-boolean v6, p0, Lcom/android/server/am/AppStateManager$AppState;->mIs64BitApp:Z

    .line 1440
    iput v3, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinUsageLevel:I

    .line 1441
    iput v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinAmsProcState:I

    .line 1442
    iput-boolean v4, p0, Lcom/android/server/am/AppStateManager$AppState;->mHasFgService:Z

    .line 1443
    return-void
.end method

.method private updateProcessLocked(Lcom/android/server/am/ProcessRecord;)V
    .locals 3
    .param p1, "record"    # Lcom/android/server/am/ProcessRecord;

    .line 1349
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1350
    :try_start_0
    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(Ljava/lang/String;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v1

    .line 1351
    .local v1, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    if-eqz v1, :cond_0

    .line 1352
    invoke-static {v1, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$mupdateProcessInfo(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Lcom/android/server/am/ProcessRecord;)V

    .line 1353
    invoke-direct {p0, v1}, Lcom/android/server/am/AppStateManager$AppState;->removeProcessIfNeeded(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V

    goto :goto_0

    .line 1354
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isKilled()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1355
    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->processStartedLocked(Lcom/android/server/am/ProcessRecord;)V

    .line 1357
    .end local v1    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    :cond_1
    :goto_0
    monitor-exit v0

    .line 1358
    return-void

    .line 1357
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private updateProviderDepends(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;Z)V
    .locals 1
    .param p1, "client"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "host"    # Lcom/android/server/am/ProcessRecord;
    .param p3, "isConnect"    # Z

    .line 1659
    iget v0, p1, Lcom/android/server/am/ProcessRecord;->mPid:I

    invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v0

    .line 1660
    .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    if-eqz v0, :cond_0

    .line 1661
    invoke-static {v0, p2, p3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$mupdateProviderDepends(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Lcom/android/server/am/ProcessRecord;Z)V

    .line 1663
    :cond_0
    return-void
.end method

.method private updateServiceDepends(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;ZJ)V
    .locals 14
    .param p1, "client"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "service"    # Lcom/android/server/am/ProcessRecord;
    .param p3, "isConnect"    # Z
    .param p4, "flags"    # J

    .line 1637
    move-object v6, p0

    move-object v7, p1

    move-object/from16 v8, p2

    iget v0, v7, Lcom/android/server/am/ProcessRecord;->mPid:I

    invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v9

    .line 1638
    .local v9, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    const-wide/32 v0, 0x4000000

    and-long v0, p4, v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    const/4 v1, 0x1

    const/4 v4, 0x0

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v4

    :goto_0
    move v10, v0

    .line 1639
    .local v10, "fgService":Z
    if-eqz v9, :cond_1

    .line 1640
    move/from16 v11, p3

    invoke-static {v9, v8, v11, v10}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$mupdateServiceDepends(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Lcom/android/server/am/ProcessRecord;ZZ)V

    goto :goto_2

    .line 1641
    :cond_1
    move/from16 v11, p3

    iget v0, v7, Lcom/android/server/am/ProcessRecord;->mPid:I

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v5

    if-ne v0, v5, :cond_3

    iget v0, v8, Lcom/android/server/am/ProcessRecord;->uid:I

    iget v5, v6, Lcom/android/server/am/AppStateManager$AppState;->mUid:I

    if-eq v0, v5, :cond_3

    .line 1642
    const-wide/32 v12, 0x8000

    and-long v12, p4, v12

    cmp-long v0, v12, v2

    if-eqz v0, :cond_2

    move v5, v1

    goto :goto_1

    :cond_2
    move v5, v4

    .line 1643
    .local v5, "isJobService":Z
    :goto_1
    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p3

    move v4, v10

    invoke-direct/range {v0 .. v5}, Lcom/android/server/am/AppStateManager$AppState;->updateSystemServiceDepends(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;ZZZ)V

    .line 1645
    .end local v5    # "isJobService":Z
    :cond_3
    :goto_2
    return-void
.end method

.method private updateSystemServiceDepends(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;ZZZ)V
    .locals 7
    .param p1, "client"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "service"    # Lcom/android/server/am/ProcessRecord;
    .param p3, "isConnect"    # Z
    .param p4, "fgService"    # Z
    .param p5, "jobService"    # Z

    .line 1649
    if-eqz p3, :cond_0

    if-nez p4, :cond_0

    if-nez p5, :cond_1

    .line 1650
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmSmartPowerPolicyManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    move-result-object v1

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    iget-object v4, p2, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    iget v5, p2, Lcom/android/server/am/ProcessRecord;->uid:I

    iget v6, p2, Lcom/android/server/am/ProcessRecord;->mPid:I

    move v2, p3

    invoke-virtual/range {v1 .. v6}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->onDependChanged(ZLjava/lang/String;Ljava/lang/String;II)V

    .line 1655
    :cond_1
    return-void
.end method

.method private updateVisibility()V
    .locals 7

    .line 1446
    const/4 v0, 0x0

    .line 1447
    .local v0, "foreground":Z
    const/4 v1, 0x0

    .line 1448
    .local v1, "inSplitMode":Z
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v2

    .line 1449
    :try_start_0
    iget-object v3, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v3}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 1450
    .local v4, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    invoke-static {v4}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$mupdateVisibility(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V

    .line 1451
    invoke-virtual {v4}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isVisible()Z

    move-result v5

    or-int/2addr v0, v5

    .line 1452
    invoke-virtual {v4}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->inSplitMode()Z

    move-result v5

    or-int/2addr v1, v5

    .line 1453
    invoke-virtual {v4}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->inCurrentStack()Z

    move-result v5

    or-int/2addr v0, v5

    .line 1454
    .end local v4    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    goto :goto_0

    .line 1455
    :cond_0
    iget-boolean v3, p0, Lcom/android/server/am/AppStateManager$AppState;->mForeground:Z

    if-eq v0, v3, :cond_1

    .line 1456
    invoke-direct {p0, v0}, Lcom/android/server/am/AppStateManager$AppState;->setForegroundLocked(Z)V

    .line 1457
    :cond_1
    iget v3, p0, Lcom/android/server/am/AppStateManager$AppState;->mTopAppCount:I

    const/4 v4, 0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-lez v3, :cond_2

    .line 1458
    invoke-direct {p0, v4, v5}, Lcom/android/server/am/AppStateManager$AppState;->updateCurrentScenarioAction(IZ)V

    goto :goto_1

    .line 1461
    :cond_2
    invoke-direct {p0, v4, v6}, Lcom/android/server/am/AppStateManager$AppState;->updateCurrentScenarioAction(IZ)V

    .line 1464
    :goto_1
    iget v3, p0, Lcom/android/server/am/AppStateManager$AppState;->mFreeFormCount:I

    const/16 v4, 0x10

    if-lez v3, :cond_3

    .line 1465
    invoke-direct {p0, v4, v5}, Lcom/android/server/am/AppStateManager$AppState;->updateCurrentScenarioAction(IZ)V

    goto :goto_2

    .line 1468
    :cond_3
    invoke-direct {p0, v4, v6}, Lcom/android/server/am/AppStateManager$AppState;->updateCurrentScenarioAction(IZ)V

    .line 1471
    :goto_2
    iget v3, p0, Lcom/android/server/am/AppStateManager$AppState;->mOverlayCount:I

    const/16 v4, 0x20

    if-lez v3, :cond_4

    .line 1472
    invoke-direct {p0, v4, v5}, Lcom/android/server/am/AppStateManager$AppState;->updateCurrentScenarioAction(IZ)V

    goto :goto_3

    .line 1475
    :cond_4
    invoke-direct {p0, v4, v6}, Lcom/android/server/am/AppStateManager$AppState;->updateCurrentScenarioAction(IZ)V

    .line 1478
    :goto_3
    const/16 v3, 0x8

    if-eqz v0, :cond_5

    .line 1479
    invoke-direct {p0, v3, v6}, Lcom/android/server/am/AppStateManager$AppState;->updateCurrentScenarioAction(IZ)V

    goto :goto_4

    .line 1482
    :cond_5
    invoke-direct {p0, v3, v5}, Lcom/android/server/am/AppStateManager$AppState;->updateCurrentScenarioAction(IZ)V

    .line 1485
    :goto_4
    const v3, 0x8000

    if-eqz v1, :cond_6

    .line 1486
    invoke-direct {p0, v3, v5}, Lcom/android/server/am/AppStateManager$AppState;->updateCurrentScenarioAction(IZ)V

    goto :goto_5

    .line 1489
    :cond_6
    invoke-direct {p0, v3, v6}, Lcom/android/server/am/AppStateManager$AppState;->updateCurrentScenarioAction(IZ)V

    .line 1492
    :goto_5
    monitor-exit v2

    .line 1493
    return-void

    .line 1492
    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method


# virtual methods
.method public dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
    .locals 4
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "args"    # [Ljava/lang/String;
    .param p3, "opti"    # I

    .line 1694
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1695
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mUid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") state="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppState:I

    .line 1696
    invoke-static {v2}, Lcom/android/server/am/AppStateManager;->appStateToString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1695
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1697
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "    minAdj="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinOomAdj:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " minPriority="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinPriorityScore:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " hdur="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v2}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmSmartPowerPolicyManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    move-result-object v2

    .line 1698
    invoke-virtual {v2, p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->getHibernateDuration(Lcom/android/server/am/AppStateManager$AppState;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1697
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1700
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "    proc size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v2}, Landroid/util/ArrayMap;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1701
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "    noRestrict="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v2}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmSmartPowerPolicyManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/AppStateManager$AppState;->mPackageName:Ljava/lang/String;

    .line 1702
    invoke-virtual {v2, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isNoRestrictApp(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " locked="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mIsLockedApp:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " autoStart="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mIsAutoStartApp:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sysSign="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mSystemSignature:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sysApp="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mSystemApp:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is64Bit="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mIs64BitApp:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1701
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1708
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v2}, Landroid/util/ArrayMap;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 1709
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v2, v1}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 1710
    .local v2, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    invoke-virtual {v2, p1, p2, p3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V

    .line 1708
    .end local v2    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1712
    .end local v1    # "i":I
    :cond_0
    monitor-exit v0

    .line 1713
    return-void

    .line 1712
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getActions()J
    .locals 2

    .line 964
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mScenarioActions:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getAdj()I
    .locals 1

    .line 1079
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinOomAdj:I

    return v0
.end method

.method public getAppSwitchFgCount()I
    .locals 1

    .line 1089
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppSwitchFgCount:I

    return v0
.end method

.method getCurrentState()I
    .locals 1

    .line 1209
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppState:I

    return v0
.end method

.method public getDependsProcess()Landroid/util/ArrayMap;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;",
            ">;"
        }
    .end annotation

    .line 1290
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    .line 1291
    .local v0, "dependsList":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;>;"
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1292
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v2}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 1293
    .local v3, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    invoke-static {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmServiceDependProcs(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Landroid/util/ArrayMap;

    move-result-object v4

    invoke-virtual {v4}, Landroid/util/ArrayMap;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 1294
    invoke-static {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmServiceDependProcs(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Landroid/util/ArrayMap;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/util/ArrayMap;->putAll(Landroid/util/ArrayMap;)V

    .line 1296
    :cond_0
    invoke-static {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmProviderDependProcs(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Landroid/util/ArrayMap;

    move-result-object v4

    invoke-virtual {v4}, Landroid/util/ArrayMap;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 1297
    invoke-static {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmProviderDependProcs(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Landroid/util/ArrayMap;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/util/ArrayMap;->putAll(Landroid/util/ArrayMap;)V

    .line 1299
    .end local v3    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    :cond_1
    goto :goto_0

    .line 1300
    :cond_2
    monitor-exit v1

    .line 1301
    return-object v0

    .line 1300
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public getLastTopTime()J
    .locals 2

    .line 1306
    iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mLastTopTime:J

    return-wide v0
.end method

.method public getLastVideoPlayTimeStamp()J
    .locals 2

    .line 1310
    iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mLastVideoPlayTimeStamp:J

    return-wide v0
.end method

.method public getLaunchCount()I
    .locals 1

    .line 1099
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mLaunchCount:I

    return v0
.end method

.method public getMainProcAdj()I
    .locals 1

    .line 1084
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mMainProcOomAdj:I

    return v0
.end method

.method public getMainProcStartTime()J
    .locals 2

    .line 1314
    iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mMainProcStartTime:J

    return-wide v0
.end method

.method public getMinAmsProcState()I
    .locals 1

    .line 1176
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinAmsProcState:I

    return v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .line 1059
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getPriorityScore()I
    .locals 1

    .line 1069
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinPriorityScore:I

    return v0
.end method

.method getProcessState(I)I
    .locals 2
    .param p1, "pid"    # I

    .line 1180
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v0

    .line 1181
    .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    if-eqz v0, :cond_0

    .line 1182
    invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I

    move-result v1

    return v1

    .line 1184
    :cond_0
    const/4 v1, -0x1

    return v1
.end method

.method public getProcessState(Ljava/lang/String;)I
    .locals 2
    .param p1, "processName"    # Ljava/lang/String;

    .line 1201
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(Ljava/lang/String;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v0

    .line 1202
    .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    if-eqz v0, :cond_0

    .line 1203
    invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I

    move-result v1

    return v1

    .line 1205
    :cond_0
    const/4 v1, -0x1

    return v1
.end method

.method getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    .locals 4
    .param p1, "pid"    # I

    .line 1245
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1246
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 1247
    .local v2, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    invoke-virtual {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPid()I

    move-result v3

    if-ne v3, p1, :cond_0

    .line 1248
    monitor-exit v0

    return-object v2

    .line 1250
    .end local v2    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    :cond_0
    goto :goto_0

    .line 1251
    :cond_1
    monitor-exit v0

    const/4 v0, 0x0

    return-object v0

    .line 1252
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method getRunningProcess(Ljava/lang/String;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    .locals 2
    .param p1, "processName"    # Ljava/lang/String;

    .line 1239
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1240
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    monitor-exit v0

    return-object v1

    .line 1241
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getRunningProcessList()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/smartpower/IAppState$IRunningProcess;",
            ">;"
        }
    .end annotation

    .line 1214
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1215
    .local v0, "runningProcList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1216
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v2}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 1217
    .local v3, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I

    move-result v4

    if-lez v4, :cond_0

    .line 1218
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1220
    .end local v3    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    :cond_0
    goto :goto_0

    .line 1221
    :cond_1
    monitor-exit v1

    .line 1222
    return-object v0

    .line 1221
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method getRunningProcessList(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList<",
            "Lcom/android/server/am/AppStateManager$AppState$RunningProcess;",
            ">;"
        }
    .end annotation

    .line 1226
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1227
    .local v0, "runningProcList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/AppStateManager$AppState$RunningProcess;>;"
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1228
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v2}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 1229
    .local v3, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I

    move-result v4

    if-lez v4, :cond_0

    invoke-static {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmPackageName(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Ljava/lang/String;

    move-result-object v4

    .line 1230
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1231
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1233
    .end local v3    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    :cond_0
    goto :goto_0

    .line 1234
    :cond_1
    monitor-exit v1

    .line 1235
    return-object v0

    .line 1234
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public getTotalTimeInForeground()J
    .locals 2

    .line 1109
    iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mTotalTimeInForeground:J

    return-wide v0
.end method

.method public getUid()I
    .locals 1

    .line 1064
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mUid:I

    return v0
.end method

.method public getUsageLevel()I
    .locals 1

    .line 1074
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinUsageLevel:I

    return v0
.end method

.method public hasActivityApp()Z
    .locals 4

    .line 1188
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1189
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 1190
    .local v2, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    invoke-virtual {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I

    move-result v3

    if-lez v3, :cond_0

    .line 1191
    invoke-virtual {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->hasActivity()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1192
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 1194
    .end local v2    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    :cond_0
    goto :goto_0

    .line 1195
    :cond_1
    monitor-exit v0

    const/4 v0, 0x0

    return v0

    .line 1196
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public hasForegroundService()Z
    .locals 1

    .line 1054
    iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mHasFgService:Z

    return v0
.end method

.method public hasProtectProcess()Z
    .locals 1

    .line 1171
    iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mHasProtectProcess:Z

    return v0
.end method

.method public isAlive()Z
    .locals 2

    .line 1114
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1115
    :try_start_0
    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppState:I

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    monitor-exit v0

    return v1

    .line 1116
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isAutoStartApp()Z
    .locals 1

    .line 1161
    iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mIsAutoStartApp:Z

    return v0
.end method

.method public isIdle()Z
    .locals 2

    .line 1121
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppState:I

    const/4 v1, 0x5

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isInactive()Z
    .locals 2

    .line 1126
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppState:I

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isIs64BitApp()Z
    .locals 1

    .line 1151
    iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mIs64BitApp:Z

    return v0
.end method

.method public isLockedApp()Z
    .locals 1

    .line 1166
    iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mIsLockedApp:Z

    return v0
.end method

.method public isProcessIdle(I)Z
    .locals 4
    .param p1, "pid"    # I

    .line 1264
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v0

    .line 1265
    .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 1266
    invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I

    move-result v2

    const/4 v3, 0x5

    if-le v2, v3, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1

    .line 1268
    :cond_1
    return v1
.end method

.method isProcessKilled(I)Z
    .locals 3
    .param p1, "pid"    # I

    .line 1256
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v0

    .line 1257
    .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    const/4 v1, 0x1

    if-eqz v0, :cond_2

    .line 1258
    invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I

    move-result v2

    if-lez v2, :cond_1

    invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isKilled()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1

    .line 1260
    :cond_2
    return v1
.end method

.method public isProcessPerceptible()Z
    .locals 5

    .line 1131
    const/4 v0, 0x0

    .line 1132
    .local v0, "isPerceptible":Z
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mProcLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1133
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    invoke-virtual {v2}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 1134
    .local v3, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isProcessPerceptible()Z

    move-result v4

    or-int/2addr v0, v4

    .line 1135
    .end local v3    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    goto :goto_0

    .line 1136
    :cond_0
    monitor-exit v1

    .line 1137
    return v0

    .line 1136
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public isProcessPerceptible(I)Z
    .locals 2
    .param p1, "pid"    # I

    .line 1273
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v0

    .line 1274
    .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    if-eqz v0, :cond_0

    .line 1275
    invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isProcessPerceptible()Z

    move-result v1

    return v1

    .line 1277
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public isProcessPerceptible(Ljava/lang/String;)Z
    .locals 2
    .param p1, "processName"    # Ljava/lang/String;

    .line 1282
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(Ljava/lang/String;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v0

    .line 1283
    .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    if-eqz v0, :cond_0

    .line 1284
    invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isProcessPerceptible()Z

    move-result v1

    return v1

    .line 1286
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public isSystemApp()Z
    .locals 1

    .line 1142
    iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mSystemApp:Z

    return v0
.end method

.method public isSystemSignature()Z
    .locals 1

    .line 1147
    iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mSystemSignature:Z

    return v0
.end method

.method public isVsible()Z
    .locals 1

    .line 1156
    iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mForeground:Z

    return v0
.end method

.method public setLaunchCount(I)V
    .locals 0
    .param p1, "launchCount"    # I

    .line 1094
    iput p1, p0, Lcom/android/server/am/AppStateManager$AppState;->mLaunchCount:I

    .line 1095
    return-void
.end method

.method public setTotalTimeInForeground(J)V
    .locals 0
    .param p1, "totalTime"    # J

    .line 1104
    iput-wide p1, p0, Lcom/android/server/am/AppStateManager$AppState;->mTotalTimeInForeground:J

    .line 1105
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1717
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mUid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppState:I

    .line 1718
    invoke-static {v1}, Lcom/android/server/am/AppStateManager;->appStateToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " adj="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinOomAdj:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " proc size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mRunningProcs:Landroid/util/ArrayMap;

    .line 1719
    invoke-virtual {v1}, Landroid/util/ArrayMap;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1717
    return-object v0
.end method
