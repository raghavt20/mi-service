public class com.android.server.am.ProcessSceneCleaner extends com.android.server.am.ProcessCleanerBase {
	 /* .source "ProcessSceneCleaner.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/am/ProcessSceneCleaner$H; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
/* # instance fields */
private com.android.server.am.ActivityManagerService mAMS;
private android.content.Context mContext;
private com.android.server.am.ProcessSceneCleaner$H mHandler;
private com.android.server.am.ProcessManagerService mPMS;
private com.android.server.am.ProcessPolicy mProcessPolicy;
private com.android.server.am.SystemPressureController mSysPressureCtrl;
/* # direct methods */
static void -$$Nest$mhandleKillAll ( com.android.server.am.ProcessSceneCleaner p0, miui.process.ProcessConfig p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessSceneCleaner;->handleKillAll(Lmiui/process/ProcessConfig;)V */
	 return;
} // .end method
static void -$$Nest$mhandleKillAny ( com.android.server.am.ProcessSceneCleaner p0, miui.process.ProcessConfig p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessSceneCleaner;->handleKillAny(Lmiui/process/ProcessConfig;)V */
	 return;
} // .end method
static Boolean -$$Nest$mhandleSwipeKill ( com.android.server.am.ProcessSceneCleaner p0, miui.process.ProcessConfig p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = 	 /* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessSceneCleaner;->handleSwipeKill(Lmiui/process/ProcessConfig;)Z */
} // .end method
public com.android.server.am.ProcessSceneCleaner ( ) {
	 /* .locals 0 */
	 /* .param p1, "ams" # Lcom/android/server/am/ActivityManagerService; */
	 /* .line 41 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessCleanerBase;-><init>(Lcom/android/server/am/ActivityManagerService;)V */
	 /* .line 42 */
	 this.mAMS = p1;
	 /* .line 43 */
	 return;
} // .end method
private java.util.Map getKillPackageList ( android.util.ArrayMap p0 ) {
	 /* .locals 7 */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "(", */
	 /* "Landroid/util/ArrayMap<", */
	 /* "Ljava/lang/Integer;", */
	 /* "Ljava/util/List<", */
	 /* "Ljava/lang/String;", */
	 /* ">;>;)", */
	 /* "Ljava/util/Map<", */
	 /* "Ljava/lang/String;", */
	 /* "Ljava/lang/Integer;", */
	 /* ">;" */
	 /* } */
} // .end annotation
/* .line 269 */
/* .local p1, "killingPackageMaps":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/Integer;Ljava/util/List<Ljava/lang/String;>;>;" */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 270 */
/* .local v0, "killPackage":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;" */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = (( android.util.ArrayMap ) p1 ).size ( ); // invoke-virtual {p1}, Landroid/util/ArrayMap;->size()I
/* if-ge v1, v2, :cond_1 */
/* .line 271 */
(( android.util.ArrayMap ) p1 ).keyAt ( v1 ); // invoke-virtual {p1, v1}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/Integer; */
v2 = (( java.lang.Integer ) v2 ).intValue ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
/* .line 272 */
/* .local v2, "killLevel":I */
(( android.util.ArrayMap ) p1 ).valueAt ( v1 ); // invoke-virtual {p1, v1}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Ljava/util/List; */
/* .line 273 */
/* .local v3, "killingPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v4 = if ( v3 != null) { // if-eqz v3, :cond_0
/* if-lez v4, :cond_0 */
/* .line 274 */
v5 = } // :goto_1
if ( v5 != null) { // if-eqz v5, :cond_0
/* check-cast v5, Ljava/lang/String; */
/* .line 275 */
/* .local v5, "pkg":Ljava/lang/String; */
java.lang.Integer .valueOf ( v2 );
/* .line 276 */
} // .end local v5 # "pkg":Ljava/lang/String;
/* .line 270 */
} // .end local v2 # "killLevel":I
} // .end local v3 # "killingPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 279 */
} // .end local v1 # "i":I
} // :cond_1
} // .end method
private void handleKillAll ( miui.process.ProcessConfig p0 ) {
/* .locals 21 */
/* .param p1, "config" # Lmiui/process/ProcessConfig; */
/* .line 128 */
/* move-object/from16 v9, p0 */
/* move-object/from16 v10, p1 */
v0 = this.mPMS;
/* if-nez v0, :cond_0 */
/* .line 129 */
return;
/* .line 131 */
} // :cond_0
v11 = /* invoke-virtual/range {p1 ..p1}, Lmiui/process/ProcessConfig;->getPolicy()I */
/* .line 132 */
/* .local v11, "policy":I */
v0 = /* invoke-virtual/range {p1 ..p1}, Lmiui/process/ProcessConfig;->getPolicy()I */
(( com.android.server.am.ProcessSceneCleaner ) v9 ).getKillReason ( v0 ); // invoke-virtual {v9, v0}, Lcom/android/server/am/ProcessSceneCleaner;->getKillReason(I)Ljava/lang/String;
/* .line 133 */
/* .local v12, "reason":Ljava/lang/String; */
v0 = this.mAMS;
v0 = this.mActivityTaskManager;
com.android.server.wm.WindowProcessUtils .getPerceptibleRecentAppList ( v0 );
/* .line 135 */
/* .local v13, "fgTaskPackageMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/String;>;" */
v0 = this.mProcessPolicy;
(( com.android.server.am.ProcessSceneCleaner ) v9 ).getProcessPolicyWhiteList ( v10, v0 ); // invoke-virtual {v9, v10, v0}, Lcom/android/server/am/ProcessSceneCleaner;->getProcessPolicyWhiteList(Lmiui/process/ProcessConfig;Lcom/android/server/am/ProcessPolicy;)Ljava/util/List;
/* .line 136 */
/* .local v14, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
if ( v13 != null) { // if-eqz v13, :cond_1
if ( v14 != null) { // if-eqz v14, :cond_1
/* .line 137 */
/* .line 139 */
} // :cond_1
int v0 = 2; // const/4 v0, 0x2
/* if-ne v11, v0, :cond_2 */
/* .line 140 */
v0 = android.os.UserHandle .getCallingUserId ( );
v1 = this.mPMS;
(( com.android.server.am.ProcessSceneCleaner ) v9 ).removeAllTasks ( v0, v1 ); // invoke-virtual {v9, v0, v1}, Lcom/android/server/am/ProcessSceneCleaner;->removeAllTasks(ILcom/android/server/am/ProcessManagerService;)V
/* .line 142 */
} // :cond_2
v0 = this.mProcessPolicy;
(( com.android.server.am.ProcessSceneCleaner ) v9 ).removeTasksIfNeeded ( v10, v0, v14, v13 ); // invoke-virtual {v9, v10, v0, v14, v13}, Lcom/android/server/am/ProcessSceneCleaner;->removeTasksIfNeeded(Lmiui/process/ProcessConfig;Lcom/android/server/am/ProcessPolicy;Ljava/util/List;Ljava/util/Map;)V
/* .line 144 */
} // :goto_0
v0 = this.mSmartPowerService;
/* .line 145 */
/* .local v15, "runningAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;" */
(( java.util.ArrayList ) v15 ).iterator ( ); // invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
} // :goto_1
v0 = /* invoke-interface/range {v16 ..v16}, Ljava/util/Iterator;->hasNext()Z */
if ( v0 != null) { // if-eqz v0, :cond_8
/* invoke-interface/range {v16 ..v16}, Ljava/util/Iterator;->next()Ljava/lang/Object; */
/* move-object v8, v0 */
/* check-cast v8, Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 146 */
/* .local v8, "runningProcess":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
int v0 = 1; // const/4 v0, 0x1
/* .line 147 */
/* .local v0, "isForceStop":Z */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v8 ).getProcessRecord ( ); // invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;
/* .line 148 */
/* .local v17, "app":Lcom/android/server/am/ProcessRecord; */
v1 = /* invoke-virtual/range {p1 ..p1}, Lmiui/process/ProcessConfig;->getPolicy()I */
int v2 = 1; // const/4 v2, 0x1
/* if-ne v1, v2, :cond_5 */
/* .line 150 */
com.android.server.wm.MiuiSoScManagerStub .get ( );
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v8 ).getPackageName ( ); // invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;
v1 = (( com.android.server.wm.MiuiSoScManagerStub ) v1 ).isInSoScSingleMode ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/wm/MiuiSoScManagerStub;->isInSoScSingleMode(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 151 */
/* .line 153 */
} // :cond_3
v1 = this.mProcessPolicy;
(( com.android.server.am.ProcessPolicy ) v1 ).getOneKeyCleanWhiteList ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessPolicy;->getOneKeyCleanWhiteList()Ljava/util/HashMap;
/* .line 154 */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v8 ).getPackageName ( ); // invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;
/* .line 153 */
v1 = (( java.util.HashMap ) v1 ).containsKey ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 155 */
int v0 = 0; // const/4 v0, 0x0
/* .line 156 */
v1 = this.mProcessPolicy;
(( com.android.server.am.ProcessPolicy ) v1 ).getOneKeyCleanWhiteList ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessPolicy;->getOneKeyCleanWhiteList()Ljava/util/HashMap;
/* .line 157 */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v8 ).getPackageName ( ); // invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;
/* .line 156 */
(( java.util.HashMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/util/List; */
/* .line 157 */
v1 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v8 ).getProcessName ( ); // invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 158 */
/* .line 157 */
} // :cond_4
/* move/from16 v18, v0 */
/* .line 162 */
} // :cond_5
/* move/from16 v18, v0 */
} // .end local v0 # "isForceStop":Z
/* .local v18, "isForceStop":Z */
} // :goto_2
v0 = (( com.android.server.am.ProcessSceneCleaner ) v9 ).isCurrentProcessInBackup ( v8 ); // invoke-virtual {v9, v8}, Lcom/android/server/am/ProcessSceneCleaner;->isCurrentProcessInBackup(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z
/* if-nez v0, :cond_7 */
/* .line 163 */
v0 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v8 ).getPackageName ( ); // invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;
/* if-nez v0, :cond_6 */
/* .line 164 */
v5 = this.mPMS;
final String v6 = "ProcessSceneCleaner"; // const-string v6, "ProcessSceneCleaner"
v7 = this.mHandler;
v4 = this.mContext;
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, v17 */
/* move v2, v11 */
/* move-object v3, v12 */
/* move-object/from16 v19, v4 */
/* move/from16 v4, v18 */
/* move-object/from16 v20, v8 */
} // .end local v8 # "runningProcess":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
/* .local v20, "runningProcess":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* move-object/from16 v8, v19 */
/* invoke-virtual/range {v0 ..v8}, Lcom/android/server/am/ProcessSceneCleaner;->killOnce(Lcom/android/server/am/ProcessRecord;ILjava/lang/String;ZLcom/android/server/am/ProcessManagerService;Ljava/lang/String;Landroid/os/Handler;Landroid/content/Context;)V */
/* .line 163 */
} // .end local v20 # "runningProcess":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
/* .restart local v8 # "runningProcess":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
} // :cond_6
/* move-object/from16 v20, v8 */
} // .end local v8 # "runningProcess":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
/* .restart local v20 # "runningProcess":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 162 */
} // .end local v20 # "runningProcess":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
/* .restart local v8 # "runningProcess":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
} // :cond_7
/* move-object/from16 v20, v8 */
/* .line 166 */
} // .end local v8 # "runningProcess":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
} // .end local v17 # "app":Lcom/android/server/am/ProcessRecord;
} // .end local v18 # "isForceStop":Z
} // :goto_3
/* goto/16 :goto_1 */
/* .line 167 */
} // :cond_8
return;
} // .end method
private void handleKillAny ( miui.process.ProcessConfig p0 ) {
/* .locals 22 */
/* .param p1, "config" # Lmiui/process/ProcessConfig; */
/* .line 234 */
/* move-object/from16 v9, p0 */
v0 = this.mPMS;
/* if-nez v0, :cond_0 */
/* .line 235 */
return;
/* .line 237 */
} // :cond_0
v0 = /* invoke-virtual/range {p1 ..p1}, Lmiui/process/ProcessConfig;->isUserIdInvalid()Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 238 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "userId:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = /* invoke-virtual/range {p1 ..p1}, Lmiui/process/ProcessConfig;->getUserId()I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " is invalid"; // const-string v1, " is invalid"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "ProcessSceneCleaner"; // const-string v1, "ProcessSceneCleaner"
android.util.Slog .w ( v1,v0 );
/* .line 239 */
return;
/* .line 241 */
} // :cond_1
v10 = /* invoke-virtual/range {p1 ..p1}, Lmiui/process/ProcessConfig;->getPolicy()I */
/* .line 242 */
/* .local v10, "policy":I */
/* invoke-virtual/range {p1 ..p1}, Lmiui/process/ProcessConfig;->getKillingPackageMaps()Landroid/util/ArrayMap; */
/* .line 245 */
/* .local v11, "killingPackageMaps":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/Integer;Ljava/util/List<Ljava/lang/String;>;>;" */
/* move-object/from16 v12, p1 */
/* invoke-direct {v9, v11, v12}, Lcom/android/server/am/ProcessSceneCleaner;->removeTasksByPackages(Landroid/util/ArrayMap;Lmiui/process/ProcessConfig;)V */
/* .line 247 */
v0 = this.mSmartPowerService;
/* .line 248 */
/* .local v13, "runningAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;" */
/* invoke-direct {v9, v11}, Lcom/android/server/am/ProcessSceneCleaner;->getKillPackageList(Landroid/util/ArrayMap;)Ljava/util/Map; */
/* .line 249 */
/* .local v14, "killPackages":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;" */
/* .line 250 */
/* .local v15, "killPkg":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
(( java.util.ArrayList ) v13 ).iterator ( ); // invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
} // :goto_0
v0 = /* invoke-interface/range {v16 ..v16}, Ljava/util/Iterator;->hasNext()Z */
if ( v0 != null) { // if-eqz v0, :cond_6
/* invoke-interface/range {v16 ..v16}, Ljava/util/Iterator;->next()Ljava/lang/Object; */
/* move-object v8, v0 */
/* check-cast v8, Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 251 */
/* .local v8, "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v8 ).getProcessRecord ( ); // invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;
/* .line 252 */
/* .local v17, "app":Lcom/android/server/am/ProcessRecord; */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v8 ).getPackageName ( ); // invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;
/* .line 253 */
v0 = /* .local v7, "pkgName":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 254 */
v0 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v8 ).getAdj ( ); // invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I
/* if-lez v0, :cond_4 */
/* .line 255 */
v0 = (( com.android.server.am.ProcessSceneCleaner ) v9 ).isCurrentProcessInBackup ( v8 ); // invoke-virtual {v9, v8}, Lcom/android/server/am/ProcessSceneCleaner;->isCurrentProcessInBackup(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z
/* if-nez v0, :cond_3 */
/* .line 256 */
/* check-cast v0, Ljava/lang/Integer; */
v6 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* .line 257 */
/* .local v6, "killLevel":I */
/* const/16 v0, 0x64 */
/* if-eq v6, v0, :cond_2 */
/* .line 258 */
(( com.android.server.am.ProcessSceneCleaner ) v9 ).getKillReason ( v10 ); // invoke-virtual {v9, v10}, Lcom/android/server/am/ProcessSceneCleaner;->getKillReason(I)Ljava/lang/String;
v4 = this.mHandler;
v5 = this.mContext;
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, v17 */
/* move v3, v6 */
/* invoke-virtual/range {v0 ..v5}, Lcom/android/server/am/ProcessSceneCleaner;->killOnce(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;ILandroid/os/Handler;Landroid/content/Context;)V */
/* .line 260 */
} // :cond_2
v2 = /* invoke-virtual/range {p1 ..p1}, Lmiui/process/ProcessConfig;->getPolicy()I */
(( com.android.server.am.ProcessSceneCleaner ) v9 ).getKillReason ( v10 ); // invoke-virtual {v9, v10}, Lcom/android/server/am/ProcessSceneCleaner;->getKillReason(I)Ljava/lang/String;
int v4 = 1; // const/4 v4, 0x1
v5 = this.mPMS;
final String v18 = "ProcessSceneCleaner"; // const-string v18, "ProcessSceneCleaner"
v1 = this.mHandler;
v0 = this.mContext;
/* move-object/from16 v19, v0 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v20, v1 */
/* move-object/from16 v1, v17 */
/* move/from16 v21, v6 */
} // .end local v6 # "killLevel":I
/* .local v21, "killLevel":I */
/* move-object/from16 v6, v18 */
/* move-object/from16 v18, v7 */
} // .end local v7 # "pkgName":Ljava/lang/String;
/* .local v18, "pkgName":Ljava/lang/String; */
/* move-object/from16 v7, v20 */
/* move-object/from16 v20, v8 */
} // .end local v8 # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
/* .local v20, "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* move-object/from16 v8, v19 */
/* invoke-virtual/range {v0 ..v8}, Lcom/android/server/am/ProcessSceneCleaner;->killOnce(Lcom/android/server/am/ProcessRecord;ILjava/lang/String;ZLcom/android/server/am/ProcessManagerService;Ljava/lang/String;Landroid/os/Handler;Landroid/content/Context;)V */
/* .line 255 */
} // .end local v18 # "pkgName":Ljava/lang/String;
} // .end local v20 # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
} // .end local v21 # "killLevel":I
/* .restart local v7 # "pkgName":Ljava/lang/String; */
/* .restart local v8 # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
} // :cond_3
/* move-object/from16 v18, v7 */
/* move-object/from16 v20, v8 */
} // .end local v7 # "pkgName":Ljava/lang/String;
} // .end local v8 # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
/* .restart local v18 # "pkgName":Ljava/lang/String; */
/* .restart local v20 # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 254 */
} // .end local v18 # "pkgName":Ljava/lang/String;
} // .end local v20 # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
/* .restart local v7 # "pkgName":Ljava/lang/String; */
/* .restart local v8 # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
} // :cond_4
/* move-object/from16 v18, v7 */
/* move-object/from16 v20, v8 */
} // .end local v7 # "pkgName":Ljava/lang/String;
} // .end local v8 # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
/* .restart local v18 # "pkgName":Ljava/lang/String; */
/* .restart local v20 # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 253 */
} // .end local v18 # "pkgName":Ljava/lang/String;
} // .end local v20 # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
/* .restart local v7 # "pkgName":Ljava/lang/String; */
/* .restart local v8 # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
} // :cond_5
/* move-object/from16 v18, v7 */
/* move-object/from16 v20, v8 */
/* .line 264 */
} // .end local v7 # "pkgName":Ljava/lang/String;
} // .end local v8 # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
} // .end local v17 # "app":Lcom/android/server/am/ProcessRecord;
} // :goto_1
/* .line 265 */
} // :cond_6
return;
} // .end method
private Boolean handleSwipeKill ( miui.process.ProcessConfig p0 ) {
/* .locals 18 */
/* .param p1, "config" # Lmiui/process/ProcessConfig; */
/* .line 170 */
/* move-object/from16 v9, p0 */
v0 = /* invoke-virtual/range {p1 ..p1}, Lmiui/process/ProcessConfig;->isUserIdInvalid()Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_9 */
v0 = /* invoke-virtual/range {p1 ..p1}, Lmiui/process/ProcessConfig;->isTaskIdInvalid()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* move-object/from16 v14, p1 */
/* goto/16 :goto_3 */
/* .line 175 */
} // :cond_0
/* invoke-virtual/range {p1 ..p1}, Lmiui/process/ProcessConfig;->getKillingPackage()Ljava/lang/String; */
/* .line 176 */
/* .local v10, "packageName":Ljava/lang/String; */
v11 = /* invoke-virtual/range {p1 ..p1}, Lmiui/process/ProcessConfig;->getTaskId()I */
/* .line 177 */
/* .local v11, "taskId":I */
v0 = /* invoke-virtual/range {p1 ..p1}, Lmiui/process/ProcessConfig;->getPolicy()I */
(( com.android.server.am.ProcessSceneCleaner ) v9 ).getKillReason ( v0 ); // invoke-virtual {v9, v0}, Lcom/android/server/am/ProcessSceneCleaner;->getKillReason(I)Ljava/lang/String;
/* .line 180 */
/* .local v12, "killReason":Ljava/lang/String; */
v0 = /* invoke-virtual/range {p1 ..p1}, Lmiui/process/ProcessConfig;->isRemoveTaskNeeded()Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 181 */
v0 = /* invoke-virtual/range {p1 ..p1}, Lmiui/process/ProcessConfig;->getTaskId()I */
(( com.android.server.am.ProcessSceneCleaner ) v9 ).removeTaskIfNeeded ( v0 ); // invoke-virtual {v9, v0}, Lcom/android/server/am/ProcessSceneCleaner;->removeTaskIfNeeded(I)V
/* .line 184 */
} // :cond_1
v0 = this.mPMS;
v2 = /* invoke-virtual/range {p1 ..p1}, Lmiui/process/ProcessConfig;->getUserId()I */
(( com.android.server.am.ProcessManagerService ) v0 ).getProcessRecordList ( v10, v2 ); // invoke-virtual {v0, v10, v2}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordList(Ljava/lang/String;I)Ljava/util/List;
/* .line 185 */
/* .local v13, "infoList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;" */
v0 = if ( v13 != null) { // if-eqz v13, :cond_8
if ( v0 != null) { // if-eqz v0, :cond_2
/* move-object/from16 v14, p1 */
/* .line 188 */
} // :cond_2
v0 = /* invoke-direct {v9, v13, v11}, Lcom/android/server/am/ProcessSceneCleaner;->isAppHasOtherTask(Ljava/util/List;I)Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 189 */
/* move-object/from16 v14, p1 */
v0 = /* invoke-direct {v9, v11, v14}, Lcom/android/server/am/ProcessSceneCleaner;->killAppForHasOtherTask(ILmiui/process/ProcessConfig;)Z */
/* .line 191 */
} // :cond_3
/* move-object/from16 v14, p1 */
v0 = } // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_7
/* move-object v8, v0 */
/* check-cast v8, Lcom/android/server/am/ProcessRecord; */
/* .line 192 */
/* .local v8, "proc":Lcom/android/server/am/ProcessRecord; */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v0 != null) { // if-eqz v0, :cond_4
v0 = this.mServices;
v0 = (( com.android.server.am.ProcessServiceRecord ) v0 ).hasForegroundServices ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessServiceRecord;->hasForegroundServices()Z
/* if-nez v0, :cond_6 */
} // :cond_4
v0 = this.info;
v0 = this.packageName;
v1 = this.processName;
/* .line 193 */
v0 = (( com.android.server.am.ProcessSceneCleaner ) v9 ).isCurrentProcessInBackup ( v0, v1 ); // invoke-virtual {v9, v0, v1}, Lcom/android/server/am/ProcessSceneCleaner;->isCurrentProcessInBackup(Ljava/lang/String;Ljava/lang/String;)Z
/* if-nez v0, :cond_5 */
/* .line 194 */
v2 = /* invoke-virtual/range {p1 ..p1}, Lmiui/process/ProcessConfig;->getPolicy()I */
int v4 = 1; // const/4 v4, 0x1
v5 = this.mPMS;
final String v6 = "ProcessSceneCleaner"; // const-string v6, "ProcessSceneCleaner"
v7 = this.mHandler;
v3 = this.mContext;
/* move-object/from16 v0, p0 */
/* move-object v1, v8 */
/* move-object/from16 v16, v3 */
/* move-object v3, v12 */
/* move-object/from16 v17, v8 */
} // .end local v8 # "proc":Lcom/android/server/am/ProcessRecord;
/* .local v17, "proc":Lcom/android/server/am/ProcessRecord; */
/* move-object/from16 v8, v16 */
/* invoke-virtual/range {v0 ..v8}, Lcom/android/server/am/ProcessSceneCleaner;->killOnce(Lcom/android/server/am/ProcessRecord;ILjava/lang/String;ZLcom/android/server/am/ProcessManagerService;Ljava/lang/String;Landroid/os/Handler;Landroid/content/Context;)V */
/* .line 193 */
} // .end local v17 # "proc":Lcom/android/server/am/ProcessRecord;
/* .restart local v8 # "proc":Lcom/android/server/am/ProcessRecord; */
} // :cond_5
/* move-object/from16 v17, v8 */
/* .line 196 */
} // .end local v8 # "proc":Lcom/android/server/am/ProcessRecord;
} // :cond_6
} // :goto_1
/* .line 197 */
} // :cond_7
int v0 = 1; // const/4 v0, 0x1
/* .line 185 */
} // :cond_8
/* move-object/from16 v14, p1 */
/* .line 186 */
} // :goto_2
/* .line 170 */
} // .end local v10 # "packageName":Ljava/lang/String;
} // .end local v11 # "taskId":I
} // .end local v12 # "killReason":Ljava/lang/String;
} // .end local v13 # "infoList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
} // :cond_9
/* move-object/from16 v14, p1 */
/* .line 171 */
} // :goto_3
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "userId:" */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = /* invoke-virtual/range {p1 ..p1}, Lmiui/process/ProcessConfig;->getUserId()I */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " or taskId:"; // const-string v2, " or taskId:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 172 */
v2 = /* invoke-virtual/range {p1 ..p1}, Lmiui/process/ProcessConfig;->getTaskId()I */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " is invalid"; // const-string v2, " is invalid"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 171 */
final String v2 = "ProcessSceneCleaner"; // const-string v2, "ProcessSceneCleaner"
android.util.Slog .w ( v2,v0 );
/* .line 173 */
} // .end method
private Boolean isAppHasOtherTask ( java.util.List p0, Integer p1 ) {
/* .locals 4 */
/* .param p2, "taskId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/am/ProcessRecord;", */
/* ">;I)Z" */
/* } */
} // .end annotation
/* .line 201 */
/* .local p1, "infoList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;" */
int v0 = 0; // const/4 v0, 0x0
/* .line 202 */
/* .local v0, "appHasOtherTask":Z */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/android/server/am/ProcessRecord; */
/* .line 203 */
/* .local v2, "proc":Lcom/android/server/am/ProcessRecord; */
/* nop */
/* .line 204 */
(( com.android.server.am.ProcessRecord ) v2 ).getWindowProcessController ( ); // invoke-virtual {v2}, Lcom/android/server/am/ProcessRecord;->getWindowProcessController()Lcom/android/server/wm/WindowProcessController;
/* .line 203 */
v3 = com.android.server.wm.WindowProcessUtils .isProcessHasActivityInOtherTaskLocked ( v3,p2 );
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 205 */
int v0 = 1; // const/4 v0, 0x1
/* .line 207 */
} // .end local v2 # "proc":Lcom/android/server/am/ProcessRecord;
} // :cond_0
/* .line 208 */
} // :cond_1
} // .end method
private Boolean killAppForHasOtherTask ( Integer p0, miui.process.ProcessConfig p1 ) {
/* .locals 12 */
/* .param p1, "taskId" # I */
/* .param p2, "config" # Lmiui/process/ProcessConfig; */
/* .line 212 */
int v0 = 0; // const/4 v0, 0x0
/* .line 213 */
/* .local v0, "taskTopApp":Lcom/android/server/am/ProcessRecord; */
com.android.server.wm.WindowProcessUtils .getTaskTopApp ( p1 );
/* .line 214 */
/* .local v1, "wpc":Lcom/android/server/wm/WindowProcessController; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 215 */
v2 = this.mOwner;
/* move-object v0, v2 */
/* check-cast v0, Lcom/android/server/am/ProcessRecord; */
/* .line 217 */
} // :cond_0
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 219 */
/* nop */
/* .line 221 */
(( com.android.server.am.ProcessRecord ) v0 ).getWindowProcessController ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessRecord;->getWindowProcessController()Lcom/android/server/wm/WindowProcessController;
/* .line 220 */
v11 = com.android.server.wm.WindowProcessUtils .isProcessHasActivityInOtherTaskLocked ( v2,p1 );
/* .line 223 */
/* .local v11, "processHasOtherTask":Z */
/* if-nez v11, :cond_2 */
/* sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
v2 = this.mServices;
/* .line 224 */
v2 = (( com.android.server.am.ProcessServiceRecord ) v2 ).hasForegroundServices ( ); // invoke-virtual {v2}, Lcom/android/server/am/ProcessServiceRecord;->hasForegroundServices()Z
/* if-nez v2, :cond_2 */
} // :cond_1
v2 = this.info;
v2 = this.packageName;
v3 = this.processName;
/* .line 225 */
v2 = (( com.android.server.am.ProcessSceneCleaner ) p0 ).isCurrentProcessInBackup ( v2, v3 ); // invoke-virtual {p0, v2, v3}, Lcom/android/server/am/ProcessSceneCleaner;->isCurrentProcessInBackup(Ljava/lang/String;Ljava/lang/String;)Z
/* if-nez v2, :cond_2 */
/* .line 226 */
v4 = (( miui.process.ProcessConfig ) p2 ).getPolicy ( ); // invoke-virtual {p2}, Lmiui/process/ProcessConfig;->getPolicy()I
v2 = (( miui.process.ProcessConfig ) p2 ).getPolicy ( ); // invoke-virtual {p2}, Lmiui/process/ProcessConfig;->getPolicy()I
(( com.android.server.am.ProcessSceneCleaner ) p0 ).getKillReason ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/am/ProcessSceneCleaner;->getKillReason(I)Ljava/lang/String;
int v6 = 0; // const/4 v6, 0x0
v7 = this.mPMS;
final String v8 = "ProcessSceneCleaner"; // const-string v8, "ProcessSceneCleaner"
v9 = this.mHandler;
v10 = this.mContext;
/* move-object v2, p0 */
/* move-object v3, v0 */
/* invoke-virtual/range {v2 ..v10}, Lcom/android/server/am/ProcessSceneCleaner;->killOnce(Lcom/android/server/am/ProcessRecord;ILjava/lang/String;ZLcom/android/server/am/ProcessManagerService;Ljava/lang/String;Landroid/os/Handler;Landroid/content/Context;)V */
/* .line 230 */
} // .end local v11 # "processHasOtherTask":Z
} // :cond_2
int v2 = 1; // const/4 v2, 0x1
} // .end method
private void removeTasksByPackages ( android.util.ArrayMap p0, miui.process.ProcessConfig p1 ) {
/* .locals 5 */
/* .param p2, "config" # Lmiui/process/ProcessConfig; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;>;", */
/* "Lmiui/process/ProcessConfig;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 284 */
/* .local p1, "packageMaps":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/Integer;Ljava/util/List<Ljava/lang/String;>;>;" */
v0 = (( miui.process.ProcessConfig ) p2 ).isRemoveTaskNeeded ( ); // invoke-virtual {p2}, Lmiui/process/ProcessConfig;->isRemoveTaskNeeded()Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 285 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 286 */
/* .local v0, "removedTasksInPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = (( android.util.ArrayMap ) p1 ).size ( ); // invoke-virtual {p1}, Landroid/util/ArrayMap;->size()I
/* if-ge v1, v2, :cond_1 */
/* .line 287 */
(( android.util.ArrayMap ) p1 ).keyAt ( v1 ); // invoke-virtual {p1, v1}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/Integer; */
v2 = (( java.lang.Integer ) v2 ).intValue ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
/* .line 288 */
/* .local v2, "killLevel":I */
/* const/16 v3, 0x65 */
/* if-eq v2, v3, :cond_0 */
/* .line 289 */
java.lang.Integer .valueOf ( v2 );
(( android.util.ArrayMap ) p1 ).get ( v3 ); // invoke-virtual {p1, v3}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Ljava/util/List; */
/* .line 290 */
/* .local v3, "killedPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v4 = if ( v3 != null) { // if-eqz v3, :cond_0
/* if-nez v4, :cond_0 */
/* .line 291 */
/* .line 286 */
} // .end local v2 # "killLevel":I
} // .end local v3 # "killedPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 296 */
} // .end local v1 # "i":I
} // :cond_1
/* .line 297 */
/* .local v1, "pkgIterator":Ljava/util/Iterator; */
v2 = } // :goto_1
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 298 */
/* check-cast v2, Ljava/lang/String; */
/* .line 299 */
/* .local v2, "pkg":Ljava/lang/String; */
v3 = android.text.TextUtils .isEmpty ( v2 );
/* if-nez v3, :cond_2 */
v3 = this.mPMS;
v3 = (( com.android.server.am.ProcessSceneCleaner ) p0 ).isTrimMemoryEnable ( v2, v3 ); // invoke-virtual {p0, v2, v3}, Lcom/android/server/am/ProcessSceneCleaner;->isTrimMemoryEnable(Ljava/lang/String;Lcom/android/server/am/ProcessManagerService;)Z
/* if-nez v3, :cond_2 */
/* .line 300 */
/* .line 302 */
} // .end local v2 # "pkg":Ljava/lang/String;
} // :cond_2
/* .line 303 */
} // :cond_3
v2 = (( miui.process.ProcessConfig ) p2 ).getUserId ( ); // invoke-virtual {p2}, Lmiui/process/ProcessConfig;->getUserId()I
v3 = this.mProcessPolicy;
(( com.android.server.am.ProcessSceneCleaner ) p0 ).removeTasksInPackages ( v0, v2, v3 ); // invoke-virtual {p0, v0, v2, v3}, Lcom/android/server/am/ProcessSceneCleaner;->removeTasksInPackages(Ljava/util/List;ILcom/android/server/am/ProcessPolicy;)V
/* .line 305 */
} // .end local v0 # "removedTasksInPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v1 # "pkgIterator":Ljava/util/Iterator;
} // :cond_4
return;
} // .end method
/* # virtual methods */
android.os.Message createMessage ( Integer p0, miui.process.ProcessConfig p1, android.os.Handler p2 ) {
/* .locals 1 */
/* .param p1, "event" # I */
/* .param p2, "config" # Lmiui/process/ProcessConfig; */
/* .param p3, "handler" # Landroid/os/Handler; */
/* .line 308 */
(( android.os.Handler ) p3 ).obtainMessage ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 309 */
/* .local v0, "msg":Landroid/os/Message; */
this.obj = p2;
/* .line 310 */
} // .end method
public Boolean sceneKillProcess ( miui.process.ProcessConfig p0 ) {
/* .locals 3 */
/* .param p1, "config" # Lmiui/process/ProcessConfig; */
/* .line 55 */
v0 = this.mHandler;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_3
/* if-nez p1, :cond_0 */
/* .line 58 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 59 */
/* .local v0, "success":Z */
v2 = (( miui.process.ProcessConfig ) p1 ).getPolicy ( ); // invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getPolicy()I
/* packed-switch v2, :pswitch_data_0 */
/* :pswitch_0 */
/* .line 68 */
/* :pswitch_1 */
(( miui.process.ProcessConfig ) p1 ).getKillingPackage ( ); // invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getKillingPackage()Ljava/lang/String;
v2 = android.text.TextUtils .isEmpty ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 69 */
/* .line 71 */
} // :cond_1
v1 = this.mHandler;
int v2 = 2; // const/4 v2, 0x2
(( com.android.server.am.ProcessSceneCleaner ) p0 ).createMessage ( v2, p1, v1 ); // invoke-virtual {p0, v2, p1, v1}, Lcom/android/server/am/ProcessSceneCleaner;->createMessage(ILmiui/process/ProcessConfig;Landroid/os/Handler;)Landroid/os/Message;
(( com.android.server.am.ProcessSceneCleaner$H ) v1 ).sendMessage ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessSceneCleaner$H;->sendMessage(Landroid/os/Message;)Z
/* .line 72 */
int v0 = 1; // const/4 v0, 0x1
/* .line 73 */
/* .line 77 */
/* :pswitch_2 */
(( miui.process.ProcessConfig ) p1 ).getKillingPackageMaps ( ); // invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getKillingPackageMaps()Landroid/util/ArrayMap;
/* if-nez v2, :cond_2 */
/* .line 78 */
/* .line 80 */
} // :cond_2
v1 = this.mHandler;
int v2 = 3; // const/4 v2, 0x3
(( com.android.server.am.ProcessSceneCleaner ) p0 ).createMessage ( v2, p1, v1 ); // invoke-virtual {p0, v2, p1, v1}, Lcom/android/server/am/ProcessSceneCleaner;->createMessage(ILmiui/process/ProcessConfig;Landroid/os/Handler;)Landroid/os/Message;
(( com.android.server.am.ProcessSceneCleaner$H ) v1 ).sendMessage ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessSceneCleaner$H;->sendMessage(Landroid/os/Message;)Z
/* .line 81 */
int v0 = 1; // const/4 v0, 0x1
/* .line 82 */
/* .line 64 */
/* :pswitch_3 */
v1 = this.mHandler;
int v2 = 1; // const/4 v2, 0x1
(( com.android.server.am.ProcessSceneCleaner ) p0 ).createMessage ( v2, p1, v1 ); // invoke-virtual {p0, v2, p1, v1}, Lcom/android/server/am/ProcessSceneCleaner;->createMessage(ILmiui/process/ProcessConfig;Landroid/os/Handler;)Landroid/os/Message;
(( com.android.server.am.ProcessSceneCleaner$H ) v1 ).sendMessage ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessSceneCleaner$H;->sendMessage(Landroid/os/Message;)Z
/* .line 65 */
int v0 = 1; // const/4 v0, 0x1
/* .line 66 */
/* nop */
/* .line 86 */
} // :goto_0
/* .line 56 */
} // .end local v0 # "success":Z
} // :cond_3
} // :goto_1
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_3 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_3 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_2 */
} // .end packed-switch
} // .end method
public void systemReady ( com.android.server.am.ProcessManagerService p0, android.os.Looper p1, android.content.Context p2 ) {
/* .locals 1 */
/* .param p1, "pms" # Lcom/android/server/am/ProcessManagerService; */
/* .param p2, "myLooper" # Landroid/os/Looper; */
/* .param p3, "context" # Landroid/content/Context; */
/* .line 46 */
/* invoke-super {p0, p3, p1}, Lcom/android/server/am/ProcessCleanerBase;->systemReady(Landroid/content/Context;Lcom/android/server/am/ProcessManagerService;)V */
/* .line 47 */
this.mContext = p3;
/* .line 48 */
this.mPMS = p1;
/* .line 49 */
(( com.android.server.am.ProcessManagerService ) p1 ).getProcessPolicy ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessManagerService;->getProcessPolicy()Lcom/android/server/am/ProcessPolicy;
this.mProcessPolicy = v0;
/* .line 50 */
com.android.server.am.SystemPressureController .getInstance ( );
this.mSysPressureCtrl = v0;
/* .line 51 */
/* new-instance v0, Lcom/android/server/am/ProcessSceneCleaner$H; */
/* invoke-direct {v0, p0, p2}, Lcom/android/server/am/ProcessSceneCleaner$H;-><init>(Lcom/android/server/am/ProcessSceneCleaner;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 52 */
return;
} // .end method
