class com.android.server.am.AppStateManager$1 extends android.content.BroadcastReceiver {
	 /* .source "AppStateManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/AppStateManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.am.AppStateManager this$0; //synthetic
/* # direct methods */
 com.android.server.am.AppStateManager$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/AppStateManager; */
/* .line 238 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 241 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 242 */
/* .local v0, "action":Ljava/lang/String; */
final String v1 = "android.intent.action.USER_REMOVED"; // const-string v1, "android.intent.action.USER_REMOVED"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 243 */
	 /* nop */
	 /* .line 244 */
	 final String v1 = "android.intent.extra.user_handle"; // const-string v1, "android.intent.extra.user_handle"
	 /* const/16 v2, -0x2710 */
	 v1 = 	 (( android.content.Intent ) p2 ).getIntExtra ( v1, v2 ); // invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
	 /* .line 245 */
	 /* .local v1, "userId":I */
	 /* if-eq v1, v2, :cond_0 */
	 /* .line 246 */
	 v2 = this.this$0;
	 com.android.server.am.AppStateManager .-$$Nest$fgetmMainHandler ( v2 );
	 int v3 = 3; // const/4 v3, 0x3
	 java.lang.Integer .valueOf ( v1 );
	 (( com.android.server.am.AppStateManager$MainHandler ) v2 ).obtainMessage ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lcom/android/server/am/AppStateManager$MainHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
	 /* .line 247 */
	 /* .local v2, "msg":Landroid/os/Message; */
	 v3 = this.this$0;
	 com.android.server.am.AppStateManager .-$$Nest$fgetmMainHandler ( v3 );
	 (( com.android.server.am.AppStateManager$MainHandler ) v3 ).sendMessage ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/server/am/AppStateManager$MainHandler;->sendMessage(Landroid/os/Message;)Z
	 /* .line 250 */
} // .end local v1 # "userId":I
} // .end local v2 # "msg":Landroid/os/Message;
} // :cond_0
return;
} // .end method
