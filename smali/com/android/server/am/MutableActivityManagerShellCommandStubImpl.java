public class com.android.server.am.MutableActivityManagerShellCommandStubImpl implements com.android.server.am.MutableActivityManagerShellCommandStub {
	 /* .source "MutableActivityManagerShellCommandStubImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final Integer DEFAULT_MAX_SHELL_SYNC_BROADCAST;
	 private static final java.lang.String PROPERTIES_MAX_SHELL_SYNC_BROADCAST;
	 private static java.util.concurrent.atomic.AtomicInteger mShellSyncBroadcast;
	 /* # instance fields */
	 com.android.server.am.ActivityManagerShellCommand activityManagerShellCommand;
	 /* # direct methods */
	 static com.android.server.am.MutableActivityManagerShellCommandStubImpl ( ) {
		 /* .locals 2 */
		 /* .line 20 */
		 /* new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger; */
		 int v1 = 0; // const/4 v1, 0x0
		 /* invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V */
		 return;
	 } // .end method
	 public com.android.server.am.MutableActivityManagerShellCommandStubImpl ( ) {
		 /* .locals 0 */
		 /* .line 14 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Integer addLog ( java.io.PrintWriter p0, java.lang.String p1 ) {
		 /* .locals 4 */
		 /* .param p1, "pw" # Ljava/io/PrintWriter; */
		 /* .param p2, "cmd" # Ljava/lang/String; */
		 /* .annotation system Ldalvik/annotation/Throws; */
		 /* value = { */
		 /* Landroid/os/RemoteException; */
		 /* } */
	 } // .end annotation
	 /* .line 31 */
	 v0 = 	 (( java.lang.String ) p2 ).hashCode ( ); // invoke-virtual {p2}, Ljava/lang/String;->hashCode()I
	 int v1 = 1; // const/4 v1, 0x1
	 int v2 = 0; // const/4 v2, 0x0
	 int v3 = -1; // const/4 v3, -0x1
	 /* sparse-switch v0, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
final String v0 = "app-logging"; // const-string v0, "app-logging"
v0 = (( java.lang.String ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* move v0, v1 */
	 /* :sswitch_1 */
	 final String v0 = "move-to-privatecast"; // const-string v0, "move-to-privatecast"
	 v0 = 	 (( java.lang.String ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 int v0 = 4; // const/4 v0, 0x4
		 /* :sswitch_2 */
		 final String v0 = "logging"; // const-string v0, "logging"
		 v0 = 		 (( java.lang.String ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* move v0, v2 */
			 /* :sswitch_3 */
			 final String v0 = "move-to-cast"; // const-string v0, "move-to-cast"
			 v0 = 			 (( java.lang.String ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 if ( v0 != null) { // if-eqz v0, :cond_0
				 int v0 = 2; // const/4 v0, 0x2
				 /* :sswitch_4 */
				 final String v0 = "exit-cast"; // const-string v0, "exit-cast"
				 v0 = 				 (( java.lang.String ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
				 if ( v0 != null) { // if-eqz v0, :cond_0
					 int v0 = 3; // const/4 v0, 0x3
					 /* :sswitch_5 */
					 final String v0 = "exit-privatecast"; // const-string v0, "exit-privatecast"
					 v0 = 					 (( java.lang.String ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
					 if ( v0 != null) { // if-eqz v0, :cond_0
						 int v0 = 5; // const/4 v0, 0x5
					 } // :goto_0
					 /* move v0, v3 */
				 } // :goto_1
				 /* packed-switch v0, :pswitch_data_0 */
				 /* .line 57 */
				 /* .line 53 */
				 /* :pswitch_0 */
				 v0 = this.activityManagerShellCommand;
				 v0 = this.mWindowInterface;
				 /* .line 54 */
				 /* .line 49 */
				 /* :pswitch_1 */
				 v0 = this.activityManagerShellCommand;
				 v0 = this.mWindowInterface;
				 /* .line 50 */
				 /* .line 45 */
				 /* :pswitch_2 */
				 v0 = this.activityManagerShellCommand;
				 v0 = this.mTaskInterface;
				 /* .line 46 */
				 /* .line 41 */
				 /* :pswitch_3 */
				 v0 = this.activityManagerShellCommand;
				 v0 = this.mTaskInterface;
				 /* .line 42 */
				 /* .line 36 */
				 /* :pswitch_4 */
				 (( com.android.server.am.MutableActivityManagerShellCommandStubImpl ) p0 ).runAppLogging ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/MutableActivityManagerShellCommandStubImpl;->runAppLogging(Ljava/io/PrintWriter;)V
				 /* .line 37 */
				 /* .line 33 */
				 /* :pswitch_5 */
				 (( com.android.server.am.MutableActivityManagerShellCommandStubImpl ) p0 ).runLogging ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/MutableActivityManagerShellCommandStubImpl;->runLogging(Ljava/io/PrintWriter;)V
				 /* .line 34 */
				 /* :sswitch_data_0 */
				 /* .sparse-switch */
				 /* -0x5b7dcaed -> :sswitch_5 */
				 /* -0x546a1972 -> :sswitch_4 */
				 /* -0x165edeab -> :sswitch_3 */
				 /* 0x1466cb5f -> :sswitch_2 */
				 /* 0x584b9c6c -> :sswitch_1 */
				 /* 0x7fb577b3 -> :sswitch_0 */
			 } // .end sparse-switch
			 /* :pswitch_data_0 */
			 /* .packed-switch 0x0 */
			 /* :pswitch_5 */
			 /* :pswitch_4 */
			 /* :pswitch_3 */
			 /* :pswitch_2 */
			 /* :pswitch_1 */
			 /* :pswitch_0 */
		 } // .end packed-switch
	 } // .end method
	 public void init ( com.android.server.am.ActivityManagerShellCommand p0 ) {
		 /* .locals 0 */
		 /* .param p1, "activityManagerShellCommand" # Lcom/android/server/am/ActivityManagerShellCommand; */
		 /* .line 26 */
		 this.activityManagerShellCommand = p1;
		 /* .line 27 */
		 return;
	 } // .end method
	 public void runAppLogging ( java.io.PrintWriter p0 ) {
		 /* .locals 6 */
		 /* .param p1, "pw" # Ljava/io/PrintWriter; */
		 /* .annotation system Ldalvik/annotation/Throws; */
		 /* value = { */
		 /* Landroid/os/RemoteException; */
		 /* } */
	 } // .end annotation
	 /* .line 74 */
	 v0 = this.activityManagerShellCommand;
	 (( com.android.server.am.ActivityManagerShellCommand ) v0 ).getNextArgRequired ( ); // invoke-virtual {v0}, Lcom/android/server/am/ActivityManagerShellCommand;->getNextArgRequired()Ljava/lang/String;
	 /* .line 75 */
	 /* .local v0, "processName":Ljava/lang/String; */
	 v1 = this.activityManagerShellCommand;
	 (( com.android.server.am.ActivityManagerShellCommand ) v1 ).getNextArgRequired ( ); // invoke-virtual {v1}, Lcom/android/server/am/ActivityManagerShellCommand;->getNextArgRequired()Ljava/lang/String;
	 v1 = 	 java.lang.Integer .parseInt ( v1 );
	 /* .line 76 */
	 /* .local v1, "uid":I */
	 v2 = this.activityManagerShellCommand;
	 (( com.android.server.am.ActivityManagerShellCommand ) v2 ).getNextArgRequired ( ); // invoke-virtual {v2}, Lcom/android/server/am/ActivityManagerShellCommand;->getNextArgRequired()Ljava/lang/String;
	 /* .line 77 */
	 /* .local v2, "cmd":Ljava/lang/String; */
	 final String v3 = "enable-text"; // const-string v3, "enable-text"
	 v4 = 	 (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 /* if-nez v4, :cond_0 */
	 final String v4 = "disable-text"; // const-string v4, "disable-text"
	 v4 = 	 (( java.lang.String ) v4 ).equals ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 /* if-nez v4, :cond_0 */
	 /* .line 78 */
	 final String v3 = "Error: wrong args , must be enable-text or disable-text"; // const-string v3, "Error: wrong args , must be enable-text or disable-text"
	 (( java.io.PrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
	 /* .line 79 */
	 return;
	 /* .line 81 */
} // :cond_0
v3 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 83 */
/* .local v3, "enable":Z */
} // :goto_0
v4 = this.activityManagerShellCommand;
(( com.android.server.am.ActivityManagerShellCommand ) v4 ).getNextArg ( ); // invoke-virtual {v4}, Lcom/android/server/am/ActivityManagerShellCommand;->getNextArg()Ljava/lang/String;
/* move-object v5, v4 */
/* .local v5, "config":Ljava/lang/String; */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 84 */
v4 = this.activityManagerShellCommand;
v4 = this.mInterface;
/* .line 86 */
} // :cond_1
return;
} // .end method
public void runLogging ( java.io.PrintWriter p0 ) {
/* .locals 4 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 61 */
v0 = this.activityManagerShellCommand;
(( com.android.server.am.ActivityManagerShellCommand ) v0 ).getNextArgRequired ( ); // invoke-virtual {v0}, Lcom/android/server/am/ActivityManagerShellCommand;->getNextArgRequired()Ljava/lang/String;
/* .line 62 */
/* .local v0, "cmd":Ljava/lang/String; */
final String v1 = "enable-text"; // const-string v1, "enable-text"
v2 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_0 */
final String v2 = "disable-text"; // const-string v2, "disable-text"
v2 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_0 */
/* .line 63 */
final String v1 = "Error: wrong args , must be enable-text or disable-text"; // const-string v1, "Error: wrong args , must be enable-text or disable-text"
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 64 */
return;
/* .line 66 */
} // :cond_0
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 68 */
/* .local v1, "enable":Z */
} // :goto_0
v2 = this.activityManagerShellCommand;
(( com.android.server.am.ActivityManagerShellCommand ) v2 ).getNextArg ( ); // invoke-virtual {v2}, Lcom/android/server/am/ActivityManagerShellCommand;->getNextArg()Ljava/lang/String;
/* move-object v3, v2 */
/* .local v3, "config":Ljava/lang/String; */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 69 */
v2 = this.activityManagerShellCommand;
v2 = this.mInterface;
/* .line 71 */
} // :cond_1
return;
} // .end method
public Boolean waitForFinishIfNeeded ( com.android.server.am.ActivityManagerShellCommand$IntentReceiver p0 ) {
/* .locals 3 */
/* .param p1, "receiver" # Lcom/android/server/am/ActivityManagerShellCommand$IntentReceiver; */
/* .line 90 */
v0 = android.os.Build .isDebuggable ( );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 91 */
v0 = com.android.server.am.MutableActivityManagerShellCommandStubImpl.mShellSyncBroadcast;
v0 = (( java.util.concurrent.atomic.AtomicInteger ) v0 ).incrementAndGet ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I
/* .line 92 */
final String v1 = "persist.sys.max_shell_sync_broadcast"; // const-string v1, "persist.sys.max_shell_sync_broadcast"
int v2 = 5; // const/4 v2, 0x5
v1 = android.os.SystemProperties .getInt ( v1,v2 );
/* if-gt v0, v1, :cond_0 */
/* .line 94 */
(( com.android.server.am.ActivityManagerShellCommand$IntentReceiver ) p1 ).waitForFinish ( ); // invoke-virtual {p1}, Lcom/android/server/am/ActivityManagerShellCommand$IntentReceiver;->waitForFinish()V
/* .line 96 */
} // :cond_0
v0 = com.android.server.am.MutableActivityManagerShellCommandStubImpl.mShellSyncBroadcast;
(( java.util.concurrent.atomic.AtomicInteger ) v0 ).decrementAndGet ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I
/* .line 98 */
} // :cond_1
(( com.android.server.am.ActivityManagerShellCommand$IntentReceiver ) p1 ).waitForFinish ( ); // invoke-virtual {p1}, Lcom/android/server/am/ActivityManagerShellCommand$IntentReceiver;->waitForFinish()V
/* .line 100 */
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
