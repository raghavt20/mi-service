.class Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;
.super Ljava/lang/Object;
.source "MiuiMemReclaimer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/MiuiMemReclaimer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CompactProcInfo"
.end annotation


# instance fields
.field public final action:Ljava/lang/String;

.field public compactDurationMillis:J

.field public lastCompactTimeMillis:J

.field public final proc:Lcom/miui/server/smartpower/IAppState$IRunningProcess;

.field public rss:[J

.field public rssAfter:[J

.field public rssDiff:[J


# direct methods
.method static bridge synthetic -$$Nest$mcomputeRssDiff(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->computeRssDiff(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetPid(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)I
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->getPid()I

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mgetProcessName(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->getProcessName()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$misRssValid(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->isRssValid()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$smgenCompactAction(II)Ljava/lang/String;
    .locals 0

    invoke-static {p0, p1}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->genCompactAction(II)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)V
    .locals 3
    .param p1, "proc"    # Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .param p2, "mode"    # I

    .line 656
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 652
    const/4 v0, 0x4

    new-array v1, v0, [J

    fill-array-data v1, :array_0

    iput-object v1, p0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rssDiff:[J

    .line 653
    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->lastCompactTimeMillis:J

    .line 654
    iput-wide v1, p0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->compactDurationMillis:J

    .line 657
    iput-object p1, p0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->proc:Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 658
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAmsProcState()I

    move-result v1

    invoke-static {v1, p2}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->genCompactAction(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->action:Ljava/lang/String;

    .line 659
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPid()I

    move-result v1

    if-lez v1, :cond_0

    .line 660
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->getRss(I)[J

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rss:[J

    goto :goto_0

    .line 662
    :cond_0
    new-array v0, v0, [J

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rss:[J

    .line 664
    :goto_0
    return-void

    :array_0
    .array-data 8
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_1
    .array-data 8
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method private computeRssDiff(Z)V
    .locals 8
    .param p1, "isCompacted"    # Z

    .line 696
    if-nez p1, :cond_0

    .line 697
    iget-object v0, p0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rss:[J

    iput-object v0, p0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rssAfter:[J

    .line 698
    return-void

    .line 700
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->proc:Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    invoke-virtual {v0}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->getRss(I)[J

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rssAfter:[J

    .line 701
    iget-object v1, p0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rssDiff:[J

    iget-object v2, p0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rss:[J

    const/4 v3, 0x0

    aget-wide v4, v2, v3

    aget-wide v6, v0, v3

    sub-long/2addr v4, v6

    aput-wide v4, v1, v3

    .line 702
    const/4 v3, 0x1

    aget-wide v4, v2, v3

    aget-wide v6, v0, v3

    sub-long/2addr v4, v6

    aput-wide v4, v1, v3

    .line 703
    const/4 v3, 0x2

    aget-wide v4, v2, v3

    aget-wide v6, v0, v3

    sub-long/2addr v4, v6

    aput-wide v4, v1, v3

    .line 704
    const/4 v3, 0x3

    aget-wide v4, v2, v3

    aget-wide v6, v0, v3

    sub-long/2addr v4, v6

    aput-wide v4, v1, v3

    .line 705
    iget-object v0, p0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->proc:Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    invoke-virtual {v0}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->updatePss()V

    .line 706
    return-void
.end method

.method private static genCompactAction(II)Ljava/lang/String;
    .locals 1
    .param p0, "procState"    # I
    .param p1, "mode"    # I

    .line 675
    invoke-static {p0}, Lcom/android/server/am/OomAdjusterImpl;->isCacheProcessState(I)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    const/16 v0, 0xf

    if-ne p0, v0, :cond_0

    goto :goto_0

    .line 680
    :cond_0
    const-string v0, "anon"

    return-object v0

    .line 678
    :cond_1
    :goto_0
    const-string v0, "all"

    return-object v0
.end method

.method private getPid()I
    .locals 1

    .line 667
    iget-object v0, p0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->proc:Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    invoke-virtual {v0}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPid()I

    move-result v0

    return v0
.end method

.method private getProcessName()Ljava/lang/String;
    .locals 1

    .line 671
    iget-object v0, p0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->proc:Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    invoke-virtual {v0}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private isRssValid()Z
    .locals 8

    .line 685
    iget-object v0, p0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rss:[J

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 686
    return v1

    .line 688
    :cond_0
    aget-wide v2, v0, v1

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    const/4 v3, 0x1

    if-nez v2, :cond_1

    aget-wide v6, v0, v3

    cmp-long v2, v6, v4

    if-nez v2, :cond_1

    const/4 v2, 0x2

    aget-wide v6, v0, v2

    cmp-long v2, v6, v4

    if-nez v2, :cond_1

    const/4 v2, 0x3

    aget-wide v6, v0, v2

    cmp-long v0, v6, v4

    if-nez v0, :cond_1

    .line 690
    return v1

    .line 692
    :cond_1
    return v3
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 23

    .line 709
    move-object/from16 v0, p0

    const/4 v1, 0x0

    .line 710
    .local v1, "date":Ljava/lang/String;
    iget-wide v2, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->lastCompactTimeMillis:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 711
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string/jumbo v3, "yy-MM-dd HH:mm:ss.SS"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 712
    .local v2, "dateformat":Ljava/text/SimpleDateFormat;
    iget-wide v3, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->lastCompactTimeMillis:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 714
    .end local v2    # "dateformat":Ljava/text/SimpleDateFormat;
    :cond_0
    iget-object v2, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->proc:Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 716
    invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPid()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v2, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->proc:Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;

    move-result-object v4

    iget-object v2, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->proc:Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUid()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget-object v2, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->proc:Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iget-object v2, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rss:[J

    const/4 v15, 0x0

    aget-wide v7, v2, v15

    .line 717
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    iget-object v2, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rss:[J

    const/16 v16, 0x1

    aget-wide v8, v2, v16

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    iget-object v2, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rss:[J

    const/16 v17, 0x2

    aget-wide v9, v2, v17

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    iget-object v2, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rss:[J

    const/16 v18, 0x3

    aget-wide v10, v2, v18

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    iget-object v2, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rssAfter:[J

    aget-wide v11, v2, v15

    .line 718
    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    iget-object v2, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rssAfter:[J

    aget-wide v12, v2, v16

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    iget-object v2, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rssAfter:[J

    aget-wide v13, v2, v17

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    iget-object v2, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rssAfter:[J

    aget-wide v19, v2, v18

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    iget-object v2, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rssDiff:[J

    aget-wide v19, v2, v15

    .line 719
    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    iget-object v2, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rssDiff:[J

    aget-wide v19, v2, v16

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    iget-object v2, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rssDiff:[J

    aget-wide v19, v2, v17

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    iget-object v2, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rssDiff:[J

    aget-wide v18, v2, v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    move-object/from16 v22, v3

    iget-wide v2, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->compactDurationMillis:J

    .line 720
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    iget-object v2, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->action:Ljava/lang/String;

    move-object/from16 v21, v2

    move-object/from16 v19, v1

    move-object/from16 v3, v22

    filled-new-array/range {v3 .. v21}, [Ljava/lang/Object;

    move-result-object v2

    .line 714
    const-string v3, "%d %s uid:%d adj:%d rss[%d, %d, %d, %d] rssAfter[%d, %d, %d, %d] rssDiff[%d, %d, %d, %d] lastCompactTime:%s compactDuration:%dms action:%s"

    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
