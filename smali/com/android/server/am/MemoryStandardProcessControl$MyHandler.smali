.class Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;
.super Landroid/os/Handler;
.source "MemoryStandardProcessControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/MemoryStandardProcessControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/MemoryStandardProcessControl;


# direct methods
.method public constructor <init>(Lcom/android/server/am/MemoryStandardProcessControl;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 813
    iput-object p1, p0, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    .line 814
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 815
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .line 819
    iget v0, p1, Landroid/os/Message;->what:I

    const-string v1, "MemoryStandardProcessControl"

    packed-switch v0, :pswitch_data_0

    goto :goto_1

    .line 833
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/android/server/am/MemoryStandardProcessControl;->-$$Nest$mhandleAppHeapWhenBackground(Lcom/android/server/am/MemoryStandardProcessControl;ILjava/lang/String;)V

    .line 834
    goto :goto_1

    .line 841
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    iget-object v0, v0, Lcom/android/server/am/MemoryStandardProcessControl;->mMemoryStandardMap:Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;

    invoke-virtual {v0}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 842
    iget-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    iget-object v0, v0, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapStandard:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 836
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    iget-object v0, v0, Lcom/android/server/am/MemoryStandardProcessControl;->mWarnedAppMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;

    .line 837
    .local v2, "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
    invoke-virtual {v2}, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 838
    .end local v2    # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
    goto :goto_0

    .line 839
    :cond_0
    goto :goto_1

    .line 830
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1}, Lcom/android/server/am/MemoryStandardProcessControl;->-$$Nest$mhandleMemPressure(Lcom/android/server/am/MemoryStandardProcessControl;I)V

    .line 831
    goto :goto_1

    .line 827
    :pswitch_4
    iget-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryStandardProcessControl;->killProcess(Z)V

    .line 828
    goto :goto_1

    .line 824
    :pswitch_5
    iget-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryStandardProcessControl;->killProcess(Z)V

    .line 825
    goto :goto_1

    .line 821
    :pswitch_6
    iget-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    invoke-virtual {v0}, Lcom/android/server/am/MemoryStandardProcessControl;->periodicMemoryDetection()V

    .line 822
    nop

    .line 846
    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
