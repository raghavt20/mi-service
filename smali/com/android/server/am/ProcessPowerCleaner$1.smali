.class Lcom/android/server/am/ProcessPowerCleaner$1;
.super Landroid/database/ContentObserver;
.source "ProcessPowerCleaner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/ProcessPowerCleaner;->registerCloudObserver(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ProcessPowerCleaner;


# direct methods
.method constructor <init>(Lcom/android/server/am/ProcessPowerCleaner;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/ProcessPowerCleaner;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 345
    iput-object p1, p0, Lcom/android/server/am/ProcessPowerCleaner$1;->this$0:Lcom/android/server/am/ProcessPowerCleaner;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 2
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 348
    const-string v0, "perf_proc_power"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Lcom/android/server/am/ProcessPowerCleaner$1;->this$0:Lcom/android/server/am/ProcessPowerCleaner;

    invoke-static {v0}, Lcom/android/server/am/ProcessPowerCleaner;->-$$Nest$fgetmContext(Lcom/android/server/am/ProcessPowerCleaner;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/am/ProcessPowerCleaner;->-$$Nest$mupdateCloudControlParas(Lcom/android/server/am/ProcessPowerCleaner;Landroid/content/Context;)V

    .line 351
    :cond_0
    return-void
.end method
