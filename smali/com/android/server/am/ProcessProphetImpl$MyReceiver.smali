.class public Lcom/android/server/am/ProcessProphetImpl$MyReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ProcessProphetImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ProcessProphetImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MyReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ProcessProphetImpl;


# direct methods
.method public constructor <init>(Lcom/android/server/am/ProcessProphetImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/ProcessProphetImpl;

    .line 1061
    iput-object p1, p0, Lcom/android/server/am/ProcessProphetImpl$MyReceiver;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 1064
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1065
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1066
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetImpl$MyReceiver;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    iget-object v1, v1, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl$MyReceiver;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    iget-object v2, v2, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1067
    invoke-static {}, Lcom/android/server/am/ProcessProphetImpl;->-$$Nest$sfgetDEBUG()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1068
    const-string v1, "ProcessProphet"

    const-string/jumbo v2, "user present."

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1070
    :cond_0
    const-string v1, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1072
    const-string v1, "android.bluetooth.profile.extra.STATE"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 1073
    .local v1, "state":I
    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 1074
    const-string v3, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/bluetooth/BluetoothDevice;

    .line 1075
    .local v3, "device":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    .line 1076
    .local v4, "deviceName":Ljava/lang/String;
    iget-object v5, p0, Lcom/android/server/am/ProcessProphetImpl$MyReceiver;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    iget-object v5, v5, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    invoke-virtual {v5, v2, v4}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 1077
    .local v2, "msg":Landroid/os/Message;
    iget-object v5, p0, Lcom/android/server/am/ProcessProphetImpl$MyReceiver;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    iget-object v5, v5, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    invoke-virtual {v5, v2}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z

    .end local v2    # "msg":Landroid/os/Message;
    .end local v3    # "device":Landroid/bluetooth/BluetoothDevice;
    .end local v4    # "deviceName":Ljava/lang/String;
    goto :goto_0

    .line 1078
    :cond_1
    if-nez v1, :cond_2

    .line 1079
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl$MyReceiver;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    iget-object v2, v2, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 1080
    .restart local v2    # "msg":Landroid/os/Message;
    iget-object v3, p0, Lcom/android/server/am/ProcessProphetImpl$MyReceiver;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    iget-object v3, v3, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    invoke-virtual {v3, v2}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 1078
    .end local v2    # "msg":Landroid/os/Message;
    :cond_2
    :goto_0
    nop

    .line 1083
    .end local v1    # "state":I
    :cond_3
    :goto_1
    return-void
.end method
