class com.android.server.am.MiuiBoosterUtilsStubImpl$MiuiBoosterInfo {
	 /* .source "MiuiBoosterUtilsStubImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/MiuiBoosterUtilsStubImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "MiuiBoosterInfo" */
} // .end annotation
/* # instance fields */
private Integer level;
private req_tids;
final com.android.server.am.MiuiBoosterUtilsStubImpl this$0; //synthetic
private Integer timeout;
private Integer uid;
/* # direct methods */
static Integer -$$Nest$fgetlevel ( com.android.server.am.MiuiBoosterUtilsStubImpl$MiuiBoosterInfo p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;->level:I */
} // .end method
static -$$Nest$fgetreq_tids ( com.android.server.am.MiuiBoosterUtilsStubImpl$MiuiBoosterInfo p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.req_tids;
} // .end method
static Integer -$$Nest$fgettimeout ( com.android.server.am.MiuiBoosterUtilsStubImpl$MiuiBoosterInfo p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;->timeout:I */
} // .end method
static Integer -$$Nest$fgetuid ( com.android.server.am.MiuiBoosterUtilsStubImpl$MiuiBoosterInfo p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;->uid:I */
} // .end method
public com.android.server.am.MiuiBoosterUtilsStubImpl$MiuiBoosterInfo ( ) {
/* .locals 0 */
/* .param p2, "uid" # I */
/* .param p3, "req_tids" # [I */
/* .param p4, "timeout" # I */
/* .param p5, "level" # I */
/* .line 441 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 442 */
/* iput p2, p0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;->uid:I */
/* .line 443 */
this.req_tids = p3;
/* .line 444 */
/* iput p4, p0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;->timeout:I */
/* .line 445 */
/* iput p5, p0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;->level:I */
/* .line 446 */
return;
} // .end method
