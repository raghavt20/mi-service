public class com.android.server.am.AutoStartManagerServiceStubImpl implements com.android.server.am.AutoStartManagerServiceStub {
	 /* .source "AutoStartManagerServiceStubImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Boolean ENABLE_SIGSTOP_KILL;
private static final java.lang.String TAG;
private static java.util.HashMap startServiceWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private miui.security.SecurityManagerInternal mSecurityInternal;
/* # direct methods */
static com.android.server.am.AutoStartManagerServiceStubImpl ( ) {
/* .locals 3 */
/* .line 48 */
/* nop */
/* .line 49 */
final String v0 = "persist.proc.enable_sigstop"; // const-string v0, "persist.proc.enable_sigstop"
int v1 = 1; // const/4 v1, 0x1
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.am.AutoStartManagerServiceStubImpl.ENABLE_SIGSTOP_KILL = (v0!= 0);
/* .line 51 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 54 */
final String v1 = "com.miui.huanji"; // const-string v1, "com.miui.huanji"
final String v2 = "com.miui.huanji.parsebak.RestoreXSpaceService"; // const-string v2, "com.miui.huanji.parsebak.RestoreXSpaceService"
(( java.util.HashMap ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 55 */
return;
} // .end method
public com.android.server.am.AutoStartManagerServiceStubImpl ( ) {
/* .locals 0 */
/* .line 44 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
private static Boolean isAllowStartServiceByUid ( android.content.Context p0, android.content.Intent p1, Integer p2 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "service" # Landroid/content/Intent; */
/* .param p2, "uid" # I */
/* .line 300 */
final String v0 = "appops"; // const-string v0, "appops"
(( android.content.Context ) p0 ).getSystemService ( v0 ); // invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/AppOpsManager; */
/* .line 301 */
/* .local v0, "aom":Landroid/app/AppOpsManager; */
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_0 */
/* .line 302 */
/* .line 304 */
} // :cond_0
(( android.content.Intent ) p1 ).getComponent ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* const/16 v3, 0x2718 */
v2 = (( android.app.AppOpsManager ) v0 ).checkOpNoThrow ( v3, p2, v2 ); // invoke-virtual {v0, v3, p2, v2}, Landroid/app/AppOpsManager;->checkOpNoThrow(IILjava/lang/String;)I
/* .line 305 */
/* .local v2, "mode":I */
/* if-nez v2, :cond_1 */
/* .line 306 */
/* .line 308 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // .end method
private java.util.HashMap searchAllUncoverdProcsPid ( java.util.List p0 ) {
/* .locals 7 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;", */
/* ">;)", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/Integer;", */
/* "Landroid/util/Pair<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
/* .line 132 */
/* .local p1, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;>;" */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 133 */
/* .local v0, "resPids":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/String;>;>;" */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo; */
/* .line 134 */
/* .local v2, "p":Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo; */
/* new-instance v3, Landroid/util/Pair; */
/* iget v4, v2, Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;->uid:I */
java.lang.Integer .valueOf ( v4 );
v5 = this.processName;
/* invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 135 */
/* .local v3, "procInfo":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/String;>;" */
/* iget v4, v2, Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;->uid:I */
/* iget v5, v2, Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;->pid:I */
com.android.server.am.AutoStartManagerServiceStubImpl .searchNativeProc ( v4,v5 );
/* .line 136 */
/* .local v4, "natiiveProcs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
v6 = } // :goto_1
if ( v6 != null) { // if-eqz v6, :cond_0
/* check-cast v6, Ljava/lang/Integer; */
/* .line 137 */
/* .local v6, "nPid":Ljava/lang/Integer; */
(( java.util.HashMap ) v0 ).put ( v6, v3 ); // invoke-virtual {v0, v6, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 138 */
} // .end local v6 # "nPid":Ljava/lang/Integer;
/* .line 139 */
} // .end local v2 # "p":Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;
} // .end local v3 # "procInfo":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/String;>;"
} // .end local v4 # "natiiveProcs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // :cond_0
/* .line 140 */
} // :cond_1
} // .end method
public static java.util.List searchNativeProc ( Integer p0, Integer p1 ) {
/* .locals 6 */
/* .param p0, "uid" # I */
/* .param p1, "pid" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(II)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 144 */
int v0 = 0; // const/4 v0, 0x0
/* .line 145 */
/* .local v0, "reader":Ljava/io/BufferedReader; */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 147 */
/* .local v1, "nativeProcs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
try { // :try_start_0
miui.process.ProcessManagerNative .getCgroupFilePath ( p0,p1 );
/* .line 148 */
/* .local v2, "fileName":Ljava/lang/String; */
/* new-instance v3, Ljava/io/BufferedReader; */
/* new-instance v4, Ljava/io/FileReader; */
/* new-instance v5, Ljava/io/File; */
/* invoke-direct {v5, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v4, v5}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V */
/* invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* move-object v0, v3 */
/* .line 149 */
int v3 = 0; // const/4 v3, 0x0
/* .line 150 */
/* .local v3, "line":Ljava/lang/String; */
} // :goto_0
(( java.io.BufferedReader ) v0 ).readLine ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v3, v4 */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 151 */
v4 = java.lang.Integer .parseInt ( v3 );
/* .line 152 */
/* .local v4, "proc":I */
/* if-lez v4, :cond_0 */
/* if-eq p1, v4, :cond_0 */
/* .line 153 */
java.lang.Integer .valueOf ( v4 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 155 */
} // .end local v4 # "proc":I
} // :cond_0
/* .line 150 */
} // .end local v2 # "fileName":Ljava/lang/String;
} // .end local v3 # "line":Ljava/lang/String;
} // :cond_1
/* .line 160 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 157 */
/* :catch_0 */
/* move-exception v2 */
/* .line 158 */
/* .local v2, "e":Ljava/lang/Exception; */
try { // :try_start_1
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local v2 # "e":Ljava/lang/Exception;
/* .line 160 */
} // :goto_1
miui.io.IoUtils .closeQuietly ( v0 );
/* .line 161 */
/* throw v2 */
/* .line 156 */
/* :catch_1 */
/* move-exception v2 */
/* .line 160 */
} // :goto_2
miui.io.IoUtils .closeQuietly ( v0 );
/* .line 161 */
/* nop */
/* .line 162 */
} // .end method
private void trackAppBehavior ( Integer p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 7 */
/* .param p1, "behavior" # I */
/* .param p2, "pkg" # Ljava/lang/String; */
/* .param p3, "info" # Ljava/lang/String; */
/* .line 179 */
v0 = this.mSecurityInternal;
/* if-nez v0, :cond_0 */
/* .line 180 */
/* const-class v0, Lmiui/security/SecurityManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lmiui/security/SecurityManagerInternal; */
this.mSecurityInternal = v0;
/* .line 182 */
} // :cond_0
v1 = this.mSecurityInternal;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 183 */
/* const-wide/16 v4, 0x1 */
/* move v2, p1 */
/* move-object v3, p2 */
/* move-object v6, p3 */
/* invoke-virtual/range {v1 ..v6}, Lmiui/security/SecurityManagerInternal;->recordAppBehaviorAsync(ILjava/lang/String;JLjava/lang/String;)V */
/* .line 185 */
} // :cond_1
return;
} // .end method
/* # virtual methods */
public Boolean canRestartServiceLocked ( java.lang.String p0, Integer p1, java.lang.String p2 ) {
/* .locals 6 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .param p3, "message" # Ljava/lang/String; */
/* .line 257 */
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
/* move-object v0, p0 */
/* move-object v1, p1 */
/* move v2, p2 */
/* move-object v3, p3 */
v0 = /* invoke-virtual/range {v0 ..v5}, Lcom/android/server/am/AutoStartManagerServiceStubImpl;->canRestartServiceLocked(Ljava/lang/String;ILjava/lang/String;Landroid/content/ComponentName;Z)Z */
} // .end method
public Boolean canRestartServiceLocked ( java.lang.String p0, Integer p1, java.lang.String p2, android.content.ComponentName p3, Boolean p4 ) {
/* .locals 8 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .param p3, "message" # Ljava/lang/String; */
/* .param p4, "component" # Landroid/content/ComponentName; */
/* .param p5, "isNote" # Z */
/* .line 262 */
/* const-class v0, Lcom/miui/app/AppOpsServiceInternal; */
com.android.server.LocalServices .getService ( v0 );
v0 = /* check-cast v0, Lcom/miui/app/AppOpsServiceInternal; */
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 263 */
/* .line 265 */
} // :cond_0
if ( p4 != null) { // if-eqz p4, :cond_1
v0 = com.android.server.am.AutoStartManagerServiceStubImpl.startServiceWhiteList;
v0 = (( java.util.HashMap ) v0 ).containsKey ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 266 */
v0 = com.android.server.am.AutoStartManagerServiceStubImpl.startServiceWhiteList;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/String; */
(( android.content.ComponentName ) p4 ).getClassName ( ); // invoke-virtual {p4}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 267 */
/* .line 274 */
} // :cond_1
android.app.ActivityThread .currentApplication ( );
/* const-class v2, Landroid/app/AppOpsManager; */
(( android.app.Application ) v0 ).getSystemService ( v2 ); // invoke-virtual {v0, v2}, Landroid/app/Application;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/AppOpsManager; */
/* .line 276 */
/* .local v0, "appOpsManager":Landroid/app/AppOpsManager; */
if ( p5 != null) { // if-eqz p5, :cond_2
/* .line 277 */
/* const/16 v3, 0x2718 */
int v6 = 0; // const/4 v6, 0x0
/* move-object v2, v0 */
/* move v4, p2 */
/* move-object v5, p1 */
/* move-object v7, p3 */
v2 = /* invoke-virtual/range {v2 ..v7}, Landroid/app/AppOpsManager;->noteOpNoThrow(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I */
/* .line 278 */
/* .local v2, "mode":I */
if ( p4 != null) { // if-eqz p4, :cond_3
/* .line 279 */
/* nop */
/* .line 280 */
(( android.content.ComponentName ) p4 ).flattenToShortString ( ); // invoke-virtual {p4}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
/* .line 279 */
int v4 = 6; // const/4 v4, 0x6
/* invoke-direct {p0, v4, p1, v3}, Lcom/android/server/am/AutoStartManagerServiceStubImpl;->trackAppBehavior(ILjava/lang/String;Ljava/lang/String;)V */
/* .line 283 */
} // .end local v2 # "mode":I
} // :cond_2
/* const/16 v2, 0x2718 */
v2 = (( android.app.AppOpsManager ) v0 ).checkOpNoThrow ( v2, p2, p1 ); // invoke-virtual {v0, v2, p2, p1}, Landroid/app/AppOpsManager;->checkOpNoThrow(IILjava/lang/String;)I
/* .line 285 */
/* .restart local v2 # "mode":I */
} // :cond_3
} // :goto_0
/* if-nez v2, :cond_4 */
/* .line 286 */
/* .line 289 */
} // :cond_4
v3 = com.miui.server.process.ProcessManagerInternal .checkCtsProcess ( p1 );
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 290 */
/* .line 293 */
} // :cond_5
if ( p5 != null) { // if-eqz p5, :cond_6
if ( p4 != null) { // if-eqz p4, :cond_6
/* .line 294 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "MIUILOG- Reject RestartService service :"; // const-string v3, "MIUILOG- Reject RestartService service :"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p4 ); // invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v3 = " uid : "; // const-string v3, " uid : "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "AutoStartManagerServiceStubImpl"; // const-string v3, "AutoStartManagerServiceStubImpl"
android.util.Slog .i ( v3,v1 );
/* .line 296 */
} // :cond_6
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean isAllowStartService ( android.content.Context p0, android.content.Intent p1, Integer p2 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "service" # Landroid/content/Intent; */
/* .param p3, "userId" # I */
/* .line 189 */
int v0 = 1; // const/4 v0, 0x1
try { // :try_start_0
(( android.content.Intent ) p2 ).getComponent ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 191 */
/* .local v1, "packageName":Ljava/lang/String; */
com.android.server.notification.BarrageListenerServiceStub .getInstance ( );
v2 = /* .line 192 */
/* .line 193 */
/* .local v2, "isAllow":Z */
/* if-nez v2, :cond_0 */
/* .line 194 */
final String v3 = "AutoStartManagerServiceStubImpl"; // const-string v3, "AutoStartManagerServiceStubImpl"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "MiBarrage: Disallowing to start service {"; // const-string v5, "MiBarrage: Disallowing to start service {"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v5, "}" */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v3,v4 );
/* .line 195 */
int v0 = 0; // const/4 v0, 0x0
/* .line 198 */
} // :cond_0
android.app.AppGlobals .getPackageManager ( );
/* .line 199 */
/* .local v3, "packageManager":Landroid/content/pm/IPackageManager; */
/* const-wide/16 v4, 0x0 */
/* .line 200 */
/* .local v4, "applicationInfo":Landroid/content/pm/ApplicationInfo; */
/* if-nez v4, :cond_1 */
/* .line 201 */
/* .line 203 */
} // :cond_1
/* iget v5, v4, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 204 */
/* .local v5, "uid":I */
v0 = (( com.android.server.am.AutoStartManagerServiceStubImpl ) p0 ).isAllowStartService ( p1, p2, p3, v5 ); // invoke-virtual {p0, p1, p2, p3, v5}, Lcom/android/server/am/AutoStartManagerServiceStubImpl;->isAllowStartService(Landroid/content/Context;Landroid/content/Intent;II)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 205 */
} // .end local v1 # "packageName":Ljava/lang/String;
} // .end local v2 # "isAllow":Z
} // .end local v3 # "packageManager":Landroid/content/pm/IPackageManager;
} // .end local v4 # "applicationInfo":Landroid/content/pm/ApplicationInfo;
} // .end local v5 # "uid":I
/* :catch_0 */
/* move-exception v1 */
/* .line 206 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 208 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // .end method
public Boolean isAllowStartService ( android.content.Context p0, android.content.Intent p1, Integer p2, Integer p3 ) {
/* .locals 16 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "service" # Landroid/content/Intent; */
/* .param p3, "userId" # I */
/* .param p4, "uid" # I */
/* .line 213 */
/* move-object/from16 v1, p1 */
/* move/from16 v8, p4 */
int v9 = 1; // const/4 v9, 0x1
try { // :try_start_0
final String v0 = "appops"; // const-string v0, "appops"
(( android.content.Context ) v1 ).getSystemService ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/AppOpsManager; */
/* .line 214 */
/* .local v0, "aom":Landroid/app/AppOpsManager; */
/* if-nez v0, :cond_0 */
/* .line 215 */
/* .line 217 */
} // :cond_0
android.app.ActivityManager .getService ( );
/* check-cast v2, Lcom/android/server/am/ActivityManagerService; */
/* move-object v10, v2 */
/* .line 218 */
/* .local v10, "ams":Lcom/android/server/am/ActivityManagerService; */
android.app.AppGlobals .getPackageManager ( );
int v4 = 0; // const/4 v4, 0x0
/* const-wide/16 v5, 0x400 */
/* move-object/from16 v3, p2 */
/* move/from16 v7, p3 */
/* invoke-interface/range {v2 ..v7}, Landroid/content/pm/IPackageManager;->resolveService(Landroid/content/Intent;Ljava/lang/String;JI)Landroid/content/pm/ResolveInfo; */
/* move-object v11, v2 */
/* .line 220 */
/* .local v11, "rInfo":Landroid/content/pm/ResolveInfo; */
if ( v11 != null) { // if-eqz v11, :cond_1
v2 = this.serviceInfo;
} // :cond_1
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* move-object v12, v2 */
/* .line 221 */
/* .local v12, "sInfo":Landroid/content/pm/ServiceInfo; */
/* if-nez v12, :cond_2 */
/* .line 222 */
/* .line 224 */
} // :cond_2
v2 = this.serviceInfo;
v2 = this.applicationInfo;
/* iget v2, v2, Landroid/content/pm/ApplicationInfo;->flags:I */
/* and-int/2addr v2, v9 */
/* if-nez v2, :cond_6 */
v2 = this.serviceInfo;
v2 = this.applicationInfo;
v2 = this.packageName;
/* .line 225 */
int v13 = 0; // const/4 v13, 0x0
v2 = miui.content.pm.PreloadedAppPolicy .isProtectedDataApp ( v1,v2,v13 );
if ( v2 != null) { // if-eqz v2, :cond_3
/* move-object/from16 v5, p0 */
/* move-object/from16 v6, p2 */
/* move/from16 v7, p3 */
/* goto/16 :goto_1 */
/* .line 229 */
} // :cond_3
/* invoke-virtual/range {p2 ..p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName; */
(( android.content.ComponentName ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* move-object v14, v2 */
/* .line 230 */
/* .local v14, "packageName":Ljava/lang/String; */
v2 = this.mActivityTaskManager;
v3 = this.processName;
v2 = com.android.server.wm.WindowProcessUtils .isPackageRunning ( v2,v14,v3,v8 );
/* move v15, v2 */
/* .line 232 */
/* .local v15, "isRunning":Z */
/* if-nez v15, :cond_5 */
/* .line 237 */
/* const/16 v3, 0x2718 */
int v6 = 0; // const/4 v6, 0x0
final String v7 = "AutoStartManagerServiceStubImpl#isAllowStartService"; // const-string v7, "AutoStartManagerServiceStubImpl#isAllowStartService"
/* move-object v2, v0 */
/* move/from16 v4, p4 */
/* move-object v5, v14 */
v2 = /* invoke-virtual/range {v2 ..v7}, Landroid/app/AppOpsManager;->noteOpNoThrow(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I */
/* .line 240 */
/* .local v2, "mode":I */
/* nop */
/* .line 241 */
/* invoke-virtual/range {p2 ..p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName; */
(( android.content.ComponentName ) v3 ).flattenToShortString ( ); // invoke-virtual {v3}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_3 */
/* .line 240 */
int v4 = 6; // const/4 v4, 0x6
/* move-object/from16 v5, p0 */
try { // :try_start_1
/* invoke-direct {v5, v4, v14, v3}, Lcom/android/server/am/AutoStartManagerServiceStubImpl;->trackAppBehavior(ILjava/lang/String;Ljava/lang/String;)V */
/* .line 242 */
/* if-nez v2, :cond_4 */
/* .line 243 */
/* .line 245 */
} // :cond_4
final String v3 = "AutoStartManagerServiceStubImpl"; // const-string v3, "AutoStartManagerServiceStubImpl"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "MIUILOG- Reject service :"; // const-string v6, "MIUILOG- Reject service :"
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_2 */
/* move-object/from16 v6, p2 */
try { // :try_start_2
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v7 = " userId : "; // const-string v7, " userId : "
(( java.lang.StringBuilder ) v4 ).append ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_1 */
/* move/from16 v7, p3 */
try { // :try_start_3
(( java.lang.StringBuilder ) v4 ).append ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = " uid : "; // const-string v9, " uid : "
(( java.lang.StringBuilder ) v4 ).append ( v9 ); // invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v8 ); // invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v4 );
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 246 */
/* .line 249 */
} // .end local v0 # "aom":Landroid/app/AppOpsManager;
} // .end local v2 # "mode":I
} // .end local v10 # "ams":Lcom/android/server/am/ActivityManagerService;
} // .end local v11 # "rInfo":Landroid/content/pm/ResolveInfo;
} // .end local v12 # "sInfo":Landroid/content/pm/ServiceInfo;
} // .end local v14 # "packageName":Ljava/lang/String;
} // .end local v15 # "isRunning":Z
/* :catch_0 */
/* move-exception v0 */
/* :catch_1 */
/* move-exception v0 */
/* :catch_2 */
/* move-exception v0 */
/* .line 232 */
/* .restart local v0 # "aom":Landroid/app/AppOpsManager; */
/* .restart local v10 # "ams":Lcom/android/server/am/ActivityManagerService; */
/* .restart local v11 # "rInfo":Landroid/content/pm/ResolveInfo; */
/* .restart local v12 # "sInfo":Landroid/content/pm/ServiceInfo; */
/* .restart local v14 # "packageName":Ljava/lang/String; */
/* .restart local v15 # "isRunning":Z */
} // :cond_5
/* move-object/from16 v5, p0 */
/* move-object/from16 v6, p2 */
/* move/from16 v7, p3 */
/* .line 251 */
} // .end local v0 # "aom":Landroid/app/AppOpsManager;
} // .end local v10 # "ams":Lcom/android/server/am/ActivityManagerService;
} // .end local v11 # "rInfo":Landroid/content/pm/ResolveInfo;
} // .end local v12 # "sInfo":Landroid/content/pm/ServiceInfo;
} // .end local v14 # "packageName":Ljava/lang/String;
} // .end local v15 # "isRunning":Z
/* .line 224 */
/* .restart local v0 # "aom":Landroid/app/AppOpsManager; */
/* .restart local v10 # "ams":Lcom/android/server/am/ActivityManagerService; */
/* .restart local v11 # "rInfo":Landroid/content/pm/ResolveInfo; */
/* .restart local v12 # "sInfo":Landroid/content/pm/ServiceInfo; */
} // :cond_6
/* move-object/from16 v5, p0 */
/* move-object/from16 v6, p2 */
/* move/from16 v7, p3 */
/* .line 227 */
} // :goto_1
int v2 = 1; // const/4 v2, 0x1
/* .line 249 */
} // .end local v0 # "aom":Landroid/app/AppOpsManager;
} // .end local v10 # "ams":Lcom/android/server/am/ActivityManagerService;
} // .end local v11 # "rInfo":Landroid/content/pm/ResolveInfo;
} // .end local v12 # "sInfo":Landroid/content/pm/ServiceInfo;
/* :catch_3 */
/* move-exception v0 */
/* move-object/from16 v5, p0 */
} // :goto_2
/* move-object/from16 v6, p2 */
} // :goto_3
/* move/from16 v7, p3 */
/* .line 250 */
/* .local v0, "e":Ljava/lang/Exception; */
} // :goto_4
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 252 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_5
int v2 = 1; // const/4 v2, 0x1
} // .end method
void sendSignalToProcessLocked ( java.util.List p0, java.lang.String p1, Integer p2, Boolean p3 ) {
/* .locals 10 */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "signal" # I */
/* .param p4, "needKillAgain" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;", */
/* ">;", */
/* "Ljava/lang/String;", */
/* "IZ)V" */
/* } */
} // .end annotation
/* .line 89 */
/* .local p1, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;>;" */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AutoStartManagerServiceStubImpl;->searchAllUncoverdProcsPid(Ljava/util/List;)Ljava/util/HashMap; */
/* .line 95 */
/* .local v0, "allUncoverdPids":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/String;>;>;" */
v2 = } // :goto_0
final String v3 = " s: "; // const-string v3, " s: "
final String v4 = "AutoStartManagerServiceStubImpl"; // const-string v4, "AutoStartManagerServiceStubImpl"
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo; */
/* .line 96 */
/* .local v2, "proc":Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "prepare force stop p:"; // const-string v6, "prepare force stop p:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, v2, Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;->pid:I */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p3 ); // invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v3 );
/* .line 97 */
/* iget v3, v2, Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;->pid:I */
android.os.Process .sendSignal ( v3,p3 );
/* .line 98 */
/* iget v3, v2, Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;->pid:I */
java.lang.Integer .valueOf ( v3 );
(( java.util.HashMap ) v0 ).remove ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 99 */
} // .end local v2 # "proc":Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;
/* .line 104 */
} // :cond_0
(( java.util.HashMap ) v0 ).keySet ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;
v2 = } // :goto_1
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Ljava/lang/Integer; */
/* .line 105 */
/* .local v2, "pid":Ljava/lang/Integer; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "prepare force stop native p:"; // const-string v6, "prepare force stop native p:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p3 ); // invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v5 );
/* .line 106 */
v5 = (( java.lang.Integer ) v2 ).intValue ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
android.os.Process .sendSignal ( v5,p3 );
/* .line 107 */
/* if-nez p4, :cond_1 */
/* .line 108 */
/* nop */
/* .line 109 */
(( java.util.HashMap ) v0 ).get ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v5, Landroid/util/Pair; */
v5 = this.second;
/* check-cast v5, Ljava/lang/String; */
/* .line 108 */
/* const/16 v6, 0x22 */
/* invoke-direct {p0, v6, p2, v5}, Lcom/android/server/am/AutoStartManagerServiceStubImpl;->trackAppBehavior(ILjava/lang/String;Ljava/lang/String;)V */
/* .line 111 */
} // .end local v2 # "pid":Ljava/lang/Integer;
} // :cond_1
/* .line 113 */
} // :cond_2
if ( p4 != null) { // if-eqz p4, :cond_4
/* .line 114 */
v2 = } // :goto_2
if ( v2 != null) { // if-eqz v2, :cond_4
/* check-cast v2, Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo; */
/* .line 115 */
/* .local v2, "p":Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo; */
/* iget v3, v2, Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;->pid:I */
/* .local v3, "pid":I */
/* iget v4, v2, Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;->uid:I */
/* .line 116 */
/* .local v4, "uid":I */
/* if-lez v3, :cond_3 */
/* int-to-long v5, v4 */
/* int-to-long v7, v3 */
v5 = com.android.server.am.ProcessUtils .isDiedProcess ( v5,v6,v7,v8 );
/* if-nez v5, :cond_3 */
/* .line 117 */
java.lang.Integer .valueOf ( v4 );
java.lang.Integer .valueOf ( v3 );
v7 = this.processName;
/* .line 118 */
/* const/16 v8, 0x3e9 */
java.lang.Integer .valueOf ( v8 );
final String v9 = "Kill Again"; // const-string v9, "Kill Again"
/* filled-new-array {v5, v6, v7, v8, v9}, [Ljava/lang/Object; */
/* .line 117 */
/* const/16 v6, 0x7547 */
android.util.EventLog .writeEvent ( v6,v5 );
/* .line 119 */
android.os.Process .killProcessQuiet ( v3 );
/* .line 120 */
android.os.Process .killProcessGroup ( v4,v3 );
/* .line 122 */
} // .end local v2 # "p":Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;
} // .end local v3 # "pid":I
} // .end local v4 # "uid":I
} // :cond_3
/* .line 124 */
} // :cond_4
return;
} // .end method
public void signalStopProcessesLocked ( java.util.ArrayList p0, Boolean p1, java.lang.String p2, Integer p3 ) {
/* .locals 6 */
/* .param p2, "allowRestart" # Z */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .param p4, "uid" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Landroid/util/Pair<", */
/* "Lcom/android/server/am/ProcessRecord;", */
/* "Ljava/lang/Boolean;", */
/* ">;>;Z", */
/* "Ljava/lang/String;", */
/* "I)V" */
/* } */
} // .end annotation
/* .line 63 */
/* .local p1, "procs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/util/Pair<Lcom/android/server/am/ProcessRecord;Ljava/lang/Boolean;>;>;" */
/* sget-boolean v0, Lcom/android/server/am/AutoStartManagerServiceStubImpl;->ENABLE_SIGSTOP_KILL:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 65 */
} // :cond_0
android.app.ActivityManager .getService ( );
/* check-cast v0, Lcom/android/server/am/ActivityManagerService; */
/* .line 66 */
/* .local v0, "ams":Lcom/android/server/am/ActivityManagerService; */
if ( p2 != null) { // if-eqz p2, :cond_1
final String v1 = "AutoStartManagerServiceStubImpl#signalStopProcessesLocked"; // const-string v1, "AutoStartManagerServiceStubImpl#signalStopProcessesLocked"
v1 = (( com.android.server.am.AutoStartManagerServiceStubImpl ) p0 ).canRestartServiceLocked ( p3, p4, v1 ); // invoke-virtual {p0, p3, p4, v1}, Lcom/android/server/am/AutoStartManagerServiceStubImpl;->canRestartServiceLocked(Ljava/lang/String;ILjava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 68 */
return;
/* .line 70 */
} // :cond_1
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 71 */
/* .local v1, "tmpProcs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;>;" */
(( java.util.ArrayList ) p1 ).iterator ( ); // invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_2
/* check-cast v3, Landroid/util/Pair; */
/* .line 72 */
/* .local v3, "proc":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/android/server/am/ProcessRecord;Ljava/lang/Boolean;>;" */
/* new-instance v4, Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo; */
v5 = this.first;
/* check-cast v5, Lcom/android/server/am/ProcessRecord; */
/* invoke-direct {v4, v5}, Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;-><init>(Lcom/android/server/am/ProcessRecord;)V */
(( java.util.ArrayList ) v1 ).add ( v4 ); // invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 73 */
} // .end local v3 # "proc":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/android/server/am/ProcessRecord;Ljava/lang/Boolean;>;"
/* .line 75 */
} // :cond_2
/* const/16 v2, 0x13 */
int v3 = 0; // const/4 v3, 0x0
(( com.android.server.am.AutoStartManagerServiceStubImpl ) p0 ).sendSignalToProcessLocked ( v1, p3, v2, v3 ); // invoke-virtual {p0, v1, p3, v2, v3}, Lcom/android/server/am/AutoStartManagerServiceStubImpl;->sendSignalToProcessLocked(Ljava/util/List;Ljava/lang/String;IZ)V
/* .line 79 */
v2 = this.mHandler;
/* new-instance v3, Lcom/android/server/am/AutoStartManagerServiceStubImpl$1; */
/* invoke-direct {v3, p0, v1, p3}, Lcom/android/server/am/AutoStartManagerServiceStubImpl$1;-><init>(Lcom/android/server/am/AutoStartManagerServiceStubImpl;Ljava/util/ArrayList;Ljava/lang/String;)V */
/* const-wide/16 v4, 0x1f4 */
(( com.android.server.am.ActivityManagerService$MainHandler ) v2 ).postDelayed ( v3, v4, v5 ); // invoke-virtual {v2, v3, v4, v5}, Lcom/android/server/am/ActivityManagerService$MainHandler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 85 */
return;
} // .end method
