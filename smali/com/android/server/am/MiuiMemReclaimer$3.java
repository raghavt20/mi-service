class com.android.server.am.MiuiMemReclaimer$3 implements java.util.Comparator {
	 /* .source "MiuiMemReclaimer.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/am/MiuiMemReclaimer;->sortProcsByRss(Ljava/util/List;Ljava/lang/String;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Object;", */
/* "Ljava/util/Comparator<", */
/* "Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* # instance fields */
final com.android.server.am.MiuiMemReclaimer this$0; //synthetic
final java.lang.String val$action; //synthetic
/* # direct methods */
 com.android.server.am.MiuiMemReclaimer$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/MiuiMemReclaimer; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 453 */
this.this$0 = p1;
this.val$action = p2;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public Integer compare ( com.android.server.am.MiuiMemReclaimer$CompactProcInfo p0, com.android.server.am.MiuiMemReclaimer$CompactProcInfo p1 ) {
/* .locals 5 */
/* .param p1, "t1" # Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo; */
/* .param p2, "t2" # Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo; */
/* .line 456 */
v0 = this.val$action;
v1 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
int v2 = 2; // const/4 v2, 0x2
int v3 = 1; // const/4 v3, 0x1
int v4 = 0; // const/4 v4, 0x0
/* sparse-switch v1, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
final String v1 = "file"; // const-string v1, "file"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v3 */
/* :sswitch_1 */
final String v1 = "anon"; // const-string v1, "anon"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v2 */
/* :sswitch_2 */
final String v1 = "all"; // const-string v1, "all"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v4 */
} // :goto_0
int v0 = -1; // const/4 v0, -0x1
} // :goto_1
/* packed-switch v0, :pswitch_data_0 */
/* .line 464 */
v0 = this.rss;
/* aget-wide v0, v0, v4 */
v2 = this.rss;
/* aget-wide v2, v2, v4 */
v0 = java.lang.Long .compare ( v0,v1,v2,v3 );
/* .line 462 */
/* :pswitch_0 */
v0 = this.rss;
/* aget-wide v0, v0, v2 */
v3 = this.rss;
/* aget-wide v2, v3, v2 */
v0 = java.lang.Long .compare ( v0,v1,v2,v3 );
/* .line 460 */
/* :pswitch_1 */
v0 = this.rss;
/* aget-wide v0, v0, v3 */
v2 = this.rss;
/* aget-wide v2, v2, v3 */
v0 = java.lang.Long .compare ( v0,v1,v2,v3 );
/* .line 458 */
/* :pswitch_2 */
v0 = this.rss;
/* aget-wide v0, v0, v4 */
v2 = this.rss;
/* aget-wide v2, v2, v4 */
v0 = java.lang.Long .compare ( v0,v1,v2,v3 );
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x179a1 -> :sswitch_2 */
/* 0x2dc2cc -> :sswitch_1 */
/* 0x2ff57c -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public Integer compare ( java.lang.Object p0, java.lang.Object p1 ) { //bridge//synthethic
/* .locals 0 */
/* .line 453 */
/* check-cast p1, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo; */
/* check-cast p2, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo; */
p1 = (( com.android.server.am.MiuiMemReclaimer$3 ) p0 ).compare ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/am/MiuiMemReclaimer$3;->compare(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)I
} // .end method
