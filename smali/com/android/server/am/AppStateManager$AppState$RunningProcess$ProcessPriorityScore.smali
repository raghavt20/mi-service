.class public Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;
.super Ljava/lang/Object;
.source "AppStateManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ProcessPriorityScore"
.end annotation


# static fields
.field private static final APP_ACTIVE_THRESHOLD_HIGH:J = 0x927c0L

.field private static final APP_ACTIVE_THRESHOLD_LOW:J = 0x36ee80L

.field private static final APP_ACTIVE_THRESHOLD_MODERATE:J = 0x1b7740L

.field private static final MAX_APP_WEEK_LAUNCH_COUNT:I = 0x15

.field private static final MINIMUM_MEMORY_GROWTH_THRESHOLD:I = 0x19000

.field private static final MIN_APP_FOREGROUND_EVENT_DURATION:J = 0x1388L

.field private static final PROCESS_ACTIVE_LEVEL_CRITICAL:I = 0x1

.field private static final PROCESS_ACTIVE_LEVEL_HEAVY:I = 0x0

.field private static final PROCESS_ACTIVE_LEVEL_LOW:I = 0x3

.field private static final PROCESS_ACTIVE_LEVEL_MODERATE:I = 0x2

.field private static final PROCESS_MEM_LEVEL_CRITICAL:I = 0x3

.field private static final PROCESS_MEM_LEVEL_HIGH:I = 0x4

.field private static final PROCESS_MEM_LEVEL_LOW:I = 0x1

.field private static final PROCESS_MEM_LEVEL_MODERATE:I = 0x2

.field public static final PROCESS_PRIORITY_ACTIVE_USAGE_FACTOR:I = 0x64

.field public static final PROCESS_PRIORITY_LEVEL_UNKNOWN:I = 0x3e8

.field public static final PROCESS_PRIORITY_MEM_FACTOR:I = 0x1

.field public static final PROCESS_PRIORITY_TYPE_FACTOR:I = 0xa

.field private static final PROCESS_TYPE_LEVEL_MAINPROC_HAS_ACTIVITY:I = 0x1

.field private static final PROCESS_TYPE_LEVEL_NO_ACTIVITY:I = 0x3

.field private static final PROCESS_TYPE_LEVEL_SUBPROC_HAS_ACTIVITY:I = 0x2

.field private static final PROCESS_USAGE_ACTIVE_LEVEL_DEFAULT:I = 0x9

.field private static final PROCESS_USAGE_LEVEL_AUTOSTART:I = 0x4

.field public static final PROCESS_USAGE_LEVEL_CRITICAL:I = 0x5

.field public static final PROCESS_USAGE_LEVEL_LOW:I = 0x7

.field public static final PROCESS_USAGE_LEVEL_MODERATE:I = 0x6


# instance fields
.field private final MEMORY_GROWTH_THRESHOLD:J

.field private final TOTAL_MEMEORY_KB:J

.field private mPriorityScore:I

.field private mUsageLevel:I

.field final synthetic this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;


# direct methods
.method static bridge synthetic -$$Nest$fgetmPriorityScore(Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->mPriorityScore:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmUsageLevel(Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->mUsageLevel:I

    return p0
.end method

.method static bridge synthetic -$$Nest$mupdatePriorityScore(Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->updatePriorityScore()V

    return-void
.end method

.method public constructor <init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V
    .locals 4
    .param p1, "this$2"    # Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 3015
    iput-object p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3051
    invoke-static {}, Landroid/os/Process;->getTotalMemory()J

    move-result-wide v0

    const-wide/16 v2, 0x400

    div-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->TOTAL_MEMEORY_KB:J

    .line 3053
    const-wide/16 v2, 0x64

    div-long/2addr v0, v2

    .line 3054
    const-wide/32 v2, 0x19000

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->MEMORY_GROWTH_THRESHOLD:J

    .line 3053
    return-void
.end method

.method private computeActiveAndUsageStatLevel()I
    .locals 2

    .line 3105
    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->computeUsageStatLevel()I

    move-result v0

    iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->mUsageLevel:I

    .line 3107
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmIsLockedApp(Lcom/android/server/am/AppStateManager$AppState;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmSmartPowerPolicyManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmPid(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isDependedProcess(I)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 3113
    :cond_0
    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->computeAppActiveLevel()I

    move-result v0

    .line 3115
    .local v0, "activeLevel":I
    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->mUsageLevel:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    return v1

    .line 3109
    .end local v0    # "activeLevel":I
    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method private computeAppActiveLevel()I
    .locals 9

    .line 3127
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmIsLockedApp(Lcom/android/server/am/AppStateManager$AppState;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmSmartPowerPolicyManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmPid(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isDependedProcess(I)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 3132
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmProcessRecord(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Lcom/android/server/am/ProcessRecord;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getLastTopTime()J

    move-result-wide v0

    .line 3133
    .local v0, "lastTopTime":J
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmProcessRecord(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Lcom/android/server/am/ProcessRecord;

    move-result-object v2

    iget-object v2, v2, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    .line 3134
    invoke-virtual {v2}, Lcom/android/server/am/ProcessStateRecord;->getLastSwitchToTopTime()J

    move-result-wide v2

    sub-long v2, v0, v2

    .line 3135
    .local v2, "appTopDuration":J
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v0

    .line 3137
    .local v4, "backgroundTime":J
    const/16 v6, 0x9

    .line 3138
    .local v6, "activeLevel":I
    const-wide/16 v7, 0x1388

    cmp-long v7, v2, v7

    if-lez v7, :cond_3

    .line 3139
    const-wide/32 v7, 0x927c0

    cmp-long v7, v4, v7

    if-gtz v7, :cond_1

    .line 3141
    const/4 v6, 0x1

    goto :goto_0

    .line 3142
    :cond_1
    const-wide/32 v7, 0x1b7740

    cmp-long v7, v4, v7

    if-gtz v7, :cond_2

    .line 3144
    const/4 v6, 0x2

    goto :goto_0

    .line 3145
    :cond_2
    const-wide/32 v7, 0x36ee80

    cmp-long v7, v4, v7

    if-gtz v7, :cond_3

    .line 3147
    const/4 v6, 0x3

    .line 3151
    :cond_3
    :goto_0
    const/16 v7, 0x9

    if-ne v6, v7, :cond_4

    iget-object v7, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    iget-object v7, v7, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v7}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmIsAutoStartApp(Lcom/android/server/am/AppStateManager$AppState;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 3152
    const/4 v6, 0x4

    .line 3155
    :cond_4
    return v6

    .line 3129
    .end local v0    # "lastTopTime":J
    .end local v2    # "appTopDuration":J
    .end local v4    # "backgroundTime":J
    .end local v6    # "activeLevel":I
    :cond_5
    :goto_1
    const/4 v0, 0x0

    return v0
.end method

.method private computeMemUsageLevel()I
    .locals 9

    .line 3210
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmLastPss(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)J

    move-result-wide v0

    sget-wide v2, Lcom/miui/app/smartpower/SmartPowerSettings;->PROC_MEM_LVL3_PSS_LIMIT_KB:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 3211
    const/4 v0, 0x4

    .local v0, "memLevel":I
    goto :goto_0

    .line 3212
    .end local v0    # "memLevel":I
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmLastPss(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)J

    move-result-wide v0

    sget-wide v2, Lcom/miui/app/smartpower/SmartPowerSettings;->PROC_MEM_LVL2_PSS_LIMIT_KB:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    .line 3213
    const/4 v0, 0x3

    .restart local v0    # "memLevel":I
    goto :goto_0

    .line 3214
    .end local v0    # "memLevel":I
    :cond_1
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmLastPss(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)J

    move-result-wide v0

    sget-wide v2, Lcom/miui/app/smartpower/SmartPowerSettings;->PROC_MEM_LVL1_PSS_LIMIT_KB:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    .line 3215
    const/4 v0, 0x2

    .restart local v0    # "memLevel":I
    goto :goto_0

    .line 3217
    .end local v0    # "memLevel":I
    :cond_2
    const/4 v0, 0x1

    .line 3224
    .restart local v0    # "memLevel":I
    :goto_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmProcessRecord(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Lcom/android/server/am/ProcessRecord;

    move-result-object v1

    iget-object v1, v1, Lcom/android/server/am/ProcessRecord;->mProfile:Lcom/android/server/am/ProcessProfileRecord;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessProfileRecord;->getInitialIdlePss()J

    move-result-wide v1

    .line 3225
    .local v1, "initialIdlePss":J
    const/4 v3, 0x1

    if-ne v0, v3, :cond_3

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmLastPss(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)J

    move-result-wide v3

    const-wide/16 v5, 0x3

    mul-long/2addr v5, v1

    const-wide/16 v7, 0x2

    div-long/2addr v5, v7

    cmp-long v3, v3, v5

    if-lez v3, :cond_3

    iget-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmLastPss(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)J

    move-result-wide v3

    iget-wide v5, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->MEMORY_GROWTH_THRESHOLD:J

    add-long/2addr v5, v1

    cmp-long v3, v3, v5

    if-lez v3, :cond_3

    .line 3229
    const/4 v0, 0x3

    .line 3231
    :cond_3
    return v0
.end method

.method private computeTypeLevel()I
    .locals 2

    .line 3195
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmProcessRecord(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Lcom/android/server/am/ProcessRecord;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/am/ProcessRecord;->hasActivities()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3196
    const/4 v0, 0x3

    .local v0, "typeLevel":I
    goto :goto_0

    .line 3197
    .end local v0    # "typeLevel":I
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmProcessName(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmPackageName(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 3199
    const/4 v0, 0x2

    .restart local v0    # "typeLevel":I
    goto :goto_0

    .line 3202
    .end local v0    # "typeLevel":I
    :cond_1
    const/4 v0, 0x1

    .line 3204
    .restart local v0    # "typeLevel":I
    :goto_0
    return v0
.end method

.method private computeUsageStatLevel()I
    .locals 9

    .line 3159
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmSmartPowerPolicyManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmPackageName(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Ljava/lang/String;

    move-result-object v1

    .line 3160
    invoke-virtual {v0, v1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->getUsageStatsInfo(Ljava/lang/String;)Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;

    move-result-object v0

    .line 3161
    .local v0, "usageStats":Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;
    const/16 v1, 0x9

    .line 3162
    .local v1, "usageLevel":I
    if-eqz v0, :cond_4

    .line 3163
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmProcessRecord(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Lcom/android/server/am/ProcessRecord;

    move-result-object v2

    iget-object v2, v2, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v2}, Lcom/android/server/am/ProcessStateRecord;->getLastTopTime()J

    move-result-wide v2

    .line 3164
    invoke-virtual {v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->getLastTimeVisible()J

    move-result-wide v4

    .line 3163
    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 3166
    .local v2, "lastTimeVisible":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v2

    const-wide/32 v6, 0x5265c00

    cmp-long v4, v4, v6

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-gez v4, :cond_0

    move v4, v5

    goto :goto_0

    :cond_0
    move v4, v6

    .line 3167
    .local v4, "recentlyLunch":Z
    :goto_0
    nop

    .line 3168
    invoke-virtual {v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->getAppLaunchCount()I

    move-result v7

    const/16 v8, 0x15

    if-le v7, v8, :cond_1

    goto :goto_1

    :cond_1
    move v5, v6

    .line 3172
    .local v5, "recentlyUsage":Z
    :goto_1
    if-eqz v4, :cond_2

    if-eqz v5, :cond_2

    .line 3173
    const/4 v1, 0x5

    goto :goto_2

    .line 3174
    :cond_2
    if-eqz v4, :cond_3

    .line 3176
    const/4 v1, 0x6

    goto :goto_2

    .line 3177
    :cond_3
    if-eqz v5, :cond_4

    .line 3179
    const/4 v1, 0x7

    .line 3182
    .end local v2    # "lastTimeVisible":J
    .end local v4    # "recentlyLunch":Z
    .end local v5    # "recentlyUsage":Z
    :cond_4
    :goto_2
    return v1
.end method

.method private updatePriorityScore()V
    .locals 5

    .line 3094
    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->computeTypeLevel()I

    move-result v0

    .line 3095
    .local v0, "typeLevel":I
    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->computeMemUsageLevel()I

    move-result v1

    .line 3096
    .local v1, "memLevel":I
    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->computeActiveAndUsageStatLevel()I

    move-result v2

    .line 3098
    .local v2, "activeUsageLevel":I
    mul-int/lit8 v3, v1, 0x1

    mul-int/lit8 v4, v0, 0xa

    add-int/2addr v3, v4

    mul-int/lit8 v4, v2, 0x64

    add-int/2addr v3, v4

    iput v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->mPriorityScore:I

    .line 3101
    return-void
.end method
