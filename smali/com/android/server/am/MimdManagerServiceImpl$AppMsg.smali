.class Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;
.super Ljava/lang/Object;
.source "MimdManagerServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/MimdManagerServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AppMsg"
.end annotation


# instance fields
.field mAppIdx:I

.field mPid:I

.field mUid:I

.field private final mUpdatePid:Ljava/lang/Object;

.field final synthetic this$0:Lcom/android/server/am/MimdManagerServiceImpl;


# direct methods
.method public constructor <init>(Lcom/android/server/am/MimdManagerServiceImpl;II)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/server/am/MimdManagerServiceImpl;
    .param p2, "idx"    # I
    .param p3, "pid"    # I

    .line 86
    iput-object p1, p0, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;->this$0:Lcom/android/server/am/MimdManagerServiceImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;->mUpdatePid:Ljava/lang/Object;

    .line 87
    iput p2, p0, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;->mAppIdx:I

    .line 88
    iput p3, p0, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;->mPid:I

    .line 89
    return-void
.end method


# virtual methods
.method public UpdatePid(I)V
    .locals 2
    .param p1, "pid"    # I

    .line 92
    iget-object v0, p0, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;->mUpdatePid:Ljava/lang/Object;

    monitor-enter v0

    .line 93
    :try_start_0
    iput p1, p0, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;->mPid:I

    .line 94
    monitor-exit v0

    .line 95
    return-void

    .line 94
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
