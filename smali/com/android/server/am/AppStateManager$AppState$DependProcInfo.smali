.class Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;
.super Ljava/lang/Object;
.source "AppStateManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/AppStateManager$AppState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DependProcInfo"
.end annotation


# instance fields
.field private mPid:I

.field private mProcessName:Ljava/lang/String;

.field private mUid:I

.field final synthetic this$1:Lcom/android/server/am/AppStateManager$AppState;


# direct methods
.method static bridge synthetic -$$Nest$fgetmPid(Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;->mPid:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmProcessName(Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;->mProcessName:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmUid(Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;->mUid:I

    return p0
.end method

.method private constructor <init>(Lcom/android/server/am/AppStateManager$AppState;IILjava/lang/String;)V
    .locals 1
    .param p1, "this$1"    # Lcom/android/server/am/AppStateManager$AppState;
    .param p2, "uid"    # I
    .param p3, "pid"    # I
    .param p4, "processName"    # Ljava/lang/String;

    .line 3240
    iput-object p1, p0, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3237
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;->mUid:I

    .line 3238
    iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;->mPid:I

    .line 3241
    iput p2, p0, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;->mUid:I

    .line 3242
    iput p3, p0, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;->mPid:I

    .line 3243
    iput-object p4, p0, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;->mProcessName:Ljava/lang/String;

    .line 3244
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/am/AppStateManager$AppState;IILjava/lang/String;Lcom/android/server/am/AppStateManager$AppState$DependProcInfo-IA;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;-><init>(Lcom/android/server/am/AppStateManager$AppState;IILjava/lang/String;)V

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .line 3248
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;->mProcessName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;->mUid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;->mPid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
