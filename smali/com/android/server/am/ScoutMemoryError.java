public class com.android.server.am.ScoutMemoryError {
	 /* .source "ScoutMemoryError.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 private static com.android.server.am.ScoutMemoryError memoryError;
	 /* # instance fields */
	 private android.content.Context mContext;
	 private com.android.server.am.ActivityManagerService mService;
	 /* # direct methods */
	 public static void $r8$lambda$8n2G-gTUp88IzYCZkG2H6HqO4Tw ( com.android.server.am.ScoutMemoryError p0, com.android.server.am.ProcessRecord p1, java.lang.String p2, java.lang.String p3, com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo p4 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/am/ScoutMemoryError;->lambda$showAppDisplayMemoryErrorDialog$1(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V */
		 return;
	 } // .end method
	 public static void $r8$lambda$g-8vQobdIi3vZa77d5NvdrS2wSM ( com.android.server.am.ScoutMemoryError p0, com.android.server.am.ProcessRecord p1, java.lang.String p2, java.lang.String p3 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/am/ScoutMemoryError;->lambda$showAppMemoryErrorDialog$0(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;)V */
		 return;
	 } // .end method
	 public com.android.server.am.ScoutMemoryError ( ) {
		 /* .locals 0 */
		 /* .line 13 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static com.android.server.am.ScoutMemoryError getInstance ( ) {
		 /* .locals 1 */
		 /* .line 22 */
		 v0 = com.android.server.am.ScoutMemoryError.memoryError;
		 /* if-nez v0, :cond_0 */
		 /* .line 23 */
		 /* new-instance v0, Lcom/android/server/am/ScoutMemoryError; */
		 /* invoke-direct {v0}, Lcom/android/server/am/ScoutMemoryError;-><init>()V */
		 /* .line 25 */
	 } // :cond_0
	 v0 = com.android.server.am.ScoutMemoryError.memoryError;
} // .end method
private java.lang.String getPackageLabelLocked ( com.android.server.am.ProcessRecord p0 ) {
	 /* .locals 3 */
	 /* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
	 /* .line 34 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 35 */
	 /* .local v0, "label":Ljava/lang/String; */
	 if ( p1 != null) { // if-eqz p1, :cond_0
		 (( com.android.server.am.ProcessRecord ) p1 ).getPkgList ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPkgList()Lcom/android/server/am/PackageList;
		 v1 = 		 (( com.android.server.am.PackageList ) v1 ).size ( ); // invoke-virtual {v1}, Lcom/android/server/am/PackageList;->size()I
		 int v2 = 1; // const/4 v2, 0x1
		 /* if-ne v1, v2, :cond_0 */
		 /* .line 36 */
		 v1 = this.mContext;
		 (( android.content.Context ) v1 ).getPackageManager ( ); // invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
		 v2 = this.info;
		 (( android.content.pm.PackageManager ) v1 ).getApplicationLabel ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
		 /* .line 37 */
		 /* .local v1, "labelChar":Ljava/lang/CharSequence; */
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 /* .line 38 */
			 /* .line 41 */
		 } // .end local v1 # "labelChar":Ljava/lang/CharSequence;
	 } // :cond_0
} // .end method
private void lambda$showAppDisplayMemoryErrorDialog$1 ( com.android.server.am.ProcessRecord p0, java.lang.String p1, java.lang.String p2, com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo p3 ) { //synthethic
	 /* .locals 0 */
	 /* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
	 /* .param p2, "packageLabel" # Ljava/lang/String; */
	 /* .param p3, "reason" # Ljava/lang/String; */
	 /* .param p4, "errorInfo" # Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo; */
	 /* .line 61 */
	 (( com.android.server.am.ScoutMemoryError ) p0 ).showDisplayMemoryErrorDialog ( p1, p2, p3, p4 ); // invoke-virtual {p0, p1, p2, p3, p4}, Lcom/android/server/am/ScoutMemoryError;->showDisplayMemoryErrorDialog(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V
	 return;
} // .end method
private void lambda$showAppMemoryErrorDialog$0 ( com.android.server.am.ProcessRecord p0, java.lang.String p1, java.lang.String p2 ) { //synthethic
	 /* .locals 0 */
	 /* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
	 /* .param p2, "packageLabel" # Ljava/lang/String; */
	 /* .param p3, "reason" # Ljava/lang/String; */
	 /* .line 50 */
	 (( com.android.server.am.ScoutMemoryError ) p0 ).showMemoryErrorDialog ( p1, p2, p3 ); // invoke-virtual {p0, p1, p2, p3}, Lcom/android/server/am/ScoutMemoryError;->showMemoryErrorDialog(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;)V
	 return;
} // .end method
/* # virtual methods */
public void init ( com.android.server.am.ActivityManagerService p0, android.content.Context p1 ) {
	 /* .locals 0 */
	 /* .param p1, "ams" # Lcom/android/server/am/ActivityManagerService; */
	 /* .param p2, "context" # Landroid/content/Context; */
	 /* .line 29 */
	 this.mService = p1;
	 /* .line 30 */
	 this.mContext = p2;
	 /* .line 31 */
	 return;
} // .end method
public Boolean scheduleCrashApp ( com.android.server.am.ProcessRecord p0, java.lang.String p1 ) {
	 /* .locals 4 */
	 /* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
	 /* .param p2, "reason" # Ljava/lang/String; */
	 /* .line 115 */
	 int v0 = 0; // const/4 v0, 0x0
	 if ( p1 != null) { // if-eqz p1, :cond_0
		 /* .line 116 */
		 (( com.android.server.am.ProcessRecord ) p1 ).getThread ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
		 /* .line 117 */
		 /* .local v1, "thread":Landroid/app/IApplicationThread; */
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 /* .line 118 */
			 v2 = this.mService;
			 /* monitor-enter v2 */
			 /* .line 119 */
			 int v3 = 0; // const/4 v3, 0x0
			 try { // :try_start_0
				 (( com.android.server.am.ProcessRecord ) p1 ).scheduleCrashLocked ( p2, v0, v3 ); // invoke-virtual {p1, p2, v0, v3}, Lcom/android/server/am/ProcessRecord;->scheduleCrashLocked(Ljava/lang/String;ILandroid/os/Bundle;)V
				 /* .line 120 */
				 /* monitor-exit v2 */
				 int v0 = 1; // const/4 v0, 0x1
				 /* .line 121 */
				 /* :catchall_0 */
				 /* move-exception v0 */
				 /* monitor-exit v2 */
				 /* :try_end_0 */
				 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
				 /* throw v0 */
				 /* .line 124 */
			 } // .end local v1 # "thread":Landroid/app/IApplicationThread;
		 } // :cond_0
	 } // .end method
	 public Boolean showAppDisplayMemoryErrorDialog ( com.android.server.am.ProcessRecord p0, java.lang.String p1, com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo p2 ) {
		 /* .locals 9 */
		 /* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
		 /* .param p2, "reason" # Ljava/lang/String; */
		 /* .param p3, "errorInfo" # Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo; */
		 /* .line 56 */
		 /* invoke-direct {p0, p1}, Lcom/android/server/am/ScoutMemoryError;->getPackageLabelLocked(Lcom/android/server/am/ProcessRecord;)Ljava/lang/String; */
		 /* .line 57 */
		 /* .local v6, "packageLabel":Ljava/lang/String; */
		 /* if-nez v6, :cond_0 */
		 /* .line 58 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* .line 60 */
	 } // :cond_0
	 v0 = this.mService;
	 v7 = this.mUiHandler;
	 /* new-instance v8, Lcom/android/server/am/ScoutMemoryError$$ExternalSyntheticLambda0; */
	 /* move-object v0, v8 */
	 /* move-object v1, p0 */
	 /* move-object v2, p1 */
	 /* move-object v3, v6 */
	 /* move-object v4, p2 */
	 /* move-object v5, p3 */
	 /* invoke-direct/range {v0 ..v5}, Lcom/android/server/am/ScoutMemoryError$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/am/ScoutMemoryError;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V */
	 (( android.os.Handler ) v7 ).postAtFrontOfQueue ( v8 ); // invoke-virtual {v7, v8}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z
	 /* .line 62 */
	 int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean showAppMemoryErrorDialog ( com.android.server.am.ProcessRecord p0, java.lang.String p1 ) {
	 /* .locals 3 */
	 /* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
	 /* .param p2, "reason" # Ljava/lang/String; */
	 /* .line 45 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/am/ScoutMemoryError;->getPackageLabelLocked(Lcom/android/server/am/ProcessRecord;)Ljava/lang/String; */
	 /* .line 46 */
	 /* .local v0, "packageLabel":Ljava/lang/String; */
	 /* if-nez v0, :cond_0 */
	 /* .line 47 */
	 int v1 = 0; // const/4 v1, 0x0
	 /* .line 49 */
} // :cond_0
v1 = this.mService;
v1 = this.mUiHandler;
/* new-instance v2, Lcom/android/server/am/ScoutMemoryError$$ExternalSyntheticLambda1; */
/* invoke-direct {v2, p0, p1, v0, p2}, Lcom/android/server/am/ScoutMemoryError$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/am/ScoutMemoryError;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;)V */
(( android.os.Handler ) v1 ).postAtFrontOfQueue ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z
/* .line 51 */
int v1 = 1; // const/4 v1, 0x1
} // .end method
public void showDisplayMemoryErrorDialog ( com.android.server.am.ProcessRecord p0, java.lang.String p1, java.lang.String p2, com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo p3 ) {
/* .locals 3 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "packageLabel" # Ljava/lang/String; */
/* .param p3, "reason" # Ljava/lang/String; */
/* .param p4, "errorInfo" # Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo; */
/* .line 87 */
int v0 = 0; // const/4 v0, 0x0
/* .line 89 */
/* .local v0, "showDialogSuccess":Z */
com.android.server.am.MiuiWarnings .getInstance ( );
/* new-instance v2, Lcom/android/server/am/ScoutMemoryError$2; */
/* invoke-direct {v2, p0, p1, p3, p4}, Lcom/android/server/am/ScoutMemoryError$2;-><init>(Lcom/android/server/am/ScoutMemoryError;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V */
v0 = (( com.android.server.am.MiuiWarnings ) v1 ).showWarningDialog ( p2, v2 ); // invoke-virtual {v1, p2, v2}, Lcom/android/server/am/MiuiWarnings;->showWarningDialog(Ljava/lang/String;Lcom/android/server/am/MiuiWarnings$WarningCallback;)Z
/* .line 107 */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 108 */
	 com.miui.server.stability.ScoutDisplayMemoryManager .getInstance ( );
	 int v2 = 1; // const/4 v2, 0x1
	 (( com.miui.server.stability.ScoutDisplayMemoryManager ) v1 ).setShowDialogState ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->setShowDialogState(Z)V
	 /* .line 110 */
} // :cond_0
final String v1 = "ScoutMemoryError"; // const-string v1, "ScoutMemoryError"
final String v2 = "occur memory leak, showWarningDialog fail"; // const-string v2, "occur memory leak, showWarningDialog fail"
android.util.Slog .d ( v1,v2 );
/* .line 112 */
} // :goto_0
return;
} // .end method
public void showMemoryErrorDialog ( com.android.server.am.ProcessRecord p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 3 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "packageLabel" # Ljava/lang/String; */
/* .param p3, "reason" # Ljava/lang/String; */
/* .line 66 */
int v0 = 0; // const/4 v0, 0x0
/* .line 68 */
/* .local v0, "showDialogSuccess":Z */
com.android.server.am.MiuiWarnings .getInstance ( );
/* new-instance v2, Lcom/android/server/am/ScoutMemoryError$1; */
/* invoke-direct {v2, p0, p1, p3}, Lcom/android/server/am/ScoutMemoryError$1;-><init>(Lcom/android/server/am/ScoutMemoryError;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V */
v0 = (( com.android.server.am.MiuiWarnings ) v1 ).showWarningDialog ( p2, v2 ); // invoke-virtual {v1, p2, v2}, Lcom/android/server/am/MiuiWarnings;->showWarningDialog(Ljava/lang/String;Lcom/android/server/am/MiuiWarnings$WarningCallback;)Z
/* .line 80 */
/* if-nez v0, :cond_0 */
/* .line 81 */
final String v1 = "ScoutMemoryError"; // const-string v1, "ScoutMemoryError"
final String v2 = "occur memory leak, showWarningDialog fail"; // const-string v2, "occur memory leak, showWarningDialog fail"
android.util.Slog .d ( v1,v2 );
/* .line 83 */
} // :cond_0
return;
} // .end method
