.class public Lcom/android/server/am/AppNotRespondingDialogImpl;
.super Lcom/android/server/am/AppNotRespondingDialogStub;
.source "AppNotRespondingDialogImpl.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Lcom/android/server/am/AppNotRespondingDialogStub;-><init>()V

    return-void
.end method


# virtual methods
.method onCreate(Lcom/android/server/am/AppNotRespondingDialog;)Z
    .locals 2
    .param p1, "dialog"    # Lcom/android/server/am/AppNotRespondingDialog;

    .line 28
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Lcom/android/server/am/AppNotRespondingDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x10201d8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setId(I)V

    .line 29
    const/4 v0, -0x2

    invoke-virtual {p1, v0}, Lcom/android/server/am/AppNotRespondingDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x10201dc

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setId(I)V

    .line 30
    const/4 v0, -0x3

    invoke-virtual {p1, v0}, Lcom/android/server/am/AppNotRespondingDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x10201da

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setId(I)V

    .line 31
    const/4 v0, 0x1

    return v0
.end method

.method onInit(Lcom/android/server/am/AppNotRespondingDialog;ZLandroid/os/Message;Landroid/os/Message;Landroid/os/Message;)V
    .locals 3
    .param p1, "dialog"    # Lcom/android/server/am/AppNotRespondingDialog;
    .param p2, "hasErrorReceiver"    # Z
    .param p3, "forceCloseMsg"    # Landroid/os/Message;
    .param p4, "waitMsg"    # Landroid/os/Message;
    .param p5, "waitAndReportMsg"    # Landroid/os/Message;

    .line 12
    invoke-virtual {p1}, Lcom/android/server/am/AppNotRespondingDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 13
    .local v0, "res":Landroid/content/res/Resources;
    nop

    .line 14
    const v1, 0x110f001b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 13
    const/4 v2, -0x1

    invoke-virtual {p1, v2, v1, p3}, Lcom/android/server/am/AppNotRespondingDialog;->setButton(ILjava/lang/CharSequence;Landroid/os/Message;)V

    .line 16
    nop

    .line 17
    const v1, 0x110f003a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 16
    const/4 v2, -0x2

    invoke-virtual {p1, v2, v1, p4}, Lcom/android/server/am/AppNotRespondingDialog;->setButton(ILjava/lang/CharSequence;Landroid/os/Message;)V

    .line 20
    if-eqz p2, :cond_0

    .line 21
    nop

    .line 22
    const v1, 0x110f0028

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 21
    const/4 v2, -0x3

    invoke-virtual {p1, v2, v1, p5}, Lcom/android/server/am/AppNotRespondingDialog;->setButton(ILjava/lang/CharSequence;Landroid/os/Message;)V

    .line 25
    :cond_0
    return-void
.end method
