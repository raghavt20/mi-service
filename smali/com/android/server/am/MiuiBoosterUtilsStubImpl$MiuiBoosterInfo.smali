.class Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;
.super Ljava/lang/Object;
.source "MiuiBoosterUtilsStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/MiuiBoosterUtilsStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MiuiBoosterInfo"
.end annotation


# instance fields
.field private level:I

.field private req_tids:[I

.field final synthetic this$0:Lcom/android/server/am/MiuiBoosterUtilsStubImpl;

.field private timeout:I

.field private uid:I


# direct methods
.method static bridge synthetic -$$Nest$fgetlevel(Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;->level:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetreq_tids(Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;)[I
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;->req_tids:[I

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgettimeout(Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;->timeout:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetuid(Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;->uid:I

    return p0
.end method

.method public constructor <init>(Lcom/android/server/am/MiuiBoosterUtilsStubImpl;I[III)V
    .locals 0
    .param p2, "uid"    # I
    .param p3, "req_tids"    # [I
    .param p4, "timeout"    # I
    .param p5, "level"    # I

    .line 441
    iput-object p1, p0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;->this$0:Lcom/android/server/am/MiuiBoosterUtilsStubImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 442
    iput p2, p0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;->uid:I

    .line 443
    iput-object p3, p0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;->req_tids:[I

    .line 444
    iput p4, p0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;->timeout:I

    .line 445
    iput p5, p0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;->level:I

    .line 446
    return-void
.end method
