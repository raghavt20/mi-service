.class Lcom/android/server/am/MemoryControlCloud$6;
.super Landroid/database/ContentObserver;
.source "MemoryControlCloud.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/MemoryControlCloud;->registerCloudAppHeapListObserver(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/MemoryControlCloud;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/android/server/am/MemoryControlCloud;Landroid/os/Handler;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/MemoryControlCloud;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 221
    iput-object p1, p0, Lcom/android/server/am/MemoryControlCloud$6;->this$0:Lcom/android/server/am/MemoryControlCloud;

    iput-object p3, p0, Lcom/android/server/am/MemoryControlCloud$6;->val$context:Landroid/content/Context;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 3
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 224
    if-eqz p2, :cond_2

    const-string v0, "cloud_memory_standard_appheap_list_json"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 225
    iget-object v1, p0, Lcom/android/server/am/MemoryControlCloud$6;->val$context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, -0x2

    invoke-static {v1, v0, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 227
    .local v0, "str":Ljava/lang/String;
    if-eqz v0, :cond_1

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 230
    :cond_0
    iget-object v1, p0, Lcom/android/server/am/MemoryControlCloud$6;->this$0:Lcom/android/server/am/MemoryControlCloud;

    invoke-static {v1}, Lcom/android/server/am/MemoryControlCloud;->-$$Nest$fgetmspc(Lcom/android/server/am/MemoryControlCloud;)Lcom/android/server/am/MemoryStandardProcessControl;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/am/MemoryStandardProcessControl;->callUpdateCloudAppHeapConfig(Ljava/lang/String;)V

    goto :goto_1

    .line 228
    :cond_1
    :goto_0
    return-void

    .line 232
    .end local v0    # "str":Ljava/lang/String;
    :cond_2
    :goto_1
    return-void
.end method
