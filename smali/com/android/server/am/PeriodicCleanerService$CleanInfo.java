class com.android.server.am.PeriodicCleanerService$CleanInfo {
	 /* .source "PeriodicCleanerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/PeriodicCleanerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "CleanInfo" */
} // .end annotation
/* # instance fields */
private Integer mAgingThresHold;
private android.util.SparseArray mCleanList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private Long mCleanTime;
private Integer mPressure;
private java.lang.String mReason;
private Boolean mValid;
final com.android.server.am.PeriodicCleanerService this$0; //synthetic
/* # direct methods */
static Integer -$$Nest$fgetmAgingThresHold ( com.android.server.am.PeriodicCleanerService$CleanInfo p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->mAgingThresHold:I */
} // .end method
static android.util.SparseArray -$$Nest$fgetmCleanList ( com.android.server.am.PeriodicCleanerService$CleanInfo p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCleanList;
} // .end method
static Long -$$Nest$fgetmCleanTime ( com.android.server.am.PeriodicCleanerService$CleanInfo p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->mCleanTime:J */
/* return-wide v0 */
} // .end method
static Integer -$$Nest$fgetmPressure ( com.android.server.am.PeriodicCleanerService$CleanInfo p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->mPressure:I */
} // .end method
static java.lang.String -$$Nest$fgetmReason ( com.android.server.am.PeriodicCleanerService$CleanInfo p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mReason;
} // .end method
 com.android.server.am.PeriodicCleanerService$CleanInfo ( ) {
/* .locals 0 */
/* .param p2, "currentTime" # J */
/* .param p4, "pressure" # I */
/* .param p5, "agingThresHold" # I */
/* .param p6, "reason" # Ljava/lang/String; */
/* .line 1964 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 1961 */
int p1 = 0; // const/4 p1, 0x0
/* iput-boolean p1, p0, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->mValid:Z */
/* .line 1962 */
/* new-instance p1, Landroid/util/SparseArray; */
/* invoke-direct {p1}, Landroid/util/SparseArray;-><init>()V */
this.mCleanList = p1;
/* .line 1965 */
/* iput-wide p2, p0, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->mCleanTime:J */
/* .line 1966 */
/* iput p4, p0, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->mPressure:I */
/* .line 1967 */
/* iput p5, p0, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->mAgingThresHold:I */
/* .line 1968 */
this.mReason = p6;
/* .line 1969 */
return;
} // .end method
/* # virtual methods */
public void addCleanList ( Integer p0, java.util.List p1 ) {
/* .locals 2 */
/* .param p1, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 1972 */
/* .local p2, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = this.mCleanList;
(( android.util.SparseArray ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1973 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Already exists old mapping for user "; // const-string v1, "Already exists old mapping for user "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "PeriodicCleaner"; // const-string v1, "PeriodicCleaner"
android.util.Slog .e ( v1,v0 );
/* .line 1975 */
} // :cond_0
v0 = this.mCleanList;
(( android.util.SparseArray ) v0 ).put ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 1976 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->mValid:Z */
/* .line 1977 */
return;
} // .end method
public Boolean isValid ( ) {
/* .locals 1 */
/* .line 1980 */
/* iget-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->mValid:Z */
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 1984 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "["; // const-string v1, "["
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->mCleanTime:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = ": "; // const-string v1, ": "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mCleanList;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = "]"; // const-string v1, "]"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
