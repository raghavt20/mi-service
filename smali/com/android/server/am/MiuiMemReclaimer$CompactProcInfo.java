class com.android.server.am.MiuiMemReclaimer$CompactProcInfo {
	 /* .source "MiuiMemReclaimer.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/MiuiMemReclaimer; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "CompactProcInfo" */
} // .end annotation
/* # instance fields */
public final java.lang.String action;
public Long compactDurationMillis;
public Long lastCompactTimeMillis;
public final com.miui.server.smartpower.IAppState$IRunningProcess proc;
public rss;
public rssAfter;
public rssDiff;
/* # direct methods */
static void -$$Nest$mcomputeRssDiff ( com.android.server.am.MiuiMemReclaimer$CompactProcInfo p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->computeRssDiff(Z)V */
return;
} // .end method
static Integer -$$Nest$mgetPid ( com.android.server.am.MiuiMemReclaimer$CompactProcInfo p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->getPid()I */
} // .end method
static java.lang.String -$$Nest$mgetProcessName ( com.android.server.am.MiuiMemReclaimer$CompactProcInfo p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->getProcessName()Ljava/lang/String; */
} // .end method
static Boolean -$$Nest$misRssValid ( com.android.server.am.MiuiMemReclaimer$CompactProcInfo p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->isRssValid()Z */
} // .end method
static java.lang.String -$$Nest$smgenCompactAction ( Integer p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
com.android.server.am.MiuiMemReclaimer$CompactProcInfo .genCompactAction ( p0,p1 );
} // .end method
public com.android.server.am.MiuiMemReclaimer$CompactProcInfo ( ) {
/* .locals 3 */
/* .param p1, "proc" # Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .param p2, "mode" # I */
/* .line 656 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 652 */
int v0 = 4; // const/4 v0, 0x4
/* new-array v1, v0, [J */
/* fill-array-data v1, :array_0 */
this.rssDiff = v1;
/* .line 653 */
/* const-wide/16 v1, -0x1 */
/* iput-wide v1, p0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->lastCompactTimeMillis:J */
/* .line 654 */
/* iput-wide v1, p0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->compactDurationMillis:J */
/* .line 657 */
this.proc = p1;
/* .line 658 */
v1 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getAmsProcState ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAmsProcState()I
com.android.server.am.MiuiMemReclaimer$CompactProcInfo .genCompactAction ( v1,p2 );
this.action = v1;
/* .line 659 */
v1 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPid()I
/* if-lez v1, :cond_0 */
/* .line 660 */
v0 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPid()I
android.os.Process .getRss ( v0 );
this.rss = v0;
/* .line 662 */
} // :cond_0
/* new-array v0, v0, [J */
/* fill-array-data v0, :array_1 */
this.rss = v0;
/* .line 664 */
} // :goto_0
return;
/* :array_0 */
/* .array-data 8 */
/* 0x0 */
/* 0x0 */
/* 0x0 */
/* 0x0 */
} // .end array-data
/* :array_1 */
/* .array-data 8 */
/* 0x0 */
/* 0x0 */
/* 0x0 */
/* 0x0 */
} // .end array-data
} // .end method
private void computeRssDiff ( Boolean p0 ) {
/* .locals 8 */
/* .param p1, "isCompacted" # Z */
/* .line 696 */
/* if-nez p1, :cond_0 */
/* .line 697 */
v0 = this.rss;
this.rssAfter = v0;
/* .line 698 */
return;
/* .line 700 */
} // :cond_0
v0 = this.proc;
v0 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v0 ).getPid ( ); // invoke-virtual {v0}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPid()I
android.os.Process .getRss ( v0 );
this.rssAfter = v0;
/* .line 701 */
v1 = this.rssDiff;
v2 = this.rss;
int v3 = 0; // const/4 v3, 0x0
/* aget-wide v4, v2, v3 */
/* aget-wide v6, v0, v3 */
/* sub-long/2addr v4, v6 */
/* aput-wide v4, v1, v3 */
/* .line 702 */
int v3 = 1; // const/4 v3, 0x1
/* aget-wide v4, v2, v3 */
/* aget-wide v6, v0, v3 */
/* sub-long/2addr v4, v6 */
/* aput-wide v4, v1, v3 */
/* .line 703 */
int v3 = 2; // const/4 v3, 0x2
/* aget-wide v4, v2, v3 */
/* aget-wide v6, v0, v3 */
/* sub-long/2addr v4, v6 */
/* aput-wide v4, v1, v3 */
/* .line 704 */
int v3 = 3; // const/4 v3, 0x3
/* aget-wide v4, v2, v3 */
/* aget-wide v6, v0, v3 */
/* sub-long/2addr v4, v6 */
/* aput-wide v4, v1, v3 */
/* .line 705 */
v0 = this.proc;
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v0 ).updatePss ( ); // invoke-virtual {v0}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->updatePss()V
/* .line 706 */
return;
} // .end method
private static java.lang.String genCompactAction ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p0, "procState" # I */
/* .param p1, "mode" # I */
/* .line 675 */
v0 = com.android.server.am.OomAdjusterImpl .isCacheProcessState ( p0 );
/* if-nez v0, :cond_1 */
int v0 = 2; // const/4 v0, 0x2
/* if-ne p1, v0, :cond_0 */
/* const/16 v0, 0xf */
/* if-ne p0, v0, :cond_0 */
/* .line 680 */
} // :cond_0
final String v0 = "anon"; // const-string v0, "anon"
/* .line 678 */
} // :cond_1
} // :goto_0
final String v0 = "all"; // const-string v0, "all"
} // .end method
private Integer getPid ( ) {
/* .locals 1 */
/* .line 667 */
v0 = this.proc;
v0 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v0 ).getPid ( ); // invoke-virtual {v0}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPid()I
} // .end method
private java.lang.String getProcessName ( ) {
/* .locals 1 */
/* .line 671 */
v0 = this.proc;
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v0 ).getProcessName ( ); // invoke-virtual {v0}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;
} // .end method
private Boolean isRssValid ( ) {
/* .locals 8 */
/* .line 685 */
v0 = this.rss;
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 686 */
/* .line 688 */
} // :cond_0
/* aget-wide v2, v0, v1 */
/* const-wide/16 v4, 0x0 */
/* cmp-long v2, v2, v4 */
int v3 = 1; // const/4 v3, 0x1
/* if-nez v2, :cond_1 */
/* aget-wide v6, v0, v3 */
/* cmp-long v2, v6, v4 */
/* if-nez v2, :cond_1 */
int v2 = 2; // const/4 v2, 0x2
/* aget-wide v6, v0, v2 */
/* cmp-long v2, v6, v4 */
/* if-nez v2, :cond_1 */
int v2 = 3; // const/4 v2, 0x3
/* aget-wide v6, v0, v2 */
/* cmp-long v0, v6, v4 */
/* if-nez v0, :cond_1 */
/* .line 690 */
/* .line 692 */
} // :cond_1
} // .end method
/* # virtual methods */
public java.lang.String toString ( ) {
/* .locals 23 */
/* .line 709 */
/* move-object/from16 v0, p0 */
int v1 = 0; // const/4 v1, 0x0
/* .line 710 */
/* .local v1, "date":Ljava/lang/String; */
/* iget-wide v2, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->lastCompactTimeMillis:J */
/* const-wide/16 v4, 0x0 */
/* cmp-long v2, v2, v4 */
/* if-lez v2, :cond_0 */
/* .line 711 */
/* new-instance v2, Ljava/text/SimpleDateFormat; */
/* const-string/jumbo v3, "yy-MM-dd HH:mm:ss.SS" */
/* invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V */
/* .line 712 */
/* .local v2, "dateformat":Ljava/text/SimpleDateFormat; */
/* iget-wide v3, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->lastCompactTimeMillis:J */
java.lang.Long .valueOf ( v3,v4 );
(( java.text.SimpleDateFormat ) v2 ).format ( v3 ); // invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;
/* .line 714 */
} // .end local v2 # "dateformat":Ljava/text/SimpleDateFormat;
} // :cond_0
v2 = this.proc;
/* .line 716 */
v2 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v2 ).getPid ( ); // invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPid()I
java.lang.Integer .valueOf ( v2 );
v2 = this.proc;
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v2 ).getProcessName ( ); // invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;
v2 = this.proc;
v2 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v2 ).getUid ( ); // invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUid()I
java.lang.Integer .valueOf ( v2 );
v2 = this.proc;
v2 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v2 ).getAdj ( ); // invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I
java.lang.Integer .valueOf ( v2 );
v2 = this.rss;
int v15 = 0; // const/4 v15, 0x0
/* aget-wide v7, v2, v15 */
/* .line 717 */
java.lang.Long .valueOf ( v7,v8 );
v2 = this.rss;
/* const/16 v16, 0x1 */
/* aget-wide v8, v2, v16 */
java.lang.Long .valueOf ( v8,v9 );
v2 = this.rss;
/* const/16 v17, 0x2 */
/* aget-wide v9, v2, v17 */
java.lang.Long .valueOf ( v9,v10 );
v2 = this.rss;
/* const/16 v18, 0x3 */
/* aget-wide v10, v2, v18 */
java.lang.Long .valueOf ( v10,v11 );
v2 = this.rssAfter;
/* aget-wide v11, v2, v15 */
/* .line 718 */
java.lang.Long .valueOf ( v11,v12 );
v2 = this.rssAfter;
/* aget-wide v12, v2, v16 */
java.lang.Long .valueOf ( v12,v13 );
v2 = this.rssAfter;
/* aget-wide v13, v2, v17 */
java.lang.Long .valueOf ( v13,v14 );
v2 = this.rssAfter;
/* aget-wide v19, v2, v18 */
/* invoke-static/range {v19 ..v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long; */
v2 = this.rssDiff;
/* aget-wide v19, v2, v15 */
/* .line 719 */
/* invoke-static/range {v19 ..v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long; */
v2 = this.rssDiff;
/* aget-wide v19, v2, v16 */
/* invoke-static/range {v19 ..v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long; */
v2 = this.rssDiff;
/* aget-wide v19, v2, v17 */
/* invoke-static/range {v19 ..v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long; */
v2 = this.rssDiff;
/* aget-wide v18, v2, v18 */
/* invoke-static/range {v18 ..v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long; */
/* move-object/from16 v22, v3 */
/* iget-wide v2, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->compactDurationMillis:J */
/* .line 720 */
java.lang.Long .valueOf ( v2,v3 );
v2 = this.action;
/* move-object/from16 v21, v2 */
/* move-object/from16 v19, v1 */
/* move-object/from16 v3, v22 */
/* filled-new-array/range {v3 ..v21}, [Ljava/lang/Object; */
/* .line 714 */
final String v3 = "%d %s uid:%d adj:%d rss[%d, %d, %d, %d] rssAfter[%d, %d, %d, %d] rssDiff[%d, %d, %d, %d] lastCompactTime:%s compactDuration:%dms action:%s"; // const-string v3, "%d %s uid:%d adj:%d rss[%d, %d, %d, %d] rssAfter[%d, %d, %d, %d] rssDiff[%d, %d, %d, %d] lastCompactTime:%s compactDuration:%dms action:%s"
java.lang.String .format ( v3,v2 );
} // .end method
