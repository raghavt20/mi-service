.class Lcom/android/server/am/MiuiMemoryService$ConnectionHandler;
.super Ljava/lang/Object;
.source "MiuiMemoryService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/MiuiMemoryService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConnectionHandler"
.end annotation


# instance fields
.field private mInput:Ljava/io/InputStreamReader;

.field private mIsContinue:Z

.field private mSocket:Landroid/net/LocalSocket;

.field final synthetic this$0:Lcom/android/server/am/MiuiMemoryService;


# direct methods
.method public constructor <init>(Lcom/android/server/am/MiuiMemoryService;Landroid/net/LocalSocket;)V
    .locals 0
    .param p2, "clientSocket"    # Landroid/net/LocalSocket;

    .line 109
    iput-object p1, p0, Lcom/android/server/am/MiuiMemoryService$ConnectionHandler;->this$0:Lcom/android/server/am/MiuiMemoryService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/android/server/am/MiuiMemoryService$ConnectionHandler;->mIsContinue:Z

    .line 107
    const/4 p1, 0x0

    iput-object p1, p0, Lcom/android/server/am/MiuiMemoryService$ConnectionHandler;->mInput:Ljava/io/InputStreamReader;

    .line 110
    iput-object p2, p0, Lcom/android/server/am/MiuiMemoryService$ConnectionHandler;->mSocket:Landroid/net/LocalSocket;

    .line 111
    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .line 120
    const-string v0, "mi_reclaim connection: "

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mi_reclaim new connection: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/am/MiuiMemoryService$ConnectionHandler;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v2}, Landroid/net/LocalSocket;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiMemoryService"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    const/4 v1, 0x0

    .line 123
    .local v1, "bufferedReader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v3, Ljava/io/InputStreamReader;

    iget-object v4, p0, Lcom/android/server/am/MiuiMemoryService$ConnectionHandler;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v4}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    iput-object v3, p0, Lcom/android/server/am/MiuiMemoryService$ConnectionHandler;->mInput:Ljava/io/InputStreamReader;

    .line 124
    new-instance v3, Ljava/io/BufferedReader;

    iget-object v4, p0, Lcom/android/server/am/MiuiMemoryService$ConnectionHandler;->mInput:Ljava/io/InputStreamReader;

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object v1, v3

    .line 125
    :goto_0
    iget-boolean v3, p0, Lcom/android/server/am/MiuiMemoryService$ConnectionHandler;->mIsContinue:Z

    if-eqz v3, :cond_3

    .line 126
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .line 127
    .local v3, "data":Ljava/lang/String;
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 128
    .local v4, "result":[Ljava/lang/String;
    const/4 v5, 0x0

    aget-object v6, v4, v5

    if-eqz v6, :cond_2

    const/4 v6, 0x1

    aget-object v7, v4, v6

    if-nez v7, :cond_0

    goto :goto_1

    .line 132
    :cond_0
    aget-object v5, v4, v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 133
    .local v5, "vmPressureLevel":I
    aget-object v6, v4, v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 134
    .local v6, "vmPressurePolicy":I
    sget-boolean v7, Lcom/android/server/am/MiuiMemoryService;->DEBUG:Z

    if-eqz v7, :cond_1

    .line 135
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "vmPressureLevel = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " , vmPressurePolicy = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    :cond_1
    iget-object v7, p0, Lcom/android/server/am/MiuiMemoryService$ConnectionHandler;->this$0:Lcom/android/server/am/MiuiMemoryService;

    invoke-static {v7, v5, v6}, Lcom/android/server/am/MiuiMemoryService;->-$$Nest$mhandleReclaimTrigger(Lcom/android/server/am/MiuiMemoryService;II)V

    .line 139
    .end local v3    # "data":Ljava/lang/String;
    .end local v4    # "result":[Ljava/lang/String;
    .end local v5    # "vmPressureLevel":I
    .end local v6    # "vmPressurePolicy":I
    goto :goto_0

    .line 129
    .restart local v3    # "data":Ljava/lang/String;
    .restart local v4    # "result":[Ljava/lang/String;
    :cond_2
    :goto_1
    const-string v5, "Received mi_reclaim data error"

    invoke-static {v2, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 130
    goto :goto_0

    .line 147
    .end local v3    # "data":Ljava/lang/String;
    .end local v4    # "result":[Ljava/lang/String;
    :cond_3
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 150
    :cond_4
    :goto_2
    goto :goto_3

    .line 148
    :catch_0
    move-exception v0

    .line 151
    goto :goto_3

    .line 146
    :catchall_0
    move-exception v0

    goto :goto_4

    .line 142
    :catch_1
    move-exception v3

    .line 143
    .local v3, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {p0}, Lcom/android/server/am/MiuiMemoryService$ConnectionHandler;->terminate()V

    .line 144
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lcom/android/server/am/MiuiMemoryService$ConnectionHandler;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v4}, Landroid/net/LocalSocket;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " Exception"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 147
    .end local v3    # "e":Ljava/lang/Exception;
    if-eqz v1, :cond_4

    :try_start_3
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_2

    .line 140
    :catch_2
    move-exception v3

    .line 141
    .local v3, "e":Ljava/io/IOException;
    :try_start_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lcom/android/server/am/MiuiMemoryService$ConnectionHandler;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v4}, Landroid/net/LocalSocket;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " IOException"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 147
    .end local v3    # "e":Ljava/io/IOException;
    if-eqz v1, :cond_4

    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_2

    .line 152
    :goto_3
    return-void

    .line 147
    :goto_4
    if-eqz v1, :cond_5

    :try_start_6
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_5

    .line 148
    :catch_3
    move-exception v2

    goto :goto_6

    .line 150
    :cond_5
    :goto_5
    nop

    .line 151
    :goto_6
    throw v0
.end method

.method public terminate()V
    .locals 2

    .line 114
    const-string v0, "MiuiMemoryService"

    const-string v1, "Reclaim trigger terminate!"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/am/MiuiMemoryService$ConnectionHandler;->mIsContinue:Z

    .line 116
    return-void
.end method
