public class com.android.server.am.PreloadAppControllerImpl implements com.android.server.am.PreloadAppControllerStub {
	 /* .source "PreloadAppControllerImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;, */
	 /* Lcom/android/server/am/PreloadAppControllerImpl$CallbackDeathRecipient; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Integer APP_NOT_PRELOAD;
public static final Boolean DEBUG;
private static Boolean ENABLE;
private static final Integer FLAG_DISPLAY_ID_SHIFT;
private static final Integer MAX_PRELOAD_COUNT;
public static final Integer MIN_TOTAL_MEMORY;
public static final Integer PACKAGE_NAME_EMPTY;
public static final Integer PERMISSION_DENIED;
public static final java.lang.String PREFIX;
public static final Integer START_ALREADY_LOAD_APP;
public static final Integer START_CONFIG_NULL;
public static final Integer START_ERROR;
public static final Integer START_FAIL_SPEED_TEST;
public static final Integer START_FORBIDDEN_LOW_MEMORY;
public static final Integer START_FORBIDDEN_SYSTEM_APP;
public static final Integer START_FREQUENTLY_KILLED;
public static final Integer START_GREEZE_IS_DISABLE;
public static final Integer START_LAUNCH_INTENT_NOT_FOUND;
public static final Integer START_NO_INTERCEPT;
public static final Integer START_PACKAGE_NAME_IN_BLACK_LIST;
public static final Integer START_PACKAGE_NAME_NOT_FOUND;
public static final Integer START_PERSISTENT_APP;
public static final Integer START_PRELOAD_IS_DISABLE;
public static final Integer START_SWITCH_PRELOAD_STATE_ERROR;
public static final Integer START_TOP_APP_IN_BLACK_LIST;
private static final java.lang.String TAG;
private static volatile Integer mPreloadLifecycleState;
private static java.util.Set sBlackList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static final sDefaultSchedAffinity;
private static com.android.server.am.PreloadAppControllerImpl sInstance;
public static final sPreloadSchedAffinity;
private static java.util.Set sRelaunchBlackList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.Set sTopAppBlackList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.Set sWindowBlackList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private com.android.server.am.ActivityManagerService mActivityManagerService;
private java.util.List mAllPreloadAppInfos;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/android/server/wm/PreloadLifecycle;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.am.PreloadAppControllerImpl$PreloadHandler mHandler;
private final java.lang.Object mPreloadingAppLock;
private com.android.server.am.ProcessManagerService mProcessManagerService;
private volatile Boolean mSpeedTestState;
/* # direct methods */
static com.android.server.wm.PreloadLifecycle -$$Nest$mremovePreloadAppInfo ( com.android.server.am.PreloadAppControllerImpl p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/PreloadAppControllerImpl;->removePreloadAppInfo(Ljava/lang/String;)Lcom/android/server/wm/PreloadLifecycle; */
} // .end method
static Integer -$$Nest$mstartPreloadApp ( com.android.server.am.PreloadAppControllerImpl p0, com.android.server.wm.PreloadLifecycle p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/am/PreloadAppControllerImpl;->startPreloadApp(Lcom/android/server/wm/PreloadLifecycle;)I */
} // .end method
static void -$$Nest$munRegistPreloadCallback ( com.android.server.am.PreloadAppControllerImpl p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/PreloadAppControllerImpl;->unRegistPreloadCallback(I)V */
return;
} // .end method
static void -$$Nest$sfputmPreloadLifecycleState ( Integer p0 ) { //bridge//synthethic
/* .locals 0 */
return;
} // .end method
static com.android.server.am.PreloadAppControllerImpl ( ) {
/* .locals 2 */
/* .line 69 */
/* nop */
/* .line 70 */
final String v0 = "persist.sys.preload.debug"; // const-string v0, "persist.sys.preload.debug"
int v1 = 1; // const/4 v1, 0x1
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.am.PreloadAppControllerImpl.DEBUG = (v0!= 0);
/* .line 71 */
/* nop */
/* .line 72 */
final String v0 = "persist.sys.preload.enable"; // const-string v0, "persist.sys.preload.enable"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.am.PreloadAppControllerImpl.ENABLE = (v0!= 0);
/* .line 75 */
/* const/16 v0, 0x8 */
/* new-array v0, v0, [I */
/* fill-array-data v0, :array_0 */
/* .line 76 */
int v0 = 6; // const/4 v0, 0x6
/* new-array v0, v0, [I */
/* fill-array-data v0, :array_1 */
/* .line 77 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 78 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 79 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 80 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 85 */
v0 = com.android.server.am.PreloadAppControllerImpl.sBlackList;
final String v1 = "com.xiaomi.youpin"; // const-string v1, "com.xiaomi.youpin"
/* .line 86 */
v0 = com.android.server.am.PreloadAppControllerImpl.sBlackList;
final String v1 = "com.ss.android.ugc.live"; // const-string v1, "com.ss.android.ugc.live"
/* .line 87 */
v0 = com.android.server.am.PreloadAppControllerImpl.sWindowBlackList;
final String v1 = "com.miui.systemAdSolution"; // const-string v1, "com.miui.systemAdSolution"
/* .line 88 */
v0 = com.android.server.am.PreloadAppControllerImpl.sTopAppBlackList;
final String v1 = "com.android.camera"; // const-string v1, "com.android.camera"
/* .line 89 */
v0 = com.android.server.am.PreloadAppControllerImpl.sRelaunchBlackList;
final String v1 = "com.ss.android.ugc"; // const-string v1, "com.ss.android.ugc"
/* .line 90 */
return;
/* nop */
/* :array_0 */
/* .array-data 4 */
/* 0x0 */
/* 0x1 */
/* 0x2 */
/* 0x3 */
/* 0x4 */
/* 0x5 */
/* 0x6 */
/* 0x7 */
} // .end array-data
/* :array_1 */
/* .array-data 4 */
/* 0x0 */
/* 0x1 */
/* 0x2 */
/* 0x3 */
/* 0x4 */
/* 0x5 */
} // .end array-data
} // .end method
public com.android.server.am.PreloadAppControllerImpl ( ) {
/* .locals 1 */
/* .line 43 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 94 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mPreloadingAppLock = v0;
/* .line 95 */
/* new-instance v0, Ljava/util/LinkedList; */
/* invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V */
this.mAllPreloadAppInfos = v0;
return;
} // .end method
private com.android.server.wm.PreloadLifecycle createPreloadLifeCycle ( Boolean p0, java.lang.String p1, miui.process.LifecycleConfig p2 ) {
/* .locals 7 */
/* .param p1, "ignoreMemory" # Z */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "config" # Lmiui/process/LifecycleConfig; */
/* .line 250 */
/* new-instance v0, Lcom/android/server/wm/PreloadLifecycle; */
/* invoke-direct {v0, p1, p2, p3}, Lcom/android/server/wm/PreloadLifecycle;-><init>(ZLjava/lang/String;Lmiui/process/LifecycleConfig;)V */
/* .line 251 */
/* .local v0, "lifecycle":Lcom/android/server/wm/PreloadLifecycle; */
/* sget-boolean v1, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 252 */
final String v1 = "PreloadAppControllerImpl"; // const-string v1, "PreloadAppControllerImpl"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "received preloadApp request, "; // const-string v3, "received preloadApp request, "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v2 );
/* .line 254 */
} // :cond_0
v1 = this.mPreloadingAppLock;
/* monitor-enter v1 */
/* .line 255 */
try { // :try_start_0
v2 = v2 = this.mAllPreloadAppInfos;
/* .line 256 */
/* .local v2, "size":I */
/* const/16 v3, 0xa */
int v4 = 0; // const/4 v4, 0x0
/* if-lt v2, v3, :cond_1 */
/* .line 257 */
final String v3 = "PreloadAppControllerImpl"; // const-string v3, "PreloadAppControllerImpl"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = " applications have been preloaded, no more preload"; // const-string v6, " applications have been preloaded, no more preload"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v3,v5 );
/* .line 258 */
/* monitor-exit v1 */
/* .line 260 */
} // :cond_1
v3 = com.android.server.am.PreloadAppControllerImpl .getUidFromPackageName ( p2 );
/* .line 261 */
/* .local v3, "uid":I */
/* if-gez v3, :cond_2 */
/* .line 262 */
v4 = this.mAllPreloadAppInfos;
/* .line 263 */
/* monitor-exit v1 */
/* .line 265 */
} // :cond_2
v5 = this.mActivityManagerService;
v5 = this.mProcessList;
/* .line 266 */
(( com.android.server.am.ProcessList ) v5 ).getProcessNamesLOSP ( ); // invoke-virtual {v5}, Lcom/android/server/am/ProcessList;->getProcessNamesLOSP()Lcom/android/server/am/ProcessList$MyProcessMap;
(( com.android.server.am.ProcessList$MyProcessMap ) v5 ).get ( p2, v3 ); // invoke-virtual {v5, p2, v3}, Lcom/android/server/am/ProcessList$MyProcessMap;->get(Ljava/lang/String;I)Ljava/lang/Object;
/* check-cast v5, Lcom/android/server/am/ProcessRecord; */
/* .line 267 */
/* .local v5, "app":Lcom/android/server/am/ProcessRecord; */
/* if-nez v5, :cond_3 */
/* .line 268 */
/* monitor-exit v1 */
/* .line 271 */
} // .end local v2 # "size":I
} // .end local v3 # "uid":I
} // .end local v5 # "app":Lcom/android/server/am/ProcessRecord;
} // :cond_3
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 272 */
final String v1 = "PreloadAppControllerImpl"; // const-string v1, "PreloadAppControllerImpl"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "preloadApp: "; // const-string v3, "preloadApp: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " already exits, skip it"; // const-string v3, " already exits, skip it"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v2 );
/* .line 273 */
/* .line 271 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
} // .end method
public static com.android.server.am.PreloadAppControllerImpl getInstance ( ) {
/* .locals 1 */
/* .line 125 */
/* const-class v0, Lcom/android/server/am/PreloadAppControllerStub; */
com.miui.base.MiuiStubUtil .getImpl ( v0 );
/* check-cast v0, Lcom/android/server/am/PreloadAppControllerImpl; */
} // .end method
public static java.lang.String getPackageName ( Integer p0 ) {
/* .locals 4 */
/* .param p0, "uid" # I */
/* .line 289 */
v0 = com.android.server.am.PreloadAppControllerImpl.sInstance;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 290 */
v0 = this.mPreloadingAppLock;
/* monitor-enter v0 */
/* .line 291 */
try { // :try_start_0
v1 = com.android.server.am.PreloadAppControllerImpl.sInstance;
v1 = this.mAllPreloadAppInfos;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/android/server/wm/PreloadLifecycle; */
/* .line 292 */
/* .local v2, "lifecycle":Lcom/android/server/wm/PreloadLifecycle; */
v3 = (( com.android.server.wm.PreloadLifecycle ) v2 ).getUid ( ); // invoke-virtual {v2}, Lcom/android/server/wm/PreloadLifecycle;->getUid()I
/* if-ne v3, p0, :cond_0 */
/* .line 293 */
(( com.android.server.wm.PreloadLifecycle ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Lcom/android/server/wm/PreloadLifecycle;->getPackageName()Ljava/lang/String;
/* monitor-exit v0 */
/* .line 295 */
} // .end local v2 # "lifecycle":Lcom/android/server/wm/PreloadLifecycle;
} // :cond_0
/* .line 296 */
} // :cond_1
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 298 */
} // :cond_2
} // :goto_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Integer getPreloadType ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 315 */
v0 = this.mPreloadingAppLock;
/* monitor-enter v0 */
/* .line 316 */
try { // :try_start_0
v1 = this.mAllPreloadAppInfos;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/android/server/wm/PreloadLifecycle; */
/* .line 317 */
/* .local v2, "lifecycle":Lcom/android/server/wm/PreloadLifecycle; */
(( com.android.server.wm.PreloadLifecycle ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Lcom/android/server/wm/PreloadLifecycle;->getPackageName()Ljava/lang/String;
v3 = (( java.lang.String ) p1 ).equals ( v3 ); // invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 318 */
v1 = (( com.android.server.wm.PreloadLifecycle ) v2 ).getPreloadType ( ); // invoke-virtual {v2}, Lcom/android/server/wm/PreloadLifecycle;->getPreloadType()I
/* monitor-exit v0 */
/* .line 320 */
} // .end local v2 # "lifecycle":Lcom/android/server/wm/PreloadLifecycle;
} // :cond_0
/* .line 321 */
} // :cond_1
/* monitor-exit v0 */
/* .line 322 */
int v0 = -1; // const/4 v0, -0x1
/* .line 321 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public static Integer getUidFromPackageName ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .line 302 */
v0 = com.android.server.am.PreloadAppControllerImpl.sInstance;
if ( v0 != null) { // if-eqz v0, :cond_2
if ( p0 != null) { // if-eqz p0, :cond_2
/* .line 303 */
v0 = this.mPreloadingAppLock;
/* monitor-enter v0 */
/* .line 304 */
try { // :try_start_0
v1 = com.android.server.am.PreloadAppControllerImpl.sInstance;
v1 = this.mAllPreloadAppInfos;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/android/server/wm/PreloadLifecycle; */
/* .line 305 */
/* .local v2, "lifecycle":Lcom/android/server/wm/PreloadLifecycle; */
(( com.android.server.wm.PreloadLifecycle ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Lcom/android/server/wm/PreloadLifecycle;->getPackageName()Ljava/lang/String;
v3 = (( java.lang.String ) p0 ).equals ( v3 ); // invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 306 */
v1 = (( com.android.server.wm.PreloadLifecycle ) v2 ).getUid ( ); // invoke-virtual {v2}, Lcom/android/server/wm/PreloadLifecycle;->getUid()I
/* monitor-exit v0 */
/* .line 308 */
} // .end local v2 # "lifecycle":Lcom/android/server/wm/PreloadLifecycle;
} // :cond_0
/* .line 309 */
} // :cond_1
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 311 */
} // :cond_2
} // :goto_1
int v0 = -1; // const/4 v0, -0x1
} // .end method
public static Boolean inBlackList ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .line 447 */
v0 = v0 = com.android.server.am.PreloadAppControllerImpl.sBlackList;
} // .end method
public static Boolean inRelaunchBlackList ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .line 459 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p0, :cond_0 */
/* .line 460 */
/* .line 463 */
} // :cond_0
v1 = com.android.server.am.PreloadAppControllerImpl.sRelaunchBlackList;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Ljava/lang/String; */
/* .line 464 */
/* .local v2, "appName":Ljava/lang/String; */
v3 = (( java.lang.String ) p0 ).contains ( v2 ); // invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 465 */
int v0 = 1; // const/4 v0, 0x1
/* .line 467 */
} // .end local v2 # "appName":Ljava/lang/String;
} // :cond_1
/* .line 468 */
} // :cond_2
} // .end method
public static Boolean inTopAppBlackList ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .line 455 */
v0 = v0 = com.android.server.am.PreloadAppControllerImpl.sTopAppBlackList;
} // .end method
public static Boolean inWindowBlackList ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .line 451 */
v0 = v0 = com.android.server.am.PreloadAppControllerImpl.sWindowBlackList;
} // .end method
private void initPreloadEnableState ( ) {
/* .locals 4 */
/* .line 115 */
android.os.Process .getTotalMemory ( );
/* move-result-wide v0 */
/* const/16 v2, 0x1e */
/* shr-long/2addr v0, v2 */
/* const-wide/16 v2, 0x6 */
/* cmp-long v0, v0, v2 */
/* if-gez v0, :cond_0 */
/* .line 116 */
int v0 = 0; // const/4 v0, 0x0
com.android.server.am.PreloadAppControllerImpl.ENABLE = (v0!= 0);
/* .line 117 */
final String v0 = "PreloadAppControllerImpl"; // const-string v0, "PreloadAppControllerImpl"
final String v1 = "preloadApp low total memory, disable"; // const-string v1, "preloadApp low total memory, disable"
android.util.Slog .e ( v0,v1 );
/* .line 118 */
return;
/* .line 121 */
} // :cond_0
/* sget-boolean v0, Lcom/android/server/am/PreloadAppControllerImpl;->ENABLE:Z */
v1 = com.android.server.wm.PreloadStateManagerImpl .checkEnablePreload ( );
/* and-int/2addr v0, v1 */
com.android.server.am.PreloadAppControllerImpl.ENABLE = (v0!= 0);
/* .line 122 */
return;
} // .end method
public static Boolean isEnable ( ) {
/* .locals 1 */
/* .line 129 */
/* sget-boolean v0, Lcom/android/server/am/PreloadAppControllerImpl;->ENABLE:Z */
} // .end method
public static Long queryFreezeTimeoutFromUid ( Integer p0 ) {
/* .locals 5 */
/* .param p0, "uid" # I */
/* .line 498 */
v0 = com.android.server.am.PreloadAppControllerImpl.sInstance;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 499 */
v0 = this.mPreloadingAppLock;
/* monitor-enter v0 */
/* .line 500 */
try { // :try_start_0
v1 = com.android.server.am.PreloadAppControllerImpl.sInstance;
v1 = this.mAllPreloadAppInfos;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/android/server/wm/PreloadLifecycle; */
/* .line 501 */
/* .local v2, "lifecycle":Lcom/android/server/wm/PreloadLifecycle; */
v3 = (( com.android.server.wm.PreloadLifecycle ) v2 ).getUid ( ); // invoke-virtual {v2}, Lcom/android/server/wm/PreloadLifecycle;->getUid()I
/* if-ne v3, p0, :cond_0 */
/* .line 502 */
(( com.android.server.wm.PreloadLifecycle ) v2 ).getFreezeTimeout ( ); // invoke-virtual {v2}, Lcom/android/server/wm/PreloadLifecycle;->getFreezeTimeout()J
/* move-result-wide v3 */
/* monitor-exit v0 */
/* return-wide v3 */
/* .line 504 */
} // .end local v2 # "lifecycle":Lcom/android/server/wm/PreloadLifecycle;
} // :cond_0
/* .line 505 */
} // :cond_1
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 507 */
} // :cond_2
} // :goto_1
/* const-wide/16 v0, 0x5208 */
/* return-wide v0 */
} // .end method
public static Long queryKillTimeoutFromUid ( Integer p0 ) {
/* .locals 5 */
/* .param p0, "uid" # I */
/* .line 485 */
v0 = com.android.server.am.PreloadAppControllerImpl.sInstance;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 486 */
v0 = this.mPreloadingAppLock;
/* monitor-enter v0 */
/* .line 487 */
try { // :try_start_0
v1 = com.android.server.am.PreloadAppControllerImpl.sInstance;
v1 = this.mAllPreloadAppInfos;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/android/server/wm/PreloadLifecycle; */
/* .line 488 */
/* .local v2, "lifecycle":Lcom/android/server/wm/PreloadLifecycle; */
v3 = (( com.android.server.wm.PreloadLifecycle ) v2 ).getUid ( ); // invoke-virtual {v2}, Lcom/android/server/wm/PreloadLifecycle;->getUid()I
/* if-ne v3, p0, :cond_0 */
/* .line 489 */
(( com.android.server.wm.PreloadLifecycle ) v2 ).getKillTimeout ( ); // invoke-virtual {v2}, Lcom/android/server/wm/PreloadLifecycle;->getKillTimeout()J
/* move-result-wide v3 */
/* monitor-exit v0 */
/* return-wide v3 */
/* .line 491 */
} // .end local v2 # "lifecycle":Lcom/android/server/wm/PreloadLifecycle;
} // :cond_0
/* .line 492 */
} // :cond_1
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 494 */
} // :cond_2
} // :goto_1
/* const-wide/32 v0, 0x493e0 */
/* return-wide v0 */
} // .end method
public static querySchedAffinityFromUid ( Integer p0 ) {
/* .locals 4 */
/* .param p0, "uid" # I */
/* .line 472 */
v0 = com.android.server.am.PreloadAppControllerImpl.sInstance;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 473 */
v0 = this.mPreloadingAppLock;
/* monitor-enter v0 */
/* .line 474 */
try { // :try_start_0
v1 = com.android.server.am.PreloadAppControllerImpl.sInstance;
v1 = this.mAllPreloadAppInfos;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/android/server/wm/PreloadLifecycle; */
/* .line 475 */
/* .local v2, "lifecycle":Lcom/android/server/wm/PreloadLifecycle; */
v3 = (( com.android.server.wm.PreloadLifecycle ) v2 ).getUid ( ); // invoke-virtual {v2}, Lcom/android/server/wm/PreloadLifecycle;->getUid()I
/* if-ne v3, p0, :cond_0 */
/* .line 476 */
(( com.android.server.wm.PreloadLifecycle ) v2 ).getPreloadSchedAffinity ( ); // invoke-virtual {v2}, Lcom/android/server/wm/PreloadLifecycle;->getPreloadSchedAffinity()[I
/* monitor-exit v0 */
/* .line 478 */
} // .end local v2 # "lifecycle":Lcom/android/server/wm/PreloadLifecycle;
} // :cond_0
/* .line 479 */
} // :cond_1
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 481 */
} // :cond_2
} // :goto_1
v0 = com.android.server.am.PreloadAppControllerImpl.sPreloadSchedAffinity;
} // .end method
public static Long queryStopTimeoutFromUid ( Integer p0 ) {
/* .locals 5 */
/* .param p0, "uid" # I */
/* .line 511 */
v0 = com.android.server.am.PreloadAppControllerImpl.sInstance;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 512 */
v0 = this.mPreloadingAppLock;
/* monitor-enter v0 */
/* .line 513 */
try { // :try_start_0
v1 = com.android.server.am.PreloadAppControllerImpl.sInstance;
v1 = this.mAllPreloadAppInfos;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/android/server/wm/PreloadLifecycle; */
/* .line 514 */
/* .local v2, "lifecycle":Lcom/android/server/wm/PreloadLifecycle; */
v3 = (( com.android.server.wm.PreloadLifecycle ) v2 ).getUid ( ); // invoke-virtual {v2}, Lcom/android/server/wm/PreloadLifecycle;->getUid()I
/* if-ne v3, p0, :cond_0 */
/* .line 515 */
(( com.android.server.wm.PreloadLifecycle ) v2 ).getStopTimeout ( ); // invoke-virtual {v2}, Lcom/android/server/wm/PreloadLifecycle;->getStopTimeout()J
/* move-result-wide v3 */
/* monitor-exit v0 */
/* return-wide v3 */
/* .line 517 */
} // .end local v2 # "lifecycle":Lcom/android/server/wm/PreloadLifecycle;
} // :cond_0
/* .line 518 */
} // :cond_1
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 520 */
} // :cond_2
} // :goto_1
/* const-wide/16 v0, 0x4e20 */
/* return-wide v0 */
} // .end method
private Integer realStartPreloadApp ( java.lang.String p0, com.android.server.wm.PreloadLifecycle p1, android.content.pm.ApplicationInfo p2 ) {
/* .locals 23 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "lifecycle" # Lcom/android/server/wm/PreloadLifecycle; */
/* .param p3, "info" # Landroid/content/pm/ApplicationInfo; */
/* .line 388 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v11, p1 */
v1 = this.mActivityManagerService;
v1 = this.mContext;
(( android.content.Context ) v1 ).getPackageManager ( ); // invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
(( android.content.pm.PackageManager ) v1 ).getLaunchIntentForPackage ( v11 ); // invoke-virtual {v1, v11}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 390 */
/* .local v10, "intent":Landroid/content/Intent; */
if ( v10 != null) { // if-eqz v10, :cond_3
/* .line 391 */
/* sget-boolean v1, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
final String v12 = "PreloadAppControllerImpl"; // const-string v12, "PreloadAppControllerImpl"
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 392 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "preloadAppstart app: "; // const-string v2, "preloadAppstart app: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v11 ); // invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v12,v1 );
/* .line 394 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
(( android.content.Intent ) v10 ).setPackage ( v1 ); // invoke-virtual {v10, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 395 */
android.app.ActivityOptions .makeBasic ( );
/* .line 396 */
/* .local v9, "options":Landroid/app/ActivityOptions; */
v8 = /* invoke-static/range {p1 ..p1}, Lcom/android/server/wm/PreloadStateManagerImpl;->getOrCreatePreloadDisplayId(Ljava/lang/String;)I */
/* .line 397 */
/* .local v8, "displayId":I */
/* move-object/from16 v7, p2 */
(( com.android.server.wm.PreloadLifecycle ) v7 ).setDisplayId ( v8 ); // invoke-virtual {v7, v8}, Lcom/android/server/wm/PreloadLifecycle;->setDisplayId(I)V
/* .line 398 */
(( android.app.ActivityOptions ) v9 ).setLaunchDisplayId ( v8 ); // invoke-virtual {v9, v8}, Landroid/app/ActivityOptions;->setLaunchDisplayId(I)Landroid/app/ActivityOptions;
/* .line 400 */
v1 = this.mHandler;
/* new-instance v2, Lcom/android/server/am/PreloadAppControllerImpl$1; */
/* invoke-direct {v2, v0}, Lcom/android/server/am/PreloadAppControllerImpl$1;-><init>(Lcom/android/server/am/PreloadAppControllerImpl;)V */
/* const-wide/16 v3, 0xbb8 */
(( com.android.server.am.PreloadAppControllerImpl$PreloadHandler ) v1 ).postDelayed ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 407 */
final String v1 = "preloadApp PreloadLifecycle.PRELOAD_SOON"; // const-string v1, "preloadApp PreloadLifecycle.PRELOAD_SOON"
android.util.Slog .e ( v12,v1 );
/* .line 408 */
int v1 = 2; // const/4 v1, 0x2
/* .line 409 */
com.miui.server.sptm.SpeedTestModeServiceImpl .getInstance ( );
v1 = (( com.miui.server.sptm.SpeedTestModeServiceImpl ) v1 ).getSPTMCloudEnableNew ( ); // invoke-virtual {v1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->getSPTMCloudEnableNew()Z
/* if-nez v1, :cond_1 */
/* .line 410 */
v12 = this.mActivityManagerService;
int v13 = 0; // const/4 v13, 0x0
int v14 = 0; // const/4 v14, 0x0
/* const/16 v16, 0x0 */
/* const/16 v17, 0x0 */
/* const/16 v18, 0x0 */
/* const/16 v19, 0x0 */
/* const/16 v20, 0x0 */
/* const/16 v21, 0x0 */
/* .line 411 */
(( android.app.ActivityOptions ) v9 ).toBundle ( ); // invoke-virtual {v9}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;
/* .line 410 */
/* move-object v15, v10 */
v1 = /* invoke-virtual/range {v12 ..v22}, Lcom/android/server/am/ActivityManagerService;->startActivity(Landroid/app/IApplicationThread;Ljava/lang/String;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILandroid/app/ProfilerInfo;Landroid/os/Bundle;)I */
/* .line 413 */
} // :cond_1
v1 = this.mActivityManagerService;
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
/* new-instance v6, Lcom/android/server/am/HostingRecord; */
/* .line 415 */
v2 = /* invoke-virtual/range {p2 ..p2}, Lcom/android/server/wm/PreloadLifecycle;->getPreloadType()I */
java.lang.String .valueOf ( v2 );
/* invoke-direct {v6, v2, v11}, Lcom/android/server/am/HostingRecord;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
int v13 = 0; // const/4 v13, 0x0
int v14 = 0; // const/4 v14, 0x0
int v15 = 0; // const/4 v15, 0x0
/* .line 417 */
com.android.server.am.ActivityManagerServiceStub .get ( );
/* .line 418 */
v3 = android.os.Binder .getCallingPid ( );
(( com.android.server.am.ActivityManagerServiceStub ) v2 ).getPackageNameByPid ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/am/ActivityManagerServiceStub;->getPackageNameByPid(I)Ljava/lang/String;
/* .line 413 */
/* move-object/from16 v2, p1 */
/* move-object/from16 v3, p3 */
/* move v7, v13 */
/* move v13, v8 */
} // .end local v8 # "displayId":I
/* .local v13, "displayId":I */
/* move v8, v14 */
/* move-object v14, v9 */
} // .end local v9 # "options":Landroid/app/ActivityOptions;
/* .local v14, "options":Landroid/app/ActivityOptions; */
/* move v9, v15 */
/* move-object v15, v10 */
} // .end local v10 # "intent":Landroid/content/Intent;
/* .local v15, "intent":Landroid/content/Intent; */
/* move-object/from16 v10, v16 */
/* invoke-virtual/range {v1 ..v10}, Lcom/android/server/am/ActivityManagerService;->startProcessLocked(Ljava/lang/String;Landroid/content/pm/ApplicationInfo;ZILcom/android/server/am/HostingRecord;IZZLjava/lang/String;)Lcom/android/server/am/ProcessRecord; */
/* if-nez v1, :cond_2 */
/* .line 419 */
final String v1 = "error in startProcessLocked!"; // const-string v1, "error in startProcessLocked!"
android.util.Slog .w ( v12,v1 );
/* .line 420 */
/* const/16 v1, -0x1e4 */
/* .line 422 */
} // :cond_2
final String v1 = "AMS startProcessLocked success"; // const-string v1, "AMS startProcessLocked success"
android.util.Slog .w ( v12,v1 );
/* .line 423 */
int v1 = 0; // const/4 v1, 0x0
/* .line 428 */
} // .end local v13 # "displayId":I
} // .end local v14 # "options":Landroid/app/ActivityOptions;
} // .end local v15 # "intent":Landroid/content/Intent;
/* .restart local v10 # "intent":Landroid/content/Intent; */
} // :cond_3
/* const/16 v1, -0x1ed */
} // .end method
private com.android.server.wm.PreloadLifecycle removePreloadAppInfo ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "uid" # I */
/* .line 349 */
/* sget-boolean v0, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 350 */
final String v0 = "PreloadAppControllerImpl"; // const-string v0, "PreloadAppControllerImpl"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "preloadApp removePreloadAppInfo uid:"; // const-string v2, "preloadApp removePreloadAppInfo uid:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v1 );
/* .line 352 */
} // :cond_0
v0 = this.mPreloadingAppLock;
/* monitor-enter v0 */
/* .line 353 */
try { // :try_start_0
v1 = this.mAllPreloadAppInfos;
/* .line 354 */
/* .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/wm/PreloadLifecycle;>;" */
int v2 = 0; // const/4 v2, 0x0
/* .line 355 */
/* .local v2, "removeItem":Lcom/android/server/wm/PreloadLifecycle; */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 356 */
/* check-cast v3, Lcom/android/server/wm/PreloadLifecycle; */
/* .line 357 */
/* .local v3, "p":Lcom/android/server/wm/PreloadLifecycle; */
v4 = (( com.android.server.wm.PreloadLifecycle ) v3 ).getUid ( ); // invoke-virtual {v3}, Lcom/android/server/wm/PreloadLifecycle;->getUid()I
/* if-ne v4, p1, :cond_1 */
/* .line 358 */
/* .line 359 */
/* move-object v2, v3 */
/* .line 361 */
} // .end local v3 # "p":Lcom/android/server/wm/PreloadLifecycle;
} // :cond_1
/* .line 362 */
} // :cond_2
/* monitor-exit v0 */
/* .line 363 */
} // .end local v1 # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/wm/PreloadLifecycle;>;"
} // .end local v2 # "removeItem":Lcom/android/server/wm/PreloadLifecycle;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private com.android.server.wm.PreloadLifecycle removePreloadAppInfo ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 367 */
/* if-nez p1, :cond_0 */
/* .line 368 */
int v0 = 0; // const/4 v0, 0x0
/* .line 370 */
} // :cond_0
/* sget-boolean v0, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 371 */
final String v0 = "PreloadAppControllerImpl"; // const-string v0, "PreloadAppControllerImpl"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "preloadApp removePreloadAppInfo packageName:"; // const-string v2, "preloadApp removePreloadAppInfo packageName:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v1 );
/* .line 373 */
} // :cond_1
v0 = this.mPreloadingAppLock;
/* monitor-enter v0 */
/* .line 374 */
try { // :try_start_0
v1 = this.mAllPreloadAppInfos;
/* .line 375 */
/* .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/wm/PreloadLifecycle;>;" */
int v2 = 0; // const/4 v2, 0x0
/* .line 376 */
/* .local v2, "removeItem":Lcom/android/server/wm/PreloadLifecycle; */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 377 */
/* check-cast v3, Lcom/android/server/wm/PreloadLifecycle; */
/* .line 378 */
/* .local v3, "p":Lcom/android/server/wm/PreloadLifecycle; */
(( com.android.server.wm.PreloadLifecycle ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Lcom/android/server/wm/PreloadLifecycle;->getPackageName()Ljava/lang/String;
v4 = (( java.lang.String ) p1 ).equals ( v4 ); // invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 379 */
/* .line 380 */
/* move-object v2, v3 */
/* .line 382 */
} // .end local v3 # "p":Lcom/android/server/wm/PreloadLifecycle;
} // :cond_2
/* .line 383 */
} // :cond_3
/* monitor-exit v0 */
/* .line 384 */
} // .end local v1 # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/wm/PreloadLifecycle;>;"
} // .end local v2 # "removeItem":Lcom/android/server/wm/PreloadLifecycle;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private Integer startPreloadApp ( com.android.server.wm.PreloadLifecycle p0 ) {
/* .locals 13 */
/* .param p1, "lifecycle" # Lcom/android/server/wm/PreloadLifecycle; */
/* .line 143 */
(( com.android.server.wm.PreloadLifecycle ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Lcom/android/server/wm/PreloadLifecycle;->getPackageName()Ljava/lang/String;
/* .line 144 */
/* .local v0, "packageName":Ljava/lang/String; */
v1 = (( com.android.server.wm.PreloadLifecycle ) p1 ).isIgnoreMemory ( ); // invoke-virtual {p1}, Lcom/android/server/wm/PreloadLifecycle;->isIgnoreMemory()Z
/* .line 145 */
/* .local v1, "ignoreMemory":Z */
(( com.android.server.wm.PreloadLifecycle ) p1 ).getConfig ( ); // invoke-virtual {p1}, Lcom/android/server/wm/PreloadLifecycle;->getConfig()Lmiui/process/LifecycleConfig;
/* .line 146 */
/* .local v2, "config":Lmiui/process/LifecycleConfig; */
/* sget-boolean v3, Lcom/android/server/am/PreloadAppControllerImpl;->ENABLE:Z */
final String v4 = "PreloadAppControllerImpl"; // const-string v4, "PreloadAppControllerImpl"
/* if-nez v3, :cond_1 */
/* .line 147 */
/* sget-boolean v3, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 148 */
final String v3 = "preloadApp is disable!!!"; // const-string v3, "preloadApp is disable!!!"
android.util.Slog .w ( v4,v3 );
/* .line 150 */
} // :cond_0
/* const/16 v3, -0x1e6 */
/* .line 153 */
} // :cond_1
/* iget-boolean v3, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mSpeedTestState:Z */
int v5 = 1; // const/4 v5, 0x1
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 154 */
v3 = (( com.android.server.wm.PreloadLifecycle ) p1 ).getPreloadType ( ); // invoke-virtual {p1}, Lcom/android/server/wm/PreloadLifecycle;->getPreloadType()I
/* if-eq v3, v5, :cond_3 */
/* .line 155 */
/* sget-boolean v3, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 156 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "preloadApp fail! mSpeedTestState="; // const-string v5, "preloadApp fail! mSpeedTestState="
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v5, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mSpeedTestState:Z */
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v4,v3 );
/* .line 158 */
} // :cond_2
/* const/16 v3, -0x1e4 */
/* .line 161 */
} // :cond_3
v3 = this.mProcessManagerService;
v3 = (( com.android.server.am.ProcessManagerService ) v3 ).checkPermission ( ); // invoke-virtual {v3}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z
/* if-nez v3, :cond_5 */
/* .line 162 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " Permission Denial: from pid="; // const-string v5, " Permission Denial: from pid="
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 163 */
v5 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = ", uid="; // const-string v5, ", uid="
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 164 */
/* .local v3, "msg":Ljava/lang/String; */
/* sget-boolean v5, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 165 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "preloadApp"; // const-string v6, "preloadApp"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v4,v5 );
/* .line 167 */
} // :cond_4
/* const/16 v4, -0x1e8 */
/* .line 169 */
} // .end local v3 # "msg":Ljava/lang/String;
} // :cond_5
/* if-nez v0, :cond_6 */
/* .line 170 */
/* const/16 v3, -0x1ef */
/* .line 172 */
} // :cond_6
v3 = com.android.server.am.PreloadAppControllerImpl .inBlackList ( v0 );
if ( v3 != null) { // if-eqz v3, :cond_7
/* .line 173 */
/* const/16 v3, -0x1eb */
/* .line 175 */
} // :cond_7
/* if-nez v2, :cond_8 */
/* .line 176 */
/* const/16 v3, -0x1e5 */
/* .line 178 */
} // :cond_8
v3 = this.mProcessManagerService;
/* .line 179 */
v3 = (( com.android.server.am.ProcessManagerService ) v3 ).frequentlyKilledForPreload ( v0 ); // invoke-virtual {v3, v0}, Lcom/android/server/am/ProcessManagerService;->frequentlyKilledForPreload(Ljava/lang/String;)Z
final String v6 = "preloadApp skip "; // const-string v6, "preloadApp skip "
if ( v3 != null) { // if-eqz v3, :cond_a
v3 = (( miui.process.LifecycleConfig ) v2 ).forceStart ( ); // invoke-virtual {v2}, Lmiui/process/LifecycleConfig;->forceStart()Z
/* if-nez v3, :cond_a */
/* .line 180 */
/* sget-boolean v3, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_9
/* .line 181 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v6 ); // invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ", because of errors or killed by user before"; // const-string v5, ", because of errors or killed by user before"
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v4,v3 );
/* .line 184 */
} // :cond_9
/* const/16 v3, -0x1e7 */
/* .line 186 */
} // :cond_a
/* if-nez v1, :cond_c */
v3 = com.android.server.am.ProcessUtils .isLowMemory ( );
if ( v3 != null) { // if-eqz v3, :cond_c
/* .line 187 */
/* sget-boolean v3, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_b
/* .line 188 */
final String v3 = "low memory! skip preloadApp!"; // const-string v3, "low memory! skip preloadApp!"
android.util.Slog .w ( v4,v3 );
/* .line 190 */
} // :cond_b
/* const/16 v3, -0x1ee */
/* .line 193 */
} // :cond_c
v3 = this.mActivityManagerService;
v3 = this.mAtmInternal;
(( com.android.server.wm.ActivityTaskManagerInternal ) v3 ).getTopApp ( ); // invoke-virtual {v3}, Lcom/android/server/wm/ActivityTaskManagerInternal;->getTopApp()Lcom/android/server/wm/WindowProcessController;
/* .line 194 */
/* .local v3, "wpc":Lcom/android/server/wm/WindowProcessController; */
if ( v3 != null) { // if-eqz v3, :cond_d
v7 = this.mOwner;
/* check-cast v7, Lcom/android/server/am/ProcessRecord; */
} // :cond_d
int v7 = 0; // const/4 v7, 0x0
/* .line 196 */
/* .local v7, "r":Lcom/android/server/am/ProcessRecord; */
} // :goto_0
if ( v7 != null) { // if-eqz v7, :cond_f
v8 = this.processName;
v8 = com.android.server.am.PreloadAppControllerImpl .inTopAppBlackList ( v8 );
if ( v8 != null) { // if-eqz v8, :cond_f
/* .line 197 */
/* sget-boolean v5, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
if ( v5 != null) { // if-eqz v5, :cond_e
/* .line 198 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = "because topApp is "; // const-string v6, "because topApp is "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.processName;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v4,v5 );
/* .line 200 */
} // :cond_e
/* const/16 v4, -0x1ec */
/* .line 203 */
} // :cond_f
int v6 = 0; // const/4 v6, 0x0
/* .line 205 */
/* .local v6, "info":Landroid/content/pm/ApplicationInfo; */
/* const/16 v8, -0x1f3 */
try { // :try_start_0
android.app.AppGlobals .getPackageManager ( );
/* const-wide/16 v10, 0x400 */
int v12 = 0; // const/4 v12, 0x0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v6, v9 */
/* .line 212 */
/* nop */
/* .line 214 */
/* if-nez v6, :cond_10 */
/* .line 215 */
/* .line 217 */
} // :cond_10
/* iget v8, v6, Landroid/content/pm/ApplicationInfo;->flags:I */
/* and-int/2addr v5, v8 */
/* if-lez v5, :cond_11 */
/* .line 218 */
/* const/16 v4, -0x1f2 */
/* .line 221 */
} // :cond_11
v5 = this.mActivityManagerService;
v5 = this.mProcessList;
/* .line 222 */
(( com.android.server.am.ProcessList ) v5 ).getProcessNamesLOSP ( ); // invoke-virtual {v5}, Lcom/android/server/am/ProcessList;->getProcessNamesLOSP()Lcom/android/server/am/ProcessList$MyProcessMap;
/* iget v8, v6, Landroid/content/pm/ApplicationInfo;->uid:I */
(( com.android.server.am.ProcessList$MyProcessMap ) v5 ).get ( v0, v8 ); // invoke-virtual {v5, v0, v8}, Lcom/android/server/am/ProcessList$MyProcessMap;->get(Ljava/lang/String;I)Ljava/lang/Object;
/* check-cast v5, Lcom/android/server/am/ProcessRecord; */
/* .line 224 */
/* .local v5, "app":Lcom/android/server/am/ProcessRecord; */
if ( v5 != null) { // if-eqz v5, :cond_16
/* .line 225 */
v8 = (( com.android.server.am.ProcessRecord ) v5 ).isPersistent ( ); // invoke-virtual {v5}, Lcom/android/server/am/ProcessRecord;->isPersistent()Z
final String v9 = "preloadApp: "; // const-string v9, "preloadApp: "
if ( v8 != null) { // if-eqz v8, :cond_13
/* .line 226 */
/* sget-boolean v8, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
if ( v8 != null) { // if-eqz v8, :cond_12
/* .line 227 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v0 ); // invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = " is persistent, skip!"; // const-string v9, " is persistent, skip!"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v4,v8 );
/* .line 229 */
} // :cond_12
/* const/16 v4, -0x1f0 */
/* .line 231 */
} // :cond_13
com.miui.server.sptm.SpeedTestModeServiceImpl .getInstance ( );
v8 = (( com.miui.server.sptm.SpeedTestModeServiceImpl ) v8 ).isSpeedTestMode ( ); // invoke-virtual {v8}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->isSpeedTestMode()Z
/* if-nez v8, :cond_15 */
/* .line 232 */
/* sget-boolean v8, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
if ( v8 != null) { // if-eqz v8, :cond_14
/* .line 233 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v0 ); // invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = " already exits, skip it"; // const-string v9, " already exits, skip it"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v4,v8 );
/* .line 236 */
} // :cond_14
/* const/16 v4, -0x1f1 */
/* .line 238 */
} // :cond_15
v4 = this.mActivityManagerService;
v8 = this.info;
v8 = this.packageName;
/* iget v9, v5, Lcom/android/server/am/ProcessRecord;->userId:I */
final String v10 = "preload app exits"; // const-string v10, "preload app exits"
(( com.android.server.am.ActivityManagerService ) v4 ).forceStopPackage ( v8, v9, v12, v10 ); // invoke-virtual {v4, v8, v9, v12, v10}, Lcom/android/server/am/ActivityManagerService;->forceStopPackage(Ljava/lang/String;IILjava/lang/String;)V
/* .line 243 */
} // :cond_16
/* iget v4, v6, Landroid/content/pm/ApplicationInfo;->uid:I */
(( com.android.server.wm.PreloadLifecycle ) p1 ).setUid ( v4 ); // invoke-virtual {p1, v4}, Lcom/android/server/wm/PreloadLifecycle;->setUid(I)V
/* .line 244 */
v4 = /* invoke-direct {p0, v0, p1, v6}, Lcom/android/server/am/PreloadAppControllerImpl;->realStartPreloadApp(Ljava/lang/String;Lcom/android/server/wm/PreloadLifecycle;Landroid/content/pm/ApplicationInfo;)I */
/* .line 207 */
} // .end local v5 # "app":Lcom/android/server/am/ProcessRecord;
/* :catch_0 */
/* move-exception v5 */
/* .line 208 */
/* .local v5, "e":Landroid/os/RemoteException; */
/* sget-boolean v9, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
if ( v9 != null) { // if-eqz v9, :cond_17
/* .line 209 */
final String v9 = "preloadApp error in getApplicationInfo!"; // const-string v9, "preloadApp error in getApplicationInfo!"
android.util.Slog .w ( v4,v9,v5 );
/* .line 211 */
} // :cond_17
} // .end method
private void unRegistPreloadCallback ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "type" # I */
/* .line 619 */
v0 = this.mHandler;
int v1 = 4; // const/4 v1, 0x4
android.os.Message .obtain ( v0,v1 );
/* .line 620 */
/* .local v0, "msg":Landroid/os/Message; */
/* iput p1, v0, Landroid/os/Message;->arg1:I */
/* .line 621 */
v1 = this.mHandler;
(( com.android.server.am.PreloadAppControllerImpl$PreloadHandler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 622 */
return;
} // .end method
/* # virtual methods */
public java.util.Set getPreloadingApps ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 277 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 278 */
/* .local v0, "packages":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
v1 = this.mPreloadingAppLock;
/* monitor-enter v1 */
/* .line 279 */
try { // :try_start_0
v2 = this.mAllPreloadAppInfos;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Lcom/android/server/wm/PreloadLifecycle; */
/* .line 280 */
/* .local v3, "lifecycle":Lcom/android/server/wm/PreloadLifecycle; */
v4 = (( com.android.server.wm.PreloadLifecycle ) v3 ).isAlreadyPreloaded ( ); // invoke-virtual {v3}, Lcom/android/server/wm/PreloadLifecycle;->isAlreadyPreloaded()Z
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 281 */
(( com.android.server.wm.PreloadLifecycle ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Lcom/android/server/wm/PreloadLifecycle;->getPackageName()Ljava/lang/String;
/* .line 283 */
} // .end local v3 # "lifecycle":Lcom/android/server/wm/PreloadLifecycle;
} // :cond_0
/* .line 284 */
} // :cond_1
/* monitor-exit v1 */
/* .line 285 */
/* .line 284 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public void init ( com.android.server.am.ActivityManagerService p0, com.android.server.am.ProcessManagerService p1, com.android.server.ServiceThread p2 ) {
/* .locals 2 */
/* .param p1, "ams" # Lcom/android/server/am/ActivityManagerService; */
/* .param p2, "processManagerService" # Lcom/android/server/am/ProcessManagerService; */
/* .param p3, "thread" # Lcom/android/server/ServiceThread; */
/* .line 102 */
v0 = this.mHandler;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 103 */
return;
/* .line 106 */
} // :cond_0
com.android.server.am.PreloadAppControllerImpl .getInstance ( );
/* .line 107 */
this.mActivityManagerService = p1;
/* .line 108 */
this.mProcessManagerService = p2;
/* .line 109 */
/* new-instance v0, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler; */
(( com.android.server.ServiceThread ) p3 ).getLooper ( ); // invoke-virtual {p3}, Lcom/android/server/ServiceThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;-><init>(Lcom/android/server/am/PreloadAppControllerImpl;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 111 */
/* invoke-direct {p0}, Lcom/android/server/am/PreloadAppControllerImpl;->initPreloadEnableState()V */
/* .line 112 */
return;
} // .end method
public Boolean interceptWindowInjector ( android.view.WindowManager$LayoutParams p0 ) {
/* .locals 3 */
/* .param p1, "attrs" # Landroid/view/WindowManager$LayoutParams; */
/* .line 433 */
v0 = this.packageName;
v0 = com.android.server.am.PreloadAppControllerImpl .inWindowBlackList ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 434 */
/* sget-boolean v0, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z */
final String v1 = "PreloadAppControllerImpl"; // const-string v1, "PreloadAppControllerImpl"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 435 */
final String v0 = "preloadApp inWindowBlackList"; // const-string v0, "preloadApp inWindowBlackList"
android.util.Slog .w ( v1,v0 );
/* .line 438 */
} // :cond_0
int v2 = 2; // const/4 v2, 0x2
/* if-ne v0, v2, :cond_1 */
/* .line 439 */
final String v0 = "preloadApp window should add to PreloadDisplay"; // const-string v0, "preloadApp window should add to PreloadDisplay"
android.util.Slog .w ( v1,v0 );
/* .line 440 */
int v0 = 1; // const/4 v0, 0x1
/* .line 443 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer killPreloadApp ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 639 */
v0 = this.mProcessManagerService;
v0 = (( com.android.server.am.ProcessManagerService ) v0 ).checkPermission ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z
/* if-nez v0, :cond_0 */
/* .line 640 */
/* const/16 v0, -0x1e8 */
/* .line 643 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* .line 644 */
/* const/16 v0, -0x1ef */
/* .line 647 */
} // :cond_1
v0 = this.mPreloadingAppLock;
/* monitor-enter v0 */
/* .line 648 */
try { // :try_start_0
v1 = com.android.server.am.PreloadAppControllerImpl .getUidFromPackageName ( p1 );
/* .line 649 */
/* .local v1, "uid":I */
/* if-gez v1, :cond_2 */
/* .line 650 */
/* monitor-exit v0 */
/* const/16 v0, -0x1e3 */
/* .line 652 */
} // :cond_2
final String v2 = "PreloadAppControllerImpl"; // const-string v2, "PreloadAppControllerImpl"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
v4 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " advance kill preloadApp "; // const-string v4, " advance kill preloadApp "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* .line 653 */
com.android.server.wm.PreloadStateManagerImpl .killPreloadApp ( p1,v1 );
/* .line 654 */
} // .end local v1 # "uid":I
/* monitor-exit v0 */
/* .line 656 */
/* const/16 v0, 0x1f4 */
/* .line 654 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void onPreloadAppKilled ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .line 335 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/PreloadAppControllerImpl;->removePreloadAppInfo(I)Lcom/android/server/wm/PreloadLifecycle; */
/* .line 336 */
return;
} // .end method
public void onPreloadAppStarted ( Integer p0, java.lang.String p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "pid" # I */
/* .line 326 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/PreloadAppControllerImpl;->removePreloadAppInfo(I)Lcom/android/server/wm/PreloadLifecycle; */
/* .line 327 */
v0 = this.mHandler;
int v1 = 5; // const/4 v1, 0x5
android.os.Message .obtain ( v0,v1 );
/* .line 328 */
/* .local v0, "msg":Landroid/os/Message; */
this.obj = p2;
/* .line 329 */
/* iput p3, v0, Landroid/os/Message;->arg1:I */
/* .line 330 */
v1 = /* invoke-direct {p0, p2}, Lcom/android/server/am/PreloadAppControllerImpl;->getPreloadType(Ljava/lang/String;)I */
/* iput v1, v0, Landroid/os/Message;->arg2:I */
/* .line 331 */
v1 = this.mHandler;
(( com.android.server.am.PreloadAppControllerImpl$PreloadHandler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 332 */
return;
} // .end method
public void onProcessKilled ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .param p2, "processName" # Ljava/lang/String; */
/* .line 339 */
/* const-string/jumbo v0, "timeout_kill_preloadApp" */
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 340 */
v0 = com.android.server.am.PreloadAppControllerImpl .getUidFromPackageName ( p2 );
com.android.server.wm.PreloadStateManagerImpl .enableAudio ( v0 );
/* .line 342 */
} // :cond_0
return;
} // .end method
protected void preloadAppEnqueue ( java.lang.String p0, Boolean p1, miui.process.LifecycleConfig p2 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "ignoreMemory" # Z */
/* .param p3, "config" # Lmiui/process/LifecycleConfig; */
/* .line 134 */
/* invoke-direct {p0, p2, p1, p3}, Lcom/android/server/am/PreloadAppControllerImpl;->createPreloadLifeCycle(ZLjava/lang/String;Lmiui/process/LifecycleConfig;)Lcom/android/server/wm/PreloadLifecycle; */
/* .line 135 */
/* .local v0, "lifecycle":Lcom/android/server/wm/PreloadLifecycle; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 136 */
v1 = this.mHandler;
int v2 = 1; // const/4 v2, 0x1
android.os.Message .obtain ( v1,v2 );
/* .line 137 */
/* .local v1, "msg":Landroid/os/Message; */
this.obj = v0;
/* .line 138 */
v2 = this.mHandler;
(( com.android.server.am.PreloadAppControllerImpl$PreloadHandler ) v2 ).sendMessage ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 140 */
} // .end local v1 # "msg":Landroid/os/Message;
} // :cond_0
return;
} // .end method
public void registerPreloadCallback ( miui.process.IPreloadCallback p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "callback" # Lmiui/process/IPreloadCallback; */
/* .param p2, "type" # I */
/* .line 626 */
final String v0 = "PreloadAppControllerImpl"; // const-string v0, "PreloadAppControllerImpl"
try { // :try_start_0
/* new-instance v2, Lcom/android/server/am/PreloadAppControllerImpl$CallbackDeathRecipient; */
/* invoke-direct {v2, p0, p2}, Lcom/android/server/am/PreloadAppControllerImpl$CallbackDeathRecipient;-><init>(Lcom/android/server/am/PreloadAppControllerImpl;I)V */
int v3 = 0; // const/4 v3, 0x0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 630 */
/* nop */
/* .line 631 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "registPreloadCallback , type="; // const-string v2, "registPreloadCallback , type="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = "pid="; // const-string v2, "pid="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v1 );
/* .line 632 */
v0 = this.mHandler;
int v1 = 3; // const/4 v1, 0x3
android.os.Message .obtain ( v0,v1 );
/* .line 633 */
/* .local v0, "msg":Landroid/os/Message; */
this.obj = p1;
/* .line 634 */
/* iput p2, v0, Landroid/os/Message;->arg1:I */
/* .line 635 */
v1 = this.mHandler;
(( com.android.server.am.PreloadAppControllerImpl$PreloadHandler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 636 */
return;
/* .line 627 */
} // .end local v0 # "msg":Landroid/os/Message;
/* :catch_0 */
/* move-exception v1 */
/* .line 628 */
/* .local v1, "e":Landroid/os/RemoteException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "registPreloadCallback fail due to linkToDeath, type "; // const-string v3, "registPreloadCallback fail due to linkToDeath, type "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v2 );
/* .line 629 */
return;
} // .end method
public void setSpeedTestState ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "speedTestState" # Z */
/* .line 345 */
/* iput-boolean p1, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mSpeedTestState:Z */
/* .line 346 */
return;
} // .end method
