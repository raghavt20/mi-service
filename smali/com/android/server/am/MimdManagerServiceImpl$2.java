class com.android.server.am.MimdManagerServiceImpl$2 extends android.app.IProcessObserver$Stub {
	 /* .source "MimdManagerServiceImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/MimdManagerServiceImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.am.MimdManagerServiceImpl this$0; //synthetic
/* # direct methods */
 com.android.server.am.MimdManagerServiceImpl$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/MimdManagerServiceImpl; */
/* .line 318 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/app/IProcessObserver$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onForegroundActivitiesChanged ( Integer p0, Integer p1, Boolean p2 ) {
/* .locals 3 */
/* .param p1, "pid" # I */
/* .param p2, "uid" # I */
/* .param p3, "foreground" # Z */
/* .line 321 */
/* new-instance v0, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo; */
v1 = this.this$0;
/* invoke-direct {v0, v1}, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;-><init>(Lcom/android/server/am/MimdManagerServiceImpl;)V */
/* .line 322 */
/* .local v0, "info":Lcom/android/server/am/MimdManagerServiceImpl$AppInfo; */
/* iput p1, v0, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;->pid:I */
/* .line 323 */
/* iput p2, v0, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;->uid:I */
/* .line 324 */
/* iput-boolean p3, v0, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;->foreground:Z */
/* .line 325 */
v1 = this.this$0;
com.android.server.am.MimdManagerServiceImpl .-$$Nest$fgetmMimdManagerHandler ( v1 );
v2 = this.this$0;
com.android.server.am.MimdManagerServiceImpl .-$$Nest$fgetmMimdManagerHandler ( v2 );
java.util.Objects .requireNonNull ( v2 );
int v2 = 2; // const/4 v2, 0x2
(( com.android.server.am.MimdManagerServiceImpl$MimdManagerHandler ) v1 ).obtainMessage ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 326 */
/* .local v1, "msg":Landroid/os/Message; */
v2 = this.this$0;
com.android.server.am.MimdManagerServiceImpl .-$$Nest$fgetmMimdManagerHandler ( v2 );
(( com.android.server.am.MimdManagerServiceImpl$MimdManagerHandler ) v2 ).sendMessage ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 327 */
return;
} // .end method
public void onForegroundServicesChanged ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 0 */
/* .param p1, "pid" # I */
/* .param p2, "uid" # I */
/* .param p3, "serviceTypes" # I */
/* .line 332 */
return;
} // .end method
public void onProcessDied ( Integer p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "pid" # I */
/* .param p2, "uid" # I */
/* .line 336 */
/* new-instance v0, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo; */
v1 = this.this$0;
/* invoke-direct {v0, v1}, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;-><init>(Lcom/android/server/am/MimdManagerServiceImpl;)V */
/* .line 337 */
/* .local v0, "info":Lcom/android/server/am/MimdManagerServiceImpl$AppInfo; */
/* iput p1, v0, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;->pid:I */
/* .line 338 */
/* iput p2, v0, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;->uid:I */
/* .line 339 */
v1 = this.this$0;
com.android.server.am.MimdManagerServiceImpl .-$$Nest$fgetmMimdManagerHandler ( v1 );
v2 = this.this$0;
com.android.server.am.MimdManagerServiceImpl .-$$Nest$fgetmMimdManagerHandler ( v2 );
java.util.Objects .requireNonNull ( v2 );
int v2 = 3; // const/4 v2, 0x3
(( com.android.server.am.MimdManagerServiceImpl$MimdManagerHandler ) v1 ).obtainMessage ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 340 */
/* .local v1, "msg":Landroid/os/Message; */
v2 = this.this$0;
com.android.server.am.MimdManagerServiceImpl .-$$Nest$fgetmMimdManagerHandler ( v2 );
(( com.android.server.am.MimdManagerServiceImpl$MimdManagerHandler ) v2 ).sendMessage ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 341 */
return;
} // .end method
