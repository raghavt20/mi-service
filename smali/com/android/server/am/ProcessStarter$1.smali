.class Lcom/android/server/am/ProcessStarter$1;
.super Ljava/lang/Object;
.source "ProcessStarter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/ProcessStarter;->recordDiedProcessIfNeeded(Ljava/lang/String;Ljava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ProcessStarter;

.field final synthetic val$pkgName:Ljava/lang/String;

.field final synthetic val$processName:Ljava/lang/String;

.field final synthetic val$uid:I


# direct methods
.method constructor <init>(Lcom/android/server/am/ProcessStarter;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/ProcessStarter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 290
    iput-object p1, p0, Lcom/android/server/am/ProcessStarter$1;->this$0:Lcom/android/server/am/ProcessStarter;

    iput-object p2, p0, Lcom/android/server/am/ProcessStarter$1;->val$pkgName:Ljava/lang/String;

    iput p3, p0, Lcom/android/server/am/ProcessStarter$1;->val$uid:I

    iput-object p4, p0, Lcom/android/server/am/ProcessStarter$1;->val$processName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 293
    iget-object v0, p0, Lcom/android/server/am/ProcessStarter$1;->this$0:Lcom/android/server/am/ProcessStarter;

    invoke-static {v0}, Lcom/android/server/am/ProcessStarter;->-$$Nest$fgetmPms(Lcom/android/server/am/ProcessStarter;)Lcom/android/server/am/ProcessManagerService;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/ProcessStarter$1;->val$pkgName:Ljava/lang/String;

    iget v2, p0, Lcom/android/server/am/ProcessStarter$1;->val$uid:I

    invoke-virtual {v0, v1, v2}, Lcom/android/server/am/ProcessManagerService;->isAllowAutoStart(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 294
    const-string v0, "ProcessStarter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "process "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/am/ProcessStarter$1;->val$processName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " No restart permission!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    return-void

    .line 297
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ProcessStarter$1;->this$0:Lcom/android/server/am/ProcessStarter;

    invoke-static {v0}, Lcom/android/server/am/ProcessStarter;->-$$Nest$fgetmProcessDiedRecordMap(Lcom/android/server/am/ProcessStarter;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/ProcessStarter$1;->val$processName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 298
    .local v0, "value":Ljava/lang/Integer;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x5

    if-lt v1, v2, :cond_1

    .line 299
    const-string v1, "ProcessStarter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "process "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/ProcessStarter$1;->val$processName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " frequent died!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    return-void

    .line 302
    :cond_1
    iget-object v1, p0, Lcom/android/server/am/ProcessStarter$1;->this$0:Lcom/android/server/am/ProcessStarter;

    invoke-static {v1}, Lcom/android/server/am/ProcessStarter;->-$$Nest$fgetmAms(Lcom/android/server/am/ProcessStarter;)Lcom/android/server/am/ActivityManagerService;

    move-result-object v1

    monitor-enter v1

    .line 303
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/ProcessStarter$1;->this$0:Lcom/android/server/am/ProcessStarter;

    iget-object v3, p0, Lcom/android/server/am/ProcessStarter$1;->val$pkgName:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/server/am/ProcessStarter$1;->val$processName:Ljava/lang/String;

    iget v5, p0, Lcom/android/server/am/ProcessStarter$1;->val$uid:I

    .line 304
    invoke-static {v5}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v5

    const/4 v6, 0x2

    invoke-static {v6}, Lcom/android/server/am/ProcessStarter;->makeHostingTypeFromFlag(I)Ljava/lang/String;

    move-result-object v6

    .line 303
    invoke-static {v2, v3, v4, v5, v6}, Lcom/android/server/am/ProcessStarter;->-$$Nest$mstartProcessLocked(Lcom/android/server/am/ProcessStarter;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/android/server/am/ProcessRecord;

    .line 306
    monitor-exit v1

    .line 307
    return-void

    .line 306
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
