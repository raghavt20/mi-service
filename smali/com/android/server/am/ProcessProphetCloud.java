public class com.android.server.am.ProcessProphetCloud {
	 /* .source "ProcessProphetCloud.java" */
	 /* # static fields */
	 private static final java.lang.String CLOUD_PROCPROPHET_EMPTYMEM_THRESH;
	 private static final java.lang.String CLOUD_PROCPROPHET_ENABLE;
	 private static final java.lang.String CLOUD_PROCPROPHET_INTERVAL_TIME;
	 private static final java.lang.String CLOUD_PROCPROPHET_LAUNCH_BLACKLIST;
	 private static final java.lang.String CLOUD_PROCPROPHET_MEMPRES_THRESH;
	 private static final java.lang.String CLOUD_PROCPROPHET_STARTFREQ_THRESH;
	 private static final java.lang.String CLOUD_PROCPROPHET_START_LIMITLIST;
	 private static Boolean DEBUG;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private android.content.Context mContext;
	 private com.android.server.am.ProcessProphetImpl mProcessProphetImpl;
	 private com.android.server.am.ProcessProphetModel mProcessProphetModel;
	 /* # direct methods */
	 static com.android.server.am.ProcessProphetCloud ( ) {
		 /* .locals 2 */
		 /* .line 40 */
		 /* nop */
		 /* .line 41 */
		 final String v0 = "persist.sys.procprophet.debug"; // const-string v0, "persist.sys.procprophet.debug"
		 int v1 = 0; // const/4 v1, 0x0
		 v0 = 		 android.os.SystemProperties .getBoolean ( v0,v1 );
		 com.android.server.am.ProcessProphetCloud.DEBUG = (v0!= 0);
		 /* .line 40 */
		 return;
	 } // .end method
	 public com.android.server.am.ProcessProphetCloud ( ) {
		 /* .locals 1 */
		 /* .line 17 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 36 */
		 int v0 = 0; // const/4 v0, 0x0
		 this.mProcessProphetImpl = v0;
		 /* .line 37 */
		 this.mProcessProphetModel = v0;
		 /* .line 38 */
		 this.mContext = v0;
		 return;
	 } // .end method
	 /* # virtual methods */
	 public java.lang.Long getCloudAttribute ( java.lang.String p0 ) {
		 /* .locals 10 */
		 /* .param p1, "operations" # Ljava/lang/String; */
		 /* .line 286 */
		 final String v0 = "close io failed: "; // const-string v0, "close io failed: "
		 final String v1 = "ProcessProphetCloud"; // const-string v1, "ProcessProphetCloud"
		 int v2 = 0; // const/4 v2, 0x0
		 /* .line 287 */
		 /* .local v2, "sReader":Ljava/io/StringReader; */
		 int v3 = 0; // const/4 v3, 0x0
		 /* .line 288 */
		 /* .local v3, "jReader":Lcom/google/gson/stream/JsonReader; */
		 android.os.Process .getTotalMemory ( );
		 /* move-result-wide v4 */
		 /* const-wide/32 v6, 0x40000000 */
		 /* div-long/2addr v4, v6 */
		 /* const-wide/16 v6, 0x1 */
		 /* add-long/2addr v4, v6 */
		 /* long-to-int v4, v4 */
		 /* .line 290 */
		 /* .local v4, "totalNameGB":I */
		 int v5 = 4; // const/4 v5, 0x4
		 /* if-gt v4, v5, :cond_0 */
		 /* .line 291 */
		 int v4 = 4; // const/4 v4, 0x4
		 /* .line 293 */
	 } // :cond_0
	 /* const/16 v5, 0xc */
	 /* if-lt v4, v5, :cond_1 */
	 /* .line 294 */
	 /* const/16 v4, 0xc */
	 /* .line 298 */
} // :cond_1
try { // :try_start_0
	 /* new-instance v5, Ljava/io/StringReader; */
	 /* invoke-direct {v5, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V */
	 /* move-object v2, v5 */
	 /* .line 299 */
	 /* new-instance v5, Lcom/google/gson/stream/JsonReader; */
	 /* invoke-direct {v5, v2}, Lcom/google/gson/stream/JsonReader;-><init>(Ljava/io/Reader;)V */
	 /* move-object v3, v5 */
	 /* .line 300 */
	 (( com.google.gson.stream.JsonReader ) v3 ).beginObject ( ); // invoke-virtual {v3}, Lcom/google/gson/stream/JsonReader;->beginObject()V
	 /* .line 301 */
} // :goto_0
v5 = (( com.google.gson.stream.JsonReader ) v3 ).hasNext ( ); // invoke-virtual {v3}, Lcom/google/gson/stream/JsonReader;->hasNext()Z
if ( v5 != null) { // if-eqz v5, :cond_3
	 /* .line 302 */
	 (( com.google.gson.stream.JsonReader ) v3 ).nextName ( ); // invoke-virtual {v3}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;
	 v5 = 	 java.lang.Integer .parseInt ( v5 );
	 /* .line 303 */
	 /* .local v5, "nameNumb":I */
	 (( com.google.gson.stream.JsonReader ) v3 ).nextString ( ); // invoke-virtual {v3}, Lcom/google/gson/stream/JsonReader;->nextString()Ljava/lang/String;
	 /* .line 304 */
	 /* .local v6, "valueStr":Ljava/lang/String; */
	 /* if-ne v4, v5, :cond_2 */
	 /* .line 305 */
	 java.lang.Long .parseLong ( v6 );
	 /* move-result-wide v7 */
	 java.lang.Long .valueOf ( v7,v8 );
	 /* :try_end_0 */
	 /* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_2 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* .line 312 */
	 /* nop */
	 /* .line 314 */
	 try { // :try_start_1
		 (( com.google.gson.stream.JsonReader ) v3 ).close ( ); // invoke-virtual {v3}, Lcom/google/gson/stream/JsonReader;->close()V
		 /* :try_end_1 */
		 /* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
		 /* .line 317 */
		 /* .line 315 */
		 /* :catch_0 */
		 /* move-exception v8 */
		 /* .line 316 */
		 /* .local v8, "e":Ljava/io/IOException; */
		 /* new-instance v9, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
		 (( java.lang.StringBuilder ) v9 ).append ( v0 ); // invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.io.IOException ) v8 ).getMessage ( ); // invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;
		 (( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Slog .e ( v1,v0 );
		 /* .line 319 */
	 } // .end local v8 # "e":Ljava/io/IOException;
} // :goto_1
/* nop */
/* .line 320 */
(( java.io.StringReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/StringReader;->close()V
/* .line 305 */
/* .line 307 */
} // .end local v5 # "nameNumb":I
} // .end local v6 # "valueStr":Ljava/lang/String;
} // :cond_2
/* .line 308 */
} // :cond_3
try { // :try_start_2
(( com.google.gson.stream.JsonReader ) v3 ).endObject ( ); // invoke-virtual {v3}, Lcom/google/gson/stream/JsonReader;->endObject()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 312 */
/* nop */
/* .line 314 */
try { // :try_start_3
(( com.google.gson.stream.JsonReader ) v3 ).close ( ); // invoke-virtual {v3}, Lcom/google/gson/stream/JsonReader;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_1 */
/* .line 317 */
/* .line 315 */
/* :catch_1 */
/* move-exception v5 */
/* .line 316 */
/* .local v5, "e":Ljava/io/IOException; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.IOException ) v5 ).getMessage ( ); // invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v0 );
/* .line 319 */
} // .end local v5 # "e":Ljava/io/IOException;
} // :goto_2
/* nop */
/* .line 320 */
} // :goto_3
(( java.io.StringReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/StringReader;->close()V
/* .line 312 */
/* :catchall_0 */
/* move-exception v5 */
/* .line 309 */
/* :catch_2 */
/* move-exception v5 */
/* .line 310 */
/* .restart local v5 # "e":Ljava/io/IOException; */
try { // :try_start_4
final String v6 = "json reader error"; // const-string v6, "json reader error"
android.util.Slog .e ( v1,v6 );
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 312 */
/* nop */
} // .end local v5 # "e":Ljava/io/IOException;
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 314 */
try { // :try_start_5
(( com.google.gson.stream.JsonReader ) v3 ).close ( ); // invoke-virtual {v3}, Lcom/google/gson/stream/JsonReader;->close()V
/* :try_end_5 */
/* .catch Ljava/io/IOException; {:try_start_5 ..:try_end_5} :catch_3 */
/* .line 317 */
/* .line 315 */
/* :catch_3 */
/* move-exception v5 */
/* .line 316 */
/* .restart local v5 # "e":Ljava/io/IOException; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.IOException ) v5 ).getMessage ( ); // invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v0 );
/* .line 319 */
} // .end local v5 # "e":Ljava/io/IOException;
} // :cond_4
} // :goto_4
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 320 */
/* .line 323 */
} // :cond_5
} // :goto_5
/* const-wide/16 v0, -0x1 */
java.lang.Long .valueOf ( v0,v1 );
/* .line 312 */
} // :goto_6
if ( v3 != null) { // if-eqz v3, :cond_6
/* .line 314 */
try { // :try_start_6
(( com.google.gson.stream.JsonReader ) v3 ).close ( ); // invoke-virtual {v3}, Lcom/google/gson/stream/JsonReader;->close()V
/* :try_end_6 */
/* .catch Ljava/io/IOException; {:try_start_6 ..:try_end_6} :catch_4 */
/* .line 317 */
/* .line 315 */
/* :catch_4 */
/* move-exception v6 */
/* .line 316 */
/* .local v6, "e":Ljava/io/IOException; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.IOException ) v6 ).getMessage ( ); // invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v0 );
/* .line 319 */
} // .end local v6 # "e":Ljava/io/IOException;
} // :cond_6
} // :goto_7
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 320 */
(( java.io.StringReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/StringReader;->close()V
/* .line 322 */
} // :cond_7
/* throw v5 */
} // .end method
public void initCloud ( com.android.server.am.ProcessProphetImpl p0, com.android.server.am.ProcessProphetModel p1, android.content.Context p2 ) {
/* .locals 2 */
/* .param p1, "ppi" # Lcom/android/server/am/ProcessProphetImpl; */
/* .param p2, "ppm" # Lcom/android/server/am/ProcessProphetModel; */
/* .param p3, "context" # Landroid/content/Context; */
/* .line 44 */
this.mProcessProphetImpl = p1;
/* .line 45 */
this.mProcessProphetModel = p2;
/* .line 46 */
this.mContext = p3;
/* .line 47 */
final String v0 = "ProcessProphetCloud"; // const-string v0, "ProcessProphetCloud"
if ( p1 != null) { // if-eqz p1, :cond_1
/* if-nez p2, :cond_0 */
/* .line 51 */
} // :cond_0
final String v1 = "initCloud success"; // const-string v1, "initCloud success"
android.util.Slog .i ( v0,v1 );
/* .line 52 */
return;
/* .line 48 */
} // :cond_1
} // :goto_0
final String v1 = "initCloud failure."; // const-string v1, "initCloud failure."
android.util.Slog .e ( v0,v1 );
/* .line 49 */
return;
} // .end method
public void registerProcProphetCloudObserver ( ) {
/* .locals 5 */
/* .line 56 */
/* new-instance v0, Lcom/android/server/am/ProcessProphetCloud$1; */
v1 = this.mProcessProphetImpl;
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/am/ProcessProphetCloud$1;-><init>(Lcom/android/server/am/ProcessProphetCloud;Landroid/os/Handler;)V */
/* .line 105 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 106 */
final String v2 = "cloud_procprophet_enable"; // const-string v2, "cloud_procprophet_enable"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 105 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 109 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 110 */
final String v2 = "cloud_procprophet_interval_time"; // const-string v2, "cloud_procprophet_interval_time"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 109 */
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 113 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 114 */
final String v2 = "cloud_procprophet_startfreq_thresh"; // const-string v2, "cloud_procprophet_startfreq_thresh"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 113 */
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 117 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 118 */
final String v2 = "cloud_procprophet_mempres_thresh"; // const-string v2, "cloud_procprophet_mempres_thresh"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 117 */
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 121 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 122 */
final String v2 = "cloud_procprophet_emptymem_thresh"; // const-string v2, "cloud_procprophet_emptymem_thresh"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 121 */
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 125 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 126 */
final String v2 = "cloud_procprophet_start_limitlist"; // const-string v2, "cloud_procprophet_start_limitlist"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 125 */
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 129 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 130 */
final String v2 = "cloud_procprophet_launch_blacklist"; // const-string v2, "cloud_procprophet_launch_blacklist"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 129 */
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 132 */
return;
} // .end method
public void updateEmptyMemThresCloudControlParas ( ) {
/* .locals 7 */
/* .line 226 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "cloud_procprophet_emptymem_thresh"; // const-string v1, "cloud_procprophet_emptymem_thresh"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 228 */
v0 = this.mContext;
/* .line 229 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 228 */
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 232 */
/* .local v0, "emptyMemThresStr":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v1, :cond_1 */
/* .line 233 */
(( com.android.server.am.ProcessProphetCloud ) p0 ).getCloudAttribute ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/am/ProcessProphetCloud;->getCloudAttribute(Ljava/lang/String;)Ljava/lang/Long;
/* .line 234 */
/* .local v1, "tmpEmptylong":Ljava/lang/Long; */
(( java.lang.Long ) v1 ).longValue ( ); // invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
/* move-result-wide v2 */
/* const-wide/16 v4, -0x1 */
/* cmp-long v2, v2, v4 */
final String v3 = "ProcessProphetCloud"; // const-string v3, "ProcessProphetCloud"
/* if-nez v2, :cond_0 */
/* .line 235 */
final String v2 = "getCloudAttribute operations is error , return -1"; // const-string v2, "getCloudAttribute operations is error , return -1"
android.util.Slog .e ( v3,v2 );
/* .line 236 */
return;
/* .line 238 */
} // :cond_0
v2 = this.mProcessProphetImpl;
(( java.lang.Long ) v1 ).longValue ( ); // invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
/* move-result-wide v4 */
final String v6 = "emptyMem"; // const-string v6, "emptyMem"
(( com.android.server.am.ProcessProphetImpl ) v2 ).updateImplThreshold ( v4, v5, v6 ); // invoke-virtual {v2, v4, v5, v6}, Lcom/android/server/am/ProcessProphetImpl;->updateImplThreshold(JLjava/lang/String;)V
/* .line 239 */
/* sget-boolean v2, Lcom/android/server/am/ProcessProphetCloud;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 240 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "receive info from cloud: Empty Process Memory Threshold - "; // const-string v4, "receive info from cloud: Empty Process Memory Threshold - "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v2 );
/* .line 245 */
} // .end local v0 # "emptyMemThresStr":Ljava/lang/String;
} // .end local v1 # "tmpEmptylong":Ljava/lang/Long;
} // :cond_1
return;
} // .end method
public void updateEnableCloudControlParas ( ) {
/* .locals 4 */
/* .line 147 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "cloud_procprophet_enable"; // const-string v1, "cloud_procprophet_enable"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 149 */
v0 = this.mContext;
/* .line 150 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 149 */
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 153 */
/* .local v0, "enableStr":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v1, :cond_0 */
/* .line 154 */
/* new-instance v1, Ljava/lang/Boolean; */
/* invoke-direct {v1, v0}, Ljava/lang/Boolean;-><init>(Ljava/lang/String;)V */
v1 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
java.lang.Boolean .valueOf ( v1 );
/* .line 155 */
/* .local v1, "enableB":Ljava/lang/Boolean; */
v2 = this.mProcessProphetImpl;
v3 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
(( com.android.server.am.ProcessProphetImpl ) v2 ).updateEnable ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/am/ProcessProphetImpl;->updateEnable(Z)V
/* .line 156 */
/* sget-boolean v2, Lcom/android/server/am/ProcessProphetCloud;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 157 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "receive info from cloud: ProcessProphetEnable - "; // const-string v3, "receive info from cloud: ProcessProphetEnable - "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "ProcessProphetCloud"; // const-string v3, "ProcessProphetCloud"
android.util.Slog .i ( v3,v2 );
/* .line 161 */
} // .end local v0 # "enableStr":Ljava/lang/String;
} // .end local v1 # "enableB":Ljava/lang/Boolean;
} // :cond_0
return;
} // .end method
public void updateLaunchProcCloudControlParas ( ) {
/* .locals 4 */
/* .line 267 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "cloud_procprophet_launch_blacklist"; // const-string v1, "cloud_procprophet_launch_blacklist"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 269 */
v0 = this.mContext;
/* .line 270 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 269 */
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 273 */
/* .local v0, "luchBLStr":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v1, :cond_0 */
/* .line 274 */
final String v1 = ","; // const-string v1, ","
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 275 */
/* .local v1, "arrStr":[Ljava/lang/String; */
v2 = this.mProcessProphetImpl;
final String v3 = "blakclist"; // const-string v3, "blakclist"
(( com.android.server.am.ProcessProphetImpl ) v2 ).updateList ( v1, v3 ); // invoke-virtual {v2, v1, v3}, Lcom/android/server/am/ProcessProphetImpl;->updateList([Ljava/lang/String;Ljava/lang/String;)V
/* .line 276 */
/* sget-boolean v2, Lcom/android/server/am/ProcessProphetCloud;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 277 */
final String v2 = "ProcessProphetCloud"; // const-string v2, "ProcessProphetCloud"
final String v3 = "receive info from cloud: LaunchProcBlackList"; // const-string v3, "receive info from cloud: LaunchProcBlackList"
android.util.Slog .i ( v2,v3 );
/* .line 281 */
} // .end local v0 # "luchBLStr":Ljava/lang/String;
} // .end local v1 # "arrStr":[Ljava/lang/String;
} // :cond_0
return;
} // .end method
public void updateMemThresCloudControlParas ( ) {
/* .locals 7 */
/* .line 203 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "cloud_procprophet_mempres_thresh"; // const-string v1, "cloud_procprophet_mempres_thresh"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 205 */
v0 = this.mContext;
/* .line 206 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 205 */
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 209 */
/* .local v0, "memThresStr":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v1, :cond_1 */
/* .line 210 */
(( com.android.server.am.ProcessProphetCloud ) p0 ).getCloudAttribute ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/am/ProcessProphetCloud;->getCloudAttribute(Ljava/lang/String;)Ljava/lang/Long;
/* .line 211 */
/* .local v1, "tmpMemlong":Ljava/lang/Long; */
(( java.lang.Long ) v1 ).longValue ( ); // invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
/* move-result-wide v2 */
/* const-wide/16 v4, -0x1 */
/* cmp-long v2, v2, v4 */
final String v3 = "ProcessProphetCloud"; // const-string v3, "ProcessProphetCloud"
/* if-nez v2, :cond_0 */
/* .line 212 */
final String v2 = "getCloudAttribute operations is error , return -1"; // const-string v2, "getCloudAttribute operations is error , return -1"
android.util.Slog .e ( v3,v2 );
/* .line 213 */
return;
/* .line 215 */
} // :cond_0
v2 = this.mProcessProphetImpl;
(( java.lang.Long ) v1 ).longValue ( ); // invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
/* move-result-wide v4 */
final String v6 = "memPress"; // const-string v6, "memPress"
(( com.android.server.am.ProcessProphetImpl ) v2 ).updateImplThreshold ( v4, v5, v6 ); // invoke-virtual {v2, v4, v5, v6}, Lcom/android/server/am/ProcessProphetImpl;->updateImplThreshold(JLjava/lang/String;)V
/* .line 216 */
/* sget-boolean v2, Lcom/android/server/am/ProcessProphetCloud;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 217 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "receive info from cloud: Memory Pressure Threshold - "; // const-string v4, "receive info from cloud: Memory Pressure Threshold - "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v2 );
/* .line 222 */
} // .end local v0 # "memThresStr":Ljava/lang/String;
} // .end local v1 # "tmpMemlong":Ljava/lang/Long;
} // :cond_1
return;
} // .end method
public void updateProcProphetCloudControlParas ( ) {
/* .locals 0 */
/* .line 136 */
(( com.android.server.am.ProcessProphetCloud ) p0 ).updateEnableCloudControlParas ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetCloud;->updateEnableCloudControlParas()V
/* .line 137 */
(( com.android.server.am.ProcessProphetCloud ) p0 ).updateTrackTimeCloudControlParas ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetCloud;->updateTrackTimeCloudControlParas()V
/* .line 138 */
(( com.android.server.am.ProcessProphetCloud ) p0 ).updateStartProcFreqThresCloudControlParas ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetCloud;->updateStartProcFreqThresCloudControlParas()V
/* .line 139 */
(( com.android.server.am.ProcessProphetCloud ) p0 ).updateMemThresCloudControlParas ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetCloud;->updateMemThresCloudControlParas()V
/* .line 140 */
(( com.android.server.am.ProcessProphetCloud ) p0 ).updateEmptyMemThresCloudControlParas ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetCloud;->updateEmptyMemThresCloudControlParas()V
/* .line 141 */
(( com.android.server.am.ProcessProphetCloud ) p0 ).updateStartProcLimitCloudControlParas ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetCloud;->updateStartProcLimitCloudControlParas()V
/* .line 142 */
(( com.android.server.am.ProcessProphetCloud ) p0 ).updateLaunchProcCloudControlParas ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetCloud;->updateLaunchProcCloudControlParas()V
/* .line 143 */
return;
} // .end method
public void updateStartProcFreqThresCloudControlParas ( ) {
/* .locals 5 */
/* .line 184 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "cloud_procprophet_startfreq_thresh"; // const-string v1, "cloud_procprophet_startfreq_thresh"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 186 */
v0 = this.mContext;
/* .line 187 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 186 */
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 190 */
/* .local v0, "probThresStr":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v1, :cond_0 */
/* .line 191 */
final String v1 = ","; // const-string v1, ","
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 192 */
/* .local v1, "probThress":[Ljava/lang/String; */
v2 = this.mProcessProphetModel;
(( com.android.server.am.ProcessProphetModel ) v2 ).updateModelThreshold ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/am/ProcessProphetModel;->updateModelThreshold([Ljava/lang/String;)V
/* .line 193 */
/* sget-boolean v2, Lcom/android/server/am/ProcessProphetCloud;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 194 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "receive info from cloud: Process Frequent Threshold - "; // const-string v3, "receive info from cloud: Process Frequent Threshold - "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
int v3 = 0; // const/4 v3, 0x0
/* aget-object v3, v1, v3 */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " "; // const-string v3, " "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
int v4 = 1; // const/4 v4, 0x1
/* aget-object v4, v1, v4 */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
int v3 = 2; // const/4 v3, 0x2
/* aget-object v3, v1, v3 */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "ProcessProphetCloud"; // const-string v3, "ProcessProphetCloud"
android.util.Slog .i ( v3,v2 );
/* .line 199 */
} // .end local v0 # "probThresStr":Ljava/lang/String;
} // .end local v1 # "probThress":[Ljava/lang/String;
} // :cond_0
return;
} // .end method
public void updateStartProcLimitCloudControlParas ( ) {
/* .locals 4 */
/* .line 249 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "cloud_procprophet_start_limitlist"; // const-string v1, "cloud_procprophet_start_limitlist"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 251 */
v0 = this.mContext;
/* .line 252 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 251 */
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 255 */
/* .local v0, "procLimitStr":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v1, :cond_0 */
/* .line 256 */
final String v1 = ","; // const-string v1, ","
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 257 */
/* .local v1, "arrStr":[Ljava/lang/String; */
v2 = this.mProcessProphetImpl;
/* const-string/jumbo v3, "whitelist" */
(( com.android.server.am.ProcessProphetImpl ) v2 ).updateList ( v1, v3 ); // invoke-virtual {v2, v1, v3}, Lcom/android/server/am/ProcessProphetImpl;->updateList([Ljava/lang/String;Ljava/lang/String;)V
/* .line 258 */
/* sget-boolean v2, Lcom/android/server/am/ProcessProphetCloud;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 259 */
final String v2 = "ProcessProphetCloud"; // const-string v2, "ProcessProphetCloud"
final String v3 = "receive info from cloud: StartProcLimitList"; // const-string v3, "receive info from cloud: StartProcLimitList"
android.util.Slog .i ( v2,v3 );
/* .line 263 */
} // .end local v0 # "procLimitStr":Ljava/lang/String;
} // .end local v1 # "arrStr":[Ljava/lang/String;
} // :cond_0
return;
} // .end method
public void updateTrackTimeCloudControlParas ( ) {
/* .locals 4 */
/* .line 165 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "cloud_procprophet_interval_time"; // const-string v1, "cloud_procprophet_interval_time"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 167 */
v0 = this.mContext;
/* .line 168 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 167 */
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 171 */
/* .local v0, "intervalStr":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v1, :cond_0 */
/* .line 172 */
v1 = java.lang.Integer .parseInt ( v0 );
/* .line 173 */
/* .local v1, "valTime":I */
v2 = this.mProcessProphetImpl;
(( com.android.server.am.ProcessProphetImpl ) v2 ).updateTrackInterval ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/am/ProcessProphetImpl;->updateTrackInterval(I)V
/* .line 174 */
/* sget-boolean v2, Lcom/android/server/am/ProcessProphetCloud;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 175 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "receive info from cloud: ProcessProphet-Onetrack interval - "; // const-string v3, "receive info from cloud: ProcessProphet-Onetrack interval - "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "ProcessProphetCloud"; // const-string v3, "ProcessProphetCloud"
android.util.Slog .i ( v3,v2 );
/* .line 180 */
} // .end local v0 # "intervalStr":Ljava/lang/String;
} // .end local v1 # "valTime":I
} // :cond_0
return;
} // .end method
