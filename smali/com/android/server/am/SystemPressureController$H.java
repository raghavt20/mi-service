class com.android.server.am.SystemPressureController$H extends android.os.Handler {
	 /* .source "SystemPressureController.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/SystemPressureController; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "H" */
} // .end annotation
/* # instance fields */
final com.android.server.am.SystemPressureController this$0; //synthetic
/* # direct methods */
public com.android.server.am.SystemPressureController$H ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 129 */
this.this$0 = p1;
/* .line 130 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 131 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 3 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 135 */
/* invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V */
/* .line 136 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 148 */
/* :pswitch_0 */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Ljava/lang/Boolean; */
v1 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
(( com.android.server.am.SystemPressureController ) v0 ).updateScreenState ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/SystemPressureController;->updateScreenState(Z)V
/* .line 149 */
/* .line 144 */
/* :pswitch_1 */
v0 = this.this$0;
com.android.server.am.SystemPressureController .-$$Nest$fgetmContext ( v0 );
com.android.server.am.SystemPressureController .-$$Nest$mregisterCloudObserver ( v0,v1 );
/* .line 145 */
v0 = this.this$0;
com.android.server.am.SystemPressureController .-$$Nest$mupdateCloudControlData ( v0 );
/* .line 146 */
/* .line 141 */
/* :pswitch_2 */
v0 = this.this$0;
com.android.server.am.SystemPressureController .-$$Nest$mresetStartingAppState ( v0 );
/* .line 142 */
/* .line 138 */
/* :pswitch_3 */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Ljava/lang/Long; */
(( java.lang.Long ) v1 ).longValue ( ); // invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
/* move-result-wide v1 */
com.android.server.am.SystemPressureController .-$$Nest$mcleanUpMemory ( v0,v1,v2 );
/* .line 139 */
/* nop */
/* .line 153 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
