.class public final Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;
.super Ljava/lang/Object;
.source "ProcessPolicy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ProcessPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ActiveUidRecord"
.end annotation


# static fields
.field static final ACTIVE_AUDIO:I = 0x1

.field static final ACTIVE_GPS:I = 0x2

.field static final NO_ACTIVE:I


# instance fields
.field public flag:I

.field public uid:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "_uid"    # I

    .line 348
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 349
    iput p1, p0, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->uid:I

    .line 350
    return-void
.end method

.method private makeActiveString(Ljava/lang/StringBuilder;)V
    .locals 2
    .param p1, "sb"    # Ljava/lang/StringBuilder;

    .line 353
    const-string v0, "flag :"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 354
    iget v0, p0, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->flag:I

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 355
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 356
    const/4 v0, 0x0

    .line 357
    .local v0, "printed":Z
    iget v1, p0, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->flag:I

    if-nez v1, :cond_0

    .line 358
    const-string v1, "NONE"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 359
    return-void

    .line 362
    :cond_0
    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 363
    const/4 v0, 0x1

    .line 364
    const-string v1, "A"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 367
    :cond_1
    iget v1, p0, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->flag:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_3

    .line 368
    if-eqz v0, :cond_2

    .line 369
    const-string/jumbo v1, "|"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 371
    :cond_2
    const-string v1, "G"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 373
    :cond_3
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .line 377
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 378
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "ActiveUidRecord{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 379
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 380
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 381
    iget v2, p0, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->uid:I

    invoke-static {v0, v2}, Landroid/os/UserHandle;->formatUid(Ljava/lang/StringBuilder;I)V

    .line 382
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 383
    invoke-direct {p0, v0}, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->makeActiveString(Ljava/lang/StringBuilder;)V

    .line 384
    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 385
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
