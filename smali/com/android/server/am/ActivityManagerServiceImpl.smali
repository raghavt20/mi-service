.class public Lcom/android/server/am/ActivityManagerServiceImpl;
.super Lcom/android/server/am/ActivityManagerServiceStub;
.source "ActivityManagerServiceImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.am.ActivityManagerServiceStub$$"
.end annotation


# static fields
.field public static final BOOST_DURATION:J = 0xbb8L

.field private static final BOOST_TAG:Ljava/lang/String; = "Boost"

.field private static final CARLINK:Ljava/lang/String; = "com.miui.carlink"

.field private static final CARLINK_CARLIFE:Ljava/lang/String; = "com.baidu.carlife.xiaomi"

.field private static JOB_ANRS:Ljava/util/List; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final KEEP_FOREGROUND_DURATION:J = 0x4e20L

.field public static final MIUI_APP_TAG:Ljava/lang/String; = "MIUIScout App"

.field private static final MIUI_NOTIFICATION:Ljava/lang/String; = "com.miui.notification"

.field private static final MIUI_VOICE:Ljava/lang/String; = "com.miui.voiceassist"

.field private static final MI_PUSH:Ljava/lang/String; = "com.xiaomi.mipush.sdk.PushMessageHandler"

.field private static final MI_VOICE:Ljava/lang/String; = "com.miui.voiceassist/com.xiaomi.voiceassistant.VoiceService"

.field private static final PHONE_CARWITH_CASTING:I = 0x1

.field private static final PROP_DISABLE_AUTORESTART_APP_PREFIX:Ljava/lang/String; = "sys.rescuepartyplus.disable_autorestart."

.field static final PUSH_SERVICE_WHITELIST_TIMEOUT:I = 0xea60

.field public static final SIGNAL_QUIT:I = 0x3

.field private static final TAG:Ljava/lang/String; = "ActivityManagerServiceImpl"

.field private static final UCAR_CASTING_STATE:Ljava/lang/String; = "ucar_casting_state"

.field private static final WEIXIN:Ljava/lang/String; = "com.tencent.mm"

.field public static final WIDGET_PROVIDER_WHITE_LIST:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final XIAOMI_BLUETOOTH:Ljava/lang/String; = "com.xiaomi.bluetooth"

.field private static final XMSF:Ljava/lang/String; = "com.xiaomi.xmsf"

.field private static dumpFlag:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private static dumpTraceRequestList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final mIgnoreAuthorityList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static requestDumpTraceCount:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static splitDecouplePkgList:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private greezer:Lcom/miui/server/greeze/GreezeManagerInternal;

.field mAmService:Lcom/android/server/am/ActivityManagerService;

.field private mBackupingList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field mContext:Landroid/content/Context;

.field private mCurrentSplitIntent:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private mForceVkInternal:Lcom/xiaomi/vkmode/service/MiuiForceVkServiceInternal;

.field private mInstrUid:I

.field private mLastSplitIntent:Landroid/content/Intent;

.field private mPackageManager:Landroid/content/pm/PackageManagerInternal;

.field private mPerfService:Lcom/android/internal/app/IPerfShielder;

.field private mSecurityInternal:Lmiui/security/SecurityManagerInternal;

.field protected mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

.field private mSplitActivityEntryStack:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/util/Stack<",
            "Landroid/os/IBinder;",
            ">;>;"
        }
    .end annotation
.end field

.field private mSplitExtras:Landroid/os/Bundle;

.field private mStabilityLocalServiceInternal:Lcom/miui/server/stability/StabilityLocalServiceInternal;

.field mSystemReady:Z

.field private mUsingVibratorUids:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final splitEntryStackLock:Ljava/lang/Object;


# direct methods
.method public static synthetic $r8$lambda$WCheiagbehbr4vA-OB9ebiRDuH4(Lcom/android/server/am/ActivityManagerServiceImpl;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/ActivityManagerServiceImpl;->lambda$dumpSystemTraces$0(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetrequestDumpTraceCount()Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    sget-object v0, Lcom/android/server/am/ActivityManagerServiceImpl;->requestDumpTraceCount:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 5

    .line 178
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/am/ActivityManagerServiceImpl;->dumpTraceRequestList:Ljava/util/ArrayList;

    .line 179
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/android/server/am/ActivityManagerServiceImpl;->requestDumpTraceCount:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 180
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/android/server/am/ActivityManagerServiceImpl;->dumpFlag:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 182
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/am/ActivityManagerServiceImpl;->WIDGET_PROVIDER_WHITE_LIST:Ljava/util/List;

    .line 207
    const-string v1, "com.android.calendar"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 209
    new-instance v0, Lcom/android/server/am/ActivityManagerServiceImpl$1;

    invoke-direct {v0}, Lcom/android/server/am/ActivityManagerServiceImpl$1;-><init>()V

    sput-object v0, Lcom/android/server/am/ActivityManagerServiceImpl;->splitDecouplePkgList:Ljava/util/Map;

    .line 695
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/server/am/ActivityManagerServiceImpl;->mIgnoreAuthorityList:Ljava/util/HashSet;

    .line 699
    const-string v1, "com.miui.securitycenter.zman.fileProvider"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 700
    const-string v1, "com.xiaomi.misettings.FileProvider"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 701
    const-string v1, "com.xiaomi.mirror.remoteprovider"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 702
    const-string v1, "com.xiaomi.aiasst.service.fileProvider"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 703
    const-string v1, "com.miui.bugreport.fileprovider"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 704
    const-string v1, "com.miui.cleanmaster.fileProvider"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1253
    new-instance v0, Ljava/util/ArrayList;

    const-string v1, "No response to onStopJob"

    const-string v2, "required notification not provided"

    const-string v3, "Timed out while trying to bind"

    const-string v4, "No response to onStartJob"

    filled-new-array {v3, v4, v1, v2}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/android/server/am/ActivityManagerServiceImpl;->JOB_ANRS:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 151
    invoke-direct {p0}, Lcom/android/server/am/ActivityManagerServiceStub;-><init>()V

    .line 189
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mCurrentSplitIntent:Ljava/util/Map;

    .line 193
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->splitEntryStackLock:Ljava/lang/Object;

    .line 1288
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->greezer:Lcom/miui/server/greeze/GreezeManagerInternal;

    .line 1792
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mBackupingList:Ljava/util/ArrayList;

    .line 1811
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mInstrUid:I

    .line 1829
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mUsingVibratorUids:Ljava/util/Set;

    return-void
.end method

.method private checkServiceWakePath(Landroid/content/Intent;Ljava/lang/String;Lmiui/security/CallerInfo;I)Z
    .locals 9
    .param p1, "service"    # Landroid/content/Intent;
    .param p2, "resolvedType"    # Ljava/lang/String;
    .param p3, "callerInfo"    # Lmiui/security/CallerInfo;
    .param p4, "userId"    # I

    .line 474
    :try_start_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v0

    const-wide/16 v3, 0x400

    move-object v1, p1

    move-object v2, p2

    move v5, p4

    invoke-interface/range {v0 .. v5}, Landroid/content/pm/IPackageManager;->resolveService(Landroid/content/Intent;Ljava/lang/String;JI)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 476
    .local v0, "rInfo":Landroid/content/pm/ResolveInfo;
    if-eqz v0, :cond_0

    iget-object v1, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 478
    .local v1, "sInfo":Landroid/content/pm/ServiceInfo;
    :goto_0
    const-class v2, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-static {v2}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-interface {v2, p1, p3, v1}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->shouldInterceptService(Landroid/content/Intent;Lmiui/security/CallerInfo;Landroid/content/pm/ServiceInfo;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    const/4 v4, 0x0

    const/16 v7, 0x8

    .line 479
    move-object v3, p3

    move-object v5, p1

    move-object v6, v1

    move v8, p4

    invoke-static/range {v2 .. v8}, Lcom/android/server/am/ActivityManagerServiceImpl;->checkWakePath(Lcom/android/server/am/ActivityManagerService;Lmiui/security/CallerInfo;Ljava/lang/String;Landroid/content/Intent;Landroid/content/pm/ComponentInfo;II)Z

    move-result v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v2, :cond_1

    goto :goto_1

    .line 485
    .end local v0    # "rInfo":Landroid/content/pm/ResolveInfo;
    .end local v1    # "sInfo":Landroid/content/pm/ServiceInfo;
    :cond_1
    goto :goto_2

    .line 481
    .restart local v0    # "rInfo":Landroid/content/pm/ResolveInfo;
    .restart local v1    # "sInfo":Landroid/content/pm/ServiceInfo;
    :cond_2
    :goto_1
    const/4 v2, 0x0

    return v2

    .line 483
    .end local v0    # "rInfo":Landroid/content/pm/ResolveInfo;
    .end local v1    # "sInfo":Landroid/content/pm/ServiceInfo;
    :catch_0
    move-exception v0

    .line 486
    :goto_2
    const/4 v0, 0x1

    return v0
.end method

.method private static checkThawTime(ILjava/lang/String;Lcom/miui/server/greeze/GreezeManagerInternal;)Z
    .locals 11
    .param p0, "uid"    # I
    .param p1, "report"    # Ljava/lang/String;
    .param p2, "greezer"    # Lcom/miui/server/greeze/GreezeManagerInternal;

    .line 1257
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "checkThawTime uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " report="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ActivityManagerServiceImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1258
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    .line 1259
    return v2

    .line 1261
    :cond_0
    const/16 v0, 0x2710

    .line 1262
    .local v0, "timeout":I
    const-string v3, "Broadcast of"

    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "executing service"

    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 1263
    const-string v3, "ContentProvider not"

    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    .line 1265
    :cond_1
    const-string v3, "Input dispatching"

    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1266
    const/16 v0, 0x1770

    goto :goto_1

    .line 1267
    :cond_2
    const-string v3, "App requested: isolate_instructions:"

    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1268
    const/16 v0, 0x7d0

    goto :goto_1

    .line 1269
    :cond_3
    sget-object v3, Lcom/android/server/am/ActivityManagerServiceImpl;->JOB_ANRS:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1270
    const/16 v0, 0x7d0

    goto :goto_1

    .line 1272
    :cond_4
    return v2

    .line 1264
    :cond_5
    :goto_0
    const/16 v0, 0x4e20

    .line 1274
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "checkThawTime thawTime="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1275
    const/4 v4, 0x1

    invoke-virtual {p2, p0, v4}, Lcom/miui/server/greeze/GreezeManagerInternal;->getLastThawedTime(II)J

    move-result-wide v5

    invoke-virtual {v3, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " now="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1276
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    invoke-virtual {v3, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1274
    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1277
    invoke-virtual {p2, p0, v4}, Lcom/miui/server/greeze/GreezeManagerInternal;->getLastThawedTime(II)J

    move-result-wide v5

    .line 1278
    .local v5, "thawTime":J
    const/16 v3, 0x2710

    if-lt p0, v3, :cond_6

    const/16 v3, 0x4e1f

    if-gt p0, v3, :cond_6

    const-wide/16 v7, 0x0

    cmp-long v3, v5, v7

    if-lez v3, :cond_6

    .line 1280
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v7

    sub-long/2addr v7, v5

    int-to-long v9, v0

    cmp-long v3, v7, v9

    if-gez v3, :cond_6

    .line 1281
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "matched "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " app time uid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1282
    return v4

    .line 1285
    :cond_6
    return v2
.end method

.method private static checkTime(JLjava/lang/String;)V
    .locals 6
    .param p0, "startTime"    # J
    .param p2, "where"    # Ljava/lang/String;

    .line 684
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 685
    .local v0, "now":J
    sub-long v2, v0, p0

    const-wide/16 v4, 0x3e8

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 687
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MIUILOG-checkTime:Slow operation: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sub-long v3, v0, p0

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms so far, now at "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ActivityManagerServiceImpl"

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 689
    :cond_0
    return-void
.end method

.method static checkWakePath(Lcom/android/server/am/ActivityManagerService;Lmiui/security/CallerInfo;Ljava/lang/String;Landroid/content/Intent;Landroid/content/pm/ComponentInfo;II)Z
    .locals 27
    .param p0, "ams"    # Lcom/android/server/am/ActivityManagerService;
    .param p1, "callerInfo"    # Lmiui/security/CallerInfo;
    .param p2, "callingPackage"    # Ljava/lang/String;
    .param p3, "intent"    # Landroid/content/Intent;
    .param p4, "info"    # Landroid/content/pm/ComponentInfo;
    .param p5, "wakeType"    # I
    .param p6, "userId"    # I

    .line 593
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move/from16 v14, p5

    move/from16 v15, p6

    const/4 v13, 0x1

    if-eqz v1, :cond_f

    if-eqz v3, :cond_f

    if-nez v4, :cond_0

    move/from16 v23, v13

    goto/16 :goto_6

    .line 597
    :cond_0
    invoke-static {}, Lmiui/security/WakePathChecker;->getInstance()Lmiui/security/WakePathChecker;

    move-result-object v12

    .line 598
    .local v12, "checker":Lmiui/security/WakePathChecker;
    invoke-virtual {v12, v3, v4, v14, v15}, Lmiui/security/WakePathChecker;->updatePath(Landroid/content/Intent;Landroid/content/pm/ComponentInfo;II)V

    .line 600
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 602
    .local v10, "startTime":J
    const/4 v0, -0x1

    .line 603
    .local v0, "callerUid":I
    const-string v16, ""

    .line 604
    .local v16, "callerPkg":Ljava/lang/String;
    const/16 v17, 0x0

    const-string v9, "ActivityManagerServiceImpl"

    if-eqz v2, :cond_5

    .line 605
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, v2, Lmiui/security/CallerInfo;->callerPkg:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":widgetProvider"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 606
    .local v8, "widgetProcessName":Ljava/lang/String;
    sget-object v5, Lcom/android/server/am/ActivityManagerServiceImpl;->WIDGET_PROVIDER_WHITE_LIST:Ljava/util/List;

    iget-object v6, v2, Lmiui/security/CallerInfo;->callerPkg:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    iget-object v5, v2, Lmiui/security/CallerInfo;->callerProcessName:Ljava/lang/String;

    .line 607
    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 609
    if-ne v14, v13, :cond_1

    .line 610
    iget-object v5, v2, Lmiui/security/CallerInfo;->callerPkg:Ljava/lang/String;

    invoke-static {v5}, Lcom/android/server/am/PendingIntentRecordImpl;->containsPendingIntent(Ljava/lang/String;)Z

    move-result v5

    xor-int/2addr v5, v13

    move/from16 v18, v5

    .local v5, "abort":Z
    goto :goto_0

    .line 612
    .end local v5    # "abort":Z
    :cond_1
    iget-object v5, v1, Lcom/android/server/am/ActivityManagerService;->mActivityTaskManager:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v6, v4, Landroid/content/pm/ComponentInfo;->processName:Ljava/lang/String;

    iget-object v7, v4, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v7, v7, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v5, v6, v7}, Lcom/android/server/wm/WindowProcessUtils;->isProcessRunning(Lcom/android/server/wm/ActivityTaskManagerService;Ljava/lang/String;I)Z

    move-result v5

    xor-int/2addr v5, v13

    move/from16 v18, v5

    .line 615
    .local v18, "abort":Z
    :goto_0
    if-eqz v18, :cond_2

    .line 617
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MIUILOG- Reject widget call from "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v2, Lmiui/security/CallerInfo;->callerPkg:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v9, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 618
    invoke-static {}, Lmiui/security/WakePathChecker;->getInstance()Lmiui/security/WakePathChecker;

    move-result-object v5

    iget-object v6, v2, Lmiui/security/CallerInfo;->callerPkg:Ljava/lang/String;

    iget-object v7, v4, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    iget v9, v2, Lmiui/security/CallerInfo;->callerUid:I

    .line 619
    invoke-static {v9}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v9

    iget-object v13, v4, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v13, v13, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 620
    invoke-static {v13}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v13

    const/16 v19, 0x0

    .line 618
    move-object/from16 v20, v8

    .end local v8    # "widgetProcessName":Ljava/lang/String;
    .local v20, "widgetProcessName":Ljava/lang/String;
    move/from16 v8, p5

    move-wide/from16 v21, v10

    .end local v10    # "startTime":J
    .local v21, "startTime":J
    move v10, v13

    move/from16 v11, v19

    invoke-virtual/range {v5 .. v11}, Lmiui/security/WakePathChecker;->recordWakePathCall(Ljava/lang/String;Ljava/lang/String;IIIZ)V

    .line 621
    return v17

    .line 615
    .end local v20    # "widgetProcessName":Ljava/lang/String;
    .end local v21    # "startTime":J
    .restart local v8    # "widgetProcessName":Ljava/lang/String;
    .restart local v10    # "startTime":J
    :cond_2
    move-object/from16 v20, v8

    move-wide/from16 v21, v10

    .end local v8    # "widgetProcessName":Ljava/lang/String;
    .end local v10    # "startTime":J
    .restart local v20    # "widgetProcessName":Ljava/lang/String;
    .restart local v21    # "startTime":J
    goto :goto_1

    .line 607
    .end local v18    # "abort":Z
    .end local v20    # "widgetProcessName":Ljava/lang/String;
    .end local v21    # "startTime":J
    .restart local v8    # "widgetProcessName":Ljava/lang/String;
    .restart local v10    # "startTime":J
    :cond_3
    move-object/from16 v20, v8

    move-wide/from16 v21, v10

    .end local v8    # "widgetProcessName":Ljava/lang/String;
    .end local v10    # "startTime":J
    .restart local v20    # "widgetProcessName":Ljava/lang/String;
    .restart local v21    # "startTime":J
    goto :goto_1

    .line 606
    .end local v20    # "widgetProcessName":Ljava/lang/String;
    .end local v21    # "startTime":J
    .restart local v8    # "widgetProcessName":Ljava/lang/String;
    .restart local v10    # "startTime":J
    :cond_4
    move-object/from16 v20, v8

    move-wide/from16 v21, v10

    .line 624
    .end local v8    # "widgetProcessName":Ljava/lang/String;
    .end local v10    # "startTime":J
    .restart local v20    # "widgetProcessName":Ljava/lang/String;
    .restart local v21    # "startTime":J
    :goto_1
    iget-object v5, v2, Lmiui/security/CallerInfo;->callerPkg:Ljava/lang/String;

    .line 625
    .end local v16    # "callerPkg":Ljava/lang/String;
    .local v5, "callerPkg":Ljava/lang/String;
    iget v0, v2, Lmiui/security/CallerInfo;->callerUid:I

    .line 626
    .end local v20    # "widgetProcessName":Ljava/lang/String;
    move-object v11, v5

    goto :goto_2

    .line 627
    .end local v5    # "callerPkg":Ljava/lang/String;
    .end local v21    # "startTime":J
    .restart local v10    # "startTime":J
    .restart local v16    # "callerPkg":Ljava/lang/String;
    :cond_5
    move-wide/from16 v21, v10

    .end local v10    # "startTime":J
    .restart local v21    # "startTime":J
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_8

    const/4 v5, 0x4

    if-ne v14, v5, :cond_8

    .line 629
    const-string v5, "android.intent.extra.UID"

    const/4 v6, -0x1

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 630
    .end local v0    # "callerUid":I
    .local v5, "callerUid":I
    if-eq v5, v6, :cond_7

    .line 632
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/am/ActivityManagerService;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v0

    invoke-interface {v0, v5}, Landroid/content/pm/IPackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v0

    .line 633
    .local v0, "pkgs":[Ljava/lang/String;
    if-eqz v0, :cond_6

    array-length v6, v0

    if-eqz v6, :cond_6

    .line 634
    aget-object v6, v0, v17
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v16, v6

    .line 639
    .end local v0    # "pkgs":[Ljava/lang/String;
    :cond_6
    move v0, v5

    move-object/from16 v11, v16

    goto :goto_2

    .line 636
    :catch_0
    move-exception v0

    .line 637
    .local v0, "e":Ljava/lang/Exception;
    const-string v6, "getPackagesFor uid exception!"

    invoke-static {v9, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 638
    const-string v0, "android"

    .line 639
    .end local v16    # "callerPkg":Ljava/lang/String;
    .local v0, "callerPkg":Ljava/lang/String;
    move-object v11, v0

    move v0, v5

    goto :goto_2

    .line 641
    .end local v0    # "callerPkg":Ljava/lang/String;
    .restart local v16    # "callerPkg":Ljava/lang/String;
    :cond_7
    const-string v0, "android"

    move-object v11, v0

    move v0, v5

    .end local v16    # "callerPkg":Ljava/lang/String;
    .restart local v0    # "callerPkg":Ljava/lang/String;
    goto :goto_2

    .line 644
    .end local v5    # "callerUid":I
    .local v0, "callerUid":I
    .restart local v16    # "callerPkg":Ljava/lang/String;
    :cond_8
    move-object/from16 v5, p2

    move-object v11, v5

    .line 648
    .end local v16    # "callerPkg":Ljava/lang/String;
    .local v11, "callerPkg":Ljava/lang/String;
    :goto_2
    const/4 v5, -0x1

    .line 649
    .local v5, "calleeUid":I
    iget-object v10, v4, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    .line 650
    .local v10, "calleePkg":Ljava/lang/String;
    iget-object v8, v4, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    .line 651
    .local v8, "className":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    .line 652
    .local v7, "action":Ljava/lang/String;
    iget-object v6, v4, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v6, :cond_9

    .line 653
    iget-object v6, v4, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v5, v6, Landroid/content/pm/ApplicationInfo;->uid:I

    move v6, v5

    goto :goto_3

    .line 652
    :cond_9
    move v6, v5

    .line 656
    .end local v5    # "calleeUid":I
    .local v6, "calleeUid":I
    :goto_3
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 657
    return v13

    .line 660
    :cond_a
    invoke-static {v11, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 661
    return v13

    .line 664
    :cond_b
    if-ltz v6, :cond_e

    .line 665
    iget-object v5, v1, Lcom/android/server/am/ActivityManagerService;->mActivityTaskManager:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v13, v4, Landroid/content/pm/ComponentInfo;->processName:Ljava/lang/String;

    invoke-static {v5, v10, v13, v6}, Lcom/android/server/wm/WindowProcessUtils;->isPackageRunning(Lcom/android/server/wm/ActivityTaskManagerService;Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 667
    shl-int/lit8 v13, v14, 0xc

    const/16 v17, 0x1

    move-object v5, v12

    move/from16 v18, v6

    .end local v6    # "calleeUid":I
    .local v18, "calleeUid":I
    move-object v6, v7

    move-object/from16 v23, v7

    .end local v7    # "action":Ljava/lang/String;
    .local v23, "action":Ljava/lang/String;
    move-object v7, v8

    move-object/from16 v24, v8

    .end local v8    # "className":Ljava/lang/String;
    .local v24, "className":Ljava/lang/String;
    move-object v8, v11

    move-object/from16 v25, v9

    move-object v9, v10

    move-object/from16 v26, v10

    .end local v10    # "calleePkg":Ljava/lang/String;
    .local v26, "calleePkg":Ljava/lang/String;
    move/from16 v10, p6

    move-object v1, v11

    .end local v11    # "callerPkg":Ljava/lang/String;
    .local v1, "callerPkg":Ljava/lang/String;
    move v11, v13

    move-object/from16 v19, v12

    .end local v12    # "checker":Lmiui/security/WakePathChecker;
    .local v19, "checker":Lmiui/security/WakePathChecker;
    move/from16 v12, v17

    invoke-virtual/range {v5 .. v12}, Lmiui/security/WakePathChecker;->calleeAliveMatchBlackRule(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)Z

    move-result v5

    const/4 v13, 0x1

    xor-int/2addr v5, v13

    .line 669
    .local v5, "isAllow":Z
    if-nez v5, :cond_c

    .line 670
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MIUILOG-Reject alive wakepath call "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " caller= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " callee= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v12, v26

    .end local v26    # "calleePkg":Ljava/lang/String;
    .local v12, "calleePkg":Ljava/lang/String;
    invoke-virtual {v6, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " classname="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v11, v24

    .end local v24    # "className":Ljava/lang/String;
    .local v11, "className":Ljava/lang/String;
    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " action="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v10, v23

    .end local v23    # "action":Ljava/lang/String;
    .local v10, "action":Ljava/lang/String;
    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " wakeType="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v7, v25

    invoke-static {v7, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 669
    .end local v10    # "action":Ljava/lang/String;
    .end local v11    # "className":Ljava/lang/String;
    .end local v12    # "calleePkg":Ljava/lang/String;
    .restart local v23    # "action":Ljava/lang/String;
    .restart local v24    # "className":Ljava/lang/String;
    .restart local v26    # "calleePkg":Ljava/lang/String;
    :cond_c
    move-object/from16 v10, v23

    move-object/from16 v11, v24

    move-object/from16 v12, v26

    .line 674
    .end local v23    # "action":Ljava/lang/String;
    .end local v24    # "className":Ljava/lang/String;
    .end local v26    # "calleePkg":Ljava/lang/String;
    .restart local v10    # "action":Ljava/lang/String;
    .restart local v11    # "className":Ljava/lang/String;
    .restart local v12    # "calleePkg":Ljava/lang/String;
    :goto_4
    return v5

    .line 665
    .end local v1    # "callerPkg":Ljava/lang/String;
    .end local v5    # "isAllow":Z
    .end local v18    # "calleeUid":I
    .end local v19    # "checker":Lmiui/security/WakePathChecker;
    .restart local v6    # "calleeUid":I
    .restart local v7    # "action":Ljava/lang/String;
    .restart local v8    # "className":Ljava/lang/String;
    .local v10, "calleePkg":Ljava/lang/String;
    .local v11, "callerPkg":Ljava/lang/String;
    .local v12, "checker":Lmiui/security/WakePathChecker;
    :cond_d
    move/from16 v18, v6

    move-object v1, v11

    move-object/from16 v19, v12

    const/4 v13, 0x1

    move-object v11, v8

    move-object v12, v10

    move-object v10, v7

    .end local v6    # "calleeUid":I
    .end local v7    # "action":Ljava/lang/String;
    .end local v8    # "className":Ljava/lang/String;
    .restart local v1    # "callerPkg":Ljava/lang/String;
    .local v10, "action":Ljava/lang/String;
    .local v11, "className":Ljava/lang/String;
    .local v12, "calleePkg":Ljava/lang/String;
    .restart local v18    # "calleeUid":I
    .restart local v19    # "checker":Lmiui/security/WakePathChecker;
    goto :goto_5

    .line 664
    .end local v1    # "callerPkg":Ljava/lang/String;
    .end local v18    # "calleeUid":I
    .end local v19    # "checker":Lmiui/security/WakePathChecker;
    .restart local v6    # "calleeUid":I
    .restart local v7    # "action":Ljava/lang/String;
    .restart local v8    # "className":Ljava/lang/String;
    .local v10, "calleePkg":Ljava/lang/String;
    .local v11, "callerPkg":Ljava/lang/String;
    .local v12, "checker":Lmiui/security/WakePathChecker;
    :cond_e
    move/from16 v18, v6

    move-object v1, v11

    move-object/from16 v19, v12

    move-object v11, v8

    move-object v12, v10

    move-object v10, v7

    .line 678
    .end local v6    # "calleeUid":I
    .end local v7    # "action":Ljava/lang/String;
    .end local v8    # "className":Ljava/lang/String;
    .restart local v1    # "callerPkg":Ljava/lang/String;
    .local v10, "action":Ljava/lang/String;
    .local v11, "className":Ljava/lang/String;
    .local v12, "calleePkg":Ljava/lang/String;
    .restart local v18    # "calleeUid":I
    .restart local v19    # "checker":Lmiui/security/WakePathChecker;
    :goto_5
    move-object/from16 v5, v19

    move-object v6, v10

    move-object v7, v11

    move-object v8, v1

    move-object v9, v12

    move-object/from16 v16, v10

    .end local v10    # "action":Ljava/lang/String;
    .local v16, "action":Ljava/lang/String;
    move v10, v0

    move-object/from16 v17, v11

    .end local v11    # "className":Ljava/lang/String;
    .local v17, "className":Ljava/lang/String;
    move/from16 v11, v18

    move-object/from16 v20, v12

    .end local v12    # "calleePkg":Ljava/lang/String;
    .local v20, "calleePkg":Ljava/lang/String;
    move/from16 v12, p5

    move/from16 v23, v13

    move/from16 v13, p6

    invoke-virtual/range {v5 .. v13}, Lmiui/security/WakePathChecker;->matchWakePathRule(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)Z

    move-result v5

    xor-int/lit8 v5, v5, 0x1

    .line 679
    .local v5, "ret":Z
    const-string v6, "checkWakePath"

    move-wide/from16 v7, v21

    .end local v21    # "startTime":J
    .local v7, "startTime":J
    invoke-static {v7, v8, v6}, Lcom/android/server/am/ActivityManagerServiceImpl;->checkTime(JLjava/lang/String;)V

    .line 680
    return v5

    .line 593
    .end local v0    # "callerUid":I
    .end local v1    # "callerPkg":Ljava/lang/String;
    .end local v5    # "ret":Z
    .end local v7    # "startTime":J
    .end local v16    # "action":Ljava/lang/String;
    .end local v17    # "className":Ljava/lang/String;
    .end local v18    # "calleeUid":I
    .end local v19    # "checker":Lmiui/security/WakePathChecker;
    .end local v20    # "calleePkg":Ljava/lang/String;
    :cond_f
    move/from16 v23, v13

    .line 594
    :goto_6
    return v23
.end method

.method private static doForegroundBoost(Lcom/android/server/am/ProcessRecord;J)Z
    .locals 4
    .param p0, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p1, "beginTime"    # J

    .line 1244
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p1

    const-wide/16 v2, 0x4e20

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 1245
    const/4 v0, 0x0

    return v0

    .line 1247
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getCurrentSchedulingGroup()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 1248
    iget-object v0, p0, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessStateRecord;->setCurrentSchedulingGroup(I)V

    .line 1250
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method private static doTopAppBoost(Lcom/android/server/am/ProcessRecord;J)Z
    .locals 4
    .param p0, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p1, "beginTime"    # J

    .line 1231
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p1

    const-wide/16 v2, 0xbb8

    cmp-long v0, v0, v2

    if-gtz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    .line 1232
    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getCurrentSchedulingGroup()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 1235
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getCurrentSchedulingGroup()I

    move-result v0

    if-ge v0, v1, :cond_1

    .line 1236
    iget-object v0, p0, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessStateRecord;->setCurrentSchedulingGroup(I)V

    .line 1237
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Process is boosted to top app, processName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Boost"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1240
    :cond_1
    const/4 v0, 0x1

    return v0

    .line 1233
    :cond_2
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method private dumpAppLogText(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;I)Z
    .locals 9
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;
    .param p4, "opti"    # I

    .line 1670
    array-length v0, p3

    const/4 v1, 0x0

    if-ge p4, v0, :cond_3

    .line 1671
    aget-object v0, p3, p4

    .line 1672
    .local v0, "dumpPackage":Ljava/lang/String;
    const/4 v2, 0x1

    add-int/2addr p4, v2

    .line 1677
    array-length v3, p3

    if-ge p4, v3, :cond_2

    .line 1679
    :try_start_0
    aget-object v3, p3, p4

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 1680
    .local v3, "uid":I
    iget-object v4, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v4
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_2

    .line 1681
    :try_start_1
    iget-object v5, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    invoke-virtual {v5, v0, v3}, Lcom/android/server/am/ActivityManagerService;->getProcessRecordLocked(Ljava/lang/String;I)Lcom/android/server/am/ProcessRecord;

    move-result-object v5

    .line 1682
    .local v5, "app":Lcom/android/server/am/ProcessRecord;
    if-eqz v5, :cond_1

    .line 1683
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "\n** APP LOGGIN in pid "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v5, Lcom/android/server/am/ProcessRecord;->mPid:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] **"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1685
    invoke-virtual {v5}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v7, v6

    .local v7, "thread":Landroid/app/IApplicationThread;
    if-eqz v6, :cond_0

    .line 1687
    :try_start_2
    new-instance v6, Lcom/android/internal/os/TransferPipe;

    invoke-direct {v6}, Lcom/android/internal/os/TransferPipe;-><init>()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1689
    .local v6, "tp":Lcom/android/internal/os/TransferPipe;
    :try_start_3
    invoke-virtual {v6}, Lcom/android/internal/os/TransferPipe;->getWriteFd()Landroid/os/ParcelFileDescriptor;

    move-result-object v8

    invoke-interface {v7, v8}, Landroid/app/IApplicationThread;->dumpLogText(Landroid/os/ParcelFileDescriptor;)V

    .line 1690
    invoke-virtual {v6, p1}, Lcom/android/internal/os/TransferPipe;->go(Ljava/io/FileDescriptor;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1692
    :try_start_4
    invoke-virtual {v6}, Lcom/android/internal/os/TransferPipe;->kill()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1693
    nop

    .line 1694
    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    return v2

    .line 1692
    :catchall_0
    move-exception v2

    :try_start_6
    invoke-virtual {v6}, Lcom/android/internal/os/TransferPipe;->kill()V

    .line 1693
    nop

    .end local v0    # "dumpPackage":Ljava/lang/String;
    .end local v3    # "uid":I
    .end local v5    # "app":Lcom/android/server/am/ProcessRecord;
    .end local v7    # "thread":Landroid/app/IApplicationThread;
    .end local p0    # "this":Lcom/android/server/am/ActivityManagerServiceImpl;
    .end local p1    # "fd":Ljava/io/FileDescriptor;
    .end local p2    # "pw":Ljava/io/PrintWriter;
    .end local p3    # "args":[Ljava/lang/String;
    .end local p4    # "opti":I
    throw v2
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1698
    .end local v6    # "tp":Lcom/android/internal/os/TransferPipe;
    .restart local v0    # "dumpPackage":Ljava/lang/String;
    .restart local v3    # "uid":I
    .restart local v5    # "app":Lcom/android/server/am/ProcessRecord;
    .restart local v7    # "thread":Landroid/app/IApplicationThread;
    .restart local p0    # "this":Lcom/android/server/am/ActivityManagerServiceImpl;
    .restart local p1    # "fd":Ljava/io/FileDescriptor;
    .restart local p2    # "pw":Ljava/io/PrintWriter;
    .restart local p3    # "args":[Ljava/lang/String;
    .restart local p4    # "opti":I
    :catch_0
    move-exception v2

    .line 1699
    .local v2, "e":Landroid/os/RemoteException;
    :try_start_7
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Got RemoteException! "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1700
    invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V

    goto :goto_0

    .line 1695
    .end local v2    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v2

    .line 1696
    .local v2, "e":Ljava/io/IOException;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Got IoException! "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1697
    invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V

    .line 1701
    .end local v2    # "e":Ljava/io/IOException;
    nop

    .line 1703
    .end local v7    # "thread":Landroid/app/IApplicationThread;
    :cond_0
    :goto_0
    nop

    .line 1707
    .end local v5    # "app":Lcom/android/server/am/ProcessRecord;
    monitor-exit v4

    .line 1711
    .end local v3    # "uid":I
    goto :goto_1

    .line 1704
    .restart local v3    # "uid":I
    .restart local v5    # "app":Lcom/android/server/am/ProcessRecord;
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "app-logging: "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "("

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ") not running."

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1705
    monitor-exit v4

    return v1

    .line 1707
    .end local v5    # "app":Lcom/android/server/am/ProcessRecord;
    :catchall_1
    move-exception v2

    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .end local v0    # "dumpPackage":Ljava/lang/String;
    .end local p0    # "this":Lcom/android/server/am/ActivityManagerServiceImpl;
    .end local p1    # "fd":Ljava/io/FileDescriptor;
    .end local p2    # "pw":Ljava/io/PrintWriter;
    .end local p3    # "args":[Ljava/lang/String;
    .end local p4    # "opti":I
    :try_start_8
    throw v2
    :try_end_8
    .catch Ljava/lang/NumberFormatException; {:try_start_8 .. :try_end_8} :catch_2

    .line 1708
    .end local v3    # "uid":I
    .restart local v0    # "dumpPackage":Ljava/lang/String;
    .restart local p0    # "this":Lcom/android/server/am/ActivityManagerServiceImpl;
    .restart local p1    # "fd":Ljava/io/FileDescriptor;
    .restart local p2    # "pw":Ljava/io/PrintWriter;
    .restart local p3    # "args":[Ljava/lang/String;
    .restart local p4    # "opti":I
    :catch_2
    move-exception v2

    .line 1709
    .local v2, "e":Ljava/lang/NumberFormatException;
    const-string v3, "app-logging: uid format is error, please input integer."

    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1710
    return v1

    .line 1713
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_2
    const-string v2, "app-logging: no uid specified."

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1715
    :goto_1
    return v1

    .line 1674
    .end local v0    # "dumpPackage":Ljava/lang/String;
    :cond_3
    const-string v0, "app-logging: no process name specified"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1675
    return v1
.end method

.method private static ensureDeviceProvisioned(Landroid/content/Context;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .line 288
    invoke-static {p0}, Lcom/android/server/am/ActivityManagerServiceImpl;->isDeviceProvisioned(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 289
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 291
    .local v0, "pm":Landroid/content/pm/PackageManager;
    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    const-string v2, "com.android.provision.activities.DefaultActivity"

    const-string v3, "com.android.provision"

    if-nez v1, :cond_0

    .line 292
    new-instance v1, Landroid/content/ComponentName;

    invoke-direct {v1, v3, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .local v1, "checkEnableName":Landroid/content/ComponentName;
    goto :goto_0

    .line 295
    .end local v1    # "checkEnableName":Landroid/content/ComponentName;
    :cond_0
    new-instance v1, Landroid/content/ComponentName;

    const-string v4, "com.google.android.setupwizard"

    const-string v5, "com.google.android.setupwizard.SetupWizardActivity"

    invoke-direct {v1, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    .restart local v1    # "checkEnableName":Landroid/content/ComponentName;
    :goto_0
    if-eqz v0, :cond_2

    .line 300
    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    .line 302
    const-string v4, "ActivityManagerServiceImpl"

    const-string v5, "The device provisioned state is inconsistent,try to restore."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "device_provisioned"

    const/4 v6, 0x1

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 306
    sget-boolean v4, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v4, :cond_1

    .line 307
    new-instance v4, Landroid/content/ComponentName;

    invoke-direct {v4, v3, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v4

    .line 309
    .local v2, "name":Landroid/content/ComponentName;
    invoke-virtual {v0, v2, v6, v6}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 312
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.MAIN"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 313
    .local v3, "intent":Landroid/content/Intent;
    invoke-virtual {v3, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 314
    const/high16 v4, 0x10000000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 315
    const-string v4, "android.intent.category.HOME"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 316
    invoke-virtual {p0, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 317
    .end local v2    # "name":Landroid/content/ComponentName;
    .end local v3    # "intent":Landroid/content/Intent;
    goto :goto_1

    .line 318
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "user_setup_complete"

    invoke-static {v2, v3, v6}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 324
    .end local v0    # "pm":Landroid/content/pm/PackageManager;
    .end local v1    # "checkEnableName":Landroid/content/ComponentName;
    :cond_2
    :goto_1
    return-void
.end method

.method private generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair;
    .locals 3
    .param p1, "enabled"    # Ljava/lang/String;
    .param p2, "disabled"    # Ljava/lang/String;
    .param p3, "config"    # Z
    .param p4, "group"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1754
    const-string v0, " "

    if-eqz p3, :cond_1

    .line 1755
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, p4

    goto :goto_0

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    .line 1757
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object v0, p4

    goto :goto_1

    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 1759
    :goto_2
    new-instance v0, Landroid/util/Pair;

    invoke-direct {v0, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method private getGreezeService()Lcom/miui/server/greeze/GreezeManagerInternal;
    .locals 1

    .line 1290
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->greezer:Lcom/miui/server/greeze/GreezeManagerInternal;

    if-nez v0, :cond_0

    .line 1291
    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerInternal;->getInstance()Lcom/miui/server/greeze/GreezeManagerInternal;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->greezer:Lcom/miui/server/greeze/GreezeManagerInternal;

    .line 1292
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->greezer:Lcom/miui/server/greeze/GreezeManagerInternal;

    return-object v0
.end method

.method public static getInstance()Lcom/android/server/am/ActivityManagerServiceImpl;
    .locals 1

    .line 236
    const-class v0, Lcom/android/server/am/ActivityManagerServiceStub;

    invoke-static {v0}, Lcom/miui/base/MiuiStubUtil;->getImpl(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/ActivityManagerServiceImpl;

    return-object v0
.end method

.method private getIntentInfo(IZ)[Landroid/os/Parcelable;
    .locals 3
    .param p1, "pid"    # I
    .param p2, "isForLast"    # Z

    .line 1423
    if-eqz p2, :cond_0

    .line 1424
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/os/Parcelable;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mLastSplitIntent:Landroid/content/Intent;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mSplitExtras:Landroid/os/Bundle;

    aput-object v2, v0, v1

    return-object v0

    .line 1426
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mCurrentSplitIntent:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    const/4 v1, 0x0

    filled-new-array {v0, v1}, [Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method private static isDeviceProvisioned(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .line 283
    nop

    .line 284
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 283
    const-string v1, "device_provisioned"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method

.method private isProtectProcess(ILjava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "uid"    # I
    .param p2, "pkgName"    # Ljava/lang/String;
    .param p3, "procName"    # Ljava/lang/String;

    .line 1459
    const/16 v0, 0x1d8

    .line 1464
    .local v0, "protectFlag":I
    iget-object v1, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-interface {v1, p1, p3}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->isProcessPerceptible(ILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    .line 1465
    invoke-interface {v1, v0, p2, p3}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->isProcessWhiteList(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 1469
    :cond_0
    const/4 v1, 0x0

    return v1

    .line 1467
    :cond_1
    :goto_0
    const/4 v1, 0x1

    return v1
.end method

.method private static isSystem(Ljava/lang/String;I)Z
    .locals 7
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "userId"    # I

    .line 1861
    const-class v0, Landroid/content/pm/PackageManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageManagerInternal;

    .line 1862
    .local v0, "internal":Landroid/content/pm/PackageManagerInternal;
    const-wide/16 v3, 0x0

    const/16 v5, 0x3e8

    move-object v1, v0

    move-object v2, p0

    move v6, p1

    invoke-virtual/range {v1 .. v6}, Landroid/content/pm/PackageManagerInternal;->getApplicationInfo(Ljava/lang/String;JII)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 1864
    .local v1, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    if-eqz v1, :cond_1

    .line 1865
    invoke-virtual {v1}, Landroid/content/pm/ApplicationInfo;->isSystemApp()Z

    move-result v2

    if-nez v2, :cond_0

    iget v2, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 1866
    invoke-static {v2}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v2

    const/16 v3, 0x2710

    if-ge v2, v3, :cond_1

    :cond_0
    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    .line 1864
    :goto_0
    return v2
.end method

.method public static isSystemPackage(Ljava/lang/String;I)Z
    .locals 4
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "userId"    # I

    .line 919
    const/4 v0, 0x1

    :try_start_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-interface {v1, p0, v2, v3, p1}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;JI)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 920
    .local v1, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    if-nez v1, :cond_0

    return v0

    .line 921
    :cond_0
    iget v2, v1, Landroid/content/pm/ApplicationInfo;->flags:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 922
    .local v2, "flags":I
    and-int/lit8 v3, v2, 0x1

    if-nez v3, :cond_2

    and-int/lit16 v3, v2, 0x80

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :cond_2
    :goto_0
    return v0

    .line 924
    .end local v1    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    .end local v2    # "flags":I
    :catch_0
    move-exception v1

    .line 925
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 927
    .end local v1    # "e":Ljava/lang/Exception;
    return v0
.end method

.method private synthetic lambda$dumpSystemTraces$0(Ljava/lang/String;)V
    .locals 6
    .param p1, "path"    # Ljava/lang/String;

    .line 1138
    const-string v0, "MIUIScout App"

    sget v1, Lcom/android/server/am/ActivityManagerService;->MY_PID:I

    invoke-static {v0, v1}, Lcom/android/server/ScoutHelper;->CheckDState(Ljava/lang/String;I)Ljava/lang/Boolean;

    .line 1140
    const-string v0, "MIUIScout App"

    const-string v1, "Start dumping system_server trace ..."

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1141
    sget v0, Lcom/android/server/am/ActivityManagerService;->MY_PID:I

    const-string v1, "App Scout Exception"

    invoke-virtual {p0, v0, p1, v1}, Lcom/android/server/am/ActivityManagerServiceImpl;->dumpOneProcessTraces(ILjava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 1144
    .local v0, "systemTraceFile":Ljava/io/File;
    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 1145
    const-string v2, "MIUIScout App"

    const-string v3, "Dump scout system trace file successfully!"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1152
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1153
    .local v2, "tempArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v3, Lcom/android/server/am/ActivityManagerServiceImpl;->dumpTraceRequestList:Ljava/util/ArrayList;

    monitor-enter v3

    .line 1154
    :try_start_0
    sget-object v4, Lcom/android/server/am/ActivityManagerServiceImpl;->dumpTraceRequestList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1155
    .local v5, "filePath":Ljava/lang/String;
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1156
    nop

    .end local v5    # "filePath":Ljava/lang/String;
    goto :goto_0

    .line 1157
    :cond_0
    sget-object v4, Lcom/android/server/am/ActivityManagerServiceImpl;->dumpTraceRequestList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 1158
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1159
    sget-object v3, Lcom/android/server/am/ActivityManagerServiceImpl;->dumpFlag:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1160
    const-string v1, "MIUIScout App"

    const-string/jumbo v3, "starting copying file"

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1163
    sget-object v1, Lcom/android/server/am/ActivityManagerServiceImpl;->requestDumpTraceCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    if-lez v1, :cond_1

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 1164
    new-instance v1, Lcom/android/server/am/ActivityManagerServiceImpl$2;

    invoke-direct {v1, p0, v2, v0}, Lcom/android/server/am/ActivityManagerServiceImpl$2;-><init>(Lcom/android/server/am/ActivityManagerServiceImpl;Ljava/util/ArrayList;Ljava/io/File;)V

    .line 1192
    invoke-virtual {v1}, Lcom/android/server/am/ActivityManagerServiceImpl$2;->start()V

    .line 1194
    :cond_1
    return-void

    .line 1158
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 1147
    .end local v2    # "tempArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    const-string v2, "MIUIScout App"

    const-string v3, "Dump scout system trace file fail!"

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1148
    sget-object v2, Lcom/android/server/am/ActivityManagerServiceImpl;->dumpFlag:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1149
    return-void
.end method

.method private recordAppBehavior(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 7
    .param p1, "sourcePkg"    # Ljava/lang/String;
    .param p2, "targetPkg"    # Ljava/lang/String;
    .param p3, "targetUid"    # I
    .param p4, "grantUri"    # Ljava/lang/String;

    .line 1846
    invoke-static {p3}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v0

    const/16 v1, 0x2710

    if-lt v0, v1, :cond_1

    .line 1847
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-static {v0}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v0

    invoke-static {p1, v0}, Lcom/android/server/am/ActivityManagerServiceImpl;->isSystem(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1848
    invoke-static {p3}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v0

    invoke-static {p2, v0}, Lcom/android/server/am/ActivityManagerServiceImpl;->isSystem(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1849
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mSecurityInternal:Lmiui/security/SecurityManagerInternal;

    if-nez v0, :cond_0

    .line 1850
    const-class v0, Lmiui/security/SecurityManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/security/SecurityManagerInternal;

    iput-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mSecurityInternal:Lmiui/security/SecurityManagerInternal;

    .line 1852
    :cond_0
    iget-object v1, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mSecurityInternal:Lmiui/security/SecurityManagerInternal;

    if-eqz v1, :cond_1

    .line 1853
    const/16 v2, 0x1e

    const-wide/16 v4, 0x1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "@"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v3, p2

    invoke-virtual/range {v1 .. v6}, Lmiui/security/SecurityManagerInternal;->recordAppBehaviorAsync(ILjava/lang/String;JLjava/lang/String;)V

    .line 1858
    :cond_1
    return-void
.end method

.method private setIntentInfo(Landroid/content/Intent;ILandroid/os/Bundle;Z)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "pid"    # I
    .param p3, "bundle"    # Landroid/os/Bundle;
    .param p4, "isForLast"    # Z

    .line 1430
    if-eqz p4, :cond_0

    .line 1431
    iput-object p1, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mLastSplitIntent:Landroid/content/Intent;

    .line 1432
    iput-object p3, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mSplitExtras:Landroid/os/Bundle;

    goto :goto_0

    .line 1434
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mCurrentSplitIntent:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1435
    const-string v0, "ActivityManagerServiceImpl"

    const-string v1, "CRITICAL_LOG add intent info."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1437
    :cond_1
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mCurrentSplitIntent:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1439
    :goto_0
    return-void
.end method


# virtual methods
.method public addToEntryStack(ILandroid/os/IBinder;ILandroid/content/Intent;)V
    .locals 5
    .param p1, "pid"    # I
    .param p2, "token"    # Landroid/os/IBinder;
    .param p3, "resultCode"    # I
    .param p4, "resultData"    # Landroid/content/Intent;

    .line 1404
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->splitEntryStackLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1405
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mSplitActivityEntryStack:Ljava/util/Map;

    if-nez v1, :cond_0

    .line 1406
    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v1, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mSplitActivityEntryStack:Ljava/util/Map;

    .line 1407
    const-string v1, "ActivityManagerServiceImpl"

    const-string v2, "SplitEntryStack: init"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1409
    :cond_0
    iget-object v1, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mSplitActivityEntryStack:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Stack;

    .line 1410
    .local v1, "stack":Ljava/util/Stack;, "Ljava/util/Stack<Landroid/os/IBinder;>;"
    if-nez v1, :cond_1

    .line 1411
    new-instance v2, Ljava/util/Stack;

    invoke-direct {v2}, Ljava/util/Stack;-><init>()V

    move-object v1, v2

    .line 1412
    iget-object v2, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mSplitActivityEntryStack:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1413
    const-string v2, "ActivityManagerServiceImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SplitEntryStack: create stack, pid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1415
    :cond_1
    move-object v2, p2

    check-cast v2, Landroid/os/Binder;

    invoke-virtual {v1, v2}, Ljava/util/Stack;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1416
    invoke-virtual {v1, p2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1417
    const-string v2, "ActivityManagerServiceImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SplitEntryStack: push to stack, size="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/util/Stack;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " pid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1419
    .end local v1    # "stack":Ljava/util/Stack;, "Ljava/util/Stack<Landroid/os/IBinder;>;"
    :cond_2
    monitor-exit v0

    .line 1420
    return-void

    .line 1419
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public backupBind(IZ)V
    .locals 3
    .param p1, "uid"    # I
    .param p2, "start"    # Z

    .line 1794
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->greezer:Lcom/miui/server/greeze/GreezeManagerInternal;

    if-nez v0, :cond_0

    return-void

    .line 1795
    :cond_0
    const/16 v0, 0x2710

    if-lt p1, v0, :cond_4

    const/16 v0, 0x4e1f

    if-le p1, v0, :cond_1

    goto :goto_1

    .line 1796
    :cond_1
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mBackupingList:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz p2, :cond_2

    .line 1797
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mBackupingList:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1798
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->greezer:Lcom/miui/server/greeze/GreezeManagerInternal;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/miui/server/greeze/GreezeManagerInternal;->notifyBackup(IZ)V

    goto :goto_0

    .line 1799
    :cond_2
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mBackupingList:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    if-nez p2, :cond_3

    .line 1800
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mBackupingList:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1802
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1803
    .local v0, "b":Landroid/os/Bundle;
    const-string/jumbo v1, "uid"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1804
    const-string/jumbo v1, "start"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1805
    invoke-static {}, Lcom/miui/whetstone/PowerKeeperPolicy;->getInstance()Lcom/miui/whetstone/PowerKeeperPolicy;

    move-result-object v1

    const/16 v2, 0x12

    invoke-virtual {v1, v2, v0}, Lcom/miui/whetstone/PowerKeeperPolicy;->notifyEvent(ILandroid/os/Bundle;)V

    .line 1807
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_3
    :goto_0
    return-void

    .line 1795
    :cond_4
    :goto_1
    return-void
.end method

.method public checkAppDisableStatus(Ljava/lang/String;)Z
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1765
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "sys.rescuepartyplus.disable_autorestart."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1766
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Disable App ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] auto start!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ActivityManagerServiceImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1767
    const/4 v0, 0x1

    return v0

    .line 1769
    :cond_0
    return v1
.end method

.method public checkRunningCompatibility(Landroid/app/IApplicationThread;ILcom/android/server/am/ContentProviderRecord;I)Z
    .locals 11
    .param p1, "caller"    # Landroid/app/IApplicationThread;
    .param p2, "callingUid"    # I
    .param p3, "record"    # Lcom/android/server/am/ContentProviderRecord;
    .param p4, "userId"    # I

    .line 507
    iget-boolean v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mSystemReady:Z

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 508
    return v1

    .line 510
    :cond_0
    if-eqz p3, :cond_5

    iget-object v0, p3, Lcom/android/server/am/ContentProviderRecord;->name:Landroid/content/ComponentName;

    if-nez v0, :cond_1

    goto :goto_1

    .line 514
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 515
    .local v0, "intent":Landroid/content/Intent;
    iget-object v2, p3, Lcom/android/server/am/ContentProviderRecord;->name:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p3, Lcom/android/server/am/ContentProviderRecord;->name:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 516
    const-string v2, "android.intent.extra.UID"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 519
    iget-object v2, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mActivityTaskManager:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-static {v2, p1}, Lcom/android/server/wm/WindowProcessUtils;->getCallerInfo(Lcom/android/server/wm/ActivityTaskManagerService;Landroid/app/IApplicationThread;)Lmiui/security/CallerInfo;

    move-result-object v9

    .line 521
    .local v9, "callerInfo":Lmiui/security/CallerInfo;
    if-nez v9, :cond_3

    .line 522
    iget-object v2, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mPackageManager:Landroid/content/pm/PackageManagerInternal;

    invoke-virtual {v2, p2}, Landroid/content/pm/PackageManagerInternal;->getPackage(I)Lcom/android/server/pm/pkg/AndroidPackage;

    move-result-object v2

    .line 523
    .local v2, "aPackage":Lcom/android/server/pm/pkg/AndroidPackage;
    if-nez v2, :cond_2

    .line 524
    return v1

    .line 526
    :cond_2
    invoke-interface {v2}, Lcom/android/server/pm/pkg/AndroidPackage;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 527
    .local v2, "callerPkg":Ljava/lang/String;
    move-object v10, v2

    goto :goto_0

    .line 528
    .end local v2    # "callerPkg":Ljava/lang/String;
    :cond_3
    iget-object v2, v9, Lmiui/security/CallerInfo;->callerPkg:Ljava/lang/String;

    move-object v10, v2

    .line 530
    .local v10, "callerPkg":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/android/server/clipboard/ClipboardServiceStub;->get()Lcom/android/server/clipboard/ClipboardServiceStub;

    move-result-object v2

    iget-object v3, p3, Lcom/android/server/am/ContentProviderRecord;->info:Landroid/content/pm/ProviderInfo;

    .line 531
    invoke-virtual {v2, v10, p2, v3, p4}, Lcom/android/server/clipboard/ClipboardServiceStub;->checkProviderWakePathForClipboard(Ljava/lang/String;ILandroid/content/pm/ProviderInfo;I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 532
    return v1

    .line 534
    :cond_4
    iget-object v2, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    const/4 v4, 0x0

    iget-object v6, p3, Lcom/android/server/am/ContentProviderRecord;->info:Landroid/content/pm/ProviderInfo;

    const/4 v7, 0x4

    move-object v3, v9

    move-object v5, v0

    move v8, p4

    invoke-static/range {v2 .. v8}, Lcom/android/server/am/ActivityManagerServiceImpl;->checkWakePath(Lcom/android/server/am/ActivityManagerService;Lmiui/security/CallerInfo;Ljava/lang/String;Landroid/content/Intent;Landroid/content/pm/ComponentInfo;II)Z

    move-result v1

    return v1

    .line 511
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v9    # "callerInfo":Lmiui/security/CallerInfo;
    .end local v10    # "callerPkg":Ljava/lang/String;
    :cond_5
    :goto_1
    return v1
.end method

.method public checkRunningCompatibility(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;I)Z
    .locals 2
    .param p1, "caller"    # Landroid/app/IApplicationThread;
    .param p2, "service"    # Landroid/content/Intent;
    .param p3, "resolvedType"    # Ljava/lang/String;
    .param p4, "userId"    # I

    .line 445
    iget-boolean v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mSystemReady:Z

    if-nez v0, :cond_0

    .line 446
    const/4 v0, 0x1

    return v0

    .line 448
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mActivityTaskManager:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-static {v0, p1}, Lcom/android/server/wm/WindowProcessUtils;->getCallerInfo(Lcom/android/server/wm/ActivityTaskManagerService;Landroid/app/IApplicationThread;)Lmiui/security/CallerInfo;

    move-result-object v0

    .line 449
    .local v0, "callerInfo":Lmiui/security/CallerInfo;
    invoke-direct {p0, p2, p3, v0, p4}, Lcom/android/server/am/ActivityManagerServiceImpl;->checkServiceWakePath(Landroid/content/Intent;Ljava/lang/String;Lmiui/security/CallerInfo;I)Z

    move-result v1

    return v1
.end method

.method public checkRunningCompatibility(Landroid/app/IApplicationThread;Landroid/content/pm/ActivityInfo;Landroid/content/Intent;ILjava/lang/String;)Z
    .locals 8
    .param p1, "caller"    # Landroid/app/IApplicationThread;
    .param p2, "info"    # Landroid/content/pm/ActivityInfo;
    .param p3, "intent"    # Landroid/content/Intent;
    .param p4, "userId"    # I
    .param p5, "callingPackage"    # Ljava/lang/String;

    .line 492
    if-nez p2, :cond_0

    .line 493
    const/4 v0, 0x1

    return v0

    .line 495
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mActivityTaskManager:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-static {v0, p1}, Lcom/android/server/wm/WindowProcessUtils;->getCallerInfo(Lcom/android/server/wm/ActivityTaskManagerService;Landroid/app/IApplicationThread;)Lmiui/security/CallerInfo;

    move-result-object v0

    .line 496
    .local v0, "callerInfo":Lmiui/security/CallerInfo;
    if-eqz v0, :cond_1

    .line 497
    invoke-static {}, Landroid/app/PrivacyTestModeStub;->get()Landroid/app/PrivacyTestModeStub;

    move-result-object v1

    iget-object v2, v0, Lmiui/security/CallerInfo;->callerProcessName:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mContext:Landroid/content/Context;

    move-object v3, p2

    move-object v4, p3

    move-object v5, p5

    invoke-virtual/range {v1 .. v6}, Landroid/app/PrivacyTestModeStub;->collectPrivacyTestModeInfo(Ljava/lang/String;Landroid/content/pm/ActivityInfo;Landroid/content/Intent;Ljava/lang/String;Landroid/content/Context;)V

    .line 499
    :cond_1
    iget-object v1, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    const/4 v6, 0x1

    move-object v2, v0

    move-object v3, p5

    move-object v4, p3

    move-object v5, p2

    move v7, p4

    invoke-static/range {v1 .. v7}, Lcom/android/server/am/ActivityManagerServiceImpl;->checkWakePath(Lcom/android/server/am/ActivityManagerService;Lmiui/security/CallerInfo;Ljava/lang/String;Landroid/content/Intent;Landroid/content/pm/ComponentInfo;II)Z

    move-result v1

    return v1
.end method

.method public checkRunningCompatibility(Landroid/content/ComponentName;III)Z
    .locals 16
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "callingUid"    # I
    .param p3, "callingPid"    # I
    .param p4, "userId"    # I

    .line 541
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 542
    return v1

    .line 544
    :cond_0
    if-eqz p1, :cond_b

    invoke-static {}, Landroid/miui/AppOpsUtils;->isXOptMode()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object/from16 v15, p0

    move/from16 v3, p3

    move/from16 v14, p4

    goto/16 :goto_5

    .line 547
    :cond_1
    invoke-static/range {p2 .. p2}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v2

    .line 548
    .local v2, "callingAppId":I
    const/16 v0, 0x3e8

    if-eq v2, v0, :cond_a

    if-eqz v2, :cond_a

    const/16 v0, 0x7d0

    if-ne v2, v0, :cond_2

    move-object/from16 v15, p0

    move/from16 v3, p3

    move/from16 v14, p4

    goto/16 :goto_4

    .line 553
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v10

    .line 554
    .local v10, "calleePackage":Ljava/lang/String;
    invoke-static/range {p3 .. p3}, Lcom/android/server/am/ProcessUtils;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;

    move-result-object v11

    .line 555
    .local v11, "callerInfo":Lcom/android/server/am/ProcessRecord;
    const/4 v12, 0x0

    const-string v0, "ActivityManagerServiceImpl"

    if-eqz v11, :cond_9

    iget-object v3, v11, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    if-nez v3, :cond_3

    move-object/from16 v15, p0

    move/from16 v14, p4

    goto/16 :goto_3

    .line 559
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v11, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":widgetProvider"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 560
    .local v13, "widgetProcessName":Ljava/lang/String;
    sget-object v3, Lcom/android/server/am/ActivityManagerServiceImpl;->WIDGET_PROVIDER_WHITE_LIST:Ljava/util/List;

    iget-object v4, v11, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, v11, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 561
    invoke-virtual {v13, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 562
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MIUILOG- Reject widget call from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v11, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 563
    invoke-static {}, Lmiui/security/WakePathChecker;->getInstance()Lmiui/security/WakePathChecker;

    move-result-object v3

    iget-object v0, v11, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v4, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/4 v6, 0x0

    iget v0, v11, Lcom/android/server/am/ProcessRecord;->uid:I

    .line 564
    invoke-static {v0}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v7

    const/4 v9, 0x0

    .line 563
    move-object v5, v10

    move/from16 v8, p4

    invoke-virtual/range {v3 .. v9}, Lmiui/security/WakePathChecker;->recordWakePathCall(Ljava/lang/String;Ljava/lang/String;IIIZ)V

    .line 567
    :cond_4
    iget-object v0, v11, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, v11, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 568
    invoke-static {v0, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    move-object/from16 v15, p0

    move/from16 v14, p4

    goto :goto_2

    .line 572
    :cond_5
    :try_start_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_2

    const-wide/16 v3, 0x0

    move/from16 v14, p4

    :try_start_1
    invoke-interface {v0, v10, v3, v4, v14}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;JI)Landroid/content/pm/ApplicationInfo;

    move-result-object v0
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 574
    .local v0, "ai":Landroid/content/pm/ApplicationInfo;
    if-eqz v0, :cond_7

    move-object/from16 v15, p0

    :try_start_2
    iget-object v3, v15, Lcom/android/server/am/ActivityManagerServiceImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mActivityTaskManager:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v4, v0, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    iget v5, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v3, v10, v4, v5}, Lcom/android/server/wm/WindowProcessUtils;->isPackageRunning(Lcom/android/server/wm/ActivityTaskManagerService;Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v3
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    if-eqz v3, :cond_6

    goto :goto_0

    .line 580
    .end local v0    # "ai":Landroid/content/pm/ApplicationInfo;
    :cond_6
    goto :goto_1

    .line 578
    :catch_0
    move-exception v0

    goto :goto_1

    .line 574
    .restart local v0    # "ai":Landroid/content/pm/ApplicationInfo;
    :cond_7
    move-object/from16 v15, p0

    .line 576
    :goto_0
    return v1

    .line 578
    .end local v0    # "ai":Landroid/content/pm/ApplicationInfo;
    :catch_1
    move-exception v0

    move-object/from16 v15, p0

    goto :goto_1

    :catch_2
    move-exception v0

    move-object/from16 v15, p0

    move/from16 v14, p4

    .line 581
    :goto_1
    invoke-static {}, Lmiui/security/WakePathChecker;->getInstance()Lmiui/security/WakePathChecker;

    move-result-object v3

    iget-object v0, v11, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v4, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/4 v6, 0x0

    iget v0, v11, Lcom/android/server/am/ProcessRecord;->uid:I

    .line 583
    invoke-static {v0}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v7

    const/4 v9, 0x0

    .line 581
    move-object v5, v10

    move/from16 v8, p4

    invoke-virtual/range {v3 .. v9}, Lmiui/security/WakePathChecker;->recordWakePathCall(Ljava/lang/String;Ljava/lang/String;IIIZ)V

    .line 584
    return v12

    .line 567
    :cond_8
    move-object/from16 v15, p0

    move/from16 v14, p4

    .line 569
    :goto_2
    return v1

    .line 555
    .end local v13    # "widgetProcessName":Ljava/lang/String;
    :cond_9
    move-object/from16 v15, p0

    move/from16 v14, p4

    .line 556
    :goto_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MIUILOG- Reject call from dying process "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v3, p3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 557
    return v12

    .line 548
    .end local v10    # "calleePackage":Ljava/lang/String;
    .end local v11    # "callerInfo":Lcom/android/server/am/ProcessRecord;
    :cond_a
    move-object/from16 v15, p0

    move/from16 v3, p3

    move/from16 v14, p4

    .line 551
    :goto_4
    return v1

    .line 544
    .end local v2    # "callingAppId":I
    :cond_b
    move-object/from16 v15, p0

    move/from16 v3, p3

    move/from16 v14, p4

    .line 545
    :goto_5
    return v1
.end method

.method public checkRunningCompatibility(Landroid/content/Intent;Ljava/lang/String;III)Z
    .locals 3
    .param p1, "service"    # Landroid/content/Intent;
    .param p2, "resolvedType"    # Ljava/lang/String;
    .param p3, "callingUid"    # I
    .param p4, "callingPid"    # I
    .param p5, "userId"    # I

    .line 455
    iget-boolean v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mSystemReady:Z

    if-nez v0, :cond_0

    .line 456
    const/4 v0, 0x1

    return v0

    .line 458
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mActivityTaskManager:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-static {v0, p4, p3}, Lcom/android/server/wm/WindowProcessUtils;->getCallerInfo(Lcom/android/server/wm/ActivityTaskManagerService;II)Lmiui/security/CallerInfo;

    move-result-object v0

    .line 459
    .local v0, "callerInfo":Lmiui/security/CallerInfo;
    if-nez v0, :cond_1

    .line 460
    invoke-static {p4}, Lcom/android/server/am/ProcessUtils;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;

    move-result-object v1

    .line 461
    .local v1, "record":Lcom/android/server/am/ProcessRecord;
    if-eqz v1, :cond_1

    .line 462
    new-instance v2, Lmiui/security/CallerInfo;

    invoke-direct {v2}, Lmiui/security/CallerInfo;-><init>()V

    move-object v0, v2

    .line 463
    iput p3, v0, Lmiui/security/CallerInfo;->callerUid:I

    .line 464
    iget-object v2, v1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iput-object v2, v0, Lmiui/security/CallerInfo;->callerPkg:Ljava/lang/String;

    .line 465
    iput p4, v0, Lmiui/security/CallerInfo;->callerPid:I

    .line 466
    iget-object v2, v1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    iput-object v2, v0, Lmiui/security/CallerInfo;->callerProcessName:Ljava/lang/String;

    .line 469
    .end local v1    # "record":Lcom/android/server/am/ProcessRecord;
    :cond_1
    invoke-direct {p0, p1, p2, v0, p5}, Lcom/android/server/am/ActivityManagerServiceImpl;->checkServiceWakePath(Landroid/content/Intent;Ljava/lang/String;Lmiui/security/CallerInfo;I)Z

    move-result v1

    return v1
.end method

.method public checkStartInputMethodSettingsActivity(Landroid/content/IIntentSender;)Z
    .locals 3
    .param p1, "target"    # Landroid/content/IIntentSender;

    .line 1646
    instance-of v0, p1, Lcom/android/server/am/PendingIntentRecord;

    if-eqz v0, :cond_0

    .line 1647
    move-object v0, p1

    check-cast v0, Lcom/android/server/am/PendingIntentRecord;

    .line 1648
    .local v0, "pendingIntentRecord":Lcom/android/server/am/PendingIntentRecord;
    iget-object v1, v0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    iget-object v1, v1, Lcom/android/server/am/PendingIntentRecord$Key;->requestIntent:Landroid/content/Intent;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    iget-object v1, v1, Lcom/android/server/am/PendingIntentRecord$Key;->requestIntent:Landroid/content/Intent;

    .line 1650
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 1649
    const-string v2, "android.settings.INPUT_METHOD_SETTINGS"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1651
    const/4 v1, 0x1

    return v1

    .line 1654
    .end local v0    # "pendingIntentRecord":Lcom/android/server/am/PendingIntentRecord;
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public cleanUpApplicationRecordLocked(Lcom/android/server/am/ProcessRecord;)V
    .locals 3
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 1332
    invoke-super {p0, p1}, Lcom/android/server/am/ActivityManagerServiceStub;->cleanUpApplicationRecordLocked(Lcom/android/server/am/ProcessRecord;)V

    .line 1333
    sget-boolean v0, Lmiui/os/DeviceFeature;->SUPPORT_SPLIT_ACTIVITY:Z

    if-eqz v0, :cond_2

    .line 1334
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mSplitActivityEntryStack:Ljava/util/Map;

    const-string v1, "ActivityManagerServiceImpl"

    if-eqz v0, :cond_0

    iget v2, p1, Lcom/android/server/am/ProcessRecord;->mPid:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1335
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Split main entrance killed, clear sub activities for "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mPid "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p1, Lcom/android/server/am/ProcessRecord;->mPid:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1336
    iget v0, p1, Lcom/android/server/am/ProcessRecord;->mPid:I

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Lcom/android/server/am/ActivityManagerServiceImpl;->clearEntryStack(ILandroid/os/IBinder;)V

    .line 1337
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mSplitActivityEntryStack:Ljava/util/Map;

    iget v2, p1, Lcom/android/server/am/ProcessRecord;->mPid:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1339
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mCurrentSplitIntent:Ljava/util/Map;

    if-eqz v0, :cond_1

    .line 1340
    iget v2, p1, Lcom/android/server/am/ProcessRecord;->mPid:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1342
    :cond_1
    const-string v0, "Cleaning tablet split stack."

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1344
    :cond_2
    return-void
.end method

.method public clearEntryStack(ILandroid/os/IBinder;)V
    .locals 8
    .param p1, "pid"    # I
    .param p2, "selfToken"    # Landroid/os/IBinder;

    .line 1377
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->splitEntryStackLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1378
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mSplitActivityEntryStack:Ljava/util/Map;

    if-eqz v1, :cond_6

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_2

    .line 1381
    :cond_0
    iget-object v1, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mSplitActivityEntryStack:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Stack;

    .line 1382
    .local v1, "stack":Ljava/util/Stack;, "Ljava/util/Stack<Landroid/os/IBinder;>;"
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/util/Stack;->empty()Z

    move-result v2

    if-nez v2, :cond_5

    if-eqz p2, :cond_1

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    goto :goto_1

    .line 1385
    :cond_1
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 1386
    .local v2, "ident":J
    :goto_0
    invoke-virtual {v1}, Ljava/util/Stack;->empty()Z

    move-result v4

    if-nez v4, :cond_3

    .line 1387
    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/IBinder;

    .line 1388
    .local v4, "token":Landroid/os/IBinder;
    const-string v5, "ActivityManagerServiceImpl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SplitEntryStack: pop stack, size="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/util/Stack;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " pid="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1389
    if-eqz v4, :cond_2

    invoke-virtual {v4, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1390
    iget-object v5, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v5, v4, v7, v6, v7}, Lcom/android/server/am/ActivityManagerService;->finishActivity(Landroid/os/IBinder;ILandroid/content/Intent;I)Z

    .line 1392
    .end local v4    # "token":Landroid/os/IBinder;
    :cond_2
    goto :goto_0

    .line 1395
    :cond_3
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1396
    if-eqz p2, :cond_4

    .line 1397
    invoke-virtual {v1, p2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1398
    const-string v4, "ActivityManagerServiceImpl"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SplitEntryStack: push self to stack, size="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/util/Stack;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " pid="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1400
    .end local v1    # "stack":Ljava/util/Stack;, "Ljava/util/Stack<Landroid/os/IBinder;>;"
    .end local v2    # "ident":J
    :cond_4
    monitor-exit v0

    .line 1401
    return-void

    .line 1383
    .restart local v1    # "stack":Ljava/util/Stack;, "Ljava/util/Stack<Landroid/os/IBinder;>;"
    :cond_5
    :goto_1
    monitor-exit v0

    return-void

    .line 1379
    .end local v1    # "stack":Ljava/util/Stack;, "Ljava/util/Stack<Landroid/os/IBinder;>;"
    :cond_6
    :goto_2
    monitor-exit v0

    return-void

    .line 1400
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public doBoostEx(Lcom/android/server/am/ProcessRecord;J)Z
    .locals 3
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "beginTime"    # J

    .line 955
    const/4 v0, 0x0

    .line 957
    .local v0, "boostNeededNext":Z
    invoke-static {p1, p2, p3}, Lcom/android/server/am/ActivityManagerServiceImpl;->doTopAppBoost(Lcom/android/server/am/ProcessRecord;J)Z

    move-result v1

    or-int/2addr v0, v1

    .line 959
    const-string v1, "com.tencent.mm"

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 960
    invoke-static {p1, p2, p3}, Lcom/android/server/am/ActivityManagerServiceImpl;->doForegroundBoost(Lcom/android/server/am/ProcessRecord;J)Z

    move-result v1

    or-int/2addr v0, v1

    .line 962
    :cond_0
    return v0
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;IZZLjava/lang/String;)Z
    .locals 1
    .param p1, "cmd"    # Ljava/lang/String;
    .param p2, "fd"    # Ljava/io/FileDescriptor;
    .param p3, "pw"    # Ljava/io/PrintWriter;
    .param p4, "args"    # [Ljava/lang/String;
    .param p5, "opti"    # I
    .param p6, "dumpAll"    # Z
    .param p7, "dumpClient"    # Z
    .param p8, "dumpPackage"    # Ljava/lang/String;

    .line 1659
    const-string v0, "logging"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1660
    invoke-virtual {p0, p3}, Lcom/android/server/am/ActivityManagerServiceImpl;->dumpLogText(Ljava/io/PrintWriter;)V

    .line 1661
    const/4 v0, 0x1

    return v0

    .line 1662
    :cond_0
    const-string v0, "app-logging"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1663
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/android/server/am/ActivityManagerServiceImpl;->dumpAppLogText(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;I)Z

    move-result v0

    return v0

    .line 1665
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public dumpAppStackTraces(Ljava/util/ArrayList;Landroid/util/SparseArray;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 15
    .param p4, "subject"    # Ljava/lang/String;
    .param p5, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;",
            "Landroid/util/SparseArray<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/io/File;"
        }
    .end annotation

    .line 1060
    .local p1, "firstPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .local p2, "lastPids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/Boolean;>;"
    .local p3, "nativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    move-object/from16 v1, p4

    const/4 v2, 0x0

    .line 1062
    .local v2, "extraPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dumpStackTraces pids="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object/from16 v3, p2

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " nativepids="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object/from16 v4, p3

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v5, "MIUIScout App"

    invoke-static {v5, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1064
    new-instance v0, Ljava/io/File;

    move-object/from16 v6, p5

    invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object v7, v0

    .line 1067
    .local v7, "tracesFile":Ljava/io/File;
    :try_start_0
    invoke-virtual {v7}, Ljava/io/File;->createNewFile()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1068
    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    const/16 v8, 0x180

    const/4 v9, -0x1

    invoke-static {v0, v8, v9, v9}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1073
    :cond_0
    nop

    .line 1074
    if-eqz v1, :cond_1

    .line 1075
    :try_start_1
    new-instance v0, Ljava/io/FileOutputStream;

    const/4 v8, 0x1

    invoke-direct {v0, v7, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-object v8, v0

    .line 1076
    .local v8, "fos":Ljava/io/FileOutputStream;
    :try_start_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Subject: "

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, "\n"

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1077
    .local v0, "header":Ljava/lang/String;
    sget-object v9, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v9}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1078
    .end local v0    # "header":Ljava/lang/String;
    :try_start_3
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 1080
    .end local v8    # "fos":Ljava/io/FileOutputStream;
    goto :goto_1

    .line 1075
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    :catchall_0
    move-exception v0

    move-object v9, v0

    :try_start_4
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v0

    move-object v10, v0

    :try_start_5
    invoke-virtual {v9, v10}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v2    # "extraPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v7    # "tracesFile":Ljava/io/File;
    .end local p0    # "this":Lcom/android/server/am/ActivityManagerServiceImpl;
    .end local p1    # "firstPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local p2    # "lastPids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/Boolean;>;"
    .end local p3    # "nativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local p4    # "subject":Ljava/lang/String;
    .end local p5    # "path":Ljava/lang/String;
    :goto_0
    throw v9
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    .line 1078
    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "extraPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v7    # "tracesFile":Ljava/io/File;
    .restart local p0    # "this":Lcom/android/server/am/ActivityManagerServiceImpl;
    .restart local p1    # "firstPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local p2    # "lastPids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/Boolean;>;"
    .restart local p3    # "nativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local p4    # "subject":Ljava/lang/String;
    .restart local p5    # "path":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1079
    .local v0, "e":Ljava/io/IOException;
    const-string v8, "Exception writing subject to scout dump file:"

    invoke-static {v5, v8, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1083
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    :goto_1
    nop

    .line 1084
    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-static/range {p3 .. p3}, Ljava/util/concurrent/CompletableFuture;->completedFuture(Ljava/lang/Object;)Ljava/util/concurrent/CompletableFuture;

    move-result-object v11

    invoke-static {v2}, Ljava/util/concurrent/CompletableFuture;->completedFuture(Ljava/lang/Object;)Ljava/util/concurrent/CompletableFuture;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    .line 1083
    move-object/from16 v10, p1

    invoke-static/range {v9 .. v14}, Lcom/android/server/am/StackTracesDumpHelper;->dumpStackTraces(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Lcom/android/internal/os/anr/AnrLatencyTracker;)J

    .line 1086
    return-object v7

    .line 1070
    :catch_1
    move-exception v0

    .line 1071
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v8, "Exception creating scout dump file:"

    invoke-static {v5, v8, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1072
    const/4 v5, 0x0

    return-object v5
.end method

.method public dumpLogText(Ljava/io/PrintWriter;)V
    .locals 5
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 1719
    const-string v0, "ACTIVITY MANAGER LOGGING (dumpsys activity logging)"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1720
    new-instance v0, Landroid/util/Pair;

    const-string v1, ""

    invoke-direct {v0, v1, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1721
    .local v0, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    sget-boolean v3, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_BACKGROUND_CHECK:Z

    const-string v4, "DEBUG_BACKGROUND_CHECK"

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 1722
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    sget-boolean v3, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_BROADCAST:Z

    const-string v4, "DEBUG_BROADCAST"

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 1723
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    sget-boolean v3, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_BROADCAST_BACKGROUND:Z

    const-string v4, "DEBUG_BROADCAST_BACKGROUND"

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 1724
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    sget-boolean v3, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_BROADCAST_LIGHT:Z

    const-string v4, "DEBUG_BROADCAST_LIGHT"

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 1725
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    sget-boolean v3, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_BROADCAST_DEFERRAL:Z

    const-string v4, "DEBUG_BROADCAST_DEFERRAL"

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 1726
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    sget-boolean v3, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_PROVIDER:Z

    const-string v4, "DEBUG_PROVIDER"

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 1727
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    sget-boolean v3, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_SERVICE:Z

    const-string v4, "DEBUG_SERVICE"

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 1728
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    sget-boolean v3, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_FOREGROUND_SERVICE:Z

    const-string v4, "DEBUG_FOREGROUND_SERVICE"

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 1729
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    sget-boolean v3, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_SERVICE_EXECUTING:Z

    const-string v4, "DEBUG_SERVICE_EXECUTING"

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 1730
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    sget-boolean v3, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_ALLOWLISTS:Z

    const-string v4, "DEBUG_ALLOWLISTS"

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 1731
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    sget-boolean v3, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_RECENTS:Z

    const-string v4, "DEBUG_RECENTS"

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 1732
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    sget-boolean v3, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_RECENTS_TRIM_TASKS:Z

    const-string v4, "DEBUG_RECENTS_TRIM_TASKS"

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 1733
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    sget-boolean v3, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_ROOT_TASK:Z

    const-string v4, "DEBUG_ROOT_TASK"

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 1734
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    sget-boolean v3, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_SWITCH:Z

    const-string v4, "DEBUG_SWITCH"

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 1735
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    sget-boolean v3, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_TRANSITION:Z

    const-string v4, "DEBUG_TRANSITION"

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 1736
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    sget-boolean v3, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_VISIBILITY:Z

    const-string v4, "DEBUG_VISIBILITY"

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 1737
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    sget-boolean v3, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_APP:Z

    const-string v4, "DEBUG_APP"

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 1738
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    sget-boolean v3, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_IDLE:Z

    const-string v4, "DEBUG_IDLE"

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 1739
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    sget-boolean v3, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_RELEASE:Z

    const-string v4, "DEBUG_RELEASE"

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 1740
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    sget-boolean v3, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_USER_LEAVING:Z

    const-string v4, "DEBUG_USER_LEAVING"

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 1741
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    sget-boolean v3, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_PERMISSIONS_REVIEW:Z

    const-string v4, "DEBUG_PERMISSIONS_REVIEW"

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 1742
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    sget-boolean v3, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_RESULTS:Z

    const-string v4, "DEBUG_RESULTS"

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 1743
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    sget-boolean v3, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_ACTIVITY_STARTS:Z

    const-string v4, "DEBUG_ACTIVITY_STARTS"

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 1744
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    sget-boolean v3, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_CLEANUP:Z

    const-string v4, "DEBUG_CLEANUP"

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 1745
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    sget-boolean v3, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_METRICS:Z

    const-string v4, "DEBUG_METRICS"

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 1746
    const-string v1, "Enabled log groups:"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1747
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1748
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 1749
    const-string v1, "Disabled log groups:"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1750
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1751
    return-void
.end method

.method public dumpMiuiJavaTrace(I)V
    .locals 3
    .param p1, "pid"    # I

    .line 1213
    invoke-static {p1}, Landroid/os/Process;->getThreadGroupLeader(I)I

    move-result v0

    if-ne v0, p1, :cond_0

    .line 1214
    const-string v0, "MIUI ANR"

    invoke-static {v0, p1}, Lcom/android/server/ScoutHelper;->getOomAdjOfPid(Ljava/lang/String;I)I

    move-result v1

    const/16 v2, -0x3e8

    if-le v1, v2, :cond_0

    .line 1215
    const/4 v1, 0x3

    invoke-static {p1, v1}, Landroid/os/Process;->sendSignal(II)V

    .line 1216
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Scout] Send SIGNAL_QUIT to generate java stack dump. Pid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1218
    :cond_0
    return-void
.end method

.method public dumpMiuiStackTraces([I)Ljava/lang/String;
    .locals 14
    .param p1, "pids"    # [I

    .line 967
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 968
    .local v0, "callingUid":I
    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v1

    const/16 v2, 0x3e8

    if-ne v1, v2, :cond_6

    .line 972
    array-length v1, p1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ge v1, v3, :cond_0

    .line 973
    return-object v2

    .line 975
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    const/4 v4, 0x3

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 976
    .local v1, "javapids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(I)V

    move-object v4, v5

    .line 977
    .local v4, "nativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    array-length v6, p1

    if-ge v5, v6, :cond_4

    .line 978
    const-string v6, "ActivityManagerServiceImpl"

    aget v7, p1, v5

    invoke-static {v6, v7}, Lcom/android/server/ScoutHelper;->getOomAdjOfPid(Ljava/lang/String;I)I

    move-result v6

    .line 979
    .local v6, "adj":I
    invoke-static {v6}, Lcom/android/server/ScoutHelper;->checkIsJavaOrNativeProcess(I)I

    move-result v7

    .line 980
    .local v7, "isJavaOrNativeProcess":I
    if-nez v7, :cond_1

    .line 981
    goto :goto_1

    .line 982
    :cond_1
    if-ne v7, v3, :cond_2

    .line 983
    aget v8, p1, v5

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 984
    :cond_2
    const/4 v8, 0x2

    if-ne v7, v8, :cond_3

    .line 985
    aget v8, p1, v5

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 977
    .end local v6    # "adj":I
    .end local v7    # "isJavaOrNativeProcess":I
    :cond_3
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 988
    .end local v5    # "i":I
    :cond_4
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v4}, Ljava/util/concurrent/CompletableFuture;->completedFuture(Ljava/lang/Object;)Ljava/util/concurrent/CompletableFuture;

    move-result-object v8

    const/4 v9, 0x0

    const-string v10, "App Scout Exception"

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v5, v1

    invoke-static/range {v5 .. v13}, Lcom/android/server/am/StackTracesDumpHelper;->dumpStackTraces(Ljava/util/ArrayList;Lcom/android/internal/os/ProcessCpuTracker;Landroid/util/SparseBooleanArray;Ljava/util/concurrent/Future;Ljava/io/StringWriter;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/Executor;Lcom/android/internal/os/anr/AnrLatencyTracker;)Ljava/io/File;

    move-result-object v3

    .line 990
    .local v3, "mTraceFile":Ljava/io/File;
    if-eqz v3, :cond_5

    .line 991
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 993
    :cond_5
    return-object v2

    .line 969
    .end local v1    # "javapids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v3    # "mTraceFile":Ljava/io/File;
    .end local v4    # "nativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_6
    new-instance v1, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Only the system process can call dumpMiuiStackTraces, received request from uid: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public dumpMiuiStackTracesForCmdlines([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 17
    .param p1, "cmdlines"    # [Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "subject"    # Ljava/lang/String;

    .line 999
    move-object/from16 v1, p1

    const-string v2, "ActivityManagerServiceImpl"

    if-eqz v1, :cond_a

    array-length v0, v1

    const/4 v3, 0x1

    if-lt v0, v3, :cond_a

    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object/from16 v7, p2

    move-object/from16 v10, p3

    goto/16 :goto_7

    .line 1004
    :cond_0
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v4

    .line 1005
    .local v4, "callingUid":I
    invoke-static {v4}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v0

    const/16 v5, 0x3e8

    const-string v6, "MIUIScout App"

    if-eq v0, v5, :cond_1

    .line 1006
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Only system process can call dumpMiuiStackTracesForCmdlines, received request from uid:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1008
    return-void

    .line 1012
    :cond_1
    invoke-static/range {p1 .. p1}, Landroid/os/Process;->getPidsForCommands([Ljava/lang/String;)[I

    move-result-object v5

    .line 1013
    .local v5, "pids":[I
    if-eqz v5, :cond_9

    array-length v0, v5

    if-ge v0, v3, :cond_2

    move-object/from16 v7, p2

    move-object/from16 v10, p3

    goto/16 :goto_6

    .line 1019
    :cond_2
    new-instance v0, Ljava/io/File;

    move-object/from16 v7, p2

    invoke-direct {v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object v8, v0

    .line 1021
    .local v8, "tracesFile":Ljava/io/File;
    :try_start_0
    invoke-virtual {v8}, Ljava/io/File;->createNewFile()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1022
    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    const/16 v9, 0x180

    const/4 v10, -0x1

    invoke-static {v0, v9, v10, v10}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 1027
    :cond_3
    nop

    .line 1030
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1031
    :try_start_1
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v8, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v9, v0

    .line 1032
    .local v9, "fos":Ljava/io/FileOutputStream;
    :try_start_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Subject: "

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object/from16 v10, p3

    :try_start_3
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v11, "\n"

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1033
    .local v0, "header":Ljava/lang/String;
    sget-object v11, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v11}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1034
    .end local v0    # "header":Ljava/lang/String;
    :try_start_4
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 1036
    .end local v9    # "fos":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 1031
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    :catchall_0
    move-exception v0

    goto :goto_0

    :catchall_1
    move-exception v0

    move-object/from16 v10, p3

    :goto_0
    move-object v11, v0

    :try_start_5
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto :goto_1

    :catchall_2
    move-exception v0

    move-object v12, v0

    :try_start_6
    invoke-virtual {v11, v12}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v4    # "callingUid":I
    .end local v5    # "pids":[I
    .end local v8    # "tracesFile":Ljava/io/File;
    .end local p0    # "this":Lcom/android/server/am/ActivityManagerServiceImpl;
    .end local p1    # "cmdlines":[Ljava/lang/String;
    .end local p2    # "path":Ljava/lang/String;
    .end local p3    # "subject":Ljava/lang/String;
    :goto_1
    throw v11
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0

    .line 1034
    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "callingUid":I
    .restart local v5    # "pids":[I
    .restart local v8    # "tracesFile":Ljava/io/File;
    .restart local p0    # "this":Lcom/android/server/am/ActivityManagerServiceImpl;
    .restart local p1    # "cmdlines":[Ljava/lang/String;
    .restart local p2    # "path":Ljava/lang/String;
    .restart local p3    # "subject":Ljava/lang/String;
    :catch_0
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v0

    move-object/from16 v10, p3

    .line 1035
    .local v0, "e":Ljava/io/IOException;
    :goto_2
    const-string v9, "Exception writing subject to scout dump file:"

    invoke-static {v6, v9, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    .line 1030
    .end local v0    # "e":Ljava/io/IOException;
    :cond_4
    move-object/from16 v10, p3

    .line 1040
    :goto_3
    new-instance v0, Ljava/util/ArrayList;

    const/4 v6, 0x4

    invoke-direct {v0, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 1041
    .local v0, "javaPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9, v6}, Ljava/util/ArrayList;-><init>(I)V

    move-object v6, v9

    .line 1042
    .local v6, "nativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_4
    array-length v11, v5

    if-ge v9, v11, :cond_8

    .line 1043
    aget v11, v5, v9

    invoke-static {v2, v11}, Lcom/android/server/ScoutHelper;->getOomAdjOfPid(Ljava/lang/String;I)I

    move-result v11

    .line 1044
    .local v11, "adj":I
    invoke-static {v11}, Lcom/android/server/ScoutHelper;->checkIsJavaOrNativeProcess(I)I

    move-result v12

    .line 1045
    .local v12, "isJavaOrNativeProcess":I
    if-nez v12, :cond_5

    .line 1046
    goto :goto_5

    .line 1047
    :cond_5
    if-ne v12, v3, :cond_6

    .line 1048
    aget v13, v5, v9

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 1049
    :cond_6
    const/4 v13, 0x2

    if-ne v12, v13, :cond_7

    .line 1050
    aget v13, v5, v9

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v6, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1042
    .end local v11    # "adj":I
    .end local v12    # "isJavaOrNativeProcess":I
    :cond_7
    :goto_5
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    .line 1053
    .end local v9    # "i":I
    :cond_8
    nop

    .line 1054
    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    .line 1055
    invoke-static {v6}, Ljava/util/concurrent/CompletableFuture;->completedFuture(Ljava/lang/Object;)Ljava/util/concurrent/CompletableFuture;

    move-result-object v13

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    .line 1053
    move-object v12, v0

    invoke-static/range {v11 .. v16}, Lcom/android/server/am/StackTracesDumpHelper;->dumpStackTraces(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Lcom/android/internal/os/anr/AnrLatencyTracker;)J

    .line 1057
    return-void

    .line 1024
    .end local v0    # "javaPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v6    # "nativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :catch_2
    move-exception v0

    move-object/from16 v10, p3

    .line 1025
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "Exception creating scout dump file:"

    invoke-static {v6, v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1026
    return-void

    .line 1013
    .end local v0    # "e":Ljava/io/IOException;
    .end local v8    # "tracesFile":Ljava/io/File;
    :cond_9
    move-object/from16 v7, p2

    move-object/from16 v10, p3

    .line 1014
    :goto_6
    const-string v0, "dumpStackTraces failed, no pid found, "

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1015
    return-void

    .line 999
    .end local v4    # "callingUid":I
    .end local v5    # "pids":[I
    :cond_a
    move-object/from16 v7, p2

    move-object/from16 v10, p3

    .line 1000
    :goto_7
    const-string v0, "dumpStackTraces failed, Invalidate param!"

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1001
    return-void
.end method

.method public dumpOneProcessTraces(ILjava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 11
    .param p1, "pid"    # I
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "subject"    # Ljava/lang/String;

    .line 1091
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1092
    .local v0, "firstPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object v7, v1

    .line 1093
    .local v7, "nativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-string v1, "ActivityManagerServiceImpl"

    invoke-static {v1, p1}, Lcom/android/server/ScoutHelper;->getOomAdjOfPid(Ljava/lang/String;I)I

    move-result v8

    .line 1094
    .local v8, "adj":I
    invoke-static {v8}, Lcom/android/server/ScoutHelper;->checkIsJavaOrNativeProcess(I)I

    move-result v9

    .line 1095
    .local v9, "isJavaOrNativeProcess":I
    const/4 v1, 0x0

    const-string v2, "MIUIScout App"

    const/4 v3, 0x1

    if-ne v9, v3, :cond_0

    .line 1096
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1097
    :cond_0
    const/4 v4, 0x2

    if-ne v9, v4, :cond_3

    .line 1098
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1104
    :goto_0
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object v10, v4

    .line 1106
    .local v10, "traceFile":Ljava/io/File;
    :try_start_0
    invoke-virtual {v10}, Ljava/io/File;->createNewFile()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1107
    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x180

    const/4 v6, -0x1

    invoke-static {v4, v5, v6, v6}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1112
    :cond_1
    nop

    .line 1114
    if-eqz p3, :cond_2

    .line 1115
    :try_start_1
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v10, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1116
    .local v1, "fos":Ljava/io/FileOutputStream;
    :try_start_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Subject: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1117
    .local v3, "header":Ljava/lang/String;
    sget-object v4, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1118
    .end local v3    # "header":Ljava/lang/String;
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 1120
    .end local v1    # "fos":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 1115
    .restart local v1    # "fos":Ljava/io/FileOutputStream;
    :catchall_0
    move-exception v3

    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v4

    :try_start_5
    invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "firstPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v7    # "nativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v8    # "adj":I
    .end local v9    # "isJavaOrNativeProcess":I
    .end local v10    # "traceFile":Ljava/io/File;
    .end local p0    # "this":Lcom/android/server/am/ActivityManagerServiceImpl;
    .end local p1    # "pid":I
    .end local p2    # "path":Ljava/lang/String;
    .end local p3    # "subject":Ljava/lang/String;
    :goto_1
    throw v3
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    .line 1118
    .end local v1    # "fos":Ljava/io/FileOutputStream;
    .restart local v0    # "firstPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v7    # "nativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v8    # "adj":I
    .restart local v9    # "isJavaOrNativeProcess":I
    .restart local v10    # "traceFile":Ljava/io/File;
    .restart local p0    # "this":Lcom/android/server/am/ActivityManagerServiceImpl;
    .restart local p1    # "pid":I
    .restart local p2    # "path":Ljava/lang/String;
    .restart local p3    # "subject":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 1119
    .local v1, "e":Ljava/io/IOException;
    const-string v3, "Exception writing subject to scout dump file:"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1123
    .end local v1    # "e":Ljava/io/IOException;
    :cond_2
    :goto_2
    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v7}, Ljava/util/concurrent/CompletableFuture;->completedFuture(Ljava/lang/Object;)Ljava/util/concurrent/CompletableFuture;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, v0

    invoke-static/range {v1 .. v6}, Lcom/android/server/am/StackTracesDumpHelper;->dumpStackTraces(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Lcom/android/internal/os/anr/AnrLatencyTracker;)J

    .line 1125
    return-object v10

    .line 1109
    :catch_1
    move-exception v3

    .line 1110
    .local v3, "e":Ljava/io/IOException;
    const-string v4, "Exception creating scout dump file:"

    invoke-static {v2, v4, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1111
    return-object v1

    .line 1100
    .end local v3    # "e":Ljava/io/IOException;
    .end local v10    # "traceFile":Ljava/io/File;
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "can not distinguish for this process\'s adj"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1101
    return-object v1
.end method

.method public dumpSystemTraces(Ljava/lang/String;)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;

    .line 1130
    sget-object v0, Lcom/android/server/am/ActivityManagerServiceImpl;->requestDumpTraceCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    .line 1132
    sget-object v0, Lcom/android/server/am/ActivityManagerServiceImpl;->requestDumpTraceCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-lez v0, :cond_2

    sget-object v0, Lcom/android/server/am/ActivityManagerServiceImpl;->dumpFlag:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1133
    sget-object v0, Lcom/android/server/am/ActivityManagerServiceImpl;->requestDumpTraceCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndDecrement()I

    .line 1134
    sget-object v0, Lcom/android/server/am/ActivityManagerServiceImpl;->dumpFlag:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1135
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    .line 1137
    .local v0, "executor":Ljava/util/concurrent/ExecutorService;
    :try_start_0
    new-instance v1, Lcom/android/server/am/ActivityManagerServiceImpl$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1}, Lcom/android/server/am/ActivityManagerServiceImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/am/ActivityManagerServiceImpl;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1199
    const-string v1, "MIUIScout App"

    const-string v2, "dumpSystemTraces finally shutdown."

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1200
    if-eqz v0, :cond_0

    :goto_0
    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    goto :goto_1

    .line 1199
    :catchall_0
    move-exception v1

    goto :goto_2

    .line 1196
    :catch_0
    move-exception v1

    .line 1197
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v2, "MIUIScout App"

    const-string v3, "Exception occurs while dumping system scout trace file:"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1199
    nop

    .end local v1    # "e":Ljava/lang/Exception;
    const-string v1, "MIUIScout App"

    const-string v2, "dumpSystemTraces finally shutdown."

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1200
    if-eqz v0, :cond_0

    goto :goto_0

    .line 1203
    .end local v0    # "executor":Ljava/util/concurrent/ExecutorService;
    :cond_0
    :goto_1
    goto :goto_3

    .line 1199
    .restart local v0    # "executor":Ljava/util/concurrent/ExecutorService;
    :goto_2
    const-string v2, "MIUIScout App"

    const-string v3, "dumpSystemTraces finally shutdown."

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1200
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 1201
    :cond_1
    throw v1

    .line 1204
    .end local v0    # "executor":Ljava/util/concurrent/ExecutorService;
    :cond_2
    sget-object v0, Lcom/android/server/am/ActivityManagerServiceImpl;->dumpTraceRequestList:Ljava/util/ArrayList;

    monitor-enter v0

    .line 1205
    :try_start_2
    sget-object v1, Lcom/android/server/am/ActivityManagerServiceImpl;->dumpTraceRequestList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1206
    monitor-exit v0

    .line 1209
    :goto_3
    return-void

    .line 1206
    :catchall_1
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1
.end method

.method public enableAmsDebugConfig(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "config"    # Ljava/lang/String;
    .param p2, "enable"    # Z

    .line 1498
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "enableAMSDebugConfig, config=:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", enable=:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ActivityManagerServiceImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1499
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    goto/16 :goto_0

    :sswitch_0
    const-string v0, "DEBUG_OOM_ADJ"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc

    goto/16 :goto_1

    :sswitch_1
    const-string v0, "DEBUG_BROADCAST_DEFERRAL"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    goto/16 :goto_1

    :sswitch_2
    const-string v0, "DEBUG_POWER"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xe

    goto/16 :goto_1

    :sswitch_3
    const-string v0, "DEBUG_PROVIDER"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x12

    goto/16 :goto_1

    :sswitch_4
    const-string v0, "DEBUG_SWITCH"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x1e

    goto/16 :goto_1

    :sswitch_5
    const-string v0, "DEBUG_USAGE_STATS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x18

    goto/16 :goto_1

    :sswitch_6
    const-string v0, "DEBUG_BACKGROUND_CHECK"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto/16 :goto_1

    :sswitch_7
    const-string v0, "DEBUG_FOREGROUND_SERVICE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x15

    goto/16 :goto_1

    :sswitch_8
    const-string v0, "DEBUG_MU"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xa

    goto/16 :goto_1

    :sswitch_9
    const-string v0, "DEBUG_POWER_QUICK"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xf

    goto/16 :goto_1

    :sswitch_a
    const-string v0, "DEBUG_PROCESS_OBSERVERS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x10

    goto/16 :goto_1

    :sswitch_b
    const-string v0, "DEBUG_BACKUP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto/16 :goto_1

    :sswitch_c
    const-string v0, "DEBUG_SERVICE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x14

    goto/16 :goto_1

    :sswitch_d
    const-string v0, "DEBUG_ROOT_TASK"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x1d

    goto/16 :goto_1

    :sswitch_e
    const-string v0, "DEBUG_NETWORK"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xb

    goto/16 :goto_1

    :sswitch_f
    const-string v0, "DEBUG_ACTIVITY_STARTS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x27

    goto/16 :goto_1

    :sswitch_10
    const-string v0, "DEBUG_BROADCAST_BACKGROUND"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    goto/16 :goto_1

    :sswitch_11
    const-string v0, "DEBUG_PROCESSES"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x11

    goto/16 :goto_1

    :sswitch_12
    const-string v0, "DEBUG_BROADCAST_LIGHT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    goto/16 :goto_1

    :sswitch_13
    const-string v0, "DEBUG_RESULTS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x26

    goto/16 :goto_1

    :sswitch_14
    const-string v0, "DEBUG_RELEASE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x23

    goto/16 :goto_1

    :sswitch_15
    const-string v0, "DEBUG_RECENTS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x1b

    goto/16 :goto_1

    :sswitch_16
    const-string v0, "DEBUG_PERMISSIONS_REVIEW_ATMS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x25

    goto/16 :goto_1

    :sswitch_17
    const-string v0, "DEBUG_BROADCAST"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto/16 :goto_1

    :sswitch_18
    const-string v0, "DEBUG_USER_LEAVING"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x24

    goto/16 :goto_1

    :sswitch_19
    const-string v0, "DEBUG_RECENTS_TRIM_TASKS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x1c

    goto/16 :goto_1

    :sswitch_1a
    const-string v0, "DEBUG_METRICS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x29

    goto/16 :goto_1

    :sswitch_1b
    const-string v0, "DEBUG_CLEANUP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x28

    goto/16 :goto_1

    :sswitch_1c
    const-string v0, "DEBUG_PERMISSIONS_REVIEW"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x19

    goto/16 :goto_1

    :sswitch_1d
    const-string v0, "DEBUG_SERVICE_EXECUTING"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x16

    goto/16 :goto_1

    :sswitch_1e
    const-string v0, "DEBUG_COMPACTION"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x7

    goto/16 :goto_1

    :sswitch_1f
    const-string v0, "DEBUG_OOM_ADJ_REASON"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xd

    goto/16 :goto_1

    :sswitch_20
    const-string v0, "DEBUG_UID_OBSERVERS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x17

    goto/16 :goto_1

    :sswitch_21
    const-string v0, "DEBUG_ALLOWLISTS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x1a

    goto :goto_1

    :sswitch_22
    const-string v0, "DEBUG_IDLE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x22

    goto :goto_1

    :sswitch_23
    const-string v0, "DEBUG_TRANSITION"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x1f

    goto :goto_1

    :sswitch_24
    const-string v0, "DEBUG_FREEZER"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    goto :goto_1

    :sswitch_25
    const-string v0, "DEBUG_PSS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x13

    goto :goto_1

    :sswitch_26
    const-string v0, "DEBUG_LRU"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x9

    goto :goto_1

    :sswitch_27
    const-string v0, "DEBUG_APP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x21

    goto :goto_1

    :sswitch_28
    const-string v0, "DEBUG_ANR"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_29
    const-string v0, "DEBUG_VISIBILITY"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x20

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    goto/16 :goto_2

    .line 1624
    :pswitch_0
    sput-boolean p2, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_METRICS:Z

    .line 1625
    goto/16 :goto_2

    .line 1621
    :pswitch_1
    sput-boolean p2, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_CLEANUP:Z

    .line 1622
    goto/16 :goto_2

    .line 1618
    :pswitch_2
    sput-boolean p2, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_ACTIVITY_STARTS:Z

    .line 1619
    goto/16 :goto_2

    .line 1615
    :pswitch_3
    sput-boolean p2, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_RESULTS:Z

    .line 1616
    goto/16 :goto_2

    .line 1612
    :pswitch_4
    sput-boolean p2, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_PERMISSIONS_REVIEW:Z

    .line 1613
    goto/16 :goto_2

    .line 1609
    :pswitch_5
    sput-boolean p2, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_USER_LEAVING:Z

    .line 1610
    goto/16 :goto_2

    .line 1606
    :pswitch_6
    sput-boolean p2, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_RELEASE:Z

    .line 1607
    goto/16 :goto_2

    .line 1603
    :pswitch_7
    sput-boolean p2, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_IDLE:Z

    .line 1604
    goto/16 :goto_2

    .line 1600
    :pswitch_8
    sput-boolean p2, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_APP:Z

    .line 1601
    goto/16 :goto_2

    .line 1597
    :pswitch_9
    sput-boolean p2, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_VISIBILITY:Z

    .line 1598
    goto/16 :goto_2

    .line 1594
    :pswitch_a
    sput-boolean p2, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_TRANSITION:Z

    .line 1595
    goto/16 :goto_2

    .line 1591
    :pswitch_b
    sput-boolean p2, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_SWITCH:Z

    .line 1592
    goto/16 :goto_2

    .line 1588
    :pswitch_c
    sput-boolean p2, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_ROOT_TASK:Z

    .line 1589
    goto/16 :goto_2

    .line 1585
    :pswitch_d
    sput-boolean p2, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_RECENTS_TRIM_TASKS:Z

    .line 1586
    goto/16 :goto_2

    .line 1582
    :pswitch_e
    sput-boolean p2, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_RECENTS:Z

    .line 1583
    goto/16 :goto_2

    .line 1579
    :pswitch_f
    sput-boolean p2, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_ALLOWLISTS:Z

    .line 1580
    goto/16 :goto_2

    .line 1576
    :pswitch_10
    sput-boolean p2, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_PERMISSIONS_REVIEW:Z

    .line 1577
    goto :goto_2

    .line 1573
    :pswitch_11
    sput-boolean p2, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_USAGE_STATS:Z

    .line 1574
    goto :goto_2

    .line 1570
    :pswitch_12
    sput-boolean p2, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_UID_OBSERVERS:Z

    .line 1571
    goto :goto_2

    .line 1567
    :pswitch_13
    sput-boolean p2, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_SERVICE_EXECUTING:Z

    .line 1568
    goto :goto_2

    .line 1564
    :pswitch_14
    sput-boolean p2, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_FOREGROUND_SERVICE:Z

    .line 1565
    goto :goto_2

    .line 1561
    :pswitch_15
    sput-boolean p2, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_SERVICE:Z

    .line 1562
    goto :goto_2

    .line 1558
    :pswitch_16
    sput-boolean p2, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_PSS:Z

    .line 1559
    goto :goto_2

    .line 1555
    :pswitch_17
    sput-boolean p2, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_PROVIDER:Z

    .line 1556
    goto :goto_2

    .line 1552
    :pswitch_18
    sput-boolean p2, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_PROCESSES:Z

    .line 1553
    goto :goto_2

    .line 1549
    :pswitch_19
    sput-boolean p2, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_PROCESS_OBSERVERS:Z

    .line 1550
    goto :goto_2

    .line 1546
    :pswitch_1a
    sput-boolean p2, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_POWER_QUICK:Z

    .line 1547
    goto :goto_2

    .line 1543
    :pswitch_1b
    sput-boolean p2, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_POWER:Z

    .line 1544
    goto :goto_2

    .line 1540
    :pswitch_1c
    sput-boolean p2, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_OOM_ADJ_REASON:Z

    .line 1541
    goto :goto_2

    .line 1537
    :pswitch_1d
    sput-boolean p2, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_OOM_ADJ:Z

    .line 1538
    goto :goto_2

    .line 1534
    :pswitch_1e
    sput-boolean p2, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_NETWORK:Z

    .line 1535
    goto :goto_2

    .line 1531
    :pswitch_1f
    sput-boolean p2, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_MU:Z

    .line 1532
    goto :goto_2

    .line 1528
    :pswitch_20
    sput-boolean p2, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_LRU:Z

    .line 1529
    goto :goto_2

    .line 1525
    :pswitch_21
    sput-boolean p2, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_FREEZER:Z

    .line 1526
    goto :goto_2

    .line 1522
    :pswitch_22
    sput-boolean p2, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_COMPACTION:Z

    .line 1523
    goto :goto_2

    .line 1519
    :pswitch_23
    sput-boolean p2, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_BROADCAST_DEFERRAL:Z

    .line 1520
    goto :goto_2

    .line 1516
    :pswitch_24
    sput-boolean p2, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_BROADCAST_LIGHT:Z

    .line 1517
    goto :goto_2

    .line 1513
    :pswitch_25
    sput-boolean p2, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_BROADCAST_BACKGROUND:Z

    .line 1514
    goto :goto_2

    .line 1510
    :pswitch_26
    sput-boolean p2, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_BROADCAST:Z

    .line 1511
    goto :goto_2

    .line 1507
    :pswitch_27
    sput-boolean p2, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_BACKUP:Z

    .line 1508
    goto :goto_2

    .line 1504
    :pswitch_28
    sput-boolean p2, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_BACKGROUND_CHECK:Z

    .line 1505
    goto :goto_2

    .line 1501
    :pswitch_29
    sput-boolean p2, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_ANR:Z

    .line 1502
    nop

    .line 1629
    :goto_2
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7caac762 -> :sswitch_29
        -0x65a58647 -> :sswitch_28
        -0x65a5860b -> :sswitch_27
        -0x65a55c7d -> :sswitch_26
        -0x65a54d5c -> :sswitch_25
        -0x609ef971 -> :sswitch_24
        -0x5725bd1f -> :sswitch_23
        -0x4f07c5a0 -> :sswitch_22
        -0x3fa7c488 -> :sswitch_21
        -0x33a5061e -> :sswitch_20
        -0x2bd2be66 -> :sswitch_1f
        -0x2384c46f -> :sswitch_1e
        -0x13398d64 -> :sswitch_1d
        -0x105f7ae1 -> :sswitch_1c
        -0x990bb88 -> :sswitch_1b
        -0x3a95169 -> :sswitch_1a
        -0x3a5b41c -> :sswitch_19
        -0x2a29454 -> :sswitch_18
        0x31bad95 -> :sswitch_17
        0x32a6039 -> :sswitch_16
        0x3e0734c -> :sswitch_15
        0x45f15db -> :sswitch_14
        0x4c929ca -> :sswitch_13
        0x95409cc -> :sswitch_12
        0x22591031 -> :sswitch_11
        0x238c0a18 -> :sswitch_10
        0x2d964575 -> :sswitch_f
        0x313f4802 -> :sswitch_e
        0x31af5fb6 -> :sswitch_d
        0x39a1b489 -> :sswitch_c
        0x47b2f94e -> :sswitch_b
        0x48e2e741 -> :sswitch_a
        0x497569e7 -> :sswitch_9
        0x4f4d3f34 -> :sswitch_8
        0x52bdf925 -> :sswitch_7
        0x55e50723 -> :sswitch_6
        0x655e1835 -> :sswitch_5
        0x65ee3ac0 -> :sswitch_4
        0x6d83d27d -> :sswitch_3
        0x6e76dfd9 -> :sswitch_2
        0x761ef2d5 -> :sswitch_1
        0x76d6c1a9 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public enableAppDebugConfig(Lcom/android/server/am/ActivityManagerService;Ljava/lang/String;ZLjava/lang/String;I)V
    .locals 4
    .param p1, "activityManagerService"    # Lcom/android/server/am/ActivityManagerService;
    .param p2, "config"    # Ljava/lang/String;
    .param p3, "enable"    # Z
    .param p4, "processName"    # Ljava/lang/String;
    .param p5, "uid"    # I

    .line 1871
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_3

    .line 1874
    :cond_0
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 1875
    .local v0, "callerUid":I
    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_2

    if-eqz v0, :cond_2

    const/16 v1, 0x7d0

    if-ne v0, v1, :cond_1

    goto :goto_0

    .line 1888
    :cond_1
    const-string v1, "ActivityManagerServiceImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " can not enable activity thread debug config."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1876
    :cond_2
    :goto_0
    monitor-enter p0

    .line 1877
    :try_start_0
    invoke-virtual {p1, p4, p5}, Lcom/android/server/am/ActivityManagerService;->getProcessRecordLocked(Ljava/lang/String;I)Lcom/android/server/am/ProcessRecord;

    move-result-object v1

    .line 1879
    .local v1, "app":Lcom/android/server/am/ProcessRecord;
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v3, v2

    .local v3, "thread":Landroid/app/IApplicationThread;
    if-eqz v2, :cond_3

    .line 1881
    :try_start_1
    invoke-interface {v3, p2, p3}, Landroid/app/IApplicationThread;->enableDebugConfig(Ljava/lang/String;Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1884
    goto :goto_1

    .line 1882
    :catch_0
    move-exception v2

    .line 1886
    .end local v1    # "app":Lcom/android/server/am/ProcessRecord;
    .end local v3    # "thread":Landroid/app/IApplicationThread;
    :cond_3
    :goto_1
    :try_start_2
    monitor-exit p0

    .line 1890
    :goto_2
    return-void

    .line 1886
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 1872
    .end local v0    # "callerUid":I
    :cond_4
    :goto_3
    return-void
.end method

.method finishBooting()V
    .locals 5

    .line 337
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 338
    .local v0, "bootCompleteTime":J
    invoke-static {}, Lmiui/mqsas/sdk/BootEventManager;->getInstance()Lmiui/mqsas/sdk/BootEventManager;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lmiui/mqsas/sdk/BootEventManager;->setUIReady(J)V

    .line 339
    invoke-static {}, Lmiui/mqsas/sdk/BootEventManager;->getInstance()Lmiui/mqsas/sdk/BootEventManager;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lmiui/mqsas/sdk/BootEventManager;->setBootComplete(J)V

    .line 340
    invoke-static {}, Lcom/miui/server/xspace/XSpaceManagerServiceStub;->getInstance()Lcom/miui/server/xspace/XSpaceManagerServiceStub;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/miui/server/xspace/XSpaceManagerServiceStub;->init(Landroid/content/Context;)V

    .line 341
    invoke-static {}, Lcom/android/server/wm/AppResurrectionServiceStub;->getInstance()Lcom/android/server/wm/AppResurrectionServiceStub;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/android/server/wm/AppResurrectionServiceStub;->init(Landroid/content/Context;)V

    .line 342
    iget-object v2, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lmiui/drm/DrmBroadcast;->getInstance(Landroid/content/Context;)Lmiui/drm/DrmBroadcast;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/drm/DrmBroadcast;->broadcast()V

    .line 344
    new-instance v2, Landroid/content/Intent;

    const-string v3, "miui.intent.action.FINISH_BOOTING"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 345
    .local v2, "intent":Landroid/content/Intent;
    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 346
    iget-object v3, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mContext:Landroid/content/Context;

    sget-object v4, Landroid/os/UserHandle;->SYSTEM:Landroid/os/UserHandle;

    invoke-virtual {v3, v2, v4}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 347
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getInstance()Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->updateResizeBlackList(Landroid/content/Context;)V

    .line 348
    sget-boolean v3, Lmiui/mqsas/scout/ScoutUtils;->REBOOT_COREDUMP:Z

    if-nez v3, :cond_0

    invoke-static {}, Lmiui/mqsas/scout/ScoutUtils;->isLibraryTest()Z

    .line 351
    :cond_0
    iget-object v3, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mStabilityLocalServiceInternal:Lcom/miui/server/stability/StabilityLocalServiceInternal;

    if-eqz v3, :cond_1

    .line 352
    iget-object v4, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-interface {v3, v4}, Lcom/miui/server/stability/StabilityLocalServiceInternal;->initContext(Landroid/content/Context;)V

    .line 353
    iget-object v3, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mStabilityLocalServiceInternal:Lcom/miui/server/stability/StabilityLocalServiceInternal;

    invoke-interface {v3}, Lcom/miui/server/stability/StabilityLocalServiceInternal;->startMemoryMonitor()V

    .line 356
    :cond_1
    invoke-static {}, Lcom/android/server/am/SystemPressureControllerStub;->getInstance()Lcom/android/server/am/SystemPressureControllerStub;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/server/am/SystemPressureControllerStub;->finishBooting()V

    .line 357
    return-void
.end method

.method public finishBootingAsUser(I)V
    .locals 2
    .param p1, "userId"    # I

    .line 730
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getInstance()Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    move-result-object v0

    const-string v1, "finishBooting"

    invoke-virtual {v0, p1, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->restartSubScreenUiIfNeeded(ILjava/lang/String;)V

    .line 731
    return-void
.end method

.method public getAppStartMode(IIILjava/lang/String;)I
    .locals 16
    .param p1, "uid"    # I
    .param p2, "defMode"    # I
    .param p3, "callingPid"    # I
    .param p4, "callingPackage"    # Ljava/lang/String;

    .line 385
    move-object/from16 v1, p0

    move-object/from16 v2, p4

    const-string v0, "com.xiaomi.xmsf"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    const/4 v3, 0x0

    if-nez v0, :cond_2

    const-string v0, "com.miui.voiceassist"

    .line 386
    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "com.miui.carlink"

    .line 387
    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "com.baidu.carlife.xiaomi"

    .line 388
    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v1, Lcom/android/server/am/ActivityManagerServiceImpl;->mContext:Landroid/content/Context;

    .line 390
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v4, "ucar_casting_state"

    invoke-static {v0, v4, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v4, 0x1

    if-eq v4, v0, :cond_2

    :cond_0
    const-string v0, "com.miui.notification"

    .line 391
    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "com.xiaomi.bluetooth"

    .line 392
    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 409
    :cond_1
    return p2

    .line 393
    :cond_2
    :goto_0
    iget-object v0, v1, Lcom/android/server/am/ActivityManagerServiceImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    move/from16 v4, p1

    invoke-virtual {v0, v4}, Lcom/android/server/am/ProcessList;->getUidRecordLOSP(I)Lcom/android/server/am/UidRecord;

    move-result-object v5

    .line 394
    .local v5, "record":Lcom/android/server/am/UidRecord;
    if-eqz v5, :cond_4

    invoke-virtual {v5}, Lcom/android/server/am/UidRecord;->isIdle()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v5}, Lcom/android/server/am/UidRecord;->isCurAllowListed()Z

    move-result v0

    if-nez v0, :cond_4

    .line 396
    iget-object v0, v1, Lcom/android/server/am/ActivityManagerServiceImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    iget-object v6, v0, Lcom/android/server/am/ActivityManagerService;->mPidsSelfLocked:Lcom/android/server/am/ActivityManagerService$PidMap;

    monitor-enter v6

    .line 397
    :try_start_0
    iget-object v0, v1, Lcom/android/server/am/ActivityManagerServiceImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mPidsSelfLocked:Lcom/android/server/am/ActivityManagerService$PidMap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move/from16 v7, p3

    :try_start_1
    invoke-virtual {v0, v7}, Lcom/android/server/am/ActivityManagerService$PidMap;->get(I)Lcom/android/server/am/ProcessRecord;

    move-result-object v0

    .line 398
    .local v0, "proc":Lcom/android/server/am/ProcessRecord;
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 399
    if-eqz v0, :cond_3

    iget v6, v0, Lcom/android/server/am/ProcessRecord;->uid:I

    goto :goto_1

    :cond_3
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v6

    :goto_1
    move v15, v6

    .line 400
    .local v15, "callingUid":I
    iget-object v8, v1, Lcom/android/server/am/ActivityManagerServiceImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    invoke-virtual {v5}, Lcom/android/server/am/UidRecord;->getUid()I

    move-result v9

    const-wide/32 v10, 0xea60

    const/16 v12, 0x65

    const-string v13, "push-service-launch"

    const/4 v14, 0x0

    invoke-virtual/range {v8 .. v15}, Lcom/android/server/am/ActivityManagerService;->tempAllowlistUidLocked(IJILjava/lang/String;II)V

    goto :goto_3

    .line 398
    .end local v0    # "proc":Lcom/android/server/am/ProcessRecord;
    .end local v15    # "callingUid":I
    :catchall_0
    move-exception v0

    move/from16 v7, p3

    :goto_2
    :try_start_2
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    .line 394
    :cond_4
    move/from16 v7, p3

    .line 407
    :goto_3
    return v3
.end method

.method public getBroadcastProcessQueue(Ljava/lang/String;I)Lcom/android/server/am/BroadcastProcessQueue;
    .locals 2
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 374
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    iget-boolean v0, v0, Lcom/android/server/am/ActivityManagerService;->mEnableModernQueue:Z

    if-eqz v0, :cond_0

    .line 375
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/am/ActivityManagerService;->broadcastQueueForFlags(I)Lcom/android/server/am/BroadcastQueue;

    move-result-object v0

    .line 376
    .local v0, "bc":Lcom/android/server/am/BroadcastQueue;
    invoke-virtual {v0, p1, p2}, Lcom/android/server/am/BroadcastQueue;->getBroadcastProcessQueue(Ljava/lang/String;I)Lcom/android/server/am/BroadcastProcessQueue;

    move-result-object v1

    return-object v1

    .line 378
    .end local v0    # "bc":Lcom/android/server/am/BroadcastQueue;
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDefaultMaxCachedProcesses()I
    .locals 6

    .line 1910
    const-string v0, "persist.sys.spc.enabled"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1911
    const/16 v0, 0x3e8

    return v0

    .line 1913
    :cond_0
    const-wide/32 v0, 0x40000000

    .line 1914
    .local v0, "ramSizeGB":J
    invoke-static {}, Landroid/os/Process;->getTotalMemory()J

    move-result-wide v2

    const-wide/32 v4, 0x40000000

    div-long/2addr v2, v4

    .line 1915
    .local v2, "totalMemByte":J
    const-wide/16 v4, 0x2

    cmp-long v4, v2, v4

    if-gez v4, :cond_1

    .line 1916
    const/16 v4, 0xc

    return v4

    .line 1917
    :cond_1
    const-wide/16 v4, 0x3

    cmp-long v4, v2, v4

    if-gtz v4, :cond_2

    .line 1918
    const/16 v4, 0x10

    return v4

    .line 1919
    :cond_2
    const-wide/16 v4, 0x4

    cmp-long v4, v2, v4

    if-gtz v4, :cond_3

    .line 1920
    const/16 v4, 0x18

    return v4

    .line 1922
    :cond_3
    const/16 v4, 0x20

    return v4
.end method

.method public getOomAdjOfPid(I)I
    .locals 4
    .param p1, "pid"    # I

    .line 1222
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 1223
    .local v0, "callingUid":I
    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v1

    const/16 v2, 0x3e8

    if-ne v1, v2, :cond_0

    .line 1227
    const-string v1, "ActivityManagerServiceImpl"

    invoke-static {v1, p1}, Lcom/android/server/ScoutHelper;->getOomAdjOfPid(Ljava/lang/String;I)I

    move-result v1

    return v1

    .line 1224
    :cond_0
    new-instance v1, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Only the system process can call getOomAdjOfPid, received request from uid: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getPackageHoldOn(Landroid/os/Parcel;Landroid/os/Parcel;)Z
    .locals 6
    .param p1, "data"    # Landroid/os/Parcel;
    .param p2, "reply"    # Landroid/os/Parcel;

    .line 879
    const-string v0, "android.app.IActivityManager"

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 880
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 881
    .local v0, "callingUid":I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1

    .line 882
    .local v1, "ident":J
    invoke-virtual {p2}, Landroid/os/Parcel;->writeNoException()V

    .line 884
    :try_start_0
    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v3

    const/16 v4, 0x3e8

    if-eq v3, v4, :cond_0

    .line 885
    const-string v3, ""

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 886
    const-string v3, "ActivityManagerServiceImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Permission Denial: getPackageHoldOn() not from system "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 888
    :cond_0
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->get()Lcom/android/server/wm/ActivityTaskManagerServiceStub;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->getPackageHoldOn()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 891
    :goto_0
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 892
    nop

    .line 893
    const/4 v3, 0x1

    return v3

    .line 891
    :catchall_0
    move-exception v3

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 892
    throw v3
.end method

.method public getPackageNameByPid(I)Ljava/lang/String;
    .locals 1
    .param p1, "pid"    # I

    .line 439
    invoke-static {p1}, Lcom/android/server/am/ProcessUtils;->getPackageNameByPid(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPackageNameForPid(I)Ljava/lang/String;
    .locals 3
    .param p1, "pid"    # I

    .line 1780
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mPidsSelfLocked:Lcom/android/server/am/ActivityManagerService$PidMap;

    monitor-enter v0

    .line 1781
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    iget-object v1, v1, Lcom/android/server/am/ActivityManagerService;->mPidsSelfLocked:Lcom/android/server/am/ActivityManagerService$PidMap;

    invoke-virtual {v1, p1}, Lcom/android/server/am/ActivityManagerService$PidMap;->get(I)Lcom/android/server/am/ProcessRecord;

    move-result-object v1

    .line 1782
    .local v1, "app":Lcom/android/server/am/ProcessRecord;
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    monitor-exit v0

    return-object v2

    .line 1783
    .end local v1    # "app":Lcom/android/server/am/ProcessRecord;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getProcessNameByPid(I)Ljava/lang/String;
    .locals 1
    .param p1, "pid"    # I

    .line 434
    invoke-static {p1}, Lcom/android/server/am/ProcessUtils;->getProcessNameByPid(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTopSplitActivity(I)Landroid/os/IBinder;
    .locals 3
    .param p1, "pid"    # I

    .line 1355
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mSplitActivityEntryStack:Ljava/util/Map;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 1358
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mSplitActivityEntryStack:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    .line 1359
    .local v0, "stack":Ljava/util/Stack;, "Ljava/util/Stack<Landroid/os/IBinder;>;"
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/IBinder;

    :cond_1
    return-object v1

    .line 1356
    .end local v0    # "stack":Ljava/util/Stack;, "Ljava/util/Stack<Landroid/os/IBinder;>;"
    :cond_2
    :goto_0
    return-object v1
.end method

.method public ignoreSpecifiedAuthority(Ljava/lang/String;)Z
    .locals 1
    .param p1, "authority"    # Ljava/lang/String;

    .line 710
    sget-object v0, Lcom/android/server/am/ActivityManagerServiceImpl;->mIgnoreAuthorityList:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method init(Lcom/android/server/am/ActivityManagerService;Landroid/content/Context;)V
    .locals 4
    .param p1, "ams"    # Lcom/android/server/am/ActivityManagerService;
    .param p2, "context"    # Landroid/content/Context;

    .line 242
    iput-object p1, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    .line 243
    iput-object p2, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mContext:Landroid/content/Context;

    .line 244
    invoke-static {}, Lcom/android/server/am/MiuiWarnings;->getInstance()Lcom/android/server/am/MiuiWarnings;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/server/am/MiuiWarnings;->init(Landroid/content/Context;)V

    .line 245
    invoke-static {}, Lcom/android/server/am/BroadcastQueueModernStubImpl;->getInstance()Lcom/android/server/am/BroadcastQueueModernStubImpl;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    iget-object v2, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, v2}, Lcom/android/server/am/BroadcastQueueModernStubImpl;->init(Lcom/android/server/am/ActivityManagerService;Landroid/content/Context;)V

    .line 247
    new-instance v0, Lcom/android/server/am/DumpScoutTraceThread;

    const-string v1, "DumpScoutTraceThread"

    invoke-direct {v0, v1, p0}, Lcom/android/server/am/DumpScoutTraceThread;-><init>(Ljava/lang/String;Lcom/android/server/am/ActivityManagerServiceImpl;)V

    .line 248
    .local v0, "dumpScoutTraceThread":Lcom/android/server/am/DumpScoutTraceThread;
    invoke-virtual {v0}, Lcom/android/server/am/DumpScoutTraceThread;->start()V

    .line 249
    const-string v1, "DumpScoutTraceThread begin running."

    const-string v2, "ActivityManagerServiceImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    sget-boolean v1, Lmiui/os/Build;->IS_DEBUGGABLE:Z

    if-eqz v1, :cond_0

    .line 251
    const-string v1, "debug.block_system"

    const/4 v3, 0x0

    invoke-static {v1, v3}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 252
    const-string v1, "boot monitor system_watchdog..."

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    const-wide v1, 0x7fffffffffffffffL

    invoke-static {v1, v2}, Landroid/os/SystemClock;->sleep(J)V

    .line 255
    :cond_0
    const-class v1, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    iput-object v1, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    .line 256
    const-class v1, Lcom/miui/server/stability/StabilityLocalServiceInternal;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/stability/StabilityLocalServiceInternal;

    iput-object v1, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mStabilityLocalServiceInternal:Lcom/miui/server/stability/StabilityLocalServiceInternal;

    .line 257
    return-void
.end method

.method public isActiveInstruUid(I)Z
    .locals 1
    .param p1, "uid"    # I

    .line 1827
    iget v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mInstrUid:I

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isAllowedOperatorGetPhoneNumber(Lcom/android/server/am/ActivityManagerService;Ljava/lang/String;)Z
    .locals 16
    .param p1, "ams"    # Lcom/android/server/am/ActivityManagerService;
    .param p2, "permission"    # Ljava/lang/String;

    .line 740
    const-string v0, ";"

    move-object/from16 v1, p2

    invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 741
    .local v0, "content":[Ljava/lang/String;
    array-length v2, v0

    const/4 v3, 0x4

    const/4 v4, 0x1

    if-ne v2, v3, :cond_2

    .line 742
    const/4 v2, 0x3

    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 743
    .local v2, "pid":I
    invoke-static {v2}, Lcom/android/server/am/ProcessUtils;->getPackageNameByPid(I)Ljava/lang/String;

    move-result-object v3

    .line 744
    .local v3, "packageName":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 745
    return v4

    .line 747
    :cond_0
    aget-object v5, v0, v4

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    .line 748
    .local v13, "op":I
    const/4 v5, 0x2

    aget-object v5, v0, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    .line 749
    .local v14, "uid":I
    move-object/from16 v15, p1

    iget-object v5, v15, Lcom/android/server/am/ActivityManagerService;->mAppOpsService:Lcom/android/server/appop/AppOpsService;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const-string v11, "ActivityManagerServiceImpl#isAllowedOperatorGetPhoneNumber"

    const/4 v12, 0x0

    move v6, v13

    move v7, v14

    move-object v8, v3

    invoke-virtual/range {v5 .. v12}, Lcom/android/server/appop/AppOpsService;->noteOperation(IILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;Z)Landroid/app/SyncNotedAppOp;

    move-result-object v5

    .line 751
    invoke-virtual {v5}, Landroid/app/SyncNotedAppOp;->getOpMode()I

    move-result v5

    if-nez v5, :cond_1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    .line 749
    :goto_0
    return v4

    .line 753
    .end local v2    # "pid":I
    .end local v3    # "packageName":Ljava/lang/String;
    .end local v13    # "op":I
    .end local v14    # "uid":I
    :cond_2
    move-object/from16 v15, p1

    return v4
.end method

.method public isBackuping(I)Z
    .locals 2
    .param p1, "uid"    # I

    .line 1809
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mBackupingList:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isBoostNeeded(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "hostingType"    # Ljava/lang/String;
    .param p3, "hostingName"    # Ljava/lang/String;

    .line 934
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->callerPackage:Ljava/lang/String;

    .line 936
    .local v0, "callerPackage":Ljava/lang/String;
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/am/ActivityManagerServiceImpl;->isSystemPackage(Ljava/lang/String;I)Z

    move-result v2

    .line 937
    .local v2, "isSystem":Z
    const-string/jumbo v3, "service"

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_0

    .line 938
    const-string v3, "com.xiaomi.mipush.sdk.PushMessageHandler"

    invoke-virtual {p3, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 939
    const-string v3, "com.xiaomi.xmsf"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    move v3, v4

    goto :goto_0

    :cond_0
    move v3, v1

    .line 940
    .local v3, "isNeeded":Z
    :goto_0
    const-string v5, "com.miui.voiceassist/com.xiaomi.voiceassistant.VoiceService"

    invoke-virtual {v5, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    move v5, v1

    goto :goto_2

    :cond_2
    :goto_1
    move v5, v4

    :goto_2
    move v3, v5

    .line 941
    const-string v5, "com.miui.notification"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    if-eqz v3, :cond_4

    :cond_3
    move v1, v4

    .line 942
    .end local v3    # "isNeeded":Z
    .local v1, "isNeeded":Z
    :cond_4
    const-string v3, "com.tencent.mm"

    iget-object v4, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 943
    const/4 v1, 0x1

    .line 945
    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "hostingType="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", hostingName="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", callerPackage="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", isSystem="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", isBoostNeeded="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Boost"

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 948
    return v1
.end method

.method public isKillProvider(Lcom/android/server/am/ContentProviderRecord;Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;)Z
    .locals 3
    .param p1, "cpr"    # Lcom/android/server/am/ContentProviderRecord;
    .param p2, "proc"    # Lcom/android/server/am/ProcessRecord;
    .param p3, "capp"    # Lcom/android/server/am/ProcessRecord;

    .line 1445
    iget-object v0, p3, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I

    move-result v0

    const/16 v1, 0xfa

    if-le v0, v1, :cond_0

    .line 1446
    invoke-static {p3}, Lcom/android/server/am/ProcessUtils;->isHomeProcess(Lcom/android/server/am/ProcessRecord;)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p3, Lcom/android/server/am/ProcessRecord;->uid:I

    iget-object v1, p3, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v2, p3, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 1447
    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/am/ActivityManagerServiceImpl;->isProtectProcess(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1448
    invoke-virtual {p3}, Lcom/android/server/am/ProcessRecord;->getWindowProcessController()Lcom/android/server/wm/WindowProcessController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/WindowProcessController;->isPreviousProcess()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1449
    const/4 v0, 0x1

    return v0

    .line 1451
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "visible app "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p3, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " depends on provider "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/android/server/am/ContentProviderRecord;->name:Landroid/content/ComponentName;

    .line 1452
    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " in dying proc "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1453
    const-string v1, "??"

    if-eqz p2, :cond_1

    iget-object v2, p2, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    goto :goto_0

    :cond_1
    move-object v2, v1

    :goto_0
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " (adj "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1454
    if-eqz p2, :cond_2

    iget-object v1, p2, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessStateRecord;->getSetAdj()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :cond_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1451
    const-string v1, "ActivityManagerServiceImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1455
    const/4 v0, 0x0

    return v0
.end method

.method public isRestrictBackgroundAction(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;)Z
    .locals 7
    .param p1, "localhost"    # Ljava/lang/String;
    .param p2, "callerUid"    # I
    .param p3, "callerPkgName"    # Ljava/lang/String;
    .param p4, "calleeUid"    # I
    .param p5, "calleePkgName"    # Ljava/lang/String;

    .line 329
    invoke-direct {p0}, Lcom/android/server/am/ActivityManagerServiceImpl;->getGreezeService()Lcom/miui/server/greeze/GreezeManagerInternal;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 330
    invoke-direct {p0}, Lcom/android/server/am/ActivityManagerServiceImpl;->getGreezeService()Lcom/miui/server/greeze/GreezeManagerInternal;

    move-result-object v1

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/miui/server/greeze/GreezeManagerInternal;->isRestrictBackgroundAction(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;)Z

    move-result v0

    return v0

    .line 332
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method isStartWithBackupRestriction(Landroid/content/Context;Ljava/lang/String;Lcom/android/server/am/ProcessRecord;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "backupPkgName"    # Ljava/lang/String;
    .param p3, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 716
    invoke-virtual {p3}, Lcom/android/server/am/ProcessRecord;->getActiveInstrumentation()Lcom/android/server/am/ActiveInstrumentation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 717
    invoke-virtual {p3}, Lcom/android/server/am/ProcessRecord;->getActiveInstrumentation()Lcom/android/server/am/ActiveInstrumentation;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/am/ActiveInstrumentation;->mTargetInfo:Landroid/content/pm/ApplicationInfo;

    goto :goto_0

    :cond_0
    iget-object v0, p3, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    .line 720
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    :goto_0
    const/4 v1, 0x0

    return v1
.end method

.method public isSystemApp(I)Z
    .locals 3
    .param p1, "pid"    # I

    .line 1773
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mPidsSelfLocked:Lcom/android/server/am/ActivityManagerService$PidMap;

    monitor-enter v0

    .line 1774
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    iget-object v1, v1, Lcom/android/server/am/ActivityManagerService;->mPidsSelfLocked:Lcom/android/server/am/ActivityManagerService$PidMap;

    invoke-virtual {v1, p1}, Lcom/android/server/am/ActivityManagerService$PidMap;->get(I)Lcom/android/server/am/ProcessRecord;

    move-result-object v1

    .line 1775
    .local v1, "app":Lcom/android/server/am/ProcessRecord;
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v2}, Landroid/content/pm/ApplicationInfo;->isSystemApp()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    monitor-exit v0

    return v2

    .line 1776
    .end local v1    # "app":Lcom/android/server/am/ProcessRecord;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isTopSplitActivity(ILandroid/os/IBinder;)Z
    .locals 3
    .param p1, "pid"    # I
    .param p2, "token"    # Landroid/os/IBinder;

    .line 1347
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mSplitActivityEntryStack:Ljava/util/Map;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    if-nez p2, :cond_0

    goto :goto_0

    .line 1350
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mSplitActivityEntryStack:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    .line 1351
    .local v0, "stack":Ljava/util/Stack;, "Ljava/util/Stack<Landroid/os/IBinder;>;"
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1

    .line 1348
    .end local v0    # "stack":Ljava/util/Stack;, "Ljava/util/Stack<Landroid/os/IBinder;>;"
    :cond_2
    :goto_0
    return v1
.end method

.method public isVibratorActive(I)Z
    .locals 2
    .param p1, "uid"    # I

    .line 1837
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mUsingVibratorUids:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public killPackageProcesses(Ljava/lang/String;IILjava/lang/String;)Z
    .locals 18
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "appId"    # I
    .param p3, "userId"    # I
    .param p4, "reason"    # Ljava/lang/String;

    .line 1476
    move-object/from16 v1, p0

    const/4 v2, 0x0

    .line 1477
    .local v2, "result":Z
    iget-object v3, v1, Lcom/android/server/am/ActivityManagerServiceImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v3

    .line 1478
    :try_start_0
    invoke-static {}, Lcom/android/server/am/ActivityManagerService;->boostPriorityForLockedSection()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1481
    :try_start_1
    iget-object v0, v1, Lcom/android/server/am/ActivityManagerServiceImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    iget-object v4, v0, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x1

    const/4 v14, 0x0

    const/16 v15, 0xd

    const/16 v16, 0x0

    move-object/from16 v5, p1

    move/from16 v6, p2

    move/from16 v7, p3

    move-object/from16 v17, p4

    invoke-virtual/range {v4 .. v17}, Lcom/android/server/am/ProcessList;->killPackageProcessesLSP(Ljava/lang/String;IIIZZZZZZIILjava/lang/String;)Z

    move-result v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v2, v0

    .line 1485
    goto :goto_0

    .line 1483
    :catch_0
    move-exception v0

    .line 1484
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v4, "ActivityManagerServiceImpl"

    const-string v5, "invoke killPackageProcessesLocked error:"

    invoke-static {v4, v5, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1486
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    invoke-static {}, Lcom/android/server/am/ActivityManagerService;->resetPriorityAfterLockedSection()V

    .line 1487
    monitor-exit v3

    return v2

    .line 1488
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public killProcessDueToResolutionChanged()V
    .locals 2

    .line 725
    new-instance v0, Lmiui/process/ProcessConfig;

    const/16 v1, 0x12

    invoke-direct {v0, v1}, Lmiui/process/ProcessConfig;-><init>(I)V

    .line 726
    .local v0, "config":Lmiui/process/ProcessConfig;
    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lmiui/process/ProcessConfig;->setPriority(I)V

    .line 727
    invoke-static {v0}, Lmiui/process/ProcessManager;->kill(Lmiui/process/ProcessConfig;)Z

    .line 728
    return-void
.end method

.method markAmsReady()V
    .locals 3

    .line 360
    invoke-static {}, Lmiui/mqsas/sdk/BootEventManager;->getInstance()Lmiui/mqsas/sdk/BootEventManager;

    move-result-object v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lmiui/mqsas/sdk/BootEventManager;->setAmsReady(J)V

    .line 361
    return-void
.end method

.method markUIReady()V
    .locals 3

    .line 364
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 365
    .local v0, "bootCompleteTime":J
    invoke-static {}, Lmiui/mqsas/sdk/BootEventManager;->getInstance()Lmiui/mqsas/sdk/BootEventManager;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lmiui/mqsas/sdk/BootEventManager;->setUIReady(J)V

    .line 366
    invoke-static {}, Lmiui/mqsas/sdk/BootEventManager;->getInstance()Lmiui/mqsas/sdk/BootEventManager;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lmiui/mqsas/sdk/BootEventManager;->setBootComplete(J)V

    .line 367
    return-void
.end method

.method public moveUserToForeground(I)V
    .locals 2
    .param p1, "newUserId"    # I

    .line 735
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getInstance()Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    move-result-object v0

    const-string v1, "moveUserToForeground"

    invoke-virtual {v0, p1, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->restartSubScreenUiIfNeeded(ILjava/lang/String;)V

    .line 736
    return-void
.end method

.method public notifyCrashToVkService(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "inNative"    # Z
    .param p3, "stackTrace"    # Ljava/lang/String;

    .line 1929
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mForceVkInternal:Lcom/xiaomi/vkmode/service/MiuiForceVkServiceInternal;

    if-nez v0, :cond_0

    .line 1930
    const-class v0, Lcom/xiaomi/vkmode/service/MiuiForceVkServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/vkmode/service/MiuiForceVkServiceInternal;

    iput-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mForceVkInternal:Lcom/xiaomi/vkmode/service/MiuiForceVkServiceInternal;

    .line 1932
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mForceVkInternal:Lcom/xiaomi/vkmode/service/MiuiForceVkServiceInternal;

    if-eqz v0, :cond_1

    .line 1933
    invoke-interface {v0, p1, p2, p3}, Lcom/xiaomi/vkmode/service/MiuiForceVkServiceInternal;->onAppCrashed(Ljava/lang/String;ZLjava/lang/String;)V

    .line 1935
    :cond_1
    return-void
.end method

.method public notifyDumpAllInfo()V
    .locals 1

    .line 1954
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->greezer:Lcom/miui/server/greeze/GreezeManagerInternal;

    if-eqz v0, :cond_0

    .line 1955
    invoke-virtual {v0}, Lcom/miui/server/greeze/GreezeManagerInternal;->notifyDumpAllInfo()V

    .line 1957
    :cond_0
    return-void
.end method

.method public notifyDumpAppInfo(II)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 1948
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->greezer:Lcom/miui/server/greeze/GreezeManagerInternal;

    if-eqz v0, :cond_0

    .line 1949
    invoke-virtual {v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerInternal;->notifyDumpAppInfo(II)V

    .line 1951
    :cond_0
    return-void
.end method

.method public notifyExcuteServices(Lcom/android/server/am/ProcessRecord;)V
    .locals 2
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 1787
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->greezer:Lcom/miui/server/greeze/GreezeManagerInternal;

    if-eqz v0, :cond_0

    .line 1788
    iget v1, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/GreezeManagerInternal;->notifyExcuteServices(I)V

    .line 1790
    :cond_0
    return-void
.end method

.method public onGrantUriPermission(Ljava/lang/String;Ljava/lang/String;ILcom/android/server/uri/GrantUri;)V
    .locals 1
    .param p1, "sourcePkg"    # Ljava/lang/String;
    .param p2, "targetPkg"    # Ljava/lang/String;
    .param p3, "targetUid"    # I
    .param p4, "grantUri"    # Lcom/android/server/uri/GrantUri;

    .line 1842
    invoke-virtual {p4}, Lcom/android/server/uri/GrantUri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/server/am/ActivityManagerServiceImpl;->recordAppBehavior(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 1843
    return-void
.end method

.method onSystemReady()V
    .locals 3

    .line 261
    invoke-static {}, Lmiui/mqsas/sdk/BootEventManager;->getInstance()Lmiui/mqsas/sdk/BootEventManager;

    move-result-object v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lmiui/mqsas/sdk/BootEventManager;->setAmsReady(J)V

    .line 262
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mSystemReady:Z

    .line 263
    const-class v0, Landroid/content/pm/PackageManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageManagerInternal;

    iput-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mPackageManager:Landroid/content/pm/PackageManagerInternal;

    .line 271
    :try_start_0
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/am/ActivityManagerServiceImpl;->ensureDeviceProvisioned(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 274
    goto :goto_0

    .line 272
    :catch_0
    move-exception v0

    .line 273
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ActivityManagerServiceImpl"

    const-string v2, "ensureDeviceProvisioned occurs Exception."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 276
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    invoke-static {}, Lcom/android/server/am/MemoryStandardProcessControlStub;->getInstance()Lcom/android/server/am/MemoryStandardProcessControlStub;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    invoke-interface {v0, v1, v2}, Lcom/android/server/am/MemoryStandardProcessControlStub;->init(Landroid/content/Context;Lcom/android/server/am/ActivityManagerService;)Z

    .line 277
    invoke-static {}, Lcom/android/server/am/MemoryFreezeStub;->getInstance()Lcom/android/server/am/MemoryFreezeStub;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    invoke-interface {v0, v1, v2}, Lcom/android/server/am/MemoryFreezeStub;->init(Landroid/content/Context;Lcom/android/server/am/ActivityManagerService;)V

    .line 278
    invoke-static {}, Lcom/android/server/am/ProcessProphetStub;->getInstance()Lcom/android/server/am/ProcessProphetStub;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    invoke-interface {v0, v1, v2}, Lcom/android/server/am/ProcessProphetStub;->init(Landroid/content/Context;Lcom/android/server/am/ActivityManagerService;)V

    .line 279
    invoke-static {}, Lcom/android/server/wm/MiuiFreezeStub;->getInstance()Lcom/android/server/wm/MiuiFreezeStub;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    iget-object v1, v1, Lcom/android/server/am/ActivityManagerService;->mActivityTaskManager:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-interface {v0, v1}, Lcom/android/server/wm/MiuiFreezeStub;->init(Lcom/android/server/wm/ActivityTaskManagerService;)V

    .line 280
    return-void
.end method

.method public onTransact(Lcom/android/server/am/ActivityManagerService;ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 22
    .param p1, "service"    # Lcom/android/server/am/ActivityManagerService;
    .param p2, "code"    # I
    .param p3, "data"    # Landroid/os/Parcel;
    .param p4, "reply"    # Landroid/os/Parcel;
    .param p5, "flags"    # I

    .line 759
    move-object/from16 v0, p0

    move/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    const v4, 0xfffffe

    if-ne v1, v4, :cond_0

    .line 760
    move-object/from16 v4, p1

    invoke-virtual {v0, v4, v2, v3}, Lcom/android/server/am/ActivityManagerServiceImpl;->setPackageHoldOn(Lcom/android/server/am/ActivityManagerService;Landroid/os/Parcel;Landroid/os/Parcel;)Z

    move-result v5

    return v5

    .line 761
    :cond_0
    move-object/from16 v4, p1

    const v5, 0xfffffd

    if-ne v1, v5, :cond_1

    .line 762
    invoke-virtual {v0, v2, v3}, Lcom/android/server/am/ActivityManagerServiceImpl;->getPackageHoldOn(Landroid/os/Parcel;Landroid/os/Parcel;)Z

    move-result v5

    return v5

    .line 763
    :cond_1
    const v5, 0xfffffc

    const-string v6, "android.app.IActivityManager"

    const/4 v7, 0x1

    const/4 v8, 0x0

    if-ne v1, v5, :cond_2

    .line 764
    invoke-virtual {v2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 765
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getInstance()Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getLastResumedActivityInfo()Landroid/content/pm/ActivityInfo;

    move-result-object v5

    .line 766
    .local v5, "info":Landroid/content/pm/ActivityInfo;
    invoke-virtual/range {p4 .. p4}, Landroid/os/Parcel;->writeNoException()V

    .line 767
    invoke-virtual {v3, v5, v8}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 768
    return v7

    .line 771
    .end local v5    # "info":Landroid/content/pm/ActivityInfo;
    :cond_2
    const v5, 0xfffda1

    if-ne v1, v5, :cond_3

    .line 772
    invoke-virtual {v2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 773
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->readInt()I

    move-result v5

    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Lcom/android/server/am/ActivityManagerServiceImpl;->isTopSplitActivity(ILandroid/os/IBinder;)Z

    move-result v5

    .line 774
    .local v5, "isTop":I
    invoke-virtual/range {p4 .. p4}, Landroid/os/Parcel;->writeNoException()V

    .line 775
    invoke-virtual {v3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 776
    return v7

    .line 778
    .end local v5    # "isTop":I
    :cond_3
    const v5, 0xfffda0

    const-string v9, "ms"

    const-string v10, "ActivityManagerServiceImpl"

    if-ne v1, v5, :cond_9

    .line 779
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v5

    .line 780
    .local v5, "uid":I
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v11

    invoke-virtual {v0, v11}, Lcom/android/server/am/ActivityManagerServiceImpl;->getPackageNameByPid(I)Ljava/lang/String;

    move-result-object v15

    .line 781
    .local v15, "pkg":Ljava/lang/String;
    invoke-static {v5}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 782
    iget-object v11, v0, Lcom/android/server/am/ActivityManagerServiceImpl;->mPackageManager:Landroid/content/pm/PackageManagerInternal;

    const-wide/16 v13, 0x0

    invoke-static {v5}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v16

    move-object v12, v15

    move-object v7, v15

    .end local v15    # "pkg":Ljava/lang/String;
    .local v7, "pkg":Ljava/lang/String;
    move v15, v5

    invoke-virtual/range {v11 .. v16}, Landroid/content/pm/PackageManagerInternal;->getApplicationInfo(Ljava/lang/String;JII)Landroid/content/pm/ApplicationInfo;

    move-result-object v17

    .line 783
    .local v17, "ainfo":Landroid/content/pm/ApplicationInfo;
    if-eqz v17, :cond_4

    invoke-virtual/range {v17 .. v17}, Landroid/content/pm/ApplicationInfo;->isSystemApp()Z

    move-result v11

    if-nez v11, :cond_8

    invoke-virtual/range {v17 .. v17}, Landroid/content/pm/ApplicationInfo;->isProduct()Z

    move-result v11

    if-nez v11, :cond_8

    .line 784
    :cond_4
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v18

    .line 785
    .local v18, "startTime":J
    sget-object v11, Lcom/android/server/am/ActivityManagerServiceImpl;->splitDecouplePkgList:Ljava/util/Map;

    invoke-interface {v11, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v11

    const-string v15, ", security check took: "

    if-nez v11, :cond_5

    .line 786
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v11

    sub-long v11, v11, v18

    .line 787
    .local v11, "duration":J
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Permission Denial: not from uid "

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v10, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 788
    return v8

    .line 790
    .end local v11    # "duration":J
    :cond_5
    iget-object v11, v0, Lcom/android/server/am/ActivityManagerServiceImpl;->mPackageManager:Landroid/content/pm/PackageManagerInternal;

    const-wide/16 v13, 0x40

    invoke-static {v5}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v16

    move-object v12, v7

    move-object/from16 v20, v15

    move v15, v5

    invoke-virtual/range {v11 .. v16}, Landroid/content/pm/PackageManagerInternal;->getPackageInfo(Ljava/lang/String;JII)Landroid/content/pm/PackageInfo;

    move-result-object v11

    iget-object v11, v11, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    aget-object v11, v11, v8

    .line 791
    .local v11, "releaseSig":Landroid/content/pm/Signature;
    sget-object v12, Lcom/android/server/am/ActivityManagerServiceImpl;->splitDecouplePkgList:Ljava/util/Map;

    invoke-interface {v12, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/List;

    invoke-virtual {v11}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v13

    invoke-virtual {v0, v13}, Lcom/android/server/am/ActivityManagerServiceImpl;->sha256([B)Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_6

    .line 792
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v12

    sub-long v12, v12, v18

    .line 793
    .local v12, "duration":J
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Permission Denial: not set package "

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v14, v20

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v10, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 794
    return v8

    .line 796
    .end local v12    # "duration":J
    :cond_6
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    sub-long v8, v8, v18

    .line 797
    .local v8, "duration":J
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "security check took "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "ms."

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 781
    .end local v7    # "pkg":Ljava/lang/String;
    .end local v8    # "duration":J
    .end local v11    # "releaseSig":Landroid/content/pm/Signature;
    .end local v17    # "ainfo":Landroid/content/pm/ApplicationInfo;
    .end local v18    # "startTime":J
    .restart local v15    # "pkg":Ljava/lang/String;
    :cond_7
    move-object v7, v15

    .line 800
    .end local v15    # "pkg":Ljava/lang/String;
    .restart local v7    # "pkg":Ljava/lang/String;
    :cond_8
    :goto_0
    invoke-virtual {v2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 801
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->readInt()I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/android/server/am/ActivityManagerServiceImpl;->getTopSplitActivity(I)Landroid/os/IBinder;

    move-result-object v6

    .line 802
    .local v6, "token":Landroid/os/IBinder;
    invoke-virtual/range {p4 .. p4}, Landroid/os/Parcel;->writeNoException()V

    .line 803
    invoke-virtual {v3, v6}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 804
    const/4 v11, 0x1

    return v11

    .line 806
    .end local v5    # "uid":I
    .end local v6    # "token":Landroid/os/IBinder;
    .end local v7    # "pkg":Ljava/lang/String;
    :cond_9
    move v11, v7

    const v5, 0xfffda2

    if-ne v1, v5, :cond_a

    .line 807
    invoke-virtual {v2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 808
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->readInt()I

    move-result v5

    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Lcom/android/server/am/ActivityManagerServiceImpl;->removeFromEntryStack(ILandroid/os/IBinder;)V

    .line 809
    return v11

    .line 811
    :cond_a
    const v5, 0xfffda3

    if-ne v1, v5, :cond_b

    .line 812
    invoke-virtual {v2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 813
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->readInt()I

    move-result v5

    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Lcom/android/server/am/ActivityManagerServiceImpl;->clearEntryStack(ILandroid/os/IBinder;)V

    .line 814
    return v11

    .line 816
    :cond_b
    const v5, 0xfffda4

    if-ne v1, v5, :cond_c

    .line 817
    invoke-virtual {v2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 818
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->readInt()I

    move-result v5

    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v6

    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->readInt()I

    move-result v7

    sget-object v8, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Intent;

    invoke-virtual {v0, v5, v6, v7, v8}, Lcom/android/server/am/ActivityManagerServiceImpl;->addToEntryStack(ILandroid/os/IBinder;ILandroid/content/Intent;)V

    .line 819
    invoke-virtual/range {p4 .. p4}, Landroid/os/Parcel;->writeNoException()V

    .line 820
    const/4 v5, 0x1

    return v5

    .line 822
    :cond_c
    const v5, 0xfffda5

    if-ne v1, v5, :cond_e

    .line 823
    invoke-virtual {v2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 824
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->readInt()I

    move-result v5

    .line 825
    .local v5, "pid":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-lez v6, :cond_d

    const/4 v6, 0x1

    goto :goto_1

    :cond_d
    move v6, v8

    .line 826
    .local v6, "isForLast":Z
    :goto_1
    invoke-direct {v0, v5, v6}, Lcom/android/server/am/ActivityManagerServiceImpl;->getIntentInfo(IZ)[Landroid/os/Parcelable;

    move-result-object v7

    .line 827
    .local v7, "parcelables":[Landroid/os/Parcelable;
    invoke-virtual/range {p4 .. p4}, Landroid/os/Parcel;->writeNoException()V

    .line 828
    invoke-virtual {v3, v7, v8}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    .line 829
    const/4 v8, 0x1

    return v8

    .line 831
    .end local v5    # "pid":I
    .end local v6    # "isForLast":Z
    .end local v7    # "parcelables":[Landroid/os/Parcelable;
    :cond_e
    const v5, 0xfffda6

    if-ne v1, v5, :cond_14

    .line 832
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v5

    .line 833
    .local v5, "uid":I
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v7

    invoke-virtual {v0, v7}, Lcom/android/server/am/ActivityManagerServiceImpl;->getPackageNameByPid(I)Ljava/lang/String;

    move-result-object v7

    .line 834
    .local v7, "pkg":Ljava/lang/String;
    invoke-static {v5}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v11

    if-eqz v11, :cond_12

    .line 835
    iget-object v11, v0, Lcom/android/server/am/ActivityManagerServiceImpl;->mPackageManager:Landroid/content/pm/PackageManagerInternal;

    const-wide/16 v13, 0x0

    invoke-static {v5}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v16

    move-object v12, v7

    move v15, v5

    invoke-virtual/range {v11 .. v16}, Landroid/content/pm/PackageManagerInternal;->getApplicationInfo(Ljava/lang/String;JII)Landroid/content/pm/ApplicationInfo;

    move-result-object v17

    .line 836
    .restart local v17    # "ainfo":Landroid/content/pm/ApplicationInfo;
    if-eqz v17, :cond_f

    invoke-virtual/range {v17 .. v17}, Landroid/content/pm/ApplicationInfo;->isSystemApp()Z

    move-result v11

    if-nez v11, :cond_12

    invoke-virtual/range {v17 .. v17}, Landroid/content/pm/ApplicationInfo;->isProduct()Z

    move-result v11

    if-nez v11, :cond_12

    .line 837
    :cond_f
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v18

    .line 838
    .restart local v18    # "startTime":J
    sget-object v11, Lcom/android/server/am/ActivityManagerServiceImpl;->splitDecouplePkgList:Ljava/util/Map;

    invoke-interface {v11, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v11

    const-string v15, " security check took: "

    if-nez v11, :cond_10

    .line 839
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v11

    sub-long v11, v11, v18

    .line 840
    .local v11, "duration":J
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Permission Denial: not from uid: "

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v10, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 841
    return v8

    .line 843
    .end local v11    # "duration":J
    :cond_10
    iget-object v11, v0, Lcom/android/server/am/ActivityManagerServiceImpl;->mPackageManager:Landroid/content/pm/PackageManagerInternal;

    const-wide/16 v13, 0x40

    invoke-static {v5}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v16

    move-object v12, v7

    move-object/from16 v21, v15

    move v15, v5

    invoke-virtual/range {v11 .. v16}, Landroid/content/pm/PackageManagerInternal;->getPackageInfo(Ljava/lang/String;JII)Landroid/content/pm/PackageInfo;

    move-result-object v11

    iget-object v11, v11, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    aget-object v11, v11, v8

    .line 844
    .local v11, "releaseSig":Landroid/content/pm/Signature;
    sget-object v12, Lcom/android/server/am/ActivityManagerServiceImpl;->splitDecouplePkgList:Ljava/util/Map;

    invoke-interface {v12, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/List;

    invoke-virtual {v11}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v13

    invoke-virtual {v0, v13}, Lcom/android/server/am/ActivityManagerServiceImpl;->sha256([B)Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_11

    .line 845
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v12

    sub-long v12, v12, v18

    .line 846
    .restart local v12    # "duration":J
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Permission Denial: not set package: "

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v14, v21

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v10, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 847
    return v8

    .line 849
    .end local v12    # "duration":J
    :cond_11
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v12

    sub-long v12, v12, v18

    .line 850
    .restart local v12    # "duration":J
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "security check took: "

    invoke-virtual {v8, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v10, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 853
    .end local v11    # "releaseSig":Landroid/content/pm/Signature;
    .end local v12    # "duration":J
    .end local v17    # "ainfo":Landroid/content/pm/ApplicationInfo;
    .end local v18    # "startTime":J
    :cond_12
    const/4 v8, 0x0

    .line 854
    .local v8, "isForLast":Z
    invoke-virtual {v2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 855
    const-class v6, Landroid/content/Intent;

    invoke-virtual {v6}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Landroid/content/Intent;

    .line 856
    .local v6, "intent":Landroid/content/Intent;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->readInt()I

    move-result v9

    .line 857
    .local v9, "pid":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v10

    .line 858
    .local v10, "bundle":Landroid/os/Bundle;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->readInt()I

    move-result v11

    if-lez v11, :cond_13

    .line 859
    const/4 v8, 0x1

    .line 861
    :cond_13
    invoke-direct {v0, v6, v9, v10, v8}, Lcom/android/server/am/ActivityManagerServiceImpl;->setIntentInfo(Landroid/content/Intent;ILandroid/os/Bundle;Z)V

    .line 862
    invoke-virtual/range {p4 .. p4}, Landroid/os/Parcel;->writeNoException()V

    .line 863
    const/4 v11, 0x1

    return v11

    .line 865
    .end local v5    # "uid":I
    .end local v6    # "intent":Landroid/content/Intent;
    .end local v7    # "pkg":Ljava/lang/String;
    .end local v8    # "isForLast":Z
    .end local v9    # "pid":I
    .end local v10    # "bundle":Landroid/os/Bundle;
    :cond_14
    const v5, 0xfffd9f

    if-ne v1, v5, :cond_15

    .line 866
    invoke-virtual {v2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 867
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v5

    .line 868
    .local v5, "token":Landroid/os/IBinder;
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->get()Lcom/android/server/wm/ActivityTaskManagerServiceStub;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->getSplitTaskInfo(Landroid/os/IBinder;)Landroid/app/ActivityManager$RunningTaskInfo;

    move-result-object v6

    .line 869
    .local v6, "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    invoke-virtual/range {p4 .. p4}, Landroid/os/Parcel;->writeNoException()V

    .line 870
    invoke-virtual {v3, v6, v8}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 871
    const/4 v7, 0x1

    return v7

    .line 874
    .end local v5    # "token":Landroid/os/IBinder;
    .end local v6    # "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_15
    return v8
.end method

.method public recordBroadcastLog(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6
    .param p1, "callerApp"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "action"    # Ljava/lang/String;
    .param p3, "callerPackage"    # Ljava/lang/String;
    .param p4, "callingUid"    # I

    .line 1894
    const/4 v0, 0x5

    const-string v1, ". Callers="

    const-string v2, " pkg "

    const-string v3, "Sending non-protected broadcast "

    const-string v4, "ActivityManagerServiceImpl"

    if-eqz p1, :cond_2

    .line 1895
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " from system "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1896
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->toShortString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1897
    if-eqz p3, :cond_1

    const-string v2, "android"

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    .line 1898
    :cond_0
    const-string v0, ""

    goto :goto_1

    :cond_1
    :goto_0
    invoke-static {v0}, Landroid/os/Debug;->getCallers(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1895
    invoke-static {v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1900
    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " from system uid "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1901
    invoke-static {p4}, Landroid/os/UserHandle;->formatUid(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1902
    invoke-static {v0}, Landroid/os/Debug;->getCallers(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1900
    invoke-static {v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1904
    :goto_2
    return-void
.end method

.method public removeFromEntryStack(ILandroid/os/IBinder;)V
    .locals 5
    .param p1, "pid"    # I
    .param p2, "token"    # Landroid/os/IBinder;

    .line 1363
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->splitEntryStackLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1364
    if-eqz p2, :cond_0

    .line 1365
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mSplitActivityEntryStack:Ljava/util/Map;

    if-eqz v1, :cond_0

    .line 1366
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Stack;

    .line 1367
    .local v1, "stack":Ljava/util/Stack;, "Ljava/util/Stack<Landroid/os/IBinder;>;"
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/Stack;->empty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1368
    invoke-virtual {v1, p2}, Ljava/util/Stack;->remove(Ljava/lang/Object;)Z

    .line 1369
    const-string v2, "ActivityManagerServiceImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SplitEntryStack: remove from stack, size="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/util/Stack;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " pid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1373
    .end local v1    # "stack":Ljava/util/Stack;, "Ljava/util/Stack<Landroid/os/IBinder;>;"
    :cond_0
    monitor-exit v0

    .line 1374
    return-void

    .line 1373
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method reportBootEvent()V
    .locals 0

    .line 370
    invoke-static {}, Lmiui/mqsas/sdk/BootEventManager;->getInstance()Lmiui/mqsas/sdk/BootEventManager;

    invoke-static {}, Lmiui/mqsas/sdk/BootEventManager;->reportBootEvent()V

    .line 371
    return-void
.end method

.method public setActiveInstrumentation(Landroid/content/ComponentName;)V
    .locals 4
    .param p1, "instr"    # Landroid/content/ComponentName;

    .line 1813
    const/4 v0, -0x1

    if-eqz p1, :cond_2

    .line 1814
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 1815
    .local v1, "mInstrPkg":Ljava/lang/String;
    if-nez v1, :cond_0

    return-void

    .line 1817
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 1818
    .local v2, "mPm":Landroid/content/pm/PackageManager;
    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    .line 1819
    .local v3, "info":Landroid/content/pm/ApplicationInfo;
    if-eqz v3, :cond_1

    iget v0, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    :cond_1
    iput v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mInstrUid:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1821
    .end local v2    # "mPm":Landroid/content/pm/PackageManager;
    .end local v3    # "info":Landroid/content/pm/ApplicationInfo;
    goto :goto_0

    .line 1820
    :catch_0
    move-exception v0

    .line 1822
    .end local v1    # "mInstrPkg":Ljava/lang/String;
    :goto_0
    goto :goto_1

    .line 1823
    :cond_2
    iput v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mInstrUid:I

    .line 1825
    :goto_1
    return-void
.end method

.method public setPackageHoldOn(Lcom/android/server/am/ActivityManagerService;Landroid/os/Parcel;Landroid/os/Parcel;)Z
    .locals 6
    .param p1, "service"    # Lcom/android/server/am/ActivityManagerService;
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;

    .line 898
    const-string v0, "android.app.IActivityManager"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 899
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 900
    .local v0, "callingUid":I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1

    .line 902
    .local v1, "ident":J
    :try_start_0
    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v3

    const/16 v4, 0x3e8

    if-ne v3, v4, :cond_0

    .line 903
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->get()Lcom/android/server/wm/ActivityTaskManagerServiceStub;

    move-result-object v3

    iget-object v4, p1, Lcom/android/server/am/ActivityManagerService;->mActivityTaskManager:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 904
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 903
    invoke-virtual {v3, v4, v5}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->setPackageHoldOn(Lcom/android/server/wm/ActivityTaskManagerService;Ljava/lang/String;)V

    goto :goto_0

    .line 906
    :cond_0
    const-string v3, "ActivityManagerServiceImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Permission Denial: setPackageHoldOn() not from system uid "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 910
    :goto_0
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 911
    nop

    .line 912
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 913
    const/4 v3, 0x1

    return v3

    .line 910
    :catchall_0
    move-exception v3

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 911
    throw v3
.end method

.method public setVibratorState(IZ)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "active"    # Z

    .line 1831
    if-eqz p2, :cond_0

    .line 1832
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mUsingVibratorUids:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1834
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mUsingVibratorUids:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1835
    :goto_0
    return-void
.end method

.method public sha256([B)Ljava/lang/String;
    .locals 7
    .param p1, "bytes"    # [B

    .line 1961
    const/4 v0, 0x0

    .line 1963
    .local v0, "digest":Ljava/security/MessageDigest;
    :try_start_0
    const-string v1, "SHA-256"

    invoke-static {v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    move-object v0, v1

    .line 1964
    invoke-virtual {v0, p1}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v1

    .line 1965
    .local v1, "hash":[B
    new-instance v2, Ljava/lang/StringBuilder;

    array-length v3, v1

    mul-int/lit8 v3, v3, 0x2

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1966
    .local v2, "hexString":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v4, v1

    if-ge v3, v4, :cond_1

    .line 1967
    aget-byte v4, v1, v3

    and-int/lit16 v4, v4, 0xff

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    .line 1968
    .local v4, "hex":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    .line 1969
    const/16 v5, 0x30

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1971
    :cond_0
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1966
    nop

    .end local v4    # "hex":Ljava/lang/String;
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1973
    .end local v3    # "i":I
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v3

    .line 1974
    .end local v1    # "hash":[B
    .end local v2    # "hexString":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v1

    .line 1975
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    .line 1977
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    const-string v1, ""

    return-object v1
.end method

.method public skipFrozenAppAnr(Landroid/content/pm/ApplicationInfo;ILjava/lang/String;)Z
    .locals 10
    .param p1, "info"    # Landroid/content/pm/ApplicationInfo;
    .param p2, "uid"    # I
    .param p3, "report"    # Ljava/lang/String;

    .line 1299
    invoke-direct {p0}, Lcom/android/server/am/ActivityManagerServiceImpl;->getGreezeService()Lcom/miui/server/greeze/GreezeManagerInternal;

    move-result-object v0

    .line 1300
    .local v0, "greezer":Lcom/miui/server/greeze/GreezeManagerInternal;
    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 1302
    :cond_0
    iget v2, p1, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v2}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v2

    .line 1303
    .local v2, "appid":I
    iget v3, p1, Landroid/content/pm/ApplicationInfo;->uid:I

    if-eq p2, v3, :cond_1

    .line 1304
    invoke-static {p2}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v2

    .line 1305
    :cond_1
    const/16 v3, 0x3e8

    if-gt v2, v3, :cond_2

    return v1

    .line 1307
    :cond_2
    const-class v3, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-static {v3}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-interface {v3, p1, p2, p3}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->skipFrozenAppAnr(Landroid/content/pm/ApplicationInfo;ILjava/lang/String;)Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_3

    .line 1308
    return v4

    .line 1311
    :cond_3
    invoke-virtual {v0, v2}, Lcom/miui/server/greeze/GreezeManagerInternal;->isUidFrozen(I)Z

    move-result v3

    const-string v5, "ActivityManagerServiceImpl"

    if-eqz v3, :cond_4

    .line 1312
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " matched appid is "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1313
    return v4

    .line 1315
    :cond_4
    invoke-static {p2, p3, v0}, Lcom/android/server/am/ActivityManagerServiceImpl;->checkThawTime(ILjava/lang/String;Lcom/miui/server/greeze/GreezeManagerInternal;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1316
    return v4

    .line 1317
    :cond_5
    sget v3, Lcom/miui/server/greeze/GreezeManagerInternal;->GREEZER_MODULE_ALL:I

    invoke-virtual {v0, v3}, Lcom/miui/server/greeze/GreezeManagerInternal;->getFrozenUids(I)[I

    move-result-object v3

    .line 1318
    .local v3, "frozenUids":[I
    sget v4, Lcom/miui/server/greeze/GreezeManagerInternal;->GREEZER_MODULE_ALL:I

    invoke-virtual {v0, v4}, Lcom/miui/server/greeze/GreezeManagerInternal;->getFrozenPids(I)[I

    move-result-object v4

    .line 1319
    .local v4, "frozenPids":[I
    array-length v6, v3

    if-lez v6, :cond_6

    .line 1320
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "frozen uids:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v3}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1321
    :cond_6
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 1322
    .local v6, "inf":Ljava/lang/StringBuilder;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    array-length v8, v4

    if-ge v7, v8, :cond_7

    .line 1323
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "pid: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    aget v9, v4, v7

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/uid:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    aget v9, v4, v7

    invoke-static {v9}, Landroid/os/Process;->getUidForPid(I)I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1322
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 1325
    .end local v7    # "i":I
    :cond_7
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "frozen procs: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1326
    return v1
.end method

.method public skipFrozenServiceTimeout(Lcom/android/server/am/ProcessRecord;Z)Z
    .locals 2
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "fg"    # Z

    .line 1940
    iget-object v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->greezer:Lcom/miui/server/greeze/GreezeManagerInternal;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget v1, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/GreezeManagerInternal;->isUidFrozen(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1941
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Skip Frozen Uid: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " service Timeout! fg: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ActivityManagerServiceImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1942
    const/4 v0, 0x1

    return v0

    .line 1944
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public skipPruneOldTraces()Z
    .locals 1

    .line 1634
    invoke-static {}, Lmiui/mqsas/scout/ScoutUtils;->isLibraryTest()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1635
    const/4 v0, 0x1

    return v0

    .line 1637
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public startProcessLocked(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "hostingType"    # Ljava/lang/String;
    .param p3, "hostingNameStr"    # Ljava/lang/String;

    .line 416
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->callerPackage:Ljava/lang/String;

    .line 417
    .local v0, "callerPackage":Ljava/lang/String;
    const-string v1, "com.xiaomi.xmsf"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 418
    const-string v1, "com.miui.notification"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 419
    :cond_0
    iget-object v1, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    iget-object v1, v1, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    iget v2, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessList;->getUidRecordLOSP(I)Lcom/android/server/am/UidRecord;

    move-result-object v1

    .line 420
    .local v1, "record":Lcom/android/server/am/UidRecord;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/android/server/am/UidRecord;->isIdle()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/android/server/am/UidRecord;->isCurAllowListed()Z

    move-result v2

    if-nez v2, :cond_1

    .line 421
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    .line 422
    .local v2, "callingUid":I
    iget-object v3, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    invoke-virtual {v1}, Lcom/android/server/am/UidRecord;->getUid()I

    move-result v4

    const-wide/32 v5, 0xea60

    const/16 v7, 0x65

    const-string v8, "push-service-launch"

    const/4 v9, 0x0

    move v10, v2

    invoke-virtual/range {v3 .. v10}, Lcom/android/server/am/ActivityManagerService;->tempAllowlistUidLocked(IJILjava/lang/String;II)V

    .line 430
    .end local v1    # "record":Lcom/android/server/am/UidRecord;
    .end local v2    # "callingUid":I
    :cond_1
    return-void
.end method

.method public syncFontForWebView()V
    .locals 0

    .line 1493
    invoke-static {}, Lmiui/util/font/SymlinkUtils;->onAttachApplication()V

    .line 1494
    return-void
.end method
