class com.android.server.am.MimdManagerServiceImpl$1 extends android.content.BroadcastReceiver {
	 /* .source "MimdManagerServiceImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/MimdManagerServiceImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.am.MimdManagerServiceImpl this$0; //synthetic
/* # direct methods */
 com.android.server.am.MimdManagerServiceImpl$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/MimdManagerServiceImpl; */
/* .line 198 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 201 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 202 */
/* .local v0, "action":Ljava/lang/String; */
final String v1 = "com.miui.powerkeeper_sleep_changed"; // const-string v1, "com.miui.powerkeeper_sleep_changed"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 203 */
	 /* const-string/jumbo v1, "state" */
	 int v2 = -1; // const/4 v2, -0x1
	 v1 = 	 (( android.content.Intent ) p2 ).getIntExtra ( v1, v2 ); // invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
	 /* .line 204 */
	 /* .local v1, "state":I */
	 int v2 = 1; // const/4 v2, 0x1
	 /* if-ne v2, v1, :cond_0 */
	 /* .line 205 */
	 v2 = this.this$0;
	 com.android.server.am.MimdManagerServiceImpl .-$$Nest$fgetmMimdManagerHandler ( v2 );
	 v3 = this.this$0;
	 com.android.server.am.MimdManagerServiceImpl .-$$Nest$fgetmMimdManagerHandler ( v3 );
	 java.util.Objects .requireNonNull ( v3 );
	 int v3 = 4; // const/4 v3, 0x4
	 (( com.android.server.am.MimdManagerServiceImpl$MimdManagerHandler ) v2 ).obtainMessage ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;->obtainMessage(I)Landroid/os/Message;
	 /* .line 206 */
	 /* .local v2, "msg":Landroid/os/Message; */
	 v3 = this.this$0;
	 com.android.server.am.MimdManagerServiceImpl .-$$Nest$fgetmMimdManagerHandler ( v3 );
	 (( com.android.server.am.MimdManagerServiceImpl$MimdManagerHandler ) v3 ).sendMessage ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;->sendMessage(Landroid/os/Message;)Z
	 /* .line 209 */
} // .end local v1 # "state":I
} // .end local v2 # "msg":Landroid/os/Message;
} // :cond_0
return;
} // .end method
