public class com.android.server.am.MimdManagerServiceImpl implements com.android.server.am.MimdManagerServiceStub {
	 /* .source "MimdManagerServiceImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;, */
	 /* Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;, */
	 /* Lcom/android/server/am/MimdManagerServiceImpl$AppMsg; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Long ACTION_CODE_SHIFT;
private static final Long ACTION_CODE_SLEEP;
public static final java.lang.String ACTION_SLEEP_CHANGED;
private static final Boolean DBG_MIMD;
public static final java.lang.String EXTRA_STATE;
private static final java.lang.String MEMCG_PROCS_NODE;
private static final Long POLICY_TYPE_APPBG;
private static final Long POLICY_TYPE_APPDIE;
private static final Long POLICY_TYPE_APPFG;
private static final Long POLICY_TYPE_MISC;
private static final Long POLICY_TYPE_NONE;
public static final Integer STATE_ENTER_SLEEP;
private static final java.lang.String TAG;
private static final Long UID_CODE_SHIFT;
private static final java.lang.String UID_PREFIX;
private static java.lang.String mAppMemCgroupPath;
private static Integer mPolicyControlMaskLocal;
private static Boolean sEnableMimdService;
/* # instance fields */
private final java.lang.String MIMD_CONFIG_PATH;
private final java.lang.String MIMD_HWSERVICE_TRIGGER_PATH;
private java.util.Map appIndexMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
android.content.IntentFilter filter;
private android.app.IActivityManager mActivityManagerService;
private android.content.Context mContext;
private final java.lang.Object mDieLock;
private final java.lang.Object mFgLock;
public android.os.HandlerThread mHandlerThread;
private com.android.server.am.MimdManagerServiceImpl$MimdManagerHandler mMimdManagerHandler;
private final java.lang.Object mPolicyControlLock;
private final android.app.IProcessObserver$Stub mProcessObserver;
private android.content.BroadcastReceiver mSleepModeReceiver;
/* # direct methods */
static com.android.server.am.MimdManagerServiceImpl$MimdManagerHandler -$$Nest$fgetmMimdManagerHandler ( com.android.server.am.MimdManagerServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMimdManagerHandler;
} // .end method
static void -$$Nest$mappDied ( com.android.server.am.MimdManagerServiceImpl p0, Integer p1, Integer p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/am/MimdManagerServiceImpl;->appDied(II)V */
return;
} // .end method
static void -$$Nest$mentrySleepMode ( com.android.server.am.MimdManagerServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/MimdManagerServiceImpl;->entrySleepMode()V */
return;
} // .end method
static void -$$Nest$mforegroundChanged ( com.android.server.am.MimdManagerServiceImpl p0, Integer p1, Integer p2, Boolean p3 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/am/MimdManagerServiceImpl;->foregroundChanged(IIZ)V */
return;
} // .end method
static void -$$Nest$mmimdManagerInit ( com.android.server.am.MimdManagerServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/MimdManagerServiceImpl;->mimdManagerInit()V */
return;
} // .end method
static void -$$Nest$monAppProcessStart ( com.android.server.am.MimdManagerServiceImpl p0, Integer p1, Integer p2, java.lang.String p3 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/am/MimdManagerServiceImpl;->onAppProcessStart(IILjava/lang/String;)V */
return;
} // .end method
static com.android.server.am.MimdManagerServiceImpl ( ) {
/* .locals 2 */
/* .line 50 */
final String v0 = "/dev/memcg/mimd"; // const-string v0, "/dev/memcg/mimd"
/* .line 55 */
final String v0 = "MimdManagerServiceImpl"; // const-string v0, "MimdManagerServiceImpl"
int v1 = 3; // const/4 v1, 0x3
v0 = android.util.Log .isLoggable ( v0,v1 );
com.android.server.am.MimdManagerServiceImpl.DBG_MIMD = (v0!= 0);
/* .line 67 */
int v0 = 0; // const/4 v0, 0x0
/* .line 73 */
com.android.server.am.MimdManagerServiceImpl.sEnableMimdService = (v0!= 0);
return;
} // .end method
public com.android.server.am.MimdManagerServiceImpl ( ) {
/* .locals 1 */
/* .line 48 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 53 */
final String v0 = "/sys/module/perf_helper/mimd/mimdtrigger"; // const-string v0, "/sys/module/perf_helper/mimd/mimdtrigger"
this.MIMD_HWSERVICE_TRIGGER_PATH = v0;
/* .line 54 */
final String v0 = "/odm/etc/mimdconfig"; // const-string v0, "/odm/etc/mimdconfig"
this.MIMD_CONFIG_PATH = v0;
/* .line 57 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mPolicyControlLock = v0;
/* .line 58 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mFgLock = v0;
/* .line 59 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mDieLock = v0;
/* .line 71 */
int v0 = 0; // const/4 v0, 0x0
this.mMimdManagerHandler = v0;
/* .line 72 */
this.mHandlerThread = v0;
/* .line 103 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.appIndexMap = v0;
/* .line 198 */
/* new-instance v0, Lcom/android/server/am/MimdManagerServiceImpl$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/am/MimdManagerServiceImpl$1;-><init>(Lcom/android/server/am/MimdManagerServiceImpl;)V */
this.mSleepModeReceiver = v0;
/* .line 318 */
/* new-instance v0, Lcom/android/server/am/MimdManagerServiceImpl$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/am/MimdManagerServiceImpl$2;-><init>(Lcom/android/server/am/MimdManagerServiceImpl;)V */
this.mProcessObserver = v0;
return;
} // .end method
private Boolean ParseMimdconfigAppmap ( ) {
/* .locals 23 */
/* .line 422 */
/* move-object/from16 v1, p0 */
final String v0 = "index:"; // const-string v0, "index:"
final String v2 = "app:"; // const-string v2, "app:"
final String v3 = "read app map failed"; // const-string v3, "read app map failed"
/* new-instance v4, Ljava/io/File; */
final String v5 = "/odm/etc/mimdconfig"; // const-string v5, "/odm/etc/mimdconfig"
/* invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 423 */
/* .local v4, "file":Ljava/io/File; */
int v5 = 0; // const/4 v5, 0x0
/* .line 424 */
/* .local v5, "ret":Z */
v6 = (( java.io.File ) v4 ).exists ( ); // invoke-virtual {v4}, Ljava/io/File;->exists()Z
final String v7 = " "; // const-string v7, " "
final String v8 = "MimdManagerServiceImpl"; // const-string v8, "MimdManagerServiceImpl"
if ( v6 != null) { // if-eqz v6, :cond_a
/* .line 425 */
int v6 = 0; // const/4 v6, 0x0
/* .line 427 */
/* .local v6, "reader":Ljava/io/BufferedReader; */
try { // :try_start_0
	 /* new-instance v9, Ljava/io/BufferedReader; */
	 /* new-instance v10, Ljava/io/FileReader; */
	 /* invoke-direct {v10, v4}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V */
	 /* invoke-direct {v9, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
	 /* move-object v6, v9 */
	 /* .line 428 */
	 int v9 = 0; // const/4 v9, 0x0
	 /* .line 429 */
	 /* .local v9, "tmpString":Ljava/lang/String; */
	 int v10 = 0; // const/4 v10, 0x0
	 /* .line 430 */
	 /* .local v10, "appMapFlag":Z */
} // :goto_0
(( java.io.BufferedReader ) v6 ).readLine ( ); // invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_4 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_2 */
/* move-object v9, v11 */
if ( v11 != null) { // if-eqz v11, :cond_8
	 /* .line 431 */
	 int v11 = 0; // const/4 v11, 0x0
	 /* if-nez v10, :cond_1 */
	 /* .line 432 */
	 try { // :try_start_1
		 final String v12 = "[\\s]+"; // const-string v12, "[\\s]+"
		 (( java.lang.String ) v9 ).split ( v12 ); // invoke-virtual {v9, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
		 /* .line 433 */
		 /* .local v12, "words":[Ljava/lang/String; */
		 /* aget-object v11, v12, v11 */
		 final String v13 = "app_map"; // const-string v13, "app_map"
		 v11 = 		 (( java.lang.String ) v11 ).equals ( v13 ); // invoke-virtual {v11, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v11 != null) { // if-eqz v11, :cond_0
			 /* .line 434 */
			 int v10 = 1; // const/4 v10, 0x1
			 /* .line 433 */
		 } // :cond_0
		 /* move-object/from16 v18, v0 */
		 /* move-object/from16 v17, v2 */
		 /* move-object/from16 v19, v4 */
		 /* move/from16 v20, v5 */
		 /* goto/16 :goto_2 */
		 /* .line 468 */
	 } // .end local v9 # "tmpString":Ljava/lang/String;
} // .end local v10 # "appMapFlag":Z
} // .end local v12 # "words":[Ljava/lang/String;
/* :catchall_0 */
/* move-exception v0 */
/* move-object v2, v0 */
/* move-object/from16 v19, v4 */
/* move/from16 v20, v5 */
/* goto/16 :goto_7 */
/* .line 465 */
/* :catch_0 */
/* move-exception v0 */
/* move-object/from16 v19, v4 */
/* move/from16 v20, v5 */
/* goto/16 :goto_6 */
/* .line 438 */
/* .restart local v9 # "tmpString":Ljava/lang/String; */
/* .restart local v10 # "appMapFlag":Z */
} // :cond_1
int v12 = 1; // const/4 v12, 0x1
/* if-ne v10, v12, :cond_2 */
/* const-string/jumbo v13, "}" */
v13 = (( java.lang.String ) v9 ).lastIndexOf ( v13 ); // invoke-virtual {v9, v13}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* if-ltz v13, :cond_2 */
/* .line 439 */
/* move-object/from16 v19, v4 */
/* move/from16 v20, v5 */
/* goto/16 :goto_3 */
/* .line 441 */
} // :cond_2
try { // :try_start_2
v13 = (( java.lang.String ) v9 ).lastIndexOf ( v2 ); // invoke-virtual {v9, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I
/* if-ltz v13, :cond_7 */
v13 = (( java.lang.String ) v9 ).lastIndexOf ( v0 ); // invoke-virtual {v9, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I
/* if-ltz v13, :cond_7 */
/* .line 442 */
final String v13 = "\\s+"; // const-string v13, "\\s+"
java.util.regex.Pattern .compile ( v13 );
/* .line 443 */
/* .local v13, "pattern":Ljava/util/regex/Pattern; */
(( java.util.regex.Pattern ) v13 ).matcher ( v9 ); // invoke-virtual {v13, v9}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
/* .line 444 */
/* .local v14, "matcher":Ljava/util/regex/Matcher; */
(( java.util.regex.Matcher ) v14 ).replaceAll ( v7 ); // invoke-virtual {v14, v7}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;
(( java.lang.String ) v15 ).trim ( ); // invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;
/* move-object v9, v15 */
/* .line 445 */
(( java.lang.String ) v9 ).split ( v7 ); // invoke-virtual {v9, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 446 */
/* .local v15, "sStr":[Ljava/lang/String; */
/* array-length v12, v15 */
int v11 = 2; // const/4 v11, 0x2
/* if-ne v12, v11, :cond_6 */
/* .line 447 */
int v11 = 0; // const/4 v11, 0x0
/* aget-object v12, v15, v11 */
v11 = (( java.lang.String ) v12 ).lastIndexOf ( v2 ); // invoke-virtual {v12, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I
/* .line 448 */
/* .local v11, "pkgPos":I */
/* move-object/from16 v17, v2 */
int v12 = 1; // const/4 v12, 0x1
/* aget-object v2, v15, v12 */
v2 = (( java.lang.String ) v2 ).lastIndexOf ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I
/* .line 449 */
/* .local v2, "idxPos":I */
/* if-ltz v11, :cond_5 */
/* if-ltz v2, :cond_5 */
/* .line 450 */
/* move-object/from16 v18, v0 */
int v12 = 0; // const/4 v12, 0x0
/* aget-object v0, v15, v12 */
(( java.lang.String ) v0 ).substring ( v11 ); // invoke-virtual {v0, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;
(( java.lang.String ) v0 ).trim ( ); // invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;
/* .line 451 */
/* .local v0, "pkgName":Ljava/lang/String; */
int v12 = 1; // const/4 v12, 0x1
/* aget-object v12, v15, v12 */
(( java.lang.String ) v12 ).substring ( v2 ); // invoke-virtual {v12, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;
(( java.lang.String ) v12 ).trim ( ); // invoke-virtual {v12}, Ljava/lang/String;->trim()Ljava/lang/String;
/* .line 453 */
/* .local v12, "pkgIndex":Ljava/lang/String; */
/* move/from16 v16, v2 */
} // .end local v2 # "idxPos":I
/* .local v16, "idxPos":I */
v2 = (( java.lang.String ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/String;->length()I
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_4 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_2 */
/* move-object/from16 v19, v4 */
} // .end local v4 # "file":Ljava/io/File;
/* .local v19, "file":Ljava/io/File; */
int v4 = 4; // const/4 v4, 0x4
/* if-le v2, v4, :cond_4 */
try { // :try_start_3
v2 = (( java.lang.String ) v12 ).length ( ); // invoke-virtual {v12}, Ljava/lang/String;->length()I
int v4 = 6; // const/4 v4, 0x6
/* if-le v2, v4, :cond_4 */
/* .line 454 */
final String v2 = "[0-9]*"; // const-string v2, "[0-9]*"
java.util.regex.Pattern .compile ( v2 );
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_2 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* .line 455 */
/* .local v2, "patternNum":Ljava/util/regex/Pattern; */
/* move/from16 v20, v5 */
} // .end local v5 # "ret":Z
/* .local v20, "ret":Z */
try { // :try_start_4
(( java.lang.String ) v12 ).substring ( v4 ); // invoke-virtual {v12, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;
(( java.util.regex.Pattern ) v2 ).matcher ( v5 ); // invoke-virtual {v2, v5}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
/* .line 456 */
/* .local v5, "isNum":Ljava/util/regex/Matcher; */
v21 = (( java.util.regex.Matcher ) v5 ).matches ( ); // invoke-virtual {v5}, Ljava/util/regex/Matcher;->matches()Z
if ( v21 != null) { // if-eqz v21, :cond_3
/* .line 457 */
/* new-instance v4, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg; */
/* move-object/from16 v22, v2 */
int v2 = 6; // const/4 v2, 0x6
} // .end local v2 # "patternNum":Ljava/util/regex/Pattern;
/* .local v22, "patternNum":Ljava/util/regex/Pattern; */
(( java.lang.String ) v12 ).substring ( v2 ); // invoke-virtual {v12, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;
v2 = java.lang.Integer .parseInt ( v2 );
/* move-object/from16 v21, v5 */
int v5 = 0; // const/4 v5, 0x0
} // .end local v5 # "isNum":Ljava/util/regex/Matcher;
/* .local v21, "isNum":Ljava/util/regex/Matcher; */
/* invoke-direct {v4, v1, v2, v5}, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;-><init>(Lcom/android/server/am/MimdManagerServiceImpl;II)V */
/* move-object v2, v4 */
/* .line 458 */
/* .local v2, "appMsg":Lcom/android/server/am/MimdManagerServiceImpl$AppMsg; */
v4 = this.appIndexMap;
int v5 = 4; // const/4 v5, 0x4
(( java.lang.String ) v0 ).substring ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_1 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_3 */
/* .line 456 */
} // .end local v21 # "isNum":Ljava/util/regex/Matcher;
} // .end local v22 # "patternNum":Ljava/util/regex/Pattern;
/* .local v2, "patternNum":Ljava/util/regex/Pattern; */
/* .restart local v5 # "isNum":Ljava/util/regex/Matcher; */
} // :cond_3
/* move-object/from16 v22, v2 */
/* move-object/from16 v21, v5 */
} // .end local v2 # "patternNum":Ljava/util/regex/Pattern;
} // .end local v5 # "isNum":Ljava/util/regex/Matcher;
/* .restart local v21 # "isNum":Ljava/util/regex/Matcher; */
/* .restart local v22 # "patternNum":Ljava/util/regex/Pattern; */
/* .line 465 */
} // .end local v0 # "pkgName":Ljava/lang/String;
} // .end local v9 # "tmpString":Ljava/lang/String;
} // .end local v10 # "appMapFlag":Z
} // .end local v11 # "pkgPos":I
} // .end local v12 # "pkgIndex":Ljava/lang/String;
} // .end local v13 # "pattern":Ljava/util/regex/Pattern;
} // .end local v14 # "matcher":Ljava/util/regex/Matcher;
} // .end local v15 # "sStr":[Ljava/lang/String;
} // .end local v16 # "idxPos":I
} // .end local v21 # "isNum":Ljava/util/regex/Matcher;
} // .end local v22 # "patternNum":Ljava/util/regex/Pattern;
/* :catch_1 */
/* move-exception v0 */
/* goto/16 :goto_6 */
/* .line 468 */
} // .end local v20 # "ret":Z
/* .local v5, "ret":Z */
/* :catchall_1 */
/* move-exception v0 */
/* move/from16 v20, v5 */
/* move-object v2, v0 */
} // .end local v5 # "ret":Z
/* .restart local v20 # "ret":Z */
/* goto/16 :goto_7 */
/* .line 465 */
} // .end local v20 # "ret":Z
/* .restart local v5 # "ret":Z */
/* :catch_2 */
/* move-exception v0 */
/* move/from16 v20, v5 */
} // .end local v5 # "ret":Z
/* .restart local v20 # "ret":Z */
/* goto/16 :goto_6 */
/* .line 453 */
} // .end local v20 # "ret":Z
/* .restart local v0 # "pkgName":Ljava/lang/String; */
/* .restart local v5 # "ret":Z */
/* .restart local v9 # "tmpString":Ljava/lang/String; */
/* .restart local v10 # "appMapFlag":Z */
/* .restart local v11 # "pkgPos":I */
/* .restart local v12 # "pkgIndex":Ljava/lang/String; */
/* .restart local v13 # "pattern":Ljava/util/regex/Pattern; */
/* .restart local v14 # "matcher":Ljava/util/regex/Matcher; */
/* .restart local v15 # "sStr":[Ljava/lang/String; */
/* .restart local v16 # "idxPos":I */
} // :cond_4
/* move/from16 v20, v5 */
} // .end local v5 # "ret":Z
/* .restart local v20 # "ret":Z */
/* .line 449 */
} // .end local v0 # "pkgName":Ljava/lang/String;
} // .end local v12 # "pkgIndex":Ljava/lang/String;
} // .end local v16 # "idxPos":I
} // .end local v19 # "file":Ljava/io/File;
} // .end local v20 # "ret":Z
/* .local v2, "idxPos":I */
/* .restart local v4 # "file":Ljava/io/File; */
/* .restart local v5 # "ret":Z */
} // :cond_5
/* move-object/from16 v18, v0 */
/* move/from16 v16, v2 */
/* move-object/from16 v19, v4 */
/* move/from16 v20, v5 */
} // .end local v2 # "idxPos":I
} // .end local v4 # "file":Ljava/io/File;
} // .end local v5 # "ret":Z
/* .restart local v16 # "idxPos":I */
/* .restart local v19 # "file":Ljava/io/File; */
/* .restart local v20 # "ret":Z */
/* .line 446 */
} // .end local v11 # "pkgPos":I
} // .end local v16 # "idxPos":I
} // .end local v19 # "file":Ljava/io/File;
} // .end local v20 # "ret":Z
/* .restart local v4 # "file":Ljava/io/File; */
/* .restart local v5 # "ret":Z */
} // :cond_6
/* move-object/from16 v18, v0 */
/* move-object/from16 v17, v2 */
/* move-object/from16 v19, v4 */
/* move/from16 v20, v5 */
/* .line 463 */
} // .end local v4 # "file":Ljava/io/File;
} // .end local v5 # "ret":Z
} // .end local v13 # "pattern":Ljava/util/regex/Pattern;
} // .end local v14 # "matcher":Ljava/util/regex/Matcher;
} // .end local v15 # "sStr":[Ljava/lang/String;
/* .restart local v19 # "file":Ljava/io/File; */
/* .restart local v20 # "ret":Z */
} // :goto_1
/* move-object/from16 v2, v17 */
/* move-object/from16 v0, v18 */
/* move-object/from16 v4, v19 */
/* move/from16 v5, v20 */
/* goto/16 :goto_0 */
/* .line 441 */
} // .end local v19 # "file":Ljava/io/File;
} // .end local v20 # "ret":Z
/* .restart local v4 # "file":Ljava/io/File; */
/* .restart local v5 # "ret":Z */
} // :cond_7
/* move-object/from16 v18, v0 */
/* move-object/from16 v17, v2 */
/* move-object/from16 v19, v4 */
/* move/from16 v20, v5 */
/* .line 430 */
} // .end local v4 # "file":Ljava/io/File;
} // .end local v5 # "ret":Z
/* .restart local v19 # "file":Ljava/io/File; */
/* .restart local v20 # "ret":Z */
} // :goto_2
/* move-object/from16 v2, v17 */
/* move-object/from16 v0, v18 */
/* move-object/from16 v4, v19 */
/* move/from16 v5, v20 */
/* goto/16 :goto_0 */
} // .end local v19 # "file":Ljava/io/File;
} // .end local v20 # "ret":Z
/* .restart local v4 # "file":Ljava/io/File; */
/* .restart local v5 # "ret":Z */
} // :cond_8
/* move-object/from16 v19, v4 */
/* move/from16 v20, v5 */
/* .line 468 */
} // .end local v4 # "file":Ljava/io/File;
} // .end local v5 # "ret":Z
} // .end local v9 # "tmpString":Ljava/lang/String;
} // .end local v10 # "appMapFlag":Z
/* .restart local v19 # "file":Ljava/io/File; */
/* .restart local v20 # "ret":Z */
} // :goto_3
/* nop */
/* .line 470 */
try { // :try_start_5
(( java.io.BufferedReader ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
/* :try_end_5 */
/* .catch Ljava/io/IOException; {:try_start_5 ..:try_end_5} :catch_3 */
/* .line 473 */
} // :goto_4
/* goto/16 :goto_9 */
/* .line 471 */
/* :catch_3 */
/* move-exception v0 */
/* move-object v2, v0 */
/* move-object v0, v2 */
/* .line 472 */
/* .local v0, "e":Ljava/io/IOException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
} // :goto_5
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v8,v2 );
} // .end local v0 # "e":Ljava/io/IOException;
/* .line 468 */
} // .end local v19 # "file":Ljava/io/File;
} // .end local v20 # "ret":Z
/* .restart local v4 # "file":Ljava/io/File; */
/* .restart local v5 # "ret":Z */
/* :catchall_2 */
/* move-exception v0 */
/* move-object/from16 v19, v4 */
/* move/from16 v20, v5 */
/* move-object v2, v0 */
} // .end local v4 # "file":Ljava/io/File;
} // .end local v5 # "ret":Z
/* .restart local v19 # "file":Ljava/io/File; */
/* .restart local v20 # "ret":Z */
/* .line 465 */
} // .end local v19 # "file":Ljava/io/File;
} // .end local v20 # "ret":Z
/* .restart local v4 # "file":Ljava/io/File; */
/* .restart local v5 # "ret":Z */
/* :catch_4 */
/* move-exception v0 */
/* move-object/from16 v19, v4 */
/* move/from16 v20, v5 */
/* .line 466 */
} // .end local v4 # "file":Ljava/io/File;
} // .end local v5 # "ret":Z
/* .restart local v0 # "e":Ljava/io/IOException; */
/* .restart local v19 # "file":Ljava/io/File; */
/* .restart local v20 # "ret":Z */
} // :goto_6
try { // :try_start_6
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v8,v2 );
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_3 */
/* .line 468 */
/* nop */
} // .end local v0 # "e":Ljava/io/IOException;
if ( v6 != null) { // if-eqz v6, :cond_b
/* .line 470 */
try { // :try_start_7
(( java.io.BufferedReader ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
/* :try_end_7 */
/* .catch Ljava/io/IOException; {:try_start_7 ..:try_end_7} :catch_5 */
/* .line 471 */
/* :catch_5 */
/* move-exception v0 */
/* move-object v2, v0 */
/* move-object v0, v2 */
/* .line 472 */
/* .restart local v0 # "e":Ljava/io/IOException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 468 */
} // .end local v0 # "e":Ljava/io/IOException;
/* :catchall_3 */
/* move-exception v0 */
/* move-object v2, v0 */
} // :goto_7
if ( v6 != null) { // if-eqz v6, :cond_9
/* .line 470 */
try { // :try_start_8
(( java.io.BufferedReader ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
/* :try_end_8 */
/* .catch Ljava/io/IOException; {:try_start_8 ..:try_end_8} :catch_6 */
/* .line 473 */
/* .line 471 */
/* :catch_6 */
/* move-exception v0 */
/* move-object v4, v0 */
/* move-object v0, v4 */
/* .line 472 */
/* .restart local v0 # "e":Ljava/io/IOException; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v8,v3 );
/* .line 475 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :cond_9
} // :goto_8
/* throw v2 */
/* .line 424 */
} // .end local v6 # "reader":Ljava/io/BufferedReader;
} // .end local v19 # "file":Ljava/io/File;
} // .end local v20 # "ret":Z
/* .restart local v4 # "file":Ljava/io/File; */
/* .restart local v5 # "ret":Z */
} // :cond_a
/* move-object/from16 v19, v4 */
/* move/from16 v20, v5 */
/* .line 478 */
} // .end local v4 # "file":Ljava/io/File;
} // .end local v5 # "ret":Z
/* .restart local v19 # "file":Ljava/io/File; */
/* .restart local v20 # "ret":Z */
} // :cond_b
} // :goto_9
v0 = v0 = this.appIndexMap;
/* if-lez v0, :cond_e */
/* .line 479 */
v0 = this.appIndexMap;
v2 = } // :goto_a
if ( v2 != null) { // if-eqz v2, :cond_d
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 480 */
/* .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;>;" */
/* sget-boolean v3, Lcom/android/server/am/MimdManagerServiceImpl;->DBG_MIMD:Z */
if ( v3 != null) { // if-eqz v3, :cond_c
/* .line 481 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* check-cast v4, Ljava/lang/String; */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v7 ); // invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v4, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg; */
/* iget v4, v4, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;->mAppIdx:I */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v8,v3 );
/* .line 482 */
} // .end local v2 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;>;"
} // :cond_c
/* .line 483 */
} // :cond_d
int v5 = 1; // const/4 v5, 0x1
} // .end local v20 # "ret":Z
/* .restart local v5 # "ret":Z */
/* .line 478 */
} // .end local v5 # "ret":Z
/* .restart local v20 # "ret":Z */
} // :cond_e
/* move/from16 v5, v20 */
/* .line 486 */
} // .end local v20 # "ret":Z
/* .restart local v5 # "ret":Z */
} // :goto_b
} // .end method
private Boolean PolicyControlTrigger ( Long p0 ) {
/* .locals 6 */
/* .param p1, "mPolicyType" # J */
/* .line 501 */
v0 = this.mPolicyControlLock;
/* monitor-enter v0 */
/* .line 502 */
int v1 = 0; // const/4 v1, 0x0
/* .line 503 */
/* .local v1, "mIsTrigger":I */
/* const-wide/16 v2, 0x3e8 */
try { // :try_start_0
/* rem-long v2, p1, v2 */
/* long-to-int v2, v2 */
/* .line 504 */
/* .local v2, "i":I */
/* .line 506 */
/* .local v3, "mTmpMask":I */
/* const-wide/16 v4, 0x0 */
/* cmp-long v4, p1, v4 */
int v5 = 0; // const/4 v5, 0x0
/* if-nez v4, :cond_0 */
/* .line 507 */
/* monitor-exit v0 */
/* .line 510 */
} // :cond_0
} // :goto_0
/* add-int/lit8 v4, v2, -0x1 */
} // .end local v2 # "i":I
/* .local v4, "i":I */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 511 */
/* div-int/lit8 v2, v3, 0xa */
/* move v3, v2 */
/* move v2, v4 */
/* .line 512 */
} // :cond_1
/* rem-int/lit8 v2, v3, 0xa */
/* move v1, v2 */
/* .line 514 */
/* if-nez v1, :cond_2 */
/* .line 515 */
/* monitor-exit v0 */
/* .line 518 */
} // :cond_2
/* invoke-direct {p0, p1, p2}, Lcom/android/server/am/MimdManagerServiceImpl;->TriggerPolicy(J)V */
/* .line 519 */
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 520 */
} // .end local v1 # "mIsTrigger":I
} // .end local v3 # "mTmpMask":I
} // .end local v4 # "i":I
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void TriggerPolicy ( Long p0 ) {
/* .locals 7 */
/* .param p1, "mPolicyType" # J */
/* .line 524 */
final String v0 = "close mimdtrigger failed"; // const-string v0, "close mimdtrigger failed"
final String v1 = "MimdManagerServiceImpl"; // const-string v1, "MimdManagerServiceImpl"
/* new-instance v2, Ljava/io/File; */
final String v3 = "/sys/module/perf_helper/mimd/mimdtrigger"; // const-string v3, "/sys/module/perf_helper/mimd/mimdtrigger"
/* invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 525 */
/* .local v2, "file":Ljava/io/File; */
v3 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 526 */
int v3 = 0; // const/4 v3, 0x0
/* .line 528 */
/* .local v3, "fos":Ljava/io/FileOutputStream; */
try { // :try_start_0
/* new-instance v4, Ljava/io/FileOutputStream; */
/* invoke-direct {v4, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V */
/* move-object v3, v4 */
/* .line 529 */
java.lang.Long .toString ( p1,p2 );
(( java.lang.String ) v4 ).getBytes ( ); // invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B
(( java.io.FileOutputStream ) v3 ).write ( v4 ); // invoke-virtual {v3, v4}, Ljava/io/FileOutputStream;->write([B)V
/* .line 530 */
/* sget-boolean v4, Lcom/android/server/am/MimdManagerServiceImpl;->DBG_MIMD:Z */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 531 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "write mimdtrigger string " */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1, p2 ); // invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v4 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 535 */
} // :cond_0
/* nop */
/* .line 537 */
try { // :try_start_1
(( java.io.FileOutputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 540 */
} // :goto_0
/* .line 538 */
/* :catch_0 */
/* move-exception v4 */
/* .line 539 */
/* .local v4, "e":Ljava/io/IOException; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
} // :goto_1
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v0 );
} // .end local v4 # "e":Ljava/io/IOException;
/* .line 535 */
/* :catchall_0 */
/* move-exception v4 */
/* .line 532 */
/* :catch_1 */
/* move-exception v4 */
/* .line 533 */
/* .local v4, "e":Ljava/lang/Exception; */
try { // :try_start_2
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "write mimdtrigger failed" */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v5 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 535 */
/* nop */
} // .end local v4 # "e":Ljava/lang/Exception;
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 537 */
try { // :try_start_3
(( java.io.FileOutputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_2 */
/* .line 538 */
/* :catch_2 */
/* move-exception v4 */
/* .line 539 */
/* .local v4, "e":Ljava/io/IOException; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 535 */
} // .end local v4 # "e":Ljava/io/IOException;
} // :goto_2
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 537 */
try { // :try_start_4
(( java.io.FileOutputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_3 */
/* .line 540 */
/* .line 538 */
/* :catch_3 */
/* move-exception v5 */
/* .line 539 */
/* .local v5, "e":Ljava/io/IOException; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v0 );
/* .line 542 */
} // .end local v5 # "e":Ljava/io/IOException;
} // :cond_1
} // :goto_3
/* throw v4 */
/* .line 544 */
} // .end local v3 # "fos":Ljava/io/FileOutputStream;
} // :cond_2
} // :goto_4
return;
} // .end method
private void appDied ( Integer p0, Integer p1 ) {
/* .locals 12 */
/* .param p1, "pid" # I */
/* .param p2, "uid" # I */
/* .line 299 */
v0 = this.mDieLock;
/* monitor-enter v0 */
/* .line 300 */
try { // :try_start_0
/* invoke-direct {p0, p2}, Lcom/android/server/am/MimdManagerServiceImpl;->getPackageNameByUid(I)Ljava/lang/String; */
/* .line 301 */
/* .local v1, "pkgName":Ljava/lang/String; */
v2 = v2 = this.appIndexMap;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 302 */
v2 = this.appIndexMap;
/* check-cast v2, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg; */
/* .line 303 */
/* .local v2, "appMsg":Lcom/android/server/am/MimdManagerServiceImpl$AppMsg; */
/* iget v3, v2, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;->mPid:I */
/* if-lez v3, :cond_0 */
/* iget v3, v2, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;->mPid:I */
/* if-ne v3, p1, :cond_0 */
/* .line 304 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
java.lang.Integer .toString ( p1 );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " died"; // const-string v4, " died"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const-wide/16 v4, 0x8 */
android.os.Trace .traceBegin ( v4,v5,v3 );
/* .line 305 */
/* iget v3, v2, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;->mAppIdx:I */
/* .line 308 */
/* .local v3, "appidx":I */
int v6 = 0; // const/4 v6, 0x0
(( com.android.server.am.MimdManagerServiceImpl$AppMsg ) v2 ).UpdatePid ( v6 ); // invoke-virtual {v2, v6}, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;->UpdatePid(I)V
/* .line 309 */
/* int-to-long v6, v3 */
/* const-wide/16 v8, 0x3e8 */
/* mul-long/2addr v6, v8 */
/* const-wide/16 v8, 0x4 */
/* add-long/2addr v6, v8 */
/* int-to-long v8, p2 */
/* const-wide/32 v10, 0x186a0 */
/* mul-long/2addr v8, v10 */
/* add-long/2addr v6, v8 */
/* .line 310 */
/* .local v6, "tmpPT":J */
/* invoke-direct {p0, v6, v7}, Lcom/android/server/am/MimdManagerServiceImpl;->PolicyControlTrigger(J)Z */
/* .line 311 */
final String v8 = "MimdManagerServiceImpl"; // const-string v8, "MimdManagerServiceImpl"
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "ProcessDied, pid=:"; // const-string v10, "ProcessDied, pid=:"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( p1 ); // invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = " uid="; // const-string v10, " uid="
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( p2 ); // invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = " type="; // const-string v10, " type="
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v6, v7 ); // invoke-virtual {v9, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v8,v9 );
/* .line 312 */
android.os.Trace .traceEnd ( v4,v5 );
/* .line 315 */
} // .end local v1 # "pkgName":Ljava/lang/String;
} // .end local v2 # "appMsg":Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;
} // .end local v3 # "appidx":I
} // .end local v6 # "tmpPT":J
} // :cond_0
/* monitor-exit v0 */
/* .line 316 */
return;
/* .line 315 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private Boolean checkAppCgroup ( Integer p0 ) {
/* .locals 6 */
/* .param p1, "uid" # I */
/* .line 359 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
v1 = com.android.server.am.MimdManagerServiceImpl.mAppMemCgroupPath;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "/"; // const-string v1, "/"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "uid_" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 360 */
/* .local v0, "uidPath":Ljava/lang/String; */
/* new-instance v1, Ljava/io/File; */
/* invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 361 */
/* .local v1, "appCgDir":Ljava/io/File; */
v2 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
int v3 = 1; // const/4 v3, 0x1
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 362 */
/* .line 365 */
} // :cond_0
v2 = (( java.io.File ) v1 ).mkdir ( ); // invoke-virtual {v1}, Ljava/io/File;->mkdir()Z
final String v4 = "MimdManagerServiceImpl"; // const-string v4, "MimdManagerServiceImpl"
int v5 = 0; // const/4 v5, 0x0
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 366 */
(( java.io.File ) v1 ).setReadable ( v3, v5 ); // invoke-virtual {v1, v3, v5}, Ljava/io/File;->setReadable(ZZ)Z
/* .line 367 */
(( java.io.File ) v1 ).setWritable ( v3, v3 ); // invoke-virtual {v1, v3, v3}, Ljava/io/File;->setWritable(ZZ)Z
/* .line 368 */
(( java.io.File ) v1 ).setExecutable ( v3, v5 ); // invoke-virtual {v1, v3, v5}, Ljava/io/File;->setExecutable(ZZ)Z
/* .line 369 */
/* sget-boolean v2, Lcom/android/server/am/MimdManagerServiceImpl;->DBG_MIMD:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 370 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "create app cgroup succeed, path:"; // const-string v5, "create app cgroup succeed, path:"
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v4,v2 );
/* .line 371 */
} // :cond_1
/* .line 373 */
} // :cond_2
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "create app cgroup failed, path:"; // const-string v3, "create app cgroup failed, path:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v4,v2 );
/* .line 374 */
} // .end method
private static void deleteFolder ( java.io.File p0 ) {
/* .locals 4 */
/* .param p0, "folder" # Ljava/io/File; */
/* .line 378 */
v0 = (( java.io.File ) p0 ).isDirectory ( ); // invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 379 */
(( java.io.File ) p0 ).listFiles ( ); // invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;
/* .line 380 */
/* .local v0, "files":[Ljava/io/File; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 381 */
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* if-ge v2, v1, :cond_0 */
/* aget-object v3, v0, v2 */
/* .line 382 */
/* .local v3, "file":Ljava/io/File; */
com.android.server.am.MimdManagerServiceImpl .deleteFolder ( v3 );
/* .line 381 */
} // .end local v3 # "file":Ljava/io/File;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 386 */
} // .end local v0 # "files":[Ljava/io/File;
} // :cond_0
(( java.io.File ) p0 ).delete ( ); // invoke-virtual {p0}, Ljava/io/File;->delete()Z
/* .line 387 */
return;
} // .end method
private void entrySleepMode ( ) {
/* .locals 5 */
/* .line 213 */
final String v0 = "entry sleep"; // const-string v0, "entry sleep"
/* const-wide/16 v1, 0x8 */
android.os.Trace .traceBegin ( v1,v2,v0 );
/* .line 214 */
/* const-wide/16 v3, 0x3e9 */
v0 = /* invoke-direct {p0, v3, v4}, Lcom/android/server/am/MimdManagerServiceImpl;->PolicyControlTrigger(J)Z */
/* .line 215 */
/* .local v0, "res":Z */
/* sget-boolean v3, Lcom/android/server/am/MimdManagerServiceImpl;->DBG_MIMD:Z */
if ( v3 != null) { // if-eqz v3, :cond_0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 216 */
final String v3 = "MimdManagerServiceImpl"; // const-string v3, "MimdManagerServiceImpl"
final String v4 = "enable sleep mode to reclaim memory!"; // const-string v4, "enable sleep mode to reclaim memory!"
android.util.Log .d ( v3,v4 );
/* .line 217 */
} // :cond_0
android.os.Trace .traceEnd ( v1,v2 );
/* .line 218 */
return;
} // .end method
private void foregroundChanged ( Integer p0, Integer p1, Boolean p2 ) {
/* .locals 12 */
/* .param p1, "pid" # I */
/* .param p2, "uid" # I */
/* .param p3, "foreground" # Z */
/* .line 268 */
v0 = this.mFgLock;
/* monitor-enter v0 */
/* .line 269 */
try { // :try_start_0
/* invoke-direct {p0, p2}, Lcom/android/server/am/MimdManagerServiceImpl;->getPackageNameByUid(I)Ljava/lang/String; */
/* .line 270 */
/* .local v1, "pkgName":Ljava/lang/String; */
v2 = v2 = this.appIndexMap;
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 271 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
java.lang.Integer .toString ( p1 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " to fg "; // const-string v3, " to fg "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.Boolean .toString ( p3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const-wide/16 v3, 0x8 */
android.os.Trace .traceBegin ( v3,v4,v2 );
/* .line 272 */
v2 = this.appIndexMap;
/* check-cast v2, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg; */
/* .line 273 */
/* .local v2, "appMsg":Lcom/android/server/am/MimdManagerServiceImpl$AppMsg; */
/* iget v5, v2, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;->mPid:I */
/* if-nez v5, :cond_0 */
/* .line 274 */
(( com.android.server.am.MimdManagerServiceImpl$AppMsg ) v2 ).UpdatePid ( p1 ); // invoke-virtual {v2, p1}, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;->UpdatePid(I)V
/* .line 276 */
} // :cond_0
/* iget v5, v2, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;->mUid:I */
/* if-eq v5, p2, :cond_1 */
/* .line 277 */
/* iget v5, v2, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;->mUid:I */
com.android.server.am.MimdManagerServiceImpl .removeEmptyMemcgDir ( v5 );
/* .line 278 */
/* iput p2, v2, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;->mUid:I */
/* .line 281 */
} // :cond_1
/* iget v5, v2, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;->mAppIdx:I */
/* .line 283 */
/* .local v5, "appidx":I */
/* const-wide/32 v6, 0x186a0 */
/* const-wide/16 v8, 0x3e8 */
if ( p3 != null) { // if-eqz p3, :cond_2
/* .line 284 */
/* int-to-long v10, v5 */
/* mul-long/2addr v10, v8 */
/* const-wide/16 v8, 0x2 */
/* add-long/2addr v10, v8 */
/* int-to-long v8, p2 */
/* mul-long/2addr v8, v6 */
/* add-long/2addr v10, v8 */
/* .line 285 */
/* .local v10, "tmpPT":J */
/* sget-boolean v6, Lcom/android/server/am/MimdManagerServiceImpl;->DBG_MIMD:Z */
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 286 */
final String v6 = "MimdManagerServiceImpl"; // const-string v6, "MimdManagerServiceImpl"
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Foreground changed, foreground pid=:"; // const-string v8, "Foreground changed, foreground pid=:"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( p1 ); // invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v8 = " Pkg="; // const-string v8, " Pkg="
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v1 ); // invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v6,v7 );
/* .line 288 */
} // .end local v10 # "tmpPT":J
} // :cond_2
/* int-to-long v10, v5 */
/* mul-long/2addr v10, v8 */
/* const-wide/16 v8, 0x3 */
/* add-long/2addr v10, v8 */
/* int-to-long v8, p2 */
/* mul-long/2addr v8, v6 */
/* add-long/2addr v10, v8 */
/* .line 289 */
/* .restart local v10 # "tmpPT":J */
/* sget-boolean v6, Lcom/android/server/am/MimdManagerServiceImpl;->DBG_MIMD:Z */
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 290 */
final String v6 = "MimdManagerServiceImpl"; // const-string v6, "MimdManagerServiceImpl"
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Foreground changed, background pid=:"; // const-string v8, "Foreground changed, background pid=:"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( p1 ); // invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v8 = " Pkg="; // const-string v8, " Pkg="
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v1 ); // invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v6,v7 );
/* .line 292 */
} // :cond_3
} // :goto_0
/* invoke-direct {p0, v10, v11}, Lcom/android/server/am/MimdManagerServiceImpl;->PolicyControlTrigger(J)Z */
/* .line 293 */
android.os.Trace .traceEnd ( v3,v4 );
/* .line 295 */
} // .end local v1 # "pkgName":Ljava/lang/String;
} // .end local v2 # "appMsg":Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;
} // .end local v5 # "appidx":I
} // .end local v10 # "tmpPT":J
} // :cond_4
/* monitor-exit v0 */
/* .line 296 */
return;
/* .line 295 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private java.lang.String getPackageNameByUid ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 491 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
(( android.content.pm.PackageManager ) v0 ).getPackagesForUid ( p1 ); // invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;
/* .line 492 */
/* .local v0, "pkgs":[Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* array-length v1, v0 */
/* if-lez v1, :cond_0 */
/* .line 493 */
int v1 = 0; // const/4 v1, 0x0
/* aget-object v1, v0, v1 */
/* .local v1, "packageName":Ljava/lang/String; */
/* .line 495 */
} // .end local v1 # "packageName":Ljava/lang/String;
} // :cond_0
java.lang.Integer .toString ( p1 );
/* .line 497 */
/* .restart local v1 # "packageName":Ljava/lang/String; */
} // :goto_0
} // .end method
private void getPolicyControlMaskLocal ( ) {
/* .locals 15 */
/* .line 229 */
final String v0 = "PolicyControlMaskLocal:"; // const-string v0, "PolicyControlMaskLocal:"
final String v1 = "read PolicyControlMaskLocal failed"; // const-string v1, "read PolicyControlMaskLocal failed"
final String v2 = "MimdManagerServiceImpl"; // const-string v2, "MimdManagerServiceImpl"
int v3 = 0; // const/4 v3, 0x0
/* .line 230 */
/* .local v3, "br":Ljava/io/BufferedReader; */
/* new-instance v4, Ljava/io/File; */
final String v5 = "/odm/etc/mimdconfig"; // const-string v5, "/odm/etc/mimdconfig"
/* invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 231 */
/* .local v4, "file":Ljava/io/File; */
v5 = (( java.io.File ) v4 ).exists ( ); // invoke-virtual {v4}, Ljava/io/File;->exists()Z
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 232 */
int v5 = 0; // const/4 v5, 0x0
/* .line 234 */
/* .local v5, "reader":Ljava/io/BufferedReader; */
try { // :try_start_0
/* new-instance v6, Ljava/io/BufferedReader; */
/* new-instance v7, Ljava/io/FileReader; */
/* invoke-direct {v7, v4}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V */
/* invoke-direct {v6, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* move-object v5, v6 */
/* .line 235 */
int v6 = 0; // const/4 v6, 0x0
/* .line 236 */
/* .local v6, "tmpString":Ljava/lang/String; */
} // :cond_0
} // :goto_0
(( java.io.BufferedReader ) v5 ).readLine ( ); // invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v6, v7 */
if ( v7 != null) { // if-eqz v7, :cond_2
/* .line 237 */
v7 = (( java.lang.String ) v6 ).lastIndexOf ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I
/* if-ltz v7, :cond_0 */
/* .line 238 */
final String v7 = "\\s+"; // const-string v7, "\\s+"
java.util.regex.Pattern .compile ( v7 );
/* .line 239 */
/* .local v7, "pattern":Ljava/util/regex/Pattern; */
(( java.util.regex.Pattern ) v7 ).matcher ( v6 ); // invoke-virtual {v7, v6}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
/* .line 240 */
/* .local v8, "matcher":Ljava/util/regex/Matcher; */
final String v9 = " "; // const-string v9, " "
(( java.util.regex.Matcher ) v8 ).replaceAll ( v9 ); // invoke-virtual {v8, v9}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;
(( java.lang.String ) v9 ).trim ( ); // invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;
/* move-object v6, v9 */
/* .line 241 */
v9 = (( java.lang.String ) v6 ).lastIndexOf ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I
/* .line 242 */
/* .local v9, "idx":I */
/* if-ltz v9, :cond_1 */
/* .line 243 */
(( java.lang.String ) v6 ).substring ( v9 ); // invoke-virtual {v6, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;
(( java.lang.String ) v10 ).trim ( ); // invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;
/* .line 244 */
/* .local v10, "pcmStr":Ljava/lang/String; */
v11 = (( java.lang.String ) v10 ).length ( ); // invoke-virtual {v10}, Ljava/lang/String;->length()I
/* const/16 v12, 0x17 */
/* if-le v11, v12, :cond_1 */
/* .line 245 */
final String v11 = "[0-9]*"; // const-string v11, "[0-9]*"
java.util.regex.Pattern .compile ( v11 );
/* .line 246 */
/* .local v11, "patternNum":Ljava/util/regex/Pattern; */
(( java.lang.String ) v10 ).substring ( v12 ); // invoke-virtual {v10, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;
(( java.util.regex.Pattern ) v11 ).matcher ( v13 ); // invoke-virtual {v11, v13}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
/* .line 247 */
/* .local v13, "isNum":Ljava/util/regex/Matcher; */
v14 = (( java.util.regex.Matcher ) v13 ).matches ( ); // invoke-virtual {v13}, Ljava/util/regex/Matcher;->matches()Z
if ( v14 != null) { // if-eqz v14, :cond_1
/* .line 248 */
(( java.lang.String ) v10 ).substring ( v12 ); // invoke-virtual {v10, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;
v12 = java.lang.Integer .parseInt ( v12 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 251 */
} // .end local v7 # "pattern":Ljava/util/regex/Pattern;
} // .end local v8 # "matcher":Ljava/util/regex/Matcher;
} // .end local v9 # "idx":I
} // .end local v10 # "pcmStr":Ljava/lang/String;
} // .end local v11 # "patternNum":Ljava/util/regex/Pattern;
} // .end local v13 # "isNum":Ljava/util/regex/Matcher;
} // :cond_1
/* .line 256 */
} // .end local v6 # "tmpString":Ljava/lang/String;
} // :cond_2
/* nop */
/* .line 258 */
try { // :try_start_1
(( java.io.BufferedReader ) v5 ).close ( ); // invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 261 */
} // :goto_1
/* .line 259 */
/* :catch_0 */
/* move-exception v0 */
/* .line 260 */
/* .local v0, "e":Ljava/io/IOException; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
} // :goto_2
(( java.lang.StringBuilder ) v6 ).append ( v1 ); // invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v1 );
} // .end local v0 # "e":Ljava/io/IOException;
/* .line 256 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 253 */
/* :catch_1 */
/* move-exception v0 */
/* .line 254 */
/* .restart local v0 # "e":Ljava/io/IOException; */
try { // :try_start_2
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v1 ); // invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v6 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 256 */
/* nop */
} // .end local v0 # "e":Ljava/io/IOException;
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 258 */
try { // :try_start_3
(( java.io.BufferedReader ) v5 ).close ( ); // invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_2 */
/* .line 259 */
/* :catch_2 */
/* move-exception v0 */
/* .line 260 */
/* .restart local v0 # "e":Ljava/io/IOException; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 256 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_3
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 258 */
try { // :try_start_4
(( java.io.BufferedReader ) v5 ).close ( ); // invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_3 */
/* .line 261 */
/* .line 259 */
/* :catch_3 */
/* move-exception v6 */
/* .line 260 */
/* .local v6, "e":Ljava/io/IOException; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v1 ); // invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v1 );
/* .line 263 */
} // .end local v6 # "e":Ljava/io/IOException;
} // :cond_3
} // :goto_4
/* throw v0 */
/* .line 265 */
} // .end local v5 # "reader":Ljava/io/BufferedReader;
} // :cond_4
} // :goto_5
return;
} // .end method
private java.lang.String getProcessNameByPid ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "pid" # I */
/* .line 345 */
int v0 = 0; // const/4 v0, 0x0
/* .line 346 */
/* .local v0, "processName":Ljava/lang/String; */
com.android.server.am.ProcessUtils .getProcessNameByPid ( p1 );
/* .line 347 */
} // .end method
private void mimdManagerInit ( ) {
/* .locals 5 */
/* .line 174 */
/* sget-boolean v0, Lcom/android/server/am/MimdManagerServiceImpl;->sEnableMimdService:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 175 */
/* invoke-direct {p0}, Lcom/android/server/am/MimdManagerServiceImpl;->policyControlInit()V */
/* .line 176 */
/* sget-boolean v0, Lcom/android/server/am/MimdManagerServiceImpl;->DBG_MIMD:Z */
final String v1 = "MimdManagerServiceImpl"; // const-string v1, "MimdManagerServiceImpl"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 177 */
final String v2 = "init per-app memcg reclaim succeed!"; // const-string v2, "init per-app memcg reclaim succeed!"
android.util.Log .d ( v1,v2 );
/* .line 179 */
} // :cond_0
/* new-instance v2, Landroid/content/IntentFilter; */
final String v3 = "com.miui.powerkeeper_sleep_changed"; // const-string v3, "com.miui.powerkeeper_sleep_changed"
/* invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
this.filter = v2;
/* .line 180 */
v3 = this.mContext;
v4 = this.mSleepModeReceiver;
(( android.content.Context ) v3 ).registerReceiver ( v4, v2 ); // invoke-virtual {v3, v4, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 181 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 182 */
final String v0 = "register sleep mode receiver succeed!"; // const-string v0, "register sleep mode receiver succeed!"
android.util.Log .d ( v1,v0 );
/* .line 184 */
} // :cond_1
return;
} // .end method
private void onAppProcessStart ( Integer p0, Integer p1, java.lang.String p2 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "pkg" # Ljava/lang/String; */
/* .line 351 */
v0 = this.appIndexMap;
v0 = /* invoke-direct {p0, p2}, Lcom/android/server/am/MimdManagerServiceImpl;->getProcessNameByPid(I)Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/am/MimdManagerServiceImpl;->checkAppCgroup(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 353 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
v1 = com.android.server.am.MimdManagerServiceImpl.mAppMemCgroupPath;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "/"; // const-string v1, "/"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v2, "uid_" */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 354 */
/* .local v0, "uidPath":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "cgroup.procs"; // const-string v2, "cgroup.procs"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
java.lang.Integer .toString ( p2 );
com.android.server.am.MimdManagerServiceImpl .writeToSys ( v1,v2 );
/* .line 356 */
} // .end local v0 # "uidPath":Ljava/lang/String;
} // :cond_0
return;
} // .end method
private void policyControlInit ( ) {
/* .locals 3 */
/* .line 186 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/sys/module/perf_helper/mimd/mimdtrigger"; // const-string v1, "/sys/module/perf_helper/mimd/mimdtrigger"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 187 */
/* .local v0, "mTriggerFile":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 188 */
/* invoke-direct {p0}, Lcom/android/server/am/MimdManagerServiceImpl;->getPolicyControlMaskLocal()V */
/* .line 189 */
v1 = /* invoke-direct {p0}, Lcom/android/server/am/MimdManagerServiceImpl;->ParseMimdconfigAppmap()Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 190 */
android.app.ActivityManager .getService ( );
this.mActivityManagerService = v1;
/* .line 191 */
/* invoke-direct {p0}, Lcom/android/server/am/MimdManagerServiceImpl;->registeForegroundReceiver()V */
/* .line 194 */
} // :cond_0
final String v1 = "MimdManagerServiceImpl"; // const-string v1, "MimdManagerServiceImpl"
final String v2 = "mimd trigger file not find!"; // const-string v2, "mimd trigger file not find!"
android.util.Log .e ( v1,v2 );
/* .line 196 */
} // :cond_1
} // :goto_0
return;
} // .end method
private void registeForegroundReceiver ( ) {
/* .locals 3 */
/* .line 222 */
try { // :try_start_0
v0 = this.mActivityManagerService;
v1 = this.mProcessObserver;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 225 */
/* .line 223 */
/* :catch_0 */
/* move-exception v0 */
/* .line 224 */
/* .local v0, "e":Landroid/os/RemoteException; */
final String v1 = "MimdManagerServiceImpl"; // const-string v1, "MimdManagerServiceImpl"
final String v2 = "mimd manager service registerProcessObserver failed"; // const-string v2, "mimd manager service registerProcessObserver failed"
android.util.Log .e ( v1,v2 );
/* .line 226 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
private static void removeEmptyMemcgDir ( Integer p0 ) {
/* .locals 4 */
/* .param p0, "uid" # I */
/* .line 390 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
v1 = com.android.server.am.MimdManagerServiceImpl.mAppMemCgroupPath;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "/"; // const-string v1, "/"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "uid_" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 391 */
/* .local v0, "uidPath":Ljava/lang/String; */
/* new-instance v1, Ljava/io/File; */
/* invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 392 */
/* .local v1, "appCgDir":Ljava/io/File; */
v2 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 393 */
com.android.server.am.MimdManagerServiceImpl .deleteFolder ( v1 );
/* .line 394 */
/* sget-boolean v2, Lcom/android/server/am/MimdManagerServiceImpl;->DBG_MIMD:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 395 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "remove app cgroup succeed,path:"; // const-string v3, "remove app cgroup succeed,path:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MimdManagerServiceImpl"; // const-string v3, "MimdManagerServiceImpl"
android.util.Log .d ( v3,v2 );
/* .line 396 */
} // :cond_0
return;
/* .line 398 */
} // :cond_1
return;
} // .end method
private static void writeToSys ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 7 */
/* .param p0, "path" # Ljava/lang/String; */
/* .param p1, "content" # Ljava/lang/String; */
/* .line 401 */
final String v0 = "close path failed, err:"; // const-string v0, "close path failed, err:"
final String v1 = "MimdManagerServiceImpl"; // const-string v1, "MimdManagerServiceImpl"
/* new-instance v2, Ljava/io/File; */
/* invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 402 */
/* .local v2, "file":Ljava/io/File; */
int v3 = 0; // const/4 v3, 0x0
/* .line 403 */
/* .local v3, "writer":Ljava/io/FileWriter; */
v4 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 405 */
try { // :try_start_0
/* new-instance v4, Ljava/io/FileWriter; */
/* invoke-direct {v4, p0}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V */
/* move-object v3, v4 */
/* .line 406 */
(( java.io.FileWriter ) v3 ).write ( p1 ); // invoke-virtual {v3, p1}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 410 */
/* nop */
/* .line 412 */
try { // :try_start_1
(( java.io.FileWriter ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 415 */
} // :goto_0
/* .line 413 */
/* :catch_0 */
/* move-exception v4 */
/* .line 414 */
/* .local v4, "e":Ljava/io/IOException; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
} // :goto_1
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v0 );
} // .end local v4 # "e":Ljava/io/IOException;
/* .line 410 */
/* :catchall_0 */
/* move-exception v4 */
/* .line 407 */
/* :catch_1 */
/* move-exception v4 */
/* .line 408 */
/* .restart local v4 # "e":Ljava/io/IOException; */
try { // :try_start_2
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "open or write failed, err:"; // const-string v6, "open or write failed, err:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v5 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 410 */
/* nop */
} // .end local v4 # "e":Ljava/io/IOException;
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 412 */
try { // :try_start_3
(( java.io.FileWriter ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_2 */
/* .line 413 */
/* :catch_2 */
/* move-exception v4 */
/* .line 414 */
/* .restart local v4 # "e":Ljava/io/IOException; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 410 */
} // .end local v4 # "e":Ljava/io/IOException;
} // :goto_2
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 412 */
try { // :try_start_4
(( java.io.FileWriter ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_3 */
/* .line 415 */
/* .line 413 */
/* :catch_3 */
/* move-exception v5 */
/* .line 414 */
/* .local v5, "e":Ljava/io/IOException; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v0 );
/* .line 417 */
} // .end local v5 # "e":Ljava/io/IOException;
} // :cond_0
} // :goto_3
/* throw v4 */
/* .line 419 */
} // :cond_1
} // :goto_4
return;
} // .end method
/* # virtual methods */
public void onProcessStart ( Integer p0, Integer p1, java.lang.String p2 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .line 125 */
/* sget-boolean v0, Lcom/android/server/am/MimdManagerServiceImpl;->sEnableMimdService:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 126 */
/* new-instance v0, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo; */
/* invoke-direct {v0, p0}, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;-><init>(Lcom/android/server/am/MimdManagerServiceImpl;)V */
/* .line 127 */
/* .local v0, "info":Lcom/android/server/am/MimdManagerServiceImpl$AppInfo; */
/* iput p2, v0, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;->pid:I */
/* .line 128 */
/* iput p1, v0, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;->uid:I */
/* .line 129 */
this.packageName = p3;
/* .line 130 */
v1 = this.mMimdManagerHandler;
java.util.Objects .requireNonNull ( v1 );
int v2 = 1; // const/4 v2, 0x1
(( com.android.server.am.MimdManagerServiceImpl$MimdManagerHandler ) v1 ).obtainMessage ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 131 */
/* .local v1, "msg":Landroid/os/Message; */
v2 = this.mMimdManagerHandler;
(( com.android.server.am.MimdManagerServiceImpl$MimdManagerHandler ) v2 ).sendMessage ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 133 */
} // .end local v0 # "info":Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;
} // .end local v1 # "msg":Landroid/os/Message;
} // :cond_0
return;
} // .end method
public void systemReady ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 107 */
final String v0 = "persist.sys.mimd.reclaim.enable"; // const-string v0, "persist.sys.mimd.reclaim.enable"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.am.MimdManagerServiceImpl.sEnableMimdService = (v0!= 0);
/* .line 108 */
this.mContext = p1;
/* .line 109 */
final String v2 = "MimdManagerServiceImpl"; // const-string v2, "MimdManagerServiceImpl"
/* if-nez v0, :cond_0 */
/* .line 110 */
final String v0 = "All Features are disabled, do not create HandlerThread!"; // const-string v0, "All Features are disabled, do not create HandlerThread!"
android.util.Log .e ( v2,v0 );
/* .line 111 */
return;
/* .line 113 */
} // :cond_0
/* new-instance v0, Landroid/os/HandlerThread; */
final String v3 = "mimd"; // const-string v3, "mimd"
/* invoke-direct {v0, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mHandlerThread = v0;
/* .line 114 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 115 */
/* new-instance v0, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler; */
v3 = this.mHandlerThread;
(( android.os.HandlerThread ) v3 ).getLooper ( ); // invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
int v4 = 0; // const/4 v4, 0x0
/* invoke-direct {v0, p0, v3, v4}, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;-><init>(Lcom/android/server/am/MimdManagerServiceImpl;Landroid/os/Looper;Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler-IA;)V */
this.mMimdManagerHandler = v0;
/* .line 117 */
java.util.Objects .requireNonNull ( v0 );
(( com.android.server.am.MimdManagerServiceImpl$MimdManagerHandler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;->obtainMessage(I)Landroid/os/Message;
/* .line 118 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.mMimdManagerHandler;
(( com.android.server.am.MimdManagerServiceImpl$MimdManagerHandler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 119 */
/* sget-boolean v1, Lcom/android/server/am/MimdManagerServiceImpl;->DBG_MIMD:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 120 */
final String v1 = "Create MimdManagerService succeed!"; // const-string v1, "Create MimdManagerService succeed!"
android.util.Log .d ( v2,v1 );
/* .line 121 */
} // :cond_1
return;
} // .end method
