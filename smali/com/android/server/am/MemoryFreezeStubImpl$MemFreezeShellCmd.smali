.class Lcom/android/server/am/MemoryFreezeStubImpl$MemFreezeShellCmd;
.super Landroid/os/ShellCommand;
.source "MemoryFreezeStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/MemoryFreezeStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MemFreezeShellCmd"
.end annotation


# instance fields
.field mMemoryFreezeStubImpl:Lcom/android/server/am/MemoryFreezeStubImpl;

.field final synthetic this$0:Lcom/android/server/am/MemoryFreezeStubImpl;


# direct methods
.method public constructor <init>(Lcom/android/server/am/MemoryFreezeStubImpl;Lcom/android/server/am/MemoryFreezeStubImpl;)V
    .locals 0
    .param p2, "memoryFreezeStubImpl"    # Lcom/android/server/am/MemoryFreezeStubImpl;

    .line 716
    iput-object p1, p0, Lcom/android/server/am/MemoryFreezeStubImpl$MemFreezeShellCmd;->this$0:Lcom/android/server/am/MemoryFreezeStubImpl;

    invoke-direct {p0}, Landroid/os/ShellCommand;-><init>()V

    .line 717
    iput-object p2, p0, Lcom/android/server/am/MemoryFreezeStubImpl$MemFreezeShellCmd;->mMemoryFreezeStubImpl:Lcom/android/server/am/MemoryFreezeStubImpl;

    .line 718
    return-void
.end method


# virtual methods
.method public onCommand(Ljava/lang/String;)I
    .locals 5
    .param p1, "cmd"    # Ljava/lang/String;

    .line 722
    if-nez p1, :cond_0

    .line 723
    invoke-virtual {p0, p1}, Lcom/android/server/am/MemoryFreezeStubImpl$MemFreezeShellCmd;->handleDefaultCommands(Ljava/lang/String;)I

    move-result v0

    return v0

    .line 725
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/am/MemoryFreezeStubImpl$MemFreezeShellCmd;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 727
    .local v0, "pw":Ljava/io/PrintWriter;
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_1
    goto :goto_0

    :sswitch_0
    const-string v2, "dump"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v1

    goto :goto_1

    :sswitch_1
    const-string v2, "enable"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :goto_0
    const/4 v2, -0x1

    :goto_1
    packed-switch v2, :pswitch_data_0

    .line 739
    invoke-virtual {p0, p1}, Lcom/android/server/am/MemoryFreezeStubImpl$MemFreezeShellCmd;->handleDefaultCommands(Ljava/lang/String;)I

    move-result v1

    goto :goto_3

    .line 733
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/server/am/MemoryFreezeStubImpl$MemFreezeShellCmd;->getNextArgRequired()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 734
    .local v2, "enable":Z
    iget-object v3, p0, Lcom/android/server/am/MemoryFreezeStubImpl$MemFreezeShellCmd;->this$0:Lcom/android/server/am/MemoryFreezeStubImpl;

    invoke-static {v3, v2}, Lcom/android/server/am/MemoryFreezeStubImpl;->-$$Nest$fputmEnable(Lcom/android/server/am/MemoryFreezeStubImpl;Z)V

    .line 735
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "memory freeze enabled: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 736
    goto :goto_2

    .line 729
    .end local v2    # "enable":Z
    :pswitch_1
    iget-object v2, p0, Lcom/android/server/am/MemoryFreezeStubImpl$MemFreezeShellCmd;->mMemoryFreezeStubImpl:Lcom/android/server/am/MemoryFreezeStubImpl;

    invoke-static {v2, v0}, Lcom/android/server/am/MemoryFreezeStubImpl;->-$$Nest$mdump(Lcom/android/server/am/MemoryFreezeStubImpl;Ljava/io/PrintWriter;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 730
    nop

    .line 744
    :goto_2
    goto :goto_4

    .line 739
    :goto_3
    return v1

    .line 741
    :catch_0
    move-exception v2

    .line 742
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error occurred. Check logcat for details. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 743
    const-string v3, "MFZ"

    const-string v4, "Error running shell command"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 745
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_4
    return v1

    :sswitch_data_0
    .sparse-switch
        -0x4d6ada7d -> :sswitch_1
        0x2f39f4 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onHelp()V
    .locals 3

    .line 750
    invoke-virtual {p0}, Lcom/android/server/am/MemoryFreezeStubImpl$MemFreezeShellCmd;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 751
    .local v0, "pw":Ljava/io/PrintWriter;
    const-string v1, "Memory Freeze commands:"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 752
    const-string v1, "  help"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 753
    const-string v1, "    Print this help text."

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 754
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 755
    const-string v2, "  dump"

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 756
    const-string v2, "    Print current app memory freeze info."

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 757
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 758
    const-string v2, "  enable [true|false]"

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 759
    const-string v2, "    Enable/Disable memory freeze."

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 760
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 761
    const-string v1, "  debug [true|false]"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 762
    const-string v1, "    Enable/Disable debug config."

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 763
    return-void
.end method
