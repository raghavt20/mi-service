class com.android.server.am.MemoryStandardProcessControl$MemoryStandard {
	 /* .source "MemoryStandardProcessControl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/MemoryStandardProcessControl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "MemoryStandard" */
} // .end annotation
/* # instance fields */
public final java.lang.String mParent;
public final java.util.Set mProcesses;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public final Long mPss;
final com.android.server.am.MemoryStandardProcessControl this$0; //synthetic
/* # direct methods */
public com.android.server.am.MemoryStandardProcessControl$MemoryStandard ( ) {
/* .locals 0 */
/* .param p2, "pss" # J */
/* .param p5, "parent" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(J", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Ljava/lang/String;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 1285 */
/* .local p4, "processes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 1283 */
/* new-instance p1, Ljava/util/HashSet; */
/* invoke-direct {p1}, Ljava/util/HashSet;-><init>()V */
this.mProcesses = p1;
/* .line 1286 */
/* iput-wide p2, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;->mPss:J */
/* .line 1287 */
this.mParent = p5;
/* .line 1288 */
/* .line 1289 */
return;
} // .end method
/* # virtual methods */
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 1293 */
/* iget-wide v0, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;->mPss:J */
/* .line 1294 */
java.lang.Long .valueOf ( v0,v1 );
v1 = this.mProcesses;
(( java.lang.Object ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
v2 = this.mParent;
/* filled-new-array {v0, v1, v2}, [Ljava/lang/Object; */
/* .line 1293 */
/* const-string/jumbo v1, "{ pss: %d, processes: %s, parent: %s }" */
java.lang.String .format ( v1,v0 );
} // .end method
