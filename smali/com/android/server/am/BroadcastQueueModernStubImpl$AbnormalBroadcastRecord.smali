.class Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;
.super Ljava/lang/Object;
.source "BroadcastQueueModernStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/BroadcastQueueModernStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AbnormalBroadcastRecord"
.end annotation


# instance fields
.field action:Ljava/lang/String;

.field callerPackage:Ljava/lang/String;

.field userId:I


# direct methods
.method constructor <init>(Lcom/android/server/am/BroadcastRecord;)V
    .locals 1
    .param p1, "r"    # Lcom/android/server/am/BroadcastRecord;

    .line 845
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 846
    iget-object v0, p1, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;->action:Ljava/lang/String;

    .line 847
    iget-object v0, p1, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;->callerPackage:Ljava/lang/String;

    .line 848
    iget v0, p1, Lcom/android/server/am/BroadcastRecord;->userId:I

    iput v0, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;->userId:I

    .line 849
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .line 853
    instance-of v0, p1, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;

    if-eqz v0, :cond_1

    .line 854
    move-object v0, p1

    check-cast v0, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;

    .line 855
    .local v0, "r":Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;
    iget-object v1, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;->action:Ljava/lang/String;

    iget-object v2, v0, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;->action:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;->callerPackage:Ljava/lang/String;

    iget-object v2, v0, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;->callerPackage:Ljava/lang/String;

    .line 856
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;->userId:I

    iget v2, v0, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;->userId:I

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 855
    :goto_0
    return v1

    .line 859
    .end local v0    # "r":Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;
    :cond_1
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 4

    .line 864
    const/4 v0, 0x1

    .line 865
    .local v0, "hashCode":I
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;->action:Ljava/lang/String;

    const/4 v3, 0x0

    if-nez v2, :cond_0

    move v2, v3

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_0
    add-int/2addr v1, v2

    .line 866
    .end local v0    # "hashCode":I
    .local v1, "hashCode":I
    mul-int/lit8 v0, v1, 0x1f

    iget-object v2, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;->callerPackage:Ljava/lang/String;

    if-nez v2, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    :goto_1
    add-int/2addr v0, v3

    .line 867
    .end local v1    # "hashCode":I
    .restart local v0    # "hashCode":I
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;->userId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    add-int/2addr v1, v2

    .line 868
    .end local v0    # "hashCode":I
    .restart local v1    # "hashCode":I
    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 873
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AbnormalBroadcastRecord{action=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;->action:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", callerPackage=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;->callerPackage:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", userId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;->userId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
