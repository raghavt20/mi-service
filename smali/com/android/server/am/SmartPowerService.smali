.class public Lcom/android/server/am/SmartPowerService;
.super Lmiui/smartpower/ISmartPowerManager$Stub;
.source "SmartPowerService.java"

# interfaces
.implements Lcom/android/server/am/SmartPowerServiceStub;
.implements Lcom/miui/app/smartpower/SmartPowerServiceInternal;


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.miui.server.smartpower.SmartPowerServiceStub$$"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/SmartPowerService$MyHandler;,
        Lcom/android/server/am/SmartPowerService$SmartPowerShellCommand;,
        Lcom/android/server/am/SmartPowerService$Lifecycle;
    }
.end annotation


# static fields
.field private static final CLOUD_ALARM_WHITE_LIST:Ljava/lang/String; = "power_alarm_white_list"

.field private static final CLOUD_BROADCAST_WHITE_LIST:Ljava/lang/String; = "power_broadcast_white_list"

.field private static final CLOUD_CONTROL_KEY:Ljava/lang/String; = "perf_shielder_smartpower"

.field private static final CLOUD_CONTROL_MOUDLE:Ljava/lang/String; = "perf_smartpower"

.field private static final CLOUD_DISPLAY_ENABLE:Ljava/lang/String; = "perf_power_display_enable"

.field private static final CLOUD_ENABLE:Ljava/lang/String; = "perf_power_appState_enable"

.field private static final CLOUD_FROZEN_ENABLE:Ljava/lang/String; = "perf_power_freeze_enable"

.field private static final CLOUD_GAME_PAY_PROTECT_ENABLE:Ljava/lang/String; = "perf_game_pay_protect_enable"

.field private static final CLOUD_INTERCEPT_ENABLE:Ljava/lang/String; = "perf_power_intercept_enable"

.field private static final CLOUD_PKG_WHITE_LIST:Ljava/lang/String; = "power_pkg_white_list"

.field private static final CLOUD_PROC_WHITE_LIST:Ljava/lang/String; = "power_proc_white_list"

.field private static final CLOUD_PROVIDER_WHITE_LIST:Ljava/lang/String; = "power_provider_white_list"

.field private static final CLOUD_SCREENON_WHITE_LIST:Ljava/lang/String; = "power_screenon_white_list"

.field private static final CLOUD_SERVICE_WHITE_LIST:Ljava/lang/String; = "power_service_white_list"

.field private static final CLOUD_WINDOW_ENABLE:Ljava/lang/String; = "perf_power_window_enable"

.field public static final DEBUG:Z

.field public static final SERVICE_NAME:Ljava/lang/String; = "smartpower"

.field public static final TAG:Ljava/lang/String; = "SmartPower"

.field public static final THREAD_GROUP_FOREGROUND:I = 0x1


# instance fields
.field private mAMS:Lcom/android/server/am/ActivityManagerService;

.field private mATMS:Lcom/android/server/wm/ActivityTaskManagerService;

.field private mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

.field private mAppStateManager:Lcom/android/server/am/AppStateManager;

.field private mComponentsManager:Lcom/miui/server/smartpower/ComponentsManager;

.field private mContext:Landroid/content/Context;

.field private mHandler:Lcom/android/server/am/SmartPowerService$MyHandler;

.field private mHandlerTh:Landroid/os/HandlerThread;

.field private mPackageManager:Landroid/content/pm/PackageManagerInternal;

.field private mPowerFrozenManager:Lcom/miui/server/smartpower/PowerFrozenManager;

.field private mSmartBoostPolicyManager:Lcom/miui/server/smartpower/SmartBoostPolicyManager;

.field private mSmartCpuPolicyManager:Lcom/miui/server/smartpower/SmartCpuPolicyManager;

.field private mSmartDisplayPolicyManager:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

.field private mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

.field private mSmartScenarioManager:Lcom/miui/server/smartpower/SmartScenarioManager;

.field private mSmartThermalPolicyManager:Lcom/miui/server/smartpower/SmartThermalPolicyManager;

.field private mSmartWindowPolicyManager:Lcom/miui/server/smartpower/SmartWindowPolicyManager;

.field private mSystemReady:Z

.field private mWMS:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method static bridge synthetic -$$Nest$mupdateCloudControlParas(Lcom/android/server/am/SmartPowerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/SmartPowerService;->updateCloudControlParas()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 87
    sget-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->DEBUG_ALL:Z

    sput-boolean v0, Lcom/android/server/am/SmartPowerService;->DEBUG:Z

    return-void
.end method

.method constructor <init>()V
    .locals 3

    .line 135
    invoke-direct {p0}, Lmiui/smartpower/ISmartPowerManager$Stub;-><init>()V

    .line 111
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/am/SmartPowerService;->mPowerFrozenManager:Lcom/miui/server/smartpower/PowerFrozenManager;

    .line 112
    iput-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    .line 113
    iput-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    .line 114
    iput-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    .line 115
    iput-object v0, p0, Lcom/android/server/am/SmartPowerService;->mComponentsManager:Lcom/miui/server/smartpower/ComponentsManager;

    .line 117
    iput-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartScenarioManager:Lcom/miui/server/smartpower/SmartScenarioManager;

    .line 118
    iput-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartThermalPolicyManager:Lcom/miui/server/smartpower/SmartThermalPolicyManager;

    .line 119
    iput-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartDisplayPolicyManager:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    .line 120
    iput-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartWindowPolicyManager:Lcom/miui/server/smartpower/SmartWindowPolicyManager;

    .line 121
    iput-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartBoostPolicyManager:Lcom/miui/server/smartpower/SmartBoostPolicyManager;

    .line 130
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "SmartPowerService"

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/android/server/am/SmartPowerService;->mHandlerTh:Landroid/os/HandlerThread;

    .line 133
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    .line 136
    const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-static {v0, p0}, Lcom/android/server/LocalServices;->addService(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 137
    return-void
.end method

.method private checkPermission()Z
    .locals 2

    .line 219
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 220
    .local v0, "callingUid":I
    const/16 v1, 0x2710

    if-ge v0, v1, :cond_0

    .line 221
    const/4 v1, 0x1

    return v1

    .line 223
    :cond_0
    iget-object v1, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v1, v0}, Lcom/android/server/am/AppStateManager;->isSystemApp(I)Z

    move-result v1

    return v1
.end method

.method private dumpConfig(Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 1392
    const-string/jumbo v0, "smartpower config:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1393
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  appstate("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/miui/app/smartpower/SmartPowerSettings;->APP_STATE_ENABLE:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") fz("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_FROZEN_ENABLE:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") intercept("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_INTERCEPT_ENABLE:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1396
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1397
    return-void
.end method

.method protected static isUsbDataTransferActive(J)Z
    .locals 4
    .param p0, "functions"    # J

    .line 725
    const-wide/16 v0, 0x4

    and-long/2addr v0, p0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    const-wide/16 v0, 0x10

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private logCloudControlParas(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/Object;

    .line 917
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "sync cloud control "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SmartPower"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 918
    return-void
.end method

.method private registerCloudObserver(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 819
    new-instance v0, Lcom/android/server/am/SmartPowerService$1;

    iget-object v1, p0, Lcom/android/server/am/SmartPowerService;->mHandler:Lcom/android/server/am/SmartPowerService$MyHandler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/am/SmartPowerService$1;-><init>(Lcom/android/server/am/SmartPowerService;Landroid/os/Handler;)V

    .line 828
    .local v0, "observer":Landroid/database/ContentObserver;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 829
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v2

    .line 828
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 831
    return-void
.end method

.method private updateCloudControlParas()V
    .locals 19

    .line 834
    move-object/from16 v1, p0

    const-string v0, "power_service_white_list"

    const-string v2, "power_provider_white_list"

    const-string v3, "power_alarm_white_list"

    const-string v4, "power_broadcast_white_list"

    const-string v5, "power_screenon_white_list"

    const-string v6, "power_proc_white_list"

    const-string v7, "power_pkg_white_list"

    const-string v8, "perf_game_pay_protect_enable"

    const-string v9, "perf_power_window_enable"

    const-string v10, "perf_power_display_enable"

    const-string v11, "perf_power_intercept_enable"

    const-string v12, "perf_power_appState_enable"

    const-string v13, "perf_power_freeze_enable"

    iget-object v14, v1, Lcom/android/server/am/SmartPowerService;->mContext:Landroid/content/Context;

    .line 835
    invoke-virtual {v14}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    .line 834
    const-string v15, "perf_smartpower"

    move-object/from16 v16, v0

    const-string v0, "perf_shielder_smartpower"

    move-object/from16 v17, v2

    const-string v2, ""

    invoke-static {v14, v15, v0, v2}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 836
    .local v2, "data":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 837
    return-void

    .line 840
    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 841
    .local v0, "jsonObject":Lorg/json/JSONObject;
    invoke-virtual {v0, v13}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 842
    invoke-virtual {v0, v13}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v14

    .line 843
    .local v14, "frozenEnable":Z
    iget-object v15, v1, Lcom/android/server/am/SmartPowerService;->mPowerFrozenManager:Lcom/miui/server/smartpower/PowerFrozenManager;

    invoke-virtual {v15, v14}, Lcom/miui/server/smartpower/PowerFrozenManager;->syncCloudControlSettings(Z)V

    .line 844
    const-string v15, "persist.sys.smartpower.fz.enable"
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-object/from16 v18, v2

    .end local v2    # "data":Ljava/lang/String;
    .local v18, "data":Ljava/lang/String;
    :try_start_1
    invoke-static {v14}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {v15, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 845
    invoke-static {v14}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v1, v13, v2}, Lcom/android/server/am/SmartPowerService;->logCloudControlParas(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 841
    .end local v14    # "frozenEnable":Z
    .end local v18    # "data":Ljava/lang/String;
    .restart local v2    # "data":Ljava/lang/String;
    :cond_1
    move-object/from16 v18, v2

    .line 848
    .end local v2    # "data":Ljava/lang/String;
    .restart local v18    # "data":Ljava/lang/String;
    :goto_0
    invoke-virtual {v0, v12}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 849
    invoke-virtual {v0, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 850
    .local v2, "appStateEnable":Ljava/lang/String;
    const-string v13, "persist.sys.smartpower.appstate.enable"

    invoke-static {v13, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 851
    invoke-direct {v1, v12, v2}, Lcom/android/server/am/SmartPowerService;->logCloudControlParas(Ljava/lang/String;Ljava/lang/Object;)V

    .line 854
    .end local v2    # "appStateEnable":Ljava/lang/String;
    :cond_2
    invoke-virtual {v0, v11}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 855
    invoke-virtual {v0, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 856
    .local v2, "interceptEnable":Ljava/lang/String;
    const-string v12, "persist.sys.smartpower.intercept.enable"

    invoke-static {v12, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 857
    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v12

    sput-boolean v12, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_INTERCEPT_ENABLE:Z

    .line 858
    invoke-direct {v1, v11, v2}, Lcom/android/server/am/SmartPowerService;->logCloudControlParas(Ljava/lang/String;Ljava/lang/Object;)V

    .line 861
    .end local v2    # "interceptEnable":Ljava/lang/String;
    :cond_3
    invoke-virtual {v0, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 862
    invoke-virtual {v0, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 863
    .local v2, "dipslayEnable":Ljava/lang/String;
    const-string v11, "persist.sys.smartpower.display.enable"

    invoke-static {v11, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 864
    invoke-direct {v1, v10, v2}, Lcom/android/server/am/SmartPowerService;->logCloudControlParas(Ljava/lang/String;Ljava/lang/Object;)V

    .line 867
    .end local v2    # "dipslayEnable":Ljava/lang/String;
    :cond_4
    invoke-virtual {v0, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 868
    invoke-virtual {v0, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 869
    .local v2, "windowEnable":Ljava/lang/String;
    const-string v10, "persist.sys.smartpower.window.enable"

    invoke-static {v10, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 870
    invoke-direct {v1, v9, v2}, Lcom/android/server/am/SmartPowerService;->logCloudControlParas(Ljava/lang/String;Ljava/lang/Object;)V

    .line 873
    .end local v2    # "windowEnable":Ljava/lang/String;
    :cond_5
    invoke-virtual {v0, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 874
    invoke-virtual {v0, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 875
    .local v2, "gamePayProtectEnable":Ljava/lang/String;
    const-string v9, "persist.sys.smartpower.gamepay.protect.enabled"

    invoke-static {v9, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 876
    nop

    .line 877
    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v9

    sput-boolean v9, Lcom/miui/app/smartpower/SmartPowerSettings;->GAME_PAY_PROTECT_ENABLED:Z

    .line 878
    invoke-direct {v1, v8, v2}, Lcom/android/server/am/SmartPowerService;->logCloudControlParas(Ljava/lang/String;Ljava/lang/Object;)V

    .line 881
    .end local v2    # "gamePayProtectEnable":Ljava/lang/String;
    :cond_6
    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 882
    .local v2, "pkgWhiteListString":Ljava/lang/String;
    iget-object v8, v1, Lcom/android/server/am/SmartPowerService;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    invoke-virtual {v8, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->updateCloudPackageWhiteList(Ljava/lang/String;)V

    .line 883
    invoke-direct {v1, v7, v2}, Lcom/android/server/am/SmartPowerService;->logCloudControlParas(Ljava/lang/String;Ljava/lang/Object;)V

    .line 885
    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 886
    .local v7, "procWhiteListString":Ljava/lang/String;
    iget-object v8, v1, Lcom/android/server/am/SmartPowerService;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    invoke-virtual {v8, v7}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->updateCloudProcessWhiteList(Ljava/lang/String;)V

    .line 887
    invoke-direct {v1, v6, v7}, Lcom/android/server/am/SmartPowerService;->logCloudControlParas(Ljava/lang/String;Ljava/lang/Object;)V

    .line 889
    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 890
    .local v6, "screenonWhiteListString":Ljava/lang/String;
    iget-object v8, v1, Lcom/android/server/am/SmartPowerService;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    invoke-virtual {v8, v6}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->updateCloudSreenonWhiteList(Ljava/lang/String;)V

    .line 891
    invoke-direct {v1, v5, v6}, Lcom/android/server/am/SmartPowerService;->logCloudControlParas(Ljava/lang/String;Ljava/lang/Object;)V

    .line 893
    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 894
    .local v5, "broadcastWhiteListString":Ljava/lang/String;
    iget-object v8, v1, Lcom/android/server/am/SmartPowerService;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    invoke-virtual {v8, v5}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->updateCloudBroadcastWhiteLit(Ljava/lang/String;)V

    .line 895
    invoke-direct {v1, v4, v5}, Lcom/android/server/am/SmartPowerService;->logCloudControlParas(Ljava/lang/String;Ljava/lang/Object;)V

    .line 898
    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 899
    .local v4, "alarmWhiteListString":Ljava/lang/String;
    iget-object v8, v1, Lcom/android/server/am/SmartPowerService;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    invoke-virtual {v8, v4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->updateCloudAlarmWhiteLit(Ljava/lang/String;)V

    .line 900
    invoke-direct {v1, v3, v4}, Lcom/android/server/am/SmartPowerService;->logCloudControlParas(Ljava/lang/String;Ljava/lang/Object;)V

    .line 903
    move-object/from16 v3, v17

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 904
    .local v8, "providerWhiteListString":Ljava/lang/String;
    iget-object v9, v1, Lcom/android/server/am/SmartPowerService;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    invoke-virtual {v9, v8}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->updateCloudProviderWhiteLit(Ljava/lang/String;)V

    .line 905
    invoke-direct {v1, v3, v8}, Lcom/android/server/am/SmartPowerService;->logCloudControlParas(Ljava/lang/String;Ljava/lang/Object;)V

    .line 908
    move-object/from16 v3, v16

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 909
    .local v9, "serviceWhiteListString":Ljava/lang/String;
    iget-object v10, v1, Lcom/android/server/am/SmartPowerService;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    invoke-virtual {v10, v9}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->updateCloudServiceWhiteLit(Ljava/lang/String;)V

    .line 910
    invoke-direct {v1, v3, v9}, Lcom/android/server/am/SmartPowerService;->logCloudControlParas(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 913
    .end local v0    # "jsonObject":Lorg/json/JSONObject;
    .end local v2    # "pkgWhiteListString":Ljava/lang/String;
    .end local v4    # "alarmWhiteListString":Ljava/lang/String;
    .end local v5    # "broadcastWhiteListString":Ljava/lang/String;
    .end local v6    # "screenonWhiteListString":Ljava/lang/String;
    .end local v7    # "procWhiteListString":Ljava/lang/String;
    .end local v8    # "providerWhiteListString":Ljava/lang/String;
    .end local v9    # "serviceWhiteListString":Ljava/lang/String;
    goto :goto_2

    .line 911
    :catch_0
    move-exception v0

    goto :goto_1

    .end local v18    # "data":Ljava/lang/String;
    .local v2, "data":Ljava/lang/String;
    :catch_1
    move-exception v0

    move-object/from16 v18, v2

    .line 912
    .end local v2    # "data":Ljava/lang/String;
    .local v0, "e":Lorg/json/JSONException;
    .restart local v18    # "data":Ljava/lang/String;
    :goto_1
    const-string v2, "SmartPower"

    const-string/jumbo v3, "updateCloudData error :"

    invoke-static {v2, v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 914
    .end local v0    # "e":Lorg/json/JSONException;
    :goto_2
    return-void
.end method


# virtual methods
.method public addFrozenPid(II)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 591
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 592
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mPowerFrozenManager:Lcom/miui/server/smartpower/PowerFrozenManager;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/PowerFrozenManager;->addFrozenPid(II)V

    .line 594
    :cond_0
    return-void
.end method

.method public applyOomAdjLocked(Lcom/android/server/am/ProcessRecord;)V
    .locals 1
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 647
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 648
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mComponentsManager:Lcom/miui/server/smartpower/ComponentsManager;

    invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/ComponentsManager;->onApplyOomAdjLocked(Lcom/android/server/am/ProcessRecord;)V

    .line 649
    invoke-static {}, Lcom/android/server/am/SystemPressureControllerStub;->getInstance()Lcom/android/server/am/SystemPressureControllerStub;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/am/SystemPressureControllerStub;->onApplyOomAdjLocked(Lcom/android/server/am/ProcessRecord;)V

    .line 651
    :cond_0
    return-void
.end method

.method public checkSystemSignature(I)Z
    .locals 2
    .param p1, "uid"    # I

    .line 1085
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 1086
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 1087
    invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState;->isSystemSignature()Z

    move-result v1

    return v1

    .line 1089
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 4
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .line 1333
    const-string v0, ""

    iget-object v1, p0, Lcom/android/server/am/SmartPowerService;->mContext:Landroid/content/Context;

    const-string v2, "SmartPower"

    invoke-static {v1, v2, p2}, Lcom/android/internal/util/DumpUtils;->checkDumpPermission(Landroid/content/Context;Ljava/lang/String;Ljava/io/PrintWriter;)Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    .line 1334
    :cond_0
    const-string/jumbo v1, "smart power (smartpower):"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1335
    const/4 v1, 0x0

    .line 1337
    .local v1, "opti":I
    :try_start_0
    array-length v2, p3

    if-ge v1, v2, :cond_9

    .line 1338
    aget-object v2, p3, v1

    .line 1339
    .local v2, "parm":Ljava/lang/String;
    add-int/lit8 v1, v1, 0x1

    .line 1340
    const-string v3, "policy"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1341
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    invoke-virtual {v0, p2, p3, v1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 1342
    :cond_1
    const-string v3, "appstate"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1343
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0, p2, p3, v1}, Lcom/android/server/am/AppStateManager;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 1344
    :cond_2
    const-string v3, "fz"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1345
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mPowerFrozenManager:Lcom/miui/server/smartpower/PowerFrozenManager;

    invoke-virtual {v0, p2, p3, v1}, Lcom/miui/server/smartpower/PowerFrozenManager;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 1346
    :cond_3
    const-string v3, "scene"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1347
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartScenarioManager:Lcom/miui/server/smartpower/SmartScenarioManager;

    invoke-virtual {v0, p2, p3, v1}, Lcom/miui/server/smartpower/SmartScenarioManager;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V

    goto :goto_0

    .line 1348
    :cond_4
    const-string v3, "display"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1349
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartDisplayPolicyManager:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-virtual {v0, p2, p3, v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V

    goto :goto_0

    .line 1350
    :cond_5
    const-string/jumbo v3, "window"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1351
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartWindowPolicyManager:Lcom/miui/server/smartpower/SmartWindowPolicyManager;

    invoke-virtual {v0, p2, p3, v1}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V

    goto :goto_0

    .line 1352
    :cond_6
    const-string/jumbo v3, "set"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1353
    invoke-virtual {p0, p2, p3, v1}, Lcom/android/server/am/SmartPowerService;->dumpSettings(Ljava/io/PrintWriter;[Ljava/lang/String;I)V

    .line 1354
    invoke-direct {p0, p2}, Lcom/android/server/am/SmartPowerService;->dumpConfig(Ljava/io/PrintWriter;)V

    goto :goto_0

    .line 1355
    :cond_7
    const-string/jumbo v3, "stack"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1356
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0, p2, p3, v1}, Lcom/android/server/am/AppStateManager;->dumpStack(Ljava/io/PrintWriter;[Ljava/lang/String;I)V

    goto :goto_0

    .line 1357
    :cond_8
    const-string v3, "-a"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1358
    invoke-direct {p0, p2}, Lcom/android/server/am/SmartPowerService;->dumpConfig(Ljava/io/PrintWriter;)V

    .line 1359
    iget-object v3, p0, Lcom/android/server/am/SmartPowerService;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    invoke-virtual {v3, p2, p3, v1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V

    .line 1360
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1361
    iget-object v3, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v3, p2, p3, v1}, Lcom/android/server/am/AppStateManager;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V

    .line 1362
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1363
    iget-object v3, p0, Lcom/android/server/am/SmartPowerService;->mSmartScenarioManager:Lcom/miui/server/smartpower/SmartScenarioManager;

    invoke-virtual {v3, p2, p3, v1}, Lcom/miui/server/smartpower/SmartScenarioManager;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V

    .line 1364
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1365
    iget-object v3, p0, Lcom/android/server/am/SmartPowerService;->mPowerFrozenManager:Lcom/miui/server/smartpower/PowerFrozenManager;

    invoke-virtual {v3, p2, p3, v1}, Lcom/miui/server/smartpower/PowerFrozenManager;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V

    .line 1366
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1371
    .end local v2    # "parm":Ljava/lang/String;
    :cond_9
    :goto_0
    goto :goto_1

    .line 1369
    :catch_0
    move-exception v0

    .line 1370
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1372
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method

.method public dumpSettings(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
    .locals 5
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "args"    # [Ljava/lang/String;
    .param p3, "opti"    # I

    .line 1375
    add-int/lit8 v0, p3, 0x1

    array-length v1, p2

    if-ge v0, v1, :cond_4

    .line 1376
    add-int/lit8 v0, p3, 0x1

    .end local p3    # "opti":I
    .local v0, "opti":I
    aget-object p3, p2, p3

    .line 1377
    .local p3, "key":Ljava/lang/String;
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "opti":I
    .local v1, "opti":I
    aget-object v0, p2, v0

    .line 1378
    .local v0, "value":Ljava/lang/String;
    const-string v2, "fz"

    invoke-virtual {p3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    const-string v3, "false"

    const-string/jumbo v4, "true"

    if-eqz v2, :cond_1

    .line 1379
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1380
    :cond_0
    const-string v2, "persist.sys.smartpower.fz.enable"

    invoke-static {v2, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1382
    :cond_1
    const-string v2, "intercept"

    invoke-virtual {p3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1383
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1384
    :cond_2
    const-string v2, "persist.sys.smartpower.intercept.enable"

    invoke-static {v2, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1385
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    sput-boolean v2, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_INTERCEPT_ENABLE:Z

    .line 1389
    .end local v0    # "value":Ljava/lang/String;
    .end local p3    # "key":Ljava/lang/String;
    :cond_3
    :goto_0
    move p3, v1

    .end local v1    # "opti":I
    .local p3, "opti":I
    :cond_4
    return-void
.end method

.method public getAllAppState()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/smartpower/IAppState;",
            ">;"
        }
    .end annotation

    .line 924
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 925
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0}, Lcom/android/server/am/AppStateManager;->getAllAppState()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0

    .line 927
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAppState(I)Lcom/miui/server/smartpower/IAppState;
    .locals 1
    .param p1, "uid"    # I

    .line 946
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 947
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    return-object v0

    .line 949
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getBackgroundCpuCoreNum()I
    .locals 1

    .line 1077
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 1078
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartCpuPolicyManager:Lcom/miui/server/smartpower/SmartCpuPolicyManager;

    invoke-virtual {v0}, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->getBackgroundCpuCoreNum()I

    move-result v0

    return v0

    .line 1080
    :cond_0
    const/4 v0, 0x4

    return v0
.end method

.method public getLastMusicPlayTimeStamp(I)J
    .locals 2
    .param p1, "pid"    # I

    .line 1053
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 1054
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/AppPowerResourceManager;->getLastMusicPlayTimeStamp(I)J

    move-result-wide v0

    return-wide v0

    .line 1056
    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getLruProcesses()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/smartpower/IAppState$IRunningProcess;",
            ">;"
        }
    .end annotation

    .line 954
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 955
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0}, Lcom/android/server/am/AppStateManager;->getLruProcesses()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0

    .line 957
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getLruProcesses(ILjava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "uid"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/smartpower/IAppState$IRunningProcess;",
            ">;"
        }
    .end annotation

    .line 962
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 963
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/am/AppStateManager;->getLruProcesses(ILjava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0

    .line 965
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getRunningProcess(ILjava/lang/String;)Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .locals 1
    .param p1, "uid"    # I
    .param p2, "processName"    # Ljava/lang/String;

    .line 970
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 971
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/am/AppStateManager;->getRunningProcess(ILjava/lang/String;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v0

    return-object v0

    .line 973
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public hibernateAllIfNeeded(Ljava/lang/String;)V
    .locals 1
    .param p1, "reason"    # Ljava/lang/String;

    .line 979
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 980
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager;->hibernateAllIfNeeded(Ljava/lang/String;)V

    .line 982
    :cond_0
    return-void
.end method

.method public isAppAudioActive(I)Z
    .locals 2
    .param p1, "uid"    # I

    .line 1026
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 1027
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    invoke-virtual {v0, p1, v1}, Lcom/miui/server/smartpower/AppPowerResourceManager;->isAppResActive(II)Z

    move-result v0

    return v0

    .line 1030
    :cond_0
    return v1
.end method

.method public isAppAudioActive(II)Z
    .locals 2
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 1044
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 1045
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    invoke-virtual {v0, p1, p2, v1}, Lcom/miui/server/smartpower/AppPowerResourceManager;->isAppResActive(III)Z

    move-result v0

    return v0

    .line 1048
    :cond_0
    return v1
.end method

.method public isAppGpsActive(I)Z
    .locals 2
    .param p1, "uid"    # I

    .line 1035
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 1036
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    const/4 v1, 0x3

    invoke-virtual {v0, p1, v1}, Lcom/miui/server/smartpower/AppPowerResourceManager;->isAppResActive(II)Z

    move-result v0

    return v0

    .line 1039
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public isAppGpsActive(II)Z
    .locals 2
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 1061
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 1062
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    const/4 v1, 0x3

    invoke-virtual {v0, p1, p2, v1}, Lcom/miui/server/smartpower/AppPowerResourceManager;->isAppResActive(III)Z

    move-result v0

    return v0

    .line 1065
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public isProcessInTaskStack(II)Z
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 1002
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 1003
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/am/AppStateManager;->isProcessInTaskStack(II)Z

    move-result v0

    return v0

    .line 1005
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isProcessInTaskStack(Ljava/lang/String;)Z
    .locals 1
    .param p1, "prcName"    # Ljava/lang/String;

    .line 1010
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 1011
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager;->isProcessInTaskStack(Ljava/lang/String;)Z

    move-result v0

    return v0

    .line 1013
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isProcessPerceptible(ILjava/lang/String;)Z
    .locals 1
    .param p1, "uid"    # I
    .param p2, "procName"    # Ljava/lang/String;

    .line 994
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 995
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/am/AppStateManager;->isProcessPerceptible(ILjava/lang/String;)Z

    move-result v0

    return v0

    .line 997
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isProcessWhiteList(ILjava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "flag"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "procName"    # Ljava/lang/String;

    .line 1018
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 1019
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isProcessWhiteList(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0

    .line 1021
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isUidIdle(I)Z
    .locals 1
    .param p1, "uid"    # I

    .line 241
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager;->isUidIdle(I)Z

    move-result v0

    return v0

    .line 244
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isUidVisible(I)Z
    .locals 1
    .param p1, "uid"    # I

    .line 986
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 987
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager;->isUidVisible(I)Z

    move-result v0

    return v0

    .line 989
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public notifyCameraForegroundState(Ljava/lang/String;ZLjava/lang/String;II)V
    .locals 7
    .param p1, "cameraId"    # Ljava/lang/String;
    .param p2, "isForeground"    # Z
    .param p3, "caller"    # Ljava/lang/String;
    .param p4, "callerUid"    # I
    .param p5, "callerPid"    # I

    .line 812
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 813
    iget-object v1, p0, Lcom/android/server/am/SmartPowerService;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/miui/server/smartpower/AppPowerResourceManager;->notifyCameraForegroundState(Ljava/lang/String;ZLjava/lang/String;II)V

    .line 816
    :cond_0
    return-void
.end method

.method public notifyMultiSenceEnable(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .line 1120
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 1121
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartBoostPolicyManager:Lcom/miui/server/smartpower/SmartBoostPolicyManager;

    invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/SmartBoostPolicyManager;->setMultiSenceEnable(Z)V

    .line 1123
    :cond_0
    return-void
.end method

.method public onAcquireLocation(IILandroid/location/ILocationListener;)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "listener"    # Landroid/location/ILocationListener;

    .line 739
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 740
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/AppPowerResourceManager;->onAquireLocation(IILandroid/location/ILocationListener;)V

    .line 742
    :cond_0
    return-void
.end method

.method public onAcquireWakelock(Landroid/os/IBinder;ILjava/lang/String;II)V
    .locals 7
    .param p1, "lock"    # Landroid/os/IBinder;
    .param p2, "flags"    # I
    .param p3, "tag"    # Ljava/lang/String;
    .param p4, "ownerUid"    # I
    .param p5, "ownerPid"    # I

    .line 756
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 757
    iget-object v1, p0, Lcom/android/server/am/SmartPowerService;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    .line 758
    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/miui/server/smartpower/AppPowerResourceManager;->onAcquireWakelock(Landroid/os/IBinder;ILjava/lang/String;II)V

    .line 760
    :cond_0
    return-void
.end method

.method public onActivityLaunched(ILjava/lang/String;Lcom/android/server/wm/ActivityRecord;I)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "processName"    # Ljava/lang/String;
    .param p3, "record"    # Lcom/android/server/wm/ActivityRecord;
    .param p4, "launchState"    # I

    .line 623
    invoke-static {}, Lcom/android/server/am/MiProcessTracker;->getInstance()Lcom/android/server/am/MiProcessTracker;

    move-result-object v0

    iget-object v1, p3, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-virtual {v0, p2, v1, p1, p4}, Lcom/android/server/am/MiProcessTracker;->onActivityLaunched(Ljava/lang/String;Ljava/lang/String;II)V

    .line 625
    return-void
.end method

.method public onActivityReusmeUnchecked(Ljava/lang/String;IILjava/lang/String;IILjava/lang/String;Z)V
    .locals 11
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "pid"    # I
    .param p4, "packageName"    # Ljava/lang/String;
    .param p5, "launchedFromUid"    # I
    .param p6, "launchedFromPid"    # I
    .param p7, "launchedFromPackage"    # Ljava/lang/String;
    .param p8, "isColdStart"    # Z

    .line 404
    move-object v0, p0

    iget-boolean v1, v0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v1, :cond_0

    .line 405
    invoke-static {}, Lcom/android/server/am/SystemPressureControllerStub;->getInstance()Lcom/android/server/am/SystemPressureControllerStub;

    move-result-object v1

    new-instance v10, Lcom/android/server/am/ControllerActivityInfo;

    move-object v2, v10

    move v3, p2

    move v4, p3

    move-object v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v2 .. v9}, Lcom/android/server/am/ControllerActivityInfo;-><init>(IILjava/lang/String;IILjava/lang/String;Z)V

    invoke-virtual {v1, v10}, Lcom/android/server/am/SystemPressureControllerStub;->activityResumeUnchecked(Lcom/android/server/am/ControllerActivityInfo;)V

    .line 409
    :cond_0
    return-void
.end method

.method public onActivityStartUnchecked(Ljava/lang/String;IILjava/lang/String;IILjava/lang/String;Z)V
    .locals 20
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "pid"    # I
    .param p4, "packageName"    # Ljava/lang/String;
    .param p5, "launchedFromUid"    # I
    .param p6, "launchedFromPid"    # I
    .param p7, "launchedFromPackage"    # Ljava/lang/String;
    .param p8, "isColdStart"    # Z

    .line 373
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v1, :cond_0

    .line 374
    iget-object v2, v0, Lcom/android/server/am/SmartPowerService;->mComponentsManager:Lcom/miui/server/smartpower/ComponentsManager;

    move-object/from16 v3, p1

    move/from16 v4, p6

    move/from16 v5, p2

    move-object/from16 v6, p4

    move/from16 v7, p8

    invoke-virtual/range {v2 .. v7}, Lcom/miui/server/smartpower/ComponentsManager;->activityStartBeforeLocked(Ljava/lang/String;IILjava/lang/String;Z)V

    .line 376
    invoke-static {}, Lcom/android/server/am/SystemPressureControllerStub;->getInstance()Lcom/android/server/am/SystemPressureControllerStub;

    move-result-object v1

    new-instance v10, Lcom/android/server/am/ControllerActivityInfo;

    move-object v2, v10

    move/from16 v3, p2

    move/from16 v4, p3

    move-object/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v2 .. v9}, Lcom/android/server/am/ControllerActivityInfo;-><init>(IILjava/lang/String;IILjava/lang/String;Z)V

    invoke-virtual {v1, v10}, Lcom/android/server/am/SystemPressureControllerStub;->activityStartBeforeLocked(Lcom/android/server/am/ControllerActivityInfo;)V

    .line 379
    iget-object v11, v0, Lcom/android/server/am/SmartPowerService;->mSmartDisplayPolicyManager:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    move-object/from16 v12, p1

    move/from16 v13, p2

    move/from16 v14, p3

    move-object/from16 v15, p4

    move/from16 v16, p5

    move/from16 v17, p6

    move-object/from16 v18, p7

    move/from16 v19, p8

    invoke-virtual/range {v11 .. v19}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyActivityStartUnchecked(Ljava/lang/String;IILjava/lang/String;IILjava/lang/String;Z)V

    .line 383
    :cond_0
    return-void
.end method

.method public onActivityVisibilityChanged(ILjava/lang/String;Lcom/android/server/wm/ActivityRecord;Z)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "processName"    # Ljava/lang/String;
    .param p3, "record"    # Lcom/android/server/wm/ActivityRecord;
    .param p4, "visible"    # Z

    .line 614
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 615
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mComponentsManager:Lcom/miui/server/smartpower/ComponentsManager;

    .line 616
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/smartpower/ComponentsManager;->activityVisibilityChangedLocked(ILjava/lang/String;Lcom/android/server/wm/ActivityRecord;Z)V

    .line 618
    :cond_0
    return-void
.end method

.method public onAlarmStatusChanged(IZ)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "active"    # Z

    .line 501
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 502
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mComponentsManager:Lcom/miui/server/smartpower/ComponentsManager;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/ComponentsManager;->alarmStatusChangedLocked(IZ)V

    .line 504
    :cond_0
    return-void
.end method

.method public onAppTransitionStartLocked(J)V
    .locals 1
    .param p1, "animDuration"    # J

    .line 1128
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 1129
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartDisplayPolicyManager:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyAppTransitionStartLocked(J)V

    .line 1131
    :cond_0
    return-void
.end method

.method public onBackupChanged(ZLcom/android/server/am/ProcessRecord;)V
    .locals 3
    .param p1, "active"    # Z
    .param p2, "proc"    # Lcom/android/server/am/ProcessRecord;

    .line 703
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 704
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    iget v1, p2, Lcom/android/server/am/ProcessRecord;->uid:I

    iget-object v2, p2, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, p1, v1, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->onBackupChanged(ZILjava/lang/String;)V

    .line 705
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/am/AppStateManager;->onBackupChanged(ZLcom/android/server/am/ProcessRecord;)V

    .line 707
    :cond_0
    return-void
.end method

.method public onBackupServiceAppChanged(ZII)V
    .locals 1
    .param p1, "active"    # Z
    .param p2, "uid"    # I
    .param p3, "pid"    # I

    .line 711
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 712
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/am/AppStateManager;->onBackupServiceAppChanged(ZII)V

    .line 714
    :cond_0
    return-void
.end method

.method public onBluetoothEvent(ZIIILjava/lang/String;I)V
    .locals 15
    .param p1, "isConnect"    # Z
    .param p2, "bleType"    # I
    .param p3, "uid"    # I
    .param p4, "pid"    # I
    .param p5, "pkg"    # Ljava/lang/String;
    .param p6, "data"    # I

    .line 773
    move-object v0, p0

    move/from16 v7, p3

    iget-boolean v1, v0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v1, :cond_6

    .line 774
    if-lez v7, :cond_3

    .line 775
    if-lez p4, :cond_0

    .line 776
    iget-object v1, v0, Lcom/android/server/am/SmartPowerService;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    move/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p6

    invoke-virtual/range {v1 .. v6}, Lcom/miui/server/smartpower/AppPowerResourceManager;->onBluetoothEvent(ZIIII)V

    move-object/from16 v2, p5

    goto/16 :goto_3

    .line 778
    :cond_0
    iget-object v1, v0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v1, v7}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v8

    .line 779
    .local v8, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-nez v8, :cond_1

    return-void

    .line 780
    :cond_1
    nop

    .line 781
    invoke-virtual {v8}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcessList()Ljava/util/ArrayList;

    move-result-object v9

    .line 782
    .local v9, "runningProcs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
    const/4 v1, 0x0

    move v10, v1

    .local v10, "i":I
    :goto_0
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v10, v1, :cond_2

    .line 783
    iget-object v1, v0, Lcom/android/server/am/SmartPowerService;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    .line 784
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPid()I

    move-result v5

    .line 783
    move/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move/from16 v6, p6

    invoke-virtual/range {v1 .. v6}, Lcom/miui/server/smartpower/AppPowerResourceManager;->onBluetoothEvent(ZIIII)V

    .line 782
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 786
    .end local v8    # "appState":Lcom/android/server/am/AppStateManager$AppState;
    .end local v9    # "runningProcs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
    .end local v10    # "i":I
    :cond_2
    move-object/from16 v2, p5

    goto :goto_3

    .line 787
    :cond_3
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 788
    iget-object v1, v0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    move-object/from16 v2, p5

    invoke-virtual {v1, v2}, Lcom/android/server/am/AppStateManager;->getAppState(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 789
    .local v1, "appStates":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/AppStateManager$AppState;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/am/AppStateManager$AppState;

    .line 790
    .local v4, "appState":Lcom/android/server/am/AppStateManager$AppState;
    invoke-virtual {v4}, Lcom/android/server/am/AppStateManager$AppState;->getUid()I

    move-result v5

    .line 791
    .local v5, "realUid":I
    nop

    .line 792
    invoke-virtual {v4}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcessList()Ljava/util/ArrayList;

    move-result-object v6

    .line 793
    .local v6, "runningProcs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
    const/4 v8, 0x0

    move v14, v8

    .local v14, "i":I
    :goto_2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v14, v8, :cond_4

    .line 794
    iget-object v8, v0, Lcom/android/server/am/SmartPowerService;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    .line 795
    invoke-virtual {v6, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    invoke-virtual {v9}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPid()I

    move-result v12

    .line 794
    move/from16 v9, p1

    move/from16 v10, p2

    move v11, v5

    move/from16 v13, p6

    invoke-virtual/range {v8 .. v13}, Lcom/miui/server/smartpower/AppPowerResourceManager;->onBluetoothEvent(ZIIII)V

    .line 793
    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    .line 797
    .end local v4    # "appState":Lcom/android/server/am/AppStateManager$AppState;
    .end local v5    # "realUid":I
    .end local v6    # "runningProcs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
    .end local v14    # "i":I
    :cond_4
    goto :goto_1

    .line 787
    .end local v1    # "appStates":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/AppStateManager$AppState;>;"
    :cond_5
    move-object/from16 v2, p5

    goto :goto_3

    .line 773
    :cond_6
    move-object/from16 v2, p5

    .line 800
    :cond_7
    :goto_3
    return-void
.end method

.method public onBroadcastStatusChanged(Lcom/android/server/am/ProcessRecord;ZLandroid/content/Intent;)V
    .locals 1
    .param p1, "processRecord"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "active"    # Z
    .param p3, "intent"    # Landroid/content/Intent;

    .line 415
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 416
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mComponentsManager:Lcom/miui/server/smartpower/ComponentsManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/ComponentsManager;->broadcastStatusChangedLocked(Lcom/android/server/am/ProcessRecord;ZLandroid/content/Intent;)V

    .line 419
    :cond_0
    return-void
.end method

.method public onContentProviderStatusChanged(ILcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ContentProviderRecord;Z)V
    .locals 2
    .param p1, "callingUid"    # I
    .param p2, "callingProc"    # Lcom/android/server/am/ProcessRecord;
    .param p3, "hostingProc"    # Lcom/android/server/am/ProcessRecord;
    .param p4, "record"    # Lcom/android/server/am/ContentProviderRecord;
    .param p5, "isRunning"    # Z

    .line 455
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    .line 456
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mComponentsManager:Lcom/miui/server/smartpower/ComponentsManager;

    invoke-virtual {p4}, Lcom/android/server/am/ContentProviderRecord;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p3, v1}, Lcom/miui/server/smartpower/ComponentsManager;->contentProviderStatusChangedLocked(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V

    .line 458
    :cond_0
    return-void
.end method

.method public onCpuExceptionEvents(I)V
    .locals 1
    .param p1, "type"    # I

    .line 1113
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 1114
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartCpuPolicyManager:Lcom/miui/server/smartpower/SmartCpuPolicyManager;

    invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->cpuExceptionEvents(I)V

    .line 1116
    :cond_0
    return-void
.end method

.method public onCpuPressureEvents(I)V
    .locals 1
    .param p1, "level"    # I

    .line 1106
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 1107
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartCpuPolicyManager:Lcom/miui/server/smartpower/SmartCpuPolicyManager;

    invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->cpuPressureEvents(I)V

    .line 1109
    :cond_0
    return-void
.end method

.method public onDisplayDeviceStateChangeLocked(I)V
    .locals 1
    .param p1, "deviceState"    # I

    .line 1195
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 1196
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartDisplayPolicyManager:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyDisplayDeviceStateChangeLocked(I)V

    .line 1198
    :cond_0
    return-void
.end method

.method public onDisplaySwitchResolutionLocked(III)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "density"    # I

    .line 1203
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 1204
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartDisplayPolicyManager:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyDisplaySwitchResolutionLocked(III)V

    .line 1206
    :cond_0
    return-void
.end method

.method public onExternalAudioRegister(II)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 318
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->onExternalAudioRegister(II)V

    .line 321
    :cond_0
    return-void
.end method

.method public onFocusedWindowChangeLocked(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "oldPackage"    # Ljava/lang/String;
    .param p2, "newPackage"    # Ljava/lang/String;

    .line 1187
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 1188
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartDisplayPolicyManager:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyFocusedWindowChangeLocked(Ljava/lang/String;Ljava/lang/String;)V

    .line 1190
    :cond_0
    return-void
.end method

.method public onForegroundActivityChangedLocked(Ljava/lang/String;IILjava/lang/String;IILjava/lang/String;ZIILjava/lang/String;)V
    .locals 11
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "pid"    # I
    .param p4, "packageName"    # Ljava/lang/String;
    .param p5, "launchedFromUid"    # I
    .param p6, "launchedFromPid"    # I
    .param p7, "launchedFromPackage"    # Ljava/lang/String;
    .param p8, "isColdStart"    # Z
    .param p9, "taskId"    # I
    .param p10, "callingUid"    # I
    .param p11, "callingPkg"    # Ljava/lang/String;

    .line 390
    move-object v0, p0

    iget-boolean v1, v0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v1, :cond_0

    .line 391
    iget-object v2, v0, Lcom/android/server/am/SmartPowerService;->mComponentsManager:Lcom/miui/server/smartpower/ComponentsManager;

    move/from16 v3, p6

    move v4, p2

    move v5, p3

    move-object v6, p1

    move/from16 v7, p9

    move/from16 v8, p10

    move-object/from16 v9, p11

    invoke-virtual/range {v2 .. v9}, Lcom/miui/server/smartpower/ComponentsManager;->onForegroundActivityChangedLocked(IIILjava/lang/String;IILjava/lang/String;)V

    .line 393
    invoke-static {}, Lcom/android/server/am/SystemPressureControllerStub;->getInstance()Lcom/android/server/am/SystemPressureControllerStub;

    move-result-object v1

    new-instance v10, Lcom/android/server/am/ControllerActivityInfo;

    move-object v2, v10

    move v3, p2

    move v4, p3

    move-object v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v2 .. v9}, Lcom/android/server/am/ControllerActivityInfo;-><init>(IILjava/lang/String;IILjava/lang/String;Z)V

    .line 394
    invoke-virtual {v1, v10}, Lcom/android/server/am/SystemPressureControllerStub;->foregroundActivityChangedLocked(Lcom/android/server/am/ControllerActivityInfo;)V

    .line 398
    :cond_0
    return-void
.end method

.method public onHoldScreenUidChanged(IZ)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "hold"    # Z

    .line 639
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 640
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/am/AppStateManager;->onHoldScreenUidChanged(IZ)V

    .line 642
    :cond_0
    return-void
.end method

.method public onInputMethodShow(I)V
    .locals 1
    .param p1, "uid"    # I

    .line 731
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 732
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager;->onInputMethodShow(I)V

    .line 734
    :cond_0
    return-void
.end method

.method public onInsetAnimationHide(I)V
    .locals 1
    .param p1, "type"    # I

    .line 1151
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 1152
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartDisplayPolicyManager:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyInsetAnimationHide(I)V

    .line 1154
    :cond_0
    return-void
.end method

.method public onInsetAnimationShow(I)V
    .locals 1
    .param p1, "type"    # I

    .line 1144
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 1145
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartDisplayPolicyManager:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyInsetAnimationShow(I)V

    .line 1147
    :cond_0
    return-void
.end method

.method public onMediaKey(I)V
    .locals 1
    .param p1, "uid"    # I

    .line 680
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 681
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager;->onMediaKey(I)V

    .line 683
    :cond_0
    return-void
.end method

.method public onMediaKey(II)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 673
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 674
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/am/AppStateManager;->onMediaKey(II)V

    .line 676
    :cond_0
    return-void
.end method

.method public onMultiTaskActionEnd(Lmiui/smartpower/MultiTaskActionManager$ActionInfo;)V
    .locals 4
    .param p1, "actionInfo"    # Lmiui/smartpower/MultiTaskActionManager$ActionInfo;

    .line 1262
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_1

    .line 1263
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    .line 1264
    .local v0, "callingPid":I
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    .line 1265
    .local v1, "callingUid":I
    invoke-direct {p0}, Lcom/android/server/am/SmartPowerService;->checkPermission()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1266
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Permission Denial: onMultiTaskActionEnd from pid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", uid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1268
    .local v2, "msg":Ljava/lang/String;
    const-string v3, "SmartPower"

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1269
    return-void

    .line 1271
    .end local v2    # "msg":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/android/server/am/SmartPowerService;->mSmartWindowPolicyManager:Lcom/miui/server/smartpower/SmartWindowPolicyManager;

    invoke-virtual {v2, p1, v0}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->onMultiTaskActionEnd(Lmiui/smartpower/MultiTaskActionManager$ActionInfo;I)V

    .line 1273
    .end local v0    # "callingPid":I
    .end local v1    # "callingUid":I
    :cond_1
    return-void
.end method

.method public onMultiTaskActionStart(Lmiui/smartpower/MultiTaskActionManager$ActionInfo;)V
    .locals 4
    .param p1, "actionInfo"    # Lmiui/smartpower/MultiTaskActionManager$ActionInfo;

    .line 1247
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_1

    .line 1248
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    .line 1249
    .local v0, "callingPid":I
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    .line 1250
    .local v1, "callingUid":I
    invoke-direct {p0}, Lcom/android/server/am/SmartPowerService;->checkPermission()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1251
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Permission Denial: onMultiTaskActionStart from pid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", uid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1253
    .local v2, "msg":Ljava/lang/String;
    const-string v3, "SmartPower"

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1254
    return-void

    .line 1256
    .end local v2    # "msg":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/android/server/am/SmartPowerService;->mSmartWindowPolicyManager:Lcom/miui/server/smartpower/SmartWindowPolicyManager;

    invoke-virtual {v2, p1, v0}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->onMultiTaskActionStart(Lmiui/smartpower/MultiTaskActionManager$ActionInfo;I)V

    .line 1258
    .end local v0    # "callingPid":I
    .end local v1    # "callingUid":I
    :cond_1
    return-void
.end method

.method public onPendingPackagesExempt(Ljava/lang/String;)V
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .line 665
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 666
    .local v0, "appStates":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/AppStateManager$AppState;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/AppStateManager$AppState;

    .line 667
    .local v2, "appState":Lcom/android/server/am/AppStateManager$AppState;
    iget-object v3, p0, Lcom/android/server/am/SmartPowerService;->mComponentsManager:Lcom/miui/server/smartpower/ComponentsManager;

    invoke-virtual {v2}, Lcom/android/server/am/AppStateManager$AppState;->getUid()I

    move-result v4

    const-string v5, "exempt"

    invoke-virtual {v3, v4, v5}, Lcom/miui/server/smartpower/ComponentsManager;->onSendPendingIntent(ILjava/lang/String;)V

    .line 668
    .end local v2    # "appState":Lcom/android/server/am/AppStateManager$AppState;
    goto :goto_0

    .line 669
    :cond_0
    return-void
.end method

.method public onPlayerEvent(IIII)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "piid"    # I
    .param p4, "event"    # I

    .line 286
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 287
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/smartpower/AppPowerResourceManager;->onPlayerEvent(IIII)V

    .line 290
    :cond_0
    return-void
.end method

.method public onPlayerRlease(III)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "piid"    # I

    .line 278
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/AppPowerResourceManager;->onPlayerRlease(III)V

    .line 282
    :cond_0
    return-void
.end method

.method public onPlayerTrack(IIII)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "piid"    # I
    .param p4, "sessionId"    # I

    .line 271
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/smartpower/AppPowerResourceManager;->onPlayerTrack(IIII)V

    .line 274
    :cond_0
    return-void
.end method

.method public onProcessKill(Lcom/android/server/am/ProcessRecord;)V
    .locals 1
    .param p1, "r"    # Lcom/android/server/am/ProcessRecord;

    .line 348
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 349
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager;->processKilledLocked(Lcom/android/server/am/ProcessRecord;)V

    .line 351
    :cond_0
    return-void
.end method

.method public onProcessStart(Lcom/android/server/am/ProcessRecord;)V
    .locals 2
    .param p1, "r"    # Lcom/android/server/am/ProcessRecord;

    .line 340
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 341
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/android/server/am/AppStateManager;->processStartedLocked(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V

    .line 343
    :cond_0
    return-void
.end method

.method public onProviderConnectionChanged(Lcom/android/server/am/ContentProviderConnection;Z)V
    .locals 3
    .param p1, "conn"    # Lcom/android/server/am/ContentProviderConnection;
    .param p2, "isConnect"    # Z

    .line 491
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/android/server/am/ContentProviderConnection;->client:Lcom/android/server/am/ProcessRecord;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/server/am/ContentProviderConnection;->provider:Lcom/android/server/am/ContentProviderRecord;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/server/am/ContentProviderConnection;->provider:Lcom/android/server/am/ContentProviderRecord;

    iget-object v0, v0, Lcom/android/server/am/ContentProviderRecord;->proc:Lcom/android/server/am/ProcessRecord;

    if-eqz v0, :cond_0

    .line 493
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mComponentsManager:Lcom/miui/server/smartpower/ComponentsManager;

    iget-object v1, p1, Lcom/android/server/am/ContentProviderConnection;->client:Lcom/android/server/am/ProcessRecord;

    iget-object v2, p1, Lcom/android/server/am/ContentProviderConnection;->provider:Lcom/android/server/am/ContentProviderRecord;

    iget-object v2, v2, Lcom/android/server/am/ContentProviderRecord;->proc:Lcom/android/server/am/ProcessRecord;

    invoke-virtual {v0, v1, v2, p2}, Lcom/miui/server/smartpower/ComponentsManager;->providerConnectionChangedLocked(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;Z)V

    .line 496
    :cond_0
    return-void
.end method

.method public onRecentsAnimationEnd()V
    .locals 1

    .line 1165
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 1166
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartDisplayPolicyManager:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-virtual {v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyRecentsAnimationEnd()V

    .line 1168
    :cond_0
    return-void
.end method

.method public onRecentsAnimationStart(Lcom/android/server/wm/ActivityRecord;)V
    .locals 1
    .param p1, "targetActivity"    # Lcom/android/server/wm/ActivityRecord;

    .line 1158
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 1159
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartDisplayPolicyManager:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyRecentsAnimationStart(Lcom/android/server/wm/ActivityRecord;)V

    .line 1161
    :cond_0
    return-void
.end method

.method public onRecorderEvent(III)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "riid"    # I
    .param p3, "event"    # I

    .line 310
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 311
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/AppPowerResourceManager;->onRecorderEvent(III)V

    .line 314
    :cond_0
    return-void
.end method

.method public onRecorderRlease(II)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "riid"    # I

    .line 302
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 303
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/AppPowerResourceManager;->onRecorderRlease(II)V

    .line 306
    :cond_0
    return-void
.end method

.method public onRecorderTrack(III)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "riid"    # I

    .line 294
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 295
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/AppPowerResourceManager;->onRecorderTrack(III)V

    .line 298
    :cond_0
    return-void
.end method

.method public onReleaseLocation(IILandroid/location/ILocationListener;)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "listener"    # Landroid/location/ILocationListener;

    .line 747
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 748
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/AppPowerResourceManager;->onReleaseLocation(IILandroid/location/ILocationListener;)V

    .line 750
    :cond_0
    return-void
.end method

.method public onReleaseWakelock(Landroid/os/IBinder;I)V
    .locals 1
    .param p1, "lock"    # Landroid/os/IBinder;
    .param p2, "flags"    # I

    .line 765
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 766
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/AppPowerResourceManager;->onReleaseWakelock(Landroid/os/IBinder;I)V

    .line 768
    :cond_0
    return-void
.end method

.method public onRemoteAnimationEnd()V
    .locals 1

    .line 1179
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 1180
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartDisplayPolicyManager:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-virtual {v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyRemoteAnimationEnd()V

    .line 1182
    :cond_0
    return-void
.end method

.method public onRemoteAnimationStart([Landroid/view/RemoteAnimationTarget;)V
    .locals 1
    .param p1, "appTargets"    # [Landroid/view/RemoteAnimationTarget;

    .line 1172
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 1173
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartDisplayPolicyManager:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyRemoteAnimationStart([Landroid/view/RemoteAnimationTarget;)V

    .line 1175
    :cond_0
    return-void
.end method

.method public onSendPendingIntent(Lcom/android/server/am/PendingIntentRecord$Key;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 4
    .param p1, "key"    # Lcom/android/server/am/PendingIntentRecord$Key;
    .param p2, "callingPkg"    # Ljava/lang/String;
    .param p3, "targetPkg"    # Ljava/lang/String;
    .param p4, "intent"    # Landroid/content/Intent;

    .line 657
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 658
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mPackageManager:Landroid/content/pm/PackageManagerInternal;

    const-wide/16 v1, 0x0

    iget v3, p1, Lcom/android/server/am/PendingIntentRecord$Key;->userId:I

    invoke-virtual {v0, p3, v1, v2, v3}, Landroid/content/pm/PackageManagerInternal;->getPackageUid(Ljava/lang/String;JI)I

    move-result v0

    .line 659
    .local v0, "uid":I
    iget-object v1, p0, Lcom/android/server/am/SmartPowerService;->mComponentsManager:Lcom/miui/server/smartpower/ComponentsManager;

    invoke-virtual {p1}, Lcom/android/server/am/PendingIntentRecord$Key;->typeName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/miui/server/smartpower/ComponentsManager;->onSendPendingIntent(ILjava/lang/String;)V

    .line 661
    .end local v0    # "uid":I
    :cond_0
    return-void
.end method

.method public onServiceConnectionChanged(Lcom/android/server/am/AppBindRecord;Z)V
    .locals 8
    .param p1, "bindRecord"    # Lcom/android/server/am/AppBindRecord;
    .param p2, "isConnect"    # Z

    .line 463
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_3

    if-eqz p1, :cond_3

    iget-object v0, p1, Lcom/android/server/am/AppBindRecord;->client:Lcom/android/server/am/ProcessRecord;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/android/server/am/AppBindRecord;->service:Lcom/android/server/am/ServiceRecord;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/android/server/am/AppBindRecord;->intent:Lcom/android/server/am/IntentBindRecord;

    if-eqz v0, :cond_3

    .line 466
    if-nez p2, :cond_0

    iget-object v0, p1, Lcom/android/server/am/AppBindRecord;->intent:Lcom/android/server/am/IntentBindRecord;

    iget-object v0, v0, Lcom/android/server/am/IntentBindRecord;->apps:Landroid/util/ArrayMap;

    iget-object v1, p1, Lcom/android/server/am/AppBindRecord;->client:Lcom/android/server/am/ProcessRecord;

    invoke-virtual {v0, v1}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 467
    return-void

    .line 469
    :cond_0
    iget-object v0, p1, Lcom/android/server/am/AppBindRecord;->service:Lcom/android/server/am/ServiceRecord;

    iget-object v0, v0, Lcom/android/server/am/ServiceRecord;->app:Lcom/android/server/am/ProcessRecord;

    .line 470
    .local v0, "serviceApp":Lcom/android/server/am/ProcessRecord;
    if-nez v0, :cond_2

    .line 472
    iget-object v1, p1, Lcom/android/server/am/AppBindRecord;->service:Lcom/android/server/am/ServiceRecord;

    iget-object v1, v1, Lcom/android/server/am/ServiceRecord;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget v1, v1, Landroid/content/pm/ServiceInfo;->flags:I

    .line 473
    .local v1, "serviceFlag":I
    and-int/lit8 v2, v1, 0x2

    if-eqz v2, :cond_1

    .line 474
    iget-object v2, p1, Lcom/android/server/am/AppBindRecord;->service:Lcom/android/server/am/ServiceRecord;

    iget-object v0, v2, Lcom/android/server/am/ServiceRecord;->isolationHostProc:Lcom/android/server/am/ProcessRecord;

    goto :goto_0

    .line 475
    :cond_1
    iget-object v2, p1, Lcom/android/server/am/AppBindRecord;->service:Lcom/android/server/am/ServiceRecord;

    iget-object v2, v2, Lcom/android/server/am/ServiceRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v2, :cond_2

    .line 476
    iget-object v2, p0, Lcom/android/server/am/SmartPowerService;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget-object v3, p1, Lcom/android/server/am/AppBindRecord;->service:Lcom/android/server/am/ServiceRecord;

    iget-object v3, v3, Lcom/android/server/am/ServiceRecord;->processName:Ljava/lang/String;

    iget-object v4, p1, Lcom/android/server/am/AppBindRecord;->service:Lcom/android/server/am/ServiceRecord;

    iget-object v4, v4, Lcom/android/server/am/ServiceRecord;->appInfo:Landroid/content/pm/ApplicationInfo;

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v2, v3, v4}, Lcom/android/server/am/ActivityManagerService;->getProcessRecordLocked(Ljava/lang/String;I)Lcom/android/server/am/ProcessRecord;

    move-result-object v0

    .line 480
    .end local v1    # "serviceFlag":I
    :cond_2
    :goto_0
    if-eqz v0, :cond_3

    iget v1, v0, Lcom/android/server/am/ProcessRecord;->mPid:I

    if-lez v1, :cond_3

    .line 481
    iget-object v2, p0, Lcom/android/server/am/SmartPowerService;->mComponentsManager:Lcom/miui/server/smartpower/ComponentsManager;

    iget-object v3, p1, Lcom/android/server/am/AppBindRecord;->client:Lcom/android/server/am/ProcessRecord;

    iget-object v1, p1, Lcom/android/server/am/AppBindRecord;->intent:Lcom/android/server/am/IntentBindRecord;

    .line 482
    invoke-virtual {v1}, Lcom/android/server/am/IntentBindRecord;->collectFlags()J

    move-result-wide v6

    .line 481
    move-object v4, v0

    move v5, p2

    invoke-virtual/range {v2 .. v7}, Lcom/miui/server/smartpower/ComponentsManager;->serviceConnectionChangedLocked(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;ZJ)V

    .line 486
    .end local v0    # "serviceApp":Lcom/android/server/am/ProcessRecord;
    :cond_3
    return-void
.end method

.method public onServiceStatusChanged(Lcom/android/server/am/ProcessRecord;ZLcom/android/server/am/ServiceRecord;I)V
    .locals 6
    .param p1, "processRecord"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "active"    # Z
    .param p3, "record"    # Lcom/android/server/am/ServiceRecord;
    .param p4, "execution"    # I

    .line 425
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    .line 426
    if-eqz p2, :cond_1

    const/4 v0, 0x2

    if-ne p4, v0, :cond_1

    .line 427
    invoke-virtual {p3}, Lcom/android/server/am/ServiceRecord;->getConnections()Landroid/util/ArrayMap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 428
    .local v1, "connList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/ConnectionRecord;

    .line 429
    .local v3, "conn":Lcom/android/server/am/ConnectionRecord;
    iget-object v4, v3, Lcom/android/server/am/ConnectionRecord;->binding:Lcom/android/server/am/AppBindRecord;

    const/4 v5, 0x1

    invoke-virtual {p0, v4, v5}, Lcom/android/server/am/SmartPowerService;->onServiceConnectionChanged(Lcom/android/server/am/AppBindRecord;Z)V

    .line 430
    .end local v3    # "conn":Lcom/android/server/am/ConnectionRecord;
    goto :goto_1

    .line 431
    .end local v1    # "connList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
    :cond_0
    goto :goto_0

    .line 433
    :cond_1
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mComponentsManager:Lcom/miui/server/smartpower/ComponentsManager;

    iget-object v1, p3, Lcom/android/server/am/ServiceRecord;->shortInstanceName:Ljava/lang/String;

    invoke-virtual {v0, p1, p2, v1, p4}, Lcom/miui/server/smartpower/ComponentsManager;->serviceStatusChangedLocked(Lcom/android/server/am/ProcessRecord;ZLjava/lang/String;I)V

    .line 437
    :cond_2
    return-void
.end method

.method public onShellCommand(Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)V
    .locals 8
    .param p1, "in"    # Ljava/io/FileDescriptor;
    .param p2, "out"    # Ljava/io/FileDescriptor;
    .param p3, "err"    # Ljava/io/FileDescriptor;
    .param p4, "args"    # [Ljava/lang/String;
    .param p5, "callback"    # Landroid/os/ShellCallback;
    .param p6, "resultReceiver"    # Landroid/os/ResultReceiver;

    .line 1403
    new-instance v0, Lcom/android/server/am/SmartPowerService$SmartPowerShellCommand;

    invoke-direct {v0, p0}, Lcom/android/server/am/SmartPowerService$SmartPowerShellCommand;-><init>(Lcom/android/server/am/SmartPowerService;)V

    .line 1404
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, Lcom/android/server/am/SmartPowerService$SmartPowerShellCommand;->exec(Landroid/os/Binder;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)I

    .line 1405
    return-void
.end method

.method public onTransitionAnimateEnd(I)V
    .locals 5
    .param p1, "type"    # I

    .line 1298
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_1

    .line 1299
    invoke-direct {p0}, Lcom/android/server/am/SmartPowerService;->checkPermission()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1300
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Permission Denial: onTransitionAnimateEnd from pid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1301
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1302
    .local v0, "msg":Ljava/lang/String;
    const-string v1, "SmartPower"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1303
    return-void

    .line 1305
    .end local v0    # "msg":Ljava/lang/String;
    :cond_0
    invoke-static {p1}, Lmiui/smartpower/SmartPowerManager;->transitionAnimateTypeToString(I)Ljava/lang/String;

    move-result-object v0

    .line 1306
    .local v0, "typeString":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Transit: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const-wide/16 v3, 0x20

    invoke-static {v3, v4, v1, v2}, Landroid/os/Trace;->asyncTraceEnd(JLjava/lang/String;I)V

    .line 1309
    .end local v0    # "typeString":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public onTransitionAnimateStart(ILandroid/window/TransitionInfo;)V
    .locals 5
    .param p1, "type"    # I
    .param p2, "info"    # Landroid/window/TransitionInfo;

    .line 1280
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_1

    .line 1281
    invoke-direct {p0}, Lcom/android/server/am/SmartPowerService;->checkPermission()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1282
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Permission Denial: onTransitionAnimateStart from pid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1283
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1284
    .local v0, "msg":Ljava/lang/String;
    const-string v1, "SmartPower"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1285
    return-void

    .line 1287
    .end local v0    # "msg":Ljava/lang/String;
    :cond_0
    invoke-static {p1}, Lmiui/smartpower/SmartPowerManager;->transitionAnimateTypeToString(I)Ljava/lang/String;

    move-result-object v0

    .line 1288
    .local v0, "typeString":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Transit: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const-wide/16 v3, 0x20

    invoke-static {v3, v4, v1, v2}, Landroid/os/Trace;->asyncTraceBegin(JLjava/lang/String;I)V

    .line 1291
    .end local v0    # "typeString":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public onTransitionEnd(III)V
    .locals 1
    .param p1, "syncId"    # I
    .param p2, "type"    # I
    .param p3, "flags"    # I

    .line 1326
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 1327
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartDisplayPolicyManager:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyTransitionEnd(III)V

    .line 1329
    :cond_0
    return-void
.end method

.method public onTransitionStart(Landroid/window/TransitionInfo;)V
    .locals 1
    .param p1, "info"    # Landroid/window/TransitionInfo;

    .line 1316
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 1317
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartDisplayPolicyManager:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyTransitionStart(Landroid/window/TransitionInfo;)V

    .line 1319
    :cond_0
    return-void
.end method

.method public onUsbStateChanged(ZJLandroid/content/Intent;)V
    .locals 2
    .param p1, "isConnected"    # Z
    .param p2, "functions"    # J
    .param p4, "intent"    # Landroid/content/Intent;

    .line 718
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 719
    invoke-static {p2, p3}, Lcom/android/server/am/SmartPowerService;->isUsbDataTransferActive(J)Z

    move-result v0

    .line 720
    .local v0, "isUsbDataTrans":Z
    iget-object v1, p0, Lcom/android/server/am/SmartPowerService;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    invoke-virtual {v1, p1, v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->onUsbStateChanged(ZZ)V

    .line 722
    .end local v0    # "isUsbDataTrans":Z
    :cond_0
    return-void
.end method

.method public onVpnChanged(ZILjava/lang/String;)V
    .locals 1
    .param p1, "active"    # Z
    .param p2, "uid"    # I
    .param p3, "packageName"    # Ljava/lang/String;

    .line 695
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 696
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->onVpnChanged(ZILjava/lang/String;)V

    .line 698
    :cond_0
    return-void
.end method

.method public onWallpaperComponentChanged(ZILjava/lang/String;)V
    .locals 1
    .param p1, "active"    # Z
    .param p2, "uid"    # I
    .param p3, "packageName"    # Ljava/lang/String;

    .line 688
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 689
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->onWallpaperComponentChangedLocked(ZILjava/lang/String;)V

    .line 691
    :cond_0
    return-void
.end method

.method public onWindowAnimationStartLocked(JLandroid/view/SurfaceControl;)V
    .locals 1
    .param p1, "animDuration"    # J
    .param p3, "animationLeash"    # Landroid/view/SurfaceControl;

    .line 1136
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 1137
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartDisplayPolicyManager:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyWindowAnimationStartLocked(JLandroid/view/SurfaceControl;)V

    .line 1140
    :cond_0
    return-void
.end method

.method public onWindowVisibilityChanged(IILcom/android/server/policy/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;Z)V
    .locals 7
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "win"    # Lcom/android/server/policy/WindowManagerPolicy$WindowState;
    .param p4, "attrs"    # Landroid/view/WindowManager$LayoutParams;
    .param p5, "visible"    # Z

    .line 631
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 632
    iget-object v1, p0, Lcom/android/server/am/SmartPowerService;->mComponentsManager:Lcom/miui/server/smartpower/ComponentsManager;

    move v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/miui/server/smartpower/ComponentsManager;->windowVisibilityChangedLocked(IILcom/android/server/policy/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;Z)V

    .line 633
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartDisplayPolicyManager:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-virtual {v0, p3, p4, p5}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyWindowVisibilityChangedLocked(Lcom/android/server/policy/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;Z)V

    .line 635
    :cond_0
    return-void
.end method

.method public playbackStateChanged(IIII)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "oldState"    # I
    .param p4, "newState"    # I

    .line 249
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 250
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/smartpower/AppPowerResourceManager;->playbackStateChanged(IIII)V

    .line 253
    :cond_0
    return-void
.end method

.method public powerFrozenServiceReady(Z)V
    .locals 1
    .param p1, "ready"    # Z

    .line 554
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 555
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mPowerFrozenManager:Lcom/miui/server/smartpower/PowerFrozenManager;

    invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/PowerFrozenManager;->serviceReady(Z)V

    .line 557
    :cond_0
    return-void
.end method

.method public recordAudioFocus(IILjava/lang/String;Z)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "clientId"    # Ljava/lang/String;
    .param p4, "request"    # Z

    .line 257
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/smartpower/AppPowerResourceManager;->recordAudioFocus(IILjava/lang/String;Z)V

    .line 260
    :cond_0
    return-void
.end method

.method public recordAudioFocusLoss(ILjava/lang/String;I)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "clientId"    # Ljava/lang/String;
    .param p3, "focusLoss"    # I

    .line 264
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/AppPowerResourceManager;->recordAudioFocusLoss(ILjava/lang/String;I)V

    .line 267
    :cond_0
    return-void
.end method

.method public registThermalScenarioCallback(Lmiui/smartpower/IScenarioCallback;)V
    .locals 2
    .param p1, "callback"    # Lmiui/smartpower/IScenarioCallback;

    .line 228
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_1

    .line 229
    invoke-direct {p0}, Lcom/android/server/am/SmartPowerService;->checkPermission()Z

    move-result v0

    if-nez v0, :cond_0

    .line 230
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Permission Denial: registThermalScenarioCallback from pid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 231
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 232
    .local v0, "msg":Ljava/lang/String;
    const-string v1, "SmartPower"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    return-void

    .line 235
    .end local v0    # "msg":Ljava/lang/String;
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartThermalPolicyManager:Lcom/miui/server/smartpower/SmartThermalPolicyManager;

    invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->registThermalScenarioCallback(Lmiui/smartpower/IScenarioCallback;)V

    .line 237
    :cond_1
    return-void
.end method

.method protected registerAppStateListener(Lcom/android/server/am/AppStateManager$IProcessStateCallback;)V
    .locals 1
    .param p1, "callback"    # Lcom/android/server/am/AppStateManager$IProcessStateCallback;

    .line 1093
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 1094
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager;->registerAppStateListener(Lcom/android/server/am/AppStateManager$IProcessStateCallback;)Z

    .line 1096
    :cond_0
    return-void
.end method

.method public registerMultiTaskActionListener(ILandroid/os/Handler;Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;)Z
    .locals 1
    .param p1, "listenerFlag"    # I
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "listener"    # Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;

    .line 1225
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 1226
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartWindowPolicyManager:Lcom/miui/server/smartpower/SmartWindowPolicyManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->registerMultiTaskActionListener(ILandroid/os/Handler;Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;)Z

    move-result v0

    return v0

    .line 1229
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public removeFrozenPid(II)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 605
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 606
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mPowerFrozenManager:Lcom/miui/server/smartpower/PowerFrozenManager;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/PowerFrozenManager;->removeFrozenPid(II)V

    .line 608
    :cond_0
    return-void
.end method

.method public reportBinderState(IIIIJ)V
    .locals 8
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "tid"    # I
    .param p4, "binderState"    # I
    .param p5, "now"    # J

    .line 584
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 585
    iget-object v1, p0, Lcom/android/server/am/SmartPowerService;->mPowerFrozenManager:Lcom/miui/server/smartpower/PowerFrozenManager;

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move-wide v6, p5

    invoke-virtual/range {v1 .. v7}, Lcom/miui/server/smartpower/PowerFrozenManager;->reportBinderState(IIIIJ)V

    .line 587
    :cond_0
    return-void
.end method

.method public reportBinderTrans(IIIIIZJJ)V
    .locals 13
    .param p1, "dstUid"    # I
    .param p2, "dstPid"    # I
    .param p3, "callerUid"    # I
    .param p4, "callerPid"    # I
    .param p5, "callerTid"    # I
    .param p6, "isOneway"    # Z
    .param p7, "now"    # J
    .param p9, "buffer"    # J

    .line 576
    move-object v0, p0

    iget-boolean v1, v0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v1, :cond_0

    .line 577
    iget-object v2, v0, Lcom/android/server/am/SmartPowerService;->mPowerFrozenManager:Lcom/miui/server/smartpower/PowerFrozenManager;

    move v3, p1

    move v4, p2

    move/from16 v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    move-wide/from16 v9, p7

    move-wide/from16 v11, p9

    invoke-virtual/range {v2 .. v12}, Lcom/miui/server/smartpower/PowerFrozenManager;->reportBinderTrans(IIIIIZJJ)V

    .line 580
    :cond_0
    return-void
.end method

.method public reportNet(IJ)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "now"    # J

    .line 568
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 569
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mPowerFrozenManager:Lcom/miui/server/smartpower/PowerFrozenManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/PowerFrozenManager;->reportNet(IJ)V

    .line 571
    :cond_0
    return-void
.end method

.method public reportSignal(IIJ)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "now"    # J

    .line 561
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 562
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mPowerFrozenManager:Lcom/miui/server/smartpower/PowerFrozenManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/smartpower/PowerFrozenManager;->reportSignal(IIJ)V

    .line 564
    :cond_0
    return-void
.end method

.method public reportTrackStatus(IIIZ)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "sessionId"    # I
    .param p4, "isMuted"    # Z

    .line 804
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 805
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/smartpower/AppPowerResourceManager;->reportTrackStatus(IIIZ)V

    .line 807
    :cond_0
    return-void
.end method

.method public setProcessPss(Lcom/android/server/am/ProcessRecord;JJ)V
    .locals 7
    .param p1, "r"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "pss"    # J
    .param p4, "swapPss"    # J

    .line 363
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 364
    iget-object v1, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    move-object v2, p1

    move-wide v3, p2

    move-wide v5, p4

    invoke-virtual/range {v1 .. v6}, Lcom/android/server/am/AppStateManager;->setProcessPss(Lcom/android/server/am/ProcessRecord;JJ)V

    .line 366
    :cond_0
    return-void
.end method

.method public shouldInterceptAlarm(II)Z
    .locals 1
    .param p1, "uid"    # I
    .param p2, "type"    # I

    .line 509
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 510
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->shouldInterceptAlarmLocked(II)Z

    move-result v0

    return v0

    .line 512
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public shouldInterceptBroadcast(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/BroadcastRecord;)Z
    .locals 9
    .param p1, "recProc"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "record"    # Lcom/android/server/am/BroadcastRecord;

    .line 518
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 519
    iget-object v1, p0, Lcom/android/server/am/SmartPowerService;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    iget v2, p2, Lcom/android/server/am/BroadcastRecord;->callingUid:I

    iget v3, p2, Lcom/android/server/am/BroadcastRecord;->callingPid:I

    iget-object v4, p2, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    iget-object v6, p2, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    iget-boolean v7, p2, Lcom/android/server/am/BroadcastRecord;->ordered:Z

    iget-boolean v8, p2, Lcom/android/server/am/BroadcastRecord;->sticky:Z

    move-object v5, p1

    invoke-virtual/range {v1 .. v8}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->shouldInterceptBroadcastLocked(IILjava/lang/String;Lcom/android/server/am/ProcessRecord;Landroid/content/Intent;ZZ)Z

    move-result v0

    return v0

    .line 523
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public shouldInterceptProvider(ILcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;Z)Z
    .locals 1
    .param p1, "callingUid"    # I
    .param p2, "callingProc"    # Lcom/android/server/am/ProcessRecord;
    .param p3, "hostingProc"    # Lcom/android/server/am/ProcessRecord;
    .param p4, "isRunning"    # Z

    .line 443
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    .line 444
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    .line 445
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->shouldInterceptProviderLocked(ILcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;Z)Z

    move-result v0

    .line 444
    return v0

    .line 447
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public shouldInterceptService(Landroid/content/Intent;Lmiui/security/CallerInfo;Landroid/content/pm/ServiceInfo;)Z
    .locals 1
    .param p1, "service"    # Landroid/content/Intent;
    .param p2, "callingInfo"    # Lmiui/security/CallerInfo;
    .param p3, "serviceInfo"    # Landroid/content/pm/ServiceInfo;

    .line 537
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    iget-object v0, p3, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v0, :cond_0

    .line 539
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->shouldInterceptService(Landroid/content/Intent;Lmiui/security/CallerInfo;Landroid/content/pm/ServiceInfo;)Z

    move-result v0

    return v0

    .line 541
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public shouldInterceptUpdateDisplayModeSpecs(ILcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;)Z
    .locals 1
    .param p1, "displayId"    # I
    .param p2, "modeSpecs"    # Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;

    .line 1212
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 1213
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartDisplayPolicyManager:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    .line 1214
    invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->shouldInterceptUpdateDisplayModeSpecs(ILcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;)Z

    move-result v0

    .line 1213
    return v0

    .line 1216
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public skipFrozenAppAnr(Landroid/content/pm/ApplicationInfo;ILjava/lang/String;)Z
    .locals 1
    .param p1, "info"    # Landroid/content/pm/ApplicationInfo;
    .param p2, "uid"    # I
    .param p3, "report"    # Ljava/lang/String;

    .line 528
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 529
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->skipFrozenAppAnr(Landroid/content/pm/ApplicationInfo;ILjava/lang/String;)Z

    move-result v0

    return v0

    .line 531
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method systemReady(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 156
    iput-object p1, p0, Lcom/android/server/am/SmartPowerService;->mContext:Landroid/content/Context;

    .line 157
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mHandlerTh:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 158
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/ActivityManagerService;

    iput-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAMS:Lcom/android/server/am/ActivityManagerService;

    .line 159
    invoke-static {}, Landroid/app/ActivityTaskManager;->getService()Landroid/app/IActivityTaskManager;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/ActivityTaskManagerService;

    iput-object v0, p0, Lcom/android/server/am/SmartPowerService;->mATMS:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 160
    const-string/jumbo v0, "window"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/WindowManagerService;

    iput-object v0, p0, Lcom/android/server/am/SmartPowerService;->mWMS:Lcom/android/server/wm/WindowManagerService;

    .line 161
    const-class v0, Landroid/content/pm/PackageManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageManagerInternal;

    iput-object v0, p0, Lcom/android/server/am/SmartPowerService;->mPackageManager:Landroid/content/pm/PackageManagerInternal;

    .line 162
    new-instance v0, Lcom/android/server/am/SmartPowerService$MyHandler;

    iget-object v1, p0, Lcom/android/server/am/SmartPowerService;->mHandlerTh:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/am/SmartPowerService$MyHandler;-><init>(Lcom/android/server/am/SmartPowerService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/am/SmartPowerService;->mHandler:Lcom/android/server/am/SmartPowerService$MyHandler;

    .line 163
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mHandlerTh:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getThreadId()I

    move-result v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/Process;->setThreadGroupAndCpuset(II)V

    .line 165
    new-instance v0, Lcom/miui/server/smartpower/PowerFrozenManager;

    invoke-direct {v0}, Lcom/miui/server/smartpower/PowerFrozenManager;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/SmartPowerService;->mPowerFrozenManager:Lcom/miui/server/smartpower/PowerFrozenManager;

    .line 166
    new-instance v0, Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    iget-object v2, p0, Lcom/android/server/am/SmartPowerService;->mHandlerTh:Landroid/os/HandlerThread;

    .line 167
    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/SmartPowerService;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget-object v4, p0, Lcom/android/server/am/SmartPowerService;->mPowerFrozenManager:Lcom/miui/server/smartpower/PowerFrozenManager;

    invoke-direct {v0, p1, v2, v3, v4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/android/server/am/ActivityManagerService;Lcom/miui/server/smartpower/PowerFrozenManager;)V

    iput-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    .line 168
    new-instance v0, Lcom/miui/server/smartpower/AppPowerResourceManager;

    iget-object v2, p0, Lcom/android/server/am/SmartPowerService;->mHandlerTh:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, p1, v2}, Lcom/miui/server/smartpower/AppPowerResourceManager;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    .line 169
    new-instance v0, Lcom/android/server/am/AppStateManager;

    iget-object v2, p0, Lcom/android/server/am/SmartPowerService;->mHandlerTh:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/SmartPowerService;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget-object v4, p0, Lcom/android/server/am/SmartPowerService;->mPowerFrozenManager:Lcom/miui/server/smartpower/PowerFrozenManager;

    invoke-direct {v0, p1, v2, v3, v4}, Lcom/android/server/am/AppStateManager;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/android/server/am/ActivityManagerService;Lcom/miui/server/smartpower/PowerFrozenManager;)V

    iput-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    .line 170
    new-instance v0, Lcom/miui/server/smartpower/ComponentsManager;

    iget-object v2, p0, Lcom/android/server/am/SmartPowerService;->mHandlerTh:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, p1, v2}, Lcom/miui/server/smartpower/ComponentsManager;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/am/SmartPowerService;->mComponentsManager:Lcom/miui/server/smartpower/ComponentsManager;

    .line 171
    new-instance v0, Lcom/miui/server/smartpower/SmartCpuPolicyManager;

    iget-object v2, p0, Lcom/android/server/am/SmartPowerService;->mAMS:Lcom/android/server/am/ActivityManagerService;

    invoke-direct {v0, p1, v2}, Lcom/miui/server/smartpower/SmartCpuPolicyManager;-><init>(Landroid/content/Context;Lcom/android/server/am/ActivityManagerService;)V

    iput-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartCpuPolicyManager:Lcom/miui/server/smartpower/SmartCpuPolicyManager;

    .line 172
    new-instance v0, Lcom/miui/server/smartpower/SmartScenarioManager;

    iget-object v2, p0, Lcom/android/server/am/SmartPowerService;->mHandlerTh:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, p1, v2}, Lcom/miui/server/smartpower/SmartScenarioManager;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartScenarioManager:Lcom/miui/server/smartpower/SmartScenarioManager;

    .line 173
    new-instance v2, Lcom/miui/server/smartpower/SmartThermalPolicyManager;

    invoke-direct {v2, v0}, Lcom/miui/server/smartpower/SmartThermalPolicyManager;-><init>(Lcom/miui/server/smartpower/SmartScenarioManager;)V

    iput-object v2, p0, Lcom/android/server/am/SmartPowerService;->mSmartThermalPolicyManager:Lcom/miui/server/smartpower/SmartThermalPolicyManager;

    .line 174
    new-instance v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    iget-object v2, p0, Lcom/android/server/am/SmartPowerService;->mWMS:Lcom/android/server/wm/WindowManagerService;

    invoke-direct {v0, p1, v2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;-><init>(Landroid/content/Context;Lcom/android/server/wm/WindowManagerService;)V

    iput-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartDisplayPolicyManager:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    .line 175
    new-instance v0, Lcom/miui/server/smartpower/SmartWindowPolicyManager;

    iget-object v2, p0, Lcom/android/server/am/SmartPowerService;->mHandlerTh:Landroid/os/HandlerThread;

    .line 176
    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/SmartPowerService;->mWMS:Lcom/android/server/wm/WindowManagerService;

    invoke-direct {v0, p1, v2, v3}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/android/server/wm/WindowManagerService;)V

    iput-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartWindowPolicyManager:Lcom/miui/server/smartpower/SmartWindowPolicyManager;

    .line 177
    new-instance v0, Lcom/miui/server/smartpower/SmartBoostPolicyManager;

    iget-object v2, p0, Lcom/android/server/am/SmartPowerService;->mHandlerTh:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, p1, v2}, Lcom/miui/server/smartpower/SmartBoostPolicyManager;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartBoostPolicyManager:Lcom/miui/server/smartpower/SmartBoostPolicyManager;

    .line 179
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    iget-object v2, p0, Lcom/android/server/am/SmartPowerService;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    iget-object v3, p0, Lcom/android/server/am/SmartPowerService;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    iget-object v4, p0, Lcom/android/server/am/SmartPowerService;->mSmartScenarioManager:Lcom/miui/server/smartpower/SmartScenarioManager;

    invoke-virtual {v0, v2, v3, v4}, Lcom/android/server/am/AppStateManager;->init(Lcom/miui/server/smartpower/AppPowerResourceManager;Lcom/miui/server/smartpower/SmartPowerPolicyManager;Lcom/miui/server/smartpower/SmartScenarioManager;)V

    .line 181
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    invoke-virtual {v0}, Lcom/miui/server/smartpower/AppPowerResourceManager;->init()V

    .line 182
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    iget-object v2, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    iget-object v3, p0, Lcom/android/server/am/SmartPowerService;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    invoke-virtual {v0, v2, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->init(Lcom/android/server/am/AppStateManager;Lcom/miui/server/smartpower/AppPowerResourceManager;)V

    .line 183
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mComponentsManager:Lcom/miui/server/smartpower/ComponentsManager;

    iget-object v2, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    iget-object v3, p0, Lcom/android/server/am/SmartPowerService;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    invoke-virtual {v0, v2, v3}, Lcom/miui/server/smartpower/ComponentsManager;->init(Lcom/android/server/am/AppStateManager;Lcom/miui/server/smartpower/SmartPowerPolicyManager;)V

    .line 184
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartCpuPolicyManager:Lcom/miui/server/smartpower/SmartCpuPolicyManager;

    iget-object v2, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    iget-object v3, p0, Lcom/android/server/am/SmartPowerService;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    invoke-virtual {v0, v2, v3}, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->init(Lcom/android/server/am/AppStateManager;Lcom/miui/server/smartpower/SmartPowerPolicyManager;)V

    .line 185
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartDisplayPolicyManager:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    iget-object v2, p0, Lcom/android/server/am/SmartPowerService;->mSmartScenarioManager:Lcom/miui/server/smartpower/SmartScenarioManager;

    iget-object v3, p0, Lcom/android/server/am/SmartPowerService;->mSmartWindowPolicyManager:Lcom/miui/server/smartpower/SmartWindowPolicyManager;

    invoke-virtual {v0, v2, v3}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->init(Lcom/miui/server/smartpower/SmartScenarioManager;Lcom/miui/server/smartpower/SmartWindowPolicyManager;)V

    .line 186
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartBoostPolicyManager:Lcom/miui/server/smartpower/SmartBoostPolicyManager;

    invoke-virtual {v0}, Lcom/miui/server/smartpower/SmartBoostPolicyManager;->init()V

    .line 187
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartWindowPolicyManager:Lcom/miui/server/smartpower/SmartWindowPolicyManager;

    iget-object v2, p0, Lcom/android/server/am/SmartPowerService;->mSmartDisplayPolicyManager:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    iget-object v3, p0, Lcom/android/server/am/SmartPowerService;->mSmartBoostPolicyManager:Lcom/miui/server/smartpower/SmartBoostPolicyManager;

    invoke-virtual {v0, v2, v3}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->init(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Lcom/miui/server/smartpower/SmartBoostPolicyManager;)V

    .line 189
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/android/server/am/SmartPowerService;->registerCloudObserver(Landroid/content/Context;)V

    .line 190
    iput-boolean v1, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    .line 192
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartDisplayPolicyManager:Lcom/miui/server/smartpower/SmartDisplayPolicyManager;

    invoke-virtual {v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->systemReady()V

    .line 193
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartBoostPolicyManager:Lcom/miui/server/smartpower/SmartBoostPolicyManager;

    invoke-virtual {v0}, Lcom/miui/server/smartpower/SmartBoostPolicyManager;->systemReady()V

    .line 194
    return-void
.end method

.method public thawedByOther(II)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 598
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 599
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mPowerFrozenManager:Lcom/miui/server/smartpower/PowerFrozenManager;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/PowerFrozenManager;->thawedByOther(II)V

    .line 601
    :cond_0
    return-void
.end method

.method public uidAudioStatusChanged(IZ)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "active"    # Z

    .line 325
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/AppPowerResourceManager;->uidAudioStatusChanged(IZ)V

    .line 328
    :cond_0
    return-void
.end method

.method public uidVideoStatusChanged(IZ)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "active"    # Z

    .line 332
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 333
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/AppPowerResourceManager;->uidVideoStatusChanged(IZ)V

    .line 335
    :cond_0
    return-void
.end method

.method protected unRegisterAppStateListener(Lcom/android/server/am/AppStateManager$IProcessStateCallback;)V
    .locals 1
    .param p1, "callback"    # Lcom/android/server/am/AppStateManager$IProcessStateCallback;

    .line 1099
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 1100
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager;->unRegisterAppStateListener(Lcom/android/server/am/AppStateManager$IProcessStateCallback;)V

    .line 1102
    :cond_0
    return-void
.end method

.method public unregisterMultiTaskActionListener(ILcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;)Z
    .locals 1
    .param p1, "listenerFlag"    # I
    .param p2, "listener"    # Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;

    .line 1238
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 1239
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartWindowPolicyManager:Lcom/miui/server/smartpower/SmartWindowPolicyManager;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->unregisterMultiTaskActionListener(ILcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;)Z

    move-result v0

    return v0

    .line 1242
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public updateActivitiesVisibility()V
    .locals 1

    .line 547
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 548
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mComponentsManager:Lcom/miui/server/smartpower/ComponentsManager;

    invoke-virtual {v0}, Lcom/miui/server/smartpower/ComponentsManager;->updateActivitiesVisibilityLocked()V

    .line 550
    :cond_0
    return-void
.end method

.method public updateAllAppUsageStats()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/smartpower/IAppState;",
            ">;"
        }
    .end annotation

    .line 935
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 936
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0}, Lcom/android/server/am/AppStateManager;->updateAllAppUsageStats()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0

    .line 938
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public updateCpuStatsNow(Z)J
    .locals 2
    .param p1, "isReset"    # Z

    .line 1070
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 1071
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mSmartCpuPolicyManager:Lcom/miui/server/smartpower/SmartCpuPolicyManager;

    invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->updateCpuStatsNow(Z)J

    move-result-wide v0

    return-wide v0

    .line 1073
    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public updateProcess(Lcom/android/server/am/ProcessRecord;)V
    .locals 1
    .param p1, "r"    # Lcom/android/server/am/ProcessRecord;

    .line 356
    iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 357
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService;->mAppStateManager:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager;->updateProcessLocked(Lcom/android/server/am/ProcessRecord;)V

    .line 359
    :cond_0
    return-void
.end method
