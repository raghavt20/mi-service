class com.android.server.am.MemoryStandardProcessControl$RestartProcessInfo {
	 /* .source "MemoryStandardProcessControl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/MemoryStandardProcessControl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "RestartProcessInfo" */
} // .end annotation
/* # instance fields */
public java.lang.String mPackageName;
public java.lang.String mProcessName;
public Integer mUserId;
final com.android.server.am.MemoryStandardProcessControl this$0; //synthetic
/* # direct methods */
public com.android.server.am.MemoryStandardProcessControl$RestartProcessInfo ( ) {
/* .locals 0 */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "processName" # Ljava/lang/String; */
/* .param p4, "userId" # I */
/* .line 1273 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 1274 */
this.mPackageName = p2;
/* .line 1275 */
this.mProcessName = p3;
/* .line 1276 */
/* iput p4, p0, Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo;->mUserId:I */
/* .line 1277 */
return;
} // .end method
