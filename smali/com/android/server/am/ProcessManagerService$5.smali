.class Lcom/android/server/am/ProcessManagerService$5;
.super Ljava/lang/Object;
.source "ProcessManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/ProcessManagerService;->notifyActivityChanged(Landroid/content/ComponentName;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ProcessManagerService;

.field final synthetic val$curActivityComponent:Landroid/content/ComponentName;


# direct methods
.method constructor <init>(Lcom/android/server/am/ProcessManagerService;Landroid/content/ComponentName;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/ProcessManagerService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1035
    iput-object p1, p0, Lcom/android/server/am/ProcessManagerService$5;->this$0:Lcom/android/server/am/ProcessManagerService;

    iput-object p2, p0, Lcom/android/server/am/ProcessManagerService$5;->val$curActivityComponent:Landroid/content/ComponentName;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .line 1038
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$5;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-static {v0}, Lcom/android/server/am/ProcessManagerService;->-$$Nest$fgetmForegroundInfoManager(Lcom/android/server/am/ProcessManagerService;)Lcom/android/server/wm/ForegroundInfoManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService$5;->val$curActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Lcom/android/server/wm/ForegroundInfoManager;->notifyActivityChanged(Landroid/content/ComponentName;)V

    .line 1039
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$5;->val$curActivityComponent:Landroid/content/ComponentName;

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    const-string v1, "notifyActivityChanged"

    invoke-static {v1, v0}, Lcom/android/server/camera/CameraOpt;->callMethod(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 1040
    return-void
.end method
