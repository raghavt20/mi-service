.class public Lcom/android/server/am/GameProcessKiller;
.super Ljava/lang/Object;
.source "GameProcessKiller.java"

# interfaces
.implements Lcom/android/server/am/IGameProcessAction;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;,
        Lcom/android/server/am/GameProcessKiller$PackageMemInfo;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = true

.field private static final KILL_REASON:Ljava/lang/String; = "game memory reclaim"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mConfig:Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;

.field private mReclaimer:Lcom/android/server/am/GameMemoryReclaimer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 31
    const-class v0, Lcom/android/server/am/GameProcessKiller;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/am/GameProcessKiller;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/android/server/am/GameMemoryReclaimer;Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;)V
    .locals 0
    .param p1, "reclaimer"    # Lcom/android/server/am/GameMemoryReclaimer;
    .param p2, "cfg"    # Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/android/server/am/GameProcessKiller;->mReclaimer:Lcom/android/server/am/GameMemoryReclaimer;

    .line 40
    iput-object p2, p0, Lcom/android/server/am/GameProcessKiller;->mConfig:Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;

    .line 41
    return-void
.end method

.method private isUidSystem(I)Z
    .locals 1
    .param p1, "uid"    # I

    .line 45
    const v0, 0x186a0

    rem-int/2addr p1, v0

    .line 47
    const/16 v0, 0x2710

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public doAction(J)J
    .locals 16
    .param p1, "need"    # J

    .line 99
    move-object/from16 v1, p0

    const-wide/16 v2, 0x0

    .line 100
    .local v2, "reclaim":J
    const/4 v0, 0x0

    .line 105
    .local v0, "num":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "doKill:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/android/server/am/GameProcessKiller;->mConfig:Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;

    iget v5, v5, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mPrio:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-wide/32 v5, 0x80000

    invoke-static {v5, v6, v4}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 106
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v7

    .line 107
    .local v7, "time":J
    iget-object v4, v1, Lcom/android/server/am/GameProcessKiller;->mReclaimer:Lcom/android/server/am/GameMemoryReclaimer;

    invoke-virtual {v4, v1}, Lcom/android/server/am/GameMemoryReclaimer;->filterPackageInfos(Lcom/android/server/am/IGameProcessAction;)Ljava/util/List;

    move-result-object v4

    .line 108
    .local v4, "packageList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/GameProcessKiller$PackageMemInfo;>;"
    const-class v9, Landroid/app/ActivityManagerInternal;

    .line 109
    invoke-static {v9}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/app/ActivityManagerInternal;

    .line 110
    .local v9, "amInternal":Landroid/app/ActivityManagerInternal;
    if-eqz v4, :cond_2

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v10

    if-lez v10, :cond_2

    .line 111
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move-wide v11, v2

    move v2, v0

    .end local v0    # "num":I
    .local v2, "num":I
    .local v11, "reclaim":J
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/android/server/am/GameProcessKiller$PackageMemInfo;

    .line 112
    .local v3, "pkg":Lcom/android/server/am/GameProcessKiller$PackageMemInfo;
    iget-object v0, v1, Lcom/android/server/am/GameProcessKiller;->mReclaimer:Lcom/android/server/am/GameMemoryReclaimer;

    iget v13, v3, Lcom/android/server/am/GameProcessKiller$PackageMemInfo;->mUid:I

    invoke-virtual {v0, v13}, Lcom/android/server/am/GameMemoryReclaimer;->getPackageNameByUid(I)Ljava/lang/String;

    move-result-object v13

    .line 113
    .local v13, "name":Ljava/lang/String;
    iget-object v0, v1, Lcom/android/server/am/GameProcessKiller;->mReclaimer:Lcom/android/server/am/GameMemoryReclaimer;

    iget-object v14, v0, Lcom/android/server/am/GameMemoryReclaimer;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v14

    .line 114
    nop

    .line 115
    :try_start_0
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v0

    const-string v15, "game memory reclaim"

    .line 114
    invoke-virtual {v9, v13, v0, v15}, Landroid/app/ActivityManagerInternal;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V

    .line 116
    monitor-exit v14
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 117
    iget-wide v14, v3, Lcom/android/server/am/GameProcessKiller$PackageMemInfo;->mMemSize:J

    add-long/2addr v11, v14

    .line 118
    add-int/lit8 v2, v2, 0x1

    .line 119
    cmp-long v0, v11, p1

    if-ltz v0, :cond_0

    .line 120
    move v0, v2

    move-wide v2, v11

    goto :goto_1

    .line 121
    .end local v3    # "pkg":Lcom/android/server/am/GameProcessKiller$PackageMemInfo;
    :cond_0
    goto :goto_0

    .line 116
    .restart local v3    # "pkg":Lcom/android/server/am/GameProcessKiller$PackageMemInfo;
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v14
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 111
    .end local v3    # "pkg":Lcom/android/server/am/GameProcessKiller$PackageMemInfo;
    .end local v13    # "name":Ljava/lang/String;
    :cond_1
    move v0, v2

    move-wide v2, v11

    .line 123
    .end local v11    # "reclaim":J
    .restart local v0    # "num":I
    .local v2, "reclaim":J
    :cond_2
    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    sub-long/2addr v10, v7

    .line 124
    .end local v7    # "time":J
    .local v10, "time":J
    sget-object v7, Lcom/android/server/am/GameProcessKiller;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "kill "

    invoke-virtual {v8, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v12, " packages, and reclaim mem: "

    invoke-virtual {v8, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v12, ", during "

    invoke-virtual {v8, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v12, "ms"

    invoke-virtual {v8, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    invoke-static {v5, v6}, Landroid/os/Trace;->traceEnd(J)V

    .line 126
    return-wide v2
.end method

.method getMinProcState()I
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/android/server/am/GameProcessKiller;->mConfig:Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;

    iget v0, v0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mMinProcState:I

    return v0
.end method

.method getPrio()I
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/android/server/am/GameProcessKiller;->mConfig:Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;

    iget v0, v0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mPrio:I

    return v0
.end method

.method shouldSkip(I)Z
    .locals 3
    .param p1, "uid"    # I

    .line 59
    invoke-direct {p0, p1}, Lcom/android/server/am/GameProcessKiller;->isUidSystem(I)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 60
    return v1

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/GameProcessKiller;->mConfig:Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;

    iget-boolean v0, v0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mSkipActive:Z

    if-eqz v0, :cond_1

    .line 63
    invoke-static {}, Lcom/miui/server/process/ProcessManagerInternal;->getInstance()Lcom/miui/server/process/ProcessManagerInternal;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/process/ProcessManagerInternal;->getProcessPolicy()Lcom/android/server/am/IProcessPolicy;

    move-result-object v0

    .line 64
    const/4 v2, 0x3

    invoke-interface {v0, v2}, Lcom/android/server/am/IProcessPolicy;->getActiveUidList(I)Ljava/util/List;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65
    return v1

    .line 69
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/miui/server/process/ProcessManagerInternal;->getInstance()Lcom/miui/server/process/ProcessManagerInternal;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/process/ProcessManagerInternal;->getForegroundInfo()Lmiui/process/ForegroundInfo;

    move-result-object v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    .local v0, "foregroundInfo":Lmiui/process/ForegroundInfo;
    goto :goto_0

    .line 70
    .end local v0    # "foregroundInfo":Lmiui/process/ForegroundInfo;
    :catch_0
    move-exception v0

    .line 71
    .local v0, "ex":Landroid/os/RemoteException;
    const/4 v2, 0x0

    move-object v0, v2

    .line 73
    .local v0, "foregroundInfo":Lmiui/process/ForegroundInfo;
    :goto_0
    if-nez v0, :cond_2

    .line 74
    return v1

    .line 76
    :cond_2
    iget v2, v0, Lmiui/process/ForegroundInfo;->mForegroundUid:I

    if-eq p1, v2, :cond_4

    iget v2, v0, Lmiui/process/ForegroundInfo;->mMultiWindowForegroundUid:I

    if-ne p1, v2, :cond_3

    goto :goto_1

    .line 79
    :cond_3
    const/4 v1, 0x0

    return v1

    .line 78
    :cond_4
    :goto_1
    return v1
.end method

.method public shouldSkip(Lcom/android/server/am/ProcessRecord;)Z
    .locals 4
    .param p1, "proc"    # Lcom/android/server/am/ProcessRecord;

    .line 84
    iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    invoke-virtual {p0, v0}, Lcom/android/server/am/GameProcessKiller;->shouldSkip(I)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 85
    return v1

    .line 86
    :cond_0
    invoke-static {}, Lcom/miui/server/process/ProcessManagerInternal;->getInstance()Lcom/miui/server/process/ProcessManagerInternal;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/process/ProcessManagerInternal;->getProcessPolicy()Lcom/android/server/am/IProcessPolicy;

    move-result-object v0

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 87
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v3

    invoke-interface {v0, v2, v3}, Lcom/android/server/am/IProcessPolicy;->isLockedApplication(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 88
    return v1

    .line 89
    :cond_1
    iget-object v0, p0, Lcom/android/server/am/GameProcessKiller;->mConfig:Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;

    iget-object v0, v0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mWhiteList:Ljava/util/List;

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/android/server/am/GameProcessKiller;->mConfig:Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;

    iget-object v0, v0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mWhiteList:Ljava/util/List;

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 91
    :cond_2
    iget-object v0, p0, Lcom/android/server/am/GameProcessKiller;->mConfig:Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;

    iget-boolean v0, v0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mSkipForeground:Z

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getWindowProcessController()Lcom/android/server/wm/WindowProcessController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/WindowProcessController;->isInterestingToUser()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 92
    return v1

    .line 94
    :cond_3
    const/4 v0, 0x0

    return v0

    .line 90
    :cond_4
    :goto_0
    return v1
.end method
