class com.android.server.am.MemoryControlCloud$5 extends android.database.ContentObserver {
	 /* .source "MemoryControlCloud.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/am/MemoryControlCloud;->registerCloudProcListObserver(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.am.MemoryControlCloud this$0; //synthetic
final android.content.Context val$context; //synthetic
/* # direct methods */
 com.android.server.am.MemoryControlCloud$5 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/MemoryControlCloud; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 188 */
this.this$0 = p1;
this.val$context = p3;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 3 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 191 */
if ( p2 != null) { // if-eqz p2, :cond_2
	 final String v0 = "cloud_memory_standard_proc_list_json"; // const-string v0, "cloud_memory_standard_proc_list_json"
	 android.provider.Settings$System .getUriFor ( v0 );
	 v1 = 	 (( android.net.Uri ) p2 ).equals ( v1 ); // invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_2
		 /* .line 192 */
		 v1 = this.val$context;
		 (( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
		 int v2 = -2; // const/4 v2, -0x2
		 android.provider.Settings$System .getStringForUser ( v1,v0,v2 );
		 /* .line 194 */
		 /* .local v0, "str":Ljava/lang/String; */
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 final String v1 = ""; // const-string v1, ""
			 v1 = 			 (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 if ( v1 != null) { // if-eqz v1, :cond_0
				 /* .line 197 */
			 } // :cond_0
			 v1 = this.this$0;
			 com.android.server.am.MemoryControlCloud .-$$Nest$fgetmspc ( v1 );
			 (( com.android.server.am.MemoryStandardProcessControl ) v1 ).callUpdateCloudConfig ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/am/MemoryStandardProcessControl;->callUpdateCloudConfig(Ljava/lang/String;)V
			 /* .line 195 */
		 } // :cond_1
	 } // :goto_0
	 return;
	 /* .line 199 */
} // .end local v0 # "str":Ljava/lang/String;
} // :cond_2
} // :goto_1
return;
} // .end method
