class com.android.server.am.ScoutMemoryError$1 implements com.android.server.am.MiuiWarnings$WarningCallback {
	 /* .source "ScoutMemoryError.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/am/ScoutMemoryError;->showMemoryErrorDialog(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.am.ScoutMemoryError this$0; //synthetic
final com.android.server.am.ProcessRecord val$app; //synthetic
final java.lang.String val$reason; //synthetic
/* # direct methods */
 com.android.server.am.ScoutMemoryError$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/ScoutMemoryError; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 69 */
this.this$0 = p1;
this.val$app = p2;
this.val$reason = p3;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onCallback ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "positive" # Z */
/* .line 72 */
final String v0 = "ScoutMemoryError"; // const-string v0, "ScoutMemoryError"
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 73 */
/* const-string/jumbo v1, "showMemoryErrorDialog ok" */
android.util.Slog .d ( v0,v1 );
/* .line 74 */
v0 = this.this$0;
v1 = this.val$app;
v2 = this.val$reason;
(( com.android.server.am.ScoutMemoryError ) v0 ).scheduleCrashApp ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/am/ScoutMemoryError;->scheduleCrashApp(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)Z
/* .line 76 */
} // :cond_0
/* const-string/jumbo v1, "showMemoryErrorDialog cancel" */
android.util.Slog .d ( v0,v1 );
/* .line 78 */
} // :goto_0
return;
} // .end method
