.class Lcom/android/server/am/GameMemoryReclaimer$1;
.super Ljava/lang/Object;
.source "GameMemoryReclaimer.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/GameMemoryReclaimer;->filterAllProcessInfos()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/android/server/am/ProcessRecord;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/GameMemoryReclaimer;


# direct methods
.method constructor <init>(Lcom/android/server/am/GameMemoryReclaimer;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/GameMemoryReclaimer;

    .line 111
    iput-object p1, p0, Lcom/android/server/am/GameMemoryReclaimer$1;->this$0:Lcom/android/server/am/GameMemoryReclaimer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compareTo(Lcom/android/server/am/ProcessRecord;)I
    .locals 8
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 114
    const/4 v0, 0x1

    .line 116
    .local v0, "skip":Z
    iget-object v1, p0, Lcom/android/server/am/GameMemoryReclaimer$1;->this$0:Lcom/android/server/am/GameMemoryReclaimer;

    invoke-static {v1}, Lcom/android/server/am/GameMemoryReclaimer;->-$$Nest$fgetmProcessActions(Lcom/android/server/am/GameMemoryReclaimer;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/IGameProcessAction;

    .line 117
    .local v2, "action":Lcom/android/server/am/IGameProcessAction;
    instance-of v3, v2, Lcom/android/server/am/GameProcessCompactor;

    if-nez v3, :cond_0

    .line 118
    goto :goto_0

    .line 119
    :cond_0
    invoke-interface {v2, p1}, Lcom/android/server/am/IGameProcessAction;->shouldSkip(Lcom/android/server/am/ProcessRecord;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/android/server/am/GameMemoryReclaimer$1;->this$0:Lcom/android/server/am/GameMemoryReclaimer;

    invoke-static {v3}, Lcom/android/server/am/GameMemoryReclaimer;->-$$Nest$fgetmAllProcessInfos(Lcom/android/server/am/GameMemoryReclaimer;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 120
    iget-object v3, p0, Lcom/android/server/am/GameMemoryReclaimer$1;->this$0:Lcom/android/server/am/GameMemoryReclaimer;

    iget-object v3, v3, Lcom/android/server/am/GameMemoryReclaimer;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v3

    .line 121
    :try_start_0
    iget-object v4, p0, Lcom/android/server/am/GameMemoryReclaimer$1;->this$0:Lcom/android/server/am/GameMemoryReclaimer;

    invoke-static {v4}, Lcom/android/server/am/GameMemoryReclaimer;->-$$Nest$fgetmCompactInfos(Lcom/android/server/am/GameMemoryReclaimer;)Ljava/util/Map;

    move-result-object v4

    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 122
    iget-object v4, p0, Lcom/android/server/am/GameMemoryReclaimer$1;->this$0:Lcom/android/server/am/GameMemoryReclaimer;

    invoke-static {v4}, Lcom/android/server/am/GameMemoryReclaimer;->-$$Nest$fgetmCompactInfos(Lcom/android/server/am/GameMemoryReclaimer;)Ljava/util/Map;

    move-result-object v4

    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    new-instance v6, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;

    .line 123
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I

    move-result v7

    invoke-direct {v6, v7}, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;-><init>(I)V

    .line 122
    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    :cond_1
    iget-object v4, p0, Lcom/android/server/am/GameMemoryReclaimer$1;->this$0:Lcom/android/server/am/GameMemoryReclaimer;

    invoke-static {v4}, Lcom/android/server/am/GameMemoryReclaimer;->-$$Nest$fgetmCompactInfos(Lcom/android/server/am/GameMemoryReclaimer;)Ljava/util/Map;

    move-result-object v4

    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;

    .line 126
    .local v4, "info":Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;
    iget-object v5, p0, Lcom/android/server/am/GameMemoryReclaimer$1;->this$0:Lcom/android/server/am/GameMemoryReclaimer;

    invoke-static {v5}, Lcom/android/server/am/GameMemoryReclaimer;->-$$Nest$fgetmAllProcessInfos(Lcom/android/server/am/GameMemoryReclaimer;)Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    monitor-exit v3

    .line 128
    const/4 v0, 0x0

    goto :goto_1

    .line 127
    .end local v4    # "info":Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;
    :catchall_0
    move-exception v1

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 130
    .end local v2    # "action":Lcom/android/server/am/IGameProcessAction;
    :cond_2
    :goto_1
    goto :goto_0

    .line 131
    :cond_3
    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 111
    check-cast p1, Lcom/android/server/am/ProcessRecord;

    invoke-virtual {p0, p1}, Lcom/android/server/am/GameMemoryReclaimer$1;->compareTo(Lcom/android/server/am/ProcessRecord;)I

    move-result p1

    return p1
.end method
