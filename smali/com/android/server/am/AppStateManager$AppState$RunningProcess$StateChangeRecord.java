class com.android.server.am.AppStateManager$AppState$RunningProcess$StateChangeRecord {
	 /* .source "AppStateManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "StateChangeRecord" */
} // .end annotation
/* # instance fields */
private Integer mAdj;
private java.lang.String mChangeReason;
private Long mInterval;
private Integer mNewState;
private Integer mOldState;
private Long mTimeStamp;
final com.android.server.am.AppStateManager$AppState$RunningProcess this$2; //synthetic
/* # direct methods */
static Long -$$Nest$fgetmInterval ( com.android.server.am.AppStateManager$AppState$RunningProcess$StateChangeRecord p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->mInterval:J */
/* return-wide v0 */
} // .end method
static Integer -$$Nest$fgetmNewState ( com.android.server.am.AppStateManager$AppState$RunningProcess$StateChangeRecord p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->mNewState:I */
} // .end method
static Integer -$$Nest$fgetmOldState ( com.android.server.am.AppStateManager$AppState$RunningProcess$StateChangeRecord p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->mOldState:I */
} // .end method
static Long -$$Nest$fgetmTimeStamp ( com.android.server.am.AppStateManager$AppState$RunningProcess$StateChangeRecord p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->mTimeStamp:J */
/* return-wide v0 */
} // .end method
static Long -$$Nest$mgetTimeStamp ( com.android.server.am.AppStateManager$AppState$RunningProcess$StateChangeRecord p0 ) { //bridge//synthethic
/* .locals 2 */
/* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->getTimeStamp()J */
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
private com.android.server.am.AppStateManager$AppState$RunningProcess$StateChangeRecord ( ) {
/* .locals 2 */
/* .param p2, "oldState" # I */
/* .param p3, "newState" # I */
/* .param p4, "intervalMills" # J */
/* .param p6, "reson" # Ljava/lang/String; */
/* .param p7, "adj" # I */
/* .line 2923 */
this.this$2 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 2924 */
/* iput p2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->mOldState:I */
/* .line 2925 */
/* iput p3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->mNewState:I */
/* .line 2926 */
/* iput-wide p4, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->mInterval:J */
/* .line 2927 */
this.mChangeReason = p6;
/* .line 2928 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->mTimeStamp:J */
/* .line 2929 */
/* iput p7, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->mAdj:I */
/* .line 2930 */
return;
} // .end method
 com.android.server.am.AppStateManager$AppState$RunningProcess$StateChangeRecord ( ) { //synthethic
/* .locals 0 */
/* invoke-direct/range {p0 ..p7}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;-><init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;IIJLjava/lang/String;I)V */
return;
} // .end method
private Long getTimeStamp ( ) {
/* .locals 2 */
/* .line 2933 */
/* iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->mTimeStamp:J */
/* return-wide v0 */
} // .end method
/* # virtual methods */
public java.lang.String toString ( ) {
/* .locals 5 */
/* .line 2938 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->mOldState:I */
/* .line 2939 */
com.android.server.am.AppStateManager .processStateToString ( v0 );
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->mNewState:I */
com.android.server.am.AppStateManager .processStateToString ( v1 );
/* iget-wide v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->mInterval:J */
/* .line 2940 */
java.lang.Long .valueOf ( v2,v3 );
v3 = this.mChangeReason;
/* iget v4, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->mAdj:I */
java.lang.Integer .valueOf ( v4 );
/* filled-new-array {v0, v1, v2, v3, v4}, [Ljava/lang/Object; */
/* .line 2938 */
final String v1 = "%s->%s(%dms) R(%s) adj=%s."; // const-string v1, "%s->%s(%dms) R(%s) adj=%s."
java.lang.String .format ( v1,v0 );
} // .end method
