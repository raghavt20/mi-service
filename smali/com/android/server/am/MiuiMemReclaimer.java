public class com.android.server.am.MiuiMemReclaimer {
	 /* .source "MiuiMemReclaimer.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/am/MiuiMemReclaimer$CompactorHandler;, */
	 /* Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Long ANON_RSS_LIMIT_KB;
private static final java.lang.String COMPACTION_PROC_NAME_CAMERA;
private static final java.lang.String COMPACTION_PROC_NAME_WALLPAPER;
private static final java.lang.String COMPACTION_PROC_NAME_WECHAT;
private static final java.util.ArrayList COMPACTION_PROC_NAME_WHITE_LIST;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.String COMPACT_ACTION_ANON;
private static final java.lang.String COMPACT_ACTION_FILE;
private static final java.lang.String COMPACT_ACTION_FULL;
private static final Long COMPACT_ALL_MIN_INTERVAL;
private static final Integer COMPACT_GLOBALLY_MSG;
private static final Integer COMPACT_PROCESSES_MSG;
private static final Integer COMPACT_PROCESS_MSG;
private static final Long COMPACT_PROC_MIN_INTERVAL;
private static final Integer EVENT_TAG;
private static final Long FILE_RSS_LIMIT_KB;
private static final java.lang.String RECLAIM_EVENT_NODE;
private static final Boolean RECLAIM_IF_NEEDED;
private static final java.lang.String TAG;
private static final Long TOTAL_MEMORY;
private static final Boolean USE_LEGACY_COMPACTION;
private static final Integer VMPRESS_CRITICAL_RECLAIM_SIZE_MB;
private static final Integer VMPRESS_LOW_RECLAIM_SIZE_MB;
private static final Integer VMPRESS_MEDIUM_RECLAIM_SIZE_MB;
private static final Integer VM_ANON;
private static final Integer VM_FILE;
private static final Integer VM_RSS;
private static final Integer VM_SWAP;
private static volatile com.android.server.am.ProcessManagerService sPms;
/* # instance fields */
private Boolean DEBUG;
private final Integer MAX_COMPACTION_COUNT;
private Boolean mBatteryLow;
private final android.content.BroadcastReceiver mBatteryReceiver;
private final java.util.Map mCompactionStats;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.os.Handler mCompactorHandler;
private final android.os.HandlerThread mCompactorThread;
private android.content.Context mContext;
private android.content.IntentFilter mIntentFilter;
private Boolean mInterruptNeeded;
private Long mLastCompactionTimeMillis;
private com.android.server.am.CachedAppOptimizer$DefaultProcessDependencies mProcessDependencies;
private com.android.server.am.ProcessPolicy mProcessPolicy;
private com.miui.app.smartpower.SmartPowerServiceInternal mSmartPowerService;
/* # direct methods */
static Integer -$$Nest$fgetMAX_COMPACTION_COUNT ( com.android.server.am.MiuiMemReclaimer p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/MiuiMemReclaimer;->MAX_COMPACTION_COUNT:I */
} // .end method
static void -$$Nest$fputmBatteryLow ( com.android.server.am.MiuiMemReclaimer p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/am/MiuiMemReclaimer;->mBatteryLow:Z */
return;
} // .end method
static void -$$Nest$mperformCompactProcess ( com.android.server.am.MiuiMemReclaimer p0, com.miui.server.smartpower.IAppState$IRunningProcess p1, Integer p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/am/MiuiMemReclaimer;->performCompactProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)V */
return;
} // .end method
static void -$$Nest$mperformCompactProcesses ( com.android.server.am.MiuiMemReclaimer p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/MiuiMemReclaimer;->performCompactProcesses(I)V */
return;
} // .end method
static com.android.server.am.MiuiMemReclaimer ( ) {
/* .locals 4 */
/* .line 55 */
android.os.Process .getTotalMemory ( );
/* move-result-wide v0 */
/* const/16 v2, 0x1e */
/* shr-long/2addr v0, v2 */
/* sput-wide v0, Lcom/android/server/am/MiuiMemReclaimer;->TOTAL_MEMORY:J */
/* .line 59 */
/* const-wide/16 v2, 0x8 */
/* cmp-long v0, v0, v2 */
int v1 = 0; // const/4 v1, 0x0
/* if-gez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
/* move v0, v1 */
} // :goto_0
com.android.server.am.MiuiMemReclaimer.RECLAIM_IF_NEEDED = (v0!= 0);
/* .line 63 */
/* nop */
/* .line 64 */
final String v0 = "persist.sys.mms.use_legacy"; // const-string v0, "persist.sys.mms.use_legacy"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.am.MiuiMemReclaimer.USE_LEGACY_COMPACTION = (v0!= 0);
/* .line 66 */
/* nop */
/* .line 67 */
final String v0 = "persist.sys.mms.anon_rss"; // const-string v0, "persist.sys.mms.anon_rss"
/* const-wide/16 v1, 0x1388 */
android.os.SystemProperties .getLong ( v0,v1,v2 );
/* move-result-wide v0 */
/* sput-wide v0, Lcom/android/server/am/MiuiMemReclaimer;->ANON_RSS_LIMIT_KB:J */
/* .line 68 */
/* nop */
/* .line 69 */
final String v0 = "persist.sys.mms.file_rss"; // const-string v0, "persist.sys.mms.file_rss"
/* const-wide/16 v1, 0x4e20 */
android.os.SystemProperties .getLong ( v0,v1,v2 );
/* move-result-wide v0 */
/* sput-wide v0, Lcom/android/server/am/MiuiMemReclaimer;->FILE_RSS_LIMIT_KB:J */
/* .line 71 */
final String v0 = "persist.sys.mms.compact_min_interval"; // const-string v0, "persist.sys.mms.compact_min_interval"
/* const-wide/32 v1, 0xea60 */
android.os.SystemProperties .getLong ( v0,v1,v2 );
/* move-result-wide v0 */
/* sput-wide v0, Lcom/android/server/am/MiuiMemReclaimer;->COMPACT_ALL_MIN_INTERVAL:J */
/* .line 73 */
final String v0 = "persist.sys.mms.compact_proc_min_interval"; // const-string v0, "persist.sys.mms.compact_proc_min_interval"
/* const-wide/16 v1, 0x2710 */
android.os.SystemProperties .getLong ( v0,v1,v2 );
/* move-result-wide v0 */
/* sput-wide v0, Lcom/android/server/am/MiuiMemReclaimer;->COMPACT_PROC_MIN_INTERVAL:J */
/* .line 79 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 81 */
final String v1 = "com.miui.home"; // const-string v1, "com.miui.home"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 82 */
final String v1 = "com.android.systemui"; // const-string v1, "com.android.systemui"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 83 */
final String v1 = "com.miui.screenrecorder"; // const-string v1, "com.miui.screenrecorder"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 84 */
return;
} // .end method
public com.android.server.am.MiuiMemReclaimer ( ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 164 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 56 */
/* sget-boolean v0, Lcom/android/server/am/MiuiMemoryService;->DEBUG:Z */
/* iput-boolean v0, p0, Lcom/android/server/am/MiuiMemReclaimer;->DEBUG:Z */
/* .line 108 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/am/MiuiMemReclaimer;->mInterruptNeeded:Z */
/* .line 112 */
/* new-instance v1, Lcom/android/server/am/CachedAppOptimizer$DefaultProcessDependencies; */
/* invoke-direct {v1}, Lcom/android/server/am/CachedAppOptimizer$DefaultProcessDependencies;-><init>()V */
this.mProcessDependencies = v1;
/* .line 119 */
/* iget-boolean v1, p0, Lcom/android/server/am/MiuiMemReclaimer;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* const/16 v1, 0x3e8 */
} // :cond_0
/* const/16 v1, 0x12c */
} // :goto_0
/* iput v1, p0, Lcom/android/server/am/MiuiMemReclaimer;->MAX_COMPACTION_COUNT:I */
/* .line 121 */
/* new-instance v1, Lcom/android/server/am/MiuiMemReclaimer$1; */
/* invoke-direct {v1, p0}, Lcom/android/server/am/MiuiMemReclaimer$1;-><init>(Lcom/android/server/am/MiuiMemReclaimer;)V */
this.mCompactionStats = v1;
/* .line 155 */
/* new-instance v1, Lcom/android/server/am/MiuiMemReclaimer$2; */
/* invoke-direct {v1, p0}, Lcom/android/server/am/MiuiMemReclaimer$2;-><init>(Lcom/android/server/am/MiuiMemReclaimer;)V */
this.mBatteryReceiver = v1;
/* .line 165 */
/* new-instance v2, Landroid/os/HandlerThread; */
final String v3 = "MiuiMemoryService_Compactor"; // const-string v3, "MiuiMemoryService_Compactor"
/* invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mCompactorThread = v2;
/* .line 166 */
(( android.os.HandlerThread ) v2 ).start ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V
/* .line 167 */
/* new-instance v3, Lcom/android/server/am/MiuiMemReclaimer$CompactorHandler; */
(( android.os.HandlerThread ) v2 ).getLooper ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v3, p0, v4}, Lcom/android/server/am/MiuiMemReclaimer$CompactorHandler;-><init>(Lcom/android/server/am/MiuiMemReclaimer;Landroid/os/Looper;)V */
this.mCompactorHandler = v3;
/* .line 168 */
/* nop */
/* .line 169 */
v2 = (( android.os.HandlerThread ) v2 ).getThreadId ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->getThreadId()I
/* .line 168 */
int v3 = 2; // const/4 v3, 0x2
android.os.Process .setThreadGroupAndCpuset ( v2,v3 );
/* .line 170 */
/* const-class v2, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
com.android.server.LocalServices .getService ( v2 );
/* check-cast v2, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
this.mSmartPowerService = v2;
/* .line 171 */
this.mContext = p1;
/* .line 172 */
/* iput-boolean v0, p0, Lcom/android/server/am/MiuiMemReclaimer;->mBatteryLow:Z */
/* .line 173 */
/* new-instance v0, Landroid/content/IntentFilter; */
final String v2 = "android.intent.action.BATTERY_CHANGED"; // const-string v2, "android.intent.action.BATTERY_CHANGED"
/* invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
this.mIntentFilter = v0;
/* .line 174 */
v2 = this.mContext;
(( android.content.Context ) v2 ).registerReceiver ( v1, v0 ); // invoke-virtual {v2, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 175 */
return;
} // .end method
public static void cancelSt ( ) {
/* .locals 2 */
/* .line 193 */
/* sget-boolean v0, Lcom/android/server/am/MiuiMemReclaimer;->RECLAIM_IF_NEEDED:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 194 */
final String v0 = "/sys/kernel/mi_reclaim/event"; // const-string v0, "/sys/kernel/mi_reclaim/event"
int v1 = 0; // const/4 v1, 0x0
com.android.server.am.MiuiMemReclaimer .writeToNode ( v0,v1 );
/* .line 195 */
} // :cond_0
return;
} // .end method
public static void enterSt ( ) {
/* .locals 2 */
/* .line 188 */
/* sget-boolean v0, Lcom/android/server/am/MiuiMemReclaimer;->RECLAIM_IF_NEEDED:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 189 */
final String v0 = "/sys/kernel/mi_reclaim/event"; // const-string v0, "/sys/kernel/mi_reclaim/event"
int v1 = 2; // const/4 v1, 0x2
com.android.server.am.MiuiMemReclaimer .writeToNode ( v0,v1 );
/* .line 190 */
} // :cond_0
return;
} // .end method
private java.util.List filterCompactionProcs ( Integer p0 ) {
/* .locals 8 */
/* .param p1, "mode" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 406 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 407 */
/* .local v0, "targetProcs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;>;" */
v1 = this.mSmartPowerService;
/* .line 408 */
/* .local v1, "appStateList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState;>;" */
(( java.util.ArrayList ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_3
/* check-cast v3, Lcom/miui/server/smartpower/IAppState; */
/* .line 409 */
/* .local v3, "appState":Lcom/miui/server/smartpower/IAppState; */
v4 = (( com.miui.server.smartpower.IAppState ) v3 ).isVsible ( ); // invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState;->isVsible()Z
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 410 */
/* .line 412 */
} // :cond_0
(( com.miui.server.smartpower.IAppState ) v3 ).getRunningProcessList ( ); // invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState;->getRunningProcessList()Ljava/util/ArrayList;
(( java.util.ArrayList ) v4 ).iterator ( ); // invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v5 = } // :goto_1
if ( v5 != null) { // if-eqz v5, :cond_2
/* check-cast v5, Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 413 */
/* .local v5, "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
v6 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v5 ).getAdj ( ); // invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I
/* if-lez v6, :cond_1 */
/* .line 414 */
v6 = (( com.android.server.am.MiuiMemReclaimer ) p0 ).isCompactNeeded ( v5, p1 ); // invoke-virtual {p0, v5, p1}, Lcom/android/server/am/MiuiMemReclaimer;->isCompactNeeded(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)Z
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 415 */
/* new-instance v6, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo; */
/* invoke-direct {v6, v5, p1}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;-><init>(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)V */
/* .line 416 */
/* .local v6, "info":Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo; */
v7 = /* invoke-direct {p0, v6}, Lcom/android/server/am/MiuiMemReclaimer;->isCompactSatisfied(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)Z */
if ( v7 != null) { // if-eqz v7, :cond_1
/* .line 418 */
} // .end local v5 # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
} // .end local v6 # "info":Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;
} // :cond_1
/* .line 419 */
} // .end local v3 # "appState":Lcom/miui/server/smartpower/IAppState;
} // :cond_2
/* .line 420 */
v2 = } // :cond_3
/* if-nez v2, :cond_4 */
/* .line 421 */
final String v2 = "all"; // const-string v2, "all"
/* invoke-direct {p0, v0, v2}, Lcom/android/server/am/MiuiMemReclaimer;->sortProcsByRss(Ljava/util/List;Ljava/lang/String;)V */
/* .line 422 */
/* invoke-direct {p0, v0, v2}, Lcom/android/server/am/MiuiMemReclaimer;->logTargetProcsDetails(Ljava/util/List;Ljava/lang/String;)V */
/* .line 425 */
} // :cond_4
} // .end method
private android.os.Message generateMessage ( Integer p0, Integer p1, java.lang.Object p2 ) {
/* .locals 2 */
/* .param p1, "what" # I */
/* .param p2, "arg1" # I */
/* .param p3, "obj" # Ljava/lang/Object; */
/* .line 291 */
android.os.Message .obtain ( );
/* .line 292 */
/* .local v0, "msg":Landroid/os/Message; */
/* iput p1, v0, Landroid/os/Message;->what:I */
/* .line 293 */
int v1 = -1; // const/4 v1, -0x1
/* if-eq p2, v1, :cond_0 */
/* iput p2, v0, Landroid/os/Message;->arg1:I */
/* .line 294 */
} // :cond_0
if ( p3 != null) { // if-eqz p3, :cond_1
this.obj = p3;
/* .line 295 */
} // :cond_1
} // .end method
private java.util.Set getActiveUidSet ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 429 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 430 */
/* .local v0, "activeUidSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
v1 = this.mProcessPolicy;
/* if-nez v1, :cond_1 */
/* .line 431 */
com.android.server.am.MiuiMemReclaimer .getProcessManagerService ( );
/* if-nez v1, :cond_0 */
/* .line 432 */
/* .line 434 */
} // :cond_0
com.android.server.am.MiuiMemReclaimer .getProcessManagerService ( );
(( com.android.server.am.ProcessManagerService ) v1 ).getProcessPolicy ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessManagerService;->getProcessPolicy()Lcom/android/server/am/ProcessPolicy;
this.mProcessPolicy = v1;
/* .line 436 */
} // :cond_1
v1 = this.mProcessPolicy;
/* .line 437 */
int v2 = 3; // const/4 v2, 0x3
(( com.android.server.am.ProcessPolicy ) v1 ).getActiveUidRecordList ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessPolicy;->getActiveUidRecordList(I)Ljava/util/List;
/* .line 438 */
/* .local v1, "activeUidRecords":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;>;" */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_2
/* check-cast v3, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
/* .line 439 */
/* .local v3, "record":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
/* iget v4, v3, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->uid:I */
java.lang.Integer .valueOf ( v4 );
/* .line 440 */
} // .end local v3 # "record":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;
/* .line 441 */
} // :cond_2
} // .end method
private static com.android.server.am.ProcessManagerService getProcessManagerService ( ) {
/* .locals 1 */
/* .line 445 */
v0 = com.android.server.am.MiuiMemReclaimer.sPms;
/* if-nez v0, :cond_0 */
/* .line 446 */
final String v0 = "ProcessManager"; // const-string v0, "ProcessManager"
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lcom/android/server/am/ProcessManagerService; */
/* .line 448 */
} // :cond_0
v0 = com.android.server.am.MiuiMemReclaimer.sPms;
} // .end method
private Boolean isAdjInCompactRange ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "adj" # I */
/* .param p2, "mode" # I */
/* .line 535 */
/* const/16 v0, 0x64 */
int v1 = 0; // const/4 v1, 0x0
/* packed-switch p2, :pswitch_data_0 */
/* .line 559 */
/* :pswitch_0 */
/* if-ge p1, v0, :cond_3 */
/* .line 560 */
/* .line 553 */
/* :pswitch_1 */
/* if-lt p1, v0, :cond_0 */
/* const/16 v0, 0x3b6 */
/* if-le p1, v0, :cond_3 */
/* .line 555 */
} // :cond_0
/* .line 546 */
/* :pswitch_2 */
/* const/16 v0, 0xc8 */
/* if-le p1, v0, :cond_1 */
/* const/16 v0, 0x320 */
/* if-lt p1, v0, :cond_3 */
/* .line 547 */
} // :cond_1
/* .line 539 */
/* :pswitch_3 */
/* if-le p1, v0, :cond_2 */
/* const/16 v0, 0x384 */
/* if-lt p1, v0, :cond_3 */
/* .line 540 */
} // :cond_2
/* .line 564 */
} // :cond_3
int v0 = 1; // const/4 v0, 0x1
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_3 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
private Boolean isCameraMode ( ) {
/* .locals 2 */
/* .line 485 */
/* nop */
/* .line 486 */
com.android.server.am.SystemPressureController .getInstance ( );
(( com.android.server.am.SystemPressureController ) v0 ).getForegroundPackageName ( ); // invoke-virtual {v0}, Lcom/android/server/am/SystemPressureController;->getForegroundPackageName()Ljava/lang/String;
/* .line 485 */
final String v1 = "com.android.camera"; // const-string v1, "com.android.camera"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
} // .end method
private Boolean isCompactSatisfied ( com.android.server.am.MiuiMemReclaimer$CompactProcInfo p0 ) {
/* .locals 14 */
/* .param p1, "info" # Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo; */
/* .line 568 */
v0 = com.android.server.am.MiuiMemReclaimer$CompactProcInfo .-$$Nest$mgetPid ( p1 );
final String v1 = "Skipping compaction for process "; // const-string v1, "Skipping compaction for process "
final String v2 = "MiuiMemoryService"; // const-string v2, "MiuiMemoryService"
int v3 = 0; // const/4 v3, 0x0
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = com.android.server.am.MiuiMemReclaimer$CompactProcInfo .-$$Nest$misRssValid ( p1 );
/* if-nez v0, :cond_0 */
/* goto/16 :goto_3 */
/* .line 576 */
} // :cond_0
v0 = this.rss;
int v4 = 1; // const/4 v4, 0x1
/* aget-wide v5, v0, v4 */
/* .line 577 */
/* .local v5, "rssFile":J */
v0 = this.rss;
int v7 = 2; // const/4 v7, 0x2
/* aget-wide v8, v0, v7 */
/* .line 578 */
/* .local v8, "rssAnon":J */
v0 = this.action;
v10 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
/* sparse-switch v10, :sswitch_data_0 */
} // :cond_1
/* :sswitch_0 */
final String v7 = "file"; // const-string v7, "file"
v0 = (( java.lang.String ) v0 ).equals ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* move v7, v4 */
/* :sswitch_1 */
final String v7 = "anon"; // const-string v7, "anon"
v0 = (( java.lang.String ) v0 ).equals ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* move v7, v3 */
/* :sswitch_2 */
final String v10 = "all"; // const-string v10, "all"
v0 = (( java.lang.String ) v0 ).equals ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
} // :goto_0
int v7 = -1; // const/4 v7, -0x1
} // :goto_1
final String v0 = " KB"; // const-string v0, " KB"
/* const-wide/16 v10, 0x0 */
/* packed-switch v7, :pswitch_data_0 */
/* goto/16 :goto_2 */
/* .line 594 */
/* :pswitch_0 */
/* sget-wide v12, Lcom/android/server/am/MiuiMemReclaimer;->FILE_RSS_LIMIT_KB:J */
/* cmp-long v7, v12, v10 */
/* if-lez v7, :cond_2 */
/* cmp-long v7, v5, v12 */
/* if-gez v7, :cond_2 */
/* sget-wide v12, Lcom/android/server/am/MiuiMemReclaimer;->ANON_RSS_LIMIT_KB:J */
/* cmp-long v7, v12, v10 */
/* if-lez v7, :cond_2 */
/* cmp-long v7, v8, v12 */
/* if-gez v7, :cond_2 */
/* .line 596 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = com.android.server.am.MiuiMemReclaimer$CompactProcInfo .-$$Nest$mgetPid ( p1 );
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ", full RSS is too small "; // const-string v4, ", full RSS is too small "
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v5, v6 ); // invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v0 );
/* .line 598 */
/* .line 587 */
/* :pswitch_1 */
/* sget-wide v12, Lcom/android/server/am/MiuiMemReclaimer;->FILE_RSS_LIMIT_KB:J */
/* cmp-long v7, v12, v10 */
/* if-lez v7, :cond_2 */
/* cmp-long v7, v5, v12 */
/* if-gez v7, :cond_2 */
/* .line 588 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = com.android.server.am.MiuiMemReclaimer$CompactProcInfo .-$$Nest$mgetPid ( p1 );
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ", file RSS is too small "; // const-string v4, ", file RSS is too small "
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v5, v6 ); // invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v0 );
/* .line 590 */
/* .line 580 */
/* :pswitch_2 */
/* sget-wide v12, Lcom/android/server/am/MiuiMemReclaimer;->ANON_RSS_LIMIT_KB:J */
/* cmp-long v7, v12, v10 */
/* if-lez v7, :cond_2 */
/* cmp-long v7, v8, v12 */
/* if-gez v7, :cond_2 */
/* .line 581 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = com.android.server.am.MiuiMemReclaimer$CompactProcInfo .-$$Nest$mgetPid ( p1 );
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ", anon RSS is too small "; // const-string v4, ", anon RSS is too small "
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v8, v9 ); // invoke-virtual {v1, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v0 );
/* .line 583 */
/* .line 602 */
} // :cond_2
} // :goto_2
/* .line 569 */
} // .end local v5 # "rssFile":J
} // .end local v8 # "rssAnon":J
} // :cond_3
} // :goto_3
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = com.android.server.am.MiuiMemReclaimer$CompactProcInfo .-$$Nest$mgetPid ( p1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " with no memory usage.Dead?"; // const-string v1, " with no memory usage.Dead?"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v0 );
/* .line 571 */
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x179a1 -> :sswitch_2 */
/* 0x2dc2cc -> :sswitch_1 */
/* 0x2ff57c -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private Boolean isLowBattery ( ) {
/* .locals 1 */
/* .line 152 */
/* iget-boolean v0, p0, Lcom/android/server/am/MiuiMemReclaimer;->mBatteryLow:Z */
} // .end method
private Boolean isProtectProcess ( Integer p0, Integer p1, java.lang.String p2, java.lang.String p3 ) {
/* .locals 2 */
/* .param p1, "mode" # I */
/* .param p2, "uid" # I */
/* .param p3, "pkgName" # Ljava/lang/String; */
/* .param p4, "procName" # Ljava/lang/String; */
/* .line 299 */
/* const/16 v0, 0x1d8 */
/* .line 304 */
/* .local v0, "protectFlag":I */
v1 = v1 = this.mSmartPowerService;
/* if-nez v1, :cond_1 */
v1 = this.mSmartPowerService;
v1 = /* .line 305 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 309 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 307 */
} // :cond_1
} // :goto_0
int v1 = 1; // const/4 v1, 0x1
} // .end method
private void logTargetProcsDetails ( java.util.List p0, java.lang.String p1 ) {
/* .locals 12 */
/* .param p2, "action" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;", */
/* ">;", */
/* "Ljava/lang/String;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 471 */
/* .local p1, "infoList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;>;" */
v0 = if ( p1 != null) { // if-eqz p1, :cond_3
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 472 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/am/MiuiMemReclaimer;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 473 */
int v0 = 0; // const/4 v0, 0x0
/* .line 474 */
/* .local v0, "num":I */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo; */
/* .line 475 */
/* .local v2, "info":Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo; */
v3 = com.android.server.am.MiuiMemReclaimer$CompactProcInfo .-$$Nest$mgetPid ( v2 );
/* if-nez v3, :cond_1 */
/* .line 476 */
} // :cond_1
/* add-int/lit8 v0, v0, 0x1 */
/* .line 478 */
java.lang.Integer .valueOf ( v0 );
v4 = com.android.server.am.MiuiMemReclaimer$CompactProcInfo .-$$Nest$mgetPid ( v2 );
java.lang.Integer .valueOf ( v4 );
com.android.server.am.MiuiMemReclaimer$CompactProcInfo .-$$Nest$mgetProcessName ( v2 );
v6 = this.rss;
int v7 = 0; // const/4 v7, 0x0
/* aget-wide v6, v6, v7 */
java.lang.Long .valueOf ( v6,v7 );
v6 = this.rss;
int v8 = 1; // const/4 v8, 0x1
/* aget-wide v8, v6, v8 */
/* .line 479 */
java.lang.Long .valueOf ( v8,v9 );
v6 = this.rss;
int v9 = 2; // const/4 v9, 0x2
/* aget-wide v9, v6, v9 */
java.lang.Long .valueOf ( v9,v10 );
v6 = this.rss;
int v10 = 3; // const/4 v10, 0x3
/* aget-wide v10, v6, v10 */
java.lang.Long .valueOf ( v10,v11 );
/* move-object v6, p2 */
/* filled-new-array/range {v3 ..v10}, [Ljava/lang/Object; */
/* .line 476 */
final String v4 = "No.%s Proc %s:%s action=%s rss[%s, %s, %s, %s]"; // const-string v4, "No.%s Proc %s:%s action=%s rss[%s, %s, %s, %s]"
java.lang.String .format ( v4,v3 );
final String v4 = "MiuiMemoryService"; // const-string v4, "MiuiMemoryService"
android.util.Slog .d ( v4,v3 );
/* .line 480 */
} // .end local v2 # "info":Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;
/* .line 482 */
} // .end local v0 # "num":I
} // :cond_2
return;
/* .line 471 */
} // :cond_3
} // :goto_1
return;
} // .end method
private void performCompactProcess ( com.miui.server.smartpower.IAppState$IRunningProcess p0, Integer p1 ) {
/* .locals 7 */
/* .param p1, "proc" # Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .param p2, "mode" # I */
/* .line 313 */
if ( p1 != null) { // if-eqz p1, :cond_8
v0 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPid()I
/* if-lez v0, :cond_8 */
v0 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).isKilled ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->isKilled()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* goto/16 :goto_3 */
/* .line 314 */
} // :cond_0
/* new-instance v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo; */
/* invoke-direct {v0, p1, p2}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;-><init>(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)V */
/* .line 315 */
/* .local v0, "info":Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo; */
v1 = /* invoke-direct {p0, v0}, Lcom/android/server/am/MiuiMemReclaimer;->isCompactSatisfied(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)Z */
/* if-nez v1, :cond_2 */
/* .line 316 */
/* iget-boolean v1, p0, Lcom/android/server/am/MiuiMemReclaimer;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
final String v1 = "MiuiMemoryService"; // const-string v1, "MiuiMemoryService"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Proc "; // const-string v3, "Proc "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = com.android.server.am.MiuiMemReclaimer$CompactProcInfo .-$$Nest$mgetPid ( v0 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " isn\'t compact satisfied"; // const-string v3, " isn\'t compact satisfied"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 317 */
} // :cond_1
return;
/* .line 320 */
} // :cond_2
v1 = this.proc;
v1 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v1 ).isKilled ( ); // invoke-virtual {v1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->isKilled()Z
/* if-nez v1, :cond_7 */
v1 = this.proc;
/* .line 321 */
v1 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v1 ).getAdj ( ); // invoke-virtual {v1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I
v1 = /* invoke-direct {p0, v1, p2}, Lcom/android/server/am/MiuiMemReclaimer;->isAdjInCompactRange(II)Z */
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 322 */
v1 = (( com.android.server.am.MiuiMemReclaimer ) p0 ).isCompactNeeded ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/am/MiuiMemReclaimer;->isCompactNeeded(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)Z
/* if-nez v1, :cond_3 */
/* goto/16 :goto_2 */
/* .line 325 */
} // :cond_3
com.android.server.am.SystemPressureController .getInstance ( );
v1 = (( com.android.server.am.SystemPressureController ) v1 ).isStartingApp ( ); // invoke-virtual {v1}, Lcom/android/server/am/SystemPressureController;->isStartingApp()Z
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 326 */
com.android.server.am.SystemPressureController .getInstance ( );
v1 = this.mStartingAppLock;
/* monitor-enter v1 */
/* .line 327 */
try { // :try_start_0
com.android.server.am.SystemPressureController .getInstance ( );
v2 = (( com.android.server.am.SystemPressureController ) v2 ).isStartingApp ( ); // invoke-virtual {v2}, Lcom/android/server/am/SystemPressureController;->isStartingApp()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 329 */
try { // :try_start_1
com.android.server.am.SystemPressureController .getInstance ( );
v2 = this.mStartingAppLock;
/* .line 330 */
/* const-wide/16 v3, 0x7d0 */
(( java.lang.Object ) v2 ).wait ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/lang/Object;->wait(J)V
/* :try_end_1 */
/* .catch Ljava/lang/InterruptedException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 333 */
/* .line 331 */
/* :catch_0 */
/* move-exception v2 */
/* .line 332 */
/* .local v2, "e":Ljava/lang/InterruptedException; */
try { // :try_start_2
(( java.lang.InterruptedException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V
/* .line 335 */
} // .end local v2 # "e":Ljava/lang/InterruptedException;
} // :cond_4
} // :goto_0
/* monitor-exit v1 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v2 */
/* .line 337 */
} // :cond_5
} // :goto_1
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
/* .line 339 */
/* .local v1, "startTime":J */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "MiuiCompact:"; // const-string v4, "MiuiCompact:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.action;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " "; // const-string v4, " "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 340 */
v4 = com.android.server.am.MiuiMemReclaimer$CompactProcInfo .-$$Nest$mgetPid ( v0 );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ":"; // const-string v4, ":"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.am.MiuiMemReclaimer$CompactProcInfo .-$$Nest$mgetProcessName ( v0 );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "-"; // const-string v4, "-"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.proc;
v4 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v4 ).getAdj ( ); // invoke-virtual {v4}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 339 */
/* const-wide/16 v4, 0x1 */
android.os.Trace .traceBegin ( v4,v5,v3 );
/* .line 341 */
v3 = this.action;
v6 = com.android.server.am.MiuiMemReclaimer$CompactProcInfo .-$$Nest$mgetPid ( v0 );
(( com.android.server.am.MiuiMemReclaimer ) p0 ).performCompaction ( v3, v6 ); // invoke-virtual {p0, v3, v6}, Lcom/android/server/am/MiuiMemReclaimer;->performCompaction(Ljava/lang/String;I)V
/* .line 342 */
android.os.Trace .traceEnd ( v4,v5 );
/* .line 344 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
/* iput-wide v3, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->lastCompactTimeMillis:J */
/* .line 345 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v3 */
/* sub-long/2addr v3, v1 */
/* iput-wide v3, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->compactDurationMillis:J */
/* .line 346 */
int v3 = 1; // const/4 v3, 0x1
com.android.server.am.MiuiMemReclaimer$CompactProcInfo .-$$Nest$mcomputeRssDiff ( v0,v3 );
/* .line 347 */
v3 = this.mCompactionStats;
/* monitor-enter v3 */
/* .line 348 */
try { // :try_start_3
v4 = this.mCompactionStats;
v5 = com.android.server.am.MiuiMemReclaimer$CompactProcInfo .-$$Nest$mgetPid ( v0 );
java.lang.Integer .valueOf ( v5 );
/* .line 349 */
v4 = this.mCompactionStats;
v5 = com.android.server.am.MiuiMemReclaimer$CompactProcInfo .-$$Nest$mgetPid ( v0 );
java.lang.Integer .valueOf ( v5 );
/* .line 350 */
/* monitor-exit v3 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* .line 351 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Compacted proc "; // const-string v4, "Compacted proc "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.am.MiuiMemReclaimer$CompactProcInfo ) v0 ).toString ( ); // invoke-virtual {v0}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const v4, 0x13ba0 */
android.util.EventLog .writeEvent ( v4,v3 );
/* .line 352 */
/* iget-boolean v3, p0, Lcom/android/server/am/MiuiMemReclaimer;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_6
final String v3 = "MiuiMemoryService"; // const-string v3, "MiuiMemoryService"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Compacted proc "; // const-string v5, "Compacted proc "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.am.MiuiMemReclaimer$CompactProcInfo ) v0 ).toString ( ); // invoke-virtual {v0}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v4 );
/* .line 353 */
} // :cond_6
return;
/* .line 350 */
/* :catchall_1 */
/* move-exception v4 */
try { // :try_start_4
/* monitor-exit v3 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* throw v4 */
/* .line 323 */
} // .end local v1 # "startTime":J
} // :cond_7
} // :goto_2
return;
/* .line 313 */
} // .end local v0 # "info":Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;
} // :cond_8
} // :goto_3
return;
} // .end method
private void performCompactProcesses ( Integer p0 ) {
/* .locals 10 */
/* .param p1, "mode" # I */
/* .line 356 */
int v0 = 1; // const/4 v0, 0x1
/* if-eq p1, v0, :cond_1 */
/* .line 357 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
/* iget-wide v3, p0, Lcom/android/server/am/MiuiMemReclaimer;->mLastCompactionTimeMillis:J */
/* sub-long/2addr v1, v3 */
/* sget-wide v3, Lcom/android/server/am/MiuiMemReclaimer;->COMPACT_ALL_MIN_INTERVAL:J */
/* cmp-long v1, v1, v3 */
/* if-gez v1, :cond_1 */
/* .line 359 */
/* iget-boolean v0, p0, Lcom/android/server/am/MiuiMemReclaimer;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
final String v0 = "MiuiMemoryService"; // const-string v0, "MiuiMemoryService"
final String v1 = "Skip compaction for frequently"; // const-string v1, "Skip compaction for frequently"
android.util.Slog .d ( v0,v1 );
/* .line 360 */
} // :cond_0
return;
/* .line 362 */
} // :cond_1
/* iget-boolean v1, p0, Lcom/android/server/am/MiuiMemReclaimer;->mInterruptNeeded:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 363 */
final String v0 = "MiuiMemoryService"; // const-string v0, "MiuiMemoryService"
final String v1 = "Compact processes skipped"; // const-string v1, "Compact processes skipped"
android.util.Slog .w ( v0,v1 );
/* .line 364 */
return;
/* .line 367 */
} // :cond_2
/* invoke-direct {p0, p1}, Lcom/android/server/am/MiuiMemReclaimer;->filterCompactionProcs(I)Ljava/util/List; */
/* .line 368 */
v2 = /* .local v1, "compactTargetProcs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;>;" */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 369 */
final String v0 = "MiuiMemoryService"; // const-string v0, "MiuiMemoryService"
final String v2 = "No process can compact."; // const-string v2, "No process can compact."
android.util.Slog .w ( v0,v2 );
/* .line 370 */
return;
/* .line 372 */
} // :cond_3
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_8
/* check-cast v3, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo; */
/* .line 373 */
/* .local v3, "info":Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo; */
/* iget-boolean v4, p0, Lcom/android/server/am/MiuiMemReclaimer;->mInterruptNeeded:Z */
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 374 */
final String v0 = "MiuiMemoryService"; // const-string v0, "MiuiMemoryService"
final String v2 = "Compact processes interrupted"; // const-string v2, "Compact processes interrupted"
android.util.Slog .w ( v0,v2 );
/* .line 375 */
return;
/* .line 378 */
} // :cond_4
v4 = this.proc;
v4 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v4 ).isKilled ( ); // invoke-virtual {v4}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->isKilled()Z
/* if-nez v4, :cond_7 */
v4 = this.proc;
/* .line 379 */
v4 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v4 ).getAmsProcState ( ); // invoke-virtual {v4}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAmsProcState()I
v4 = /* invoke-direct {p0, v4, p1}, Lcom/android/server/am/MiuiMemReclaimer;->isAdjInCompactRange(II)Z */
/* if-nez v4, :cond_5 */
/* goto/16 :goto_1 */
/* .line 384 */
} // :cond_5
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v4 */
/* .line 386 */
/* .local v4, "startTime":J */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "MiuiCompact:"; // const-string v7, "MiuiCompact:"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.action;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " "; // const-string v7, " "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 387 */
v7 = com.android.server.am.MiuiMemReclaimer$CompactProcInfo .-$$Nest$mgetPid ( v3 );
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = ":"; // const-string v7, ":"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.am.MiuiMemReclaimer$CompactProcInfo .-$$Nest$mgetProcessName ( v3 );
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = "-"; // const-string v7, "-"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.proc;
v7 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v7 ).getAdj ( ); // invoke-virtual {v7}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 386 */
/* const-wide/16 v7, 0x1 */
android.os.Trace .traceBegin ( v7,v8,v6 );
/* .line 388 */
v6 = this.action;
v9 = com.android.server.am.MiuiMemReclaimer$CompactProcInfo .-$$Nest$mgetPid ( v3 );
(( com.android.server.am.MiuiMemReclaimer ) p0 ).performCompaction ( v6, v9 ); // invoke-virtual {p0, v6, v9}, Lcom/android/server/am/MiuiMemReclaimer;->performCompaction(Ljava/lang/String;I)V
/* .line 389 */
android.os.Trace .traceEnd ( v7,v8 );
/* .line 391 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v6 */
/* iput-wide v6, v3, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->lastCompactTimeMillis:J */
/* .line 392 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v6 */
/* sub-long/2addr v6, v4 */
/* iput-wide v6, v3, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->compactDurationMillis:J */
/* .line 393 */
com.android.server.am.MiuiMemReclaimer$CompactProcInfo .-$$Nest$mcomputeRssDiff ( v3,v0 );
/* .line 394 */
v6 = this.mCompactionStats;
/* monitor-enter v6 */
/* .line 395 */
try { // :try_start_0
v7 = this.mCompactionStats;
v8 = com.android.server.am.MiuiMemReclaimer$CompactProcInfo .-$$Nest$mgetPid ( v3 );
java.lang.Integer .valueOf ( v8 );
/* .line 396 */
v7 = this.mCompactionStats;
v8 = com.android.server.am.MiuiMemReclaimer$CompactProcInfo .-$$Nest$mgetPid ( v3 );
java.lang.Integer .valueOf ( v8 );
/* .line 397 */
/* monitor-exit v6 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 398 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Compacted proc "; // const-string v7, "Compacted proc "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.am.MiuiMemReclaimer$CompactProcInfo ) v3 ).toString ( ); // invoke-virtual {v3}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const v7, 0x13ba0 */
android.util.EventLog .writeEvent ( v7,v6 );
/* .line 399 */
/* iget-boolean v6, p0, Lcom/android/server/am/MiuiMemReclaimer;->DEBUG:Z */
if ( v6 != null) { // if-eqz v6, :cond_6
final String v6 = "MiuiMemoryService"; // const-string v6, "MiuiMemoryService"
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Compacted proc "; // const-string v8, "Compacted proc "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.am.MiuiMemReclaimer$CompactProcInfo ) v3 ).toString ( ); // invoke-virtual {v3}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v6,v7 );
/* .line 400 */
} // .end local v3 # "info":Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;
} // .end local v4 # "startTime":J
} // :cond_6
/* goto/16 :goto_0 */
/* .line 397 */
/* .restart local v3 # "info":Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo; */
/* .restart local v4 # "startTime":J */
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_1
/* monitor-exit v6 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v0 */
/* .line 380 */
} // .end local v4 # "startTime":J
} // :cond_7
} // :goto_1
final String v4 = "MiuiMemoryService"; // const-string v4, "MiuiMemoryService"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
com.android.server.am.MiuiMemReclaimer$CompactProcInfo .-$$Nest$mgetProcessName ( v3 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " changed, compaction skipped."; // const-string v6, " changed, compaction skipped."
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v4,v5 );
/* .line 381 */
/* goto/16 :goto_0 */
/* .line 401 */
} // .end local v3 # "info":Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;
} // :cond_8
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* iput-wide v2, p0, Lcom/android/server/am/MiuiMemReclaimer;->mLastCompactionTimeMillis:J */
/* .line 402 */
final String v0 = "MiuiMemoryService"; // const-string v0, "MiuiMemoryService"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Compact processes success! Compact mode: "; // const-string v3, "Compact processes success! Compact mode: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v2 );
/* .line 403 */
return;
} // .end method
private static void performCompactionLegacy ( java.lang.String p0, Integer p1 ) {
/* .locals 5 */
/* .param p0, "action" # Ljava/lang/String; */
/* .param p1, "pid" # I */
/* .line 615 */
int v0 = 0; // const/4 v0, 0x0
/* .line 617 */
/* .local v0, "fos":Ljava/io/FileOutputStream; */
try { // :try_start_0
/* new-instance v1, Ljava/io/FileOutputStream; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "/proc/"; // const-string v3, "/proc/"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = "/reclaim"; // const-string v3, "/reclaim"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V */
/* move-object v0, v1 */
/* .line 618 */
(( java.lang.String ) p0 ).getBytes ( ); // invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B
(( java.io.FileOutputStream ) v0 ).write ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 623 */
/* nop */
/* .line 624 */
try { // :try_start_1
(( java.io.FileOutputStream ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 628 */
} // :cond_0
} // :goto_0
/* .line 626 */
/* :catch_0 */
/* move-exception v1 */
/* .line 629 */
/* .line 622 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 619 */
/* :catch_1 */
/* move-exception v1 */
/* .line 620 */
/* .local v1, "e":Ljava/io/IOException; */
try { // :try_start_2
final String v2 = "MiuiMemoryService"; // const-string v2, "MiuiMemoryService"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Compaction failed: pid "; // const-string v4, "Compaction failed: pid "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 623 */
} // .end local v1 # "e":Ljava/io/IOException;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 624 */
try { // :try_start_3
(( java.io.FileOutputStream ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 630 */
} // :goto_1
return;
/* .line 623 */
} // :goto_2
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 624 */
try { // :try_start_4
(( java.io.FileOutputStream ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_2 */
/* .line 626 */
/* :catch_2 */
/* move-exception v2 */
/* .line 628 */
} // :cond_1
} // :goto_3
/* nop */
/* .line 629 */
} // :goto_4
/* throw v1 */
} // .end method
private void performCompactionNew ( java.lang.String p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "pid" # I */
/* .line 634 */
try { // :try_start_0
v0 = (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
/* sparse-switch v0, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
final String v0 = "file"; // const-string v0, "file"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
/* :sswitch_1 */
final String v0 = "anon"; // const-string v0, "anon"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 2; // const/4 v0, 0x2
/* :sswitch_2 */
final String v0 = "all"; // const-string v0, "all"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
int v0 = -1; // const/4 v0, -0x1
} // :goto_1
/* packed-switch v0, :pswitch_data_0 */
/* .line 636 */
/* :pswitch_0 */
v0 = this.mProcessDependencies;
v1 = com.android.server.am.CachedAppOptimizer$CompactProfile.FULL;
(( com.android.server.am.CachedAppOptimizer$DefaultProcessDependencies ) v0 ).performCompaction ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Lcom/android/server/am/CachedAppOptimizer$DefaultProcessDependencies;->performCompaction(Lcom/android/server/am/CachedAppOptimizer$CompactProfile;I)V
/* .line 638 */
/* :pswitch_1 */
v0 = this.mProcessDependencies;
v1 = com.android.server.am.CachedAppOptimizer$CompactProfile.SOME;
(( com.android.server.am.CachedAppOptimizer$DefaultProcessDependencies ) v0 ).performCompaction ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Lcom/android/server/am/CachedAppOptimizer$DefaultProcessDependencies;->performCompaction(Lcom/android/server/am/CachedAppOptimizer$CompactProfile;I)V
/* .line 640 */
/* :pswitch_2 */
v0 = this.mProcessDependencies;
v1 = com.android.server.am.CachedAppOptimizer$CompactProfile.ANON;
(( com.android.server.am.CachedAppOptimizer$DefaultProcessDependencies ) v0 ).performCompaction ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Lcom/android/server/am/CachedAppOptimizer$DefaultProcessDependencies;->performCompaction(Lcom/android/server/am/CachedAppOptimizer$CompactProfile;I)V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 644 */
} // :goto_2
/* .line 642 */
/* :catch_0 */
/* move-exception v0 */
/* .line 643 */
/* .local v0, "e":Ljava/io/IOException; */
(( java.io.IOException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
/* .line 645 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_3
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x179a1 -> :sswitch_2 */
/* 0x2dc2cc -> :sswitch_1 */
/* 0x2ff57c -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_0 */
/* :pswitch_1 */
/* :pswitch_2 */
} // .end packed-switch
} // .end method
public static void reclaimPage ( ) {
/* .locals 2 */
/* .line 183 */
/* sget-boolean v0, Lcom/android/server/am/MiuiMemReclaimer;->RECLAIM_IF_NEEDED:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 184 */
final String v0 = "/sys/kernel/mi_reclaim/event"; // const-string v0, "/sys/kernel/mi_reclaim/event"
int v1 = 1; // const/4 v1, 0x1
com.android.server.am.MiuiMemReclaimer .writeToNode ( v0,v1 );
/* .line 185 */
} // :cond_0
return;
} // .end method
public static void reclaimPage ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "reclaimSizeMB" # I */
/* .line 178 */
/* sget-boolean v0, Lcom/android/server/am/MiuiMemReclaimer;->RECLAIM_IF_NEEDED:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 179 */
final String v0 = "/sys/kernel/mi_reclaim/event"; // const-string v0, "/sys/kernel/mi_reclaim/event"
com.android.server.am.MiuiMemReclaimer .writeToNode ( v0,p0 );
/* .line 180 */
} // :cond_0
return;
} // .end method
private void sortProcsByRss ( java.util.List p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p2, "action" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;", */
/* ">;", */
/* "Ljava/lang/String;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 452 */
/* .local p1, "infoList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;>;" */
v0 = if ( p1 != null) { // if-eqz p1, :cond_1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 453 */
} // :cond_0
/* new-instance v0, Lcom/android/server/am/MiuiMemReclaimer$3; */
/* invoke-direct {v0, p0, p2}, Lcom/android/server/am/MiuiMemReclaimer$3;-><init>(Lcom/android/server/am/MiuiMemReclaimer;Ljava/lang/String;)V */
java.util.Collections .sort ( p1,v0 );
/* .line 468 */
return;
/* .line 452 */
} // :cond_1
} // :goto_0
return;
} // .end method
public static void writeToNode ( java.lang.String p0, Integer p1 ) {
/* .locals 8 */
/* .param p0, "node" # Ljava/lang/String; */
/* .param p1, "value" # I */
/* .line 198 */
final String v0 = "MiuiMemoryService"; // const-string v0, "MiuiMemoryService"
int v1 = 0; // const/4 v1, 0x0
/* .line 199 */
/* .local v1, "writer":Ljava/io/FileWriter; */
/* new-instance v2, Ljava/io/File; */
/* invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 200 */
/* .local v2, "file":Ljava/io/File; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " "; // const-string v4, " "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p0 ); // invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ":"; // const-string v4, ":"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 201 */
/* .local v3, "commMsg":Ljava/lang/String; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "error"; // const-string v5, "error"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 202 */
/* .local v4, "errMsg":Ljava/lang/String; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "success" */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 203 */
/* .local v5, "succMsg":Ljava/lang/String; */
v6 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
/* if-nez v6, :cond_0 */
/* .line 204 */
return;
/* .line 206 */
} // :cond_0
try { // :try_start_0
/* new-instance v6, Ljava/io/FileWriter; */
/* invoke-direct {v6, v2}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V */
/* move-object v1, v6 */
/* .line 207 */
java.lang.String .valueOf ( p1 );
(( java.io.FileWriter ) v1 ).write ( v6 ); // invoke-virtual {v1, v6}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 211 */
/* nop */
/* .line 213 */
try { // :try_start_1
(( java.io.FileWriter ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 216 */
} // :goto_0
/* .line 214 */
/* :catch_0 */
/* move-exception v6 */
/* .line 215 */
/* .local v6, "e":Ljava/io/IOException; */
android.util.Slog .e ( v0,v4,v6 );
} // .end local v6 # "e":Ljava/io/IOException;
/* .line 211 */
/* :catchall_0 */
/* move-exception v6 */
/* .line 208 */
/* :catch_1 */
/* move-exception v6 */
/* .line 209 */
/* .restart local v6 # "e":Ljava/io/IOException; */
try { // :try_start_2
android.util.Slog .e ( v0,v4,v6 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 211 */
/* nop */
} // .end local v6 # "e":Ljava/io/IOException;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 213 */
try { // :try_start_3
(( java.io.FileWriter ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 219 */
} // :cond_1
} // :goto_1
return;
/* .line 211 */
} // :goto_2
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 213 */
try { // :try_start_4
(( java.io.FileWriter ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_2 */
/* .line 216 */
/* .line 214 */
/* :catch_2 */
/* move-exception v7 */
/* .line 215 */
/* .local v7, "e":Ljava/io/IOException; */
android.util.Slog .e ( v0,v4,v7 );
/* .line 218 */
} // .end local v7 # "e":Ljava/io/IOException;
} // :cond_2
} // :goto_3
/* throw v6 */
} // .end method
/* # virtual methods */
public void dumpCompactionStats ( java.io.PrintWriter p0 ) {
/* .locals 6 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 222 */
v0 = v0 = this.mCompactionStats;
/* if-nez v0, :cond_0 */
/* .line 223 */
final String v0 = "Compaction never done!"; // const-string v0, "Compaction never done!"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 224 */
return;
/* .line 226 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 227 */
/* .local v0, "num":I */
final String v1 = "Compaction Statistics:"; // const-string v1, "Compaction Statistics:"
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 228 */
v1 = this.mCompactionStats;
/* monitor-enter v1 */
/* .line 230 */
try { // :try_start_0
v2 = this.mCompactionStats;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Ljava/util/Map$Entry; */
/* .line 231 */
/* .local v3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;>;" */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 232 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "No."; // const-string v5, "No."
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = ": "; // const-string v5, ": "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v5, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo; */
(( com.android.server.am.MiuiMemReclaimer$CompactProcInfo ) v5 ).toString ( ); // invoke-virtual {v5}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v4 ); // invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 233 */
} // .end local v3 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;>;"
/* .line 234 */
} // :cond_1
/* monitor-exit v1 */
/* .line 235 */
return;
/* .line 234 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public synchronized void interruptProcCompaction ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "pid" # I */
/* monitor-enter p0 */
/* .line 281 */
try { // :try_start_0
/* if-ne v0, p1, :cond_0 */
/* .line 282 */
v0 = this.mProcessDependencies;
(( com.android.server.am.CachedAppOptimizer$DefaultProcessDependencies ) v0 ).interruptProcCompaction ( ); // invoke-virtual {v0}, Lcom/android/server/am/CachedAppOptimizer$DefaultProcessDependencies;->interruptProcCompaction()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 284 */
} // .end local p0 # "this":Lcom/android/server/am/MiuiMemReclaimer;
} // :cond_0
/* monitor-exit p0 */
return;
/* .line 280 */
} // .end local p1 # "pid":I
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
public synchronized void interruptProcsCompaction ( ) {
/* .locals 1 */
/* monitor-enter p0 */
/* .line 277 */
int v0 = 1; // const/4 v0, 0x1
try { // :try_start_0
/* iput-boolean v0, p0, Lcom/android/server/am/MiuiMemReclaimer;->mInterruptNeeded:Z */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 278 */
/* monitor-exit p0 */
return;
/* .line 276 */
} // .end local p0 # "this":Lcom/android/server/am/MiuiMemReclaimer;
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
public Boolean isCompactNeeded ( com.miui.server.smartpower.IAppState$IRunningProcess p0, Integer p1 ) {
/* .locals 12 */
/* .param p1, "proc" # Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .param p2, "mode" # I */
/* .line 490 */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getProcessName ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;
/* .line 491 */
/* .local v0, "procName":Ljava/lang/String; */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;
/* .line 492 */
/* .local v1, "pkgName":Ljava/lang/String; */
v2 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUid()I
/* .line 493 */
/* .local v2, "uid":I */
v3 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getAdj ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I
/* .line 495 */
/* .local v3, "adj":I */
int v4 = 0; // const/4 v4, 0x0
int v5 = 4; // const/4 v5, 0x4
/* if-ne p2, v5, :cond_1 */
/* .line 496 */
v6 = com.android.server.am.ProcessListStub .get ( );
/* if-nez v6, :cond_0 */
/* .line 497 */
v6 = /* invoke-direct {p0}, Lcom/android/server/am/MiuiMemReclaimer;->isCameraMode()Z */
/* if-nez v6, :cond_0 */
/* .line 498 */
v6 = /* invoke-direct {p0}, Lcom/android/server/am/MiuiMemReclaimer;->isLowBattery()Z */
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 499 */
} // :cond_0
/* .line 502 */
} // :cond_1
v6 = com.android.server.am.MiuiMemReclaimer.COMPACTION_PROC_NAME_WHITE_LIST;
v6 = (( java.util.ArrayList ) v6 ).contains ( v0 ); // invoke-virtual {v6, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v6, :cond_a */
/* .line 503 */
final String v6 = "com.miui.miwallpaper"; // const-string v6, "com.miui.miwallpaper"
v6 = (( java.lang.String ) v0 ).startsWith ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* if-nez v6, :cond_a */
/* if-nez p2, :cond_2 */
/* .line 505 */
final String v6 = "com.android.camera"; // const-string v6, "com.android.camera"
v6 = android.text.TextUtils .equals ( v6,v0 );
/* if-nez v6, :cond_a */
} // :cond_2
int v6 = 2; // const/4 v6, 0x2
/* if-eq p2, v6, :cond_3 */
/* .line 507 */
final String v6 = "com.tencent.mm"; // const-string v6, "com.tencent.mm"
v6 = android.text.TextUtils .equals ( v6,v0 );
/* if-nez v6, :cond_a */
} // :cond_3
int v6 = 3; // const/4 v6, 0x3
/* if-ne p2, v6, :cond_4 */
/* .line 510 */
v6 = android.text.TextUtils .equals ( v1,v0 );
/* if-nez v6, :cond_4 */
/* .line 513 */
} // :cond_4
/* const/16 v6, 0x3e8 */
/* if-le v2, v6, :cond_9 */
v6 = /* invoke-direct {p0, v3, p2}, Lcom/android/server/am/MiuiMemReclaimer;->isAdjInCompactRange(II)Z */
if ( v6 != null) { // if-eqz v6, :cond_9
/* .line 514 */
v6 = /* invoke-direct {p0, p2, v2, v1, v0}, Lcom/android/server/am/MiuiMemReclaimer;->isProtectProcess(IILjava/lang/String;Ljava/lang/String;)Z */
if ( v6 != null) { // if-eqz v6, :cond_5
/* .line 517 */
} // :cond_5
/* if-ne p2, v5, :cond_8 */
/* .line 518 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v5 */
/* .line 519 */
/* .local v5, "nowTime":J */
v7 = this.mCompactionStats;
v8 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPid()I
java.lang.Integer .valueOf ( v8 );
/* check-cast v7, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo; */
/* .line 520 */
/* .local v7, "procInfo":Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo; */
if ( v7 != null) { // if-eqz v7, :cond_6
/* iget-wide v8, v7, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->lastCompactTimeMillis:J */
/* sub-long v8, v5, v8 */
/* sget-wide v10, Lcom/android/server/am/MiuiMemReclaimer;->COMPACT_PROC_MIN_INTERVAL:J */
/* cmp-long v8, v8, v10 */
/* if-gez v8, :cond_6 */
v8 = this.action;
/* .line 523 */
v9 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getAmsProcState ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAmsProcState()I
com.android.server.am.MiuiMemReclaimer$CompactProcInfo .-$$Nest$smgenCompactAction ( v9,p2 );
/* .line 522 */
v8 = (( java.lang.String ) v8 ).equals ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_6
/* .line 524 */
/* .line 525 */
} // :cond_6
v8 = v8 = this.mSmartPowerService;
if ( v8 != null) { // if-eqz v8, :cond_7
/* .line 526 */
/* .line 527 */
} // :cond_7
final String v8 = "camera-improve"; // const-string v8, "camera-improve"
(( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getAdjType ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdjType()Ljava/lang/String;
v8 = (( java.lang.String ) v8 ).equals ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_8
/* .line 528 */
/* .line 531 */
} // .end local v5 # "nowTime":J
} // .end local v7 # "procInfo":Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;
} // :cond_8
int v4 = 1; // const/4 v4, 0x1
/* .line 515 */
} // :cond_9
} // :goto_0
/* .line 511 */
} // :cond_a
} // :goto_1
} // .end method
public void performCompaction ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "pid" # I */
/* .line 606 */
/* sget-boolean v0, Lcom/android/server/am/MiuiMemReclaimer;->USE_LEGACY_COMPACTION:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 607 */
com.android.server.am.MiuiMemReclaimer .performCompactionLegacy ( p1,p2 );
/* .line 609 */
} // :cond_0
/* invoke-direct {p0, p1, p2}, Lcom/android/server/am/MiuiMemReclaimer;->performCompactionNew(Ljava/lang/String;I)V */
/* .line 611 */
} // :goto_0
return;
} // .end method
public void runGlobalCompaction ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "vmPressureLevel" # I */
/* .line 238 */
int v0 = -1; // const/4 v0, -0x1
/* .line 239 */
/* .local v0, "reclaimSizeMB":I */
/* packed-switch p1, :pswitch_data_0 */
/* .line 248 */
/* :pswitch_0 */
/* const/16 v0, 0x64 */
/* .line 249 */
/* .line 244 */
/* :pswitch_1 */
/* const/16 v0, 0x32 */
/* .line 245 */
/* .line 241 */
/* :pswitch_2 */
/* const/16 v0, 0x1e */
/* .line 242 */
/* nop */
/* .line 253 */
} // :goto_0
/* if-gez v0, :cond_0 */
return;
/* .line 254 */
} // :cond_0
v1 = this.mCompactorHandler;
/* .line 255 */
/* const/16 v2, 0x64 */
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {p0, v2, v0, v3}, Lcom/android/server/am/MiuiMemReclaimer;->generateMessage(IILjava/lang/Object;)Landroid/os/Message; */
/* .line 254 */
(( android.os.Handler ) v1 ).sendMessage ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 256 */
/* iget-boolean v1, p0, Lcom/android/server/am/MiuiMemReclaimer;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Global reclaim "; // const-string v2, "Global reclaim "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = "MB."; // const-string v2, "MB."
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiMemoryService"; // const-string v2, "MiuiMemoryService"
android.util.Slog .d ( v2,v1 );
/* .line 257 */
} // :cond_1
return;
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void runProcCompaction ( com.miui.server.smartpower.IAppState$IRunningProcess p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "proc" # Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .param p2, "mode" # I */
/* .line 260 */
if ( p1 != null) { // if-eqz p1, :cond_1
v0 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPid()I
/* if-lez v0, :cond_1 */
/* .line 261 */
v0 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).isSystemApp ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->isSystemApp()Z
/* if-nez v0, :cond_1 */
v0 = (( com.android.server.am.MiuiMemReclaimer ) p0 ).isCompactNeeded ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/am/MiuiMemReclaimer;->isCompactNeeded(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)Z
/* if-nez v0, :cond_0 */
/* .line 264 */
} // :cond_0
v0 = this.mCompactorHandler;
/* .line 265 */
/* const/16 v1, 0x65 */
/* invoke-direct {p0, v1, p2, p1}, Lcom/android/server/am/MiuiMemReclaimer;->generateMessage(IILjava/lang/Object;)Landroid/os/Message; */
/* .line 264 */
(( android.os.Handler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 266 */
return;
/* .line 262 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void runProcsCompaction ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "mode" # I */
/* .line 269 */
/* monitor-enter p0 */
/* .line 270 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
/* iput-boolean v0, p0, Lcom/android/server/am/MiuiMemReclaimer;->mInterruptNeeded:Z */
/* .line 271 */
/* monitor-exit p0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 272 */
v0 = this.mCompactorHandler;
/* .line 273 */
/* const/16 v1, 0x66 */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {p0, v1, p1, v2}, Lcom/android/server/am/MiuiMemReclaimer;->generateMessage(IILjava/lang/Object;)Landroid/os/Message; */
/* .line 272 */
(( android.os.Handler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 274 */
return;
/* .line 271 */
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_1
/* monitor-exit p0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v0 */
} // .end method
public void setAppStartingMode ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "appStarting" # Z */
/* .line 287 */
v0 = this.mProcessDependencies;
(( com.android.server.am.CachedAppOptimizer$DefaultProcessDependencies ) v0 ).setAppStartingMode ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/CachedAppOptimizer$DefaultProcessDependencies;->setAppStartingMode(Z)V
/* .line 288 */
return;
} // .end method
