public class com.android.server.am.SchedBoostManagerInternalStubImpl implements com.android.server.am.SchedBoostManagerInternalStub {
	 /* .source "SchedBoostManagerInternalStubImpl.java" */
	 /* # interfaces */
	 /* # instance fields */
	 private com.miui.server.rtboost.SchedBoostManagerInternal mSchedBoostService;
	 /* # direct methods */
	 public com.android.server.am.SchedBoostManagerInternalStubImpl ( ) {
		 /* .locals 0 */
		 /* .line 10 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 private com.miui.server.rtboost.SchedBoostManagerInternal getSchedBoostService ( ) {
		 /* .locals 1 */
		 /* .line 29 */
		 v0 = this.mSchedBoostService;
		 /* if-nez v0, :cond_0 */
		 /* .line 30 */
		 /* const-class v0, Lcom/miui/server/rtboost/SchedBoostManagerInternal; */
		 com.android.server.LocalServices .getService ( v0 );
		 /* check-cast v0, Lcom/miui/server/rtboost/SchedBoostManagerInternal; */
		 this.mSchedBoostService = v0;
		 /* .line 32 */
	 } // :cond_0
	 v0 = this.mSchedBoostService;
} // .end method
/* # virtual methods */
public void boostHomeAnim ( Long p0, Integer p1 ) {
	 /* .locals 1 */
	 /* .param p1, "duration" # J */
	 /* .param p3, "mode" # I */
	 /* .line 25 */
	 /* invoke-direct {p0}, Lcom/android/server/am/SchedBoostManagerInternalStubImpl;->getSchedBoostService()Lcom/miui/server/rtboost/SchedBoostManagerInternal; */
	 /* .line 26 */
	 return;
} // .end method
public void setRenderThreadTid ( com.android.server.wm.WindowProcessController p0 ) {
	 /* .locals 1 */
	 /* .param p1, "wpc" # Lcom/android/server/wm/WindowProcessController; */
	 /* .line 20 */
	 /* invoke-direct {p0}, Lcom/android/server/am/SchedBoostManagerInternalStubImpl;->getSchedBoostService()Lcom/miui/server/rtboost/SchedBoostManagerInternal; */
	 /* .line 21 */
	 return;
} // .end method
public void setSchedMode ( java.lang.String p0, Integer p1, Integer p2, Integer p3, Long p4 ) {
	 /* .locals 6 */
	 /* .param p1, "procName" # Ljava/lang/String; */
	 /* .param p2, "pid" # I */
	 /* .param p3, "rtid" # I */
	 /* .param p4, "schedMode" # I */
	 /* .param p5, "timeout" # J */
	 /* .line 15 */
	 /* invoke-direct {p0}, Lcom/android/server/am/SchedBoostManagerInternalStubImpl;->getSchedBoostService()Lcom/miui/server/rtboost/SchedBoostManagerInternal; */
	 /* filled-new-array {p2, p3}, [I */
	 /* move-wide v2, p5 */
	 /* move-object v4, p1 */
	 /* move v5, p4 */
	 /* invoke-interface/range {v0 ..v5}, Lcom/miui/server/rtboost/SchedBoostManagerInternal;->beginSchedThreads([IJLjava/lang/String;I)V */
	 /* .line 16 */
	 return;
} // .end method
