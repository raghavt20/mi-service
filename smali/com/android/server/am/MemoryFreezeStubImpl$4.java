class com.android.server.am.MemoryFreezeStubImpl$4 extends android.os.IVoldTaskListener$Stub {
	 /* .source "MemoryFreezeStubImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/am/MemoryFreezeStubImpl;->checkAndWriteBack()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.am.MemoryFreezeStubImpl this$0; //synthetic
/* # direct methods */
 com.android.server.am.MemoryFreezeStubImpl$4 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/MemoryFreezeStubImpl; */
/* .line 457 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/os/IVoldTaskListener$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onFinished ( Integer p0, android.os.PersistableBundle p1 ) {
/* .locals 2 */
/* .param p1, "status" # I */
/* .param p2, "extras" # Landroid/os/PersistableBundle; */
/* .line 465 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "WB finished, count "; // const-string v1, "WB finished, count "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "MFZ_PAGE_COUNT"; // const-string v1, "MFZ_PAGE_COUNT"
v1 = (( android.os.PersistableBundle ) p2 ).getInt ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/PersistableBundle;->getInt(Ljava/lang/String;)I
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MFZ"; // const-string v1, "MFZ"
android.util.Slog .d ( v1,v0 );
/* .line 466 */
v0 = this.this$0;
(( com.android.server.am.MemoryFreezeStubImpl ) v0 ).extmFlushFinished ( ); // invoke-virtual {v0}, Lcom/android/server/am/MemoryFreezeStubImpl;->extmFlushFinished()V
/* .line 467 */
return;
} // .end method
public void onStatus ( Integer p0, android.os.PersistableBundle p1 ) {
/* .locals 0 */
/* .param p1, "status" # I */
/* .param p2, "extras" # Landroid/os/PersistableBundle; */
/* .line 461 */
return;
} // .end method
