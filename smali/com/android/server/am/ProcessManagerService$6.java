class com.android.server.am.ProcessManagerService$6 implements java.lang.Runnable {
	 /* .source "ProcessManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/am/ProcessManagerService;->enableBinderMonitor(II)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.am.ProcessManagerService this$0; //synthetic
final Integer val$enable; //synthetic
final Integer val$pid; //synthetic
/* # direct methods */
 com.android.server.am.ProcessManagerService$6 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/ProcessManagerService; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 1066 */
this.this$0 = p1;
/* iput p2, p0, Lcom/android/server/am/ProcessManagerService$6;->val$pid:I */
/* iput p3, p0, Lcom/android/server/am/ProcessManagerService$6;->val$enable:I */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 3 */
/* .line 1070 */
try { // :try_start_0
v0 = this.this$0;
com.android.server.am.ProcessManagerService .-$$Nest$fgetmBinderDelayWriter ( v0 );
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* iget v2, p0, Lcom/android/server/am/ProcessManagerService$6;->val$pid:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " "; // const-string v2, " "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/am/ProcessManagerService$6;->val$enable:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.FileWriter ) v0 ).write ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
/* .line 1071 */
v0 = this.this$0;
com.android.server.am.ProcessManagerService .-$$Nest$fgetmBinderDelayWriter ( v0 );
(( java.io.FileWriter ) v0 ).flush ( ); // invoke-virtual {v0}, Ljava/io/FileWriter;->flush()V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1074 */
/* .line 1072 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1073 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "ProcessManager"; // const-string v1, "ProcessManager"
final String v2 = "error write exception log"; // const-string v2, "error write exception log"
android.util.Slog .e ( v1,v2 );
/* .line 1075 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
