.class public Lcom/android/server/am/ProcessListStubImpl;
.super Ljava/lang/Object;
.source "ProcessListStubImpl.java"

# interfaces
.implements Lcom/android/server/am/ProcessListStub;


# static fields
.field private static final APP_START_TIMEOUT_DUMP_DIR:Ljava/lang/String; = "/data/miuilog/stability/scout/app/"

.field private static final APP_START_TIMEOUT_DUMP_PREFIX:Ljava/lang/String; = "app_start_timeout_"

.field private static final DEF_MIN_EXTRA_FREE_KB:I

.field private static final ENABLE_MI_EXTRA_FREE:Z

.field private static final ENABLE_MI_SIZE_EXTRA_FREE:Z

.field private static final ENABLE_MI_SIZE_EXTRA_FREE_GAME_ONLY:Z

.field private static final KILL_REASON_START_TIMEOUT:Ljava/lang/String; = "start timeout"

.field private static final TAG:Ljava/lang/String; = "ProcessListStubImpl"


# instance fields
.field private mGameMode:Z

.field private mLastDisplayHeight:I

.field private mLastDisplayWidth:I

.field mPeriodicCleaner:Lcom/android/server/am/PeriodicCleanerInternalStub;

.field mPmi:Lcom/miui/server/process/ProcessManagerInternal;

.field private mSystemRenderThreadTid:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 41
    const-string v0, "persist.sys.spc.extra_free_enable"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/am/ProcessListStubImpl;->ENABLE_MI_EXTRA_FREE:Z

    .line 43
    const-string v0, "persist.sys.spc.extra_free_kbytes"

    const v2, 0xb13f

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/server/am/ProcessListStubImpl;->DEF_MIN_EXTRA_FREE_KB:I

    .line 45
    const-string v0, "persist.sys.spc.mi_extra_free_game_only"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/am/ProcessListStubImpl;->ENABLE_MI_SIZE_EXTRA_FREE_GAME_ONLY:Z

    .line 47
    const-string v0, "persist.sys.spc.mi_extra_free_enable"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/am/ProcessListStubImpl;->ENABLE_MI_SIZE_EXTRA_FREE:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mPmi:Lcom/miui/server/process/ProcessManagerInternal;

    .line 36
    iput-object v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mPeriodicCleaner:Lcom/android/server/am/PeriodicCleanerInternalStub;

    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mLastDisplayWidth:I

    .line 38
    iput v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mLastDisplayHeight:I

    .line 39
    iput-boolean v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mGameMode:Z

    .line 55
    iput v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mSystemRenderThreadTid:I

    return-void
.end method

.method public static getInstance()Lcom/android/server/am/ProcessListStubImpl;
    .locals 1

    .line 58
    const-class v0, Lcom/android/server/am/ProcessListStub;

    invoke-static {v0}, Lcom/miui/base/MiuiStubUtil;->getImpl(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/ProcessListStubImpl;

    return-object v0
.end method

.method public static getProcState(I)C
    .locals 4
    .param p0, "pid"    # I

    .line 126
    :try_start_0
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/FileReader;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/proc/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/stat"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    .local v0, "bufferedReader":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    .line 129
    .local v1, "stat":Ljava/lang/String;
    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x2

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 130
    :try_start_2
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 129
    return v2

    .line 126
    .end local v1    # "stat":Ljava/lang/String;
    :catchall_0
    move-exception v1

    :try_start_3
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v2

    :try_start_4
    invoke-virtual {v1, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "pid":I
    :goto_0
    throw v1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 130
    .end local v0    # "bufferedReader":Ljava/io/BufferedReader;
    .restart local p0    # "pid":I
    :catch_0
    move-exception v0

    .line 131
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "fail to get proc state of pid "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ProcessListStubImpl"

    invoke-static {v2, v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 132
    const/4 v1, 0x0

    return v1
.end method


# virtual methods
.method public computeExtraFreeKbytes(JI)I
    .locals 2
    .param p1, "totalMemMb"    # J
    .param p3, "oldReserve"    # I

    .line 203
    sget-boolean v0, Lcom/android/server/am/ProcessListStubImpl;->ENABLE_MI_EXTRA_FREE:Z

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x1800

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 204
    sget v0, Lcom/android/server/am/ProcessListStubImpl;->DEF_MIN_EXTRA_FREE_KB:I

    invoke-static {v0, p3}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0

    .line 206
    :cond_0
    return p3
.end method

.method public dumpWhenAppStartTimeout(Lcom/android/server/am/ProcessRecord;)V
    .locals 8
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 101
    invoke-static {}, Lmiui/mqsas/scout/ScoutUtils;->isLibraryTest()Z

    move-result v0

    if-nez v0, :cond_0

    .line 102
    return-void

    .line 104
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "pid "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lcom/android/server/am/ProcessRecord;->mPid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " stated timeout and its state is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lcom/android/server/am/ProcessRecord;->mPid:I

    .line 105
    invoke-static {v1}, Lcom/android/server/am/ProcessListStubImpl;->getProcState(I)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 104
    const-string v1, "ProcessListStubImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/data/miuilog/stability/scout/app/app_start_timeout_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "_"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v4, p1, Lcom/android/server/am/ProcessRecord;->mPid:I

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ".trace"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 108
    .local v0, "dumpFile":Ljava/lang/String;
    iget v4, p1, Lcom/android/server/am/ProcessRecord;->mPid:I

    const/4 v5, 0x1

    invoke-static {v4, v0, v5}, Landroid/os/Debug;->dumpJavaBacktraceToFileTimeout(ILjava/lang/String;I)Z

    move-result v4

    const-string v6, " because of app started timeout"

    if-eqz v4, :cond_1

    .line 109
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "succeed to dump java trace to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 112
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "fail to dump java trace for "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v7, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " when it started timeout, try to dump native trace"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/android/server/am/ProcessRecord;->mPid:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".native.trace"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 116
    iget v2, p1, Lcom/android/server/am/ProcessRecord;->mPid:I

    invoke-static {v2, v0, v5}, Landroid/os/Debug;->dumpNativeBacktraceToFileTimeout(ILjava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 117
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "succeed to dump native trace to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 120
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "fail to dump native trace for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    :goto_0
    return-void
.end method

.method public getLastDisplayHeight()I
    .locals 1

    .line 269
    iget v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mLastDisplayHeight:I

    return v0
.end method

.method public getLastDisplayWidth()I
    .locals 1

    .line 274
    iget v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mLastDisplayWidth:I

    return v0
.end method

.method public getSystemRenderThreadTid()I
    .locals 1

    .line 168
    iget v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mSystemRenderThreadTid:I

    return v0
.end method

.method public hookAppZygoteStart(Landroid/content/pm/ApplicationInfo;)V
    .locals 2
    .param p1, "info"    # Landroid/content/pm/ApplicationInfo;

    .line 246
    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerInternal;->getInstance()Lcom/miui/server/greeze/GreezeManagerInternal;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/miui/server/greeze/GreezeManagerInternal;->handleAppZygoteStart(Landroid/content/pm/ApplicationInfo;Z)V

    .line 247
    return-void
.end method

.method public isAllowRestartProcessLock(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Lcom/android/server/am/HostingRecord;)Z
    .locals 8
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "flag"    # I
    .param p3, "uid"    # I
    .param p4, "pkgName"    # Ljava/lang/String;
    .param p5, "callerPackage"    # Ljava/lang/String;
    .param p6, "hostingRecord"    # Lcom/android/server/am/HostingRecord;

    .line 179
    iget-object v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mPmi:Lcom/miui/server/process/ProcessManagerInternal;

    if-nez v0, :cond_0

    .line 180
    const-class v0, Lcom/miui/server/process/ProcessManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/process/ProcessManagerInternal;

    iput-object v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mPmi:Lcom/miui/server/process/ProcessManagerInternal;

    .line 181
    if-nez v0, :cond_0

    .line 182
    const/4 v0, 0x1

    return v0

    .line 185
    :cond_0
    iget-object v1, p0, Lcom/android/server/am/ProcessListStubImpl;->mPmi:Lcom/miui/server/process/ProcessManagerInternal;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-virtual/range {v1 .. v7}, Lcom/miui/server/process/ProcessManagerInternal;->isAllowRestartProcessLock(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Lcom/android/server/am/HostingRecord;)Z

    move-result v0

    return v0
.end method

.method public isGameMode()Z
    .locals 1

    .line 279
    iget-boolean v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mGameMode:Z

    return v0
.end method

.method public isNeedTraceProcess(Lcom/android/server/am/ProcessRecord;)Z
    .locals 1
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 173
    invoke-static {}, Lcom/android/server/am/MiuiProcessPolicyManager;->getInstance()Lcom/android/server/am/MiuiProcessPolicyManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/am/MiuiProcessPolicyManager;->isNeedTraceProcess(Lcom/android/server/am/ProcessRecord;)Z

    move-result v0

    return v0
.end method

.method public needSetMiSizeExtraFree()Z
    .locals 1

    .line 251
    sget-boolean v0, Lcom/android/server/am/ProcessListStubImpl;->ENABLE_MI_SIZE_EXTRA_FREE:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/android/server/am/ProcessListStubImpl;->ENABLE_MI_SIZE_EXTRA_FREE_GAME_ONLY:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mGameMode:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public needSetMiSizeExtraFreeForGameMode(Z)Z
    .locals 1
    .param p1, "gameMode"    # Z

    .line 257
    iput-boolean p1, p0, Lcom/android/server/am/ProcessListStubImpl;->mGameMode:Z

    .line 258
    sget-boolean v0, Lcom/android/server/am/ProcessListStubImpl;->ENABLE_MI_SIZE_EXTRA_FREE:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/server/am/ProcessListStubImpl;->ENABLE_MI_SIZE_EXTRA_FREE_GAME_ONLY:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public notifyAmsProcessKill(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
    .locals 1
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "reason"    # Ljava/lang/String;

    .line 91
    iget-object v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mPmi:Lcom/miui/server/process/ProcessManagerInternal;

    if-nez v0, :cond_0

    .line 92
    const-class v0, Lcom/miui/server/process/ProcessManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/process/ProcessManagerInternal;

    iput-object v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mPmi:Lcom/miui/server/process/ProcessManagerInternal;

    .line 93
    if-nez v0, :cond_0

    .line 94
    return-void

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mPmi:Lcom/miui/server/process/ProcessManagerInternal;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/process/ProcessManagerInternal;->notifyAmsProcessKill(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V

    .line 98
    return-void
.end method

.method public notifyLmkProcessKill(IIJIIIILjava/lang/String;)V
    .locals 12
    .param p1, "uid"    # I
    .param p2, "oomScore"    # I
    .param p3, "rssInBytes"    # J
    .param p5, "freeMemKb"    # I
    .param p6, "freeSwapKb"    # I
    .param p7, "killReason"    # I
    .param p8, "thrashing"    # I
    .param p9, "processName"    # Ljava/lang/String;

    .line 140
    move-object v0, p0

    iget-object v1, v0, Lcom/android/server/am/ProcessListStubImpl;->mPmi:Lcom/miui/server/process/ProcessManagerInternal;

    if-nez v1, :cond_0

    .line 141
    const-class v1, Lcom/miui/server/process/ProcessManagerInternal;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/process/ProcessManagerInternal;

    iput-object v1, v0, Lcom/android/server/am/ProcessListStubImpl;->mPmi:Lcom/miui/server/process/ProcessManagerInternal;

    .line 142
    if-nez v1, :cond_0

    .line 143
    return-void

    .line 146
    :cond_0
    iget-object v2, v0, Lcom/android/server/am/ProcessListStubImpl;->mPmi:Lcom/miui/server/process/ProcessManagerInternal;

    move v3, p1

    move v4, p2

    move-wide v5, p3

    move/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p8

    move-object/from16 v11, p9

    invoke-virtual/range {v2 .. v11}, Lcom/miui/server/process/ProcessManagerInternal;->notifyLmkProcessKill(IIJIIIILjava/lang/String;)V

    .line 148
    return-void
.end method

.method public notifyProcessDied(Lcom/android/server/am/ProcessRecord;)V
    .locals 1
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 152
    iget-object v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mPmi:Lcom/miui/server/process/ProcessManagerInternal;

    if-nez v0, :cond_0

    .line 153
    const-class v0, Lcom/miui/server/process/ProcessManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/process/ProcessManagerInternal;

    iput-object v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mPmi:Lcom/miui/server/process/ProcessManagerInternal;

    .line 154
    if-nez v0, :cond_0

    .line 155
    return-void

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mPmi:Lcom/miui/server/process/ProcessManagerInternal;

    invoke-virtual {v0, p1}, Lcom/miui/server/process/ProcessManagerInternal;->notifyProcessDied(Lcom/android/server/am/ProcessRecord;)V

    .line 159
    return-void
.end method

.method public notifyProcessStarted(Lcom/android/server/am/ProcessRecord;I)V
    .locals 4
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "pid"    # I

    .line 63
    invoke-static {}, Lcom/android/server/am/MiuiProcessPolicyManager;->getInstance()Lcom/android/server/am/MiuiProcessPolicyManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/am/MiuiProcessPolicyManager;->promoteImportantProcAdj(Lcom/android/server/am/ProcessRecord;)V

    .line 64
    invoke-static {}, Lcom/android/server/am/ProcessProphetStub;->getInstance()Lcom/android/server/am/ProcessProphetStub;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/android/server/am/ProcessProphetStub;->reportProcStarted(Lcom/android/server/am/ProcessRecord;I)V

    .line 66
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    filled-new-array {v0, v1, v2, v3}, [Ljava/lang/Object;

    move-result-object v0

    const-string v1, "notifyProcessStarted"

    invoke-static {v1, v0}, Lcom/android/server/camera/CameraOpt;->callMethod(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    iget-object v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mPmi:Lcom/miui/server/process/ProcessManagerInternal;

    if-nez v0, :cond_0

    .line 70
    const-class v0, Lcom/miui/server/process/ProcessManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/process/ProcessManagerInternal;

    iput-object v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mPmi:Lcom/miui/server/process/ProcessManagerInternal;

    .line 71
    if-nez v0, :cond_0

    .line 72
    return-void

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mPmi:Lcom/miui/server/process/ProcessManagerInternal;

    invoke-virtual {v0, p1}, Lcom/miui/server/process/ProcessManagerInternal;->notifyProcessStarted(Lcom/android/server/am/ProcessRecord;)V

    .line 76
    invoke-static {}, Lcom/android/server/net/NetworkManagementServiceStub;->getInstance()Lcom/android/server/net/NetworkManagementServiceStub;

    move-result-object v0

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget v2, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    invoke-interface {v0, v1, p2, v2}, Lcom/android/server/net/NetworkManagementServiceStub;->setPidForPackage(Ljava/lang/String;II)V

    .line 79
    iget-object v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mPeriodicCleaner:Lcom/android/server/am/PeriodicCleanerInternalStub;

    if-nez v0, :cond_1

    .line 80
    const-class v0, Lcom/android/server/am/PeriodicCleanerInternalStub;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/PeriodicCleanerInternalStub;

    iput-object v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mPeriodicCleaner:Lcom/android/server/am/PeriodicCleanerInternalStub;

    .line 81
    if-nez v0, :cond_1

    .line 82
    return-void

    .line 85
    :cond_1
    iget-object v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mPeriodicCleaner:Lcom/android/server/am/PeriodicCleanerInternalStub;

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getHostingRecord()Lcom/android/server/am/HostingRecord;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/am/HostingRecord;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/android/server/am/PeriodicCleanerInternalStub;->reportStartProcess(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    return-void
.end method

.method public restartDiedAppOrNot(Lcom/android/server/am/ProcessRecord;ZZZ)Z
    .locals 1
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "isHomeApp"    # Z
    .param p3, "allowRestart"    # Z
    .param p4, "fromBinderDied"    # Z

    .line 192
    iget-object v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mPmi:Lcom/miui/server/process/ProcessManagerInternal;

    if-nez v0, :cond_0

    .line 193
    const-class v0, Lcom/miui/server/process/ProcessManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/process/ProcessManagerInternal;

    iput-object v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mPmi:Lcom/miui/server/process/ProcessManagerInternal;

    .line 194
    if-nez v0, :cond_0

    .line 195
    return p3

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mPmi:Lcom/miui/server/process/ProcessManagerInternal;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/process/ProcessManagerInternal;->restartDiedAppOrNot(Lcom/android/server/am/ProcessRecord;ZZZ)Z

    move-result v0

    return v0
.end method

.method public setLastDisplayWidthAndHeight(II)V
    .locals 0
    .param p1, "lastDisplayWidth"    # I
    .param p2, "lastDisplayHeight"    # I

    .line 263
    iput p1, p0, Lcom/android/server/am/ProcessListStubImpl;->mLastDisplayWidth:I

    .line 264
    iput p2, p0, Lcom/android/server/am/ProcessListStubImpl;->mLastDisplayHeight:I

    .line 265
    return-void
.end method

.method public setSystemRenderThreadTidLocked(I)V
    .locals 0
    .param p1, "tid"    # I

    .line 164
    iput p1, p0, Lcom/android/server/am/ProcessListStubImpl;->mSystemRenderThreadTid:I

    .line 165
    return-void
.end method

.method public writePerfEvent(ILjava/lang/String;III)V
    .locals 5
    .param p1, "uid"    # I
    .param p2, "procName"    # Ljava/lang/String;
    .param p3, "minOomScore"    # I
    .param p4, "oomScore"    # I
    .param p5, "killReason"    # I

    .line 212
    new-instance v0, Ljava/io/File;

    const-string v1, "/dev/mi_exception_log"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 213
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 214
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 216
    .local v1, "perfjson":Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "EventID"

    const v3, 0x35c37939

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 217
    const-string v2, "ProcessName"

    invoke-virtual {v1, v2, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 218
    const-string v2, "UID"

    invoke-virtual {v1, v2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 219
    const-string v2, "MinOomAdj"

    invoke-virtual {v1, v2, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 220
    const-string v2, "OomAdj"

    invoke-virtual {v1, v2, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 221
    const-string v2, "Reason"

    invoke-virtual {v1, v2, p5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 224
    goto :goto_0

    .line 222
    :catch_0
    move-exception v2

    .line 223
    .local v2, "e":Lorg/json/JSONException;
    const-string v3, "ProcessListStubImpl"

    const-string v4, "error put event"

    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    .end local v2    # "e":Lorg/json/JSONException;
    :goto_0
    const/4 v2, 0x0

    .line 227
    .local v2, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    move-object v2, v3

    .line 228
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/FileOutputStream;->write([B)V

    .line 229
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 233
    nop

    .line 235
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 233
    :catchall_0
    move-exception v3

    goto :goto_2

    .line 230
    :catch_1
    move-exception v3

    .line 231
    .local v3, "e":Ljava/io/IOException;
    :try_start_3
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 233
    .end local v3    # "e":Ljava/io/IOException;
    if-eqz v2, :cond_1

    .line 235
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 238
    :goto_1
    goto :goto_4

    .line 236
    :catch_2
    move-exception v3

    .line 237
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    .end local v3    # "e":Ljava/io/IOException;
    goto :goto_1

    .line 233
    :goto_2
    if-eqz v2, :cond_0

    .line 235
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 238
    goto :goto_3

    .line 236
    :catch_3
    move-exception v4

    .line 237
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    .line 240
    .end local v4    # "e":Ljava/io/IOException;
    :cond_0
    :goto_3
    throw v3

    .line 242
    .end local v1    # "perfjson":Lorg/json/JSONObject;
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    :cond_1
    :goto_4
    return-void
.end method
