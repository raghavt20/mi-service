.class Lcom/android/server/am/AppProfilerImpl$1;
.super Ljava/lang/Object;
.source "AppProfilerImpl.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/AppProfilerImpl;->dumpCpuInfo(Lcom/android/server/utils/PriorityDump$PriorityDumper;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/AppProfilerImpl;

.field final synthetic val$args:[Ljava/lang/String;

.field final synthetic val$dumper:Lcom/android/server/utils/PriorityDump$PriorityDumper;

.field final synthetic val$fd:Ljava/io/FileDescriptor;

.field final synthetic val$pw:Ljava/io/PrintWriter;


# direct methods
.method constructor <init>(Lcom/android/server/am/AppProfilerImpl;Lcom/android/server/utils/PriorityDump$PriorityDumper;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/AppProfilerImpl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 445
    iput-object p1, p0, Lcom/android/server/am/AppProfilerImpl$1;->this$0:Lcom/android/server/am/AppProfilerImpl;

    iput-object p2, p0, Lcom/android/server/am/AppProfilerImpl$1;->val$dumper:Lcom/android/server/utils/PriorityDump$PriorityDumper;

    iput-object p3, p0, Lcom/android/server/am/AppProfilerImpl$1;->val$fd:Ljava/io/FileDescriptor;

    iput-object p4, p0, Lcom/android/server/am/AppProfilerImpl$1;->val$pw:Ljava/io/PrintWriter;

    iput-object p5, p0, Lcom/android/server/am/AppProfilerImpl$1;->val$args:[Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 445
    invoke-virtual {p0}, Lcom/android/server/am/AppProfilerImpl$1;->call()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/lang/Void;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 448
    iget-object v0, p0, Lcom/android/server/am/AppProfilerImpl$1;->val$dumper:Lcom/android/server/utils/PriorityDump$PriorityDumper;

    iget-object v1, p0, Lcom/android/server/am/AppProfilerImpl$1;->val$fd:Ljava/io/FileDescriptor;

    iget-object v2, p0, Lcom/android/server/am/AppProfilerImpl$1;->val$pw:Ljava/io/PrintWriter;

    iget-object v3, p0, Lcom/android/server/am/AppProfilerImpl$1;->val$args:[Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/android/server/utils/PriorityDump;->dump(Lcom/android/server/utils/PriorityDump$PriorityDumper;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 449
    const/4 v0, 0x0

    return-object v0
.end method
