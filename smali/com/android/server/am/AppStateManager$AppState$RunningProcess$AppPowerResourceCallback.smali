.class Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;
.super Ljava/lang/Object;
.source "AppStateManager.java"

# interfaces
.implements Lcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AppPowerResourceCallback"
.end annotation


# instance fields
.field final synthetic this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;


# direct methods
.method private constructor <init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V
    .locals 0

    .line 2944
    iput-object p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;-><init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V

    return-void
.end method


# virtual methods
.method public noteResourceActive(II)V
    .locals 3
    .param p1, "type"    # I
    .param p2, "behavier"    # I

    .line 2948
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "resource active  uid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmUid(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " pid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmPid(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 2950
    invoke-static {p1}, Lcom/miui/server/smartpower/AppPowerResourceManager;->typeToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2951
    .local v0, "reason":Ljava/lang/String;
    sget-boolean v1, Lcom/android/server/am/AppStateManager;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 2952
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetLOG_TAG(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2954
    :cond_0
    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    .line 2955
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v2, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fputmAudioActive(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Z)V

    .line 2956
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1, p2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fputmAudioBehavier(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;I)V

    goto :goto_0

    .line 2957
    :cond_1
    const/4 v2, 0x3

    if-ne p1, v2, :cond_2

    .line 2958
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v2, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fputmGpsActive(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Z)V

    .line 2959
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1, p2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fputmGpsBehavier(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;I)V

    goto :goto_0

    .line 2960
    :cond_2
    const/4 v2, 0x2

    if-ne p1, v2, :cond_3

    .line 2961
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v2, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fputmNetActive(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Z)V

    .line 2962
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1, p2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fputmNetBehavier(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;I)V

    goto :goto_0

    .line 2963
    :cond_3
    const/4 v2, 0x5

    if-ne p1, v2, :cond_4

    .line 2964
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v2, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fputmBleActive(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Z)V

    goto :goto_0

    .line 2965
    :cond_4
    const/4 v2, 0x6

    if-ne p1, v2, :cond_5

    .line 2966
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v2, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fputmCameraActive(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Z)V

    .line 2967
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1, p2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fputmCamBehavier(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;I)V

    goto :goto_0

    .line 2968
    :cond_5
    const/4 v2, 0x7

    if-ne p1, v2, :cond_6

    .line 2969
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v2, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fputmDisplayActive(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Z)V

    .line 2970
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1, p2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fputmDisplayBehavier(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;I)V

    .line 2972
    :cond_6
    :goto_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    iget-object v1, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$mupdateCurrentResourceBehavier(Lcom/android/server/am/AppStateManager$AppState;)V

    .line 2973
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1, v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$mbecomeActiveIfNeeded(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V

    .line 2974
    return-void
.end method

.method public noteResourceInactive(II)V
    .locals 4
    .param p1, "type"    # I
    .param p2, "behavier"    # I

    .line 2978
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "resource inactive  uid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmUid(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " pid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmPid(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 2980
    invoke-static {p1}, Lcom/miui/server/smartpower/AppPowerResourceManager;->typeToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2981
    .local v0, "reason":Ljava/lang/String;
    sget-boolean v1, Lcom/android/server/am/AppStateManager;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 2982
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetLOG_TAG(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2984
    :cond_0
    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p1, v1, :cond_1

    .line 2985
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1, v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fputmAudioActive(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Z)V

    .line 2986
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1, v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fputmAudioBehavier(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;I)V

    goto :goto_0

    .line 2987
    :cond_1
    const/4 v1, 0x3

    if-ne p1, v1, :cond_2

    .line 2988
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1, v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fputmGpsActive(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Z)V

    .line 2989
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1, v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fputmGpsBehavier(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;I)V

    goto :goto_0

    .line 2990
    :cond_2
    const/4 v1, 0x5

    if-ne p1, v1, :cond_3

    .line 2991
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1, v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fputmBleActive(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Z)V

    goto :goto_0

    .line 2992
    :cond_3
    const/4 v1, 0x2

    if-ne p1, v1, :cond_4

    .line 2993
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    iget-object v1, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmHandler(Lcom/android/server/am/AppStateManager$AppState;)Lcom/android/server/am/AppStateManager$AppState$MyHandler;

    move-result-object v1

    new-instance v3, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback$1;

    invoke-direct {v3, p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback$1;-><init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;)V

    invoke-virtual {v1, v3}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->post(Ljava/lang/Runnable;)Z

    .line 3001
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1, v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fputmNetActive(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Z)V

    .line 3002
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1, v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fputmNetBehavier(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;I)V

    goto :goto_0

    .line 3003
    :cond_4
    const/4 v1, 0x6

    if-ne p1, v1, :cond_5

    .line 3004
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1, v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fputmCameraActive(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Z)V

    .line 3005
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1, v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fputmCamBehavier(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;I)V

    goto :goto_0

    .line 3006
    :cond_5
    const/4 v1, 0x7

    if-ne p1, v1, :cond_6

    .line 3007
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1, v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fputmDisplayActive(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Z)V

    .line 3008
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1, v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fputmDisplayBehavier(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;I)V

    .line 3010
    :cond_6
    :goto_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    iget-object v1, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$mupdateCurrentResourceBehavier(Lcom/android/server/am/AppStateManager$AppState;)V

    .line 3011
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1, v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$msendBecomeInactiveMsg(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V

    .line 3012
    return-void
.end method
