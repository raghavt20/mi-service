.class Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback$1;
.super Ljava/lang/Object;
.source "AppStateManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->noteResourceInactive(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$3:Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;


# direct methods
.method constructor <init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;)V
    .locals 0
    .param p1, "this$3"    # Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;

    .line 2993
    iput-object p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback$1;->this$3:Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 2996
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback$1;->this$3:Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmAppPowerResourceManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/AppPowerResourceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback$1;->this$3:Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;

    iget-object v1, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmResourcesCallback(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback$1;->this$3:Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;

    iget-object v2, v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmUid(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v2

    const/4 v3, 0x2

    invoke-virtual {v0, v3, v1, v2}, Lcom/miui/server/smartpower/AppPowerResourceManager;->unRegisterCallback(ILcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;I)V

    .line 2999
    return-void
.end method
