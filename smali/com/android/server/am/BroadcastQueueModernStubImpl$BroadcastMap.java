class com.android.server.am.BroadcastQueueModernStubImpl$BroadcastMap {
	 /* .source "BroadcastQueueModernStubImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/BroadcastQueueModernStubImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "BroadcastMap" */
} // .end annotation
/* # instance fields */
private java.lang.String action;
private java.lang.String packageName;
/* # direct methods */
public com.android.server.am.BroadcastQueueModernStubImpl$BroadcastMap ( ) {
/* .locals 0 */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 144 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 145 */
this.action = p1;
/* .line 146 */
this.packageName = p2;
/* .line 147 */
return;
} // .end method
/* # virtual methods */
public Boolean equals ( java.lang.Object p0 ) {
/* .locals 3 */
/* .param p1, "obj" # Ljava/lang/Object; */
/* .line 150 */
/* instance-of v0, p1, Lcom/android/server/am/BroadcastQueueModernStubImpl$BroadcastMap; */
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 151 */
	 /* move-object v0, p1 */
	 /* check-cast v0, Lcom/android/server/am/BroadcastQueueModernStubImpl$BroadcastMap; */
	 /* .line 153 */
	 /* .local v0, "broadcastMapObj":Lcom/android/server/am/BroadcastQueueModernStubImpl$BroadcastMap; */
	 v1 = this.action;
	 v2 = this.action;
	 v1 = 	 (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 v1 = this.packageName;
		 v2 = this.packageName;
		 /* .line 154 */
		 v1 = 		 (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 int v1 = 1; // const/4 v1, 0x1
		 } // :cond_0
		 int v1 = 0; // const/4 v1, 0x0
		 /* .line 153 */
	 } // :goto_0
	 /* .line 156 */
} // .end local v0 # "broadcastMapObj":Lcom/android/server/am/BroadcastQueueModernStubImpl$BroadcastMap;
} // :cond_1
v0 = /* invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z */
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 160 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "action: "; // const-string v1, "action: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.action;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", packageName: "; // const-string v1, ", packageName: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.packageName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 161 */
/* .local v0, "result":Ljava/lang/String; */
} // .end method
