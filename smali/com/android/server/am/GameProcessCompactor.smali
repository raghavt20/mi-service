.class public Lcom/android/server/am/GameProcessCompactor;
.super Ljava/lang/Object;
.source "GameProcessCompactor.java"

# interfaces
.implements Lcom/android/server/am/IGameProcessAction;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;,
        Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = true

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mConfig:Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;

.field private mReclaimer:Lcom/android/server/am/GameMemoryReclaimer;


# direct methods
.method static bridge synthetic -$$Nest$sfgetTAG()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/server/am/GameProcessCompactor;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 31
    const-class v0, Lcom/android/server/am/GameProcessCompactor;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/am/GameProcessCompactor;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/android/server/am/GameMemoryReclaimer;Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;)V
    .locals 0
    .param p1, "reclaimer"    # Lcom/android/server/am/GameMemoryReclaimer;
    .param p2, "cfg"    # Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/android/server/am/GameProcessCompactor;->mReclaimer:Lcom/android/server/am/GameMemoryReclaimer;

    .line 39
    iput-object p2, p0, Lcom/android/server/am/GameProcessCompactor;->mConfig:Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;

    .line 40
    return-void
.end method


# virtual methods
.method public doAction(J)J
    .locals 15
    .param p1, "need"    # J

    .line 72
    move-object v0, p0

    const/4 v1, 0x0

    .line 77
    .local v1, "num":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doCompact:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/android/server/am/GameProcessCompactor;->mConfig:Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;

    iget v3, v3, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mPrio:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-wide/32 v3, 0x80000

    invoke-static {v3, v4, v2}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 78
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    .line 79
    .local v5, "time":J
    const-wide/16 v7, 0x0

    .line 80
    .local v7, "reclaim":J
    iget-object v2, v0, Lcom/android/server/am/GameProcessCompactor;->mReclaimer:Lcom/android/server/am/GameMemoryReclaimer;

    invoke-virtual {v2, p0}, Lcom/android/server/am/GameMemoryReclaimer;->filterProcessInfos(Lcom/android/server/am/IGameProcessAction;)Ljava/util/List;

    move-result-object v2

    .line 81
    .local v2, "pidList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;>;"
    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_1

    .line 82
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;

    .line 83
    .local v10, "compInfo":Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;
    invoke-virtual {v10, p0}, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->compact(Lcom/android/server/am/GameProcessCompactor;)J

    move-result-wide v11

    .line 84
    .local v11, "income":J
    const-wide/16 v13, 0x0

    cmp-long v13, v11, v13

    if-ltz v13, :cond_0

    .line 85
    add-int/lit8 v1, v1, 0x1

    .line 86
    add-long/2addr v7, v11

    .line 87
    cmp-long v13, v7, p1

    if-gez v13, :cond_1

    iget-object v13, v0, Lcom/android/server/am/GameProcessCompactor;->mConfig:Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;

    iget v13, v13, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mCompactMaxNum:I

    if-lez v13, :cond_0

    iget-object v13, v0, Lcom/android/server/am/GameProcessCompactor;->mConfig:Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;

    iget v13, v13, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mCompactMaxNum:I

    if-lt v1, v13, :cond_0

    .line 89
    goto :goto_1

    .line 91
    .end local v10    # "compInfo":Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;
    :cond_0
    goto :goto_0

    .line 93
    .end local v11    # "income":J
    :cond_1
    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v9

    sub-long/2addr v9, v5

    .line 94
    .end local v5    # "time":J
    .local v9, "time":J
    sget-object v5, Lcom/android/server/am/GameProcessCompactor;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "compact "

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v11, " processes, and reclaim mem: "

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v11, ", during "

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v11, "ms"

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    invoke-static {v3, v4}, Landroid/os/Trace;->traceEnd(J)V

    .line 96
    return-wide v7
.end method

.method getIncomePctThreshold()J
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/android/server/am/GameProcessCompactor;->mConfig:Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;

    iget v0, v0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mCompactIncomePctThreshold:I

    int-to-long v0, v0

    return-wide v0
.end method

.method getIntervalThreshold()J
    .locals 2

    .line 100
    iget-object v0, p0, Lcom/android/server/am/GameProcessCompactor;->mConfig:Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;

    iget v0, v0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mCompactIntervalThreshold:I

    int-to-long v0, v0

    return-wide v0
.end method

.method getPrio()I
    .locals 1

    .line 108
    iget-object v0, p0, Lcom/android/server/am/GameProcessCompactor;->mConfig:Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;

    iget v0, v0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mPrio:I

    return v0
.end method

.method public shouldSkip(Lcom/android/server/am/ProcessRecord;)Z
    .locals 4
    .param p1, "proc"    # Lcom/android/server/am/ProcessRecord;

    .line 44
    iget-object v0, p0, Lcom/android/server/am/GameProcessCompactor;->mConfig:Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;

    iget-boolean v0, v0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mSkipActive:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 45
    invoke-static {}, Lcom/miui/server/process/ProcessManagerInternal;->getInstance()Lcom/miui/server/process/ProcessManagerInternal;

    move-result-object v0

    .line 46
    invoke-virtual {v0}, Lcom/miui/server/process/ProcessManagerInternal;->getProcessPolicy()Lcom/android/server/am/IProcessPolicy;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/android/server/am/IProcessPolicy;->getActiveUidList(I)Ljava/util/List;

    move-result-object v0

    iget v2, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    return v1

    .line 51
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/miui/server/process/ProcessManagerInternal;->getInstance()Lcom/miui/server/process/ProcessManagerInternal;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/process/ProcessManagerInternal;->getForegroundInfo()Lmiui/process/ForegroundInfo;

    move-result-object v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    .local v0, "foregroundInfo":Lmiui/process/ForegroundInfo;
    goto :goto_0

    .line 52
    .end local v0    # "foregroundInfo":Lmiui/process/ForegroundInfo;
    :catch_0
    move-exception v0

    .line 53
    .local v0, "ex":Landroid/os/RemoteException;
    const/4 v2, 0x0

    move-object v0, v2

    .line 55
    .local v0, "foregroundInfo":Lmiui/process/ForegroundInfo;
    :goto_0
    if-nez v0, :cond_1

    .line 56
    return v1

    .line 58
    :cond_1
    iget v2, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    iget v3, v0, Lmiui/process/ForegroundInfo;->mForegroundUid:I

    if-eq v2, v3, :cond_8

    iget v2, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    iget v3, v0, Lmiui/process/ForegroundInfo;->mMultiWindowForegroundUid:I

    if-ne v2, v3, :cond_2

    goto :goto_3

    .line 61
    :cond_2
    iget-object v2, p0, Lcom/android/server/am/GameProcessCompactor;->mConfig:Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;

    iget-object v2, v2, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mWhiteList:Ljava/util/List;

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/android/server/am/GameProcessCompactor;->mConfig:Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;

    iget-object v2, v2, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mWhiteList:Ljava/util/List;

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    .line 63
    :cond_3
    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v2}, Lcom/android/server/am/ProcessStateRecord;->getSetAdj()I

    move-result v2

    iget-object v3, p0, Lcom/android/server/am/GameProcessCompactor;->mConfig:Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;

    iget v3, v3, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mMinAdj:I

    if-le v2, v3, :cond_6

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v2}, Lcom/android/server/am/ProcessStateRecord;->getSetAdj()I

    move-result v2

    iget-object v3, p0, Lcom/android/server/am/GameProcessCompactor;->mConfig:Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;

    iget v3, v3, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mMaxAdj:I

    if-le v2, v3, :cond_4

    goto :goto_1

    .line 65
    :cond_4
    iget-object v2, p0, Lcom/android/server/am/GameProcessCompactor;->mConfig:Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;

    iget-boolean v2, v2, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mSkipForeground:Z

    if-eqz v2, :cond_5

    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getWindowProcessController()Lcom/android/server/wm/WindowProcessController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/wm/WindowProcessController;->isInterestingToUser()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 66
    return v1

    .line 67
    :cond_5
    const/4 v1, 0x0

    return v1

    .line 64
    :cond_6
    :goto_1
    return v1

    .line 62
    :cond_7
    :goto_2
    return v1

    .line 60
    :cond_8
    :goto_3
    return v1
.end method
