.class Lcom/android/server/am/ProcessPowerCleaner$ScreenStatusReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ProcessPowerCleaner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ProcessPowerCleaner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScreenStatusReceiver"
.end annotation


# instance fields
.field private SCREEN_OFF:Ljava/lang/String;

.field private SCREEN_ON:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/server/am/ProcessPowerCleaner;


# direct methods
.method private constructor <init>(Lcom/android/server/am/ProcessPowerCleaner;)V
    .locals 0

    .line 375
    iput-object p1, p0, Lcom/android/server/am/ProcessPowerCleaner$ScreenStatusReceiver;->this$0:Lcom/android/server/am/ProcessPowerCleaner;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 377
    const-string p1, "android.intent.action.SCREEN_ON"

    iput-object p1, p0, Lcom/android/server/am/ProcessPowerCleaner$ScreenStatusReceiver;->SCREEN_ON:Ljava/lang/String;

    .line 378
    const-string p1, "android.intent.action.SCREEN_OFF"

    iput-object p1, p0, Lcom/android/server/am/ProcessPowerCleaner$ScreenStatusReceiver;->SCREEN_OFF:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/am/ProcessPowerCleaner;Lcom/android/server/am/ProcessPowerCleaner$ScreenStatusReceiver-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessPowerCleaner$ScreenStatusReceiver;-><init>(Lcom/android/server/am/ProcessPowerCleaner;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 382
    iget-object v0, p0, Lcom/android/server/am/ProcessPowerCleaner$ScreenStatusReceiver;->SCREEN_ON:Ljava/lang/String;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 383
    iget-object v0, p0, Lcom/android/server/am/ProcessPowerCleaner$ScreenStatusReceiver;->this$0:Lcom/android/server/am/ProcessPowerCleaner;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/am/ProcessPowerCleaner;->-$$Nest$fputmIsScreenOffState(Lcom/android/server/am/ProcessPowerCleaner;Z)V

    .line 384
    iget-object v0, p0, Lcom/android/server/am/ProcessPowerCleaner$ScreenStatusReceiver;->this$0:Lcom/android/server/am/ProcessPowerCleaner;

    invoke-static {v0}, Lcom/android/server/am/ProcessPowerCleaner;->-$$Nest$fgetmHandler(Lcom/android/server/am/ProcessPowerCleaner;)Lcom/android/server/am/ProcessPowerCleaner$H;

    move-result-object v0

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessPowerCleaner$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 385
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/am/ProcessPowerCleaner$ScreenStatusReceiver;->this$0:Lcom/android/server/am/ProcessPowerCleaner;

    invoke-static {v1}, Lcom/android/server/am/ProcessPowerCleaner;->-$$Nest$fgetmHandler(Lcom/android/server/am/ProcessPowerCleaner;)Lcom/android/server/am/ProcessPowerCleaner$H;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/am/ProcessPowerCleaner$H;->sendMessage(Landroid/os/Message;)Z

    .line 386
    .end local v0    # "msg":Landroid/os/Message;
    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ProcessPowerCleaner$ScreenStatusReceiver;->SCREEN_OFF:Ljava/lang/String;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/am/ProcessPowerCleaner$ScreenStatusReceiver;->this$0:Lcom/android/server/am/ProcessPowerCleaner;

    invoke-static {v0}, Lcom/android/server/am/ProcessPowerCleaner;->-$$Nest$fgetmHandler(Lcom/android/server/am/ProcessPowerCleaner;)Lcom/android/server/am/ProcessPowerCleaner$H;

    move-result-object v0

    .line 387
    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessPowerCleaner$H;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 388
    iget-object v0, p0, Lcom/android/server/am/ProcessPowerCleaner$ScreenStatusReceiver;->this$0:Lcom/android/server/am/ProcessPowerCleaner;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/android/server/am/ProcessPowerCleaner;->-$$Nest$fputmIsScreenOffState(Lcom/android/server/am/ProcessPowerCleaner;Z)V

    .line 389
    iget-object v0, p0, Lcom/android/server/am/ProcessPowerCleaner$ScreenStatusReceiver;->this$0:Lcom/android/server/am/ProcessPowerCleaner;

    invoke-static {v0}, Lcom/android/server/am/ProcessPowerCleaner;->-$$Nest$fgetmHandler(Lcom/android/server/am/ProcessPowerCleaner;)Lcom/android/server/am/ProcessPowerCleaner$H;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessPowerCleaner$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 390
    .restart local v0    # "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/am/ProcessPowerCleaner$ScreenStatusReceiver;->this$0:Lcom/android/server/am/ProcessPowerCleaner;

    invoke-static {v1}, Lcom/android/server/am/ProcessPowerCleaner;->-$$Nest$fgetmHandler(Lcom/android/server/am/ProcessPowerCleaner;)Lcom/android/server/am/ProcessPowerCleaner$H;

    move-result-object v1

    const-wide/32 v2, 0x493e0

    invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/am/ProcessPowerCleaner$H;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 392
    .end local v0    # "msg":Landroid/os/Message;
    :cond_1
    :goto_0
    return-void
.end method
