public class com.android.server.am.SmartPowerService extends miui.smartpower.ISmartPowerManager$Stub implements com.android.server.am.SmartPowerServiceStub implements com.miui.app.smartpower.SmartPowerServiceInternal {
	 /* .source "SmartPowerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.miui.server.smartpower.SmartPowerServiceStub$$" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/android/server/am/SmartPowerService$MyHandler;, */
/* Lcom/android/server/am/SmartPowerService$SmartPowerShellCommand;, */
/* Lcom/android/server/am/SmartPowerService$Lifecycle; */
/* } */
} // .end annotation
/* # static fields */
private static final java.lang.String CLOUD_ALARM_WHITE_LIST;
private static final java.lang.String CLOUD_BROADCAST_WHITE_LIST;
private static final java.lang.String CLOUD_CONTROL_KEY;
private static final java.lang.String CLOUD_CONTROL_MOUDLE;
private static final java.lang.String CLOUD_DISPLAY_ENABLE;
private static final java.lang.String CLOUD_ENABLE;
private static final java.lang.String CLOUD_FROZEN_ENABLE;
private static final java.lang.String CLOUD_GAME_PAY_PROTECT_ENABLE;
private static final java.lang.String CLOUD_INTERCEPT_ENABLE;
private static final java.lang.String CLOUD_PKG_WHITE_LIST;
private static final java.lang.String CLOUD_PROC_WHITE_LIST;
private static final java.lang.String CLOUD_PROVIDER_WHITE_LIST;
private static final java.lang.String CLOUD_SCREENON_WHITE_LIST;
private static final java.lang.String CLOUD_SERVICE_WHITE_LIST;
private static final java.lang.String CLOUD_WINDOW_ENABLE;
public static final Boolean DEBUG;
public static final java.lang.String SERVICE_NAME;
public static final java.lang.String TAG;
public static final Integer THREAD_GROUP_FOREGROUND;
/* # instance fields */
private com.android.server.am.ActivityManagerService mAMS;
private com.android.server.wm.ActivityTaskManagerService mATMS;
private com.miui.server.smartpower.AppPowerResourceManager mAppPowerResourceManager;
private com.android.server.am.AppStateManager mAppStateManager;
private com.miui.server.smartpower.ComponentsManager mComponentsManager;
private android.content.Context mContext;
private com.android.server.am.SmartPowerService$MyHandler mHandler;
private android.os.HandlerThread mHandlerTh;
private android.content.pm.PackageManagerInternal mPackageManager;
private com.miui.server.smartpower.PowerFrozenManager mPowerFrozenManager;
private com.miui.server.smartpower.SmartBoostPolicyManager mSmartBoostPolicyManager;
private com.miui.server.smartpower.SmartCpuPolicyManager mSmartCpuPolicyManager;
private com.miui.server.smartpower.SmartDisplayPolicyManager mSmartDisplayPolicyManager;
private com.miui.server.smartpower.SmartPowerPolicyManager mSmartPowerPolicyManager;
private com.miui.server.smartpower.SmartScenarioManager mSmartScenarioManager;
private com.miui.server.smartpower.SmartThermalPolicyManager mSmartThermalPolicyManager;
private com.miui.server.smartpower.SmartWindowPolicyManager mSmartWindowPolicyManager;
private Boolean mSystemReady;
private com.android.server.wm.WindowManagerService mWMS;
/* # direct methods */
static void -$$Nest$mupdateCloudControlParas ( com.android.server.am.SmartPowerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/SmartPowerService;->updateCloudControlParas()V */
return;
} // .end method
static com.android.server.am.SmartPowerService ( ) {
/* .locals 1 */
/* .line 87 */
/* sget-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->DEBUG_ALL:Z */
com.android.server.am.SmartPowerService.DEBUG = (v0!= 0);
return;
} // .end method
 com.android.server.am.SmartPowerService ( ) {
/* .locals 3 */
/* .line 135 */
/* invoke-direct {p0}, Lmiui/smartpower/ISmartPowerManager$Stub;-><init>()V */
/* .line 111 */
int v0 = 0; // const/4 v0, 0x0
this.mPowerFrozenManager = v0;
/* .line 112 */
this.mAppStateManager = v0;
/* .line 113 */
this.mSmartPowerPolicyManager = v0;
/* .line 114 */
this.mAppPowerResourceManager = v0;
/* .line 115 */
this.mComponentsManager = v0;
/* .line 117 */
this.mSmartScenarioManager = v0;
/* .line 118 */
this.mSmartThermalPolicyManager = v0;
/* .line 119 */
this.mSmartDisplayPolicyManager = v0;
/* .line 120 */
this.mSmartWindowPolicyManager = v0;
/* .line 121 */
this.mSmartBoostPolicyManager = v0;
/* .line 130 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "SmartPowerService"; // const-string v1, "SmartPowerService"
int v2 = -2; // const/4 v2, -0x2
/* invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V */
this.mHandlerTh = v0;
/* .line 133 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
/* .line 136 */
/* const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
com.android.server.LocalServices .addService ( v0,p0 );
/* .line 137 */
return;
} // .end method
private Boolean checkPermission ( ) {
/* .locals 2 */
/* .line 219 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 220 */
/* .local v0, "callingUid":I */
/* const/16 v1, 0x2710 */
/* if-ge v0, v1, :cond_0 */
/* .line 221 */
int v1 = 1; // const/4 v1, 0x1
/* .line 223 */
} // :cond_0
v1 = this.mAppStateManager;
v1 = (( com.android.server.am.AppStateManager ) v1 ).isSystemApp ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/am/AppStateManager;->isSystemApp(I)Z
} // .end method
private void dumpConfig ( java.io.PrintWriter p0 ) {
/* .locals 2 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 1392 */
/* const-string/jumbo v0, "smartpower config:" */
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1393 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " appstate("; // const-string v1, " appstate("
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v1, Lcom/miui/app/smartpower/SmartPowerSettings;->APP_STATE_ENABLE:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ") fz("; // const-string v1, ") fz("
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v1, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_FROZEN_ENABLE:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ") intercept("; // const-string v1, ") intercept("
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v1, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_INTERCEPT_ENABLE:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ")"; // const-string v1, ")"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1396 */
final String v0 = ""; // const-string v0, ""
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1397 */
return;
} // .end method
protected static Boolean isUsbDataTransferActive ( Long p0 ) {
/* .locals 4 */
/* .param p0, "functions" # J */
/* .line 725 */
/* const-wide/16 v0, 0x4 */
/* and-long/2addr v0, p0 */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
/* if-nez v0, :cond_1 */
/* const-wide/16 v0, 0x10 */
/* and-long/2addr v0, p0 */
/* cmp-long v0, v0, v2 */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
private void logCloudControlParas ( java.lang.String p0, java.lang.Object p1 ) {
/* .locals 2 */
/* .param p1, "key" # Ljava/lang/String; */
/* .param p2, "data" # Ljava/lang/Object; */
/* .line 917 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "sync cloud control " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SmartPower"; // const-string v1, "SmartPower"
android.util.Slog .d ( v1,v0 );
/* .line 918 */
return;
} // .end method
private void registerCloudObserver ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 819 */
/* new-instance v0, Lcom/android/server/am/SmartPowerService$1; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/am/SmartPowerService$1;-><init>(Lcom/android/server/am/SmartPowerService;Landroid/os/Handler;)V */
/* .line 828 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 829 */
android.provider.MiuiSettings$SettingsCloudData .getCloudDataNotifyUri ( );
/* .line 828 */
int v3 = 0; // const/4 v3, 0x0
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0 ); // invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 831 */
return;
} // .end method
private void updateCloudControlParas ( ) {
/* .locals 19 */
/* .line 834 */
/* move-object/from16 v1, p0 */
final String v0 = "power_service_white_list"; // const-string v0, "power_service_white_list"
final String v2 = "power_provider_white_list"; // const-string v2, "power_provider_white_list"
final String v3 = "power_alarm_white_list"; // const-string v3, "power_alarm_white_list"
final String v4 = "power_broadcast_white_list"; // const-string v4, "power_broadcast_white_list"
final String v5 = "power_screenon_white_list"; // const-string v5, "power_screenon_white_list"
final String v6 = "power_proc_white_list"; // const-string v6, "power_proc_white_list"
final String v7 = "power_pkg_white_list"; // const-string v7, "power_pkg_white_list"
final String v8 = "perf_game_pay_protect_enable"; // const-string v8, "perf_game_pay_protect_enable"
final String v9 = "perf_power_window_enable"; // const-string v9, "perf_power_window_enable"
final String v10 = "perf_power_display_enable"; // const-string v10, "perf_power_display_enable"
final String v11 = "perf_power_intercept_enable"; // const-string v11, "perf_power_intercept_enable"
final String v12 = "perf_power_appState_enable"; // const-string v12, "perf_power_appState_enable"
final String v13 = "perf_power_freeze_enable"; // const-string v13, "perf_power_freeze_enable"
v14 = this.mContext;
/* .line 835 */
(( android.content.Context ) v14 ).getContentResolver ( ); // invoke-virtual {v14}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 834 */
final String v15 = "perf_smartpower"; // const-string v15, "perf_smartpower"
/* move-object/from16 v16, v0 */
final String v0 = "perf_shielder_smartpower"; // const-string v0, "perf_shielder_smartpower"
/* move-object/from16 v17, v2 */
final String v2 = ""; // const-string v2, ""
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( v14,v15,v0,v2 );
/* .line 836 */
/* .local v2, "data":Ljava/lang/String; */
v0 = android.text.TextUtils .isEmpty ( v2 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 837 */
return;
/* .line 840 */
} // :cond_0
try { // :try_start_0
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* .line 841 */
/* .local v0, "jsonObject":Lorg/json/JSONObject; */
v14 = (( org.json.JSONObject ) v0 ).has ( v13 ); // invoke-virtual {v0, v13}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v14 != null) { // if-eqz v14, :cond_1
/* .line 842 */
v14 = (( org.json.JSONObject ) v0 ).optBoolean ( v13 ); // invoke-virtual {v0, v13}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z
/* .line 843 */
/* .local v14, "frozenEnable":Z */
v15 = this.mPowerFrozenManager;
(( com.miui.server.smartpower.PowerFrozenManager ) v15 ).syncCloudControlSettings ( v14 ); // invoke-virtual {v15, v14}, Lcom/miui/server/smartpower/PowerFrozenManager;->syncCloudControlSettings(Z)V
/* .line 844 */
final String v15 = "persist.sys.smartpower.fz.enable"; // const-string v15, "persist.sys.smartpower.fz.enable"
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_1 */
/* move-object/from16 v18, v2 */
} // .end local v2 # "data":Ljava/lang/String;
/* .local v18, "data":Ljava/lang/String; */
try { // :try_start_1
java.lang.String .valueOf ( v14 );
android.os.SystemProperties .set ( v15,v2 );
/* .line 845 */
java.lang.Boolean .valueOf ( v14 );
/* invoke-direct {v1, v13, v2}, Lcom/android/server/am/SmartPowerService;->logCloudControlParas(Ljava/lang/String;Ljava/lang/Object;)V */
/* .line 841 */
} // .end local v14 # "frozenEnable":Z
} // .end local v18 # "data":Ljava/lang/String;
/* .restart local v2 # "data":Ljava/lang/String; */
} // :cond_1
/* move-object/from16 v18, v2 */
/* .line 848 */
} // .end local v2 # "data":Ljava/lang/String;
/* .restart local v18 # "data":Ljava/lang/String; */
} // :goto_0
v2 = (( org.json.JSONObject ) v0 ).has ( v12 ); // invoke-virtual {v0, v12}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 849 */
(( org.json.JSONObject ) v0 ).optString ( v12 ); // invoke-virtual {v0, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 850 */
/* .local v2, "appStateEnable":Ljava/lang/String; */
final String v13 = "persist.sys.smartpower.appstate.enable"; // const-string v13, "persist.sys.smartpower.appstate.enable"
android.os.SystemProperties .set ( v13,v2 );
/* .line 851 */
/* invoke-direct {v1, v12, v2}, Lcom/android/server/am/SmartPowerService;->logCloudControlParas(Ljava/lang/String;Ljava/lang/Object;)V */
/* .line 854 */
} // .end local v2 # "appStateEnable":Ljava/lang/String;
} // :cond_2
v2 = (( org.json.JSONObject ) v0 ).has ( v11 ); // invoke-virtual {v0, v11}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 855 */
(( org.json.JSONObject ) v0 ).optString ( v11 ); // invoke-virtual {v0, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 856 */
/* .local v2, "interceptEnable":Ljava/lang/String; */
final String v12 = "persist.sys.smartpower.intercept.enable"; // const-string v12, "persist.sys.smartpower.intercept.enable"
android.os.SystemProperties .set ( v12,v2 );
/* .line 857 */
v12 = java.lang.Boolean .parseBoolean ( v2 );
com.miui.app.smartpower.SmartPowerSettings.PROP_INTERCEPT_ENABLE = (v12!= 0);
/* .line 858 */
/* invoke-direct {v1, v11, v2}, Lcom/android/server/am/SmartPowerService;->logCloudControlParas(Ljava/lang/String;Ljava/lang/Object;)V */
/* .line 861 */
} // .end local v2 # "interceptEnable":Ljava/lang/String;
} // :cond_3
v2 = (( org.json.JSONObject ) v0 ).has ( v10 ); // invoke-virtual {v0, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 862 */
(( org.json.JSONObject ) v0 ).optString ( v10 ); // invoke-virtual {v0, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 863 */
/* .local v2, "dipslayEnable":Ljava/lang/String; */
final String v11 = "persist.sys.smartpower.display.enable"; // const-string v11, "persist.sys.smartpower.display.enable"
android.os.SystemProperties .set ( v11,v2 );
/* .line 864 */
/* invoke-direct {v1, v10, v2}, Lcom/android/server/am/SmartPowerService;->logCloudControlParas(Ljava/lang/String;Ljava/lang/Object;)V */
/* .line 867 */
} // .end local v2 # "dipslayEnable":Ljava/lang/String;
} // :cond_4
v2 = (( org.json.JSONObject ) v0 ).has ( v9 ); // invoke-virtual {v0, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 868 */
(( org.json.JSONObject ) v0 ).optString ( v9 ); // invoke-virtual {v0, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 869 */
/* .local v2, "windowEnable":Ljava/lang/String; */
final String v10 = "persist.sys.smartpower.window.enable"; // const-string v10, "persist.sys.smartpower.window.enable"
android.os.SystemProperties .set ( v10,v2 );
/* .line 870 */
/* invoke-direct {v1, v9, v2}, Lcom/android/server/am/SmartPowerService;->logCloudControlParas(Ljava/lang/String;Ljava/lang/Object;)V */
/* .line 873 */
} // .end local v2 # "windowEnable":Ljava/lang/String;
} // :cond_5
v2 = (( org.json.JSONObject ) v0 ).has ( v8 ); // invoke-virtual {v0, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 874 */
(( org.json.JSONObject ) v0 ).optString ( v8 ); // invoke-virtual {v0, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 875 */
/* .local v2, "gamePayProtectEnable":Ljava/lang/String; */
final String v9 = "persist.sys.smartpower.gamepay.protect.enabled"; // const-string v9, "persist.sys.smartpower.gamepay.protect.enabled"
android.os.SystemProperties .set ( v9,v2 );
/* .line 876 */
/* nop */
/* .line 877 */
v9 = java.lang.Boolean .parseBoolean ( v2 );
com.miui.app.smartpower.SmartPowerSettings.GAME_PAY_PROTECT_ENABLED = (v9!= 0);
/* .line 878 */
/* invoke-direct {v1, v8, v2}, Lcom/android/server/am/SmartPowerService;->logCloudControlParas(Ljava/lang/String;Ljava/lang/Object;)V */
/* .line 881 */
} // .end local v2 # "gamePayProtectEnable":Ljava/lang/String;
} // :cond_6
(( org.json.JSONObject ) v0 ).optString ( v7 ); // invoke-virtual {v0, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 882 */
/* .local v2, "pkgWhiteListString":Ljava/lang/String; */
v8 = this.mSmartPowerPolicyManager;
(( com.miui.server.smartpower.SmartPowerPolicyManager ) v8 ).updateCloudPackageWhiteList ( v2 ); // invoke-virtual {v8, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->updateCloudPackageWhiteList(Ljava/lang/String;)V
/* .line 883 */
/* invoke-direct {v1, v7, v2}, Lcom/android/server/am/SmartPowerService;->logCloudControlParas(Ljava/lang/String;Ljava/lang/Object;)V */
/* .line 885 */
(( org.json.JSONObject ) v0 ).optString ( v6 ); // invoke-virtual {v0, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 886 */
/* .local v7, "procWhiteListString":Ljava/lang/String; */
v8 = this.mSmartPowerPolicyManager;
(( com.miui.server.smartpower.SmartPowerPolicyManager ) v8 ).updateCloudProcessWhiteList ( v7 ); // invoke-virtual {v8, v7}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->updateCloudProcessWhiteList(Ljava/lang/String;)V
/* .line 887 */
/* invoke-direct {v1, v6, v7}, Lcom/android/server/am/SmartPowerService;->logCloudControlParas(Ljava/lang/String;Ljava/lang/Object;)V */
/* .line 889 */
(( org.json.JSONObject ) v0 ).optString ( v5 ); // invoke-virtual {v0, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 890 */
/* .local v6, "screenonWhiteListString":Ljava/lang/String; */
v8 = this.mSmartPowerPolicyManager;
(( com.miui.server.smartpower.SmartPowerPolicyManager ) v8 ).updateCloudSreenonWhiteList ( v6 ); // invoke-virtual {v8, v6}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->updateCloudSreenonWhiteList(Ljava/lang/String;)V
/* .line 891 */
/* invoke-direct {v1, v5, v6}, Lcom/android/server/am/SmartPowerService;->logCloudControlParas(Ljava/lang/String;Ljava/lang/Object;)V */
/* .line 893 */
(( org.json.JSONObject ) v0 ).optString ( v4 ); // invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 894 */
/* .local v5, "broadcastWhiteListString":Ljava/lang/String; */
v8 = this.mSmartPowerPolicyManager;
(( com.miui.server.smartpower.SmartPowerPolicyManager ) v8 ).updateCloudBroadcastWhiteLit ( v5 ); // invoke-virtual {v8, v5}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->updateCloudBroadcastWhiteLit(Ljava/lang/String;)V
/* .line 895 */
/* invoke-direct {v1, v4, v5}, Lcom/android/server/am/SmartPowerService;->logCloudControlParas(Ljava/lang/String;Ljava/lang/Object;)V */
/* .line 898 */
(( org.json.JSONObject ) v0 ).optString ( v3 ); // invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 899 */
/* .local v4, "alarmWhiteListString":Ljava/lang/String; */
v8 = this.mSmartPowerPolicyManager;
(( com.miui.server.smartpower.SmartPowerPolicyManager ) v8 ).updateCloudAlarmWhiteLit ( v4 ); // invoke-virtual {v8, v4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->updateCloudAlarmWhiteLit(Ljava/lang/String;)V
/* .line 900 */
/* invoke-direct {v1, v3, v4}, Lcom/android/server/am/SmartPowerService;->logCloudControlParas(Ljava/lang/String;Ljava/lang/Object;)V */
/* .line 903 */
/* move-object/from16 v3, v17 */
(( org.json.JSONObject ) v0 ).optString ( v3 ); // invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 904 */
/* .local v8, "providerWhiteListString":Ljava/lang/String; */
v9 = this.mSmartPowerPolicyManager;
(( com.miui.server.smartpower.SmartPowerPolicyManager ) v9 ).updateCloudProviderWhiteLit ( v8 ); // invoke-virtual {v9, v8}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->updateCloudProviderWhiteLit(Ljava/lang/String;)V
/* .line 905 */
/* invoke-direct {v1, v3, v8}, Lcom/android/server/am/SmartPowerService;->logCloudControlParas(Ljava/lang/String;Ljava/lang/Object;)V */
/* .line 908 */
/* move-object/from16 v3, v16 */
(( org.json.JSONObject ) v0 ).optString ( v3 ); // invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 909 */
/* .local v9, "serviceWhiteListString":Ljava/lang/String; */
v10 = this.mSmartPowerPolicyManager;
(( com.miui.server.smartpower.SmartPowerPolicyManager ) v10 ).updateCloudServiceWhiteLit ( v9 ); // invoke-virtual {v10, v9}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->updateCloudServiceWhiteLit(Ljava/lang/String;)V
/* .line 910 */
/* invoke-direct {v1, v3, v9}, Lcom/android/server/am/SmartPowerService;->logCloudControlParas(Ljava/lang/String;Ljava/lang/Object;)V */
/* :try_end_1 */
/* .catch Lorg/json/JSONException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 913 */
} // .end local v0 # "jsonObject":Lorg/json/JSONObject;
} // .end local v2 # "pkgWhiteListString":Ljava/lang/String;
} // .end local v4 # "alarmWhiteListString":Ljava/lang/String;
} // .end local v5 # "broadcastWhiteListString":Ljava/lang/String;
} // .end local v6 # "screenonWhiteListString":Ljava/lang/String;
} // .end local v7 # "procWhiteListString":Ljava/lang/String;
} // .end local v8 # "providerWhiteListString":Ljava/lang/String;
} // .end local v9 # "serviceWhiteListString":Ljava/lang/String;
/* .line 911 */
/* :catch_0 */
/* move-exception v0 */
} // .end local v18 # "data":Ljava/lang/String;
/* .local v2, "data":Ljava/lang/String; */
/* :catch_1 */
/* move-exception v0 */
/* move-object/from16 v18, v2 */
/* .line 912 */
} // .end local v2 # "data":Ljava/lang/String;
/* .local v0, "e":Lorg/json/JSONException; */
/* .restart local v18 # "data":Ljava/lang/String; */
} // :goto_1
final String v2 = "SmartPower"; // const-string v2, "SmartPower"
/* const-string/jumbo v3, "updateCloudData error :" */
android.util.Slog .e ( v2,v3,v0 );
/* .line 914 */
} // .end local v0 # "e":Lorg/json/JSONException;
} // :goto_2
return;
} // .end method
/* # virtual methods */
public void addFrozenPid ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 591 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 592 */
v0 = this.mPowerFrozenManager;
(( com.miui.server.smartpower.PowerFrozenManager ) v0 ).addFrozenPid ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/PowerFrozenManager;->addFrozenPid(II)V
/* .line 594 */
} // :cond_0
return;
} // .end method
public void applyOomAdjLocked ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 1 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 647 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 648 */
v0 = this.mComponentsManager;
(( com.miui.server.smartpower.ComponentsManager ) v0 ).onApplyOomAdjLocked ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/ComponentsManager;->onApplyOomAdjLocked(Lcom/android/server/am/ProcessRecord;)V
/* .line 649 */
com.android.server.am.SystemPressureControllerStub .getInstance ( );
(( com.android.server.am.SystemPressureControllerStub ) v0 ).onApplyOomAdjLocked ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/SystemPressureControllerStub;->onApplyOomAdjLocked(Lcom/android/server/am/ProcessRecord;)V
/* .line 651 */
} // :cond_0
return;
} // .end method
public Boolean checkSystemSignature ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 1085 */
v0 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v0 ).getAppState ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 1086 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1087 */
v1 = (( com.android.server.am.AppStateManager$AppState ) v0 ).isSystemSignature ( ); // invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState;->isSystemSignature()Z
/* .line 1089 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
protected void dump ( java.io.FileDescriptor p0, java.io.PrintWriter p1, java.lang.String[] p2 ) {
/* .locals 4 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .param p3, "args" # [Ljava/lang/String; */
/* .line 1333 */
final String v0 = ""; // const-string v0, ""
v1 = this.mContext;
final String v2 = "SmartPower"; // const-string v2, "SmartPower"
v1 = com.android.internal.util.DumpUtils .checkDumpPermission ( v1,v2,p2 );
/* if-nez v1, :cond_0 */
return;
/* .line 1334 */
} // :cond_0
/* const-string/jumbo v1, "smart power (smartpower):" */
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1335 */
int v1 = 0; // const/4 v1, 0x0
/* .line 1337 */
/* .local v1, "opti":I */
try { // :try_start_0
/* array-length v2, p3 */
/* if-ge v1, v2, :cond_9 */
/* .line 1338 */
/* aget-object v2, p3, v1 */
/* .line 1339 */
/* .local v2, "parm":Ljava/lang/String; */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 1340 */
final String v3 = "policy"; // const-string v3, "policy"
v3 = (( java.lang.String ) v2 ).contains ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1341 */
v0 = this.mSmartPowerPolicyManager;
(( com.miui.server.smartpower.SmartPowerPolicyManager ) v0 ).dump ( p2, p3, v1 ); // invoke-virtual {v0, p2, p3, v1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
/* goto/16 :goto_0 */
/* .line 1342 */
} // :cond_1
final String v3 = "appstate"; // const-string v3, "appstate"
v3 = (( java.lang.String ) v2 ).contains ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 1343 */
v0 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v0 ).dump ( p2, p3, v1 ); // invoke-virtual {v0, p2, p3, v1}, Lcom/android/server/am/AppStateManager;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
/* goto/16 :goto_0 */
/* .line 1344 */
} // :cond_2
final String v3 = "fz"; // const-string v3, "fz"
v3 = (( java.lang.String ) v2 ).contains ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 1345 */
v0 = this.mPowerFrozenManager;
(( com.miui.server.smartpower.PowerFrozenManager ) v0 ).dump ( p2, p3, v1 ); // invoke-virtual {v0, p2, p3, v1}, Lcom/miui/server/smartpower/PowerFrozenManager;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
/* goto/16 :goto_0 */
/* .line 1346 */
} // :cond_3
final String v3 = "scene"; // const-string v3, "scene"
v3 = (( java.lang.String ) v2 ).contains ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 1347 */
v0 = this.mSmartScenarioManager;
(( com.miui.server.smartpower.SmartScenarioManager ) v0 ).dump ( p2, p3, v1 ); // invoke-virtual {v0, p2, p3, v1}, Lcom/miui/server/smartpower/SmartScenarioManager;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
/* .line 1348 */
} // :cond_4
final String v3 = "display"; // const-string v3, "display"
v3 = (( java.lang.String ) v2 ).contains ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 1349 */
v0 = this.mSmartDisplayPolicyManager;
(( com.miui.server.smartpower.SmartDisplayPolicyManager ) v0 ).dump ( p2, p3, v1 ); // invoke-virtual {v0, p2, p3, v1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
/* .line 1350 */
} // :cond_5
/* const-string/jumbo v3, "window" */
v3 = (( java.lang.String ) v2 ).contains ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v3 != null) { // if-eqz v3, :cond_6
/* .line 1351 */
v0 = this.mSmartWindowPolicyManager;
(( com.miui.server.smartpower.SmartWindowPolicyManager ) v0 ).dump ( p2, p3, v1 ); // invoke-virtual {v0, p2, p3, v1}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
/* .line 1352 */
} // :cond_6
/* const-string/jumbo v3, "set" */
v3 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_7
/* .line 1353 */
(( com.android.server.am.SmartPowerService ) p0 ).dumpSettings ( p2, p3, v1 ); // invoke-virtual {p0, p2, p3, v1}, Lcom/android/server/am/SmartPowerService;->dumpSettings(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
/* .line 1354 */
/* invoke-direct {p0, p2}, Lcom/android/server/am/SmartPowerService;->dumpConfig(Ljava/io/PrintWriter;)V */
/* .line 1355 */
} // :cond_7
/* const-string/jumbo v3, "stack" */
v3 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_8
/* .line 1356 */
v0 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v0 ).dumpStack ( p2, p3, v1 ); // invoke-virtual {v0, p2, p3, v1}, Lcom/android/server/am/AppStateManager;->dumpStack(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
/* .line 1357 */
} // :cond_8
final String v3 = "-a"; // const-string v3, "-a"
v3 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_9
/* .line 1358 */
/* invoke-direct {p0, p2}, Lcom/android/server/am/SmartPowerService;->dumpConfig(Ljava/io/PrintWriter;)V */
/* .line 1359 */
v3 = this.mSmartPowerPolicyManager;
(( com.miui.server.smartpower.SmartPowerPolicyManager ) v3 ).dump ( p2, p3, v1 ); // invoke-virtual {v3, p2, p3, v1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
/* .line 1360 */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1361 */
v3 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v3 ).dump ( p2, p3, v1 ); // invoke-virtual {v3, p2, p3, v1}, Lcom/android/server/am/AppStateManager;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
/* .line 1362 */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1363 */
v3 = this.mSmartScenarioManager;
(( com.miui.server.smartpower.SmartScenarioManager ) v3 ).dump ( p2, p3, v1 ); // invoke-virtual {v3, p2, p3, v1}, Lcom/miui/server/smartpower/SmartScenarioManager;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
/* .line 1364 */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1365 */
v3 = this.mPowerFrozenManager;
(( com.miui.server.smartpower.PowerFrozenManager ) v3 ).dump ( p2, p3, v1 ); // invoke-virtual {v3, p2, p3, v1}, Lcom/miui/server/smartpower/PowerFrozenManager;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
/* .line 1366 */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1371 */
} // .end local v2 # "parm":Ljava/lang/String;
} // :cond_9
} // :goto_0
/* .line 1369 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1370 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 1372 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
public void dumpSettings ( java.io.PrintWriter p0, java.lang.String[] p1, Integer p2 ) {
/* .locals 5 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "args" # [Ljava/lang/String; */
/* .param p3, "opti" # I */
/* .line 1375 */
/* add-int/lit8 v0, p3, 0x1 */
/* array-length v1, p2 */
/* if-ge v0, v1, :cond_4 */
/* .line 1376 */
/* add-int/lit8 v0, p3, 0x1 */
} // .end local p3 # "opti":I
/* .local v0, "opti":I */
/* aget-object p3, p2, p3 */
/* .line 1377 */
/* .local p3, "key":Ljava/lang/String; */
/* add-int/lit8 v1, v0, 0x1 */
} // .end local v0 # "opti":I
/* .local v1, "opti":I */
/* aget-object v0, p2, v0 */
/* .line 1378 */
/* .local v0, "value":Ljava/lang/String; */
final String v2 = "fz"; // const-string v2, "fz"
v2 = (( java.lang.String ) p3 ).contains ( v2 ); // invoke-virtual {p3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
final String v3 = "false"; // const-string v3, "false"
/* const-string/jumbo v4, "true" */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1379 */
v2 = (( java.lang.String ) v0 ).equals ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_0 */
v2 = (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 1380 */
} // :cond_0
final String v2 = "persist.sys.smartpower.fz.enable"; // const-string v2, "persist.sys.smartpower.fz.enable"
android.os.SystemProperties .set ( v2,v0 );
/* .line 1382 */
} // :cond_1
final String v2 = "intercept"; // const-string v2, "intercept"
v2 = (( java.lang.String ) p3 ).contains ( v2 ); // invoke-virtual {p3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 1383 */
v2 = (( java.lang.String ) v0 ).equals ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_2 */
v2 = (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 1384 */
} // :cond_2
final String v2 = "persist.sys.smartpower.intercept.enable"; // const-string v2, "persist.sys.smartpower.intercept.enable"
android.os.SystemProperties .set ( v2,v0 );
/* .line 1385 */
v2 = java.lang.Boolean .parseBoolean ( v0 );
com.miui.app.smartpower.SmartPowerSettings.PROP_INTERCEPT_ENABLE = (v2!= 0);
/* .line 1389 */
} // .end local v0 # "value":Ljava/lang/String;
} // .end local p3 # "key":Ljava/lang/String;
} // :cond_3
} // :goto_0
/* move p3, v1 */
} // .end local v1 # "opti":I
/* .local p3, "opti":I */
} // :cond_4
return;
} // .end method
public java.util.ArrayList getAllAppState ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/miui/server/smartpower/IAppState;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 924 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 925 */
v0 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v0 ).getAllAppState ( ); // invoke-virtual {v0}, Lcom/android/server/am/AppStateManager;->getAllAppState()Ljava/util/ArrayList;
/* .line 927 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public com.miui.server.smartpower.IAppState getAppState ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 946 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 947 */
v0 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v0 ).getAppState ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 949 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer getBackgroundCpuCoreNum ( ) {
/* .locals 1 */
/* .line 1077 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1078 */
v0 = this.mSmartCpuPolicyManager;
v0 = (( com.miui.server.smartpower.SmartCpuPolicyManager ) v0 ).getBackgroundCpuCoreNum ( ); // invoke-virtual {v0}, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->getBackgroundCpuCoreNum()I
/* .line 1080 */
} // :cond_0
int v0 = 4; // const/4 v0, 0x4
} // .end method
public Long getLastMusicPlayTimeStamp ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "pid" # I */
/* .line 1053 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1054 */
v0 = this.mAppPowerResourceManager;
(( com.miui.server.smartpower.AppPowerResourceManager ) v0 ).getLastMusicPlayTimeStamp ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/AppPowerResourceManager;->getLastMusicPlayTimeStamp(I)J
/* move-result-wide v0 */
/* return-wide v0 */
/* .line 1056 */
} // :cond_0
/* const-wide/16 v0, 0x0 */
/* return-wide v0 */
} // .end method
public java.util.ArrayList getLruProcesses ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/miui/server/smartpower/IAppState$IRunningProcess;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 954 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 955 */
v0 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v0 ).getLruProcesses ( ); // invoke-virtual {v0}, Lcom/android/server/am/AppStateManager;->getLruProcesses()Ljava/util/ArrayList;
/* .line 957 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public java.util.ArrayList getLruProcesses ( Integer p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/miui/server/smartpower/IAppState$IRunningProcess;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 962 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 963 */
v0 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v0 ).getLruProcesses ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/am/AppStateManager;->getLruProcesses(ILjava/lang/String;)Ljava/util/ArrayList;
/* .line 965 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public com.miui.server.smartpower.IAppState$IRunningProcess getRunningProcess ( Integer p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "processName" # Ljava/lang/String; */
/* .line 970 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 971 */
v0 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v0 ).getRunningProcess ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/am/AppStateManager;->getRunningProcess(ILjava/lang/String;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 973 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void hibernateAllIfNeeded ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .line 979 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 980 */
v0 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v0 ).hibernateAllIfNeeded ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager;->hibernateAllIfNeeded(Ljava/lang/String;)V
/* .line 982 */
} // :cond_0
return;
} // .end method
public Boolean isAppAudioActive ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 1026 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1027 */
v0 = this.mAppPowerResourceManager;
v0 = (( com.miui.server.smartpower.AppPowerResourceManager ) v0 ).isAppResActive ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/miui/server/smartpower/AppPowerResourceManager;->isAppResActive(II)Z
/* .line 1030 */
} // :cond_0
} // .end method
public Boolean isAppAudioActive ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 1044 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1045 */
v0 = this.mAppPowerResourceManager;
v0 = (( com.miui.server.smartpower.AppPowerResourceManager ) v0 ).isAppResActive ( p1, p2, v1 ); // invoke-virtual {v0, p1, p2, v1}, Lcom/miui/server/smartpower/AppPowerResourceManager;->isAppResActive(III)Z
/* .line 1048 */
} // :cond_0
} // .end method
public Boolean isAppGpsActive ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 1035 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1036 */
v0 = this.mAppPowerResourceManager;
int v1 = 3; // const/4 v1, 0x3
v0 = (( com.miui.server.smartpower.AppPowerResourceManager ) v0 ).isAppResActive ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/miui/server/smartpower/AppPowerResourceManager;->isAppResActive(II)Z
/* .line 1039 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean isAppGpsActive ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 1061 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1062 */
v0 = this.mAppPowerResourceManager;
int v1 = 3; // const/4 v1, 0x3
v0 = (( com.miui.server.smartpower.AppPowerResourceManager ) v0 ).isAppResActive ( p1, p2, v1 ); // invoke-virtual {v0, p1, p2, v1}, Lcom/miui/server/smartpower/AppPowerResourceManager;->isAppResActive(III)Z
/* .line 1065 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean isProcessInTaskStack ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 1002 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1003 */
v0 = this.mAppStateManager;
v0 = (( com.android.server.am.AppStateManager ) v0 ).isProcessInTaskStack ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/am/AppStateManager;->isProcessInTaskStack(II)Z
/* .line 1005 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isProcessInTaskStack ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "prcName" # Ljava/lang/String; */
/* .line 1010 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1011 */
v0 = this.mAppStateManager;
v0 = (( com.android.server.am.AppStateManager ) v0 ).isProcessInTaskStack ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager;->isProcessInTaskStack(Ljava/lang/String;)Z
/* .line 1013 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isProcessPerceptible ( Integer p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "procName" # Ljava/lang/String; */
/* .line 994 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 995 */
v0 = this.mAppStateManager;
v0 = (( com.android.server.am.AppStateManager ) v0 ).isProcessPerceptible ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/am/AppStateManager;->isProcessPerceptible(ILjava/lang/String;)Z
/* .line 997 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isProcessWhiteList ( Integer p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "flag" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "procName" # Ljava/lang/String; */
/* .line 1018 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1019 */
v0 = this.mSmartPowerPolicyManager;
v0 = (( com.miui.server.smartpower.SmartPowerPolicyManager ) v0 ).isProcessWhiteList ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isProcessWhiteList(ILjava/lang/String;Ljava/lang/String;)Z
/* .line 1021 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isUidIdle ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 241 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 242 */
v0 = this.mAppStateManager;
v0 = (( com.android.server.am.AppStateManager ) v0 ).isUidIdle ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager;->isUidIdle(I)Z
/* .line 244 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isUidVisible ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 986 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 987 */
v0 = this.mAppStateManager;
v0 = (( com.android.server.am.AppStateManager ) v0 ).isUidVisible ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager;->isUidVisible(I)Z
/* .line 989 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void notifyCameraForegroundState ( java.lang.String p0, Boolean p1, java.lang.String p2, Integer p3, Integer p4 ) {
/* .locals 7 */
/* .param p1, "cameraId" # Ljava/lang/String; */
/* .param p2, "isForeground" # Z */
/* .param p3, "caller" # Ljava/lang/String; */
/* .param p4, "callerUid" # I */
/* .param p5, "callerPid" # I */
/* .line 812 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 813 */
v1 = this.mAppPowerResourceManager;
/* move-object v2, p1 */
/* move v3, p2 */
/* move-object v4, p3 */
/* move v5, p4 */
/* move v6, p5 */
/* invoke-virtual/range {v1 ..v6}, Lcom/miui/server/smartpower/AppPowerResourceManager;->notifyCameraForegroundState(Ljava/lang/String;ZLjava/lang/String;II)V */
/* .line 816 */
} // :cond_0
return;
} // .end method
public void notifyMultiSenceEnable ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "enable" # Z */
/* .line 1120 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1121 */
v0 = this.mSmartBoostPolicyManager;
(( com.miui.server.smartpower.SmartBoostPolicyManager ) v0 ).setMultiSenceEnable ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/SmartBoostPolicyManager;->setMultiSenceEnable(Z)V
/* .line 1123 */
} // :cond_0
return;
} // .end method
public void onAcquireLocation ( Integer p0, Integer p1, android.location.ILocationListener p2 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "listener" # Landroid/location/ILocationListener; */
/* .line 739 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 740 */
v0 = this.mAppPowerResourceManager;
(( com.miui.server.smartpower.AppPowerResourceManager ) v0 ).onAquireLocation ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/AppPowerResourceManager;->onAquireLocation(IILandroid/location/ILocationListener;)V
/* .line 742 */
} // :cond_0
return;
} // .end method
public void onAcquireWakelock ( android.os.IBinder p0, Integer p1, java.lang.String p2, Integer p3, Integer p4 ) {
/* .locals 7 */
/* .param p1, "lock" # Landroid/os/IBinder; */
/* .param p2, "flags" # I */
/* .param p3, "tag" # Ljava/lang/String; */
/* .param p4, "ownerUid" # I */
/* .param p5, "ownerPid" # I */
/* .line 756 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 757 */
v1 = this.mAppPowerResourceManager;
/* .line 758 */
/* move-object v2, p1 */
/* move v3, p2 */
/* move-object v4, p3 */
/* move v5, p4 */
/* move v6, p5 */
/* invoke-virtual/range {v1 ..v6}, Lcom/miui/server/smartpower/AppPowerResourceManager;->onAcquireWakelock(Landroid/os/IBinder;ILjava/lang/String;II)V */
/* .line 760 */
} // :cond_0
return;
} // .end method
public void onActivityLaunched ( Integer p0, java.lang.String p1, com.android.server.wm.ActivityRecord p2, Integer p3 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "processName" # Ljava/lang/String; */
/* .param p3, "record" # Lcom/android/server/wm/ActivityRecord; */
/* .param p4, "launchState" # I */
/* .line 623 */
com.android.server.am.MiProcessTracker .getInstance ( );
v1 = this.packageName;
(( com.android.server.am.MiProcessTracker ) v0 ).onActivityLaunched ( p2, v1, p1, p4 ); // invoke-virtual {v0, p2, v1, p1, p4}, Lcom/android/server/am/MiProcessTracker;->onActivityLaunched(Ljava/lang/String;Ljava/lang/String;II)V
/* .line 625 */
return;
} // .end method
public void onActivityReusmeUnchecked ( java.lang.String p0, Integer p1, Integer p2, java.lang.String p3, Integer p4, Integer p5, java.lang.String p6, Boolean p7 ) {
/* .locals 11 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .param p3, "pid" # I */
/* .param p4, "packageName" # Ljava/lang/String; */
/* .param p5, "launchedFromUid" # I */
/* .param p6, "launchedFromPid" # I */
/* .param p7, "launchedFromPackage" # Ljava/lang/String; */
/* .param p8, "isColdStart" # Z */
/* .line 404 */
/* move-object v0, p0 */
/* iget-boolean v1, v0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 405 */
com.android.server.am.SystemPressureControllerStub .getInstance ( );
/* new-instance v10, Lcom/android/server/am/ControllerActivityInfo; */
/* move-object v2, v10 */
/* move v3, p2 */
/* move v4, p3 */
/* move-object v5, p4 */
/* move/from16 v6, p5 */
/* move/from16 v7, p6 */
/* move-object/from16 v8, p7 */
/* move/from16 v9, p8 */
/* invoke-direct/range {v2 ..v9}, Lcom/android/server/am/ControllerActivityInfo;-><init>(IILjava/lang/String;IILjava/lang/String;Z)V */
(( com.android.server.am.SystemPressureControllerStub ) v1 ).activityResumeUnchecked ( v10 ); // invoke-virtual {v1, v10}, Lcom/android/server/am/SystemPressureControllerStub;->activityResumeUnchecked(Lcom/android/server/am/ControllerActivityInfo;)V
/* .line 409 */
} // :cond_0
return;
} // .end method
public void onActivityStartUnchecked ( java.lang.String p0, Integer p1, Integer p2, java.lang.String p3, Integer p4, Integer p5, java.lang.String p6, Boolean p7 ) {
/* .locals 20 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .param p3, "pid" # I */
/* .param p4, "packageName" # Ljava/lang/String; */
/* .param p5, "launchedFromUid" # I */
/* .param p6, "launchedFromPid" # I */
/* .param p7, "launchedFromPackage" # Ljava/lang/String; */
/* .param p8, "isColdStart" # Z */
/* .line 373 */
/* move-object/from16 v0, p0 */
/* iget-boolean v1, v0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 374 */
v2 = this.mComponentsManager;
/* move-object/from16 v3, p1 */
/* move/from16 v4, p6 */
/* move/from16 v5, p2 */
/* move-object/from16 v6, p4 */
/* move/from16 v7, p8 */
/* invoke-virtual/range {v2 ..v7}, Lcom/miui/server/smartpower/ComponentsManager;->activityStartBeforeLocked(Ljava/lang/String;IILjava/lang/String;Z)V */
/* .line 376 */
com.android.server.am.SystemPressureControllerStub .getInstance ( );
/* new-instance v10, Lcom/android/server/am/ControllerActivityInfo; */
/* move-object v2, v10 */
/* move/from16 v3, p2 */
/* move/from16 v4, p3 */
/* move-object/from16 v5, p4 */
/* move/from16 v6, p5 */
/* move/from16 v7, p6 */
/* move-object/from16 v8, p7 */
/* move/from16 v9, p8 */
/* invoke-direct/range {v2 ..v9}, Lcom/android/server/am/ControllerActivityInfo;-><init>(IILjava/lang/String;IILjava/lang/String;Z)V */
(( com.android.server.am.SystemPressureControllerStub ) v1 ).activityStartBeforeLocked ( v10 ); // invoke-virtual {v1, v10}, Lcom/android/server/am/SystemPressureControllerStub;->activityStartBeforeLocked(Lcom/android/server/am/ControllerActivityInfo;)V
/* .line 379 */
v11 = this.mSmartDisplayPolicyManager;
/* move-object/from16 v12, p1 */
/* move/from16 v13, p2 */
/* move/from16 v14, p3 */
/* move-object/from16 v15, p4 */
/* move/from16 v16, p5 */
/* move/from16 v17, p6 */
/* move-object/from16 v18, p7 */
/* move/from16 v19, p8 */
/* invoke-virtual/range {v11 ..v19}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyActivityStartUnchecked(Ljava/lang/String;IILjava/lang/String;IILjava/lang/String;Z)V */
/* .line 383 */
} // :cond_0
return;
} // .end method
public void onActivityVisibilityChanged ( Integer p0, java.lang.String p1, com.android.server.wm.ActivityRecord p2, Boolean p3 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "processName" # Ljava/lang/String; */
/* .param p3, "record" # Lcom/android/server/wm/ActivityRecord; */
/* .param p4, "visible" # Z */
/* .line 614 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 615 */
v0 = this.mComponentsManager;
/* .line 616 */
(( com.miui.server.smartpower.ComponentsManager ) v0 ).activityVisibilityChangedLocked ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/smartpower/ComponentsManager;->activityVisibilityChangedLocked(ILjava/lang/String;Lcom/android/server/wm/ActivityRecord;Z)V
/* .line 618 */
} // :cond_0
return;
} // .end method
public void onAlarmStatusChanged ( Integer p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "active" # Z */
/* .line 501 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 502 */
v0 = this.mComponentsManager;
(( com.miui.server.smartpower.ComponentsManager ) v0 ).alarmStatusChangedLocked ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/ComponentsManager;->alarmStatusChangedLocked(IZ)V
/* .line 504 */
} // :cond_0
return;
} // .end method
public void onAppTransitionStartLocked ( Long p0 ) {
/* .locals 1 */
/* .param p1, "animDuration" # J */
/* .line 1128 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1129 */
v0 = this.mSmartDisplayPolicyManager;
(( com.miui.server.smartpower.SmartDisplayPolicyManager ) v0 ).notifyAppTransitionStartLocked ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyAppTransitionStartLocked(J)V
/* .line 1131 */
} // :cond_0
return;
} // .end method
public void onBackupChanged ( Boolean p0, com.android.server.am.ProcessRecord p1 ) {
/* .locals 3 */
/* .param p1, "active" # Z */
/* .param p2, "proc" # Lcom/android/server/am/ProcessRecord; */
/* .line 703 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 704 */
v0 = this.mSmartPowerPolicyManager;
/* iget v1, p2, Lcom/android/server/am/ProcessRecord;->uid:I */
v2 = this.info;
v2 = this.packageName;
(( com.miui.server.smartpower.SmartPowerPolicyManager ) v0 ).onBackupChanged ( p1, v1, v2 ); // invoke-virtual {v0, p1, v1, v2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->onBackupChanged(ZILjava/lang/String;)V
/* .line 705 */
v0 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v0 ).onBackupChanged ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/am/AppStateManager;->onBackupChanged(ZLcom/android/server/am/ProcessRecord;)V
/* .line 707 */
} // :cond_0
return;
} // .end method
public void onBackupServiceAppChanged ( Boolean p0, Integer p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "active" # Z */
/* .param p2, "uid" # I */
/* .param p3, "pid" # I */
/* .line 711 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 712 */
v0 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v0 ).onBackupServiceAppChanged ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/am/AppStateManager;->onBackupServiceAppChanged(ZII)V
/* .line 714 */
} // :cond_0
return;
} // .end method
public void onBluetoothEvent ( Boolean p0, Integer p1, Integer p2, Integer p3, java.lang.String p4, Integer p5 ) {
/* .locals 15 */
/* .param p1, "isConnect" # Z */
/* .param p2, "bleType" # I */
/* .param p3, "uid" # I */
/* .param p4, "pid" # I */
/* .param p5, "pkg" # Ljava/lang/String; */
/* .param p6, "data" # I */
/* .line 773 */
/* move-object v0, p0 */
/* move/from16 v7, p3 */
/* iget-boolean v1, v0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 774 */
/* if-lez v7, :cond_3 */
/* .line 775 */
/* if-lez p4, :cond_0 */
/* .line 776 */
v1 = this.mAppPowerResourceManager;
/* move/from16 v2, p1 */
/* move/from16 v3, p2 */
/* move/from16 v4, p3 */
/* move/from16 v5, p4 */
/* move/from16 v6, p6 */
/* invoke-virtual/range {v1 ..v6}, Lcom/miui/server/smartpower/AppPowerResourceManager;->onBluetoothEvent(ZIIII)V */
/* move-object/from16 v2, p5 */
/* goto/16 :goto_3 */
/* .line 778 */
} // :cond_0
v1 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v1 ).getAppState ( v7 ); // invoke-virtual {v1, v7}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 779 */
/* .local v8, "appState":Lcom/android/server/am/AppStateManager$AppState; */
/* if-nez v8, :cond_1 */
return;
/* .line 780 */
} // :cond_1
/* nop */
/* .line 781 */
(( com.android.server.am.AppStateManager$AppState ) v8 ).getRunningProcessList ( ); // invoke-virtual {v8}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcessList()Ljava/util/ArrayList;
/* .line 782 */
/* .local v9, "runningProcs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;" */
int v1 = 0; // const/4 v1, 0x0
/* move v10, v1 */
/* .local v10, "i":I */
} // :goto_0
v1 = (( java.util.ArrayList ) v9 ).size ( ); // invoke-virtual {v9}, Ljava/util/ArrayList;->size()I
/* if-ge v10, v1, :cond_2 */
/* .line 783 */
v1 = this.mAppPowerResourceManager;
/* .line 784 */
(( java.util.ArrayList ) v9 ).get ( v10 ); // invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
v5 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v2 ).getPid ( ); // invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPid()I
/* .line 783 */
/* move/from16 v2, p1 */
/* move/from16 v3, p2 */
/* move/from16 v4, p3 */
/* move/from16 v6, p6 */
/* invoke-virtual/range {v1 ..v6}, Lcom/miui/server/smartpower/AppPowerResourceManager;->onBluetoothEvent(ZIIII)V */
/* .line 782 */
/* add-int/lit8 v10, v10, 0x1 */
/* .line 786 */
} // .end local v8 # "appState":Lcom/android/server/am/AppStateManager$AppState;
} // .end local v9 # "runningProcs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
} // .end local v10 # "i":I
} // :cond_2
/* move-object/from16 v2, p5 */
/* .line 787 */
} // :cond_3
v1 = /* invoke-static/range {p5 ..p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z */
/* if-nez v1, :cond_5 */
/* .line 788 */
v1 = this.mAppStateManager;
/* move-object/from16 v2, p5 */
(( com.android.server.am.AppStateManager ) v1 ).getAppState ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/AppStateManager;->getAppState(Ljava/lang/String;)Ljava/util/List;
/* .line 789 */
/* .local v1, "appStates":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/AppStateManager$AppState;>;" */
v4 = } // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_7
/* check-cast v4, Lcom/android/server/am/AppStateManager$AppState; */
/* .line 790 */
/* .local v4, "appState":Lcom/android/server/am/AppStateManager$AppState; */
v5 = (( com.android.server.am.AppStateManager$AppState ) v4 ).getUid ( ); // invoke-virtual {v4}, Lcom/android/server/am/AppStateManager$AppState;->getUid()I
/* .line 791 */
/* .local v5, "realUid":I */
/* nop */
/* .line 792 */
(( com.android.server.am.AppStateManager$AppState ) v4 ).getRunningProcessList ( ); // invoke-virtual {v4}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcessList()Ljava/util/ArrayList;
/* .line 793 */
/* .local v6, "runningProcs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;" */
int v8 = 0; // const/4 v8, 0x0
/* move v14, v8 */
/* .local v14, "i":I */
} // :goto_2
v8 = (( java.util.ArrayList ) v6 ).size ( ); // invoke-virtual {v6}, Ljava/util/ArrayList;->size()I
/* if-ge v14, v8, :cond_4 */
/* .line 794 */
v8 = this.mAppPowerResourceManager;
/* .line 795 */
(( java.util.ArrayList ) v6 ).get ( v14 ); // invoke-virtual {v6, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v9, Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
v12 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v9 ).getPid ( ); // invoke-virtual {v9}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPid()I
/* .line 794 */
/* move/from16 v9, p1 */
/* move/from16 v10, p2 */
/* move v11, v5 */
/* move/from16 v13, p6 */
/* invoke-virtual/range {v8 ..v13}, Lcom/miui/server/smartpower/AppPowerResourceManager;->onBluetoothEvent(ZIIII)V */
/* .line 793 */
/* add-int/lit8 v14, v14, 0x1 */
/* .line 797 */
} // .end local v4 # "appState":Lcom/android/server/am/AppStateManager$AppState;
} // .end local v5 # "realUid":I
} // .end local v6 # "runningProcs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
} // .end local v14 # "i":I
} // :cond_4
/* .line 787 */
} // .end local v1 # "appStates":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/AppStateManager$AppState;>;"
} // :cond_5
/* move-object/from16 v2, p5 */
/* .line 773 */
} // :cond_6
/* move-object/from16 v2, p5 */
/* .line 800 */
} // :cond_7
} // :goto_3
return;
} // .end method
public void onBroadcastStatusChanged ( com.android.server.am.ProcessRecord p0, Boolean p1, android.content.Intent p2 ) {
/* .locals 1 */
/* .param p1, "processRecord" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "active" # Z */
/* .param p3, "intent" # Landroid/content/Intent; */
/* .line 415 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 416 */
v0 = this.mComponentsManager;
(( com.miui.server.smartpower.ComponentsManager ) v0 ).broadcastStatusChangedLocked ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/ComponentsManager;->broadcastStatusChangedLocked(Lcom/android/server/am/ProcessRecord;ZLandroid/content/Intent;)V
/* .line 419 */
} // :cond_0
return;
} // .end method
public void onContentProviderStatusChanged ( Integer p0, com.android.server.am.ProcessRecord p1, com.android.server.am.ProcessRecord p2, com.android.server.am.ContentProviderRecord p3, Boolean p4 ) {
/* .locals 2 */
/* .param p1, "callingUid" # I */
/* .param p2, "callingProc" # Lcom/android/server/am/ProcessRecord; */
/* .param p3, "hostingProc" # Lcom/android/server/am/ProcessRecord; */
/* .param p4, "record" # Lcom/android/server/am/ContentProviderRecord; */
/* .param p5, "isRunning" # Z */
/* .line 455 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p3 != null) { // if-eqz p3, :cond_0
/* .line 456 */
v0 = this.mComponentsManager;
(( com.android.server.am.ContentProviderRecord ) p4 ).toString ( ); // invoke-virtual {p4}, Lcom/android/server/am/ContentProviderRecord;->toString()Ljava/lang/String;
(( com.miui.server.smartpower.ComponentsManager ) v0 ).contentProviderStatusChangedLocked ( p3, v1 ); // invoke-virtual {v0, p3, v1}, Lcom/miui/server/smartpower/ComponentsManager;->contentProviderStatusChangedLocked(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
/* .line 458 */
} // :cond_0
return;
} // .end method
public void onCpuExceptionEvents ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .line 1113 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1114 */
v0 = this.mSmartCpuPolicyManager;
(( com.miui.server.smartpower.SmartCpuPolicyManager ) v0 ).cpuExceptionEvents ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->cpuExceptionEvents(I)V
/* .line 1116 */
} // :cond_0
return;
} // .end method
public void onCpuPressureEvents ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "level" # I */
/* .line 1106 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1107 */
v0 = this.mSmartCpuPolicyManager;
(( com.miui.server.smartpower.SmartCpuPolicyManager ) v0 ).cpuPressureEvents ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->cpuPressureEvents(I)V
/* .line 1109 */
} // :cond_0
return;
} // .end method
public void onDisplayDeviceStateChangeLocked ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "deviceState" # I */
/* .line 1195 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1196 */
v0 = this.mSmartDisplayPolicyManager;
(( com.miui.server.smartpower.SmartDisplayPolicyManager ) v0 ).notifyDisplayDeviceStateChangeLocked ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyDisplayDeviceStateChangeLocked(I)V
/* .line 1198 */
} // :cond_0
return;
} // .end method
public void onDisplaySwitchResolutionLocked ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "width" # I */
/* .param p2, "height" # I */
/* .param p3, "density" # I */
/* .line 1203 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1204 */
v0 = this.mSmartDisplayPolicyManager;
(( com.miui.server.smartpower.SmartDisplayPolicyManager ) v0 ).notifyDisplaySwitchResolutionLocked ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyDisplaySwitchResolutionLocked(III)V
/* .line 1206 */
} // :cond_0
return;
} // .end method
public void onExternalAudioRegister ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 318 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 319 */
v0 = this.mSmartPowerPolicyManager;
(( com.miui.server.smartpower.SmartPowerPolicyManager ) v0 ).onExternalAudioRegister ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->onExternalAudioRegister(II)V
/* .line 321 */
} // :cond_0
return;
} // .end method
public void onFocusedWindowChangeLocked ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "oldPackage" # Ljava/lang/String; */
/* .param p2, "newPackage" # Ljava/lang/String; */
/* .line 1187 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1188 */
v0 = this.mSmartDisplayPolicyManager;
(( com.miui.server.smartpower.SmartDisplayPolicyManager ) v0 ).notifyFocusedWindowChangeLocked ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyFocusedWindowChangeLocked(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1190 */
} // :cond_0
return;
} // .end method
public void onForegroundActivityChangedLocked ( java.lang.String p0, Integer p1, Integer p2, java.lang.String p3, Integer p4, Integer p5, java.lang.String p6, Boolean p7, Integer p8, Integer p9, java.lang.String p10 ) {
/* .locals 11 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .param p3, "pid" # I */
/* .param p4, "packageName" # Ljava/lang/String; */
/* .param p5, "launchedFromUid" # I */
/* .param p6, "launchedFromPid" # I */
/* .param p7, "launchedFromPackage" # Ljava/lang/String; */
/* .param p8, "isColdStart" # Z */
/* .param p9, "taskId" # I */
/* .param p10, "callingUid" # I */
/* .param p11, "callingPkg" # Ljava/lang/String; */
/* .line 390 */
/* move-object v0, p0 */
/* iget-boolean v1, v0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 391 */
v2 = this.mComponentsManager;
/* move/from16 v3, p6 */
/* move v4, p2 */
/* move v5, p3 */
/* move-object v6, p1 */
/* move/from16 v7, p9 */
/* move/from16 v8, p10 */
/* move-object/from16 v9, p11 */
/* invoke-virtual/range {v2 ..v9}, Lcom/miui/server/smartpower/ComponentsManager;->onForegroundActivityChangedLocked(IIILjava/lang/String;IILjava/lang/String;)V */
/* .line 393 */
com.android.server.am.SystemPressureControllerStub .getInstance ( );
/* new-instance v10, Lcom/android/server/am/ControllerActivityInfo; */
/* move-object v2, v10 */
/* move v3, p2 */
/* move v4, p3 */
/* move-object v5, p4 */
/* move/from16 v6, p5 */
/* move/from16 v7, p6 */
/* move-object/from16 v8, p7 */
/* move/from16 v9, p8 */
/* invoke-direct/range {v2 ..v9}, Lcom/android/server/am/ControllerActivityInfo;-><init>(IILjava/lang/String;IILjava/lang/String;Z)V */
/* .line 394 */
(( com.android.server.am.SystemPressureControllerStub ) v1 ).foregroundActivityChangedLocked ( v10 ); // invoke-virtual {v1, v10}, Lcom/android/server/am/SystemPressureControllerStub;->foregroundActivityChangedLocked(Lcom/android/server/am/ControllerActivityInfo;)V
/* .line 398 */
} // :cond_0
return;
} // .end method
public void onHoldScreenUidChanged ( Integer p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "hold" # Z */
/* .line 639 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 640 */
v0 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v0 ).onHoldScreenUidChanged ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/am/AppStateManager;->onHoldScreenUidChanged(IZ)V
/* .line 642 */
} // :cond_0
return;
} // .end method
public void onInputMethodShow ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 731 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 732 */
v0 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v0 ).onInputMethodShow ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager;->onInputMethodShow(I)V
/* .line 734 */
} // :cond_0
return;
} // .end method
public void onInsetAnimationHide ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .line 1151 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1152 */
v0 = this.mSmartDisplayPolicyManager;
(( com.miui.server.smartpower.SmartDisplayPolicyManager ) v0 ).notifyInsetAnimationHide ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyInsetAnimationHide(I)V
/* .line 1154 */
} // :cond_0
return;
} // .end method
public void onInsetAnimationShow ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .line 1144 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1145 */
v0 = this.mSmartDisplayPolicyManager;
(( com.miui.server.smartpower.SmartDisplayPolicyManager ) v0 ).notifyInsetAnimationShow ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyInsetAnimationShow(I)V
/* .line 1147 */
} // :cond_0
return;
} // .end method
public void onMediaKey ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 680 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 681 */
v0 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v0 ).onMediaKey ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager;->onMediaKey(I)V
/* .line 683 */
} // :cond_0
return;
} // .end method
public void onMediaKey ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 673 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 674 */
v0 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v0 ).onMediaKey ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/am/AppStateManager;->onMediaKey(II)V
/* .line 676 */
} // :cond_0
return;
} // .end method
public void onMultiTaskActionEnd ( miui.smartpower.MultiTaskActionManager$ActionInfo p0 ) {
/* .locals 4 */
/* .param p1, "actionInfo" # Lmiui/smartpower/MultiTaskActionManager$ActionInfo; */
/* .line 1262 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1263 */
v0 = android.os.Binder .getCallingPid ( );
/* .line 1264 */
/* .local v0, "callingPid":I */
v1 = android.os.Binder .getCallingUid ( );
/* .line 1265 */
/* .local v1, "callingUid":I */
v2 = /* invoke-direct {p0}, Lcom/android/server/am/SmartPowerService;->checkPermission()Z */
/* if-nez v2, :cond_0 */
/* .line 1266 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Permission Denial: onMultiTaskActionEnd from pid="; // const-string v3, "Permission Denial: onMultiTaskActionEnd from pid="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", uid="; // const-string v3, ", uid="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1268 */
/* .local v2, "msg":Ljava/lang/String; */
final String v3 = "SmartPower"; // const-string v3, "SmartPower"
android.util.Slog .w ( v3,v2 );
/* .line 1269 */
return;
/* .line 1271 */
} // .end local v2 # "msg":Ljava/lang/String;
} // :cond_0
v2 = this.mSmartWindowPolicyManager;
(( com.miui.server.smartpower.SmartWindowPolicyManager ) v2 ).onMultiTaskActionEnd ( p1, v0 ); // invoke-virtual {v2, p1, v0}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->onMultiTaskActionEnd(Lmiui/smartpower/MultiTaskActionManager$ActionInfo;I)V
/* .line 1273 */
} // .end local v0 # "callingPid":I
} // .end local v1 # "callingUid":I
} // :cond_1
return;
} // .end method
public void onMultiTaskActionStart ( miui.smartpower.MultiTaskActionManager$ActionInfo p0 ) {
/* .locals 4 */
/* .param p1, "actionInfo" # Lmiui/smartpower/MultiTaskActionManager$ActionInfo; */
/* .line 1247 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1248 */
v0 = android.os.Binder .getCallingPid ( );
/* .line 1249 */
/* .local v0, "callingPid":I */
v1 = android.os.Binder .getCallingUid ( );
/* .line 1250 */
/* .local v1, "callingUid":I */
v2 = /* invoke-direct {p0}, Lcom/android/server/am/SmartPowerService;->checkPermission()Z */
/* if-nez v2, :cond_0 */
/* .line 1251 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Permission Denial: onMultiTaskActionStart from pid="; // const-string v3, "Permission Denial: onMultiTaskActionStart from pid="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", uid="; // const-string v3, ", uid="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1253 */
/* .local v2, "msg":Ljava/lang/String; */
final String v3 = "SmartPower"; // const-string v3, "SmartPower"
android.util.Slog .w ( v3,v2 );
/* .line 1254 */
return;
/* .line 1256 */
} // .end local v2 # "msg":Ljava/lang/String;
} // :cond_0
v2 = this.mSmartWindowPolicyManager;
(( com.miui.server.smartpower.SmartWindowPolicyManager ) v2 ).onMultiTaskActionStart ( p1, v0 ); // invoke-virtual {v2, p1, v0}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->onMultiTaskActionStart(Lmiui/smartpower/MultiTaskActionManager$ActionInfo;I)V
/* .line 1258 */
} // .end local v0 # "callingPid":I
} // .end local v1 # "callingUid":I
} // :cond_1
return;
} // .end method
public void onPendingPackagesExempt ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 665 */
v0 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v0 ).getAppState ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(Ljava/lang/String;)Ljava/util/List;
/* .line 666 */
/* .local v0, "appStates":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/AppStateManager$AppState;>;" */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Lcom/android/server/am/AppStateManager$AppState; */
/* .line 667 */
/* .local v2, "appState":Lcom/android/server/am/AppStateManager$AppState; */
v3 = this.mComponentsManager;
v4 = (( com.android.server.am.AppStateManager$AppState ) v2 ).getUid ( ); // invoke-virtual {v2}, Lcom/android/server/am/AppStateManager$AppState;->getUid()I
final String v5 = "exempt"; // const-string v5, "exempt"
(( com.miui.server.smartpower.ComponentsManager ) v3 ).onSendPendingIntent ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Lcom/miui/server/smartpower/ComponentsManager;->onSendPendingIntent(ILjava/lang/String;)V
/* .line 668 */
} // .end local v2 # "appState":Lcom/android/server/am/AppStateManager$AppState;
/* .line 669 */
} // :cond_0
return;
} // .end method
public void onPlayerEvent ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "piid" # I */
/* .param p4, "event" # I */
/* .line 286 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 287 */
v0 = this.mAppPowerResourceManager;
(( com.miui.server.smartpower.AppPowerResourceManager ) v0 ).onPlayerEvent ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/smartpower/AppPowerResourceManager;->onPlayerEvent(IIII)V
/* .line 290 */
} // :cond_0
return;
} // .end method
public void onPlayerRlease ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "piid" # I */
/* .line 278 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 279 */
v0 = this.mAppPowerResourceManager;
(( com.miui.server.smartpower.AppPowerResourceManager ) v0 ).onPlayerRlease ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/AppPowerResourceManager;->onPlayerRlease(III)V
/* .line 282 */
} // :cond_0
return;
} // .end method
public void onPlayerTrack ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "piid" # I */
/* .param p4, "sessionId" # I */
/* .line 271 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 272 */
v0 = this.mAppPowerResourceManager;
(( com.miui.server.smartpower.AppPowerResourceManager ) v0 ).onPlayerTrack ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/smartpower/AppPowerResourceManager;->onPlayerTrack(IIII)V
/* .line 274 */
} // :cond_0
return;
} // .end method
public void onProcessKill ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 1 */
/* .param p1, "r" # Lcom/android/server/am/ProcessRecord; */
/* .line 348 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 349 */
v0 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v0 ).processKilledLocked ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager;->processKilledLocked(Lcom/android/server/am/ProcessRecord;)V
/* .line 351 */
} // :cond_0
return;
} // .end method
public void onProcessStart ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 2 */
/* .param p1, "r" # Lcom/android/server/am/ProcessRecord; */
/* .line 340 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 341 */
v0 = this.mAppStateManager;
v1 = this.info;
v1 = this.packageName;
(( com.android.server.am.AppStateManager ) v0 ).processStartedLocked ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/android/server/am/AppStateManager;->processStartedLocked(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
/* .line 343 */
} // :cond_0
return;
} // .end method
public void onProviderConnectionChanged ( com.android.server.am.ContentProviderConnection p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "conn" # Lcom/android/server/am/ContentProviderConnection; */
/* .param p2, "isConnect" # Z */
/* .line 491 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p1 != null) { // if-eqz p1, :cond_0
v0 = this.client;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.provider;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.provider;
v0 = this.proc;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 493 */
v0 = this.mComponentsManager;
v1 = this.client;
v2 = this.provider;
v2 = this.proc;
(( com.miui.server.smartpower.ComponentsManager ) v0 ).providerConnectionChangedLocked ( v1, v2, p2 ); // invoke-virtual {v0, v1, v2, p2}, Lcom/miui/server/smartpower/ComponentsManager;->providerConnectionChangedLocked(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;Z)V
/* .line 496 */
} // :cond_0
return;
} // .end method
public void onRecentsAnimationEnd ( ) {
/* .locals 1 */
/* .line 1165 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1166 */
v0 = this.mSmartDisplayPolicyManager;
(( com.miui.server.smartpower.SmartDisplayPolicyManager ) v0 ).notifyRecentsAnimationEnd ( ); // invoke-virtual {v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyRecentsAnimationEnd()V
/* .line 1168 */
} // :cond_0
return;
} // .end method
public void onRecentsAnimationStart ( com.android.server.wm.ActivityRecord p0 ) {
/* .locals 1 */
/* .param p1, "targetActivity" # Lcom/android/server/wm/ActivityRecord; */
/* .line 1158 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1159 */
v0 = this.mSmartDisplayPolicyManager;
(( com.miui.server.smartpower.SmartDisplayPolicyManager ) v0 ).notifyRecentsAnimationStart ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyRecentsAnimationStart(Lcom/android/server/wm/ActivityRecord;)V
/* .line 1161 */
} // :cond_0
return;
} // .end method
public void onRecorderEvent ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "riid" # I */
/* .param p3, "event" # I */
/* .line 310 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 311 */
v0 = this.mAppPowerResourceManager;
(( com.miui.server.smartpower.AppPowerResourceManager ) v0 ).onRecorderEvent ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/AppPowerResourceManager;->onRecorderEvent(III)V
/* .line 314 */
} // :cond_0
return;
} // .end method
public void onRecorderRlease ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "riid" # I */
/* .line 302 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 303 */
v0 = this.mAppPowerResourceManager;
(( com.miui.server.smartpower.AppPowerResourceManager ) v0 ).onRecorderRlease ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/AppPowerResourceManager;->onRecorderRlease(II)V
/* .line 306 */
} // :cond_0
return;
} // .end method
public void onRecorderTrack ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "riid" # I */
/* .line 294 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 295 */
v0 = this.mAppPowerResourceManager;
(( com.miui.server.smartpower.AppPowerResourceManager ) v0 ).onRecorderTrack ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/AppPowerResourceManager;->onRecorderTrack(III)V
/* .line 298 */
} // :cond_0
return;
} // .end method
public void onReleaseLocation ( Integer p0, Integer p1, android.location.ILocationListener p2 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "listener" # Landroid/location/ILocationListener; */
/* .line 747 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 748 */
v0 = this.mAppPowerResourceManager;
(( com.miui.server.smartpower.AppPowerResourceManager ) v0 ).onReleaseLocation ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/AppPowerResourceManager;->onReleaseLocation(IILandroid/location/ILocationListener;)V
/* .line 750 */
} // :cond_0
return;
} // .end method
public void onReleaseWakelock ( android.os.IBinder p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "lock" # Landroid/os/IBinder; */
/* .param p2, "flags" # I */
/* .line 765 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 766 */
v0 = this.mAppPowerResourceManager;
(( com.miui.server.smartpower.AppPowerResourceManager ) v0 ).onReleaseWakelock ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/AppPowerResourceManager;->onReleaseWakelock(Landroid/os/IBinder;I)V
/* .line 768 */
} // :cond_0
return;
} // .end method
public void onRemoteAnimationEnd ( ) {
/* .locals 1 */
/* .line 1179 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1180 */
v0 = this.mSmartDisplayPolicyManager;
(( com.miui.server.smartpower.SmartDisplayPolicyManager ) v0 ).notifyRemoteAnimationEnd ( ); // invoke-virtual {v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyRemoteAnimationEnd()V
/* .line 1182 */
} // :cond_0
return;
} // .end method
public void onRemoteAnimationStart ( android.view.RemoteAnimationTarget[] p0 ) {
/* .locals 1 */
/* .param p1, "appTargets" # [Landroid/view/RemoteAnimationTarget; */
/* .line 1172 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1173 */
v0 = this.mSmartDisplayPolicyManager;
(( com.miui.server.smartpower.SmartDisplayPolicyManager ) v0 ).notifyRemoteAnimationStart ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyRemoteAnimationStart([Landroid/view/RemoteAnimationTarget;)V
/* .line 1175 */
} // :cond_0
return;
} // .end method
public void onSendPendingIntent ( com.android.server.am.PendingIntentRecord$Key p0, java.lang.String p1, java.lang.String p2, android.content.Intent p3 ) {
/* .locals 4 */
/* .param p1, "key" # Lcom/android/server/am/PendingIntentRecord$Key; */
/* .param p2, "callingPkg" # Ljava/lang/String; */
/* .param p3, "targetPkg" # Ljava/lang/String; */
/* .param p4, "intent" # Landroid/content/Intent; */
/* .line 657 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 658 */
v0 = this.mPackageManager;
/* const-wide/16 v1, 0x0 */
/* iget v3, p1, Lcom/android/server/am/PendingIntentRecord$Key;->userId:I */
v0 = (( android.content.pm.PackageManagerInternal ) v0 ).getPackageUid ( p3, v1, v2, v3 ); // invoke-virtual {v0, p3, v1, v2, v3}, Landroid/content/pm/PackageManagerInternal;->getPackageUid(Ljava/lang/String;JI)I
/* .line 659 */
/* .local v0, "uid":I */
v1 = this.mComponentsManager;
(( com.android.server.am.PendingIntentRecord$Key ) p1 ).typeName ( ); // invoke-virtual {p1}, Lcom/android/server/am/PendingIntentRecord$Key;->typeName()Ljava/lang/String;
(( com.miui.server.smartpower.ComponentsManager ) v1 ).onSendPendingIntent ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Lcom/miui/server/smartpower/ComponentsManager;->onSendPendingIntent(ILjava/lang/String;)V
/* .line 661 */
} // .end local v0 # "uid":I
} // :cond_0
return;
} // .end method
public void onServiceConnectionChanged ( com.android.server.am.AppBindRecord p0, Boolean p1 ) {
/* .locals 8 */
/* .param p1, "bindRecord" # Lcom/android/server/am/AppBindRecord; */
/* .param p2, "isConnect" # Z */
/* .line 463 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
if ( p1 != null) { // if-eqz p1, :cond_3
v0 = this.client;
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = this.service;
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = this.intent;
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 466 */
/* if-nez p2, :cond_0 */
v0 = this.intent;
v0 = this.apps;
v1 = this.client;
v0 = (( android.util.ArrayMap ) v0 ).containsKey ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 467 */
return;
/* .line 469 */
} // :cond_0
v0 = this.service;
v0 = this.app;
/* .line 470 */
/* .local v0, "serviceApp":Lcom/android/server/am/ProcessRecord; */
/* if-nez v0, :cond_2 */
/* .line 472 */
v1 = this.service;
v1 = this.serviceInfo;
/* iget v1, v1, Landroid/content/pm/ServiceInfo;->flags:I */
/* .line 473 */
/* .local v1, "serviceFlag":I */
/* and-int/lit8 v2, v1, 0x2 */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 474 */
v2 = this.service;
v0 = this.isolationHostProc;
/* .line 475 */
} // :cond_1
v2 = this.service;
v2 = this.appInfo;
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 476 */
v2 = this.mAMS;
v3 = this.service;
v3 = this.processName;
v4 = this.service;
v4 = this.appInfo;
/* iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I */
(( com.android.server.am.ActivityManagerService ) v2 ).getProcessRecordLocked ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lcom/android/server/am/ActivityManagerService;->getProcessRecordLocked(Ljava/lang/String;I)Lcom/android/server/am/ProcessRecord;
/* .line 480 */
} // .end local v1 # "serviceFlag":I
} // :cond_2
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_3
/* iget v1, v0, Lcom/android/server/am/ProcessRecord;->mPid:I */
/* if-lez v1, :cond_3 */
/* .line 481 */
v2 = this.mComponentsManager;
v3 = this.client;
v1 = this.intent;
/* .line 482 */
(( com.android.server.am.IntentBindRecord ) v1 ).collectFlags ( ); // invoke-virtual {v1}, Lcom/android/server/am/IntentBindRecord;->collectFlags()J
/* move-result-wide v6 */
/* .line 481 */
/* move-object v4, v0 */
/* move v5, p2 */
/* invoke-virtual/range {v2 ..v7}, Lcom/miui/server/smartpower/ComponentsManager;->serviceConnectionChangedLocked(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;ZJ)V */
/* .line 486 */
} // .end local v0 # "serviceApp":Lcom/android/server/am/ProcessRecord;
} // :cond_3
return;
} // .end method
public void onServiceStatusChanged ( com.android.server.am.ProcessRecord p0, Boolean p1, com.android.server.am.ServiceRecord p2, Integer p3 ) {
/* .locals 6 */
/* .param p1, "processRecord" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "active" # Z */
/* .param p3, "record" # Lcom/android/server/am/ServiceRecord; */
/* .param p4, "execution" # I */
/* .line 425 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
if ( p1 != null) { // if-eqz p1, :cond_2
/* .line 426 */
if ( p2 != null) { // if-eqz p2, :cond_1
int v0 = 2; // const/4 v0, 0x2
/* if-ne p4, v0, :cond_1 */
/* .line 427 */
(( com.android.server.am.ServiceRecord ) p3 ).getConnections ( ); // invoke-virtual {p3}, Lcom/android/server/am/ServiceRecord;->getConnections()Landroid/util/ArrayMap;
(( android.util.ArrayMap ) v0 ).values ( ); // invoke-virtual {v0}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/util/ArrayList; */
/* .line 428 */
/* .local v1, "connList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;" */
(( java.util.ArrayList ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v3 = } // :goto_1
if ( v3 != null) { // if-eqz v3, :cond_0
/* check-cast v3, Lcom/android/server/am/ConnectionRecord; */
/* .line 429 */
/* .local v3, "conn":Lcom/android/server/am/ConnectionRecord; */
v4 = this.binding;
int v5 = 1; // const/4 v5, 0x1
(( com.android.server.am.SmartPowerService ) p0 ).onServiceConnectionChanged ( v4, v5 ); // invoke-virtual {p0, v4, v5}, Lcom/android/server/am/SmartPowerService;->onServiceConnectionChanged(Lcom/android/server/am/AppBindRecord;Z)V
/* .line 430 */
} // .end local v3 # "conn":Lcom/android/server/am/ConnectionRecord;
/* .line 431 */
} // .end local v1 # "connList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ConnectionRecord;>;"
} // :cond_0
/* .line 433 */
} // :cond_1
v0 = this.mComponentsManager;
v1 = this.shortInstanceName;
(( com.miui.server.smartpower.ComponentsManager ) v0 ).serviceStatusChangedLocked ( p1, p2, v1, p4 ); // invoke-virtual {v0, p1, p2, v1, p4}, Lcom/miui/server/smartpower/ComponentsManager;->serviceStatusChangedLocked(Lcom/android/server/am/ProcessRecord;ZLjava/lang/String;I)V
/* .line 437 */
} // :cond_2
return;
} // .end method
public void onShellCommand ( java.io.FileDescriptor p0, java.io.FileDescriptor p1, java.io.FileDescriptor p2, java.lang.String[] p3, android.os.ShellCallback p4, android.os.ResultReceiver p5 ) {
/* .locals 8 */
/* .param p1, "in" # Ljava/io/FileDescriptor; */
/* .param p2, "out" # Ljava/io/FileDescriptor; */
/* .param p3, "err" # Ljava/io/FileDescriptor; */
/* .param p4, "args" # [Ljava/lang/String; */
/* .param p5, "callback" # Landroid/os/ShellCallback; */
/* .param p6, "resultReceiver" # Landroid/os/ResultReceiver; */
/* .line 1403 */
/* new-instance v0, Lcom/android/server/am/SmartPowerService$SmartPowerShellCommand; */
/* invoke-direct {v0, p0}, Lcom/android/server/am/SmartPowerService$SmartPowerShellCommand;-><init>(Lcom/android/server/am/SmartPowerService;)V */
/* .line 1404 */
/* move-object v1, p0 */
/* move-object v2, p1 */
/* move-object v3, p2 */
/* move-object v4, p3 */
/* move-object v5, p4 */
/* move-object v6, p5 */
/* move-object v7, p6 */
/* invoke-virtual/range {v0 ..v7}, Lcom/android/server/am/SmartPowerService$SmartPowerShellCommand;->exec(Landroid/os/Binder;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)I */
/* .line 1405 */
return;
} // .end method
public void onTransitionAnimateEnd ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "type" # I */
/* .line 1298 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1299 */
v0 = /* invoke-direct {p0}, Lcom/android/server/am/SmartPowerService;->checkPermission()Z */
/* if-nez v0, :cond_0 */
/* .line 1300 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Permission Denial: onTransitionAnimateEnd from pid="; // const-string v1, "Permission Denial: onTransitionAnimateEnd from pid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1301 */
v1 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", uid="; // const-string v1, ", uid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1302 */
/* .local v0, "msg":Ljava/lang/String; */
final String v1 = "SmartPower"; // const-string v1, "SmartPower"
android.util.Slog .w ( v1,v0 );
/* .line 1303 */
return;
/* .line 1305 */
} // .end local v0 # "msg":Ljava/lang/String;
} // :cond_0
miui.smartpower.SmartPowerManager .transitionAnimateTypeToString ( p1 );
/* .line 1306 */
/* .local v0, "typeString":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Transit: "; // const-string v2, "Transit: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v2 = 0; // const/4 v2, 0x0
/* const-wide/16 v3, 0x20 */
android.os.Trace .asyncTraceEnd ( v3,v4,v1,v2 );
/* .line 1309 */
} // .end local v0 # "typeString":Ljava/lang/String;
} // :cond_1
return;
} // .end method
public void onTransitionAnimateStart ( Integer p0, android.window.TransitionInfo p1 ) {
/* .locals 5 */
/* .param p1, "type" # I */
/* .param p2, "info" # Landroid/window/TransitionInfo; */
/* .line 1280 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1281 */
v0 = /* invoke-direct {p0}, Lcom/android/server/am/SmartPowerService;->checkPermission()Z */
/* if-nez v0, :cond_0 */
/* .line 1282 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Permission Denial: onTransitionAnimateStart from pid="; // const-string v1, "Permission Denial: onTransitionAnimateStart from pid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1283 */
v1 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", uid="; // const-string v1, ", uid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1284 */
/* .local v0, "msg":Ljava/lang/String; */
final String v1 = "SmartPower"; // const-string v1, "SmartPower"
android.util.Slog .w ( v1,v0 );
/* .line 1285 */
return;
/* .line 1287 */
} // .end local v0 # "msg":Ljava/lang/String;
} // :cond_0
miui.smartpower.SmartPowerManager .transitionAnimateTypeToString ( p1 );
/* .line 1288 */
/* .local v0, "typeString":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Transit: "; // const-string v2, "Transit: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v2 = 0; // const/4 v2, 0x0
/* const-wide/16 v3, 0x20 */
android.os.Trace .asyncTraceBegin ( v3,v4,v1,v2 );
/* .line 1291 */
} // .end local v0 # "typeString":Ljava/lang/String;
} // :cond_1
return;
} // .end method
public void onTransitionEnd ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "syncId" # I */
/* .param p2, "type" # I */
/* .param p3, "flags" # I */
/* .line 1326 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1327 */
v0 = this.mSmartDisplayPolicyManager;
(( com.miui.server.smartpower.SmartDisplayPolicyManager ) v0 ).notifyTransitionEnd ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyTransitionEnd(III)V
/* .line 1329 */
} // :cond_0
return;
} // .end method
public void onTransitionStart ( android.window.TransitionInfo p0 ) {
/* .locals 1 */
/* .param p1, "info" # Landroid/window/TransitionInfo; */
/* .line 1316 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1317 */
v0 = this.mSmartDisplayPolicyManager;
(( com.miui.server.smartpower.SmartDisplayPolicyManager ) v0 ).notifyTransitionStart ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyTransitionStart(Landroid/window/TransitionInfo;)V
/* .line 1319 */
} // :cond_0
return;
} // .end method
public void onUsbStateChanged ( Boolean p0, Long p1, android.content.Intent p2 ) {
/* .locals 2 */
/* .param p1, "isConnected" # Z */
/* .param p2, "functions" # J */
/* .param p4, "intent" # Landroid/content/Intent; */
/* .line 718 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 719 */
v0 = com.android.server.am.SmartPowerService .isUsbDataTransferActive ( p2,p3 );
/* .line 720 */
/* .local v0, "isUsbDataTrans":Z */
v1 = this.mSmartPowerPolicyManager;
(( com.miui.server.smartpower.SmartPowerPolicyManager ) v1 ).onUsbStateChanged ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->onUsbStateChanged(ZZ)V
/* .line 722 */
} // .end local v0 # "isUsbDataTrans":Z
} // :cond_0
return;
} // .end method
public void onVpnChanged ( Boolean p0, Integer p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "active" # Z */
/* .param p2, "uid" # I */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .line 695 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 696 */
v0 = this.mSmartPowerPolicyManager;
(( com.miui.server.smartpower.SmartPowerPolicyManager ) v0 ).onVpnChanged ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->onVpnChanged(ZILjava/lang/String;)V
/* .line 698 */
} // :cond_0
return;
} // .end method
public void onWallpaperComponentChanged ( Boolean p0, Integer p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "active" # Z */
/* .param p2, "uid" # I */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .line 688 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 689 */
v0 = this.mSmartPowerPolicyManager;
(( com.miui.server.smartpower.SmartPowerPolicyManager ) v0 ).onWallpaperComponentChangedLocked ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->onWallpaperComponentChangedLocked(ZILjava/lang/String;)V
/* .line 691 */
} // :cond_0
return;
} // .end method
public void onWindowAnimationStartLocked ( Long p0, android.view.SurfaceControl p1 ) {
/* .locals 1 */
/* .param p1, "animDuration" # J */
/* .param p3, "animationLeash" # Landroid/view/SurfaceControl; */
/* .line 1136 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1137 */
v0 = this.mSmartDisplayPolicyManager;
(( com.miui.server.smartpower.SmartDisplayPolicyManager ) v0 ).notifyWindowAnimationStartLocked ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyWindowAnimationStartLocked(JLandroid/view/SurfaceControl;)V
/* .line 1140 */
} // :cond_0
return;
} // .end method
public void onWindowVisibilityChanged ( Integer p0, Integer p1, com.android.server.policy.WindowManagerPolicy$WindowState p2, android.view.WindowManager$LayoutParams p3, Boolean p4 ) {
/* .locals 7 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "win" # Lcom/android/server/policy/WindowManagerPolicy$WindowState; */
/* .param p4, "attrs" # Landroid/view/WindowManager$LayoutParams; */
/* .param p5, "visible" # Z */
/* .line 631 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 632 */
v1 = this.mComponentsManager;
/* move v2, p1 */
/* move v3, p2 */
/* move-object v4, p3 */
/* move-object v5, p4 */
/* move v6, p5 */
/* invoke-virtual/range {v1 ..v6}, Lcom/miui/server/smartpower/ComponentsManager;->windowVisibilityChangedLocked(IILcom/android/server/policy/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;Z)V */
/* .line 633 */
v0 = this.mSmartDisplayPolicyManager;
(( com.miui.server.smartpower.SmartDisplayPolicyManager ) v0 ).notifyWindowVisibilityChangedLocked ( p3, p4, p5 ); // invoke-virtual {v0, p3, p4, p5}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->notifyWindowVisibilityChangedLocked(Lcom/android/server/policy/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;Z)V
/* .line 635 */
} // :cond_0
return;
} // .end method
public void playbackStateChanged ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "oldState" # I */
/* .param p4, "newState" # I */
/* .line 249 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 250 */
v0 = this.mAppPowerResourceManager;
(( com.miui.server.smartpower.AppPowerResourceManager ) v0 ).playbackStateChanged ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/smartpower/AppPowerResourceManager;->playbackStateChanged(IIII)V
/* .line 253 */
} // :cond_0
return;
} // .end method
public void powerFrozenServiceReady ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "ready" # Z */
/* .line 554 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 555 */
v0 = this.mPowerFrozenManager;
(( com.miui.server.smartpower.PowerFrozenManager ) v0 ).serviceReady ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/PowerFrozenManager;->serviceReady(Z)V
/* .line 557 */
} // :cond_0
return;
} // .end method
public void recordAudioFocus ( Integer p0, Integer p1, java.lang.String p2, Boolean p3 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "clientId" # Ljava/lang/String; */
/* .param p4, "request" # Z */
/* .line 257 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 258 */
v0 = this.mAppPowerResourceManager;
(( com.miui.server.smartpower.AppPowerResourceManager ) v0 ).recordAudioFocus ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/smartpower/AppPowerResourceManager;->recordAudioFocus(IILjava/lang/String;Z)V
/* .line 260 */
} // :cond_0
return;
} // .end method
public void recordAudioFocusLoss ( Integer p0, java.lang.String p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "clientId" # Ljava/lang/String; */
/* .param p3, "focusLoss" # I */
/* .line 264 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 265 */
v0 = this.mAppPowerResourceManager;
(( com.miui.server.smartpower.AppPowerResourceManager ) v0 ).recordAudioFocusLoss ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/AppPowerResourceManager;->recordAudioFocusLoss(ILjava/lang/String;I)V
/* .line 267 */
} // :cond_0
return;
} // .end method
public void registThermalScenarioCallback ( miui.smartpower.IScenarioCallback p0 ) {
/* .locals 2 */
/* .param p1, "callback" # Lmiui/smartpower/IScenarioCallback; */
/* .line 228 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 229 */
v0 = /* invoke-direct {p0}, Lcom/android/server/am/SmartPowerService;->checkPermission()Z */
/* if-nez v0, :cond_0 */
/* .line 230 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Permission Denial: registThermalScenarioCallback from pid="; // const-string v1, "Permission Denial: registThermalScenarioCallback from pid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 231 */
v1 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", uid="; // const-string v1, ", uid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 232 */
/* .local v0, "msg":Ljava/lang/String; */
final String v1 = "SmartPower"; // const-string v1, "SmartPower"
android.util.Slog .w ( v1,v0 );
/* .line 233 */
return;
/* .line 235 */
} // .end local v0 # "msg":Ljava/lang/String;
} // :cond_0
v0 = this.mSmartThermalPolicyManager;
(( com.miui.server.smartpower.SmartThermalPolicyManager ) v0 ).registThermalScenarioCallback ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/SmartThermalPolicyManager;->registThermalScenarioCallback(Lmiui/smartpower/IScenarioCallback;)V
/* .line 237 */
} // :cond_1
return;
} // .end method
protected void registerAppStateListener ( com.android.server.am.AppStateManager$IProcessStateCallback p0 ) {
/* .locals 1 */
/* .param p1, "callback" # Lcom/android/server/am/AppStateManager$IProcessStateCallback; */
/* .line 1093 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1094 */
v0 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v0 ).registerAppStateListener ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager;->registerAppStateListener(Lcom/android/server/am/AppStateManager$IProcessStateCallback;)Z
/* .line 1096 */
} // :cond_0
return;
} // .end method
public Boolean registerMultiTaskActionListener ( Integer p0, android.os.Handler p1, com.miui.app.smartpower.SmartPowerServiceInternal$MultiTaskActionListener p2 ) {
/* .locals 1 */
/* .param p1, "listenerFlag" # I */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .param p3, "listener" # Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener; */
/* .line 1225 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1226 */
v0 = this.mSmartWindowPolicyManager;
v0 = (( com.miui.server.smartpower.SmartWindowPolicyManager ) v0 ).registerMultiTaskActionListener ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->registerMultiTaskActionListener(ILandroid/os/Handler;Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;)Z
/* .line 1229 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void removeFrozenPid ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 605 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 606 */
v0 = this.mPowerFrozenManager;
(( com.miui.server.smartpower.PowerFrozenManager ) v0 ).removeFrozenPid ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/PowerFrozenManager;->removeFrozenPid(II)V
/* .line 608 */
} // :cond_0
return;
} // .end method
public void reportBinderState ( Integer p0, Integer p1, Integer p2, Integer p3, Long p4 ) {
/* .locals 8 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "tid" # I */
/* .param p4, "binderState" # I */
/* .param p5, "now" # J */
/* .line 584 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 585 */
v1 = this.mPowerFrozenManager;
/* move v2, p1 */
/* move v3, p2 */
/* move v4, p3 */
/* move v5, p4 */
/* move-wide v6, p5 */
/* invoke-virtual/range {v1 ..v7}, Lcom/miui/server/smartpower/PowerFrozenManager;->reportBinderState(IIIIJ)V */
/* .line 587 */
} // :cond_0
return;
} // .end method
public void reportBinderTrans ( Integer p0, Integer p1, Integer p2, Integer p3, Integer p4, Boolean p5, Long p6, Long p7 ) {
/* .locals 13 */
/* .param p1, "dstUid" # I */
/* .param p2, "dstPid" # I */
/* .param p3, "callerUid" # I */
/* .param p4, "callerPid" # I */
/* .param p5, "callerTid" # I */
/* .param p6, "isOneway" # Z */
/* .param p7, "now" # J */
/* .param p9, "buffer" # J */
/* .line 576 */
/* move-object v0, p0 */
/* iget-boolean v1, v0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 577 */
v2 = this.mPowerFrozenManager;
/* move v3, p1 */
/* move v4, p2 */
/* move/from16 v5, p3 */
/* move/from16 v6, p4 */
/* move/from16 v7, p5 */
/* move/from16 v8, p6 */
/* move-wide/from16 v9, p7 */
/* move-wide/from16 v11, p9 */
/* invoke-virtual/range {v2 ..v12}, Lcom/miui/server/smartpower/PowerFrozenManager;->reportBinderTrans(IIIIIZJJ)V */
/* .line 580 */
} // :cond_0
return;
} // .end method
public void reportNet ( Integer p0, Long p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "now" # J */
/* .line 568 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 569 */
v0 = this.mPowerFrozenManager;
(( com.miui.server.smartpower.PowerFrozenManager ) v0 ).reportNet ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/PowerFrozenManager;->reportNet(IJ)V
/* .line 571 */
} // :cond_0
return;
} // .end method
public void reportSignal ( Integer p0, Integer p1, Long p2 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "now" # J */
/* .line 561 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 562 */
v0 = this.mPowerFrozenManager;
(( com.miui.server.smartpower.PowerFrozenManager ) v0 ).reportSignal ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/smartpower/PowerFrozenManager;->reportSignal(IIJ)V
/* .line 564 */
} // :cond_0
return;
} // .end method
public void reportTrackStatus ( Integer p0, Integer p1, Integer p2, Boolean p3 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "sessionId" # I */
/* .param p4, "isMuted" # Z */
/* .line 804 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 805 */
v0 = this.mAppPowerResourceManager;
(( com.miui.server.smartpower.AppPowerResourceManager ) v0 ).reportTrackStatus ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/smartpower/AppPowerResourceManager;->reportTrackStatus(IIIZ)V
/* .line 807 */
} // :cond_0
return;
} // .end method
public void setProcessPss ( com.android.server.am.ProcessRecord p0, Long p1, Long p2 ) {
/* .locals 7 */
/* .param p1, "r" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "pss" # J */
/* .param p4, "swapPss" # J */
/* .line 363 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 364 */
v1 = this.mAppStateManager;
/* move-object v2, p1 */
/* move-wide v3, p2 */
/* move-wide v5, p4 */
/* invoke-virtual/range {v1 ..v6}, Lcom/android/server/am/AppStateManager;->setProcessPss(Lcom/android/server/am/ProcessRecord;JJ)V */
/* .line 366 */
} // :cond_0
return;
} // .end method
public Boolean shouldInterceptAlarm ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "type" # I */
/* .line 509 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 510 */
v0 = this.mSmartPowerPolicyManager;
v0 = (( com.miui.server.smartpower.SmartPowerPolicyManager ) v0 ).shouldInterceptAlarmLocked ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->shouldInterceptAlarmLocked(II)Z
/* .line 512 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean shouldInterceptBroadcast ( com.android.server.am.ProcessRecord p0, com.android.server.am.BroadcastRecord p1 ) {
/* .locals 9 */
/* .param p1, "recProc" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "record" # Lcom/android/server/am/BroadcastRecord; */
/* .line 518 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 519 */
v1 = this.mSmartPowerPolicyManager;
/* iget v2, p2, Lcom/android/server/am/BroadcastRecord;->callingUid:I */
/* iget v3, p2, Lcom/android/server/am/BroadcastRecord;->callingPid:I */
v4 = this.callerPackage;
v6 = this.intent;
/* iget-boolean v7, p2, Lcom/android/server/am/BroadcastRecord;->ordered:Z */
/* iget-boolean v8, p2, Lcom/android/server/am/BroadcastRecord;->sticky:Z */
/* move-object v5, p1 */
v0 = /* invoke-virtual/range {v1 ..v8}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->shouldInterceptBroadcastLocked(IILjava/lang/String;Lcom/android/server/am/ProcessRecord;Landroid/content/Intent;ZZ)Z */
/* .line 523 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean shouldInterceptProvider ( Integer p0, com.android.server.am.ProcessRecord p1, com.android.server.am.ProcessRecord p2, Boolean p3 ) {
/* .locals 1 */
/* .param p1, "callingUid" # I */
/* .param p2, "callingProc" # Lcom/android/server/am/ProcessRecord; */
/* .param p3, "hostingProc" # Lcom/android/server/am/ProcessRecord; */
/* .param p4, "isRunning" # Z */
/* .line 443 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p3 != null) { // if-eqz p3, :cond_0
/* .line 444 */
v0 = this.mSmartPowerPolicyManager;
/* .line 445 */
v0 = (( com.miui.server.smartpower.SmartPowerPolicyManager ) v0 ).shouldInterceptProviderLocked ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->shouldInterceptProviderLocked(ILcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;Z)Z
/* .line 444 */
/* .line 447 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean shouldInterceptService ( android.content.Intent p0, miui.security.CallerInfo p1, android.content.pm.ServiceInfo p2 ) {
/* .locals 1 */
/* .param p1, "service" # Landroid/content/Intent; */
/* .param p2, "callingInfo" # Lmiui/security/CallerInfo; */
/* .param p3, "serviceInfo" # Landroid/content/pm/ServiceInfo; */
/* .line 537 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p2 != null) { // if-eqz p2, :cond_0
if ( p3 != null) { // if-eqz p3, :cond_0
v0 = this.applicationInfo;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 539 */
v0 = this.mSmartPowerPolicyManager;
v0 = (( com.miui.server.smartpower.SmartPowerPolicyManager ) v0 ).shouldInterceptService ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->shouldInterceptService(Landroid/content/Intent;Lmiui/security/CallerInfo;Landroid/content/pm/ServiceInfo;)Z
/* .line 541 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean shouldInterceptUpdateDisplayModeSpecs ( Integer p0, com.android.server.display.mode.DisplayModeDirector$DesiredDisplayModeSpecs p1 ) {
/* .locals 1 */
/* .param p1, "displayId" # I */
/* .param p2, "modeSpecs" # Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs; */
/* .line 1212 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1213 */
v0 = this.mSmartDisplayPolicyManager;
/* .line 1214 */
v0 = (( com.miui.server.smartpower.SmartDisplayPolicyManager ) v0 ).shouldInterceptUpdateDisplayModeSpecs ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->shouldInterceptUpdateDisplayModeSpecs(ILcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;)Z
/* .line 1213 */
/* .line 1216 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean skipFrozenAppAnr ( android.content.pm.ApplicationInfo p0, Integer p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "info" # Landroid/content/pm/ApplicationInfo; */
/* .param p2, "uid" # I */
/* .param p3, "report" # Ljava/lang/String; */
/* .line 528 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 529 */
v0 = this.mSmartPowerPolicyManager;
v0 = (( com.miui.server.smartpower.SmartPowerPolicyManager ) v0 ).skipFrozenAppAnr ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->skipFrozenAppAnr(Landroid/content/pm/ApplicationInfo;ILjava/lang/String;)Z
/* .line 531 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
void systemReady ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 156 */
this.mContext = p1;
/* .line 157 */
v0 = this.mHandlerTh;
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 158 */
android.app.ActivityManager .getService ( );
/* check-cast v0, Lcom/android/server/am/ActivityManagerService; */
this.mAMS = v0;
/* .line 159 */
android.app.ActivityTaskManager .getService ( );
/* check-cast v0, Lcom/android/server/wm/ActivityTaskManagerService; */
this.mATMS = v0;
/* .line 160 */
/* const-string/jumbo v0, "window" */
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/WindowManagerService; */
this.mWMS = v0;
/* .line 161 */
/* const-class v0, Landroid/content/pm/PackageManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Landroid/content/pm/PackageManagerInternal; */
this.mPackageManager = v0;
/* .line 162 */
/* new-instance v0, Lcom/android/server/am/SmartPowerService$MyHandler; */
v1 = this.mHandlerTh;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/am/SmartPowerService$MyHandler;-><init>(Lcom/android/server/am/SmartPowerService;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 163 */
v0 = this.mHandlerTh;
v0 = (( android.os.HandlerThread ) v0 ).getThreadId ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->getThreadId()I
int v1 = 1; // const/4 v1, 0x1
android.os.Process .setThreadGroupAndCpuset ( v0,v1 );
/* .line 165 */
/* new-instance v0, Lcom/miui/server/smartpower/PowerFrozenManager; */
/* invoke-direct {v0}, Lcom/miui/server/smartpower/PowerFrozenManager;-><init>()V */
this.mPowerFrozenManager = v0;
/* .line 166 */
/* new-instance v0, Lcom/miui/server/smartpower/SmartPowerPolicyManager; */
v2 = this.mHandlerTh;
/* .line 167 */
(( android.os.HandlerThread ) v2 ).getLooper ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
v3 = this.mAMS;
v4 = this.mPowerFrozenManager;
/* invoke-direct {v0, p1, v2, v3, v4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/android/server/am/ActivityManagerService;Lcom/miui/server/smartpower/PowerFrozenManager;)V */
this.mSmartPowerPolicyManager = v0;
/* .line 168 */
/* new-instance v0, Lcom/miui/server/smartpower/AppPowerResourceManager; */
v2 = this.mHandlerTh;
(( android.os.HandlerThread ) v2 ).getLooper ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p1, v2}, Lcom/miui/server/smartpower/AppPowerResourceManager;-><init>(Landroid/content/Context;Landroid/os/Looper;)V */
this.mAppPowerResourceManager = v0;
/* .line 169 */
/* new-instance v0, Lcom/android/server/am/AppStateManager; */
v2 = this.mHandlerTh;
(( android.os.HandlerThread ) v2 ).getLooper ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
v3 = this.mAMS;
v4 = this.mPowerFrozenManager;
/* invoke-direct {v0, p1, v2, v3, v4}, Lcom/android/server/am/AppStateManager;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/android/server/am/ActivityManagerService;Lcom/miui/server/smartpower/PowerFrozenManager;)V */
this.mAppStateManager = v0;
/* .line 170 */
/* new-instance v0, Lcom/miui/server/smartpower/ComponentsManager; */
v2 = this.mHandlerTh;
(( android.os.HandlerThread ) v2 ).getLooper ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p1, v2}, Lcom/miui/server/smartpower/ComponentsManager;-><init>(Landroid/content/Context;Landroid/os/Looper;)V */
this.mComponentsManager = v0;
/* .line 171 */
/* new-instance v0, Lcom/miui/server/smartpower/SmartCpuPolicyManager; */
v2 = this.mAMS;
/* invoke-direct {v0, p1, v2}, Lcom/miui/server/smartpower/SmartCpuPolicyManager;-><init>(Landroid/content/Context;Lcom/android/server/am/ActivityManagerService;)V */
this.mSmartCpuPolicyManager = v0;
/* .line 172 */
/* new-instance v0, Lcom/miui/server/smartpower/SmartScenarioManager; */
v2 = this.mHandlerTh;
(( android.os.HandlerThread ) v2 ).getLooper ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p1, v2}, Lcom/miui/server/smartpower/SmartScenarioManager;-><init>(Landroid/content/Context;Landroid/os/Looper;)V */
this.mSmartScenarioManager = v0;
/* .line 173 */
/* new-instance v2, Lcom/miui/server/smartpower/SmartThermalPolicyManager; */
/* invoke-direct {v2, v0}, Lcom/miui/server/smartpower/SmartThermalPolicyManager;-><init>(Lcom/miui/server/smartpower/SmartScenarioManager;)V */
this.mSmartThermalPolicyManager = v2;
/* .line 174 */
/* new-instance v0, Lcom/miui/server/smartpower/SmartDisplayPolicyManager; */
v2 = this.mWMS;
/* invoke-direct {v0, p1, v2}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;-><init>(Landroid/content/Context;Lcom/android/server/wm/WindowManagerService;)V */
this.mSmartDisplayPolicyManager = v0;
/* .line 175 */
/* new-instance v0, Lcom/miui/server/smartpower/SmartWindowPolicyManager; */
v2 = this.mHandlerTh;
/* .line 176 */
(( android.os.HandlerThread ) v2 ).getLooper ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
v3 = this.mWMS;
/* invoke-direct {v0, p1, v2, v3}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/android/server/wm/WindowManagerService;)V */
this.mSmartWindowPolicyManager = v0;
/* .line 177 */
/* new-instance v0, Lcom/miui/server/smartpower/SmartBoostPolicyManager; */
v2 = this.mHandlerTh;
(( android.os.HandlerThread ) v2 ).getLooper ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p1, v2}, Lcom/miui/server/smartpower/SmartBoostPolicyManager;-><init>(Landroid/content/Context;Landroid/os/Looper;)V */
this.mSmartBoostPolicyManager = v0;
/* .line 179 */
v0 = this.mAppStateManager;
v2 = this.mAppPowerResourceManager;
v3 = this.mSmartPowerPolicyManager;
v4 = this.mSmartScenarioManager;
(( com.android.server.am.AppStateManager ) v0 ).init ( v2, v3, v4 ); // invoke-virtual {v0, v2, v3, v4}, Lcom/android/server/am/AppStateManager;->init(Lcom/miui/server/smartpower/AppPowerResourceManager;Lcom/miui/server/smartpower/SmartPowerPolicyManager;Lcom/miui/server/smartpower/SmartScenarioManager;)V
/* .line 181 */
v0 = this.mAppPowerResourceManager;
(( com.miui.server.smartpower.AppPowerResourceManager ) v0 ).init ( ); // invoke-virtual {v0}, Lcom/miui/server/smartpower/AppPowerResourceManager;->init()V
/* .line 182 */
v0 = this.mSmartPowerPolicyManager;
v2 = this.mAppStateManager;
v3 = this.mAppPowerResourceManager;
(( com.miui.server.smartpower.SmartPowerPolicyManager ) v0 ).init ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->init(Lcom/android/server/am/AppStateManager;Lcom/miui/server/smartpower/AppPowerResourceManager;)V
/* .line 183 */
v0 = this.mComponentsManager;
v2 = this.mAppStateManager;
v3 = this.mSmartPowerPolicyManager;
(( com.miui.server.smartpower.ComponentsManager ) v0 ).init ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lcom/miui/server/smartpower/ComponentsManager;->init(Lcom/android/server/am/AppStateManager;Lcom/miui/server/smartpower/SmartPowerPolicyManager;)V
/* .line 184 */
v0 = this.mSmartCpuPolicyManager;
v2 = this.mAppStateManager;
v3 = this.mSmartPowerPolicyManager;
(( com.miui.server.smartpower.SmartCpuPolicyManager ) v0 ).init ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->init(Lcom/android/server/am/AppStateManager;Lcom/miui/server/smartpower/SmartPowerPolicyManager;)V
/* .line 185 */
v0 = this.mSmartDisplayPolicyManager;
v2 = this.mSmartScenarioManager;
v3 = this.mSmartWindowPolicyManager;
(( com.miui.server.smartpower.SmartDisplayPolicyManager ) v0 ).init ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->init(Lcom/miui/server/smartpower/SmartScenarioManager;Lcom/miui/server/smartpower/SmartWindowPolicyManager;)V
/* .line 186 */
v0 = this.mSmartBoostPolicyManager;
(( com.miui.server.smartpower.SmartBoostPolicyManager ) v0 ).init ( ); // invoke-virtual {v0}, Lcom/miui/server/smartpower/SmartBoostPolicyManager;->init()V
/* .line 187 */
v0 = this.mSmartWindowPolicyManager;
v2 = this.mSmartDisplayPolicyManager;
v3 = this.mSmartBoostPolicyManager;
(( com.miui.server.smartpower.SmartWindowPolicyManager ) v0 ).init ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->init(Lcom/miui/server/smartpower/SmartDisplayPolicyManager;Lcom/miui/server/smartpower/SmartBoostPolicyManager;)V
/* .line 189 */
v0 = this.mContext;
/* invoke-direct {p0, v0}, Lcom/android/server/am/SmartPowerService;->registerCloudObserver(Landroid/content/Context;)V */
/* .line 190 */
/* iput-boolean v1, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
/* .line 192 */
v0 = this.mSmartDisplayPolicyManager;
(( com.miui.server.smartpower.SmartDisplayPolicyManager ) v0 ).systemReady ( ); // invoke-virtual {v0}, Lcom/miui/server/smartpower/SmartDisplayPolicyManager;->systemReady()V
/* .line 193 */
v0 = this.mSmartBoostPolicyManager;
(( com.miui.server.smartpower.SmartBoostPolicyManager ) v0 ).systemReady ( ); // invoke-virtual {v0}, Lcom/miui/server/smartpower/SmartBoostPolicyManager;->systemReady()V
/* .line 194 */
return;
} // .end method
public void thawedByOther ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 598 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 599 */
v0 = this.mPowerFrozenManager;
(( com.miui.server.smartpower.PowerFrozenManager ) v0 ).thawedByOther ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/PowerFrozenManager;->thawedByOther(II)V
/* .line 601 */
} // :cond_0
return;
} // .end method
public void uidAudioStatusChanged ( Integer p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "active" # Z */
/* .line 325 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 326 */
v0 = this.mAppPowerResourceManager;
(( com.miui.server.smartpower.AppPowerResourceManager ) v0 ).uidAudioStatusChanged ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/AppPowerResourceManager;->uidAudioStatusChanged(IZ)V
/* .line 328 */
} // :cond_0
return;
} // .end method
public void uidVideoStatusChanged ( Integer p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "active" # Z */
/* .line 332 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 333 */
v0 = this.mAppPowerResourceManager;
(( com.miui.server.smartpower.AppPowerResourceManager ) v0 ).uidVideoStatusChanged ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/AppPowerResourceManager;->uidVideoStatusChanged(IZ)V
/* .line 335 */
} // :cond_0
return;
} // .end method
protected void unRegisterAppStateListener ( com.android.server.am.AppStateManager$IProcessStateCallback p0 ) {
/* .locals 1 */
/* .param p1, "callback" # Lcom/android/server/am/AppStateManager$IProcessStateCallback; */
/* .line 1099 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1100 */
v0 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v0 ).unRegisterAppStateListener ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager;->unRegisterAppStateListener(Lcom/android/server/am/AppStateManager$IProcessStateCallback;)V
/* .line 1102 */
} // :cond_0
return;
} // .end method
public Boolean unregisterMultiTaskActionListener ( Integer p0, com.miui.app.smartpower.SmartPowerServiceInternal$MultiTaskActionListener p1 ) {
/* .locals 1 */
/* .param p1, "listenerFlag" # I */
/* .param p2, "listener" # Lcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener; */
/* .line 1238 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1239 */
v0 = this.mSmartWindowPolicyManager;
v0 = (( com.miui.server.smartpower.SmartWindowPolicyManager ) v0 ).unregisterMultiTaskActionListener ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/smartpower/SmartWindowPolicyManager;->unregisterMultiTaskActionListener(ILcom/miui/app/smartpower/SmartPowerServiceInternal$MultiTaskActionListener;)Z
/* .line 1242 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void updateActivitiesVisibility ( ) {
/* .locals 1 */
/* .line 547 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 548 */
v0 = this.mComponentsManager;
(( com.miui.server.smartpower.ComponentsManager ) v0 ).updateActivitiesVisibilityLocked ( ); // invoke-virtual {v0}, Lcom/miui/server/smartpower/ComponentsManager;->updateActivitiesVisibilityLocked()V
/* .line 550 */
} // :cond_0
return;
} // .end method
public java.util.ArrayList updateAllAppUsageStats ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/miui/server/smartpower/IAppState;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 935 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 936 */
v0 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v0 ).updateAllAppUsageStats ( ); // invoke-virtual {v0}, Lcom/android/server/am/AppStateManager;->updateAllAppUsageStats()Ljava/util/ArrayList;
/* .line 938 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Long updateCpuStatsNow ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "isReset" # Z */
/* .line 1070 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1071 */
v0 = this.mSmartCpuPolicyManager;
(( com.miui.server.smartpower.SmartCpuPolicyManager ) v0 ).updateCpuStatsNow ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/SmartCpuPolicyManager;->updateCpuStatsNow(Z)J
/* move-result-wide v0 */
/* return-wide v0 */
/* .line 1073 */
} // :cond_0
/* const-wide/16 v0, 0x0 */
/* return-wide v0 */
} // .end method
public void updateProcess ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 1 */
/* .param p1, "r" # Lcom/android/server/am/ProcessRecord; */
/* .line 356 */
/* iget-boolean v0, p0, Lcom/android/server/am/SmartPowerService;->mSystemReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 357 */
v0 = this.mAppStateManager;
(( com.android.server.am.AppStateManager ) v0 ).updateProcessLocked ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager;->updateProcessLocked(Lcom/android/server/am/ProcessRecord;)V
/* .line 359 */
} // :cond_0
return;
} // .end method
