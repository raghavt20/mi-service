.class public Lcom/android/server/am/MemoryFreezeStubImpl;
.super Ljava/lang/Object;
.source "MemoryFreezeStubImpl.java"

# interfaces
.implements Lcom/android/server/am/MemoryFreezeStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/MemoryFreezeStubImpl$BinderService;,
        Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;,
        Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;,
        Lcom/android/server/am/MemoryFreezeStubImpl$MemFreezeShellCmd;
    }
.end annotation


# static fields
.field private static final CLOUD_MEMFREEZE_ENABLE:Ljava/lang/String; = "cloud_memFreeze_control"

.field private static DEBUG:Z = false

.field private static final DEFAULT_FREEZER_DEBOUNCE_TIMEOUT:J = 0x1d4c0L

.field private static final EXTM_ENABLE_PROP:Ljava/lang/String; = "persist.miui.extm.enable"

.field private static final FLUSH_STAT_INDEX:I = 0x0

.field private static final FLUSH_STAT_PATH:Ljava/lang/String; = "/sys/block/zram0/mfz_compr_data_size"

.field private static final MEMFREEZE_DEBUG_PROP:Ljava/lang/String; = "persist.miui.extm.debug_enable"

.field private static final MEMFREEZE_ENABLE_PROP:Ljava/lang/String; = "persist.sys.mfz.enable"

.field private static final MEMFREEZE_POLICY_PROP:Ljava/lang/String; = "persist.sys.mfz.mem_policy"

.field private static final MSG_CHECK_KILL:I = 0x6

.field private static final MSG_CHECK_RECLAIM:I = 0x4

.field private static final MSG_CHECK_UNUSED:I = 0x3

.field private static final MSG_CHECK_WRITE_BACK:I = 0x5

.field private static final MSG_REGISTER_CLOUD_OBSERVER:I = 0x1

.field private static final MSG_REGISTER_MIUIFREEZE_OBSERVER:I = 0x2

.field private static final MSG_REGISTER_WHITE_LIST_OBSERVER:I = 0x7

.field private static final PROCESS_FREEZE_LIST:Ljava/lang/String; = "miui_freeze"

.field private static final TAG:Ljava/lang/String; = "MFZ"

.field private static final WHITELIST_CLOUD_PATH:Ljava/lang/String; = "/data/system/mfz.xml"

.field private static final WHITELIST_DEFAULT_PATH:Ljava/lang/String; = "/product/etc/mfz.xml"

.field private static sBlackListApp:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sThreshholdKill:J

.field private static sThreshholdReclaim:J

.field private static sThreshholdWriteback:J

.field private static sWhiteListApp:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private volatile extmEnable:Z

.field private isInit:Z

.field private mAMS:Lcom/android/server/am/ActivityManagerService;

.field private mATMS:Lcom/android/server/wm/ActivityTaskManagerService;

.field private mBinderService:Lcom/android/server/am/MemoryFreezeStubImpl$BinderService;

.field private mContext:Landroid/content/Context;

.field private volatile mEnable:Z

.field private volatile mFreezerDebounceTimeout:J

.field public mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mKillingUID:I

.field private mLastStartPackage:Ljava/lang/String;

.field private mLastStartPackageTime:J

.field private mMemoryFreezeCloud:Lcom/android/server/am/MemoryFreezeCloud;

.field private mPMS:Lcom/android/server/am/ProcessManagerService;

.field private mProcessFrozenUid:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mRunningPackages:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Integer;",
            "Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mUnusedCheckTime:J

.field private mUnusedKillTime:J

.field private mWMInternal:Lcom/android/server/wm/WindowManagerInternal;

.field private storageManager:Landroid/os/storage/IStorageManager;


# direct methods
.method public static synthetic $r8$lambda$XB3BsXcRhHSs1Ucjkb71JqOB1H0(Lcom/android/server/am/MemoryFreezeStubImpl;JLjava/util/List;Ljava/lang/Integer;Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/android/server/am/MemoryFreezeStubImpl;->lambda$checkUnused$0(JLjava/util/List;Ljava/lang/Integer;Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/am/MemoryFreezeStubImpl;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMemoryFreezeCloud(Lcom/android/server/am/MemoryFreezeStubImpl;)Lcom/android/server/am/MemoryFreezeCloud;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mMemoryFreezeCloud:Lcom/android/server/am/MemoryFreezeCloud;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmEnable(Lcom/android/server/am/MemoryFreezeStubImpl;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mEnable:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mcheckAndKill(Lcom/android/server/am/MemoryFreezeStubImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->checkAndKill()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mcheckAndReclaim(Lcom/android/server/am/MemoryFreezeStubImpl;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/MemoryFreezeStubImpl;->checkAndReclaim(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mcheckAndWriteBack(Lcom/android/server/am/MemoryFreezeStubImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->checkAndWriteBack()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mcheckUnused(Lcom/android/server/am/MemoryFreezeStubImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->checkUnused()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mdump(Lcom/android/server/am/MemoryFreezeStubImpl;Ljava/io/PrintWriter;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/MemoryFreezeStubImpl;->dump(Ljava/io/PrintWriter;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 84
    const-wide/32 v0, 0x200000

    sput-wide v0, Lcom/android/server/am/MemoryFreezeStubImpl;->sThreshholdReclaim:J

    .line 85
    const-wide/32 v0, 0x300000

    sput-wide v0, Lcom/android/server/am/MemoryFreezeStubImpl;->sThreshholdWriteback:J

    .line 86
    const-wide/32 v0, 0x400000

    sput-wide v0, Lcom/android/server/am/MemoryFreezeStubImpl;->sThreshholdKill:J

    .line 88
    const-string v0, "persist.miui.extm.debug_enable"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/am/MemoryFreezeStubImpl;->DEBUG:Z

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/am/MemoryFreezeStubImpl;->sWhiteListApp:Ljava/util/List;

    .line 90
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/am/MemoryFreezeStubImpl;->sBlackListApp:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    const-string v0, "persist.miui.extm.enable"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    move v2, v1

    :goto_0
    iput-boolean v2, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->extmEnable:Z

    .line 93
    const-string v0, "persist.sys.mfz.enable"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mEnable:Z

    .line 94
    const-wide/32 v2, 0x1d4c0

    iput-wide v2, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mFreezerDebounceTimeout:J

    .line 95
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mAMS:Lcom/android/server/am/ActivityManagerService;

    .line 96
    iput-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mWMInternal:Lcom/android/server/wm/WindowManagerInternal;

    .line 97
    iput-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mATMS:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 98
    iput-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mPMS:Lcom/android/server/am/ProcessManagerService;

    .line 99
    iput-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->storageManager:Landroid/os/storage/IStorageManager;

    .line 100
    iput-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mBinderService:Lcom/android/server/am/MemoryFreezeStubImpl$BinderService;

    .line 101
    iput-boolean v1, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->isInit:Z

    .line 102
    iput-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mContext:Landroid/content/Context;

    .line 103
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mRunningPackages:Ljava/util/HashMap;

    .line 104
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "MFZ"

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandlerThread:Landroid/os/HandlerThread;

    .line 105
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mProcessFrozenUid:Ljava/util/List;

    .line 106
    iput-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    .line 107
    iput-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mMemoryFreezeCloud:Lcom/android/server/am/MemoryFreezeCloud;

    .line 108
    iput-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mLastStartPackage:Ljava/lang/String;

    .line 109
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mLastStartPackageTime:J

    .line 110
    const-wide/32 v0, 0x240c8400

    iput-wide v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mUnusedCheckTime:J

    .line 111
    const-wide v0, 0x90321000L

    iput-wide v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mUnusedKillTime:J

    .line 112
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mKillingUID:I

    return-void
.end method

.method private checkAndKill()V
    .locals 11

    .line 485
    invoke-direct {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->countBgMem()J

    move-result-wide v0

    sget-wide v2, Lcom/android/server/am/MemoryFreezeStubImpl;->sThreshholdKill:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 486
    const-string v0, "MFZ"

    const-string v1, "don\'t need to kill."

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 487
    return-void

    .line 490
    :cond_0
    const/4 v0, -0x1

    .line 491
    .local v0, "needKillUid":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mFreezerDebounceTimeout:J

    sub-long/2addr v1, v3

    .line 492
    .local v1, "needKillLastStopTime":J
    iget-object v3, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mRunningPackages:Ljava/util/HashMap;

    monitor-enter v3

    .line 493
    :try_start_0
    iget-object v4, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mRunningPackages:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 494
    .local v5, "uid":I
    iget-object v6, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mRunningPackages:Ljava/util/HashMap;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;

    .line 495
    .local v6, "target":Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;
    invoke-static {v6}, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->-$$Nest$fgetmlastActivityStopTime(Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;)J

    move-result-wide v7

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-lez v7, :cond_1

    invoke-static {v6}, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->-$$Nest$fgetmlastActivityStopTime(Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;)J

    move-result-wide v7

    cmp-long v7, v7, v1

    if-gez v7, :cond_1

    .line 497
    move v0, v5

    .line 498
    invoke-static {v6}, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->-$$Nest$fgetmlastActivityStopTime(Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;)J

    move-result-wide v7

    move-wide v1, v7

    .line 500
    .end local v5    # "uid":I
    .end local v6    # "target":Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;
    :cond_1
    goto :goto_0

    .line 501
    :cond_2
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 502
    if-lez v0, :cond_5

    .line 503
    iget-object v3, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mPMS:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v3, v0}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordByUid(I)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/am/ProcessRecord;

    .line 504
    .local v4, "app":Lcom/android/server/am/ProcessRecord;
    if-eqz v4, :cond_3

    iget-object v5, v4, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v5}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I

    move-result v5

    const/16 v6, 0x2bc

    if-lt v5, v6, :cond_3

    .line 505
    iget-object v5, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mPMS:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v5}, Lcom/android/server/am/ProcessManagerService;->getProcessKiller()Lcom/android/server/am/ProcessKiller;

    move-result-object v5

    const-string v6, "mfz-overmem"

    const/4 v7, 0x0

    invoke-virtual {v5, v4, v6, v7}, Lcom/android/server/am/ProcessKiller;->killApplication(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Z)Z

    .line 506
    const-string v5, "MFZ"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Killing "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v4, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v4, Lcom/android/server/am/ProcessRecord;->mPid:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") since overmem."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 508
    .end local v4    # "app":Lcom/android/server/am/ProcessRecord;
    :cond_3
    goto :goto_1

    .line 509
    :cond_4
    iput v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mKillingUID:I

    .line 515
    return-void

    .line 511
    :cond_5
    const/4 v3, -0x1

    iput v3, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mKillingUID:I

    .line 512
    const-string v3, "MFZ"

    const-string v4, "Stop checking and killing as no apps to kill."

    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 513
    return-void

    .line 501
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4
.end method

.method private checkAndReclaim(I)V
    .locals 7
    .param p1, "uid"    # I

    .line 395
    invoke-direct {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->countBgMem()J

    move-result-wide v0

    sget-wide v2, Lcom/android/server/am/MemoryFreezeStubImpl;->sThreshholdReclaim:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 396
    const-string v0, "MFZ"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "don\'t need to PR "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    return-void

    .line 400
    :cond_0
    const-string v0, ""

    .line 401
    .local v0, "pkgName":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mRunningPackages:Ljava/util/HashMap;

    monitor-enter v1

    .line 402
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mRunningPackages:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;

    .line 403
    .local v2, "target":Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;
    if-nez v2, :cond_1

    .line 404
    monitor-exit v1

    return-void

    .line 406
    :cond_1
    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->-$$Nest$fputmPendingFreeze(Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;Z)V

    .line 407
    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->-$$Nest$fputmFrozen(Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;Z)V

    .line 408
    invoke-static {v2}, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->-$$Nest$fgetmPackageName(Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;)Ljava/lang/String;

    move-result-object v4

    move-object v0, v4

    .line 409
    .end local v2    # "target":Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 410
    iget-object v1, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mWMInternal:Lcom/android/server/wm/WindowManagerInternal;

    invoke-virtual {v1}, Lcom/android/server/wm/WindowManagerInternal;->getVisibleWindowOwner()Ljava/util/List;

    move-result-object v2

    .line 411
    .local v2, "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    const/4 v4, -0x1

    if-eq v1, v4, :cond_2

    .line 412
    const-string v1, "MFZ"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "don\'t PR "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " as visible."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    return-void

    .line 416
    :cond_2
    const/4 v1, 0x0

    .line 417
    .local v1, "app":Lcom/android/server/am/ProcessRecord;
    iget-object v4, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mAMS:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v4

    .line 418
    :try_start_1
    iget-object v5, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mAMS:Lcom/android/server/am/ActivityManagerService;

    invoke-virtual {v5, v0, p1}, Lcom/android/server/am/ActivityManagerService;->getProcessRecordLocked(Ljava/lang/String;I)Lcom/android/server/am/ProcessRecord;

    move-result-object v5

    move-object v1, v5

    .line 419
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 420
    if-nez v1, :cond_3

    .line 421
    const-string v3, "MFZ"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "didn\'t find process record of "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    return-void

    .line 424
    :cond_3
    const-string v4, "MFZ"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "PR "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",uid:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",pid:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v1, Lcom/android/server/am/ProcessRecord;->mPid:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    iget-object v4, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget-object v4, v4, Lcom/android/server/am/ActivityManagerService;->mOomAdjuster:Lcom/android/server/am/OomAdjuster;

    iget-object v4, v4, Lcom/android/server/am/OomAdjuster;->mCachedAppOptimizer:Lcom/android/server/am/CachedAppOptimizer;

    new-instance v5, Lcom/android/server/am/MemoryFreezeStubImpl$3;

    invoke-direct {v5, p0}, Lcom/android/server/am/MemoryFreezeStubImpl$3;-><init>(Lcom/android/server/am/MemoryFreezeStubImpl;)V

    invoke-virtual {v4, v1, v3, v5}, Lcom/android/server/am/CachedAppOptimizer;->compactMemoryFreezeApp(Lcom/android/server/am/ProcessRecord;ZLjava/lang/Runnable;)V

    .line 432
    return-void

    .line 419
    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 409
    .end local v1    # "app":Lcom/android/server/am/ProcessRecord;
    .end local v2    # "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :catchall_1
    move-exception v2

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v2
.end method

.method private checkAndWriteBack()V
    .locals 4

    .line 448
    invoke-direct {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->countBgMem()J

    move-result-wide v0

    sget-wide v2, Lcom/android/server/am/MemoryFreezeStubImpl;->sThreshholdWriteback:J

    cmp-long v0, v0, v2

    const-string v1, "MFZ"

    if-gez v0, :cond_0

    .line 449
    const-string v0, "don\'t need to WB."

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    return-void

    .line 452
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->storageManager:Landroid/os/storage/IStorageManager;

    if-nez v0, :cond_1

    .line 453
    return-void

    .line 456
    :cond_1
    :try_start_0
    const-string v0, "WB in progress"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->storageManager:Landroid/os/storage/IStorageManager;

    new-instance v2, Lcom/android/server/am/MemoryFreezeStubImpl$4;

    invoke-direct {v2, p0}, Lcom/android/server/am/MemoryFreezeStubImpl$4;-><init>(Lcom/android/server/am/MemoryFreezeStubImpl;)V

    const/4 v3, 0x2

    invoke-interface {v0, v3, v2}, Landroid/os/storage/IStorageManager;->runExtmFlushPages(ILandroid/os/IVoldTaskListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 472
    goto :goto_0

    .line 469
    :catch_0
    move-exception v0

    .line 470
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "WB error"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 473
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private checkUnused()V
    .locals 10

    .line 372
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mPMS:Lcom/android/server/am/ProcessManagerService;

    if-nez v0, :cond_0

    return-void

    .line 373
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 374
    .local v0, "now":J
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 375
    .local v2, "killList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iget-object v3, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mRunningPackages:Ljava/util/HashMap;

    monitor-enter v3

    .line 376
    :try_start_0
    iget-object v4, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mRunningPackages:Ljava/util/HashMap;

    new-instance v5, Lcom/android/server/am/MemoryFreezeStubImpl$$ExternalSyntheticLambda0;

    invoke-direct {v5, p0, v0, v1, v2}, Lcom/android/server/am/MemoryFreezeStubImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/am/MemoryFreezeStubImpl;JLjava/util/List;)V

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->forEach(Ljava/util/function/BiConsumer;)V

    .line 382
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 383
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 384
    .local v4, "uid":I
    iget-object v5, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mPMS:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v5, v4}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordByUid(I)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/am/ProcessRecord;

    .line 385
    .local v6, "app":Lcom/android/server/am/ProcessRecord;
    if-eqz v6, :cond_1

    iget-object v7, v6, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v7}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I

    move-result v7

    const/16 v8, 0x2bc

    if-lt v7, v8, :cond_1

    .line 386
    iget-object v7, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mPMS:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v7}, Lcom/android/server/am/ProcessManagerService;->getProcessKiller()Lcom/android/server/am/ProcessKiller;

    move-result-object v7

    const-string v8, "mfz-overtime"

    const/4 v9, 0x0

    invoke-virtual {v7, v6, v8, v9}, Lcom/android/server/am/ProcessKiller;->killApplication(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Z)Z

    .line 387
    const-string v7, "MFZ"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "kill "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v6, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v6, Lcom/android/server/am/ProcessRecord;->mPid:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") since overtime."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    .end local v6    # "app":Lcom/android/server/am/ProcessRecord;
    :cond_1
    goto :goto_1

    .line 390
    .end local v4    # "uid":I
    :cond_2
    goto :goto_0

    .line 391
    :cond_3
    iget-object v3, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    iget-wide v5, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mUnusedCheckTime:J

    invoke-virtual {v3, v4, v5, v6}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 392
    return-void

    .line 382
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4
.end method

.method private countBgFlush()J
    .locals 10

    .line 540
    const-wide/16 v0, 0x0

    .line 541
    .local v0, "flushCount":J
    new-instance v2, Ljava/io/File;

    const-string v3, "/sys/block/zram0/mfz_compr_data_size"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 542
    .local v2, "file":Ljava/io/File;
    const/4 v3, 0x0

    .line 544
    .local v3, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/FileReader;

    invoke-direct {v5, v2}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v4, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object v3, v4

    .line 545
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    .line 546
    .local v4, "line":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 547
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    const-string v6, " +"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 548
    .local v5, "res":[Ljava/lang/String;
    array-length v6, v5

    const/4 v7, 0x1

    if-lt v6, v7, :cond_0

    const/4 v6, 0x0

    aget-object v7, v5, v6

    if-eqz v7, :cond_0

    .line 549
    aget-object v6, v5, v6

    invoke-static {v6}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, 0x400

    div-long/2addr v6, v8
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-wide v0, v6

    .line 555
    .end local v4    # "line":Ljava/lang/String;
    .end local v5    # "res":[Ljava/lang/String;
    :cond_0
    nop

    .line 557
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 560
    :goto_0
    goto :goto_1

    .line 558
    :catch_0
    move-exception v4

    .line 559
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    .end local v4    # "e":Ljava/io/IOException;
    goto :goto_0

    .line 555
    :catchall_0
    move-exception v4

    goto :goto_2

    .line 552
    :catch_1
    move-exception v4

    .line 553
    .local v4, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 555
    .end local v4    # "e":Ljava/lang/Exception;
    if-eqz v3, :cond_1

    .line 557
    :try_start_3
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 563
    :cond_1
    :goto_1
    return-wide v0

    .line 555
    :goto_2
    if-eqz v3, :cond_2

    .line 557
    :try_start_4
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 560
    goto :goto_3

    .line 558
    :catch_2
    move-exception v5

    .line 559
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    .line 562
    .end local v5    # "e":Ljava/io/IOException;
    :cond_2
    :goto_3
    throw v4
.end method

.method private countBgMem()J
    .locals 12

    .line 518
    const-wide/16 v0, 0x0

    .line 519
    .local v0, "pssTotal":J
    iget-object v2, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mWMInternal:Lcom/android/server/wm/WindowManagerInternal;

    invoke-virtual {v2}, Lcom/android/server/wm/WindowManagerInternal;->getVisibleWindowOwner()Ljava/util/List;

    move-result-object v2

    .line 520
    .local v2, "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iget-object v3, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mRunningPackages:Ljava/util/HashMap;

    monitor-enter v3

    .line 521
    :try_start_0
    iget-object v4, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mRunningPackages:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 522
    .local v5, "uid":I
    iget-object v6, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mRunningPackages:Ljava/util/HashMap;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;

    .line 523
    .local v6, "target":Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;
    invoke-static {v6}, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->-$$Nest$fgetmPid(Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;)I

    move-result v7

    if-lez v7, :cond_1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v7

    const/4 v8, -0x1

    if-ne v7, v8, :cond_1

    .line 524
    invoke-static {v6}, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->-$$Nest$fgetmPid(Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;)I

    move-result v7

    invoke-static {v7}, Landroid/os/Process;->getPss(I)J

    move-result-wide v7

    const-wide/16 v9, 0x400

    div-long/2addr v7, v9

    .line 525
    .local v7, "pss":J
    sget-boolean v9, Lcom/android/server/am/MemoryFreezeStubImpl;->DEBUG:Z

    if-eqz v9, :cond_0

    .line 526
    const-string v9, "MFZ"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "pss: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v6}, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->-$$Nest$fgetmPackageName(Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ",uid:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ",pid:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v6}, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->-$$Nest$fgetmPid(Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;)I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 529
    :cond_0
    add-long/2addr v0, v7

    .line 531
    .end local v5    # "uid":I
    .end local v6    # "target":Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;
    .end local v7    # "pss":J
    :cond_1
    goto :goto_0

    .line 532
    :cond_2
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 533
    invoke-direct {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->countBgFlush()J

    move-result-wide v3

    .line 534
    .local v3, "flushTotal":J
    add-long v5, v0, v3

    .line 535
    .local v5, "countBgMem":J
    const-string v7, "MFZ"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "mem count: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " + "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    return-wide v5

    .line 532
    .end local v3    # "flushTotal":J
    .end local v5    # "countBgMem":J
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4
.end method

.method private dump(Ljava/io/PrintWriter;)V
    .locals 10
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 674
    const-string v0, "---- Start of MEMFREEZE ----"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 675
    const-string v0, "App State:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 676
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mRunningPackages:Ljava/util/HashMap;

    monitor-enter v0

    .line 677
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mRunningPackages:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 678
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;

    .line 679
    .local v3, "tmpAppInfo":Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    .line 680
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%-25s"

    const/4 v6, 0x1

    new-array v7, v6, [Ljava/lang/Object;

    invoke-static {v3}, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->-$$Nest$fgetmPackageName(Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    aput-object v8, v7, v9

    invoke-static {v5, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 681
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " Frozen="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%-5s"

    new-array v7, v6, [Ljava/lang/Object;

    invoke-static {v3}, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->-$$Nest$fgetmFrozen(Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;)Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v5, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 682
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " Pending="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%-5s"

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3}, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->-$$Nest$fgetmPendingFreeze(Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;)Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 683
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;>;"
    .end local v3    # "tmpAppInfo":Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;
    goto/16 :goto_0

    .line 684
    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 685
    iget-object v1, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mProcessFrozenUid:Ljava/util/List;

    monitor-enter v1

    .line 686
    :try_start_1
    const-string v0, "Process Frozen:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 687
    const-string v0, "  Current Frozen Uid:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 688
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mProcessFrozenUid:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 689
    .local v0, "size":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v0, :cond_1

    .line 690
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mProcessFrozenUid:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 689
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 692
    .end local v0    # "size":I
    .end local v2    # "i":I
    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 693
    const-string v0, "\n  Total(Y/N) : "

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 694
    const-string v0, "\n---- End of MEMFREEZE ----"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 695
    return-void

    .line 692
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 684
    :catchall_1
    move-exception v1

    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1
.end method

.method public static getInstance()Lcom/android/server/am/MemoryFreezeStubImpl;
    .locals 1

    .line 120
    const-class v0, Lcom/android/server/am/MemoryFreezeStub;

    invoke-static {v0}, Lcom/miui/base/MiuiStubUtil;->getImpl(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/MemoryFreezeStubImpl;

    return-object v0
.end method

.method private static getStorageManager()Landroid/os/storage/IStorageManager;
    .locals 3

    .line 167
    :try_start_0
    const-string v0, "mount"

    .line 168
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 167
    invoke-static {v0}, Landroid/os/storage/IStorageManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/storage/IStorageManager;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/VerifyError; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 169
    :catch_0
    move-exception v0

    .line 170
    .local v0, "e":Ljava/lang/VerifyError;
    const-string v1, "MFZ"

    const-string v2, "get StorageManager error"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    const/4 v1, 0x0

    return-object v1
.end method

.method private synthetic lambda$checkUnused$0(JLjava/util/List;Ljava/lang/Integer;Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;)V
    .locals 4
    .param p1, "now"    # J
    .param p3, "killList"    # Ljava/util/List;
    .param p4, "key"    # Ljava/lang/Integer;
    .param p5, "value"    # Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;

    .line 377
    invoke-static {p5}, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->-$$Nest$fgetmlastActivityStopTime(Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    invoke-static {p5}, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->-$$Nest$fgetmlastActivityStopTime(Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;)J

    move-result-wide v0

    sub-long v0, p1, v0

    iget-wide v2, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mUnusedKillTime:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 379
    invoke-static {p5}, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->-$$Nest$fgetmUid(Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 381
    :cond_0
    return-void
.end method

.method private updateThreshhold()V
    .locals 7

    .line 176
    const-string v0, ","

    const-string v1, "persist.sys.mfz.mem_policy"

    const-string v2, "2048,3072,4096"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 178
    .local v1, "prop":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 179
    .local v2, "res":[Ljava/lang/String;
    array-length v3, v2

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    .line 180
    const/4 v3, 0x0

    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide/16 v5, 0x400

    mul-long/2addr v3, v5

    sput-wide v3, Lcom/android/server/am/MemoryFreezeStubImpl;->sThreshholdReclaim:J

    .line 181
    const/4 v3, 0x1

    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    mul-long/2addr v3, v5

    sput-wide v3, Lcom/android/server/am/MemoryFreezeStubImpl;->sThreshholdWriteback:J

    .line 182
    const/4 v3, 0x2

    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    mul-long/2addr v3, v5

    sput-wide v3, Lcom/android/server/am/MemoryFreezeStubImpl;->sThreshholdKill:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    .end local v2    # "res":[Ljava/lang/String;
    :cond_0
    goto :goto_0

    .line 184
    :catch_0
    move-exception v2

    .line 185
    .local v2, "e":Ljava/lang/Exception;
    const-wide/32 v3, 0x200000

    sput-wide v3, Lcom/android/server/am/MemoryFreezeStubImpl;->sThreshholdReclaim:J

    .line 186
    const-wide/32 v3, 0x300000

    sput-wide v3, Lcom/android/server/am/MemoryFreezeStubImpl;->sThreshholdWriteback:J

    .line 187
    const-wide/32 v3, 0x400000

    sput-wide v3, Lcom/android/server/am/MemoryFreezeStubImpl;->sThreshholdKill:J

    .line 188
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 190
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mfz policy: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-wide v3, Lcom/android/server/am/MemoryFreezeStubImpl;->sThreshholdReclaim:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-wide v3, Lcom/android/server/am/MemoryFreezeStubImpl;->sThreshholdWriteback:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-wide v2, Lcom/android/server/am/MemoryFreezeStubImpl;->sThreshholdKill:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "MFZ"

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    return-void
.end method


# virtual methods
.method public activityResumed(ILjava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "uid"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "processName"    # Ljava/lang/String;

    .line 328
    invoke-virtual {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->isEnable()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 329
    :cond_0
    sget-object v0, Lcom/android/server/am/MemoryFreezeStubImpl;->sWhiteListApp:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 330
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 331
    .local v0, "now":J
    iget-object v2, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mLastStartPackage:Ljava/lang/String;

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-wide v2, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mLastStartPackageTime:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x1388

    cmp-long v2, v2, v4

    if-gtz v2, :cond_1

    .line 334
    return-void

    .line 336
    :cond_1
    iput-wide v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mLastStartPackageTime:J

    .line 337
    iput-object p2, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mLastStartPackage:Ljava/lang/String;

    .line 338
    iget-object v2, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mRunningPackages:Ljava/util/HashMap;

    monitor-enter v2

    .line 339
    :try_start_0
    iget-object v3, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mRunningPackages:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;

    .line 340
    .local v3, "target":Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;
    if-nez v3, :cond_2

    .line 341
    new-instance v4, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;

    invoke-direct {v4, p0, p1, p2}, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;-><init>(Lcom/android/server/am/MemoryFreezeStubImpl;ILjava/lang/String;)V

    move-object v3, v4

    .line 342
    iget-object v4, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mRunningPackages:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 344
    :cond_2
    iget-object v4, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x4

    invoke-virtual {v4, v6, v5}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->removeEqualMessages(ILjava/lang/Object;)V

    .line 345
    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->-$$Nest$fputmPendingFreeze(Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;Z)V

    .line 346
    invoke-static {v3, v4}, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->-$$Nest$fputmFrozen(Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;Z)V

    .line 348
    .end local v3    # "target":Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 349
    const-string v2, "MFZ"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "activityResumed packageName:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "|"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",uid:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 348
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 351
    .end local v0    # "now":J
    :cond_3
    :goto_1
    return-void
.end method

.method public activityStopped(IILjava/lang/String;)V
    .locals 4
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "packageName"    # Ljava/lang/String;

    .line 355
    invoke-virtual {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->isEnable()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 356
    :cond_0
    sget-object v0, Lcom/android/server/am/MemoryFreezeStubImpl;->sWhiteListApp:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 357
    :cond_1
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mRunningPackages:Ljava/util/HashMap;

    monitor-enter v0

    .line 358
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mRunningPackages:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;

    .line 359
    .local v1, "target":Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;
    if-eqz v1, :cond_2

    .line 360
    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->-$$Nest$fputmPendingFreeze(Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;Z)V

    .line 361
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->-$$Nest$fputmlastActivityStopTime(Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;J)V

    .line 362
    invoke-static {v1, p2}, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->-$$Nest$fputmPid(Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;I)V

    .line 364
    .end local v1    # "target":Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;
    :cond_2
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 365
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v0, v2, v1}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->removeEqualMessages(ILjava/lang/Object;)V

    .line 366
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 367
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    iget-wide v2, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mFreezerDebounceTimeout:J

    invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 368
    const-string v1, "MFZ"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "activityStopped packageName:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",uid:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",pid:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    return-void

    .line 364
    .end local v0    # "msg":Landroid/os/Message;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public extmFlushFinished()V
    .locals 4

    .line 476
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 477
    const-string v0, "MFZ"

    const-string v2, "remove kill msg"

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->removeMessages(I)V

    .line 480
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 481
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    const-wide/16 v2, 0x2710

    invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 482
    return-void
.end method

.method public getWhitePackages()V
    .locals 7

    .line 195
    const-string v0, "/data/system/mfz.xml"

    .line 196
    .local v0, "path":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    const-string v2, "MFZ"

    if-nez v1, :cond_0

    .line 197
    const-string v0, "/product/etc/mfz.xml"

    .line 198
    const-string v1, "looking for default xml."

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    :cond_0
    const/4 v1, 0x0

    .line 201
    .local v1, "inputStream":Ljava/io/InputStream;
    const/4 v3, 0x0

    .line 204
    .local v3, "xmlParser":Lorg/xmlpull/v1/XmlPullParser;
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    move-object v1, v4

    .line 205
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    move-object v3, v4

    .line 206
    const-string/jumbo v4, "utf-8"

    invoke-interface {v3, v1, v4}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 207
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v4

    .line 208
    .local v4, "event":I
    :goto_0
    const/4 v5, 0x1

    if-eq v4, v5, :cond_2

    .line 209
    packed-switch v4, :pswitch_data_0

    :pswitch_0
    goto :goto_1

    .line 218
    :pswitch_1
    goto :goto_1

    .line 213
    :pswitch_2
    const-string v5, "designated-package"

    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 214
    sget-object v5, Lcom/android/server/am/MemoryFreezeStubImpl;->sWhiteListApp:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v3, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 211
    :pswitch_3
    nop

    .line 222
    :cond_1
    :goto_1
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v5
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move v4, v5

    goto :goto_0

    .line 227
    .end local v4    # "event":I
    :cond_2
    nop

    .line 229
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 233
    goto :goto_3

    :catchall_0
    move-exception v2

    goto :goto_2

    .line 230
    :catch_0
    move-exception v4

    .line 231
    .local v4, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 233
    nop

    .end local v4    # "e":Ljava/io/IOException;
    goto :goto_3

    :goto_2
    const/4 v1, 0x0

    .line 234
    throw v2

    .line 227
    :catchall_1
    move-exception v4

    goto :goto_6

    .line 224
    :catch_1
    move-exception v4

    .line 225
    .local v4, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 227
    nop

    .end local v4    # "e":Ljava/lang/Exception;
    if-eqz v1, :cond_3

    .line 229
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 233
    nop

    :goto_3
    const/4 v1, 0x0

    .line 234
    goto :goto_5

    .line 233
    :catchall_2
    move-exception v2

    goto :goto_4

    .line 230
    :catch_2
    move-exception v4

    .line 231
    .local v4, "e":Ljava/io/IOException;
    :try_start_5
    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 233
    nop

    .end local v4    # "e":Ljava/io/IOException;
    goto :goto_3

    :goto_4
    const/4 v1, 0x0

    .line 234
    throw v2

    .line 237
    :cond_3
    :goto_5
    return-void

    .line 227
    :goto_6
    if-eqz v1, :cond_4

    .line 229
    :try_start_6
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 233
    nop

    :goto_7
    const/4 v1, 0x0

    .line 234
    goto :goto_9

    .line 233
    :catchall_3
    move-exception v2

    goto :goto_8

    .line 230
    :catch_3
    move-exception v5

    .line 231
    .local v5, "e":Ljava/io/IOException;
    :try_start_7
    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 233
    nop

    .end local v5    # "e":Ljava/io/IOException;
    goto :goto_7

    :goto_8
    const/4 v1, 0x0

    .line 234
    throw v2

    .line 236
    :cond_4
    :goto_9
    throw v4

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public init(Landroid/content/Context;Lcom/android/server/am/ActivityManagerService;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ams"    # Lcom/android/server/am/ActivityManagerService;

    .line 126
    const-string v0, "ro.miui.build.region"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "cn"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 127
    .local v0, "isCnVersion":Z
    if-nez v0, :cond_0

    return-void

    .line 128
    :cond_0
    iget-boolean v1, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->isInit:Z

    if-eqz v1, :cond_1

    return-void

    .line 129
    :cond_1
    iget-boolean v1, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->extmEnable:Z

    if-nez v1, :cond_2

    return-void

    .line 130
    :cond_2
    iget-boolean v1, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mEnable:Z

    if-nez v1, :cond_3

    return-void

    .line 131
    :cond_3
    iput-object p1, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mContext:Landroid/content/Context;

    .line 132
    new-instance v1, Lcom/android/server/am/MemoryFreezeCloud;

    invoke-direct {v1}, Lcom/android/server/am/MemoryFreezeCloud;-><init>()V

    iput-object v1, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mMemoryFreezeCloud:Lcom/android/server/am/MemoryFreezeCloud;

    .line 133
    invoke-virtual {v1, p1}, Lcom/android/server/am/MemoryFreezeCloud;->initCloud(Landroid/content/Context;)V

    .line 134
    iget-object v1, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 135
    new-instance v1, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    iget-object v2, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;-><init>(Lcom/android/server/am/MemoryFreezeStubImpl;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    .line 137
    invoke-virtual {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->getWhitePackages()V

    .line 138
    invoke-direct {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->updateThreshhold()V

    .line 140
    iget-object v1, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 141
    .local v1, "msgRegisterCloud":Landroid/os/Message;
    iget-object v3, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    invoke-virtual {v3, v1}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z

    .line 147
    iget-object v3, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    .line 148
    .local v3, "msgCheckUnused":Landroid/os/Message;
    iget-object v4, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    iget-wide v5, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mUnusedCheckTime:J

    invoke-virtual {v4, v3, v5, v6}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 149
    iget-object v4, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    .line 150
    .local v4, "msgRegisterWhiteList":Landroid/os/Message;
    iget-object v5, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    invoke-virtual {v5, v4}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z

    .line 152
    new-instance v5, Lcom/android/server/am/MemoryFreezeStubImpl$BinderService;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v6}, Lcom/android/server/am/MemoryFreezeStubImpl$BinderService;-><init>(Lcom/android/server/am/MemoryFreezeStubImpl;Lcom/android/server/am/MemoryFreezeStubImpl$BinderService-IA;)V

    iput-object v5, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mBinderService:Lcom/android/server/am/MemoryFreezeStubImpl$BinderService;

    .line 153
    const-string v6, "memfreeze"

    invoke-static {v6, v5}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 155
    iput-object p2, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mAMS:Lcom/android/server/am/ActivityManagerService;

    .line 156
    const-class v5, Lcom/android/server/wm/WindowManagerInternal;

    invoke-static {v5}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/wm/WindowManagerInternal;

    iput-object v5, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mWMInternal:Lcom/android/server/wm/WindowManagerInternal;

    .line 157
    iget-object v5, p2, Lcom/android/server/am/ActivityManagerService;->mActivityTaskManager:Lcom/android/server/wm/ActivityTaskManagerService;

    iput-object v5, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mATMS:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 158
    const-string v5, "ProcessManager"

    invoke-static {v5}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v5

    check-cast v5, Lcom/android/server/am/ProcessManagerService;

    iput-object v5, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mPMS:Lcom/android/server/am/ProcessManagerService;

    .line 159
    invoke-static {}, Lcom/android/server/am/MemoryFreezeStubImpl;->getStorageManager()Landroid/os/storage/IStorageManager;

    move-result-object v5

    iput-object v5, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->storageManager:Landroid/os/storage/IStorageManager;

    .line 160
    iput-boolean v2, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->isInit:Z

    .line 161
    sget-boolean v2, Lcom/android/server/am/MemoryFreezeStubImpl;->DEBUG:Z

    if-eqz v2, :cond_4

    .line 162
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "complete init, default applist: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v5, Lcom/android/server/am/MemoryFreezeStubImpl;->sWhiteListApp:Ljava/util/List;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v5, "MFZ"

    invoke-static {v5, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    :cond_4
    return-void
.end method

.method public isEnable()Z
    .locals 1

    .line 116
    iget-boolean v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->extmEnable:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mEnable:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->isInit:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isMemoryFreezeWhiteList(Ljava/lang/String;)Z
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 319
    invoke-virtual {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->isEnable()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 320
    :cond_0
    sget-object v0, Lcom/android/server/am/MemoryFreezeStubImpl;->sWhiteListApp:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 321
    const/4 v0, 0x1

    return v0

    .line 323
    :cond_1
    return v1
.end method

.method public isNeedProtect(Lcom/android/server/am/ProcessRecord;)Z
    .locals 3
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 568
    invoke-virtual {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->isEnable()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 569
    :cond_0
    if-nez p1, :cond_1

    return v1

    .line 570
    :cond_1
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 571
    .local v0, "packageName":Ljava/lang/String;
    sget-object v2, Lcom/android/server/am/MemoryFreezeStubImpl;->sWhiteListApp:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lcom/android/server/am/MemoryFreezeStubImpl;->sBlackListApp:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 572
    const/4 v1, 0x1

    return v1

    .line 574
    :cond_2
    return v1
.end method

.method public processReclaimFinished()V
    .locals 4

    .line 435
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->hasMessages(I)Z

    move-result v0

    const-string v2, "MFZ"

    if-eqz v0, :cond_0

    .line 436
    const-string v0, "remove kill msg"

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->removeMessages(I)V

    .line 439
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 440
    const-string v0, "remove WB msg"

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 441
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->removeMessages(I)V

    .line 443
    :cond_1
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 444
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    const-wide/16 v2, 0x2710

    invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 445
    return-void
.end method

.method public registerCloudObserver(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 304
    new-instance v0, Lcom/android/server/am/MemoryFreezeStubImpl$2;

    iget-object v1, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/am/MemoryFreezeStubImpl$2;-><init>(Lcom/android/server/am/MemoryFreezeStubImpl;Landroid/os/Handler;)V

    .line 312
    .local v0, "observer":Landroid/database/ContentObserver;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 313
    const-string v2, "cloud_memFreeze_control"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 312
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 315
    return-void
.end method

.method public registerMiuiFreezeObserver(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 290
    new-instance v0, Lcom/android/server/am/MemoryFreezeStubImpl$1;

    iget-object v1, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/am/MemoryFreezeStubImpl$1;-><init>(Lcom/android/server/am/MemoryFreezeStubImpl;Landroid/os/Handler;)V

    .line 298
    .local v0, "observer":Landroid/database/ContentObserver;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 299
    const-string v2, "miui_freeze"

    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 298
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 301
    return-void
.end method

.method public reportAppDied(Lcom/android/server/am/ProcessRecord;)V
    .locals 7
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 579
    invoke-virtual {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->isEnable()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 580
    :cond_0
    if-nez p1, :cond_1

    return-void

    .line 581
    :cond_1
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 582
    .local v0, "processName":Ljava/lang/String;
    iget v1, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    .line 583
    .local v1, "uid":I
    iget v2, p1, Lcom/android/server/am/ProcessRecord;->mPid:I

    .line 584
    .local v2, "pid":I
    if-eqz v0, :cond_3

    sget-object v3, Lcom/android/server/am/MemoryFreezeStubImpl;->sWhiteListApp:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 585
    iget-object v3, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    const/4 v4, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->removeEqualMessages(ILjava/lang/Object;)V

    .line 586
    iget-object v3, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mRunningPackages:Ljava/util/HashMap;

    monitor-enter v3

    .line 587
    :try_start_0
    iget-object v4, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mRunningPackages:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;

    .line 588
    .local v4, "target":Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;
    if-eqz v4, :cond_2

    .line 589
    iget-object v5, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mRunningPackages:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 591
    .end local v4    # "target":Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;
    :cond_2
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 592
    const-string v3, "MFZ"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "report "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is died, uid:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",pid:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 593
    iget v3, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mKillingUID:I

    if-ne v1, v3, :cond_3

    .line 595
    iget-object v3, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    const/4 v4, 0x6

    invoke-virtual {v3, v4}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->hasMessages(I)Z

    move-result v3

    if-nez v3, :cond_3

    .line 596
    iget-object v3, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    invoke-virtual {v3, v4}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    .line 597
    .local v3, "msg":Landroid/os/Message;
    iget-object v4, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    invoke-virtual {v4, v3}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 591
    .end local v3    # "msg":Landroid/os/Message;
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 601
    :cond_3
    :goto_0
    return-void
.end method

.method public updateCloudControlParas()V
    .locals 3

    .line 240
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "cloud_memFreeze_control"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mContext:Landroid/content/Context;

    .line 243
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 242
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 246
    .local v0, "enableStr":Ljava/lang/String;
    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->extmEnable:Z

    .line 247
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "set enable state from database: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->extmEnable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MFZ"

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    .end local v0    # "enableStr":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public updateFrozedUids(Ljava/util/List;Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 605
    .local p1, "freezelist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .local p2, "thawlist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-virtual {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->isEnable()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 606
    :cond_0
    const-string/jumbo v0, "update process freeze list:"

    .line 608
    .local v0, "mUpdateFrozenInfo":Ljava/lang/String;
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 609
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    .line 610
    .local v1, "size":I
    iget-object v2, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mProcessFrozenUid:Ljava/util/List;

    monitor-enter v2

    .line 611
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_2

    .line 612
    :try_start_0
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 613
    .local v4, "value":I
    iget-object v5, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mProcessFrozenUid:Ljava/util/List;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 614
    iget-object v5, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mProcessFrozenUid:Ljava/util/List;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 611
    .end local v4    # "value":I
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 617
    .end local v3    # "i":I
    :cond_2
    monitor-exit v2

    goto :goto_1

    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 620
    .end local v1    # "size":I
    :cond_3
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    .line 621
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    .line 622
    .restart local v1    # "size":I
    iget-object v2, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mProcessFrozenUid:Ljava/util/List;

    monitor-enter v2

    .line 623
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_2
    if-ge v3, v1, :cond_5

    .line 624
    :try_start_1
    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 625
    .restart local v4    # "value":I
    iget-object v5, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mProcessFrozenUid:Ljava/util/List;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 626
    iget-object v5, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mProcessFrozenUid:Ljava/util/List;

    new-instance v6, Ljava/lang/Integer;

    invoke-direct {v6, v4}, Ljava/lang/Integer;-><init>(I)V

    invoke-interface {v5, v6}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 623
    .end local v4    # "value":I
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 629
    .end local v3    # "i":I
    :cond_5
    monitor-exit v2

    goto :goto_3

    :catchall_1
    move-exception v3

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v3

    .line 632
    .end local v1    # "size":I
    :cond_6
    :goto_3
    iget-object v1, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mProcessFrozenUid:Ljava/util/List;

    monitor-enter v1

    .line 633
    :try_start_2
    iget-object v2, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mProcessFrozenUid:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 634
    .local v2, "size":I
    if-lez v2, :cond_a

    .line 635
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_4
    if-ge v3, v2, :cond_9

    .line 636
    iget-object v4, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mProcessFrozenUid:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 637
    .restart local v4    # "value":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 638
    .local v5, "subStr":Ljava/lang/String;
    iget-object v6, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v6

    .line 639
    .local v6, "packages":[Ljava/lang/String;
    if-nez v6, :cond_7

    goto :goto_6

    .line 640
    :cond_7
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_5
    array-length v8, v6

    if-ge v7, v8, :cond_8

    .line 641
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "|"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    aget-object v9, v6, v7

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object v5, v8

    .line 640
    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    .line 643
    .end local v7    # "j":I
    :cond_8
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object v0, v7

    .line 635
    .end local v4    # "value":I
    .end local v5    # "subStr":Ljava/lang/String;
    .end local v6    # "packages":[Ljava/lang/String;
    :goto_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .end local v3    # "i":I
    :cond_9
    goto :goto_7

    .line 646
    :cond_a
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v0, v3

    .line 649
    .end local v2    # "size":I
    :goto_7
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 650
    sget-boolean v1, Lcom/android/server/am/MemoryFreezeStubImpl;->DEBUG:Z

    if-eqz v1, :cond_b

    .line 651
    const-string v1, "MFZ"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 653
    :cond_b
    return-void

    .line 649
    :catchall_2
    move-exception v2

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    throw v2
.end method

.method public updateMiuiFreezeInfo()V
    .locals 11

    .line 252
    invoke-virtual {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->isEnable()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 253
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "miui_freeze"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 255
    .local v0, "miuiFreezeStr":Ljava/lang/String;
    sget-boolean v1, Lcom/android/server/am/MemoryFreezeStubImpl;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 256
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SettingsContentObserver change str="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MFZ"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    :cond_1
    if-eqz v0, :cond_6

    .line 259
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 260
    .local v1, "thawList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 261
    .local v2, "freezeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const-string v3, ""

    .local v3, "freezeSub":Ljava/lang/String;
    const-string v4, ""

    .line 263
    .local v4, "thawSub":Ljava/lang/String;
    const-string/jumbo v5, "thaw"

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    const-string v6, "freeze"

    const-string v7, ";freeze:"

    const/4 v8, 0x1

    if-eqz v5, :cond_2

    .line 264
    const-string/jumbo v5, "thaw:"

    invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    aget-object v9, v9, v8

    invoke-virtual {v9, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    aget-object v4, v9, v10

    .line 265
    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 266
    invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    aget-object v5, v5, v8

    invoke-virtual {v5, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    aget-object v3, v5, v8

    goto :goto_0

    .line 268
    :cond_2
    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 269
    invoke-virtual {v0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    aget-object v3, v5, v8

    .line 272
    :cond_3
    :goto_0
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    const-string v6, ", "

    if-lez v5, :cond_4

    .line 273
    invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 274
    .local v5, "tmpStr":[Ljava/lang/String;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    array-length v8, v5

    if-ge v7, v8, :cond_4

    .line 275
    aget-object v8, v5, v7

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 274
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 279
    .end local v5    # "tmpStr":[Ljava/lang/String;
    .end local v7    # "i":I
    :cond_4
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_5

    .line 280
    invoke-virtual {v3, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 281
    .restart local v5    # "tmpStr":[Ljava/lang/String;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_2
    array-length v7, v5

    if-ge v6, v7, :cond_5

    .line 282
    aget-object v7, v5, v6

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 281
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 285
    .end local v5    # "tmpStr":[Ljava/lang/String;
    .end local v6    # "i":I
    :cond_5
    invoke-virtual {p0, v2, v1}, Lcom/android/server/am/MemoryFreezeStubImpl;->updateFrozedUids(Ljava/util/List;Ljava/util/List;)V

    .line 287
    .end local v1    # "thawList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v2    # "freezeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v3    # "freezeSub":Ljava/lang/String;
    .end local v4    # "thawSub":Ljava/lang/String;
    :cond_6
    return-void
.end method
