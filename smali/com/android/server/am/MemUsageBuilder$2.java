class com.android.server.am.MemUsageBuilder$2 implements java.util.Comparator {
	 /* .source "MemUsageBuilder.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/am/MemUsageBuilder;->buildTopProcs()Ljava/lang/String; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Object;", */
/* "Ljava/util/Comparator<", */
/* "Lcom/android/server/am/ProcessMemInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* # instance fields */
final com.android.server.am.MemUsageBuilder this$0; //synthetic
/* # direct methods */
 com.android.server.am.MemUsageBuilder$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/MemUsageBuilder; */
/* .line 457 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public Integer compare ( com.android.server.am.ProcessMemInfo p0, com.android.server.am.ProcessMemInfo p1 ) {
/* .locals 4 */
/* .param p1, "lhs" # Lcom/android/server/am/ProcessMemInfo; */
/* .param p2, "rhs" # Lcom/android/server/am/ProcessMemInfo; */
/* .line 459 */
/* iget-wide v0, p1, Lcom/android/server/am/ProcessMemInfo;->pss:J */
/* iget-wide v2, p2, Lcom/android/server/am/ProcessMemInfo;->pss:J */
/* cmp-long v0, v0, v2 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 460 */
/* iget-wide v0, p1, Lcom/android/server/am/ProcessMemInfo;->pss:J */
/* iget-wide v2, p2, Lcom/android/server/am/ProcessMemInfo;->pss:J */
/* cmp-long v0, v0, v2 */
/* if-gez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = -1; // const/4 v0, -0x1
} // :goto_0
/* .line 462 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer compare ( java.lang.Object p0, java.lang.Object p1 ) { //bridge//synthethic
/* .locals 0 */
/* .line 457 */
/* check-cast p1, Lcom/android/server/am/ProcessMemInfo; */
/* check-cast p2, Lcom/android/server/am/ProcessMemInfo; */
p1 = (( com.android.server.am.MemUsageBuilder$2 ) p0 ).compare ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/am/MemUsageBuilder$2;->compare(Lcom/android/server/am/ProcessMemInfo;Lcom/android/server/am/ProcessMemInfo;)I
} // .end method
