class com.android.server.am.PeriodicCleanerService$6 extends android.content.BroadcastReceiver {
	 /* .source "PeriodicCleanerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/PeriodicCleanerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.am.PeriodicCleanerService this$0; //synthetic
/* # direct methods */
 com.android.server.am.PeriodicCleanerService$6 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/PeriodicCleanerService; */
/* .line 1731 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 1734 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 1735 */
/* .local v0, "action":Ljava/lang/String; */
final String v1 = "android.intent.action.SCREEN_ON"; // const-string v1, "android.intent.action.SCREEN_ON"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v2 = 2; // const/4 v2, 0x2
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 1736 */
	 v1 = this.this$0;
	 int v3 = 1; // const/4 v3, 0x1
	 com.android.server.am.PeriodicCleanerService .-$$Nest$fputmScreenState ( v1,v3 );
	 /* .line 1737 */
	 v1 = this.this$0;
	 com.android.server.am.PeriodicCleanerService .-$$Nest$fgetmHandler ( v1 );
	 (( com.android.server.am.PeriodicCleanerService$MyHandler ) v1 ).removeMessages ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/PeriodicCleanerService$MyHandler;->removeMessages(I)V
	 /* .line 1738 */
} // :cond_0
final String v1 = "android.intent.action.SCREEN_OFF"; // const-string v1, "android.intent.action.SCREEN_OFF"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 1739 */
	 v1 = this.this$0;
	 com.android.server.am.PeriodicCleanerService .-$$Nest$fputmScreenState ( v1,v2 );
	 /* .line 1740 */
	 v1 = this.this$0;
	 com.android.server.am.PeriodicCleanerService .-$$Nest$fgetmHandler ( v1 );
	 int v2 = 3; // const/4 v2, 0x3
	 /* const-wide/32 v3, 0xea60 */
	 (( com.android.server.am.PeriodicCleanerService$MyHandler ) v1 ).sendEmptyMessageDelayed ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Lcom/android/server/am/PeriodicCleanerService$MyHandler;->sendEmptyMessageDelayed(IJ)Z
	 /* .line 1742 */
} // :cond_1
} // :goto_0
return;
} // .end method
