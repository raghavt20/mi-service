class com.android.server.am.ProcessSceneCleaner$H extends android.os.Handler {
	 /* .source "ProcessSceneCleaner.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/ProcessSceneCleaner; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "H" */
} // .end annotation
/* # static fields */
public static final Integer KILL_ALL_EVENT;
public static final Integer KILL_ANY_EVENT;
public static final Integer SWIPE_KILL_EVENT;
/* # instance fields */
final com.android.server.am.ProcessSceneCleaner this$0; //synthetic
/* # direct methods */
public com.android.server.am.ProcessSceneCleaner$H ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 94 */
this.this$0 = p1;
/* .line 95 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 96 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 6 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 100 */
/* invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V */
/* .line 101 */
v0 = this.obj;
/* check-cast v0, Lmiui/process/ProcessConfig; */
/* .line 102 */
/* .local v0, "config":Lmiui/process/ProcessConfig; */
/* iget v1, p1, Landroid/os/Message;->what:I */
/* const-wide/32 v2, 0x80000 */
/* packed-switch v1, :pswitch_data_0 */
/* goto/16 :goto_0 */
/* .line 116 */
/* :pswitch_0 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "SceneKillAny"; // const-string v4, "SceneKillAny"
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.this$0;
/* .line 117 */
v5 = (( miui.process.ProcessConfig ) v0 ).getPolicy ( ); // invoke-virtual {v0}, Lmiui/process/ProcessConfig;->getPolicy()I
(( com.android.server.am.ProcessSceneCleaner ) v4 ).getKillReason ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/am/ProcessSceneCleaner;->getKillReason(I)Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 116 */
android.os.Trace .traceBegin ( v2,v3,v1 );
/* .line 118 */
v1 = this.this$0;
com.android.server.am.ProcessSceneCleaner .-$$Nest$mhandleKillAny ( v1,v0 );
/* .line 119 */
android.os.Trace .traceEnd ( v2,v3 );
/* .line 120 */
/* .line 110 */
/* :pswitch_1 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "SceneSwipeKill"; // const-string v4, "SceneSwipeKill"
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.this$0;
/* .line 111 */
v5 = (( miui.process.ProcessConfig ) v0 ).getPolicy ( ); // invoke-virtual {v0}, Lmiui/process/ProcessConfig;->getPolicy()I
(( com.android.server.am.ProcessSceneCleaner ) v4 ).getKillReason ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/am/ProcessSceneCleaner;->getKillReason(I)Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 110 */
android.os.Trace .traceBegin ( v2,v3,v1 );
/* .line 112 */
v1 = this.this$0;
com.android.server.am.ProcessSceneCleaner .-$$Nest$mhandleSwipeKill ( v1,v0 );
/* .line 113 */
android.os.Trace .traceEnd ( v2,v3 );
/* .line 114 */
/* .line 104 */
/* :pswitch_2 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "SceneKillAll:"; // const-string v4, "SceneKillAll:"
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.this$0;
/* .line 105 */
v5 = (( miui.process.ProcessConfig ) v0 ).getPolicy ( ); // invoke-virtual {v0}, Lmiui/process/ProcessConfig;->getPolicy()I
(( com.android.server.am.ProcessSceneCleaner ) v4 ).getKillReason ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/am/ProcessSceneCleaner;->getKillReason(I)Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 104 */
android.os.Trace .traceBegin ( v2,v3,v1 );
/* .line 106 */
v1 = this.this$0;
com.android.server.am.ProcessSceneCleaner .-$$Nest$mhandleKillAll ( v1,v0 );
/* .line 107 */
android.os.Trace .traceEnd ( v2,v3 );
/* .line 108 */
/* nop */
/* .line 124 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
