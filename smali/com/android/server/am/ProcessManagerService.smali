.class public Lcom/android/server/am/ProcessManagerService;
.super Lmiui/process/ProcessManagerNative;
.source "ProcessManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/ProcessManagerService$MainHandler;,
        Lcom/android/server/am/ProcessManagerService$LocalService;,
        Lcom/android/server/am/ProcessManagerService$ProcessMangaerShellCommand;,
        Lcom/android/server/am/ProcessManagerService$Lifecycle;
    }
.end annotation


# static fields
.field private static final BINDER_MONITOR_FD_PATH:Ljava/lang/String; = "/proc/mi_log/binder_delay"

.field private static final DEBUG:Z = true

.field public static final DEVICE:Ljava/lang/String;

.field static final MAX_PROCESS_CONFIG_HISTORY:I = 0x1e

.field static final RESTORE_AI_PROCESSES_INFO_MSG:I = 0x1

.field static final SKIP_PRELOAD_COUNT_LIMIT:I = 0x2

.field private static final TAG:Ljava/lang/String; = "ProcessManager"

.field static final USER_OWNER:I = 0x0

.field static final USER_XSPACE:I = 0x3e7


# instance fields
.field private mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

.field private mAppOpsManager:Landroid/app/AppOpsManager;

.field private mBinderDelayWriter:Ljava/io/FileWriter;

.field private mContext:Landroid/content/Context;

.field private mDisplayManagerInternal:Landroid/hardware/display/DisplayManagerInternal;

.field private mForegroundInfoManager:Lcom/android/server/wm/ForegroundInfoManager;

.field final mHandler:Lcom/android/server/am/ProcessManagerService$MainHandler;

.field mHistoryNext:I

.field private final mInternal:Lcom/miui/server/process/ProcessManagerInternal;

.field private mLruProcesses:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/server/am/ProcessRecord;",
            ">;"
        }
    .end annotation
.end field

.field private mMiuiApplicationThreadManager:Lcom/android/server/am/MiuiApplicationThreadManager;

.field private mNotificationManager:Landroid/app/INotificationManager;

.field private mPerfService:Lcom/android/internal/app/IPerfShielder;

.field private mPkms:Landroid/content/pm/PackageManager;

.field private mPreloadAppController:Lcom/android/server/am/PreloadAppControllerImpl;

.field final mProcessConfigHistory:[Lmiui/process/ProcessConfig;

.field private mProcessKiller:Lcom/android/server/am/ProcessKiller;

.field private mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

.field private mProcessStarter:Lcom/android/server/am/ProcessStarter;

.field private mSchedBoostService:Lcom/miui/server/rtboost/SchedBoostManagerInternal;

.field final mServiceThread:Lcom/android/server/ServiceThread;

.field private mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

.field private mSystemSignatures:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Landroid/content/pm/Signature;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetmActivityManagerService(Lcom/android/server/am/ProcessManagerService;)Lcom/android/server/am/ActivityManagerService;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/ProcessManagerService;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmBinderDelayWriter(Lcom/android/server/am/ProcessManagerService;)Ljava/io/FileWriter;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/ProcessManagerService;->mBinderDelayWriter:Ljava/io/FileWriter;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmForegroundInfoManager(Lcom/android/server/am/ProcessManagerService;)Lcom/android/server/wm/ForegroundInfoManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/ProcessManagerService;->mForegroundInfoManager:Lcom/android/server/wm/ForegroundInfoManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMiuiApplicationThreadManager(Lcom/android/server/am/ProcessManagerService;)Lcom/android/server/am/MiuiApplicationThreadManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/ProcessManagerService;->mMiuiApplicationThreadManager:Lcom/android/server/am/MiuiApplicationThreadManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPreloadAppController(Lcom/android/server/am/ProcessManagerService;)Lcom/android/server/am/PreloadAppControllerImpl;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/ProcessManagerService;->mPreloadAppController:Lcom/android/server/am/PreloadAppControllerImpl;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmProcessPolicy(Lcom/android/server/am/ProcessManagerService;)Lcom/android/server/am/ProcessPolicy;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/ProcessManagerService;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmProcessStarter(Lcom/android/server/am/ProcessManagerService;)Lcom/android/server/am/ProcessStarter;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/ProcessManagerService;->mProcessStarter:Lcom/android/server/am/ProcessStarter;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mgenerateRunningProcessInfo(Lcom/android/server/am/ProcessManagerService;Lcom/android/server/am/ProcessRecord;)Lmiui/process/RunningProcessInfo;
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessManagerService;->generateRunningProcessInfo(Lcom/android/server/am/ProcessRecord;)Lmiui/process/RunningProcessInfo;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$misAppHasForegroundServices(Lcom/android/server/am/ProcessManagerService;Lcom/android/server/am/ProcessRecord;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessManagerService;->isAppHasForegroundServices(Lcom/android/server/am/ProcessRecord;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mnotifyAmsProcessKill(Lcom/android/server/am/ProcessManagerService;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/am/ProcessManagerService;->notifyAmsProcessKill(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mnotifyProcessDied(Lcom/android/server/am/ProcessManagerService;Lcom/android/server/am/ProcessRecord;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessManagerService;->notifyProcessDied(Lcom/android/server/am/ProcessRecord;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mnotifyProcessStarted(Lcom/android/server/am/ProcessManagerService;Lcom/android/server/am/ProcessRecord;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessManagerService;->notifyProcessStarted(Lcom/android/server/am/ProcessRecord;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 159
    sget-object v0, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/am/ProcessManagerService;->DEVICE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 181
    invoke-direct {p0}, Lmiui/process/ProcessManagerNative;-><init>()V

    .line 152
    const/16 v0, 0x1e

    new-array v0, v0, [Lmiui/process/ProcessConfig;

    iput-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mProcessConfigHistory:[Lmiui/process/ProcessConfig;

    .line 153
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/am/ProcessManagerService;->mHistoryNext:I

    .line 182
    iput-object p1, p0, Lcom/android/server/am/ProcessManagerService;->mContext:Landroid/content/Context;

    .line 183
    const-string v0, "appops"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AppOpsManager;

    iput-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mAppOpsManager:Landroid/app/AppOpsManager;

    .line 184
    invoke-static {}, Landroid/app/NotificationManager;->getService()Landroid/app/INotificationManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mNotificationManager:Landroid/app/INotificationManager;

    .line 185
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/ActivityManagerService;

    iput-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    .line 187
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mContext:Landroid/content/Context;

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 188
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mPkms:Landroid/content/pm/PackageManager;

    .line 190
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessList;->getLruProcessesLOSP()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mLruProcesses:Ljava/util/ArrayList;

    .line 192
    new-instance v0, Lcom/android/server/ServiceThread;

    const-string v1, "ProcessManager"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/android/server/ServiceThread;-><init>(Ljava/lang/String;IZ)V

    iput-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mServiceThread:Lcom/android/server/ServiceThread;

    .line 193
    invoke-virtual {v0}, Lcom/android/server/ServiceThread;->start()V

    .line 194
    new-instance v1, Lcom/android/server/am/ProcessManagerService$MainHandler;

    invoke-virtual {v0}, Lcom/android/server/ServiceThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/server/am/ProcessManagerService$MainHandler;-><init>(Lcom/android/server/am/ProcessManagerService;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mHandler:Lcom/android/server/am/ProcessManagerService$MainHandler;

    .line 196
    invoke-static {}, Lcom/android/server/am/PreloadAppControllerImpl;->getInstance()Lcom/android/server/am/PreloadAppControllerImpl;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/am/ProcessManagerService;->mPreloadAppController:Lcom/android/server/am/PreloadAppControllerImpl;

    .line 197
    iget-object v3, p0, Lcom/android/server/am/ProcessManagerService;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    invoke-virtual {v2, v3, p0, v0}, Lcom/android/server/am/PreloadAppControllerImpl;->init(Lcom/android/server/am/ActivityManagerService;Lcom/android/server/am/ProcessManagerService;Lcom/android/server/ServiceThread;)V

    .line 198
    new-instance v2, Lcom/android/server/am/ProcessKiller;

    iget-object v3, p0, Lcom/android/server/am/ProcessManagerService;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    invoke-direct {v2, v3}, Lcom/android/server/am/ProcessKiller;-><init>(Lcom/android/server/am/ActivityManagerService;)V

    iput-object v2, p0, Lcom/android/server/am/ProcessManagerService;->mProcessKiller:Lcom/android/server/am/ProcessKiller;

    .line 199
    new-instance v2, Lcom/android/server/am/ProcessPolicy;

    iget-object v3, p0, Lcom/android/server/am/ProcessManagerService;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    iget-object v4, p0, Lcom/android/server/am/ProcessManagerService;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-direct {v2, p0, v3, v4, v0}, Lcom/android/server/am/ProcessPolicy;-><init>(Lcom/android/server/am/ProcessManagerService;Lcom/android/server/am/ActivityManagerService;Landroid/view/accessibility/AccessibilityManager;Lcom/android/server/ServiceThread;)V

    iput-object v2, p0, Lcom/android/server/am/ProcessManagerService;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    .line 201
    new-instance v0, Lcom/android/server/am/ProcessStarter;

    iget-object v2, p0, Lcom/android/server/am/ProcessManagerService;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    invoke-direct {v0, p0, v2, v1}, Lcom/android/server/am/ProcessStarter;-><init>(Lcom/android/server/am/ProcessManagerService;Lcom/android/server/am/ActivityManagerService;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mProcessStarter:Lcom/android/server/am/ProcessStarter;

    .line 203
    invoke-static {}, Lcom/android/server/power/stats/BatteryStatsManagerStub;->getInstance()Lcom/android/server/power/stats/BatteryStatsManagerStub;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    invoke-interface {v0, v1}, Lcom/android/server/power/stats/BatteryStatsManagerStub;->setActiveCallback(Lcom/android/server/power/stats/BatteryStatsManagerStub$ActiveCallback;)V

    .line 205
    new-instance v0, Lcom/android/server/am/MiuiApplicationThreadManager;

    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    invoke-direct {v0, v1}, Lcom/android/server/am/MiuiApplicationThreadManager;-><init>(Lcom/android/server/am/ActivityManagerService;)V

    iput-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mMiuiApplicationThreadManager:Lcom/android/server/am/MiuiApplicationThreadManager;

    .line 206
    new-instance v0, Lcom/android/server/wm/ForegroundInfoManager;

    invoke-direct {v0, p0}, Lcom/android/server/wm/ForegroundInfoManager;-><init>(Lcom/android/server/am/ProcessManagerService;)V

    iput-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mForegroundInfoManager:Lcom/android/server/wm/ForegroundInfoManager;

    .line 207
    invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->systemReady()V

    .line 209
    new-instance v0, Lcom/android/server/am/ProcessManagerService$LocalService;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/am/ProcessManagerService$LocalService;-><init>(Lcom/android/server/am/ProcessManagerService;Lcom/android/server/am/ProcessManagerService$LocalService-IA;)V

    iput-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mInternal:Lcom/miui/server/process/ProcessManagerInternal;

    .line 210
    const-class v1, Lcom/miui/server/process/ProcessManagerInternal;

    invoke-static {v1, v0}, Lcom/android/server/LocalServices;->addService(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 211
    const-class v0, Landroid/hardware/display/DisplayManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManagerInternal;

    iput-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mDisplayManagerInternal:Landroid/hardware/display/DisplayManagerInternal;

    .line 212
    const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    iput-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    .line 213
    invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->probeCgroupVersion()V

    .line 214
    return-void
.end method

.method private addConfigToHistory(Lmiui/process/ProcessConfig;)V
    .locals 3
    .param p1, "config"    # Lmiui/process/ProcessConfig;

    .line 1525
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lmiui/process/ProcessConfig;->setKillingClockTime(J)V

    .line 1526
    iget v0, p0, Lcom/android/server/am/ProcessManagerService;->mHistoryNext:I

    const/4 v1, 0x1

    const/16 v2, 0x1e

    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/am/ProcessManagerService;->ringAdvance(III)I

    move-result v0

    iput v0, p0, Lcom/android/server/am/ProcessManagerService;->mHistoryNext:I

    .line 1527
    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mProcessConfigHistory:[Lmiui/process/ProcessConfig;

    aput-object p1, v1, v0

    .line 1528
    return-void
.end method

.method private fillRunningProcessInfoList(Ljava/util/List;Lcom/android/server/am/ProcessRecord;)V
    .locals 2
    .param p2, "app"    # Lcom/android/server/am/ProcessRecord;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lmiui/process/RunningProcessInfo;",
            ">;",
            "Lcom/android/server/am/ProcessRecord;",
            ")V"
        }
    .end annotation

    .line 1158
    .local p1, "infoList":Ljava/util/List;, "Ljava/util/List<Lmiui/process/RunningProcessInfo;>;"
    invoke-direct {p0, p2}, Lcom/android/server/am/ProcessManagerService;->generateRunningProcessInfo(Lcom/android/server/am/ProcessRecord;)Lmiui/process/RunningProcessInfo;

    move-result-object v0

    .line 1159
    .local v0, "info":Lmiui/process/RunningProcessInfo;
    if-eqz v0, :cond_0

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1160
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1162
    :cond_0
    return-void
.end method

.method private generateActiveUidInfoLocked(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;)Lmiui/process/ActiveUidInfo;
    .locals 3
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "activeUidRecord"    # Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;

    .line 1250
    const/4 v0, 0x0

    .line 1251
    .local v0, "info":Lmiui/process/ActiveUidInfo;
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->mErrorState:Lcom/android/server/am/ProcessErrorStateRecord;

    .line 1252
    invoke-virtual {v1}, Lcom/android/server/am/ProcessErrorStateRecord;->isCrashing()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->mErrorState:Lcom/android/server/am/ProcessErrorStateRecord;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessErrorStateRecord;->isNotResponding()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1253
    new-instance v1, Lmiui/process/ActiveUidInfo;

    invoke-direct {v1}, Lmiui/process/ActiveUidInfo;-><init>()V

    move-object v0, v1

    .line 1254
    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iput-object v1, v0, Lmiui/process/ActiveUidInfo;->packageName:Ljava/lang/String;

    .line 1255
    iget v1, p2, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->uid:I

    iput v1, v0, Lmiui/process/ActiveUidInfo;->uid:I

    .line 1256
    iget v1, p2, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->flag:I

    iput v1, v0, Lmiui/process/ActiveUidInfo;->flag:I

    .line 1257
    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I

    move-result v1

    iput v1, v0, Lmiui/process/ActiveUidInfo;->curAdj:I

    .line 1258
    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessStateRecord;->getCurProcState()I

    move-result v1

    iput v1, v0, Lmiui/process/ActiveUidInfo;->curProcState:I

    .line 1259
    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->mServices:Lcom/android/server/am/ProcessServiceRecord;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessServiceRecord;->hasForegroundServices()Z

    move-result v1

    iput-boolean v1, v0, Lmiui/process/ActiveUidInfo;->foregroundServices:Z

    .line 1260
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getUidRecord()Lcom/android/server/am/UidRecord;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/am/UidRecord;->getLastBackgroundTime()J

    move-result-wide v1

    iput-wide v1, v0, Lmiui/process/ActiveUidInfo;->lastBackgroundTime:J

    .line 1261
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getUidRecord()Lcom/android/server/am/UidRecord;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/am/UidRecord;->getNumOfProcs()I

    move-result v1

    iput v1, v0, Lmiui/process/ActiveUidInfo;->numProcs:I

    .line 1262
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPackageList()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lmiui/process/ActiveUidInfo;->pkgList:[Ljava/lang/String;

    .line 1264
    :cond_0
    return-object v0
.end method

.method private generateRunningProcessInfo(Lcom/android/server/am/ProcessRecord;)Lmiui/process/RunningProcessInfo;
    .locals 2
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 1165
    const/4 v0, 0x0

    .line 1166
    .local v0, "info":Lmiui/process/RunningProcessInfo;
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->mErrorState:Lcom/android/server/am/ProcessErrorStateRecord;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessErrorStateRecord;->isCrashing()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->mErrorState:Lcom/android/server/am/ProcessErrorStateRecord;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessErrorStateRecord;->isNotResponding()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1167
    new-instance v1, Lmiui/process/RunningProcessInfo;

    invoke-direct {v1}, Lmiui/process/RunningProcessInfo;-><init>()V

    move-object v0, v1

    .line 1168
    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    iput-object v1, v0, Lmiui/process/RunningProcessInfo;->mProcessName:Ljava/lang/String;

    .line 1169
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I

    move-result v1

    iput v1, v0, Lmiui/process/RunningProcessInfo;->mPid:I

    .line 1170
    iget v1, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    iput v1, v0, Lmiui/process/RunningProcessInfo;->mUid:I

    .line 1171
    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I

    move-result v1

    iput v1, v0, Lmiui/process/RunningProcessInfo;->mAdj:I

    .line 1172
    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessStateRecord;->getCurProcState()I

    move-result v1

    iput v1, v0, Lmiui/process/RunningProcessInfo;->mProcState:I

    .line 1173
    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessStateRecord;->hasForegroundActivities()Z

    move-result v1

    iput-boolean v1, v0, Lmiui/process/RunningProcessInfo;->mHasForegroundActivities:Z

    .line 1174
    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->mServices:Lcom/android/server/am/ProcessServiceRecord;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessServiceRecord;->hasForegroundServices()Z

    move-result v1

    iput-boolean v1, v0, Lmiui/process/RunningProcessInfo;->mHasForegroundServices:Z

    .line 1175
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPackageList()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lmiui/process/RunningProcessInfo;->mPkgList:[Ljava/lang/String;

    .line 1176
    iget-boolean v1, v0, Lmiui/process/RunningProcessInfo;->mHasForegroundServices:Z

    if-nez v1, :cond_1

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->mServices:Lcom/android/server/am/ProcessServiceRecord;

    .line 1177
    invoke-virtual {v1}, Lcom/android/server/am/ProcessServiceRecord;->getForegroundServiceTypes()I

    move-result v1

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    iput-boolean v1, v0, Lmiui/process/RunningProcessInfo;->mLocationForeground:Z

    .line 1180
    :cond_2
    return-object v0
.end method

.method private getAppNotificationWithFlag(Ljava/lang/String;II)Ljava/util/List;
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II)",
            "Ljava/util/List<",
            "Landroid/service/notification/StatusBarNotification;",
            ">;"
        }
    .end annotation

    .line 1184
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1186
    .local v0, "notificationList":Ljava/util/List;, "Ljava/util/List<Landroid/service/notification/StatusBarNotification;>;"
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mNotificationManager:Landroid/app/INotificationManager;

    .line 1187
    invoke-static {p2}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v2

    invoke-interface {v1, p1, v2}, Landroid/app/INotificationManager;->getAppActiveNotifications(Ljava/lang/String;I)Landroid/content/pm/ParceledListSlice;

    move-result-object v1

    .line 1188
    .local v1, "notificaionList":Landroid/content/pm/ParceledListSlice;, "Landroid/content/pm/ParceledListSlice<Landroid/service/notification/StatusBarNotification;>;"
    invoke-virtual {v1}, Landroid/content/pm/ParceledListSlice;->getList()Ljava/util/List;

    move-result-object v2

    .line 1189
    .local v2, "notifications":Ljava/util/List;, "Ljava/util/List<Landroid/service/notification/StatusBarNotification;>;"
    if-eqz v2, :cond_3

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_1

    .line 1192
    :cond_0
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/service/notification/StatusBarNotification;

    .line 1193
    .local v4, "statusBarNotification":Landroid/service/notification/StatusBarNotification;
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 1194
    invoke-virtual {v4}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v5

    .line 1195
    .local v5, "notification":Landroid/app/Notification;
    iget v6, v5, Landroid/app/Notification;->flags:I

    and-int/2addr v6, p3

    if-eqz v6, :cond_1

    .line 1196
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1199
    .end local v4    # "statusBarNotification":Landroid/service/notification/StatusBarNotification;
    .end local v5    # "notification":Landroid/app/Notification;
    :cond_1
    goto :goto_0

    .line 1202
    .end local v1    # "notificaionList":Landroid/content/pm/ParceledListSlice;, "Landroid/content/pm/ParceledListSlice<Landroid/service/notification/StatusBarNotification;>;"
    .end local v2    # "notifications":Ljava/util/List;, "Ljava/util/List<Landroid/service/notification/StatusBarNotification;>;"
    :cond_2
    goto :goto_2

    .line 1190
    .restart local v1    # "notificaionList":Landroid/content/pm/ParceledListSlice;, "Landroid/content/pm/ParceledListSlice<Landroid/service/notification/StatusBarNotification;>;"
    .restart local v2    # "notifications":Ljava/util/List;, "Ljava/util/List<Landroid/service/notification/StatusBarNotification;>;"
    :cond_3
    :goto_1
    return-object v0

    .line 1200
    .end local v1    # "notificaionList":Landroid/content/pm/ParceledListSlice;, "Landroid/content/pm/ParceledListSlice<Landroid/service/notification/StatusBarNotification;>;"
    .end local v2    # "notifications":Ljava/util/List;, "Ljava/util/List<Landroid/service/notification/StatusBarNotification;>;"
    :catch_0
    move-exception v1

    .line 1203
    :goto_2
    return-object v0
.end method

.method private getKillReason(I)Ljava/lang/String;
    .locals 1
    .param p1, "policy"    # I

    .line 429
    const/4 v0, 0x0

    .line 430
    .local v0, "reason":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 480
    :pswitch_0
    const-string v0, "Unknown"

    goto :goto_0

    .line 477
    :pswitch_1
    const-string v0, "DisplaySizeChanged"

    .line 478
    goto :goto_0

    .line 474
    :pswitch_2
    const-string v0, "AutoLockOffCleanByPriority"

    .line 475
    goto :goto_0

    .line 471
    :pswitch_3
    const-string v0, "AutoSystemAbnormalClean"

    .line 472
    goto :goto_0

    .line 468
    :pswitch_4
    const-string v0, "AutoLockOffClean"

    .line 469
    goto :goto_0

    .line 465
    :pswitch_5
    const-string v0, "AutoSleepClean"

    .line 466
    goto :goto_0

    .line 462
    :pswitch_6
    const-string v0, "AutoIdleKill"

    .line 463
    goto :goto_0

    .line 459
    :pswitch_7
    const-string v0, "AutoThermalKill"

    .line 460
    goto :goto_0

    .line 456
    :pswitch_8
    const-string v0, "AutoPowerKill"

    .line 457
    goto :goto_0

    .line 453
    :pswitch_9
    const-string v0, "UserDefined"

    .line 454
    goto :goto_0

    .line 450
    :pswitch_a
    const-string v0, "SwipeUpClean"

    .line 451
    goto :goto_0

    .line 438
    :pswitch_b
    const-string v0, "GarbageClean"

    .line 439
    goto :goto_0

    .line 447
    :pswitch_c
    const-string v0, "OptimizationClean"

    .line 448
    goto :goto_0

    .line 444
    :pswitch_d
    const-string v0, "GameClean"

    .line 445
    goto :goto_0

    .line 441
    :pswitch_e
    const-string v0, "LockScreenClean"

    .line 442
    goto :goto_0

    .line 435
    :pswitch_f
    const-string v0, "ForceClean"

    .line 436
    goto :goto_0

    .line 432
    :pswitch_10
    const-string v0, "OneKeyClean"

    .line 433
    nop

    .line 482
    :goto_0
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private getPackageNameByPid(I)Ljava/lang/String;
    .locals 2
    .param p1, "pid"    # I

    .line 683
    invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;

    move-result-object v0

    .line 684
    .local v0, "processRecord":Lcom/android/server/am/ProcessRecord;
    if-eqz v0, :cond_0

    .line 685
    iget-object v1, v0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    return-object v1

    .line 687
    :cond_0
    const/4 v1, 0x0

    return-object v1
.end method

.method private increaseRecordCount(Ljava/lang/String;Ljava/util/Map;)V
    .locals 2
    .param p1, "processName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 774
    .local p2, "recordMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 775
    .local v0, "expCount":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 776
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 778
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object v0, v1

    invoke-interface {p2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 779
    return-void
.end method

.method private isAppHasForegroundServices(Lcom/android/server/am/ProcessRecord;)Z
    .locals 2
    .param p1, "processRecord"    # Lcom/android/server/am/ProcessRecord;

    .line 768
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v0

    .line 769
    :try_start_0
    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->mServices:Lcom/android/server/am/ProcessServiceRecord;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessServiceRecord;->hasForegroundServices()Z

    move-result v1

    monitor-exit v0

    return v1

    .line 770
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private isPackageInList(Ljava/lang/String;I)Z
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "flags"    # I

    .line 640
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 641
    return v0

    .line 645
    :cond_0
    invoke-static {}, Lmiui/enterprise/ApplicationHelperStub;->getInstance()Lmiui/enterprise/IApplicationHelper;

    move-result-object v1

    invoke-interface {v1, p1}, Lmiui/enterprise/IApplicationHelper;->isKeepLiveWhiteApp(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    .line 646
    return v2

    .line 650
    :cond_1
    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    invoke-virtual {v1, p2}, Lcom/android/server/am/ProcessPolicy;->getWhiteList(I)Ljava/util/List;

    move-result-object v1

    .line 651
    .local v1, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 652
    .local v4, "item":Ljava/lang/String;
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 653
    return v2

    .line 655
    .end local v4    # "item":Ljava/lang/String;
    :cond_2
    goto :goto_0

    .line 656
    :cond_3
    return v0
.end method

.method private isSystemApp(I)Z
    .locals 2
    .param p1, "pid"    # I

    .line 660
    invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;

    move-result-object v0

    .line 661
    .local v0, "processRecord":Lcom/android/server/am/ProcessRecord;
    if-eqz v0, :cond_0

    .line 662
    invoke-direct {p0, v0}, Lcom/android/server/am/ProcessManagerService;->isSystemApp(Lcom/android/server/am/ProcessRecord;)Z

    move-result v1

    return v1

    .line 664
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method private isSystemApp(Lcom/android/server/am/ProcessRecord;)Z
    .locals 2
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 668
    const/4 v0, 0x0

    if-eqz p1, :cond_1

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    if-eqz v1, :cond_1

    .line 669
    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit16 v1, v1, 0x81

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    .line 672
    :cond_1
    return v0
.end method

.method private isUidSystem(I)Z
    .locals 1
    .param p1, "uid"    # I

    .line 677
    const v0, 0x186a0

    rem-int/2addr p1, v0

    .line 679
    const/16 v0, 0x2710

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private notifyAmsProcessKill(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
    .locals 5
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "reason"    # Ljava/lang/String;

    .line 1051
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    filled-new-array {v0, v1, v2, v3}, [Ljava/lang/Object;

    move-result-object v0

    const-string v1, "notifyProcessDied"

    invoke-static {v1, v0}, Lcom/android/server/camera/CameraOpt;->callMethod(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 1053
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 1054
    .local v0, "processName":Ljava/lang/String;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 1057
    :cond_0
    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mPreloadAppController:Lcom/android/server/am/PreloadAppControllerImpl;

    invoke-virtual {v1, p2, v0}, Lcom/android/server/am/PreloadAppControllerImpl;->onProcessKilled(Ljava/lang/String;Ljava/lang/String;)V

    .line 1058
    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mProcessStarter:Lcom/android/server/am/ProcessStarter;

    invoke-virtual {v1, v0, p2}, Lcom/android/server/am/ProcessStarter;->recordKillProcessIfNeeded(Ljava/lang/String;Ljava/lang/String;)V

    .line 1060
    const-class v1, Lcom/miui/server/migard/MiGardInternal;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/migard/MiGardInternal;

    .line 1061
    .local v1, "migard":Lcom/miui/server/migard/MiGardInternal;
    iget v2, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I

    move-result v3

    iget-object v4, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, p2}, Lcom/miui/server/migard/MiGardInternal;->onProcessKilled(IILjava/lang/String;Ljava/lang/String;)V

    .line 1062
    invoke-static {}, Lcom/android/server/am/MiProcessTracker;->getInstance()Lcom/android/server/am/MiProcessTracker;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Lcom/android/server/am/MiProcessTracker;->recordAmKillProcess(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V

    .line 1063
    return-void

    .line 1055
    .end local v1    # "migard":Lcom/miui/server/migard/MiGardInternal;
    :cond_1
    :goto_0
    return-void
.end method

.method private notifyProcessDied(Lcom/android/server/am/ProcessRecord;)V
    .locals 8
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 1080
    if-eqz p1, :cond_2

    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    if-eqz v0, :cond_2

    iget-boolean v0, p1, Lcom/android/server/am/ProcessRecord;->isolated:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 1083
    :cond_0
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 1084
    .local v0, "packageName":Ljava/lang/String;
    iget v1, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    .line 1085
    .local v1, "uid":I
    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 1086
    .local v2, "processName":Ljava/lang/String;
    invoke-static {}, Lcom/android/server/am/OomAdjusterImpl;->getInstance()Lcom/android/server/am/OomAdjusterImpl;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/android/server/am/OomAdjusterImpl;->notifyProcessDied(Lcom/android/server/am/ProcessRecord;)V

    .line 1087
    iget-object v3, p0, Lcom/android/server/am/ProcessManagerService;->mProcessStarter:Lcom/android/server/am/ProcessStarter;

    invoke-virtual {v3, v0, v2, v1}, Lcom/android/server/am/ProcessStarter;->restartCameraIfNeeded(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1088
    iget-object v3, p0, Lcom/android/server/am/ProcessManagerService;->mProcessStarter:Lcom/android/server/am/ProcessStarter;

    invoke-virtual {v3, v0, v2, v1}, Lcom/android/server/am/ProcessStarter;->recordDiedProcessIfNeeded(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1089
    const-class v3, Lcom/miui/server/migard/MiGardInternal;

    invoke-static {v3}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/migard/MiGardInternal;

    .line 1090
    .local v3, "miGardInternal":Lcom/miui/server/migard/MiGardInternal;
    if-eqz v3, :cond_1

    .line 1091
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/miui/server/migard/MiGardInternal;->notifyProcessDied(I)V

    .line 1093
    :cond_1
    iget v4, p1, Lcom/android/server/am/ProcessRecord;->mPid:I

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Lcom/android/server/am/ProcessManagerService;->enableBinderMonitor(II)V

    .line 1095
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-object v5, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget v5, v5, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget-object v6, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v6, v6, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v7, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    filled-new-array {v4, v5, v6, v7}, [Ljava/lang/Object;

    move-result-object v4

    const-string v5, "notifyProcessDied"

    invoke-static {v5, v4}, Lcom/android/server/camera/CameraOpt;->callMethod(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 1097
    return-void

    .line 1081
    .end local v0    # "packageName":Ljava/lang/String;
    .end local v1    # "uid":I
    .end local v2    # "processName":Ljava/lang/String;
    .end local v3    # "miGardInternal":Lcom/miui/server/migard/MiGardInternal;
    :cond_2
    :goto_0
    return-void
.end method

.method private notifyProcessStarted(Lcom/android/server/am/ProcessRecord;)V
    .locals 5
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 1045
    const-class v0, Lcom/miui/server/migard/MiGardInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/migard/MiGardInternal;

    .line 1046
    .local v0, "migard":Lcom/miui/server/migard/MiGardInternal;
    iget v1, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I

    move-result v2

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v4, p1, Lcom/android/server/am/ProcessRecord;->callerPackage:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/miui/server/migard/MiGardInternal;->onProcessStart(IILjava/lang/String;Ljava/lang/String;)V

    .line 1047
    return-void
.end method

.method private reduceRecordCountDelay(Ljava/lang/String;Ljava/util/Map;J)V
    .locals 2
    .param p1, "processName"    # Ljava/lang/String;
    .param p3, "delay"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;J)V"
        }
    .end annotation

    .line 783
    .local p2, "recordMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mHandler:Lcom/android/server/am/ProcessManagerService$MainHandler;

    new-instance v1, Lcom/android/server/am/ProcessManagerService$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/android/server/am/ProcessManagerService$1;-><init>(Lcom/android/server/am/ProcessManagerService;Ljava/util/Map;Ljava/lang/String;)V

    invoke-virtual {v0, v1, p3, p4}, Lcom/android/server/am/ProcessManagerService$MainHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 798
    return-void
.end method

.method private final ringAdvance(III)I
    .locals 1
    .param p1, "x"    # I
    .param p2, "increment"    # I
    .param p3, "ringSize"    # I

    .line 1518
    add-int/2addr p1, p2

    .line 1519
    if-gez p1, :cond_0

    add-int/lit8 v0, p3, -0x1

    return v0

    .line 1520
    :cond_0
    if-lt p1, p3, :cond_1

    const/4 v0, 0x0

    return v0

    .line 1521
    :cond_1
    return p1
.end method


# virtual methods
.method public addMiuiApplicationThread(Lmiui/process/IMiuiApplicationThread;I)V
    .locals 2
    .param p1, "applicationThread"    # Lmiui/process/IMiuiApplicationThread;
    .param p2, "pid"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 995
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    if-ne v0, p2, :cond_0

    .line 1001
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mMiuiApplicationThreadManager:Lcom/android/server/am/MiuiApplicationThreadManager;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/am/MiuiApplicationThreadManager;->addMiuiApplicationThread(Lmiui/process/IMiuiApplicationThread;I)V

    .line 1002
    return-void

    .line 996
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Permission Denial: ProcessManager.addMiuiApplicationThread() from pid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 997
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 998
    .local v0, "msg":Ljava/lang/String;
    const-string v1, "ProcessManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 999
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public beginSchedThreads([IJII)V
    .locals 9
    .param p1, "tids"    # [I
    .param p2, "duration"    # J
    .param p4, "pid"    # I
    .param p5, "mode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 1546
    invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z

    move-result v0

    const-string v1, "ProcessManager"

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkSystemSignature()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x3

    if-ne p5, v0, :cond_0

    goto :goto_0

    .line 1548
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Permission Denial: ProcessManager.beginSchedThreads() from pid="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1549
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", uid="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1550
    .local v0, "msg":Ljava/lang/String;
    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1551
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1553
    .end local v0    # "msg":Ljava/lang/String;
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mSchedBoostService:Lcom/miui/server/rtboost/SchedBoostManagerInternal;

    if-nez v0, :cond_2

    .line 1554
    const-class v0, Lcom/miui/server/rtboost/SchedBoostManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/rtboost/SchedBoostManagerInternal;

    iput-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mSchedBoostService:Lcom/miui/server/rtboost/SchedBoostManagerInternal;

    .line 1556
    :cond_2
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mSchedBoostService:Lcom/miui/server/rtboost/SchedBoostManagerInternal;

    if-eqz v0, :cond_7

    .line 1557
    if-eqz p1, :cond_6

    array-length v0, p1

    const/4 v2, 0x1

    if-ge v0, v2, :cond_3

    goto :goto_1

    .line 1560
    :cond_3
    if-gez p4, :cond_4

    .line 1561
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result p4

    .line 1563
    :cond_4
    invoke-direct {p0, p4}, Lcom/android/server/am/ProcessManagerService;->getPackageNameByPid(I)Ljava/lang/String;

    move-result-object v0

    .line 1565
    .local v0, "pkgName":Ljava/lang/String;
    invoke-static {}, Lcom/android/server/wm/RealTimeModeControllerStub;->get()Lcom/android/server/wm/RealTimeModeControllerStub;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/android/server/wm/RealTimeModeControllerStub;->checkCallerPermission(Ljava/lang/String;)Z

    move-result v8

    .line 1566
    .local v8, "isEnabled":Z
    if-nez v8, :cond_5

    .line 1567
    const-string v2, "beginSchedThreads is not Enabled"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1568
    return-void

    .line 1571
    :cond_5
    iget-object v2, p0, Lcom/android/server/am/ProcessManagerService;->mSchedBoostService:Lcom/miui/server/rtboost/SchedBoostManagerInternal;

    move-object v3, p1

    move-wide v4, p2

    move-object v6, v0

    move v7, p5

    invoke-interface/range {v2 .. v7}, Lcom/miui/server/rtboost/SchedBoostManagerInternal;->beginSchedThreads([IJLjava/lang/String;I)V

    goto :goto_2

    .line 1558
    .end local v0    # "pkgName":Ljava/lang/String;
    .end local v8    # "isEnabled":Z
    :cond_6
    :goto_1
    return-void

    .line 1573
    :cond_7
    :goto_2
    return-void
.end method

.method checkPermission()Z
    .locals 5

    .line 486
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    .line 487
    .local v0, "callingPid":I
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    .line 488
    .local v1, "callingUid":I
    const/16 v2, 0x2710

    const/4 v3, 0x1

    if-ge v1, v2, :cond_0

    .line 489
    return v3

    .line 492
    :cond_0
    invoke-virtual {p0, v0}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;

    move-result-object v2

    .line 493
    .local v2, "app":Lcom/android/server/am/ProcessRecord;
    invoke-direct {p0, v2}, Lcom/android/server/am/ProcessManagerService;->isSystemApp(Lcom/android/server/am/ProcessRecord;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 494
    return v3

    .line 497
    :cond_1
    const/4 v3, 0x0

    return v3
.end method

.method checkSystemSignature()Z
    .locals 2

    .line 501
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 502
    .local v0, "callingUid":I
    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-interface {v1, v0}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->checkSystemSignature(I)Z

    move-result v1

    return v1
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 6
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .line 1630
    invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1631
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Permission Denial: can\'t dump ProcessManager from pid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1632
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1633
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1631
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1634
    return-void

    .line 1637
    :cond_0
    const-string v0, "Process Config:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1638
    iget v0, p0, Lcom/android/server/am/ProcessManagerService;->mHistoryNext:I

    .line 1639
    .local v0, "lastIndex":I
    iget v1, p0, Lcom/android/server/am/ProcessManagerService;->mHistoryNext:I

    .line 1640
    .local v1, "ringIndex":I
    const/4 v2, 0x0

    .line 1642
    .local v2, "i":I
    :cond_1
    const/4 v3, -0x1

    if-ne v1, v3, :cond_2

    goto :goto_0

    .line 1643
    :cond_2
    iget-object v4, p0, Lcom/android/server/am/ProcessManagerService;->mProcessConfigHistory:[Lmiui/process/ProcessConfig;

    aget-object v4, v4, v1

    .line 1644
    .local v4, "config":Lmiui/process/ProcessConfig;
    if-nez v4, :cond_3

    goto :goto_0

    .line 1645
    :cond_3
    const-string v5, "  #"

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1646
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 1647
    const-string v5, ": "

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1648
    invoke-virtual {v4}, Lmiui/process/ProcessConfig;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1649
    const/16 v5, 0x1e

    invoke-direct {p0, v1, v3, v5}, Lcom/android/server/am/ProcessManagerService;->ringAdvance(III)I

    move-result v1

    .line 1650
    nop

    .end local v4    # "config":Lmiui/process/ProcessConfig;
    add-int/lit8 v2, v2, 0x1

    .line 1651
    if-ne v1, v0, :cond_1

    .line 1653
    :goto_0
    iget-object v3, p0, Lcom/android/server/am/ProcessManagerService;->mForegroundInfoManager:Lcom/android/server/wm/ForegroundInfoManager;

    const-string v4, "    "

    invoke-virtual {v3, p2, v4}, Lcom/android/server/wm/ForegroundInfoManager;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 1654
    iget-object v3, p0, Lcom/android/server/am/ProcessManagerService;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    invoke-virtual {v3, p2, v4}, Lcom/android/server/am/ProcessPolicy;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 1655
    return-void
.end method

.method public enableBinderMonitor(II)V
    .locals 2
    .param p1, "pid"    # I
    .param p2, "enable"    # I

    .line 1066
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mHandler:Lcom/android/server/am/ProcessManagerService$MainHandler;

    new-instance v1, Lcom/android/server/am/ProcessManagerService$6;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/server/am/ProcessManagerService$6;-><init>(Lcom/android/server/am/ProcessManagerService;II)V

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessManagerService$MainHandler;->post(Ljava/lang/Runnable;)Z

    .line 1077
    return-void
.end method

.method public enableHomeSchedBoost(Z)V
    .locals 2
    .param p1, "enable"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 1532
    invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1533
    const-string v0, "ProcessManager"

    const-string v1, "Permission Denial: can\'t enable Home Sched Boost"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1536
    :cond_0
    const-class v0, Lcom/miui/server/rtboost/SchedBoostManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/rtboost/SchedBoostManagerInternal;

    .line 1537
    .local v0, "si":Lcom/miui/server/rtboost/SchedBoostManagerInternal;
    if-eqz v0, :cond_1

    .line 1538
    invoke-interface {v0, p1}, Lcom/miui/server/rtboost/SchedBoostManagerInternal;->enableSchedBoost(Z)V

    .line 1540
    :cond_1
    return-void
.end method

.method public foregroundInfoChanged(Ljava/lang/String;Landroid/content/ComponentName;Ljava/lang/String;)V
    .locals 1
    .param p1, "foregroundPackageName"    # Ljava/lang/String;
    .param p2, "component"    # Landroid/content/ComponentName;
    .param p3, "resultToProc"    # Ljava/lang/String;

    .line 845
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mProcessStarter:Lcom/android/server/am/ProcessStarter;

    invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessStarter;->foregroundActivityChanged(Ljava/lang/String;)V

    .line 846
    invoke-static {}, Lcom/android/server/am/OomAdjusterImpl;->getInstance()Lcom/android/server/am/OomAdjusterImpl;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/am/OomAdjusterImpl;->foregroundInfoChanged(Ljava/lang/String;Landroid/content/ComponentName;Ljava/lang/String;)V

    .line 848
    return-void
.end method

.method public frequentlyKilledForPreload(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1285
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mProcessStarter:Lcom/android/server/am/ProcessStarter;

    invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessStarter;->frequentlyKilledForPreload(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public getActiveUidInfo(I)Ljava/util/List;
    .locals 11
    .param p1, "flag"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Lmiui/process/ActiveUidInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 1207
    invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1214
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessPolicy;->getActiveUidRecordList(I)Ljava/util/List;

    move-result-object v0

    .line 1215
    .local v0, "activeUidRecords":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;>;"
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    .line 1217
    .local v1, "activeUidInfos":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lmiui/process/ActiveUidInfo;>;"
    iget-object v2, p0, Lcom/android/server/am/ProcessManagerService;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v2

    .line 1218
    :try_start_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;

    .line 1219
    .local v4, "r":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;
    iget v5, v4, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->uid:I

    invoke-virtual {p0, v5}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordByUid(I)Ljava/util/List;

    move-result-object v5

    .line 1220
    .local v5, "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/server/am/ProcessRecord;

    .line 1222
    .local v7, "app":Lcom/android/server/am/ProcessRecord;
    iget v8, v4, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->uid:I

    invoke-virtual {v1, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lmiui/process/ActiveUidInfo;

    move-object v9, v8

    .local v9, "activeUidInfo":Lmiui/process/ActiveUidInfo;
    if-eqz v8, :cond_1

    .line 1224
    iget-object v8, v7, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v8}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I

    move-result v8

    iget v10, v9, Lmiui/process/ActiveUidInfo;->curAdj:I

    if-ge v8, v10, :cond_0

    .line 1225
    iget-object v8, v7, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v8}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I

    move-result v8

    iput v8, v9, Lmiui/process/ActiveUidInfo;->curAdj:I

    .line 1227
    :cond_0
    iget-object v8, v7, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v8}, Lcom/android/server/am/ProcessStateRecord;->getCurProcState()I

    move-result v8

    iget v10, v9, Lmiui/process/ActiveUidInfo;->curProcState:I

    if-ge v8, v10, :cond_2

    .line 1228
    iget-object v8, v7, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v8}, Lcom/android/server/am/ProcessStateRecord;->getCurProcState()I

    move-result v8

    iput v8, v9, Lmiui/process/ActiveUidInfo;->curProcState:I

    goto :goto_2

    .line 1231
    :cond_1
    invoke-direct {p0, v7, v4}, Lcom/android/server/am/ProcessManagerService;->generateActiveUidInfoLocked(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;)Lmiui/process/ActiveUidInfo;

    move-result-object v8

    .line 1232
    .end local v9    # "activeUidInfo":Lmiui/process/ActiveUidInfo;
    .local v8, "activeUidInfo":Lmiui/process/ActiveUidInfo;
    if-eqz v8, :cond_2

    .line 1233
    iget v9, v4, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->uid:I

    invoke-virtual {v1, v9, v8}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1236
    .end local v7    # "app":Lcom/android/server/am/ProcessRecord;
    .end local v8    # "activeUidInfo":Lmiui/process/ActiveUidInfo;
    :cond_2
    :goto_2
    goto :goto_1

    .line 1237
    .end local v4    # "r":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;
    .end local v5    # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    :cond_3
    goto :goto_0

    .line 1239
    :cond_4
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1241
    .local v3, "activeUidInfoList":Ljava/util/List;, "Ljava/util/List<Lmiui/process/ActiveUidInfo;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_3
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v5

    if-ge v4, v5, :cond_5

    .line 1242
    invoke-virtual {v1, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lmiui/process/ActiveUidInfo;

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1241
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 1245
    .end local v4    # "i":I
    :cond_5
    monitor-exit v2

    return-object v3

    .line 1246
    .end local v3    # "activeUidInfoList":Ljava/util/List;, "Ljava/util/List<Lmiui/process/ActiveUidInfo;>;"
    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 1208
    .end local v0    # "activeUidRecords":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;>;"
    .end local v1    # "activeUidInfos":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lmiui/process/ActiveUidInfo;>;"
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Permission Denial: ProcessManager.getActiveUidInfo() from pid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1209
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1210
    .local v0, "msg":Ljava/lang/String;
    const-string v1, "ProcessManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1211
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getForegroundApplicationThread()Lmiui/process/IMiuiApplicationThread;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 1006
    invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1012
    invoke-static {}, Lcom/android/server/wm/WindowProcessUtils;->getTopRunningPidLocked()I

    move-result v0

    .line 1013
    .local v0, "pid":I
    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mMiuiApplicationThreadManager:Lcom/android/server/am/MiuiApplicationThreadManager;

    invoke-virtual {v1, v0}, Lcom/android/server/am/MiuiApplicationThreadManager;->getMiuiApplicationThread(I)Lmiui/process/IMiuiApplicationThread;

    move-result-object v1

    return-object v1

    .line 1007
    .end local v0    # "pid":I
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Permission Denial: ProcessManager.getForegroundApplicationThread() from pid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1008
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1009
    .local v0, "msg":Ljava/lang/String;
    const-string v1, "ProcessManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1010
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getForegroundInfo()Lmiui/process/ForegroundInfo;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 984
    invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 990
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mForegroundInfoManager:Lcom/android/server/wm/ForegroundInfoManager;

    invoke-virtual {v0}, Lcom/android/server/wm/ForegroundInfoManager;->getForegroundInfo()Lmiui/process/ForegroundInfo;

    move-result-object v0

    return-object v0

    .line 985
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Permission Denial: ProcessManager.unregisterForegroundInfoListener() from pid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 986
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 987
    .local v0, "msg":Ljava/lang/String;
    const-string v1, "ProcessManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 988
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getInternal()Lcom/miui/server/process/ProcessManagerInternal;
    .locals 1

    .line 218
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mInternal:Lcom/miui/server/process/ProcessManagerInternal;

    return-object v0
.end method

.method protected getKillReason(Lmiui/process/ProcessConfig;)Ljava/lang/String;
    .locals 2
    .param p1, "config"    # Lmiui/process/ProcessConfig;

    .line 420
    invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getPolicy()I

    move-result v0

    .line 421
    .local v0, "policy":I
    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getReason()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 422
    invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getReason()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 424
    :cond_0
    invoke-direct {p0, v0}, Lcom/android/server/am/ProcessManagerService;->getKillReason(I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getLockedApplication(I)Ljava/util/List;
    .locals 1
    .param p1, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 532
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessPolicy;->getLockedApplication(I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getProcessKiller()Lcom/android/server/am/ProcessKiller;
    .locals 1

    .line 270
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mProcessKiller:Lcom/android/server/am/ProcessKiller;

    return-object v0
.end method

.method getProcessPolicy()Lcom/android/server/am/ProcessPolicy;
    .locals 1

    .line 266
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    return-object v0
.end method

.method public getProcessRecord(Ljava/lang/String;)Lcom/android/server/am/ProcessRecord;
    .locals 4
    .param p1, "processName"    # Ljava/lang/String;

    .line 712
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v0

    .line 713
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mLruProcesses:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_1

    .line 714
    iget-object v2, p0, Lcom/android/server/am/ProcessManagerService;->mLruProcesses:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/ProcessRecord;

    .line 715
    .local v2, "app":Lcom/android/server/am/ProcessRecord;
    invoke-virtual {v2}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, v2, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 716
    monitor-exit v0

    return-object v2

    .line 713
    .end local v2    # "app":Lcom/android/server/am/ProcessRecord;
    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 719
    .end local v1    # "i":I
    :cond_1
    monitor-exit v0

    .line 720
    const/4 v0, 0x0

    return-object v0

    .line 719
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getProcessRecord(Ljava/lang/String;I)Lcom/android/server/am/ProcessRecord;
    .locals 4
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 698
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v0

    .line 699
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mLruProcesses:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_1

    .line 700
    iget-object v2, p0, Lcom/android/server/am/ProcessManagerService;->mLruProcesses:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/ProcessRecord;

    .line 701
    .local v2, "app":Lcom/android/server/am/ProcessRecord;
    invoke-virtual {v2}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, v2, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget v3, v2, Lcom/android/server/am/ProcessRecord;->userId:I

    if-ne v3, p2, :cond_0

    .line 703
    monitor-exit v0

    return-object v2

    .line 699
    .end local v2    # "app":Lcom/android/server/am/ProcessRecord;
    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 706
    .end local v1    # "i":I
    :cond_1
    monitor-exit v0

    .line 707
    const/4 v0, 0x0

    return-object v0

    .line 706
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;
    .locals 2
    .param p1, "pid"    # I

    .line 691
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mPidsSelfLocked:Lcom/android/server/am/ActivityManagerService$PidMap;

    monitor-enter v0

    .line 692
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    iget-object v1, v1, Lcom/android/server/am/ActivityManagerService;->mPidsSelfLocked:Lcom/android/server/am/ActivityManagerService$PidMap;

    invoke-virtual {v1, p1}, Lcom/android/server/am/ActivityManagerService$PidMap;->get(I)Lcom/android/server/am/ProcessRecord;

    move-result-object v1

    monitor-exit v0

    return-object v1

    .line 693
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getProcessRecordByUid(I)Ljava/util/List;
    .locals 5
    .param p1, "uid"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Lcom/android/server/am/ProcessRecord;",
            ">;"
        }
    .end annotation

    .line 754
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 755
    .local v0, "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v1

    .line 756
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/ProcessManagerService;->mLruProcesses:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_1

    .line 757
    iget-object v3, p0, Lcom/android/server/am/ProcessManagerService;->mLruProcesses:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/ProcessRecord;

    .line 758
    .local v3, "app":Lcom/android/server/am/ProcessRecord;
    invoke-virtual {v3}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, v3, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    if-ne v4, p1, :cond_0

    .line 759
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 756
    .end local v3    # "app":Lcom/android/server/am/ProcessRecord;
    :cond_0
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 762
    .end local v2    # "i":I
    :cond_1
    monitor-exit v1

    .line 763
    return-object v0

    .line 762
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public getProcessRecordList(Ljava/lang/String;I)Ljava/util/List;
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List<",
            "Lcom/android/server/am/ProcessRecord;",
            ">;"
        }
    .end annotation

    .line 725
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 726
    .local v0, "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v1

    .line 727
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/ProcessManagerService;->mLruProcesses:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_1

    .line 728
    iget-object v3, p0, Lcom/android/server/am/ProcessManagerService;->mLruProcesses:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/ProcessRecord;

    .line 729
    .local v3, "app":Lcom/android/server/am/ProcessRecord;
    invoke-virtual {v3}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lcom/android/server/am/ProcessRecord;->getPkgList()Lcom/android/server/am/PackageList;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/android/server/am/PackageList;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget v4, v3, Lcom/android/server/am/ProcessRecord;->userId:I

    if-ne v4, p2, :cond_0

    .line 731
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 727
    .end local v3    # "app":Lcom/android/server/am/ProcessRecord;
    :cond_0
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 734
    .end local v2    # "i":I
    :cond_1
    monitor-exit v1

    .line 735
    return-object v0

    .line 734
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public getProcessRecordListByPackageAndUid(Ljava/lang/String;I)Ljava/util/List;
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List<",
            "Lcom/android/server/am/ProcessRecord;",
            ">;"
        }
    .end annotation

    .line 740
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 741
    .local v0, "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v1

    .line 742
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/ProcessManagerService;->mLruProcesses:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_1

    .line 743
    iget-object v3, p0, Lcom/android/server/am/ProcessManagerService;->mLruProcesses:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/ProcessRecord;

    .line 744
    .local v3, "app":Lcom/android/server/am/ProcessRecord;
    invoke-virtual {v3}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lcom/android/server/am/ProcessRecord;->getPkgList()Lcom/android/server/am/PackageList;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/android/server/am/PackageList;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v3, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    if-ne v4, p2, :cond_0

    .line 746
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 742
    .end local v3    # "app":Lcom/android/server/am/ProcessRecord;
    :cond_0
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 749
    .end local v2    # "i":I
    :cond_1
    monitor-exit v1

    .line 750
    return-object v0

    .line 749
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public getRenderThreadTidByPid(I)I
    .locals 3
    .param p1, "pid"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 1606
    invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z

    move-result v0

    const-string v1, "ProcessManager"

    if-eqz v0, :cond_2

    .line 1614
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getRenderThreadTidByPid, caller="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1617
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 1618
    invoke-static {}, Lcom/android/server/am/ProcessListStubImpl;->getInstance()Lcom/android/server/am/ProcessListStubImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/am/ProcessListStubImpl;->getSystemRenderThreadTid()I

    move-result v0

    return v0

    .line 1621
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;

    move-result-object v0

    .line 1622
    .local v0, "proc":Lcom/android/server/am/ProcessRecord;
    if-eqz v0, :cond_1

    .line 1623
    invoke-virtual {v0}, Lcom/android/server/am/ProcessRecord;->getRenderThreadTid()I

    move-result v1

    return v1

    .line 1625
    :cond_1
    const/4 v1, 0x0

    return v1

    .line 1607
    .end local v0    # "proc":Lcom/android/server/am/ProcessRecord;
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Permission Denial: ProcessManager.updateConfig() from pid="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1608
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", uid="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1609
    .local v0, "msg":Ljava/lang/String;
    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1610
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getRunningProcessInfo(IILjava/lang/String;Ljava/lang/String;I)Ljava/util/List;
    .locals 5
    .param p1, "pid"    # I
    .param p2, "uid"    # I
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "processName"    # Ljava/lang/String;
    .param p5, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List<",
            "Lmiui/process/RunningProcessInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 1102
    invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1110
    if-gtz p5, :cond_0

    .line 1111
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result p5

    move v0, p5

    goto :goto_0

    .line 1110
    :cond_0
    move v0, p5

    .line 1114
    .end local p5    # "userId":I
    .local v0, "userId":I
    :goto_0
    new-instance p5, Ljava/util/ArrayList;

    invoke-direct {p5}, Ljava/util/ArrayList;-><init>()V

    move-object v1, p5

    .line 1115
    .local v1, "processInfoList":Ljava/util/List;, "Ljava/util/List<Lmiui/process/RunningProcessInfo;>;"
    iget-object v2, p0, Lcom/android/server/am/ProcessManagerService;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v2

    .line 1117
    if-lez p1, :cond_1

    .line 1118
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;

    move-result-object p5

    .line 1119
    .local p5, "app":Lcom/android/server/am/ProcessRecord;
    invoke-direct {p0, v1, p5}, Lcom/android/server/am/ProcessManagerService;->fillRunningProcessInfoList(Ljava/util/List;Lcom/android/server/am/ProcessRecord;)V

    .line 1120
    monitor-exit v2

    return-object v1

    .line 1124
    .end local p5    # "app":Lcom/android/server/am/ProcessRecord;
    :cond_1
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p5

    if-nez p5, :cond_2

    .line 1125
    invoke-virtual {p0, p4, v0}, Lcom/android/server/am/ProcessManagerService;->getProcessRecord(Ljava/lang/String;I)Lcom/android/server/am/ProcessRecord;

    move-result-object p5

    .line 1126
    .restart local p5    # "app":Lcom/android/server/am/ProcessRecord;
    invoke-direct {p0, v1, p5}, Lcom/android/server/am/ProcessManagerService;->fillRunningProcessInfoList(Ljava/util/List;Lcom/android/server/am/ProcessRecord;)V

    .line 1127
    monitor-exit v2

    return-object v1

    .line 1131
    .end local p5    # "app":Lcom/android/server/am/ProcessRecord;
    :cond_2
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p5

    if-nez p5, :cond_4

    if-lez p2, :cond_4

    .line 1132
    invoke-virtual {p0, p3, p2}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordListByPackageAndUid(Ljava/lang/String;I)Ljava/util/List;

    move-result-object p5

    .line 1133
    .local p5, "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/am/ProcessRecord;

    .line 1134
    .local v4, "app":Lcom/android/server/am/ProcessRecord;
    invoke-direct {p0, v1, v4}, Lcom/android/server/am/ProcessManagerService;->fillRunningProcessInfoList(Ljava/util/List;Lcom/android/server/am/ProcessRecord;)V

    .line 1135
    .end local v4    # "app":Lcom/android/server/am/ProcessRecord;
    goto :goto_1

    .line 1136
    :cond_3
    monitor-exit v2

    return-object v1

    .line 1139
    .end local p5    # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    :cond_4
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p5

    if-nez p5, :cond_5

    .line 1140
    invoke-virtual {p0, p3, v0}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordList(Ljava/lang/String;I)Ljava/util/List;

    move-result-object p5

    .line 1141
    .restart local p5    # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/am/ProcessRecord;

    .line 1142
    .restart local v4    # "app":Lcom/android/server/am/ProcessRecord;
    invoke-direct {p0, v1, v4}, Lcom/android/server/am/ProcessManagerService;->fillRunningProcessInfoList(Ljava/util/List;Lcom/android/server/am/ProcessRecord;)V

    .line 1143
    .end local v4    # "app":Lcom/android/server/am/ProcessRecord;
    goto :goto_2

    .line 1146
    .end local p5    # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    :cond_5
    if-lez p2, :cond_6

    .line 1147
    invoke-virtual {p0, p2}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordByUid(I)Ljava/util/List;

    move-result-object p5

    .line 1148
    .restart local p5    # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/am/ProcessRecord;

    .line 1149
    .restart local v4    # "app":Lcom/android/server/am/ProcessRecord;
    invoke-direct {p0, v1, v4}, Lcom/android/server/am/ProcessManagerService;->fillRunningProcessInfoList(Ljava/util/List;Lcom/android/server/am/ProcessRecord;)V

    .line 1150
    .end local v4    # "app":Lcom/android/server/am/ProcessRecord;
    goto :goto_3

    .line 1152
    .end local p5    # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    :cond_6
    monitor-exit v2

    .line 1154
    return-object v1

    .line 1152
    :catchall_0
    move-exception p5

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p5

    .line 1103
    .end local v0    # "userId":I
    .end local v1    # "processInfoList":Ljava/util/List;, "Ljava/util/List<Lmiui/process/RunningProcessInfo;>;"
    .local p5, "userId":I
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Permission Denial: ProcessManager.getRunningProcessInfo() from pid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1104
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1105
    .local v0, "msg":Ljava/lang/String;
    const-string v1, "ProcessManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1106
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method isAllowAutoStart(Ljava/lang/String;I)Z
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 618
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mAppOpsManager:Landroid/app/AppOpsManager;

    const/16 v1, 0x2718

    invoke-virtual {v0, v1, p2, p1}, Landroid/app/AppOpsManager;->checkOpNoThrow(IILjava/lang/String;)I

    move-result v0

    .line 619
    .local v0, "mode":I
    if-nez v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method isAppDisabledForPkms(Ljava/lang/String;)Z
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .line 626
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mPkms:Landroid/content/pm/PackageManager;

    .line 627
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->isPackageSuspendedForUser(Ljava/lang/String;I)Z

    move-result v0

    .line 628
    .local v0, "isSuspend":Z
    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mPkms:Landroid/content/pm/PackageManager;

    .line 629
    invoke-virtual {v1, p1}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    move-result v1

    .line 630
    .local v1, "mode":I
    const/4 v2, 0x1

    if-eqz v0, :cond_0

    .line 631
    return v2

    .line 633
    :cond_0
    const/4 v3, 0x2

    if-eq v1, v3, :cond_2

    const/4 v3, 0x3

    if-eq v1, v3, :cond_2

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :cond_2
    :goto_0
    return v2
.end method

.method public isForceStopEnable(Lcom/android/server/am/ProcessRecord;I)Z
    .locals 4
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "policy"    # I

    .line 585
    const/16 v0, 0xd

    const/4 v1, 0x1

    if-ne p2, v0, :cond_0

    .line 586
    return v1

    .line 590
    :cond_0
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 591
    return v2

    .line 595
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessManagerService;->isSystemApp(Lcom/android/server/am/ProcessRecord;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 596
    return v2

    .line 600
    :cond_2
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {p0, v0, v3}, Lcom/android/server/am/ProcessManagerService;->isAllowAutoStart(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 601
    return v2

    .line 605
    :cond_3
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/16 v3, 0x22

    invoke-direct {p0, v0, v3}, Lcom/android/server/am/ProcessManagerService;->isPackageInList(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 607
    return v2

    .line 610
    :cond_4
    return v1
.end method

.method isInWhiteList(Lcom/android/server/am/ProcessRecord;II)Z
    .locals 5
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "userId"    # I
    .param p3, "policy"    # I

    .line 341
    iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    const/16 v1, 0x3ea

    invoke-static {v1}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v1

    const/4 v2, 0x1

    if-ne v0, v1, :cond_0

    .line 342
    return v2

    .line 346
    :cond_0
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-string v1, "com.android.cts"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 347
    const-string v1, "android.devicepolicy.cts"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto/16 :goto_2

    .line 351
    :cond_1
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    invoke-virtual {v0, p3}, Lcom/android/server/am/ProcessPolicy;->getPolicyFlags(I)I

    move-result v0

    .line 352
    .local v0, "flags":I
    sget-boolean v1, Lcom/miui/enterprise/settings/EnterpriseSettings;->ENTERPRISE_ACTIVATED:Z

    if-nez v1, :cond_2

    sget-boolean v1, Lmiui/enterprise/EnterpriseManagerStub;->ENTERPRISE_ACTIVATED:Z

    if-eqz v1, :cond_3

    .line 354
    :cond_2
    or-int/lit16 v0, v0, 0x1000

    .line 356
    :cond_3
    packed-switch p3, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_1

    .line 405
    :pswitch_1
    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/android/server/am/ProcessManagerService;->isPackageInList(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 406
    invoke-virtual {v1, v3}, Lcom/android/server/am/ProcessPolicy;->isInProcessStaticWhiteList(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    .line 407
    invoke-virtual {v1, p1}, Lcom/android/server/am/ProcessPolicy;->isProcessImportant(Lcom/android/server/am/ProcessRecord;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 408
    invoke-virtual {v1, v3}, Lcom/android/server/am/ProcessPolicy;->isInExtraPackageList(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 409
    invoke-virtual {v1, v3}, Lcom/android/server/am/ProcessPolicy;->isInSystemCleanWhiteList(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 410
    :cond_4
    return v2

    .line 403
    :pswitch_2
    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessPolicy;->isInDisplaySizeWhiteList(Ljava/lang/String;)Z

    move-result v1

    return v1

    .line 358
    :pswitch_3
    const/4 v0, 0x5

    .line 360
    sget-boolean v1, Lcom/miui/enterprise/settings/EnterpriseSettings;->ENTERPRISE_ACTIVATED:Z

    if-eqz v1, :cond_5

    .line 361
    or-int/lit16 v0, v0, 0x1000

    .line 364
    :cond_5
    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/android/server/am/ProcessManagerService;->isPackageInList(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 365
    invoke-virtual {v1, v3}, Lcom/android/server/am/ProcessPolicy;->isInProcessStaticWhiteList(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    .line 366
    invoke-virtual {v1, p1}, Lcom/android/server/am/ProcessPolicy;->isProcessImportant(Lcom/android/server/am/ProcessRecord;)Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v4, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 367
    invoke-virtual {v1, v3, v4, v2}, Lcom/android/server/am/ProcessPolicy;->isFastBootEnable(Ljava/lang/String;IZ)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 368
    :cond_6
    return v2

    .line 380
    :pswitch_4
    const/4 v0, 0x5

    .line 382
    sget-boolean v1, Lcom/miui/enterprise/settings/EnterpriseSettings;->ENTERPRISE_ACTIVATED:Z

    if-eqz v1, :cond_7

    .line 383
    or-int/lit16 v0, v0, 0x1000

    .line 386
    :cond_7
    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/android/server/am/ProcessManagerService;->isPackageInList(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 387
    invoke-virtual {v1, v3}, Lcom/android/server/am/ProcessPolicy;->isInProcessStaticWhiteList(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 388
    invoke-virtual {v1, v3, p2}, Lcom/android/server/am/ProcessPolicy;->isLockedApplication(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    .line 389
    invoke-virtual {v1, p1}, Lcom/android/server/am/ProcessPolicy;->isProcessImportant(Lcom/android/server/am/ProcessRecord;)Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v4, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 390
    invoke-virtual {v1, v3, v4, v2}, Lcom/android/server/am/ProcessPolicy;->isFastBootEnable(Ljava/lang/String;IZ)Z

    move-result v1

    if-eqz v1, :cond_8

    goto :goto_0

    .line 396
    :cond_8
    :pswitch_5
    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/android/server/am/ProcessManagerService;->isPackageInList(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_9

    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 397
    invoke-virtual {v1, v3}, Lcom/android/server/am/ProcessPolicy;->isInProcessStaticWhiteList(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_9

    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    .line 398
    invoke-virtual {v1, p1}, Lcom/android/server/am/ProcessPolicy;->isProcessImportant(Lcom/android/server/am/ProcessRecord;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 399
    :cond_9
    return v2

    .line 391
    :cond_a
    :goto_0
    return v2

    .line 416
    :cond_b
    :goto_1
    const/4 v1, 0x0

    return v1

    .line 348
    .end local v0    # "flags":I
    :cond_c
    :goto_2
    return v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_5
        :pswitch_5
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method

.method public isLockedApplication(Ljava/lang/String;I)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 544
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/am/ProcessPolicy;->isLockedApplication(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public isTrimMemoryEnable(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 614
    const/16 v0, 0x10

    invoke-direct {p0, p1, v0}, Lcom/android/server/am/ProcessManagerService;->isPackageInList(Ljava/lang/String;I)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public kill(Lmiui/process/ProcessConfig;)Z
    .locals 3
    .param p1, "config"    # Lmiui/process/ProcessConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 281
    invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z

    move-result v0

    if-nez v0, :cond_0

    .line 282
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Permission Denial: ProcessManager.kill() from pid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 283
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 284
    .local v0, "msg":Ljava/lang/String;
    const-string v1, "ProcessManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    const/4 v1, 0x0

    return v1

    .line 288
    .end local v0    # "msg":Ljava/lang/String;
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessManagerService;->addConfigToHistory(Lmiui/process/ProcessConfig;)V

    .line 289
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mContext:Landroid/content/Context;

    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/android/server/am/ProcessPolicy;->resetWhiteList(Landroid/content/Context;I)V

    .line 290
    invoke-static {}, Lcom/android/server/am/SystemPressureController;->getInstance()Lcom/android/server/am/SystemPressureController;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/am/SystemPressureController;->kill(Lmiui/process/ProcessConfig;)Z

    move-result v0

    .line 291
    .local v0, "success":Z
    invoke-static {}, Lcom/android/server/am/ProcessRecordStub;->get()Lcom/android/server/am/ProcessRecordStub;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/server/am/ProcessRecordStub;->reportAppPss()V

    .line 292
    return v0
.end method

.method public killAllBackgroundExceptLocked(Lmiui/process/ProcessConfig;)Z
    .locals 13
    .param p1, "config"    # Lmiui/process/ProcessConfig;

    .line 296
    invoke-virtual {p1}, Lmiui/process/ProcessConfig;->isPriorityInvalid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 297
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "priority:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getPriority()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is invalid"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 298
    .local v0, "msg":Ljava/lang/String;
    const-string v1, "ProcessManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    const/4 v1, 0x0

    return v1

    .line 301
    .end local v0    # "msg":Ljava/lang/String;
    :cond_0
    invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getPriority()I

    move-result v0

    .line 302
    .local v0, "maxProcState":I
    invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getReason()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 303
    invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getPolicy()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/android/server/am/ProcessManagerService;->getKillReason(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getReason()Ljava/lang/String;

    move-result-object v1

    .line 304
    .local v1, "killReason":Ljava/lang/String;
    :goto_0
    iget-object v2, p0, Lcom/android/server/am/ProcessManagerService;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v2

    .line 305
    :try_start_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 306
    .local v3, "procs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;"
    iget-object v4, p0, Lcom/android/server/am/ProcessManagerService;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    iget-object v4, v4, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    .line 307
    .local v4, "processList":Lcom/android/server/am/ProcessList;
    invoke-virtual {v4}, Lcom/android/server/am/ProcessList;->getProcessNamesLOSP()Lcom/android/server/am/ProcessList$MyProcessMap;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/server/am/ProcessList$MyProcessMap;->getMap()Landroid/util/ArrayMap;

    move-result-object v5

    invoke-virtual {v5}, Landroid/util/ArrayMap;->size()I

    move-result v5

    .line 308
    .local v5, "NP":I
    const/4 v6, 0x0

    .local v6, "ip":I
    :goto_1
    if-ge v6, v5, :cond_7

    .line 309
    invoke-virtual {v4}, Lcom/android/server/am/ProcessList;->getProcessNamesLOSP()Lcom/android/server/am/ProcessList$MyProcessMap;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/server/am/ProcessList$MyProcessMap;->getMap()Landroid/util/ArrayMap;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/util/SparseArray;

    .line 310
    .local v7, "apps":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/am/ProcessRecord;>;"
    invoke-virtual {v7}, Landroid/util/SparseArray;->size()I

    move-result v8

    .line 311
    .local v8, "NA":I
    const/4 v9, 0x0

    .local v9, "ia":I
    :goto_2
    if-ge v9, v8, :cond_6

    .line 312
    invoke-virtual {v7, v9}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/server/am/ProcessRecord;

    .line 313
    .local v10, "app":Lcom/android/server/am/ProcessRecord;
    invoke-virtual {v10}, Lcom/android/server/am/ProcessRecord;->isRemoved()Z

    move-result v11

    if-nez v11, :cond_4

    if-ltz v0, :cond_2

    iget-object v11, v10, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    .line 314
    invoke-virtual {v11}, Lcom/android/server/am/ProcessStateRecord;->getSetProcState()I

    move-result v11

    if-le v11, v0, :cond_3

    :cond_2
    iget-object v11, v10, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    .line 315
    invoke-virtual {v11}, Lcom/android/server/am/ProcessStateRecord;->hasShownUi()Z

    move-result v11

    if-nez v11, :cond_4

    .line 316
    :cond_3
    invoke-static {}, Lcom/android/server/display/DisplayManagerServiceStub;->getInstance()Lcom/android/server/display/DisplayManagerServiceStub;

    move-result-object v11

    iget-object v12, v10, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 317
    invoke-virtual {v11, v12}, Lcom/android/server/display/DisplayManagerServiceStub;->isInResolutionSwitchBlackList(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 318
    :cond_4
    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 311
    .end local v10    # "app":Lcom/android/server/am/ProcessRecord;
    :cond_5
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 308
    .end local v7    # "apps":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/am/ProcessRecord;>;"
    .end local v8    # "NA":I
    .end local v9    # "ia":I
    :cond_6
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 322
    .end local v6    # "ip":I
    :cond_7
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 323
    .local v6, "N":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_3
    const/4 v8, 0x1

    if-ge v7, v6, :cond_9

    .line 324
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/server/am/ProcessRecord;

    .line 328
    .local v9, "app":Lcom/android/server/am/ProcessRecord;
    invoke-static {}, Lcom/android/server/display/DisplayManagerServiceStub;->getInstance()Lcom/android/server/display/DisplayManagerServiceStub;

    move-result-object v10

    iget-object v11, v9, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 329
    invoke-virtual {v10, v11}, Lcom/android/server/display/DisplayManagerServiceStub;->isInResolutionSwitchProtectList(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 330
    goto :goto_4

    .line 332
    :cond_8
    iget-object v10, p0, Lcom/android/server/am/ProcessManagerService;->mProcessKiller:Lcom/android/server/am/ProcessKiller;

    invoke-virtual {v10, v9, v1, v8}, Lcom/android/server/am/ProcessKiller;->forceStopPackage(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Z)V

    .line 323
    .end local v9    # "app":Lcom/android/server/am/ProcessRecord;
    :goto_4
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 334
    .end local v3    # "procs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;"
    .end local v4    # "processList":Lcom/android/server/am/ProcessList;
    .end local v5    # "NP":I
    .end local v6    # "N":I
    .end local v7    # "i":I
    :cond_9
    monitor-exit v2

    .line 335
    return v8

    .line 334
    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public killPreloadApp(Ljava/lang/String;)I
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1281
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mPreloadAppController:Lcom/android/server/am/PreloadAppControllerImpl;

    invoke-virtual {v0, p1}, Lcom/android/server/am/PreloadAppControllerImpl;->killPreloadApp(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public notifyActivityChanged(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "curActivityComponent"    # Landroid/content/ComponentName;

    .line 1035
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mHandler:Lcom/android/server/am/ProcessManagerService$MainHandler;

    new-instance v1, Lcom/android/server/am/ProcessManagerService$5;

    invoke-direct {v1, p0, p1}, Lcom/android/server/am/ProcessManagerService$5;-><init>(Lcom/android/server/am/ProcessManagerService;Landroid/content/ComponentName;)V

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessManagerService$MainHandler;->post(Ljava/lang/Runnable;)Z

    .line 1042
    return-void
.end method

.method public notifyBluetoothEvent(ZIIILjava/lang/String;I)V
    .locals 7
    .param p1, "isConnect"    # Z
    .param p2, "bleType"    # I
    .param p3, "uid"    # I
    .param p4, "pid"    # I
    .param p5, "pkg"    # Ljava/lang/String;
    .param p6, "data"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 1595
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->onBluetoothEvent(ZIIILjava/lang/String;I)V

    .line 1596
    return-void
.end method

.method public notifyForegroundInfoChanged(Lcom/android/server/wm/FgActivityChangedInfo;)V
    .locals 2
    .param p1, "fgInfo"    # Lcom/android/server/wm/FgActivityChangedInfo;

    .line 1017
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mHandler:Lcom/android/server/am/ProcessManagerService$MainHandler;

    new-instance v1, Lcom/android/server/am/ProcessManagerService$3;

    invoke-direct {v1, p0, p1}, Lcom/android/server/am/ProcessManagerService$3;-><init>(Lcom/android/server/am/ProcessManagerService;Lcom/android/server/wm/FgActivityChangedInfo;)V

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessManagerService$MainHandler;->post(Ljava/lang/Runnable;)Z

    .line 1023
    return-void
.end method

.method public notifyForegroundWindowChanged(Lcom/android/server/wm/FgWindowChangedInfo;)V
    .locals 2
    .param p1, "fgInfo"    # Lcom/android/server/wm/FgWindowChangedInfo;

    .line 1026
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mHandler:Lcom/android/server/am/ProcessManagerService$MainHandler;

    new-instance v1, Lcom/android/server/am/ProcessManagerService$4;

    invoke-direct {v1, p0, p1}, Lcom/android/server/am/ProcessManagerService$4;-><init>(Lcom/android/server/am/ProcessManagerService;Lcom/android/server/wm/FgWindowChangedInfo;)V

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessManagerService$MainHandler;->post(Ljava/lang/Runnable;)Z

    .line 1032
    return-void
.end method

.method public onShellCommand(Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)V
    .locals 8
    .param p1, "in"    # Ljava/io/FileDescriptor;
    .param p2, "out"    # Ljava/io/FileDescriptor;
    .param p3, "err"    # Ljava/io/FileDescriptor;
    .param p4, "args"    # [Ljava/lang/String;
    .param p5, "callback"    # Landroid/os/ShellCallback;
    .param p6, "resultReceiver"    # Landroid/os/ResultReceiver;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 1292
    new-instance v0, Lcom/android/server/am/ProcessManagerService$ProcessMangaerShellCommand;

    invoke-direct {v0, p0}, Lcom/android/server/am/ProcessManagerService$ProcessMangaerShellCommand;-><init>(Lcom/android/server/am/ProcessManagerService;)V

    .line 1293
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, Lcom/android/server/am/ProcessManagerService$ProcessMangaerShellCommand;->exec(Landroid/os/Binder;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)I

    .line 1294
    return-void
.end method

.method public protectCurrentProcess(ZI)Z
    .locals 6
    .param p1, "isProtected"    # Z
    .param p2, "timeout"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 852
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;

    move-result-object v0

    .line 853
    .local v0, "app":Lcom/android/server/am/ProcessRecord;
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    iget-object v2, v0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessPolicy;->isInAppProtectList(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 860
    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    invoke-virtual {v1, v0, p1}, Lcom/android/server/am/ProcessPolicy;->protectCurrentProcess(Lcom/android/server/am/ProcessRecord;Z)Z

    move-result v1

    .line 861
    .local v1, "success":Z
    if-eqz p1, :cond_0

    if-lez p2, :cond_0

    .line 862
    iget-object v2, p0, Lcom/android/server/am/ProcessManagerService;->mHandler:Lcom/android/server/am/ProcessManagerService$MainHandler;

    new-instance v3, Lcom/android/server/am/ProcessManagerService$2;

    invoke-direct {v3, p0, v0}, Lcom/android/server/am/ProcessManagerService$2;-><init>(Lcom/android/server/am/ProcessManagerService;Lcom/android/server/am/ProcessRecord;)V

    int-to-long v4, p2

    invoke-virtual {v2, v3, v4, v5}, Lcom/android/server/am/ProcessManagerService$MainHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 870
    :cond_0
    return v1

    .line 854
    .end local v1    # "success":Z
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Permission Denial: ProcessManager.protectCurrentProcess() from pid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 855
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", uid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 856
    .local v1, "msg":Ljava/lang/String;
    const-string v2, "ProcessManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 857
    new-instance v2, Ljava/lang/SecurityException;

    invoke-direct {v2, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public registerActivityChangeListener(Ljava/util/List;Ljava/util/List;Lmiui/process/IActivityChangeListener;)V
    .locals 2
    .param p3, "listener"    # Lmiui/process/IActivityChangeListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lmiui/process/IActivityChangeListener;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 961
    .local p1, "targetPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "targetActivities":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 967
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mForegroundInfoManager:Lcom/android/server/wm/ForegroundInfoManager;

    .line 968
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/wm/ForegroundInfoManager;->registerActivityChangeListener(Ljava/util/List;Ljava/util/List;Lmiui/process/IActivityChangeListener;)V

    .line 969
    return-void

    .line 962
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Permission Denial: ProcessManager.registerActivityChangeListener() from pid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 963
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 964
    .local v0, "msg":Ljava/lang/String;
    const-string v1, "ProcessManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 965
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public registerForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V
    .locals 3
    .param p1, "listener"    # Lmiui/process/IForegroundInfoListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 905
    invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z

    move-result v0

    const-string v1, "ProcessManager"

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkSystemSignature()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 906
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Permission Denial: ProcessManager.registerForegroundInfoListener() from pid="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 907
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", uid="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 908
    .local v0, "msg":Ljava/lang/String;
    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 909
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 912
    .end local v0    # "msg":Ljava/lang/String;
    :cond_1
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "registerForegroundInfoListener, caller="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", listener="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 915
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mForegroundInfoManager:Lcom/android/server/wm/ForegroundInfoManager;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/ForegroundInfoManager;->registerForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V

    .line 916
    return-void
.end method

.method public registerForegroundWindowListener(Lmiui/process/IForegroundWindowListener;)V
    .locals 3
    .param p1, "listener"    # Lmiui/process/IForegroundWindowListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 933
    invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z

    move-result v0

    const-string v1, "ProcessManager"

    if-eqz v0, :cond_0

    .line 940
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "registerForegroundWindowListener, caller="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", listener="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 943
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mForegroundInfoManager:Lcom/android/server/wm/ForegroundInfoManager;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/ForegroundInfoManager;->registerForegroundWindowListener(Lmiui/process/IForegroundWindowListener;)V

    .line 944
    return-void

    .line 934
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Permission Denial: ProcessManager.registerForegroundWindowListener() from pid="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 935
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", uid="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 936
    .local v0, "msg":Ljava/lang/String;
    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 937
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public registerPreloadCallback(Lmiui/process/IPreloadCallback;I)V
    .locals 1
    .param p1, "callback"    # Lmiui/process/IPreloadCallback;
    .param p2, "type"    # I

    .line 1269
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mPreloadAppController:Lcom/android/server/am/PreloadAppControllerImpl;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/am/PreloadAppControllerImpl;->registerPreloadCallback(Lmiui/process/IPreloadCallback;I)V

    .line 1270
    return-void
.end method

.method public reportGameScene(Ljava/lang/String;II)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "gameScene"    # I
    .param p3, "appState"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 1578
    invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z

    move-result v0

    const-string v1, "ProcessManager"

    if-eqz v0, :cond_0

    .line 1585
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pms : packageName = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " gameScene ="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " appState = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1588
    invoke-static {}, Lcom/android/server/am/OomAdjusterImpl;->getInstance()Lcom/android/server/am/OomAdjusterImpl;

    move-result-object v0

    .line 1589
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/am/OomAdjusterImpl;->updateGameSceneRecordMap(Ljava/lang/String;II)V

    .line 1590
    return-void

    .line 1579
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Permission Denial: ProcessManager.reportGameScene() from pid="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1580
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", uid="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1581
    .local v0, "msg":Ljava/lang/String;
    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1582
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public reportTrackStatus(IIIZ)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "sessionId"    # I
    .param p4, "isMuted"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 1601
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->reportTrackStatus(IIIZ)V

    .line 1602
    return-void
.end method

.method public setProcessMaxAdjLock(ILcom/android/server/am/ProcessRecord;II)V
    .locals 2
    .param p1, "userId"    # I
    .param p2, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p3, "maxAdj"    # I
    .param p4, "maxProcState"    # I

    .line 876
    if-nez p2, :cond_0

    .line 877
    return-void

    .line 879
    :cond_0
    sget v0, Lmiui/process/ProcessManager;->LOCKED_MAX_ADJ:I

    if-le p3, v0, :cond_1

    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    iget-object v1, p2, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 880
    invoke-virtual {v0, v1, p1}, Lcom/android/server/am/ProcessPolicy;->isLockedApplication(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 881
    sget p3, Lmiui/process/ProcessManager;->LOCKED_MAX_ADJ:I

    .line 882
    sget p4, Lmiui/process/ProcessManager;->LOCKED_MAX_PROCESS_STATE:I

    .line 884
    :cond_1
    iget-object v0, p2, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v0, p3}, Lcom/android/server/am/ProcessStateRecord;->setMaxAdj(I)V

    .line 885
    invoke-static {p2, p4}, Lcom/android/server/am/IProcessPolicy;->setAppMaxProcState(Lcom/android/server/am/ProcessRecord;I)V

    .line 886
    return-void
.end method

.method public shutdown()V
    .locals 0

    .line 263
    return-void
.end method

.method public skipCurrentProcessInBackup(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;I)Z
    .locals 4
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "userId"    # I

    .line 556
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lmiui/app/backup/BackupManager;->getBackupManager(Landroid/content/Context;)Lmiui/app/backup/BackupManager;

    move-result-object v0

    .line 557
    .local v0, "backupManager":Lmiui/app/backup/BackupManager;
    invoke-virtual {v0}, Lmiui/app/backup/BackupManager;->getState()I

    move-result v1

    if-eqz v1, :cond_3

    .line 558
    invoke-virtual {v0}, Lmiui/app/backup/BackupManager;->getCurrentRunningPackage()Ljava/lang/String;

    move-result-object v1

    .line 559
    .local v1, "curRunningPkg":Ljava/lang/String;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    if-eqz p1, :cond_3

    .line 560
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPkgList()Lcom/android/server/am/PackageList;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/android/server/am/PackageList;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p1, Lcom/android/server/am/ProcessRecord;->userId:I

    if-ne v2, p3, :cond_3

    .line 562
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "skip kill:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p1, :cond_2

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    goto :goto_0

    :cond_2
    move-object v3, p2

    :goto_0
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for Backup app"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ProcessManager"

    invoke-static {v3, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 563
    const/4 v2, 0x1

    return v2

    .line 566
    .end local v1    # "curRunningPkg":Ljava/lang/String;
    :cond_3
    const/4 v1, 0x0

    return v1
.end method

.method public startPreloadApp(Ljava/lang/String;ZZLmiui/process/LifecycleConfig;)I
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "ignoreMemory"    # Z
    .param p3, "sync"    # Z
    .param p4, "config"    # Lmiui/process/LifecycleConfig;

    .line 1275
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mPreloadAppController:Lcom/android/server/am/PreloadAppControllerImpl;

    invoke-virtual {v0, p1, p2, p4}, Lcom/android/server/am/PreloadAppControllerImpl;->preloadAppEnqueue(Ljava/lang/String;ZLmiui/process/LifecycleConfig;)V

    .line 1276
    const/16 v0, 0x1f4

    return v0
.end method

.method public startProcesses(Ljava/util/List;IZII)I
    .locals 8
    .param p2, "startProcessCount"    # I
    .param p3, "ignoreMemory"    # Z
    .param p4, "userId"    # I
    .param p5, "flag"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lmiui/process/PreloadProcessData;",
            ">;IZII)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 814
    .local p1, "dataList":Ljava/util/List;, "Ljava/util/List<Lmiui/process/PreloadProcessData;>;"
    invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z

    move-result v0

    const-string v1, "ProcessManager"

    if-eqz v0, :cond_5

    .line 820
    if-eqz p1, :cond_4

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_4

    .line 823
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lt v0, p2, :cond_3

    .line 826
    const/4 v0, 0x0

    if-nez p3, :cond_0

    invoke-static {}, Lcom/android/server/am/ProcessUtils;->isLowMemory()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 827
    const-string v2, "low memory! skip start process!"

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 828
    return v0

    .line 830
    :cond_0
    if-gtz p2, :cond_1

    .line 831
    const-string/jumbo v2, "startProcessCount <= 0, skip start process!"

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 832
    return v0

    .line 835
    :cond_1
    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mProcessStarter:Lcom/android/server/am/ProcessStarter;

    invoke-virtual {v1, p1, p5}, Lcom/android/server/am/ProcessStarter;->isAllowPreloadProcess(Ljava/util/List;I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 836
    return v0

    .line 839
    :cond_2
    iget-object v2, p0, Lcom/android/server/am/ProcessManagerService;->mProcessStarter:Lcom/android/server/am/ProcessStarter;

    move-object v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    move v7, p5

    invoke-virtual/range {v2 .. v7}, Lcom/android/server/am/ProcessStarter;->startProcesses(Ljava/util/List;IZII)I

    move-result v0

    return v0

    .line 824
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "illegal start number!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 821
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "packageNames cannot be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 815
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Permission Denial: ProcessManager.startMutiProcesses() from pid="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 816
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", uid="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 817
    .local v0, "msg":Ljava/lang/String;
    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 818
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected systemReady()V
    .locals 2

    .line 222
    nop

    .line 223
    const-string v0, "perfshielder"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 222
    invoke-static {v0}, Lcom/android/internal/app/IPerfShielder$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/app/IPerfShielder;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mPerfService:Lcom/android/internal/app/IPerfShielder;

    .line 224
    if-eqz v0, :cond_0

    .line 225
    check-cast v0, Lcom/miui/server/PerfShielderService;

    invoke-virtual {v0}, Lcom/miui/server/PerfShielderService;->systemReady()V

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessPolicy;->systemReady(Landroid/content/Context;)V

    .line 228
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mProcessStarter:Lcom/android/server/am/ProcessStarter;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessStarter;->systemReady()V

    .line 229
    invoke-static {}, Lcom/android/server/am/MiProcessTracker;->getInstance()Lcom/android/server/am/MiProcessTracker;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/server/am/MiProcessTracker;->systemReady(Landroid/content/Context;)V

    .line 231
    :try_start_0
    new-instance v0, Ljava/io/FileWriter;

    const-string v1, "/proc/mi_log/binder_delay"

    invoke-direct {v0, v1}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mBinderDelayWriter:Ljava/io/FileWriter;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 234
    goto :goto_0

    .line 232
    :catch_0
    move-exception v0

    .line 233
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 235
    .end local v0    # "e":Ljava/io/IOException;
    :goto_0
    return-void
.end method

.method public unregisterActivityChangeListener(Lmiui/process/IActivityChangeListener;)V
    .locals 2
    .param p1, "listener"    # Lmiui/process/IActivityChangeListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 973
    invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 979
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mForegroundInfoManager:Lcom/android/server/wm/ForegroundInfoManager;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/ForegroundInfoManager;->unregisterActivityChangeListener(Lmiui/process/IActivityChangeListener;)V

    .line 980
    return-void

    .line 974
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Permission Denial: ProcessManager.unregisterActivityChangeListener() from pid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 975
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 976
    .local v0, "msg":Ljava/lang/String;
    const-string v1, "ProcessManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 977
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public unregisterForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V
    .locals 2
    .param p1, "listener"    # Lmiui/process/IForegroundInfoListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 921
    invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkSystemSignature()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 922
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Permission Denial: ProcessManager.unregisterForegroundInfoListener() from pid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 923
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 924
    .local v0, "msg":Ljava/lang/String;
    const-string v1, "ProcessManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 925
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 927
    .end local v0    # "msg":Ljava/lang/String;
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mForegroundInfoManager:Lcom/android/server/wm/ForegroundInfoManager;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/ForegroundInfoManager;->unregisterForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V

    .line 928
    return-void
.end method

.method public unregisterForegroundWindowListener(Lmiui/process/IForegroundWindowListener;)V
    .locals 2
    .param p1, "listener"    # Lmiui/process/IForegroundWindowListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 949
    invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 955
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mForegroundInfoManager:Lcom/android/server/wm/ForegroundInfoManager;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/ForegroundInfoManager;->unregisterForegroundWindowListener(Lmiui/process/IForegroundWindowListener;)V

    .line 956
    return-void

    .line 950
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Permission Denial: ProcessManager.unregisterForegroundWindowListener() from pid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 951
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 952
    .local v0, "msg":Ljava/lang/String;
    const-string v1, "ProcessManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 953
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public updateApplicationLockedState(Ljava/lang/String;IZ)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I
    .param p3, "isLocked"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 515
    invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 521
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p2, p1, p3}, Lcom/android/server/am/ProcessPolicy;->updateApplicationLockedState(Landroid/content/Context;ILjava/lang/String;Z)V

    .line 522
    return-void

    .line 516
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Permission Denial: ProcessManager.updateApplicationLockedState() from pid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 517
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 518
    .local v0, "msg":Ljava/lang/String;
    const-string v1, "ProcessManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public updateCloudData(Lmiui/process/ProcessCloudData;)V
    .locals 2
    .param p1, "cloudData"    # Lmiui/process/ProcessCloudData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 890
    invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 896
    if-eqz p1, :cond_0

    .line 899
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessPolicy;->updateCloudData(Lmiui/process/ProcessCloudData;)V

    .line 900
    return-void

    .line 897
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "cloudData cannot be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 891
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Permission Denial: ProcessManager.updateCloudWhiteList() from pid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 892
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 893
    .local v0, "msg":Ljava/lang/String;
    const-string v1, "ProcessManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 894
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public updateConfig(Lmiui/process/ProcessConfig;)V
    .locals 2
    .param p1, "config"    # Lmiui/process/ProcessConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 802
    invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 809
    return-void

    .line 803
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Permission Denial: ProcessManager.updateConfig() from pid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 804
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 805
    .local v0, "msg":Ljava/lang/String;
    const-string v1, "ProcessManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 806
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
