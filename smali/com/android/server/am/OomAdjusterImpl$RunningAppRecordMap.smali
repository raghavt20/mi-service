.class Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;
.super Ljava/lang/Object;
.source "OomAdjusterImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/OomAdjusterImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RunningAppRecordMap"
.end annotation


# instance fields
.field private final mList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mMap:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 704
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 709
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;->mMap:Landroid/util/ArrayMap;

    .line 714
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;->mList:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap-IA;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .line 746
    iget-object v0, p0, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;->mMap:Landroid/util/ArrayMap;

    invoke-virtual {v0}, Landroid/util/ArrayMap;->clear()V

    .line 747
    iget-object v0, p0, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;->mList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 748
    return-void
.end method

.method public getForKey(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 717
    iget-object v0, p0, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;->mMap:Landroid/util/ArrayMap;

    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 718
    .local v0, "value":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, -0x1

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public getIndexForKey(Ljava/lang/String;)I
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 722
    iget-object v0, p0, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;->mList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 751
    iget-object v0, p0, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;->mList:Ljava/util/List;

    return-object v0
.end method

.method public put(ILjava/lang/String;Ljava/lang/Integer;)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "gameScence"    # Ljava/lang/Integer;

    .line 733
    iget-object v0, p0, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;->mList:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 734
    iget-object v0, p0, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;->mList:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 736
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;->mList:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 737
    iget-object v0, p0, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;->mMap:Landroid/util/ArrayMap;

    invoke-virtual {v0, p2, p3}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 738
    return-void
.end method

.method public put(Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "gameScence"    # Ljava/lang/Integer;

    .line 726
    iget-object v0, p0, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;->mList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 727
    iget-object v0, p0, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;->mList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 729
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;->mMap:Landroid/util/ArrayMap;

    invoke-virtual {v0, p1, p2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 730
    return-void
.end method

.method public removeForKey(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 741
    iget-object v0, p0, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;->mMap:Landroid/util/ArrayMap;

    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 742
    iget-object v0, p0, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;->mList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 743
    return-void
.end method

.method public size()I
    .locals 1

    .line 755
    iget-object v0, p0, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;->mList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
