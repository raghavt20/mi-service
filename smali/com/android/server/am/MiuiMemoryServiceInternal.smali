.class public interface abstract Lcom/android/server/am/MiuiMemoryServiceInternal;
.super Ljava/lang/Object;
.source "MiuiMemoryServiceInternal.java"


# static fields
.field public static final COMPACT_MODE_CAMERA:I = 0x0

.field public static final COMPACT_MODE_LMKD:I = 0x3

.field public static final COMPACT_MODE_PMC:I = 0x4

.field public static final COMPACT_MODE_SCREENOFF_CHARGING:I = 0x2

.field public static final COMPACT_MODE_SPTM:I = 0x1

.field public static final VMPRESS_LEVEL_CRITICAL:I = 0x2

.field public static final VMPRESS_LEVEL_LOW:I = 0x0

.field public static final VMPRESS_LEVEL_MEDIUM:I = 0x1

.field public static final VMPRESS_LEVEL_SUPER_CRITICAL:I = 0x3


# virtual methods
.method public abstract interruptProcCompaction(I)V
.end method

.method public abstract interruptProcsCompaction()V
.end method

.method public abstract isCompactNeeded(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)Z
.end method

.method public abstract performCompaction(Ljava/lang/String;I)V
.end method

.method public abstract runGlobalCompaction(I)V
.end method

.method public abstract runProcCompaction(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)V
.end method

.method public abstract runProcsCompaction(I)V
.end method

.method public abstract setAppStartingMode(Z)V
.end method

.method public abstract writeLmkd(Z)V
.end method
