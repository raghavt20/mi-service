public class com.android.server.am.MemoryControlJobService extends android.app.job.JobService {
	 /* .source "MemoryControlJobService.java" */
	 /* # static fields */
	 private static final Integer DETECTION_JOB_ID;
	 private static final Integer KILL_JOB_ID;
	 private static Long PERIODIC_DETECTION_TIME;
	 private static final java.lang.String TAG;
	 private static com.android.server.am.MemoryStandardProcessControl mspc;
	 private static android.content.Context sContext;
	 /* # instance fields */
	 private final Integer SCREEN_STATE_OFF;
	 public java.lang.Runnable mFinishCallback;
	 private android.app.job.JobParameters mJobParams;
	 private Boolean mStarted;
	 /* # direct methods */
	 static android.app.job.JobParameters -$$Nest$fgetmJobParams ( com.android.server.am.MemoryControlJobService p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.mJobParams;
	 } // .end method
	 static Boolean -$$Nest$fgetmStarted ( com.android.server.am.MemoryControlJobService p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* iget-boolean p0, p0, Lcom/android/server/am/MemoryControlJobService;->mStarted:Z */
	 } // .end method
	 static void -$$Nest$fputmStarted ( com.android.server.am.MemoryControlJobService p0, Boolean p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* iput-boolean p1, p0, Lcom/android/server/am/MemoryControlJobService;->mStarted:Z */
		 return;
	 } // .end method
	 static android.content.Context -$$Nest$sfgetsContext ( ) { //bridge//synthethic
		 /* .locals 1 */
		 v0 = com.android.server.am.MemoryControlJobService.sContext;
	 } // .end method
	 static com.android.server.am.MemoryControlJobService ( ) {
		 /* .locals 2 */
		 /* .line 22 */
		 /* const-wide/32 v0, 0x124f80 */
		 /* sput-wide v0, Lcom/android/server/am/MemoryControlJobService;->PERIODIC_DETECTION_TIME:J */
		 /* .line 23 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* .line 24 */
		 return;
	 } // .end method
	 public com.android.server.am.MemoryControlJobService ( ) {
		 /* .locals 1 */
		 /* .line 18 */
		 /* invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V */
		 /* .line 25 */
		 int v0 = 2; // const/4 v0, 0x2
		 /* iput v0, p0, Lcom/android/server/am/MemoryControlJobService;->SCREEN_STATE_OFF:I */
		 /* .line 29 */
		 /* new-instance v0, Lcom/android/server/am/MemoryControlJobService$1; */
		 /* invoke-direct {v0, p0}, Lcom/android/server/am/MemoryControlJobService$1;-><init>(Lcom/android/server/am/MemoryControlJobService;)V */
		 this.mFinishCallback = v0;
		 return;
	 } // .end method
	 public static void detectionSchedule ( android.content.Context p0 ) {
		 /* .locals 5 */
		 /* .param p0, "context" # Landroid/content/Context; */
		 /* .line 46 */
		 v0 = com.android.server.am.MemoryControlJobService.sContext;
		 /* if-nez v0, :cond_0 */
		 /* .line 47 */
		 /* .line 49 */
	 } // :cond_0
	 final String v0 = "MemoryStandardProcessControl"; // const-string v0, "MemoryStandardProcessControl"
	 final String v1 = "detectionSchedule is called"; // const-string v1, "detectionSchedule is called"
	 android.util.Slog .d ( v0,v1 );
	 /* .line 50 */
	 /* new-instance v0, Landroid/content/ComponentName; */
	 /* const-class v1, Lcom/android/server/am/MemoryControlJobService; */
	 (( java.lang.Class ) v1 ).getName ( ); // invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;
	 final String v2 = "android"; // const-string v2, "android"
	 /* invoke-direct {v0, v2, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
	 /* .line 51 */
	 /* .local v0, "serviceName":Landroid/content/ComponentName; */
	 final String v1 = "jobscheduler"; // const-string v1, "jobscheduler"
	 (( android.content.Context ) p0 ).getSystemService ( v1 ); // invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
	 /* check-cast v1, Landroid/app/job/JobScheduler; */
	 /* .line 52 */
	 /* .local v1, "js":Landroid/app/job/JobScheduler; */
	 /* new-instance v2, Landroid/app/job/JobInfo$Builder; */
	 /* const v3, 0x13c05 */
	 /* invoke-direct {v2, v3, v0}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V */
	 /* .line 53 */
	 /* .local v2, "detectionBuilder":Landroid/app/job/JobInfo$Builder; */
	 int v3 = 1; // const/4 v3, 0x1
	 (( android.app.job.JobInfo$Builder ) v2 ).setRequiresBatteryNotLow ( v3 ); // invoke-virtual {v2, v3}, Landroid/app/job/JobInfo$Builder;->setRequiresBatteryNotLow(Z)Landroid/app/job/JobInfo$Builder;
	 /* .line 54 */
	 /* sget-wide v3, Lcom/android/server/am/MemoryControlJobService;->PERIODIC_DETECTION_TIME:J */
	 (( android.app.job.JobInfo$Builder ) v2 ).setPeriodic ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/app/job/JobInfo$Builder;->setPeriodic(J)Landroid/app/job/JobInfo$Builder;
	 /* .line 55 */
	 (( android.app.job.JobInfo$Builder ) v2 ).build ( ); // invoke-virtual {v2}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;
	 (( android.app.job.JobScheduler ) v1 ).schedule ( v3 ); // invoke-virtual {v1, v3}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I
	 /* .line 56 */
	 return;
} // .end method
public static void killSchedule ( android.content.Context p0 ) {
	 /* .locals 9 */
	 /* .param p0, "context" # Landroid/content/Context; */
	 /* .line 59 */
	 v0 = com.android.server.am.MemoryControlJobService.sContext;
	 /* if-nez v0, :cond_0 */
	 /* .line 60 */
	 /* .line 62 */
} // :cond_0
final String v0 = "MemoryStandardProcessControl"; // const-string v0, "MemoryStandardProcessControl"
final String v1 = "killSchedule is called"; // const-string v1, "killSchedule is called"
android.util.Slog .d ( v0,v1 );
/* .line 63 */
int v0 = 3; // const/4 v0, 0x3
int v1 = 1; // const/4 v1, 0x1
com.android.server.am.MemoryControlJobService .offsetFromTodayMidnight ( v1,v0 );
(( java.util.Calendar ) v0 ).getTimeInMillis ( ); // invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J
/* move-result-wide v2 */
/* .line 64 */
/* .local v2, "tomorrow3AM":J */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v4 */
/* sub-long v4, v2, v4 */
/* .line 65 */
/* .local v4, "nextScheduleTime":J */
/* new-instance v0, Landroid/content/ComponentName; */
/* const-class v6, Lcom/android/server/am/MemoryControlJobService; */
(( java.lang.Class ) v6 ).getName ( ); // invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;
final String v7 = "android"; // const-string v7, "android"
/* invoke-direct {v0, v7, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 66 */
/* .local v0, "serviceName":Landroid/content/ComponentName; */
final String v6 = "jobscheduler"; // const-string v6, "jobscheduler"
(( android.content.Context ) p0 ).getSystemService ( v6 ); // invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v6, Landroid/app/job/JobScheduler; */
/* .line 67 */
/* .local v6, "js":Landroid/app/job/JobScheduler; */
/* new-instance v7, Landroid/app/job/JobInfo$Builder; */
/* const v8, 0x13c06 */
/* invoke-direct {v7, v8, v0}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V */
/* .line 68 */
/* .local v7, "killBuilder":Landroid/app/job/JobInfo$Builder; */
(( android.app.job.JobInfo$Builder ) v7 ).setRequiresBatteryNotLow ( v1 ); // invoke-virtual {v7, v1}, Landroid/app/job/JobInfo$Builder;->setRequiresBatteryNotLow(Z)Landroid/app/job/JobInfo$Builder;
/* .line 69 */
(( android.app.job.JobInfo$Builder ) v7 ).setMinimumLatency ( v4, v5 ); // invoke-virtual {v7, v4, v5}, Landroid/app/job/JobInfo$Builder;->setMinimumLatency(J)Landroid/app/job/JobInfo$Builder;
/* .line 70 */
(( android.app.job.JobInfo$Builder ) v7 ).build ( ); // invoke-virtual {v7}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;
(( android.app.job.JobScheduler ) v6 ).schedule ( v1 ); // invoke-virtual {v6, v1}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I
/* .line 71 */
return;
} // .end method
private static java.util.Calendar offsetFromTodayMidnight ( Integer p0, Integer p1 ) {
/* .locals 3 */
/* .param p0, "nDays" # I */
/* .param p1, "nHours" # I */
/* .line 118 */
java.util.Calendar .getInstance ( );
/* .line 119 */
/* .local v0, "calendar":Ljava/util/Calendar; */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v1 */
(( java.util.Calendar ) v0 ).setTimeInMillis ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V
/* .line 120 */
/* const/16 v1, 0xb */
(( java.util.Calendar ) v0 ).set ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Ljava/util/Calendar;->set(II)V
/* .line 121 */
/* const/16 v1, 0xc */
int v2 = 0; // const/4 v2, 0x0
(( java.util.Calendar ) v0 ).set ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V
/* .line 122 */
/* const/16 v1, 0xd */
(( java.util.Calendar ) v0 ).set ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V
/* .line 123 */
/* const/16 v1, 0xe */
(( java.util.Calendar ) v0 ).set ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V
/* .line 124 */
int v1 = 5; // const/4 v1, 0x5
(( java.util.Calendar ) v0 ).add ( v1, p0 ); // invoke-virtual {v0, v1, p0}, Ljava/util/Calendar;->add(II)V
/* .line 125 */
} // .end method
/* # virtual methods */
public Boolean onStartJob ( android.app.job.JobParameters p0 ) {
/* .locals 6 */
/* .param p1, "params" # Landroid/app/job/JobParameters; */
/* .line 75 */
v0 = com.android.server.am.MemoryControlJobService.mspc;
/* if-nez v0, :cond_0 */
/* .line 76 */
com.android.server.am.MemoryStandardProcessControlStub .getInstance ( );
/* check-cast v0, Lcom/android/server/am/MemoryStandardProcessControl; */
/* .line 79 */
} // :cond_0
v0 = com.android.server.am.MemoryControlJobService.mspc;
/* iget v0, v0, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenState:I */
/* .line 80 */
/* .local v0, "screenState":I */
v1 = com.android.server.am.MemoryControlJobService.mspc;
v1 = (( com.android.server.am.MemoryStandardProcessControl ) v1 ).isEnable ( ); // invoke-virtual {v1}, Lcom/android/server/am/MemoryStandardProcessControl;->isEnable()Z
/* .line 81 */
/* .local v1, "enable":Z */
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 82 */
v3 = (( android.app.job.JobParameters ) p1 ).getJobId ( ); // invoke-virtual {p1}, Landroid/app/job/JobParameters;->getJobId()I
/* const v4, 0x13c05 */
int v5 = 2; // const/4 v5, 0x2
/* if-ne v3, v4, :cond_2 */
/* .line 83 */
/* if-eq v0, v5, :cond_1 */
/* .line 85 */
v3 = com.android.server.am.MemoryControlJobService.mspc;
(( com.android.server.am.MemoryStandardProcessControl ) v3 ).callPeriodicDetection ( ); // invoke-virtual {v3}, Lcom/android/server/am/MemoryStandardProcessControl;->callPeriodicDetection()V
/* .line 87 */
} // :cond_1
/* .line 88 */
} // :cond_2
v3 = (( android.app.job.JobParameters ) p1 ).getJobId ( ); // invoke-virtual {p1}, Landroid/app/job/JobParameters;->getJobId()I
/* const v4, 0x13c06 */
/* if-ne v3, v4, :cond_4 */
/* .line 89 */
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v5, :cond_3 */
/* .line 91 */
v3 = com.android.server.am.MemoryControlJobService.mspc;
(( com.android.server.am.MemoryStandardProcessControl ) v3 ).callKillProcess ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/server/am/MemoryStandardProcessControl;->callKillProcess(Z)V
/* .line 93 */
} // :cond_3
this.mJobParams = p1;
/* .line 94 */
v3 = this.mFinishCallback;
/* monitor-enter v3 */
/* .line 95 */
try { // :try_start_0
/* iput-boolean v2, p0, Lcom/android/server/am/MemoryControlJobService;->mStarted:Z */
/* .line 96 */
/* monitor-exit v3 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 97 */
v3 = com.android.server.am.MemoryControlJobService.mspc;
v4 = this.mFinishCallback;
(( com.android.server.am.MemoryStandardProcessControl ) v3 ).callRunnable ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/am/MemoryStandardProcessControl;->callRunnable(Ljava/lang/Runnable;)V
/* .line 98 */
/* .line 96 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
/* .line 101 */
} // :cond_4
} // .end method
public Boolean onStopJob ( android.app.job.JobParameters p0 ) {
/* .locals 4 */
/* .param p1, "params" # Landroid/app/job/JobParameters; */
/* .line 106 */
v0 = (( android.app.job.JobParameters ) p1 ).getJobId ( ); // invoke-virtual {p1}, Landroid/app/job/JobParameters;->getJobId()I
/* .line 107 */
/* .local v0, "jobId":I */
/* const v1, 0x13c06 */
int v2 = 0; // const/4 v2, 0x0
/* if-ne v0, v1, :cond_0 */
/* .line 108 */
/* iput-boolean v2, p0, Lcom/android/server/am/MemoryControlJobService;->mStarted:Z */
/* .line 109 */
v1 = com.android.server.am.MemoryControlJobService.mspc;
v3 = this.mFinishCallback;
(( com.android.server.am.MemoryStandardProcessControl ) v1 ).callRunnable ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/am/MemoryStandardProcessControl;->callRunnable(Ljava/lang/Runnable;)V
/* .line 111 */
} // :cond_0
/* sget-boolean v1, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 112 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "onStopJob, jobId: "; // const-string v3, "onStopJob, jobId: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MemoryStandardProcessControl"; // const-string v3, "MemoryStandardProcessControl"
android.util.Slog .d ( v3,v1 );
/* .line 114 */
} // :cond_1
} // .end method
