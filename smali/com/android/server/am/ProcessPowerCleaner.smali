.class public Lcom/android/server/am/ProcessPowerCleaner;
.super Lcom/android/server/am/ProcessCleanerBase;
.source "ProcessPowerCleaner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/ProcessPowerCleaner$ScreenStatusReceiver;,
        Lcom/android/server/am/ProcessPowerCleaner$H;
    }
.end annotation


# static fields
.field private static final ACTIVE_PROCESS_MIX_THRESHOLD:F

.field private static final ACTIVE_PROCESS_THRESHOLD_RATE:F = 0.75f

.field private static final BACKGROUND_PROCESS_SINGLE_CPU_RATIO:D = 25.0

.field private static final CHECK_CPU_GROWTH_RATE:I = 0x2

.field private static final CHECK_CPU_MAX_TIME_MS:I = 0x36ee80

.field private static final DESK_CLOCK_PROCESS_NAME:Ljava/lang/String; = "com.android.deskclock"

.field private static final SCREEN_OFF_DELAYED_TIME:I = 0x493e0

.field private static final SCREEN_OFF_FROZEN_DELAYED_TIME:I = 0x2bf20

.field private static final SCREEN_OFF_FROZEN_REASON:Ljava/lang/String; = "lock off frozen"

.field private static final SCREEN_OFF_START_CPU_CHECK_TIME:I = 0x16e360

.field private static final TAG:Ljava/lang/String; = "ProcessPowerCleaner"

.field private static final THERMAL_KILL_ALL_PROCESS_MINADJ:I = 0xc8


# instance fields
.field private mAMS:Lcom/android/server/am/ActivityManagerService;

.field private mActiveProcessThreshold:F

.field private mCheckCPUTime:I

.field private mContext:Landroid/content/Context;

.field private mHandler:Lcom/android/server/am/ProcessPowerCleaner$H;

.field private mIsScreenOffState:Z

.field private mLockOffCleanTestEnable:Z

.field private mLockOffCleanTestThreshold:F

.field private mPMS:Lcom/android/server/am/ProcessManagerService;

.field private mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

.field private mScreenStatusReceiver:Lcom/android/server/am/ProcessPowerCleaner$ScreenStatusReceiver;

.field private mSysPressureCtrl:Lcom/android/server/am/SystemPressureController;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/am/ProcessPowerCleaner;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/ProcessPowerCleaner;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/am/ProcessPowerCleaner;)Lcom/android/server/am/ProcessPowerCleaner$H;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/ProcessPowerCleaner;->mHandler:Lcom/android/server/am/ProcessPowerCleaner$H;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmIsScreenOffState(Lcom/android/server/am/ProcessPowerCleaner;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/am/ProcessPowerCleaner;->mIsScreenOffState:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleKillAll(Lcom/android/server/am/ProcessPowerCleaner;Lmiui/process/ProcessConfig;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/am/ProcessPowerCleaner;->handleKillAll(Lmiui/process/ProcessConfig;Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleScreenOffEvent(Lcom/android/server/am/ProcessPowerCleaner;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/ProcessPowerCleaner;->handleScreenOffEvent()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleThermalKillProc(Lcom/android/server/am/ProcessPowerCleaner;Lmiui/process/ProcessConfig;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessPowerCleaner;->handleThermalKillProc(Lmiui/process/ProcessConfig;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mpowerFrozenAll(Lcom/android/server/am/ProcessPowerCleaner;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/ProcessPowerCleaner;->powerFrozenAll()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mresetLockOffConfig(Lcom/android/server/am/ProcessPowerCleaner;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/ProcessPowerCleaner;->resetLockOffConfig()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateCloudControlParas(Lcom/android/server/am/ProcessPowerCleaner;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessPowerCleaner;->updateCloudControlParas(Landroid/content/Context;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 41
    sget v0, Landroid/os/spc/PressureStateSettings;->SCREEN_OFF_PROCESS_ACTIVE_MIX_THRESHOLD:I

    int-to-float v0, v0

    const/high16 v1, 0x3f800000    # 1.0f

    div-float/2addr v0, v1

    sput v0, Lcom/android/server/am/ProcessPowerCleaner;->ACTIVE_PROCESS_MIX_THRESHOLD:F

    return-void
.end method

.method public constructor <init>(Lcom/android/server/am/ActivityManagerService;)V
    .locals 2
    .param p1, "ams"    # Lcom/android/server/am/ActivityManagerService;

    .line 66
    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessCleanerBase;-><init>(Lcom/android/server/am/ActivityManagerService;)V

    .line 50
    sget v0, Landroid/os/spc/PressureStateSettings;->SCREEN_OFF_PROCESS_ACTIVE_THRESHOLD:I

    int-to-float v0, v0

    const/high16 v1, 0x3f800000    # 1.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/server/am/ProcessPowerCleaner;->mActiveProcessThreshold:F

    .line 52
    const v0, 0x3a83126f    # 0.001f

    iput v0, p0, Lcom/android/server/am/ProcessPowerCleaner;->mLockOffCleanTestThreshold:F

    .line 53
    const v0, 0x927c0

    iput v0, p0, Lcom/android/server/am/ProcessPowerCleaner;->mCheckCPUTime:I

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/am/ProcessPowerCleaner;->mIsScreenOffState:Z

    .line 55
    iput-boolean v0, p0, Lcom/android/server/am/ProcessPowerCleaner;->mLockOffCleanTestEnable:Z

    .line 67
    iput-object p1, p0, Lcom/android/server/am/ProcessPowerCleaner;->mAMS:Lcom/android/server/am/ActivityManagerService;

    .line 68
    return-void
.end method

.method private cleanAllSubProcess()V
    .locals 1

    .line 362
    iget-object v0, p0, Lcom/android/server/am/ProcessPowerCleaner;->mSysPressureCtrl:Lcom/android/server/am/SystemPressureController;

    if-eqz v0, :cond_0

    .line 363
    invoke-virtual {v0}, Lcom/android/server/am/SystemPressureController;->cleanAllSubProcess()V

    .line 365
    :cond_0
    return-void
.end method

.method private handleKillAll(Lmiui/process/ProcessConfig;Z)V
    .locals 23
    .param p1, "config"    # Lmiui/process/ProcessConfig;
    .param p2, "isKillSystemProc"    # Z

    .line 170
    move-object/from16 v9, p0

    move-object/from16 v10, p1

    iget-object v0, v9, Lcom/android/server/am/ProcessPowerCleaner;->mPMS:Lcom/android/server/am/ProcessManagerService;

    if-nez v0, :cond_0

    .line 171
    return-void

    .line 173
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lmiui/process/ProcessConfig;->getPolicy()I

    move-result v11

    .line 174
    .local v11, "policy":I
    invoke-virtual/range {p1 .. p1}, Lmiui/process/ProcessConfig;->getPolicy()I

    move-result v0

    invoke-virtual {v9, v0}, Lcom/android/server/am/ProcessPowerCleaner;->getKillReason(I)Ljava/lang/String;

    move-result-object v12

    .line 176
    .local v12, "reason":Ljava/lang/String;
    iget-object v0, v9, Lcom/android/server/am/ProcessPowerCleaner;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mActivityTaskManager:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-static {v0}, Lcom/android/server/wm/WindowProcessUtils;->getPerceptibleRecentAppList(Lcom/android/server/wm/ActivityTaskManagerService;)Ljava/util/Map;

    move-result-object v13

    .line 178
    .local v13, "fgTaskPackageMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/String;>;"
    iget-object v0, v9, Lcom/android/server/am/ProcessPowerCleaner;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    invoke-virtual {v9, v10, v0}, Lcom/android/server/am/ProcessPowerCleaner;->getProcessPolicyWhiteList(Lmiui/process/ProcessConfig;Lcom/android/server/am/ProcessPolicy;)Ljava/util/List;

    move-result-object v14

    .line 179
    .local v14, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v13, :cond_1

    if-eqz v14, :cond_1

    .line 180
    invoke-interface {v13}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v14, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 182
    :cond_1
    iget-object v0, v9, Lcom/android/server/am/ProcessPowerCleaner;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    invoke-virtual {v9, v10, v0, v14, v13}, Lcom/android/server/am/ProcessPowerCleaner;->removeTasksIfNeeded(Lmiui/process/ProcessConfig;Lcom/android/server/am/ProcessPolicy;Ljava/util/List;Ljava/util/Map;)V

    .line 183
    iget-object v0, v9, Lcom/android/server/am/ProcessPowerCleaner;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-interface {v0}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->getAllAppState()Ljava/util/ArrayList;

    move-result-object v15

    .line 184
    .local v15, "appList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState;>;"
    if-eqz v15, :cond_a

    invoke-virtual {v15}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    goto/16 :goto_3

    .line 187
    :cond_2
    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_3
    :goto_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v17, v0

    check-cast v17, Lcom/miui/server/smartpower/IAppState;

    .line 188
    .local v17, "appState":Lcom/miui/server/smartpower/IAppState;
    invoke-virtual/range {v17 .. v17}, Lcom/miui/server/smartpower/IAppState;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v14, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    goto :goto_0

    .line 189
    :cond_4
    const/16 v0, 0x14

    if-ne v11, v0, :cond_5

    .line 190
    invoke-virtual/range {v17 .. v17}, Lcom/miui/server/smartpower/IAppState;->getAdj()I

    move-result v0

    if-lez v0, :cond_3

    .line 191
    invoke-virtual/range {v17 .. v17}, Lcom/miui/server/smartpower/IAppState;->isSystemApp()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 192
    goto :goto_0

    .line 194
    :cond_5
    invoke-virtual/range {v17 .. v17}, Lcom/miui/server/smartpower/IAppState;->getRunningProcessList()Ljava/util/ArrayList;

    move-result-object v18

    .line 195
    .local v18, "runningAppList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_1
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 196
    .local v8, "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;

    move-result-object v20

    .line 197
    .local v20, "app":Lcom/android/server/am/ProcessRecord;
    invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I

    move-result v0

    if-lez v0, :cond_7

    .line 198
    invoke-virtual {v9, v8}, Lcom/android/server/am/ProcessPowerCleaner;->isCurrentProcessInBackup(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 199
    const/4 v4, 0x1

    iget-object v5, v9, Lcom/android/server/am/ProcessPowerCleaner;->mPMS:Lcom/android/server/am/ProcessManagerService;

    const-string v6, "ProcessPowerCleaner"

    iget-object v7, v9, Lcom/android/server/am/ProcessPowerCleaner;->mHandler:Lcom/android/server/am/ProcessPowerCleaner$H;

    iget-object v3, v9, Lcom/android/server/am/ProcessPowerCleaner;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move v2, v11

    move-object/from16 v21, v3

    move-object v3, v12

    move-object/from16 v22, v8

    .end local v8    # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .local v22, "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    move-object/from16 v8, v21

    invoke-virtual/range {v0 .. v8}, Lcom/android/server/am/ProcessPowerCleaner;->killOnce(Lcom/android/server/am/ProcessRecord;ILjava/lang/String;ZLcom/android/server/am/ProcessManagerService;Ljava/lang/String;Landroid/os/Handler;Landroid/content/Context;)V

    goto :goto_2

    .line 198
    .end local v22    # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .restart local v8    # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    :cond_6
    move-object/from16 v22, v8

    .end local v8    # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .restart local v22    # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    goto :goto_2

    .line 197
    .end local v22    # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .restart local v8    # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    :cond_7
    move-object/from16 v22, v8

    .line 201
    .end local v8    # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .end local v20    # "app":Lcom/android/server/am/ProcessRecord;
    :goto_2
    goto :goto_1

    .line 202
    .end local v17    # "appState":Lcom/miui/server/smartpower/IAppState;
    .end local v18    # "runningAppList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
    :cond_8
    goto :goto_0

    .line 203
    :cond_9
    return-void

    .line 185
    :cond_a
    :goto_3
    return-void
.end method

.method private handleKillApp(Lmiui/process/ProcessConfig;)Z
    .locals 17
    .param p1, "config"    # Lmiui/process/ProcessConfig;

    .line 206
    move-object/from16 v9, p0

    invoke-virtual/range {p1 .. p1}, Lmiui/process/ProcessConfig;->isUidInvalid()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 207
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "uid:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual/range {p1 .. p1}, Lmiui/process/ProcessConfig;->getUserId()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is invalid"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "ProcessPowerCleaner"

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    return v1

    .line 210
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lmiui/process/ProcessConfig;->getKillingPackage()Ljava/lang/String;

    move-result-object v10

    .line 211
    .local v10, "packageName":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lmiui/process/ProcessConfig;->getUid()I

    move-result v11

    .line 212
    .local v11, "uid":I
    invoke-virtual/range {p1 .. p1}, Lmiui/process/ProcessConfig;->getPolicy()I

    move-result v12

    .line 214
    .local v12, "policy":I
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {v11}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_1

    .line 218
    :cond_1
    iget-object v0, v9, Lcom/android/server/am/ProcessPowerCleaner;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    .line 219
    invoke-interface {v0, v11, v10}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->getLruProcesses(ILjava/lang/String;)Ljava/util/ArrayList;

    move-result-object v13

    .line 220
    .local v13, "runningAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v15, v0

    check-cast v15, Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 221
    .local v15, "runningProcess":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    invoke-virtual {v15}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;

    move-result-object v16

    .line 222
    .local v16, "app":Lcom/android/server/am/ProcessRecord;
    invoke-virtual {v9, v15}, Lcom/android/server/am/ProcessPowerCleaner;->isCurrentProcessInBackup(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 223
    invoke-virtual {v9, v12}, Lcom/android/server/am/ProcessPowerCleaner;->getKillReason(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    iget-object v5, v9, Lcom/android/server/am/ProcessPowerCleaner;->mPMS:Lcom/android/server/am/ProcessManagerService;

    const-string v6, "ProcessPowerCleaner"

    iget-object v7, v9, Lcom/android/server/am/ProcessPowerCleaner;->mHandler:Lcom/android/server/am/ProcessPowerCleaner$H;

    iget-object v8, v9, Lcom/android/server/am/ProcessPowerCleaner;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move v2, v12

    invoke-virtual/range {v0 .. v8}, Lcom/android/server/am/ProcessPowerCleaner;->killOnce(Lcom/android/server/am/ProcessRecord;ILjava/lang/String;ZLcom/android/server/am/ProcessManagerService;Ljava/lang/String;Landroid/os/Handler;Landroid/content/Context;)V

    .line 225
    .end local v15    # "runningProcess":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .end local v16    # "app":Lcom/android/server/am/ProcessRecord;
    :cond_2
    goto :goto_0

    .line 226
    :cond_3
    const/4 v0, 0x1

    return v0

    .line 215
    .end local v13    # "runningAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
    :cond_4
    :goto_1
    return v1
.end method

.method private handleScreenOffEvent()V
    .locals 4

    .line 317
    iget-object v0, p0, Lcom/android/server/am/ProcessPowerCleaner;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->updateCpuStatsNow(Z)J

    .line 318
    invoke-direct {p0}, Lcom/android/server/am/ProcessPowerCleaner;->cleanAllSubProcess()V

    .line 319
    invoke-direct {p0}, Lcom/android/server/am/ProcessPowerCleaner;->powerFrozenAll()V

    .line 321
    iget-object v0, p0, Lcom/android/server/am/ProcessPowerCleaner;->mHandler:Lcom/android/server/am/ProcessPowerCleaner$H;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessPowerCleaner$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 322
    .local v0, "message":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/am/ProcessPowerCleaner;->mHandler:Lcom/android/server/am/ProcessPowerCleaner$H;

    const-wide/32 v2, 0x16e360

    invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/am/ProcessPowerCleaner$H;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 323
    return-void
.end method

.method private handleThermalKillProc(Lmiui/process/ProcessConfig;)V
    .locals 16
    .param p1, "config"    # Lmiui/process/ProcessConfig;

    .line 230
    move-object/from16 v9, p0

    iget-object v0, v9, Lcom/android/server/am/ProcessPowerCleaner;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-interface {v0}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->getAllAppState()Ljava/util/ArrayList;

    move-result-object v10

    .line 231
    .local v10, "appList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState;>;"
    if-eqz v10, :cond_6

    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_2

    .line 235
    :cond_0
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_1
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/miui/server/smartpower/IAppState;

    .line 236
    .local v12, "appState":Lcom/miui/server/smartpower/IAppState;
    invoke-virtual {v12}, Lcom/miui/server/smartpower/IAppState;->isSystemApp()Z

    move-result v0

    if-nez v0, :cond_1

    .line 237
    invoke-virtual {v12}, Lcom/miui/server/smartpower/IAppState;->getAdj()I

    move-result v0

    const/16 v1, 0xc8

    if-gt v0, v1, :cond_2

    .line 238
    goto :goto_0

    .line 240
    :cond_2
    invoke-virtual {v12}, Lcom/miui/server/smartpower/IAppState;->getRunningProcessList()Ljava/util/ArrayList;

    move-result-object v13

    .line 241
    .local v13, "runningAppList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v15, v0

    check-cast v15, Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 242
    .local v15, "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    invoke-virtual {v9, v15}, Lcom/android/server/am/ProcessPowerCleaner;->isCurrentProcessInBackup(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 243
    invoke-virtual {v15}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lmiui/process/ProcessConfig;->getPolicy()I

    move-result v2

    .line 244
    invoke-virtual/range {p1 .. p1}, Lmiui/process/ProcessConfig;->getPolicy()I

    move-result v0

    invoke-virtual {v9, v0}, Lcom/android/server/am/ProcessPowerCleaner;->getKillReason(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    iget-object v5, v9, Lcom/android/server/am/ProcessPowerCleaner;->mPMS:Lcom/android/server/am/ProcessManagerService;

    const-string v6, "ProcessPowerCleaner"

    iget-object v7, v9, Lcom/android/server/am/ProcessPowerCleaner;->mHandler:Lcom/android/server/am/ProcessPowerCleaner$H;

    iget-object v8, v9, Lcom/android/server/am/ProcessPowerCleaner;->mContext:Landroid/content/Context;

    .line 243
    move-object/from16 v0, p0

    invoke-virtual/range {v0 .. v8}, Lcom/android/server/am/ProcessPowerCleaner;->killOnce(Lcom/android/server/am/ProcessRecord;ILjava/lang/String;ZLcom/android/server/am/ProcessManagerService;Ljava/lang/String;Landroid/os/Handler;Landroid/content/Context;)V

    .line 246
    .end local v15    # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    :cond_3
    goto :goto_1

    .line 247
    .end local v12    # "appState":Lcom/miui/server/smartpower/IAppState;
    .end local v13    # "runningAppList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
    :cond_4
    goto :goto_0

    .line 248
    :cond_5
    return-void

    .line 232
    :cond_6
    :goto_2
    return-void
.end method

.method private killActiveProcess(FILjava/util/List;)V
    .locals 28
    .param p1, "threshold"    # F
    .param p2, "policy"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FI",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 276
    .local p3, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v10, p0

    move/from16 v11, p1

    move/from16 v12, p2

    iget-object v0, v10, Lcom/android/server/am/ProcessPowerCleaner;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    const/4 v13, 0x0

    invoke-interface {v0, v13}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->updateCpuStatsNow(Z)J

    move-result-wide v14

    .line 277
    .local v14, "uptimeSince":J
    const-wide/16 v0, 0x0

    cmp-long v0, v14, v0

    if-gtz v0, :cond_0

    .line 278
    return-void

    .line 280
    :cond_0
    iget-object v0, v10, Lcom/android/server/am/ProcessPowerCleaner;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-interface {v0}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->getAllAppState()Ljava/util/ArrayList;

    move-result-object v16

    .line 281
    .local v16, "appStates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState;>;"
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/miui/server/smartpower/IAppState;

    .line 282
    .local v17, "appInfo":Lcom/miui/server/smartpower/IAppState;
    invoke-virtual/range {v17 .. v17}, Lcom/miui/server/smartpower/IAppState;->getRunningProcessList()Ljava/util/ArrayList;

    move-result-object v18

    .line 283
    .local v18, "procs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
    invoke-virtual/range {v17 .. v17}, Lcom/miui/server/smartpower/IAppState;->getAdj()I

    move-result v1

    const/16 v9, 0xc8

    if-le v1, v9, :cond_1

    const/4 v1, 0x1

    move v5, v1

    goto :goto_1

    :cond_1
    move v5, v13

    .line 284
    .local v5, "isForceStop":Z
    :goto_1
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_2
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 285
    .local v8, "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getCurCpuTime()J

    move-result-wide v6

    .line 286
    .local v6, "curCpuTime":J
    invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getLastCpuTime()J

    move-result-wide v20

    .line 287
    .local v20, "lastCpuTime":J
    invoke-virtual {v8, v6, v7}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->setLastCpuTime(J)V

    .line 288
    iget-object v1, v10, Lcom/android/server/am/ProcessPowerCleaner;->mAMS:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v1

    .line 289
    nop

    .line 290
    :try_start_0
    invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;

    move-result-object v2

    invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUid()I

    move-result v3

    iget-object v4, v10, Lcom/android/server/am/ProcessPowerCleaner;->mPMS:Lcom/android/server/am/ProcessManagerService;

    .line 289
    invoke-virtual {v10, v2, v3, v12, v4}, Lcom/android/server/am/ProcessPowerCleaner;->isInWhiteListLock(Lcom/android/server/am/ProcessRecord;IILcom/android/server/am/ProcessManagerService;)Z

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v2, :cond_2

    .line 291
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 293
    :catchall_0
    move-exception v0

    move-wide/from16 v26, v6

    move-object v10, v8

    goto/16 :goto_4

    :cond_2
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 294
    invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUid()I

    move-result v1

    invoke-virtual {v10, v1}, Lcom/android/server/am/ProcessPowerCleaner;->isAudioOrGPSApp(I)Z

    move-result v1

    if-nez v1, :cond_8

    .line 295
    invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v4, p3

    invoke-interface {v4, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 296
    invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I

    move-result v1

    if-lt v1, v9, :cond_6

    .line 297
    invoke-virtual {v10, v8}, Lcom/android/server/am/ProcessPowerCleaner;->isCurrentProcessInBackup(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 298
    invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->hasForegrundService()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 299
    goto :goto_2

    .line 301
    :cond_3
    sub-long v1, v6, v20

    iget-object v3, v10, Lcom/android/server/am/ProcessPowerCleaner;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    .line 302
    invoke-interface {v3}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->getBackgroundCpuCoreNum()I

    move-result v3

    int-to-long v9, v3

    div-long v9, v1, v9

    .line 303
    .local v9, "cpuUsedTime":J
    long-to-float v1, v9

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v1, v2

    long-to-float v3, v14

    div-float/2addr v1, v3

    cmpl-float v1, v1, v11

    if-ltz v1, :cond_4

    .line 304
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v3, p0

    invoke-virtual {v3, v12}, Lcom/android/server/am/ProcessPowerCleaner;->getKillReason(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v13, " over "

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 305
    invoke-static {v14, v15}, Landroid/util/TimeUtils;->formatDuration(J)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v13, " used "

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 306
    invoke-static {v9, v10}, Landroid/util/TimeUtils;->formatDuration(J)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v13, " ("

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    long-to-float v13, v9

    mul-float/2addr v13, v2

    long-to-float v2, v14

    div-float/2addr v13, v2

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%) threshold "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 309
    .local v13, "reason":Ljava/lang/String;
    invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;

    move-result-object v2

    iget-object v1, v3, Lcom/android/server/am/ProcessPowerCleaner;->mPMS:Lcom/android/server/am/ProcessManagerService;

    const-string v22, "ProcessPowerCleaner"

    move-object/from16 v23, v0

    iget-object v0, v3, Lcom/android/server/am/ProcessPowerCleaner;->mHandler:Lcom/android/server/am/ProcessPowerCleaner$H;

    move-wide/from16 v24, v9

    .end local v9    # "cpuUsedTime":J
    .local v24, "cpuUsedTime":J
    iget-object v9, v3, Lcom/android/server/am/ProcessPowerCleaner;->mContext:Landroid/content/Context;

    move-object v10, v1

    move-object/from16 v1, p0

    move/from16 v3, p2

    move-object v4, v13

    move-wide/from16 v26, v6

    .end local v6    # "curCpuTime":J
    .local v26, "curCpuTime":J
    move-object v6, v10

    move-object/from16 v7, v22

    move-object v10, v8

    .end local v8    # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .local v10, "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    move-object v8, v0

    const/16 v0, 0xc8

    invoke-virtual/range {v1 .. v9}, Lcom/android/server/am/ProcessPowerCleaner;->killOnce(Lcom/android/server/am/ProcessRecord;ILjava/lang/String;ZLcom/android/server/am/ProcessManagerService;Ljava/lang/String;Landroid/os/Handler;Landroid/content/Context;)V

    goto :goto_3

    .line 303
    .end local v10    # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .end local v13    # "reason":Ljava/lang/String;
    .end local v24    # "cpuUsedTime":J
    .end local v26    # "curCpuTime":J
    .restart local v6    # "curCpuTime":J
    .restart local v8    # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .restart local v9    # "cpuUsedTime":J
    :cond_4
    move-object/from16 v23, v0

    move-wide/from16 v26, v6

    move-wide/from16 v24, v9

    const/16 v0, 0xc8

    move-object v10, v8

    .line 312
    .end local v6    # "curCpuTime":J
    .end local v8    # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .end local v9    # "cpuUsedTime":J
    .end local v20    # "lastCpuTime":J
    :goto_3
    const/4 v13, 0x0

    move-object/from16 v10, p0

    move v9, v0

    move-object/from16 v0, v23

    goto/16 :goto_2

    .line 297
    .restart local v6    # "curCpuTime":J
    .restart local v8    # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .restart local v20    # "lastCpuTime":J
    :cond_5
    move-object/from16 v23, v0

    move-wide/from16 v26, v6

    move-object v10, v8

    move v0, v9

    .end local v6    # "curCpuTime":J
    .end local v8    # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .restart local v10    # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .restart local v26    # "curCpuTime":J
    const/4 v13, 0x0

    move-object/from16 v10, p0

    move-object/from16 v0, v23

    goto/16 :goto_2

    .line 296
    .end local v10    # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .end local v26    # "curCpuTime":J
    .restart local v6    # "curCpuTime":J
    .restart local v8    # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    :cond_6
    move-object/from16 v23, v0

    move-wide/from16 v26, v6

    move-object v10, v8

    move v0, v9

    .end local v6    # "curCpuTime":J
    .end local v8    # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .restart local v10    # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .restart local v26    # "curCpuTime":J
    const/4 v13, 0x0

    move-object/from16 v10, p0

    move-object/from16 v0, v23

    goto/16 :goto_2

    .line 295
    .end local v10    # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .end local v26    # "curCpuTime":J
    .restart local v6    # "curCpuTime":J
    .restart local v8    # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    :cond_7
    move-object/from16 v23, v0

    move-wide/from16 v26, v6

    move-object v10, v8

    move v0, v9

    .end local v6    # "curCpuTime":J
    .end local v8    # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .restart local v10    # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .restart local v26    # "curCpuTime":J
    const/4 v13, 0x0

    move-object/from16 v10, p0

    move-object/from16 v0, v23

    goto/16 :goto_2

    .line 294
    .end local v10    # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .end local v26    # "curCpuTime":J
    .restart local v6    # "curCpuTime":J
    .restart local v8    # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    :cond_8
    move-object/from16 v23, v0

    move-wide/from16 v26, v6

    move-object v10, v8

    move v0, v9

    .end local v6    # "curCpuTime":J
    .end local v8    # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .restart local v10    # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .restart local v26    # "curCpuTime":J
    const/4 v13, 0x0

    move-object/from16 v10, p0

    move-object/from16 v0, v23

    goto/16 :goto_2

    .line 293
    .end local v10    # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .end local v26    # "curCpuTime":J
    .restart local v6    # "curCpuTime":J
    .restart local v8    # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    :catchall_1
    move-exception v0

    move-wide/from16 v26, v6

    move-object v10, v8

    .end local v6    # "curCpuTime":J
    .end local v8    # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .restart local v10    # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .restart local v26    # "curCpuTime":J
    :goto_4
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    throw v0

    :catchall_2
    move-exception v0

    goto :goto_4

    .line 284
    .end local v10    # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .end local v20    # "lastCpuTime":J
    .end local v26    # "curCpuTime":J
    :cond_9
    move-object/from16 v23, v0

    .line 313
    .end local v5    # "isForceStop":Z
    .end local v17    # "appInfo":Lcom/miui/server/smartpower/IAppState;
    .end local v18    # "procs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
    const/4 v13, 0x0

    move-object/from16 v10, p0

    goto/16 :goto_0

    .line 314
    :cond_a
    return-void
.end method

.method private powerFrozenAll()V
    .locals 4

    .line 326
    sget-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_FROZEN_ENABLE:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/am/ProcessPowerCleaner;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    if-eqz v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/android/server/am/ProcessPowerCleaner;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    const-string v1, "lock off frozen"

    invoke-interface {v0, v1}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->hibernateAllIfNeeded(Ljava/lang/String;)V

    .line 328
    const-string v0, "ProcessPowerCleaner"

    const-string v1, "Frozen: lock off frozen"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    iget-object v0, p0, Lcom/android/server/am/ProcessPowerCleaner;->mHandler:Lcom/android/server/am/ProcessPowerCleaner$H;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessPowerCleaner$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 330
    .local v0, "message":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/am/ProcessPowerCleaner;->mHandler:Lcom/android/server/am/ProcessPowerCleaner$H;

    const-wide/32 v2, 0x2bf20

    invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/am/ProcessPowerCleaner$H;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 332
    .end local v0    # "message":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method private registerCloudObserver(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 345
    new-instance v0, Lcom/android/server/am/ProcessPowerCleaner$1;

    iget-object v1, p0, Lcom/android/server/am/ProcessPowerCleaner;->mHandler:Lcom/android/server/am/ProcessPowerCleaner$H;

    invoke-direct {v0, p0, v1}, Lcom/android/server/am/ProcessPowerCleaner$1;-><init>(Lcom/android/server/am/ProcessPowerCleaner;Landroid/os/Handler;)V

    .line 354
    .local v0, "observer":Landroid/database/ContentObserver;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "perf_proc_power"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 356
    return-void
.end method

.method private resetLockOffConfig()V
    .locals 2

    .line 368
    iget-object v0, p0, Lcom/android/server/am/ProcessPowerCleaner;->mHandler:Lcom/android/server/am/ProcessPowerCleaner$H;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessPowerCleaner$H;->removeMessages(I)V

    .line 369
    iget-object v0, p0, Lcom/android/server/am/ProcessPowerCleaner;->mHandler:Lcom/android/server/am/ProcessPowerCleaner$H;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessPowerCleaner$H;->removeMessages(I)V

    .line 370
    iget-object v0, p0, Lcom/android/server/am/ProcessPowerCleaner;->mHandler:Lcom/android/server/am/ProcessPowerCleaner$H;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessPowerCleaner$H;->removeMessages(I)V

    .line 371
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lcom/android/server/am/ProcessPowerCleaner;->mActiveProcessThreshold:F

    .line 372
    const v0, 0x927c0

    iput v0, p0, Lcom/android/server/am/ProcessPowerCleaner;->mCheckCPUTime:I

    .line 373
    return-void
.end method

.method private updateCloudControlParas(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .line 359
    return-void
.end method


# virtual methods
.method createMessage(ILmiui/process/ProcessConfig;Landroid/os/Handler;)Landroid/os/Message;
    .locals 1
    .param p1, "event"    # I
    .param p2, "config"    # Lmiui/process/ProcessConfig;
    .param p3, "handler"    # Landroid/os/Handler;

    .line 335
    invoke-virtual {p3, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 336
    .local v0, "msg":Landroid/os/Message;
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 337
    return-object v0
.end method

.method public handleAutoLockOff()V
    .locals 6

    .line 251
    iget-object v0, p0, Lcom/android/server/am/ProcessPowerCleaner;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mActivityTaskManager:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-static {v0}, Lcom/android/server/wm/WindowProcessUtils;->getPerceptibleRecentAppList(Lcom/android/server/wm/ActivityTaskManagerService;)Ljava/util/Map;

    move-result-object v0

    .line 253
    .local v0, "fgTaskPackageMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/android/server/am/ProcessPowerCleaner;->cleanAllSubProcess()V

    .line 254
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 255
    .local v1, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "com.android.deskclock"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 256
    if-eqz v0, :cond_0

    .line 257
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 259
    :cond_0
    iget-boolean v2, p0, Lcom/android/server/am/ProcessPowerCleaner;->mLockOffCleanTestEnable:Z

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/android/server/am/ProcessPowerCleaner;->mLockOffCleanTestThreshold:F

    goto :goto_0

    .line 260
    :cond_1
    iget v2, p0, Lcom/android/server/am/ProcessPowerCleaner;->mActiveProcessThreshold:F

    :goto_0
    nop

    .line 259
    const/16 v3, 0x16

    invoke-direct {p0, v2, v3, v1}, Lcom/android/server/am/ProcessPowerCleaner;->killActiveProcess(FILjava/util/List;)V

    .line 262
    iget-boolean v2, p0, Lcom/android/server/am/ProcessPowerCleaner;->mLockOffCleanTestEnable:Z

    if-eqz v2, :cond_2

    .line 263
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/server/am/ProcessPowerCleaner;->mLockOffCleanTestEnable:Z

    .line 264
    return-void

    .line 267
    :cond_2
    iget-object v2, p0, Lcom/android/server/am/ProcessPowerCleaner;->mHandler:Lcom/android/server/am/ProcessPowerCleaner$H;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/android/server/am/ProcessPowerCleaner$H;->removeMessages(I)V

    .line 268
    iget-object v2, p0, Lcom/android/server/am/ProcessPowerCleaner;->mHandler:Lcom/android/server/am/ProcessPowerCleaner$H;

    invoke-virtual {v2, v3}, Lcom/android/server/am/ProcessPowerCleaner$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 269
    .local v2, "message":Landroid/os/Message;
    iget-object v3, p0, Lcom/android/server/am/ProcessPowerCleaner;->mHandler:Lcom/android/server/am/ProcessPowerCleaner$H;

    iget v4, p0, Lcom/android/server/am/ProcessPowerCleaner;->mCheckCPUTime:I

    int-to-long v4, v4

    invoke-virtual {v3, v2, v4, v5}, Lcom/android/server/am/ProcessPowerCleaner$H;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 270
    iget v3, p0, Lcom/android/server/am/ProcessPowerCleaner;->mActiveProcessThreshold:F

    const/high16 v4, 0x3f400000    # 0.75f

    mul-float/2addr v3, v4

    sget v4, Lcom/android/server/am/ProcessPowerCleaner;->ACTIVE_PROCESS_MIX_THRESHOLD:F

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    iput v3, p0, Lcom/android/server/am/ProcessPowerCleaner;->mActiveProcessThreshold:F

    .line 272
    iget v3, p0, Lcom/android/server/am/ProcessPowerCleaner;->mCheckCPUTime:I

    mul-int/lit8 v3, v3, 0x2

    const v4, 0x36ee80

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, p0, Lcom/android/server/am/ProcessPowerCleaner;->mCheckCPUTime:I

    .line 273
    return-void
.end method

.method public powerKillProcess(Lmiui/process/ProcessConfig;)Z
    .locals 3
    .param p1, "config"    # Lmiui/process/ProcessConfig;

    .line 144
    iget-object v0, p0, Lcom/android/server/am/ProcessPowerCleaner;->mHandler:Lcom/android/server/am/ProcessPowerCleaner$H;

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    goto :goto_1

    .line 147
    :cond_0
    const/4 v0, 0x0

    .line 148
    .local v0, "success":Z
    invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getPolicy()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 150
    :pswitch_1
    iget-object v1, p0, Lcom/android/server/am/ProcessPowerCleaner;->mHandler:Lcom/android/server/am/ProcessPowerCleaner$H;

    const/4 v2, 0x1

    invoke-virtual {p0, v2, p1, v1}, Lcom/android/server/am/ProcessPowerCleaner;->createMessage(ILmiui/process/ProcessConfig;Landroid/os/Handler;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessPowerCleaner$H;->sendMessage(Landroid/os/Message;)Z

    .line 151
    const/4 v0, 0x1

    .line 152
    goto :goto_0

    .line 161
    :pswitch_2
    iget-object v1, p0, Lcom/android/server/am/ProcessPowerCleaner;->mHandler:Lcom/android/server/am/ProcessPowerCleaner$H;

    const/4 v2, 0x2

    invoke-virtual {p0, v2, p1, v1}, Lcom/android/server/am/ProcessPowerCleaner;->createMessage(ILmiui/process/ProcessConfig;Landroid/os/Handler;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessPowerCleaner$H;->sendMessage(Landroid/os/Message;)Z

    .line 162
    goto :goto_0

    .line 156
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessPowerCleaner;->handleKillApp(Lmiui/process/ProcessConfig;)Z

    move-result v0

    .line 157
    nop

    .line 166
    :goto_0
    return v0

    .line 145
    .end local v0    # "success":Z
    :cond_1
    :goto_1
    const/4 v0, 0x0

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setLockOffCleanTestEnable(Z)V
    .locals 0
    .param p1, "lockOffCleanTestEnable"    # Z

    .line 341
    iput-boolean p1, p0, Lcom/android/server/am/ProcessPowerCleaner;->mLockOffCleanTestEnable:Z

    .line 342
    return-void
.end method

.method public systemReady(Landroid/content/Context;Lcom/android/server/am/ProcessManagerService;Landroid/os/Looper;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pms"    # Lcom/android/server/am/ProcessManagerService;
    .param p3, "myLooper"    # Landroid/os/Looper;

    .line 71
    invoke-super {p0, p1, p2}, Lcom/android/server/am/ProcessCleanerBase;->systemReady(Landroid/content/Context;Lcom/android/server/am/ProcessManagerService;)V

    .line 72
    iput-object p2, p0, Lcom/android/server/am/ProcessPowerCleaner;->mPMS:Lcom/android/server/am/ProcessManagerService;

    .line 73
    invoke-virtual {p2}, Lcom/android/server/am/ProcessManagerService;->getProcessPolicy()Lcom/android/server/am/ProcessPolicy;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/ProcessPowerCleaner;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    .line 74
    invoke-static {}, Lcom/android/server/am/SystemPressureController;->getInstance()Lcom/android/server/am/SystemPressureController;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/ProcessPowerCleaner;->mSysPressureCtrl:Lcom/android/server/am/SystemPressureController;

    .line 75
    iput-object p1, p0, Lcom/android/server/am/ProcessPowerCleaner;->mContext:Landroid/content/Context;

    .line 76
    const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    iput-object v0, p0, Lcom/android/server/am/ProcessPowerCleaner;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    .line 78
    new-instance v0, Lcom/android/server/am/ProcessPowerCleaner$ScreenStatusReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/am/ProcessPowerCleaner$ScreenStatusReceiver;-><init>(Lcom/android/server/am/ProcessPowerCleaner;Lcom/android/server/am/ProcessPowerCleaner$ScreenStatusReceiver-IA;)V

    iput-object v0, p0, Lcom/android/server/am/ProcessPowerCleaner;->mScreenStatusReceiver:Lcom/android/server/am/ProcessPowerCleaner$ScreenStatusReceiver;

    .line 79
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 80
    .local v0, "screenStatus":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 81
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 82
    iget-object v1, p0, Lcom/android/server/am/ProcessPowerCleaner;->mScreenStatusReceiver:Lcom/android/server/am/ProcessPowerCleaner$ScreenStatusReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 84
    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessPowerCleaner;->registerCloudObserver(Landroid/content/Context;)V

    .line 85
    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessPowerCleaner;->updateCloudControlParas(Landroid/content/Context;)V

    .line 87
    new-instance v1, Lcom/android/server/am/ProcessPowerCleaner$H;

    invoke-direct {v1, p0, p3}, Lcom/android/server/am/ProcessPowerCleaner$H;-><init>(Lcom/android/server/am/ProcessPowerCleaner;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/server/am/ProcessPowerCleaner;->mHandler:Lcom/android/server/am/ProcessPowerCleaner$H;

    .line 88
    return-void
.end method
