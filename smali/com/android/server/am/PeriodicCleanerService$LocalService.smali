.class final Lcom/android/server/am/PeriodicCleanerService$LocalService;
.super Ljava/lang/Object;
.source "PeriodicCleanerService.java"

# interfaces
.implements Lcom/android/server/am/PeriodicCleanerInternalStub;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/PeriodicCleanerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "LocalService"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/PeriodicCleanerService;


# direct methods
.method private constructor <init>(Lcom/android/server/am/PeriodicCleanerService;)V
    .locals 0

    .line 1709
    iput-object p1, p0, Lcom/android/server/am/PeriodicCleanerService$LocalService;->this$0:Lcom/android/server/am/PeriodicCleanerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/am/PeriodicCleanerService;Lcom/android/server/am/PeriodicCleanerService$LocalService-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/PeriodicCleanerService$LocalService;-><init>(Lcom/android/server/am/PeriodicCleanerService;)V

    return-void
.end method


# virtual methods
.method public enablePeriodicOrNot(Z)V
    .locals 2
    .param p1, "periodicState"    # Z

    .line 1726
    iget-object v0, p0, Lcom/android/server/am/PeriodicCleanerService$LocalService;->this$0:Lcom/android/server/am/PeriodicCleanerService;

    invoke-static {v0, p1}, Lcom/android/server/am/PeriodicCleanerService;->-$$Nest$fputmReady(Lcom/android/server/am/PeriodicCleanerService;Z)V

    .line 1727
    invoke-static {}, Lcom/android/server/am/PeriodicCleanerService;->-$$Nest$sfgetDEBUG()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "set periodicCleanerPolicy State to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PeriodicCleaner"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1728
    :cond_0
    return-void
.end method

.method public reportEvent(Ljava/lang/String;Ljava/lang/String;IIZ)V
    .locals 8
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "className"    # Ljava/lang/String;
    .param p3, "uid"    # I
    .param p4, "eventType"    # I
    .param p5, "fullscreen"    # Z

    .line 1712
    new-instance v7, Lcom/android/server/am/PeriodicCleanerService$MyEvent;

    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService$LocalService;->this$0:Lcom/android/server/am/PeriodicCleanerService;

    move-object v0, v7

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/android/server/am/PeriodicCleanerService$MyEvent;-><init>(Lcom/android/server/am/PeriodicCleanerService;Ljava/lang/String;Ljava/lang/String;IIZ)V

    .line 1713
    .local v0, "event":Lcom/android/server/am/PeriodicCleanerService$MyEvent;
    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService$LocalService;->this$0:Lcom/android/server/am/PeriodicCleanerService;

    invoke-static {v1}, Lcom/android/server/am/PeriodicCleanerService;->-$$Nest$fgetmHandler(Lcom/android/server/am/PeriodicCleanerService;)Lcom/android/server/am/PeriodicCleanerService$MyHandler;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Lcom/android/server/am/PeriodicCleanerService$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 1714
    return-void
.end method

.method public reportMemPressure(I)V
    .locals 3
    .param p1, "pressureState"    # I

    .line 1717
    iget-object v0, p0, Lcom/android/server/am/PeriodicCleanerService$LocalService;->this$0:Lcom/android/server/am/PeriodicCleanerService;

    invoke-static {v0}, Lcom/android/server/am/PeriodicCleanerService;->-$$Nest$fgetmHandler(Lcom/android/server/am/PeriodicCleanerService;)Lcom/android/server/am/PeriodicCleanerService$MyHandler;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Lcom/android/server/am/PeriodicCleanerService$MyHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1718
    return-void
.end method

.method public reportStartProcess(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "hostType"    # Ljava/lang/String;

    .line 1721
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1722
    .local v0, "processInfo":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService$LocalService;->this$0:Lcom/android/server/am/PeriodicCleanerService;

    invoke-static {v1}, Lcom/android/server/am/PeriodicCleanerService;->-$$Nest$fgetmHandler(Lcom/android/server/am/PeriodicCleanerService;)Lcom/android/server/am/PeriodicCleanerService$MyHandler;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2, v0}, Lcom/android/server/am/PeriodicCleanerService$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 1723
    return-void
.end method
