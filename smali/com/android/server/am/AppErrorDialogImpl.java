public class com.android.server.am.AppErrorDialogImpl extends com.android.server.am.AppErrorDialogStub {
	 /* .source "AppErrorDialogImpl.java" */
	 /* # instance fields */
	 private android.app.ApplicationErrorReport$CrashInfo mCrashInfo;
	 private java.lang.String mCrashPackage;
	 private com.android.server.am.AppErrorDialog mDialog;
	 private final java.util.concurrent.atomic.AtomicBoolean mReported;
	 /* # direct methods */
	 public static void $r8$lambda$VAjEk5XpMvIZpCLxECcApzP_6Z8 ( com.android.server.am.AppErrorDialogImpl p0, android.content.Context p1, android.os.Message p2, android.content.DialogInterface p3, Integer p4 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/am/AppErrorDialogImpl;->lambda$onInit$0(Landroid/content/Context;Landroid/os/Message;Landroid/content/DialogInterface;I)V */
		 return;
	 } // .end method
	 static void -$$Nest$monReportClicked ( com.android.server.am.AppErrorDialogImpl p0, android.content.Context p1, android.os.Message p2, Boolean p3 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/am/AppErrorDialogImpl;->onReportClicked(Landroid/content/Context;Landroid/os/Message;Z)V */
		 return;
	 } // .end method
	 public com.android.server.am.AppErrorDialogImpl ( ) {
		 /* .locals 2 */
		 /* .line 20 */
		 /* invoke-direct {p0}, Lcom/android/server/am/AppErrorDialogStub;-><init>()V */
		 /* .line 23 */
		 /* new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean; */
		 int v1 = 0; // const/4 v1, 0x0
		 /* invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V */
		 this.mReported = v0;
		 return;
	 } // .end method
	 private void lambda$onInit$0 ( android.content.Context p0, android.os.Message p1, android.content.DialogInterface p2, Integer p3 ) { //synthethic
		 /* .locals 1 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "reportMsg" # Landroid/os/Message; */
		 /* .param p3, "dialog1" # Landroid/content/DialogInterface; */
		 /* .param p4, "which" # I */
		 /* .line 49 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* invoke-direct {p0, p1, p2, v0}, Lcom/android/server/am/AppErrorDialogImpl;->onReportClicked(Landroid/content/Context;Landroid/os/Message;Z)V */
		 return;
	 } // .end method
	 private void onReportClicked ( android.content.Context p0, android.os.Message p1, Boolean p2 ) {
		 /* .locals 3 */
		 /* .param p1, "ctx" # Landroid/content/Context; */
		 /* .param p2, "reportMsg" # Landroid/os/Message; */
		 /* .param p3, "jumpToPreview" # Z */
		 /* .line 73 */
		 v0 = this.mReported;
		 int v1 = 0; // const/4 v1, 0x0
		 int v2 = 1; // const/4 v2, 0x1
		 v0 = 		 (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).compareAndSet ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z
		 /* if-nez v0, :cond_0 */
		 /* .line 74 */
		 return;
		 /* .line 76 */
	 } // :cond_0
	 if ( p3 != null) { // if-eqz p3, :cond_1
		 /* .line 77 */
		 v0 = this.mCrashPackage;
		 v1 = this.mCrashInfo;
		 com.android.server.am.MiuiErrorReport .startFcPreviewActivity ( p1,v0,v1 );
		 /* .line 79 */
	 } // :cond_1
	 (( android.os.Message ) p2 ).sendToTarget ( ); // invoke-virtual {p2}, Landroid/os/Message;->sendToTarget()V
	 /* .line 80 */
	 return;
} // .end method
/* # virtual methods */
Boolean onCreate ( ) {
	 /* .locals 4 */
	 /* .line 62 */
	 v0 = this.mDialog;
	 /* .line 63 */
	 (( com.android.server.am.AppErrorDialog ) v0 ).getContext ( ); // invoke-virtual {v0}, Lcom/android/server/am/AppErrorDialog;->getContext()Landroid/content/Context;
	 (( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
	 final String v1 = "id"; // const-string v1, "id"
	 final String v2 = "miuix.stub"; // const-string v2, "miuix.stub"
	 final String v3 = "message"; // const-string v3, "message"
	 v0 = 	 (( android.content.res.Resources ) v0 ).getIdentifier ( v3, v1, v2 ); // invoke-virtual {v0, v3, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
	 /* .line 64 */
	 /* .local v0, "msgViewId":I */
	 v1 = this.mDialog;
	 (( com.android.server.am.AppErrorDialog ) v1 ).findViewById ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/am/AppErrorDialog;->findViewById(I)Landroid/view/View;
	 /* check-cast v1, Landroid/widget/TextView; */
	 /* .line 65 */
	 /* .local v1, "msgView":Landroid/widget/TextView; */
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 66 */
		 android.text.method.LinkMovementMethod .getInstance ( );
		 (( android.widget.TextView ) v1 ).setMovementMethod ( v2 ); // invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V
		 /* .line 68 */
	 } // :cond_0
	 int v2 = 1; // const/4 v2, 0x1
} // .end method
void onInit ( com.android.server.am.AppErrorDialog p0, android.os.Message p1, android.os.Message p2 ) {
	 /* .locals 9 */
	 /* .param p1, "dialog" # Lcom/android/server/am/AppErrorDialog; */
	 /* .param p2, "forceQuitMsg" # Landroid/os/Message; */
	 /* .param p3, "reportMsg" # Landroid/os/Message; */
	 /* .line 29 */
	 this.mDialog = p1;
	 /* .line 30 */
	 (( com.android.server.am.AppErrorDialog ) p1 ).getContext ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppErrorDialog;->getContext()Landroid/content/Context;
	 /* .line 31 */
	 /* .local v0, "context":Landroid/content/Context; */
	 (( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
	 /* .line 32 */
	 /* .local v1, "res":Landroid/content/res/Resources; */
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 /* const v3, 0x110f01e9 */
	 (( android.content.res.Resources ) v1 ).getString ( v3 ); // invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 final String v3 = "\n\n"; // const-string v3, "\n\n"
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 /* .line 33 */
	 /* .local v2, "dialogBody":Ljava/lang/String; */
	 /* const v3, 0x110f01ea */
	 (( android.content.res.Resources ) v1 ).getString ( v3 ); // invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
	 /* .line 34 */
	 /* .local v3, "dialogDumpToPreview":Ljava/lang/String; */
	 /* new-instance v4, Landroid/text/SpannableStringBuilder; */
	 /* new-instance v5, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
	 (( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 /* invoke-direct {v4, v5}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V */
	 /* .line 36 */
	 /* .local v4, "textBuilder":Landroid/text/SpannableStringBuilder; */
	 /* new-instance v5, Lcom/android/server/am/AppErrorDialogImpl$1; */
	 /* invoke-direct {v5, p0, v0, p3}, Lcom/android/server/am/AppErrorDialogImpl$1;-><init>(Lcom/android/server/am/AppErrorDialogImpl;Landroid/content/Context;Landroid/os/Message;)V */
	 /* .line 42 */
	 /* .local v5, "span":Landroid/text/style/ClickableSpan; */
	 v6 = 	 (( java.lang.String ) v2 ).length ( ); // invoke-virtual {v2}, Ljava/lang/String;->length()I
	 /* .line 43 */
	 v7 = 	 (( java.lang.String ) v2 ).length ( ); // invoke-virtual {v2}, Ljava/lang/String;->length()I
	 v8 = 	 (( java.lang.String ) v3 ).length ( ); // invoke-virtual {v3}, Ljava/lang/String;->length()I
	 /* add-int/2addr v7, v8 */
	 /* .line 42 */
	 /* const/16 v8, 0x21 */
	 (( android.text.SpannableStringBuilder ) v4 ).setSpan ( v5, v6, v7, v8 ); // invoke-virtual {v4, v5, v6, v7, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V
	 /* .line 45 */
	 (( com.android.server.am.AppErrorDialog ) p1 ).setMessage ( v4 ); // invoke-virtual {p1, v4}, Lcom/android/server/am/AppErrorDialog;->setMessage(Ljava/lang/CharSequence;)V
	 /* .line 46 */
	 int v6 = 0; // const/4 v6, 0x0
	 (( com.android.server.am.AppErrorDialog ) p1 ).setCancelable ( v6 ); // invoke-virtual {p1, v6}, Lcom/android/server/am/AppErrorDialog;->setCancelable(Z)V
	 /* .line 47 */
	 /* nop */
	 /* .line 48 */
	 (( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
	 /* const v7, 0x110f0028 */
	 (( android.content.res.Resources ) v6 ).getText ( v7 ); // invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;
	 /* new-instance v7, Lcom/android/server/am/AppErrorDialogImpl$$ExternalSyntheticLambda0; */
	 /* invoke-direct {v7, p0, v0, p3}, Lcom/android/server/am/AppErrorDialogImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/am/AppErrorDialogImpl;Landroid/content/Context;Landroid/os/Message;)V */
	 /* .line 47 */
	 int v8 = -1; // const/4 v8, -0x1
	 (( com.android.server.am.AppErrorDialog ) p1 ).setButton ( v8, v6, v7 ); // invoke-virtual {p1, v8, v6, v7}, Lcom/android/server/am/AppErrorDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
	 /* .line 50 */
	 /* nop */
	 /* .line 51 */
	 (( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
	 /* const/high16 v7, 0x1040000 */
	 (( android.content.res.Resources ) v6 ).getText ( v7 ); // invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;
	 /* .line 50 */
	 int v7 = -2; // const/4 v7, -0x2
	 (( com.android.server.am.AppErrorDialog ) p1 ).setButton ( v7, v6, p2 ); // invoke-virtual {p1, v7, v6, p2}, Lcom/android/server/am/AppErrorDialog;->setButton(ILjava/lang/CharSequence;Landroid/os/Message;)V
	 /* .line 53 */
	 return;
} // .end method
void setCrashInfo ( java.lang.String p0, android.app.ApplicationErrorReport$CrashInfo p1 ) {
	 /* .locals 0 */
	 /* .param p1, "crashPkg" # Ljava/lang/String; */
	 /* .param p2, "crashInfo" # Landroid/app/ApplicationErrorReport$CrashInfo; */
	 /* .line 57 */
	 this.mCrashPackage = p1;
	 /* .line 58 */
	 this.mCrashInfo = p2;
	 /* .line 59 */
	 return;
} // .end method
