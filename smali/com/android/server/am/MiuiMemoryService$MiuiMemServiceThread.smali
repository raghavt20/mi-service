.class Lcom/android/server/am/MiuiMemoryService$MiuiMemServiceThread;
.super Ljava/lang/Thread;
.source "MiuiMemoryService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/MiuiMemoryService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MiuiMemServiceThread"
.end annotation


# static fields
.field public static final HOST_NAME:Ljava/lang/String; = "mi_reclaim"


# instance fields
.field final synthetic this$0:Lcom/android/server/am/MiuiMemoryService;


# direct methods
.method private constructor <init>(Lcom/android/server/am/MiuiMemoryService;)V
    .locals 0

    .line 65
    iput-object p1, p0, Lcom/android/server/am/MiuiMemoryService$MiuiMemServiceThread;->this$0:Lcom/android/server/am/MiuiMemoryService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/am/MiuiMemoryService;Lcom/android/server/am/MiuiMemoryService$MiuiMemServiceThread-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/MiuiMemoryService$MiuiMemServiceThread;-><init>(Lcom/android/server/am/MiuiMemoryService;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 70
    const-string v0, "mi_reclaim connection finally shutdown!"

    const-string v1, "MiuiMemoryService"

    const/4 v2, 0x0

    .line 71
    .local v2, "serverSocket":Landroid/net/LocalServerSocket;
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v3

    .line 74
    .local v3, "threadExecutor":Ljava/util/concurrent/ExecutorService;
    :try_start_0
    sget-boolean v4, Lcom/android/server/am/MiuiMemoryService;->DEBUG:Z

    if-eqz v4, :cond_0

    const-string v4, "Create local socket: mi_reclaim"

    invoke-static {v1, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    :cond_0
    new-instance v4, Landroid/net/LocalServerSocket;

    const-string v5, "mi_reclaim"

    invoke-direct {v4, v5}, Landroid/net/LocalServerSocket;-><init>(Ljava/lang/String;)V

    move-object v2, v4

    .line 79
    :goto_0
    sget-boolean v4, Lcom/android/server/am/MiuiMemoryService;->DEBUG:Z

    if-eqz v4, :cond_1

    const-string v4, "Waiting Client connected..."

    invoke-static {v1, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    :cond_1
    invoke-virtual {v2}, Landroid/net/LocalServerSocket;->accept()Landroid/net/LocalSocket;

    move-result-object v4

    .line 82
    .local v4, "socket":Landroid/net/LocalSocket;
    const/16 v5, 0x100

    invoke-virtual {v4, v5}, Landroid/net/LocalSocket;->setReceiveBufferSize(I)V

    .line 83
    invoke-virtual {v4, v5}, Landroid/net/LocalSocket;->setSendBufferSize(I)V

    .line 84
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "There is a client is accepted: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Landroid/net/LocalSocket;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    new-instance v5, Lcom/android/server/am/MiuiMemoryService$ConnectionHandler;

    iget-object v6, p0, Lcom/android/server/am/MiuiMemoryService$MiuiMemServiceThread;->this$0:Lcom/android/server/am/MiuiMemoryService;

    invoke-direct {v5, v6, v4}, Lcom/android/server/am/MiuiMemoryService$ConnectionHandler;-><init>(Lcom/android/server/am/MiuiMemoryService;Landroid/net/LocalSocket;)V

    invoke-interface {v3, v5}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86
    .end local v4    # "socket":Landroid/net/LocalSocket;
    goto :goto_0

    .line 90
    :catchall_0
    move-exception v4

    goto :goto_3

    .line 87
    :catch_0
    move-exception v4

    .line 88
    .local v4, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v5, "mi_reclaim connection catch Exception"

    invoke-static {v1, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 90
    nop

    .end local v4    # "e":Ljava/lang/Exception;
    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    if-eqz v3, :cond_2

    invoke-interface {v3}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 92
    :cond_2
    if-eqz v2, :cond_3

    .line 94
    :try_start_2
    invoke-virtual {v2}, Landroid/net/LocalServerSocket;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 97
    :goto_1
    goto :goto_2

    .line 95
    :catch_1
    move-exception v0

    .line 96
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .end local v0    # "e":Ljava/io/IOException;
    goto :goto_1

    .line 100
    :cond_3
    :goto_2
    sget-boolean v0, Lcom/android/server/am/MiuiMemoryService;->DEBUG:Z

    if-eqz v0, :cond_4

    const-string v0, "mi_reclaim connection ended!"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    :cond_4
    return-void

    .line 90
    :goto_3
    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    if-eqz v3, :cond_5

    invoke-interface {v3}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 92
    :cond_5
    if-eqz v2, :cond_6

    .line 94
    :try_start_3
    invoke-virtual {v2}, Landroid/net/LocalServerSocket;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 97
    goto :goto_4

    .line 95
    :catch_2
    move-exception v0

    .line 96
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 99
    .end local v0    # "e":Ljava/io/IOException;
    :cond_6
    :goto_4
    throw v4
.end method
