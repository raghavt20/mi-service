class com.android.server.am.MemoryControlCloud$3 extends android.database.ContentObserver {
	 /* .source "MemoryControlCloud.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/am/MemoryControlCloud;->registerCloudEnableObserver(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.am.MemoryControlCloud this$0; //synthetic
final android.content.Context val$context; //synthetic
/* # direct methods */
 com.android.server.am.MemoryControlCloud$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/MemoryControlCloud; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 79 */
this.this$0 = p1;
this.val$context = p3;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 5 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 82 */
final String v0 = "MemoryStandardProcessControl"; // const-string v0, "MemoryStandardProcessControl"
if ( p2 != null) { // if-eqz p2, :cond_0
	 /* .line 83 */
	 final String v1 = "cloud_memory_standard_appheap_enable"; // const-string v1, "cloud_memory_standard_appheap_enable"
	 android.provider.Settings$System .getUriFor ( v1 );
	 v2 = 	 (( android.net.Uri ) p2 ).equals ( v2 ); // invoke-virtual {p2, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 /* .line 85 */
		 try { // :try_start_0
			 v2 = this.this$0;
			 com.android.server.am.MemoryControlCloud .-$$Nest$fgetmspc ( v2 );
			 v3 = this.val$context;
			 /* .line 86 */
			 (( android.content.Context ) v3 ).getContentResolver ( ); // invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
			 int v4 = -2; // const/4 v4, -0x2
			 android.provider.Settings$System .getStringForUser ( v3,v1,v4 );
			 /* .line 85 */
			 v1 = 			 java.lang.Boolean .parseBoolean ( v1 );
			 /* iput-boolean v1, v2, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapEnable:Z */
			 /* :try_end_0 */
			 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 91 */
			 /* .line 88 */
			 /* :catch_0 */
			 /* move-exception v1 */
			 /* .line 89 */
			 /* .local v1, "e":Ljava/lang/Exception; */
			 /* new-instance v2, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v3 = "cloud mAppHeapEnable check failed: "; // const-string v3, "cloud mAppHeapEnable check failed: "
			 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 android.util.Slog .e ( v0,v2 );
			 /* .line 90 */
			 v2 = this.this$0;
			 com.android.server.am.MemoryControlCloud .-$$Nest$fgetmspc ( v2 );
			 int v3 = 0; // const/4 v3, 0x0
			 /* iput-boolean v3, v2, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapEnable:Z */
			 /* .line 92 */
		 } // .end local v1 # "e":Ljava/lang/Exception;
	 } // :goto_0
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "cloud app heap control received: "; // const-string v2, "cloud app heap control received: "
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v2 = this.this$0;
	 com.android.server.am.MemoryControlCloud .-$$Nest$fgetmspc ( v2 );
	 /* iget-boolean v2, v2, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapEnable:Z */
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .w ( v0,v1 );
	 /* .line 94 */
} // :cond_0
return;
} // .end method
