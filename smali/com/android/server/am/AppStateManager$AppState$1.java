class com.android.server.am.AppStateManager$AppState$1 implements java.lang.Runnable {
	 /* .source "AppStateManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/am/AppStateManager$AppState;->updateCurrentScenarioAction(IZ)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.am.AppStateManager$AppState this$1; //synthetic
final Integer val$action; //synthetic
final Boolean val$active; //synthetic
/* # direct methods */
 com.android.server.am.AppStateManager$AppState$1 ( ) {
/* .locals 0 */
/* .param p1, "this$1" # Lcom/android/server/am/AppStateManager$AppState; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 976 */
this.this$1 = p1;
/* iput p2, p0, Lcom/android/server/am/AppStateManager$AppState$1;->val$action:I */
/* iput-boolean p3, p0, Lcom/android/server/am/AppStateManager$AppState$1;->val$active:Z */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 4 */
/* .line 979 */
v0 = this.this$1;
v0 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmSmartScenarioManager ( v0 );
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$1;->val$action:I */
v2 = this.this$1;
/* iget-boolean v3, p0, Lcom/android/server/am/AppStateManager$AppState$1;->val$active:Z */
(( com.miui.server.smartpower.SmartScenarioManager ) v0 ).onAppActionChanged ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/miui/server/smartpower/SmartScenarioManager;->onAppActionChanged(ILcom/android/server/am/AppStateManager$AppState;Z)V
/* .line 980 */
/* sget-boolean v0, Lcom/android/server/am/AppStateManager;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 981 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
v1 = this.this$1;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = " actions("; // const-string v1, " actions("
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.this$1;
v1 = com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmScenarioActions ( v1 );
/* .line 982 */
com.miui.server.smartpower.SmartScenarioManager .actionTypeToString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ")"; // const-string v1, ")"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 981 */
final String v1 = "SmartPower"; // const-string v1, "SmartPower"
android.util.Slog .i ( v1,v0 );
/* .line 985 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$1;->val$action:I */
/* const/16 v1, 0x400 */
/* if-ne v0, v1, :cond_1 */
/* .line 986 */
v0 = this.this$1;
/* iget-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$1;->val$active:Z */
com.android.server.am.AppStateManager$AppState .-$$Nest$mrecordVideoPlayIfNeeded ( v0,v1 );
/* .line 988 */
} // :cond_1
return;
} // .end method
