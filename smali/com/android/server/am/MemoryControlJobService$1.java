class com.android.server.am.MemoryControlJobService$1 implements java.lang.Runnable {
	 /* .source "MemoryControlJobService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/MemoryControlJobService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.am.MemoryControlJobService this$0; //synthetic
/* # direct methods */
 com.android.server.am.MemoryControlJobService$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/MemoryControlJobService; */
/* .line 29 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 4 */
/* .line 32 */
final String v0 = "MemoryStandardProcessControl"; // const-string v0, "MemoryStandardProcessControl"
final String v1 = "Complete callback"; // const-string v1, "Complete callback"
android.util.Slog .i ( v0,v1 );
/* .line 33 */
v0 = this.this$0;
v0 = this.mFinishCallback;
/* monitor-enter v0 */
/* .line 34 */
try { // :try_start_0
	 v1 = this.this$0;
	 v1 = 	 com.android.server.am.MemoryControlJobService .-$$Nest$fgetmStarted ( v1 );
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 36 */
		 v1 = this.this$0;
		 com.android.server.am.MemoryControlJobService .-$$Nest$fgetmJobParams ( v1 );
		 int v3 = 0; // const/4 v3, 0x0
		 (( com.android.server.am.MemoryControlJobService ) v1 ).jobFinished ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/am/MemoryControlJobService;->jobFinished(Landroid/app/job/JobParameters;Z)V
		 /* .line 37 */
		 v1 = this.this$0;
		 com.android.server.am.MemoryControlJobService .-$$Nest$fputmStarted ( v1,v3 );
		 /* .line 39 */
	 } // :cond_0
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* .line 41 */
	 com.android.server.am.MemoryControlJobService .-$$Nest$sfgetsContext ( );
	 com.android.server.am.MemoryControlJobService .killSchedule ( v0 );
	 /* .line 42 */
	 return;
	 /* .line 39 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 try { // :try_start_1
		 /* monitor-exit v0 */
		 /* :try_end_1 */
		 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
		 /* throw v1 */
	 } // .end method
