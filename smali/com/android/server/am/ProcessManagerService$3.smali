.class Lcom/android/server/am/ProcessManagerService$3;
.super Ljava/lang/Object;
.source "ProcessManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/ProcessManagerService;->notifyForegroundInfoChanged(Lcom/android/server/wm/FgActivityChangedInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ProcessManagerService;

.field final synthetic val$fgInfo:Lcom/android/server/wm/FgActivityChangedInfo;


# direct methods
.method constructor <init>(Lcom/android/server/am/ProcessManagerService;Lcom/android/server/wm/FgActivityChangedInfo;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/ProcessManagerService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1017
    iput-object p1, p0, Lcom/android/server/am/ProcessManagerService$3;->this$0:Lcom/android/server/am/ProcessManagerService;

    iput-object p2, p0, Lcom/android/server/am/ProcessManagerService$3;->val$fgInfo:Lcom/android/server/wm/FgActivityChangedInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .line 1020
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$3;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-static {v0}, Lcom/android/server/am/ProcessManagerService;->-$$Nest$fgetmForegroundInfoManager(Lcom/android/server/am/ProcessManagerService;)Lcom/android/server/wm/ForegroundInfoManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService$3;->val$fgInfo:Lcom/android/server/wm/FgActivityChangedInfo;

    invoke-virtual {v0, v1}, Lcom/android/server/wm/ForegroundInfoManager;->notifyForegroundInfoChanged(Lcom/android/server/wm/FgActivityChangedInfo;)V

    .line 1021
    return-void
.end method
