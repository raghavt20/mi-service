.class public Lcom/android/server/am/ProcessKiller;
.super Ljava/lang/Object;
.source "ProcessKiller.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ProcessManager"


# instance fields
.field private mActivityManagerService:Lcom/android/server/am/ActivityManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/am/ActivityManagerService;)V
    .locals 0
    .param p1, "ams"    # Lcom/android/server/am/ActivityManagerService;

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/android/server/am/ProcessKiller;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    .line 23
    return-void
.end method

.method private forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I
    .param p3, "reason"    # Ljava/lang/String;

    .line 79
    const-class v0, Landroid/app/ActivityManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManagerInternal;

    .line 80
    invoke-virtual {v0, p1, p2, p3}, Landroid/app/ActivityManagerInternal;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V

    .line 81
    return-void
.end method

.method private isInterestingToUser(Lcom/android/server/am/ProcessRecord;)Z
    .locals 1
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 26
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getWindowProcessController()Lcom/android/server/wm/WindowProcessController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/WindowProcessController;->isInterestingToUser()Z

    move-result v0

    return v0
.end method

.method private killLocked(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
    .locals 2
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "reason"    # Ljava/lang/String;

    .line 73
    if-eqz p1, :cond_0

    .line 74
    const/16 v0, 0xd

    const/4 v1, 0x1

    invoke-virtual {p1, p2, v0, v1}, Lcom/android/server/am/ProcessRecord;->killLocked(Ljava/lang/String;IZ)V

    .line 76
    :cond_0
    return-void
.end method

.method public static scheduleCrashApp(Lcom/android/server/am/ActivityManagerService;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)Z
    .locals 3
    .param p0, "mService"    # Lcom/android/server/am/ActivityManagerService;
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "reason"    # Ljava/lang/String;

    .line 84
    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 85
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v1

    .line 86
    .local v1, "thread":Landroid/app/IApplicationThread;
    if-eqz v1, :cond_0

    .line 87
    monitor-enter p0

    .line 89
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p1, p2, v0, v2}, Lcom/android/server/am/ProcessRecord;->scheduleCrashLocked(Ljava/lang/String;ILandroid/os/Bundle;)V

    .line 90
    monitor-exit p0

    const/4 v0, 0x1

    return v0

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 94
    .end local v1    # "thread":Landroid/app/IApplicationThread;
    :cond_0
    return v0
.end method

.method private scheduleTrimMemory(Lcom/android/server/am/ProcessRecord;I)V
    .locals 1
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "level"    # I

    .line 63
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 65
    :try_start_0
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v0

    invoke-interface {v0, p2}, Landroid/app/IApplicationThread;->scheduleTrimMemory(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    goto :goto_0

    .line 66
    :catch_0
    move-exception v0

    .line 67
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 70
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_0
    return-void
.end method


# virtual methods
.method public forceStopPackage(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "reason"    # Ljava/lang/String;
    .param p3, "evenForeground"    # Z

    .line 30
    if-nez p3, :cond_0

    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessKiller;->isInterestingToUser(Lcom/android/server/am/ProcessRecord;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    return-void

    .line 33
    :cond_0
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget v1, p1, Lcom/android/server/am/ProcessRecord;->userId:I

    invoke-direct {p0, v0, v1, p2}, Lcom/android/server/am/ProcessKiller;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V

    .line 34
    return-void
.end method

.method public killApplication(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Z)Z
    .locals 2
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "reason"    # Ljava/lang/String;
    .param p3, "evenForeground"    # Z

    .line 37
    iget-object v0, p0, Lcom/android/server/am/ProcessKiller;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v0

    .line 38
    if-nez p3, :cond_0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessKiller;->isInterestingToUser(Lcom/android/server/am/ProcessRecord;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 39
    monitor-exit v0

    const/4 v0, 0x0

    return v0

    .line 41
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/android/server/am/ProcessKiller;->killLocked(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V

    .line 42
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 43
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public killBackgroundApplication(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
    .locals 3
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "reason"    # Ljava/lang/String;

    .line 47
    iget-object v0, p0, Lcom/android/server/am/ProcessKiller;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget v2, p1, Lcom/android/server/am/ProcessRecord;->userId:I

    invoke-virtual {v0, v1, v2, p2}, Lcom/android/server/am/ActivityManagerService;->killBackgroundProcesses(Ljava/lang/String;ILjava/lang/String;)V

    .line 48
    return-void
.end method

.method public trimMemory(Lcom/android/server/am/ProcessRecord;Z)V
    .locals 2
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "evenForeground"    # Z

    .line 51
    if-nez p2, :cond_0

    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessKiller;->isInterestingToUser(Lcom/android/server/am/ProcessRecord;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    return-void

    .line 55
    :cond_0
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-string v1, "android"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 56
    const/16 v0, 0x3c

    invoke-direct {p0, p1, v0}, Lcom/android/server/am/ProcessKiller;->scheduleTrimMemory(Lcom/android/server/am/ProcessRecord;I)V

    goto :goto_0

    .line 58
    :cond_1
    const/16 v0, 0x50

    invoke-direct {p0, p1, v0}, Lcom/android/server/am/ProcessKiller;->scheduleTrimMemory(Lcom/android/server/am/ProcessRecord;I)V

    .line 60
    :goto_0
    return-void
.end method
