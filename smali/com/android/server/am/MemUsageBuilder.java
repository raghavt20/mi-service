public class com.android.server.am.MemUsageBuilder {
	 /* .source "MemUsageBuilder.java" */
	 /* # static fields */
	 private static final Long DUMP_DMABUF_THRESHOLD;
	 private static final Long DUMP_GPUMemory_THRESHOLD;
	 static final DUMP_MEM_BUCKETS;
	 /* # instance fields */
	 Long cachedPss;
	 java.lang.String fullJava;
	 java.lang.String fullNative;
	 private final com.android.server.am.AppProfiler mAppProfiler;
	 private final java.util.ArrayList memInfos;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/ArrayList<", */
	 /* "Lcom/android/server/am/ProcessMemInfo;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private com.android.server.am.ScoutMeminfo scoutInfo;
java.lang.String shortNative;
java.lang.String stack;
java.lang.String subject;
java.lang.String summary;
final java.lang.String title;
java.lang.String topProcs;
Long totalMemtrack;
Long totalMemtrackGl;
Long totalMemtrackGraphics;
Long totalPss;
Long totalSwapPss;
/* # direct methods */
static com.android.server.am.MemUsageBuilder ( ) {
/* .locals 1 */
/* .line 35 */
/* const/16 v0, 0x17 */
/* new-array v0, v0, [J */
/* fill-array-data v0, :array_0 */
return;
/* :array_0 */
/* .array-data 8 */
/* 0x1400 */
/* 0x1c00 */
/* 0x2800 */
/* 0x3c00 */
/* 0x5000 */
/* 0x7800 */
/* 0xa000 */
/* 0x14000 */
/* 0x1e000 */
/* 0x28000 */
/* 0x32000 */
/* 0x3e800 */
/* 0x4b000 */
/* 0x57800 */
/* 0x64000 */
/* 0x7d000 */
/* 0x96000 */
/* 0xc8000 */
/* 0x100000 */
/* 0x200000 */
/* 0x500000 */
/* 0xa00000 */
/* 0x1400000 */
} // .end array-data
} // .end method
 com.android.server.am.MemUsageBuilder ( ) {
/* .locals 2 */
/* .param p1, "appProfiler" # Lcom/android/server/am/AppProfiler; */
/* .param p3, "title" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/android/server/am/AppProfiler;", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/am/ProcessMemInfo;", */
/* ">;", */
/* "Ljava/lang/String;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 68 */
/* .local p2, "memInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessMemInfo;>;" */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 52 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrackGraphics:J */
/* .line 53 */
/* iput-wide v0, p0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrackGl:J */
/* .line 54 */
/* iput-wide v0, p0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J */
/* .line 55 */
/* iput-wide v0, p0, Lcom/android/server/am/MemUsageBuilder;->totalSwapPss:J */
/* .line 56 */
/* iput-wide v0, p0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrack:J */
/* .line 57 */
/* iput-wide v0, p0, Lcom/android/server/am/MemUsageBuilder;->cachedPss:J */
/* .line 60 */
final String v0 = ""; // const-string v0, ""
this.subject = v0;
/* .line 61 */
this.stack = v0;
/* .line 62 */
this.fullNative = v0;
/* .line 63 */
this.shortNative = v0;
/* .line 64 */
this.fullJava = v0;
/* .line 65 */
this.summary = v0;
/* .line 66 */
this.topProcs = v0;
/* .line 69 */
this.memInfos = p2;
/* .line 70 */
this.mAppProfiler = p1;
/* .line 71 */
this.title = p3;
/* .line 72 */
/* invoke-direct {p0}, Lcom/android/server/am/MemUsageBuilder;->prepare()V */
/* .line 73 */
return;
} // .end method
static void appendBasicMemEntry ( java.lang.StringBuilder p0, Integer p1, Integer p2, Long p3, Long p4, java.lang.String p5 ) {
/* .locals 2 */
/* .param p0, "sb" # Ljava/lang/StringBuilder; */
/* .param p1, "oomAdj" # I */
/* .param p2, "procState" # I */
/* .param p3, "pss" # J */
/* .param p5, "memtrack" # J */
/* .param p7, "name" # Ljava/lang/String; */
/* .line 485 */
final String v0 = " "; // const-string v0, " "
(( java.lang.StringBuilder ) p0 ).append ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 486 */
int v0 = 0; // const/4 v0, 0x0
com.android.server.am.ProcessList .makeOomAdjString ( p1,v0 );
(( java.lang.StringBuilder ) p0 ).append ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 487 */
/* const/16 v0, 0x20 */
(( java.lang.StringBuilder ) p0 ).append ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 488 */
com.android.server.am.ProcessList .makeProcStateString ( p2 );
(( java.lang.StringBuilder ) p0 ).append ( v1 ); // invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 489 */
(( java.lang.StringBuilder ) p0 ).append ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 490 */
com.android.server.am.ProcessList .appendRamKb ( p0,p3,p4 );
/* .line 491 */
final String v0 = ": "; // const-string v0, ": "
(( java.lang.StringBuilder ) p0 ).append ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 492 */
(( java.lang.StringBuilder ) p0 ).append ( p7 ); // invoke-virtual {p0, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 493 */
/* const-wide/16 v0, 0x0 */
/* cmp-long v0, p5, v0 */
/* if-lez v0, :cond_0 */
/* .line 494 */
final String v0 = " ("; // const-string v0, " ("
(( java.lang.StringBuilder ) p0 ).append ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 495 */
com.android.server.am.ActivityManagerService .stringifyKBSize ( p5,p6 );
(( java.lang.StringBuilder ) p0 ).append ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 496 */
final String v0 = " memtrack)"; // const-string v0, " memtrack)"
(( java.lang.StringBuilder ) p0 ).append ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 498 */
} // :cond_0
return;
} // .end method
static final void appendMemBucket ( java.lang.StringBuilder p0, Long p1, java.lang.String p2, Boolean p3 ) {
/* .locals 10 */
/* .param p0, "out" # Ljava/lang/StringBuilder; */
/* .param p1, "memKB" # J */
/* .param p3, "label" # Ljava/lang/String; */
/* .param p4, "stackLike" # Z */
/* .line 516 */
/* const/16 v0, 0x2e */
v0 = (( java.lang.String ) p3 ).lastIndexOf ( v0 ); // invoke-virtual {p3, v0}, Ljava/lang/String;->lastIndexOf(I)I
/* .line 517 */
/* .local v0, "start":I */
/* if-ltz v0, :cond_0 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 518 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 519 */
} // :goto_0
v1 = (( java.lang.String ) p3 ).length ( ); // invoke-virtual {p3}, Ljava/lang/String;->length()I
/* .line 520 */
/* .local v1, "end":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_1
v3 = com.android.server.am.MemUsageBuilder.DUMP_MEM_BUCKETS;
/* array-length v4, v3 */
final String v5 = "MB."; // const-string v5, "MB."
final String v6 = "MB "; // const-string v6, "MB "
/* const-wide/16 v7, 0x400 */
/* if-ge v2, v4, :cond_3 */
/* .line 521 */
/* aget-wide v3, v3, v2 */
/* cmp-long v9, v3, p1 */
/* if-ltz v9, :cond_2 */
/* .line 522 */
/* div-long/2addr v3, v7 */
/* .line 523 */
/* .local v3, "bucket":J */
(( java.lang.StringBuilder ) p0 ).append ( v3, v4 ); // invoke-virtual {p0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
/* .line 524 */
if ( p4 != null) { // if-eqz p4, :cond_1
} // :cond_1
/* move-object v5, v6 */
} // :goto_2
(( java.lang.StringBuilder ) p0 ).append ( v5 ); // invoke-virtual {p0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 525 */
(( java.lang.StringBuilder ) p0 ).append ( p3, v0, v1 ); // invoke-virtual {p0, p3, v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;
/* .line 526 */
return;
/* .line 520 */
} // .end local v3 # "bucket":J
} // :cond_2
/* add-int/lit8 v2, v2, 0x1 */
/* .line 529 */
} // .end local v2 # "i":I
} // :cond_3
/* div-long v2, p1, v7 */
(( java.lang.StringBuilder ) p0 ).append ( v2, v3 ); // invoke-virtual {p0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
/* .line 530 */
if ( p4 != null) { // if-eqz p4, :cond_4
} // :cond_4
/* move-object v5, v6 */
} // :goto_3
(( java.lang.StringBuilder ) p0 ).append ( v5 ); // invoke-virtual {p0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 531 */
(( java.lang.StringBuilder ) p0 ).append ( p3, v0, v1 ); // invoke-virtual {p0, p3, v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;
/* .line 532 */
return;
} // .end method
static void appendMemInfo ( java.lang.StringBuilder p0, com.android.server.am.ProcessMemInfo p1 ) {
/* .locals 8 */
/* .param p0, "sb" # Ljava/lang/StringBuilder; */
/* .param p1, "mi" # Lcom/android/server/am/ProcessMemInfo; */
/* .line 501 */
/* iget v1, p1, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I */
/* iget v2, p1, Lcom/android/server/am/ProcessMemInfo;->procState:I */
/* iget-wide v3, p1, Lcom/android/server/am/ProcessMemInfo;->pss:J */
/* iget-wide v5, p1, Lcom/android/server/am/ProcessMemInfo;->memtrack:J */
v7 = this.name;
/* move-object v0, p0 */
/* invoke-static/range {v0 ..v7}, Lcom/android/server/am/MemUsageBuilder;->appendBasicMemEntry(Ljava/lang/StringBuilder;IIJJLjava/lang/String;)V */
/* .line 502 */
final String v0 = " (pid "; // const-string v0, " (pid "
(( java.lang.StringBuilder ) p0 ).append ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 503 */
/* iget v0, p1, Lcom/android/server/am/ProcessMemInfo;->pid:I */
(( java.lang.StringBuilder ) p0 ).append ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 504 */
final String v0 = ") "; // const-string v0, ") "
(( java.lang.StringBuilder ) p0 ).append ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 505 */
v0 = this.adjType;
(( java.lang.StringBuilder ) p0 ).append ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 506 */
/* const/16 v0, 0xa */
(( java.lang.StringBuilder ) p0 ).append ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 507 */
v1 = this.adjReason;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 508 */
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) p0 ).append ( v1 ); // invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 509 */
v1 = this.adjReason;
(( java.lang.StringBuilder ) p0 ).append ( v1 ); // invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 510 */
(( java.lang.StringBuilder ) p0 ).append ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 512 */
} // :cond_0
return;
} // .end method
private void calculateCachedPss ( ) {
/* .locals 7 */
/* .line 146 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
v1 = this.memInfos;
v1 = (( java.util.ArrayList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
/* .local v1, "size":I */
} // :goto_0
/* if-ge v0, v1, :cond_1 */
/* .line 147 */
v2 = this.memInfos;
(( java.util.ArrayList ) v2 ).get ( v0 ); // invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/am/ProcessMemInfo; */
/* .line 148 */
/* .local v2, "mi":Lcom/android/server/am/ProcessMemInfo; */
/* iget v3, v2, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I */
/* const/16 v4, 0x384 */
/* if-lt v3, v4, :cond_0 */
/* .line 149 */
/* iget-wide v3, p0, Lcom/android/server/am/MemUsageBuilder;->cachedPss:J */
/* iget-wide v5, v2, Lcom/android/server/am/ProcessMemInfo;->pss:J */
/* add-long/2addr v3, v5 */
/* iput-wide v3, p0, Lcom/android/server/am/MemUsageBuilder;->cachedPss:J */
/* .line 146 */
} // .end local v2 # "mi":Lcom/android/server/am/ProcessMemInfo;
} // :cond_0
/* add-int/lit8 v0, v0, 0x1 */
/* .line 152 */
} // .end local v0 # "i":I
} // .end local v1 # "size":I
} // :cond_1
return;
} // .end method
static void lambda$buildSummary$1 ( Long[] p0, com.android.server.am.ScoutMeminfo p1 ) { //synthethic
/* .locals 0 */
/* .param p0, "infos" # [J */
/* .param p1, "scout" # Lcom/android/server/am/ScoutMeminfo; */
/* .line 332 */
(( com.android.server.am.ScoutMeminfo ) p1 ).setInfos ( p0 ); // invoke-virtual {p1, p0}, Lcom/android/server/am/ScoutMeminfo;->setInfos([J)V
return;
} // .end method
static void lambda$buildSummary$10 ( Long p0, com.android.server.am.ScoutMeminfo p1 ) { //synthethic
/* .locals 0 */
/* .param p0, "gpuUsage" # J */
/* .param p2, "scout" # Lcom/android/server/am/ScoutMeminfo; */
/* .line 418 */
(( com.android.server.am.ScoutMeminfo ) p2 ).setGpuUsage ( p0, p1 ); // invoke-virtual {p2, p0, p1}, Lcom/android/server/am/ScoutMeminfo;->setGpuUsage(J)V
return;
} // .end method
static void lambda$buildSummary$11 ( Long p0, com.android.server.am.ScoutMeminfo p1 ) { //synthethic
/* .locals 0 */
/* .param p0, "gpuPrivateUsage" # J */
/* .param p2, "scout" # Lcom/android/server/am/ScoutMeminfo; */
/* .line 419 */
(( com.android.server.am.ScoutMeminfo ) p2 ).setGpuPrivateUsage ( p0, p1 ); // invoke-virtual {p2, p0, p1}, Lcom/android/server/am/ScoutMeminfo;->setGpuPrivateUsage(J)V
return;
} // .end method
static void lambda$buildSummary$12 ( com.android.internal.util.MemInfoReader p0, com.android.server.am.ScoutMeminfo p1 ) { //synthethic
/* .locals 0 */
/* .param p0, "memInfo" # Lcom/android/internal/util/MemInfoReader; */
/* .param p1, "scout" # Lcom/android/server/am/ScoutMeminfo; */
/* .line 435 */
(( com.android.server.am.ScoutMeminfo ) p1 ).setMeminfo ( p0 ); // invoke-virtual {p1, p0}, Lcom/android/server/am/ScoutMeminfo;->setMeminfo(Lcom/android/internal/util/MemInfoReader;)V
return;
} // .end method
static void lambda$buildSummary$13 ( Long p0, com.android.server.am.ScoutMeminfo p1 ) { //synthethic
/* .locals 0 */
/* .param p0, "finalKernelUsed" # J */
/* .param p2, "scout" # Lcom/android/server/am/ScoutMeminfo; */
/* .line 436 */
(( com.android.server.am.ScoutMeminfo ) p2 ).setKernelUsed ( p0, p1 ); // invoke-virtual {p2, p0, p1}, Lcom/android/server/am/ScoutMeminfo;->setKernelUsed(J)V
return;
} // .end method
static void lambda$buildSummary$2 ( Long[] p0, com.android.server.am.ScoutMeminfo p1 ) { //synthethic
/* .locals 0 */
/* .param p0, "ksm" # [J */
/* .param p1, "scout" # Lcom/android/server/am/ScoutMeminfo; */
/* .line 333 */
(( com.android.server.am.ScoutMeminfo ) p1 ).setKsm ( p0 ); // invoke-virtual {p1, p0}, Lcom/android/server/am/ScoutMeminfo;->setKsm([J)V
return;
} // .end method
static void lambda$buildSummary$3 ( Long p0, com.android.server.am.ScoutMeminfo p1 ) { //synthethic
/* .locals 0 */
/* .param p0, "ionHeap" # J */
/* .param p2, "scout" # Lcom/android/server/am/ScoutMeminfo; */
/* .line 342 */
(( com.android.server.am.ScoutMeminfo ) p2 ).setIonHeap ( p0, p1 ); // invoke-virtual {p2, p0, p1}, Lcom/android/server/am/ScoutMeminfo;->setIonHeap(J)V
return;
} // .end method
static void lambda$buildSummary$4 ( Long p0, com.android.server.am.ScoutMeminfo p1 ) { //synthethic
/* .locals 0 */
/* .param p0, "ionPool" # J */
/* .param p2, "scout" # Lcom/android/server/am/ScoutMeminfo; */
/* .line 343 */
(( com.android.server.am.ScoutMeminfo ) p2 ).setIonPool ( p0, p1 ); // invoke-virtual {p2, p0, p1}, Lcom/android/server/am/ScoutMeminfo;->setIonPool(J)V
return;
} // .end method
static void lambda$buildSummary$5 ( Long p0, com.android.server.am.ScoutMeminfo p1 ) { //synthethic
/* .locals 0 */
/* .param p0, "dmabufMapped" # J */
/* .param p2, "scout" # Lcom/android/server/am/ScoutMeminfo; */
/* .line 344 */
(( com.android.server.am.ScoutMeminfo ) p2 ).setDmabufMapped ( p0, p1 ); // invoke-virtual {p2, p0, p1}, Lcom/android/server/am/ScoutMeminfo;->setDmabufMapped(J)V
return;
} // .end method
static void lambda$buildSummary$6 ( Long p0, com.android.server.am.ScoutMeminfo p1 ) { //synthethic
/* .locals 0 */
/* .param p0, "totalExportedDmabuf" # J */
/* .param p2, "scout" # Lcom/android/server/am/ScoutMeminfo; */
/* .line 371 */
(( com.android.server.am.ScoutMeminfo ) p2 ).setTotalExportedDmabuf ( p0, p1 ); // invoke-virtual {p2, p0, p1}, Lcom/android/server/am/ScoutMeminfo;->setTotalExportedDmabuf(J)V
return;
} // .end method
static void lambda$buildSummary$7 ( Long p0, com.android.server.am.ScoutMeminfo p1 ) { //synthethic
/* .locals 0 */
/* .param p0, "totalExportedDmabufHeap" # J */
/* .param p2, "scout" # Lcom/android/server/am/ScoutMeminfo; */
/* .line 381 */
(( com.android.server.am.ScoutMeminfo ) p2 ).setTotalExportedDmabufHeap ( p0, p1 ); // invoke-virtual {p2, p0, p1}, Lcom/android/server/am/ScoutMeminfo;->setTotalExportedDmabufHeap(J)V
return;
} // .end method
static void lambda$buildSummary$8 ( Long p0, com.android.server.am.ScoutMeminfo p1 ) { //synthethic
/* .locals 0 */
/* .param p0, "totalDmabufHeapPool" # J */
/* .param p2, "scout" # Lcom/android/server/am/ScoutMeminfo; */
/* .line 389 */
(( com.android.server.am.ScoutMeminfo ) p2 ).setTotalDmabufHeapPool ( p0, p1 ); // invoke-virtual {p2, p0, p1}, Lcom/android/server/am/ScoutMeminfo;->setTotalDmabufHeapPool(J)V
return;
} // .end method
static void lambda$buildSummary$9 ( Long p0, com.android.server.am.ScoutMeminfo p1 ) { //synthethic
/* .locals 0 */
/* .param p0, "gpuDmaBufUsage" # J */
/* .param p2, "scout" # Lcom/android/server/am/ScoutMeminfo; */
/* .line 409 */
(( com.android.server.am.ScoutMeminfo ) p2 ).setGpuDmaBufUsage ( p0, p1 ); // invoke-virtual {p2, p0, p1}, Lcom/android/server/am/ScoutMeminfo;->setGpuDmaBufUsage(J)V
return;
} // .end method
static Boolean lambda$prepare$0 ( com.android.internal.os.ProcessCpuTracker$Stats p0 ) { //synthethic
/* .locals 4 */
/* .param p0, "st" # Lcom/android/internal/os/ProcessCpuTracker$Stats; */
/* .line 99 */
/* iget-wide v0, p0, Lcom/android/internal/os/ProcessCpuTracker$Stats;->vsize:J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
/* if-lez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private void prepare ( ) {
/* .locals 22 */
/* .line 90 */
/* move-object/from16 v0, p0 */
/* new-instance v1, Landroid/util/SparseArray; */
v2 = this.memInfos;
v2 = (( java.util.ArrayList ) v2 ).size ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->size()I
/* invoke-direct {v1, v2}, Landroid/util/SparseArray;-><init>(I)V */
/* .line 91 */
/* .local v1, "infoMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/am/ProcessMemInfo;>;" */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
v3 = this.memInfos;
v3 = (( java.util.ArrayList ) v3 ).size ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
/* .local v3, "size":I */
} // :goto_0
/* if-ge v2, v3, :cond_0 */
/* .line 92 */
v4 = this.memInfos;
(( java.util.ArrayList ) v4 ).get ( v2 ); // invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v4, Lcom/android/server/am/ProcessMemInfo; */
/* .line 93 */
/* .local v4, "mi":Lcom/android/server/am/ProcessMemInfo; */
/* iget v5, v4, Lcom/android/server/am/ProcessMemInfo;->pid:I */
(( android.util.SparseArray ) v1 ).put ( v5, v4 ); // invoke-virtual {v1, v5, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 91 */
} // .end local v4 # "mi":Lcom/android/server/am/ProcessMemInfo;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 95 */
} // .end local v2 # "i":I
} // .end local v3 # "size":I
} // :cond_0
v2 = this.mAppProfiler;
(( com.android.server.am.AppProfiler ) v2 ).updateCpuStatsNow ( ); // invoke-virtual {v2}, Lcom/android/server/am/AppProfiler;->updateCpuStatsNow()V
/* .line 96 */
int v2 = 4; // const/4 v2, 0x4
/* new-array v2, v2, [J */
/* .line 97 */
/* .local v2, "memtrackTmp":[J */
int v3 = 2; // const/4 v3, 0x2
/* new-array v4, v3, [J */
/* .line 99 */
/* .local v4, "swaptrackTmp":[J */
v5 = this.mAppProfiler;
/* new-instance v6, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda13; */
/* invoke-direct {v6}, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda13;-><init>()V */
(( com.android.server.am.AppProfiler ) v5 ).getCpuStats ( v6 ); // invoke-virtual {v5, v6}, Lcom/android/server/am/AppProfiler;->getCpuStats(Ljava/util/function/Predicate;)Ljava/util/List;
/* .line 100 */
v6 = /* .local v5, "stats":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;" */
/* .line 101 */
/* .local v6, "statsCount":I */
int v7 = 0; // const/4 v7, 0x0
/* .local v7, "i":I */
} // :goto_1
int v8 = 0; // const/4 v8, 0x0
/* const-wide/16 v9, 0x0 */
int v11 = 1; // const/4 v11, 0x1
/* if-ge v7, v6, :cond_3 */
/* .line 102 */
/* check-cast v12, Lcom/android/internal/os/ProcessCpuTracker$Stats; */
/* .line 103 */
/* .local v12, "st":Lcom/android/internal/os/ProcessCpuTracker$Stats; */
/* iget v13, v12, Lcom/android/internal/os/ProcessCpuTracker$Stats;->pid:I */
android.os.Debug .getPss ( v13,v4,v2 );
/* move-result-wide v13 */
/* .line 104 */
/* .local v13, "pss":J */
/* cmp-long v9, v13, v9 */
/* if-lez v9, :cond_2 */
/* .line 105 */
/* iget v9, v12, Lcom/android/internal/os/ProcessCpuTracker$Stats;->pid:I */
v9 = (( android.util.SparseArray ) v1 ).indexOfKey ( v9 ); // invoke-virtual {v1, v9}, Landroid/util/SparseArray;->indexOfKey(I)I
/* if-gez v9, :cond_1 */
/* .line 106 */
/* new-instance v9, Lcom/android/server/am/ProcessMemInfo; */
v10 = this.name;
/* iget v15, v12, Lcom/android/internal/os/ProcessCpuTracker$Stats;->pid:I */
/* const/16 v18, -0x3e8 */
/* const/16 v19, -0x1 */
final String v20 = "native"; // const-string v20, "native"
/* const/16 v21, 0x0 */
/* move/from16 v17, v15 */
/* move-object v15, v9 */
/* move-object/from16 v16, v10 */
/* invoke-direct/range {v15 ..v21}, Lcom/android/server/am/ProcessMemInfo;-><init>(Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;)V */
/* .line 108 */
/* .local v9, "mi":Lcom/android/server/am/ProcessMemInfo; */
/* iput-wide v13, v9, Lcom/android/server/am/ProcessMemInfo;->pss:J */
/* .line 109 */
/* move-object v15, v5 */
/* move/from16 v16, v6 */
} // .end local v5 # "stats":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;"
} // .end local v6 # "statsCount":I
/* .local v15, "stats":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;" */
/* .local v16, "statsCount":I */
/* aget-wide v5, v4, v11 */
/* iput-wide v5, v9, Lcom/android/server/am/ProcessMemInfo;->swapPss:J */
/* .line 110 */
/* aget-wide v5, v2, v8 */
/* iput-wide v5, v9, Lcom/android/server/am/ProcessMemInfo;->memtrack:J */
/* .line 111 */
/* iget-wide v5, v0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrackGraphics:J */
/* aget-wide v10, v2, v11 */
/* add-long/2addr v5, v10 */
/* iput-wide v5, v0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrackGraphics:J */
/* .line 112 */
/* iget-wide v5, v0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrackGl:J */
/* aget-wide v10, v2, v3 */
/* add-long/2addr v5, v10 */
/* iput-wide v5, v0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrackGl:J */
/* .line 113 */
v5 = this.memInfos;
(( java.util.ArrayList ) v5 ).add ( v9 ); // invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 105 */
} // .end local v9 # "mi":Lcom/android/server/am/ProcessMemInfo;
} // .end local v15 # "stats":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;"
} // .end local v16 # "statsCount":I
/* .restart local v5 # "stats":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;" */
/* .restart local v6 # "statsCount":I */
} // :cond_1
/* move-object v15, v5 */
/* move/from16 v16, v6 */
} // .end local v5 # "stats":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;"
} // .end local v6 # "statsCount":I
/* .restart local v15 # "stats":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;" */
/* .restart local v16 # "statsCount":I */
/* .line 104 */
} // .end local v15 # "stats":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;"
} // .end local v16 # "statsCount":I
/* .restart local v5 # "stats":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;" */
/* .restart local v6 # "statsCount":I */
} // :cond_2
/* move-object v15, v5 */
/* move/from16 v16, v6 */
/* .line 101 */
} // .end local v5 # "stats":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;"
} // .end local v6 # "statsCount":I
} // .end local v12 # "st":Lcom/android/internal/os/ProcessCpuTracker$Stats;
} // .end local v13 # "pss":J
/* .restart local v15 # "stats":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;" */
/* .restart local v16 # "statsCount":I */
} // :goto_2
/* add-int/lit8 v7, v7, 0x1 */
/* move-object v5, v15 */
/* move/from16 v6, v16 */
} // .end local v15 # "stats":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;"
} // .end local v16 # "statsCount":I
/* .restart local v5 # "stats":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;" */
/* .restart local v6 # "statsCount":I */
} // :cond_3
/* move-object v15, v5 */
/* move/from16 v16, v6 */
/* .line 118 */
} // .end local v5 # "stats":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;"
} // .end local v6 # "statsCount":I
} // .end local v7 # "i":I
/* .restart local v15 # "stats":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;" */
/* .restart local v16 # "statsCount":I */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "i":I */
v6 = this.memInfos;
v6 = (( java.util.ArrayList ) v6 ).size ( ); // invoke-virtual {v6}, Ljava/util/ArrayList;->size()I
/* .local v6, "size":I */
} // :goto_3
/* if-ge v5, v6, :cond_5 */
/* .line 119 */
v7 = this.memInfos;
(( java.util.ArrayList ) v7 ).get ( v5 ); // invoke-virtual {v7, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v7, Lcom/android/server/am/ProcessMemInfo; */
/* .line 120 */
/* .local v7, "mi":Lcom/android/server/am/ProcessMemInfo; */
/* iget-wide v12, v7, Lcom/android/server/am/ProcessMemInfo;->pss:J */
/* cmp-long v12, v12, v9 */
/* if-nez v12, :cond_4 */
/* .line 121 */
/* iget v12, v7, Lcom/android/server/am/ProcessMemInfo;->pid:I */
android.os.Debug .getPss ( v12,v4,v2 );
/* move-result-wide v12 */
/* iput-wide v12, v7, Lcom/android/server/am/ProcessMemInfo;->pss:J */
/* .line 122 */
/* aget-wide v12, v4, v11 */
/* iput-wide v12, v7, Lcom/android/server/am/ProcessMemInfo;->swapPss:J */
/* .line 123 */
/* aget-wide v12, v2, v8 */
/* iput-wide v12, v7, Lcom/android/server/am/ProcessMemInfo;->memtrack:J */
/* .line 124 */
/* iget-wide v12, v0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrackGraphics:J */
/* aget-wide v17, v2, v11 */
/* add-long v12, v12, v17 */
/* iput-wide v12, v0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrackGraphics:J */
/* .line 125 */
/* iget-wide v12, v0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrackGl:J */
/* aget-wide v17, v2, v3 */
/* add-long v12, v12, v17 */
/* iput-wide v12, v0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrackGl:J */
/* .line 127 */
} // :cond_4
/* iget-wide v12, v0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J */
/* move-object/from16 v17, v4 */
} // .end local v4 # "swaptrackTmp":[J
/* .local v17, "swaptrackTmp":[J */
/* iget-wide v3, v7, Lcom/android/server/am/ProcessMemInfo;->pss:J */
/* add-long/2addr v12, v3 */
/* iput-wide v12, v0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J */
/* .line 128 */
/* iget-wide v3, v0, Lcom/android/server/am/MemUsageBuilder;->totalSwapPss:J */
/* iget-wide v12, v7, Lcom/android/server/am/ProcessMemInfo;->swapPss:J */
/* add-long/2addr v3, v12 */
/* iput-wide v3, v0, Lcom/android/server/am/MemUsageBuilder;->totalSwapPss:J */
/* .line 129 */
/* iget-wide v3, v0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrack:J */
/* iget-wide v12, v7, Lcom/android/server/am/ProcessMemInfo;->memtrack:J */
/* add-long/2addr v3, v12 */
/* iput-wide v3, v0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrack:J */
/* .line 118 */
} // .end local v7 # "mi":Lcom/android/server/am/ProcessMemInfo;
/* add-int/lit8 v5, v5, 0x1 */
/* move-object/from16 v4, v17 */
int v3 = 2; // const/4 v3, 0x2
} // .end local v17 # "swaptrackTmp":[J
/* .restart local v4 # "swaptrackTmp":[J */
} // :cond_5
/* move-object/from16 v17, v4 */
/* .line 131 */
} // .end local v4 # "swaptrackTmp":[J
} // .end local v5 # "i":I
} // .end local v6 # "size":I
/* .restart local v17 # "swaptrackTmp":[J */
v3 = this.memInfos;
/* new-instance v4, Lcom/android/server/am/MemUsageBuilder$1; */
/* invoke-direct {v4, v0}, Lcom/android/server/am/MemUsageBuilder$1;-><init>(Lcom/android/server/am/MemUsageBuilder;)V */
java.util.Collections .sort ( v3,v4 );
/* .line 142 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/am/MemUsageBuilder;->calculateCachedPss()V */
/* .line 143 */
return;
} // .end method
/* # virtual methods */
void buildAll ( ) {
/* .locals 1 */
/* .line 80 */
(( com.android.server.am.MemUsageBuilder ) p0 ).buildSubject ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemUsageBuilder;->buildSubject()Ljava/lang/String;
this.subject = v0;
/* .line 81 */
(( com.android.server.am.MemUsageBuilder ) p0 ).buildStack ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemUsageBuilder;->buildStack()Ljava/lang/String;
this.stack = v0;
/* .line 82 */
(( com.android.server.am.MemUsageBuilder ) p0 ).buildFullNative ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemUsageBuilder;->buildFullNative()Ljava/lang/String;
this.fullNative = v0;
/* .line 83 */
(( com.android.server.am.MemUsageBuilder ) p0 ).buildShortNative ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemUsageBuilder;->buildShortNative()Ljava/lang/String;
this.shortNative = v0;
/* .line 84 */
(( com.android.server.am.MemUsageBuilder ) p0 ).buildFullJava ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemUsageBuilder;->buildFullJava()Ljava/lang/String;
this.fullJava = v0;
/* .line 85 */
(( com.android.server.am.MemUsageBuilder ) p0 ).buildSummary ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemUsageBuilder;->buildSummary()Ljava/lang/String;
this.summary = v0;
/* .line 86 */
(( com.android.server.am.MemUsageBuilder ) p0 ).buildTopProcs ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemUsageBuilder;->buildTopProcs()Ljava/lang/String;
this.topProcs = v0;
/* .line 87 */
return;
} // .end method
java.lang.String buildFullJava ( ) {
/* .locals 6 */
/* .line 155 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* const/16 v1, 0x400 */
/* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V */
/* .line 156 */
/* .local v0, "fullJavaBuilder":Ljava/lang/StringBuilder; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
v2 = this.memInfos;
v2 = (( java.util.ArrayList ) v2 ).size ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->size()I
/* .local v2, "size":I */
} // :goto_0
/* if-ge v1, v2, :cond_1 */
/* .line 157 */
v3 = this.memInfos;
(( java.util.ArrayList ) v3 ).get ( v1 ); // invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/am/ProcessMemInfo; */
/* .line 158 */
/* .local v3, "mi":Lcom/android/server/am/ProcessMemInfo; */
/* iget v4, v3, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I */
/* const/16 v5, -0x3e8 */
/* if-eq v4, v5, :cond_0 */
/* .line 159 */
com.android.server.am.MemUsageBuilder .appendMemInfo ( v0,v3 );
/* .line 156 */
} // .end local v3 # "mi":Lcom/android/server/am/ProcessMemInfo;
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 162 */
} // .end local v1 # "i":I
} // .end local v2 # "size":I
} // :cond_1
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 163 */
/* iget-wide v1, p0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J */
com.android.server.am.ProcessList .appendRamKb ( v0,v1,v2 );
/* .line 164 */
final String v1 = ": TOTAL"; // const-string v1, ": TOTAL"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 165 */
/* iget-wide v1, p0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrack:J */
/* const-wide/16 v3, 0x0 */
/* cmp-long v1, v1, v3 */
/* if-lez v1, :cond_2 */
/* .line 166 */
final String v1 = " ("; // const-string v1, " ("
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 167 */
/* iget-wide v1, p0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrack:J */
com.android.server.am.ActivityManagerService .stringifyKBSize ( v1,v2 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 168 */
final String v1 = " memtrack)"; // const-string v1, " memtrack)"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 170 */
} // :cond_2
final String v1 = "\n"; // const-string v1, "\n"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 171 */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
java.lang.String buildFullNative ( ) {
/* .locals 6 */
/* .line 204 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* const/16 v1, 0x400 */
/* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V */
/* .line 205 */
/* .local v0, "fullNativeBuilder":Ljava/lang/StringBuilder; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
v2 = this.memInfos;
v2 = (( java.util.ArrayList ) v2 ).size ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->size()I
/* .local v2, "size":I */
} // :goto_0
/* if-ge v1, v2, :cond_1 */
/* .line 206 */
v3 = this.memInfos;
(( java.util.ArrayList ) v3 ).get ( v1 ); // invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/am/ProcessMemInfo; */
/* .line 207 */
/* .local v3, "mi":Lcom/android/server/am/ProcessMemInfo; */
/* iget v4, v3, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I */
/* const/16 v5, -0x3e8 */
/* if-ne v4, v5, :cond_0 */
/* .line 208 */
com.android.server.am.MemUsageBuilder .appendMemInfo ( v0,v3 );
/* .line 205 */
} // .end local v3 # "mi":Lcom/android/server/am/ProcessMemInfo;
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 211 */
} // .end local v1 # "i":I
} // .end local v2 # "size":I
} // :cond_1
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
java.lang.String buildShortNative ( ) {
/* .locals 18 */
/* .line 175 */
/* move-object/from16 v0, p0 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* const/16 v2, 0x400 */
/* invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V */
/* .line 176 */
/* .local v1, "shortNativeBuilder":Ljava/lang/StringBuilder; */
/* const-wide/16 v2, 0x0 */
/* .line 177 */
/* .local v2, "extraNativeRam":J */
/* const-wide/16 v4, 0x0 */
/* .line 178 */
/* .local v4, "extraNativeMemtrack":J */
int v6 = 0; // const/4 v6, 0x0
/* .local v6, "i":I */
v7 = this.memInfos;
v11 = (( java.util.ArrayList ) v7 ).size ( ); // invoke-virtual {v7}, Ljava/util/ArrayList;->size()I
/* move-wide v12, v2 */
/* move-wide v14, v4 */
/* move v2, v6 */
} // .end local v4 # "extraNativeMemtrack":J
} // .end local v6 # "i":I
/* .local v2, "i":I */
/* .local v11, "size":I */
/* .local v12, "extraNativeRam":J */
/* .local v14, "extraNativeMemtrack":J */
} // :goto_0
/* if-ge v2, v11, :cond_3 */
/* .line 179 */
v3 = this.memInfos;
(( java.util.ArrayList ) v3 ).get ( v2 ); // invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* move-object v10, v3 */
/* check-cast v10, Lcom/android/server/am/ProcessMemInfo; */
/* .line 181 */
/* .local v10, "mi":Lcom/android/server/am/ProcessMemInfo; */
/* iget v3, v10, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I */
/* const/16 v4, -0x3e8 */
/* if-ne v3, v4, :cond_1 */
/* .line 183 */
/* iget-wide v3, v10, Lcom/android/server/am/ProcessMemInfo;->pss:J */
/* const-wide/16 v5, 0x200 */
/* cmp-long v3, v3, v5 */
/* if-ltz v3, :cond_0 */
/* .line 184 */
com.android.server.am.MemUsageBuilder .appendMemInfo ( v1,v10 );
/* .line 186 */
} // :cond_0
/* iget-wide v3, v10, Lcom/android/server/am/ProcessMemInfo;->pss:J */
/* add-long/2addr v12, v3 */
/* .line 187 */
/* iget-wide v3, v10, Lcom/android/server/am/ProcessMemInfo;->memtrack:J */
/* add-long/2addr v14, v3 */
/* .line 192 */
} // :cond_1
/* const-wide/16 v3, 0x0 */
/* cmp-long v3, v12, v3 */
/* if-lez v3, :cond_2 */
/* .line 193 */
/* const/16 v4, -0x3e8 */
int v5 = -1; // const/4 v5, -0x1
final String v16 = "(Other native)"; // const-string v16, "(Other native)"
/* move-object v3, v1 */
/* move-wide v6, v12 */
/* move-wide v8, v14 */
/* move-object/from16 v17, v10 */
} // .end local v10 # "mi":Lcom/android/server/am/ProcessMemInfo;
/* .local v17, "mi":Lcom/android/server/am/ProcessMemInfo; */
/* move-object/from16 v10, v16 */
/* invoke-static/range {v3 ..v10}, Lcom/android/server/am/MemUsageBuilder;->appendBasicMemEntry(Ljava/lang/StringBuilder;IIJJLjava/lang/String;)V */
/* .line 195 */
/* const/16 v3, 0xa */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 196 */
/* const-wide/16 v3, 0x0 */
/* move-wide v12, v3 */
} // .end local v12 # "extraNativeRam":J
/* .local v3, "extraNativeRam":J */
/* .line 192 */
} // .end local v3 # "extraNativeRam":J
} // .end local v17 # "mi":Lcom/android/server/am/ProcessMemInfo;
/* .restart local v10 # "mi":Lcom/android/server/am/ProcessMemInfo; */
/* .restart local v12 # "extraNativeRam":J */
} // :cond_2
/* move-object/from16 v17, v10 */
/* .line 178 */
} // .end local v10 # "mi":Lcom/android/server/am/ProcessMemInfo;
} // :goto_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 200 */
} // .end local v2 # "i":I
} // .end local v11 # "size":I
} // :cond_3
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
java.lang.String buildStack ( ) {
/* .locals 11 */
/* .line 215 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* const/16 v1, 0x80 */
/* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V */
/* .line 216 */
/* .local v0, "stack":Ljava/lang/StringBuilder; */
/* iget-wide v1, p0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J */
/* const-string/jumbo v3, "total" */
int v4 = 1; // const/4 v4, 0x1
com.android.server.am.MemUsageBuilder .appendMemBucket ( v0,v1,v2,v3,v4 );
/* .line 217 */
/* const/high16 v1, -0x80000000 */
/* .line 218 */
/* .local v1, "lastOomAdj":I */
int v2 = 1; // const/4 v2, 0x1
/* .line 219 */
/* .local v2, "firstLine":Z */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
v5 = this.memInfos;
v5 = (( java.util.ArrayList ) v5 ).size ( ); // invoke-virtual {v5}, Ljava/util/ArrayList;->size()I
/* .local v5, "size":I */
} // :goto_0
/* if-ge v3, v5, :cond_8 */
/* .line 220 */
v6 = this.memInfos;
(( java.util.ArrayList ) v6 ).get ( v3 ); // invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v6, Lcom/android/server/am/ProcessMemInfo; */
/* .line 221 */
/* .local v6, "mi":Lcom/android/server/am/ProcessMemInfo; */
/* iget v7, v6, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I */
/* const/16 v8, -0x3e8 */
/* if-eq v7, v8, :cond_7 */
/* iget v7, v6, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I */
/* const/16 v8, 0x1f4 */
/* if-lt v7, v8, :cond_0 */
/* iget v7, v6, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I */
/* const/16 v8, 0x258 */
/* if-eq v7, v8, :cond_0 */
/* iget v7, v6, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I */
/* const/16 v8, 0x2bc */
/* if-ne v7, v8, :cond_7 */
/* .line 225 */
} // :cond_0
/* iget v7, v6, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I */
final String v8 = ":"; // const-string v8, ":"
final String v9 = "$"; // const-string v9, "$"
/* if-eq v1, v7, :cond_3 */
/* .line 226 */
/* iget v1, v6, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I */
/* .line 227 */
/* iget v7, v6, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I */
/* if-ltz v7, :cond_2 */
/* .line 228 */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 229 */
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 230 */
int v2 = 0; // const/4 v2, 0x0
/* .line 232 */
} // :cond_1
final String v7 = "\n\t at "; // const-string v7, "\n\t at "
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 234 */
} // :cond_2
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 237 */
} // :cond_3
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 239 */
} // :goto_1
/* iget-wide v9, v6, Lcom/android/server/am/ProcessMemInfo;->pss:J */
v7 = this.name;
com.android.server.am.MemUsageBuilder .appendMemBucket ( v0,v9,v10,v7,v4 );
/* .line 240 */
/* iget v7, v6, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I */
/* if-ltz v7, :cond_7 */
/* add-int/lit8 v7, v3, 0x1 */
/* if-ge v7, v5, :cond_4 */
v7 = this.memInfos;
/* add-int/lit8 v9, v3, 0x1 */
/* .line 241 */
(( java.util.ArrayList ) v7 ).get ( v9 ); // invoke-virtual {v7, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v7, Lcom/android/server/am/ProcessMemInfo; */
/* iget v7, v7, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I */
/* if-eq v7, v1, :cond_7 */
/* .line 242 */
} // :cond_4
final String v7 = "("; // const-string v7, "("
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 243 */
int v7 = 0; // const/4 v7, 0x0
/* .local v7, "k":I */
} // :goto_2
v9 = com.android.server.am.ActivityManagerService.DUMP_MEM_OOM_ADJ;
/* array-length v9, v9 */
/* if-ge v7, v9, :cond_6 */
/* .line 244 */
v9 = com.android.server.am.ActivityManagerService.DUMP_MEM_OOM_ADJ;
/* aget v9, v9, v7 */
/* iget v10, v6, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I */
/* if-ne v9, v10, :cond_5 */
/* .line 245 */
v9 = com.android.server.am.ActivityManagerService.DUMP_MEM_OOM_LABEL;
/* aget-object v9, v9, v7 */
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 246 */
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 247 */
v9 = com.android.server.am.ActivityManagerService.DUMP_MEM_OOM_ADJ;
/* aget v9, v9, v7 */
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 243 */
} // :cond_5
/* add-int/lit8 v7, v7, 0x1 */
/* .line 250 */
} // .end local v7 # "k":I
} // :cond_6
final String v7 = ")"; // const-string v7, ")"
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 219 */
} // .end local v6 # "mi":Lcom/android/server/am/ProcessMemInfo;
} // :cond_7
/* add-int/lit8 v3, v3, 0x1 */
/* goto/16 :goto_0 */
/* .line 254 */
} // .end local v3 # "i":I
} // .end local v5 # "size":I
} // :cond_8
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
java.lang.String buildSubject ( ) {
/* .locals 9 */
/* .line 258 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* const/16 v1, 0x80 */
/* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V */
/* .line 259 */
/* .local v0, "tag":Ljava/lang/StringBuilder; */
v1 = this.title;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " -- "; // const-string v2, " -- "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 260 */
/* iget-wide v1, p0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J */
/* const-string/jumbo v3, "total" */
int v4 = 0; // const/4 v4, 0x0
com.android.server.am.MemUsageBuilder .appendMemBucket ( v0,v1,v2,v3,v4 );
/* .line 262 */
/* const/high16 v1, -0x80000000 */
/* .line 263 */
/* .local v1, "lastOomAdj":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
v3 = this.memInfos;
v3 = (( java.util.ArrayList ) v3 ).size ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
/* .local v3, "size":I */
} // :goto_0
/* if-ge v2, v3, :cond_4 */
/* .line 264 */
v5 = this.memInfos;
(( java.util.ArrayList ) v5 ).get ( v2 ); // invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v5, Lcom/android/server/am/ProcessMemInfo; */
/* .line 266 */
/* .local v5, "mi":Lcom/android/server/am/ProcessMemInfo; */
/* iget v6, v5, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I */
/* const/16 v7, -0x3e8 */
/* if-eq v6, v7, :cond_3 */
/* iget v6, v5, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I */
/* const/16 v7, 0x1f4 */
/* if-lt v6, v7, :cond_0 */
/* iget v6, v5, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I */
/* const/16 v7, 0x258 */
/* if-eq v6, v7, :cond_0 */
/* iget v6, v5, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I */
/* const/16 v7, 0x2bc */
/* if-ne v6, v7, :cond_3 */
/* .line 270 */
} // :cond_0
/* iget v6, v5, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I */
/* if-eq v1, v6, :cond_1 */
/* .line 271 */
/* iget v1, v5, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I */
/* .line 272 */
/* iget v6, v5, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I */
/* if-gtz v6, :cond_2 */
/* .line 273 */
final String v6 = " / "; // const-string v6, " / "
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 276 */
} // :cond_1
final String v6 = " "; // const-string v6, " "
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 278 */
} // :cond_2
} // :goto_1
/* iget v6, v5, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I */
/* if-gtz v6, :cond_3 */
/* .line 279 */
/* iget-wide v6, v5, Lcom/android/server/am/ProcessMemInfo;->pss:J */
v8 = this.name;
com.android.server.am.MemUsageBuilder .appendMemBucket ( v0,v6,v7,v8,v4 );
/* .line 263 */
} // .end local v5 # "mi":Lcom/android/server/am/ProcessMemInfo;
} // :cond_3
/* add-int/lit8 v2, v2, 0x1 */
/* .line 283 */
} // .end local v2 # "i":I
} // .end local v3 # "size":I
} // :cond_4
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
java.lang.String buildSummary ( ) {
/* .locals 30 */
/* .line 287 */
/* move-object/from16 v0, p0 */
v1 = this.scoutInfo;
java.util.Optional .ofNullable ( v1 );
/* .line 288 */
/* .local v1, "scoutInfo":Ljava/util/Optional;, "Ljava/util/Optional<Lcom/android/server/am/ScoutMeminfo;>;" */
/* new-instance v2, Lcom/android/internal/util/MemInfoReader; */
/* invoke-direct {v2}, Lcom/android/internal/util/MemInfoReader;-><init>()V */
/* .line 289 */
/* .local v2, "memInfo":Lcom/android/internal/util/MemInfoReader; */
(( com.android.internal.util.MemInfoReader ) v2 ).readMemInfo ( ); // invoke-virtual {v2}, Lcom/android/internal/util/MemInfoReader;->readMemInfo()V
/* .line 290 */
(( com.android.internal.util.MemInfoReader ) v2 ).getRawInfo ( ); // invoke-virtual {v2}, Lcom/android/internal/util/MemInfoReader;->getRawInfo()[J
/* .line 291 */
/* .local v3, "infos":[J */
int v4 = 0; // const/4 v4, 0x0
java.lang.Boolean .valueOf ( v4 );
/* .line 292 */
/* .local v5, "dumpDmabufInfo":Ljava/lang/Boolean; */
java.lang.Boolean .valueOf ( v4 );
/* .line 294 */
/* .local v6, "dumpGpuInfo":Ljava/lang/Boolean; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* const/16 v8, 0x400 */
/* invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V */
/* .line 295 */
/* .local v7, "memInfoBuilder":Ljava/lang/StringBuilder; */
android.os.Debug .getMemInfo ( v3 );
/* .line 296 */
final String v8 = " MemInfo: "; // const-string v8, " MemInfo: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 297 */
int v8 = 5; // const/4 v8, 0x5
/* aget-wide v8, v3, v8 */
com.android.server.am.ActivityManagerService .stringifyKBSize ( v8,v9 );
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = " slab, "; // const-string v9, " slab, "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 298 */
int v8 = 4; // const/4 v8, 0x4
/* aget-wide v8, v3, v8 */
com.android.server.am.ActivityManagerService .stringifyKBSize ( v8,v9 );
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = " shmem, "; // const-string v9, " shmem, "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 299 */
/* const/16 v8, 0xc */
/* aget-wide v8, v3, v8 */
com.android.server.am.ActivityManagerService .stringifyKBSize ( v8,v9 );
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 300 */
final String v9 = " vm alloc, "; // const-string v9, " vm alloc, "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 301 */
/* const/16 v8, 0xd */
/* aget-wide v8, v3, v8 */
com.android.server.am.ActivityManagerService .stringifyKBSize ( v8,v9 );
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 302 */
final String v9 = " page tables "; // const-string v9, " page tables "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 303 */
/* const/16 v8, 0xe */
/* aget-wide v8, v3, v8 */
com.android.server.am.ActivityManagerService .stringifyKBSize ( v8,v9 );
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 304 */
final String v9 = " kernel stack\n"; // const-string v9, " kernel stack\n"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 305 */
final String v8 = " "; // const-string v8, " "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 306 */
int v8 = 2; // const/4 v8, 0x2
/* aget-wide v9, v3, v8 */
com.android.server.am.ActivityManagerService .stringifyKBSize ( v9,v10 );
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v10 = " buffers, "; // const-string v10, " buffers, "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 307 */
int v9 = 3; // const/4 v9, 0x3
/* aget-wide v10, v3, v9 */
com.android.server.am.ActivityManagerService .stringifyKBSize ( v10,v11 );
(( java.lang.StringBuilder ) v7 ).append ( v10 ); // invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = " cached, "; // const-string v11, " cached, "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 308 */
/* const/16 v10, 0xb */
/* aget-wide v10, v3, v10 */
com.android.server.am.ActivityManagerService .stringifyKBSize ( v10,v11 );
(( java.lang.StringBuilder ) v7 ).append ( v10 ); // invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = " mapped, "; // const-string v11, " mapped, "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 309 */
int v10 = 1; // const/4 v10, 0x1
/* aget-wide v11, v3, v10 */
com.android.server.am.ActivityManagerService .stringifyKBSize ( v11,v12 );
(( java.lang.StringBuilder ) v7 ).append ( v11 ); // invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v12 = " free\n"; // const-string v12, " free\n"
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 310 */
/* const/16 v11, 0xa */
/* aget-wide v12, v3, v11 */
/* const-wide/16 v14, 0x0 */
/* cmp-long v12, v12, v14 */
if ( v12 != null) { // if-eqz v12, :cond_0
/* .line 311 */
final String v12 = " ZRAM: "; // const-string v12, " ZRAM: "
(( java.lang.StringBuilder ) v7 ).append ( v12 ); // invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 312 */
/* aget-wide v11, v3, v11 */
com.android.server.am.ActivityManagerService .stringifyKBSize ( v11,v12 );
(( java.lang.StringBuilder ) v7 ).append ( v11 ); // invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 313 */
final String v11 = " RAM, "; // const-string v11, " RAM, "
(( java.lang.StringBuilder ) v7 ).append ( v11 ); // invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 314 */
/* const/16 v11, 0x8 */
/* aget-wide v11, v3, v11 */
com.android.server.am.ActivityManagerService .stringifyKBSize ( v11,v12 );
(( java.lang.StringBuilder ) v7 ).append ( v11 ); // invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 315 */
final String v11 = " swap total, "; // const-string v11, " swap total, "
(( java.lang.StringBuilder ) v7 ).append ( v11 ); // invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 316 */
/* const/16 v11, 0x9 */
/* aget-wide v11, v3, v11 */
com.android.server.am.ActivityManagerService .stringifyKBSize ( v11,v12 );
(( java.lang.StringBuilder ) v7 ).append ( v11 ); // invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 317 */
final String v11 = " swap free\n"; // const-string v11, " swap free\n"
(( java.lang.StringBuilder ) v7 ).append ( v11 ); // invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 319 */
} // :cond_0
com.android.server.am.ActivityManagerService .getKsmInfo ( );
/* .line 320 */
/* .local v11, "ksm":[J */
/* aget-wide v12, v11, v10 */
/* cmp-long v12, v12, v14 */
/* if-nez v12, :cond_1 */
/* aget-wide v12, v11, v4 */
/* cmp-long v12, v12, v14 */
/* if-nez v12, :cond_1 */
/* aget-wide v12, v11, v8 */
/* cmp-long v12, v12, v14 */
/* if-nez v12, :cond_1 */
/* aget-wide v12, v11, v9 */
/* cmp-long v12, v12, v14 */
if ( v12 != null) { // if-eqz v12, :cond_2
/* .line 322 */
} // :cond_1
final String v12 = " KSM: "; // const-string v12, " KSM: "
(( java.lang.StringBuilder ) v7 ).append ( v12 ); // invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 323 */
/* aget-wide v12, v11, v10 */
com.android.server.am.ActivityManagerService .stringifyKBSize ( v12,v13 );
(( java.lang.StringBuilder ) v7 ).append ( v12 ); // invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 324 */
final String v12 = " saved from shared "; // const-string v12, " saved from shared "
(( java.lang.StringBuilder ) v7 ).append ( v12 ); // invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 325 */
/* aget-wide v12, v11, v4 */
com.android.server.am.ActivityManagerService .stringifyKBSize ( v12,v13 );
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 326 */
final String v4 = "\n "; // const-string v4, "\n "
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 327 */
/* aget-wide v12, v11, v8 */
com.android.server.am.ActivityManagerService .stringifyKBSize ( v12,v13 );
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 328 */
final String v4 = " unshared; "; // const-string v4, " unshared; "
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 329 */
/* aget-wide v8, v11, v9 */
com.android.server.am.ActivityManagerService .stringifyKBSize ( v8,v9 );
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 330 */
final String v4 = " volatile\n"; // const-string v4, " volatile\n"
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 332 */
} // :cond_2
/* new-instance v4, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda0; */
/* invoke-direct {v4, v3}, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda0;-><init>([J)V */
(( java.util.Optional ) v1 ).ifPresent ( v4 ); // invoke-virtual {v1, v4}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V
/* .line 333 */
/* new-instance v4, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda4; */
/* invoke-direct {v4, v11}, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda4;-><init>([J)V */
(( java.util.Optional ) v1 ).ifPresent ( v4 ); // invoke-virtual {v1, v4}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V
/* .line 334 */
final String v4 = " Free RAM: "; // const-string v4, " Free RAM: "
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 335 */
/* iget-wide v8, v0, Lcom/android/server/am/MemUsageBuilder;->cachedPss:J */
(( com.android.internal.util.MemInfoReader ) v2 ).getCachedSizeKb ( ); // invoke-virtual {v2}, Lcom/android/internal/util/MemInfoReader;->getCachedSizeKb()J
/* move-result-wide v12 */
/* add-long/2addr v8, v12 */
/* .line 336 */
(( com.android.internal.util.MemInfoReader ) v2 ).getFreeSizeKb ( ); // invoke-virtual {v2}, Lcom/android/internal/util/MemInfoReader;->getFreeSizeKb()J
/* move-result-wide v12 */
/* add-long/2addr v8, v12 */
/* .line 335 */
com.android.server.am.ActivityManagerService .stringifyKBSize ( v8,v9 );
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 337 */
final String v4 = "\n"; // const-string v4, "\n"
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 338 */
(( com.android.internal.util.MemInfoReader ) v2 ).getKernelUsedSizeKb ( ); // invoke-virtual {v2}, Lcom/android/internal/util/MemInfoReader;->getKernelUsedSizeKb()J
/* move-result-wide v8 */
/* .line 339 */
/* .local v8, "kernelUsed":J */
android.os.Debug .getIonHeapsSizeKb ( );
/* move-result-wide v12 */
/* .line 340 */
/* .local v12, "ionHeap":J */
/* move-object/from16 v16, v11 */
} // .end local v11 # "ksm":[J
/* .local v16, "ksm":[J */
android.os.Debug .getIonPoolsSizeKb ( );
/* move-result-wide v10 */
/* .line 341 */
/* .local v10, "ionPool":J */
android.os.Debug .getDmabufMappedSizeKb ( );
/* move-result-wide v14 */
/* .line 342 */
/* .local v14, "dmabufMapped":J */
/* move-object/from16 v19, v3 */
} // .end local v3 # "infos":[J
/* .local v19, "infos":[J */
/* new-instance v3, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda5; */
/* invoke-direct {v3, v12, v13}, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda5;-><init>(J)V */
(( java.util.Optional ) v1 ).ifPresent ( v3 ); // invoke-virtual {v1, v3}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V
/* .line 343 */
/* new-instance v3, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda6; */
/* invoke-direct {v3, v10, v11}, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda6;-><init>(J)V */
(( java.util.Optional ) v1 ).ifPresent ( v3 ); // invoke-virtual {v1, v3}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V
/* .line 344 */
/* new-instance v3, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda7; */
/* invoke-direct {v3, v14, v15}, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda7;-><init>(J)V */
(( java.util.Optional ) v1 ).ifPresent ( v3 ); // invoke-virtual {v1, v3}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V
/* .line 345 */
/* const-wide/16 v17, 0x0 */
/* cmp-long v3, v12, v17 */
/* const-wide/32 v20, 0x100000 */
/* if-ltz v3, :cond_3 */
/* cmp-long v3, v10, v17 */
/* if-ltz v3, :cond_3 */
/* .line 346 */
/* sub-long v22, v12, v14 */
/* .line 347 */
/* .local v22, "ionUnmapped":J */
final String v3 = " ION: "; // const-string v3, " ION: "
(( java.lang.StringBuilder ) v7 ).append ( v3 ); // invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 348 */
/* add-long v24, v12, v10 */
/* invoke-static/range {v24 ..v25}, Lcom/android/server/am/ActivityManagerService;->stringifyKBSize(J)Ljava/lang/String; */
(( java.lang.StringBuilder ) v7 ).append ( v3 ); // invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 349 */
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 350 */
/* add-long v8, v8, v22 */
/* .line 354 */
/* move-object v3, v5 */
/* move-object/from16 v24, v6 */
} // .end local v5 # "dumpDmabufInfo":Ljava/lang/Boolean;
} // .end local v6 # "dumpGpuInfo":Ljava/lang/Boolean;
/* .local v3, "dumpDmabufInfo":Ljava/lang/Boolean; */
/* .local v24, "dumpGpuInfo":Ljava/lang/Boolean; */
/* iget-wide v5, v0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J */
/* move-wide/from16 v25, v8 */
} // .end local v8 # "kernelUsed":J
/* .local v25, "kernelUsed":J */
/* iget-wide v8, v0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrackGraphics:J */
/* sub-long/2addr v5, v8 */
/* iput-wide v5, v0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J */
/* .line 355 */
/* add-long/2addr v5, v14 */
/* iput-wide v5, v0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J */
/* .line 356 */
} // .end local v22 # "ionUnmapped":J
/* move-object v5, v3 */
/* move-wide/from16 v28, v10 */
/* move-wide/from16 v8, v25 */
/* goto/16 :goto_1 */
/* .line 345 */
} // .end local v3 # "dumpDmabufInfo":Ljava/lang/Boolean;
} // .end local v24 # "dumpGpuInfo":Ljava/lang/Boolean;
} // .end local v25 # "kernelUsed":J
/* .restart local v5 # "dumpDmabufInfo":Ljava/lang/Boolean; */
/* .restart local v6 # "dumpGpuInfo":Ljava/lang/Boolean; */
/* .restart local v8 # "kernelUsed":J */
} // :cond_3
/* move-object v3, v5 */
/* move-object/from16 v24, v6 */
/* .line 357 */
} // .end local v5 # "dumpDmabufInfo":Ljava/lang/Boolean;
} // .end local v6 # "dumpGpuInfo":Ljava/lang/Boolean;
/* .restart local v3 # "dumpDmabufInfo":Ljava/lang/Boolean; */
/* .restart local v24 # "dumpGpuInfo":Ljava/lang/Boolean; */
android.os.Debug .getDmabufTotalExportedKb ( );
/* move-result-wide v5 */
/* .line 358 */
/* .local v5, "totalExportedDmabuf":J */
/* const-wide/16 v17, 0x0 */
/* cmp-long v22, v5, v17 */
/* if-ltz v22, :cond_5 */
/* .line 359 */
/* sub-long v22, v5, v14 */
/* .line 360 */
/* .local v22, "dmabufUnmapped":J */
/* move-object/from16 v25, v3 */
} // .end local v3 # "dumpDmabufInfo":Ljava/lang/Boolean;
/* .local v25, "dumpDmabufInfo":Ljava/lang/Boolean; */
final String v3 = "DMA-BUF: "; // const-string v3, "DMA-BUF: "
(( java.lang.StringBuilder ) v7 ).append ( v3 ); // invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 361 */
com.android.server.am.ActivityManagerService .stringifyKBSize ( v5,v6 );
(( java.lang.StringBuilder ) v7 ).append ( v3 ); // invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 362 */
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 363 */
/* cmp-long v3, v22, v20 */
/* if-lez v3, :cond_4 */
/* .line 364 */
int v3 = 1; // const/4 v3, 0x1
java.lang.Boolean .valueOf ( v3 );
/* .line 367 */
} // :cond_4
/* add-long v8, v8, v22 */
/* .line 369 */
/* move-wide/from16 v26, v8 */
} // .end local v8 # "kernelUsed":J
/* .local v26, "kernelUsed":J */
/* iget-wide v8, v0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J */
/* move-wide/from16 v28, v10 */
} // .end local v10 # "ionPool":J
/* .local v28, "ionPool":J */
/* iget-wide v10, v0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrackGraphics:J */
/* sub-long/2addr v8, v10 */
/* iput-wide v8, v0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J */
/* .line 370 */
/* add-long/2addr v8, v14 */
/* iput-wide v8, v0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J */
/* .line 371 */
/* new-instance v3, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda8; */
/* invoke-direct {v3, v5, v6}, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda8;-><init>(J)V */
(( java.util.Optional ) v1 ).ifPresent ( v3 ); // invoke-virtual {v1, v3}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V
/* move-wide/from16 v8, v26 */
/* .line 358 */
} // .end local v22 # "dmabufUnmapped":J
} // .end local v25 # "dumpDmabufInfo":Ljava/lang/Boolean;
} // .end local v26 # "kernelUsed":J
} // .end local v28 # "ionPool":J
/* .restart local v3 # "dumpDmabufInfo":Ljava/lang/Boolean; */
/* .restart local v8 # "kernelUsed":J */
/* .restart local v10 # "ionPool":J */
} // :cond_5
/* move-object/from16 v25, v3 */
/* move-wide/from16 v28, v10 */
/* .line 375 */
} // .end local v3 # "dumpDmabufInfo":Ljava/lang/Boolean;
} // .end local v10 # "ionPool":J
/* .restart local v25 # "dumpDmabufInfo":Ljava/lang/Boolean; */
/* .restart local v28 # "ionPool":J */
} // :goto_0
android.os.Debug .getDmabufHeapTotalExportedKb ( );
/* move-result-wide v10 */
/* .line 376 */
/* .local v10, "totalExportedDmabufHeap":J */
/* const-wide/16 v17, 0x0 */
/* cmp-long v3, v10, v17 */
/* if-ltz v3, :cond_6 */
/* .line 377 */
final String v3 = "DMA-BUF Heap: "; // const-string v3, "DMA-BUF Heap: "
(( java.lang.StringBuilder ) v7 ).append ( v3 ); // invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 378 */
com.android.server.am.ActivityManagerService .stringifyKBSize ( v10,v11 );
(( java.lang.StringBuilder ) v7 ).append ( v3 ); // invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 379 */
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 380 */
/* new-instance v3, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda9; */
/* invoke-direct {v3, v10, v11}, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda9;-><init>(J)V */
(( java.util.Optional ) v1 ).ifPresent ( v3 ); // invoke-virtual {v1, v3}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V
/* .line 384 */
} // :cond_6
/* move-wide/from16 v22, v5 */
} // .end local v5 # "totalExportedDmabuf":J
/* .local v22, "totalExportedDmabuf":J */
android.os.Debug .getDmabufHeapPoolsSizeKb ( );
/* move-result-wide v5 */
/* .line 385 */
/* .local v5, "totalDmabufHeapPool":J */
/* const-wide/16 v17, 0x0 */
/* cmp-long v3, v5, v17 */
/* if-ltz v3, :cond_7 */
/* .line 386 */
final String v3 = "DMA-BUF Heaps pool: "; // const-string v3, "DMA-BUF Heaps pool: "
(( java.lang.StringBuilder ) v7 ).append ( v3 ); // invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 387 */
com.android.server.am.ActivityManagerService .stringifyKBSize ( v5,v6 );
(( java.lang.StringBuilder ) v7 ).append ( v3 ); // invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 388 */
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 389 */
/* new-instance v3, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda10; */
/* invoke-direct {v3, v5, v6}, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda10;-><init>(J)V */
(( java.util.Optional ) v1 ).ifPresent ( v3 ); // invoke-virtual {v1, v3}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V
/* .line 393 */
} // .end local v5 # "totalDmabufHeapPool":J
} // .end local v10 # "totalExportedDmabufHeap":J
} // .end local v22 # "totalExportedDmabuf":J
} // :cond_7
/* move-object/from16 v5, v25 */
} // .end local v25 # "dumpDmabufInfo":Ljava/lang/Boolean;
/* .local v5, "dumpDmabufInfo":Ljava/lang/Boolean; */
} // :goto_1
android.os.Debug .getGpuTotalUsageKb ( );
/* move-result-wide v10 */
/* .line 394 */
/* .local v10, "gpuUsage":J */
/* const-wide/16 v17, 0x0 */
/* cmp-long v3, v10, v17 */
/* if-ltz v3, :cond_a */
/* .line 395 */
/* move-wide/from16 v22, v12 */
} // .end local v12 # "ionHeap":J
/* .local v22, "ionHeap":J */
android.os.Debug .getGpuPrivateMemoryKb ( );
/* move-result-wide v12 */
/* .line 396 */
/* .local v12, "gpuPrivateUsage":J */
/* cmp-long v3, v12, v17 */
/* if-ltz v3, :cond_8 */
/* .line 397 */
/* move-wide/from16 v17, v14 */
} // .end local v14 # "dmabufMapped":J
/* .local v17, "dmabufMapped":J */
/* sub-long v14, v10, v12 */
/* .line 398 */
/* .local v14, "gpuDmaBufUsage":J */
final String v3 = " GPU: "; // const-string v3, " GPU: "
(( java.lang.StringBuilder ) v7 ).append ( v3 ); // invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 399 */
com.android.server.am.ActivityManagerService .stringifyKBSize ( v10,v11 );
(( java.lang.StringBuilder ) v7 ).append ( v3 ); // invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 400 */
final String v3 = " ("; // const-string v3, " ("
(( java.lang.StringBuilder ) v7 ).append ( v3 ); // invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 401 */
com.android.server.am.ActivityManagerService .stringifyKBSize ( v14,v15 );
(( java.lang.StringBuilder ) v7 ).append ( v3 ); // invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 402 */
final String v3 = " dmabuf + "; // const-string v3, " dmabuf + "
(( java.lang.StringBuilder ) v7 ).append ( v3 ); // invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 403 */
com.android.server.am.ActivityManagerService .stringifyKBSize ( v12,v13 );
(( java.lang.StringBuilder ) v7 ).append ( v3 ); // invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 404 */
final String v3 = " private)\n"; // const-string v3, " private)\n"
(( java.lang.StringBuilder ) v7 ).append ( v3 ); // invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 407 */
/* move-object v3, v5 */
} // .end local v5 # "dumpDmabufInfo":Ljava/lang/Boolean;
/* .restart local v3 # "dumpDmabufInfo":Ljava/lang/Boolean; */
/* iget-wide v5, v0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J */
/* move-object/from16 v25, v2 */
/* move-object/from16 v26, v3 */
} // .end local v2 # "memInfo":Lcom/android/internal/util/MemInfoReader;
} // .end local v3 # "dumpDmabufInfo":Ljava/lang/Boolean;
/* .local v25, "memInfo":Lcom/android/internal/util/MemInfoReader; */
/* .local v26, "dumpDmabufInfo":Ljava/lang/Boolean; */
/* iget-wide v2, v0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrackGl:J */
/* sub-long/2addr v5, v2 */
/* iput-wide v5, v0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J */
/* .line 408 */
/* add-long/2addr v8, v12 */
/* .line 409 */
/* new-instance v2, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda11; */
/* invoke-direct {v2, v14, v15}, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda11;-><init>(J)V */
(( java.util.Optional ) v1 ).ifPresent ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V
/* .line 410 */
} // .end local v14 # "gpuDmaBufUsage":J
/* move-object/from16 v6, v24 */
/* .line 411 */
} // .end local v17 # "dmabufMapped":J
} // .end local v25 # "memInfo":Lcom/android/internal/util/MemInfoReader;
} // .end local v26 # "dumpDmabufInfo":Ljava/lang/Boolean;
/* .restart local v2 # "memInfo":Lcom/android/internal/util/MemInfoReader; */
/* .restart local v5 # "dumpDmabufInfo":Ljava/lang/Boolean; */
/* .local v14, "dmabufMapped":J */
} // :cond_8
/* move-object/from16 v25, v2 */
/* move-object/from16 v26, v5 */
/* move-wide/from16 v17, v14 */
} // .end local v2 # "memInfo":Lcom/android/internal/util/MemInfoReader;
} // .end local v5 # "dumpDmabufInfo":Ljava/lang/Boolean;
} // .end local v14 # "dmabufMapped":J
/* .restart local v17 # "dmabufMapped":J */
/* .restart local v25 # "memInfo":Lcom/android/internal/util/MemInfoReader; */
/* .restart local v26 # "dumpDmabufInfo":Ljava/lang/Boolean; */
final String v2 = " GPU: "; // const-string v2, " GPU: "
(( java.lang.StringBuilder ) v7 ).append ( v2 ); // invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 412 */
com.android.server.am.ActivityManagerService .stringifyKBSize ( v10,v11 );
(( java.lang.StringBuilder ) v7 ).append ( v2 ); // invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 413 */
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 414 */
/* cmp-long v2, v10, v20 */
/* if-lez v2, :cond_9 */
/* .line 415 */
int v2 = 1; // const/4 v2, 0x1
java.lang.Boolean .valueOf ( v2 );
/* move-object v6, v2 */
} // .end local v24 # "dumpGpuInfo":Ljava/lang/Boolean;
/* .local v2, "dumpGpuInfo":Ljava/lang/Boolean; */
/* .line 414 */
} // .end local v2 # "dumpGpuInfo":Ljava/lang/Boolean;
/* .restart local v24 # "dumpGpuInfo":Ljava/lang/Boolean; */
} // :cond_9
/* move-object/from16 v6, v24 */
/* .line 418 */
} // .end local v24 # "dumpGpuInfo":Ljava/lang/Boolean;
/* .restart local v6 # "dumpGpuInfo":Ljava/lang/Boolean; */
} // :goto_2
/* new-instance v2, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda12; */
/* invoke-direct {v2, v10, v11}, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda12;-><init>(J)V */
(( java.util.Optional ) v1 ).ifPresent ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V
/* .line 419 */
/* new-instance v2, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda1; */
/* invoke-direct {v2, v12, v13}, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda1;-><init>(J)V */
(( java.util.Optional ) v1 ).ifPresent ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V
/* .line 394 */
} // .end local v6 # "dumpGpuInfo":Ljava/lang/Boolean;
} // .end local v17 # "dmabufMapped":J
} // .end local v22 # "ionHeap":J
} // .end local v25 # "memInfo":Lcom/android/internal/util/MemInfoReader;
} // .end local v26 # "dumpDmabufInfo":Ljava/lang/Boolean;
/* .local v2, "memInfo":Lcom/android/internal/util/MemInfoReader; */
/* .restart local v5 # "dumpDmabufInfo":Ljava/lang/Boolean; */
/* .local v12, "ionHeap":J */
/* .restart local v14 # "dmabufMapped":J */
/* .restart local v24 # "dumpGpuInfo":Ljava/lang/Boolean; */
} // :cond_a
/* move-object/from16 v25, v2 */
/* move-object/from16 v26, v5 */
/* move-wide/from16 v22, v12 */
/* move-wide/from16 v17, v14 */
} // .end local v2 # "memInfo":Lcom/android/internal/util/MemInfoReader;
} // .end local v5 # "dumpDmabufInfo":Ljava/lang/Boolean;
} // .end local v12 # "ionHeap":J
} // .end local v14 # "dmabufMapped":J
/* .restart local v17 # "dmabufMapped":J */
/* .restart local v22 # "ionHeap":J */
/* .restart local v25 # "memInfo":Lcom/android/internal/util/MemInfoReader; */
/* .restart local v26 # "dumpDmabufInfo":Ljava/lang/Boolean; */
/* move-object/from16 v6, v24 */
/* .line 421 */
} // .end local v24 # "dumpGpuInfo":Ljava/lang/Boolean;
/* .restart local v6 # "dumpGpuInfo":Ljava/lang/Boolean; */
} // :goto_3
final String v2 = " Used RAM: "; // const-string v2, " Used RAM: "
(( java.lang.StringBuilder ) v7 ).append ( v2 ); // invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 422 */
/* iget-wide v2, v0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J */
/* iget-wide v12, v0, Lcom/android/server/am/MemUsageBuilder;->cachedPss:J */
/* sub-long/2addr v2, v12 */
/* add-long/2addr v2, v8 */
com.android.server.am.ActivityManagerService .stringifyKBSize ( v2,v3 );
(( java.lang.StringBuilder ) v7 ).append ( v2 ); // invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 424 */
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 428 */
final String v2 = " Lost RAM: "; // const-string v2, " Lost RAM: "
(( java.lang.StringBuilder ) v7 ).append ( v2 ); // invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 429 */
/* invoke-virtual/range {v25 ..v25}, Lcom/android/internal/util/MemInfoReader;->getTotalSizeKb()J */
/* move-result-wide v2 */
/* iget-wide v12, v0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J */
/* iget-wide v14, v0, Lcom/android/server/am/MemUsageBuilder;->totalSwapPss:J */
/* sub-long/2addr v12, v14 */
/* sub-long/2addr v2, v12 */
/* .line 430 */
/* invoke-virtual/range {v25 ..v25}, Lcom/android/internal/util/MemInfoReader;->getFreeSizeKb()J */
/* move-result-wide v12 */
/* sub-long/2addr v2, v12 */
/* invoke-virtual/range {v25 ..v25}, Lcom/android/internal/util/MemInfoReader;->getCachedSizeKb()J */
/* move-result-wide v12 */
/* sub-long/2addr v2, v12 */
/* sub-long/2addr v2, v8 */
/* .line 431 */
/* invoke-virtual/range {v25 ..v25}, Lcom/android/internal/util/MemInfoReader;->getZramTotalSizeKb()J */
/* move-result-wide v12 */
/* sub-long/2addr v2, v12 */
/* .line 429 */
com.android.server.am.ActivityManagerService .stringifyKBSize ( v2,v3 );
(( java.lang.StringBuilder ) v7 ).append ( v2 ); // invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 432 */
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 434 */
/* move-wide v2, v8 */
/* .line 435 */
/* .local v2, "finalKernelUsed":J */
/* new-instance v5, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda2; */
/* move-object/from16 v12, v25 */
} // .end local v25 # "memInfo":Lcom/android/internal/util/MemInfoReader;
/* .local v12, "memInfo":Lcom/android/internal/util/MemInfoReader; */
/* invoke-direct {v5, v12}, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda2;-><init>(Lcom/android/internal/util/MemInfoReader;)V */
(( java.util.Optional ) v1 ).ifPresent ( v5 ); // invoke-virtual {v1, v5}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V
/* .line 436 */
/* new-instance v5, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda3; */
/* invoke-direct {v5, v2, v3}, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda3;-><init>(J)V */
(( java.util.Optional ) v1 ).ifPresent ( v5 ); // invoke-virtual {v1, v5}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V
/* .line 437 */
v5 = /* invoke-virtual/range {v26 ..v26}, Ljava/lang/Boolean;->booleanValue()Z */
if ( v5 != null) { // if-eqz v5, :cond_b
/* .line 438 */
com.miui.server.stability.ScoutDisplayMemoryManager .getInstance ( );
(( com.miui.server.stability.ScoutDisplayMemoryManager ) v5 ).getDmabufUsageInfo ( ); // invoke-virtual {v5}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->getDmabufUsageInfo()Ljava/lang/String;
/* .line 439 */
/* .local v5, "dmabufInfo":Ljava/lang/String; */
if ( v5 != null) { // if-eqz v5, :cond_b
/* .line 440 */
final String v13 = "\n\nDMABUF usage Info:\n"; // const-string v13, "\n\nDMABUF usage Info:\n"
(( java.lang.StringBuilder ) v7 ).append ( v13 ); // invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 441 */
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 442 */
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 445 */
} // .end local v5 # "dmabufInfo":Ljava/lang/String;
} // :cond_b
v5 = (( java.lang.Boolean ) v6 ).booleanValue ( ); // invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z
if ( v5 != null) { // if-eqz v5, :cond_c
/* .line 446 */
com.miui.server.stability.ScoutDisplayMemoryManager .getInstance ( );
(( com.miui.server.stability.ScoutDisplayMemoryManager ) v5 ).getGpuMemoryUsageInfo ( ); // invoke-virtual {v5}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->getGpuMemoryUsageInfo()Ljava/lang/String;
/* .line 447 */
/* .local v5, "gpuInfo":Ljava/lang/String; */
if ( v5 != null) { // if-eqz v5, :cond_c
/* .line 448 */
final String v13 = "GPU usage Info:\n"; // const-string v13, "GPU usage Info:\n"
(( java.lang.StringBuilder ) v7 ).append ( v13 ); // invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 449 */
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 450 */
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 453 */
} // .end local v5 # "gpuInfo":Ljava/lang/String;
} // :cond_c
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
java.lang.String buildTopProcs ( ) {
/* .locals 9 */
/* .line 457 */
v0 = this.memInfos;
/* new-instance v1, Lcom/android/server/am/MemUsageBuilder$2; */
/* invoke-direct {v1, p0}, Lcom/android/server/am/MemUsageBuilder$2;-><init>(Lcom/android/server/am/MemUsageBuilder;)V */
java.util.Collections .sort ( v0,v1 );
/* .line 465 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* const/16 v1, 0x80 */
/* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V */
/* .line 466 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Number of processes: "; // const-string v2, "Number of processes: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.memInfos;
v2 = (( java.util.ArrayList ) v2 ).size ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->size()I
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "\n"; // const-string v2, "\n"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 467 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
v3 = this.memInfos;
v3 = (( java.util.ArrayList ) v3 ).size ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
/* .local v3, "size":I */
} // :goto_0
/* if-ge v1, v3, :cond_0 */
/* const/16 v4, 0xa */
/* if-ge v1, v4, :cond_0 */
/* .line 468 */
v4 = this.memInfos;
(( java.util.ArrayList ) v4 ).get ( v1 ); // invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v4, Lcom/android/server/am/ProcessMemInfo; */
/* .line 469 */
/* .local v4, "mi":Lcom/android/server/am/ProcessMemInfo; */
/* new-instance v5, Landroid/os/Debug$MemoryInfo; */
/* invoke-direct {v5}, Landroid/os/Debug$MemoryInfo;-><init>()V */
/* .line 470 */
/* .local v5, "info":Landroid/os/Debug$MemoryInfo; */
/* iget v6, v4, Lcom/android/server/am/ProcessMemInfo;->pid:I */
android.os.Debug .getMemoryInfo ( v6,v5 );
/* .line 471 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "TOP "; // const-string v7, "TOP "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* add-int/lit8 v7, v1, 0x1 */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 472 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = " Pid:"; // const-string v7, " Pid:"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v4, Lcom/android/server/am/ProcessMemInfo;->pid:I */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 473 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = " Process:"; // const-string v7, " Process:"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.name;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 474 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = " Pss:"; // const-string v7, " Pss:"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v7, v4, Lcom/android/server/am/ProcessMemInfo;->pss:J */
(( java.lang.StringBuilder ) v6 ).append ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 475 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = " Java Heap Pss:"; // const-string v7, " Java Heap Pss:"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v5, Landroid/os/Debug$MemoryInfo;->dalvikPss:I */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 476 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = " Native Heap Pss:"; // const-string v7, " Native Heap Pss:"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v5, Landroid/os/Debug$MemoryInfo;->nativePss:I */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 477 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = " Graphics Pss:"; // const-string v7, " Graphics Pss:"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = (( android.os.Debug$MemoryInfo ) v5 ).getSummaryGraphics ( ); // invoke-virtual {v5}, Landroid/os/Debug$MemoryInfo;->getSummaryGraphics()I
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 478 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 467 */
} // .end local v4 # "mi":Lcom/android/server/am/ProcessMemInfo;
} // .end local v5 # "info":Landroid/os/Debug$MemoryInfo;
/* add-int/lit8 v1, v1, 0x1 */
/* goto/16 :goto_0 */
/* .line 480 */
} // .end local v1 # "i":I
} // .end local v3 # "size":I
} // :cond_0
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
void setScoutInfo ( com.android.server.am.ScoutMeminfo p0 ) {
/* .locals 0 */
/* .param p1, "scoutInfo" # Lcom/android/server/am/ScoutMeminfo; */
/* .line 76 */
this.scoutInfo = p1;
/* .line 77 */
return;
} // .end method
