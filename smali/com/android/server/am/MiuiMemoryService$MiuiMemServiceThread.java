class com.android.server.am.MiuiMemoryService$MiuiMemServiceThread extends java.lang.Thread {
	 /* .source "MiuiMemoryService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/MiuiMemoryService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "MiuiMemServiceThread" */
} // .end annotation
/* # static fields */
public static final java.lang.String HOST_NAME;
/* # instance fields */
final com.android.server.am.MiuiMemoryService this$0; //synthetic
/* # direct methods */
private com.android.server.am.MiuiMemoryService$MiuiMemServiceThread ( ) {
/* .locals 0 */
/* .line 65 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Thread;-><init>()V */
return;
} // .end method
 com.android.server.am.MiuiMemoryService$MiuiMemServiceThread ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/MiuiMemoryService$MiuiMemServiceThread;-><init>(Lcom/android/server/am/MiuiMemoryService;)V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 7 */
/* .line 70 */
final String v0 = "mi_reclaim connection finally shutdown!"; // const-string v0, "mi_reclaim connection finally shutdown!"
final String v1 = "MiuiMemoryService"; // const-string v1, "MiuiMemoryService"
int v2 = 0; // const/4 v2, 0x0
/* .line 71 */
/* .local v2, "serverSocket":Landroid/net/LocalServerSocket; */
java.util.concurrent.Executors .newCachedThreadPool ( );
/* .line 74 */
/* .local v3, "threadExecutor":Ljava/util/concurrent/ExecutorService; */
try { // :try_start_0
	 /* sget-boolean v4, Lcom/android/server/am/MiuiMemoryService;->DEBUG:Z */
	 if ( v4 != null) { // if-eqz v4, :cond_0
		 final String v4 = "Create local socket: mi_reclaim"; // const-string v4, "Create local socket: mi_reclaim"
		 android.util.Slog .d ( v1,v4 );
		 /* .line 76 */
	 } // :cond_0
	 /* new-instance v4, Landroid/net/LocalServerSocket; */
	 final String v5 = "mi_reclaim"; // const-string v5, "mi_reclaim"
	 /* invoke-direct {v4, v5}, Landroid/net/LocalServerSocket;-><init>(Ljava/lang/String;)V */
	 /* move-object v2, v4 */
	 /* .line 79 */
} // :goto_0
/* sget-boolean v4, Lcom/android/server/am/MiuiMemoryService;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_1
	 final String v4 = "Waiting Client connected..."; // const-string v4, "Waiting Client connected..."
	 android.util.Slog .d ( v1,v4 );
	 /* .line 81 */
} // :cond_1
(( android.net.LocalServerSocket ) v2 ).accept ( ); // invoke-virtual {v2}, Landroid/net/LocalServerSocket;->accept()Landroid/net/LocalSocket;
/* .line 82 */
/* .local v4, "socket":Landroid/net/LocalSocket; */
/* const/16 v5, 0x100 */
(( android.net.LocalSocket ) v4 ).setReceiveBufferSize ( v5 ); // invoke-virtual {v4, v5}, Landroid/net/LocalSocket;->setReceiveBufferSize(I)V
/* .line 83 */
(( android.net.LocalSocket ) v4 ).setSendBufferSize ( v5 ); // invoke-virtual {v4, v5}, Landroid/net/LocalSocket;->setSendBufferSize(I)V
/* .line 84 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "There is a client is accepted: "; // const-string v6, "There is a client is accepted: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.net.LocalSocket ) v4 ).toString ( ); // invoke-virtual {v4}, Landroid/net/LocalSocket;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v5 );
/* .line 85 */
/* new-instance v5, Lcom/android/server/am/MiuiMemoryService$ConnectionHandler; */
v6 = this.this$0;
/* invoke-direct {v5, v6, v4}, Lcom/android/server/am/MiuiMemoryService$ConnectionHandler;-><init>(Lcom/android/server/am/MiuiMemoryService;Landroid/net/LocalSocket;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 86 */
} // .end local v4 # "socket":Landroid/net/LocalSocket;
/* .line 90 */
/* :catchall_0 */
/* move-exception v4 */
/* .line 87 */
/* :catch_0 */
/* move-exception v4 */
/* .line 88 */
/* .local v4, "e":Ljava/lang/Exception; */
try { // :try_start_1
final String v5 = "mi_reclaim connection catch Exception"; // const-string v5, "mi_reclaim connection catch Exception"
android.util.Slog .e ( v1,v5 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 90 */
/* nop */
} // .end local v4 # "e":Ljava/lang/Exception;
android.util.Slog .w ( v1,v0 );
/* .line 91 */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 92 */
} // :cond_2
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 94 */
try { // :try_start_2
	 (( android.net.LocalServerSocket ) v2 ).close ( ); // invoke-virtual {v2}, Landroid/net/LocalServerSocket;->close()V
	 /* :try_end_2 */
	 /* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_1 */
	 /* .line 97 */
} // :goto_1
/* .line 95 */
/* :catch_1 */
/* move-exception v0 */
/* .line 96 */
/* .local v0, "e":Ljava/io/IOException; */
(( java.io.IOException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
} // .end local v0 # "e":Ljava/io/IOException;
/* .line 100 */
} // :cond_3
} // :goto_2
/* sget-boolean v0, Lcom/android/server/am/MiuiMemoryService;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_4
final String v0 = "mi_reclaim connection ended!"; // const-string v0, "mi_reclaim connection ended!"
android.util.Slog .d ( v1,v0 );
/* .line 101 */
} // :cond_4
return;
/* .line 90 */
} // :goto_3
android.util.Slog .w ( v1,v0 );
/* .line 91 */
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 92 */
} // :cond_5
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 94 */
try { // :try_start_3
(( android.net.LocalServerSocket ) v2 ).close ( ); // invoke-virtual {v2}, Landroid/net/LocalServerSocket;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_2 */
/* .line 97 */
/* .line 95 */
/* :catch_2 */
/* move-exception v0 */
/* .line 96 */
/* .restart local v0 # "e":Ljava/io/IOException; */
(( java.io.IOException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
/* .line 99 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :cond_6
} // :goto_4
/* throw v4 */
} // .end method
