.class public final Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;
.super Ljava/lang/Object;
.source "ProcessProphetModel.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ProcessProphetModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BlueToothUsageModel"
.end annotation


# instance fields
.field curUseInfoMapList:Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

.field private dataList:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue<",
            "Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 2

    .line 1074
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1072
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;->dataList:Ljava/util/Queue;

    .line 1073
    new-instance v0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    invoke-direct {v0}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;->curUseInfoMapList:Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    .line 1075
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;->dataList:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 1076
    return-void
.end method


# virtual methods
.method public conclude()Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;
    .locals 6

    .line 1096
    new-instance v0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    invoke-direct {v0}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;-><init>()V

    .line 1097
    .local v0, "mBTProbTab":Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;->dataList:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    .line 1098
    .local v2, "u":Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;
    iget-object v3, v2, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->myMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1099
    .local v4, "pkgName":Ljava/lang/String;
    iget-object v5, v2, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->myMap:Ljava/util/HashMap;

    invoke-virtual {v5, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;

    invoke-virtual {v0, v5}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->update(Lcom/android/server/am/ProcessProphetModel$PkgValuePair;)V

    .line 1100
    .end local v4    # "pkgName":Ljava/lang/String;
    goto :goto_1

    .line 1101
    .end local v2    # "u":Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;
    :cond_0
    goto :goto_0

    .line 1102
    :cond_1
    invoke-static {v0}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->-$$Nest$mupdateList(Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;)Ljava/util/ArrayList;

    .line 1103
    return-object v0
.end method

.method public dump()V
    .locals 5

    .line 1106
    const-string v0, "dumping BlueTooth Usage Model"

    const-string v1, "ProcessProphetModel"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1107
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;->dataList:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    .line 1108
    .local v2, "u":Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\t"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1109
    .end local v2    # "u":Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;
    goto :goto_0

    .line 1110
    :cond_0
    return-void
.end method

.method public getAllBTUpdateTimes()J
    .locals 6

    .line 1119
    const-wide/16 v0, 0x0

    .line 1120
    .local v0, "allBTUpdateTimes":J
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;->dataList:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    .line 1121
    .local v3, "uiml":Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;
    invoke-virtual {v3}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->getUseInfoMapTimes()J

    move-result-wide v4

    add-long/2addr v0, v4

    .line 1122
    .end local v3    # "uiml":Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;
    goto :goto_0

    .line 1123
    :cond_0
    return-wide v0
.end method

.method public getBTConnectedTimes()J
    .locals 2

    .line 1114
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;->dataList:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public notifyBTConnected()V
    .locals 2

    .line 1086
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;->curUseInfoMapList:Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->getMapSize()I

    move-result v0

    if-lez v0, :cond_1

    .line 1087
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;->dataList:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    const/16 v1, 0x14

    if-lt v0, v1, :cond_0

    .line 1088
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;->dataList:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    .line 1090
    :cond_0
    new-instance v0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    invoke-direct {v0}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;->curUseInfoMapList:Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    .line 1091
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;->dataList:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 1093
    :cond_1
    return-void
.end method

.method public update(Ljava/lang/String;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1079
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;->curUseInfoMapList:Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    const-wide/high16 v1, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, p1, v1, v2}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->update(Ljava/lang/String;D)V

    .line 1080
    return-void
.end method

.method public update(Ljava/lang/String;I)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "useTime"    # I

    .line 1082
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;->curUseInfoMapList:Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    int-to-double v1, p2

    invoke-virtual {v0, p1, v1, v2}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->update(Ljava/lang/String;D)V

    .line 1083
    return-void
.end method
