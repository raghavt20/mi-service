.class public Lcom/android/server/am/PendingIntentRecordImpl;
.super Ljava/lang/Object;
.source "PendingIntentRecordImpl.java"

# interfaces
.implements Lcom/android/server/am/PendingIntentRecordStub;


# static fields
.field private static final TAG:Ljava/lang/String; = "PendingIntentRecordImpl"

.field private static final sClearPendingCallback:Ljava/lang/Runnable;

.field private static final sDefaultClearTime:I = 0x1388

.field private static final sIgnorePackages:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sLock:Ljava/lang/Object;

.field private static final sMessageHandler:Landroid/os/Handler;

.field private static final sPendingPackages:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mIGreezeManager:Lmiui/greeze/IGreezeManager;

.field private mLauncherApp:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSystemUIApp:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 50
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    sput-object v0, Lcom/android/server/am/PendingIntentRecordImpl;->sIgnorePackages:Landroid/util/ArraySet;

    .line 52
    const-string v1, "com.android.systemui"

    invoke-virtual {v0, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 53
    const-string v1, "com.miui.notification"

    invoke-virtual {v0, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 54
    const-string v1, "com.android.keyguard"

    invoke-virtual {v0, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 55
    const-string v1, "com.miui.home"

    invoke-virtual {v0, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 56
    const-string v1, "com.mi.android.globallauncher"

    invoke-virtual {v0, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 57
    const-string v1, "com.xiaomi.xmsf"

    invoke-virtual {v0, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 58
    const-string v1, "com.google.android.wearable.app.cn"

    invoke-virtual {v0, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 59
    const-string v1, "com.xiaomi.mirror"

    invoke-virtual {v0, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 60
    const-string v1, "com.miui.personalassistant"

    invoke-virtual {v0, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 61
    const-string v1, "com.miui.securitycenter"

    invoke-virtual {v0, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 68
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/server/am/PendingIntentRecordImpl;->sLock:Ljava/lang/Object;

    .line 69
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    sput-object v0, Lcom/android/server/am/PendingIntentRecordImpl;->sPendingPackages:Ljava/util/Set;

    .line 70
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    sput-object v0, Lcom/android/server/am/PendingIntentRecordImpl;->sMessageHandler:Landroid/os/Handler;

    .line 72
    new-instance v0, Lcom/android/server/am/PendingIntentRecordImpl$$ExternalSyntheticLambda0;

    invoke-direct {v0}, Lcom/android/server/am/PendingIntentRecordImpl$$ExternalSyntheticLambda0;-><init>()V

    sput-object v0, Lcom/android/server/am/PendingIntentRecordImpl;->sClearPendingCallback:Ljava/lang/Runnable;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/am/PendingIntentRecordImpl;->mIGreezeManager:Lmiui/greeze/IGreezeManager;

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    const-string v1, "com.android.systemui"

    const-string v2, "com.miui.notification"

    filled-new-array {v1, v2}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/android/server/am/PendingIntentRecordImpl;->mSystemUIApp:Ljava/util/List;

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    const-string v1, "com.miui.home"

    const-string v2, "com.mi.android.globallauncher"

    filled-new-array {v1, v2}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/android/server/am/PendingIntentRecordImpl;->mLauncherApp:Ljava/util/List;

    return-void
.end method

.method public static containsPendingIntent(Ljava/lang/String;)Z
    .locals 2
    .param p0, "packageName"    # Ljava/lang/String;

    .line 200
    sget-object v0, Lcom/android/server/am/PendingIntentRecordImpl;->sLock:Ljava/lang/Object;

    monitor-enter v0

    .line 201
    :try_start_0
    sget-object v1, Lcom/android/server/am/PendingIntentRecordImpl;->sPendingPackages:Ljava/util/Set;

    invoke-interface {v1, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    monitor-exit v0

    return v1

    .line 202
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static exemptTemporarily(Ljava/lang/String;Z)V
    .locals 5
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "ignoreSource"    # Z

    .line 177
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    return-void

    .line 181
    :cond_0
    :try_start_0
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    .line 182
    .local v0, "callingPid":I
    if-nez p1, :cond_1

    .line 183
    invoke-static {v0}, Lcom/android/server/am/ProcessUtils;->getPackageNameByPid(I)Ljava/lang/String;

    move-result-object v1

    .line 184
    .local v1, "pkg":Ljava/lang/String;
    sget-object v2, Lcom/android/server/am/PendingIntentRecordImpl;->sIgnorePackages:Landroid/util/ArraySet;

    invoke-virtual {v2, v1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 185
    return-void

    .line 188
    .end local v1    # "pkg":Ljava/lang/String;
    :cond_1
    sget-object v1, Lcom/android/server/am/PendingIntentRecordImpl;->sLock:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    :try_start_1
    sget-object v2, Lcom/android/server/am/PendingIntentRecordImpl;->sPendingPackages:Ljava/util/Set;

    invoke-interface {v2, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 190
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 191
    :try_start_2
    const-class v1, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-interface {v1, p0}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->onPendingPackagesExempt(Ljava/lang/String;)V

    .line 192
    sget-object v1, Lcom/android/server/am/PendingIntentRecordImpl;->sMessageHandler:Landroid/os/Handler;

    sget-object v2, Lcom/android/server/am/PendingIntentRecordImpl;->sClearPendingCallback:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 193
    const-wide/16 v3, 0x1388

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 196
    nop

    .end local v0    # "callingPid":I
    goto :goto_0

    .line 190
    .restart local v0    # "callingPid":I
    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .end local p0    # "packageName":Ljava/lang/String;
    .end local p1    # "ignoreSource":Z
    :try_start_4
    throw v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 194
    .end local v0    # "callingPid":I
    .restart local p0    # "packageName":Ljava/lang/String;
    .restart local p1    # "ignoreSource":Z
    :catch_0
    move-exception v0

    .line 195
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "PendingIntentRecordImpl"

    const-string v2, "exempt temporarily error!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 197
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private getGreeze()Lmiui/greeze/IGreezeManager;
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/android/server/am/PendingIntentRecordImpl;->mIGreezeManager:Lmiui/greeze/IGreezeManager;

    if-nez v0, :cond_0

    .line 86
    nop

    .line 87
    const-string v0, "greezer"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 86
    invoke-static {v0}, Lmiui/greeze/IGreezeManager$Stub;->asInterface(Landroid/os/IBinder;)Lmiui/greeze/IGreezeManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/PendingIntentRecordImpl;->mIGreezeManager:Lmiui/greeze/IGreezeManager;

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/PendingIntentRecordImpl;->mIGreezeManager:Lmiui/greeze/IGreezeManager;

    return-object v0
.end method

.method public static getInstance()Lcom/android/server/am/PendingIntentRecordImpl;
    .locals 1

    .line 47
    invoke-static {}, Lcom/android/server/am/PendingIntentRecordStub;->get()Lcom/android/server/am/PendingIntentRecordStub;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/PendingIntentRecordImpl;

    return-object v0
.end method

.method private static getTargetPkg(Lcom/android/server/am/PendingIntentRecord$Key;Landroid/content/Intent;)Ljava/lang/String;
    .locals 9
    .param p0, "key"    # Lcom/android/server/am/PendingIntentRecord$Key;
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 206
    invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v0

    .line 207
    .local v0, "targetPkg":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 208
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    .line 209
    .local v1, "component":Landroid/content/ComponentName;
    if-eqz v1, :cond_0

    .line 210
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 214
    .end local v1    # "component":Landroid/content/ComponentName;
    :cond_0
    if-nez v0, :cond_5

    .line 215
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v7

    .line 216
    .local v7, "pm":Landroid/content/pm/IPackageManager;
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v8

    .line 217
    .local v8, "userId":I
    iget v1, p0, Lcom/android/server/am/PendingIntentRecord$Key;->type:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    .line 218
    const/4 v3, 0x0

    const-wide/16 v4, 0x400

    move-object v1, v7

    move-object v2, p1

    move v6, v8

    invoke-interface/range {v1 .. v6}, Landroid/content/pm/IPackageManager;->resolveService(Landroid/content/Intent;Ljava/lang/String;JI)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    .line 220
    .local v1, "resolveIntent":Landroid/content/pm/ResolveInfo;
    if-eqz v1, :cond_1

    iget-object v2, v1, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    if-eqz v2, :cond_1

    .line 221
    iget-object v2, v1, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v0, v2, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    .line 223
    .end local v1    # "resolveIntent":Landroid/content/pm/ResolveInfo;
    :cond_1
    goto :goto_0

    .line 224
    :cond_2
    const/4 v3, 0x0

    const-wide/16 v4, 0x400

    move-object v1, v7

    move-object v2, p1

    move v6, v8

    invoke-interface/range {v1 .. v6}, Landroid/content/pm/IPackageManager;->queryIntentReceivers(Landroid/content/Intent;Ljava/lang/String;JI)Landroid/content/pm/ParceledListSlice;

    move-result-object v1

    .line 226
    .local v1, "qeury":Ljava/lang/Object;
    if-nez v1, :cond_3

    .line 227
    const/4 v2, 0x0

    return-object v2

    .line 229
    :cond_3
    const/4 v2, 0x0

    .line 230
    .local v2, "receivers":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    move-object v3, v1

    check-cast v3, Landroid/content/pm/ParceledListSlice;

    .line 231
    .local v3, "parceledList":Landroid/content/pm/ParceledListSlice;, "Landroid/content/pm/ParceledListSlice<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v3, :cond_4

    .line 232
    invoke-virtual {v3}, Landroid/content/pm/ParceledListSlice;->getList()Ljava/util/List;

    move-result-object v2

    .line 234
    :cond_4
    if-eqz v2, :cond_5

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_5

    .line 235
    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    .line 236
    .local v4, "resolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v5, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v5, :cond_5

    .line 237
    iget-object v5, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v5, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 242
    .end local v1    # "qeury":Ljava/lang/Object;
    .end local v2    # "receivers":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v3    # "parceledList":Landroid/content/pm/ParceledListSlice;, "Landroid/content/pm/ParceledListSlice<Landroid/content/pm/ResolveInfo;>;"
    .end local v4    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    .end local v7    # "pm":Landroid/content/pm/IPackageManager;
    .end local v8    # "userId":I
    :cond_5
    :goto_0
    if-nez v0, :cond_6

    .line 243
    iget-object v0, p0, Lcom/android/server/am/PendingIntentRecord$Key;->packageName:Ljava/lang/String;

    .line 245
    :cond_6
    return-object v0
.end method

.method private getUidByPackageName(Ljava/lang/String;)I
    .locals 4
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 109
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v0

    .line 111
    .local v0, "pm":Landroid/content/pm/IPackageManager;
    const-wide/16 v1, 0x0

    const/4 v3, 0x0

    :try_start_0
    invoke-interface {v0, p1, v1, v2, v3}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;JI)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 112
    .local v1, "appinfo":Landroid/content/pm/ApplicationInfo;
    if-eqz v1, :cond_0

    .line 113
    iget v2, v1, Landroid/content/pm/ApplicationInfo;->uid:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v2

    .line 116
    .end local v1    # "appinfo":Landroid/content/pm/ApplicationInfo;
    :cond_0
    goto :goto_0

    .line 114
    :catch_0
    move-exception v1

    .line 115
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "not find packageName :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "PendingIntentRecordImpl"

    invoke-static {v3, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    const/4 v1, -0x1

    return v1
.end method

.method static synthetic lambda$static$0()V
    .locals 2

    .line 73
    sget-object v0, Lcom/android/server/am/PendingIntentRecordImpl;->sLock:Ljava/lang/Object;

    monitor-enter v0

    .line 74
    :try_start_0
    sget-object v1, Lcom/android/server/am/PendingIntentRecordImpl;->sPendingPackages:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 75
    monitor-exit v0

    .line 76
    return-void

    .line 75
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private notifyGreeze(Ljava/lang/String;Ljava/lang/String;Lcom/android/server/am/PendingIntentRecord$Key;)V
    .locals 6
    .param p1, "targetPkg"    # Ljava/lang/String;
    .param p2, "pkg"    # Ljava/lang/String;
    .param p3, "key"    # Lcom/android/server/am/PendingIntentRecord$Key;

    .line 91
    iget-object v0, p0, Lcom/android/server/am/PendingIntentRecordImpl;->mSystemUIApp:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/am/PendingIntentRecordImpl;->mLauncherApp:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p3, Lcom/android/server/am/PendingIntentRecord$Key;->type:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    .line 93
    :cond_0
    invoke-direct {p0}, Lcom/android/server/am/PendingIntentRecordImpl;->getGreeze()Lmiui/greeze/IGreezeManager;

    move-result-object v0

    if-nez v0, :cond_1

    .line 94
    return-void

    .line 95
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "notifyGreeze pendingtent from "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " -> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PendingIntentRecordImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/server/am/PendingIntentRecordImpl;->getUidByPackageName(Ljava/lang/String;)I

    move-result v0

    .line 98
    .local v0, "uid":I
    invoke-direct {p0}, Lcom/android/server/am/PendingIntentRecordImpl;->getGreeze()Lmiui/greeze/IGreezeManager;

    move-result-object v2

    invoke-interface {v2, v0}, Lmiui/greeze/IGreezeManager;->isUidFrozen(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 99
    const/4 v2, 0x1

    new-array v2, v2, [I

    .line 100
    .local v2, "uids":[I
    const/4 v3, 0x0

    aput v0, v2, v3

    .line 101
    invoke-direct {p0}, Lcom/android/server/am/PendingIntentRecordImpl;->getGreeze()Lmiui/greeze/IGreezeManager;

    move-result-object v3

    const-string v4, "notifi"

    const/16 v5, 0x3e8

    invoke-interface {v3, v2, v5, v4}, Lmiui/greeze/IGreezeManager;->thawUids([IILjava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    .end local v0    # "uid":I
    .end local v2    # "uids":[I
    :cond_2
    goto :goto_0

    .line 103
    :catch_0
    move-exception v0

    .line 104
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "notifyGreeze error"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    :goto_0
    return-void
.end method


# virtual methods
.method public checkRunningCompatibility(Landroid/content/Intent;Ljava/lang/String;III)Z
    .locals 6
    .param p1, "service"    # Landroid/content/Intent;
    .param p2, "resolvedType"    # Ljava/lang/String;
    .param p3, "callingUid"    # I
    .param p4, "callingPid"    # I
    .param p5, "userId"    # I

    .line 251
    invoke-static {}, Lcom/android/server/am/ActivityManagerServiceStub;->get()Lcom/android/server/am/ActivityManagerServiceStub;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/android/server/am/ActivityManagerServiceStub;->checkRunningCompatibility(Landroid/content/Intent;Ljava/lang/String;III)Z

    move-result v0

    return v0
.end method

.method public exemptTemporarily(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 173
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/android/server/am/PendingIntentRecordImpl;->exemptTemporarily(Ljava/lang/String;Z)V

    .line 174
    return-void
.end method

.method public preSendPendingIntentInner(Lcom/android/server/am/PendingIntentRecord$Key;Landroid/content/Intent;)V
    .locals 12
    .param p1, "key"    # Lcom/android/server/am/PendingIntentRecord$Key;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 123
    if-eqz p2, :cond_7

    if-nez p1, :cond_0

    goto/16 :goto_5

    .line 127
    :cond_0
    :try_start_0
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    .line 128
    .local v0, "callingPid":I
    invoke-static {v0}, Lcom/android/server/am/ProcessUtils;->getPackageNameByPid(I)Ljava/lang/String;

    move-result-object v1

    .line 129
    .local v1, "pkg":Ljava/lang/String;
    invoke-static {p1, p2}, Lcom/android/server/am/PendingIntentRecordImpl;->getTargetPkg(Lcom/android/server/am/PendingIntentRecord$Key;Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    .line 131
    .local v2, "targetPkg":Ljava/lang/String;
    if-eqz v1, :cond_1

    const-string v3, "android"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 132
    invoke-direct {p0, v2, v1, p1}, Lcom/android/server/am/PendingIntentRecordImpl;->notifyGreeze(Ljava/lang/String;Ljava/lang/String;Lcom/android/server/am/PendingIntentRecord$Key;)V

    .line 134
    :cond_1
    sget-object v3, Lcom/android/server/am/PendingIntentRecordImpl;->sIgnorePackages:Landroid/util/ArraySet;

    invoke-virtual {v3, v1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 135
    return-void

    .line 137
    :cond_2
    const-string v3, "com.miui.home"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    const-string v3, "com.mi.android.globallauncher"

    .line 138
    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    goto :goto_1

    :cond_4
    :goto_0
    const/4 v3, 0x1

    :goto_1
    move v10, v3

    .line 139
    .local v10, "isHome":Z
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 140
    return-void

    .line 144
    :cond_5
    iget v3, p1, Lcom/android/server/am/PendingIntentRecord$Key;->type:I

    packed-switch v3, :pswitch_data_0

    .line 155
    :pswitch_0
    return-void

    .line 152
    :pswitch_1
    const/16 v3, 0x8

    .line 153
    .local v3, "wakeType":I
    move v11, v3

    goto :goto_2

    .line 146
    .end local v3    # "wakeType":I
    :pswitch_2
    const/4 v3, 0x1

    .line 147
    .restart local v3    # "wakeType":I
    move v11, v3

    goto :goto_2

    .line 149
    .end local v3    # "wakeType":I
    :pswitch_3
    const/4 v3, 0x2

    .line 150
    .restart local v3    # "wakeType":I
    move v11, v3

    .line 157
    .end local v3    # "wakeType":I
    .local v11, "wakeType":I
    :goto_2
    const-class v3, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-static {v3}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-interface {v3, p1, v1, v2, p2}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->onSendPendingIntent(Lcom/android/server/am/PendingIntentRecord$Key;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    .line 158
    invoke-static {}, Lmiui/security/WakePathChecker;->getInstance()Lmiui/security/WakePathChecker;

    move-result-object v3

    if-eqz v10, :cond_6

    const-string v4, "appwidget"

    goto :goto_3

    :cond_6
    const-string v4, "notification"

    .line 159
    :goto_3
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v7

    iget v8, p1, Lcom/android/server/am/PendingIntentRecord$Key;->userId:I

    const/4 v9, 0x1

    .line 158
    move-object v5, v2

    move v6, v11

    invoke-virtual/range {v3 .. v9}, Lmiui/security/WakePathChecker;->recordWakePathCall(Ljava/lang/String;Ljava/lang/String;IIIZ)V

    .line 161
    sget-object v3, Lcom/android/server/am/PendingIntentRecordImpl;->sLock:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    :try_start_1
    sget-object v4, Lcom/android/server/am/PendingIntentRecordImpl;->sPendingPackages:Ljava/util/Set;

    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 163
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 164
    :try_start_2
    sget-object v3, Lcom/android/server/am/PendingIntentRecordImpl;->sMessageHandler:Landroid/os/Handler;

    sget-object v4, Lcom/android/server/am/PendingIntentRecordImpl;->sClearPendingCallback:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 165
    const-wide/16 v5, 0x1388

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 168
    nop

    .end local v0    # "callingPid":I
    .end local v1    # "pkg":Ljava/lang/String;
    .end local v2    # "targetPkg":Ljava/lang/String;
    .end local v10    # "isHome":Z
    .end local v11    # "wakeType":I
    goto :goto_4

    .line 163
    .restart local v0    # "callingPid":I
    .restart local v1    # "pkg":Ljava/lang/String;
    .restart local v2    # "targetPkg":Ljava/lang/String;
    .restart local v10    # "isHome":Z
    .restart local v11    # "wakeType":I
    :catchall_0
    move-exception v4

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .end local p0    # "this":Lcom/android/server/am/PendingIntentRecordImpl;
    .end local p1    # "key":Lcom/android/server/am/PendingIntentRecord$Key;
    .end local p2    # "intent":Landroid/content/Intent;
    :try_start_4
    throw v4
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 166
    .end local v0    # "callingPid":I
    .end local v1    # "pkg":Ljava/lang/String;
    .end local v2    # "targetPkg":Ljava/lang/String;
    .end local v10    # "isHome":Z
    .end local v11    # "wakeType":I
    .restart local p0    # "this":Lcom/android/server/am/PendingIntentRecordImpl;
    .restart local p1    # "key":Lcom/android/server/am/PendingIntentRecord$Key;
    .restart local p2    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 167
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "PendingIntentRecordImpl"

    const-string v2, "preSendInner error"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 169
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_4
    return-void

    .line 124
    :cond_7
    :goto_5
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
