.class Lcom/android/server/am/AppStateManager$ActiveApps;
.super Ljava/lang/Object;
.source "AppStateManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/AppStateManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActiveApps"
.end annotation


# instance fields
.field private final mActiveApps:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/android/server/am/AppStateManager$AppState;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/server/am/AppStateManager;


# direct methods
.method constructor <init>(Lcom/android/server/am/AppStateManager;)V
    .locals 0

    .line 811
    iput-object p1, p0, Lcom/android/server/am/AppStateManager$ActiveApps;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 809
    new-instance p1, Landroid/util/SparseArray;

    invoke-direct {p1}, Landroid/util/SparseArray;-><init>()V

    iput-object p1, p0, Lcom/android/server/am/AppStateManager$ActiveApps;->mActiveApps:Landroid/util/SparseArray;

    .line 811
    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .line 830
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$ActiveApps;->mActiveApps:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 831
    return-void
.end method

.method dump(Ljava/io/PrintWriter;[Ljava/lang/String;II)V
    .locals 3
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "args"    # [Ljava/lang/String;
    .param p3, "opti"    # I
    .param p4, "dpUid"    # I

    .line 855
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$ActiveApps;->mActiveApps:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 856
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$ActiveApps;->mActiveApps:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    .line 857
    .local v1, "uid":I
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$ActiveApps;->mActiveApps:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/AppStateManager$AppState;

    .line 858
    .local v2, "app":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz p4, :cond_0

    .line 859
    if-ne v1, p4, :cond_1

    .line 860
    invoke-virtual {v2, p1, p2, p3}, Lcom/android/server/am/AppStateManager$AppState;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V

    goto :goto_1

    .line 863
    :cond_0
    invoke-virtual {v2, p1, p2, p3}, Lcom/android/server/am/AppStateManager$AppState;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V

    .line 855
    .end local v1    # "uid":I
    .end local v2    # "app":Lcom/android/server/am/AppStateManager$AppState;
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 866
    .end local v0    # "i":I
    :cond_2
    return-void
.end method

.method get(I)Lcom/android/server/am/AppStateManager$AppState;
    .locals 1
    .param p1, "uid"    # I

    .line 825
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$ActiveApps;->mActiveApps:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/AppStateManager$AppState;

    return-object v0
.end method

.method indexOfKey(I)I
    .locals 1
    .param p1, "uid"    # I

    .line 850
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$ActiveApps;->mActiveApps:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    return v0
.end method

.method keyAt(I)I
    .locals 1
    .param p1, "index"    # I

    .line 845
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$ActiveApps;->mActiveApps:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    return v0
.end method

.method put(ILcom/android/server/am/AppStateManager$AppState;)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "value"    # Lcom/android/server/am/AppStateManager$AppState;

    .line 815
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$ActiveApps;->mActiveApps:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 816
    return-void
.end method

.method remove(I)V
    .locals 1
    .param p1, "uid"    # I

    .line 820
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$ActiveApps;->mActiveApps:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 821
    return-void
.end method

.method size()I
    .locals 1

    .line 835
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$ActiveApps;->mActiveApps:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    return v0
.end method

.method valueAt(I)Lcom/android/server/am/AppStateManager$AppState;
    .locals 1
    .param p1, "index"    # I

    .line 840
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$ActiveApps;->mActiveApps:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/AppStateManager$AppState;

    return-object v0
.end method
