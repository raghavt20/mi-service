public class com.android.server.am.ActivityManagerServiceImpl extends com.android.server.am.ActivityManagerServiceStub {
	 /* .source "ActivityManagerServiceImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.am.ActivityManagerServiceStub$$" */
} // .end annotation
/* # static fields */
public static final Long BOOST_DURATION;
private static final java.lang.String BOOST_TAG;
private static final java.lang.String CARLINK;
private static final java.lang.String CARLINK_CARLIFE;
private static java.util.List JOB_ANRS;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static final Long KEEP_FOREGROUND_DURATION;
public static final java.lang.String MIUI_APP_TAG;
private static final java.lang.String MIUI_NOTIFICATION;
private static final java.lang.String MIUI_VOICE;
private static final java.lang.String MI_PUSH;
private static final java.lang.String MI_VOICE;
private static final Integer PHONE_CARWITH_CASTING;
private static final java.lang.String PROP_DISABLE_AUTORESTART_APP_PREFIX;
static final Integer PUSH_SERVICE_WHITELIST_TIMEOUT;
public static final Integer SIGNAL_QUIT;
private static final java.lang.String TAG;
private static final java.lang.String UCAR_CASTING_STATE;
private static final java.lang.String WEIXIN;
public static final java.util.List WIDGET_PROVIDER_WHITE_LIST;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.String XIAOMI_BLUETOOTH;
private static final java.lang.String XMSF;
private static java.util.concurrent.atomic.AtomicBoolean dumpFlag;
private static java.util.ArrayList dumpTraceRequestList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.HashSet mIgnoreAuthorityList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.concurrent.atomic.AtomicInteger requestDumpTraceCount;
private static java.util.Map splitDecouplePkgList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private com.miui.server.greeze.GreezeManagerInternal greezer;
com.android.server.am.ActivityManagerService mAmService;
private java.util.ArrayList mBackupingList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
android.content.Context mContext;
private java.util.Map mCurrentSplitIntent;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Landroid/content/Intent;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.xiaomi.vkmode.service.MiuiForceVkServiceInternal mForceVkInternal;
private Integer mInstrUid;
private android.content.Intent mLastSplitIntent;
private android.content.pm.PackageManagerInternal mPackageManager;
private com.android.internal.app.IPerfShielder mPerfService;
private miui.security.SecurityManagerInternal mSecurityInternal;
protected com.miui.app.smartpower.SmartPowerServiceInternal mSmartPowerService;
private java.util.Map mSplitActivityEntryStack;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/util/Stack<", */
/* "Landroid/os/IBinder;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private android.os.Bundle mSplitExtras;
private com.miui.server.stability.StabilityLocalServiceInternal mStabilityLocalServiceInternal;
Boolean mSystemReady;
private java.util.Set mUsingVibratorUids;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.lang.Object splitEntryStackLock;
/* # direct methods */
public static void $r8$lambda$WCheiagbehbr4vA-OB9ebiRDuH4 ( com.android.server.am.ActivityManagerServiceImpl p0, java.lang.String p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/ActivityManagerServiceImpl;->lambda$dumpSystemTraces$0(Ljava/lang/String;)V */
return;
} // .end method
static java.util.concurrent.atomic.AtomicInteger -$$Nest$sfgetrequestDumpTraceCount ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.am.ActivityManagerServiceImpl.requestDumpTraceCount;
} // .end method
static com.android.server.am.ActivityManagerServiceImpl ( ) {
/* .locals 5 */
/* .line 178 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 179 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V */
/* .line 180 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean; */
/* invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V */
/* .line 182 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 207 */
final String v1 = "com.android.calendar"; // const-string v1, "com.android.calendar"
/* .line 209 */
/* new-instance v0, Lcom/android/server/am/ActivityManagerServiceImpl$1; */
/* invoke-direct {v0}, Lcom/android/server/am/ActivityManagerServiceImpl$1;-><init>()V */
/* .line 695 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 699 */
final String v1 = "com.miui.securitycenter.zman.fileProvider"; // const-string v1, "com.miui.securitycenter.zman.fileProvider"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 700 */
final String v1 = "com.xiaomi.misettings.FileProvider"; // const-string v1, "com.xiaomi.misettings.FileProvider"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 701 */
final String v1 = "com.xiaomi.mirror.remoteprovider"; // const-string v1, "com.xiaomi.mirror.remoteprovider"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 702 */
final String v1 = "com.xiaomi.aiasst.service.fileProvider"; // const-string v1, "com.xiaomi.aiasst.service.fileProvider"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 703 */
final String v1 = "com.miui.bugreport.fileprovider"; // const-string v1, "com.miui.bugreport.fileprovider"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 704 */
final String v1 = "com.miui.cleanmaster.fileProvider"; // const-string v1, "com.miui.cleanmaster.fileProvider"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 1253 */
/* new-instance v0, Ljava/util/ArrayList; */
final String v1 = "No response to onStopJob"; // const-string v1, "No response to onStopJob"
final String v2 = "required notification not provided"; // const-string v2, "required notification not provided"
final String v3 = "Timed out while trying to bind"; // const-string v3, "Timed out while trying to bind"
final String v4 = "No response to onStartJob"; // const-string v4, "No response to onStartJob"
/* filled-new-array {v3, v4, v1, v2}, [Ljava/lang/String; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
return;
} // .end method
public com.android.server.am.ActivityManagerServiceImpl ( ) {
/* .locals 1 */
/* .line 151 */
/* invoke-direct {p0}, Lcom/android/server/am/ActivityManagerServiceStub;-><init>()V */
/* .line 189 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mCurrentSplitIntent = v0;
/* .line 193 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.splitEntryStackLock = v0;
/* .line 1288 */
int v0 = 0; // const/4 v0, 0x0
this.greezer = v0;
/* .line 1792 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mBackupingList = v0;
/* .line 1811 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mInstrUid:I */
/* .line 1829 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mUsingVibratorUids = v0;
return;
} // .end method
private Boolean checkServiceWakePath ( android.content.Intent p0, java.lang.String p1, miui.security.CallerInfo p2, Integer p3 ) {
/* .locals 9 */
/* .param p1, "service" # Landroid/content/Intent; */
/* .param p2, "resolvedType" # Ljava/lang/String; */
/* .param p3, "callerInfo" # Lmiui/security/CallerInfo; */
/* .param p4, "userId" # I */
/* .line 474 */
try { // :try_start_0
android.app.AppGlobals .getPackageManager ( );
/* const-wide/16 v3, 0x400 */
/* move-object v1, p1 */
/* move-object v2, p2 */
/* move v5, p4 */
/* invoke-interface/range {v0 ..v5}, Landroid/content/pm/IPackageManager;->resolveService(Landroid/content/Intent;Ljava/lang/String;JI)Landroid/content/pm/ResolveInfo; */
/* .line 476 */
/* .local v0, "rInfo":Landroid/content/pm/ResolveInfo; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = this.serviceInfo;
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 478 */
/* .local v1, "sInfo":Landroid/content/pm/ServiceInfo; */
} // :goto_0
/* const-class v2, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
com.android.server.LocalServices .getService ( v2 );
v2 = /* check-cast v2, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
/* if-nez v2, :cond_2 */
v2 = this.mAmService;
int v4 = 0; // const/4 v4, 0x0
/* const/16 v7, 0x8 */
/* .line 479 */
/* move-object v3, p3 */
/* move-object v5, p1 */
/* move-object v6, v1 */
/* move v8, p4 */
v2 = /* invoke-static/range {v2 ..v8}, Lcom/android/server/am/ActivityManagerServiceImpl;->checkWakePath(Lcom/android/server/am/ActivityManagerService;Lmiui/security/CallerInfo;Ljava/lang/String;Landroid/content/Intent;Landroid/content/pm/ComponentInfo;II)Z */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* if-nez v2, :cond_1 */
/* .line 485 */
} // .end local v0 # "rInfo":Landroid/content/pm/ResolveInfo;
} // .end local v1 # "sInfo":Landroid/content/pm/ServiceInfo;
} // :cond_1
/* .line 481 */
/* .restart local v0 # "rInfo":Landroid/content/pm/ResolveInfo; */
/* .restart local v1 # "sInfo":Landroid/content/pm/ServiceInfo; */
} // :cond_2
} // :goto_1
int v2 = 0; // const/4 v2, 0x0
/* .line 483 */
} // .end local v0 # "rInfo":Landroid/content/pm/ResolveInfo;
} // .end local v1 # "sInfo":Landroid/content/pm/ServiceInfo;
/* :catch_0 */
/* move-exception v0 */
/* .line 486 */
} // :goto_2
int v0 = 1; // const/4 v0, 0x1
} // .end method
private static Boolean checkThawTime ( Integer p0, java.lang.String p1, com.miui.server.greeze.GreezeManagerInternal p2 ) {
/* .locals 11 */
/* .param p0, "uid" # I */
/* .param p1, "report" # Ljava/lang/String; */
/* .param p2, "greezer" # Lcom/miui/server/greeze/GreezeManagerInternal; */
/* .line 1257 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "checkThawTime uid="; // const-string v1, "checkThawTime uid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " report="; // const-string v1, " report="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "ActivityManagerServiceImpl"; // const-string v1, "ActivityManagerServiceImpl"
android.util.Slog .d ( v1,v0 );
/* .line 1258 */
v0 = android.text.TextUtils .isEmpty ( p1 );
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1259 */
/* .line 1261 */
} // :cond_0
/* const/16 v0, 0x2710 */
/* .line 1262 */
/* .local v0, "timeout":I */
final String v3 = "Broadcast of"; // const-string v3, "Broadcast of"
v3 = (( java.lang.String ) p1 ).startsWith ( v3 ); // invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* if-nez v3, :cond_5 */
final String v3 = "executing service"; // const-string v3, "executing service"
v3 = (( java.lang.String ) p1 ).startsWith ( v3 ); // invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* if-nez v3, :cond_5 */
/* .line 1263 */
final String v3 = "ContentProvider not"; // const-string v3, "ContentProvider not"
v3 = (( java.lang.String ) p1 ).startsWith ( v3 ); // invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1265 */
} // :cond_1
final String v3 = "Input dispatching"; // const-string v3, "Input dispatching"
v3 = (( java.lang.String ) p1 ).startsWith ( v3 ); // invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 1266 */
/* const/16 v0, 0x1770 */
/* .line 1267 */
} // :cond_2
final String v3 = "App requested: isolate_instructions:"; // const-string v3, "App requested: isolate_instructions:"
v3 = (( java.lang.String ) p1 ).startsWith ( v3 ); // invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 1268 */
/* const/16 v0, 0x7d0 */
/* .line 1269 */
} // :cond_3
v3 = v3 = com.android.server.am.ActivityManagerServiceImpl.JOB_ANRS;
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 1270 */
/* const/16 v0, 0x7d0 */
/* .line 1272 */
} // :cond_4
/* .line 1264 */
} // :cond_5
} // :goto_0
/* const/16 v0, 0x4e20 */
/* .line 1274 */
} // :goto_1
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "checkThawTime thawTime="; // const-string v4, "checkThawTime thawTime="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1275 */
int v4 = 1; // const/4 v4, 0x1
(( com.miui.server.greeze.GreezeManagerInternal ) p2 ).getLastThawedTime ( p0, v4 ); // invoke-virtual {p2, p0, v4}, Lcom/miui/server/greeze/GreezeManagerInternal;->getLastThawedTime(II)J
/* move-result-wide v5 */
(( java.lang.StringBuilder ) v3 ).append ( v5, v6 ); // invoke-virtual {v3, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v5 = " now="; // const-string v5, " now="
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1276 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v5 */
(( java.lang.StringBuilder ) v3 ).append ( v5, v6 ); // invoke-virtual {v3, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1274 */
android.util.Slog .d ( v1,v3 );
/* .line 1277 */
(( com.miui.server.greeze.GreezeManagerInternal ) p2 ).getLastThawedTime ( p0, v4 ); // invoke-virtual {p2, p0, v4}, Lcom/miui/server/greeze/GreezeManagerInternal;->getLastThawedTime(II)J
/* move-result-wide v5 */
/* .line 1278 */
/* .local v5, "thawTime":J */
/* const/16 v3, 0x2710 */
/* if-lt p0, v3, :cond_6 */
/* const/16 v3, 0x4e1f */
/* if-gt p0, v3, :cond_6 */
/* const-wide/16 v7, 0x0 */
/* cmp-long v3, v5, v7 */
/* if-lez v3, :cond_6 */
/* .line 1280 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v7 */
/* sub-long/2addr v7, v5 */
/* int-to-long v9, v0 */
/* cmp-long v3, v7, v9 */
/* if-gez v3, :cond_6 */
/* .line 1281 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "matched "; // const-string v3, "matched "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " app time uid="; // const-string v3, " app time uid="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p0 ); // invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 1282 */
/* .line 1285 */
} // :cond_6
} // .end method
private static void checkTime ( Long p0, java.lang.String p1 ) {
/* .locals 6 */
/* .param p0, "startTime" # J */
/* .param p2, "where" # Ljava/lang/String; */
/* .line 684 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* .line 685 */
/* .local v0, "now":J */
/* sub-long v2, v0, p0 */
/* const-wide/16 v4, 0x3e8 */
/* cmp-long v2, v2, v4 */
/* if-lez v2, :cond_0 */
/* .line 687 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "MIUILOG-checkTime:Slow operation: "; // const-string v3, "MIUILOG-checkTime:Slow operation: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sub-long v3, v0, p0 */
(( java.lang.StringBuilder ) v2 ).append ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v3 = "ms so far, now at "; // const-string v3, "ms so far, now at "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "ActivityManagerServiceImpl"; // const-string v3, "ActivityManagerServiceImpl"
android.util.Slog .w ( v3,v2 );
/* .line 689 */
} // :cond_0
return;
} // .end method
static Boolean checkWakePath ( com.android.server.am.ActivityManagerService p0, miui.security.CallerInfo p1, java.lang.String p2, android.content.Intent p3, android.content.pm.ComponentInfo p4, Integer p5, Integer p6 ) {
/* .locals 27 */
/* .param p0, "ams" # Lcom/android/server/am/ActivityManagerService; */
/* .param p1, "callerInfo" # Lmiui/security/CallerInfo; */
/* .param p2, "callingPackage" # Ljava/lang/String; */
/* .param p3, "intent" # Landroid/content/Intent; */
/* .param p4, "info" # Landroid/content/pm/ComponentInfo; */
/* .param p5, "wakeType" # I */
/* .param p6, "userId" # I */
/* .line 593 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
/* move-object/from16 v3, p3 */
/* move-object/from16 v4, p4 */
/* move/from16 v14, p5 */
/* move/from16 v15, p6 */
int v13 = 1; // const/4 v13, 0x1
if ( v1 != null) { // if-eqz v1, :cond_f
if ( v3 != null) { // if-eqz v3, :cond_f
/* if-nez v4, :cond_0 */
/* move/from16 v23, v13 */
/* goto/16 :goto_6 */
/* .line 597 */
} // :cond_0
miui.security.WakePathChecker .getInstance ( );
/* .line 598 */
/* .local v12, "checker":Lmiui/security/WakePathChecker; */
(( miui.security.WakePathChecker ) v12 ).updatePath ( v3, v4, v14, v15 ); // invoke-virtual {v12, v3, v4, v14, v15}, Lmiui/security/WakePathChecker;->updatePath(Landroid/content/Intent;Landroid/content/pm/ComponentInfo;II)V
/* .line 600 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v10 */
/* .line 602 */
/* .local v10, "startTime":J */
int v0 = -1; // const/4 v0, -0x1
/* .line 603 */
/* .local v0, "callerUid":I */
final String v16 = ""; // const-string v16, ""
/* .line 604 */
/* .local v16, "callerPkg":Ljava/lang/String; */
/* const/16 v17, 0x0 */
final String v9 = "ActivityManagerServiceImpl"; // const-string v9, "ActivityManagerServiceImpl"
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 605 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
v6 = this.callerPkg;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ":widgetProvider"; // const-string v6, ":widgetProvider"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 606 */
/* .local v8, "widgetProcessName":Ljava/lang/String; */
v5 = com.android.server.am.ActivityManagerServiceImpl.WIDGET_PROVIDER_WHITE_LIST;
v5 = v6 = this.callerPkg;
/* if-nez v5, :cond_4 */
v5 = this.callerProcessName;
/* .line 607 */
v5 = (( java.lang.String ) v8 ).equals ( v5 ); // invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 609 */
/* if-ne v14, v13, :cond_1 */
/* .line 610 */
v5 = this.callerPkg;
v5 = com.android.server.am.PendingIntentRecordImpl .containsPendingIntent ( v5 );
/* xor-int/2addr v5, v13 */
/* move/from16 v18, v5 */
/* .local v5, "abort":Z */
/* .line 612 */
} // .end local v5 # "abort":Z
} // :cond_1
v5 = this.mActivityTaskManager;
v6 = this.processName;
v7 = this.applicationInfo;
/* iget v7, v7, Landroid/content/pm/ApplicationInfo;->uid:I */
v5 = com.android.server.wm.WindowProcessUtils .isProcessRunning ( v5,v6,v7 );
/* xor-int/2addr v5, v13 */
/* move/from16 v18, v5 */
/* .line 615 */
/* .local v18, "abort":Z */
} // :goto_0
if ( v18 != null) { // if-eqz v18, :cond_2
/* .line 617 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "MIUILOG- Reject widget call from "; // const-string v6, "MIUILOG- Reject widget call from "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.callerPkg;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v9,v5 );
/* .line 618 */
miui.security.WakePathChecker .getInstance ( );
v6 = this.callerPkg;
v7 = this.packageName;
/* iget v9, v2, Lmiui/security/CallerInfo;->callerUid:I */
/* .line 619 */
v9 = android.os.UserHandle .getUserId ( v9 );
v13 = this.applicationInfo;
/* iget v13, v13, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 620 */
v13 = android.os.UserHandle .getUserId ( v13 );
/* const/16 v19, 0x0 */
/* .line 618 */
/* move-object/from16 v20, v8 */
} // .end local v8 # "widgetProcessName":Ljava/lang/String;
/* .local v20, "widgetProcessName":Ljava/lang/String; */
/* move/from16 v8, p5 */
/* move-wide/from16 v21, v10 */
} // .end local v10 # "startTime":J
/* .local v21, "startTime":J */
/* move v10, v13 */
/* move/from16 v11, v19 */
/* invoke-virtual/range {v5 ..v11}, Lmiui/security/WakePathChecker;->recordWakePathCall(Ljava/lang/String;Ljava/lang/String;IIIZ)V */
/* .line 621 */
/* .line 615 */
} // .end local v20 # "widgetProcessName":Ljava/lang/String;
} // .end local v21 # "startTime":J
/* .restart local v8 # "widgetProcessName":Ljava/lang/String; */
/* .restart local v10 # "startTime":J */
} // :cond_2
/* move-object/from16 v20, v8 */
/* move-wide/from16 v21, v10 */
} // .end local v8 # "widgetProcessName":Ljava/lang/String;
} // .end local v10 # "startTime":J
/* .restart local v20 # "widgetProcessName":Ljava/lang/String; */
/* .restart local v21 # "startTime":J */
/* .line 607 */
} // .end local v18 # "abort":Z
} // .end local v20 # "widgetProcessName":Ljava/lang/String;
} // .end local v21 # "startTime":J
/* .restart local v8 # "widgetProcessName":Ljava/lang/String; */
/* .restart local v10 # "startTime":J */
} // :cond_3
/* move-object/from16 v20, v8 */
/* move-wide/from16 v21, v10 */
} // .end local v8 # "widgetProcessName":Ljava/lang/String;
} // .end local v10 # "startTime":J
/* .restart local v20 # "widgetProcessName":Ljava/lang/String; */
/* .restart local v21 # "startTime":J */
/* .line 606 */
} // .end local v20 # "widgetProcessName":Ljava/lang/String;
} // .end local v21 # "startTime":J
/* .restart local v8 # "widgetProcessName":Ljava/lang/String; */
/* .restart local v10 # "startTime":J */
} // :cond_4
/* move-object/from16 v20, v8 */
/* move-wide/from16 v21, v10 */
/* .line 624 */
} // .end local v8 # "widgetProcessName":Ljava/lang/String;
} // .end local v10 # "startTime":J
/* .restart local v20 # "widgetProcessName":Ljava/lang/String; */
/* .restart local v21 # "startTime":J */
} // :goto_1
v5 = this.callerPkg;
/* .line 625 */
} // .end local v16 # "callerPkg":Ljava/lang/String;
/* .local v5, "callerPkg":Ljava/lang/String; */
/* iget v0, v2, Lmiui/security/CallerInfo;->callerUid:I */
/* .line 626 */
} // .end local v20 # "widgetProcessName":Ljava/lang/String;
/* move-object v11, v5 */
/* .line 627 */
} // .end local v5 # "callerPkg":Ljava/lang/String;
} // .end local v21 # "startTime":J
/* .restart local v10 # "startTime":J */
/* .restart local v16 # "callerPkg":Ljava/lang/String; */
} // :cond_5
/* move-wide/from16 v21, v10 */
} // .end local v10 # "startTime":J
/* .restart local v21 # "startTime":J */
v5 = /* invoke-static/range {p2 ..p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z */
if ( v5 != null) { // if-eqz v5, :cond_8
int v5 = 4; // const/4 v5, 0x4
/* if-ne v14, v5, :cond_8 */
/* .line 629 */
final String v5 = "android.intent.extra.UID"; // const-string v5, "android.intent.extra.UID"
int v6 = -1; // const/4 v6, -0x1
v5 = (( android.content.Intent ) v3 ).getIntExtra ( v5, v6 ); // invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 630 */
} // .end local v0 # "callerUid":I
/* .local v5, "callerUid":I */
/* if-eq v5, v6, :cond_7 */
/* .line 632 */
try { // :try_start_0
/* invoke-virtual/range {p0 ..p0}, Lcom/android/server/am/ActivityManagerService;->getPackageManager()Landroid/content/pm/IPackageManager; */
/* .line 633 */
/* .local v0, "pkgs":[Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_6
/* array-length v6, v0 */
if ( v6 != null) { // if-eqz v6, :cond_6
/* .line 634 */
/* aget-object v6, v0, v17 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object/from16 v16, v6 */
/* .line 639 */
} // .end local v0 # "pkgs":[Ljava/lang/String;
} // :cond_6
/* move v0, v5 */
/* move-object/from16 v11, v16 */
/* .line 636 */
/* :catch_0 */
/* move-exception v0 */
/* .line 637 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v6 = "getPackagesFor uid exception!"; // const-string v6, "getPackagesFor uid exception!"
android.util.Log .e ( v9,v6,v0 );
/* .line 638 */
final String v0 = "android"; // const-string v0, "android"
/* .line 639 */
} // .end local v16 # "callerPkg":Ljava/lang/String;
/* .local v0, "callerPkg":Ljava/lang/String; */
/* move-object v11, v0 */
/* move v0, v5 */
/* .line 641 */
} // .end local v0 # "callerPkg":Ljava/lang/String;
/* .restart local v16 # "callerPkg":Ljava/lang/String; */
} // :cond_7
final String v0 = "android"; // const-string v0, "android"
/* move-object v11, v0 */
/* move v0, v5 */
} // .end local v16 # "callerPkg":Ljava/lang/String;
/* .restart local v0 # "callerPkg":Ljava/lang/String; */
/* .line 644 */
} // .end local v5 # "callerUid":I
/* .local v0, "callerUid":I */
/* .restart local v16 # "callerPkg":Ljava/lang/String; */
} // :cond_8
/* move-object/from16 v5, p2 */
/* move-object v11, v5 */
/* .line 648 */
} // .end local v16 # "callerPkg":Ljava/lang/String;
/* .local v11, "callerPkg":Ljava/lang/String; */
} // :goto_2
int v5 = -1; // const/4 v5, -0x1
/* .line 649 */
/* .local v5, "calleeUid":I */
v10 = this.packageName;
/* .line 650 */
/* .local v10, "calleePkg":Ljava/lang/String; */
v8 = this.name;
/* .line 651 */
/* .local v8, "className":Ljava/lang/String; */
/* invoke-virtual/range {p3 ..p3}, Landroid/content/Intent;->getAction()Ljava/lang/String; */
/* .line 652 */
/* .local v7, "action":Ljava/lang/String; */
v6 = this.applicationInfo;
if ( v6 != null) { // if-eqz v6, :cond_9
/* .line 653 */
v6 = this.applicationInfo;
/* iget v5, v6, Landroid/content/pm/ApplicationInfo;->uid:I */
/* move v6, v5 */
/* .line 652 */
} // :cond_9
/* move v6, v5 */
/* .line 656 */
} // .end local v5 # "calleeUid":I
/* .local v6, "calleeUid":I */
} // :goto_3
v5 = android.text.TextUtils .isEmpty ( v10 );
if ( v5 != null) { // if-eqz v5, :cond_a
/* .line 657 */
/* .line 660 */
} // :cond_a
v5 = android.text.TextUtils .equals ( v11,v10 );
if ( v5 != null) { // if-eqz v5, :cond_b
/* .line 661 */
/* .line 664 */
} // :cond_b
/* if-ltz v6, :cond_e */
/* .line 665 */
v5 = this.mActivityTaskManager;
v13 = this.processName;
v5 = com.android.server.wm.WindowProcessUtils .isPackageRunning ( v5,v10,v13,v6 );
if ( v5 != null) { // if-eqz v5, :cond_d
/* .line 667 */
/* shl-int/lit8 v13, v14, 0xc */
/* const/16 v17, 0x1 */
/* move-object v5, v12 */
/* move/from16 v18, v6 */
} // .end local v6 # "calleeUid":I
/* .local v18, "calleeUid":I */
/* move-object v6, v7 */
/* move-object/from16 v23, v7 */
} // .end local v7 # "action":Ljava/lang/String;
/* .local v23, "action":Ljava/lang/String; */
/* move-object v7, v8 */
/* move-object/from16 v24, v8 */
} // .end local v8 # "className":Ljava/lang/String;
/* .local v24, "className":Ljava/lang/String; */
/* move-object v8, v11 */
/* move-object/from16 v25, v9 */
/* move-object v9, v10 */
/* move-object/from16 v26, v10 */
} // .end local v10 # "calleePkg":Ljava/lang/String;
/* .local v26, "calleePkg":Ljava/lang/String; */
/* move/from16 v10, p6 */
/* move-object v1, v11 */
} // .end local v11 # "callerPkg":Ljava/lang/String;
/* .local v1, "callerPkg":Ljava/lang/String; */
/* move v11, v13 */
/* move-object/from16 v19, v12 */
} // .end local v12 # "checker":Lmiui/security/WakePathChecker;
/* .local v19, "checker":Lmiui/security/WakePathChecker; */
/* move/from16 v12, v17 */
v5 = /* invoke-virtual/range {v5 ..v12}, Lmiui/security/WakePathChecker;->calleeAliveMatchBlackRule(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)Z */
int v13 = 1; // const/4 v13, 0x1
/* xor-int/2addr v5, v13 */
/* .line 669 */
/* .local v5, "isAllow":Z */
/* if-nez v5, :cond_c */
/* .line 670 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "MIUILOG-Reject alive wakepath call "; // const-string v7, "MIUILOG-Reject alive wakepath call "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v15 ); // invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = " caller= "; // const-string v7, " caller= "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v1 ); // invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " callee= "; // const-string v7, " callee= "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v12, v26 */
} // .end local v26 # "calleePkg":Ljava/lang/String;
/* .local v12, "calleePkg":Ljava/lang/String; */
(( java.lang.StringBuilder ) v6 ).append ( v12 ); // invoke-virtual {v6, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " classname="; // const-string v7, " classname="
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v11, v24 */
} // .end local v24 # "className":Ljava/lang/String;
/* .local v11, "className":Ljava/lang/String; */
(( java.lang.StringBuilder ) v6 ).append ( v11 ); // invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " action="; // const-string v7, " action="
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v10, v23 */
} // .end local v23 # "action":Ljava/lang/String;
/* .local v10, "action":Ljava/lang/String; */
(( java.lang.StringBuilder ) v6 ).append ( v10 ); // invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " wakeType="; // const-string v7, " wakeType="
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v14 ); // invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object/from16 v7, v25 */
android.util.Slog .i ( v7,v6 );
/* .line 669 */
} // .end local v10 # "action":Ljava/lang/String;
} // .end local v11 # "className":Ljava/lang/String;
} // .end local v12 # "calleePkg":Ljava/lang/String;
/* .restart local v23 # "action":Ljava/lang/String; */
/* .restart local v24 # "className":Ljava/lang/String; */
/* .restart local v26 # "calleePkg":Ljava/lang/String; */
} // :cond_c
/* move-object/from16 v10, v23 */
/* move-object/from16 v11, v24 */
/* move-object/from16 v12, v26 */
/* .line 674 */
} // .end local v23 # "action":Ljava/lang/String;
} // .end local v24 # "className":Ljava/lang/String;
} // .end local v26 # "calleePkg":Ljava/lang/String;
/* .restart local v10 # "action":Ljava/lang/String; */
/* .restart local v11 # "className":Ljava/lang/String; */
/* .restart local v12 # "calleePkg":Ljava/lang/String; */
} // :goto_4
/* .line 665 */
} // .end local v1 # "callerPkg":Ljava/lang/String;
} // .end local v5 # "isAllow":Z
} // .end local v18 # "calleeUid":I
} // .end local v19 # "checker":Lmiui/security/WakePathChecker;
/* .restart local v6 # "calleeUid":I */
/* .restart local v7 # "action":Ljava/lang/String; */
/* .restart local v8 # "className":Ljava/lang/String; */
/* .local v10, "calleePkg":Ljava/lang/String; */
/* .local v11, "callerPkg":Ljava/lang/String; */
/* .local v12, "checker":Lmiui/security/WakePathChecker; */
} // :cond_d
/* move/from16 v18, v6 */
/* move-object v1, v11 */
/* move-object/from16 v19, v12 */
int v13 = 1; // const/4 v13, 0x1
/* move-object v11, v8 */
/* move-object v12, v10 */
/* move-object v10, v7 */
} // .end local v6 # "calleeUid":I
} // .end local v7 # "action":Ljava/lang/String;
} // .end local v8 # "className":Ljava/lang/String;
/* .restart local v1 # "callerPkg":Ljava/lang/String; */
/* .local v10, "action":Ljava/lang/String; */
/* .local v11, "className":Ljava/lang/String; */
/* .local v12, "calleePkg":Ljava/lang/String; */
/* .restart local v18 # "calleeUid":I */
/* .restart local v19 # "checker":Lmiui/security/WakePathChecker; */
/* .line 664 */
} // .end local v1 # "callerPkg":Ljava/lang/String;
} // .end local v18 # "calleeUid":I
} // .end local v19 # "checker":Lmiui/security/WakePathChecker;
/* .restart local v6 # "calleeUid":I */
/* .restart local v7 # "action":Ljava/lang/String; */
/* .restart local v8 # "className":Ljava/lang/String; */
/* .local v10, "calleePkg":Ljava/lang/String; */
/* .local v11, "callerPkg":Ljava/lang/String; */
/* .local v12, "checker":Lmiui/security/WakePathChecker; */
} // :cond_e
/* move/from16 v18, v6 */
/* move-object v1, v11 */
/* move-object/from16 v19, v12 */
/* move-object v11, v8 */
/* move-object v12, v10 */
/* move-object v10, v7 */
/* .line 678 */
} // .end local v6 # "calleeUid":I
} // .end local v7 # "action":Ljava/lang/String;
} // .end local v8 # "className":Ljava/lang/String;
/* .restart local v1 # "callerPkg":Ljava/lang/String; */
/* .local v10, "action":Ljava/lang/String; */
/* .local v11, "className":Ljava/lang/String; */
/* .local v12, "calleePkg":Ljava/lang/String; */
/* .restart local v18 # "calleeUid":I */
/* .restart local v19 # "checker":Lmiui/security/WakePathChecker; */
} // :goto_5
/* move-object/from16 v5, v19 */
/* move-object v6, v10 */
/* move-object v7, v11 */
/* move-object v8, v1 */
/* move-object v9, v12 */
/* move-object/from16 v16, v10 */
} // .end local v10 # "action":Ljava/lang/String;
/* .local v16, "action":Ljava/lang/String; */
/* move v10, v0 */
/* move-object/from16 v17, v11 */
} // .end local v11 # "className":Ljava/lang/String;
/* .local v17, "className":Ljava/lang/String; */
/* move/from16 v11, v18 */
/* move-object/from16 v20, v12 */
} // .end local v12 # "calleePkg":Ljava/lang/String;
/* .local v20, "calleePkg":Ljava/lang/String; */
/* move/from16 v12, p5 */
/* move/from16 v23, v13 */
/* move/from16 v13, p6 */
v5 = /* invoke-virtual/range {v5 ..v13}, Lmiui/security/WakePathChecker;->matchWakePathRule(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)Z */
/* xor-int/lit8 v5, v5, 0x1 */
/* .line 679 */
/* .local v5, "ret":Z */
final String v6 = "checkWakePath"; // const-string v6, "checkWakePath"
/* move-wide/from16 v7, v21 */
} // .end local v21 # "startTime":J
/* .local v7, "startTime":J */
com.android.server.am.ActivityManagerServiceImpl .checkTime ( v7,v8,v6 );
/* .line 680 */
/* .line 593 */
} // .end local v0 # "callerUid":I
} // .end local v1 # "callerPkg":Ljava/lang/String;
} // .end local v5 # "ret":Z
} // .end local v7 # "startTime":J
} // .end local v16 # "action":Ljava/lang/String;
} // .end local v17 # "className":Ljava/lang/String;
} // .end local v18 # "calleeUid":I
} // .end local v19 # "checker":Lmiui/security/WakePathChecker;
} // .end local v20 # "calleePkg":Ljava/lang/String;
} // :cond_f
/* move/from16 v23, v13 */
/* .line 594 */
} // :goto_6
} // .end method
private static Boolean doForegroundBoost ( com.android.server.am.ProcessRecord p0, Long p1 ) {
/* .locals 4 */
/* .param p0, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p1, "beginTime" # J */
/* .line 1244 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* sub-long/2addr v0, p1 */
/* const-wide/16 v2, 0x4e20 */
/* cmp-long v0, v0, v2 */
/* if-lez v0, :cond_0 */
/* .line 1245 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1247 */
} // :cond_0
v0 = this.mState;
v0 = (( com.android.server.am.ProcessStateRecord ) v0 ).getCurrentSchedulingGroup ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getCurrentSchedulingGroup()I
int v1 = 2; // const/4 v1, 0x2
/* if-ge v0, v1, :cond_1 */
/* .line 1248 */
v0 = this.mState;
(( com.android.server.am.ProcessStateRecord ) v0 ).setCurrentSchedulingGroup ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessStateRecord;->setCurrentSchedulingGroup(I)V
/* .line 1250 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // .end method
private static Boolean doTopAppBoost ( com.android.server.am.ProcessRecord p0, Long p1 ) {
/* .locals 4 */
/* .param p0, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p1, "beginTime" # J */
/* .line 1231 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* sub-long/2addr v0, p1 */
/* const-wide/16 v2, 0xbb8 */
/* cmp-long v0, v0, v2 */
/* if-gtz v0, :cond_2 */
v0 = this.mState;
/* .line 1232 */
v0 = (( com.android.server.am.ProcessStateRecord ) v0 ).getCurrentSchedulingGroup ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getCurrentSchedulingGroup()I
int v1 = 3; // const/4 v1, 0x3
/* if-ne v0, v1, :cond_0 */
/* .line 1235 */
} // :cond_0
v0 = this.mState;
v0 = (( com.android.server.am.ProcessStateRecord ) v0 ).getCurrentSchedulingGroup ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getCurrentSchedulingGroup()I
/* if-ge v0, v1, :cond_1 */
/* .line 1236 */
v0 = this.mState;
(( com.android.server.am.ProcessStateRecord ) v0 ).setCurrentSchedulingGroup ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessStateRecord;->setCurrentSchedulingGroup(I)V
/* .line 1237 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Process is boosted to top app, processName="; // const-string v1, "Process is boosted to top app, processName="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.processName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "."; // const-string v1, "."
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "Boost"; // const-string v1, "Boost"
android.util.Slog .d ( v1,v0 );
/* .line 1240 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
/* .line 1233 */
} // :cond_2
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean dumpAppLogText ( java.io.FileDescriptor p0, java.io.PrintWriter p1, java.lang.String[] p2, Integer p3 ) {
/* .locals 9 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .param p3, "args" # [Ljava/lang/String; */
/* .param p4, "opti" # I */
/* .line 1670 */
/* array-length v0, p3 */
int v1 = 0; // const/4 v1, 0x0
/* if-ge p4, v0, :cond_3 */
/* .line 1671 */
/* aget-object v0, p3, p4 */
/* .line 1672 */
/* .local v0, "dumpPackage":Ljava/lang/String; */
int v2 = 1; // const/4 v2, 0x1
/* add-int/2addr p4, v2 */
/* .line 1677 */
/* array-length v3, p3 */
/* if-ge p4, v3, :cond_2 */
/* .line 1679 */
try { // :try_start_0
/* aget-object v3, p3, p4 */
v3 = java.lang.Integer .parseInt ( v3 );
/* .line 1680 */
/* .local v3, "uid":I */
v4 = this.mAmService;
/* monitor-enter v4 */
/* :try_end_0 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .line 1681 */
try { // :try_start_1
v5 = this.mAmService;
(( com.android.server.am.ActivityManagerService ) v5 ).getProcessRecordLocked ( v0, v3 ); // invoke-virtual {v5, v0, v3}, Lcom/android/server/am/ActivityManagerService;->getProcessRecordLocked(Ljava/lang/String;I)Lcom/android/server/am/ProcessRecord;
/* .line 1682 */
/* .local v5, "app":Lcom/android/server/am/ProcessRecord; */
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 1683 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "\n** APP LOGGIN in pid "; // const-string v7, "\n** APP LOGGIN in pid "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v5, Lcom/android/server/am/ProcessRecord;->mPid:I */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = "["; // const-string v7, "["
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = "] **"; // const-string v7, "] **"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v6 ); // invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1685 */
(( com.android.server.am.ProcessRecord ) v5 ).getThread ( ); // invoke-virtual {v5}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* move-object v7, v6 */
/* .local v7, "thread":Landroid/app/IApplicationThread; */
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 1687 */
try { // :try_start_2
/* new-instance v6, Lcom/android/internal/os/TransferPipe; */
/* invoke-direct {v6}, Lcom/android/internal/os/TransferPipe;-><init>()V */
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_1 */
/* .catch Landroid/os/RemoteException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 1689 */
/* .local v6, "tp":Lcom/android/internal/os/TransferPipe; */
try { // :try_start_3
(( com.android.internal.os.TransferPipe ) v6 ).getWriteFd ( ); // invoke-virtual {v6}, Lcom/android/internal/os/TransferPipe;->getWriteFd()Landroid/os/ParcelFileDescriptor;
/* .line 1690 */
(( com.android.internal.os.TransferPipe ) v6 ).go ( p1 ); // invoke-virtual {v6, p1}, Lcom/android/internal/os/TransferPipe;->go(Ljava/io/FileDescriptor;)V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 1692 */
try { // :try_start_4
(( com.android.internal.os.TransferPipe ) v6 ).kill ( ); // invoke-virtual {v6}, Lcom/android/internal/os/TransferPipe;->kill()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_1 */
/* .catch Landroid/os/RemoteException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* .line 1693 */
/* nop */
/* .line 1694 */
try { // :try_start_5
/* monitor-exit v4 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* .line 1692 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_6
(( com.android.internal.os.TransferPipe ) v6 ).kill ( ); // invoke-virtual {v6}, Lcom/android/internal/os/TransferPipe;->kill()V
/* .line 1693 */
/* nop */
} // .end local v0 # "dumpPackage":Ljava/lang/String;
} // .end local v3 # "uid":I
} // .end local v5 # "app":Lcom/android/server/am/ProcessRecord;
} // .end local v7 # "thread":Landroid/app/IApplicationThread;
} // .end local p0 # "this":Lcom/android/server/am/ActivityManagerServiceImpl;
} // .end local p1 # "fd":Ljava/io/FileDescriptor;
} // .end local p2 # "pw":Ljava/io/PrintWriter;
} // .end local p3 # "args":[Ljava/lang/String;
} // .end local p4 # "opti":I
/* throw v2 */
/* :try_end_6 */
/* .catch Ljava/io/IOException; {:try_start_6 ..:try_end_6} :catch_1 */
/* .catch Landroid/os/RemoteException; {:try_start_6 ..:try_end_6} :catch_0 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_1 */
/* .line 1698 */
} // .end local v6 # "tp":Lcom/android/internal/os/TransferPipe;
/* .restart local v0 # "dumpPackage":Ljava/lang/String; */
/* .restart local v3 # "uid":I */
/* .restart local v5 # "app":Lcom/android/server/am/ProcessRecord; */
/* .restart local v7 # "thread":Landroid/app/IApplicationThread; */
/* .restart local p0 # "this":Lcom/android/server/am/ActivityManagerServiceImpl; */
/* .restart local p1 # "fd":Ljava/io/FileDescriptor; */
/* .restart local p2 # "pw":Ljava/io/PrintWriter; */
/* .restart local p3 # "args":[Ljava/lang/String; */
/* .restart local p4 # "opti":I */
/* :catch_0 */
/* move-exception v2 */
/* .line 1699 */
/* .local v2, "e":Landroid/os/RemoteException; */
try { // :try_start_7
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Got RemoteException! "; // const-string v8, "Got RemoteException! "
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v6 ); // invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1700 */
(( java.io.PrintWriter ) p2 ).flush ( ); // invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V
/* .line 1695 */
} // .end local v2 # "e":Landroid/os/RemoteException;
/* :catch_1 */
/* move-exception v2 */
/* .line 1696 */
/* .local v2, "e":Ljava/io/IOException; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Got IoException! "; // const-string v8, "Got IoException! "
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v6 ); // invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1697 */
(( java.io.PrintWriter ) p2 ).flush ( ); // invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V
/* .line 1701 */
} // .end local v2 # "e":Ljava/io/IOException;
/* nop */
/* .line 1703 */
} // .end local v7 # "thread":Landroid/app/IApplicationThread;
} // :cond_0
} // :goto_0
/* nop */
/* .line 1707 */
} // .end local v5 # "app":Lcom/android/server/am/ProcessRecord;
/* monitor-exit v4 */
/* .line 1711 */
} // .end local v3 # "uid":I
/* .line 1704 */
/* .restart local v3 # "uid":I */
/* .restart local v5 # "app":Lcom/android/server/am/ProcessRecord; */
} // :cond_1
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "app-logging: "; // const-string v6, "app-logging: "
(( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = "("; // const-string v6, "("
(( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = ") not running."; // const-string v6, ") not running."
(( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v2 ); // invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1705 */
/* monitor-exit v4 */
/* .line 1707 */
} // .end local v5 # "app":Lcom/android/server/am/ProcessRecord;
/* :catchall_1 */
/* move-exception v2 */
/* monitor-exit v4 */
/* :try_end_7 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_1 */
} // .end local v0 # "dumpPackage":Ljava/lang/String;
} // .end local p0 # "this":Lcom/android/server/am/ActivityManagerServiceImpl;
} // .end local p1 # "fd":Ljava/io/FileDescriptor;
} // .end local p2 # "pw":Ljava/io/PrintWriter;
} // .end local p3 # "args":[Ljava/lang/String;
} // .end local p4 # "opti":I
try { // :try_start_8
/* throw v2 */
/* :try_end_8 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_8 ..:try_end_8} :catch_2 */
/* .line 1708 */
} // .end local v3 # "uid":I
/* .restart local v0 # "dumpPackage":Ljava/lang/String; */
/* .restart local p0 # "this":Lcom/android/server/am/ActivityManagerServiceImpl; */
/* .restart local p1 # "fd":Ljava/io/FileDescriptor; */
/* .restart local p2 # "pw":Ljava/io/PrintWriter; */
/* .restart local p3 # "args":[Ljava/lang/String; */
/* .restart local p4 # "opti":I */
/* :catch_2 */
/* move-exception v2 */
/* .line 1709 */
/* .local v2, "e":Ljava/lang/NumberFormatException; */
final String v3 = "app-logging: uid format is error, please input integer."; // const-string v3, "app-logging: uid format is error, please input integer."
(( java.io.PrintWriter ) p2 ).println ( v3 ); // invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1710 */
/* .line 1713 */
} // .end local v2 # "e":Ljava/lang/NumberFormatException;
} // :cond_2
final String v2 = "app-logging: no uid specified."; // const-string v2, "app-logging: no uid specified."
(( java.io.PrintWriter ) p2 ).println ( v2 ); // invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1715 */
} // :goto_1
/* .line 1674 */
} // .end local v0 # "dumpPackage":Ljava/lang/String;
} // :cond_3
final String v0 = "app-logging: no process name specified"; // const-string v0, "app-logging: no process name specified"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1675 */
} // .end method
private static void ensureDeviceProvisioned ( android.content.Context p0 ) {
/* .locals 7 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 288 */
v0 = com.android.server.am.ActivityManagerServiceImpl .isDeviceProvisioned ( p0 );
/* if-nez v0, :cond_2 */
/* .line 289 */
(( android.content.Context ) p0 ).getPackageManager ( ); // invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 291 */
/* .local v0, "pm":Landroid/content/pm/PackageManager; */
/* sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
final String v2 = "com.android.provision.activities.DefaultActivity"; // const-string v2, "com.android.provision.activities.DefaultActivity"
final String v3 = "com.android.provision"; // const-string v3, "com.android.provision"
/* if-nez v1, :cond_0 */
/* .line 292 */
/* new-instance v1, Landroid/content/ComponentName; */
/* invoke-direct {v1, v3, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* .local v1, "checkEnableName":Landroid/content/ComponentName; */
/* .line 295 */
} // .end local v1 # "checkEnableName":Landroid/content/ComponentName;
} // :cond_0
/* new-instance v1, Landroid/content/ComponentName; */
final String v4 = "com.google.android.setupwizard"; // const-string v4, "com.google.android.setupwizard"
final String v5 = "com.google.android.setupwizard.SetupWizardActivity"; // const-string v5, "com.google.android.setupwizard.SetupWizardActivity"
/* invoke-direct {v1, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 299 */
/* .restart local v1 # "checkEnableName":Landroid/content/ComponentName; */
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 300 */
v4 = (( android.content.pm.PackageManager ) v0 ).getComponentEnabledSetting ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I
int v5 = 2; // const/4 v5, 0x2
/* if-ne v4, v5, :cond_2 */
/* .line 302 */
final String v4 = "ActivityManagerServiceImpl"; // const-string v4, "ActivityManagerServiceImpl"
final String v5 = "The device provisioned state is inconsistent,try to restore."; // const-string v5, "The device provisioned state is inconsistent,try to restore."
android.util.Log .e ( v4,v5 );
/* .line 303 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v5 = "device_provisioned"; // const-string v5, "device_provisioned"
int v6 = 1; // const/4 v6, 0x1
android.provider.Settings$Secure .putInt ( v4,v5,v6 );
/* .line 306 */
/* sget-boolean v4, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v4, :cond_1 */
/* .line 307 */
/* new-instance v4, Landroid/content/ComponentName; */
/* invoke-direct {v4, v3, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* move-object v2, v4 */
/* .line 309 */
/* .local v2, "name":Landroid/content/ComponentName; */
(( android.content.pm.PackageManager ) v0 ).setComponentEnabledSetting ( v2, v6, v6 ); // invoke-virtual {v0, v2, v6, v6}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V
/* .line 312 */
/* new-instance v3, Landroid/content/Intent; */
final String v4 = "android.intent.action.MAIN"; // const-string v4, "android.intent.action.MAIN"
/* invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 313 */
/* .local v3, "intent":Landroid/content/Intent; */
(( android.content.Intent ) v3 ).setComponent ( v2 ); // invoke-virtual {v3, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 314 */
/* const/high16 v4, 0x10000000 */
(( android.content.Intent ) v3 ).addFlags ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 315 */
final String v4 = "android.intent.category.HOME"; // const-string v4, "android.intent.category.HOME"
(( android.content.Intent ) v3 ).addCategory ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;
/* .line 316 */
(( android.content.Context ) p0 ).startActivity ( v3 ); // invoke-virtual {p0, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
/* .line 317 */
} // .end local v2 # "name":Landroid/content/ComponentName;
} // .end local v3 # "intent":Landroid/content/Intent;
/* .line 318 */
} // :cond_1
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v3, "user_setup_complete" */
android.provider.Settings$Secure .putInt ( v2,v3,v6 );
/* .line 324 */
} // .end local v0 # "pm":Landroid/content/pm/PackageManager;
} // .end local v1 # "checkEnableName":Landroid/content/ComponentName;
} // :cond_2
} // :goto_1
return;
} // .end method
private android.util.Pair generateGroups ( java.lang.String p0, java.lang.String p1, Boolean p2, java.lang.String p3 ) {
/* .locals 3 */
/* .param p1, "enabled" # Ljava/lang/String; */
/* .param p2, "disabled" # Ljava/lang/String; */
/* .param p3, "config" # Z */
/* .param p4, "group" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* "Z", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Landroid/util/Pair<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1754 */
final String v0 = " "; // const-string v0, " "
if ( p3 != null) { // if-eqz p3, :cond_1
/* .line 1755 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = android.text.TextUtils .isEmpty ( p1 );
if ( v2 != null) { // if-eqz v2, :cond_0
/* move-object v0, p4 */
} // :cond_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // :goto_0
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1757 */
} // :cond_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = android.text.TextUtils .isEmpty ( p2 );
if ( v2 != null) { // if-eqz v2, :cond_2
/* move-object v0, p4 */
} // :cond_2
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // :goto_1
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1759 */
} // :goto_2
/* new-instance v0, Landroid/util/Pair; */
/* invoke-direct {v0, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
} // .end method
private com.miui.server.greeze.GreezeManagerInternal getGreezeService ( ) {
/* .locals 1 */
/* .line 1290 */
v0 = this.greezer;
/* if-nez v0, :cond_0 */
/* .line 1291 */
com.miui.server.greeze.GreezeManagerInternal .getInstance ( );
this.greezer = v0;
/* .line 1292 */
} // :cond_0
v0 = this.greezer;
} // .end method
public static com.android.server.am.ActivityManagerServiceImpl getInstance ( ) {
/* .locals 1 */
/* .line 236 */
/* const-class v0, Lcom/android/server/am/ActivityManagerServiceStub; */
com.miui.base.MiuiStubUtil .getImpl ( v0 );
/* check-cast v0, Lcom/android/server/am/ActivityManagerServiceImpl; */
} // .end method
private android.os.Parcelable getIntentInfo ( Integer p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "pid" # I */
/* .param p2, "isForLast" # Z */
/* .line 1423 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 1424 */
int v0 = 2; // const/4 v0, 0x2
/* new-array v0, v0, [Landroid/os/Parcelable; */
int v1 = 0; // const/4 v1, 0x0
v2 = this.mLastSplitIntent;
/* aput-object v2, v0, v1 */
int v1 = 1; // const/4 v1, 0x1
v2 = this.mSplitExtras;
/* aput-object v2, v0, v1 */
/* .line 1426 */
} // :cond_0
v0 = this.mCurrentSplitIntent;
java.lang.Integer .valueOf ( p1 );
/* check-cast v0, Landroid/os/Parcelable; */
int v1 = 0; // const/4 v1, 0x0
/* filled-new-array {v0, v1}, [Landroid/os/Parcelable; */
} // .end method
private static Boolean isDeviceProvisioned ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 283 */
/* nop */
/* .line 284 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 283 */
final String v1 = "device_provisioned"; // const-string v1, "device_provisioned"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$Global .getInt ( v0,v1,v2 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
} // .end method
private Boolean isProtectProcess ( Integer p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "pkgName" # Ljava/lang/String; */
/* .param p3, "procName" # Ljava/lang/String; */
/* .line 1459 */
/* const/16 v0, 0x1d8 */
/* .line 1464 */
/* .local v0, "protectFlag":I */
v1 = v1 = this.mSmartPowerService;
/* if-nez v1, :cond_1 */
v1 = this.mSmartPowerService;
v1 = /* .line 1465 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1469 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 1467 */
} // :cond_1
} // :goto_0
int v1 = 1; // const/4 v1, 0x1
} // .end method
private static Boolean isSystem ( java.lang.String p0, Integer p1 ) {
/* .locals 7 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .param p1, "userId" # I */
/* .line 1861 */
/* const-class v0, Landroid/content/pm/PackageManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Landroid/content/pm/PackageManagerInternal; */
/* .line 1862 */
/* .local v0, "internal":Landroid/content/pm/PackageManagerInternal; */
/* const-wide/16 v3, 0x0 */
/* const/16 v5, 0x3e8 */
/* move-object v1, v0 */
/* move-object v2, p0 */
/* move v6, p1 */
/* invoke-virtual/range {v1 ..v6}, Landroid/content/pm/PackageManagerInternal;->getApplicationInfo(Ljava/lang/String;JII)Landroid/content/pm/ApplicationInfo; */
/* .line 1864 */
/* .local v1, "applicationInfo":Landroid/content/pm/ApplicationInfo; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1865 */
v2 = (( android.content.pm.ApplicationInfo ) v1 ).isSystemApp ( ); // invoke-virtual {v1}, Landroid/content/pm/ApplicationInfo;->isSystemApp()Z
/* if-nez v2, :cond_0 */
/* iget v2, v1, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 1866 */
v2 = android.os.UserHandle .getAppId ( v2 );
/* const/16 v3, 0x2710 */
/* if-ge v2, v3, :cond_1 */
} // :cond_0
int v2 = 1; // const/4 v2, 0x1
} // :cond_1
int v2 = 0; // const/4 v2, 0x0
/* .line 1864 */
} // :goto_0
} // .end method
public static Boolean isSystemPackage ( java.lang.String p0, Integer p1 ) {
/* .locals 4 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .param p1, "userId" # I */
/* .line 919 */
int v0 = 1; // const/4 v0, 0x1
try { // :try_start_0
android.app.AppGlobals .getPackageManager ( );
/* const-wide/16 v2, 0x0 */
/* .line 920 */
/* .local v1, "applicationInfo":Landroid/content/pm/ApplicationInfo; */
/* if-nez v1, :cond_0 */
/* .line 921 */
} // :cond_0
/* iget v2, v1, Landroid/content/pm/ApplicationInfo;->flags:I */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 922 */
/* .local v2, "flags":I */
/* and-int/lit8 v3, v2, 0x1 */
/* if-nez v3, :cond_2 */
/* and-int/lit16 v3, v2, 0x80 */
if ( v3 != null) { // if-eqz v3, :cond_1
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // :cond_2
} // :goto_0
/* .line 924 */
} // .end local v1 # "applicationInfo":Landroid/content/pm/ApplicationInfo;
} // .end local v2 # "flags":I
/* :catch_0 */
/* move-exception v1 */
/* .line 925 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 927 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // .end method
private void lambda$dumpSystemTraces$0 ( java.lang.String p0 ) { //synthethic
/* .locals 6 */
/* .param p1, "path" # Ljava/lang/String; */
/* .line 1138 */
final String v0 = "MIUIScout App"; // const-string v0, "MIUIScout App"
com.android.server.ScoutHelper .CheckDState ( v0,v1 );
/* .line 1140 */
final String v0 = "MIUIScout App"; // const-string v0, "MIUIScout App"
final String v1 = "Start dumping system_server trace ..."; // const-string v1, "Start dumping system_server trace ..."
android.util.Slog .i ( v0,v1 );
/* .line 1141 */
final String v1 = "App Scout Exception"; // const-string v1, "App Scout Exception"
(( com.android.server.am.ActivityManagerServiceImpl ) p0 ).dumpOneProcessTraces ( v0, p1, v1 ); // invoke-virtual {p0, v0, p1, v1}, Lcom/android/server/am/ActivityManagerServiceImpl;->dumpOneProcessTraces(ILjava/lang/String;Ljava/lang/String;)Ljava/io/File;
/* .line 1144 */
/* .local v0, "systemTraceFile":Ljava/io/File; */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1145 */
final String v2 = "MIUIScout App"; // const-string v2, "MIUIScout App"
final String v3 = "Dump scout system trace file successfully!"; // const-string v3, "Dump scout system trace file successfully!"
android.util.Slog .d ( v2,v3 );
/* .line 1152 */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* .line 1153 */
/* .local v2, "tempArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
v3 = com.android.server.am.ActivityManagerServiceImpl.dumpTraceRequestList;
/* monitor-enter v3 */
/* .line 1154 */
try { // :try_start_0
v4 = com.android.server.am.ActivityManagerServiceImpl.dumpTraceRequestList;
(( java.util.ArrayList ) v4 ).iterator ( ); // invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_0
/* check-cast v5, Ljava/lang/String; */
/* .line 1155 */
/* .local v5, "filePath":Ljava/lang/String; */
(( java.util.ArrayList ) v2 ).add ( v5 ); // invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 1156 */
/* nop */
} // .end local v5 # "filePath":Ljava/lang/String;
/* .line 1157 */
} // :cond_0
v4 = com.android.server.am.ActivityManagerServiceImpl.dumpTraceRequestList;
(( java.util.ArrayList ) v4 ).clear ( ); // invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V
/* .line 1158 */
/* monitor-exit v3 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1159 */
v3 = com.android.server.am.ActivityManagerServiceImpl.dumpFlag;
(( java.util.concurrent.atomic.AtomicBoolean ) v3 ).set ( v1 ); // invoke-virtual {v3, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 1160 */
final String v1 = "MIUIScout App"; // const-string v1, "MIUIScout App"
/* const-string/jumbo v3, "starting copying file" */
android.util.Slog .d ( v1,v3 );
/* .line 1163 */
v1 = com.android.server.am.ActivityManagerServiceImpl.requestDumpTraceCount;
v1 = (( java.util.concurrent.atomic.AtomicInteger ) v1 ).get ( ); // invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
/* if-lez v1, :cond_1 */
v1 = (( java.util.ArrayList ) v2 ).size ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->size()I
/* if-lez v1, :cond_1 */
/* .line 1164 */
/* new-instance v1, Lcom/android/server/am/ActivityManagerServiceImpl$2; */
/* invoke-direct {v1, p0, v2, v0}, Lcom/android/server/am/ActivityManagerServiceImpl$2;-><init>(Lcom/android/server/am/ActivityManagerServiceImpl;Ljava/util/ArrayList;Ljava/io/File;)V */
/* .line 1192 */
(( com.android.server.am.ActivityManagerServiceImpl$2 ) v1 ).start ( ); // invoke-virtual {v1}, Lcom/android/server/am/ActivityManagerServiceImpl$2;->start()V
/* .line 1194 */
} // :cond_1
return;
/* .line 1158 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
/* .line 1147 */
} // .end local v2 # "tempArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
} // :cond_2
final String v2 = "MIUIScout App"; // const-string v2, "MIUIScout App"
final String v3 = "Dump scout system trace file fail!"; // const-string v3, "Dump scout system trace file fail!"
android.util.Slog .w ( v2,v3 );
/* .line 1148 */
v2 = com.android.server.am.ActivityManagerServiceImpl.dumpFlag;
(( java.util.concurrent.atomic.AtomicBoolean ) v2 ).set ( v1 ); // invoke-virtual {v2, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 1149 */
return;
} // .end method
private void recordAppBehavior ( java.lang.String p0, java.lang.String p1, Integer p2, java.lang.String p3 ) {
/* .locals 7 */
/* .param p1, "sourcePkg" # Ljava/lang/String; */
/* .param p2, "targetPkg" # Ljava/lang/String; */
/* .param p3, "targetUid" # I */
/* .param p4, "grantUri" # Ljava/lang/String; */
/* .line 1846 */
v0 = android.os.UserHandle .getAppId ( p3 );
/* const/16 v1, 0x2710 */
/* if-lt v0, v1, :cond_1 */
/* .line 1847 */
v0 = android.os.Binder .getCallingUid ( );
v0 = android.os.UserHandle .getUserId ( v0 );
v0 = com.android.server.am.ActivityManagerServiceImpl .isSystem ( p1,v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1848 */
v0 = android.os.UserHandle .getUserId ( p3 );
v0 = com.android.server.am.ActivityManagerServiceImpl .isSystem ( p2,v0 );
/* if-nez v0, :cond_1 */
/* .line 1849 */
v0 = this.mSecurityInternal;
/* if-nez v0, :cond_0 */
/* .line 1850 */
/* const-class v0, Lmiui/security/SecurityManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lmiui/security/SecurityManagerInternal; */
this.mSecurityInternal = v0;
/* .line 1852 */
} // :cond_0
v1 = this.mSecurityInternal;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1853 */
/* const/16 v2, 0x1e */
/* const-wide/16 v4, 0x1 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "@"; // const-string v3, "@"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object v3, p2 */
/* invoke-virtual/range {v1 ..v6}, Lmiui/security/SecurityManagerInternal;->recordAppBehaviorAsync(ILjava/lang/String;JLjava/lang/String;)V */
/* .line 1858 */
} // :cond_1
return;
} // .end method
private void setIntentInfo ( android.content.Intent p0, Integer p1, android.os.Bundle p2, Boolean p3 ) {
/* .locals 2 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .param p2, "pid" # I */
/* .param p3, "bundle" # Landroid/os/Bundle; */
/* .param p4, "isForLast" # Z */
/* .line 1430 */
if ( p4 != null) { // if-eqz p4, :cond_0
/* .line 1431 */
this.mLastSplitIntent = p1;
/* .line 1432 */
this.mSplitExtras = p3;
/* .line 1434 */
} // :cond_0
v0 = this.mCurrentSplitIntent;
v0 = java.lang.Integer .valueOf ( p2 );
/* if-nez v0, :cond_1 */
/* .line 1435 */
final String v0 = "ActivityManagerServiceImpl"; // const-string v0, "ActivityManagerServiceImpl"
final String v1 = "CRITICAL_LOG add intent info."; // const-string v1, "CRITICAL_LOG add intent info."
android.util.Log .e ( v0,v1 );
/* .line 1437 */
} // :cond_1
v0 = this.mCurrentSplitIntent;
java.lang.Integer .valueOf ( p2 );
/* .line 1439 */
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void addToEntryStack ( Integer p0, android.os.IBinder p1, Integer p2, android.content.Intent p3 ) {
/* .locals 5 */
/* .param p1, "pid" # I */
/* .param p2, "token" # Landroid/os/IBinder; */
/* .param p3, "resultCode" # I */
/* .param p4, "resultData" # Landroid/content/Intent; */
/* .line 1404 */
v0 = this.splitEntryStackLock;
/* monitor-enter v0 */
/* .line 1405 */
try { // :try_start_0
v1 = this.mSplitActivityEntryStack;
/* if-nez v1, :cond_0 */
/* .line 1406 */
/* new-instance v1, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mSplitActivityEntryStack = v1;
/* .line 1407 */
final String v1 = "ActivityManagerServiceImpl"; // const-string v1, "ActivityManagerServiceImpl"
final String v2 = "SplitEntryStack: init"; // const-string v2, "SplitEntryStack: init"
android.util.Slog .d ( v1,v2 );
/* .line 1409 */
} // :cond_0
v1 = this.mSplitActivityEntryStack;
java.lang.Integer .valueOf ( p1 );
/* check-cast v1, Ljava/util/Stack; */
/* .line 1410 */
/* .local v1, "stack":Ljava/util/Stack;, "Ljava/util/Stack<Landroid/os/IBinder;>;" */
/* if-nez v1, :cond_1 */
/* .line 1411 */
/* new-instance v2, Ljava/util/Stack; */
/* invoke-direct {v2}, Ljava/util/Stack;-><init>()V */
/* move-object v1, v2 */
/* .line 1412 */
v2 = this.mSplitActivityEntryStack;
java.lang.Integer .valueOf ( p1 );
/* .line 1413 */
final String v2 = "ActivityManagerServiceImpl"; // const-string v2, "ActivityManagerServiceImpl"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "SplitEntryStack: create stack, pid="; // const-string v4, "SplitEntryStack: create stack, pid="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 1415 */
} // :cond_1
/* move-object v2, p2 */
/* check-cast v2, Landroid/os/Binder; */
v2 = (( java.util.Stack ) v1 ).contains ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/Stack;->contains(Ljava/lang/Object;)Z
/* if-nez v2, :cond_2 */
/* .line 1416 */
(( java.util.Stack ) v1 ).push ( p2 ); // invoke-virtual {v1, p2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1417 */
final String v2 = "ActivityManagerServiceImpl"; // const-string v2, "ActivityManagerServiceImpl"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "SplitEntryStack: push to stack, size="; // const-string v4, "SplitEntryStack: push to stack, size="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = (( java.util.Stack ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/Stack;->size()I
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " pid="; // const-string v4, " pid="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 1419 */
} // .end local v1 # "stack":Ljava/util/Stack;, "Ljava/util/Stack<Landroid/os/IBinder;>;"
} // :cond_2
/* monitor-exit v0 */
/* .line 1420 */
return;
/* .line 1419 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void backupBind ( Integer p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .param p2, "start" # Z */
/* .line 1794 */
v0 = this.greezer;
/* if-nez v0, :cond_0 */
return;
/* .line 1795 */
} // :cond_0
/* const/16 v0, 0x2710 */
/* if-lt p1, v0, :cond_4 */
/* const/16 v0, 0x4e1f */
/* if-le p1, v0, :cond_1 */
/* .line 1796 */
} // :cond_1
v0 = this.mBackupingList;
java.lang.Integer .valueOf ( p1 );
v0 = (( java.util.ArrayList ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v0, :cond_2 */
if ( p2 != null) { // if-eqz p2, :cond_2
/* .line 1797 */
v0 = this.mBackupingList;
java.lang.Integer .valueOf ( p1 );
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 1798 */
v0 = this.greezer;
int v1 = 1; // const/4 v1, 0x1
(( com.miui.server.greeze.GreezeManagerInternal ) v0 ).notifyBackup ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/miui/server/greeze/GreezeManagerInternal;->notifyBackup(IZ)V
/* .line 1799 */
} // :cond_2
v0 = this.mBackupingList;
java.lang.Integer .valueOf ( p1 );
v0 = (( java.util.ArrayList ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* if-nez p2, :cond_3 */
/* .line 1800 */
v0 = this.mBackupingList;
java.lang.Integer .valueOf ( p1 );
(( java.util.ArrayList ) v0 ).remove ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
/* .line 1802 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 1803 */
/* .local v0, "b":Landroid/os/Bundle; */
/* const-string/jumbo v1, "uid" */
(( android.os.Bundle ) v0 ).putInt ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 1804 */
/* const-string/jumbo v1, "start" */
int v2 = 0; // const/4 v2, 0x0
(( android.os.Bundle ) v0 ).putBoolean ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
/* .line 1805 */
com.miui.whetstone.PowerKeeperPolicy .getInstance ( );
/* const/16 v2, 0x12 */
(( com.miui.whetstone.PowerKeeperPolicy ) v1 ).notifyEvent ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Lcom/miui/whetstone/PowerKeeperPolicy;->notifyEvent(ILandroid/os/Bundle;)V
/* .line 1807 */
} // .end local v0 # "b":Landroid/os/Bundle;
} // :cond_3
} // :goto_0
return;
/* .line 1795 */
} // :cond_4
} // :goto_1
return;
} // .end method
public Boolean checkAppDisableStatus ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1765 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "sys.rescuepartyplus.disable_autorestart." */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1766 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Disable App ["; // const-string v1, "Disable App ["
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "] auto start!"; // const-string v1, "] auto start!"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "ActivityManagerServiceImpl"; // const-string v1, "ActivityManagerServiceImpl"
android.util.Slog .w ( v1,v0 );
/* .line 1767 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1769 */
} // :cond_0
} // .end method
public Boolean checkRunningCompatibility ( android.app.IApplicationThread p0, Integer p1, com.android.server.am.ContentProviderRecord p2, Integer p3 ) {
/* .locals 11 */
/* .param p1, "caller" # Landroid/app/IApplicationThread; */
/* .param p2, "callingUid" # I */
/* .param p3, "record" # Lcom/android/server/am/ContentProviderRecord; */
/* .param p4, "userId" # I */
/* .line 507 */
/* iget-boolean v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mSystemReady:Z */
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_0 */
/* .line 508 */
/* .line 510 */
} // :cond_0
if ( p3 != null) { // if-eqz p3, :cond_5
v0 = this.name;
/* if-nez v0, :cond_1 */
/* .line 514 */
} // :cond_1
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 515 */
/* .local v0, "intent":Landroid/content/Intent; */
v2 = this.name;
(( android.content.ComponentName ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
v3 = this.name;
(( android.content.ComponentName ) v3 ).getClassName ( ); // invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
(( android.content.Intent ) v0 ).setClassName ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 516 */
final String v2 = "android.intent.extra.UID"; // const-string v2, "android.intent.extra.UID"
(( android.content.Intent ) v0 ).putExtra ( v2, p2 ); // invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 519 */
v2 = this.mAmService;
v2 = this.mActivityTaskManager;
com.android.server.wm.WindowProcessUtils .getCallerInfo ( v2,p1 );
/* .line 521 */
/* .local v9, "callerInfo":Lmiui/security/CallerInfo; */
/* if-nez v9, :cond_3 */
/* .line 522 */
v2 = this.mPackageManager;
(( android.content.pm.PackageManagerInternal ) v2 ).getPackage ( p2 ); // invoke-virtual {v2, p2}, Landroid/content/pm/PackageManagerInternal;->getPackage(I)Lcom/android/server/pm/pkg/AndroidPackage;
/* .line 523 */
/* .local v2, "aPackage":Lcom/android/server/pm/pkg/AndroidPackage; */
/* if-nez v2, :cond_2 */
/* .line 524 */
/* .line 526 */
} // :cond_2
/* .line 527 */
/* .local v2, "callerPkg":Ljava/lang/String; */
/* move-object v10, v2 */
/* .line 528 */
} // .end local v2 # "callerPkg":Ljava/lang/String;
} // :cond_3
v2 = this.callerPkg;
/* move-object v10, v2 */
/* .line 530 */
/* .local v10, "callerPkg":Ljava/lang/String; */
} // :goto_0
com.android.server.clipboard.ClipboardServiceStub .get ( );
v3 = this.info;
/* .line 531 */
v2 = (( com.android.server.clipboard.ClipboardServiceStub ) v2 ).checkProviderWakePathForClipboard ( v10, p2, v3, p4 ); // invoke-virtual {v2, v10, p2, v3, p4}, Lcom/android/server/clipboard/ClipboardServiceStub;->checkProviderWakePathForClipboard(Ljava/lang/String;ILandroid/content/pm/ProviderInfo;I)Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 532 */
/* .line 534 */
} // :cond_4
v2 = this.mAmService;
int v4 = 0; // const/4 v4, 0x0
v6 = this.info;
int v7 = 4; // const/4 v7, 0x4
/* move-object v3, v9 */
/* move-object v5, v0 */
/* move v8, p4 */
v1 = /* invoke-static/range {v2 ..v8}, Lcom/android/server/am/ActivityManagerServiceImpl;->checkWakePath(Lcom/android/server/am/ActivityManagerService;Lmiui/security/CallerInfo;Ljava/lang/String;Landroid/content/Intent;Landroid/content/pm/ComponentInfo;II)Z */
/* .line 511 */
} // .end local v0 # "intent":Landroid/content/Intent;
} // .end local v9 # "callerInfo":Lmiui/security/CallerInfo;
} // .end local v10 # "callerPkg":Ljava/lang/String;
} // :cond_5
} // :goto_1
} // .end method
public Boolean checkRunningCompatibility ( android.app.IApplicationThread p0, android.content.Intent p1, java.lang.String p2, Integer p3 ) {
/* .locals 2 */
/* .param p1, "caller" # Landroid/app/IApplicationThread; */
/* .param p2, "service" # Landroid/content/Intent; */
/* .param p3, "resolvedType" # Ljava/lang/String; */
/* .param p4, "userId" # I */
/* .line 445 */
/* iget-boolean v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mSystemReady:Z */
/* if-nez v0, :cond_0 */
/* .line 446 */
int v0 = 1; // const/4 v0, 0x1
/* .line 448 */
} // :cond_0
v0 = this.mAmService;
v0 = this.mActivityTaskManager;
com.android.server.wm.WindowProcessUtils .getCallerInfo ( v0,p1 );
/* .line 449 */
/* .local v0, "callerInfo":Lmiui/security/CallerInfo; */
v1 = /* invoke-direct {p0, p2, p3, v0, p4}, Lcom/android/server/am/ActivityManagerServiceImpl;->checkServiceWakePath(Landroid/content/Intent;Ljava/lang/String;Lmiui/security/CallerInfo;I)Z */
} // .end method
public Boolean checkRunningCompatibility ( android.app.IApplicationThread p0, android.content.pm.ActivityInfo p1, android.content.Intent p2, Integer p3, java.lang.String p4 ) {
/* .locals 8 */
/* .param p1, "caller" # Landroid/app/IApplicationThread; */
/* .param p2, "info" # Landroid/content/pm/ActivityInfo; */
/* .param p3, "intent" # Landroid/content/Intent; */
/* .param p4, "userId" # I */
/* .param p5, "callingPackage" # Ljava/lang/String; */
/* .line 492 */
/* if-nez p2, :cond_0 */
/* .line 493 */
int v0 = 1; // const/4 v0, 0x1
/* .line 495 */
} // :cond_0
v0 = this.mAmService;
v0 = this.mActivityTaskManager;
com.android.server.wm.WindowProcessUtils .getCallerInfo ( v0,p1 );
/* .line 496 */
/* .local v0, "callerInfo":Lmiui/security/CallerInfo; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 497 */
android.app.PrivacyTestModeStub .get ( );
v2 = this.callerProcessName;
v6 = this.mContext;
/* move-object v3, p2 */
/* move-object v4, p3 */
/* move-object v5, p5 */
/* invoke-virtual/range {v1 ..v6}, Landroid/app/PrivacyTestModeStub;->collectPrivacyTestModeInfo(Ljava/lang/String;Landroid/content/pm/ActivityInfo;Landroid/content/Intent;Ljava/lang/String;Landroid/content/Context;)V */
/* .line 499 */
} // :cond_1
v1 = this.mAmService;
int v6 = 1; // const/4 v6, 0x1
/* move-object v2, v0 */
/* move-object v3, p5 */
/* move-object v4, p3 */
/* move-object v5, p2 */
/* move v7, p4 */
v1 = /* invoke-static/range {v1 ..v7}, Lcom/android/server/am/ActivityManagerServiceImpl;->checkWakePath(Lcom/android/server/am/ActivityManagerService;Lmiui/security/CallerInfo;Ljava/lang/String;Landroid/content/Intent;Landroid/content/pm/ComponentInfo;II)Z */
} // .end method
public Boolean checkRunningCompatibility ( android.content.ComponentName p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 16 */
/* .param p1, "className" # Landroid/content/ComponentName; */
/* .param p2, "callingUid" # I */
/* .param p3, "callingPid" # I */
/* .param p4, "userId" # I */
/* .line 541 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 542 */
/* .line 544 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_b
v0 = android.miui.AppOpsUtils .isXOptMode ( );
if ( v0 != null) { // if-eqz v0, :cond_1
/* move-object/from16 v15, p0 */
/* move/from16 v3, p3 */
/* move/from16 v14, p4 */
/* goto/16 :goto_5 */
/* .line 547 */
} // :cond_1
v2 = /* invoke-static/range {p2 ..p2}, Landroid/os/UserHandle;->getAppId(I)I */
/* .line 548 */
/* .local v2, "callingAppId":I */
/* const/16 v0, 0x3e8 */
/* if-eq v2, v0, :cond_a */
if ( v2 != null) { // if-eqz v2, :cond_a
/* const/16 v0, 0x7d0 */
/* if-ne v2, v0, :cond_2 */
/* move-object/from16 v15, p0 */
/* move/from16 v3, p3 */
/* move/from16 v14, p4 */
/* goto/16 :goto_4 */
/* .line 553 */
} // :cond_2
/* invoke-virtual/range {p1 ..p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String; */
/* .line 554 */
/* .local v10, "calleePackage":Ljava/lang/String; */
/* invoke-static/range {p3 ..p3}, Lcom/android/server/am/ProcessUtils;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord; */
/* .line 555 */
/* .local v11, "callerInfo":Lcom/android/server/am/ProcessRecord; */
int v12 = 0; // const/4 v12, 0x0
final String v0 = "ActivityManagerServiceImpl"; // const-string v0, "ActivityManagerServiceImpl"
if ( v11 != null) { // if-eqz v11, :cond_9
v3 = this.info;
/* if-nez v3, :cond_3 */
/* move-object/from16 v15, p0 */
/* move/from16 v14, p4 */
/* goto/16 :goto_3 */
/* .line 559 */
} // :cond_3
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
v4 = this.info;
v4 = this.packageName;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ":widgetProvider"; // const-string v4, ":widgetProvider"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 560 */
/* .local v13, "widgetProcessName":Ljava/lang/String; */
v3 = com.android.server.am.ActivityManagerServiceImpl.WIDGET_PROVIDER_WHITE_LIST;
v4 = this.info;
v3 = v4 = this.packageName;
/* if-nez v3, :cond_4 */
v3 = this.processName;
/* .line 561 */
v3 = (( java.lang.String ) v13 ).equals ( v3 ); // invoke-virtual {v13, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 562 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "MIUILOG- Reject widget call from "; // const-string v4, "MIUILOG- Reject widget call from "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.info;
v4 = this.packageName;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v3 );
/* .line 563 */
miui.security.WakePathChecker .getInstance ( );
v0 = this.info;
v4 = this.packageName;
int v6 = 0; // const/4 v6, 0x0
/* iget v0, v11, Lcom/android/server/am/ProcessRecord;->uid:I */
/* .line 564 */
v7 = android.os.UserHandle .getUserId ( v0 );
int v9 = 0; // const/4 v9, 0x0
/* .line 563 */
/* move-object v5, v10 */
/* move/from16 v8, p4 */
/* invoke-virtual/range {v3 ..v9}, Lmiui/security/WakePathChecker;->recordWakePathCall(Ljava/lang/String;Ljava/lang/String;IIIZ)V */
/* .line 567 */
} // :cond_4
v0 = this.info;
v0 = this.packageName;
v0 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v0, :cond_8 */
v0 = this.info;
v0 = this.packageName;
/* .line 568 */
v0 = android.text.TextUtils .equals ( v0,v10 );
if ( v0 != null) { // if-eqz v0, :cond_5
/* move-object/from16 v15, p0 */
/* move/from16 v14, p4 */
/* .line 572 */
} // :cond_5
try { // :try_start_0
android.app.AppGlobals .getPackageManager ( );
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_2 */
/* const-wide/16 v3, 0x0 */
/* move/from16 v14, p4 */
try { // :try_start_1
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 574 */
/* .local v0, "ai":Landroid/content/pm/ApplicationInfo; */
if ( v0 != null) { // if-eqz v0, :cond_7
/* move-object/from16 v15, p0 */
try { // :try_start_2
v3 = this.mAmService;
v3 = this.mActivityTaskManager;
v4 = this.processName;
/* iget v5, v0, Landroid/content/pm/ApplicationInfo;->uid:I */
v3 = com.android.server.wm.WindowProcessUtils .isPackageRunning ( v3,v10,v4,v5 );
/* :try_end_2 */
/* .catch Landroid/os/RemoteException; {:try_start_2 ..:try_end_2} :catch_0 */
if ( v3 != null) { // if-eqz v3, :cond_6
/* .line 580 */
} // .end local v0 # "ai":Landroid/content/pm/ApplicationInfo;
} // :cond_6
/* .line 578 */
/* :catch_0 */
/* move-exception v0 */
/* .line 574 */
/* .restart local v0 # "ai":Landroid/content/pm/ApplicationInfo; */
} // :cond_7
/* move-object/from16 v15, p0 */
/* .line 576 */
} // :goto_0
/* .line 578 */
} // .end local v0 # "ai":Landroid/content/pm/ApplicationInfo;
/* :catch_1 */
/* move-exception v0 */
/* move-object/from16 v15, p0 */
/* :catch_2 */
/* move-exception v0 */
/* move-object/from16 v15, p0 */
/* move/from16 v14, p4 */
/* .line 581 */
} // :goto_1
miui.security.WakePathChecker .getInstance ( );
v0 = this.info;
v4 = this.packageName;
int v6 = 0; // const/4 v6, 0x0
/* iget v0, v11, Lcom/android/server/am/ProcessRecord;->uid:I */
/* .line 583 */
v7 = android.os.UserHandle .getUserId ( v0 );
int v9 = 0; // const/4 v9, 0x0
/* .line 581 */
/* move-object v5, v10 */
/* move/from16 v8, p4 */
/* invoke-virtual/range {v3 ..v9}, Lmiui/security/WakePathChecker;->recordWakePathCall(Ljava/lang/String;Ljava/lang/String;IIIZ)V */
/* .line 584 */
/* .line 567 */
} // :cond_8
/* move-object/from16 v15, p0 */
/* move/from16 v14, p4 */
/* .line 569 */
} // :goto_2
/* .line 555 */
} // .end local v13 # "widgetProcessName":Ljava/lang/String;
} // :cond_9
/* move-object/from16 v15, p0 */
/* move/from16 v14, p4 */
/* .line 556 */
} // :goto_3
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "MIUILOG- Reject call from dying process "; // const-string v3, "MIUILOG- Reject call from dying process "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move/from16 v3, p3 */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 557 */
/* .line 548 */
} // .end local v10 # "calleePackage":Ljava/lang/String;
} // .end local v11 # "callerInfo":Lcom/android/server/am/ProcessRecord;
} // :cond_a
/* move-object/from16 v15, p0 */
/* move/from16 v3, p3 */
/* move/from16 v14, p4 */
/* .line 551 */
} // :goto_4
/* .line 544 */
} // .end local v2 # "callingAppId":I
} // :cond_b
/* move-object/from16 v15, p0 */
/* move/from16 v3, p3 */
/* move/from16 v14, p4 */
/* .line 545 */
} // :goto_5
} // .end method
public Boolean checkRunningCompatibility ( android.content.Intent p0, java.lang.String p1, Integer p2, Integer p3, Integer p4 ) {
/* .locals 3 */
/* .param p1, "service" # Landroid/content/Intent; */
/* .param p2, "resolvedType" # Ljava/lang/String; */
/* .param p3, "callingUid" # I */
/* .param p4, "callingPid" # I */
/* .param p5, "userId" # I */
/* .line 455 */
/* iget-boolean v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mSystemReady:Z */
/* if-nez v0, :cond_0 */
/* .line 456 */
int v0 = 1; // const/4 v0, 0x1
/* .line 458 */
} // :cond_0
v0 = this.mAmService;
v0 = this.mActivityTaskManager;
com.android.server.wm.WindowProcessUtils .getCallerInfo ( v0,p4,p3 );
/* .line 459 */
/* .local v0, "callerInfo":Lmiui/security/CallerInfo; */
/* if-nez v0, :cond_1 */
/* .line 460 */
com.android.server.am.ProcessUtils .getProcessRecordByPid ( p4 );
/* .line 461 */
/* .local v1, "record":Lcom/android/server/am/ProcessRecord; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 462 */
/* new-instance v2, Lmiui/security/CallerInfo; */
/* invoke-direct {v2}, Lmiui/security/CallerInfo;-><init>()V */
/* move-object v0, v2 */
/* .line 463 */
/* iput p3, v0, Lmiui/security/CallerInfo;->callerUid:I */
/* .line 464 */
v2 = this.info;
v2 = this.packageName;
this.callerPkg = v2;
/* .line 465 */
/* iput p4, v0, Lmiui/security/CallerInfo;->callerPid:I */
/* .line 466 */
v2 = this.processName;
this.callerProcessName = v2;
/* .line 469 */
} // .end local v1 # "record":Lcom/android/server/am/ProcessRecord;
} // :cond_1
v1 = /* invoke-direct {p0, p1, p2, v0, p5}, Lcom/android/server/am/ActivityManagerServiceImpl;->checkServiceWakePath(Landroid/content/Intent;Ljava/lang/String;Lmiui/security/CallerInfo;I)Z */
} // .end method
public Boolean checkStartInputMethodSettingsActivity ( android.content.IIntentSender p0 ) {
/* .locals 3 */
/* .param p1, "target" # Landroid/content/IIntentSender; */
/* .line 1646 */
/* instance-of v0, p1, Lcom/android/server/am/PendingIntentRecord; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1647 */
/* move-object v0, p1 */
/* check-cast v0, Lcom/android/server/am/PendingIntentRecord; */
/* .line 1648 */
/* .local v0, "pendingIntentRecord":Lcom/android/server/am/PendingIntentRecord; */
v1 = this.key;
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = this.key;
v1 = this.requestIntent;
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = this.key;
v1 = this.requestIntent;
/* .line 1650 */
(( android.content.Intent ) v1 ).getAction ( ); // invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 1649 */
final String v2 = "android.settings.INPUT_METHOD_SETTINGS"; // const-string v2, "android.settings.INPUT_METHOD_SETTINGS"
v1 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1651 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1654 */
} // .end local v0 # "pendingIntentRecord":Lcom/android/server/am/PendingIntentRecord;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void cleanUpApplicationRecordLocked ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 3 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 1332 */
/* invoke-super {p0, p1}, Lcom/android/server/am/ActivityManagerServiceStub;->cleanUpApplicationRecordLocked(Lcom/android/server/am/ProcessRecord;)V */
/* .line 1333 */
/* sget-boolean v0, Lmiui/os/DeviceFeature;->SUPPORT_SPLIT_ACTIVITY:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1334 */
v0 = this.mSplitActivityEntryStack;
final String v1 = "ActivityManagerServiceImpl"; // const-string v1, "ActivityManagerServiceImpl"
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget v2, p1, Lcom/android/server/am/ProcessRecord;->mPid:I */
v0 = java.lang.Integer .valueOf ( v2 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1335 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Split main entrance killed, clear sub activities for "; // const-string v2, "Split main entrance killed, clear sub activities for "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.info;
v2 = this.packageName;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", mPid "; // const-string v2, ", mPid "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p1, Lcom/android/server/am/ProcessRecord;->mPid:I */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v0 );
/* .line 1336 */
/* iget v0, p1, Lcom/android/server/am/ProcessRecord;->mPid:I */
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.am.ActivityManagerServiceImpl ) p0 ).clearEntryStack ( v0, v2 ); // invoke-virtual {p0, v0, v2}, Lcom/android/server/am/ActivityManagerServiceImpl;->clearEntryStack(ILandroid/os/IBinder;)V
/* .line 1337 */
v0 = this.mSplitActivityEntryStack;
/* iget v2, p1, Lcom/android/server/am/ProcessRecord;->mPid:I */
java.lang.Integer .valueOf ( v2 );
/* .line 1339 */
} // :cond_0
v0 = this.mCurrentSplitIntent;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1340 */
/* iget v2, p1, Lcom/android/server/am/ProcessRecord;->mPid:I */
java.lang.Integer .valueOf ( v2 );
/* .line 1342 */
} // :cond_1
final String v0 = "Cleaning tablet split stack."; // const-string v0, "Cleaning tablet split stack."
android.util.Slog .d ( v1,v0 );
/* .line 1344 */
} // :cond_2
return;
} // .end method
public void clearEntryStack ( Integer p0, android.os.IBinder p1 ) {
/* .locals 8 */
/* .param p1, "pid" # I */
/* .param p2, "selfToken" # Landroid/os/IBinder; */
/* .line 1377 */
v0 = this.splitEntryStackLock;
/* monitor-enter v0 */
/* .line 1378 */
try { // :try_start_0
v1 = this.mSplitActivityEntryStack;
v1 = if ( v1 != null) { // if-eqz v1, :cond_6
if ( v1 != null) { // if-eqz v1, :cond_0
/* goto/16 :goto_2 */
/* .line 1381 */
} // :cond_0
v1 = this.mSplitActivityEntryStack;
java.lang.Integer .valueOf ( p1 );
/* check-cast v1, Ljava/util/Stack; */
/* .line 1382 */
/* .local v1, "stack":Ljava/util/Stack;, "Ljava/util/Stack<Landroid/os/IBinder;>;" */
if ( v1 != null) { // if-eqz v1, :cond_5
v2 = (( java.util.Stack ) v1 ).empty ( ); // invoke-virtual {v1}, Ljava/util/Stack;->empty()Z
/* if-nez v2, :cond_5 */
if ( p2 != null) { // if-eqz p2, :cond_1
(( java.util.Stack ) v1 ).peek ( ); // invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;
v2 = (( java.lang.Object ) p2 ).equals ( v2 ); // invoke-virtual {p2, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_1 */
/* .line 1385 */
} // :cond_1
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v2 */
/* .line 1386 */
/* .local v2, "ident":J */
} // :goto_0
v4 = (( java.util.Stack ) v1 ).empty ( ); // invoke-virtual {v1}, Ljava/util/Stack;->empty()Z
/* if-nez v4, :cond_3 */
/* .line 1387 */
(( java.util.Stack ) v1 ).pop ( ); // invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;
/* check-cast v4, Landroid/os/IBinder; */
/* .line 1388 */
/* .local v4, "token":Landroid/os/IBinder; */
final String v5 = "ActivityManagerServiceImpl"; // const-string v5, "ActivityManagerServiceImpl"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "SplitEntryStack: pop stack, size="; // const-string v7, "SplitEntryStack: pop stack, size="
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = (( java.util.Stack ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/Stack;->size()I
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = " pid="; // const-string v7, " pid="
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p1 ); // invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v6 );
/* .line 1389 */
if ( v4 != null) { // if-eqz v4, :cond_2
v5 = (( java.lang.Object ) v4 ).equals ( p2 ); // invoke-virtual {v4, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
/* if-nez v5, :cond_2 */
/* .line 1390 */
v5 = this.mAmService;
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
(( com.android.server.am.ActivityManagerService ) v5 ).finishActivity ( v4, v7, v6, v7 ); // invoke-virtual {v5, v4, v7, v6, v7}, Lcom/android/server/am/ActivityManagerService;->finishActivity(Landroid/os/IBinder;ILandroid/content/Intent;I)Z
/* .line 1392 */
} // .end local v4 # "token":Landroid/os/IBinder;
} // :cond_2
/* .line 1395 */
} // :cond_3
android.os.Binder .restoreCallingIdentity ( v2,v3 );
/* .line 1396 */
if ( p2 != null) { // if-eqz p2, :cond_4
/* .line 1397 */
(( java.util.Stack ) v1 ).push ( p2 ); // invoke-virtual {v1, p2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1398 */
final String v4 = "ActivityManagerServiceImpl"; // const-string v4, "ActivityManagerServiceImpl"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "SplitEntryStack: push self to stack, size="; // const-string v6, "SplitEntryStack: push self to stack, size="
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = (( java.util.Stack ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/Stack;->size()I
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = " pid="; // const-string v6, " pid="
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v5 );
/* .line 1400 */
} // .end local v1 # "stack":Ljava/util/Stack;, "Ljava/util/Stack<Landroid/os/IBinder;>;"
} // .end local v2 # "ident":J
} // :cond_4
/* monitor-exit v0 */
/* .line 1401 */
return;
/* .line 1383 */
/* .restart local v1 # "stack":Ljava/util/Stack;, "Ljava/util/Stack<Landroid/os/IBinder;>;" */
} // :cond_5
} // :goto_1
/* monitor-exit v0 */
return;
/* .line 1379 */
} // .end local v1 # "stack":Ljava/util/Stack;, "Ljava/util/Stack<Landroid/os/IBinder;>;"
} // :cond_6
} // :goto_2
/* monitor-exit v0 */
return;
/* .line 1400 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean doBoostEx ( com.android.server.am.ProcessRecord p0, Long p1 ) {
/* .locals 3 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "beginTime" # J */
/* .line 955 */
int v0 = 0; // const/4 v0, 0x0
/* .line 957 */
/* .local v0, "boostNeededNext":Z */
v1 = com.android.server.am.ActivityManagerServiceImpl .doTopAppBoost ( p1,p2,p3 );
/* or-int/2addr v0, v1 */
/* .line 959 */
final String v1 = "com.tencent.mm"; // const-string v1, "com.tencent.mm"
v2 = this.processName;
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 960 */
v1 = com.android.server.am.ActivityManagerServiceImpl .doForegroundBoost ( p1,p2,p3 );
/* or-int/2addr v0, v1 */
/* .line 962 */
} // :cond_0
} // .end method
public Boolean dump ( java.lang.String p0, java.io.FileDescriptor p1, java.io.PrintWriter p2, java.lang.String[] p3, Integer p4, Boolean p5, Boolean p6, java.lang.String p7 ) {
/* .locals 1 */
/* .param p1, "cmd" # Ljava/lang/String; */
/* .param p2, "fd" # Ljava/io/FileDescriptor; */
/* .param p3, "pw" # Ljava/io/PrintWriter; */
/* .param p4, "args" # [Ljava/lang/String; */
/* .param p5, "opti" # I */
/* .param p6, "dumpAll" # Z */
/* .param p7, "dumpClient" # Z */
/* .param p8, "dumpPackage" # Ljava/lang/String; */
/* .line 1659 */
final String v0 = "logging"; // const-string v0, "logging"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1660 */
(( com.android.server.am.ActivityManagerServiceImpl ) p0 ).dumpLogText ( p3 ); // invoke-virtual {p0, p3}, Lcom/android/server/am/ActivityManagerServiceImpl;->dumpLogText(Ljava/io/PrintWriter;)V
/* .line 1661 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1662 */
} // :cond_0
final String v0 = "app-logging"; // const-string v0, "app-logging"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1663 */
v0 = /* invoke-direct {p0, p2, p3, p4, p5}, Lcom/android/server/am/ActivityManagerServiceImpl;->dumpAppLogText(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;I)Z */
/* .line 1665 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public java.io.File dumpAppStackTraces ( java.util.ArrayList p0, android.util.SparseArray p1, java.util.ArrayList p2, java.lang.String p3, java.lang.String p4 ) {
/* .locals 15 */
/* .param p4, "subject" # Ljava/lang/String; */
/* .param p5, "path" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;", */
/* "Landroid/util/SparseArray<", */
/* "Ljava/lang/Boolean;", */
/* ">;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/io/File;" */
/* } */
} // .end annotation
/* .line 1060 */
/* .local p1, "firstPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* .local p2, "lastPids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/Boolean;>;" */
/* .local p3, "nativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* move-object/from16 v1, p4 */
int v2 = 0; // const/4 v2, 0x0
/* .line 1062 */
/* .local v2, "extraPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "dumpStackTraces pids="; // const-string v3, "dumpStackTraces pids="
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v3, p2 */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v4 = " nativepids="; // const-string v4, " nativepids="
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v4, p3 */
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "MIUIScout App"; // const-string v5, "MIUIScout App"
android.util.Slog .i ( v5,v0 );
/* .line 1064 */
/* new-instance v0, Ljava/io/File; */
/* move-object/from16 v6, p5 */
/* invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* move-object v7, v0 */
/* .line 1067 */
/* .local v7, "tracesFile":Ljava/io/File; */
try { // :try_start_0
v0 = (( java.io.File ) v7 ).createNewFile ( ); // invoke-virtual {v7}, Ljava/io/File;->createNewFile()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1068 */
(( java.io.File ) v7 ).getAbsolutePath ( ); // invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
/* const/16 v8, 0x180 */
int v9 = -1; // const/4 v9, -0x1
android.os.FileUtils .setPermissions ( v0,v8,v9,v9 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .line 1073 */
} // :cond_0
/* nop */
/* .line 1074 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1075 */
try { // :try_start_1
/* new-instance v0, Ljava/io/FileOutputStream; */
int v8 = 1; // const/4 v8, 0x1
/* invoke-direct {v0, v7, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V */
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* move-object v8, v0 */
/* .line 1076 */
/* .local v8, "fos":Ljava/io/FileOutputStream; */
try { // :try_start_2
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "Subject: "; // const-string v9, "Subject: "
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = "\n"; // const-string v9, "\n"
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1077 */
/* .local v0, "header":Ljava/lang/String; */
v9 = java.nio.charset.StandardCharsets.UTF_8;
(( java.lang.String ) v0 ).getBytes ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B
(( java.io.FileOutputStream ) v8 ).write ( v9 ); // invoke-virtual {v8, v9}, Ljava/io/FileOutputStream;->write([B)V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 1078 */
} // .end local v0 # "header":Ljava/lang/String;
try { // :try_start_3
(( java.io.FileOutputStream ) v8 ).close ( ); // invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 1080 */
} // .end local v8 # "fos":Ljava/io/FileOutputStream;
/* .line 1075 */
/* .restart local v8 # "fos":Ljava/io/FileOutputStream; */
/* :catchall_0 */
/* move-exception v0 */
/* move-object v9, v0 */
try { // :try_start_4
(( java.io.FileOutputStream ) v8 ).close ( ); // invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* :catchall_1 */
/* move-exception v0 */
/* move-object v10, v0 */
try { // :try_start_5
(( java.lang.Throwable ) v9 ).addSuppressed ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v2 # "extraPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
} // .end local v7 # "tracesFile":Ljava/io/File;
} // .end local p0 # "this":Lcom/android/server/am/ActivityManagerServiceImpl;
} // .end local p1 # "firstPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
} // .end local p2 # "lastPids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/Boolean;>;"
} // .end local p3 # "nativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
} // .end local p4 # "subject":Ljava/lang/String;
} // .end local p5 # "path":Ljava/lang/String;
} // :goto_0
/* throw v9 */
/* :try_end_5 */
/* .catch Ljava/io/IOException; {:try_start_5 ..:try_end_5} :catch_0 */
/* .line 1078 */
} // .end local v8 # "fos":Ljava/io/FileOutputStream;
/* .restart local v2 # "extraPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* .restart local v7 # "tracesFile":Ljava/io/File; */
/* .restart local p0 # "this":Lcom/android/server/am/ActivityManagerServiceImpl; */
/* .restart local p1 # "firstPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* .restart local p2 # "lastPids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/Boolean;>;" */
/* .restart local p3 # "nativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* .restart local p4 # "subject":Ljava/lang/String; */
/* .restart local p5 # "path":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v0 */
/* .line 1079 */
/* .local v0, "e":Ljava/io/IOException; */
final String v8 = "Exception writing subject to scout dump file:"; // const-string v8, "Exception writing subject to scout dump file:"
android.util.Slog .w ( v5,v8,v0 );
/* .line 1083 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :cond_1
} // :goto_1
/* nop */
/* .line 1084 */
(( java.io.File ) v7 ).getAbsolutePath ( ); // invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
/* invoke-static/range {p3 ..p3}, Ljava/util/concurrent/CompletableFuture;->completedFuture(Ljava/lang/Object;)Ljava/util/concurrent/CompletableFuture; */
java.util.concurrent.CompletableFuture .completedFuture ( v2 );
int v13 = 0; // const/4 v13, 0x0
int v14 = 0; // const/4 v14, 0x0
/* .line 1083 */
/* move-object/from16 v10, p1 */
/* invoke-static/range {v9 ..v14}, Lcom/android/server/am/StackTracesDumpHelper;->dumpStackTraces(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Lcom/android/internal/os/anr/AnrLatencyTracker;)J */
/* .line 1086 */
/* .line 1070 */
/* :catch_1 */
/* move-exception v0 */
/* .line 1071 */
/* .restart local v0 # "e":Ljava/io/IOException; */
final String v8 = "Exception creating scout dump file:"; // const-string v8, "Exception creating scout dump file:"
android.util.Slog .w ( v5,v8,v0 );
/* .line 1072 */
int v5 = 0; // const/4 v5, 0x0
} // .end method
public void dumpLogText ( java.io.PrintWriter p0 ) {
/* .locals 5 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 1719 */
final String v0 = "ACTIVITY MANAGER LOGGING (dumpsys activity logging)"; // const-string v0, "ACTIVITY MANAGER LOGGING (dumpsys activity logging)"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1720 */
/* new-instance v0, Landroid/util/Pair; */
final String v1 = ""; // const-string v1, ""
/* invoke-direct {v0, v1, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 1721 */
/* .local v0, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;" */
v1 = this.first;
/* check-cast v1, Ljava/lang/String; */
v2 = this.second;
/* check-cast v2, Ljava/lang/String; */
/* sget-boolean v3, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_BACKGROUND_CHECK:Z */
final String v4 = "DEBUG_BACKGROUND_CHECK"; // const-string v4, "DEBUG_BACKGROUND_CHECK"
/* invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair; */
/* .line 1722 */
v1 = this.first;
/* check-cast v1, Ljava/lang/String; */
v2 = this.second;
/* check-cast v2, Ljava/lang/String; */
/* sget-boolean v3, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_BROADCAST:Z */
final String v4 = "DEBUG_BROADCAST"; // const-string v4, "DEBUG_BROADCAST"
/* invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair; */
/* .line 1723 */
v1 = this.first;
/* check-cast v1, Ljava/lang/String; */
v2 = this.second;
/* check-cast v2, Ljava/lang/String; */
/* sget-boolean v3, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_BROADCAST_BACKGROUND:Z */
final String v4 = "DEBUG_BROADCAST_BACKGROUND"; // const-string v4, "DEBUG_BROADCAST_BACKGROUND"
/* invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair; */
/* .line 1724 */
v1 = this.first;
/* check-cast v1, Ljava/lang/String; */
v2 = this.second;
/* check-cast v2, Ljava/lang/String; */
/* sget-boolean v3, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_BROADCAST_LIGHT:Z */
final String v4 = "DEBUG_BROADCAST_LIGHT"; // const-string v4, "DEBUG_BROADCAST_LIGHT"
/* invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair; */
/* .line 1725 */
v1 = this.first;
/* check-cast v1, Ljava/lang/String; */
v2 = this.second;
/* check-cast v2, Ljava/lang/String; */
/* sget-boolean v3, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_BROADCAST_DEFERRAL:Z */
final String v4 = "DEBUG_BROADCAST_DEFERRAL"; // const-string v4, "DEBUG_BROADCAST_DEFERRAL"
/* invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair; */
/* .line 1726 */
v1 = this.first;
/* check-cast v1, Ljava/lang/String; */
v2 = this.second;
/* check-cast v2, Ljava/lang/String; */
/* sget-boolean v3, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_PROVIDER:Z */
final String v4 = "DEBUG_PROVIDER"; // const-string v4, "DEBUG_PROVIDER"
/* invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair; */
/* .line 1727 */
v1 = this.first;
/* check-cast v1, Ljava/lang/String; */
v2 = this.second;
/* check-cast v2, Ljava/lang/String; */
/* sget-boolean v3, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_SERVICE:Z */
final String v4 = "DEBUG_SERVICE"; // const-string v4, "DEBUG_SERVICE"
/* invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair; */
/* .line 1728 */
v1 = this.first;
/* check-cast v1, Ljava/lang/String; */
v2 = this.second;
/* check-cast v2, Ljava/lang/String; */
/* sget-boolean v3, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_FOREGROUND_SERVICE:Z */
final String v4 = "DEBUG_FOREGROUND_SERVICE"; // const-string v4, "DEBUG_FOREGROUND_SERVICE"
/* invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair; */
/* .line 1729 */
v1 = this.first;
/* check-cast v1, Ljava/lang/String; */
v2 = this.second;
/* check-cast v2, Ljava/lang/String; */
/* sget-boolean v3, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_SERVICE_EXECUTING:Z */
final String v4 = "DEBUG_SERVICE_EXECUTING"; // const-string v4, "DEBUG_SERVICE_EXECUTING"
/* invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair; */
/* .line 1730 */
v1 = this.first;
/* check-cast v1, Ljava/lang/String; */
v2 = this.second;
/* check-cast v2, Ljava/lang/String; */
/* sget-boolean v3, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_ALLOWLISTS:Z */
final String v4 = "DEBUG_ALLOWLISTS"; // const-string v4, "DEBUG_ALLOWLISTS"
/* invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair; */
/* .line 1731 */
v1 = this.first;
/* check-cast v1, Ljava/lang/String; */
v2 = this.second;
/* check-cast v2, Ljava/lang/String; */
/* sget-boolean v3, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_RECENTS:Z */
final String v4 = "DEBUG_RECENTS"; // const-string v4, "DEBUG_RECENTS"
/* invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair; */
/* .line 1732 */
v1 = this.first;
/* check-cast v1, Ljava/lang/String; */
v2 = this.second;
/* check-cast v2, Ljava/lang/String; */
/* sget-boolean v3, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_RECENTS_TRIM_TASKS:Z */
final String v4 = "DEBUG_RECENTS_TRIM_TASKS"; // const-string v4, "DEBUG_RECENTS_TRIM_TASKS"
/* invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair; */
/* .line 1733 */
v1 = this.first;
/* check-cast v1, Ljava/lang/String; */
v2 = this.second;
/* check-cast v2, Ljava/lang/String; */
/* sget-boolean v3, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_ROOT_TASK:Z */
final String v4 = "DEBUG_ROOT_TASK"; // const-string v4, "DEBUG_ROOT_TASK"
/* invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair; */
/* .line 1734 */
v1 = this.first;
/* check-cast v1, Ljava/lang/String; */
v2 = this.second;
/* check-cast v2, Ljava/lang/String; */
/* sget-boolean v3, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_SWITCH:Z */
final String v4 = "DEBUG_SWITCH"; // const-string v4, "DEBUG_SWITCH"
/* invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair; */
/* .line 1735 */
v1 = this.first;
/* check-cast v1, Ljava/lang/String; */
v2 = this.second;
/* check-cast v2, Ljava/lang/String; */
/* sget-boolean v3, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_TRANSITION:Z */
final String v4 = "DEBUG_TRANSITION"; // const-string v4, "DEBUG_TRANSITION"
/* invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair; */
/* .line 1736 */
v1 = this.first;
/* check-cast v1, Ljava/lang/String; */
v2 = this.second;
/* check-cast v2, Ljava/lang/String; */
/* sget-boolean v3, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_VISIBILITY:Z */
final String v4 = "DEBUG_VISIBILITY"; // const-string v4, "DEBUG_VISIBILITY"
/* invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair; */
/* .line 1737 */
v1 = this.first;
/* check-cast v1, Ljava/lang/String; */
v2 = this.second;
/* check-cast v2, Ljava/lang/String; */
/* sget-boolean v3, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_APP:Z */
final String v4 = "DEBUG_APP"; // const-string v4, "DEBUG_APP"
/* invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair; */
/* .line 1738 */
v1 = this.first;
/* check-cast v1, Ljava/lang/String; */
v2 = this.second;
/* check-cast v2, Ljava/lang/String; */
/* sget-boolean v3, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_IDLE:Z */
final String v4 = "DEBUG_IDLE"; // const-string v4, "DEBUG_IDLE"
/* invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair; */
/* .line 1739 */
v1 = this.first;
/* check-cast v1, Ljava/lang/String; */
v2 = this.second;
/* check-cast v2, Ljava/lang/String; */
/* sget-boolean v3, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_RELEASE:Z */
final String v4 = "DEBUG_RELEASE"; // const-string v4, "DEBUG_RELEASE"
/* invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair; */
/* .line 1740 */
v1 = this.first;
/* check-cast v1, Ljava/lang/String; */
v2 = this.second;
/* check-cast v2, Ljava/lang/String; */
/* sget-boolean v3, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_USER_LEAVING:Z */
final String v4 = "DEBUG_USER_LEAVING"; // const-string v4, "DEBUG_USER_LEAVING"
/* invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair; */
/* .line 1741 */
v1 = this.first;
/* check-cast v1, Ljava/lang/String; */
v2 = this.second;
/* check-cast v2, Ljava/lang/String; */
/* sget-boolean v3, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_PERMISSIONS_REVIEW:Z */
final String v4 = "DEBUG_PERMISSIONS_REVIEW"; // const-string v4, "DEBUG_PERMISSIONS_REVIEW"
/* invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair; */
/* .line 1742 */
v1 = this.first;
/* check-cast v1, Ljava/lang/String; */
v2 = this.second;
/* check-cast v2, Ljava/lang/String; */
/* sget-boolean v3, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_RESULTS:Z */
final String v4 = "DEBUG_RESULTS"; // const-string v4, "DEBUG_RESULTS"
/* invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair; */
/* .line 1743 */
v1 = this.first;
/* check-cast v1, Ljava/lang/String; */
v2 = this.second;
/* check-cast v2, Ljava/lang/String; */
/* sget-boolean v3, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_ACTIVITY_STARTS:Z */
final String v4 = "DEBUG_ACTIVITY_STARTS"; // const-string v4, "DEBUG_ACTIVITY_STARTS"
/* invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair; */
/* .line 1744 */
v1 = this.first;
/* check-cast v1, Ljava/lang/String; */
v2 = this.second;
/* check-cast v2, Ljava/lang/String; */
/* sget-boolean v3, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_CLEANUP:Z */
final String v4 = "DEBUG_CLEANUP"; // const-string v4, "DEBUG_CLEANUP"
/* invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair; */
/* .line 1745 */
v1 = this.first;
/* check-cast v1, Ljava/lang/String; */
v2 = this.second;
/* check-cast v2, Ljava/lang/String; */
/* sget-boolean v3, Lcom/android/server/wm/ActivityTaskManagerDebugConfig;->DEBUG_METRICS:Z */
final String v4 = "DEBUG_METRICS"; // const-string v4, "DEBUG_METRICS"
/* invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->generateGroups(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Landroid/util/Pair; */
/* .line 1746 */
final String v1 = "Enabled log groups:"; // const-string v1, "Enabled log groups:"
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1747 */
v1 = this.first;
/* check-cast v1, Ljava/lang/String; */
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1748 */
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 1749 */
final String v1 = "Disabled log groups:"; // const-string v1, "Disabled log groups:"
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1750 */
v1 = this.second;
/* check-cast v1, Ljava/lang/String; */
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1751 */
return;
} // .end method
public void dumpMiuiJavaTrace ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "pid" # I */
/* .line 1213 */
v0 = android.os.Process .getThreadGroupLeader ( p1 );
/* if-ne v0, p1, :cond_0 */
/* .line 1214 */
final String v0 = "MIUI ANR"; // const-string v0, "MIUI ANR"
v1 = com.android.server.ScoutHelper .getOomAdjOfPid ( v0,p1 );
/* const/16 v2, -0x3e8 */
/* if-le v1, v2, :cond_0 */
/* .line 1215 */
int v1 = 3; // const/4 v1, 0x3
android.os.Process .sendSignal ( p1,v1 );
/* .line 1216 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "[Scout] Send SIGNAL_QUIT to generate java stack dump.Pid:"; // const-string v2, "[Scout] Send SIGNAL_QUIT to generate java stack dump.Pid:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v1 );
/* .line 1218 */
} // :cond_0
return;
} // .end method
public java.lang.String dumpMiuiStackTraces ( Integer[] p0 ) {
/* .locals 14 */
/* .param p1, "pids" # [I */
/* .line 967 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 968 */
/* .local v0, "callingUid":I */
v1 = android.os.UserHandle .getAppId ( v0 );
/* const/16 v2, 0x3e8 */
/* if-ne v1, v2, :cond_6 */
/* .line 972 */
/* array-length v1, p1 */
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
/* if-ge v1, v3, :cond_0 */
/* .line 973 */
/* .line 975 */
} // :cond_0
/* new-instance v1, Ljava/util/ArrayList; */
int v4 = 3; // const/4 v4, 0x3
/* invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V */
/* .line 976 */
/* .local v1, "javapids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* new-instance v5, Ljava/util/ArrayList; */
/* invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(I)V */
/* move-object v4, v5 */
/* .line 977 */
/* .local v4, "nativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "i":I */
} // :goto_0
/* array-length v6, p1 */
/* if-ge v5, v6, :cond_4 */
/* .line 978 */
final String v6 = "ActivityManagerServiceImpl"; // const-string v6, "ActivityManagerServiceImpl"
/* aget v7, p1, v5 */
v6 = com.android.server.ScoutHelper .getOomAdjOfPid ( v6,v7 );
/* .line 979 */
/* .local v6, "adj":I */
v7 = com.android.server.ScoutHelper .checkIsJavaOrNativeProcess ( v6 );
/* .line 980 */
/* .local v7, "isJavaOrNativeProcess":I */
/* if-nez v7, :cond_1 */
/* .line 981 */
/* .line 982 */
} // :cond_1
/* if-ne v7, v3, :cond_2 */
/* .line 983 */
/* aget v8, p1, v5 */
java.lang.Integer .valueOf ( v8 );
(( java.util.ArrayList ) v1 ).add ( v8 ); // invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 984 */
} // :cond_2
int v8 = 2; // const/4 v8, 0x2
/* if-ne v7, v8, :cond_3 */
/* .line 985 */
/* aget v8, p1, v5 */
java.lang.Integer .valueOf ( v8 );
(( java.util.ArrayList ) v4 ).add ( v8 ); // invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 977 */
} // .end local v6 # "adj":I
} // .end local v7 # "isJavaOrNativeProcess":I
} // :cond_3
} // :goto_1
/* add-int/lit8 v5, v5, 0x1 */
/* .line 988 */
} // .end local v5 # "i":I
} // :cond_4
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
java.util.concurrent.CompletableFuture .completedFuture ( v4 );
int v9 = 0; // const/4 v9, 0x0
final String v10 = "App Scout Exception"; // const-string v10, "App Scout Exception"
int v11 = 0; // const/4 v11, 0x0
int v12 = 0; // const/4 v12, 0x0
int v13 = 0; // const/4 v13, 0x0
/* move-object v5, v1 */
/* invoke-static/range {v5 ..v13}, Lcom/android/server/am/StackTracesDumpHelper;->dumpStackTraces(Ljava/util/ArrayList;Lcom/android/internal/os/ProcessCpuTracker;Landroid/util/SparseBooleanArray;Ljava/util/concurrent/Future;Ljava/io/StringWriter;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/Executor;Lcom/android/internal/os/anr/AnrLatencyTracker;)Ljava/io/File; */
/* .line 990 */
/* .local v3, "mTraceFile":Ljava/io/File; */
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 991 */
(( java.io.File ) v3 ).getAbsolutePath ( ); // invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
/* .line 993 */
} // :cond_5
/* .line 969 */
} // .end local v1 # "javapids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
} // .end local v3 # "mTraceFile":Ljava/io/File;
} // .end local v4 # "nativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
} // :cond_6
/* new-instance v1, Ljava/lang/SecurityException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Only the system process can call dumpMiuiStackTraces, received request from uid: "; // const-string v3, "Only the system process can call dumpMiuiStackTraces, received request from uid: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
public void dumpMiuiStackTracesForCmdlines ( java.lang.String[] p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 17 */
/* .param p1, "cmdlines" # [Ljava/lang/String; */
/* .param p2, "path" # Ljava/lang/String; */
/* .param p3, "subject" # Ljava/lang/String; */
/* .line 999 */
/* move-object/from16 v1, p1 */
final String v2 = "ActivityManagerServiceImpl"; // const-string v2, "ActivityManagerServiceImpl"
if ( v1 != null) { // if-eqz v1, :cond_a
/* array-length v0, v1 */
int v3 = 1; // const/4 v3, 0x1
/* if-lt v0, v3, :cond_a */
v0 = /* invoke-static/range {p2 ..p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* move-object/from16 v7, p2 */
/* move-object/from16 v10, p3 */
/* goto/16 :goto_7 */
/* .line 1004 */
} // :cond_0
v4 = android.os.Binder .getCallingUid ( );
/* .line 1005 */
/* .local v4, "callingUid":I */
v0 = android.os.UserHandle .getAppId ( v4 );
/* const/16 v5, 0x3e8 */
final String v6 = "MIUIScout App"; // const-string v6, "MIUIScout App"
/* if-eq v0, v5, :cond_1 */
/* .line 1006 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Only system process can call dumpMiuiStackTracesForCmdlines, received request from uid:"; // const-string v2, "Only system process can call dumpMiuiStackTracesForCmdlines, received request from uid:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v6,v0 );
/* .line 1008 */
return;
/* .line 1012 */
} // :cond_1
/* invoke-static/range {p1 ..p1}, Landroid/os/Process;->getPidsForCommands([Ljava/lang/String;)[I */
/* .line 1013 */
/* .local v5, "pids":[I */
if ( v5 != null) { // if-eqz v5, :cond_9
/* array-length v0, v5 */
/* if-ge v0, v3, :cond_2 */
/* move-object/from16 v7, p2 */
/* move-object/from16 v10, p3 */
/* goto/16 :goto_6 */
/* .line 1019 */
} // :cond_2
/* new-instance v0, Ljava/io/File; */
/* move-object/from16 v7, p2 */
/* invoke-direct {v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* move-object v8, v0 */
/* .line 1021 */
/* .local v8, "tracesFile":Ljava/io/File; */
try { // :try_start_0
v0 = (( java.io.File ) v8 ).createNewFile ( ); // invoke-virtual {v8}, Ljava/io/File;->createNewFile()Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 1022 */
(( java.io.File ) v8 ).getAbsolutePath ( ); // invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
/* const/16 v9, 0x180 */
int v10 = -1; // const/4 v10, -0x1
android.os.FileUtils .setPermissions ( v0,v9,v10,v10 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .line 1027 */
} // :cond_3
/* nop */
/* .line 1030 */
v0 = /* invoke-static/range {p3 ..p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z */
/* if-nez v0, :cond_4 */
/* .line 1031 */
try { // :try_start_1
/* new-instance v0, Ljava/io/FileOutputStream; */
/* invoke-direct {v0, v8, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V */
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_1 */
/* move-object v9, v0 */
/* .line 1032 */
/* .local v9, "fos":Ljava/io/FileOutputStream; */
try { // :try_start_2
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "Subject: "; // const-string v10, "Subject: "
(( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* move-object/from16 v10, p3 */
try { // :try_start_3
(( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = "\n"; // const-string v11, "\n"
(( java.lang.StringBuilder ) v0 ).append ( v11 ); // invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1033 */
/* .local v0, "header":Ljava/lang/String; */
v11 = java.nio.charset.StandardCharsets.UTF_8;
(( java.lang.String ) v0 ).getBytes ( v11 ); // invoke-virtual {v0, v11}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B
(( java.io.FileOutputStream ) v9 ).write ( v11 ); // invoke-virtual {v9, v11}, Ljava/io/FileOutputStream;->write([B)V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 1034 */
} // .end local v0 # "header":Ljava/lang/String;
try { // :try_start_4
(( java.io.FileOutputStream ) v9 ).close ( ); // invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 1036 */
} // .end local v9 # "fos":Ljava/io/FileOutputStream;
/* .line 1031 */
/* .restart local v9 # "fos":Ljava/io/FileOutputStream; */
/* :catchall_0 */
/* move-exception v0 */
/* :catchall_1 */
/* move-exception v0 */
/* move-object/from16 v10, p3 */
} // :goto_0
/* move-object v11, v0 */
try { // :try_start_5
(( java.io.FileOutputStream ) v9 ).close ( ); // invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_2 */
/* :catchall_2 */
/* move-exception v0 */
/* move-object v12, v0 */
try { // :try_start_6
(( java.lang.Throwable ) v11 ).addSuppressed ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v4 # "callingUid":I
} // .end local v5 # "pids":[I
} // .end local v8 # "tracesFile":Ljava/io/File;
} // .end local p0 # "this":Lcom/android/server/am/ActivityManagerServiceImpl;
} // .end local p1 # "cmdlines":[Ljava/lang/String;
} // .end local p2 # "path":Ljava/lang/String;
} // .end local p3 # "subject":Ljava/lang/String;
} // :goto_1
/* throw v11 */
/* :try_end_6 */
/* .catch Ljava/io/IOException; {:try_start_6 ..:try_end_6} :catch_0 */
/* .line 1034 */
} // .end local v9 # "fos":Ljava/io/FileOutputStream;
/* .restart local v4 # "callingUid":I */
/* .restart local v5 # "pids":[I */
/* .restart local v8 # "tracesFile":Ljava/io/File; */
/* .restart local p0 # "this":Lcom/android/server/am/ActivityManagerServiceImpl; */
/* .restart local p1 # "cmdlines":[Ljava/lang/String; */
/* .restart local p2 # "path":Ljava/lang/String; */
/* .restart local p3 # "subject":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v0 */
/* :catch_1 */
/* move-exception v0 */
/* move-object/from16 v10, p3 */
/* .line 1035 */
/* .local v0, "e":Ljava/io/IOException; */
} // :goto_2
final String v9 = "Exception writing subject to scout dump file:"; // const-string v9, "Exception writing subject to scout dump file:"
android.util.Slog .w ( v6,v9,v0 );
/* .line 1030 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :cond_4
/* move-object/from16 v10, p3 */
/* .line 1040 */
} // :goto_3
/* new-instance v0, Ljava/util/ArrayList; */
int v6 = 4; // const/4 v6, 0x4
/* invoke-direct {v0, v6}, Ljava/util/ArrayList;-><init>(I)V */
/* .line 1041 */
/* .local v0, "javaPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* new-instance v9, Ljava/util/ArrayList; */
/* invoke-direct {v9, v6}, Ljava/util/ArrayList;-><init>(I)V */
/* move-object v6, v9 */
/* .line 1042 */
/* .local v6, "nativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
int v9 = 0; // const/4 v9, 0x0
/* .local v9, "i":I */
} // :goto_4
/* array-length v11, v5 */
/* if-ge v9, v11, :cond_8 */
/* .line 1043 */
/* aget v11, v5, v9 */
v11 = com.android.server.ScoutHelper .getOomAdjOfPid ( v2,v11 );
/* .line 1044 */
/* .local v11, "adj":I */
v12 = com.android.server.ScoutHelper .checkIsJavaOrNativeProcess ( v11 );
/* .line 1045 */
/* .local v12, "isJavaOrNativeProcess":I */
/* if-nez v12, :cond_5 */
/* .line 1046 */
/* .line 1047 */
} // :cond_5
/* if-ne v12, v3, :cond_6 */
/* .line 1048 */
/* aget v13, v5, v9 */
java.lang.Integer .valueOf ( v13 );
(( java.util.ArrayList ) v0 ).add ( v13 ); // invoke-virtual {v0, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 1049 */
} // :cond_6
int v13 = 2; // const/4 v13, 0x2
/* if-ne v12, v13, :cond_7 */
/* .line 1050 */
/* aget v13, v5, v9 */
java.lang.Integer .valueOf ( v13 );
(( java.util.ArrayList ) v6 ).add ( v13 ); // invoke-virtual {v6, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 1042 */
} // .end local v11 # "adj":I
} // .end local v12 # "isJavaOrNativeProcess":I
} // :cond_7
} // :goto_5
/* add-int/lit8 v9, v9, 0x1 */
/* .line 1053 */
} // .end local v9 # "i":I
} // :cond_8
/* nop */
/* .line 1054 */
(( java.io.File ) v8 ).getAbsolutePath ( ); // invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
/* .line 1055 */
java.util.concurrent.CompletableFuture .completedFuture ( v6 );
int v14 = 0; // const/4 v14, 0x0
int v15 = 0; // const/4 v15, 0x0
/* const/16 v16, 0x0 */
/* .line 1053 */
/* move-object v12, v0 */
/* invoke-static/range {v11 ..v16}, Lcom/android/server/am/StackTracesDumpHelper;->dumpStackTraces(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Lcom/android/internal/os/anr/AnrLatencyTracker;)J */
/* .line 1057 */
return;
/* .line 1024 */
} // .end local v0 # "javaPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
} // .end local v6 # "nativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
/* :catch_2 */
/* move-exception v0 */
/* move-object/from16 v10, p3 */
/* .line 1025 */
/* .local v0, "e":Ljava/io/IOException; */
final String v2 = "Exception creating scout dump file:"; // const-string v2, "Exception creating scout dump file:"
android.util.Slog .w ( v6,v2,v0 );
/* .line 1026 */
return;
/* .line 1013 */
} // .end local v0 # "e":Ljava/io/IOException;
} // .end local v8 # "tracesFile":Ljava/io/File;
} // :cond_9
/* move-object/from16 v7, p2 */
/* move-object/from16 v10, p3 */
/* .line 1014 */
} // :goto_6
final String v0 = "dumpStackTraces failed, no pid found, "; // const-string v0, "dumpStackTraces failed, no pid found, "
android.util.Slog .w ( v2,v0 );
/* .line 1015 */
return;
/* .line 999 */
} // .end local v4 # "callingUid":I
} // .end local v5 # "pids":[I
} // :cond_a
/* move-object/from16 v7, p2 */
/* move-object/from16 v10, p3 */
/* .line 1000 */
} // :goto_7
final String v0 = "dumpStackTraces failed, Invalidate param!"; // const-string v0, "dumpStackTraces failed, Invalidate param!"
android.util.Slog .w ( v2,v0 );
/* .line 1001 */
return;
} // .end method
public java.io.File dumpOneProcessTraces ( Integer p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 11 */
/* .param p1, "pid" # I */
/* .param p2, "path" # Ljava/lang/String; */
/* .param p3, "subject" # Ljava/lang/String; */
/* .line 1091 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 1092 */
/* .local v0, "firstPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* move-object v7, v1 */
/* .line 1093 */
/* .local v7, "nativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
final String v1 = "ActivityManagerServiceImpl"; // const-string v1, "ActivityManagerServiceImpl"
v8 = com.android.server.ScoutHelper .getOomAdjOfPid ( v1,p1 );
/* .line 1094 */
/* .local v8, "adj":I */
v9 = com.android.server.ScoutHelper .checkIsJavaOrNativeProcess ( v8 );
/* .line 1095 */
/* .local v9, "isJavaOrNativeProcess":I */
int v1 = 0; // const/4 v1, 0x0
final String v2 = "MIUIScout App"; // const-string v2, "MIUIScout App"
int v3 = 1; // const/4 v3, 0x1
/* if-ne v9, v3, :cond_0 */
/* .line 1096 */
java.lang.Integer .valueOf ( p1 );
(( java.util.ArrayList ) v0 ).add ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 1097 */
} // :cond_0
int v4 = 2; // const/4 v4, 0x2
/* if-ne v9, v4, :cond_3 */
/* .line 1098 */
java.lang.Integer .valueOf ( p1 );
(( java.util.ArrayList ) v7 ).add ( v4 ); // invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 1104 */
} // :goto_0
/* new-instance v4, Ljava/io/File; */
/* invoke-direct {v4, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* move-object v10, v4 */
/* .line 1106 */
/* .local v10, "traceFile":Ljava/io/File; */
try { // :try_start_0
v4 = (( java.io.File ) v10 ).createNewFile ( ); // invoke-virtual {v10}, Ljava/io/File;->createNewFile()Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 1107 */
(( java.io.File ) v10 ).getAbsolutePath ( ); // invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
/* const/16 v5, 0x180 */
int v6 = -1; // const/4 v6, -0x1
android.os.FileUtils .setPermissions ( v4,v5,v6,v6 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .line 1112 */
} // :cond_1
/* nop */
/* .line 1114 */
if ( p3 != null) { // if-eqz p3, :cond_2
/* .line 1115 */
try { // :try_start_1
/* new-instance v1, Ljava/io/FileOutputStream; */
/* invoke-direct {v1, v10, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V */
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 1116 */
/* .local v1, "fos":Ljava/io/FileOutputStream; */
try { // :try_start_2
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Subject: "; // const-string v4, "Subject: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p3 ); // invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "\n"; // const-string v4, "\n"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1117 */
/* .local v3, "header":Ljava/lang/String; */
v4 = java.nio.charset.StandardCharsets.UTF_8;
(( java.lang.String ) v3 ).getBytes ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B
(( java.io.FileOutputStream ) v1 ).write ( v4 ); // invoke-virtual {v1, v4}, Ljava/io/FileOutputStream;->write([B)V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 1118 */
} // .end local v3 # "header":Ljava/lang/String;
try { // :try_start_3
(( java.io.FileOutputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 1120 */
} // .end local v1 # "fos":Ljava/io/FileOutputStream;
/* .line 1115 */
/* .restart local v1 # "fos":Ljava/io/FileOutputStream; */
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_4
(( java.io.FileOutputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* :catchall_1 */
/* move-exception v4 */
try { // :try_start_5
(( java.lang.Throwable ) v3 ).addSuppressed ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "firstPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
} // .end local v7 # "nativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
} // .end local v8 # "adj":I
} // .end local v9 # "isJavaOrNativeProcess":I
} // .end local v10 # "traceFile":Ljava/io/File;
} // .end local p0 # "this":Lcom/android/server/am/ActivityManagerServiceImpl;
} // .end local p1 # "pid":I
} // .end local p2 # "path":Ljava/lang/String;
} // .end local p3 # "subject":Ljava/lang/String;
} // :goto_1
/* throw v3 */
/* :try_end_5 */
/* .catch Ljava/io/IOException; {:try_start_5 ..:try_end_5} :catch_0 */
/* .line 1118 */
} // .end local v1 # "fos":Ljava/io/FileOutputStream;
/* .restart local v0 # "firstPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* .restart local v7 # "nativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* .restart local v8 # "adj":I */
/* .restart local v9 # "isJavaOrNativeProcess":I */
/* .restart local v10 # "traceFile":Ljava/io/File; */
/* .restart local p0 # "this":Lcom/android/server/am/ActivityManagerServiceImpl; */
/* .restart local p1 # "pid":I */
/* .restart local p2 # "path":Ljava/lang/String; */
/* .restart local p3 # "subject":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v1 */
/* .line 1119 */
/* .local v1, "e":Ljava/io/IOException; */
final String v3 = "Exception writing subject to scout dump file:"; // const-string v3, "Exception writing subject to scout dump file:"
android.util.Slog .w ( v2,v3,v1 );
/* .line 1123 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :cond_2
} // :goto_2
(( java.io.File ) v10 ).getAbsolutePath ( ); // invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
java.util.concurrent.CompletableFuture .completedFuture ( v7 );
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
int v6 = 0; // const/4 v6, 0x0
/* move-object v2, v0 */
/* invoke-static/range {v1 ..v6}, Lcom/android/server/am/StackTracesDumpHelper;->dumpStackTraces(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Lcom/android/internal/os/anr/AnrLatencyTracker;)J */
/* .line 1125 */
/* .line 1109 */
/* :catch_1 */
/* move-exception v3 */
/* .line 1110 */
/* .local v3, "e":Ljava/io/IOException; */
final String v4 = "Exception creating scout dump file:"; // const-string v4, "Exception creating scout dump file:"
android.util.Slog .w ( v2,v4,v3 );
/* .line 1111 */
/* .line 1100 */
} // .end local v3 # "e":Ljava/io/IOException;
} // .end local v10 # "traceFile":Ljava/io/File;
} // :cond_3
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "can not distinguish for this process\'s adj"; // const-string v4, "can not distinguish for this process\'s adj"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v8 ); // invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v2,v3 );
/* .line 1101 */
} // .end method
public void dumpSystemTraces ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "path" # Ljava/lang/String; */
/* .line 1130 */
v0 = com.android.server.am.ActivityManagerServiceImpl.requestDumpTraceCount;
(( java.util.concurrent.atomic.AtomicInteger ) v0 ).getAndIncrement ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I
/* .line 1132 */
v0 = com.android.server.am.ActivityManagerServiceImpl.requestDumpTraceCount;
v0 = (( java.util.concurrent.atomic.AtomicInteger ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
/* if-lez v0, :cond_2 */
v0 = com.android.server.am.ActivityManagerServiceImpl.dumpFlag;
v0 = (( java.util.concurrent.atomic.AtomicBoolean ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
/* if-nez v0, :cond_2 */
/* .line 1133 */
v0 = com.android.server.am.ActivityManagerServiceImpl.requestDumpTraceCount;
(( java.util.concurrent.atomic.AtomicInteger ) v0 ).getAndDecrement ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndDecrement()I
/* .line 1134 */
v0 = com.android.server.am.ActivityManagerServiceImpl.dumpFlag;
int v1 = 1; // const/4 v1, 0x1
(( java.util.concurrent.atomic.AtomicBoolean ) v0 ).set ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 1135 */
java.util.concurrent.Executors .newSingleThreadExecutor ( );
/* .line 1137 */
/* .local v0, "executor":Ljava/util/concurrent/ExecutorService; */
try { // :try_start_0
/* new-instance v1, Lcom/android/server/am/ActivityManagerServiceImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/am/ActivityManagerServiceImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/am/ActivityManagerServiceImpl;Ljava/lang/String;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1199 */
final String v1 = "MIUIScout App"; // const-string v1, "MIUIScout App"
final String v2 = "dumpSystemTraces finally shutdown."; // const-string v2, "dumpSystemTraces finally shutdown."
android.util.Slog .w ( v1,v2 );
/* .line 1200 */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :goto_0
/* .line 1199 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 1196 */
/* :catch_0 */
/* move-exception v1 */
/* .line 1197 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_1
final String v2 = "MIUIScout App"; // const-string v2, "MIUIScout App"
final String v3 = "Exception occurs while dumping system scout trace file:"; // const-string v3, "Exception occurs while dumping system scout trace file:"
android.util.Slog .w ( v2,v3,v1 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1199 */
/* nop */
} // .end local v1 # "e":Ljava/lang/Exception;
final String v1 = "MIUIScout App"; // const-string v1, "MIUIScout App"
final String v2 = "dumpSystemTraces finally shutdown."; // const-string v2, "dumpSystemTraces finally shutdown."
android.util.Slog .w ( v1,v2 );
/* .line 1200 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1203 */
} // .end local v0 # "executor":Ljava/util/concurrent/ExecutorService;
} // :cond_0
} // :goto_1
/* .line 1199 */
/* .restart local v0 # "executor":Ljava/util/concurrent/ExecutorService; */
} // :goto_2
final String v2 = "MIUIScout App"; // const-string v2, "MIUIScout App"
final String v3 = "dumpSystemTraces finally shutdown."; // const-string v3, "dumpSystemTraces finally shutdown."
android.util.Slog .w ( v2,v3 );
/* .line 1200 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1201 */
} // :cond_1
/* throw v1 */
/* .line 1204 */
} // .end local v0 # "executor":Ljava/util/concurrent/ExecutorService;
} // :cond_2
v0 = com.android.server.am.ActivityManagerServiceImpl.dumpTraceRequestList;
/* monitor-enter v0 */
/* .line 1205 */
try { // :try_start_2
v1 = com.android.server.am.ActivityManagerServiceImpl.dumpTraceRequestList;
(( java.util.ArrayList ) v1 ).add ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 1206 */
/* monitor-exit v0 */
/* .line 1209 */
} // :goto_3
return;
/* .line 1206 */
/* :catchall_1 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* throw v1 */
} // .end method
public void enableAmsDebugConfig ( java.lang.String p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "config" # Ljava/lang/String; */
/* .param p2, "enable" # Z */
/* .line 1498 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "enableAMSDebugConfig, config=:"; // const-string v1, "enableAMSDebugConfig, config=:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", enable=:"; // const-string v1, ", enable=:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "ActivityManagerServiceImpl"; // const-string v1, "ActivityManagerServiceImpl"
android.util.Slog .d ( v1,v0 );
/* .line 1499 */
v0 = (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
/* sparse-switch v0, :sswitch_data_0 */
} // :cond_0
/* goto/16 :goto_0 */
/* :sswitch_0 */
final String v0 = "DEBUG_OOM_ADJ"; // const-string v0, "DEBUG_OOM_ADJ"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0xc */
/* goto/16 :goto_1 */
/* :sswitch_1 */
final String v0 = "DEBUG_BROADCAST_DEFERRAL"; // const-string v0, "DEBUG_BROADCAST_DEFERRAL"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 6; // const/4 v0, 0x6
/* goto/16 :goto_1 */
/* :sswitch_2 */
final String v0 = "DEBUG_POWER"; // const-string v0, "DEBUG_POWER"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0xe */
/* goto/16 :goto_1 */
/* :sswitch_3 */
final String v0 = "DEBUG_PROVIDER"; // const-string v0, "DEBUG_PROVIDER"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x12 */
/* goto/16 :goto_1 */
/* :sswitch_4 */
final String v0 = "DEBUG_SWITCH"; // const-string v0, "DEBUG_SWITCH"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x1e */
/* goto/16 :goto_1 */
/* :sswitch_5 */
final String v0 = "DEBUG_USAGE_STATS"; // const-string v0, "DEBUG_USAGE_STATS"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x18 */
/* goto/16 :goto_1 */
/* :sswitch_6 */
final String v0 = "DEBUG_BACKGROUND_CHECK"; // const-string v0, "DEBUG_BACKGROUND_CHECK"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
/* goto/16 :goto_1 */
/* :sswitch_7 */
final String v0 = "DEBUG_FOREGROUND_SERVICE"; // const-string v0, "DEBUG_FOREGROUND_SERVICE"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x15 */
/* goto/16 :goto_1 */
/* :sswitch_8 */
final String v0 = "DEBUG_MU"; // const-string v0, "DEBUG_MU"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0xa */
/* goto/16 :goto_1 */
/* :sswitch_9 */
final String v0 = "DEBUG_POWER_QUICK"; // const-string v0, "DEBUG_POWER_QUICK"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0xf */
/* goto/16 :goto_1 */
/* :sswitch_a */
final String v0 = "DEBUG_PROCESS_OBSERVERS"; // const-string v0, "DEBUG_PROCESS_OBSERVERS"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x10 */
/* goto/16 :goto_1 */
/* :sswitch_b */
final String v0 = "DEBUG_BACKUP"; // const-string v0, "DEBUG_BACKUP"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 2; // const/4 v0, 0x2
/* goto/16 :goto_1 */
/* :sswitch_c */
final String v0 = "DEBUG_SERVICE"; // const-string v0, "DEBUG_SERVICE"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x14 */
/* goto/16 :goto_1 */
/* :sswitch_d */
final String v0 = "DEBUG_ROOT_TASK"; // const-string v0, "DEBUG_ROOT_TASK"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x1d */
/* goto/16 :goto_1 */
/* :sswitch_e */
final String v0 = "DEBUG_NETWORK"; // const-string v0, "DEBUG_NETWORK"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0xb */
/* goto/16 :goto_1 */
/* :sswitch_f */
final String v0 = "DEBUG_ACTIVITY_STARTS"; // const-string v0, "DEBUG_ACTIVITY_STARTS"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x27 */
/* goto/16 :goto_1 */
/* :sswitch_10 */
final String v0 = "DEBUG_BROADCAST_BACKGROUND"; // const-string v0, "DEBUG_BROADCAST_BACKGROUND"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 4; // const/4 v0, 0x4
/* goto/16 :goto_1 */
/* :sswitch_11 */
final String v0 = "DEBUG_PROCESSES"; // const-string v0, "DEBUG_PROCESSES"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x11 */
/* goto/16 :goto_1 */
/* :sswitch_12 */
final String v0 = "DEBUG_BROADCAST_LIGHT"; // const-string v0, "DEBUG_BROADCAST_LIGHT"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 5; // const/4 v0, 0x5
/* goto/16 :goto_1 */
/* :sswitch_13 */
final String v0 = "DEBUG_RESULTS"; // const-string v0, "DEBUG_RESULTS"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x26 */
/* goto/16 :goto_1 */
/* :sswitch_14 */
final String v0 = "DEBUG_RELEASE"; // const-string v0, "DEBUG_RELEASE"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x23 */
/* goto/16 :goto_1 */
/* :sswitch_15 */
final String v0 = "DEBUG_RECENTS"; // const-string v0, "DEBUG_RECENTS"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x1b */
/* goto/16 :goto_1 */
/* :sswitch_16 */
final String v0 = "DEBUG_PERMISSIONS_REVIEW_ATMS"; // const-string v0, "DEBUG_PERMISSIONS_REVIEW_ATMS"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x25 */
/* goto/16 :goto_1 */
/* :sswitch_17 */
final String v0 = "DEBUG_BROADCAST"; // const-string v0, "DEBUG_BROADCAST"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 3; // const/4 v0, 0x3
/* goto/16 :goto_1 */
/* :sswitch_18 */
final String v0 = "DEBUG_USER_LEAVING"; // const-string v0, "DEBUG_USER_LEAVING"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x24 */
/* goto/16 :goto_1 */
/* :sswitch_19 */
final String v0 = "DEBUG_RECENTS_TRIM_TASKS"; // const-string v0, "DEBUG_RECENTS_TRIM_TASKS"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x1c */
/* goto/16 :goto_1 */
/* :sswitch_1a */
final String v0 = "DEBUG_METRICS"; // const-string v0, "DEBUG_METRICS"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x29 */
/* goto/16 :goto_1 */
/* :sswitch_1b */
final String v0 = "DEBUG_CLEANUP"; // const-string v0, "DEBUG_CLEANUP"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x28 */
/* goto/16 :goto_1 */
/* :sswitch_1c */
final String v0 = "DEBUG_PERMISSIONS_REVIEW"; // const-string v0, "DEBUG_PERMISSIONS_REVIEW"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x19 */
/* goto/16 :goto_1 */
/* :sswitch_1d */
final String v0 = "DEBUG_SERVICE_EXECUTING"; // const-string v0, "DEBUG_SERVICE_EXECUTING"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x16 */
/* goto/16 :goto_1 */
/* :sswitch_1e */
final String v0 = "DEBUG_COMPACTION"; // const-string v0, "DEBUG_COMPACTION"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 7; // const/4 v0, 0x7
/* goto/16 :goto_1 */
/* :sswitch_1f */
final String v0 = "DEBUG_OOM_ADJ_REASON"; // const-string v0, "DEBUG_OOM_ADJ_REASON"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0xd */
/* goto/16 :goto_1 */
/* :sswitch_20 */
final String v0 = "DEBUG_UID_OBSERVERS"; // const-string v0, "DEBUG_UID_OBSERVERS"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x17 */
/* goto/16 :goto_1 */
/* :sswitch_21 */
final String v0 = "DEBUG_ALLOWLISTS"; // const-string v0, "DEBUG_ALLOWLISTS"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x1a */
/* :sswitch_22 */
final String v0 = "DEBUG_IDLE"; // const-string v0, "DEBUG_IDLE"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x22 */
/* :sswitch_23 */
final String v0 = "DEBUG_TRANSITION"; // const-string v0, "DEBUG_TRANSITION"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x1f */
/* :sswitch_24 */
final String v0 = "DEBUG_FREEZER"; // const-string v0, "DEBUG_FREEZER"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x8 */
/* :sswitch_25 */
final String v0 = "DEBUG_PSS"; // const-string v0, "DEBUG_PSS"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x13 */
/* :sswitch_26 */
final String v0 = "DEBUG_LRU"; // const-string v0, "DEBUG_LRU"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x9 */
/* :sswitch_27 */
final String v0 = "DEBUG_APP"; // const-string v0, "DEBUG_APP"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x21 */
/* :sswitch_28 */
final String v0 = "DEBUG_ANR"; // const-string v0, "DEBUG_ANR"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 0; // const/4 v0, 0x0
/* :sswitch_29 */
final String v0 = "DEBUG_VISIBILITY"; // const-string v0, "DEBUG_VISIBILITY"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x20 */
} // :goto_0
int v0 = -1; // const/4 v0, -0x1
} // :goto_1
/* packed-switch v0, :pswitch_data_0 */
/* goto/16 :goto_2 */
/* .line 1624 */
/* :pswitch_0 */
com.android.server.wm.ActivityTaskManagerDebugConfig.DEBUG_METRICS = (p2!= 0);
/* .line 1625 */
/* goto/16 :goto_2 */
/* .line 1621 */
/* :pswitch_1 */
com.android.server.wm.ActivityTaskManagerDebugConfig.DEBUG_CLEANUP = (p2!= 0);
/* .line 1622 */
/* goto/16 :goto_2 */
/* .line 1618 */
/* :pswitch_2 */
com.android.server.wm.ActivityTaskManagerDebugConfig.DEBUG_ACTIVITY_STARTS = (p2!= 0);
/* .line 1619 */
/* goto/16 :goto_2 */
/* .line 1615 */
/* :pswitch_3 */
com.android.server.wm.ActivityTaskManagerDebugConfig.DEBUG_RESULTS = (p2!= 0);
/* .line 1616 */
/* goto/16 :goto_2 */
/* .line 1612 */
/* :pswitch_4 */
com.android.server.wm.ActivityTaskManagerDebugConfig.DEBUG_PERMISSIONS_REVIEW = (p2!= 0);
/* .line 1613 */
/* goto/16 :goto_2 */
/* .line 1609 */
/* :pswitch_5 */
com.android.server.wm.ActivityTaskManagerDebugConfig.DEBUG_USER_LEAVING = (p2!= 0);
/* .line 1610 */
/* goto/16 :goto_2 */
/* .line 1606 */
/* :pswitch_6 */
com.android.server.wm.ActivityTaskManagerDebugConfig.DEBUG_RELEASE = (p2!= 0);
/* .line 1607 */
/* goto/16 :goto_2 */
/* .line 1603 */
/* :pswitch_7 */
com.android.server.wm.ActivityTaskManagerDebugConfig.DEBUG_IDLE = (p2!= 0);
/* .line 1604 */
/* goto/16 :goto_2 */
/* .line 1600 */
/* :pswitch_8 */
com.android.server.wm.ActivityTaskManagerDebugConfig.DEBUG_APP = (p2!= 0);
/* .line 1601 */
/* goto/16 :goto_2 */
/* .line 1597 */
/* :pswitch_9 */
com.android.server.wm.ActivityTaskManagerDebugConfig.DEBUG_VISIBILITY = (p2!= 0);
/* .line 1598 */
/* goto/16 :goto_2 */
/* .line 1594 */
/* :pswitch_a */
com.android.server.wm.ActivityTaskManagerDebugConfig.DEBUG_TRANSITION = (p2!= 0);
/* .line 1595 */
/* goto/16 :goto_2 */
/* .line 1591 */
/* :pswitch_b */
com.android.server.wm.ActivityTaskManagerDebugConfig.DEBUG_SWITCH = (p2!= 0);
/* .line 1592 */
/* goto/16 :goto_2 */
/* .line 1588 */
/* :pswitch_c */
com.android.server.wm.ActivityTaskManagerDebugConfig.DEBUG_ROOT_TASK = (p2!= 0);
/* .line 1589 */
/* goto/16 :goto_2 */
/* .line 1585 */
/* :pswitch_d */
com.android.server.wm.ActivityTaskManagerDebugConfig.DEBUG_RECENTS_TRIM_TASKS = (p2!= 0);
/* .line 1586 */
/* goto/16 :goto_2 */
/* .line 1582 */
/* :pswitch_e */
com.android.server.wm.ActivityTaskManagerDebugConfig.DEBUG_RECENTS = (p2!= 0);
/* .line 1583 */
/* goto/16 :goto_2 */
/* .line 1579 */
/* :pswitch_f */
com.android.server.am.ActivityManagerDebugConfig.DEBUG_ALLOWLISTS = (p2!= 0);
/* .line 1580 */
/* goto/16 :goto_2 */
/* .line 1576 */
/* :pswitch_10 */
com.android.server.am.ActivityManagerDebugConfig.DEBUG_PERMISSIONS_REVIEW = (p2!= 0);
/* .line 1577 */
/* .line 1573 */
/* :pswitch_11 */
com.android.server.am.ActivityManagerDebugConfig.DEBUG_USAGE_STATS = (p2!= 0);
/* .line 1574 */
/* .line 1570 */
/* :pswitch_12 */
com.android.server.am.ActivityManagerDebugConfig.DEBUG_UID_OBSERVERS = (p2!= 0);
/* .line 1571 */
/* .line 1567 */
/* :pswitch_13 */
com.android.server.am.ActivityManagerDebugConfig.DEBUG_SERVICE_EXECUTING = (p2!= 0);
/* .line 1568 */
/* .line 1564 */
/* :pswitch_14 */
com.android.server.am.ActivityManagerDebugConfig.DEBUG_FOREGROUND_SERVICE = (p2!= 0);
/* .line 1565 */
/* .line 1561 */
/* :pswitch_15 */
com.android.server.am.ActivityManagerDebugConfig.DEBUG_SERVICE = (p2!= 0);
/* .line 1562 */
/* .line 1558 */
/* :pswitch_16 */
com.android.server.am.ActivityManagerDebugConfig.DEBUG_PSS = (p2!= 0);
/* .line 1559 */
/* .line 1555 */
/* :pswitch_17 */
com.android.server.am.ActivityManagerDebugConfig.DEBUG_PROVIDER = (p2!= 0);
/* .line 1556 */
/* .line 1552 */
/* :pswitch_18 */
com.android.server.am.ActivityManagerDebugConfig.DEBUG_PROCESSES = (p2!= 0);
/* .line 1553 */
/* .line 1549 */
/* :pswitch_19 */
com.android.server.am.ActivityManagerDebugConfig.DEBUG_PROCESS_OBSERVERS = (p2!= 0);
/* .line 1550 */
/* .line 1546 */
/* :pswitch_1a */
com.android.server.am.ActivityManagerDebugConfig.DEBUG_POWER_QUICK = (p2!= 0);
/* .line 1547 */
/* .line 1543 */
/* :pswitch_1b */
com.android.server.am.ActivityManagerDebugConfig.DEBUG_POWER = (p2!= 0);
/* .line 1544 */
/* .line 1540 */
/* :pswitch_1c */
com.android.server.am.ActivityManagerDebugConfig.DEBUG_OOM_ADJ_REASON = (p2!= 0);
/* .line 1541 */
/* .line 1537 */
/* :pswitch_1d */
com.android.server.am.ActivityManagerDebugConfig.DEBUG_OOM_ADJ = (p2!= 0);
/* .line 1538 */
/* .line 1534 */
/* :pswitch_1e */
com.android.server.am.ActivityManagerDebugConfig.DEBUG_NETWORK = (p2!= 0);
/* .line 1535 */
/* .line 1531 */
/* :pswitch_1f */
com.android.server.am.ActivityManagerDebugConfig.DEBUG_MU = (p2!= 0);
/* .line 1532 */
/* .line 1528 */
/* :pswitch_20 */
com.android.server.am.ActivityManagerDebugConfig.DEBUG_LRU = (p2!= 0);
/* .line 1529 */
/* .line 1525 */
/* :pswitch_21 */
com.android.server.am.ActivityManagerDebugConfig.DEBUG_FREEZER = (p2!= 0);
/* .line 1526 */
/* .line 1522 */
/* :pswitch_22 */
com.android.server.am.ActivityManagerDebugConfig.DEBUG_COMPACTION = (p2!= 0);
/* .line 1523 */
/* .line 1519 */
/* :pswitch_23 */
com.android.server.am.ActivityManagerDebugConfig.DEBUG_BROADCAST_DEFERRAL = (p2!= 0);
/* .line 1520 */
/* .line 1516 */
/* :pswitch_24 */
com.android.server.am.ActivityManagerDebugConfig.DEBUG_BROADCAST_LIGHT = (p2!= 0);
/* .line 1517 */
/* .line 1513 */
/* :pswitch_25 */
com.android.server.am.ActivityManagerDebugConfig.DEBUG_BROADCAST_BACKGROUND = (p2!= 0);
/* .line 1514 */
/* .line 1510 */
/* :pswitch_26 */
com.android.server.am.ActivityManagerDebugConfig.DEBUG_BROADCAST = (p2!= 0);
/* .line 1511 */
/* .line 1507 */
/* :pswitch_27 */
com.android.server.am.ActivityManagerDebugConfig.DEBUG_BACKUP = (p2!= 0);
/* .line 1508 */
/* .line 1504 */
/* :pswitch_28 */
com.android.server.am.ActivityManagerDebugConfig.DEBUG_BACKGROUND_CHECK = (p2!= 0);
/* .line 1505 */
/* .line 1501 */
/* :pswitch_29 */
com.android.server.am.ActivityManagerDebugConfig.DEBUG_ANR = (p2!= 0);
/* .line 1502 */
/* nop */
/* .line 1629 */
} // :goto_2
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x7caac762 -> :sswitch_29 */
/* -0x65a58647 -> :sswitch_28 */
/* -0x65a5860b -> :sswitch_27 */
/* -0x65a55c7d -> :sswitch_26 */
/* -0x65a54d5c -> :sswitch_25 */
/* -0x609ef971 -> :sswitch_24 */
/* -0x5725bd1f -> :sswitch_23 */
/* -0x4f07c5a0 -> :sswitch_22 */
/* -0x3fa7c488 -> :sswitch_21 */
/* -0x33a5061e -> :sswitch_20 */
/* -0x2bd2be66 -> :sswitch_1f */
/* -0x2384c46f -> :sswitch_1e */
/* -0x13398d64 -> :sswitch_1d */
/* -0x105f7ae1 -> :sswitch_1c */
/* -0x990bb88 -> :sswitch_1b */
/* -0x3a95169 -> :sswitch_1a */
/* -0x3a5b41c -> :sswitch_19 */
/* -0x2a29454 -> :sswitch_18 */
/* 0x31bad95 -> :sswitch_17 */
/* 0x32a6039 -> :sswitch_16 */
/* 0x3e0734c -> :sswitch_15 */
/* 0x45f15db -> :sswitch_14 */
/* 0x4c929ca -> :sswitch_13 */
/* 0x95409cc -> :sswitch_12 */
/* 0x22591031 -> :sswitch_11 */
/* 0x238c0a18 -> :sswitch_10 */
/* 0x2d964575 -> :sswitch_f */
/* 0x313f4802 -> :sswitch_e */
/* 0x31af5fb6 -> :sswitch_d */
/* 0x39a1b489 -> :sswitch_c */
/* 0x47b2f94e -> :sswitch_b */
/* 0x48e2e741 -> :sswitch_a */
/* 0x497569e7 -> :sswitch_9 */
/* 0x4f4d3f34 -> :sswitch_8 */
/* 0x52bdf925 -> :sswitch_7 */
/* 0x55e50723 -> :sswitch_6 */
/* 0x655e1835 -> :sswitch_5 */
/* 0x65ee3ac0 -> :sswitch_4 */
/* 0x6d83d27d -> :sswitch_3 */
/* 0x6e76dfd9 -> :sswitch_2 */
/* 0x761ef2d5 -> :sswitch_1 */
/* 0x76d6c1a9 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_29 */
/* :pswitch_28 */
/* :pswitch_27 */
/* :pswitch_26 */
/* :pswitch_25 */
/* :pswitch_24 */
/* :pswitch_23 */
/* :pswitch_22 */
/* :pswitch_21 */
/* :pswitch_20 */
/* :pswitch_1f */
/* :pswitch_1e */
/* :pswitch_1d */
/* :pswitch_1c */
/* :pswitch_1b */
/* :pswitch_1a */
/* :pswitch_19 */
/* :pswitch_18 */
/* :pswitch_17 */
/* :pswitch_16 */
/* :pswitch_15 */
/* :pswitch_14 */
/* :pswitch_13 */
/* :pswitch_12 */
/* :pswitch_11 */
/* :pswitch_10 */
/* :pswitch_f */
/* :pswitch_e */
/* :pswitch_d */
/* :pswitch_c */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void enableAppDebugConfig ( com.android.server.am.ActivityManagerService p0, java.lang.String p1, Boolean p2, java.lang.String p3, Integer p4 ) {
/* .locals 4 */
/* .param p1, "activityManagerService" # Lcom/android/server/am/ActivityManagerService; */
/* .param p2, "config" # Ljava/lang/String; */
/* .param p3, "enable" # Z */
/* .param p4, "processName" # Ljava/lang/String; */
/* .param p5, "uid" # I */
/* .line 1871 */
v0 = android.text.TextUtils .isEmpty ( p2 );
/* if-nez v0, :cond_4 */
v0 = android.text.TextUtils .isEmpty ( p4 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1874 */
} // :cond_0
v0 = android.os.Binder .getCallingUid ( );
/* .line 1875 */
/* .local v0, "callerUid":I */
/* const/16 v1, 0x3e8 */
/* if-eq v0, v1, :cond_2 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* const/16 v1, 0x7d0 */
/* if-ne v0, v1, :cond_1 */
/* .line 1888 */
} // :cond_1
final String v1 = "ActivityManagerServiceImpl"; // const-string v1, "ActivityManagerServiceImpl"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " can not enable activity thread debug config."; // const-string v3, " can not enable activity thread debug config."
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v2 );
/* .line 1876 */
} // :cond_2
} // :goto_0
/* monitor-enter p0 */
/* .line 1877 */
try { // :try_start_0
(( com.android.server.am.ActivityManagerService ) p1 ).getProcessRecordLocked ( p4, p5 ); // invoke-virtual {p1, p4, p5}, Lcom/android/server/am/ActivityManagerService;->getProcessRecordLocked(Ljava/lang/String;I)Lcom/android/server/am/ProcessRecord;
/* .line 1879 */
/* .local v1, "app":Lcom/android/server/am/ProcessRecord; */
if ( v1 != null) { // if-eqz v1, :cond_3
(( com.android.server.am.ProcessRecord ) v1 ).getThread ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move-object v3, v2 */
/* .local v3, "thread":Landroid/app/IApplicationThread; */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 1881 */
try { // :try_start_1
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1884 */
/* .line 1882 */
/* :catch_0 */
/* move-exception v2 */
/* .line 1886 */
} // .end local v1 # "app":Lcom/android/server/am/ProcessRecord;
} // .end local v3 # "thread":Landroid/app/IApplicationThread;
} // :cond_3
} // :goto_1
try { // :try_start_2
/* monitor-exit p0 */
/* .line 1890 */
} // :goto_2
return;
/* .line 1886 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit p0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
/* .line 1872 */
} // .end local v0 # "callerUid":I
} // :cond_4
} // :goto_3
return;
} // .end method
void finishBooting ( ) {
/* .locals 5 */
/* .line 337 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 338 */
/* .local v0, "bootCompleteTime":J */
miui.mqsas.sdk.BootEventManager .getInstance ( );
(( miui.mqsas.sdk.BootEventManager ) v2 ).setUIReady ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Lmiui/mqsas/sdk/BootEventManager;->setUIReady(J)V
/* .line 339 */
miui.mqsas.sdk.BootEventManager .getInstance ( );
(( miui.mqsas.sdk.BootEventManager ) v2 ).setBootComplete ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Lmiui/mqsas/sdk/BootEventManager;->setBootComplete(J)V
/* .line 340 */
com.miui.server.xspace.XSpaceManagerServiceStub .getInstance ( );
v3 = this.mContext;
(( com.miui.server.xspace.XSpaceManagerServiceStub ) v2 ).init ( v3 ); // invoke-virtual {v2, v3}, Lcom/miui/server/xspace/XSpaceManagerServiceStub;->init(Landroid/content/Context;)V
/* .line 341 */
com.android.server.wm.AppResurrectionServiceStub .getInstance ( );
v3 = this.mContext;
(( com.android.server.wm.AppResurrectionServiceStub ) v2 ).init ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/wm/AppResurrectionServiceStub;->init(Landroid/content/Context;)V
/* .line 342 */
v2 = this.mContext;
miui.drm.DrmBroadcast .getInstance ( v2 );
(( miui.drm.DrmBroadcast ) v2 ).broadcast ( ); // invoke-virtual {v2}, Lmiui/drm/DrmBroadcast;->broadcast()V
/* .line 344 */
/* new-instance v2, Landroid/content/Intent; */
final String v3 = "miui.intent.action.FINISH_BOOTING"; // const-string v3, "miui.intent.action.FINISH_BOOTING"
/* invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 345 */
/* .local v2, "intent":Landroid/content/Intent; */
/* const/high16 v3, 0x10000000 */
(( android.content.Intent ) v2 ).setFlags ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 346 */
v3 = this.mContext;
v4 = android.os.UserHandle.SYSTEM;
(( android.content.Context ) v3 ).sendBroadcastAsUser ( v2, v4 ); // invoke-virtual {v3, v2, v4}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 347 */
com.android.server.wm.ActivityTaskManagerServiceImpl .getInstance ( );
v4 = this.mContext;
(( com.android.server.wm.ActivityTaskManagerServiceImpl ) v3 ).updateResizeBlackList ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->updateResizeBlackList(Landroid/content/Context;)V
/* .line 348 */
/* sget-boolean v3, Lmiui/mqsas/scout/ScoutUtils;->REBOOT_COREDUMP:Z */
/* if-nez v3, :cond_0 */
miui.mqsas.scout.ScoutUtils .isLibraryTest ( );
/* .line 351 */
} // :cond_0
v3 = this.mStabilityLocalServiceInternal;
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 352 */
v4 = this.mContext;
/* .line 353 */
v3 = this.mStabilityLocalServiceInternal;
/* .line 356 */
} // :cond_1
com.android.server.am.SystemPressureControllerStub .getInstance ( );
(( com.android.server.am.SystemPressureControllerStub ) v3 ).finishBooting ( ); // invoke-virtual {v3}, Lcom/android/server/am/SystemPressureControllerStub;->finishBooting()V
/* .line 357 */
return;
} // .end method
public void finishBootingAsUser ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "userId" # I */
/* .line 730 */
com.android.server.wm.ActivityTaskManagerServiceImpl .getInstance ( );
final String v1 = "finishBooting"; // const-string v1, "finishBooting"
(( com.android.server.wm.ActivityTaskManagerServiceImpl ) v0 ).restartSubScreenUiIfNeeded ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->restartSubScreenUiIfNeeded(ILjava/lang/String;)V
/* .line 731 */
return;
} // .end method
public Integer getAppStartMode ( Integer p0, Integer p1, Integer p2, java.lang.String p3 ) {
/* .locals 16 */
/* .param p1, "uid" # I */
/* .param p2, "defMode" # I */
/* .param p3, "callingPid" # I */
/* .param p4, "callingPackage" # Ljava/lang/String; */
/* .line 385 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p4 */
final String v0 = "com.xiaomi.xmsf"; // const-string v0, "com.xiaomi.xmsf"
v0 = (( java.lang.String ) v0 ).equalsIgnoreCase ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
int v3 = 0; // const/4 v3, 0x0
/* if-nez v0, :cond_2 */
final String v0 = "com.miui.voiceassist"; // const-string v0, "com.miui.voiceassist"
/* .line 386 */
v0 = (( java.lang.String ) v0 ).equalsIgnoreCase ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
/* if-nez v0, :cond_2 */
final String v0 = "com.miui.carlink"; // const-string v0, "com.miui.carlink"
/* .line 387 */
v0 = (( java.lang.String ) v0 ).equalsIgnoreCase ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
/* if-nez v0, :cond_2 */
final String v0 = "com.baidu.carlife.xiaomi"; // const-string v0, "com.baidu.carlife.xiaomi"
/* .line 388 */
v0 = (( java.lang.String ) v0 ).equalsIgnoreCase ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mContext;
/* .line 390 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v4, "ucar_casting_state" */
v0 = android.provider.Settings$Global .getInt ( v0,v4,v3 );
int v4 = 1; // const/4 v4, 0x1
/* if-eq v4, v0, :cond_2 */
} // :cond_0
final String v0 = "com.miui.notification"; // const-string v0, "com.miui.notification"
/* .line 391 */
v0 = (( java.lang.String ) v0 ).equalsIgnoreCase ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
/* if-nez v0, :cond_2 */
final String v0 = "com.xiaomi.bluetooth"; // const-string v0, "com.xiaomi.bluetooth"
/* .line 392 */
v0 = (( java.lang.String ) v0 ).equalsIgnoreCase ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 409 */
} // :cond_1
/* .line 393 */
} // :cond_2
} // :goto_0
v0 = this.mAmService;
v0 = this.mProcessList;
/* move/from16 v4, p1 */
(( com.android.server.am.ProcessList ) v0 ).getUidRecordLOSP ( v4 ); // invoke-virtual {v0, v4}, Lcom/android/server/am/ProcessList;->getUidRecordLOSP(I)Lcom/android/server/am/UidRecord;
/* .line 394 */
/* .local v5, "record":Lcom/android/server/am/UidRecord; */
if ( v5 != null) { // if-eqz v5, :cond_4
v0 = (( com.android.server.am.UidRecord ) v5 ).isIdle ( ); // invoke-virtual {v5}, Lcom/android/server/am/UidRecord;->isIdle()Z
if ( v0 != null) { // if-eqz v0, :cond_4
v0 = (( com.android.server.am.UidRecord ) v5 ).isCurAllowListed ( ); // invoke-virtual {v5}, Lcom/android/server/am/UidRecord;->isCurAllowListed()Z
/* if-nez v0, :cond_4 */
/* .line 396 */
v0 = this.mAmService;
v6 = this.mPidsSelfLocked;
/* monitor-enter v6 */
/* .line 397 */
try { // :try_start_0
v0 = this.mAmService;
v0 = this.mPidsSelfLocked;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move/from16 v7, p3 */
try { // :try_start_1
(( com.android.server.am.ActivityManagerService$PidMap ) v0 ).get ( v7 ); // invoke-virtual {v0, v7}, Lcom/android/server/am/ActivityManagerService$PidMap;->get(I)Lcom/android/server/am/ProcessRecord;
/* .line 398 */
/* .local v0, "proc":Lcom/android/server/am/ProcessRecord; */
/* monitor-exit v6 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* .line 399 */
if ( v0 != null) { // if-eqz v0, :cond_3
/* iget v6, v0, Lcom/android/server/am/ProcessRecord;->uid:I */
} // :cond_3
v6 = android.os.Binder .getCallingUid ( );
} // :goto_1
/* move v15, v6 */
/* .line 400 */
/* .local v15, "callingUid":I */
v8 = this.mAmService;
v9 = (( com.android.server.am.UidRecord ) v5 ).getUid ( ); // invoke-virtual {v5}, Lcom/android/server/am/UidRecord;->getUid()I
/* const-wide/32 v10, 0xea60 */
/* const/16 v12, 0x65 */
final String v13 = "push-service-launch"; // const-string v13, "push-service-launch"
int v14 = 0; // const/4 v14, 0x0
/* invoke-virtual/range {v8 ..v15}, Lcom/android/server/am/ActivityManagerService;->tempAllowlistUidLocked(IJILjava/lang/String;II)V */
/* .line 398 */
} // .end local v0 # "proc":Lcom/android/server/am/ProcessRecord;
} // .end local v15 # "callingUid":I
/* :catchall_0 */
/* move-exception v0 */
/* move/from16 v7, p3 */
} // :goto_2
try { // :try_start_2
/* monitor-exit v6 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* throw v0 */
/* :catchall_1 */
/* move-exception v0 */
/* .line 394 */
} // :cond_4
/* move/from16 v7, p3 */
/* .line 407 */
} // :goto_3
} // .end method
public com.android.server.am.BroadcastProcessQueue getBroadcastProcessQueue ( java.lang.String p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 374 */
v0 = this.mAmService;
/* iget-boolean v0, v0, Lcom/android/server/am/ActivityManagerService;->mEnableModernQueue:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 375 */
v0 = this.mAmService;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.am.ActivityManagerService ) v0 ).broadcastQueueForFlags ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ActivityManagerService;->broadcastQueueForFlags(I)Lcom/android/server/am/BroadcastQueue;
/* .line 376 */
/* .local v0, "bc":Lcom/android/server/am/BroadcastQueue; */
(( com.android.server.am.BroadcastQueue ) v0 ).getBroadcastProcessQueue ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/am/BroadcastQueue;->getBroadcastProcessQueue(Ljava/lang/String;I)Lcom/android/server/am/BroadcastProcessQueue;
/* .line 378 */
} // .end local v0 # "bc":Lcom/android/server/am/BroadcastQueue;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer getDefaultMaxCachedProcesses ( ) {
/* .locals 6 */
/* .line 1910 */
final String v0 = "persist.sys.spc.enabled"; // const-string v0, "persist.sys.spc.enabled"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1911 */
/* const/16 v0, 0x3e8 */
/* .line 1913 */
} // :cond_0
/* const-wide/32 v0, 0x40000000 */
/* .line 1914 */
/* .local v0, "ramSizeGB":J */
android.os.Process .getTotalMemory ( );
/* move-result-wide v2 */
/* const-wide/32 v4, 0x40000000 */
/* div-long/2addr v2, v4 */
/* .line 1915 */
/* .local v2, "totalMemByte":J */
/* const-wide/16 v4, 0x2 */
/* cmp-long v4, v2, v4 */
/* if-gez v4, :cond_1 */
/* .line 1916 */
/* const/16 v4, 0xc */
/* .line 1917 */
} // :cond_1
/* const-wide/16 v4, 0x3 */
/* cmp-long v4, v2, v4 */
/* if-gtz v4, :cond_2 */
/* .line 1918 */
/* const/16 v4, 0x10 */
/* .line 1919 */
} // :cond_2
/* const-wide/16 v4, 0x4 */
/* cmp-long v4, v2, v4 */
/* if-gtz v4, :cond_3 */
/* .line 1920 */
/* const/16 v4, 0x18 */
/* .line 1922 */
} // :cond_3
/* const/16 v4, 0x20 */
} // .end method
public Integer getOomAdjOfPid ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "pid" # I */
/* .line 1222 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 1223 */
/* .local v0, "callingUid":I */
v1 = android.os.UserHandle .getAppId ( v0 );
/* const/16 v2, 0x3e8 */
/* if-ne v1, v2, :cond_0 */
/* .line 1227 */
final String v1 = "ActivityManagerServiceImpl"; // const-string v1, "ActivityManagerServiceImpl"
v1 = com.android.server.ScoutHelper .getOomAdjOfPid ( v1,p1 );
/* .line 1224 */
} // :cond_0
/* new-instance v1, Ljava/lang/SecurityException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Only the system process can call getOomAdjOfPid, received request from uid: "; // const-string v3, "Only the system process can call getOomAdjOfPid, received request from uid: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
public Boolean getPackageHoldOn ( android.os.Parcel p0, android.os.Parcel p1 ) {
/* .locals 6 */
/* .param p1, "data" # Landroid/os/Parcel; */
/* .param p2, "reply" # Landroid/os/Parcel; */
/* .line 879 */
final String v0 = "android.app.IActivityManager"; // const-string v0, "android.app.IActivityManager"
(( android.os.Parcel ) p1 ).enforceInterface ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 880 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 881 */
/* .local v0, "callingUid":I */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v1 */
/* .line 882 */
/* .local v1, "ident":J */
(( android.os.Parcel ) p2 ).writeNoException ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->writeNoException()V
/* .line 884 */
try { // :try_start_0
v3 = android.os.UserHandle .getAppId ( v0 );
/* const/16 v4, 0x3e8 */
/* if-eq v3, v4, :cond_0 */
/* .line 885 */
final String v3 = ""; // const-string v3, ""
(( android.os.Parcel ) p2 ).writeString ( v3 ); // invoke-virtual {p2, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 886 */
final String v3 = "ActivityManagerServiceImpl"; // const-string v3, "ActivityManagerServiceImpl"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Permission Denial: getPackageHoldOn() not from system "; // const-string v5, "Permission Denial: getPackageHoldOn() not from system "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* .line 888 */
} // :cond_0
com.android.server.wm.ActivityTaskManagerServiceStub .get ( );
(( com.android.server.wm.ActivityTaskManagerServiceStub ) v3 ).getPackageHoldOn ( ); // invoke-virtual {v3}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->getPackageHoldOn()Ljava/lang/String;
(( android.os.Parcel ) p2 ).writeString ( v3 ); // invoke-virtual {p2, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 891 */
} // :goto_0
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 892 */
/* nop */
/* .line 893 */
int v3 = 1; // const/4 v3, 0x1
/* .line 891 */
/* :catchall_0 */
/* move-exception v3 */
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 892 */
/* throw v3 */
} // .end method
public java.lang.String getPackageNameByPid ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "pid" # I */
/* .line 439 */
com.android.server.am.ProcessUtils .getPackageNameByPid ( p1 );
} // .end method
public java.lang.String getPackageNameForPid ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "pid" # I */
/* .line 1780 */
v0 = this.mAmService;
v0 = this.mPidsSelfLocked;
/* monitor-enter v0 */
/* .line 1781 */
try { // :try_start_0
v1 = this.mAmService;
v1 = this.mPidsSelfLocked;
(( com.android.server.am.ActivityManagerService$PidMap ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/am/ActivityManagerService$PidMap;->get(I)Lcom/android/server/am/ProcessRecord;
/* .line 1782 */
/* .local v1, "app":Lcom/android/server/am/ProcessRecord; */
if ( v1 != null) { // if-eqz v1, :cond_0
v2 = this.info;
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = this.info;
v2 = this.packageName;
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* monitor-exit v0 */
/* .line 1783 */
} // .end local v1 # "app":Lcom/android/server/am/ProcessRecord;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public java.lang.String getProcessNameByPid ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "pid" # I */
/* .line 434 */
com.android.server.am.ProcessUtils .getProcessNameByPid ( p1 );
} // .end method
public android.os.IBinder getTopSplitActivity ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "pid" # I */
/* .line 1355 */
v0 = this.mSplitActivityEntryStack;
int v1 = 0; // const/4 v1, 0x0
v0 = if ( v0 != null) { // if-eqz v0, :cond_2
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1358 */
} // :cond_0
v0 = this.mSplitActivityEntryStack;
java.lang.Integer .valueOf ( p1 );
/* check-cast v0, Ljava/util/Stack; */
/* .line 1359 */
/* .local v0, "stack":Ljava/util/Stack;, "Ljava/util/Stack<Landroid/os/IBinder;>;" */
if ( v0 != null) { // if-eqz v0, :cond_1
v2 = (( java.util.Stack ) v0 ).empty ( ); // invoke-virtual {v0}, Ljava/util/Stack;->empty()Z
/* if-nez v2, :cond_1 */
(( java.util.Stack ) v0 ).peek ( ); // invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;
/* check-cast v1, Landroid/os/IBinder; */
} // :cond_1
/* .line 1356 */
} // .end local v0 # "stack":Ljava/util/Stack;, "Ljava/util/Stack<Landroid/os/IBinder;>;"
} // :cond_2
} // :goto_0
} // .end method
public Boolean ignoreSpecifiedAuthority ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "authority" # Ljava/lang/String; */
/* .line 710 */
v0 = com.android.server.am.ActivityManagerServiceImpl.mIgnoreAuthorityList;
v0 = (( java.util.HashSet ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
} // .end method
void init ( com.android.server.am.ActivityManagerService p0, android.content.Context p1 ) {
/* .locals 4 */
/* .param p1, "ams" # Lcom/android/server/am/ActivityManagerService; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 242 */
this.mAmService = p1;
/* .line 243 */
this.mContext = p2;
/* .line 244 */
com.android.server.am.MiuiWarnings .getInstance ( );
(( com.android.server.am.MiuiWarnings ) v0 ).init ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/am/MiuiWarnings;->init(Landroid/content/Context;)V
/* .line 245 */
com.android.server.am.BroadcastQueueModernStubImpl .getInstance ( );
v1 = this.mAmService;
v2 = this.mContext;
(( com.android.server.am.BroadcastQueueModernStubImpl ) v0 ).init ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/am/BroadcastQueueModernStubImpl;->init(Lcom/android/server/am/ActivityManagerService;Landroid/content/Context;)V
/* .line 247 */
/* new-instance v0, Lcom/android/server/am/DumpScoutTraceThread; */
final String v1 = "DumpScoutTraceThread"; // const-string v1, "DumpScoutTraceThread"
/* invoke-direct {v0, v1, p0}, Lcom/android/server/am/DumpScoutTraceThread;-><init>(Ljava/lang/String;Lcom/android/server/am/ActivityManagerServiceImpl;)V */
/* .line 248 */
/* .local v0, "dumpScoutTraceThread":Lcom/android/server/am/DumpScoutTraceThread; */
(( com.android.server.am.DumpScoutTraceThread ) v0 ).start ( ); // invoke-virtual {v0}, Lcom/android/server/am/DumpScoutTraceThread;->start()V
/* .line 249 */
final String v1 = "DumpScoutTraceThread begin running."; // const-string v1, "DumpScoutTraceThread begin running."
final String v2 = "ActivityManagerServiceImpl"; // const-string v2, "ActivityManagerServiceImpl"
android.util.Slog .i ( v2,v1 );
/* .line 250 */
/* sget-boolean v1, Lmiui/os/Build;->IS_DEBUGGABLE:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 251 */
final String v1 = "debug.block_system"; // const-string v1, "debug.block_system"
int v3 = 0; // const/4 v3, 0x0
v1 = android.os.SystemProperties .getBoolean ( v1,v3 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 252 */
final String v1 = "boot monitor system_watchdog..."; // const-string v1, "boot monitor system_watchdog..."
android.util.Slog .w ( v2,v1 );
/* .line 253 */
/* const-wide v1, 0x7fffffffffffffffL */
android.os.SystemClock .sleep ( v1,v2 );
/* .line 255 */
} // :cond_0
/* const-class v1, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
com.android.server.LocalServices .getService ( v1 );
/* check-cast v1, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
this.mSmartPowerService = v1;
/* .line 256 */
/* const-class v1, Lcom/miui/server/stability/StabilityLocalServiceInternal; */
com.android.server.LocalServices .getService ( v1 );
/* check-cast v1, Lcom/miui/server/stability/StabilityLocalServiceInternal; */
this.mStabilityLocalServiceInternal = v1;
/* .line 257 */
return;
} // .end method
public Boolean isActiveInstruUid ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 1827 */
/* iget v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mInstrUid:I */
/* if-ne p1, v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isAllowedOperatorGetPhoneNumber ( com.android.server.am.ActivityManagerService p0, java.lang.String p1 ) {
/* .locals 16 */
/* .param p1, "ams" # Lcom/android/server/am/ActivityManagerService; */
/* .param p2, "permission" # Ljava/lang/String; */
/* .line 740 */
final String v0 = ";"; // const-string v0, ";"
/* move-object/from16 v1, p2 */
(( java.lang.String ) v1 ).split ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 741 */
/* .local v0, "content":[Ljava/lang/String; */
/* array-length v2, v0 */
int v3 = 4; // const/4 v3, 0x4
int v4 = 1; // const/4 v4, 0x1
/* if-ne v2, v3, :cond_2 */
/* .line 742 */
int v2 = 3; // const/4 v2, 0x3
/* aget-object v2, v0, v2 */
v2 = java.lang.Integer .parseInt ( v2 );
/* .line 743 */
/* .local v2, "pid":I */
com.android.server.am.ProcessUtils .getPackageNameByPid ( v2 );
/* .line 744 */
/* .local v3, "packageName":Ljava/lang/String; */
v5 = android.text.TextUtils .isEmpty ( v3 );
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 745 */
/* .line 747 */
} // :cond_0
/* aget-object v5, v0, v4 */
v13 = java.lang.Integer .parseInt ( v5 );
/* .line 748 */
/* .local v13, "op":I */
int v5 = 2; // const/4 v5, 0x2
/* aget-object v5, v0, v5 */
v14 = java.lang.Integer .parseInt ( v5 );
/* .line 749 */
/* .local v14, "uid":I */
/* move-object/from16 v15, p1 */
v5 = this.mAppOpsService;
int v9 = 0; // const/4 v9, 0x0
int v10 = 0; // const/4 v10, 0x0
final String v11 = "ActivityManagerServiceImpl#isAllowedOperatorGetPhoneNumber"; // const-string v11, "ActivityManagerServiceImpl#isAllowedOperatorGetPhoneNumber"
int v12 = 0; // const/4 v12, 0x0
/* move v6, v13 */
/* move v7, v14 */
/* move-object v8, v3 */
/* invoke-virtual/range {v5 ..v12}, Lcom/android/server/appop/AppOpsService;->noteOperation(IILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;Z)Landroid/app/SyncNotedAppOp; */
/* .line 751 */
v5 = (( android.app.SyncNotedAppOp ) v5 ).getOpMode ( ); // invoke-virtual {v5}, Landroid/app/SyncNotedAppOp;->getOpMode()I
/* if-nez v5, :cond_1 */
} // :cond_1
int v4 = 0; // const/4 v4, 0x0
/* .line 749 */
} // :goto_0
/* .line 753 */
} // .end local v2 # "pid":I
} // .end local v3 # "packageName":Ljava/lang/String;
} // .end local v13 # "op":I
} // .end local v14 # "uid":I
} // :cond_2
/* move-object/from16 v15, p1 */
} // .end method
public Boolean isBackuping ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 1809 */
v0 = this.mBackupingList;
java.lang.Integer .valueOf ( p1 );
v0 = (( java.util.ArrayList ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
} // .end method
public Boolean isBoostNeeded ( com.android.server.am.ProcessRecord p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 6 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "hostingType" # Ljava/lang/String; */
/* .param p3, "hostingName" # Ljava/lang/String; */
/* .line 934 */
v0 = this.callerPackage;
/* .line 936 */
/* .local v0, "callerPackage":Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
v2 = com.android.server.am.ActivityManagerServiceImpl .isSystemPackage ( v0,v1 );
/* .line 937 */
/* .local v2, "isSystem":Z */
/* const-string/jumbo v3, "service" */
v3 = (( java.lang.String ) v3 ).equals ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v4 = 1; // const/4 v4, 0x1
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 938 */
final String v3 = "com.xiaomi.mipush.sdk.PushMessageHandler"; // const-string v3, "com.xiaomi.mipush.sdk.PushMessageHandler"
v3 = (( java.lang.String ) p3 ).endsWith ( v3 ); // invoke-virtual {p3, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 939 */
final String v3 = "com.xiaomi.xmsf"; // const-string v3, "com.xiaomi.xmsf"
v3 = (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* move v3, v4 */
} // :cond_0
/* move v3, v1 */
/* .line 940 */
/* .local v3, "isNeeded":Z */
} // :goto_0
final String v5 = "com.miui.voiceassist/com.xiaomi.voiceassistant.VoiceService"; // const-string v5, "com.miui.voiceassist/com.xiaomi.voiceassistant.VoiceService"
v5 = (( java.lang.String ) v5 ).equals ( p3 ); // invoke-virtual {v5, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v5, :cond_2 */
if ( v3 != null) { // if-eqz v3, :cond_1
} // :cond_1
/* move v5, v1 */
} // :cond_2
} // :goto_1
/* move v5, v4 */
} // :goto_2
/* move v3, v5 */
/* .line 941 */
final String v5 = "com.miui.notification"; // const-string v5, "com.miui.notification"
v5 = (( java.lang.String ) v5 ).equals ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v5, :cond_3 */
if ( v3 != null) { // if-eqz v3, :cond_4
} // :cond_3
/* move v1, v4 */
/* .line 942 */
} // .end local v3 # "isNeeded":Z
/* .local v1, "isNeeded":Z */
} // :cond_4
final String v3 = "com.tencent.mm"; // const-string v3, "com.tencent.mm"
v4 = this.processName;
v3 = (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 943 */
int v1 = 1; // const/4 v1, 0x1
/* .line 945 */
} // :cond_5
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "hostingType="; // const-string v4, "hostingType="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", hostingName="; // const-string v4, ", hostingName="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p3 ); // invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", callerPackage="; // const-string v4, ", callerPackage="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", isSystem="; // const-string v4, ", isSystem="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = ", isBoostNeeded="; // const-string v4, ", isBoostNeeded="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = "."; // const-string v4, "."
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "Boost"; // const-string v4, "Boost"
android.util.Slog .d ( v4,v3 );
/* .line 948 */
} // .end method
public Boolean isKillProvider ( com.android.server.am.ContentProviderRecord p0, com.android.server.am.ProcessRecord p1, com.android.server.am.ProcessRecord p2 ) {
/* .locals 3 */
/* .param p1, "cpr" # Lcom/android/server/am/ContentProviderRecord; */
/* .param p2, "proc" # Lcom/android/server/am/ProcessRecord; */
/* .param p3, "capp" # Lcom/android/server/am/ProcessRecord; */
/* .line 1445 */
v0 = this.mState;
v0 = (( com.android.server.am.ProcessStateRecord ) v0 ).getCurAdj ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I
/* const/16 v1, 0xfa */
/* if-le v0, v1, :cond_0 */
/* .line 1446 */
v0 = com.android.server.am.ProcessUtils .isHomeProcess ( p3 );
/* if-nez v0, :cond_0 */
/* iget v0, p3, Lcom/android/server/am/ProcessRecord;->uid:I */
v1 = this.info;
v1 = this.packageName;
v2 = this.processName;
/* .line 1447 */
v0 = /* invoke-direct {p0, v0, v1, v2}, Lcom/android/server/am/ActivityManagerServiceImpl;->isProtectProcess(ILjava/lang/String;Ljava/lang/String;)Z */
/* if-nez v0, :cond_0 */
/* .line 1448 */
(( com.android.server.am.ProcessRecord ) p3 ).getWindowProcessController ( ); // invoke-virtual {p3}, Lcom/android/server/am/ProcessRecord;->getWindowProcessController()Lcom/android/server/wm/WindowProcessController;
v0 = (( com.android.server.wm.WindowProcessController ) v0 ).isPreviousProcess ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowProcessController;->isPreviousProcess()Z
/* if-nez v0, :cond_0 */
/* .line 1449 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1451 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "visible app " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.processName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " depends on provider "; // const-string v1, " depends on provider "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.name;
/* .line 1452 */
(( android.content.ComponentName ) v1 ).flattenToShortString ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " in dying proc "; // const-string v1, " in dying proc "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1453 */
final String v1 = "??"; // const-string v1, "??"
if ( p2 != null) { // if-eqz p2, :cond_1
v2 = this.processName;
} // :cond_1
/* move-object v2, v1 */
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " (adj "; // const-string v2, " (adj "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1454 */
if ( p2 != null) { // if-eqz p2, :cond_2
v1 = this.mState;
v1 = (( com.android.server.am.ProcessStateRecord ) v1 ).getSetAdj ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessStateRecord;->getSetAdj()I
java.lang.Integer .valueOf ( v1 );
} // :cond_2
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = ")"; // const-string v1, ")"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1451 */
final String v1 = "ActivityManagerServiceImpl"; // const-string v1, "ActivityManagerServiceImpl"
android.util.Slog .w ( v1,v0 );
/* .line 1455 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isRestrictBackgroundAction ( java.lang.String p0, Integer p1, java.lang.String p2, Integer p3, java.lang.String p4 ) {
/* .locals 7 */
/* .param p1, "localhost" # Ljava/lang/String; */
/* .param p2, "callerUid" # I */
/* .param p3, "callerPkgName" # Ljava/lang/String; */
/* .param p4, "calleeUid" # I */
/* .param p5, "calleePkgName" # Ljava/lang/String; */
/* .line 329 */
/* invoke-direct {p0}, Lcom/android/server/am/ActivityManagerServiceImpl;->getGreezeService()Lcom/miui/server/greeze/GreezeManagerInternal; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 330 */
/* invoke-direct {p0}, Lcom/android/server/am/ActivityManagerServiceImpl;->getGreezeService()Lcom/miui/server/greeze/GreezeManagerInternal; */
/* move-object v2, p1 */
/* move v3, p2 */
/* move-object v4, p3 */
/* move v5, p4 */
/* move-object v6, p5 */
v0 = /* invoke-virtual/range {v1 ..v6}, Lcom/miui/server/greeze/GreezeManagerInternal;->isRestrictBackgroundAction(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;)Z */
/* .line 332 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
Boolean isStartWithBackupRestriction ( android.content.Context p0, java.lang.String p1, com.android.server.am.ProcessRecord p2 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "backupPkgName" # Ljava/lang/String; */
/* .param p3, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 716 */
(( com.android.server.am.ProcessRecord ) p3 ).getActiveInstrumentation ( ); // invoke-virtual {p3}, Lcom/android/server/am/ProcessRecord;->getActiveInstrumentation()Lcom/android/server/am/ActiveInstrumentation;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 717 */
(( com.android.server.am.ProcessRecord ) p3 ).getActiveInstrumentation ( ); // invoke-virtual {p3}, Lcom/android/server/am/ProcessRecord;->getActiveInstrumentation()Lcom/android/server/am/ActiveInstrumentation;
v0 = this.mTargetInfo;
} // :cond_0
v0 = this.info;
/* .line 720 */
/* .local v0, "appInfo":Landroid/content/pm/ApplicationInfo; */
} // :goto_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean isSystemApp ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "pid" # I */
/* .line 1773 */
v0 = this.mAmService;
v0 = this.mPidsSelfLocked;
/* monitor-enter v0 */
/* .line 1774 */
try { // :try_start_0
v1 = this.mAmService;
v1 = this.mPidsSelfLocked;
(( com.android.server.am.ActivityManagerService$PidMap ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/am/ActivityManagerService$PidMap;->get(I)Lcom/android/server/am/ProcessRecord;
/* .line 1775 */
/* .local v1, "app":Lcom/android/server/am/ProcessRecord; */
if ( v1 != null) { // if-eqz v1, :cond_0
v2 = this.info;
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = this.info;
v2 = (( android.content.pm.ApplicationInfo ) v2 ).isSystemApp ( ); // invoke-virtual {v2}, Landroid/content/pm/ApplicationInfo;->isSystemApp()Z
if ( v2 != null) { // if-eqz v2, :cond_0
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* monitor-exit v0 */
/* .line 1776 */
} // .end local v1 # "app":Lcom/android/server/am/ProcessRecord;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isTopSplitActivity ( Integer p0, android.os.IBinder p1 ) {
/* .locals 3 */
/* .param p1, "pid" # I */
/* .param p2, "token" # Landroid/os/IBinder; */
/* .line 1347 */
v0 = this.mSplitActivityEntryStack;
int v1 = 0; // const/4 v1, 0x0
v0 = if ( v0 != null) { // if-eqz v0, :cond_2
/* if-nez v0, :cond_2 */
/* if-nez p2, :cond_0 */
/* .line 1350 */
} // :cond_0
v0 = this.mSplitActivityEntryStack;
java.lang.Integer .valueOf ( p1 );
/* check-cast v0, Ljava/util/Stack; */
/* .line 1351 */
/* .local v0, "stack":Ljava/util/Stack;, "Ljava/util/Stack<Landroid/os/IBinder;>;" */
if ( v0 != null) { // if-eqz v0, :cond_1
v2 = (( java.util.Stack ) v0 ).empty ( ); // invoke-virtual {v0}, Ljava/util/Stack;->empty()Z
/* if-nez v2, :cond_1 */
(( java.util.Stack ) v0 ).peek ( ); // invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;
v2 = (( java.lang.Object ) p2 ).equals ( v2 ); // invoke-virtual {p2, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
/* .line 1348 */
} // .end local v0 # "stack":Ljava/util/Stack;, "Ljava/util/Stack<Landroid/os/IBinder;>;"
} // :cond_2
} // :goto_0
} // .end method
public Boolean isVibratorActive ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 1837 */
v0 = this.mUsingVibratorUids;
v0 = java.lang.Integer .valueOf ( p1 );
} // .end method
public Boolean killPackageProcesses ( java.lang.String p0, Integer p1, Integer p2, java.lang.String p3 ) {
/* .locals 18 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "appId" # I */
/* .param p3, "userId" # I */
/* .param p4, "reason" # Ljava/lang/String; */
/* .line 1476 */
/* move-object/from16 v1, p0 */
int v2 = 0; // const/4 v2, 0x0
/* .line 1477 */
/* .local v2, "result":Z */
v3 = this.mAmService;
/* monitor-enter v3 */
/* .line 1478 */
try { // :try_start_0
com.android.server.am.ActivityManagerService .boostPriorityForLockedSection ( );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1481 */
try { // :try_start_1
v0 = this.mAmService;
v4 = this.mProcessList;
int v8 = 0; // const/4 v8, 0x0
int v9 = 0; // const/4 v9, 0x0
int v10 = 1; // const/4 v10, 0x1
int v11 = 1; // const/4 v11, 0x1
int v12 = 0; // const/4 v12, 0x0
int v13 = 1; // const/4 v13, 0x1
int v14 = 0; // const/4 v14, 0x0
/* const/16 v15, 0xd */
/* const/16 v16, 0x0 */
/* move-object/from16 v5, p1 */
/* move/from16 v6, p2 */
/* move/from16 v7, p3 */
/* move-object/from16 v17, p4 */
v0 = /* invoke-virtual/range {v4 ..v17}, Lcom/android/server/am/ProcessList;->killPackageProcessesLSP(Ljava/lang/String;IIIZZZZZZIILjava/lang/String;)Z */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* move v2, v0 */
/* .line 1485 */
/* .line 1483 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1484 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_2
final String v4 = "ActivityManagerServiceImpl"; // const-string v4, "ActivityManagerServiceImpl"
final String v5 = "invoke killPackageProcessesLocked error:"; // const-string v5, "invoke killPackageProcessesLocked error:"
android.util.Slog .e ( v4,v5,v0 );
/* .line 1486 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
com.android.server.am.ActivityManagerService .resetPriorityAfterLockedSection ( );
/* .line 1487 */
/* monitor-exit v3 */
/* .line 1488 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit v3 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v0 */
} // .end method
public void killProcessDueToResolutionChanged ( ) {
/* .locals 2 */
/* .line 725 */
/* new-instance v0, Lmiui/process/ProcessConfig; */
/* const/16 v1, 0x12 */
/* invoke-direct {v0, v1}, Lmiui/process/ProcessConfig;-><init>(I)V */
/* .line 726 */
/* .local v0, "config":Lmiui/process/ProcessConfig; */
int v1 = 6; // const/4 v1, 0x6
(( miui.process.ProcessConfig ) v0 ).setPriority ( v1 ); // invoke-virtual {v0, v1}, Lmiui/process/ProcessConfig;->setPriority(I)V
/* .line 727 */
miui.process.ProcessManager .kill ( v0 );
/* .line 728 */
return;
} // .end method
void markAmsReady ( ) {
/* .locals 3 */
/* .line 360 */
miui.mqsas.sdk.BootEventManager .getInstance ( );
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
(( miui.mqsas.sdk.BootEventManager ) v0 ).setAmsReady ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lmiui/mqsas/sdk/BootEventManager;->setAmsReady(J)V
/* .line 361 */
return;
} // .end method
void markUIReady ( ) {
/* .locals 3 */
/* .line 364 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 365 */
/* .local v0, "bootCompleteTime":J */
miui.mqsas.sdk.BootEventManager .getInstance ( );
(( miui.mqsas.sdk.BootEventManager ) v2 ).setUIReady ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Lmiui/mqsas/sdk/BootEventManager;->setUIReady(J)V
/* .line 366 */
miui.mqsas.sdk.BootEventManager .getInstance ( );
(( miui.mqsas.sdk.BootEventManager ) v2 ).setBootComplete ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Lmiui/mqsas/sdk/BootEventManager;->setBootComplete(J)V
/* .line 367 */
return;
} // .end method
public void moveUserToForeground ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "newUserId" # I */
/* .line 735 */
com.android.server.wm.ActivityTaskManagerServiceImpl .getInstance ( );
final String v1 = "moveUserToForeground"; // const-string v1, "moveUserToForeground"
(( com.android.server.wm.ActivityTaskManagerServiceImpl ) v0 ).restartSubScreenUiIfNeeded ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->restartSubScreenUiIfNeeded(ILjava/lang/String;)V
/* .line 736 */
return;
} // .end method
public void notifyCrashToVkService ( java.lang.String p0, Boolean p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "inNative" # Z */
/* .param p3, "stackTrace" # Ljava/lang/String; */
/* .line 1929 */
v0 = this.mForceVkInternal;
/* if-nez v0, :cond_0 */
/* .line 1930 */
/* const-class v0, Lcom/xiaomi/vkmode/service/MiuiForceVkServiceInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/xiaomi/vkmode/service/MiuiForceVkServiceInternal; */
this.mForceVkInternal = v0;
/* .line 1932 */
} // :cond_0
v0 = this.mForceVkInternal;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1933 */
/* .line 1935 */
} // :cond_1
return;
} // .end method
public void notifyDumpAllInfo ( ) {
/* .locals 1 */
/* .line 1954 */
v0 = this.greezer;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1955 */
(( com.miui.server.greeze.GreezeManagerInternal ) v0 ).notifyDumpAllInfo ( ); // invoke-virtual {v0}, Lcom/miui/server/greeze/GreezeManagerInternal;->notifyDumpAllInfo()V
/* .line 1957 */
} // :cond_0
return;
} // .end method
public void notifyDumpAppInfo ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 1948 */
v0 = this.greezer;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1949 */
(( com.miui.server.greeze.GreezeManagerInternal ) v0 ).notifyDumpAppInfo ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerInternal;->notifyDumpAppInfo(II)V
/* .line 1951 */
} // :cond_0
return;
} // .end method
public void notifyExcuteServices ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 2 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 1787 */
v0 = this.greezer;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1788 */
/* iget v1, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
(( com.miui.server.greeze.GreezeManagerInternal ) v0 ).notifyExcuteServices ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/greeze/GreezeManagerInternal;->notifyExcuteServices(I)V
/* .line 1790 */
} // :cond_0
return;
} // .end method
public void onGrantUriPermission ( java.lang.String p0, java.lang.String p1, Integer p2, com.android.server.uri.GrantUri p3 ) {
/* .locals 1 */
/* .param p1, "sourcePkg" # Ljava/lang/String; */
/* .param p2, "targetPkg" # Ljava/lang/String; */
/* .param p3, "targetUid" # I */
/* .param p4, "grantUri" # Lcom/android/server/uri/GrantUri; */
/* .line 1842 */
(( com.android.server.uri.GrantUri ) p4 ).toString ( ); // invoke-virtual {p4}, Lcom/android/server/uri/GrantUri;->toString()Ljava/lang/String;
/* invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/server/am/ActivityManagerServiceImpl;->recordAppBehavior(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V */
/* .line 1843 */
return;
} // .end method
void onSystemReady ( ) {
/* .locals 3 */
/* .line 261 */
miui.mqsas.sdk.BootEventManager .getInstance ( );
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
(( miui.mqsas.sdk.BootEventManager ) v0 ).setAmsReady ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lmiui/mqsas/sdk/BootEventManager;->setAmsReady(J)V
/* .line 262 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mSystemReady:Z */
/* .line 263 */
/* const-class v0, Landroid/content/pm/PackageManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Landroid/content/pm/PackageManagerInternal; */
this.mPackageManager = v0;
/* .line 271 */
try { // :try_start_0
v0 = this.mContext;
com.android.server.am.ActivityManagerServiceImpl .ensureDeviceProvisioned ( v0 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 274 */
/* .line 272 */
/* :catch_0 */
/* move-exception v0 */
/* .line 273 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "ActivityManagerServiceImpl"; // const-string v1, "ActivityManagerServiceImpl"
final String v2 = "ensureDeviceProvisioned occurs Exception."; // const-string v2, "ensureDeviceProvisioned occurs Exception."
android.util.Log .e ( v1,v2,v0 );
/* .line 276 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
com.android.server.am.MemoryStandardProcessControlStub .getInstance ( );
v1 = this.mContext;
v2 = this.mAmService;
/* .line 277 */
com.android.server.am.MemoryFreezeStub .getInstance ( );
v1 = this.mContext;
v2 = this.mAmService;
/* .line 278 */
com.android.server.am.ProcessProphetStub .getInstance ( );
v1 = this.mContext;
v2 = this.mAmService;
/* .line 279 */
com.android.server.wm.MiuiFreezeStub .getInstance ( );
v1 = this.mAmService;
v1 = this.mActivityTaskManager;
/* .line 280 */
return;
} // .end method
public Boolean onTransact ( com.android.server.am.ActivityManagerService p0, Integer p1, android.os.Parcel p2, android.os.Parcel p3, Integer p4 ) {
/* .locals 22 */
/* .param p1, "service" # Lcom/android/server/am/ActivityManagerService; */
/* .param p2, "code" # I */
/* .param p3, "data" # Landroid/os/Parcel; */
/* .param p4, "reply" # Landroid/os/Parcel; */
/* .param p5, "flags" # I */
/* .line 759 */
/* move-object/from16 v0, p0 */
/* move/from16 v1, p2 */
/* move-object/from16 v2, p3 */
/* move-object/from16 v3, p4 */
/* const v4, 0xfffffe */
/* if-ne v1, v4, :cond_0 */
/* .line 760 */
/* move-object/from16 v4, p1 */
v5 = (( com.android.server.am.ActivityManagerServiceImpl ) v0 ).setPackageHoldOn ( v4, v2, v3 ); // invoke-virtual {v0, v4, v2, v3}, Lcom/android/server/am/ActivityManagerServiceImpl;->setPackageHoldOn(Lcom/android/server/am/ActivityManagerService;Landroid/os/Parcel;Landroid/os/Parcel;)Z
/* .line 761 */
} // :cond_0
/* move-object/from16 v4, p1 */
/* const v5, 0xfffffd */
/* if-ne v1, v5, :cond_1 */
/* .line 762 */
v5 = (( com.android.server.am.ActivityManagerServiceImpl ) v0 ).getPackageHoldOn ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lcom/android/server/am/ActivityManagerServiceImpl;->getPackageHoldOn(Landroid/os/Parcel;Landroid/os/Parcel;)Z
/* .line 763 */
} // :cond_1
/* const v5, 0xfffffc */
final String v6 = "android.app.IActivityManager"; // const-string v6, "android.app.IActivityManager"
int v7 = 1; // const/4 v7, 0x1
int v8 = 0; // const/4 v8, 0x0
/* if-ne v1, v5, :cond_2 */
/* .line 764 */
(( android.os.Parcel ) v2 ).enforceInterface ( v6 ); // invoke-virtual {v2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 765 */
com.android.server.wm.ActivityTaskManagerServiceImpl .getInstance ( );
(( com.android.server.wm.ActivityTaskManagerServiceImpl ) v5 ).getLastResumedActivityInfo ( ); // invoke-virtual {v5}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getLastResumedActivityInfo()Landroid/content/pm/ActivityInfo;
/* .line 766 */
/* .local v5, "info":Landroid/content/pm/ActivityInfo; */
/* invoke-virtual/range {p4 ..p4}, Landroid/os/Parcel;->writeNoException()V */
/* .line 767 */
(( android.os.Parcel ) v3 ).writeParcelable ( v5, v8 ); // invoke-virtual {v3, v5, v8}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V
/* .line 768 */
/* .line 771 */
} // .end local v5 # "info":Landroid/content/pm/ActivityInfo;
} // :cond_2
/* const v5, 0xfffda1 */
/* if-ne v1, v5, :cond_3 */
/* .line 772 */
(( android.os.Parcel ) v2 ).enforceInterface ( v6 ); // invoke-virtual {v2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 773 */
v5 = /* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->readInt()I */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder; */
v5 = (( com.android.server.am.ActivityManagerServiceImpl ) v0 ).isTopSplitActivity ( v5, v6 ); // invoke-virtual {v0, v5, v6}, Lcom/android/server/am/ActivityManagerServiceImpl;->isTopSplitActivity(ILandroid/os/IBinder;)Z
/* .line 774 */
/* .local v5, "isTop":I */
/* invoke-virtual/range {p4 ..p4}, Landroid/os/Parcel;->writeNoException()V */
/* .line 775 */
(( android.os.Parcel ) v3 ).writeInt ( v5 ); // invoke-virtual {v3, v5}, Landroid/os/Parcel;->writeInt(I)V
/* .line 776 */
/* .line 778 */
} // .end local v5 # "isTop":I
} // :cond_3
/* const v5, 0xfffda0 */
final String v9 = "ms"; // const-string v9, "ms"
final String v10 = "ActivityManagerServiceImpl"; // const-string v10, "ActivityManagerServiceImpl"
/* if-ne v1, v5, :cond_9 */
/* .line 779 */
v5 = android.os.Binder .getCallingUid ( );
/* .line 780 */
/* .local v5, "uid":I */
v11 = android.os.Binder .getCallingPid ( );
(( com.android.server.am.ActivityManagerServiceImpl ) v0 ).getPackageNameByPid ( v11 ); // invoke-virtual {v0, v11}, Lcom/android/server/am/ActivityManagerServiceImpl;->getPackageNameByPid(I)Ljava/lang/String;
/* .line 781 */
/* .local v15, "pkg":Ljava/lang/String; */
v11 = android.os.UserHandle .isApp ( v5 );
if ( v11 != null) { // if-eqz v11, :cond_7
/* .line 782 */
v11 = this.mPackageManager;
/* const-wide/16 v13, 0x0 */
v16 = android.os.UserHandle .getUserId ( v5 );
/* move-object v12, v15 */
/* move-object v7, v15 */
} // .end local v15 # "pkg":Ljava/lang/String;
/* .local v7, "pkg":Ljava/lang/String; */
/* move v15, v5 */
/* invoke-virtual/range {v11 ..v16}, Landroid/content/pm/PackageManagerInternal;->getApplicationInfo(Ljava/lang/String;JII)Landroid/content/pm/ApplicationInfo; */
/* .line 783 */
/* .local v17, "ainfo":Landroid/content/pm/ApplicationInfo; */
if ( v17 != null) { // if-eqz v17, :cond_4
v11 = /* invoke-virtual/range {v17 ..v17}, Landroid/content/pm/ApplicationInfo;->isSystemApp()Z */
/* if-nez v11, :cond_8 */
v11 = /* invoke-virtual/range {v17 ..v17}, Landroid/content/pm/ApplicationInfo;->isProduct()Z */
/* if-nez v11, :cond_8 */
/* .line 784 */
} // :cond_4
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v18 */
/* .line 785 */
/* .local v18, "startTime":J */
v11 = v11 = com.android.server.am.ActivityManagerServiceImpl.splitDecouplePkgList;
final String v15 = ", security check took: "; // const-string v15, ", security check took: "
/* if-nez v11, :cond_5 */
/* .line 786 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v11 */
/* sub-long v11, v11, v18 */
/* .line 787 */
/* .local v11, "duration":J */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v13 = "Permission Denial: not from uid "; // const-string v13, "Permission Denial: not from uid "
(( java.lang.StringBuilder ) v6 ).append ( v13 ); // invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v15 ); // invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v11, v12 ); // invoke-virtual {v6, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v9 ); // invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v10,v6 );
/* .line 788 */
/* .line 790 */
} // .end local v11 # "duration":J
} // :cond_5
v11 = this.mPackageManager;
/* const-wide/16 v13, 0x40 */
v16 = android.os.UserHandle .getUserId ( v5 );
/* move-object v12, v7 */
/* move-object/from16 v20, v15 */
/* move v15, v5 */
/* invoke-virtual/range {v11 ..v16}, Landroid/content/pm/PackageManagerInternal;->getPackageInfo(Ljava/lang/String;JII)Landroid/content/pm/PackageInfo; */
v11 = this.signatures;
/* aget-object v11, v11, v8 */
/* .line 791 */
/* .local v11, "releaseSig":Landroid/content/pm/Signature; */
v12 = com.android.server.am.ActivityManagerServiceImpl.splitDecouplePkgList;
/* check-cast v12, Ljava/util/List; */
(( android.content.pm.Signature ) v11 ).toByteArray ( ); // invoke-virtual {v11}, Landroid/content/pm/Signature;->toByteArray()[B
v12 = (( com.android.server.am.ActivityManagerServiceImpl ) v0 ).sha256 ( v13 ); // invoke-virtual {v0, v13}, Lcom/android/server/am/ActivityManagerServiceImpl;->sha256([B)Ljava/lang/String;
/* if-nez v12, :cond_6 */
/* .line 792 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v12 */
/* sub-long v12, v12, v18 */
/* .line 793 */
/* .local v12, "duration":J */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v14 = "Permission Denial: not set package "; // const-string v14, "Permission Denial: not set package "
(( java.lang.StringBuilder ) v6 ).append ( v14 ); // invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v14, v20 */
(( java.lang.StringBuilder ) v6 ).append ( v14 ); // invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v12, v13 ); // invoke-virtual {v6, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v9 ); // invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v10,v6 );
/* .line 794 */
/* .line 796 */
} // .end local v12 # "duration":J
} // :cond_6
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v8 */
/* sub-long v8, v8, v18 */
/* .line 797 */
/* .local v8, "duration":J */
/* new-instance v12, Ljava/lang/StringBuilder; */
/* invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v13, "security check took " */
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).append ( v8, v9 ); // invoke-virtual {v12, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v13 = "ms."; // const-string v13, "ms."
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).toString ( ); // invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v10,v12 );
/* .line 781 */
} // .end local v7 # "pkg":Ljava/lang/String;
} // .end local v8 # "duration":J
} // .end local v11 # "releaseSig":Landroid/content/pm/Signature;
} // .end local v17 # "ainfo":Landroid/content/pm/ApplicationInfo;
} // .end local v18 # "startTime":J
/* .restart local v15 # "pkg":Ljava/lang/String; */
} // :cond_7
/* move-object v7, v15 */
/* .line 800 */
} // .end local v15 # "pkg":Ljava/lang/String;
/* .restart local v7 # "pkg":Ljava/lang/String; */
} // :cond_8
} // :goto_0
(( android.os.Parcel ) v2 ).enforceInterface ( v6 ); // invoke-virtual {v2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 801 */
v6 = /* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->readInt()I */
(( com.android.server.am.ActivityManagerServiceImpl ) v0 ).getTopSplitActivity ( v6 ); // invoke-virtual {v0, v6}, Lcom/android/server/am/ActivityManagerServiceImpl;->getTopSplitActivity(I)Landroid/os/IBinder;
/* .line 802 */
/* .local v6, "token":Landroid/os/IBinder; */
/* invoke-virtual/range {p4 ..p4}, Landroid/os/Parcel;->writeNoException()V */
/* .line 803 */
(( android.os.Parcel ) v3 ).writeStrongBinder ( v6 ); // invoke-virtual {v3, v6}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V
/* .line 804 */
int v11 = 1; // const/4 v11, 0x1
/* .line 806 */
} // .end local v5 # "uid":I
} // .end local v6 # "token":Landroid/os/IBinder;
} // .end local v7 # "pkg":Ljava/lang/String;
} // :cond_9
/* move v11, v7 */
/* const v5, 0xfffda2 */
/* if-ne v1, v5, :cond_a */
/* .line 807 */
(( android.os.Parcel ) v2 ).enforceInterface ( v6 ); // invoke-virtual {v2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 808 */
v5 = /* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->readInt()I */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder; */
(( com.android.server.am.ActivityManagerServiceImpl ) v0 ).removeFromEntryStack ( v5, v6 ); // invoke-virtual {v0, v5, v6}, Lcom/android/server/am/ActivityManagerServiceImpl;->removeFromEntryStack(ILandroid/os/IBinder;)V
/* .line 809 */
/* .line 811 */
} // :cond_a
/* const v5, 0xfffda3 */
/* if-ne v1, v5, :cond_b */
/* .line 812 */
(( android.os.Parcel ) v2 ).enforceInterface ( v6 ); // invoke-virtual {v2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 813 */
v5 = /* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->readInt()I */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder; */
(( com.android.server.am.ActivityManagerServiceImpl ) v0 ).clearEntryStack ( v5, v6 ); // invoke-virtual {v0, v5, v6}, Lcom/android/server/am/ActivityManagerServiceImpl;->clearEntryStack(ILandroid/os/IBinder;)V
/* .line 814 */
/* .line 816 */
} // :cond_b
/* const v5, 0xfffda4 */
/* if-ne v1, v5, :cond_c */
/* .line 817 */
(( android.os.Parcel ) v2 ).enforceInterface ( v6 ); // invoke-virtual {v2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 818 */
v5 = /* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->readInt()I */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder; */
v7 = /* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->readInt()I */
v8 = android.content.Intent.CREATOR;
/* check-cast v8, Landroid/content/Intent; */
(( com.android.server.am.ActivityManagerServiceImpl ) v0 ).addToEntryStack ( v5, v6, v7, v8 ); // invoke-virtual {v0, v5, v6, v7, v8}, Lcom/android/server/am/ActivityManagerServiceImpl;->addToEntryStack(ILandroid/os/IBinder;ILandroid/content/Intent;)V
/* .line 819 */
/* invoke-virtual/range {p4 ..p4}, Landroid/os/Parcel;->writeNoException()V */
/* .line 820 */
int v5 = 1; // const/4 v5, 0x1
/* .line 822 */
} // :cond_c
/* const v5, 0xfffda5 */
/* if-ne v1, v5, :cond_e */
/* .line 823 */
(( android.os.Parcel ) v2 ).enforceInterface ( v6 ); // invoke-virtual {v2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 824 */
v5 = /* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->readInt()I */
/* .line 825 */
/* .local v5, "pid":I */
v6 = /* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->readInt()I */
/* if-lez v6, :cond_d */
int v6 = 1; // const/4 v6, 0x1
} // :cond_d
/* move v6, v8 */
/* .line 826 */
/* .local v6, "isForLast":Z */
} // :goto_1
/* invoke-direct {v0, v5, v6}, Lcom/android/server/am/ActivityManagerServiceImpl;->getIntentInfo(IZ)[Landroid/os/Parcelable; */
/* .line 827 */
/* .local v7, "parcelables":[Landroid/os/Parcelable; */
/* invoke-virtual/range {p4 ..p4}, Landroid/os/Parcel;->writeNoException()V */
/* .line 828 */
(( android.os.Parcel ) v3 ).writeParcelableArray ( v7, v8 ); // invoke-virtual {v3, v7, v8}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V
/* .line 829 */
int v8 = 1; // const/4 v8, 0x1
/* .line 831 */
} // .end local v5 # "pid":I
} // .end local v6 # "isForLast":Z
} // .end local v7 # "parcelables":[Landroid/os/Parcelable;
} // :cond_e
/* const v5, 0xfffda6 */
/* if-ne v1, v5, :cond_14 */
/* .line 832 */
v5 = android.os.Binder .getCallingUid ( );
/* .line 833 */
/* .local v5, "uid":I */
v7 = android.os.Binder .getCallingPid ( );
(( com.android.server.am.ActivityManagerServiceImpl ) v0 ).getPackageNameByPid ( v7 ); // invoke-virtual {v0, v7}, Lcom/android/server/am/ActivityManagerServiceImpl;->getPackageNameByPid(I)Ljava/lang/String;
/* .line 834 */
/* .local v7, "pkg":Ljava/lang/String; */
v11 = android.os.UserHandle .isApp ( v5 );
if ( v11 != null) { // if-eqz v11, :cond_12
/* .line 835 */
v11 = this.mPackageManager;
/* const-wide/16 v13, 0x0 */
v16 = android.os.UserHandle .getUserId ( v5 );
/* move-object v12, v7 */
/* move v15, v5 */
/* invoke-virtual/range {v11 ..v16}, Landroid/content/pm/PackageManagerInternal;->getApplicationInfo(Ljava/lang/String;JII)Landroid/content/pm/ApplicationInfo; */
/* .line 836 */
/* .restart local v17 # "ainfo":Landroid/content/pm/ApplicationInfo; */
if ( v17 != null) { // if-eqz v17, :cond_f
v11 = /* invoke-virtual/range {v17 ..v17}, Landroid/content/pm/ApplicationInfo;->isSystemApp()Z */
/* if-nez v11, :cond_12 */
v11 = /* invoke-virtual/range {v17 ..v17}, Landroid/content/pm/ApplicationInfo;->isProduct()Z */
/* if-nez v11, :cond_12 */
/* .line 837 */
} // :cond_f
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v18 */
/* .line 838 */
/* .restart local v18 # "startTime":J */
v11 = v11 = com.android.server.am.ActivityManagerServiceImpl.splitDecouplePkgList;
final String v15 = " security check took: "; // const-string v15, " security check took: "
/* if-nez v11, :cond_10 */
/* .line 839 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v11 */
/* sub-long v11, v11, v18 */
/* .line 840 */
/* .local v11, "duration":J */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v13 = "Permission Denial: not from uid: "; // const-string v13, "Permission Denial: not from uid: "
(( java.lang.StringBuilder ) v6 ).append ( v13 ); // invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v15 ); // invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v11, v12 ); // invoke-virtual {v6, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v9 ); // invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v10,v6 );
/* .line 841 */
/* .line 843 */
} // .end local v11 # "duration":J
} // :cond_10
v11 = this.mPackageManager;
/* const-wide/16 v13, 0x40 */
v16 = android.os.UserHandle .getUserId ( v5 );
/* move-object v12, v7 */
/* move-object/from16 v21, v15 */
/* move v15, v5 */
/* invoke-virtual/range {v11 ..v16}, Landroid/content/pm/PackageManagerInternal;->getPackageInfo(Ljava/lang/String;JII)Landroid/content/pm/PackageInfo; */
v11 = this.signatures;
/* aget-object v11, v11, v8 */
/* .line 844 */
/* .local v11, "releaseSig":Landroid/content/pm/Signature; */
v12 = com.android.server.am.ActivityManagerServiceImpl.splitDecouplePkgList;
/* check-cast v12, Ljava/util/List; */
(( android.content.pm.Signature ) v11 ).toByteArray ( ); // invoke-virtual {v11}, Landroid/content/pm/Signature;->toByteArray()[B
v12 = (( com.android.server.am.ActivityManagerServiceImpl ) v0 ).sha256 ( v13 ); // invoke-virtual {v0, v13}, Lcom/android/server/am/ActivityManagerServiceImpl;->sha256([B)Ljava/lang/String;
/* if-nez v12, :cond_11 */
/* .line 845 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v12 */
/* sub-long v12, v12, v18 */
/* .line 846 */
/* .restart local v12 # "duration":J */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v14 = "Permission Denial: not set package: "; // const-string v14, "Permission Denial: not set package: "
(( java.lang.StringBuilder ) v6 ).append ( v14 ); // invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v14, v21 */
(( java.lang.StringBuilder ) v6 ).append ( v14 ); // invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v12, v13 ); // invoke-virtual {v6, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v9 ); // invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v10,v6 );
/* .line 847 */
/* .line 849 */
} // .end local v12 # "duration":J
} // :cond_11
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v12 */
/* sub-long v12, v12, v18 */
/* .line 850 */
/* .restart local v12 # "duration":J */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v14, "security check took: " */
(( java.lang.StringBuilder ) v8 ).append ( v14 ); // invoke-virtual {v8, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v12, v13 ); // invoke-virtual {v8, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v10,v8 );
/* .line 853 */
} // .end local v11 # "releaseSig":Landroid/content/pm/Signature;
} // .end local v12 # "duration":J
} // .end local v17 # "ainfo":Landroid/content/pm/ApplicationInfo;
} // .end local v18 # "startTime":J
} // :cond_12
int v8 = 0; // const/4 v8, 0x0
/* .line 854 */
/* .local v8, "isForLast":Z */
(( android.os.Parcel ) v2 ).enforceInterface ( v6 ); // invoke-virtual {v2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 855 */
/* const-class v6, Landroid/content/Intent; */
(( java.lang.Class ) v6 ).getClassLoader ( ); // invoke-virtual {v6}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;
(( android.os.Parcel ) v2 ).readParcelable ( v6 ); // invoke-virtual {v2, v6}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;
/* check-cast v6, Landroid/content/Intent; */
/* .line 856 */
/* .local v6, "intent":Landroid/content/Intent; */
v9 = /* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->readInt()I */
/* .line 857 */
/* .local v9, "pid":I */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle; */
/* .line 858 */
/* .local v10, "bundle":Landroid/os/Bundle; */
v11 = /* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->readInt()I */
/* if-lez v11, :cond_13 */
/* .line 859 */
int v8 = 1; // const/4 v8, 0x1
/* .line 861 */
} // :cond_13
/* invoke-direct {v0, v6, v9, v10, v8}, Lcom/android/server/am/ActivityManagerServiceImpl;->setIntentInfo(Landroid/content/Intent;ILandroid/os/Bundle;Z)V */
/* .line 862 */
/* invoke-virtual/range {p4 ..p4}, Landroid/os/Parcel;->writeNoException()V */
/* .line 863 */
int v11 = 1; // const/4 v11, 0x1
/* .line 865 */
} // .end local v5 # "uid":I
} // .end local v6 # "intent":Landroid/content/Intent;
} // .end local v7 # "pkg":Ljava/lang/String;
} // .end local v8 # "isForLast":Z
} // .end local v9 # "pid":I
} // .end local v10 # "bundle":Landroid/os/Bundle;
} // :cond_14
/* const v5, 0xfffd9f */
/* if-ne v1, v5, :cond_15 */
/* .line 866 */
(( android.os.Parcel ) v2 ).enforceInterface ( v6 ); // invoke-virtual {v2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 867 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder; */
/* .line 868 */
/* .local v5, "token":Landroid/os/IBinder; */
com.android.server.wm.ActivityTaskManagerServiceStub .get ( );
(( com.android.server.wm.ActivityTaskManagerServiceStub ) v6 ).getSplitTaskInfo ( v5 ); // invoke-virtual {v6, v5}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->getSplitTaskInfo(Landroid/os/IBinder;)Landroid/app/ActivityManager$RunningTaskInfo;
/* .line 869 */
/* .local v6, "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo; */
/* invoke-virtual/range {p4 ..p4}, Landroid/os/Parcel;->writeNoException()V */
/* .line 870 */
(( android.os.Parcel ) v3 ).writeParcelable ( v6, v8 ); // invoke-virtual {v3, v6, v8}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V
/* .line 871 */
int v7 = 1; // const/4 v7, 0x1
/* .line 874 */
} // .end local v5 # "token":Landroid/os/IBinder;
} // .end local v6 # "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
} // :cond_15
} // .end method
public void recordBroadcastLog ( com.android.server.am.ProcessRecord p0, java.lang.String p1, java.lang.String p2, Integer p3 ) {
/* .locals 6 */
/* .param p1, "callerApp" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "action" # Ljava/lang/String; */
/* .param p3, "callerPackage" # Ljava/lang/String; */
/* .param p4, "callingUid" # I */
/* .line 1894 */
int v0 = 5; // const/4 v0, 0x5
final String v1 = ".Callers="; // const-string v1, ".Callers="
final String v2 = " pkg "; // const-string v2, " pkg "
final String v3 = "Sending non-protected broadcast "; // const-string v3, "Sending non-protected broadcast "
final String v4 = "ActivityManagerServiceImpl"; // const-string v4, "ActivityManagerServiceImpl"
if ( p1 != null) { // if-eqz p1, :cond_2
/* .line 1895 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " from system "; // const-string v5, " from system "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1896 */
(( com.android.server.am.ProcessRecord ) p1 ).toShortString ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->toShortString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p3 ); // invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1897 */
if ( p3 != null) { // if-eqz p3, :cond_1
final String v2 = "android"; // const-string v2, "android"
v2 = (( java.lang.String ) v2 ).equals ( p3 ); // invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1898 */
} // :cond_0
final String v0 = ""; // const-string v0, ""
} // :cond_1
} // :goto_0
android.os.Debug .getCallers ( v0 );
} // :goto_1
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1895 */
android.util.Log .w ( v4,v0 );
/* .line 1900 */
} // :cond_2
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " from system uid "; // const-string v5, " from system uid "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1901 */
android.os.UserHandle .formatUid ( p4 );
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p3 ); // invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1902 */
android.os.Debug .getCallers ( v0 );
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1900 */
android.util.Log .w ( v4,v0 );
/* .line 1904 */
} // :goto_2
return;
} // .end method
public void removeFromEntryStack ( Integer p0, android.os.IBinder p1 ) {
/* .locals 5 */
/* .param p1, "pid" # I */
/* .param p2, "token" # Landroid/os/IBinder; */
/* .line 1363 */
v0 = this.splitEntryStackLock;
/* monitor-enter v0 */
/* .line 1364 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 1365 */
try { // :try_start_0
v1 = this.mSplitActivityEntryStack;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1366 */
java.lang.Integer .valueOf ( p1 );
/* check-cast v1, Ljava/util/Stack; */
/* .line 1367 */
/* .local v1, "stack":Ljava/util/Stack;, "Ljava/util/Stack<Landroid/os/IBinder;>;" */
if ( v1 != null) { // if-eqz v1, :cond_0
v2 = (( java.util.Stack ) v1 ).empty ( ); // invoke-virtual {v1}, Ljava/util/Stack;->empty()Z
/* if-nez v2, :cond_0 */
/* .line 1368 */
(( java.util.Stack ) v1 ).remove ( p2 ); // invoke-virtual {v1, p2}, Ljava/util/Stack;->remove(Ljava/lang/Object;)Z
/* .line 1369 */
final String v2 = "ActivityManagerServiceImpl"; // const-string v2, "ActivityManagerServiceImpl"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "SplitEntryStack: remove from stack, size="; // const-string v4, "SplitEntryStack: remove from stack, size="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = (( java.util.Stack ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/Stack;->size()I
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " pid="; // const-string v4, " pid="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 1373 */
} // .end local v1 # "stack":Ljava/util/Stack;, "Ljava/util/Stack<Landroid/os/IBinder;>;"
} // :cond_0
/* monitor-exit v0 */
/* .line 1374 */
return;
/* .line 1373 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
void reportBootEvent ( ) {
/* .locals 0 */
/* .line 370 */
miui.mqsas.sdk.BootEventManager .getInstance ( );
miui.mqsas.sdk.BootEventManager .reportBootEvent ( );
/* .line 371 */
return;
} // .end method
public void setActiveInstrumentation ( android.content.ComponentName p0 ) {
/* .locals 4 */
/* .param p1, "instr" # Landroid/content/ComponentName; */
/* .line 1813 */
int v0 = -1; // const/4 v0, -0x1
if ( p1 != null) { // if-eqz p1, :cond_2
/* .line 1814 */
(( android.content.ComponentName ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 1815 */
/* .local v1, "mInstrPkg":Ljava/lang/String; */
/* if-nez v1, :cond_0 */
return;
/* .line 1817 */
} // :cond_0
try { // :try_start_0
v2 = this.mContext;
(( android.content.Context ) v2 ).getPackageManager ( ); // invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 1818 */
/* .local v2, "mPm":Landroid/content/pm/PackageManager; */
int v3 = 0; // const/4 v3, 0x0
(( android.content.pm.PackageManager ) v2 ).getApplicationInfo ( v1, v3 ); // invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
/* .line 1819 */
/* .local v3, "info":Landroid/content/pm/ApplicationInfo; */
if ( v3 != null) { // if-eqz v3, :cond_1
/* iget v0, v3, Landroid/content/pm/ApplicationInfo;->uid:I */
} // :cond_1
/* iput v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mInstrUid:I */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1821 */
} // .end local v2 # "mPm":Landroid/content/pm/PackageManager;
} // .end local v3 # "info":Landroid/content/pm/ApplicationInfo;
/* .line 1820 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1822 */
} // .end local v1 # "mInstrPkg":Ljava/lang/String;
} // :goto_0
/* .line 1823 */
} // :cond_2
/* iput v0, p0, Lcom/android/server/am/ActivityManagerServiceImpl;->mInstrUid:I */
/* .line 1825 */
} // :goto_1
return;
} // .end method
public Boolean setPackageHoldOn ( com.android.server.am.ActivityManagerService p0, android.os.Parcel p1, android.os.Parcel p2 ) {
/* .locals 6 */
/* .param p1, "service" # Lcom/android/server/am/ActivityManagerService; */
/* .param p2, "data" # Landroid/os/Parcel; */
/* .param p3, "reply" # Landroid/os/Parcel; */
/* .line 898 */
final String v0 = "android.app.IActivityManager"; // const-string v0, "android.app.IActivityManager"
(( android.os.Parcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 899 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 900 */
/* .local v0, "callingUid":I */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v1 */
/* .line 902 */
/* .local v1, "ident":J */
try { // :try_start_0
v3 = android.os.UserHandle .getAppId ( v0 );
/* const/16 v4, 0x3e8 */
/* if-ne v3, v4, :cond_0 */
/* .line 903 */
com.android.server.wm.ActivityTaskManagerServiceStub .get ( );
v4 = this.mActivityTaskManager;
/* .line 904 */
(( android.os.Parcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;
/* .line 903 */
(( com.android.server.wm.ActivityTaskManagerServiceStub ) v3 ).setPackageHoldOn ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->setPackageHoldOn(Lcom/android/server/wm/ActivityTaskManagerService;Ljava/lang/String;)V
/* .line 906 */
} // :cond_0
final String v3 = "ActivityManagerServiceImpl"; // const-string v3, "ActivityManagerServiceImpl"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Permission Denial: setPackageHoldOn() not from system uid "; // const-string v5, "Permission Denial: setPackageHoldOn() not from system uid "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 910 */
} // :goto_0
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 911 */
/* nop */
/* .line 912 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 913 */
int v3 = 1; // const/4 v3, 0x1
/* .line 910 */
/* :catchall_0 */
/* move-exception v3 */
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 911 */
/* throw v3 */
} // .end method
public void setVibratorState ( Integer p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "active" # Z */
/* .line 1831 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 1832 */
v0 = this.mUsingVibratorUids;
java.lang.Integer .valueOf ( p1 );
/* .line 1834 */
} // :cond_0
v0 = this.mUsingVibratorUids;
java.lang.Integer .valueOf ( p1 );
/* .line 1835 */
} // :goto_0
return;
} // .end method
public java.lang.String sha256 ( Object[] p0 ) {
/* .locals 7 */
/* .param p1, "bytes" # [B */
/* .line 1961 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1963 */
/* .local v0, "digest":Ljava/security/MessageDigest; */
try { // :try_start_0
final String v1 = "SHA-256"; // const-string v1, "SHA-256"
java.security.MessageDigest .getInstance ( v1 );
/* move-object v0, v1 */
/* .line 1964 */
(( java.security.MessageDigest ) v0 ).digest ( p1 ); // invoke-virtual {v0, p1}, Ljava/security/MessageDigest;->digest([B)[B
/* .line 1965 */
/* .local v1, "hash":[B */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* array-length v3, v1 */
/* mul-int/lit8 v3, v3, 0x2 */
/* invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V */
/* .line 1966 */
/* .local v2, "hexString":Ljava/lang/StringBuilder; */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
/* array-length v4, v1 */
/* if-ge v3, v4, :cond_1 */
/* .line 1967 */
/* aget-byte v4, v1, v3 */
/* and-int/lit16 v4, v4, 0xff */
java.lang.Integer .toHexString ( v4 );
/* .line 1968 */
/* .local v4, "hex":Ljava/lang/String; */
v5 = (( java.lang.String ) v4 ).length ( ); // invoke-virtual {v4}, Ljava/lang/String;->length()I
int v6 = 1; // const/4 v6, 0x1
/* if-ne v5, v6, :cond_0 */
/* .line 1969 */
/* const/16 v5, 0x30 */
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 1971 */
} // :cond_0
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1966 */
/* nop */
} // .end local v4 # "hex":Ljava/lang/String;
/* add-int/lit8 v3, v3, 0x1 */
/* .line 1973 */
} // .end local v3 # "i":I
} // :cond_1
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* :try_end_0 */
/* .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1974 */
} // .end local v1 # "hash":[B
} // .end local v2 # "hexString":Ljava/lang/StringBuilder;
/* :catch_0 */
/* move-exception v1 */
/* .line 1975 */
/* .local v1, "e":Ljava/security/NoSuchAlgorithmException; */
(( java.security.NoSuchAlgorithmException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V
/* .line 1977 */
} // .end local v1 # "e":Ljava/security/NoSuchAlgorithmException;
final String v1 = ""; // const-string v1, ""
} // .end method
public Boolean skipFrozenAppAnr ( android.content.pm.ApplicationInfo p0, Integer p1, java.lang.String p2 ) {
/* .locals 10 */
/* .param p1, "info" # Landroid/content/pm/ApplicationInfo; */
/* .param p2, "uid" # I */
/* .param p3, "report" # Ljava/lang/String; */
/* .line 1299 */
/* invoke-direct {p0}, Lcom/android/server/am/ActivityManagerServiceImpl;->getGreezeService()Lcom/miui/server/greeze/GreezeManagerInternal; */
/* .line 1300 */
/* .local v0, "greezer":Lcom/miui/server/greeze/GreezeManagerInternal; */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 1302 */
} // :cond_0
/* iget v2, p1, Landroid/content/pm/ApplicationInfo;->uid:I */
v2 = android.os.UserHandle .getAppId ( v2 );
/* .line 1303 */
/* .local v2, "appid":I */
/* iget v3, p1, Landroid/content/pm/ApplicationInfo;->uid:I */
/* if-eq p2, v3, :cond_1 */
/* .line 1304 */
v2 = android.os.UserHandle .getAppId ( p2 );
/* .line 1305 */
} // :cond_1
/* const/16 v3, 0x3e8 */
/* if-gt v2, v3, :cond_2 */
/* .line 1307 */
} // :cond_2
/* const-class v3, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
com.android.server.LocalServices .getService ( v3 );
v3 = /* check-cast v3, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
int v4 = 1; // const/4 v4, 0x1
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 1308 */
/* .line 1311 */
} // :cond_3
v3 = (( com.miui.server.greeze.GreezeManagerInternal ) v0 ).isUidFrozen ( v2 ); // invoke-virtual {v0, v2}, Lcom/miui/server/greeze/GreezeManagerInternal;->isUidFrozen(I)Z
final String v5 = "ActivityManagerServiceImpl"; // const-string v5, "ActivityManagerServiceImpl"
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 1312 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " matched appid is "; // const-string v3, " matched appid is "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v1 );
/* .line 1313 */
/* .line 1315 */
} // :cond_4
v3 = com.android.server.am.ActivityManagerServiceImpl .checkThawTime ( p2,p3,v0 );
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 1316 */
/* .line 1317 */
} // :cond_5
(( com.miui.server.greeze.GreezeManagerInternal ) v0 ).getFrozenUids ( v3 ); // invoke-virtual {v0, v3}, Lcom/miui/server/greeze/GreezeManagerInternal;->getFrozenUids(I)[I
/* .line 1318 */
/* .local v3, "frozenUids":[I */
(( com.miui.server.greeze.GreezeManagerInternal ) v0 ).getFrozenPids ( v4 ); // invoke-virtual {v0, v4}, Lcom/miui/server/greeze/GreezeManagerInternal;->getFrozenPids(I)[I
/* .line 1319 */
/* .local v4, "frozenPids":[I */
/* array-length v6, v3 */
/* if-lez v6, :cond_6 */
/* .line 1320 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "frozen uids:"; // const-string v7, "frozen uids:"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .toString ( v3 );
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v6 );
/* .line 1321 */
} // :cond_6
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 1322 */
/* .local v6, "inf":Ljava/lang/StringBuilder; */
int v7 = 0; // const/4 v7, 0x0
/* .local v7, "i":I */
} // :goto_0
/* array-length v8, v4 */
/* if-ge v7, v8, :cond_7 */
/* .line 1323 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "pid: "; // const-string v9, "pid: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget v9, v4, v7 */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = "/uid:"; // const-string v9, "/uid:"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget v9, v4, v7 */
v9 = android.os.Process .getUidForPid ( v9 );
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = ","; // const-string v9, ","
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1322 */
/* add-int/lit8 v7, v7, 0x1 */
/* .line 1325 */
} // .end local v7 # "i":I
} // :cond_7
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "frozen procs: "; // const-string v8, "frozen procs: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v7 );
/* .line 1326 */
} // .end method
public Boolean skipFrozenServiceTimeout ( com.android.server.am.ProcessRecord p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "fg" # Z */
/* .line 1940 */
v0 = this.greezer;
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p1 != null) { // if-eqz p1, :cond_0
/* iget v1, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
v0 = (( com.miui.server.greeze.GreezeManagerInternal ) v0 ).isUidFrozen ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/greeze/GreezeManagerInternal;->isUidFrozen(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1941 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Skip Frozen Uid: "; // const-string v1, "Skip Frozen Uid: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " service Timeout! fg: "; // const-string v1, " service Timeout! fg: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "ActivityManagerServiceImpl"; // const-string v1, "ActivityManagerServiceImpl"
android.util.Slog .d ( v1,v0 );
/* .line 1942 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1944 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean skipPruneOldTraces ( ) {
/* .locals 1 */
/* .line 1634 */
v0 = miui.mqsas.scout.ScoutUtils .isLibraryTest ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1635 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1637 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void startProcessLocked ( com.android.server.am.ProcessRecord p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 11 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "hostingType" # Ljava/lang/String; */
/* .param p3, "hostingNameStr" # Ljava/lang/String; */
/* .line 416 */
v0 = this.callerPackage;
/* .line 417 */
/* .local v0, "callerPackage":Ljava/lang/String; */
final String v1 = "com.xiaomi.xmsf"; // const-string v1, "com.xiaomi.xmsf"
v1 = (( java.lang.String ) v1 ).equalsIgnoreCase ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
/* if-nez v1, :cond_0 */
/* .line 418 */
final String v1 = "com.miui.notification"; // const-string v1, "com.miui.notification"
v1 = (( java.lang.String ) v1 ).equalsIgnoreCase ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 419 */
} // :cond_0
v1 = this.mAmService;
v1 = this.mProcessList;
/* iget v2, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
(( com.android.server.am.ProcessList ) v1 ).getUidRecordLOSP ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessList;->getUidRecordLOSP(I)Lcom/android/server/am/UidRecord;
/* .line 420 */
/* .local v1, "record":Lcom/android/server/am/UidRecord; */
if ( v1 != null) { // if-eqz v1, :cond_1
v2 = (( com.android.server.am.UidRecord ) v1 ).isIdle ( ); // invoke-virtual {v1}, Lcom/android/server/am/UidRecord;->isIdle()Z
if ( v2 != null) { // if-eqz v2, :cond_1
v2 = (( com.android.server.am.UidRecord ) v1 ).isCurAllowListed ( ); // invoke-virtual {v1}, Lcom/android/server/am/UidRecord;->isCurAllowListed()Z
/* if-nez v2, :cond_1 */
/* .line 421 */
v2 = android.os.Binder .getCallingUid ( );
/* .line 422 */
/* .local v2, "callingUid":I */
v3 = this.mAmService;
v4 = (( com.android.server.am.UidRecord ) v1 ).getUid ( ); // invoke-virtual {v1}, Lcom/android/server/am/UidRecord;->getUid()I
/* const-wide/32 v5, 0xea60 */
/* const/16 v7, 0x65 */
final String v8 = "push-service-launch"; // const-string v8, "push-service-launch"
int v9 = 0; // const/4 v9, 0x0
/* move v10, v2 */
/* invoke-virtual/range {v3 ..v10}, Lcom/android/server/am/ActivityManagerService;->tempAllowlistUidLocked(IJILjava/lang/String;II)V */
/* .line 430 */
} // .end local v1 # "record":Lcom/android/server/am/UidRecord;
} // .end local v2 # "callingUid":I
} // :cond_1
return;
} // .end method
public void syncFontForWebView ( ) {
/* .locals 0 */
/* .line 1493 */
miui.util.font.SymlinkUtils .onAttachApplication ( );
/* .line 1494 */
return;
} // .end method
