.class public Lcom/android/server/am/ProcessProphetImpl;
.super Ljava/lang/Object;
.source "ProcessProphetImpl.java"

# interfaces
.implements Lcom/android/server/am/ProcessProphetStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/ProcessProphetImpl$MyHandler;,
        Lcom/android/server/am/ProcessProphetImpl$BinderService;,
        Lcom/android/server/am/ProcessProphetImpl$MyReceiver;,
        Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;
    }
.end annotation


# static fields
.field private static final COPY_INTERVAL:J = 0x7d0L

.field private static DEBUG:Z = false

.field private static EMPTY_PROCS_PSS_THRESHOLD:J = 0x0L

.field private static final FLAG_PRELOAD:Ljava/lang/String; = "ProcessProphet"

.field private static MEM_THRESHOLD:J = 0x0L

.field private static final MSG_AUDIO_FOCUS_CHANGED:I = 0x4

.field private static final MSG_BT_CONNECTED:I = 0x2

.field private static final MSG_BT_DISCONNECTED:I = 0x3

.field private static final MSG_COPY:I = 0x6

.field private static final MSG_DUMP:I = 0xd

.field private static final MSG_IDLE_UPDATE:I = 0xb

.field private static final MSG_KILL_PROC:I = 0xa

.field private static final MSG_LAUNCH_EVENT:I = 0x5

.field private static final MSG_MEM_PRESSURE:I = 0xc

.field private static final MSG_MTBF_TEST:I = 0xe

.field private static final MSG_PROC_CHECK:I = 0x9

.field private static final MSG_PROC_DIED:I = 0x8

.field private static final MSG_PROC_START:I = 0x7

.field private static final MSG_REGISTER_CLOUD_OBSERVER:I = 0xf

.field private static final MSG_UNLOCK:I = 0x1

.field private static final PREDICTION_INTERVAL:J = 0x1d4c0L

.field private static final TAG:Ljava/lang/String; = "ProcessProphet"

.field private static TRACK_INTERVAL_TIME:I = 0x0

.field private static final WHITELIST_DEFAULT_PATH:Ljava/lang/String; = "/product/etc/procprophet.xml"

.field private static final mEmptyThreslist:[I

.field private static sBlackListBTAudio:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sBlackListLaunch:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sWhiteListEmptyProc:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private afterFirstUnlock:Z

.field private mAMKilledProcs:J

.field private mAMS:Lcom/android/server/am/ActivityManagerService;

.field private mATMS:Lcom/android/server/wm/ActivityTaskManagerService;

.field public mAliveEmptyProcs:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/android/server/am/ProcessRecord;",
            ">;"
        }
    .end annotation
.end field

.field private mBTConnected:Z

.field private mContext:Landroid/content/Context;

.field private mEnable:Z

.field public mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mHits:J

.field private mInitialized:Z

.field private mKilledProcs:J

.field private mLastAudioFocusPkg:Ljava/lang/String;

.field private mLastCopyUpTime:J

.field private mLastEmptyProcStartUpTime:J

.field private mLastLaunchedPkg:Ljava/lang/String;

.field private mLastReportTime:J

.field private mModelPredList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field private mModelTrackList:[J

.field private final mModelTrackLock:Ljava/lang/Object;

.field private mNativeKilledProcs:J

.field private mPMS:Lcom/android/server/am/ProcessManagerService;

.field private mProcessProphetCloud:Lcom/android/server/am/ProcessProphetCloud;

.field private mProcessProphetModel:Lcom/android/server/am/ProcessProphetModel;

.field private mStartedProcs:J


# direct methods
.method static bridge synthetic -$$Nest$fgetmLastCopyUpTime(Lcom/android/server/am/ProcessProphetImpl;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mLastCopyUpTime:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmProcessProphetCloud(Lcom/android/server/am/ProcessProphetImpl;)Lcom/android/server/am/ProcessProphetCloud;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/ProcessProphetImpl;->mProcessProphetCloud:Lcom/android/server/am/ProcessProphetCloud;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmProcessProphetModel(Lcom/android/server/am/ProcessProphetImpl;)Lcom/android/server/am/ProcessProphetModel;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/ProcessProphetImpl;->mProcessProphetModel:Lcom/android/server/am/ProcessProphetModel;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmLastCopyUpTime(Lcom/android/server/am/ProcessProphetImpl;J)V
    .locals 0

    iput-wide p1, p0, Lcom/android/server/am/ProcessProphetImpl;->mLastCopyUpTime:J

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetUploadData(Lcom/android/server/am/ProcessProphetImpl;)Lcom/android/internal/app/ProcessProphetInfo;
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/ProcessProphetImpl;->getUploadData()Lcom/android/internal/app/ProcessProphetInfo;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mhandleAudioFocusChanged(Lcom/android/server/am/ProcessProphetImpl;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessProphetImpl;->handleAudioFocusChanged(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleBluetoothConnected(Lcom/android/server/am/ProcessProphetImpl;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessProphetImpl;->handleBluetoothConnected(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleBluetoothDisConnected(Lcom/android/server/am/ProcessProphetImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/ProcessProphetImpl;->handleBluetoothDisConnected()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleCheckEmptyProcs(Lcom/android/server/am/ProcessProphetImpl;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessProphetImpl;->handleCheckEmptyProcs(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleCopy(Lcom/android/server/am/ProcessProphetImpl;Ljava/lang/CharSequence;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessProphetImpl;->handleCopy(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleDump(Lcom/android/server/am/ProcessProphetImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/ProcessProphetImpl;->handleDump()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleIdleUpdate(Lcom/android/server/am/ProcessProphetImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/ProcessProphetImpl;->handleIdleUpdate()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleKillProc(Lcom/android/server/am/ProcessProphetImpl;Lcom/android/server/am/ProcessRecord;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessProphetImpl;->handleKillProc(Lcom/android/server/am/ProcessRecord;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleLaunchEvent(Lcom/android/server/am/ProcessProphetImpl;Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/am/ProcessProphetImpl;->handleLaunchEvent(Ljava/lang/String;II)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleMemPressure(Lcom/android/server/am/ProcessProphetImpl;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessProphetImpl;->handleMemPressure(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandlePostInit(Lcom/android/server/am/ProcessProphetImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/ProcessProphetImpl;->handlePostInit()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleProcStarted(Lcom/android/server/am/ProcessProphetImpl;Lcom/android/server/am/ProcessRecord;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/am/ProcessProphetImpl;->handleProcStarted(Lcom/android/server/am/ProcessRecord;I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleUnlock(Lcom/android/server/am/ProcessProphetImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/ProcessProphetImpl;->handleUnlock()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mstartEmptyProc(Lcom/android/server/am/ProcessProphetImpl;Ljava/lang/String;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessProphetImpl;->startEmptyProc(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mtryToStartEmptyProc(Lcom/android/server/am/ProcessProphetImpl;Ljava/lang/String;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessProphetImpl;->tryToStartEmptyProc(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$sfgetDEBUG()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z

    return v0
.end method

.method static constructor <clinit>()V
    .locals 4

    .line 90
    const-wide/32 v0, 0x96000

    sput-wide v0, Lcom/android/server/am/ProcessProphetImpl;->MEM_THRESHOLD:J

    .line 92
    const-wide/32 v0, 0x7d000

    sput-wide v0, Lcom/android/server/am/ProcessProphetImpl;->EMPTY_PROCS_PSS_THRESHOLD:J

    .line 94
    const/4 v0, 0x1

    sput v0, Lcom/android/server/am/ProcessProphetImpl;->TRACK_INTERVAL_TIME:I

    .line 97
    const/16 v0, 0x258

    const/16 v1, 0x320

    const/4 v2, 0x0

    const/16 v3, 0x1f4

    filled-new-array {v2, v3, v0, v1}, [I

    move-result-object v0

    sput-object v0, Lcom/android/server/am/ProcessProphetImpl;->mEmptyThreslist:[I

    .line 99
    nop

    .line 100
    const-string v0, "persist.sys.procprophet.debug"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z

    .line 141
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/server/am/ProcessProphetImpl;->sWhiteListEmptyProc:Ljava/util/HashSet;

    .line 142
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/server/am/ProcessProphetImpl;->sBlackListLaunch:Ljava/util/HashSet;

    .line 143
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/server/am/ProcessProphetImpl;->sBlackListBTAudio:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    nop

    .line 102
    const-string v0, "persist.sys.procprophet.enable"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mEnable:Z

    .line 104
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mAMS:Lcom/android/server/am/ActivityManagerService;

    .line 105
    iput-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mATMS:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 106
    iput-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mPMS:Lcom/android/server/am/ProcessManagerService;

    .line 107
    iput-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mContext:Landroid/content/Context;

    .line 109
    iput-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    .line 110
    iput-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandlerThread:Landroid/os/HandlerThread;

    .line 111
    iput-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mProcessProphetModel:Lcom/android/server/am/ProcessProphetModel;

    .line 112
    iput-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mProcessProphetCloud:Lcom/android/server/am/ProcessProphetCloud;

    .line 114
    iput-boolean v1, p0, Lcom/android/server/am/ProcessProphetImpl;->afterFirstUnlock:Z

    .line 115
    iput-boolean v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mBTConnected:Z

    .line 116
    iput-boolean v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mInitialized:Z

    .line 118
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mLastEmptyProcStartUpTime:J

    .line 119
    iput-wide v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mLastCopyUpTime:J

    .line 122
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/android/server/am/ProcessProphetImpl;->mModelTrackLock:Ljava/lang/Object;

    .line 124
    iput-wide v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mLastReportTime:J

    .line 125
    iput-wide v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mStartedProcs:J

    .line 126
    iput-wide v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mHits:J

    .line 127
    iput-wide v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mKilledProcs:J

    .line 128
    iput-wide v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mAMKilledProcs:J

    .line 129
    iput-wide v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mNativeKilledProcs:J

    .line 130
    const/4 v0, 0x6

    new-array v1, v0, [J

    iput-object v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mModelTrackList:[J

    .line 132
    new-instance v1, Ljava/util/ArrayList;

    .line 134
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-static {v0, v2}, Ljava/util/Collections;->nCopies(ILjava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mModelPredList:Ljava/util/ArrayList;

    .line 137
    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mLastLaunchedPkg:Ljava/lang/String;

    .line 138
    iput-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mLastAudioFocusPkg:Ljava/lang/String;

    .line 140
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mAliveEmptyProcs:Ljava/util/HashMap;

    return-void
.end method

.method private allowToStartNewProc()Z
    .locals 6

    .line 608
    sget-boolean v0, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 609
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/android/server/am/ProcessProphetImpl;->mLastEmptyProcStartUpTime:J

    sub-long/2addr v2, v4

    .line 610
    .local v2, "uptimeDiff":J
    const-wide/32 v4, 0x1d4c0

    cmp-long v0, v2, v4

    if-gez v0, :cond_0

    .line 611
    return v1

    .line 616
    .end local v2    # "uptimeDiff":J
    :cond_0
    const-class v0, Landroid/os/BatteryManagerInternal;

    .line 617
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/BatteryManagerInternal;

    invoke-virtual {v0}, Landroid/os/BatteryManagerInternal;->getBatteryLevel()I

    move-result v0

    .line 618
    .local v0, "batteryLevel":I
    const/16 v2, 0x14

    const-string v3, "ProcessProphet"

    if-ge v0, v2, :cond_2

    .line 619
    sget-boolean v2, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "batteryLevel low: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 620
    :cond_1
    return v1

    .line 624
    :cond_2
    invoke-direct {p0}, Lcom/android/server/am/ProcessProphetImpl;->estimateMemPressure()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 625
    sget-boolean v2, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z

    if-eqz v2, :cond_3

    const-string/jumbo v2, "terminate proc start by mem pressure."

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 626
    :cond_3
    return v1

    .line 629
    :cond_4
    const/4 v1, 0x1

    return v1
.end method

.method private estimateMemPressure()Z
    .locals 10

    .line 922
    new-instance v0, Lcom/android/internal/util/MemInfoReader;

    invoke-direct {v0}, Lcom/android/internal/util/MemInfoReader;-><init>()V

    .line 923
    .local v0, "minfo":Lcom/android/internal/util/MemInfoReader;
    invoke-virtual {v0}, Lcom/android/internal/util/MemInfoReader;->readMemInfo()V

    .line 924
    invoke-virtual {v0}, Lcom/android/internal/util/MemInfoReader;->getRawInfo()[J

    move-result-object v1

    .line 925
    .local v1, "rawInfo":[J
    const/4 v2, 0x3

    aget-wide v2, v1, v2

    const/16 v4, 0x1a

    aget-wide v5, v1, v4

    add-long/2addr v2, v5

    const/4 v5, 0x2

    aget-wide v5, v1, v5

    add-long/2addr v2, v5

    .line 928
    .local v2, "otherFile":J
    const/4 v5, 0x4

    aget-wide v5, v1, v5

    const/16 v7, 0x12

    aget-wide v7, v1, v7

    add-long/2addr v5, v7

    aget-wide v7, v1, v4

    add-long/2addr v5, v7

    .line 931
    .local v5, "needRemovedFile":J
    cmp-long v4, v2, v5

    if-lez v4, :cond_0

    .line 932
    sub-long/2addr v2, v5

    .line 934
    :cond_0
    sget-boolean v4, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z

    const-string v7, "ProcessProphet"

    if-eqz v4, :cond_1

    .line 935
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Other File: "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, "KB."

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v7, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 937
    :cond_1
    sget-wide v8, Lcom/android/server/am/ProcessProphetImpl;->MEM_THRESHOLD:J

    cmp-long v4, v2, v8

    if-gez v4, :cond_2

    .line 938
    const-string v4, "mem pressure is critical."

    invoke-static {v7, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 939
    const/4 v4, 0x1

    return v4

    .line 941
    :cond_2
    const/4 v4, 0x0

    return v4
.end method

.method private getPssByUid(Ljava/lang/String;I)Ljava/lang/Long;
    .locals 22
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 945
    move-object/from16 v0, p1

    move/from16 v1, p2

    const/4 v2, 0x0

    if-eqz v0, :cond_5

    const-string v3, ""

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    if-gtz v1, :cond_0

    goto/16 :goto_2

    .line 948
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    .line 949
    .local v3, "startUpTime":J
    move-object/from16 v5, p0

    iget-object v6, v5, Lcom/android/server/am/ProcessProphetImpl;->mPMS:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v6, v0, v1}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordListByPackageAndUid(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v6

    .line 950
    .local v6, "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    sget-boolean v7, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z

    const-string v8, "ms"

    const-string v9, "ProcessProphet"

    if-eqz v7, :cond_1

    .line 951
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "get procs by uid consumed "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 952
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    sub-long/2addr v10, v3

    invoke-virtual {v7, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 951
    invoke-static {v9, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 955
    :cond_1
    const-wide/16 v10, 0x0

    .line 956
    .local v10, "totalPss":J
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/android/server/am/ProcessRecord;

    .line 957
    .local v12, "app":Lcom/android/server/am/ProcessRecord;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v15

    .line 958
    .local v15, "startUpTimeGetPss":J
    iget v13, v12, Lcom/android/server/am/ProcessRecord;->mPid:I

    invoke-static {v13, v2, v2}, Landroid/os/Debug;->getPss(I[J[J)J

    move-result-wide v13

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    .line 959
    .local v13, "dPss":Ljava/lang/Long;
    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v19

    add-long v10, v10, v19

    .line 960
    sget-boolean v14, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z

    if-eqz v14, :cond_2

    .line 961
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\tEmptyProcs "

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v14, v12, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v14, "("

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v14, v12, Lcom/android/server/am/ProcessRecord;->mPid:I

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v14, "): Pss="

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 962
    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    move-object v14, v6

    const-wide/16 v17, 0x400

    .end local v6    # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    .local v14, "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    div-long v5, v20, v17

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "MB, querying consumed "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 963
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v15

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 961
    invoke-static {v9, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 960
    .end local v14    # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    .restart local v6    # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    :cond_2
    move-object v14, v6

    .line 965
    .end local v6    # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    .end local v12    # "app":Lcom/android/server/am/ProcessRecord;
    .end local v13    # "dPss":Ljava/lang/Long;
    .end local v15    # "startUpTimeGetPss":J
    .restart local v14    # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    :goto_1
    const/4 v2, 0x0

    move-object/from16 v5, p0

    move-object v6, v14

    goto :goto_0

    .line 966
    .end local v14    # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    .restart local v6    # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    :cond_3
    move-object v14, v6

    .end local v6    # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    .restart local v14    # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    sget-boolean v2, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z

    if-eqz v2, :cond_4

    .line 967
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Total pss of "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " is "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-wide/16 v5, 0x400

    div-long v5, v10, v5

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "MB, total consumed "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 969
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v3

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 967
    invoke-static {v9, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 971
    :cond_4
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    return-object v2

    .line 946
    .end local v3    # "startUpTime":J
    .end local v10    # "totalPss":J
    .end local v14    # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    :cond_5
    :goto_2
    const/4 v2, 0x0

    return-object v2
.end method

.method private getUploadData()Lcom/android/internal/app/ProcessProphetInfo;
    .locals 49

    .line 1158
    move-object/from16 v1, p0

    invoke-virtual/range {p0 .. p0}, Lcom/android/server/am/ProcessProphetImpl;->isEnable()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 1159
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 1160
    .local v2, "curTime":J
    const/4 v4, 0x0

    .line 1162
    .local v4, "procProphetInfo":Lcom/android/internal/app/ProcessProphetInfo;
    iget-wide v5, v1, Lcom/android/server/am/ProcessProphetImpl;->mLastReportTime:J

    sub-long v5, v2, v5

    sget v0, Lcom/android/server/am/ProcessProphetImpl;->TRACK_INTERVAL_TIME:I

    int-to-long v7, v0

    const-wide/32 v9, 0x5265c00

    mul-long/2addr v7, v9

    cmp-long v0, v5, v7

    if-ltz v0, :cond_1

    .line 1163
    iget-object v5, v1, Lcom/android/server/am/ProcessProphetImpl;->mModelTrackLock:Ljava/lang/Object;

    monitor-enter v5

    .line 1164
    :try_start_0
    new-instance v0, Lcom/android/internal/app/ProcessProphetInfo;

    iget-wide v7, v1, Lcom/android/server/am/ProcessProphetImpl;->mStartedProcs:J

    iget-wide v9, v1, Lcom/android/server/am/ProcessProphetImpl;->mHits:J

    iget-wide v11, v1, Lcom/android/server/am/ProcessProphetImpl;->mKilledProcs:J

    iget-wide v13, v1, Lcom/android/server/am/ProcessProphetImpl;->mAMKilledProcs:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-wide/from16 v41, v2

    .end local v2    # "curTime":J
    .local v41, "curTime":J
    :try_start_1
    iget-wide v2, v1, Lcom/android/server/am/ProcessProphetImpl;->mNativeKilledProcs:J

    iget-object v6, v1, Lcom/android/server/am/ProcessProphetImpl;->mModelTrackList:[J

    const/4 v15, 0x0

    aget-wide v17, v6, v15

    const/4 v15, 0x1

    aget-wide v19, v6, v15

    const/4 v15, 0x2

    aget-wide v22, v6, v15

    const/4 v15, 0x3

    aget-wide v25, v6, v15

    const/4 v15, 0x4

    aget-wide v28, v6, v15

    const/4 v15, 0x5

    aget-wide v31, v6, v15

    iget-object v6, v1, Lcom/android/server/am/ProcessProphetImpl;->mModelPredList:Ljava/util/ArrayList;

    .line 1168
    const/4 v15, 0x0

    invoke-virtual {v6, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Double;

    invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v34

    iget-object v6, v1, Lcom/android/server/am/ProcessProphetImpl;->mModelPredList:Ljava/util/ArrayList;

    const/4 v15, 0x1

    invoke-virtual {v6, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Double;

    invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v36

    iget-object v6, v1, Lcom/android/server/am/ProcessProphetImpl;->mModelPredList:Ljava/util/ArrayList;

    const/4 v15, 0x2

    invoke-virtual {v6, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Double;

    invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v38

    iget-object v6, v1, Lcom/android/server/am/ProcessProphetImpl;->mModelPredList:Ljava/util/ArrayList;

    .line 1169
    const/4 v15, 0x3

    invoke-virtual {v6, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Double;

    invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v43

    iget-object v6, v1, Lcom/android/server/am/ProcessProphetImpl;->mModelPredList:Ljava/util/ArrayList;

    const/4 v15, 0x4

    invoke-virtual {v6, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Double;

    invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v45

    iget-object v6, v1, Lcom/android/server/am/ProcessProphetImpl;->mModelPredList:Ljava/util/ArrayList;

    const/4 v15, 0x5

    invoke-virtual {v6, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Double;

    invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v47

    move-object v6, v0

    move-wide v15, v2

    move-wide/from16 v21, v22

    move-wide/from16 v23, v25

    move-wide/from16 v25, v28

    move-wide/from16 v27, v31

    move-wide/from16 v29, v34

    move-wide/from16 v31, v36

    move-wide/from16 v33, v38

    move-wide/from16 v35, v43

    move-wide/from16 v37, v45

    move-wide/from16 v39, v47

    invoke-direct/range {v6 .. v40}, Lcom/android/internal/app/ProcessProphetInfo;-><init>(JJJJJJJJJJJDDDDDD)V

    move-object v4, v0

    .line 1170
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1171
    move-wide/from16 v2, v41

    .end local v41    # "curTime":J
    .restart local v2    # "curTime":J
    iput-wide v2, v1, Lcom/android/server/am/ProcessProphetImpl;->mLastReportTime:J

    .line 1172
    sget-boolean v0, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 1173
    const-string v0, "ProcessProphet"

    const-string v5, "get onetrack records by getuploadData"

    invoke-static {v0, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1170
    .end local v2    # "curTime":J
    .restart local v41    # "curTime":J
    :catchall_0
    move-exception v0

    move-wide/from16 v2, v41

    .end local v41    # "curTime":J
    .restart local v2    # "curTime":J
    goto :goto_0

    :catchall_1
    move-exception v0

    :goto_0
    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 1176
    :cond_1
    :goto_1
    return-object v4
.end method

.method private handleAudioFocusChanged(Ljava/lang/String;)V
    .locals 2
    .param p1, "callingPackageName"    # Ljava/lang/String;

    .line 427
    iget-boolean v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mBTConnected:Z

    if-nez v0, :cond_0

    .line 428
    return-void

    .line 430
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mLastAudioFocusPkg:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/android/server/am/ProcessProphetImpl;->sBlackListBTAudio:Ljava/util/HashSet;

    .line 431
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 435
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "update audio focus changed to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ProcessProphet"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mProcessProphetModel:Lcom/android/server/am/ProcessProphetModel;

    invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessProphetModel;->updateBTAudioEvent(Ljava/lang/String;)V

    .line 437
    iput-object p1, p0, Lcom/android/server/am/ProcessProphetImpl;->mLastAudioFocusPkg:Ljava/lang/String;

    .line 438
    return-void

    .line 432
    :cond_2
    :goto_0
    return-void
.end method

.method private handleBluetoothConnected(Ljava/lang/String;)V
    .locals 2
    .param p1, "deviceName"    # Ljava/lang/String;

    .line 499
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mBTConnected:Z

    .line 500
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mProcessProphetModel:Lcom/android/server/am/ProcessProphetModel;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetModel;->notifyBTConnected()V

    .line 502
    invoke-direct {p0}, Lcom/android/server/am/ProcessProphetImpl;->triggerPrediction()V

    .line 503
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Bluetooth Connected, device = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ProcessProphet"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    return-void
.end method

.method private handleBluetoothDisConnected()V
    .locals 2

    .line 510
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mBTConnected:Z

    .line 511
    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mLastAudioFocusPkg:Ljava/lang/String;

    .line 512
    const-string v0, "ProcessProphet"

    const-string v1, "Bluetooth DisConnected."

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 513
    return-void
.end method

.method private handleCheckEmptyProcs(I)V
    .locals 13
    .param p1, "cycleTime"    # I

    .line 747
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 749
    .local v0, "startUpTime":J
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl;->mProcessProphetModel:Lcom/android/server/am/ProcessProphetModel;

    iget-object v3, p0, Lcom/android/server/am/ProcessProphetImpl;->mAliveEmptyProcs:Ljava/util/HashMap;

    .line 750
    invoke-virtual {v2, v3}, Lcom/android/server/am/ProcessProphetModel;->sortEmptyProcs(Ljava/util/HashMap;)Ljava/util/ArrayList;

    move-result-object v2

    .line 753
    .local v2, "sortedList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessProphetModel$PkgValuePair;>;"
    const-wide/16 v3, 0x0

    .line 754
    .local v3, "totalPss":J
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;

    .line 755
    .local v6, "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
    if-eqz v6, :cond_0

    iget-object v7, v6, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;->pkgName:Ljava/lang/String;

    if-nez v7, :cond_1

    .line 756
    goto :goto_0

    .line 758
    :cond_1
    const/4 v7, 0x0

    .line 759
    .local v7, "app":Lcom/android/server/am/ProcessRecord;
    iget-object v8, p0, Lcom/android/server/am/ProcessProphetImpl;->mAliveEmptyProcs:Ljava/util/HashMap;

    monitor-enter v8

    .line 760
    :try_start_0
    iget-object v9, p0, Lcom/android/server/am/ProcessProphetImpl;->mAliveEmptyProcs:Ljava/util/HashMap;

    iget-object v10, v6, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;->pkgName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/server/am/ProcessRecord;

    move-object v7, v9

    .line 761
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 762
    if-nez v7, :cond_2

    .line 763
    goto :goto_0

    .line 765
    :cond_2
    iget-object v8, v7, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    iget-object v9, v7, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget v9, v9, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-direct {p0, v8, v9}, Lcom/android/server/am/ProcessProphetImpl;->getPssByUid(Ljava/lang/String;I)Ljava/lang/Long;

    move-result-object v8

    .line 766
    .local v8, "pss":Ljava/lang/Long;
    if-eqz v8, :cond_4

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    const-wide/16 v11, 0x0

    cmp-long v9, v9, v11

    if-lez v9, :cond_4

    .line 767
    iget-object v9, p0, Lcom/android/server/am/ProcessProphetImpl;->mProcessProphetModel:Lcom/android/server/am/ProcessProphetModel;

    iget-object v10, v6, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;->pkgName:Ljava/lang/String;

    invoke-virtual {v9, v10, v8}, Lcom/android/server/am/ProcessProphetModel;->updatePssInRecord(Ljava/lang/String;Ljava/lang/Long;)V

    .line 768
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    add-long/2addr v9, v3

    sget-wide v11, Lcom/android/server/am/ProcessProphetImpl;->EMPTY_PROCS_PSS_THRESHOLD:J

    cmp-long v9, v9, v11

    if-lez v9, :cond_3

    .line 769
    invoke-direct {p0, v7}, Lcom/android/server/am/ProcessProphetImpl;->tryToKillProc(Lcom/android/server/am/ProcessRecord;)V

    goto :goto_1

    .line 771
    :cond_3
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    add-long/2addr v3, v9

    .line 774
    .end local v6    # "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
    .end local v7    # "app":Lcom/android/server/am/ProcessRecord;
    .end local v8    # "pss":Ljava/lang/Long;
    :cond_4
    :goto_1
    goto :goto_0

    .line 761
    .restart local v6    # "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
    .restart local v7    # "app":Lcom/android/server/am/ProcessRecord;
    :catchall_0
    move-exception v5

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5

    .line 776
    .end local v6    # "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
    .end local v7    # "app":Lcom/android/server/am/ProcessRecord;
    :cond_5
    add-int/lit8 v5, p1, -0x1

    if-lez v5, :cond_6

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_6

    .line 777
    iget-object v5, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    add-int/lit8 v6, p1, -0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x9

    invoke-virtual {v5, v9, v6, v7, v8}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    .line 778
    .local v5, "msg":Landroid/os/Message;
    iget-object v6, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    const-wide/16 v7, 0x2710

    invoke-virtual {v6, v5, v7, v8}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 781
    .end local v5    # "msg":Landroid/os/Message;
    :cond_6
    sget-boolean v5, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z

    if-eqz v5, :cond_7

    .line 782
    const-string v5, "ProcessProphet"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "handleCheckEmptyProcs consumed "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 783
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v7

    sub-long/2addr v7, v0

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "ms"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 782
    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 785
    :cond_7
    return-void
.end method

.method private handleCopy(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/CharSequence;

    .line 519
    sget-boolean v0, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z

    const-string v1, "ProcessProphet"

    if-eqz v0, :cond_0

    .line 520
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "copied: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 522
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    const/4 v2, 0x6

    invoke-virtual {v0, v2}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 523
    const-string v0, "copy skip..."

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 524
    return-void

    .line 528
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mATMS:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->getFocusedRootTaskInfo()Landroid/app/ActivityTaskManager$RootTaskInfo;

    move-result-object v0

    .line 529
    .local v0, "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    if-eqz v0, :cond_4

    iget-object v2, v0, Landroid/app/ActivityTaskManager$RootTaskInfo;->topActivity:Landroid/content/ComponentName;

    if-nez v2, :cond_2

    goto :goto_0

    .line 534
    :cond_2
    iget-object v1, v0, Landroid/app/ActivityTaskManager$RootTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 535
    .local v1, "curTopProcName":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl;->mProcessProphetModel:Lcom/android/server/am/ProcessProphetModel;

    invoke-virtual {v2, p1, v1}, Lcom/android/server/am/ProcessProphetModel;->updateCopyEvent(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 536
    .local v2, "matchedPkgName":Ljava/lang/String;
    if-eqz v2, :cond_3

    .line 537
    invoke-direct {p0}, Lcom/android/server/am/ProcessProphetImpl;->triggerPrediction()V

    .line 541
    .end local v0    # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    .end local v1    # "curTopProcName":Ljava/lang/String;
    .end local v2    # "matchedPkgName":Ljava/lang/String;
    :cond_3
    goto :goto_1

    .line 530
    .restart local v0    # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    :cond_4
    :goto_0
    const-string v2, "getFocusedRootTaskInfo error."

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 531
    return-void

    .line 539
    .end local v0    # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    :catch_0
    move-exception v0

    .line 542
    :goto_1
    return-void
.end method

.method private handleDump()V
    .locals 9

    .line 975
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mProcessProphetModel:Lcom/android/server/am/ProcessProphetModel;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetModel;->dump()V

    .line 976
    const-string v0, "ProcessProphet"

    const-string v1, "Dumping AliveEmptyProcs:"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 977
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mAliveEmptyProcs:Ljava/util/HashMap;

    monitor-enter v0

    .line 978
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mAliveEmptyProcs:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/ProcessRecord;

    .line 979
    .local v2, "app":Lcom/android/server/am/ProcessRecord;
    iget v3, v2, Lcom/android/server/am/ProcessRecord;->mPid:I

    if-gtz v3, :cond_0

    goto :goto_0

    .line 980
    :cond_0
    iget v3, v2, Lcom/android/server/am/ProcessRecord;->mPid:I

    const/4 v4, 0x0

    invoke-static {v3, v4, v4}, Landroid/os/Debug;->getPss(I[J[J)J

    move-result-wide v3

    .line 981
    .local v3, "dPss":J
    const-string v5, "ProcessProphet"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "\tPss="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-wide/16 v7, 0x400

    div-long v7, v3, v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "MB "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 982
    nop

    .end local v2    # "app":Lcom/android/server/am/ProcessRecord;
    .end local v3    # "dPss":J
    goto :goto_0

    .line 983
    :cond_1
    monitor-exit v0

    .line 984
    return-void

    .line 983
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private handleIdleUpdate()V
    .locals 8

    .line 874
    const-string v0, "ProcessProphet"

    const-string v1, "IUpdate."

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 875
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mProcessProphetModel:Lcom/android/server/am/ProcessProphetModel;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetModel;->conclude()V

    .line 878
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mModelTrackLock:Ljava/lang/Object;

    monitor-enter v0

    .line 879
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mProcessProphetModel:Lcom/android/server/am/ProcessProphetModel;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessProphetModel;->updateModelSizeTrack()[J

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mModelTrackList:[J

    .line 881
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mProcessProphetModel:Lcom/android/server/am/ProcessProphetModel;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessProphetModel;->uploadModelPredProb()Ljava/util/ArrayList;

    move-result-object v1

    .line 882
    .local v1, "tmp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Double;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/android/server/am/ProcessProphetImpl;->mModelPredList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 883
    iget-object v3, p0, Lcom/android/server/am/ProcessProphetImpl;->mModelPredList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Double;

    invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 882
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 885
    .end local v1    # "tmp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Double;>;"
    .end local v2    # "i":I
    :cond_0
    monitor-exit v0

    .line 886
    return-void

    .line 885
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private handleKillProc(Lcom/android/server/am/ProcessRecord;)V
    .locals 8
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 691
    if-nez p1, :cond_0

    .line 692
    return-void

    .line 694
    :cond_0
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    .line 695
    .local v0, "state":Lcom/android/server/am/ProcessStateRecord;
    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I

    move-result v1

    .line 696
    .local v1, "curAdj":I
    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getCurProcState()I

    move-result v2

    .line 698
    .local v2, "curProcState":I
    sget-boolean v3, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z

    const-string v4, "ProcessProphet"

    if-eqz v3, :cond_3

    .line 699
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "trying to kill: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "("

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v5, p1, Lcom/android/server/am/ProcessRecord;->mPid:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ") curAdj="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " curProcState="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " adjType="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 703
    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getAdjType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " isKilled="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 704
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isKilled()Z

    move-result v5

    const-string/jumbo v6, "true"

    const-string v7, "false"

    if-eqz v5, :cond_1

    move-object v5, v6

    goto :goto_0

    :cond_1
    move-object v5, v7

    :goto_0
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " isKilledByAm="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 705
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isKilledByAm()Z

    move-result v5

    if-eqz v5, :cond_2

    goto :goto_1

    :cond_2
    move-object v6, v7

    :goto_1
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 699
    invoke-static {v4, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 708
    :cond_3
    iget v3, p1, Lcom/android/server/am/ProcessRecord;->mPid:I

    if-lez v3, :cond_4

    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isKilledByAm()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isKilled()Z

    move-result v3

    if-nez v3, :cond_4

    const/16 v3, 0x13

    if-ne v2, v3, :cond_4

    const/16 v3, 0x320

    if-ne v1, v3, :cond_4

    .line 711
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "killing proc: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 712
    iget-object v3, p0, Lcom/android/server/am/ProcessProphetImpl;->mPMS:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v3}, Lcom/android/server/am/ProcessManagerService;->getProcessKiller()Lcom/android/server/am/ProcessKiller;

    move-result-object v3

    const-string v4, "pp-oom"

    const/4 v5, 0x0

    invoke-virtual {v3, p1, v4, v5}, Lcom/android/server/am/ProcessKiller;->killApplication(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Z)Z

    .line 714
    :cond_4
    return-void
.end method

.method private handleLaunchEvent(Ljava/lang/String;II)V
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "launchType"    # I
    .param p3, "launchDurationMs"    # I

    .line 455
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mLastLaunchedPkg:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 456
    return-void

    .line 458
    :cond_0
    iput-object p1, p0, Lcom/android/server/am/ProcessProphetImpl;->mLastLaunchedPkg:Ljava/lang/String;

    .line 460
    sget-object v0, Lcom/android/server/am/ProcessProphetImpl;->sBlackListLaunch:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 461
    return-void

    .line 465
    :cond_1
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mProcessProphetModel:Lcom/android/server/am/ProcessProphetModel;

    invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessProphetModel;->updateLaunchEvent(Ljava/lang/String;)V

    .line 466
    sget-boolean v0, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z

    if-eqz v0, :cond_2

    .line 467
    const-string v0, "ProcessProphet"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Launched, type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", duration="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    :cond_2
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mAliveEmptyProcs:Ljava/util/HashMap;

    monitor-enter v0

    .line 472
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mAliveEmptyProcs:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 473
    iget-wide v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mHits:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mHits:J

    .line 474
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mAliveEmptyProcs:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 475
    const-string v1, "ProcessProphet"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AliveEmptyProcs remove: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " as launched."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    :cond_3
    monitor-exit v0

    .line 478
    return-void

    .line 477
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private handleMemPressure(I)V
    .locals 3
    .param p1, "newPressureState"    # I

    .line 905
    invoke-direct {p0}, Lcom/android/server/am/ProcessProphetImpl;->estimateMemPressure()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 906
    const-string v0, "ProcessProphet"

    const-string/jumbo v1, "trying to kill all empty procs."

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 907
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mAliveEmptyProcs:Ljava/util/HashMap;

    monitor-enter v0

    .line 908
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mAliveEmptyProcs:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/ProcessRecord;

    .line 909
    .local v2, "app":Lcom/android/server/am/ProcessRecord;
    if-nez v2, :cond_0

    .line 910
    goto :goto_0

    .line 912
    :cond_0
    invoke-direct {p0, v2}, Lcom/android/server/am/ProcessProphetImpl;->tryToKillProc(Lcom/android/server/am/ProcessRecord;)V

    .line 913
    .end local v2    # "app":Lcom/android/server/am/ProcessRecord;
    goto :goto_0

    .line 914
    :cond_1
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 916
    :cond_2
    :goto_1
    return-void
.end method

.method private handlePostInit()V
    .locals 8

    .line 186
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 188
    .local v0, "startUpTime":J
    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.USER_PRESENT"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 189
    .local v2, "filter":Landroid/content/IntentFilter;
    const-string v3, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 190
    iget-object v3, p0, Lcom/android/server/am/ProcessProphetImpl;->mContext:Landroid/content/Context;

    new-instance v4, Lcom/android/server/am/ProcessProphetImpl$MyReceiver;

    invoke-direct {v4, p0}, Lcom/android/server/am/ProcessProphetImpl$MyReceiver;-><init>(Lcom/android/server/am/ProcessProphetImpl;)V

    invoke-virtual {v3, v4, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 192
    new-instance v3, Lcom/android/server/am/ProcessProphetModel;

    invoke-direct {v3}, Lcom/android/server/am/ProcessProphetModel;-><init>()V

    iput-object v3, p0, Lcom/android/server/am/ProcessProphetImpl;->mProcessProphetModel:Lcom/android/server/am/ProcessProphetModel;

    .line 194
    iget-object v3, p0, Lcom/android/server/am/ProcessProphetImpl;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/server/am/ProcessProphetJobService;->schedule(Landroid/content/Context;)V

    .line 197
    invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl;->getWhitePackages()V

    .line 200
    iget-object v3, p0, Lcom/android/server/am/ProcessProphetImpl;->mContext:Landroid/content/Context;

    .line 201
    const-string v4, "clipboard"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/ClipboardManager;

    .line 202
    .local v3, "clipboardManager":Landroid/content/ClipboardManager;
    new-instance v4, Lcom/android/server/am/ProcessProphetImpl$2;

    invoke-direct {v4, p0, v3}, Lcom/android/server/am/ProcessProphetImpl$2;-><init>(Lcom/android/server/am/ProcessProphetImpl;Landroid/content/ClipboardManager;)V

    invoke-virtual {v3, v4}, Landroid/content/ClipboardManager;->addPrimaryClipChangedListener(Landroid/content/ClipboardManager$OnPrimaryClipChangedListener;)V

    .line 222
    new-instance v4, Lcom/android/server/am/ProcessProphetCloud;

    invoke-direct {v4}, Lcom/android/server/am/ProcessProphetCloud;-><init>()V

    iput-object v4, p0, Lcom/android/server/am/ProcessProphetImpl;->mProcessProphetCloud:Lcom/android/server/am/ProcessProphetCloud;

    .line 223
    iget-object v5, p0, Lcom/android/server/am/ProcessProphetImpl;->mProcessProphetModel:Lcom/android/server/am/ProcessProphetModel;

    iget-object v6, p0, Lcom/android/server/am/ProcessProphetImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v4, p0, v5, v6}, Lcom/android/server/am/ProcessProphetCloud;->initCloud(Lcom/android/server/am/ProcessProphetImpl;Lcom/android/server/am/ProcessProphetModel;Landroid/content/Context;)V

    .line 224
    iget-object v4, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    const/16 v5, 0xf

    invoke-virtual {v4, v5}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    .line 225
    .local v4, "msgRegisterCloud":Landroid/os/Message;
    iget-object v5, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    invoke-virtual {v5, v4}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z

    .line 227
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/android/server/am/ProcessProphetImpl;->mInitialized:Z

    .line 229
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "post init consumed "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v0

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "ms"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "ProcessProphet"

    invoke-static {v6, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    return-void
.end method

.method private handleProcStarted(Lcom/android/server/am/ProcessRecord;I)V
    .locals 5
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "pid"    # I

    .line 730
    const-string v0, "ProcessProphet"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "preload "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " success! pid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " uid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 732
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mAliveEmptyProcs:Ljava/util/HashMap;

    monitor-enter v0

    .line 733
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mAliveEmptyProcs:Ljava/util/HashMap;

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v1, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 734
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 735
    iget-wide v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mStartedProcs:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mStartedProcs:J

    .line 736
    sget-boolean v0, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "ProcessProphet"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "add: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 738
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->removeMessages(I)V

    .line 739
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-virtual {v0, v1, v4, v2, v3}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 740
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    const-wide/16 v2, 0x2710

    invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 741
    return-void

    .line 734
    .end local v0    # "msg":Landroid/os/Message;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private handleUnlock()V
    .locals 2

    .line 484
    iget-boolean v0, p0, Lcom/android/server/am/ProcessProphetImpl;->afterFirstUnlock:Z

    if-nez v0, :cond_1

    .line 485
    sget-boolean v0, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 486
    const-string v0, "ProcessProphet"

    const-string/jumbo v1, "skip first unlock."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 488
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/am/ProcessProphetImpl;->afterFirstUnlock:Z

    .line 489
    return-void

    .line 492
    :cond_1
    invoke-direct {p0}, Lcom/android/server/am/ProcessProphetImpl;->triggerPrediction()V

    .line 493
    return-void
.end method

.method private initThreshold()V
    .locals 6

    .line 306
    const-string v0, "ProcessProphet"

    :try_start_0
    const-string v1, "persist.sys.procprophet.mem_threshold"

    const/16 v2, 0x258

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    int-to-long v1, v1

    const-wide/16 v3, 0x400

    mul-long/2addr v1, v3

    sput-wide v1, Lcom/android/server/am/ProcessProphetImpl;->MEM_THRESHOLD:J

    .line 308
    const-string v1, "persist.sys.procprophet.max_pss"

    sget-object v2, Lcom/android/server/am/ProcessProphetImpl;->mEmptyThreslist:[I

    .line 309
    invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl;->getRamLayerIndex()I

    move-result v5

    aget v2, v2, v5

    .line 308
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    int-to-long v1, v1

    mul-long/2addr v1, v3

    sput-wide v1, Lcom/android/server/am/ProcessProphetImpl;->EMPTY_PROCS_PSS_THRESHOLD:J

    .line 310
    sget-boolean v1, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Init threshold: mem threshold is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-wide v2, Lcom/android/server/am/ProcessProphetImpl;->MEM_THRESHOLD:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Empty Process PSS Threshold is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-wide v2, Lcom/android/server/am/ProcessProphetImpl;->EMPTY_PROCS_PSS_THRESHOLD:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 314
    :cond_0
    goto :goto_0

    .line 312
    :catch_0
    move-exception v1

    .line 313
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "init mem threshold failure!"

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private startEmptyProc(Ljava/lang/String;)Z
    .locals 14
    .param p1, "processName"    # Ljava/lang/String;

    .line 652
    const/4 v0, 0x0

    if-eqz p1, :cond_4

    const-string v1, ""

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    .line 655
    :cond_0
    const/4 v1, 0x0

    .line 657
    .local v1, "info":Landroid/content/pm/ApplicationInfo;
    :try_start_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v2

    const-wide/16 v3, 0x400

    invoke-interface {v2, p1, v3, v4, v0}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;JI)Landroid/content/pm/ApplicationInfo;

    move-result-object v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 662
    .end local v1    # "info":Landroid/content/pm/ApplicationInfo;
    .local v2, "info":Landroid/content/pm/ApplicationInfo;
    nop

    .line 663
    if-nez v2, :cond_1

    .line 664
    return v0

    .line 667
    :cond_1
    const/4 v1, 0x0

    .line 668
    .local v1, "newApp":Lcom/android/server/am/ProcessRecord;
    iget-object v13, p0, Lcom/android/server/am/ProcessProphetImpl;->mAMS:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v13

    .line 669
    :try_start_1
    iget-object v3, p0, Lcom/android/server/am/ProcessProphetImpl;->mAMS:Lcom/android/server/am/ActivityManagerService;

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v8, Lcom/android/server/am/HostingRecord;

    const-string v4, "ProcessProphet"

    invoke-direct {v8, v4, p1}, Lcom/android/server/am/HostingRecord;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const-string v12, "android"

    move-object v4, p1

    move-object v5, v2

    invoke-virtual/range {v3 .. v12}, Lcom/android/server/am/ActivityManagerService;->startProcessLocked(Ljava/lang/String;Landroid/content/pm/ApplicationInfo;ZILcom/android/server/am/HostingRecord;IZZLjava/lang/String;)Lcom/android/server/am/ProcessRecord;

    move-result-object v3

    move-object v1, v3

    .line 673
    if-nez v1, :cond_2

    .line 674
    const-string v3, "ProcessProphet"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "preload "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " failed!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 675
    monitor-exit v13

    return v0

    .line 676
    :cond_2
    invoke-virtual {v1}, Lcom/android/server/am/ProcessRecord;->getPid()I

    move-result v3

    if-lez v3, :cond_3

    .line 677
    const-string v3, "ProcessProphet"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "preload "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " existed!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 678
    monitor-exit v13

    return v0

    .line 680
    :cond_3
    monitor-exit v13
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 681
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/android/server/am/ProcessProphetImpl;->mLastEmptyProcStartUpTime:J

    .line 682
    const/4 v0, 0x1

    return v0

    .line 680
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v13
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 659
    .end local v2    # "info":Landroid/content/pm/ApplicationInfo;
    .local v1, "info":Landroid/content/pm/ApplicationInfo;
    :catch_0
    move-exception v2

    .line 660
    .local v2, "e":Landroid/os/RemoteException;
    const-string v3, "ProcessProphet"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "error in getApplicationInfo!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 661
    return v0

    .line 653
    .end local v1    # "info":Landroid/content/pm/ApplicationInfo;
    .end local v2    # "e":Landroid/os/RemoteException;
    :cond_4
    :goto_0
    return v0
.end method

.method private triggerPrediction()V
    .locals 18

    .line 549
    move-object/from16 v1, p0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 550
    .local v2, "startUpTime":J
    invoke-direct/range {p0 .. p0}, Lcom/android/server/am/ProcessProphetImpl;->allowToStartNewProc()Z

    move-result v0

    if-nez v0, :cond_0

    .line 551
    return-void

    .line 554
    :cond_0
    const-string v0, "ProcessProphet"

    const-string/jumbo v4, "trying to predict."

    invoke-static {v0, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 556
    iget-object v0, v1, Lcom/android/server/am/ProcessProphetImpl;->mProcessProphetModel:Lcom/android/server/am/ProcessProphetModel;

    iget-object v4, v1, Lcom/android/server/am/ProcessProphetImpl;->mAliveEmptyProcs:Ljava/util/HashMap;

    .line 557
    invoke-virtual {v0, v4}, Lcom/android/server/am/ProcessProphetModel;->getWeightedTab(Ljava/util/HashMap;)Ljava/util/ArrayList;

    move-result-object v4

    .line 559
    .local v4, "sortedList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessProphetModel$PkgValuePair;>;"
    const-wide/16 v5, 0x0

    .line 560
    .local v5, "totalPss":J
    const/4 v0, 0x1

    .line 562
    .local v0, "allowToStartNextProc":Z
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-wide v8, v5

    move v5, v0

    .end local v0    # "allowToStartNextProc":Z
    .local v5, "allowToStartNextProc":Z
    .local v8, "totalPss":J
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;

    .line 563
    .local v6, "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
    iget-object v10, v6, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;->pkgName:Ljava/lang/String;

    .line 564
    .local v10, "procName":Ljava/lang/String;
    const/4 v11, 0x0

    .line 565
    .local v11, "app":Lcom/android/server/am/ProcessRecord;
    iget-object v12, v1, Lcom/android/server/am/ProcessProphetImpl;->mAliveEmptyProcs:Ljava/util/HashMap;

    monitor-enter v12

    .line 566
    :try_start_0
    iget-object v0, v1, Lcom/android/server/am/ProcessProphetImpl;->mAliveEmptyProcs:Ljava/util/HashMap;

    invoke-virtual {v0, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/ProcessRecord;

    move-object v11, v0

    .line 567
    monitor-exit v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 568
    if-eqz v11, :cond_3

    .line 570
    iget-object v0, v11, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-direct {v1, v10, v0}, Lcom/android/server/am/ProcessProphetImpl;->getPssByUid(Ljava/lang/String;I)Ljava/lang/Long;

    move-result-object v0

    .line 571
    .local v0, "pss":Ljava/lang/Long;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    const-wide/16 v14, 0x0

    cmp-long v12, v12, v14

    if-lez v12, :cond_2

    .line 572
    iget-object v12, v1, Lcom/android/server/am/ProcessProphetImpl;->mProcessProphetModel:Lcom/android/server/am/ProcessProphetModel;

    invoke-virtual {v12, v10, v0}, Lcom/android/server/am/ProcessProphetModel;->updatePssInRecord(Ljava/lang/String;Ljava/lang/Long;)V

    .line 573
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    add-long/2addr v12, v8

    sget-wide v14, Lcom/android/server/am/ProcessProphetImpl;->EMPTY_PROCS_PSS_THRESHOLD:J

    cmp-long v12, v12, v14

    if-lez v12, :cond_1

    .line 574
    invoke-direct {v1, v11}, Lcom/android/server/am/ProcessProphetImpl;->tryToKillProc(Lcom/android/server/am/ProcessRecord;)V

    goto :goto_1

    .line 576
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    add-long/2addr v8, v12

    .line 579
    .end local v0    # "pss":Ljava/lang/Long;
    :cond_2
    :goto_1
    goto/16 :goto_2

    :cond_3
    if-eqz v5, :cond_2

    sget-object v0, Lcom/android/server/am/ProcessProphetImpl;->sWhiteListEmptyProc:Ljava/util/HashSet;

    invoke-virtual {v0, v10}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 581
    iget-object v0, v1, Lcom/android/server/am/ProcessProphetImpl;->mProcessProphetModel:Lcom/android/server/am/ProcessProphetModel;

    invoke-virtual {v0, v10}, Lcom/android/server/am/ProcessProphetModel;->getPssInRecord(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    .line 582
    .local v0, "referencePss":Ljava/lang/Long;
    if-eqz v0, :cond_5

    .line 583
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    add-long/2addr v12, v8

    sget-wide v14, Lcom/android/server/am/ProcessProphetImpl;->EMPTY_PROCS_PSS_THRESHOLD:J

    cmp-long v12, v12, v14

    if-gtz v12, :cond_7

    .line 584
    sget-boolean v12, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z

    if-eqz v12, :cond_4

    .line 585
    const-string v12, "ProcessProphet"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " has reference pss:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    const-wide/16 v16, 0x400

    div-long v14, v14, v16

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "MB, and is not expected to exceed the threshold."

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 588
    :cond_4
    invoke-direct {v1, v10}, Lcom/android/server/am/ProcessProphetImpl;->tryToStartEmptyProc(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 589
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    add-long/2addr v8, v12

    goto :goto_2

    .line 593
    :cond_5
    sget-boolean v12, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z

    if-eqz v12, :cond_6

    const-string v12, "ProcessProphet"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " doesn\'t have a reference pss."

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 594
    :cond_6
    invoke-direct {v1, v10}, Lcom/android/server/am/ProcessProphetImpl;->tryToStartEmptyProc(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 595
    const/4 v5, 0x0

    .line 599
    .end local v0    # "referencePss":Ljava/lang/Long;
    .end local v6    # "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
    .end local v10    # "procName":Ljava/lang/String;
    .end local v11    # "app":Lcom/android/server/am/ProcessRecord;
    :cond_7
    :goto_2
    goto/16 :goto_0

    .line 567
    .restart local v6    # "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
    .restart local v10    # "procName":Ljava/lang/String;
    .restart local v11    # "app":Lcom/android/server/am/ProcessRecord;
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 600
    .end local v6    # "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
    .end local v10    # "procName":Ljava/lang/String;
    .end local v11    # "app":Lcom/android/server/am/ProcessRecord;
    :cond_8
    const-string v0, "ProcessProphet"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "prediction consumed "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    sub-long/2addr v10, v2

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "ms"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 601
    return-void
.end method

.method private tryToKillProc(Lcom/android/server/am/ProcessRecord;)V
    .locals 2
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 686
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    const/16 v1, 0xa

    invoke-virtual {v0, v1, p1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 687
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    invoke-virtual {v1, v0}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z

    .line 688
    return-void
.end method

.method private tryToStartEmptyProc(Ljava/lang/String;)Z
    .locals 2
    .param p1, "processName"    # Ljava/lang/String;

    .line 636
    const/4 v0, 0x0

    if-eqz p1, :cond_2

    const-string v1, ""

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 640
    :cond_0
    sget-object v1, Lcom/android/server/am/ProcessProphetImpl;->sWhiteListEmptyProc:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 641
    return v0

    .line 644
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "trying to start proc: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ProcessProphet"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 645
    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessProphetImpl;->startEmptyProc(Ljava/lang/String;)Z

    move-result v0

    return v0

    .line 637
    :cond_2
    :goto_0
    return v0
.end method


# virtual methods
.method public getRamLayerIndex()I
    .locals 4

    .line 286
    invoke-static {}, Landroid/os/Process;->getTotalMemory()J

    move-result-wide v0

    const-wide/32 v2, 0x40000000

    div-long/2addr v0, v2

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    long-to-int v0, v0

    .line 288
    .local v0, "memory_GB":I
    const/4 v1, 0x4

    if-gt v0, v1, :cond_0

    .line 289
    const/4 v1, 0x0

    .local v1, "index":I
    goto :goto_0

    .line 291
    .end local v1    # "index":I
    :cond_0
    div-int/lit8 v1, v0, 0x2

    add-int/lit8 v1, v1, -0x2

    .line 292
    .restart local v1    # "index":I
    const/4 v2, 0x3

    if-lt v1, v2, :cond_1

    .line 293
    const/4 v1, 0x3

    .line 296
    :cond_1
    :goto_0
    sget-boolean v2, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z

    if-eqz v2, :cond_2

    .line 297
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Total mem is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " index is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ProcessProphet"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    :cond_2
    return v1
.end method

.method public getWhitePackages()V
    .locals 16

    .line 325
    const-string v1, "ProcessProphet"

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 326
    .local v2, "startUpTime":J
    const/4 v4, 0x0

    .line 327
    .local v4, "inputStream":Ljava/io/InputStream;
    const/4 v5, 0x0

    .line 330
    .local v5, "xmlParser":Lorg/xmlpull/v1/XmlPullParser;
    const/4 v6, 0x1

    const/4 v7, 0x0

    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;

    const-string v8, "/product/etc/procprophet.xml"

    invoke-direct {v0, v8}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    move-object v4, v0

    .line 331
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    move-object v5, v0

    .line 332
    const-string/jumbo v0, "utf-8"

    invoke-interface {v5, v4, v0}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 333
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 334
    .local v0, "event":I
    :goto_0
    if-eq v0, v6, :cond_3

    .line 335
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_1

    .line 348
    :pswitch_1
    goto :goto_1

    .line 339
    :pswitch_2
    const-string/jumbo v8, "white-emptyproc"

    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 340
    sget-object v8, Lcom/android/server/am/ProcessProphetImpl;->sWhiteListEmptyProc:Ljava/util/HashSet;

    invoke-interface {v5, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 341
    :cond_0
    const-string v8, "black-launch"

    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 342
    sget-object v8, Lcom/android/server/am/ProcessProphetImpl;->sBlackListLaunch:Ljava/util/HashSet;

    invoke-interface {v5, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 343
    :cond_1
    const-string v8, "black-bt"

    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 344
    sget-object v8, Lcom/android/server/am/ProcessProphetImpl;->sBlackListBTAudio:Ljava/util/HashSet;

    invoke-interface {v5, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 337
    :pswitch_3
    nop

    .line 352
    :cond_2
    :goto_1
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v8
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move v0, v8

    goto :goto_0

    .line 357
    .end local v0    # "event":I
    :cond_3
    nop

    .line 359
    :try_start_1
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 363
    goto :goto_3

    :catchall_0
    move-exception v0

    goto :goto_2

    .line 360
    :catch_0
    move-exception v0

    move-object v8, v0

    move-object v0, v8

    .line 361
    .local v0, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v1, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 363
    nop

    .end local v0    # "e":Ljava/io/IOException;
    goto :goto_3

    :goto_2
    const/4 v1, 0x0

    .line 364
    .end local v4    # "inputStream":Ljava/io/InputStream;
    .local v1, "inputStream":Ljava/io/InputStream;
    throw v0

    .line 357
    .end local v1    # "inputStream":Ljava/io/InputStream;
    .restart local v4    # "inputStream":Ljava/io/InputStream;
    :catchall_1
    move-exception v0

    move-object v6, v0

    goto/16 :goto_c

    .line 354
    :catch_1
    move-exception v0

    .line 355
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v1, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 357
    nop

    .end local v0    # "e":Ljava/lang/Exception;
    if-eqz v4, :cond_4

    .line 359
    :try_start_4
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 363
    nop

    :goto_3
    const/4 v0, 0x0

    .line 364
    .end local v4    # "inputStream":Ljava/io/InputStream;
    .local v0, "inputStream":Ljava/io/InputStream;
    goto :goto_5

    .line 363
    .end local v0    # "inputStream":Ljava/io/InputStream;
    .restart local v4    # "inputStream":Ljava/io/InputStream;
    :catchall_2
    move-exception v0

    goto :goto_4

    .line 360
    :catch_2
    move-exception v0

    move-object v8, v0

    move-object v0, v8

    .line 361
    .local v0, "e":Ljava/io/IOException;
    :try_start_5
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v1, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 363
    nop

    .end local v0    # "e":Ljava/io/IOException;
    goto :goto_3

    :goto_4
    const/4 v1, 0x0

    .line 364
    .end local v4    # "inputStream":Ljava/io/InputStream;
    .restart local v1    # "inputStream":Ljava/io/InputStream;
    throw v0

    .line 357
    .end local v1    # "inputStream":Ljava/io/InputStream;
    .restart local v4    # "inputStream":Ljava/io/InputStream;
    :cond_4
    move-object v0, v4

    .line 368
    .end local v4    # "inputStream":Ljava/io/InputStream;
    .local v0, "inputStream":Ljava/io/InputStream;
    :goto_5
    sget-boolean v4, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z

    if-eqz v4, :cond_b

    .line 369
    const-string/jumbo v4, "whiteList: "

    .line 370
    .local v4, "whiteListDump":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/android/server/am/ProcessProphetImpl;->sWhiteListEmptyProc:Ljava/util/HashSet;

    invoke-virtual {v9}, Ljava/util/HashSet;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 371
    sget-object v8, Lcom/android/server/am/ProcessProphetImpl;->sWhiteListEmptyProc:Ljava/util/HashSet;

    invoke-virtual {v8}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_6
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    const-string v10, "...... "

    const/16 v11, 0xc8

    const-string v12, ","

    const-string v13, " "

    if-eqz v9, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 372
    .local v9, "procName":Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 373
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v14

    if-le v14, v11, :cond_5

    .line 374
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v8

    sub-int/2addr v8, v6

    invoke-virtual {v4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 375
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 376
    goto :goto_7

    .line 378
    .end local v9    # "procName":Ljava/lang/String;
    :cond_5
    goto :goto_6

    .line 379
    :cond_6
    :goto_7
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v8

    sub-int/2addr v8, v6

    invoke-virtual {v4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-static {v1, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    const-string v8, "blackListLaunch: "

    .line 382
    .local v8, "blackListLaunch":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v14, Lcom/android/server/am/ProcessProphetImpl;->sBlackListLaunch:Ljava/util/HashSet;

    invoke-virtual {v14}, Ljava/util/HashSet;->size()I

    move-result v14

    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 383
    sget-object v9, Lcom/android/server/am/ProcessProphetImpl;->sBlackListLaunch:Ljava/util/HashSet;

    invoke-virtual {v9}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_8
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_8

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 384
    .local v14, "procName":Ljava/lang/String;
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 385
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v15

    if-le v15, v11, :cond_7

    .line 386
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v9

    sub-int/2addr v9, v6

    invoke-virtual {v8, v7, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 387
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 388
    goto :goto_9

    .line 390
    .end local v14    # "procName":Ljava/lang/String;
    :cond_7
    goto :goto_8

    .line 391
    :cond_8
    :goto_9
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v9

    sub-int/2addr v9, v6

    invoke-virtual {v8, v7, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-static {v1, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    const-string v9, "blackListBT: "

    .line 394
    .local v9, "blackListBT":Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    sget-object v15, Lcom/android/server/am/ProcessProphetImpl;->sBlackListBTAudio:Ljava/util/HashSet;

    invoke-virtual {v15}, Ljava/util/HashSet;->size()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 395
    sget-object v14, Lcom/android/server/am/ProcessProphetImpl;->sBlackListBTAudio:Ljava/util/HashSet;

    invoke-virtual {v14}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_a
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_a

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 396
    .local v15, "procName":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 397
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v7

    if-le v7, v11, :cond_9

    .line 398
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v7

    sub-int/2addr v7, v6

    const/4 v11, 0x0

    invoke-virtual {v9, v11, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 399
    .end local v9    # "blackListBT":Ljava/lang/String;
    .local v7, "blackListBT":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 400
    .end local v7    # "blackListBT":Ljava/lang/String;
    .restart local v9    # "blackListBT":Ljava/lang/String;
    goto :goto_b

    .line 402
    .end local v15    # "procName":Ljava/lang/String;
    :cond_9
    const/4 v7, 0x0

    goto :goto_a

    .line 403
    :cond_a
    :goto_b
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v7

    sub-int/2addr v7, v6

    const/4 v6, 0x0

    invoke-virtual {v9, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    .end local v4    # "whiteListDump":Ljava/lang/String;
    .end local v8    # "blackListLaunch":Ljava/lang/String;
    .end local v9    # "blackListBT":Ljava/lang/String;
    :cond_b
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getWhitePackages consumed "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v2

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "ms"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    return-void

    .line 357
    .end local v0    # "inputStream":Ljava/io/InputStream;
    .local v4, "inputStream":Ljava/io/InputStream;
    :goto_c
    if-eqz v4, :cond_c

    .line 359
    :try_start_6
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 363
    nop

    :goto_d
    const/4 v4, 0x0

    .line 364
    goto :goto_f

    .line 363
    :catchall_3
    move-exception v0

    goto :goto_e

    .line 360
    :catch_3
    move-exception v0

    move-object v7, v0

    move-object v0, v7

    .line 361
    .local v0, "e":Ljava/io/IOException;
    :try_start_7
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 363
    nop

    .end local v0    # "e":Ljava/io/IOException;
    goto :goto_d

    :goto_e
    const/4 v1, 0x0

    .line 364
    .end local v4    # "inputStream":Ljava/io/InputStream;
    .restart local v1    # "inputStream":Ljava/io/InputStream;
    throw v0

    .line 366
    .end local v1    # "inputStream":Ljava/io/InputStream;
    .restart local v4    # "inputStream":Ljava/io/InputStream;
    :cond_c
    :goto_f
    throw v6

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public init(Landroid/content/Context;Lcom/android/server/am/ActivityManagerService;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ams"    # Lcom/android/server/am/ActivityManagerService;

    .line 147
    iget-boolean v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mEnable:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mInitialized:Z

    if-eqz v0, :cond_0

    goto/16 :goto_1

    .line 150
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 152
    .local v0, "startUpTime":J
    new-instance v2, Landroid/os/HandlerThread;

    const-string v3, "ProcessProphet"

    invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandlerThread:Landroid/os/HandlerThread;

    .line 153
    invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V

    .line 154
    new-instance v2, Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    iget-object v4, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v4}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v2, p0, v4}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;-><init>(Lcom/android/server/am/ProcessProphetImpl;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    .line 156
    iput-object p1, p0, Lcom/android/server/am/ProcessProphetImpl;->mContext:Landroid/content/Context;

    .line 157
    iput-object p2, p0, Lcom/android/server/am/ProcessProphetImpl;->mAMS:Lcom/android/server/am/ActivityManagerService;

    .line 158
    invoke-static {}, Landroid/app/ActivityTaskManager;->getService()Landroid/app/IActivityTaskManager;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/ActivityTaskManagerService;

    iput-object v2, p0, Lcom/android/server/am/ProcessProphetImpl;->mATMS:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 159
    const-string v2, "ProcessManager"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/ProcessManagerService;

    iput-object v2, p0, Lcom/android/server/am/ProcessProphetImpl;->mPMS:Lcom/android/server/am/ProcessManagerService;

    .line 160
    new-instance v2, Lcom/android/server/am/ProcessProphetImpl$BinderService;

    const/4 v4, 0x0

    invoke-direct {v2, p0, v4}, Lcom/android/server/am/ProcessProphetImpl$BinderService;-><init>(Lcom/android/server/am/ProcessProphetImpl;Lcom/android/server/am/ProcessProphetImpl$BinderService-IA;)V

    const-string v4, "procprophet"

    invoke-static {v4, v2}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 163
    invoke-direct {p0}, Lcom/android/server/am/ProcessProphetImpl;->initThreshold()V

    .line 165
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl;->mAMS:Lcom/android/server/am/ActivityManagerService;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl;->mATMS:Lcom/android/server/wm/ActivityTaskManagerService;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl;->mPMS:Lcom/android/server/am/ProcessManagerService;

    if-nez v2, :cond_1

    goto :goto_0

    .line 172
    :cond_1
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    new-instance v4, Lcom/android/server/am/ProcessProphetImpl$1;

    invoke-direct {v4, p0}, Lcom/android/server/am/ProcessProphetImpl$1;-><init>(Lcom/android/server/am/ProcessProphetImpl;)V

    invoke-virtual {v2, v4}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->post(Ljava/lang/Runnable;)Z

    .line 179
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pre init consumed "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v0

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "ms"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    return-void

    .line 166
    :cond_2
    :goto_0
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/server/am/ProcessProphetImpl;->mEnable:Z

    .line 167
    iput-boolean v2, p0, Lcom/android/server/am/ProcessProphetImpl;->mInitialized:Z

    .line 168
    const-string v2, "disable ProcessProphet for dependencies service not available"

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    return-void

    .line 148
    .end local v0    # "startUpTime":J
    :cond_3
    :goto_1
    return-void
.end method

.method public isEnable()Z
    .locals 1

    .line 318
    iget-boolean v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mEnable:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mInitialized:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/am/ProcessProphetImpl;->afterFirstUnlock:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isNeedProtect(Lcom/android/server/am/ProcessRecord;)Z
    .locals 9
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 828
    invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessProphetImpl;->isPredictedProc(Lcom/android/server/am/ProcessRecord;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 831
    :cond_0
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 832
    .local v0, "processName":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl;->mAliveEmptyProcs:Ljava/util/HashMap;

    monitor-enter v2

    .line 833
    :try_start_0
    iget-object v3, p0, Lcom/android/server/am/ProcessProphetImpl;->mAliveEmptyProcs:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 834
    monitor-exit v2

    return v1

    .line 836
    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 838
    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    .line 839
    .local v2, "state":Lcom/android/server/am/ProcessStateRecord;
    invoke-virtual {v2}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I

    move-result v3

    .line 840
    .local v3, "curAdj":I
    invoke-virtual {v2}, Lcom/android/server/am/ProcessStateRecord;->getCurProcState()I

    move-result v4

    .line 841
    .local v4, "curProcState":I
    sget-boolean v5, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z

    const/16 v6, 0x13

    if-eqz v5, :cond_4

    if-eq v4, v6, :cond_4

    const/16 v5, 0xa

    if-eq v4, v5, :cond_4

    const/16 v5, 0xb

    if-eq v4, v5, :cond_4

    const/16 v5, 0x14

    if-eq v4, v5, :cond_4

    .line 845
    const-string v5, "ProcessProphet"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "processName = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " curAdj="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " curProcState="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " adjType="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 848
    invoke-virtual {v2}, Lcom/android/server/am/ProcessStateRecord;->getAdjType()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " isKilled="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 849
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isKilled()Z

    move-result v8

    if-eqz v8, :cond_2

    const-string/jumbo v8, "true"

    goto :goto_0

    :cond_2
    const-string v8, "false"

    :goto_0
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " isKilledByAm="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 850
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isKilledByAm()Z

    move-result v8

    if-eqz v8, :cond_3

    const-string/jumbo v8, "true"

    goto :goto_1

    :cond_3
    const-string v8, "false"

    :goto_1
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 845
    invoke-static {v5, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 853
    :cond_4
    iget v5, p1, Lcom/android/server/am/ProcessRecord;->mPid:I

    if-lez v5, :cond_5

    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isKilledByAm()Z

    move-result v5

    if-nez v5, :cond_5

    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isKilled()Z

    move-result v5

    if-nez v5, :cond_5

    if-ne v4, v6, :cond_5

    const/16 v5, 0x320

    if-lt v3, v5, :cond_5

    .line 856
    const/4 v1, 0x1

    return v1

    .line 860
    :cond_5
    return v1

    .line 836
    .end local v2    # "state":Lcom/android/server/am/ProcessStateRecord;
    .end local v3    # "curAdj":I
    .end local v4    # "curProcState":I
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public isPredictedProc(Lcom/android/server/am/ProcessRecord;)Z
    .locals 5
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 810
    const-string v0, "ProcessProphet"

    invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl;->isEnable()Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    return v2

    .line 812
    :cond_0
    if-eqz p1, :cond_2

    :try_start_0
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getHostingRecord()Lcom/android/server/am/HostingRecord;

    move-result-object v1

    if-nez v1, :cond_1

    goto :goto_0

    .line 815
    :cond_1
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getHostingRecord()Lcom/android/server/am/HostingRecord;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/am/HostingRecord;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 816
    :catch_0
    move-exception v1

    .line 817
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Fail to check hostrecord type."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 819
    .end local v1    # "e":Ljava/lang/Exception;
    return v2

    .line 813
    :cond_2
    :goto_0
    return v2
.end method

.method public notifyAudioFocusChanged(Ljava/lang/String;)V
    .locals 2
    .param p1, "callingPackageName"    # Ljava/lang/String;

    .line 413
    invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl;->isEnable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 414
    return-void

    .line 416
    :cond_0
    sget-boolean v0, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 417
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Getting audio focus: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ProcessProphet"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    :cond_1
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, p1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 420
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    invoke-virtual {v1, v0}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z

    .line 421
    return-void
.end method

.method public notifyIdleUpdate()V
    .locals 2

    .line 865
    invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl;->isEnable()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 866
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 867
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    invoke-virtual {v1, v0}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z

    .line 868
    return-void
.end method

.method public reportLaunchEvent(Ljava/lang/String;II)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "launchType"    # I
    .param p3, "launchDurationMs"    # I

    .line 445
    invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl;->isEnable()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 446
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p2, p3, p1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 448
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    invoke-virtual {v1, v0}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z

    .line 449
    return-void
.end method

.method public reportMemPressure(I)V
    .locals 3
    .param p1, "newPressureState"    # I

    .line 893
    invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl;->isEnable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 894
    return-void

    .line 896
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    const/16 v1, 0xc

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 897
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    invoke-virtual {v1, v0}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 898
    return-void
.end method

.method public reportProcDied(Lcom/android/server/am/ProcessRecord;)V
    .locals 6
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 789
    invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl;->isEnable()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 790
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mAliveEmptyProcs:Ljava/util/HashMap;

    monitor-enter v0

    .line 791
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mAliveEmptyProcs:Ljava/util/HashMap;

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 792
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isKilledByAm()Z

    move-result v1

    const-wide/16 v2, 0x1

    if-eqz v1, :cond_1

    .line 793
    iget-wide v4, p0, Lcom/android/server/am/ProcessProphetImpl;->mAMKilledProcs:J

    add-long/2addr v4, v2

    iput-wide v4, p0, Lcom/android/server/am/ProcessProphetImpl;->mAMKilledProcs:J

    .line 794
    const-string v1, "ProcessProphet"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is killed by am."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 796
    :cond_1
    iget-wide v4, p0, Lcom/android/server/am/ProcessProphetImpl;->mNativeKilledProcs:J

    add-long/2addr v4, v2

    iput-wide v4, p0, Lcom/android/server/am/ProcessProphetImpl;->mNativeKilledProcs:J

    .line 797
    const-string v1, "ProcessProphet"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is killed by native."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 799
    :goto_0
    iget-wide v4, p0, Lcom/android/server/am/ProcessProphetImpl;->mKilledProcs:J

    add-long/2addr v4, v2

    iput-wide v4, p0, Lcom/android/server/am/ProcessProphetImpl;->mKilledProcs:J

    .line 800
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mAliveEmptyProcs:Ljava/util/HashMap;

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 801
    sget-boolean v1, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z

    if-eqz v1, :cond_2

    const-string v1, "ProcessProphet"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "remove: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " as died."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 803
    :cond_2
    monitor-exit v0

    .line 804
    return-void

    .line 803
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public reportProcStarted(Lcom/android/server/am/ProcessRecord;I)V
    .locals 3
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "pid"    # I

    .line 721
    invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessProphetImpl;->isPredictedProc(Lcom/android/server/am/ProcessRecord;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 722
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    const/4 v1, 0x7

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2, p1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 723
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    invoke-virtual {v1, v0}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z

    .line 724
    return-void
.end method

.method public testBT(Ljava/lang/String;)V
    .locals 4
    .param p1, "targetProcName"    # Ljava/lang/String;

    .line 1013
    const-string v0, "ProcessProphet"

    if-eqz p1, :cond_1

    const-string v1, ""

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 1016
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "enter BT test mode, target: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1014
    :cond_1
    :goto_0
    const-string/jumbo v1, "target proc name error!"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1019
    :goto_1
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mProcessProphetModel:Lcom/android/server/am/ProcessProphetModel;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetModel;->reset()V

    .line 1022
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    const/4 v1, 0x5

    if-ge v0, v1, :cond_2

    .line 1023
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    const/4 v2, 0x2

    const-string/jumbo v3, "testBT"

    invoke-virtual {v1, v2, v3}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1024
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    const/4 v2, 0x4

    invoke-virtual {v1, v2, p1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1025
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1022
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1028
    .end local v0    # "i":I
    :cond_2
    invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl;->notifyIdleUpdate()V

    .line 1029
    return-void
.end method

.method public testLU(Ljava/lang/String;)V
    .locals 7
    .param p1, "targetProcName"    # Ljava/lang/String;

    .line 990
    const-string v0, "ProcessProphet"

    if-eqz p1, :cond_1

    const-string v1, ""

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 993
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "enter LU test mode, target: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 991
    :cond_1
    :goto_0
    const-string/jumbo v1, "target proc name error!"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 996
    :goto_1
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mProcessProphetModel:Lcom/android/server/am/ProcessProphetModel;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetModel;->reset()V

    .line 999
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    const/4 v1, 0x5

    if-ge v0, v1, :cond_2

    .line 1000
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    mul-int/lit8 v3, v0, 0x14

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v4, v3, p1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 1001
    .local v2, "msg1":Landroid/os/Message;
    iget-object v3, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    invoke-virtual {v3, v2}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1002
    iget-object v3, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    mul-int/lit8 v5, v0, 0x14

    const-string v6, "com.miui.home"

    invoke-virtual {v3, v1, v4, v5, v6}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 1003
    .local v1, "msg2":Landroid/os/Message;
    iget-object v3, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    invoke-virtual {v3, v1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z

    .line 999
    .end local v1    # "msg2":Landroid/os/Message;
    .end local v2    # "msg1":Landroid/os/Message;
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1006
    .end local v0    # "i":I
    :cond_2
    invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl;->notifyIdleUpdate()V

    .line 1007
    return-void
.end method

.method public testMTBF()V
    .locals 8

    .line 1035
    invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl;->isEnable()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1036
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mProcessProphetModel:Lcom/android/server/am/ProcessProphetModel;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetModel;->testMTBF()V

    .line 1037
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    .line 1038
    .local v0, "randomNum":I
    const/4 v1, 0x3

    const-string v2, "MTBF testing:"

    const-string v3, "ProcessProphet"

    if-ge v0, v1, :cond_0

    .line 1039
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Prediction"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1040
    invoke-direct {p0}, Lcom/android/server/am/ProcessProphetImpl;->triggerPrediction()V

    goto/16 :goto_0

    .line 1041
    :cond_0
    const/4 v1, 0x6

    if-ge v0, v1, :cond_1

    .line 1042
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " BT connection"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1043
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    const/4 v2, 0x2

    const-string/jumbo v4, "testMTBF"

    invoke-virtual {v1, v2, v4}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 1044
    .local v1, "msg":Landroid/os/Message;
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    invoke-virtual {v2, v1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1045
    .end local v1    # "msg":Landroid/os/Message;
    goto :goto_0

    :cond_1
    const/16 v4, 0x8

    if-ge v0, v4, :cond_2

    .line 1046
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " copy v.douyin.com"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1047
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    const-string/jumbo v4, "v.douyin.com"

    invoke-virtual {v2, v1, v4}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 1048
    .restart local v1    # "msg":Landroid/os/Message;
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    invoke-virtual {v2, v1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1049
    .end local v1    # "msg":Landroid/os/Message;
    goto :goto_0

    .line 1050
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " copy m.tb.cn"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1051
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    const-string v4, "m.tb.cn"

    invoke-virtual {v2, v1, v4}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 1052
    .restart local v1    # "msg":Landroid/os/Message;
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    invoke-virtual {v2, v1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1055
    .end local v1    # "msg":Landroid/os/Message;
    :goto_0
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 1056
    .restart local v1    # "msg":Landroid/os/Message;
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    int-to-long v4, v0

    const-wide/32 v6, 0xea60

    mul-long/2addr v4, v6

    invoke-virtual {v2, v1, v4, v5}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1057
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MTBF testing: next event after "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " mins"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1059
    .end local v0    # "randomNum":I
    .end local v1    # "msg":Landroid/os/Message;
    :cond_3
    return-void
.end method

.method public updateEnable(Z)V
    .locals 2
    .param p1, "newEnable"    # Z

    .line 233
    iput-boolean p1, p0, Lcom/android/server/am/ProcessProphetImpl;->mEnable:Z

    .line 234
    if-nez p1, :cond_0

    .line 235
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->removeMessages(I)V

    .line 236
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->removeMessages(I)V

    .line 237
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->removeMessages(I)V

    .line 238
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->removeMessages(I)V

    .line 240
    :cond_0
    sget-boolean v0, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 241
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mEnable change to"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "complete."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ProcessProphet"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    :cond_1
    return-void
.end method

.method public updateImplThreshold(JLjava/lang/String;)V
    .locals 3
    .param p1, "newThres"    # J
    .param p3, "updateName"    # Ljava/lang/String;

    .line 254
    const-string v0, "memPress"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-wide/16 v1, 0x400

    if-eqz v0, :cond_0

    .line 255
    mul-long/2addr v1, p1

    sput-wide v1, Lcom/android/server/am/ProcessProphetImpl;->MEM_THRESHOLD:J

    goto :goto_0

    .line 256
    :cond_0
    const-string v0, "emptyMem"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 257
    mul-long/2addr v1, p1

    sput-wide v1, Lcom/android/server/am/ProcessProphetImpl;->EMPTY_PROCS_PSS_THRESHOLD:J

    .line 259
    :cond_1
    :goto_0
    sget-boolean v0, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z

    if-eqz v0, :cond_2

    .line 260
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " change to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " complete."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ProcessProphet"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    :cond_2
    return-void
.end method

.method public updateList([Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "newList"    # [Ljava/lang/String;
    .param p2, "updateName"    # Ljava/lang/String;

    .line 265
    const-string/jumbo v0, "whitelist"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 266
    array-length v0, p1

    :goto_0
    if-ge v1, v0, :cond_3

    aget-object v2, p1, v1

    .line 267
    .local v2, "str":Ljava/lang/String;
    sget-object v3, Lcom/android/server/am/ProcessProphetImpl;->sWhiteListEmptyProc:Ljava/util/HashSet;

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 268
    sget-object v3, Lcom/android/server/am/ProcessProphetImpl;->sWhiteListEmptyProc:Ljava/util/HashSet;

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 266
    .end local v2    # "str":Ljava/lang/String;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 271
    :cond_1
    const-string v0, "blakclist"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 272
    array-length v0, p1

    :goto_1
    if-ge v1, v0, :cond_3

    aget-object v2, p1, v1

    .line 273
    .restart local v2    # "str":Ljava/lang/String;
    sget-object v3, Lcom/android/server/am/ProcessProphetImpl;->sBlackListLaunch:Ljava/util/HashSet;

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 274
    sget-object v3, Lcom/android/server/am/ProcessProphetImpl;->sBlackListLaunch:Ljava/util/HashSet;

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 272
    .end local v2    # "str":Ljava/lang/String;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 278
    :cond_3
    sget-boolean v0, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z

    if-eqz v0, :cond_4

    .line 279
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " change complete."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ProcessProphet"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    :cond_4
    return-void
.end method

.method public updateTrackInterval(I)V
    .locals 2
    .param p1, "intervalT"    # I

    .line 246
    sget-boolean v0, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 247
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TRACK_INTERVAL_TIME"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/android/server/am/ProcessProphetImpl;->TRACK_INTERVAL_TIME:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "change to"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "complete."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ProcessProphet"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    :cond_0
    sput p1, Lcom/android/server/am/ProcessProphetImpl;->TRACK_INTERVAL_TIME:I

    .line 251
    return-void
.end method
