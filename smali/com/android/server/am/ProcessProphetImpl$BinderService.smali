.class final Lcom/android/server/am/ProcessProphetImpl$BinderService;
.super Lcom/android/internal/app/IProcessProphet$Stub;
.source "ProcessProphetImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ProcessProphetImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "BinderService"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ProcessProphetImpl;


# direct methods
.method private constructor <init>(Lcom/android/server/am/ProcessProphetImpl;)V
    .locals 0

    .line 1143
    iput-object p1, p0, Lcom/android/server/am/ProcessProphetImpl$BinderService;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    invoke-direct {p0}, Lcom/android/internal/app/IProcessProphet$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/am/ProcessProphetImpl;Lcom/android/server/am/ProcessProphetImpl$BinderService-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessProphetImpl$BinderService;-><init>(Lcom/android/server/am/ProcessProphetImpl;)V

    return-void
.end method


# virtual methods
.method public getUploadData()Lcom/android/internal/app/ProcessProphetInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 1153
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl$BinderService;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    invoke-static {v0}, Lcom/android/server/am/ProcessProphetImpl;->-$$Nest$mgetUploadData(Lcom/android/server/am/ProcessProphetImpl;)Lcom/android/internal/app/ProcessProphetInfo;

    move-result-object v0

    return-object v0
.end method

.method public onShellCommand(Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)V
    .locals 8
    .param p1, "in"    # Ljava/io/FileDescriptor;
    .param p2, "out"    # Ljava/io/FileDescriptor;
    .param p3, "err"    # Ljava/io/FileDescriptor;
    .param p4, "args"    # [Ljava/lang/String;
    .param p5, "callback"    # Landroid/os/ShellCallback;
    .param p6, "resultReceiver"    # Landroid/os/ResultReceiver;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 1148
    new-instance v0, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;

    iget-object v1, p0, Lcom/android/server/am/ProcessProphetImpl$BinderService;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;-><init>(Lcom/android/server/am/ProcessProphetImpl;Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd-IA;)V

    .line 1149
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->exec(Landroid/os/Binder;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)I

    .line 1150
    return-void
.end method
