.class public final Lcom/android/server/am/PendingIntentControllerStubImpl$Provider;
.super Ljava/lang/Object;
.source "PendingIntentControllerStubImpl$Provider.java"

# interfaces
.implements Lcom/miui/base/MiuiStubRegistry$ImplProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/PendingIntentControllerStubImpl$Provider$SINGLETON;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/miui/base/MiuiStubRegistry$ImplProvider<",
        "Lcom/android/server/am/PendingIntentControllerStubImpl;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public provideNewInstance()Lcom/android/server/am/PendingIntentControllerStubImpl;
    .locals 2

    .line 17
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Impl class com.android.server.am.PendingIntentControllerStubImpl is marked as singleton"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public bridge synthetic provideNewInstance()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/android/server/am/PendingIntentControllerStubImpl$Provider;->provideNewInstance()Lcom/android/server/am/PendingIntentControllerStubImpl;

    move-result-object v0

    return-object v0
.end method

.method public provideSingleton()Lcom/android/server/am/PendingIntentControllerStubImpl;
    .locals 1

    .line 13
    sget-object v0, Lcom/android/server/am/PendingIntentControllerStubImpl$Provider$SINGLETON;->INSTANCE:Lcom/android/server/am/PendingIntentControllerStubImpl;

    return-object v0
.end method

.method public bridge synthetic provideSingleton()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/android/server/am/PendingIntentControllerStubImpl$Provider;->provideSingleton()Lcom/android/server/am/PendingIntentControllerStubImpl;

    move-result-object v0

    return-object v0
.end method
