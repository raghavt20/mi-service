class com.android.server.am.AppStateManager$TaskRecord {
	 /* .source "AppStateManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/AppStateManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "TaskRecord" */
} // .end annotation
/* # instance fields */
java.lang.String mLastPkg;
Integer mLastUid;
final android.util.ArraySet mProcesses;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArraySet<", */
/* "Lcom/android/server/am/AppStateManager$AppState$RunningProcess;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
Integer mTaskId;
final com.android.server.am.AppStateManager this$0; //synthetic
/* # direct methods */
 com.android.server.am.AppStateManager$TaskRecord ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/android/server/am/AppStateManager; */
/* .param p2, "taskId" # I */
/* .line 3407 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 3404 */
final String v0 = ""; // const-string v0, ""
this.mLastPkg = v0;
/* .line 3405 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
this.mProcesses = v0;
/* .line 3408 */
/* iput p2, p0, Lcom/android/server/am/AppStateManager$TaskRecord;->mTaskId:I */
/* .line 3409 */
return;
} // .end method
/* # virtual methods */
void addProcess ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) {
/* .locals 1 */
/* .param p1, "process" # Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 3416 */
v0 = this.mProcesses;
(( android.util.ArraySet ) v0 ).add ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 3417 */
v0 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getUid()I
/* iput v0, p0, Lcom/android/server/am/AppStateManager$TaskRecord;->mLastUid:I */
/* .line 3418 */
(( com.android.server.am.AppStateManager$AppState$RunningProcess ) p1 ).getProcessName ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getProcessName()Ljava/lang/String;
this.mLastPkg = v0;
/* .line 3419 */
return;
} // .end method
void dump ( java.io.PrintWriter p0, java.lang.String[] p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "args" # [Ljava/lang/String; */
/* .param p3, "opti" # I */
/* .line 3445 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " Task:"; // const-string v1, " Task:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/am/AppStateManager$TaskRecord;->mTaskId:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/am/AppStateManager$TaskRecord;->mLastUid:I */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mLastPkg;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3446 */
v0 = this.mProcesses;
(( android.util.ArraySet ) v0 ).iterator ( ); // invoke-virtual {v0}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 3447 */
/* .local v1, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " "; // const-string v3, " "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3448 */
} // .end local v1 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 3449 */
} // :cond_0
return;
} // .end method
Integer getTaskId ( ) {
/* .locals 1 */
/* .line 3412 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$TaskRecord;->mTaskId:I */
} // .end method
Boolean isProcessInTask ( Integer p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 3422 */
v0 = this.mProcesses;
(( android.util.ArraySet ) v0 ).iterator ( ); // invoke-virtual {v0}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 3423 */
/* .local v1, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
v2 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v1 ).getPid ( ); // invoke-virtual {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPid()I
/* if-ne v2, p2, :cond_0 */
/* .line 3424 */
int v0 = 1; // const/4 v0, 0x1
/* .line 3426 */
} // .end local v1 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
} // :cond_0
/* .line 3427 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
Boolean isProcessInTask ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "prcName" # Ljava/lang/String; */
/* .line 3431 */
v0 = this.mProcesses;
(( android.util.ArraySet ) v0 ).iterator ( ); // invoke-virtual {v0}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 3432 */
/* .local v1, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
(( com.android.server.am.AppStateManager$AppState$RunningProcess ) v1 ).getProcessName ( ); // invoke-virtual {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getProcessName()Ljava/lang/String;
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 3433 */
int v0 = 1; // const/4 v0, 0x1
/* .line 3435 */
} // .end local v1 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
} // :cond_0
/* .line 3436 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 3441 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "TaskRecord{"; // const-string v1, "TaskRecord{"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/am/AppStateManager$TaskRecord;->mTaskId:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/am/AppStateManager$TaskRecord;->mLastUid:I */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mLastPkg;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mProcesses;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
