.class Lcom/android/server/am/ProcessManagerService$ProcessMangaerShellCommand;
.super Landroid/os/ShellCommand;
.source "ProcessManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ProcessManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ProcessMangaerShellCommand"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ProcessManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/am/ProcessManagerService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/ProcessManagerService;

    .line 1296
    iput-object p1, p0, Lcom/android/server/am/ProcessManagerService$ProcessMangaerShellCommand;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-direct {p0}, Landroid/os/ShellCommand;-><init>()V

    return-void
.end method


# virtual methods
.method public onCommand(Ljava/lang/String;)I
    .locals 7
    .param p1, "cmd"    # Ljava/lang/String;

    .line 1300
    if-nez p1, :cond_0

    .line 1301
    invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessManagerService$ProcessMangaerShellCommand;->handleDefaultCommands(Ljava/lang/String;)I

    move-result v0

    return v0

    .line 1303
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService$ProcessMangaerShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 1305
    .local v0, "pw":Ljava/io/PrintWriter;
    const/4 v1, -0x1

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_1
    goto :goto_0

    :pswitch_0
    const-string v2, "pl"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    goto :goto_1

    :goto_0
    move v2, v1

    :goto_1
    packed-switch v2, :pswitch_data_1

    .line 1312
    invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessManagerService$ProcessMangaerShellCommand;->handleDefaultCommands(Ljava/lang/String;)I

    move-result v1

    goto :goto_2

    .line 1307
    :pswitch_1
    iget-object v2, p0, Lcom/android/server/am/ProcessManagerService$ProcessMangaerShellCommand;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService$ProcessMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v3

    .line 1308
    invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService$ProcessMangaerShellCommand;->getNextArg()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 1309
    invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService$ProcessMangaerShellCommand;->getNextArg()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Lmiui/process/LifecycleConfig;->create(I)Lmiui/process/LifecycleConfig;

    move-result-object v5

    .line 1307
    const/4 v6, 0x1

    invoke-virtual {v2, v3, v4, v6, v5}, Lcom/android/server/am/ProcessManagerService;->startPreloadApp(Ljava/lang/String;ZZLmiui/process/LifecycleConfig;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1310
    nop

    .line 1316
    goto :goto_3

    .line 1312
    :goto_2
    return v1

    .line 1314
    :catch_0
    move-exception v2

    .line 1315
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1317
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_3
    return v1

    nop

    :pswitch_data_0
    .packed-switch 0xdfc
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method public onHelp()V
    .locals 2

    .line 1322
    invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService$ProcessMangaerShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 1323
    .local v0, "pw":Ljava/io/PrintWriter;
    const-string v1, "Process manager commands:"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1324
    const-string v1, "  pl PACKAGENAME"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1325
    return-void
.end method
