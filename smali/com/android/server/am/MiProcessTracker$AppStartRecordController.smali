.class Lcom/android/server/am/MiProcessTracker$AppStartRecordController;
.super Ljava/lang/Object;
.source "MiProcessTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/MiProcessTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AppStartRecordController"
.end annotation


# static fields
.field private static final APP_COLD_START_MODE:I = 0x1

.field private static final APP_HOT_START_MODE:I = 0x2

.field private static final APP_WARM_START_MODE:I = 0x3

.field private static final INTERVAL_TIME:J = 0xbb8L


# instance fields
.field private mForegroundPackageName:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/server/am/MiProcessTracker;


# direct methods
.method public static synthetic $r8$lambda$FcW9bx58BoSpww5UVZN4ptxJyeg(Lcom/miui/server/smartpower/IAppState;)Ljava/lang/String;
    .locals 0

    invoke-virtual {p0}, Lcom/miui/server/smartpower/IAppState;->getPackageName()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$monActivityLaunched(Lcom/android/server/am/MiProcessTracker$AppStartRecordController;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/am/MiProcessTracker$AppStartRecordController;->onActivityLaunched(Ljava/lang/String;Ljava/lang/String;II)V

    return-void
.end method

.method private constructor <init>(Lcom/android/server/am/MiProcessTracker;)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/server/am/MiProcessTracker;

    .line 199
    iput-object p1, p0, Lcom/android/server/am/MiProcessTracker$AppStartRecordController;->this$0:Lcom/android/server/am/MiProcessTracker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 197
    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/am/MiProcessTracker$AppStartRecordController;->mForegroundPackageName:Ljava/lang/String;

    .line 200
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/am/MiProcessTracker;Lcom/android/server/am/MiProcessTracker$AppStartRecordController-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/MiProcessTracker$AppStartRecordController;-><init>(Lcom/android/server/am/MiProcessTracker;)V

    return-void
.end method

.method static synthetic lambda$reportAppStart$0(Lcom/miui/server/smartpower/IAppState;)Z
    .locals 2
    .param p0, "app"    # Lcom/miui/server/smartpower/IAppState;

    .line 241
    invoke-virtual {p0}, Lcom/miui/server/smartpower/IAppState;->getUid()I

    move-result v0

    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_0

    sget-object v0, Lcom/android/server/am/MiProcessTracker;->trackerPkgWhiteList:Ljava/util/HashSet;

    .line 242
    invoke-virtual {p0}, Lcom/miui/server/smartpower/IAppState;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 243
    invoke-virtual {p0}, Lcom/miui/server/smartpower/IAppState;->getAppSwitchFgCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 244
    invoke-virtual {p0}, Lcom/miui/server/smartpower/IAppState;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 241
    :goto_0
    return v0
.end method

.method static synthetic lambda$reportAppStart$1(I)[Ljava/lang/String;
    .locals 1
    .param p0, "x$0"    # I

    .line 248
    new-array v0, p0, [Ljava/lang/String;

    return-object v0
.end method

.method private onActivityLaunched(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 3
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "uid"    # I
    .param p4, "launchState"    # I

    .line 204
    if-nez p4, :cond_0

    return-void

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/MiProcessTracker$AppStartRecordController;->mForegroundPackageName:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 206
    :cond_1
    iput-object p2, p0, Lcom/android/server/am/MiProcessTracker$AppStartRecordController;->mForegroundPackageName:Ljava/lang/String;

    .line 207
    const/16 v0, 0x3e8

    if-ne p3, v0, :cond_2

    return-void

    .line 208
    :cond_2
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    return-void

    .line 210
    :cond_3
    iget-object v0, p0, Lcom/android/server/am/MiProcessTracker$AppStartRecordController;->this$0:Lcom/android/server/am/MiProcessTracker;

    invoke-static {v0}, Lcom/android/server/am/MiProcessTracker;->-$$Nest$fgetmSmartPowerService(Lcom/android/server/am/MiProcessTracker;)Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    move-result-object v0

    .line 211
    invoke-interface {v0, p3}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->getAppState(I)Lcom/miui/server/smartpower/IAppState;

    move-result-object v0

    .line 212
    .local v0, "app":Lcom/miui/server/smartpower/IAppState;
    if-eqz v0, :cond_7

    sget-object v1, Lcom/android/server/am/MiProcessTracker;->trackerPkgWhiteList:Ljava/util/HashSet;

    invoke-virtual {v1, p2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    goto :goto_1

    .line 215
    :cond_4
    packed-switch p4, :pswitch_data_0

    goto :goto_0

    .line 225
    :pswitch_0
    const/4 v1, 0x2

    invoke-direct {p0, p2, v1}, Lcom/android/server/am/MiProcessTracker$AppStartRecordController;->reportAppStart(Ljava/lang/String;I)V

    .line 226
    goto :goto_0

    .line 222
    :pswitch_1
    const/4 v1, 0x3

    invoke-direct {p0, p2, v1}, Lcom/android/server/am/MiProcessTracker$AppStartRecordController;->reportAppStart(Ljava/lang/String;I)V

    .line 223
    goto :goto_0

    .line 217
    :pswitch_2
    iget-object v1, p0, Lcom/android/server/am/MiProcessTracker$AppStartRecordController;->this$0:Lcom/android/server/am/MiProcessTracker;

    invoke-static {v1}, Lcom/android/server/am/MiProcessTracker;->-$$Nest$fgetmProcessRecordController(Lcom/android/server/am/MiProcessTracker;)Lcom/android/server/am/MiProcessTracker$ProcessRecordController;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/android/server/am/MiProcessTracker$ProcessRecordController;->-$$Nest$misAppFirstStart(Lcom/android/server/am/MiProcessTracker$ProcessRecordController;Lcom/miui/server/smartpower/IAppState;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 218
    const/4 v1, 0x1

    invoke-direct {p0, p2, v1}, Lcom/android/server/am/MiProcessTracker$AppStartRecordController;->reportAppStart(Ljava/lang/String;I)V

    .line 231
    :cond_5
    :goto_0
    sget-boolean v1, Lcom/android/server/am/MiProcessTracker;->DEBUG:Z

    if-eqz v1, :cond_6

    .line 232
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityLaunched packageName = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " launchState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiProcessTracker"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    :cond_6
    return-void

    .line 213
    :cond_7
    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private reportAppStart(Ljava/lang/String;I)V
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "flag"    # I

    .line 238
    iget-object v0, p0, Lcom/android/server/am/MiProcessTracker$AppStartRecordController;->this$0:Lcom/android/server/am/MiProcessTracker;

    invoke-static {v0}, Lcom/android/server/am/MiProcessTracker;->-$$Nest$fgetmPerfShielder(Lcom/android/server/am/MiProcessTracker;)Lcom/android/internal/app/IPerfShielder;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 239
    iget-object v0, p0, Lcom/android/server/am/MiProcessTracker$AppStartRecordController;->this$0:Lcom/android/server/am/MiProcessTracker;

    invoke-static {v0}, Lcom/android/server/am/MiProcessTracker;->-$$Nest$fgetmSmartPowerService(Lcom/android/server/am/MiProcessTracker;)Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    move-result-object v0

    .line 240
    invoke-interface {v0}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->getAllAppState()Ljava/util/ArrayList;

    move-result-object v0

    .line 241
    invoke-virtual {v0}, Ljava/util/ArrayList;->stream()Ljava/util/stream/Stream;

    move-result-object v0

    new-instance v1, Lcom/android/server/am/MiProcessTracker$AppStartRecordController$$ExternalSyntheticLambda0;

    invoke-direct {v1}, Lcom/android/server/am/MiProcessTracker$AppStartRecordController$$ExternalSyntheticLambda0;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/stream/Stream;->filter(Ljava/util/function/Predicate;)Ljava/util/stream/Stream;

    move-result-object v0

    .line 245
    invoke-static {}, Ljava/util/stream/Collectors;->toList()Ljava/util/stream/Collector;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 246
    .local v0, "residentAppList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/IAppState;>;"
    invoke-interface {v0}, Ljava/util/List;->stream()Ljava/util/stream/Stream;

    move-result-object v1

    new-instance v2, Lcom/android/server/am/MiProcessTracker$AppStartRecordController$$ExternalSyntheticLambda1;

    invoke-direct {v2}, Lcom/android/server/am/MiProcessTracker$AppStartRecordController$$ExternalSyntheticLambda1;-><init>()V

    .line 247
    invoke-interface {v1, v2}, Ljava/util/stream/Stream;->map(Ljava/util/function/Function;)Ljava/util/stream/Stream;

    move-result-object v1

    .line 248
    invoke-static {}, Ljava/util/stream/Collectors;->toList()Ljava/util/stream/Collector;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    new-instance v2, Lcom/android/server/am/MiProcessTracker$AppStartRecordController$$ExternalSyntheticLambda2;

    invoke-direct {v2}, Lcom/android/server/am/MiProcessTracker$AppStartRecordController$$ExternalSyntheticLambda2;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->toArray(Ljava/util/function/IntFunction;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    .line 249
    .local v1, "residentPkgNameList":[Ljava/lang/String;
    sget-boolean v2, Lcom/android/server/am/MiProcessTracker;->DEBUG:Z

    const-string v3, "MiProcessTracker"

    if-eqz v2, :cond_0

    .line 250
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "reportAppStart packageName = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " flag = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " residentAppList size = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 252
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " residentAppList = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 253
    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 250
    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/MiProcessTracker$AppStartRecordController;->this$0:Lcom/android/server/am/MiProcessTracker;

    invoke-static {v2}, Lcom/android/server/am/MiProcessTracker;->-$$Nest$fgetmPerfShielder(Lcom/android/server/am/MiProcessTracker;)Lcom/android/internal/app/IPerfShielder;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {v2, p1, p2, v4, v1}, Lcom/android/internal/app/IPerfShielder;->reportApplicationStart(Ljava/lang/String;II[Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 260
    goto :goto_0

    .line 258
    :catch_0
    move-exception v2

    .line 259
    .local v2, "e":Landroid/os/RemoteException;
    const-string v4, "mPerfShielder is dead"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 262
    .end local v0    # "residentAppList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/IAppState;>;"
    .end local v1    # "residentPkgNameList":[Ljava/lang/String;
    .end local v2    # "e":Landroid/os/RemoteException;
    :cond_1
    :goto_0
    return-void
.end method
