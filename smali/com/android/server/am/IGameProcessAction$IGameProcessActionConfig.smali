.class public interface abstract Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;
.super Ljava/lang/Object;
.source "IGameProcessAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/IGameProcessAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IGameProcessActionConfig"
.end annotation


# virtual methods
.method public abstract addWhiteList(Ljava/util/List;Z)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation
.end method

.method public abstract getPrio()I
.end method

.method public abstract initFromJSON(Lorg/json/JSONObject;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation
.end method
