.class Lcom/android/server/am/ProcessMemoryCleaner$1;
.super Ljava/lang/Object;
.source "ProcessMemoryCleaner.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/ProcessMemoryCleaner;->cleanUpMemory(Ljava/util/List;J)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/miui/server/smartpower/IAppState$IRunningProcess;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ProcessMemoryCleaner;


# direct methods
.method constructor <init>(Lcom/android/server/am/ProcessMemoryCleaner;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/ProcessMemoryCleaner;

    .line 220
    iput-object p1, p0, Lcom/android/server/am/ProcessMemoryCleaner$1;->this$0:Lcom/android/server/am/ProcessMemoryCleaner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/miui/server/smartpower/IAppState$IRunningProcess;Lcom/miui/server/smartpower/IAppState$IRunningProcess;)I
    .locals 4
    .param p1, "o1"    # Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .param p2, "o2"    # Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 223
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->hasActivity()Z

    move-result v0

    .line 224
    .local v0, "isHav1":Z
    invoke-virtual {p2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->hasActivity()Z

    move-result v1

    .line 225
    .local v1, "isHav2":Z
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    :cond_0
    if-nez v0, :cond_2

    if-nez v1, :cond_2

    .line 226
    :cond_1
    invoke-static {p2}, Lcom/android/server/am/ProcessMemoryCleaner;->getProcPriority(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)I

    move-result v2

    invoke-static {p1}, Lcom/android/server/am/ProcessMemoryCleaner;->getProcPriority(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)I

    move-result v3

    sub-int/2addr v2, v3

    return v2

    .line 227
    :cond_2
    if-eqz v0, :cond_3

    .line 228
    const/4 v2, 0x1

    return v2

    .line 230
    :cond_3
    const/4 v2, -0x1

    return v2
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 220
    check-cast p1, Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    check-cast p2, Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    invoke-virtual {p0, p1, p2}, Lcom/android/server/am/ProcessMemoryCleaner$1;->compare(Lcom/miui/server/smartpower/IAppState$IRunningProcess;Lcom/miui/server/smartpower/IAppState$IRunningProcess;)I

    move-result p1

    return p1
.end method
