class com.android.server.am.SmartPowerService$SmartPowerShellCommand extends android.os.ShellCommand {
	 /* .source "SmartPowerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/SmartPowerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = "SmartPowerShellCommand" */
} // .end annotation
/* # instance fields */
com.android.server.am.SmartPowerService mService;
/* # direct methods */
 com.android.server.am.SmartPowerService$SmartPowerShellCommand ( ) {
/* .locals 0 */
/* .param p1, "service" # Lcom/android/server/am/SmartPowerService; */
/* .line 1411 */
/* invoke-direct {p0}, Landroid/os/ShellCommand;-><init>()V */
/* .line 1412 */
this.mService = p1;
/* .line 1413 */
return;
} // .end method
/* # virtual methods */
public Integer onCommand ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "cmd" # Ljava/lang/String; */
/* .line 1417 */
(( com.android.server.am.SmartPowerService$SmartPowerShellCommand ) p0 ).getOutFileDescriptor ( ); // invoke-virtual {p0}, Lcom/android/server/am/SmartPowerService$SmartPowerShellCommand;->getOutFileDescriptor()Ljava/io/FileDescriptor;
/* .line 1418 */
/* .local v0, "fd":Ljava/io/FileDescriptor; */
(( com.android.server.am.SmartPowerService$SmartPowerShellCommand ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/android/server/am/SmartPowerService$SmartPowerShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;
/* .line 1419 */
/* .local v1, "pw":Ljava/io/PrintWriter; */
(( com.android.server.am.SmartPowerService$SmartPowerShellCommand ) p0 ).getAllArgs ( ); // invoke-virtual {p0}, Lcom/android/server/am/SmartPowerService$SmartPowerShellCommand;->getAllArgs()[Ljava/lang/String;
/* .line 1420 */
/* .local v2, "args":[Ljava/lang/String; */
/* if-nez p1, :cond_0 */
/* .line 1421 */
v3 = (( com.android.server.am.SmartPowerService$SmartPowerShellCommand ) p0 ).handleDefaultCommands ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/SmartPowerService$SmartPowerShellCommand;->handleDefaultCommands(Ljava/lang/String;)I
/* .line 1424 */
} // :cond_0
try { // :try_start_0
v3 = this.mService;
(( com.android.server.am.SmartPowerService ) v3 ).dump ( v0, v1, v2 ); // invoke-virtual {v3, v0, v1, v2}, Lcom/android/server/am/SmartPowerService;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1427 */
/* .line 1425 */
/* :catch_0 */
/* move-exception v3 */
/* .line 1426 */
/* .local v3, "e":Ljava/lang/Exception; */
(( java.io.PrintWriter ) v1 ).println ( v3 ); // invoke-virtual {v1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
/* .line 1428 */
} // .end local v3 # "e":Ljava/lang/Exception;
} // :goto_0
int v3 = -1; // const/4 v3, -0x1
} // .end method
public void onHelp ( ) {
/* .locals 2 */
/* .line 1433 */
(( com.android.server.am.SmartPowerService$SmartPowerShellCommand ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/android/server/am/SmartPowerService$SmartPowerShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;
/* .line 1434 */
/* .local v0, "pw":Ljava/io/PrintWriter; */
/* const-string/jumbo v1, "smart power (smartpower) commands:" */
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1435 */
(( java.io.PrintWriter ) v0 ).println ( ); // invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V
/* .line 1436 */
final String v1 = " policy"; // const-string v1, " policy"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1437 */
final String v1 = " appstate"; // const-string v1, " appstate"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1438 */
final String v1 = " fz"; // const-string v1, " fz"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1439 */
(( java.io.PrintWriter ) v0 ).println ( ); // invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V
/* .line 1440 */
return;
} // .end method
