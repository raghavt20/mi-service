.class public Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;
.super Ljava/lang/Object;
.source "GameProcessCompactor.java"

# interfaces
.implements Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/GameProcessCompactor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GameProcessCompactorConfig"
.end annotation


# static fields
.field private static final COMPACTOR_INCOME_PCT:Ljava/lang/String; = "compactor-income-pct"

.field private static final COMPACTOR_INTERVAL:Ljava/lang/String; = "compactor-interval"

.field private static final COMPACT_MAX_NUM:Ljava/lang/String; = "compact-max-num"

.field private static final CONFIG_PRIO:Ljava/lang/String; = "prio"

.field private static final DEFAULT_MAX_NUM:I = -0x1

.field private static final DEFAULT_PRIO:I = 0x0

.field private static final MAX_ADJ:Ljava/lang/String; = "max-adj"

.field private static final MIN_ADJ:Ljava/lang/String; = "min-adj"

.field private static final SKIP_ACTIVE:Ljava/lang/String; = "skip-active"

.field private static final SKIP_FOREGROUND:Ljava/lang/String; = "skip-foreground"

.field private static final WHITE_LIST:Ljava/lang/String; = "white-list"


# instance fields
.field mCompactIncomePctThreshold:I

.field mCompactIntervalThreshold:I

.field mCompactMaxNum:I

.field mMaxAdj:I

.field mMinAdj:I

.field mPrio:I

.field mSkipActive:Z

.field mSkipForeground:Z

.field mWhiteList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mPrio:I

    .line 171
    const/16 v1, -0x2710

    iput v1, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mMinAdj:I

    .line 172
    const/16 v1, 0x3e9

    iput v1, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mMaxAdj:I

    .line 173
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mWhiteList:Ljava/util/List;

    .line 174
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mSkipForeground:Z

    .line 175
    iput-boolean v1, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mSkipActive:Z

    .line 176
    iput v0, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mCompactIntervalThreshold:I

    .line 177
    iput v0, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mCompactIncomePctThreshold:I

    .line 178
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mCompactMaxNum:I

    return-void
.end method


# virtual methods
.method public addWhiteList(Ljava/util/List;Z)V
    .locals 1
    .param p2, "append"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .line 230
    .local p1, "wl":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p2, :cond_0

    .line 231
    iget-object v0, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mWhiteList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 233
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mWhiteList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 234
    :goto_0
    iget-object v0, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mWhiteList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 235
    return-void
.end method

.method public getPrio()I
    .locals 1

    .line 225
    iget v0, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mPrio:I

    return v0
.end method

.method public initFromJSON(Lorg/json/JSONObject;)V
    .locals 11
    .param p1, "obj"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 185
    const-string/jumbo v0, "white-list"

    const-string v1, "compact-max-num"

    const-string/jumbo v2, "skip-foreground"

    const-string/jumbo v3, "skip-active"

    const-string v4, "compactor-income-pct"

    const-string v5, "compactor-interval"

    const-string v6, "max-adj"

    const-string v7, "min-adj"

    const-string v8, "prio"

    :try_start_0
    invoke-virtual {p1, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    const/4 v10, 0x0

    if-eqz v9, :cond_0

    .line 186
    invoke-virtual {p1, v8, v10}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v8

    iput v8, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mPrio:I

    .line 188
    :cond_0
    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 189
    const/16 v8, -0x2710

    invoke-virtual {p1, v7, v8}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v7

    iput v7, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mMinAdj:I

    .line 191
    :cond_1
    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 192
    const/16 v7, 0x3e9

    invoke-virtual {p1, v6, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mMaxAdj:I

    .line 194
    :cond_2
    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 195
    invoke-virtual {p1, v5, v10}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mCompactIntervalThreshold:I

    .line 197
    :cond_3
    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 198
    invoke-virtual {p1, v4, v10}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mCompactIncomePctThreshold:I

    .line 200
    :cond_4
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    const/4 v5, 0x1

    if-eqz v4, :cond_5

    .line 201
    invoke-virtual {p1, v3, v5}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mSkipActive:Z

    .line 203
    :cond_5
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 204
    invoke-virtual {p1, v2, v5}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mSkipForeground:Z

    .line 206
    :cond_6
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 207
    const/4 v2, -0x1

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mCompactMaxNum:I

    .line 209
    :cond_7
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 210
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 211
    .local v0, "tmpArray":Lorg/json/JSONArray;
    if-eqz v0, :cond_9

    .line 212
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_9

    .line 213
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_8

    .line 214
    iget-object v2, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mWhiteList:Ljava/util/List;

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 212
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 220
    .end local v0    # "tmpArray":Lorg/json/JSONArray;
    .end local v1    # "i":I
    :cond_9
    nop

    .line 221
    return-void

    .line 218
    :catch_0
    move-exception v0

    .line 219
    .local v0, "e":Lorg/json/JSONException;
    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 239
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 240
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "prio="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 241
    iget v1, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mPrio:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 242
    const-string v1, ", minAdj="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    iget v1, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mMinAdj:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 244
    const-string v1, ", maxAdj="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 245
    iget v1, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mMaxAdj:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 246
    const-string v1, ", compactInterval="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    iget v1, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mCompactIntervalThreshold:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 248
    const-string v1, ", compactIncomePct="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    iget v1, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mCompactIncomePctThreshold:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 250
    const-string v1, ", skipForeground="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    iget-boolean v1, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mSkipForeground:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 252
    const-string v1, ", skipActive="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    iget-boolean v1, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mSkipActive:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 254
    const-string v1, ", whiteList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    iget-object v1, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mWhiteList:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 256
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
