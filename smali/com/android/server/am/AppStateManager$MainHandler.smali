.class Lcom/android/server/am/AppStateManager$MainHandler;
.super Landroid/os/Handler;
.source "AppStateManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/AppStateManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MainHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/AppStateManager;


# direct methods
.method constructor <init>(Lcom/android/server/am/AppStateManager;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 3314
    iput-object p1, p0, Lcom/android/server/am/AppStateManager$MainHandler;->this$0:Lcom/android/server/am/AppStateManager;

    .line 3315
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 3316
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .line 3320
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 3321
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_5

    .line 3353
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 3354
    .local v0, "userId":I
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$MainHandler;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmAppLock(Lcom/android/server/am/AppStateManager;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 3355
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 3356
    .local v2, "targerUids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v4, p0, Lcom/android/server/am/AppStateManager$MainHandler;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v4}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmActiveApps(Lcom/android/server/am/AppStateManager;)Lcom/android/server/am/AppStateManager$ActiveApps;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/server/am/AppStateManager$ActiveApps;->size()I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 3357
    iget-object v4, p0, Lcom/android/server/am/AppStateManager$MainHandler;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v4}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmActiveApps(Lcom/android/server/am/AppStateManager;)Lcom/android/server/am/AppStateManager$ActiveApps;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/android/server/am/AppStateManager$ActiveApps;->valueAt(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v4

    .line 3358
    .local v4, "app":Lcom/android/server/am/AppStateManager$AppState;
    invoke-static {v4}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmUid(Lcom/android/server/am/AppStateManager$AppState;)I

    move-result v5

    invoke-static {v5}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v5

    if-ne v5, v0, :cond_0

    invoke-virtual {v4}, Lcom/android/server/am/AppStateManager$AppState;->isAlive()Z

    move-result v5

    if-nez v5, :cond_0

    .line 3359
    invoke-static {v4}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmUid(Lcom/android/server/am/AppStateManager$AppState;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3356
    .end local v4    # "app":Lcom/android/server/am/AppStateManager$AppState;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 3362
    .end local v3    # "i":I
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 3363
    .local v4, "uid":I
    iget-object v5, p0, Lcom/android/server/am/AppStateManager$MainHandler;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v5}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmActiveApps(Lcom/android/server/am/AppStateManager;)Lcom/android/server/am/AppStateManager$ActiveApps;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/android/server/am/AppStateManager$ActiveApps;->remove(I)V

    .line 3364
    .end local v4    # "uid":I
    goto :goto_1

    .line 3365
    .end local v2    # "targerUids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_2
    monitor-exit v1

    .line 3366
    goto/16 :goto_5

    .line 3365
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 3333
    .end local v0    # "userId":I
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/am/AppStateManager$AppState;

    .line 3334
    .local v0, "fromApp":Lcom/android/server/am/AppStateManager$AppState;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 3335
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v2, "reason"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3336
    .local v2, "reason":Ljava/lang/String;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 3337
    .local v3, "targetApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/AppStateManager$AppState;>;"
    iget-object v4, p0, Lcom/android/server/am/AppStateManager$MainHandler;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v4}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmAppLock(Lcom/android/server/am/AppStateManager;)Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 3338
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    :try_start_1
    iget-object v6, p0, Lcom/android/server/am/AppStateManager$MainHandler;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v6}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmActiveApps(Lcom/android/server/am/AppStateManager;)Lcom/android/server/am/AppStateManager$ActiveApps;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/server/am/AppStateManager$ActiveApps;->size()I

    move-result v6

    if-ge v5, v6, :cond_4

    .line 3339
    iget-object v6, p0, Lcom/android/server/am/AppStateManager$MainHandler;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v6}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmActiveApps(Lcom/android/server/am/AppStateManager;)Lcom/android/server/am/AppStateManager$ActiveApps;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/android/server/am/AppStateManager$ActiveApps;->valueAt(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v6

    .line 3340
    .local v6, "app":Lcom/android/server/am/AppStateManager$AppState;
    if-eq v6, v0, :cond_3

    invoke-static {v6}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmUid(Lcom/android/server/am/AppStateManager$AppState;)I

    move-result v7

    invoke-static {v7}, Landroid/os/Process;->isCoreUid(I)Z

    move-result v7

    if-nez v7, :cond_3

    .line 3341
    invoke-virtual {v6}, Lcom/android/server/am/AppStateManager$AppState;->getCurrentState()I

    move-result v7

    const/4 v8, 0x3

    if-lt v7, v8, :cond_3

    .line 3342
    invoke-virtual {v6}, Lcom/android/server/am/AppStateManager$AppState;->getCurrentState()I

    move-result v7

    const/4 v8, 0x5

    if-gt v7, v8, :cond_3

    .line 3343
    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3338
    .end local v6    # "app":Lcom/android/server/am/AppStateManager$AppState;
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 3346
    .end local v5    # "i":I
    :cond_4
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 3347
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/am/AppStateManager$AppState;

    .line 3348
    .local v5, "app":Lcom/android/server/am/AppStateManager$AppState;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "hibernate all "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$mhibernateAllInactive(Lcom/android/server/am/AppStateManager$AppState;Ljava/lang/String;)V

    .line 3349
    .end local v5    # "app":Lcom/android/server/am/AppStateManager$AppState;
    goto :goto_3

    .line 3350
    :cond_5
    goto :goto_5

    .line 3346
    :catchall_1
    move-exception v5

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v5

    .line 3323
    .end local v0    # "fromApp":Lcom/android/server/am/AppStateManager$AppState;
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v2    # "reason":Ljava/lang/String;
    .end local v3    # "targetApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/AppStateManager$AppState;>;"
    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 3324
    .local v0, "reason":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$MainHandler;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmAppLock(Lcom/android/server/am/AppStateManager;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 3325
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_4
    :try_start_3
    iget-object v3, p0, Lcom/android/server/am/AppStateManager$MainHandler;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v3}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmActiveApps(Lcom/android/server/am/AppStateManager;)Lcom/android/server/am/AppStateManager$ActiveApps;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$ActiveApps;->size()I

    move-result v3

    if-ge v2, v3, :cond_6

    .line 3326
    iget-object v3, p0, Lcom/android/server/am/AppStateManager$MainHandler;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v3}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmActiveApps(Lcom/android/server/am/AppStateManager;)Lcom/android/server/am/AppStateManager$ActiveApps;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/android/server/am/AppStateManager$ActiveApps;->valueAt(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v3

    .line 3327
    .local v3, "app":Lcom/android/server/am/AppStateManager$AppState;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "hibernate all "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$mhibernateAllIfNeeded(Lcom/android/server/am/AppStateManager$AppState;Ljava/lang/String;)V

    .line 3325
    .end local v3    # "app":Lcom/android/server/am/AppStateManager$AppState;
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 3329
    .end local v2    # "i":I
    :cond_6
    monitor-exit v1

    .line 3330
    goto :goto_5

    .line 3329
    :catchall_2
    move-exception v2

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    throw v2

    .line 3371
    .end local v0    # "reason":Ljava/lang/String;
    :goto_5
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
