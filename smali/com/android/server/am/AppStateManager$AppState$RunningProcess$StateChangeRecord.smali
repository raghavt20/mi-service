.class Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;
.super Ljava/lang/Object;
.source "AppStateManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StateChangeRecord"
.end annotation


# instance fields
.field private mAdj:I

.field private mChangeReason:Ljava/lang/String;

.field private mInterval:J

.field private mNewState:I

.field private mOldState:I

.field private mTimeStamp:J

.field final synthetic this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;


# direct methods
.method static bridge synthetic -$$Nest$fgetmInterval(Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->mInterval:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmNewState(Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->mNewState:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmOldState(Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->mOldState:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmTimeStamp(Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->mTimeStamp:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$mgetTimeStamp(Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;)J
    .locals 2

    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->getTimeStamp()J

    move-result-wide v0

    return-wide v0
.end method

.method private constructor <init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;IIJLjava/lang/String;I)V
    .locals 2
    .param p2, "oldState"    # I
    .param p3, "newState"    # I
    .param p4, "intervalMills"    # J
    .param p6, "reson"    # Ljava/lang/String;
    .param p7, "adj"    # I

    .line 2923
    iput-object p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2924
    iput p2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->mOldState:I

    .line 2925
    iput p3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->mNewState:I

    .line 2926
    iput-wide p4, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->mInterval:J

    .line 2927
    iput-object p6, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->mChangeReason:Ljava/lang/String;

    .line 2928
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->mTimeStamp:J

    .line 2929
    iput p7, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->mAdj:I

    .line 2930
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;IIJLjava/lang/String;ILcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord-IA;)V
    .locals 0

    invoke-direct/range {p0 .. p7}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;-><init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;IIJLjava/lang/String;I)V

    return-void
.end method

.method private getTimeStamp()J
    .locals 2

    .line 2933
    iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->mTimeStamp:J

    return-wide v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 5

    .line 2938
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->mOldState:I

    .line 2939
    invoke-static {v0}, Lcom/android/server/am/AppStateManager;->processStateToString(I)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->mNewState:I

    invoke-static {v1}, Lcom/android/server/am/AppStateManager;->processStateToString(I)Ljava/lang/String;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->mInterval:J

    .line 2940
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->mChangeReason:Ljava/lang/String;

    iget v4, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->mAdj:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    filled-new-array {v0, v1, v2, v3, v4}, [Ljava/lang/Object;

    move-result-object v0

    .line 2938
    const-string v1, "%s->%s(%dms) R(%s) adj=%s."

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
