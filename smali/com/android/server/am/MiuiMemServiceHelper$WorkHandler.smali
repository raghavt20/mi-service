.class Lcom/android/server/am/MiuiMemServiceHelper$WorkHandler;
.super Landroid/os/Handler;
.source "MiuiMemServiceHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/MiuiMemServiceHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WorkHandler"
.end annotation


# static fields
.field public static final CHARGING_RECLAIM:I = 0x1

.field public static final SCREENOFF_RECLAIM:I = 0x2


# instance fields
.field final synthetic this$0:Lcom/android/server/am/MiuiMemServiceHelper;


# direct methods
.method public constructor <init>(Lcom/android/server/am/MiuiMemServiceHelper;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 36
    iput-object p1, p0, Lcom/android/server/am/MiuiMemServiceHelper$WorkHandler;->this$0:Lcom/android/server/am/MiuiMemServiceHelper;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .line 40
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 41
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 44
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "start screen off reclaim... "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiMemoryService"

    invoke-static {v1, v0}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    iget-object v0, p0, Lcom/android/server/am/MiuiMemServiceHelper$WorkHandler;->this$0:Lcom/android/server/am/MiuiMemServiceHelper;

    invoke-static {v0}, Lcom/android/server/am/MiuiMemServiceHelper;->-$$Nest$fgetmMemService(Lcom/android/server/am/MiuiMemServiceHelper;)Lcom/android/server/am/MiuiMemoryService;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/server/am/MiuiMemoryService;->runProcsCompaction(I)V

    .line 49
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
