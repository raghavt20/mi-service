class com.android.server.am.OomAdjusterImpl$RunningAppRecordMap {
	 /* .source "OomAdjusterImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/OomAdjusterImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "RunningAppRecordMap" */
} // .end annotation
/* # instance fields */
private final java.util.List mList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.util.ArrayMap mMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
private com.android.server.am.OomAdjusterImpl$RunningAppRecordMap ( ) {
/* .locals 1 */
/* .line 704 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 709 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mMap = v0;
/* .line 714 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mList = v0;
return;
} // .end method
 com.android.server.am.OomAdjusterImpl$RunningAppRecordMap ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void clear ( ) {
/* .locals 1 */
/* .line 746 */
v0 = this.mMap;
(( android.util.ArrayMap ) v0 ).clear ( ); // invoke-virtual {v0}, Landroid/util/ArrayMap;->clear()V
/* .line 747 */
v0 = this.mList;
/* .line 748 */
return;
} // .end method
public java.lang.Integer getForKey ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 717 */
v0 = this.mMap;
(( android.util.ArrayMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Integer; */
/* .line 718 */
/* .local v0, "value":Ljava/lang/Integer; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
} // :cond_0
int v1 = -1; // const/4 v1, -0x1
} // :goto_0
java.lang.Integer .valueOf ( v1 );
} // .end method
public Integer getIndexForKey ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 722 */
v0 = v0 = this.mList;
} // .end method
public java.util.List getList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 751 */
v0 = this.mList;
} // .end method
public void put ( Integer p0, java.lang.String p1, java.lang.Integer p2 ) {
/* .locals 1 */
/* .param p1, "index" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "gameScence" # Ljava/lang/Integer; */
/* .line 733 */
v0 = v0 = this.mList;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 734 */
v0 = this.mList;
/* .line 736 */
} // :cond_0
v0 = this.mList;
/* .line 737 */
v0 = this.mMap;
(( android.util.ArrayMap ) v0 ).put ( p2, p3 ); // invoke-virtual {v0, p2, p3}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 738 */
return;
} // .end method
public void put ( java.lang.String p0, java.lang.Integer p1 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "gameScence" # Ljava/lang/Integer; */
/* .line 726 */
v0 = v0 = this.mList;
/* if-nez v0, :cond_0 */
/* .line 727 */
v0 = this.mList;
/* .line 729 */
} // :cond_0
v0 = this.mMap;
(( android.util.ArrayMap ) v0 ).put ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 730 */
return;
} // .end method
public void removeForKey ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 741 */
v0 = this.mMap;
(( android.util.ArrayMap ) v0 ).remove ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 742 */
v0 = this.mList;
/* .line 743 */
return;
} // .end method
public Integer size ( ) {
/* .locals 1 */
/* .line 755 */
v0 = v0 = this.mList;
} // .end method
