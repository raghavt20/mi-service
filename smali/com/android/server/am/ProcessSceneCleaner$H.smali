.class Lcom/android/server/am/ProcessSceneCleaner$H;
.super Landroid/os/Handler;
.source "ProcessSceneCleaner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ProcessSceneCleaner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "H"
.end annotation


# static fields
.field public static final KILL_ALL_EVENT:I = 0x1

.field public static final KILL_ANY_EVENT:I = 0x3

.field public static final SWIPE_KILL_EVENT:I = 0x2


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ProcessSceneCleaner;


# direct methods
.method public constructor <init>(Lcom/android/server/am/ProcessSceneCleaner;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 94
    iput-object p1, p0, Lcom/android/server/am/ProcessSceneCleaner$H;->this$0:Lcom/android/server/am/ProcessSceneCleaner;

    .line 95
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 96
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .line 100
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 101
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lmiui/process/ProcessConfig;

    .line 102
    .local v0, "config":Lmiui/process/ProcessConfig;
    iget v1, p1, Landroid/os/Message;->what:I

    const-wide/32 v2, 0x80000

    packed-switch v1, :pswitch_data_0

    goto/16 :goto_0

    .line 116
    :pswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SceneKillAny"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/android/server/am/ProcessSceneCleaner$H;->this$0:Lcom/android/server/am/ProcessSceneCleaner;

    .line 117
    invoke-virtual {v0}, Lmiui/process/ProcessConfig;->getPolicy()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/android/server/am/ProcessSceneCleaner;->getKillReason(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 116
    invoke-static {v2, v3, v1}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 118
    iget-object v1, p0, Lcom/android/server/am/ProcessSceneCleaner$H;->this$0:Lcom/android/server/am/ProcessSceneCleaner;

    invoke-static {v1, v0}, Lcom/android/server/am/ProcessSceneCleaner;->-$$Nest$mhandleKillAny(Lcom/android/server/am/ProcessSceneCleaner;Lmiui/process/ProcessConfig;)V

    .line 119
    invoke-static {v2, v3}, Landroid/os/Trace;->traceEnd(J)V

    .line 120
    goto :goto_0

    .line 110
    :pswitch_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SceneSwipeKill"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/android/server/am/ProcessSceneCleaner$H;->this$0:Lcom/android/server/am/ProcessSceneCleaner;

    .line 111
    invoke-virtual {v0}, Lmiui/process/ProcessConfig;->getPolicy()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/android/server/am/ProcessSceneCleaner;->getKillReason(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 110
    invoke-static {v2, v3, v1}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 112
    iget-object v1, p0, Lcom/android/server/am/ProcessSceneCleaner$H;->this$0:Lcom/android/server/am/ProcessSceneCleaner;

    invoke-static {v1, v0}, Lcom/android/server/am/ProcessSceneCleaner;->-$$Nest$mhandleSwipeKill(Lcom/android/server/am/ProcessSceneCleaner;Lmiui/process/ProcessConfig;)Z

    .line 113
    invoke-static {v2, v3}, Landroid/os/Trace;->traceEnd(J)V

    .line 114
    goto :goto_0

    .line 104
    :pswitch_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SceneKillAll:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/android/server/am/ProcessSceneCleaner$H;->this$0:Lcom/android/server/am/ProcessSceneCleaner;

    .line 105
    invoke-virtual {v0}, Lmiui/process/ProcessConfig;->getPolicy()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/android/server/am/ProcessSceneCleaner;->getKillReason(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 104
    invoke-static {v2, v3, v1}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 106
    iget-object v1, p0, Lcom/android/server/am/ProcessSceneCleaner$H;->this$0:Lcom/android/server/am/ProcessSceneCleaner;

    invoke-static {v1, v0}, Lcom/android/server/am/ProcessSceneCleaner;->-$$Nest$mhandleKillAll(Lcom/android/server/am/ProcessSceneCleaner;Lmiui/process/ProcessConfig;)V

    .line 107
    invoke-static {v2, v3}, Landroid/os/Trace;->traceEnd(J)V

    .line 108
    nop

    .line 124
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
