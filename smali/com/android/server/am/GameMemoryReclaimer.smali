.class public Lcom/android/server/am/GameMemoryReclaimer;
.super Ljava/lang/Object;
.source "GameMemoryReclaimer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/GameMemoryReclaimer$UidPss;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = true

.field private static final TAG:Ljava/lang/String; = "GameMemoryReclaimer"


# instance fields
.field public mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

.field private mAllPackageInfos:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/android/server/am/IGameProcessAction;",
            "Ljava/util/List<",
            "Lcom/android/server/am/GameProcessKiller$PackageMemInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field private mAllProcessInfos:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/android/server/am/IGameProcessAction;",
            "Ljava/util/List<",
            "Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field private mCompactInfos:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private mCurrentGame:Ljava/lang/String;

.field private final mHandler:Landroid/os/Handler;

.field private mProcessActions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/server/am/IGameProcessAction;",
            ">;"
        }
    .end annotation
.end field

.field private final mServiceThread:Lcom/android/server/ServiceThread;


# direct methods
.method public static synthetic $r8$lambda$CrEMA0V0cBHVFedy-6WqzzUQllo(Lcom/android/server/am/GameMemoryReclaimer;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/GameMemoryReclaimer;->lambda$notifyProcessDied$2(I)V

    return-void
.end method

.method public static synthetic $r8$lambda$M8Z9ZozU6ffpGf6QBCyFreoumIo(Lcom/android/server/am/GameMemoryReclaimer;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/am/GameMemoryReclaimer;->lambda$reclaimBackground$1(J)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmAllProcessInfos(Lcom/android/server/am/GameMemoryReclaimer;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/GameMemoryReclaimer;->mAllProcessInfos:Ljava/util/Map;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmCompactInfos(Lcom/android/server/am/GameMemoryReclaimer;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/GameMemoryReclaimer;->mCompactInfos:Ljava/util/Map;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmProcessActions(Lcom/android/server/am/GameMemoryReclaimer;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/GameMemoryReclaimer;->mProcessActions:Ljava/util/List;

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/server/am/ActivityManagerService;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ams"    # Lcom/android/server/am/ActivityManagerService;

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/GameMemoryReclaimer;->mAllProcessInfos:Ljava/util/Map;

    .line 48
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/GameMemoryReclaimer;->mAllPackageInfos:Ljava/util/Map;

    .line 50
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/GameMemoryReclaimer;->mCompactInfos:Ljava/util/Map;

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/am/GameMemoryReclaimer;->mCurrentGame:Ljava/lang/String;

    .line 55
    iput-object p1, p0, Lcom/android/server/am/GameMemoryReclaimer;->mContext:Landroid/content/Context;

    .line 57
    iput-object p2, p0, Lcom/android/server/am/GameMemoryReclaimer;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/GameMemoryReclaimer;->mProcessActions:Ljava/util/List;

    .line 60
    new-instance v0, Lcom/android/server/ServiceThread;

    const-string v1, "GameMemoryReclaimer"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/android/server/ServiceThread;-><init>(Ljava/lang/String;IZ)V

    iput-object v0, p0, Lcom/android/server/am/GameMemoryReclaimer;->mServiceThread:Lcom/android/server/ServiceThread;

    .line 61
    invoke-virtual {v0}, Lcom/android/server/ServiceThread;->start()V

    .line 62
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Lcom/android/server/ServiceThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/server/am/GameMemoryReclaimer;->mHandler:Landroid/os/Handler;

    .line 63
    return-void
.end method

.method private filterAllPackageInfos()V
    .locals 17

    .line 148
    move-object/from16 v1, p0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 149
    .local v2, "time":J
    iget-object v0, v1, Lcom/android/server/am/GameMemoryReclaimer;->mAllPackageInfos:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 150
    .local v4, "v":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/GameProcessKiller$PackageMemInfo;>;"
    invoke-interface {v4}, Ljava/util/List;->clear()V

    .end local v4    # "v":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/GameProcessKiller$PackageMemInfo;>;"
    goto :goto_0

    .line 151
    :cond_0
    iget-object v4, v1, Lcom/android/server/am/GameMemoryReclaimer;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v4

    .line 152
    :try_start_0
    iget-object v0, v1, Lcom/android/server/am/GameMemoryReclaimer;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    iget-object v0, v0, Lcom/android/server/am/ProcessList;->mActiveUids:Lcom/android/server/am/ActiveUids;

    .line 153
    .local v0, "uids":Lcom/android/server/am/ActiveUids;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    invoke-virtual {v0}, Lcom/android/server/am/ActiveUids;->size()I

    move-result v6

    if-ge v5, v6, :cond_8

    .line 154
    invoke-virtual {v0, v5}, Lcom/android/server/am/ActiveUids;->keyAt(I)I

    move-result v6

    .line 155
    .local v6, "uid":I
    invoke-virtual {v0, v5}, Lcom/android/server/am/ActiveUids;->valueAt(I)Lcom/android/server/am/UidRecord;

    move-result-object v7

    .line 156
    .local v7, "uidRecord":Lcom/android/server/am/UidRecord;
    invoke-virtual {v7}, Lcom/android/server/am/UidRecord;->getCurProcState()I

    move-result v8

    .line 157
    .local v8, "uidState":I
    const/4 v9, 0x0

    .line 158
    .local v9, "meminfo":Lcom/android/server/am/GameProcessKiller$PackageMemInfo;
    iget-object v10, v1, Lcom/android/server/am/GameMemoryReclaimer;->mProcessActions:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_7

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/android/server/am/IGameProcessAction;

    .line 159
    .local v11, "action":Lcom/android/server/am/IGameProcessAction;
    instance-of v12, v11, Lcom/android/server/am/GameProcessKiller;

    if-nez v12, :cond_1

    .line 160
    goto :goto_2

    .line 161
    :cond_1
    move-object v12, v11

    check-cast v12, Lcom/android/server/am/GameProcessKiller;

    .line 162
    .local v12, "killer":Lcom/android/server/am/GameProcessKiller;
    invoke-virtual {v12}, Lcom/android/server/am/GameProcessKiller;->getMinProcState()I

    move-result v13

    if-ge v8, v13, :cond_2

    .line 163
    goto :goto_2

    .line 164
    :cond_2
    invoke-virtual {v12, v6}, Lcom/android/server/am/GameProcessKiller;->shouldSkip(I)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 165
    goto :goto_2

    .line 168
    :cond_3
    new-instance v13, Lcom/android/server/am/GameMemoryReclaimer$UidPss;

    const/4 v14, 0x0

    invoke-direct {v13, v1, v14}, Lcom/android/server/am/GameMemoryReclaimer$UidPss;-><init>(Lcom/android/server/am/GameMemoryReclaimer;Lcom/android/server/am/GameMemoryReclaimer$UidPss-IA;)V

    .line 169
    .local v13, "uidPss":Lcom/android/server/am/GameMemoryReclaimer$UidPss;
    new-instance v14, Lcom/android/server/am/GameMemoryReclaimer$$ExternalSyntheticLambda1;

    invoke-direct {v14, v13, v12}, Lcom/android/server/am/GameMemoryReclaimer$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/am/GameMemoryReclaimer$UidPss;Lcom/android/server/am/GameProcessKiller;)V

    invoke-virtual {v7, v14}, Lcom/android/server/am/UidRecord;->forEachProcess(Ljava/util/function/Consumer;)V

    .line 174
    iget-boolean v14, v13, Lcom/android/server/am/GameMemoryReclaimer$UidPss;->skip:Z

    if-eqz v14, :cond_4

    .line 175
    goto :goto_2

    .line 176
    :cond_4
    if-nez v9, :cond_5

    .line 177
    new-instance v14, Lcom/android/server/am/GameProcessKiller$PackageMemInfo;

    move-object/from16 v16, v9

    move-object v15, v10

    .end local v9    # "meminfo":Lcom/android/server/am/GameProcessKiller$PackageMemInfo;
    .local v16, "meminfo":Lcom/android/server/am/GameProcessKiller$PackageMemInfo;
    iget-wide v9, v13, Lcom/android/server/am/GameMemoryReclaimer$UidPss;->pss:J

    invoke-direct {v14, v6, v9, v10, v8}, Lcom/android/server/am/GameProcessKiller$PackageMemInfo;-><init>(IJI)V

    move-object v9, v14

    .end local v16    # "meminfo":Lcom/android/server/am/GameProcessKiller$PackageMemInfo;
    .restart local v9    # "meminfo":Lcom/android/server/am/GameProcessKiller$PackageMemInfo;
    goto :goto_3

    .line 176
    :cond_5
    move-object/from16 v16, v9

    move-object v15, v10

    .line 178
    :goto_3
    iget-object v10, v1, Lcom/android/server/am/GameMemoryReclaimer;->mAllPackageInfos:Ljava/util/Map;

    invoke-interface {v10, v11}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 179
    iget-object v10, v1, Lcom/android/server/am/GameMemoryReclaimer;->mAllPackageInfos:Ljava/util/Map;

    invoke-interface {v10, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/List;

    invoke-interface {v10, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 181
    .end local v11    # "action":Lcom/android/server/am/IGameProcessAction;
    .end local v12    # "killer":Lcom/android/server/am/GameProcessKiller;
    .end local v13    # "uidPss":Lcom/android/server/am/GameMemoryReclaimer$UidPss;
    :cond_6
    move-object v10, v15

    goto :goto_2

    .line 153
    :cond_7
    move-object/from16 v16, v9

    .end local v9    # "meminfo":Lcom/android/server/am/GameProcessKiller$PackageMemInfo;
    .restart local v16    # "meminfo":Lcom/android/server/am/GameProcessKiller$PackageMemInfo;
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 183
    .end local v5    # "i":I
    .end local v6    # "uid":I
    .end local v7    # "uidRecord":Lcom/android/server/am/UidRecord;
    .end local v8    # "uidState":I
    .end local v16    # "meminfo":Lcom/android/server/am/GameProcessKiller$PackageMemInfo;
    :cond_8
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 184
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v2

    .line 185
    .end local v2    # "time":J
    .local v4, "time":J
    const-string v2, "GameMemoryReclaimer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "spent "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "ms to filter all packages..."

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    return-void

    .line 183
    .end local v0    # "uids":Lcom/android/server/am/ActiveUids;
    .end local v4    # "time":J
    .restart local v2    # "time":J
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private filterAllProcessInfos()V
    .locals 4

    .line 107
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 108
    .local v0, "time":J
    iget-object v2, p0, Lcom/android/server/am/GameMemoryReclaimer;->mAllProcessInfos:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 109
    .local v3, "v":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->clear()V

    .end local v3    # "v":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;>;"
    goto :goto_0

    .line 110
    :cond_0
    new-instance v2, Lcom/android/server/am/GameMemoryReclaimer$1;

    invoke-direct {v2, p0}, Lcom/android/server/am/GameMemoryReclaimer$1;-><init>(Lcom/android/server/am/GameMemoryReclaimer;)V

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/android/server/am/GameMemoryReclaimer;->getMatchedProcessList(Ljava/lang/Comparable;Ljava/util/List;)Ljava/util/List;

    .line 136
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    .line 137
    .end local v0    # "time":J
    .local v2, "time":J
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "spent "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ms to filter all processes("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/GameMemoryReclaimer;->mCompactInfos:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GameMemoryReclaimer"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    return-void
.end method

.method private getMatchedProcessList(Ljava/lang/Comparable;Ljava/util/List;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Comparable<",
            "Lcom/android/server/am/ProcessRecord;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/android/server/am/ProcessRecord;",
            ">;"
        }
    .end annotation

    .line 251
    .local p1, "condition":Ljava/lang/Comparable;, "Ljava/lang/Comparable<Lcom/android/server/am/ProcessRecord;>;"
    .local p2, "whitelist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 252
    .local v0, "procs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;"
    iget-object v1, p0, Lcom/android/server/am/GameMemoryReclaimer;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v1

    .line 253
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/GameMemoryReclaimer;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    invoke-virtual {v2}, Lcom/android/server/am/ProcessList;->getProcessNamesLOSP()Lcom/android/server/am/ProcessList$MyProcessMap;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/am/ProcessList$MyProcessMap;->getMap()Landroid/util/ArrayMap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/util/ArrayMap;->size()I

    move-result v2

    .line 254
    .local v2, "NP":I
    const/4 v3, 0x0

    .local v3, "ip":I
    :goto_0
    if-ge v3, v2, :cond_4

    .line 255
    iget-object v4, p0, Lcom/android/server/am/GameMemoryReclaimer;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    iget-object v4, v4, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    .line 256
    invoke-virtual {v4}, Lcom/android/server/am/ProcessList;->getProcessNamesLOSP()Lcom/android/server/am/ProcessList$MyProcessMap;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/server/am/ProcessList$MyProcessMap;->getMap()Landroid/util/ArrayMap;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/util/SparseArray;

    .line 257
    .local v4, "apps":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/am/ProcessRecord;>;"
    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v5

    .line 258
    .local v5, "NA":I
    const/4 v6, 0x0

    .local v6, "ia":I
    :goto_1
    if-ge v6, v5, :cond_3

    .line 259
    invoke-virtual {v4, v6}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/server/am/ProcessRecord;

    .line 260
    .local v7, "app":Lcom/android/server/am/ProcessRecord;
    invoke-virtual {v7}, Lcom/android/server/am/ProcessRecord;->isPersistent()Z

    move-result v8

    if-nez v8, :cond_2

    if-eqz p2, :cond_0

    iget-object v8, v7, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-interface {p2, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 261
    goto :goto_2

    .line 263
    :cond_0
    invoke-virtual {v7}, Lcom/android/server/am/ProcessRecord;->isRemoved()Z

    move-result v8

    if-nez v8, :cond_1

    invoke-interface {p1, v7}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v8

    if-nez v8, :cond_2

    .line 264
    :cond_1
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 258
    .end local v7    # "app":Lcom/android/server/am/ProcessRecord;
    :cond_2
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 254
    .end local v4    # "apps":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/am/ProcessRecord;>;"
    .end local v5    # "NA":I
    .end local v6    # "ia":I
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 268
    .end local v2    # "NP":I
    .end local v3    # "ip":I
    :cond_4
    monitor-exit v1

    .line 269
    return-object v0

    .line 268
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method static synthetic lambda$filterAllPackageInfos$0(Lcom/android/server/am/GameMemoryReclaimer$UidPss;Lcom/android/server/am/GameProcessKiller;Lcom/android/server/am/ProcessRecord;)V
    .locals 4
    .param p0, "uidPss"    # Lcom/android/server/am/GameMemoryReclaimer$UidPss;
    .param p1, "killer"    # Lcom/android/server/am/GameProcessKiller;
    .param p2, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 170
    iget-boolean v0, p0, Lcom/android/server/am/GameMemoryReclaimer$UidPss;->skip:Z

    invoke-virtual {p1, p2}, Lcom/android/server/am/GameProcessKiller;->shouldSkip(Lcom/android/server/am/ProcessRecord;)Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/android/server/am/GameMemoryReclaimer$UidPss;->skip:Z

    .line 171
    iget-boolean v0, p0, Lcom/android/server/am/GameMemoryReclaimer$UidPss;->skip:Z

    if-nez v0, :cond_0

    .line 172
    iget-wide v0, p0, Lcom/android/server/am/GameMemoryReclaimer$UidPss;->pss:J

    iget-object v2, p2, Lcom/android/server/am/ProcessRecord;->mProfile:Lcom/android/server/am/ProcessProfileRecord;

    invoke-virtual {v2}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/server/am/GameMemoryReclaimer$UidPss;->pss:J

    .line 173
    :cond_0
    return-void
.end method

.method private synthetic lambda$notifyProcessDied$2(I)V
    .locals 2
    .param p1, "pid"    # I

    .line 274
    iget-object v0, p0, Lcom/android/server/am/GameMemoryReclaimer;->mCompactInfos:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 275
    iget-object v0, p0, Lcom/android/server/am/GameMemoryReclaimer;->mCompactInfos:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;

    invoke-virtual {v0}, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->notifyDied()V

    .line 276
    :cond_0
    return-void
.end method

.method private synthetic lambda$reclaimBackground$1(J)V
    .locals 10
    .param p1, "need"    # J

    .line 190
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "reclaimBackground: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-wide/32 v1, 0x80000

    invoke-static {v1, v2, v0}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 191
    const-wide/16 v3, 0x0

    .line 192
    .local v3, "reclaim":J
    move-wide v5, p1

    .line 194
    .local v5, "mem":J
    iget-object v0, p0, Lcom/android/server/am/GameMemoryReclaimer;->mProcessActions:Ljava/util/List;

    monitor-enter v0

    .line 195
    :try_start_0
    iget-object v7, p0, Lcom/android/server/am/GameMemoryReclaimer;->mAllProcessInfos:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->size()I

    move-result v7

    if-lez v7, :cond_0

    .line 196
    const-string v7, "filterAllProcessInfos"

    invoke-static {v1, v2, v7}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 197
    invoke-direct {p0}, Lcom/android/server/am/GameMemoryReclaimer;->filterAllProcessInfos()V

    .line 198
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    .line 200
    :cond_0
    iget-object v7, p0, Lcom/android/server/am/GameMemoryReclaimer;->mAllPackageInfos:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->size()I

    move-result v7

    if-lez v7, :cond_1

    .line 201
    const-string v7, "filterAllPackageInfos"

    invoke-static {v1, v2, v7}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 202
    invoke-direct {p0}, Lcom/android/server/am/GameMemoryReclaimer;->filterAllPackageInfos()V

    .line 203
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    .line 206
    :cond_1
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    iget-object v8, p0, Lcom/android/server/am/GameMemoryReclaimer;->mProcessActions:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-ge v7, v8, :cond_3

    .line 207
    iget-object v8, p0, Lcom/android/server/am/GameMemoryReclaimer;->mProcessActions:Ljava/util/List;

    invoke-interface {v8, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/server/am/IGameProcessAction;

    invoke-interface {v8, v5, v6}, Lcom/android/server/am/IGameProcessAction;->doAction(J)J

    move-result-wide v8

    move-wide v3, v8

    .line 208
    cmp-long v8, v3, v5

    if-ltz v8, :cond_2

    .line 209
    goto :goto_1

    .line 210
    :cond_2
    sub-long/2addr v5, v3

    .line 206
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 212
    .end local v7    # "i":I
    :cond_3
    :goto_1
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 213
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    .line 214
    return-void

    .line 212
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method


# virtual methods
.method public addGameProcessCompactor(Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;)V
    .locals 4
    .param p1, "cfg"    # Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;

    .line 95
    if-eqz p1, :cond_0

    .line 96
    new-instance v0, Lcom/android/server/am/GameProcessCompactor;

    move-object v1, p1

    check-cast v1, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;

    invoke-direct {v0, p0, v1}, Lcom/android/server/am/GameProcessCompactor;-><init>(Lcom/android/server/am/GameMemoryReclaimer;Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;)V

    .line 97
    .local v0, "compactor":Lcom/android/server/am/GameProcessCompactor;
    iget-object v1, p0, Lcom/android/server/am/GameMemoryReclaimer;->mProcessActions:Ljava/util/List;

    monitor-enter v1

    .line 98
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/GameMemoryReclaimer;->mProcessActions:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    iget-object v2, p0, Lcom/android/server/am/GameMemoryReclaimer;->mAllProcessInfos:Ljava/util/Map;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 102
    .end local v0    # "compactor":Lcom/android/server/am/GameProcessCompactor;
    :cond_0
    :goto_0
    return-void
.end method

.method public addGameProcessKiller(Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;)V
    .locals 4
    .param p1, "cfg"    # Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;

    .line 85
    if-eqz p1, :cond_0

    .line 86
    new-instance v0, Lcom/android/server/am/GameProcessKiller;

    move-object v1, p1

    check-cast v1, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;

    invoke-direct {v0, p0, v1}, Lcom/android/server/am/GameProcessKiller;-><init>(Lcom/android/server/am/GameMemoryReclaimer;Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;)V

    .line 87
    .local v0, "killer":Lcom/android/server/am/GameProcessKiller;
    iget-object v1, p0, Lcom/android/server/am/GameMemoryReclaimer;->mProcessActions:Ljava/util/List;

    monitor-enter v1

    .line 88
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/GameMemoryReclaimer;->mProcessActions:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    iget-object v2, p0, Lcom/android/server/am/GameMemoryReclaimer;->mAllPackageInfos:Ljava/util/Map;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 92
    .end local v0    # "killer":Lcom/android/server/am/GameProcessKiller;
    :cond_0
    :goto_0
    return-void
.end method

.method filterPackageInfos(Lcom/android/server/am/IGameProcessAction;)Ljava/util/List;
    .locals 2
    .param p1, "killer"    # Lcom/android/server/am/IGameProcessAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/server/am/IGameProcessAction;",
            ")",
            "Ljava/util/List<",
            "Lcom/android/server/am/GameProcessKiller$PackageMemInfo;",
            ">;"
        }
    .end annotation

    .line 219
    iget-object v0, p0, Lcom/android/server/am/GameMemoryReclaimer;->mAllPackageInfos:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 220
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0

    .line 221
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/GameMemoryReclaimer;->mAllPackageInfos:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 222
    .local v0, "packageList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/GameProcessKiller$PackageMemInfo;>;"
    new-instance v1, Lcom/android/server/am/GameMemoryReclaimer$2;

    invoke-direct {v1, p0}, Lcom/android/server/am/GameMemoryReclaimer$2;-><init>(Lcom/android/server/am/GameMemoryReclaimer;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 230
    return-object v0
.end method

.method filterProcessInfos(Lcom/android/server/am/IGameProcessAction;)Ljava/util/List;
    .locals 1
    .param p1, "compactor"    # Lcom/android/server/am/IGameProcessAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/server/am/IGameProcessAction;",
            ")",
            "Ljava/util/List<",
            "Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;",
            ">;"
        }
    .end annotation

    .line 234
    iget-object v0, p0, Lcom/android/server/am/GameMemoryReclaimer;->mAllProcessInfos:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    iget-object v0, p0, Lcom/android/server/am/GameMemoryReclaimer;->mAllProcessInfos:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0

    .line 236
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method getPackageNameByUid(I)Ljava/lang/String;
    .locals 2
    .param p1, "uid"    # I

    .line 241
    iget-object v0, p0, Lcom/android/server/am/GameMemoryReclaimer;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v0

    .line 242
    .local v0, "pkgs":[Ljava/lang/String;
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 243
    const/4 v1, 0x0

    aget-object v1, v0, v1

    .local v1, "packageName":Ljava/lang/String;
    goto :goto_0

    .line 245
    .end local v1    # "packageName":Ljava/lang/String;
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 247
    .restart local v1    # "packageName":Ljava/lang/String;
    :goto_0
    return-object v1
.end method

.method public notifyGameBackground()V
    .locals 2

    .line 76
    iget-object v0, p0, Lcom/android/server/am/GameMemoryReclaimer;->mProcessActions:Ljava/util/List;

    monitor-enter v0

    .line 77
    const/4 v1, 0x0

    :try_start_0
    iput-object v1, p0, Lcom/android/server/am/GameMemoryReclaimer;->mCurrentGame:Ljava/lang/String;

    .line 78
    iget-object v1, p0, Lcom/android/server/am/GameMemoryReclaimer;->mProcessActions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 79
    iget-object v1, p0, Lcom/android/server/am/GameMemoryReclaimer;->mAllProcessInfos:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 80
    iget-object v1, p0, Lcom/android/server/am/GameMemoryReclaimer;->mAllPackageInfos:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 81
    monitor-exit v0

    .line 82
    return-void

    .line 81
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public notifyGameForeground(Ljava/lang/String;)V
    .locals 4
    .param p1, "game"    # Ljava/lang/String;

    .line 66
    iget-object v0, p0, Lcom/android/server/am/GameMemoryReclaimer;->mProcessActions:Ljava/util/List;

    monitor-enter v0

    .line 67
    :try_start_0
    iput-object p1, p0, Lcom/android/server/am/GameMemoryReclaimer;->mCurrentGame:Ljava/lang/String;

    .line 68
    iget-object v1, p0, Lcom/android/server/am/GameMemoryReclaimer;->mProcessActions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 69
    iget-object v1, p0, Lcom/android/server/am/GameMemoryReclaimer;->mAllProcessInfos:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 70
    iget-object v1, p0, Lcom/android/server/am/GameMemoryReclaimer;->mAllPackageInfos:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 71
    const-string v1, "GameMemoryReclaimer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "reclaim memory for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/GameMemoryReclaimer;->mCurrentGame:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    monitor-exit v0

    .line 73
    return-void

    .line 72
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public notifyProcessDied(I)V
    .locals 2
    .param p1, "pid"    # I

    .line 273
    iget-object v0, p0, Lcom/android/server/am/GameMemoryReclaimer;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/am/GameMemoryReclaimer$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0, p1}, Lcom/android/server/am/GameMemoryReclaimer$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/am/GameMemoryReclaimer;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 277
    return-void
.end method

.method public reclaimBackground(J)V
    .locals 2
    .param p1, "need"    # J

    .line 189
    iget-object v0, p0, Lcom/android/server/am/GameMemoryReclaimer;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/am/GameMemoryReclaimer$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/server/am/GameMemoryReclaimer$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/am/GameMemoryReclaimer;J)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 216
    return-void
.end method
