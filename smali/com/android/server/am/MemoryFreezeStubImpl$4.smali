.class Lcom/android/server/am/MemoryFreezeStubImpl$4;
.super Landroid/os/IVoldTaskListener$Stub;
.source "MemoryFreezeStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/MemoryFreezeStubImpl;->checkAndWriteBack()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/MemoryFreezeStubImpl;


# direct methods
.method constructor <init>(Lcom/android/server/am/MemoryFreezeStubImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/MemoryFreezeStubImpl;

    .line 457
    iput-object p1, p0, Lcom/android/server/am/MemoryFreezeStubImpl$4;->this$0:Lcom/android/server/am/MemoryFreezeStubImpl;

    invoke-direct {p0}, Landroid/os/IVoldTaskListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onFinished(ILandroid/os/PersistableBundle;)V
    .locals 2
    .param p1, "status"    # I
    .param p2, "extras"    # Landroid/os/PersistableBundle;

    .line 465
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "WB finished, count "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "MFZ_PAGE_COUNT"

    invoke-virtual {p2, v1}, Landroid/os/PersistableBundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MFZ"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$4;->this$0:Lcom/android/server/am/MemoryFreezeStubImpl;

    invoke-virtual {v0}, Lcom/android/server/am/MemoryFreezeStubImpl;->extmFlushFinished()V

    .line 467
    return-void
.end method

.method public onStatus(ILandroid/os/PersistableBundle;)V
    .locals 0
    .param p1, "status"    # I
    .param p2, "extras"    # Landroid/os/PersistableBundle;

    .line 461
    return-void
.end method
