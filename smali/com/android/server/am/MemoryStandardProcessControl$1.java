class com.android.server.am.MemoryStandardProcessControl$1 extends android.content.BroadcastReceiver {
	 /* .source "MemoryStandardProcessControl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/MemoryStandardProcessControl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.am.MemoryStandardProcessControl this$0; //synthetic
/* # direct methods */
 com.android.server.am.MemoryStandardProcessControl$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/MemoryStandardProcessControl; */
/* .line 794 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 797 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 798 */
/* .local v0, "action":Ljava/lang/String; */
final String v1 = "android.intent.action.SCREEN_ON"; // const-string v1, "android.intent.action.SCREEN_ON"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v2 = 2; // const/4 v2, 0x2
int v3 = 1; // const/4 v3, 0x1
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 799 */
	 v1 = this.this$0;
	 /* iput v3, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenState:I */
	 /* .line 800 */
	 v1 = this.this$0;
	 /* iget v1, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenOffStrategy:I */
	 /* if-ne v1, v3, :cond_1 */
	 /* .line 801 */
	 v1 = this.this$0;
	 v1 = this.mHandler;
	 (( com.android.server.am.MemoryStandardProcessControl$MyHandler ) v1 ).removeMessages ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->removeMessages(I)V
	 /* .line 803 */
} // :cond_0
final String v1 = "android.intent.action.SCREEN_OFF"; // const-string v1, "android.intent.action.SCREEN_OFF"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 804 */
	 v1 = this.this$0;
	 /* iput v2, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenState:I */
	 /* .line 805 */
	 v1 = this.this$0;
	 /* iget v1, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenOffStrategy:I */
	 /* if-ne v1, v3, :cond_1 */
	 /* .line 806 */
	 v1 = this.this$0;
	 v1 = this.mHandler;
	 int v2 = 3; // const/4 v2, 0x3
	 /* const-wide/32 v3, 0xea60 */
	 (( com.android.server.am.MemoryStandardProcessControl$MyHandler ) v1 ).sendEmptyMessageDelayed ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->sendEmptyMessageDelayed(IJ)Z
	 /* .line 809 */
} // :cond_1
} // :goto_0
return;
} // .end method
