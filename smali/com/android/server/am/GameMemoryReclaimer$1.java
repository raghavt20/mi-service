class com.android.server.am.GameMemoryReclaimer$1 implements java.lang.Comparable {
	 /* .source "GameMemoryReclaimer.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/am/GameMemoryReclaimer;->filterAllProcessInfos()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/Comparable<", */
/* "Lcom/android/server/am/ProcessRecord;", */
/* ">;" */
/* } */
} // .end annotation
/* # instance fields */
final com.android.server.am.GameMemoryReclaimer this$0; //synthetic
/* # direct methods */
 com.android.server.am.GameMemoryReclaimer$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/GameMemoryReclaimer; */
/* .line 111 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public Integer compareTo ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 8 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 114 */
int v0 = 1; // const/4 v0, 0x1
/* .line 116 */
/* .local v0, "skip":Z */
v1 = this.this$0;
com.android.server.am.GameMemoryReclaimer .-$$Nest$fgetmProcessActions ( v1 );
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_3
/* check-cast v2, Lcom/android/server/am/IGameProcessAction; */
/* .line 117 */
/* .local v2, "action":Lcom/android/server/am/IGameProcessAction; */
/* instance-of v3, v2, Lcom/android/server/am/GameProcessCompactor; */
/* if-nez v3, :cond_0 */
/* .line 118 */
/* .line 119 */
v3 = } // :cond_0
/* if-nez v3, :cond_2 */
v3 = this.this$0;
v3 = com.android.server.am.GameMemoryReclaimer .-$$Nest$fgetmAllProcessInfos ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 120 */
v3 = this.this$0;
v3 = this.mActivityManagerService;
/* monitor-enter v3 */
/* .line 121 */
try { // :try_start_0
v4 = this.this$0;
com.android.server.am.GameMemoryReclaimer .-$$Nest$fgetmCompactInfos ( v4 );
v5 = (( com.android.server.am.ProcessRecord ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I
v4 = java.lang.Integer .valueOf ( v5 );
/* if-nez v4, :cond_1 */
/* .line 122 */
v4 = this.this$0;
com.android.server.am.GameMemoryReclaimer .-$$Nest$fgetmCompactInfos ( v4 );
v5 = (( com.android.server.am.ProcessRecord ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I
java.lang.Integer .valueOf ( v5 );
/* new-instance v6, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo; */
/* .line 123 */
v7 = (( com.android.server.am.ProcessRecord ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I
/* invoke-direct {v6, v7}, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;-><init>(I)V */
/* .line 122 */
/* .line 125 */
} // :cond_1
v4 = this.this$0;
com.android.server.am.GameMemoryReclaimer .-$$Nest$fgetmCompactInfos ( v4 );
v5 = (( com.android.server.am.ProcessRecord ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I
java.lang.Integer .valueOf ( v5 );
/* check-cast v4, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo; */
/* .line 126 */
/* .local v4, "info":Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo; */
v5 = this.this$0;
com.android.server.am.GameMemoryReclaimer .-$$Nest$fgetmAllProcessInfos ( v5 );
/* check-cast v5, Ljava/util/List; */
/* .line 127 */
/* monitor-exit v3 */
/* .line 128 */
int v0 = 0; // const/4 v0, 0x0
/* .line 127 */
} // .end local v4 # "info":Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v3 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 130 */
} // .end local v2 # "action":Lcom/android/server/am/IGameProcessAction;
} // :cond_2
} // :goto_1
/* .line 131 */
} // :cond_3
} // .end method
public Integer compareTo ( java.lang.Object p0 ) { //bridge//synthethic
/* .locals 0 */
/* .line 111 */
/* check-cast p1, Lcom/android/server/am/ProcessRecord; */
p1 = (( com.android.server.am.GameMemoryReclaimer$1 ) p0 ).compareTo ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/GameMemoryReclaimer$1;->compareTo(Lcom/android/server/am/ProcessRecord;)I
} // .end method
