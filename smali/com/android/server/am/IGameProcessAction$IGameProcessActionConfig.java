public abstract class com.android.server.am.IGameProcessAction$IGameProcessActionConfig {
	 /* .source "IGameProcessAction.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/IGameProcessAction; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x609 */
/* name = "IGameProcessActionConfig" */
} // .end annotation
/* # virtual methods */
public abstract void addWhiteList ( java.util.List p0, Boolean p1 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;Z)V" */
/* } */
} // .end annotation
} // .end method
public abstract Integer getPrio ( ) {
} // .end method
public abstract void initFromJSON ( org.json.JSONObject p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Lorg/json/JSONException; */
/* } */
} // .end annotation
} // .end method
