class com.android.server.am.AppStateManager$AppState$RunningProcess$AppPowerResourceCallback implements com.miui.server.smartpower.AppPowerResource$IAppPowerResourceCallback {
	 /* .source "AppStateManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "AppPowerResourceCallback" */
} // .end annotation
/* # instance fields */
final com.android.server.am.AppStateManager$AppState$RunningProcess this$2; //synthetic
/* # direct methods */
private com.android.server.am.AppStateManager$AppState$RunningProcess$AppPowerResourceCallback ( ) {
/* .locals 0 */
/* .line 2944 */
this.this$2 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
 com.android.server.am.AppStateManager$AppState$RunningProcess$AppPowerResourceCallback ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;-><init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V */
return;
} // .end method
/* # virtual methods */
public void noteResourceActive ( Integer p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "type" # I */
/* .param p2, "behavier" # I */
/* .line 2948 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "resource active uid:"; // const-string v1, "resource active uid:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.this$2;
v1 = com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmUid ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " pid:"; // const-string v1, " pid:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.this$2;
v1 = com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmPid ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 2950 */
com.miui.server.smartpower.AppPowerResourceManager .typeToString ( p1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2951 */
/* .local v0, "reason":Ljava/lang/String; */
/* sget-boolean v1, Lcom/android/server/am/AppStateManager;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 2952 */
	 v1 = this.this$2;
	 com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetLOG_TAG ( v1 );
	 android.util.Slog .d ( v1,v0 );
	 /* .line 2954 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
/* if-ne p1, v1, :cond_1 */
/* .line 2955 */
v2 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fputmAudioActive ( v2,v1 );
/* .line 2956 */
v1 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fputmAudioBehavier ( v1,p2 );
/* .line 2957 */
} // :cond_1
int v2 = 3; // const/4 v2, 0x3
/* if-ne p1, v2, :cond_2 */
/* .line 2958 */
v2 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fputmGpsActive ( v2,v1 );
/* .line 2959 */
v1 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fputmGpsBehavier ( v1,p2 );
/* .line 2960 */
} // :cond_2
int v2 = 2; // const/4 v2, 0x2
/* if-ne p1, v2, :cond_3 */
/* .line 2961 */
v2 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fputmNetActive ( v2,v1 );
/* .line 2962 */
v1 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fputmNetBehavier ( v1,p2 );
/* .line 2963 */
} // :cond_3
int v2 = 5; // const/4 v2, 0x5
/* if-ne p1, v2, :cond_4 */
/* .line 2964 */
v2 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fputmBleActive ( v2,v1 );
/* .line 2965 */
} // :cond_4
int v2 = 6; // const/4 v2, 0x6
/* if-ne p1, v2, :cond_5 */
/* .line 2966 */
v2 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fputmCameraActive ( v2,v1 );
/* .line 2967 */
v1 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fputmCamBehavier ( v1,p2 );
/* .line 2968 */
} // :cond_5
int v2 = 7; // const/4 v2, 0x7
/* if-ne p1, v2, :cond_6 */
/* .line 2969 */
v2 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fputmDisplayActive ( v2,v1 );
/* .line 2970 */
v1 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fputmDisplayBehavier ( v1,p2 );
/* .line 2972 */
} // :cond_6
} // :goto_0
v1 = this.this$2;
v1 = this.this$1;
com.android.server.am.AppStateManager$AppState .-$$Nest$mupdateCurrentResourceBehavier ( v1 );
/* .line 2973 */
v1 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$mbecomeActiveIfNeeded ( v1,v0 );
/* .line 2974 */
return;
} // .end method
public void noteResourceInactive ( Integer p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "type" # I */
/* .param p2, "behavier" # I */
/* .line 2978 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "resource inactive uid:"; // const-string v1, "resource inactive uid:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.this$2;
v1 = com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmUid ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " pid:"; // const-string v1, " pid:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.this$2;
v1 = com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmPid ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 2980 */
com.miui.server.smartpower.AppPowerResourceManager .typeToString ( p1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2981 */
/* .local v0, "reason":Ljava/lang/String; */
/* sget-boolean v1, Lcom/android/server/am/AppStateManager;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 2982 */
v1 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetLOG_TAG ( v1 );
android.util.Slog .d ( v1,v0 );
/* .line 2984 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
int v2 = 0; // const/4 v2, 0x0
/* if-ne p1, v1, :cond_1 */
/* .line 2985 */
v1 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fputmAudioActive ( v1,v2 );
/* .line 2986 */
v1 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fputmAudioBehavier ( v1,v2 );
/* .line 2987 */
} // :cond_1
int v1 = 3; // const/4 v1, 0x3
/* if-ne p1, v1, :cond_2 */
/* .line 2988 */
v1 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fputmGpsActive ( v1,v2 );
/* .line 2989 */
v1 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fputmGpsBehavier ( v1,v2 );
/* .line 2990 */
} // :cond_2
int v1 = 5; // const/4 v1, 0x5
/* if-ne p1, v1, :cond_3 */
/* .line 2991 */
v1 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fputmBleActive ( v1,v2 );
/* .line 2992 */
} // :cond_3
int v1 = 2; // const/4 v1, 0x2
/* if-ne p1, v1, :cond_4 */
/* .line 2993 */
v1 = this.this$2;
v1 = this.this$1;
com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmHandler ( v1 );
/* new-instance v3, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback$1; */
/* invoke-direct {v3, p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback$1;-><init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;)V */
(( com.android.server.am.AppStateManager$AppState$MyHandler ) v1 ).post ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->post(Ljava/lang/Runnable;)Z
/* .line 3001 */
v1 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fputmNetActive ( v1,v2 );
/* .line 3002 */
v1 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fputmNetBehavier ( v1,v2 );
/* .line 3003 */
} // :cond_4
int v1 = 6; // const/4 v1, 0x6
/* if-ne p1, v1, :cond_5 */
/* .line 3004 */
v1 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fputmCameraActive ( v1,v2 );
/* .line 3005 */
v1 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fputmCamBehavier ( v1,v2 );
/* .line 3006 */
} // :cond_5
int v1 = 7; // const/4 v1, 0x7
/* if-ne p1, v1, :cond_6 */
/* .line 3007 */
v1 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fputmDisplayActive ( v1,v2 );
/* .line 3008 */
v1 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fputmDisplayBehavier ( v1,v2 );
/* .line 3010 */
} // :cond_6
} // :goto_0
v1 = this.this$2;
v1 = this.this$1;
com.android.server.am.AppStateManager$AppState .-$$Nest$mupdateCurrentResourceBehavier ( v1 );
/* .line 3011 */
v1 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$msendBecomeInactiveMsg ( v1,v0 );
/* .line 3012 */
return;
} // .end method
