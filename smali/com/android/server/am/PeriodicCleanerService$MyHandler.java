class com.android.server.am.PeriodicCleanerService$MyHandler extends android.os.Handler {
	 /* .source "PeriodicCleanerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/PeriodicCleanerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "MyHandler" */
} // .end annotation
/* # instance fields */
final com.android.server.am.PeriodicCleanerService this$0; //synthetic
/* # direct methods */
public com.android.server.am.PeriodicCleanerService$MyHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 1921 */
this.this$0 = p1;
/* .line 1922 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 1923 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 4 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 1927 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* :pswitch_0 */
/* .line 1947 */
/* :pswitch_1 */
v0 = this.this$0;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
/* iget v2, p1, Landroid/os/Message;->arg2:I */
v3 = this.obj;
/* check-cast v3, Ljava/lang/String; */
com.android.server.am.PeriodicCleanerService .-$$Nest$mreportCleanProcess ( v0,v1,v2,v3 );
/* .line 1948 */
/* .line 1943 */
/* :pswitch_2 */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Ljava/lang/String; */
com.android.server.am.PeriodicCleanerService .-$$Nest$mreportStartProcess ( v0,v1 );
/* .line 1944 */
/* .line 1939 */
/* :pswitch_3 */
v0 = this.this$0;
com.android.server.am.PeriodicCleanerService .-$$Nest$mhandleScreenOff ( v0 );
/* .line 1940 */
/* .line 1935 */
/* :pswitch_4 */
v0 = this.this$0;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
com.android.server.am.PeriodicCleanerService .-$$Nest$mreportMemPressure ( v0,v1 );
/* .line 1936 */
/* .line 1929 */
/* :pswitch_5 */
v0 = this.obj;
/* instance-of v0, v0, Lcom/android/server/am/PeriodicCleanerService$MyEvent; */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 1930 */
	 v0 = this.this$0;
	 v1 = this.obj;
	 /* check-cast v1, Lcom/android/server/am/PeriodicCleanerService$MyEvent; */
	 com.android.server.am.PeriodicCleanerService .-$$Nest$mreportEvent ( v0,v1 );
	 /* .line 1953 */
} // :cond_0
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_0 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
