public abstract class com.android.server.am.MiuiMemoryServiceInternal {
	 /* .source "MiuiMemoryServiceInternal.java" */
	 /* # static fields */
	 public static final Integer COMPACT_MODE_CAMERA;
	 public static final Integer COMPACT_MODE_LMKD;
	 public static final Integer COMPACT_MODE_PMC;
	 public static final Integer COMPACT_MODE_SCREENOFF_CHARGING;
	 public static final Integer COMPACT_MODE_SPTM;
	 public static final Integer VMPRESS_LEVEL_CRITICAL;
	 public static final Integer VMPRESS_LEVEL_LOW;
	 public static final Integer VMPRESS_LEVEL_MEDIUM;
	 public static final Integer VMPRESS_LEVEL_SUPER_CRITICAL;
	 /* # virtual methods */
	 public abstract void interruptProcCompaction ( Integer p0 ) {
	 } // .end method
	 public abstract void interruptProcsCompaction ( ) {
	 } // .end method
	 public abstract Boolean isCompactNeeded ( com.miui.server.smartpower.IAppState$IRunningProcess p0, Integer p1 ) {
	 } // .end method
	 public abstract void performCompaction ( java.lang.String p0, Integer p1 ) {
	 } // .end method
	 public abstract void runGlobalCompaction ( Integer p0 ) {
	 } // .end method
	 public abstract void runProcCompaction ( com.miui.server.smartpower.IAppState$IRunningProcess p0, Integer p1 ) {
	 } // .end method
	 public abstract void runProcsCompaction ( Integer p0 ) {
	 } // .end method
	 public abstract void setAppStartingMode ( Boolean p0 ) {
	 } // .end method
	 public abstract void writeLmkd ( Boolean p0 ) {
	 } // .end method
