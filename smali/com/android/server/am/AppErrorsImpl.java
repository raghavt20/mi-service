public class com.android.server.am.AppErrorsImpl extends com.android.server.am.AppErrorsStub {
	 /* .source "AppErrorsImpl.java" */
	 /* # direct methods */
	 public com.android.server.am.AppErrorsImpl ( ) {
		 /* .locals 0 */
		 /* .line 8 */
		 /* invoke-direct {p0}, Lcom/android/server/am/AppErrorsStub;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void handleShowAppErrorUI ( android.content.Context p0, com.android.server.am.ProcessErrorStateRecord p1, com.android.server.am.AppErrorDialog$Data p2 ) {
		 /* .locals 2 */
		 /* .param p1, "ctx" # Landroid/content/Context; */
		 /* .param p2, "errState" # Lcom/android/server/am/ProcessErrorStateRecord; */
		 /* .param p3, "data" # Lcom/android/server/am/AppErrorDialog$Data; */
		 /* .line 13 */
		 v0 = 		 android.provider.MiuiSettings$Secure .isForceCloseDialogEnabled ( p1 );
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 14 */
			 (( com.android.server.am.ProcessErrorStateRecord ) p2 ).getDialogController ( ); // invoke-virtual {p2}, Lcom/android/server/am/ProcessErrorStateRecord;->getDialogController()Lcom/android/server/am/ErrorDialogController;
			 (( com.android.server.am.ErrorDialogController ) v0 ).showCrashDialogs ( p3 ); // invoke-virtual {v0, p3}, Lcom/android/server/am/ErrorDialogController;->showCrashDialogs(Lcom/android/server/am/AppErrorDialog$Data;)V
			 /* .line 15 */
			 return;
			 /* .line 17 */
		 } // :cond_0
		 v0 = this.result;
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 /* .line 18 */
			 v0 = this.result;
			 (( com.android.server.am.AppErrorResult ) v0 ).set ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/AppErrorResult;->set(I)V
			 /* .line 20 */
		 } // :cond_1
		 return;
	 } // .end method
