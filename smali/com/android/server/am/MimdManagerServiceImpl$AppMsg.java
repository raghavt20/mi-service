class com.android.server.am.MimdManagerServiceImpl$AppMsg {
	 /* .source "MimdManagerServiceImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/MimdManagerServiceImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "AppMsg" */
} // .end annotation
/* # instance fields */
Integer mAppIdx;
Integer mPid;
Integer mUid;
private final java.lang.Object mUpdatePid;
final com.android.server.am.MimdManagerServiceImpl this$0; //synthetic
/* # direct methods */
public com.android.server.am.MimdManagerServiceImpl$AppMsg ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/android/server/am/MimdManagerServiceImpl; */
/* .param p2, "idx" # I */
/* .param p3, "pid" # I */
/* .line 86 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 84 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mUpdatePid = v0;
/* .line 87 */
/* iput p2, p0, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;->mAppIdx:I */
/* .line 88 */
/* iput p3, p0, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;->mPid:I */
/* .line 89 */
return;
} // .end method
/* # virtual methods */
public void UpdatePid ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "pid" # I */
/* .line 92 */
v0 = this.mUpdatePid;
/* monitor-enter v0 */
/* .line 93 */
try { // :try_start_0
	 /* iput p1, p0, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;->mPid:I */
	 /* .line 94 */
	 /* monitor-exit v0 */
	 /* .line 95 */
	 return;
	 /* .line 94 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
} // .end method
