.class public Lcom/android/server/am/MiuiMemReclaimer;
.super Ljava/lang/Object;
.source "MiuiMemReclaimer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/MiuiMemReclaimer$CompactorHandler;,
        Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;
    }
.end annotation


# static fields
.field public static final ANON_RSS_LIMIT_KB:J

.field private static final COMPACTION_PROC_NAME_CAMERA:Ljava/lang/String; = "com.android.camera"

.field private static final COMPACTION_PROC_NAME_WALLPAPER:Ljava/lang/String; = "com.miui.miwallpaper"

.field private static final COMPACTION_PROC_NAME_WECHAT:Ljava/lang/String; = "com.tencent.mm"

.field private static final COMPACTION_PROC_NAME_WHITE_LIST:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final COMPACT_ACTION_ANON:Ljava/lang/String; = "anon"

.field private static final COMPACT_ACTION_FILE:Ljava/lang/String; = "file"

.field private static final COMPACT_ACTION_FULL:Ljava/lang/String; = "all"

.field private static final COMPACT_ALL_MIN_INTERVAL:J

.field private static final COMPACT_GLOBALLY_MSG:I = 0x64

.field private static final COMPACT_PROCESSES_MSG:I = 0x66

.field private static final COMPACT_PROCESS_MSG:I = 0x65

.field private static final COMPACT_PROC_MIN_INTERVAL:J

.field private static final EVENT_TAG:I = 0x13ba0

.field private static final FILE_RSS_LIMIT_KB:J

.field private static final RECLAIM_EVENT_NODE:Ljava/lang/String; = "/sys/kernel/mi_reclaim/event"

.field private static final RECLAIM_IF_NEEDED:Z

.field private static final TAG:Ljava/lang/String; = "MiuiMemoryService"

.field private static final TOTAL_MEMORY:J

.field private static final USE_LEGACY_COMPACTION:Z

.field private static final VMPRESS_CRITICAL_RECLAIM_SIZE_MB:I = 0x64

.field private static final VMPRESS_LOW_RECLAIM_SIZE_MB:I = 0x1e

.field private static final VMPRESS_MEDIUM_RECLAIM_SIZE_MB:I = 0x32

.field private static final VM_ANON:I = 0x2

.field private static final VM_FILE:I = 0x1

.field private static final VM_RSS:I = 0x0

.field private static final VM_SWAP:I = 0x3

.field private static volatile sPms:Lcom/android/server/am/ProcessManagerService;


# instance fields
.field private DEBUG:Z

.field private final MAX_COMPACTION_COUNT:I

.field private mBatteryLow:Z

.field private final mBatteryReceiver:Landroid/content/BroadcastReceiver;

.field private final mCompactionStats:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mCompactorHandler:Landroid/os/Handler;

.field private final mCompactorThread:Landroid/os/HandlerThread;

.field private mContext:Landroid/content/Context;

.field private mIntentFilter:Landroid/content/IntentFilter;

.field private mInterruptNeeded:Z

.field private mLastCompactionTimeMillis:J

.field private mProcessDependencies:Lcom/android/server/am/CachedAppOptimizer$DefaultProcessDependencies;

.field private mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

.field private mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;


# direct methods
.method static bridge synthetic -$$Nest$fgetMAX_COMPACTION_COUNT(Lcom/android/server/am/MiuiMemReclaimer;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/MiuiMemReclaimer;->MAX_COMPACTION_COUNT:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmBatteryLow(Lcom/android/server/am/MiuiMemReclaimer;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/am/MiuiMemReclaimer;->mBatteryLow:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mperformCompactProcess(Lcom/android/server/am/MiuiMemReclaimer;Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/am/MiuiMemReclaimer;->performCompactProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mperformCompactProcesses(Lcom/android/server/am/MiuiMemReclaimer;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/MiuiMemReclaimer;->performCompactProcesses(I)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 4

    .line 55
    invoke-static {}, Landroid/os/Process;->getTotalMemory()J

    move-result-wide v0

    const/16 v2, 0x1e

    shr-long/2addr v0, v2

    sput-wide v0, Lcom/android/server/am/MiuiMemReclaimer;->TOTAL_MEMORY:J

    .line 59
    const-wide/16 v2, 0x8

    cmp-long v0, v0, v2

    const/4 v1, 0x0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/android/server/am/MiuiMemReclaimer;->RECLAIM_IF_NEEDED:Z

    .line 63
    nop

    .line 64
    const-string v0, "persist.sys.mms.use_legacy"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/am/MiuiMemReclaimer;->USE_LEGACY_COMPACTION:Z

    .line 66
    nop

    .line 67
    const-string v0, "persist.sys.mms.anon_rss"

    const-wide/16 v1, 0x1388

    invoke-static {v0, v1, v2}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    sput-wide v0, Lcom/android/server/am/MiuiMemReclaimer;->ANON_RSS_LIMIT_KB:J

    .line 68
    nop

    .line 69
    const-string v0, "persist.sys.mms.file_rss"

    const-wide/16 v1, 0x4e20

    invoke-static {v0, v1, v2}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    sput-wide v0, Lcom/android/server/am/MiuiMemReclaimer;->FILE_RSS_LIMIT_KB:J

    .line 71
    const-string v0, "persist.sys.mms.compact_min_interval"

    const-wide/32 v1, 0xea60

    invoke-static {v0, v1, v2}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    sput-wide v0, Lcom/android/server/am/MiuiMemReclaimer;->COMPACT_ALL_MIN_INTERVAL:J

    .line 73
    const-string v0, "persist.sys.mms.compact_proc_min_interval"

    const-wide/16 v1, 0x2710

    invoke-static {v0, v1, v2}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    sput-wide v0, Lcom/android/server/am/MiuiMemReclaimer;->COMPACT_PROC_MIN_INTERVAL:J

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/am/MiuiMemReclaimer;->COMPACTION_PROC_NAME_WHITE_LIST:Ljava/util/ArrayList;

    .line 81
    const-string v1, "com.miui.home"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 82
    const-string v1, "com.android.systemui"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 83
    const-string v1, "com.miui.screenrecorder"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    sget-boolean v0, Lcom/android/server/am/MiuiMemoryService;->DEBUG:Z

    iput-boolean v0, p0, Lcom/android/server/am/MiuiMemReclaimer;->DEBUG:Z

    .line 108
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/am/MiuiMemReclaimer;->mInterruptNeeded:Z

    .line 112
    new-instance v1, Lcom/android/server/am/CachedAppOptimizer$DefaultProcessDependencies;

    invoke-direct {v1}, Lcom/android/server/am/CachedAppOptimizer$DefaultProcessDependencies;-><init>()V

    iput-object v1, p0, Lcom/android/server/am/MiuiMemReclaimer;->mProcessDependencies:Lcom/android/server/am/CachedAppOptimizer$DefaultProcessDependencies;

    .line 119
    iget-boolean v1, p0, Lcom/android/server/am/MiuiMemReclaimer;->DEBUG:Z

    if-eqz v1, :cond_0

    const/16 v1, 0x3e8

    goto :goto_0

    :cond_0
    const/16 v1, 0x12c

    :goto_0
    iput v1, p0, Lcom/android/server/am/MiuiMemReclaimer;->MAX_COMPACTION_COUNT:I

    .line 121
    new-instance v1, Lcom/android/server/am/MiuiMemReclaimer$1;

    invoke-direct {v1, p0}, Lcom/android/server/am/MiuiMemReclaimer$1;-><init>(Lcom/android/server/am/MiuiMemReclaimer;)V

    iput-object v1, p0, Lcom/android/server/am/MiuiMemReclaimer;->mCompactionStats:Ljava/util/Map;

    .line 155
    new-instance v1, Lcom/android/server/am/MiuiMemReclaimer$2;

    invoke-direct {v1, p0}, Lcom/android/server/am/MiuiMemReclaimer$2;-><init>(Lcom/android/server/am/MiuiMemReclaimer;)V

    iput-object v1, p0, Lcom/android/server/am/MiuiMemReclaimer;->mBatteryReceiver:Landroid/content/BroadcastReceiver;

    .line 165
    new-instance v2, Landroid/os/HandlerThread;

    const-string v3, "MiuiMemoryService_Compactor"

    invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/android/server/am/MiuiMemReclaimer;->mCompactorThread:Landroid/os/HandlerThread;

    .line 166
    invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V

    .line 167
    new-instance v3, Lcom/android/server/am/MiuiMemReclaimer$CompactorHandler;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v3, p0, v4}, Lcom/android/server/am/MiuiMemReclaimer$CompactorHandler;-><init>(Lcom/android/server/am/MiuiMemReclaimer;Landroid/os/Looper;)V

    iput-object v3, p0, Lcom/android/server/am/MiuiMemReclaimer;->mCompactorHandler:Landroid/os/Handler;

    .line 168
    nop

    .line 169
    invoke-virtual {v2}, Landroid/os/HandlerThread;->getThreadId()I

    move-result v2

    .line 168
    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/os/Process;->setThreadGroupAndCpuset(II)V

    .line 170
    const-class v2, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-static {v2}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    iput-object v2, p0, Lcom/android/server/am/MiuiMemReclaimer;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    .line 171
    iput-object p1, p0, Lcom/android/server/am/MiuiMemReclaimer;->mContext:Landroid/content/Context;

    .line 172
    iput-boolean v0, p0, Lcom/android/server/am/MiuiMemReclaimer;->mBatteryLow:Z

    .line 173
    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/am/MiuiMemReclaimer;->mIntentFilter:Landroid/content/IntentFilter;

    .line 174
    iget-object v2, p0, Lcom/android/server/am/MiuiMemReclaimer;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 175
    return-void
.end method

.method public static cancelSt()V
    .locals 2

    .line 193
    sget-boolean v0, Lcom/android/server/am/MiuiMemReclaimer;->RECLAIM_IF_NEEDED:Z

    if-eqz v0, :cond_0

    .line 194
    const-string v0, "/sys/kernel/mi_reclaim/event"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/am/MiuiMemReclaimer;->writeToNode(Ljava/lang/String;I)V

    .line 195
    :cond_0
    return-void
.end method

.method public static enterSt()V
    .locals 2

    .line 188
    sget-boolean v0, Lcom/android/server/am/MiuiMemReclaimer;->RECLAIM_IF_NEEDED:Z

    if-eqz v0, :cond_0

    .line 189
    const-string v0, "/sys/kernel/mi_reclaim/event"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/server/am/MiuiMemReclaimer;->writeToNode(Ljava/lang/String;I)V

    .line 190
    :cond_0
    return-void
.end method

.method private filterCompactionProcs(I)Ljava/util/List;
    .locals 8
    .param p1, "mode"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;",
            ">;"
        }
    .end annotation

    .line 406
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 407
    .local v0, "targetProcs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;>;"
    iget-object v1, p0, Lcom/android/server/am/MiuiMemReclaimer;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-interface {v1}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->getAllAppState()Ljava/util/ArrayList;

    move-result-object v1

    .line 408
    .local v1, "appStateList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/smartpower/IAppState;

    .line 409
    .local v3, "appState":Lcom/miui/server/smartpower/IAppState;
    invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState;->isVsible()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 410
    goto :goto_0

    .line 412
    :cond_0
    invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState;->getRunningProcessList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 413
    .local v5, "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I

    move-result v6

    if-lez v6, :cond_1

    .line 414
    invoke-virtual {p0, v5, p1}, Lcom/android/server/am/MiuiMemReclaimer;->isCompactNeeded(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 415
    new-instance v6, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;

    invoke-direct {v6, v5, p1}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;-><init>(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)V

    .line 416
    .local v6, "info":Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;
    invoke-direct {p0, v6}, Lcom/android/server/am/MiuiMemReclaimer;->isCompactSatisfied(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 418
    .end local v5    # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .end local v6    # "info":Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;
    :cond_1
    goto :goto_1

    .line 419
    .end local v3    # "appState":Lcom/miui/server/smartpower/IAppState;
    :cond_2
    goto :goto_0

    .line 420
    :cond_3
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 421
    const-string v2, "all"

    invoke-direct {p0, v0, v2}, Lcom/android/server/am/MiuiMemReclaimer;->sortProcsByRss(Ljava/util/List;Ljava/lang/String;)V

    .line 422
    invoke-direct {p0, v0, v2}, Lcom/android/server/am/MiuiMemReclaimer;->logTargetProcsDetails(Ljava/util/List;Ljava/lang/String;)V

    .line 425
    :cond_4
    return-object v0
.end method

.method private generateMessage(IILjava/lang/Object;)Landroid/os/Message;
    .locals 2
    .param p1, "what"    # I
    .param p2, "arg1"    # I
    .param p3, "obj"    # Ljava/lang/Object;

    .line 291
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 292
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    .line 293
    const/4 v1, -0x1

    if-eq p2, v1, :cond_0

    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 294
    :cond_0
    if-eqz p3, :cond_1

    iput-object p3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 295
    :cond_1
    return-object v0
.end method

.method private getActiveUidSet()Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 429
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 430
    .local v0, "activeUidSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    iget-object v1, p0, Lcom/android/server/am/MiuiMemReclaimer;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    if-nez v1, :cond_1

    .line 431
    invoke-static {}, Lcom/android/server/am/MiuiMemReclaimer;->getProcessManagerService()Lcom/android/server/am/ProcessManagerService;

    move-result-object v1

    if-nez v1, :cond_0

    .line 432
    return-object v0

    .line 434
    :cond_0
    invoke-static {}, Lcom/android/server/am/MiuiMemReclaimer;->getProcessManagerService()Lcom/android/server/am/ProcessManagerService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/am/ProcessManagerService;->getProcessPolicy()Lcom/android/server/am/ProcessPolicy;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/am/MiuiMemReclaimer;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    .line 436
    :cond_1
    iget-object v1, p0, Lcom/android/server/am/MiuiMemReclaimer;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    .line 437
    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessPolicy;->getActiveUidRecordList(I)Ljava/util/List;

    move-result-object v1

    .line 438
    .local v1, "activeUidRecords":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;

    .line 439
    .local v3, "record":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;
    iget v4, v3, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->uid:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 440
    .end local v3    # "record":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;
    goto :goto_0

    .line 441
    :cond_2
    return-object v0
.end method

.method private static getProcessManagerService()Lcom/android/server/am/ProcessManagerService;
    .locals 1

    .line 445
    sget-object v0, Lcom/android/server/am/MiuiMemReclaimer;->sPms:Lcom/android/server/am/ProcessManagerService;

    if-nez v0, :cond_0

    .line 446
    const-string v0, "ProcessManager"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/ProcessManagerService;

    sput-object v0, Lcom/android/server/am/MiuiMemReclaimer;->sPms:Lcom/android/server/am/ProcessManagerService;

    .line 448
    :cond_0
    sget-object v0, Lcom/android/server/am/MiuiMemReclaimer;->sPms:Lcom/android/server/am/ProcessManagerService;

    return-object v0
.end method

.method private isAdjInCompactRange(II)Z
    .locals 2
    .param p1, "adj"    # I
    .param p2, "mode"    # I

    .line 535
    const/16 v0, 0x64

    const/4 v1, 0x0

    packed-switch p2, :pswitch_data_0

    .line 559
    :pswitch_0
    if-ge p1, v0, :cond_3

    .line 560
    return v1

    .line 553
    :pswitch_1
    if-lt p1, v0, :cond_0

    const/16 v0, 0x3b6

    if-le p1, v0, :cond_3

    .line 555
    :cond_0
    return v1

    .line 546
    :pswitch_2
    const/16 v0, 0xc8

    if-le p1, v0, :cond_1

    const/16 v0, 0x320

    if-lt p1, v0, :cond_3

    .line 547
    :cond_1
    return v1

    .line 539
    :pswitch_3
    if-le p1, v0, :cond_2

    const/16 v0, 0x384

    if-lt p1, v0, :cond_3

    .line 540
    :cond_2
    return v1

    .line 564
    :cond_3
    const/4 v0, 0x1

    return v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private isCameraMode()Z
    .locals 2

    .line 485
    nop

    .line 486
    invoke-static {}, Lcom/android/server/am/SystemPressureController;->getInstance()Lcom/android/server/am/SystemPressureController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/am/SystemPressureController;->getForegroundPackageName()Ljava/lang/String;

    move-result-object v0

    .line 485
    const-string v1, "com.android.camera"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private isCompactSatisfied(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)Z
    .locals 14
    .param p1, "info"    # Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;

    .line 568
    invoke-static {p1}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->-$$Nest$mgetPid(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)I

    move-result v0

    const-string v1, "Skipping compaction for process "

    const-string v2, "MiuiMemoryService"

    const/4 v3, 0x0

    if-eqz v0, :cond_3

    invoke-static {p1}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->-$$Nest$misRssValid(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_3

    .line 576
    :cond_0
    iget-object v0, p1, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rss:[J

    const/4 v4, 0x1

    aget-wide v5, v0, v4

    .line 577
    .local v5, "rssFile":J
    iget-object v0, p1, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rss:[J

    const/4 v7, 0x2

    aget-wide v8, v0, v7

    .line 578
    .local v8, "rssAnon":J
    iget-object v0, p1, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->action:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_1
    goto :goto_0

    :sswitch_0
    const-string v7, "file"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v7, v4

    goto :goto_1

    :sswitch_1
    const-string v7, "anon"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v7, v3

    goto :goto_1

    :sswitch_2
    const-string v10, "all"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    :goto_0
    const/4 v7, -0x1

    :goto_1
    const-string v0, " KB"

    const-wide/16 v10, 0x0

    packed-switch v7, :pswitch_data_0

    goto/16 :goto_2

    .line 594
    :pswitch_0
    sget-wide v12, Lcom/android/server/am/MiuiMemReclaimer;->FILE_RSS_LIMIT_KB:J

    cmp-long v7, v12, v10

    if-lez v7, :cond_2

    cmp-long v7, v5, v12

    if-gez v7, :cond_2

    sget-wide v12, Lcom/android/server/am/MiuiMemReclaimer;->ANON_RSS_LIMIT_KB:J

    cmp-long v7, v12, v10

    if-lez v7, :cond_2

    cmp-long v7, v8, v12

    if-gez v7, :cond_2

    .line 596
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->-$$Nest$mgetPid(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", full RSS is too small "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 598
    return v3

    .line 587
    :pswitch_1
    sget-wide v12, Lcom/android/server/am/MiuiMemReclaimer;->FILE_RSS_LIMIT_KB:J

    cmp-long v7, v12, v10

    if-lez v7, :cond_2

    cmp-long v7, v5, v12

    if-gez v7, :cond_2

    .line 588
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->-$$Nest$mgetPid(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", file RSS is too small "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 590
    return v3

    .line 580
    :pswitch_2
    sget-wide v12, Lcom/android/server/am/MiuiMemReclaimer;->ANON_RSS_LIMIT_KB:J

    cmp-long v7, v12, v10

    if-lez v7, :cond_2

    cmp-long v7, v8, v12

    if-gez v7, :cond_2

    .line 581
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->-$$Nest$mgetPid(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", anon RSS is too small "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 583
    return v3

    .line 602
    :cond_2
    :goto_2
    return v4

    .line 569
    .end local v5    # "rssFile":J
    .end local v8    # "rssAnon":J
    :cond_3
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->-$$Nest$mgetPid(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " with no memory usage. Dead?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 571
    return v3

    nop

    :sswitch_data_0
    .sparse-switch
        0x179a1 -> :sswitch_2
        0x2dc2cc -> :sswitch_1
        0x2ff57c -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private isLowBattery()Z
    .locals 1

    .line 152
    iget-boolean v0, p0, Lcom/android/server/am/MiuiMemReclaimer;->mBatteryLow:Z

    return v0
.end method

.method private isProtectProcess(IILjava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "mode"    # I
    .param p2, "uid"    # I
    .param p3, "pkgName"    # Ljava/lang/String;
    .param p4, "procName"    # Ljava/lang/String;

    .line 299
    const/16 v0, 0x1d8

    .line 304
    .local v0, "protectFlag":I
    iget-object v1, p0, Lcom/android/server/am/MiuiMemReclaimer;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-interface {v1, p2, p4}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->isProcessPerceptible(ILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/server/am/MiuiMemReclaimer;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    .line 305
    invoke-interface {v1, v0, p3, p4}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->isProcessWhiteList(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 309
    :cond_0
    const/4 v1, 0x0

    return v1

    .line 307
    :cond_1
    :goto_0
    const/4 v1, 0x1

    return v1
.end method

.method private logTargetProcsDetails(Ljava/util/List;Ljava/lang/String;)V
    .locals 12
    .param p2, "action"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 471
    .local p1, "infoList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;>;"
    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 472
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/am/MiuiMemReclaimer;->DEBUG:Z

    if-eqz v0, :cond_2

    .line 473
    const/4 v0, 0x0

    .line 474
    .local v0, "num":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;

    .line 475
    .local v2, "info":Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;
    invoke-static {v2}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->-$$Nest$mgetPid(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)I

    move-result v3

    if-nez v3, :cond_1

    goto :goto_0

    .line 476
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 478
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->-$$Nest$mgetPid(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v2}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->-$$Nest$mgetProcessName(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, v2, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rss:[J

    const/4 v7, 0x0

    aget-wide v6, v6, v7

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    iget-object v6, v2, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rss:[J

    const/4 v8, 0x1

    aget-wide v8, v6, v8

    .line 479
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    iget-object v6, v2, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rss:[J

    const/4 v9, 0x2

    aget-wide v9, v6, v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    iget-object v6, v2, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->rss:[J

    const/4 v10, 0x3

    aget-wide v10, v6, v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    move-object v6, p2

    filled-new-array/range {v3 .. v10}, [Ljava/lang/Object;

    move-result-object v3

    .line 476
    const-string v4, "No.%s Proc %s:%s action=%s rss[%s, %s, %s, %s]"

    invoke-static {v4, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "MiuiMemoryService"

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    .end local v2    # "info":Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;
    goto :goto_0

    .line 482
    .end local v0    # "num":I
    :cond_2
    return-void

    .line 471
    :cond_3
    :goto_1
    return-void
.end method

.method private performCompactProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)V
    .locals 7
    .param p1, "proc"    # Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .param p2, "mode"    # I

    .line 313
    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPid()I

    move-result v0

    if-lez v0, :cond_8

    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->isKilled()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_3

    .line 314
    :cond_0
    new-instance v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;

    invoke-direct {v0, p1, p2}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;-><init>(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)V

    .line 315
    .local v0, "info":Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;
    invoke-direct {p0, v0}, Lcom/android/server/am/MiuiMemReclaimer;->isCompactSatisfied(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 316
    iget-boolean v1, p0, Lcom/android/server/am/MiuiMemReclaimer;->DEBUG:Z

    if-eqz v1, :cond_1

    const-string v1, "MiuiMemoryService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Proc "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->-$$Nest$mgetPid(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " isn\'t compact satisfied"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    :cond_1
    return-void

    .line 320
    :cond_2
    iget-object v1, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->proc:Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    invoke-virtual {v1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->isKilled()Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->proc:Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 321
    invoke-virtual {v1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I

    move-result v1

    invoke-direct {p0, v1, p2}, Lcom/android/server/am/MiuiMemReclaimer;->isAdjInCompactRange(II)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 322
    invoke-virtual {p0, p1, p2}, Lcom/android/server/am/MiuiMemReclaimer;->isCompactNeeded(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)Z

    move-result v1

    if-nez v1, :cond_3

    goto/16 :goto_2

    .line 325
    :cond_3
    invoke-static {}, Lcom/android/server/am/SystemPressureController;->getInstance()Lcom/android/server/am/SystemPressureController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/am/SystemPressureController;->isStartingApp()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 326
    invoke-static {}, Lcom/android/server/am/SystemPressureController;->getInstance()Lcom/android/server/am/SystemPressureController;

    move-result-object v1

    iget-object v1, v1, Lcom/android/server/am/SystemPressureController;->mStartingAppLock:Ljava/lang/Object;

    monitor-enter v1

    .line 327
    :try_start_0
    invoke-static {}, Lcom/android/server/am/SystemPressureController;->getInstance()Lcom/android/server/am/SystemPressureController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/am/SystemPressureController;->isStartingApp()Z

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_4

    .line 329
    :try_start_1
    invoke-static {}, Lcom/android/server/am/SystemPressureController;->getInstance()Lcom/android/server/am/SystemPressureController;

    move-result-object v2

    iget-object v2, v2, Lcom/android/server/am/SystemPressureController;->mStartingAppLock:Ljava/lang/Object;

    .line 330
    const-wide/16 v3, 0x7d0

    invoke-virtual {v2, v3, v4}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 333
    goto :goto_0

    .line 331
    :catch_0
    move-exception v2

    .line 332
    .local v2, "e":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 335
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :cond_4
    :goto_0
    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 337
    :cond_5
    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    .line 339
    .local v1, "startTime":J
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MiuiCompact:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->action:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 340
    invoke-static {v0}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->-$$Nest$mgetPid(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->-$$Nest$mgetProcessName(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->proc:Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    invoke-virtual {v4}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 339
    const-wide/16 v4, 0x1

    invoke-static {v4, v5, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 341
    iget-object v3, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->action:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->-$$Nest$mgetPid(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)I

    move-result v6

    invoke-virtual {p0, v3, v6}, Lcom/android/server/am/MiuiMemReclaimer;->performCompaction(Ljava/lang/String;I)V

    .line 342
    invoke-static {v4, v5}, Landroid/os/Trace;->traceEnd(J)V

    .line 344
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->lastCompactTimeMillis:J

    .line 345
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    sub-long/2addr v3, v1

    iput-wide v3, v0, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->compactDurationMillis:J

    .line 346
    const/4 v3, 0x1

    invoke-static {v0, v3}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->-$$Nest$mcomputeRssDiff(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;Z)V

    .line 347
    iget-object v3, p0, Lcom/android/server/am/MiuiMemReclaimer;->mCompactionStats:Ljava/util/Map;

    monitor-enter v3

    .line 348
    :try_start_3
    iget-object v4, p0, Lcom/android/server/am/MiuiMemReclaimer;->mCompactionStats:Ljava/util/Map;

    invoke-static {v0}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->-$$Nest$mgetPid(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 349
    iget-object v4, p0, Lcom/android/server/am/MiuiMemReclaimer;->mCompactionStats:Ljava/util/Map;

    invoke-static {v0}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->-$$Nest$mgetPid(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 351
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Compacted proc "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const v4, 0x13ba0

    invoke-static {v4, v3}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 352
    iget-boolean v3, p0, Lcom/android/server/am/MiuiMemReclaimer;->DEBUG:Z

    if-eqz v3, :cond_6

    const-string v3, "MiuiMemoryService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Compacted proc "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    :cond_6
    return-void

    .line 350
    :catchall_1
    move-exception v4

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v4

    .line 323
    .end local v1    # "startTime":J
    :cond_7
    :goto_2
    return-void

    .line 313
    .end local v0    # "info":Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;
    :cond_8
    :goto_3
    return-void
.end method

.method private performCompactProcesses(I)V
    .locals 10
    .param p1, "mode"    # I

    .line 356
    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    .line 357
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/android/server/am/MiuiMemReclaimer;->mLastCompactionTimeMillis:J

    sub-long/2addr v1, v3

    sget-wide v3, Lcom/android/server/am/MiuiMemReclaimer;->COMPACT_ALL_MIN_INTERVAL:J

    cmp-long v1, v1, v3

    if-gez v1, :cond_1

    .line 359
    iget-boolean v0, p0, Lcom/android/server/am/MiuiMemReclaimer;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "MiuiMemoryService"

    const-string v1, "Skip compaction for frequently"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 360
    :cond_0
    return-void

    .line 362
    :cond_1
    iget-boolean v1, p0, Lcom/android/server/am/MiuiMemReclaimer;->mInterruptNeeded:Z

    if-eqz v1, :cond_2

    .line 363
    const-string v0, "MiuiMemoryService"

    const-string v1, "Compact processes skipped"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    return-void

    .line 367
    :cond_2
    invoke-direct {p0, p1}, Lcom/android/server/am/MiuiMemReclaimer;->filterCompactionProcs(I)Ljava/util/List;

    move-result-object v1

    .line 368
    .local v1, "compactTargetProcs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 369
    const-string v0, "MiuiMemoryService"

    const-string v2, "No process can compact."

    invoke-static {v0, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    return-void

    .line 372
    :cond_3
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;

    .line 373
    .local v3, "info":Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;
    iget-boolean v4, p0, Lcom/android/server/am/MiuiMemReclaimer;->mInterruptNeeded:Z

    if-eqz v4, :cond_4

    .line 374
    const-string v0, "MiuiMemoryService"

    const-string v2, "Compact processes interrupted"

    invoke-static {v0, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    return-void

    .line 378
    :cond_4
    iget-object v4, v3, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->proc:Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    invoke-virtual {v4}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->isKilled()Z

    move-result v4

    if-nez v4, :cond_7

    iget-object v4, v3, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->proc:Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 379
    invoke-virtual {v4}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAmsProcState()I

    move-result v4

    invoke-direct {p0, v4, p1}, Lcom/android/server/am/MiuiMemReclaimer;->isAdjInCompactRange(II)Z

    move-result v4

    if-nez v4, :cond_5

    goto/16 :goto_1

    .line 384
    :cond_5
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 386
    .local v4, "startTime":J
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MiuiCompact:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v3, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->action:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 387
    invoke-static {v3}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->-$$Nest$mgetPid(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v3}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->-$$Nest$mgetProcessName(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v3, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->proc:Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    invoke-virtual {v7}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 386
    const-wide/16 v7, 0x1

    invoke-static {v7, v8, v6}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 388
    iget-object v6, v3, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->action:Ljava/lang/String;

    invoke-static {v3}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->-$$Nest$mgetPid(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)I

    move-result v9

    invoke-virtual {p0, v6, v9}, Lcom/android/server/am/MiuiMemReclaimer;->performCompaction(Ljava/lang/String;I)V

    .line 389
    invoke-static {v7, v8}, Landroid/os/Trace;->traceEnd(J)V

    .line 391
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iput-wide v6, v3, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->lastCompactTimeMillis:J

    .line 392
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v4

    iput-wide v6, v3, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->compactDurationMillis:J

    .line 393
    invoke-static {v3, v0}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->-$$Nest$mcomputeRssDiff(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;Z)V

    .line 394
    iget-object v6, p0, Lcom/android/server/am/MiuiMemReclaimer;->mCompactionStats:Ljava/util/Map;

    monitor-enter v6

    .line 395
    :try_start_0
    iget-object v7, p0, Lcom/android/server/am/MiuiMemReclaimer;->mCompactionStats:Ljava/util/Map;

    invoke-static {v3}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->-$$Nest$mgetPid(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 396
    iget-object v7, p0, Lcom/android/server/am/MiuiMemReclaimer;->mCompactionStats:Ljava/util/Map;

    invoke-static {v3}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->-$$Nest$mgetPid(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 398
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Compacted proc "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const v7, 0x13ba0

    invoke-static {v7, v6}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 399
    iget-boolean v6, p0, Lcom/android/server/am/MiuiMemReclaimer;->DEBUG:Z

    if-eqz v6, :cond_6

    const-string v6, "MiuiMemoryService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Compacted proc "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 400
    .end local v3    # "info":Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;
    .end local v4    # "startTime":J
    :cond_6
    goto/16 :goto_0

    .line 397
    .restart local v3    # "info":Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;
    .restart local v4    # "startTime":J
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 380
    .end local v4    # "startTime":J
    :cond_7
    :goto_1
    const-string v4, "MiuiMemoryService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v3}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->-$$Nest$mgetProcessName(Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " changed, compaction skipped."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    goto/16 :goto_0

    .line 401
    .end local v3    # "info":Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;
    :cond_8
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/server/am/MiuiMemReclaimer;->mLastCompactionTimeMillis:J

    .line 402
    const-string v0, "MiuiMemoryService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Compact processes success! Compact mode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    return-void
.end method

.method private static performCompactionLegacy(Ljava/lang/String;I)V
    .locals 5
    .param p0, "action"    # Ljava/lang/String;
    .param p1, "pid"    # I

    .line 615
    const/4 v0, 0x0

    .line 617
    .local v0, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/proc/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/reclaim"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    .line 618
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 623
    nop

    .line 624
    :try_start_1
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 628
    :cond_0
    :goto_0
    goto :goto_1

    .line 626
    :catch_0
    move-exception v1

    .line 629
    goto :goto_1

    .line 622
    :catchall_0
    move-exception v1

    goto :goto_2

    .line 619
    :catch_1
    move-exception v1

    .line 620
    .local v1, "e":Ljava/io/IOException;
    :try_start_2
    const-string v2, "MiuiMemoryService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Compaction failed: pid "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 623
    .end local v1    # "e":Ljava/io/IOException;
    if-eqz v0, :cond_0

    .line 624
    :try_start_3
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 630
    :goto_1
    return-void

    .line 623
    :goto_2
    if-eqz v0, :cond_1

    .line 624
    :try_start_4
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_3

    .line 626
    :catch_2
    move-exception v2

    goto :goto_4

    .line 628
    :cond_1
    :goto_3
    nop

    .line 629
    :goto_4
    throw v1
.end method

.method private performCompactionNew(Ljava/lang/String;I)V
    .locals 2
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "pid"    # I

    .line 634
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string v0, "file"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_1
    const-string v0, "anon"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_2
    const-string v0, "all"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    goto :goto_2

    .line 636
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/am/MiuiMemReclaimer;->mProcessDependencies:Lcom/android/server/am/CachedAppOptimizer$DefaultProcessDependencies;

    sget-object v1, Lcom/android/server/am/CachedAppOptimizer$CompactProfile;->FULL:Lcom/android/server/am/CachedAppOptimizer$CompactProfile;

    invoke-virtual {v0, v1, p2}, Lcom/android/server/am/CachedAppOptimizer$DefaultProcessDependencies;->performCompaction(Lcom/android/server/am/CachedAppOptimizer$CompactProfile;I)V

    .line 638
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/am/MiuiMemReclaimer;->mProcessDependencies:Lcom/android/server/am/CachedAppOptimizer$DefaultProcessDependencies;

    sget-object v1, Lcom/android/server/am/CachedAppOptimizer$CompactProfile;->SOME:Lcom/android/server/am/CachedAppOptimizer$CompactProfile;

    invoke-virtual {v0, v1, p2}, Lcom/android/server/am/CachedAppOptimizer$DefaultProcessDependencies;->performCompaction(Lcom/android/server/am/CachedAppOptimizer$CompactProfile;I)V

    .line 640
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/am/MiuiMemReclaimer;->mProcessDependencies:Lcom/android/server/am/CachedAppOptimizer$DefaultProcessDependencies;

    sget-object v1, Lcom/android/server/am/CachedAppOptimizer$CompactProfile;->ANON:Lcom/android/server/am/CachedAppOptimizer$CompactProfile;

    invoke-virtual {v0, v1, p2}, Lcom/android/server/am/CachedAppOptimizer$DefaultProcessDependencies;->performCompaction(Lcom/android/server/am/CachedAppOptimizer$CompactProfile;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 644
    :goto_2
    goto :goto_3

    .line 642
    :catch_0
    move-exception v0

    .line 643
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 645
    .end local v0    # "e":Ljava/io/IOException;
    :goto_3
    return-void

    :sswitch_data_0
    .sparse-switch
        0x179a1 -> :sswitch_2
        0x2dc2cc -> :sswitch_1
        0x2ff57c -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static reclaimPage()V
    .locals 2

    .line 183
    sget-boolean v0, Lcom/android/server/am/MiuiMemReclaimer;->RECLAIM_IF_NEEDED:Z

    if-eqz v0, :cond_0

    .line 184
    const-string v0, "/sys/kernel/mi_reclaim/event"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/server/am/MiuiMemReclaimer;->writeToNode(Ljava/lang/String;I)V

    .line 185
    :cond_0
    return-void
.end method

.method public static reclaimPage(I)V
    .locals 1
    .param p0, "reclaimSizeMB"    # I

    .line 178
    sget-boolean v0, Lcom/android/server/am/MiuiMemReclaimer;->RECLAIM_IF_NEEDED:Z

    if-eqz v0, :cond_0

    .line 179
    const-string v0, "/sys/kernel/mi_reclaim/event"

    invoke-static {v0, p0}, Lcom/android/server/am/MiuiMemReclaimer;->writeToNode(Ljava/lang/String;I)V

    .line 180
    :cond_0
    return-void
.end method

.method private sortProcsByRss(Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .param p2, "action"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 452
    .local p1, "infoList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;>;"
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 453
    :cond_0
    new-instance v0, Lcom/android/server/am/MiuiMemReclaimer$3;

    invoke-direct {v0, p0, p2}, Lcom/android/server/am/MiuiMemReclaimer$3;-><init>(Lcom/android/server/am/MiuiMemReclaimer;Ljava/lang/String;)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 468
    return-void

    .line 452
    :cond_1
    :goto_0
    return-void
.end method

.method public static writeToNode(Ljava/lang/String;I)V
    .locals 8
    .param p0, "node"    # Ljava/lang/String;
    .param p1, "value"    # I

    .line 198
    const-string v0, "MiuiMemoryService"

    const/4 v1, 0x0

    .line 199
    .local v1, "writer":Ljava/io/FileWriter;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 200
    .local v2, "file":Ljava/io/File;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 201
    .local v3, "commMsg":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "error"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 202
    .local v4, "errMsg":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "success"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 203
    .local v5, "succMsg":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_0

    .line 204
    return-void

    .line 206
    :cond_0
    :try_start_0
    new-instance v6, Ljava/io/FileWriter;

    invoke-direct {v6, v2}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    move-object v1, v6

    .line 207
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 211
    nop

    .line 213
    :try_start_1
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 216
    :goto_0
    goto :goto_1

    .line 214
    :catch_0
    move-exception v6

    .line 215
    .local v6, "e":Ljava/io/IOException;
    invoke-static {v0, v4, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .end local v6    # "e":Ljava/io/IOException;
    goto :goto_0

    .line 211
    :catchall_0
    move-exception v6

    goto :goto_2

    .line 208
    :catch_1
    move-exception v6

    .line 209
    .restart local v6    # "e":Ljava/io/IOException;
    :try_start_2
    invoke-static {v0, v4, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 211
    nop

    .end local v6    # "e":Ljava/io/IOException;
    if-eqz v1, :cond_1

    .line 213
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 219
    :cond_1
    :goto_1
    return-void

    .line 211
    :goto_2
    if-eqz v1, :cond_2

    .line 213
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 216
    goto :goto_3

    .line 214
    :catch_2
    move-exception v7

    .line 215
    .local v7, "e":Ljava/io/IOException;
    invoke-static {v0, v4, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 218
    .end local v7    # "e":Ljava/io/IOException;
    :cond_2
    :goto_3
    throw v6
.end method


# virtual methods
.method public dumpCompactionStats(Ljava/io/PrintWriter;)V
    .locals 6
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 222
    iget-object v0, p0, Lcom/android/server/am/MiuiMemReclaimer;->mCompactionStats:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 223
    const-string v0, "Compaction never done!"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 224
    return-void

    .line 226
    :cond_0
    const/4 v0, 0x0

    .line 227
    .local v0, "num":I
    const-string v1, "Compaction Statistics:"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 228
    iget-object v1, p0, Lcom/android/server/am/MiuiMemReclaimer;->mCompactionStats:Ljava/util/Map;

    monitor-enter v1

    .line 230
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/MiuiMemReclaimer;->mCompactionStats:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 231
    .local v3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;>;"
    add-int/lit8 v0, v0, 0x1

    .line 232
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;

    invoke-virtual {v5}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 233
    .end local v3    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;>;"
    goto :goto_0

    .line 234
    :cond_1
    monitor-exit v1

    .line 235
    return-void

    .line 234
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public declared-synchronized interruptProcCompaction(I)V
    .locals 1
    .param p1, "pid"    # I

    monitor-enter p0

    .line 281
    :try_start_0
    sget v0, Lcom/android/server/am/CachedAppOptimizer$DefaultProcessDependencies;->mPidCompacting:I

    if-ne v0, p1, :cond_0

    .line 282
    iget-object v0, p0, Lcom/android/server/am/MiuiMemReclaimer;->mProcessDependencies:Lcom/android/server/am/CachedAppOptimizer$DefaultProcessDependencies;

    invoke-virtual {v0}, Lcom/android/server/am/CachedAppOptimizer$DefaultProcessDependencies;->interruptProcCompaction()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 284
    .end local p0    # "this":Lcom/android/server/am/MiuiMemReclaimer;
    :cond_0
    monitor-exit p0

    return-void

    .line 280
    .end local p1    # "pid":I
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized interruptProcsCompaction()V
    .locals 1

    monitor-enter p0

    .line 277
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/android/server/am/MiuiMemReclaimer;->mInterruptNeeded:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 278
    monitor-exit p0

    return-void

    .line 276
    .end local p0    # "this":Lcom/android/server/am/MiuiMemReclaimer;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isCompactNeeded(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)Z
    .locals 12
    .param p1, "proc"    # Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .param p2, "mode"    # I

    .line 490
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;

    move-result-object v0

    .line 491
    .local v0, "procName":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 492
    .local v1, "pkgName":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUid()I

    move-result v2

    .line 493
    .local v2, "uid":I
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I

    move-result v3

    .line 495
    .local v3, "adj":I
    const/4 v4, 0x0

    const/4 v5, 0x4

    if-ne p2, v5, :cond_1

    .line 496
    invoke-static {}, Lcom/android/server/am/ProcessListStub;->get()Lcom/android/server/am/ProcessListStub;

    move-result-object v6

    invoke-interface {v6}, Lcom/android/server/am/ProcessListStub;->isGameMode()Z

    move-result v6

    if-nez v6, :cond_0

    .line 497
    invoke-direct {p0}, Lcom/android/server/am/MiuiMemReclaimer;->isCameraMode()Z

    move-result v6

    if-nez v6, :cond_0

    .line 498
    invoke-direct {p0}, Lcom/android/server/am/MiuiMemReclaimer;->isLowBattery()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 499
    :cond_0
    return v4

    .line 502
    :cond_1
    sget-object v6, Lcom/android/server/am/MiuiMemReclaimer;->COMPACTION_PROC_NAME_WHITE_LIST:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_a

    .line 503
    const-string v6, "com.miui.miwallpaper"

    invoke-virtual {v0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_a

    if-nez p2, :cond_2

    .line 505
    const-string v6, "com.android.camera"

    invoke-static {v6, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_a

    :cond_2
    const/4 v6, 0x2

    if-eq p2, v6, :cond_3

    .line 507
    const-string v6, "com.tencent.mm"

    invoke-static {v6, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_a

    :cond_3
    const/4 v6, 0x3

    if-ne p2, v6, :cond_4

    .line 510
    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    goto :goto_1

    .line 513
    :cond_4
    const/16 v6, 0x3e8

    if-le v2, v6, :cond_9

    invoke-direct {p0, v3, p2}, Lcom/android/server/am/MiuiMemReclaimer;->isAdjInCompactRange(II)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 514
    invoke-direct {p0, p2, v2, v1, v0}, Lcom/android/server/am/MiuiMemReclaimer;->isProtectProcess(IILjava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    goto :goto_0

    .line 517
    :cond_5
    if-ne p2, v5, :cond_8

    .line 518
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 519
    .local v5, "nowTime":J
    iget-object v7, p0, Lcom/android/server/am/MiuiMemReclaimer;->mCompactionStats:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPid()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;

    .line 520
    .local v7, "procInfo":Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;
    if-eqz v7, :cond_6

    iget-wide v8, v7, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->lastCompactTimeMillis:J

    sub-long v8, v5, v8

    sget-wide v10, Lcom/android/server/am/MiuiMemReclaimer;->COMPACT_PROC_MIN_INTERVAL:J

    cmp-long v8, v8, v10

    if-gez v8, :cond_6

    iget-object v8, v7, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->action:Ljava/lang/String;

    .line 523
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAmsProcState()I

    move-result v9

    invoke-static {v9, p2}, Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;->-$$Nest$smgenCompactAction(II)Ljava/lang/String;

    move-result-object v9

    .line 522
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 524
    return v4

    .line 525
    :cond_6
    iget-object v8, p0, Lcom/android/server/am/MiuiMemReclaimer;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-interface {v8, v2}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->isUidVisible(I)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 526
    return v4

    .line 527
    :cond_7
    const-string v8, "camera-improve"

    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdjType()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 528
    return v4

    .line 531
    .end local v5    # "nowTime":J
    .end local v7    # "procInfo":Lcom/android/server/am/MiuiMemReclaimer$CompactProcInfo;
    :cond_8
    const/4 v4, 0x1

    return v4

    .line 515
    :cond_9
    :goto_0
    return v4

    .line 511
    :cond_a
    :goto_1
    return v4
.end method

.method public performCompaction(Ljava/lang/String;I)V
    .locals 1
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "pid"    # I

    .line 606
    sget-boolean v0, Lcom/android/server/am/MiuiMemReclaimer;->USE_LEGACY_COMPACTION:Z

    if-eqz v0, :cond_0

    .line 607
    invoke-static {p1, p2}, Lcom/android/server/am/MiuiMemReclaimer;->performCompactionLegacy(Ljava/lang/String;I)V

    goto :goto_0

    .line 609
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/android/server/am/MiuiMemReclaimer;->performCompactionNew(Ljava/lang/String;I)V

    .line 611
    :goto_0
    return-void
.end method

.method public runGlobalCompaction(I)V
    .locals 4
    .param p1, "vmPressureLevel"    # I

    .line 238
    const/4 v0, -0x1

    .line 239
    .local v0, "reclaimSizeMB":I
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 248
    :pswitch_0
    const/16 v0, 0x64

    .line 249
    goto :goto_0

    .line 244
    :pswitch_1
    const/16 v0, 0x32

    .line 245
    goto :goto_0

    .line 241
    :pswitch_2
    const/16 v0, 0x1e

    .line 242
    nop

    .line 253
    :goto_0
    if-gez v0, :cond_0

    return-void

    .line 254
    :cond_0
    iget-object v1, p0, Lcom/android/server/am/MiuiMemReclaimer;->mCompactorHandler:Landroid/os/Handler;

    .line 255
    const/16 v2, 0x64

    const/4 v3, 0x0

    invoke-direct {p0, v2, v0, v3}, Lcom/android/server/am/MiuiMemReclaimer;->generateMessage(IILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 254
    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 256
    iget-boolean v1, p0, Lcom/android/server/am/MiuiMemReclaimer;->DEBUG:Z

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Global reclaim "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "MB."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiMemoryService"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    :cond_1
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public runProcCompaction(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)V
    .locals 2
    .param p1, "proc"    # Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .param p2, "mode"    # I

    .line 260
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPid()I

    move-result v0

    if-lez v0, :cond_1

    .line 261
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->isSystemApp()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0, p1, p2}, Lcom/android/server/am/MiuiMemReclaimer;->isCompactNeeded(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 264
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/MiuiMemReclaimer;->mCompactorHandler:Landroid/os/Handler;

    .line 265
    const/16 v1, 0x65

    invoke-direct {p0, v1, p2, p1}, Lcom/android/server/am/MiuiMemReclaimer;->generateMessage(IILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 264
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 266
    return-void

    .line 262
    :cond_1
    :goto_0
    return-void
.end method

.method public runProcsCompaction(I)V
    .locals 3
    .param p1, "mode"    # I

    .line 269
    monitor-enter p0

    .line 270
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/android/server/am/MiuiMemReclaimer;->mInterruptNeeded:Z

    .line 271
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 272
    iget-object v0, p0, Lcom/android/server/am/MiuiMemReclaimer;->mCompactorHandler:Landroid/os/Handler;

    .line 273
    const/16 v1, 0x66

    const/4 v2, 0x0

    invoke-direct {p0, v1, p1, v2}, Lcom/android/server/am/MiuiMemReclaimer;->generateMessage(IILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 272
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 274
    return-void

    .line 271
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setAppStartingMode(Z)V
    .locals 1
    .param p1, "appStarting"    # Z

    .line 287
    iget-object v0, p0, Lcom/android/server/am/MiuiMemReclaimer;->mProcessDependencies:Lcom/android/server/am/CachedAppOptimizer$DefaultProcessDependencies;

    invoke-virtual {v0, p1}, Lcom/android/server/am/CachedAppOptimizer$DefaultProcessDependencies;->setAppStartingMode(Z)V

    .line 288
    return-void
.end method
