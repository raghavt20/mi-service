class com.android.server.am.MiProcessTracker$AppStartRecordController {
	 /* .source "MiProcessTracker.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/MiProcessTracker; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "AppStartRecordController" */
} // .end annotation
/* # static fields */
private static final Integer APP_COLD_START_MODE;
private static final Integer APP_HOT_START_MODE;
private static final Integer APP_WARM_START_MODE;
private static final Long INTERVAL_TIME;
/* # instance fields */
private java.lang.String mForegroundPackageName;
final com.android.server.am.MiProcessTracker this$0; //synthetic
/* # direct methods */
public static java.lang.String $r8$lambda$FcW9bx58BoSpww5UVZN4ptxJyeg ( com.miui.server.smartpower.IAppState p0 ) { //synthethic
/* .locals 0 */
(( com.miui.server.smartpower.IAppState ) p0 ).getPackageName ( ); // invoke-virtual {p0}, Lcom/miui/server/smartpower/IAppState;->getPackageName()Ljava/lang/String;
} // .end method
static void -$$Nest$monActivityLaunched ( com.android.server.am.MiProcessTracker$AppStartRecordController p0, java.lang.String p1, java.lang.String p2, Integer p3, Integer p4 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/am/MiProcessTracker$AppStartRecordController;->onActivityLaunched(Ljava/lang/String;Ljava/lang/String;II)V */
return;
} // .end method
private com.android.server.am.MiProcessTracker$AppStartRecordController ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/android/server/am/MiProcessTracker; */
/* .line 199 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 197 */
final String v0 = ""; // const-string v0, ""
this.mForegroundPackageName = v0;
/* .line 200 */
return;
} // .end method
 com.android.server.am.MiProcessTracker$AppStartRecordController ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/MiProcessTracker$AppStartRecordController;-><init>(Lcom/android/server/am/MiProcessTracker;)V */
return;
} // .end method
static Boolean lambda$reportAppStart$0 ( com.miui.server.smartpower.IAppState p0 ) { //synthethic
/* .locals 2 */
/* .param p0, "app" # Lcom/miui/server/smartpower/IAppState; */
/* .line 241 */
v0 = (( com.miui.server.smartpower.IAppState ) p0 ).getUid ( ); // invoke-virtual {p0}, Lcom/miui/server/smartpower/IAppState;->getUid()I
/* const/16 v1, 0x3e8 */
/* if-eq v0, v1, :cond_0 */
v0 = com.android.server.am.MiProcessTracker.trackerPkgWhiteList;
/* .line 242 */
(( com.miui.server.smartpower.IAppState ) p0 ).getPackageName ( ); // invoke-virtual {p0}, Lcom/miui/server/smartpower/IAppState;->getPackageName()Ljava/lang/String;
v0 = (( java.util.HashSet ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
/* .line 243 */
v0 = (( com.miui.server.smartpower.IAppState ) p0 ).getAppSwitchFgCount ( ); // invoke-virtual {p0}, Lcom/miui/server/smartpower/IAppState;->getAppSwitchFgCount()I
/* if-lez v0, :cond_0 */
/* .line 244 */
v0 = (( com.miui.server.smartpower.IAppState ) p0 ).isAlive ( ); // invoke-virtual {p0}, Lcom/miui/server/smartpower/IAppState;->isAlive()Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 241 */
} // :goto_0
} // .end method
static java.lang.String lambda$reportAppStart$1 ( Integer p0 ) { //synthethic
/* .locals 1 */
/* .param p0, "x$0" # I */
/* .line 248 */
/* new-array v0, p0, [Ljava/lang/String; */
} // .end method
private void onActivityLaunched ( java.lang.String p0, java.lang.String p1, Integer p2, Integer p3 ) {
/* .locals 3 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "uid" # I */
/* .param p4, "launchState" # I */
/* .line 204 */
/* if-nez p4, :cond_0 */
return;
/* .line 205 */
} // :cond_0
v0 = this.mForegroundPackageName;
v0 = (( java.lang.String ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
return;
/* .line 206 */
} // :cond_1
this.mForegroundPackageName = p2;
/* .line 207 */
/* const/16 v0, 0x3e8 */
/* if-ne p3, v0, :cond_2 */
return;
/* .line 208 */
} // :cond_2
v0 = (( java.lang.String ) p1 ).equals ( p2 ); // invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_3 */
return;
/* .line 210 */
} // :cond_3
v0 = this.this$0;
com.android.server.am.MiProcessTracker .-$$Nest$fgetmSmartPowerService ( v0 );
/* .line 211 */
/* .line 212 */
/* .local v0, "app":Lcom/miui/server/smartpower/IAppState; */
if ( v0 != null) { // if-eqz v0, :cond_7
v1 = com.android.server.am.MiProcessTracker.trackerPkgWhiteList;
v1 = (( java.util.HashSet ) v1 ).contains ( p2 ); // invoke-virtual {v1, p2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 215 */
} // :cond_4
/* packed-switch p4, :pswitch_data_0 */
/* .line 225 */
/* :pswitch_0 */
int v1 = 2; // const/4 v1, 0x2
/* invoke-direct {p0, p2, v1}, Lcom/android/server/am/MiProcessTracker$AppStartRecordController;->reportAppStart(Ljava/lang/String;I)V */
/* .line 226 */
/* .line 222 */
/* :pswitch_1 */
int v1 = 3; // const/4 v1, 0x3
/* invoke-direct {p0, p2, v1}, Lcom/android/server/am/MiProcessTracker$AppStartRecordController;->reportAppStart(Ljava/lang/String;I)V */
/* .line 223 */
/* .line 217 */
/* :pswitch_2 */
v1 = this.this$0;
com.android.server.am.MiProcessTracker .-$$Nest$fgetmProcessRecordController ( v1 );
v1 = com.android.server.am.MiProcessTracker$ProcessRecordController .-$$Nest$misAppFirstStart ( v1,v0 );
/* if-nez v1, :cond_5 */
/* .line 218 */
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {p0, p2, v1}, Lcom/android/server/am/MiProcessTracker$AppStartRecordController;->reportAppStart(Ljava/lang/String;I)V */
/* .line 231 */
} // :cond_5
} // :goto_0
/* sget-boolean v1, Lcom/android/server/am/MiProcessTracker;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 232 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "onActivityLaunched packageName = "; // const-string v2, "onActivityLaunched packageName = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " launchState = "; // const-string v2, " launchState = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p4 ); // invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiProcessTracker"; // const-string v2, "MiProcessTracker"
android.util.Slog .d ( v2,v1 );
/* .line 235 */
} // :cond_6
return;
/* .line 213 */
} // :cond_7
} // :goto_1
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void reportAppStart ( java.lang.String p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "flag" # I */
/* .line 238 */
v0 = this.this$0;
com.android.server.am.MiProcessTracker .-$$Nest$fgetmPerfShielder ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 239 */
v0 = this.this$0;
com.android.server.am.MiProcessTracker .-$$Nest$fgetmSmartPowerService ( v0 );
/* .line 240 */
/* .line 241 */
(( java.util.ArrayList ) v0 ).stream ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->stream()Ljava/util/stream/Stream;
/* new-instance v1, Lcom/android/server/am/MiProcessTracker$AppStartRecordController$$ExternalSyntheticLambda0; */
/* invoke-direct {v1}, Lcom/android/server/am/MiProcessTracker$AppStartRecordController$$ExternalSyntheticLambda0;-><init>()V */
/* .line 245 */
java.util.stream.Collectors .toList ( );
/* check-cast v0, Ljava/util/List; */
/* .line 246 */
/* .local v0, "residentAppList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/IAppState;>;" */
/* new-instance v2, Lcom/android/server/am/MiProcessTracker$AppStartRecordController$$ExternalSyntheticLambda1; */
/* invoke-direct {v2}, Lcom/android/server/am/MiProcessTracker$AppStartRecordController$$ExternalSyntheticLambda1;-><init>()V */
/* .line 247 */
/* .line 248 */
java.util.stream.Collectors .toList ( );
/* check-cast v1, Ljava/util/List; */
/* new-instance v2, Lcom/android/server/am/MiProcessTracker$AppStartRecordController$$ExternalSyntheticLambda2; */
/* invoke-direct {v2}, Lcom/android/server/am/MiProcessTracker$AppStartRecordController$$ExternalSyntheticLambda2;-><init>()V */
/* check-cast v1, [Ljava/lang/String; */
/* .line 249 */
/* .local v1, "residentPkgNameList":[Ljava/lang/String; */
/* sget-boolean v2, Lcom/android/server/am/MiProcessTracker;->DEBUG:Z */
final String v3 = "MiProcessTracker"; // const-string v3, "MiProcessTracker"
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 250 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "reportAppStart packageName = "; // const-string v4, "reportAppStart packageName = "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " flag = "; // const-string v4, " flag = "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " residentAppList size = "; // const-string v4, " residentAppList size = "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = /* .line 252 */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " residentAppList = "; // const-string v4, " residentAppList = "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 253 */
java.util.Arrays .toString ( v1 );
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 250 */
android.util.Slog .d ( v3,v2 );
/* .line 256 */
} // :cond_0
try { // :try_start_0
v2 = this.this$0;
v4 = com.android.server.am.MiProcessTracker .-$$Nest$fgetmPerfShielder ( v2 );
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 260 */
/* .line 258 */
/* :catch_0 */
/* move-exception v2 */
/* .line 259 */
/* .local v2, "e":Landroid/os/RemoteException; */
final String v4 = "mPerfShielder is dead"; // const-string v4, "mPerfShielder is dead"
android.util.Slog .e ( v3,v4,v2 );
/* .line 262 */
} // .end local v0 # "residentAppList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/IAppState;>;"
} // .end local v1 # "residentPkgNameList":[Ljava/lang/String;
} // .end local v2 # "e":Landroid/os/RemoteException;
} // :cond_1
} // :goto_0
return;
} // .end method
