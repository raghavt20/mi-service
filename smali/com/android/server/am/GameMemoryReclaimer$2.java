class com.android.server.am.GameMemoryReclaimer$2 implements java.util.Comparator {
	 /* .source "GameMemoryReclaimer.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/am/GameMemoryReclaimer;->filterPackageInfos(Lcom/android/server/am/IGameProcessAction;)Ljava/util/List; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Object;", */
/* "Ljava/util/Comparator<", */
/* "Lcom/android/server/am/GameProcessKiller$PackageMemInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* # instance fields */
final com.android.server.am.GameMemoryReclaimer this$0; //synthetic
/* # direct methods */
 com.android.server.am.GameMemoryReclaimer$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/GameMemoryReclaimer; */
/* .line 222 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public Integer compare ( com.android.server.am.GameProcessKiller$PackageMemInfo p0, com.android.server.am.GameProcessKiller$PackageMemInfo p1 ) {
/* .locals 4 */
/* .param p1, "o1" # Lcom/android/server/am/GameProcessKiller$PackageMemInfo; */
/* .param p2, "o2" # Lcom/android/server/am/GameProcessKiller$PackageMemInfo; */
/* .line 225 */
/* iget v0, p2, Lcom/android/server/am/GameProcessKiller$PackageMemInfo;->mState:I */
/* iget v1, p1, Lcom/android/server/am/GameProcessKiller$PackageMemInfo;->mState:I */
/* if-ne v0, v1, :cond_0 */
/* .line 226 */
/* iget-wide v0, p2, Lcom/android/server/am/GameProcessKiller$PackageMemInfo;->mMemSize:J */
/* iget-wide v2, p1, Lcom/android/server/am/GameProcessKiller$PackageMemInfo;->mMemSize:J */
/* sub-long/2addr v0, v2 */
/* long-to-int v0, v0 */
/* .line 227 */
} // :cond_0
/* iget v0, p2, Lcom/android/server/am/GameProcessKiller$PackageMemInfo;->mState:I */
/* iget v1, p1, Lcom/android/server/am/GameProcessKiller$PackageMemInfo;->mState:I */
/* sub-int/2addr v0, v1 */
} // .end method
public Integer compare ( java.lang.Object p0, java.lang.Object p1 ) { //bridge//synthethic
/* .locals 0 */
/* .line 222 */
/* check-cast p1, Lcom/android/server/am/GameProcessKiller$PackageMemInfo; */
/* check-cast p2, Lcom/android/server/am/GameProcessKiller$PackageMemInfo; */
p1 = (( com.android.server.am.GameMemoryReclaimer$2 ) p0 ).compare ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/am/GameMemoryReclaimer$2;->compare(Lcom/android/server/am/GameProcessKiller$PackageMemInfo;Lcom/android/server/am/GameProcessKiller$PackageMemInfo;)I
} // .end method
