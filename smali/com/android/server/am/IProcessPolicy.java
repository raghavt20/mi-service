public abstract class com.android.server.am.IProcessPolicy {
	 /* .source "IProcessPolicy.java" */
	 /* # static fields */
	 public static final Integer FLAG_CLOUD_WHITE_LIST;
	 public static final Integer FLAG_DISABLE_FORCE_STOP;
	 public static final Integer FLAG_DISABLE_TRIM_MEMORY;
	 public static final Integer FLAG_DYNAMIC_WHITE_LIST;
	 public static final Integer FLAG_ENABLE_CALL_PROTECT;
	 public static final Integer FLAG_ENTERPRISE_APP_LIST;
	 public static final Integer FLAG_FAST_BOOT_APP_LIST;
	 public static final Integer FLAG_NEED_TRACE_LIST;
	 public static final Integer FLAG_SECRETLY_PROTECT_APP_LIST;
	 public static final Integer FLAG_STATIC_WHILTE_LIST;
	 public static final Integer FLAG_USER_DEFINED_LIST;
	 public static final Integer FLAG_USER_REQUEST_CLEAN_WHITE_LIST;
	 public static final java.lang.String REASON_ANR;
	 public static final java.lang.String REASON_AUTO_IDLE_KILL;
	 public static final java.lang.String REASON_AUTO_LOCK_OFF_CLEAN;
	 public static final java.lang.String REASON_AUTO_LOCK_OFF_CLEAN_BY_PRIORITY;
	 public static final java.lang.String REASON_AUTO_POWER_KILL;
	 public static final java.lang.String REASON_AUTO_SLEEP_CLEAN;
	 public static final java.lang.String REASON_AUTO_SYSTEM_ABNORMAL_CLEAN;
	 public static final java.lang.String REASON_AUTO_THERMAL_KILL;
	 public static final java.lang.String REASON_AUTO_THERMAL_KILL_ALL_LEVEL_1;
	 public static final java.lang.String REASON_AUTO_THERMAL_KILL_ALL_LEVEL_2;
	 public static final java.lang.String REASON_CRASH;
	 public static final java.lang.String REASON_DISPLAY_SIZE_CHANGED;
	 public static final java.lang.String REASON_FORCE_CLEAN;
	 public static final java.lang.String REASON_GAME_CLEAN;
	 public static final java.lang.String REASON_GARBAGE_CLEAN;
	 public static final java.lang.String REASON_LOCK_SCREEN_CLEAN;
	 public static final java.lang.String REASON_LOW_MEMO;
	 public static final java.lang.String REASON_MIUI_MEMO_SERVICE;
	 public static final java.lang.String REASON_ONE_KEY_CLEAN;
	 public static final java.lang.String REASON_OPTIMIZATION_CLEAN;
	 public static final java.lang.String REASON_SCREEN_OFF_CPU_CHECK_KILL;
	 public static final java.lang.String REASON_SWIPE_UP_CLEAN;
	 public static final java.lang.String REASON_UNKNOWN;
	 public static final java.lang.String REASON_USER_DEFINED;
	 public static final java.lang.String TAG_PM;
	 public static final Boolean TAG_WITH_CLASS_NAME;
	 public static final Integer USER_ALL;
	 /* # direct methods */
	 public static Integer getAppMaxProcState ( com.android.server.am.ProcessRecord p0 ) {
		 /* .locals 1 */
		 /* .param p0, "app" # Lcom/android/server/am/ProcessRecord; */
		 /* .line 87 */
		 v0 = this.mState;
		 /* iget v0, v0, Lcom/android/server/am/ProcessStateRecord;->mMaxProcState:I */
	 } // .end method
	 public static java.lang.Long getMemoryThresholdForFastBooApp ( ) {
		 /* .locals 6 */
		 /* .line 70 */
		 android.os.Process .getTotalMemory ( );
		 /* move-result-wide v0 */
		 /* const-wide/32 v2, 0x40000000 */
		 /* div-long/2addr v0, v2 */
		 /* .line 71 */
		 /* .local v0, "deviceTotalMem":J */
		 /* const-wide/16 v2, 0x0 */
		 /* .line 72 */
		 /* .local v2, "memoryThreshold":J */
		 /* const-wide/16 v4, 0x4 */
		 /* cmp-long v4, v0, v4 */
		 /* if-gez v4, :cond_0 */
		 /* .line 73 */
		 /* const-wide/32 v2, 0xc8000 */
		 /* .line 74 */
	 } // :cond_0
	 /* const-wide/16 v4, 0x8 */
	 /* cmp-long v4, v0, v4 */
	 /* if-gez v4, :cond_1 */
	 /* .line 75 */
	 /* const-wide/32 v2, 0x177000 */
	 /* .line 77 */
} // :cond_1
/* const-wide/32 v2, 0x1f4000 */
/* .line 79 */
} // :goto_0
java.lang.Long .valueOf ( v2,v3 );
} // .end method
public static void setAppMaxProcState ( com.android.server.am.ProcessRecord p0, Integer p1 ) {
/* .locals 1 */
/* .param p0, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p1, "targetMaxProcState" # I */
/* .line 83 */
v0 = this.mState;
/* iput p1, v0, Lcom/android/server/am/ProcessStateRecord;->mMaxProcState:I */
/* .line 84 */
return;
} // .end method
/* # virtual methods */
public abstract java.util.List getActiveUidList ( Integer p0 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end method
public abstract Boolean isLockedApplication ( java.lang.String p0, Integer p1 ) {
} // .end method
