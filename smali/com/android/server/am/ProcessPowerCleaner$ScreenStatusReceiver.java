class com.android.server.am.ProcessPowerCleaner$ScreenStatusReceiver extends android.content.BroadcastReceiver {
	 /* .source "ProcessPowerCleaner.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/ProcessPowerCleaner; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "ScreenStatusReceiver" */
} // .end annotation
/* # instance fields */
private java.lang.String SCREEN_OFF;
private java.lang.String SCREEN_ON;
final com.android.server.am.ProcessPowerCleaner this$0; //synthetic
/* # direct methods */
private com.android.server.am.ProcessPowerCleaner$ScreenStatusReceiver ( ) {
/* .locals 0 */
/* .line 375 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
/* .line 377 */
final String p1 = "android.intent.action.SCREEN_ON"; // const-string p1, "android.intent.action.SCREEN_ON"
this.SCREEN_ON = p1;
/* .line 378 */
final String p1 = "android.intent.action.SCREEN_OFF"; // const-string p1, "android.intent.action.SCREEN_OFF"
this.SCREEN_OFF = p1;
return;
} // .end method
 com.android.server.am.ProcessPowerCleaner$ScreenStatusReceiver ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessPowerCleaner$ScreenStatusReceiver;-><init>(Lcom/android/server/am/ProcessPowerCleaner;)V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 382 */
v0 = this.SCREEN_ON;
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 383 */
	 v0 = this.this$0;
	 int v1 = 0; // const/4 v1, 0x0
	 com.android.server.am.ProcessPowerCleaner .-$$Nest$fputmIsScreenOffState ( v0,v1 );
	 /* .line 384 */
	 v0 = this.this$0;
	 com.android.server.am.ProcessPowerCleaner .-$$Nest$fgetmHandler ( v0 );
	 /* const/16 v1, 0xf */
	 (( com.android.server.am.ProcessPowerCleaner$H ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessPowerCleaner$H;->obtainMessage(I)Landroid/os/Message;
	 /* .line 385 */
	 /* .local v0, "msg":Landroid/os/Message; */
	 v1 = this.this$0;
	 com.android.server.am.ProcessPowerCleaner .-$$Nest$fgetmHandler ( v1 );
	 (( com.android.server.am.ProcessPowerCleaner$H ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/am/ProcessPowerCleaner$H;->sendMessage(Landroid/os/Message;)Z
	 /* .line 386 */
} // .end local v0 # "msg":Landroid/os/Message;
} // :cond_0
v0 = this.SCREEN_OFF;
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.this$0;
com.android.server.am.ProcessPowerCleaner .-$$Nest$fgetmHandler ( v0 );
/* .line 387 */
/* const/16 v1, 0x10 */
v0 = (( com.android.server.am.ProcessPowerCleaner$H ) v0 ).hasMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessPowerCleaner$H;->hasMessages(I)Z
/* if-nez v0, :cond_1 */
/* .line 388 */
v0 = this.this$0;
int v2 = 1; // const/4 v2, 0x1
com.android.server.am.ProcessPowerCleaner .-$$Nest$fputmIsScreenOffState ( v0,v2 );
/* .line 389 */
v0 = this.this$0;
com.android.server.am.ProcessPowerCleaner .-$$Nest$fgetmHandler ( v0 );
(( com.android.server.am.ProcessPowerCleaner$H ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessPowerCleaner$H;->obtainMessage(I)Landroid/os/Message;
/* .line 390 */
/* .restart local v0 # "msg":Landroid/os/Message; */
v1 = this.this$0;
com.android.server.am.ProcessPowerCleaner .-$$Nest$fgetmHandler ( v1 );
/* const-wide/32 v2, 0x493e0 */
(( com.android.server.am.ProcessPowerCleaner$H ) v1 ).sendMessageDelayed ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/am/ProcessPowerCleaner$H;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 392 */
} // .end local v0 # "msg":Landroid/os/Message;
} // :cond_1
} // :goto_0
return;
} // .end method
