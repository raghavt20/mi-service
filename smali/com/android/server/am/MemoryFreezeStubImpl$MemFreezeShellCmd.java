class com.android.server.am.MemoryFreezeStubImpl$MemFreezeShellCmd extends android.os.ShellCommand {
	 /* .source "MemoryFreezeStubImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/MemoryFreezeStubImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "MemFreezeShellCmd" */
} // .end annotation
/* # instance fields */
com.android.server.am.MemoryFreezeStubImpl mMemoryFreezeStubImpl;
final com.android.server.am.MemoryFreezeStubImpl this$0; //synthetic
/* # direct methods */
public com.android.server.am.MemoryFreezeStubImpl$MemFreezeShellCmd ( ) {
/* .locals 0 */
/* .param p2, "memoryFreezeStubImpl" # Lcom/android/server/am/MemoryFreezeStubImpl; */
/* .line 716 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/os/ShellCommand;-><init>()V */
/* .line 717 */
this.mMemoryFreezeStubImpl = p2;
/* .line 718 */
return;
} // .end method
/* # virtual methods */
public Integer onCommand ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "cmd" # Ljava/lang/String; */
/* .line 722 */
/* if-nez p1, :cond_0 */
/* .line 723 */
v0 = (( com.android.server.am.MemoryFreezeStubImpl$MemFreezeShellCmd ) p0 ).handleDefaultCommands ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/MemoryFreezeStubImpl$MemFreezeShellCmd;->handleDefaultCommands(Ljava/lang/String;)I
/* .line 725 */
} // :cond_0
(( com.android.server.am.MemoryFreezeStubImpl$MemFreezeShellCmd ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemoryFreezeStubImpl$MemFreezeShellCmd;->getOutPrintWriter()Ljava/io/PrintWriter;
/* .line 727 */
/* .local v0, "pw":Ljava/io/PrintWriter; */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
v2 = (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
/* sparse-switch v2, :sswitch_data_0 */
} // :cond_1
/* :sswitch_0 */
final String v2 = "dump"; // const-string v2, "dump"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* move v2, v1 */
/* :sswitch_1 */
final String v2 = "enable"; // const-string v2, "enable"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
	 int v2 = 1; // const/4 v2, 0x1
} // :goto_0
int v2 = -1; // const/4 v2, -0x1
} // :goto_1
/* packed-switch v2, :pswitch_data_0 */
/* .line 739 */
v1 = (( com.android.server.am.MemoryFreezeStubImpl$MemFreezeShellCmd ) p0 ).handleDefaultCommands ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/MemoryFreezeStubImpl$MemFreezeShellCmd;->handleDefaultCommands(Ljava/lang/String;)I
/* .line 733 */
/* :pswitch_0 */
(( com.android.server.am.MemoryFreezeStubImpl$MemFreezeShellCmd ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemoryFreezeStubImpl$MemFreezeShellCmd;->getNextArgRequired()Ljava/lang/String;
v2 = java.lang.Boolean .parseBoolean ( v2 );
/* .line 734 */
/* .local v2, "enable":Z */
v3 = this.this$0;
com.android.server.am.MemoryFreezeStubImpl .-$$Nest$fputmEnable ( v3,v2 );
/* .line 735 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "memory freeze enabled: "; // const-string v4, "memory freeze enabled: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v0 ).println ( v3 ); // invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 736 */
/* .line 729 */
} // .end local v2 # "enable":Z
/* :pswitch_1 */
v2 = this.mMemoryFreezeStubImpl;
com.android.server.am.MemoryFreezeStubImpl .-$$Nest$mdump ( v2,v0 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 730 */
/* nop */
/* .line 744 */
} // :goto_2
/* .line 739 */
} // :goto_3
/* .line 741 */
/* :catch_0 */
/* move-exception v2 */
/* .line 742 */
/* .local v2, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Error occurred.Check logcat for details."; // const-string v4, "Error occurred.Check logcat for details."
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v2 ).getMessage ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v0 ).println ( v3 ); // invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 743 */
final String v3 = "MFZ"; // const-string v3, "MFZ"
final String v4 = "Error running shell command"; // const-string v4, "Error running shell command"
android.util.Slog .e ( v3,v4,v2 );
/* .line 745 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_4
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x4d6ada7d -> :sswitch_1 */
/* 0x2f39f4 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void onHelp ( ) {
/* .locals 3 */
/* .line 750 */
(( com.android.server.am.MemoryFreezeStubImpl$MemFreezeShellCmd ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemoryFreezeStubImpl$MemFreezeShellCmd;->getOutPrintWriter()Ljava/io/PrintWriter;
/* .line 751 */
/* .local v0, "pw":Ljava/io/PrintWriter; */
final String v1 = "Memory Freeze commands:"; // const-string v1, "Memory Freeze commands:"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 752 */
final String v1 = " help"; // const-string v1, " help"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 753 */
final String v1 = " Print this help text."; // const-string v1, " Print this help text."
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 754 */
final String v1 = ""; // const-string v1, ""
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 755 */
final String v2 = " dump"; // const-string v2, " dump"
(( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 756 */
final String v2 = " Print current app memory freeze info."; // const-string v2, " Print current app memory freeze info."
(( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 757 */
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 758 */
final String v2 = " enable [true|false]"; // const-string v2, " enable [true|false]"
(( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 759 */
final String v2 = " Enable/Disable memory freeze."; // const-string v2, " Enable/Disable memory freeze."
(( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 760 */
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 761 */
final String v1 = " debug [true|false]"; // const-string v1, " debug [true|false]"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 762 */
final String v1 = " Enable/Disable debug config."; // const-string v1, " Enable/Disable debug config."
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 763 */
return;
} // .end method
