.class Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;
.super Landroid/os/Handler;
.source "PreloadAppControllerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/PreloadAppControllerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PreloadHandler"
.end annotation


# static fields
.field static final MSG_ENQUEUE_PRELOAD_APP_REQUEST:I = 0x1

.field static final MSG_EXECUTE_PRELOAD_APP:I = 0x2

.field static final MSG_PRELOAD_APP_START:I = 0x5

.field static final MSG_REGIST_PRELOAD_CALLBACK:I = 0x3

.field static final MSG_UNREGIST_PRELOAD_CALLBACK:I = 0x4


# instance fields
.field private mCallBacks:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lmiui/process/IPreloadCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mPendingWork:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Lcom/android/server/wm/PreloadLifecycle;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/server/am/PreloadAppControllerImpl;


# direct methods
.method public constructor <init>(Lcom/android/server/am/PreloadAppControllerImpl;Landroid/os/Looper;)V
    .locals 2
    .param p1, "this$0"    # Lcom/android/server/am/PreloadAppControllerImpl;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 534
    iput-object p1, p0, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->this$0:Lcom/android/server/am/PreloadAppControllerImpl;

    .line 535
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 525
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->mPendingWork:Ljava/util/LinkedList;

    .line 526
    new-instance v0, Landroid/util/SparseArray;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->mCallBacks:Landroid/util/SparseArray;

    .line 536
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13
    .param p1, "msg"    # Landroid/os/Message;

    .line 540
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x2

    const-string v2, "PreloadAppControllerImpl"

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_5

    .line 596
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->mCallBacks:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    .line 597
    .local v0, "size":I
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    .line 598
    .local v1, "packageName":Ljava/lang/String;
    iget v3, p1, Landroid/os/Message;->arg1:I

    .line 599
    .local v3, "pid":I
    iget v4, p1, Landroid/os/Message;->arg2:I

    .line 600
    .local v4, "type":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v0, :cond_7

    .line 601
    iget-object v6, p0, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->mCallBacks:Landroid/util/SparseArray;

    invoke-virtual {v6, v5}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v6

    if-eq v4, v6, :cond_0

    .line 602
    goto :goto_1

    .line 606
    :cond_0
    :try_start_0
    iget-object v6, p0, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->mCallBacks:Landroid/util/SparseArray;

    invoke-virtual {v6, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lmiui/process/IPreloadCallback;

    invoke-interface {v6, v1, v3}, Lmiui/process/IPreloadCallback;->onPreloadAppStarted(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 611
    goto :goto_1

    .line 607
    :catch_0
    move-exception v6

    .line 608
    .local v6, "e":Landroid/os/RemoteException;
    sget-boolean v7, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    if-eqz v7, :cond_1

    .line 609
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onPreloadAppStarted callback fail, type "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->mCallBacks:Landroid/util/SparseArray;

    invoke-virtual {v8, v5}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    .end local v6    # "e":Landroid/os/RemoteException;
    :cond_1
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 593
    .end local v0    # "size":I
    .end local v1    # "packageName":Ljava/lang/String;
    .end local v3    # "pid":I
    .end local v4    # "type":I
    .end local v5    # "i":I
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->mCallBacks:Landroid/util/SparseArray;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->delete(I)V

    .line 594
    goto/16 :goto_5

    .line 590
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->mCallBacks:Landroid/util/SparseArray;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lmiui/process/IPreloadCallback;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 591
    goto/16 :goto_5

    .line 552
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->mPendingWork:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 553
    return-void

    .line 555
    :cond_2
    iget-object v0, p0, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->mPendingWork:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/PreloadLifecycle;

    .line 556
    .local v0, "lifecycle":Lcom/android/server/wm/PreloadLifecycle;
    const-wide/16 v3, 0x0

    .line 557
    .local v3, "timeout":J
    if-eqz v0, :cond_6

    .line 558
    iget-object v5, p0, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->this$0:Lcom/android/server/am/PreloadAppControllerImpl;

    invoke-static {v5, v0}, Lcom/android/server/am/PreloadAppControllerImpl;->-$$Nest$mstartPreloadApp(Lcom/android/server/am/PreloadAppControllerImpl;Lcom/android/server/wm/PreloadLifecycle;)I

    move-result v5

    .line 559
    .local v5, "res":I
    invoke-static {v5}, Landroid/app/ActivityManager;->isStartResultSuccessful(I)Z

    move-result v6

    const-string v7, " res="

    if-eqz v6, :cond_3

    .line 560
    invoke-virtual {v0}, Lcom/android/server/wm/PreloadLifecycle;->getPreloadNextTimeout()J

    move-result-wide v3

    .line 561
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/android/server/wm/PreloadLifecycle;->setAlreadyPreloaded(Z)V

    .line 562
    sget-boolean v6, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    if-eqz v6, :cond_4

    .line 563
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "preloadApp SUCCESS preload:"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 564
    invoke-virtual {v0}, Lcom/android/server/wm/PreloadLifecycle;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 563
    invoke-static {v2, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 567
    :cond_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "preloadApp failed preload:"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 568
    invoke-virtual {v0}, Lcom/android/server/wm/PreloadLifecycle;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 567
    invoke-static {v2, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 569
    iget-object v6, p0, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->this$0:Lcom/android/server/am/PreloadAppControllerImpl;

    invoke-virtual {v0}, Lcom/android/server/wm/PreloadLifecycle;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/server/am/PreloadAppControllerImpl;->-$$Nest$mremovePreloadAppInfo(Lcom/android/server/am/PreloadAppControllerImpl;Ljava/lang/String;)Lcom/android/server/wm/PreloadLifecycle;

    .line 572
    :cond_4
    :goto_2
    invoke-virtual {v0}, Lcom/android/server/wm/PreloadLifecycle;->getDisplayId()I

    move-result v6

    .line 573
    .local v6, "displayId":I
    invoke-virtual {v0}, Lcom/android/server/wm/PreloadLifecycle;->getPreloadType()I

    move-result v7

    .line 574
    .local v7, "type":I
    shl-int/lit8 v8, v6, 0x8

    or-int/2addr v7, v8

    .line 575
    iget-object v8, p0, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->mCallBacks:Landroid/util/SparseArray;

    invoke-virtual {v8}, Landroid/util/SparseArray;->size()I

    move-result v8

    .line 576
    .local v8, "size":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_3
    if-ge v9, v8, :cond_6

    .line 578
    :try_start_1
    iget-object v10, p0, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->mCallBacks:Landroid/util/SparseArray;

    invoke-virtual {v10, v9}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lmiui/process/IPreloadCallback;

    invoke-virtual {v0}, Lcom/android/server/wm/PreloadLifecycle;->getPreloadType()I

    move-result v11

    invoke-interface {v10, v11, v5}, Lmiui/process/IPreloadCallback;->onPreloadComplete(II)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 583
    goto :goto_4

    .line 579
    :catch_1
    move-exception v10

    .line 580
    .local v10, "e":Landroid/os/RemoteException;
    sget-boolean v11, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    if-eqz v11, :cond_5

    .line 581
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "complete callback fail, type "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->mCallBacks:Landroid/util/SparseArray;

    invoke-virtual {v12, v9}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v2, v11}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 576
    .end local v10    # "e":Landroid/os/RemoteException;
    :cond_5
    :goto_4
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 587
    .end local v5    # "res":I
    .end local v6    # "displayId":I
    .end local v7    # "type":I
    .end local v8    # "size":I
    .end local v9    # "i":I
    :cond_6
    invoke-virtual {p0, v1, v3, v4}, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 588
    goto :goto_5

    .line 542
    .end local v0    # "lifecycle":Lcom/android/server/wm/PreloadLifecycle;
    .end local v3    # "timeout":J
    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/wm/PreloadLifecycle;

    .line 543
    .local v0, "preloadLifecycle":Lcom/android/server/wm/PreloadLifecycle;
    iget-object v2, p0, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->mPendingWork:Ljava/util/LinkedList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 544
    iget-object v2, p0, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->mPendingWork:Ljava/util/LinkedList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 546
    invoke-virtual {p0, v1}, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->hasMessages(I)Z

    move-result v2

    if-nez v2, :cond_7

    .line 547
    invoke-virtual {p0, v1}, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->sendEmptyMessage(I)Z

    .line 615
    .end local v0    # "preloadLifecycle":Lcom/android/server/wm/PreloadLifecycle;
    :cond_7
    :goto_5
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
