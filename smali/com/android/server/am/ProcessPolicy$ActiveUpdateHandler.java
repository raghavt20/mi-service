class com.android.server.am.ProcessPolicy$ActiveUpdateHandler extends android.os.Handler {
	 /* .source "ProcessPolicy.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/ProcessPolicy; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "ActiveUpdateHandler" */
} // .end annotation
/* # instance fields */
final com.android.server.am.ProcessPolicy this$0; //synthetic
/* # direct methods */
public com.android.server.am.ProcessPolicy$ActiveUpdateHandler ( ) {
/* .locals 2 */
/* .param p1, "this$0" # Lcom/android/server/am/ProcessPolicy; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 295 */
this.this$0 = p1;
/* .line 296 */
int v0 = 0; // const/4 v0, 0x0
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {p0, p2, v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;Z)V */
/* .line 297 */
return;
} // .end method
private void checkRemoveActiveUid ( com.android.server.am.ProcessPolicy$ActiveUidRecord p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "uidRecord" # Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
/* .param p2, "flag" # I */
/* .line 316 */
if ( p1 != null) { // if-eqz p1, :cond_1
	 /* .line 317 */
	 com.android.server.am.ProcessPolicy .-$$Nest$sfgetsLock ( );
	 /* monitor-enter v0 */
	 /* .line 318 */
	 /* packed-switch p2, :pswitch_data_0 */
	 /* .line 326 */
	 try { // :try_start_0
		 /* monitor-exit v0 */
		 /* .line 323 */
		 /* :pswitch_0 */
		 com.android.server.am.ProcessPolicy .-$$Nest$sfgetsTempInactiveGPSList ( );
		 /* iget v2, p1, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->uid:I */
		 (( android.util.SparseArray ) v1 ).remove ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/SparseArray;->remove(I)V
		 /* .line 324 */
		 /* .line 320 */
		 /* :pswitch_1 */
		 com.android.server.am.ProcessPolicy .-$$Nest$sfgetsTempInactiveAudioList ( );
		 /* iget v2, p1, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->uid:I */
		 (( android.util.SparseArray ) v1 ).remove ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/SparseArray;->remove(I)V
		 /* .line 321 */
		 /* nop */
		 /* .line 329 */
	 } // :goto_0
	 /* iget v1, p1, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->flag:I */
	 /* not-int v2, p2 */
	 /* and-int/2addr v1, v2 */
	 /* iput v1, p1, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->flag:I */
	 /* .line 330 */
	 /* iget v1, p1, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->flag:I */
	 /* if-nez v1, :cond_0 */
	 /* .line 331 */
	 com.android.server.am.ProcessPolicy .-$$Nest$sfgetsActiveUidList ( );
	 /* iget v2, p1, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->uid:I */
	 (( android.util.SparseArray ) v1 ).remove ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/SparseArray;->remove(I)V
	 /* .line 333 */
	 final String v1 = "ProcessManager"; // const-string v1, "ProcessManager"
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v3 = "real remove inactive uid : "; // const-string v3, "real remove inactive uid : "
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v3, p1, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->uid:I */
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v3 = " flag : "; // const-string v3, " flag : "
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v1,v2 );
	 /* .line 336 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 326 */
} // :goto_1
return;
/* .line 336 */
} // :goto_2
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 338 */
} // :cond_1
} // :goto_3
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 2 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 301 */
v0 = this.obj;
/* check-cast v0, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
/* .line 302 */
/* .local v0, "uidRecord":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
/* iget v1, p1, Landroid/os/Message;->what:I */
/* packed-switch v1, :pswitch_data_0 */
/* .line 307 */
/* :pswitch_0 */
int v1 = 2; // const/4 v1, 0x2
/* invoke-direct {p0, v0, v1}, Lcom/android/server/am/ProcessPolicy$ActiveUpdateHandler;->checkRemoveActiveUid(Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;I)V */
/* .line 308 */
/* .line 304 */
/* :pswitch_1 */
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {p0, v0, v1}, Lcom/android/server/am/ProcessPolicy$ActiveUpdateHandler;->checkRemoveActiveUid(Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;I)V */
/* .line 305 */
/* nop */
/* .line 313 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
