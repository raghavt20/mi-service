.class Lcom/android/server/am/MimdManagerServiceImpl$2;
.super Landroid/app/IProcessObserver$Stub;
.source "MimdManagerServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/MimdManagerServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/MimdManagerServiceImpl;


# direct methods
.method constructor <init>(Lcom/android/server/am/MimdManagerServiceImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/MimdManagerServiceImpl;

    .line 318
    iput-object p1, p0, Lcom/android/server/am/MimdManagerServiceImpl$2;->this$0:Lcom/android/server/am/MimdManagerServiceImpl;

    invoke-direct {p0}, Landroid/app/IProcessObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onForegroundActivitiesChanged(IIZ)V
    .locals 3
    .param p1, "pid"    # I
    .param p2, "uid"    # I
    .param p3, "foreground"    # Z

    .line 321
    new-instance v0, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;

    iget-object v1, p0, Lcom/android/server/am/MimdManagerServiceImpl$2;->this$0:Lcom/android/server/am/MimdManagerServiceImpl;

    invoke-direct {v0, v1}, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;-><init>(Lcom/android/server/am/MimdManagerServiceImpl;)V

    .line 322
    .local v0, "info":Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;
    iput p1, v0, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;->pid:I

    .line 323
    iput p2, v0, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;->uid:I

    .line 324
    iput-boolean p3, v0, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;->foreground:Z

    .line 325
    iget-object v1, p0, Lcom/android/server/am/MimdManagerServiceImpl$2;->this$0:Lcom/android/server/am/MimdManagerServiceImpl;

    invoke-static {v1}, Lcom/android/server/am/MimdManagerServiceImpl;->-$$Nest$fgetmMimdManagerHandler(Lcom/android/server/am/MimdManagerServiceImpl;)Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/am/MimdManagerServiceImpl$2;->this$0:Lcom/android/server/am/MimdManagerServiceImpl;

    invoke-static {v2}, Lcom/android/server/am/MimdManagerServiceImpl;->-$$Nest$fgetmMimdManagerHandler(Lcom/android/server/am/MimdManagerServiceImpl;)Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, v0}, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 326
    .local v1, "msg":Landroid/os/Message;
    iget-object v2, p0, Lcom/android/server/am/MimdManagerServiceImpl$2;->this$0:Lcom/android/server/am/MimdManagerServiceImpl;

    invoke-static {v2}, Lcom/android/server/am/MimdManagerServiceImpl;->-$$Nest$fgetmMimdManagerHandler(Lcom/android/server/am/MimdManagerServiceImpl;)Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;->sendMessage(Landroid/os/Message;)Z

    .line 327
    return-void
.end method

.method public onForegroundServicesChanged(III)V
    .locals 0
    .param p1, "pid"    # I
    .param p2, "uid"    # I
    .param p3, "serviceTypes"    # I

    .line 332
    return-void
.end method

.method public onProcessDied(II)V
    .locals 3
    .param p1, "pid"    # I
    .param p2, "uid"    # I

    .line 336
    new-instance v0, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;

    iget-object v1, p0, Lcom/android/server/am/MimdManagerServiceImpl$2;->this$0:Lcom/android/server/am/MimdManagerServiceImpl;

    invoke-direct {v0, v1}, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;-><init>(Lcom/android/server/am/MimdManagerServiceImpl;)V

    .line 337
    .local v0, "info":Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;
    iput p1, v0, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;->pid:I

    .line 338
    iput p2, v0, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;->uid:I

    .line 339
    iget-object v1, p0, Lcom/android/server/am/MimdManagerServiceImpl$2;->this$0:Lcom/android/server/am/MimdManagerServiceImpl;

    invoke-static {v1}, Lcom/android/server/am/MimdManagerServiceImpl;->-$$Nest$fgetmMimdManagerHandler(Lcom/android/server/am/MimdManagerServiceImpl;)Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/am/MimdManagerServiceImpl$2;->this$0:Lcom/android/server/am/MimdManagerServiceImpl;

    invoke-static {v2}, Lcom/android/server/am/MimdManagerServiceImpl;->-$$Nest$fgetmMimdManagerHandler(Lcom/android/server/am/MimdManagerServiceImpl;)Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, 0x3

    invoke-virtual {v1, v2, v0}, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 340
    .local v1, "msg":Landroid/os/Message;
    iget-object v2, p0, Lcom/android/server/am/MimdManagerServiceImpl$2;->this$0:Lcom/android/server/am/MimdManagerServiceImpl;

    invoke-static {v2}, Lcom/android/server/am/MimdManagerServiceImpl;->-$$Nest$fgetmMimdManagerHandler(Lcom/android/server/am/MimdManagerServiceImpl;)Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;->sendMessage(Landroid/os/Message;)Z

    .line 341
    return-void
.end method
