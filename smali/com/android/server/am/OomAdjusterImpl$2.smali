.class Lcom/android/server/am/OomAdjusterImpl$2;
.super Ljava/lang/Object;
.source "OomAdjusterImpl.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/OomAdjusterImpl;->updateResidentAppList(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/miui/server/smartpower/IAppState;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/OomAdjusterImpl;


# direct methods
.method constructor <init>(Lcom/android/server/am/OomAdjusterImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/OomAdjusterImpl;

    .line 508
    iput-object p1, p0, Lcom/android/server/am/OomAdjusterImpl$2;->this$0:Lcom/android/server/am/OomAdjusterImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/miui/server/smartpower/IAppState;Lcom/miui/server/smartpower/IAppState;)I
    .locals 5
    .param p1, "o1"    # Lcom/miui/server/smartpower/IAppState;
    .param p2, "o2"    # Lcom/miui/server/smartpower/IAppState;

    .line 511
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState;->getTotalTimeInForeground()J

    move-result-wide v0

    invoke-virtual {p2}, Lcom/miui/server/smartpower/IAppState;->getTotalTimeInForeground()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 512
    .local v0, "res":J
    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 513
    const/4 v2, -0x1

    return v2

    .line 514
    :cond_0
    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 515
    const/4 v2, 0x0

    return v2

    .line 517
    :cond_1
    const/4 v2, 0x1

    return v2
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 508
    check-cast p1, Lcom/miui/server/smartpower/IAppState;

    check-cast p2, Lcom/miui/server/smartpower/IAppState;

    invoke-virtual {p0, p1, p2}, Lcom/android/server/am/OomAdjusterImpl$2;->compare(Lcom/miui/server/smartpower/IAppState;Lcom/miui/server/smartpower/IAppState;)I

    move-result p1

    return p1
.end method
