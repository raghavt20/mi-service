public class com.android.server.am.MiuiWarnings {
	 /* .source "MiuiWarnings.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/am/MiuiWarnings$NoPreloadHolder;, */
	 /* Lcom/android/server/am/MiuiWarnings$WarningCallback; */
	 /* } */
} // .end annotation
/* # instance fields */
private android.content.Context mContext;
private com.android.server.am.ActivityManagerService mService;
/* # direct methods */
private com.android.server.am.MiuiWarnings ( ) {
	 /* .locals 0 */
	 /* .line 20 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 return;
} // .end method
 com.android.server.am.MiuiWarnings ( ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/am/MiuiWarnings;-><init>()V */
	 return;
} // .end method
private void checkService ( ) {
	 /* .locals 1 */
	 /* .line 41 */
	 v0 = this.mService;
	 /* if-nez v0, :cond_0 */
	 /* .line 42 */
	 final String v0 = "activity"; // const-string v0, "activity"
	 android.os.ServiceManager .getService ( v0 );
	 /* check-cast v0, Lcom/android/server/am/ActivityManagerService; */
	 this.mService = v0;
	 /* .line 44 */
} // :cond_0
return;
} // .end method
public static com.android.server.am.MiuiWarnings getInstance ( ) {
/* .locals 1 */
/* .line 23 */
com.android.server.am.MiuiWarnings$NoPreloadHolder .-$$Nest$sfgetINSTANCE ( );
} // .end method
/* # virtual methods */
public void init ( android.content.Context p0 ) {
/* .locals 0 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 27 */
this.mContext = p1;
/* .line 28 */
return;
} // .end method
public Boolean showWarningDialog ( java.lang.String p0, com.android.server.am.MiuiWarnings$WarningCallback p1 ) {
/* .locals 2 */
/* .param p1, "packageLabel" # Ljava/lang/String; */
/* .param p2, "callback" # Lcom/android/server/am/MiuiWarnings$WarningCallback; */
/* .line 31 */
/* invoke-direct {p0}, Lcom/android/server/am/MiuiWarnings;->checkService()V */
/* .line 32 */
v0 = this.mService;
v0 = this.mAtmInternal;
v0 = (( com.android.server.wm.ActivityTaskManagerInternal ) v0 ).canShowErrorDialogs ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerInternal;->canShowErrorDialogs()Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 33 */
	 /* new-instance v0, Lcom/android/server/am/MiuiWarningDialog; */
	 v1 = this.mContext;
	 /* invoke-direct {v0, p1, v1, p2}, Lcom/android/server/am/MiuiWarningDialog;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/android/server/am/MiuiWarnings$WarningCallback;)V */
	 /* .line 34 */
	 /* .local v0, "dialog":Landroid/app/Dialog; */
	 (( android.app.Dialog ) v0 ).show ( ); // invoke-virtual {v0}, Landroid/app/Dialog;->show()V
	 /* .line 35 */
	 int v1 = 1; // const/4 v1, 0x1
	 /* .line 37 */
} // .end local v0 # "dialog":Landroid/app/Dialog;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
