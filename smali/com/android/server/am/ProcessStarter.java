public class com.android.server.am.ProcessStarter {
	 /* .source "ProcessStarter.java" */
	 /* # static fields */
	 private static final Integer APP_PROTECTION_TIMEOUT;
	 private static final java.lang.String CAMERA_PACKAGE_NAME;
	 public static final Boolean CONFIG_CAM_LOWMEMDEV_RESTART;
	 public static final Boolean CONFIG_CAM_RESTART_3G;
	 public static final Boolean CONFIG_CAM_RESTART_4G;
	 private static final Integer DISABLE_PROC_PROTECT_THRESHOLD;
	 private static final Integer DISABLE_RESTART_PROC_THRESHOLD;
	 private static final Integer MAX_PROTECT_APP;
	 private static final Long PROCESS_RESTART_TIMEOUT;
	 public static final Long REDUCE_DIED_PROC_COUNT_DELAY;
	 private static final Integer SKIP_PRELOAD_COUNT_LIMIT;
	 static final Long SKIP_PRELOAD_KILLED_TIME_OUT;
	 private static final Integer SKIP_RESTART_COUNT_LIMIT;
	 private static final java.lang.String TAG;
	 private static java.util.ArrayList sNoCheckRestartCallerPackages;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/ArrayList<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private static java.util.List sProcRestartCallerWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List sProcRestartWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.List sProtectProcessList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private final com.android.server.am.ActivityManagerService mAms;
private Long mCameraStartTime;
private java.lang.String mForegroundPackageName;
private android.os.Handler mHandler;
public java.util.Map mKilledProcessRecordMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.util.SparseArray mLastProcessesInfo;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/am/ProcessPriorityInfo;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.am.ProcessManagerService mPms;
private java.util.Map mProcessDiedRecordMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.miui.app.smartpower.SmartPowerServiceInternal mSmartPowerService;
private Boolean mSystemReady;
/* # direct methods */
static com.android.server.am.ActivityManagerService -$$Nest$fgetmAms ( com.android.server.am.ProcessStarter p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mAms;
} // .end method
static com.android.server.am.ProcessManagerService -$$Nest$fgetmPms ( com.android.server.am.ProcessStarter p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPms;
} // .end method
static java.util.Map -$$Nest$fgetmProcessDiedRecordMap ( com.android.server.am.ProcessStarter p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mProcessDiedRecordMap;
} // .end method
static com.android.server.am.ProcessRecord -$$Nest$mstartProcessLocked ( com.android.server.am.ProcessStarter p0, java.lang.String p1, java.lang.String p2, Integer p3, java.lang.String p4 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/am/ProcessStarter;->startProcessLocked(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/android/server/am/ProcessRecord; */
} // .end method
static com.android.server.am.ProcessStarter ( ) {
/* .locals 3 */
/* .line 62 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 64 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 94 */
/* nop */
/* .line 95 */
final String v1 = "persist.sys.cam_lowmem_restart"; // const-string v1, "persist.sys.cam_lowmem_restart"
int v2 = 0; // const/4 v2, 0x0
v1 = android.os.SystemProperties .getBoolean ( v1,v2 );
com.android.server.am.ProcessStarter.CONFIG_CAM_LOWMEMDEV_RESTART = (v1!= 0);
/* .line 97 */
/* nop */
/* .line 98 */
final String v1 = "persist.sys.cam_4glowmem_restart"; // const-string v1, "persist.sys.cam_4glowmem_restart"
v1 = android.os.SystemProperties .getBoolean ( v1,v2 );
com.android.server.am.ProcessStarter.CONFIG_CAM_RESTART_4G = (v1!= 0);
/* .line 100 */
/* nop */
/* .line 101 */
final String v1 = "persist.sys.cam_3glowmem_restart"; // const-string v1, "persist.sys.cam_3glowmem_restart"
v1 = android.os.SystemProperties .getBoolean ( v1,v2 );
com.android.server.am.ProcessStarter.CONFIG_CAM_RESTART_3G = (v1!= 0);
/* .line 103 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 105 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 107 */
final String v1 = "com.tencent.mm"; // const-string v1, "com.tencent.mm"
/* .line 108 */
final String v1 = "com.tencent.mm:push"; // const-string v1, "com.tencent.mm:push"
/* .line 109 */
v0 = com.android.server.am.ProcessStarter.sNoCheckRestartCallerPackages;
final String v2 = "Webview"; // const-string v2, "Webview"
(( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 110 */
v0 = com.android.server.am.ProcessStarter.sNoCheckRestartCallerPackages;
final String v2 = "SdkSandbox"; // const-string v2, "SdkSandbox"
(( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 111 */
v0 = com.android.server.am.ProcessStarter.sProcRestartWhiteList;
/* .line 112 */
v0 = com.android.server.am.ProcessStarter.sProcRestartCallerWhiteList;
final String v1 = "com.android.systemui"; // const-string v1, "com.android.systemui"
/* .line 113 */
return;
} // .end method
public com.android.server.am.ProcessStarter ( ) {
/* .locals 1 */
/* .param p1, "pms" # Lcom/android/server/am/ProcessManagerService; */
/* .param p2, "ams" # Lcom/android/server/am/ActivityManagerService; */
/* .param p3, "handler" # Landroid/os/Handler; */
/* .line 115 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 55 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/am/ProcessStarter;->mSystemReady:Z */
/* .line 58 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mKilledProcessRecordMap = v0;
/* .line 60 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mProcessDiedRecordMap = v0;
/* .line 78 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mLastProcessesInfo = v0;
/* .line 90 */
final String v0 = ""; // const-string v0, ""
this.mForegroundPackageName = v0;
/* .line 116 */
this.mPms = p1;
/* .line 117 */
this.mAms = p2;
/* .line 118 */
this.mHandler = p3;
/* .line 119 */
return;
} // .end method
private void increaseRecordCount ( java.lang.String p0, java.util.Map p1 ) {
/* .locals 2 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 394 */
/* .local p2, "recordMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;" */
/* check-cast v0, Ljava/lang/Integer; */
/* .line 395 */
/* .local v0, "expCount":Ljava/lang/Integer; */
/* if-nez v0, :cond_0 */
/* .line 396 */
int v1 = 0; // const/4 v1, 0x0
java.lang.Integer .valueOf ( v1 );
/* .line 398 */
} // :cond_0
v1 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* add-int/lit8 v1, v1, 0x1 */
java.lang.Integer .valueOf ( v1 );
/* move-object v0, v1 */
/* .line 399 */
return;
} // .end method
private Boolean isProcRestartInterceptScenario ( ) {
/* .locals 2 */
/* .line 369 */
com.android.server.am.SystemPressureController .getInstance ( );
v1 = this.mForegroundPackageName;
v0 = (( com.android.server.am.SystemPressureController ) v0 ).isGameApp ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/SystemPressureController;->isGameApp(Ljava/lang/String;)Z
/* if-nez v0, :cond_1 */
v0 = this.mForegroundPackageName;
/* .line 370 */
final String v1 = "com.android.camera"; // const-string v1, "com.android.camera"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 371 */
com.android.server.am.SystemPressureController .getInstance ( );
v0 = (( com.android.server.am.SystemPressureController ) v0 ).isStartingApp ( ); // invoke-virtual {v0}, Lcom/android/server/am/SystemPressureController;->isStartingApp()Z
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 369 */
} // :goto_1
} // .end method
private Boolean isSystemApp ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "flag" # I */
/* .param p2, "uid" # I */
/* .line 363 */
/* and-int/lit16 v0, p1, 0x81 */
/* if-nez v0, :cond_1 */
/* const/16 v0, 0x3e8 */
/* if-ne v0, p2, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
public static java.lang.String makeHostingTypeFromFlag ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "flag" # I */
/* .line 551 */
/* packed-switch p0, :pswitch_data_0 */
/* .line 559 */
/* const-string/jumbo v0, "unknown" */
/* .local v0, "type":Ljava/lang/String; */
/* .line 556 */
} // .end local v0 # "type":Ljava/lang/String;
/* :pswitch_0 */
final String v0 = "FastRestart"; // const-string v0, "FastRestart"
/* .line 557 */
/* .restart local v0 # "type":Ljava/lang/String; */
/* .line 553 */
} // .end local v0 # "type":Ljava/lang/String;
/* :pswitch_1 */
final String v0 = "AI"; // const-string v0, "AI"
/* .line 554 */
/* .restart local v0 # "type":Ljava/lang/String; */
/* nop */
/* .line 562 */
} // :goto_0
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void reduceRecordCountDelay ( java.lang.String p0, java.util.Map p1, Long p2 ) {
/* .locals 2 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .param p3, "delay" # J */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;J)V" */
/* } */
} // .end annotation
/* .line 376 */
/* .local p2, "recordMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;" */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/am/ProcessStarter$3; */
/* invoke-direct {v1, p0, p2, p1}, Lcom/android/server/am/ProcessStarter$3;-><init>(Lcom/android/server/am/ProcessStarter;Ljava/util/Map;Ljava/lang/String;)V */
(( android.os.Handler ) v0 ).postDelayed ( v1, p3, p4 ); // invoke-virtual {v0, v1, p3, p4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 391 */
return;
} // .end method
private com.android.server.am.ProcessRecord startProcessLocked ( java.lang.String p0, java.lang.String p1, Integer p2, java.lang.String p3 ) {
/* .locals 20 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "processName" # Ljava/lang/String; */
/* .param p3, "userId" # I */
/* .param p4, "hostingType" # Ljava/lang/String; */
/* .line 131 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
final String v3 = "ProcessStarter"; // const-string v3, "ProcessStarter"
v4 = android.os.Binder .getCallingPid ( );
/* .line 132 */
/* .local v4, "callingPid":I */
v0 = /* invoke-static/range {p2 ..p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 133 */
/* move-object/from16 v0, p1 */
/* move-object v15, v0 */
} // .end local p2 # "processName":Ljava/lang/String;
/* .local v0, "processName":Ljava/lang/String; */
/* .line 132 */
} // .end local v0 # "processName":Ljava/lang/String;
/* .restart local p2 # "processName":Ljava/lang/String; */
} // :cond_0
/* move-object/from16 v15, p2 */
/* .line 135 */
} // .end local p2 # "processName":Ljava/lang/String;
/* .local v15, "processName":Ljava/lang/String; */
} // :goto_0
int v5 = 0; // const/4 v5, 0x0
/* .line 137 */
/* .local v5, "info":Landroid/content/pm/ApplicationInfo; */
int v14 = 0; // const/4 v14, 0x0
try { // :try_start_0
android.app.AppGlobals .getPackageManager ( );
/* const-wide/16 v6, 0x400 */
/* move/from16 v13, p3 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 142 */
} // .end local v5 # "info":Landroid/content/pm/ApplicationInfo;
/* .local v0, "info":Landroid/content/pm/ApplicationInfo; */
/* nop */
/* .line 143 */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 144 */
v5 = this.mAms;
v5 = this.mProcessList;
(( com.android.server.am.ProcessList ) v5 ).getProcessNamesLOSP ( ); // invoke-virtual {v5}, Lcom/android/server/am/ProcessList;->getProcessNamesLOSP()Lcom/android/server/am/ProcessList$MyProcessMap;
/* iget v6, v0, Landroid/content/pm/ApplicationInfo;->uid:I */
(( com.android.server.am.ProcessList$MyProcessMap ) v5 ).get ( v15, v6 ); // invoke-virtual {v5, v15, v6}, Lcom/android/server/am/ProcessList$MyProcessMap;->get(Ljava/lang/String;I)Ljava/lang/Object;
/* move-object/from16 v16, v5 */
/* check-cast v16, Lcom/android/server/am/ProcessRecord; */
/* .line 145 */
/* .local v16, "app":Lcom/android/server/am/ProcessRecord; */
final String v5 = "process: "; // const-string v5, "process: "
if ( v16 != null) { // if-eqz v16, :cond_2
/* .line 146 */
v6 = /* invoke-virtual/range {v16 ..v16}, Lcom/android/server/am/ProcessRecord;->isPersistent()Z */
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 147 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v15 ); // invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " is persistent, skip!"; // const-string v6, " is persistent, skip!"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v3,v5 );
/* .line 148 */
/* .line 150 */
} // :cond_1
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v15 ); // invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " already exits, just protect"; // const-string v6, " already exits, just protect"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v5 );
/* .line 151 */
/* .line 154 */
} // :cond_2
v6 = this.mPms;
v6 = (( com.android.server.am.ProcessManagerService ) v6 ).isAppDisabledForPkms ( v2 ); // invoke-virtual {v6, v2}, Lcom/android/server/am/ProcessManagerService;->isAppDisabledForPkms(Ljava/lang/String;)Z
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 155 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v15 ); // invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " is disabled or suspend, skip!"; // const-string v6, " is disabled or suspend, skip!"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v3,v5 );
/* .line 156 */
/* .line 158 */
} // :cond_3
v5 = this.mAms;
int v8 = 0; // const/4 v8, 0x0
int v9 = 0; // const/4 v9, 0x0
/* new-instance v10, Lcom/android/server/am/HostingRecord; */
/* move-object/from16 v12, p4 */
/* invoke-direct {v10, v12, v15}, Lcom/android/server/am/HostingRecord;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
int v11 = 0; // const/4 v11, 0x0
/* const/16 v17, 0x0 */
/* const/16 v18, 0x0 */
/* .line 161 */
com.android.server.am.ActivityManagerServiceStub .get ( );
(( com.android.server.am.ActivityManagerServiceStub ) v6 ).getPackageNameByPid ( v4 ); // invoke-virtual {v6, v4}, Lcom/android/server/am/ActivityManagerServiceStub;->getPackageNameByPid(I)Ljava/lang/String;
/* .line 158 */
/* move-object v6, v15 */
/* move-object v7, v0 */
/* move/from16 v12, v17 */
/* move/from16 v13, v18 */
/* move-object v2, v14 */
/* move-object/from16 v14, v19 */
/* invoke-virtual/range {v5 ..v14}, Lcom/android/server/am/ActivityManagerService;->startProcessLocked(Ljava/lang/String;Landroid/content/pm/ApplicationInfo;ZILcom/android/server/am/HostingRecord;IZZLjava/lang/String;)Lcom/android/server/am/ProcessRecord; */
/* .line 163 */
/* .local v5, "newApp":Lcom/android/server/am/ProcessRecord; */
/* if-nez v5, :cond_4 */
/* .line 164 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v7, "startProcess :" */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v15 ); // invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " failed!"; // const-string v7, " failed!"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v3,v6 );
/* .line 165 */
/* .line 167 */
} // :cond_4
v3 = this.mAms;
int v6 = 0; // const/4 v6, 0x0
(( com.android.server.am.ActivityManagerService ) v3 ).updateLruProcessLocked ( v5, v6, v2 ); // invoke-virtual {v3, v5, v6, v2}, Lcom/android/server/am/ActivityManagerService;->updateLruProcessLocked(Lcom/android/server/am/ProcessRecord;ZLcom/android/server/am/ProcessRecord;)V
/* .line 168 */
/* .line 170 */
} // .end local v5 # "newApp":Lcom/android/server/am/ProcessRecord;
} // .end local v16 # "app":Lcom/android/server/am/ProcessRecord;
} // :cond_5
/* move-object v2, v14 */
/* .line 139 */
} // .end local v0 # "info":Landroid/content/pm/ApplicationInfo;
/* .local v5, "info":Landroid/content/pm/ApplicationInfo; */
/* :catch_0 */
/* move-exception v0 */
/* move-object v2, v14 */
/* .line 140 */
/* .local v0, "e":Landroid/os/RemoteException; */
final String v6 = "error in getApplicationInfo!"; // const-string v6, "error in getApplicationInfo!"
android.util.Slog .w ( v3,v6,v0 );
/* .line 141 */
} // .end method
/* # virtual methods */
void addProtectionLocked ( com.android.server.am.ProcessRecord p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "flag" # I */
/* .line 516 */
/* packed-switch p2, :pswitch_data_0 */
/* .line 518 */
/* :pswitch_0 */
v0 = this.mState;
(( com.android.server.am.ProcessStateRecord ) v0 ).setMaxAdj ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessStateRecord;->setMaxAdj(I)V
/* .line 519 */
/* const/16 v0, 0xd */
com.android.server.am.IProcessPolicy .setAppMaxProcState ( p1,v0 );
/* .line 520 */
/* nop */
/* .line 524 */
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void foregroundActivityChanged ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 435 */
v0 = this.mForegroundPackageName;
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 436 */
return;
/* .line 438 */
} // :cond_0
this.mForegroundPackageName = p1;
/* .line 439 */
final String v0 = "com.android.camera"; // const-string v0, "com.android.camera"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 440 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/am/ProcessStarter;->mCameraStartTime:J */
/* .line 442 */
} // :cond_1
return;
} // .end method
public Boolean frequentlyKilledForPreload ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 421 */
v0 = this.mKilledProcessRecordMap;
} // :cond_0
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_3
/* check-cast v1, Ljava/lang/String; */
/* .line 422 */
/* .local v1, "killedProcess":Ljava/lang/String; */
v2 = this.mKilledProcessRecordMap;
/* check-cast v2, Ljava/lang/Integer; */
/* .line 423 */
/* .local v2, "count":Ljava/lang/Integer; */
if ( v2 != null) { // if-eqz v2, :cond_0
v3 = (( java.lang.Integer ) v2 ).intValue ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
/* if-gtz v3, :cond_1 */
/* .line 424 */
/* .line 426 */
} // :cond_1
v3 = (( java.lang.Integer ) v2 ).intValue ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
int v4 = 2; // const/4 v4, 0x2
/* if-lt v3, v4, :cond_2 */
/* .line 427 */
v3 = (( java.lang.String ) v1 ).startsWith ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 428 */
int v0 = 1; // const/4 v0, 0x1
/* .line 430 */
} // .end local v1 # "killedProcess":Ljava/lang/String;
} // .end local v2 # "count":Ljava/lang/Integer;
} // :cond_2
/* .line 431 */
} // :cond_3
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isAllowPreloadProcess ( java.util.List p0, Integer p1 ) {
/* .locals 5 */
/* .param p2, "flag" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lmiui/process/PreloadProcessData;", */
/* ">;I)Z" */
/* } */
} // .end annotation
/* .line 403 */
/* .local p1, "dataList":Ljava/util/List;, "Ljava/util/List<Lmiui/process/PreloadProcessData;>;" */
/* and-int/lit8 v0, p2, 0x1 */
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 404 */
/* .line 405 */
/* .local v0, "dataIterator":Ljava/util/Iterator; */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 406 */
/* check-cast v2, Lmiui/process/PreloadProcessData; */
/* .line 407 */
/* .local v2, "data":Lmiui/process/PreloadProcessData; */
if ( v2 != null) { // if-eqz v2, :cond_0
(( miui.process.PreloadProcessData ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Lmiui/process/PreloadProcessData;->getPackageName()Ljava/lang/String;
v3 = android.text.TextUtils .isEmpty ( v3 );
/* if-nez v3, :cond_0 */
/* .line 408 */
(( miui.process.PreloadProcessData ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Lmiui/process/PreloadProcessData;->getPackageName()Ljava/lang/String;
v3 = (( com.android.server.am.ProcessStarter ) p0 ).frequentlyKilledForPreload ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/am/ProcessStarter;->frequentlyKilledForPreload(Ljava/lang/String;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 409 */
/* .line 410 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "skip start " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( miui.process.PreloadProcessData ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Lmiui/process/PreloadProcessData;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", because of errors or killed by user before"; // const-string v4, ", because of errors or killed by user before"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "ProcessStarter"; // const-string v4, "ProcessStarter"
android.util.Slog .w ( v4,v3 );
/* .line 414 */
} // .end local v2 # "data":Lmiui/process/PreloadProcessData;
} // :cond_0
/* .line 415 */
v2 = } // :cond_1
/* if-lez v2, :cond_2 */
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
} // :goto_1
/* .line 417 */
} // .end local v0 # "dataIterator":Ljava/util/Iterator;
} // :cond_3
} // .end method
public Boolean isAllowRestartProcessLock ( java.lang.String p0, Integer p1, Integer p2, java.lang.String p3, java.lang.String p4, com.android.server.am.HostingRecord p5 ) {
/* .locals 16 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .param p2, "flag" # I */
/* .param p3, "uid" # I */
/* .param p4, "pkgName" # Ljava/lang/String; */
/* .param p5, "callerPackage" # Ljava/lang/String; */
/* .param p6, "hostingRecord" # Lcom/android/server/am/HostingRecord; */
/* .line 222 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
/* move/from16 v2, p2 */
/* move/from16 v3, p3 */
/* move-object/from16 v4, p4 */
/* move-object/from16 v5, p5 */
/* sget-boolean v6, Landroid/os/spc/PressureStateSettings;->PROCESS_RESTART_INTERCEPT_ENABLE:Z */
int v7 = 1; // const/4 v7, 0x1
if ( v6 != null) { // if-eqz v6, :cond_4
/* iget-boolean v6, v0, Lcom/android/server/am/ProcessStarter;->mSystemReady:Z */
if ( v6 != null) { // if-eqz v6, :cond_4
if ( p6 != null) { // if-eqz p6, :cond_4
/* .line 225 */
/* invoke-virtual/range {p6 ..p6}, Lcom/android/server/am/HostingRecord;->getType()Ljava/lang/String; */
final String v8 = "activity"; // const-string v8, "activity"
v6 = (( java.lang.String ) v6 ).contains ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v6, :cond_4 */
/* .line 226 */
v6 = /* invoke-direct/range {p0 ..p0}, Lcom/android/server/am/ProcessStarter;->isProcRestartInterceptScenario()Z */
if ( v6 != null) { // if-eqz v6, :cond_4
/* sget-boolean v6, Lcom/miui/app/smartpower/SmartPowerPolicyConstants;->TESTSUITSPECIFIC:Z */
/* if-nez v6, :cond_4 */
v6 = this.mPms;
/* .line 228 */
v6 = (( com.android.server.am.ProcessManagerService ) v6 ).isAllowAutoStart ( v4, v3 ); // invoke-virtual {v6, v4, v3}, Lcom/android/server/am/ProcessManagerService;->isAllowAutoStart(Ljava/lang/String;I)Z
/* if-nez v6, :cond_4 */
v6 = com.android.server.am.ProcessStarter.sProcRestartWhiteList;
v6 = /* .line 229 */
/* if-nez v6, :cond_4 */
v6 = com.android.server.am.ProcessStarter.sProcRestartCallerWhiteList;
v6 = /* .line 230 */
/* if-nez v6, :cond_4 */
v6 = this.mPms;
/* .line 231 */
(( com.android.server.am.ProcessManagerService ) v6 ).getProcessPolicy ( ); // invoke-virtual {v6}, Lcom/android/server/am/ProcessManagerService;->getProcessPolicy()Lcom/android/server/am/ProcessPolicy;
v6 = (( com.android.server.am.ProcessPolicy ) v6 ).isInProcessStaticWhiteList ( v1 ); // invoke-virtual {v6, v1}, Lcom/android/server/am/ProcessPolicy;->isInProcessStaticWhiteList(Ljava/lang/String;)Z
/* if-nez v6, :cond_4 */
v6 = this.mSmartPowerService;
/* .line 232 */
v6 = /* const/16 v8, 0x1d8 */
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 238 */
} // :cond_0
v6 = (( com.android.server.am.ProcessStarter ) v0 ).isProcessPerceptible ( v5 ); // invoke-virtual {v0, v5}, Lcom/android/server/am/ProcessStarter;->isProcessPerceptible(Ljava/lang/String;)Z
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 239 */
/* .line 243 */
} // :cond_1
final String v6 = "com.android.camera"; // const-string v6, "com.android.camera"
v8 = this.mForegroundPackageName;
v6 = (( java.lang.String ) v6 ).equals ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v8 = 0; // const/4 v8, 0x0
final String v9 = "ProcessStarter"; // const-string v9, "ProcessStarter"
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 244 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v10 */
/* .line 245 */
/* .local v10, "nowTime":J */
/* iget-wide v12, v0, Lcom/android/server/am/ProcessStarter;->mCameraStartTime:J */
/* sub-long v12, v10, v12 */
/* const-wide/16 v14, 0x1388 */
/* cmp-long v6, v12, v14 */
/* if-gez v6, :cond_2 */
/* .line 247 */
v6 = /* invoke-direct {v0, v2, v3}, Lcom/android/server/am/ProcessStarter;->isSystemApp(II)Z */
/* if-nez v6, :cond_2 */
/* .line 248 */
final String v6 = "camera start scenario!"; // const-string v6, "camera start scenario!"
android.util.Slog .d ( v9,v6 );
/* .line 249 */
/* .line 253 */
} // .end local v10 # "nowTime":J
} // :cond_2
v6 = this.mProcessDiedRecordMap;
/* check-cast v6, Ljava/lang/Integer; */
/* .line 254 */
/* .local v6, "procDiedCount":Ljava/lang/Integer; */
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 255 */
v10 = (( java.lang.Integer ) v6 ).intValue ( ); // invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I
int v11 = 3; // const/4 v11, 0x3
/* if-le v10, v11, :cond_3 */
/* .line 256 */
v10 = /* invoke-direct {v0, v2, v3}, Lcom/android/server/am/ProcessStarter;->isSystemApp(II)Z */
/* if-nez v10, :cond_3 */
/* .line 257 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "proc frequent died! proc = "; // const-string v10, "proc frequent died! proc = "
(( java.lang.StringBuilder ) v7 ).append ( v10 ); // invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v1 ); // invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v10 = " callerPkg = "; // const-string v10, " callerPkg = "
(( java.lang.StringBuilder ) v7 ).append ( v10 ); // invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v9,v7 );
/* .line 259 */
/* .line 261 */
} // :cond_3
/* .line 235 */
} // .end local v6 # "procDiedCount":Ljava/lang/Integer;
} // :cond_4
} // :goto_0
} // .end method
public Boolean isProcessPerceptible ( Integer p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .param p2, "pkgName" # Ljava/lang/String; */
/* .line 466 */
v0 = this.mSmartPowerService;
/* .line 467 */
/* .line 468 */
/* .local v0, "runningProcesses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;" */
(( java.util.ArrayList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 469 */
/* .local v2, "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
v3 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v2 ).isProcessPerceptible ( ); // invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->isProcessPerceptible()Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 470 */
int v1 = 1; // const/4 v1, 0x1
/* .line 472 */
} // .end local v2 # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
} // :cond_0
/* .line 473 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean isProcessPerceptible ( java.lang.String p0 ) {
/* .locals 7 */
/* .param p1, "callerPackage" # Ljava/lang/String; */
/* .line 477 */
v0 = this.mSmartPowerService;
/* .line 478 */
/* .local v0, "appList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState;>;" */
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_6
v2 = (( java.util.ArrayList ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 481 */
} // :cond_0
(( java.util.ArrayList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_5
/* check-cast v3, Lcom/miui/server/smartpower/IAppState; */
/* .line 482 */
/* .local v3, "appState":Lcom/miui/server/smartpower/IAppState; */
v4 = com.android.server.am.ProcessStarter.sNoCheckRestartCallerPackages;
v4 = (( java.util.ArrayList ) v4 ).contains ( p1 ); // invoke-virtual {v4, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 483 */
/* .line 485 */
} // :cond_1
(( com.miui.server.smartpower.IAppState ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState;->getPackageName()Ljava/lang/String;
v4 = (( java.lang.String ) v4 ).equals ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v4, :cond_2 */
/* .line 486 */
/* .line 489 */
} // :cond_2
(( com.miui.server.smartpower.IAppState ) v3 ).getRunningProcessList ( ); // invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState;->getRunningProcessList()Ljava/util/ArrayList;
/* .line 490 */
/* .local v2, "runningProcList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;" */
(( java.util.ArrayList ) v2 ).iterator ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v5 = } // :goto_1
if ( v5 != null) { // if-eqz v5, :cond_4
/* check-cast v5, Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 491 */
/* .local v5, "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
v6 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v5 ).isProcessPerceptible ( ); // invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->isProcessPerceptible()Z
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 492 */
/* .line 494 */
} // .end local v5 # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
} // :cond_3
/* .line 495 */
} // :cond_4
/* nop */
/* .line 497 */
} // .end local v2 # "runningProcList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
} // .end local v3 # "appState":Lcom/miui/server/smartpower/IAppState;
} // :cond_5
} // :goto_2
int v1 = 0; // const/4 v1, 0x0
/* .line 479 */
} // :cond_6
} // :goto_3
} // .end method
public void recordDiedProcessIfNeeded ( java.lang.String p0, java.lang.String p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "processName" # Ljava/lang/String; */
/* .param p3, "uid" # I */
/* .line 279 */
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 280 */
v0 = this.mProcessDiedRecordMap;
/* check-cast v0, Ljava/lang/Integer; */
/* .line 281 */
/* .local v0, "count":Ljava/lang/Integer; */
/* if-nez v0, :cond_0 */
/* .line 282 */
int v1 = 0; // const/4 v1, 0x0
java.lang.Integer .valueOf ( v1 );
/* .line 284 */
} // :cond_0
v1 = this.mProcessDiedRecordMap;
v2 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* add-int/lit8 v2, v2, 0x1 */
java.lang.Integer .valueOf ( v2 );
/* move-object v0, v2 */
/* .line 285 */
v1 = this.mProcessDiedRecordMap;
/* const-wide/32 v2, 0x927c0 */
/* invoke-direct {p0, p2, v1, v2, v3}, Lcom/android/server/am/ProcessStarter;->reduceRecordCountDelay(Ljava/lang/String;Ljava/util/Map;J)V */
/* .line 289 */
} // .end local v0 # "count":Ljava/lang/Integer;
} // :cond_1
v0 = v0 = com.android.server.am.ProcessStarter.sProtectProcessList;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 290 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/am/ProcessStarter$1; */
/* invoke-direct {v1, p0, p1, p3, p2}, Lcom/android/server/am/ProcessStarter$1;-><init>(Lcom/android/server/am/ProcessStarter;Ljava/lang/String;ILjava/lang/String;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 310 */
} // :cond_2
return;
} // .end method
public void recordKillProcessIfNeeded ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 266 */
v0 = com.android.server.am.ProcessPolicy.sLowMemKillProcReasons;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/lang/String; */
/* .line 267 */
/* .local v1, "lowMemReason":Ljava/lang/String; */
v2 = (( java.lang.String ) p2 ).contains ( v1 ); // invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 268 */
v2 = this.mKilledProcessRecordMap;
/* invoke-direct {p0, p1, v2}, Lcom/android/server/am/ProcessStarter;->increaseRecordCount(Ljava/lang/String;Ljava/util/Map;)V */
/* .line 271 */
v2 = this.mKilledProcessRecordMap;
/* const-wide/32 v3, 0x493e0 */
/* invoke-direct {p0, p1, v2, v3, v4}, Lcom/android/server/am/ProcessStarter;->reduceRecordCountDelay(Ljava/lang/String;Ljava/util/Map;J)V */
/* .line 274 */
} // .end local v1 # "lowMemReason":Ljava/lang/String;
} // :cond_0
/* .line 276 */
} // :cond_1
return;
} // .end method
public void restartCameraIfNeeded ( java.lang.String p0, java.lang.String p1, Integer p2 ) {
/* .locals 8 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "processName" # Ljava/lang/String; */
/* .param p3, "uid" # I */
/* .line 314 */
final String v0 = "com.android.camera"; // const-string v0, "com.android.camera"
v0 = android.text.TextUtils .equals ( p2,v0 );
/* if-nez v0, :cond_0 */
/* .line 315 */
return;
/* .line 318 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
int v2 = 0; // const/4 v2, 0x0
int v3 = 4; // const/4 v3, 0x4
/* if-gt v0, v3, :cond_1 */
/* move v0, v1 */
} // :cond_1
/* move v0, v2 */
/* .line 319 */
/* .local v0, "isLowMemDevice":Z */
} // :goto_0
int v5 = 3; // const/4 v5, 0x3
/* if-gt v4, v3, :cond_2 */
/* if-le v4, v5, :cond_2 */
/* move v4, v1 */
} // :cond_2
/* move v4, v2 */
/* .line 320 */
/* .local v4, "is4GMemDevice":Z */
} // :goto_1
/* if-gt v6, v5, :cond_3 */
int v6 = 2; // const/4 v6, 0x2
/* if-le v5, v6, :cond_3 */
} // :cond_3
/* move v1, v2 */
/* .line 323 */
/* .local v1, "is3GMemDevice":Z */
} // :goto_2
final String v2 = "ProcessStarter"; // const-string v2, "ProcessStarter"
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 324 */
final String v5 = " as demanded!"; // const-string v5, " as demanded!"
if ( v4 != null) { // if-eqz v4, :cond_5
/* sget-boolean v6, Lcom/android/server/am/ProcessStarter;->CONFIG_CAM_RESTART_4G:Z */
/* if-nez v6, :cond_4 */
/* sget-boolean v6, Lcom/android/server/am/ProcessStarter;->CONFIG_CAM_LOWMEMDEV_RESTART:Z */
if ( v6 != null) { // if-eqz v6, :cond_5
/* .line 325 */
} // :cond_4
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "4G Mem device restart "; // const-string v7, "4G Mem device restart "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p2 ); // invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v5 );
/* .line 326 */
} // :cond_5
if ( v1 != null) { // if-eqz v1, :cond_6
/* sget-boolean v6, Lcom/android/server/am/ProcessStarter;->CONFIG_CAM_RESTART_3G:Z */
if ( v6 != null) { // if-eqz v6, :cond_6
/* .line 327 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "3G Mem device restart "; // const-string v7, "3G Mem device restart "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p2 ); // invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v5 );
/* .line 329 */
} // :cond_6
/* const-string/jumbo v3, "skip restart camera due to lowMemDevice" */
android.util.Slog .d ( v2,v3 );
/* .line 330 */
return;
/* .line 334 */
} // :cond_7
} // :goto_3
v5 = android.os.UserHandle .getUserId ( p3 );
/* .line 335 */
/* .local v5, "userId":I */
v6 = this.mAms;
v6 = this.mUserController;
v3 = (( com.android.server.am.UserController ) v6 ).isUserRunning ( v5, v3 ); // invoke-virtual {v6, v5, v3}, Lcom/android/server/am/UserController;->isUserRunning(II)Z
/* if-nez v3, :cond_8 */
/* .line 336 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "user " */
(( java.lang.StringBuilder ) v3 ).append ( v6 ); // invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = " is still locked.Cannot restart process "; // const-string v6, " is still locked.Cannot restart process "
(( java.lang.StringBuilder ) v3 ).append ( v6 ); // invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v3 );
/* .line 338 */
return;
/* .line 341 */
} // :cond_8
v2 = this.mHandler;
/* new-instance v3, Lcom/android/server/am/ProcessStarter$2; */
/* invoke-direct {v3, p0, p1, p3, p2}, Lcom/android/server/am/ProcessStarter$2;-><init>(Lcom/android/server/am/ProcessStarter;Ljava/lang/String;ILjava/lang/String;)V */
/* const-wide/16 v6, 0x7d0 */
(( android.os.Handler ) v2 ).postDelayed ( v3, v6, v7 ); // invoke-virtual {v2, v3, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 360 */
return;
} // .end method
public Boolean restartDiedAppOrNot ( com.android.server.am.ProcessRecord p0, Boolean p1, Boolean p2 ) {
/* .locals 5 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "isHomeApp" # Z */
/* .param p3, "allowRestart" # Z */
/* .line 445 */
com.android.server.am.SystemPressureController .getInstance ( );
v0 = (( com.android.server.am.SystemPressureController ) v0 ).isCtsModeEnable ( ); // invoke-virtual {v0}, Lcom/android/server/am/SystemPressureController;->isCtsModeEnable()Z
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_7 */
v0 = this.info;
v0 = this.packageName;
/* .line 446 */
v0 = com.miui.server.process.ProcessManagerInternal .checkCtsProcess ( v0 );
/* if-nez v0, :cond_7 */
v0 = this.processName;
/* .line 447 */
v0 = com.miui.server.process.ProcessManagerInternal .checkCtsProcess ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 450 */
} // :cond_0
/* if-nez p2, :cond_6 */
/* iget-boolean v0, p0, Lcom/android/server/am/ProcessStarter;->mSystemReady:Z */
/* if-nez v0, :cond_1 */
/* .line 454 */
} // :cond_1
v0 = this.mState;
v0 = (( com.android.server.am.ProcessStateRecord ) v0 ).getCurProcState ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getCurProcState()I
/* const/16 v2, 0xa */
int v3 = 0; // const/4 v3, 0x0
/* if-gt v0, v2, :cond_5 */
/* iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
/* .line 455 */
v0 = android.os.Process .isIsolated ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 458 */
} // :cond_2
v0 = this.mPms;
v2 = this.info;
v2 = this.packageName;
v4 = this.info;
/* iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I */
v0 = (( com.android.server.am.ProcessManagerService ) v0 ).isAllowAutoStart ( v2, v4 ); // invoke-virtual {v0, v2, v4}, Lcom/android/server/am/ProcessManagerService;->isAllowAutoStart(Ljava/lang/String;I)Z
/* if-nez v0, :cond_4 */
/* iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
v2 = this.info;
v2 = this.packageName;
/* .line 459 */
v0 = (( com.android.server.am.ProcessStarter ) p0 ).isProcessPerceptible ( v0, v2 ); // invoke-virtual {p0, v0, v2}, Lcom/android/server/am/ProcessStarter;->isProcessPerceptible(ILjava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 462 */
} // :cond_3
/* .line 460 */
} // :cond_4
} // :goto_0
/* .line 456 */
} // :cond_5
} // :goto_1
/* .line 451 */
} // :cond_6
} // :goto_2
/* .line 448 */
} // :cond_7
} // :goto_3
} // .end method
void restoreLastProcessesInfoLocked ( Integer p0 ) {
/* .locals 6 */
/* .param p1, "flag" # I */
/* .line 528 */
v0 = this.mLastProcessesInfo;
(( android.util.SparseArray ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Ljava/util/List; */
/* .line 529 */
/* .local v0, "lastProcessInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessPriorityInfo;>;" */
v1 = if ( v0 != null) { // if-eqz v0, :cond_2
/* if-nez v1, :cond_2 */
/* .line 530 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
v2 = } // :goto_0
/* if-ge v1, v2, :cond_1 */
/* .line 531 */
/* check-cast v2, Lcom/android/server/am/ProcessPriorityInfo; */
/* .line 532 */
/* .local v2, "process":Lcom/android/server/am/ProcessPriorityInfo; */
v3 = this.mPms;
(( com.android.server.am.ProcessManagerService ) v3 ).getProcessPolicy ( ); // invoke-virtual {v3}, Lcom/android/server/am/ProcessManagerService;->getProcessPolicy()Lcom/android/server/am/ProcessPolicy;
v4 = this.app;
v4 = this.processName;
v5 = this.app;
/* iget v5, v5, Lcom/android/server/am/ProcessRecord;->userId:I */
/* .line 533 */
v3 = (( com.android.server.am.ProcessPolicy ) v3 ).isLockedApplication ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Lcom/android/server/am/ProcessPolicy;->isLockedApplication(Ljava/lang/String;I)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 534 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "user: " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.app;
/* iget v4, v4, Lcom/android/server/am/ProcessRecord;->userId:I */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ", packageName: "; // const-string v4, ", packageName: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.app;
v4 = this.processName;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " was Locked."; // const-string v4, " was Locked."
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "ProcessStarter"; // const-string v4, "ProcessStarter"
android.util.Slog .i ( v4,v3 );
/* .line 536 */
v3 = this.app;
v3 = this.mState;
(( com.android.server.am.ProcessStateRecord ) v3 ).setMaxAdj ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/am/ProcessStateRecord;->setMaxAdj(I)V
/* .line 537 */
v3 = this.app;
com.android.server.am.IProcessPolicy .setAppMaxProcState ( v3,v4 );
/* .line 541 */
} // :cond_0
v3 = this.app;
v3 = this.mState;
/* iget v4, v2, Lcom/android/server/am/ProcessPriorityInfo;->maxAdj:I */
(( com.android.server.am.ProcessStateRecord ) v3 ).setMaxAdj ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/am/ProcessStateRecord;->setMaxAdj(I)V
/* .line 542 */
v3 = this.app;
/* iget v4, v2, Lcom/android/server/am/ProcessPriorityInfo;->maxProcState:I */
com.android.server.am.IProcessPolicy .setAppMaxProcState ( v3,v4 );
/* .line 530 */
} // .end local v2 # "process":Lcom/android/server/am/ProcessPriorityInfo;
} // :goto_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 545 */
} // .end local v1 # "i":I
} // :cond_1
/* .line 547 */
} // :cond_2
return;
} // .end method
void saveProcessInfoLocked ( com.android.server.am.ProcessRecord p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "flag" # I */
/* .line 502 */
/* new-instance v0, Lcom/android/server/am/ProcessPriorityInfo; */
/* invoke-direct {v0}, Lcom/android/server/am/ProcessPriorityInfo;-><init>()V */
/* .line 503 */
/* .local v0, "lastProcess":Lcom/android/server/am/ProcessPriorityInfo; */
this.app = p1;
/* .line 504 */
v1 = this.mState;
v1 = (( com.android.server.am.ProcessStateRecord ) v1 ).getMaxAdj ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessStateRecord;->getMaxAdj()I
/* iput v1, v0, Lcom/android/server/am/ProcessPriorityInfo;->maxAdj:I */
/* .line 505 */
v1 = com.android.server.am.IProcessPolicy .getAppMaxProcState ( p1 );
/* iput v1, v0, Lcom/android/server/am/ProcessPriorityInfo;->maxProcState:I */
/* .line 506 */
v1 = this.mLastProcessesInfo;
(( android.util.SparseArray ) v1 ).get ( p2 ); // invoke-virtual {v1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Ljava/util/List; */
/* .line 507 */
/* .local v1, "lastProcessList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessPriorityInfo;>;" */
/* if-nez v1, :cond_0 */
/* .line 508 */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* move-object v1, v2 */
/* .line 509 */
v2 = this.mLastProcessesInfo;
(( android.util.SparseArray ) v2 ).put ( p2, v1 ); // invoke-virtual {v2, p2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 511 */
} // :cond_0
/* .line 512 */
return;
} // .end method
public Integer startProcesses ( java.util.List p0, Integer p1, Boolean p2, Integer p3, Integer p4 ) {
/* .locals 15 */
/* .param p2, "startProcessCount" # I */
/* .param p3, "ignoreMemory" # Z */
/* .param p4, "userId" # I */
/* .param p5, "flag" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lmiui/process/PreloadProcessData;", */
/* ">;IZII)I" */
/* } */
} // .end annotation
/* .line 176 */
/* .local p1, "dataList":Ljava/util/List;, "Ljava/util/List<Lmiui/process/PreloadProcessData;>;" */
/* move-object v1, p0 */
/* move/from16 v2, p2 */
/* move/from16 v3, p5 */
v4 = this.mAms;
/* monitor-enter v4 */
/* .line 177 */
int v0 = 0; // const/4 v0, 0x0
/* .line 178 */
/* .local v0, "mColdStartCount":I */
int v5 = 0; // const/4 v5, 0x0
/* .line 179 */
/* .local v5, "protectCount":I */
try { // :try_start_0
(( com.android.server.am.ProcessStarter ) p0 ).restoreLastProcessesInfoLocked ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/am/ProcessStarter;->restoreLastProcessesInfoLocked(I)V
/* .line 180 */
v6 = this.mPms;
v6 = this.mHandler;
(( com.android.server.am.ProcessManagerService$MainHandler ) v6 ).removeMessages ( v3 ); // invoke-virtual {v6, v3}, Lcom/android/server/am/ProcessManagerService$MainHandler;->removeMessages(I)V
/* .line 181 */
/* invoke-interface/range {p1 ..p1}, Ljava/util/List;->iterator()Ljava/util/Iterator; */
v7 = } // :goto_0
if ( v7 != null) { // if-eqz v7, :cond_4
/* check-cast v7, Lmiui/process/PreloadProcessData; */
/* .line 182 */
/* .local v7, "preloadProcessData":Lmiui/process/PreloadProcessData; */
/* if-lt v0, v2, :cond_0 */
/* .line 183 */
/* move/from16 v11, p4 */
/* .line 185 */
} // :cond_0
/* move-object v8, v7 */
/* .line 186 */
/* .local v8, "data":Lmiui/process/PreloadProcessData; */
if ( v8 != null) { // if-eqz v8, :cond_2
(( miui.process.PreloadProcessData ) v8 ).getPackageName ( ); // invoke-virtual {v8}, Lmiui/process/PreloadProcessData;->getPackageName()Ljava/lang/String;
v9 = android.text.TextUtils .isEmpty ( v9 );
/* if-nez v9, :cond_2 */
/* .line 187 */
(( miui.process.PreloadProcessData ) v8 ).getPackageName ( ); // invoke-virtual {v8}, Lmiui/process/PreloadProcessData;->getPackageName()Ljava/lang/String;
/* .line 188 */
/* .local v9, "process":Ljava/lang/String; */
/* nop */
/* .line 189 */
/* invoke-static/range {p5 ..p5}, Lcom/android/server/am/ProcessStarter;->makeHostingTypeFromFlag(I)Ljava/lang/String; */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 188 */
/* move/from16 v11, p4 */
try { // :try_start_1
/* invoke-direct {p0, v9, v9, v11, v10}, Lcom/android/server/am/ProcessStarter;->startProcessLocked(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/android/server/am/ProcessRecord; */
/* .line 190 */
/* .local v10, "app":Lcom/android/server/am/ProcessRecord; */
if ( v10 != null) { // if-eqz v10, :cond_3
/* .line 191 */
(( com.android.server.am.ProcessStarter ) p0 ).saveProcessInfoLocked ( v10, v3 ); // invoke-virtual {p0, v10, v3}, Lcom/android/server/am/ProcessStarter;->saveProcessInfoLocked(Lcom/android/server/am/ProcessRecord;I)V
/* .line 192 */
(( com.android.server.am.ProcessStarter ) p0 ).addProtectionLocked ( v10, v3 ); // invoke-virtual {p0, v10, v3}, Lcom/android/server/am/ProcessStarter;->addProtectionLocked(Lcom/android/server/am/ProcessRecord;I)V
/* .line 193 */
/* add-int/lit8 v5, v5, 0x1 */
/* .line 195 */
int v12 = 5; // const/4 v12, 0x5
/* if-lt v5, v12, :cond_1 */
/* .line 196 */
final String v6 = "ProcessStarter"; // const-string v6, "ProcessStarter"
/* new-instance v12, Ljava/lang/StringBuilder; */
/* invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V */
final String v13 = "preload and protect processes max limit is: 5, while now count is: "; // const-string v13, "preload and protect processes max limit is: 5, while now count is: "
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).append ( v2 ); // invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).toString ( ); // invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v6,v12 );
/* .line 198 */
/* .line 200 */
} // :cond_1
v12 = this.mHandler;
/* const-wide/32 v13, 0x1b7740 */
(( android.os.Handler ) v12 ).sendEmptyMessageDelayed ( v3, v13, v14 ); // invoke-virtual {v12, v3, v13, v14}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 186 */
} // .end local v9 # "process":Ljava/lang/String;
} // .end local v10 # "app":Lcom/android/server/am/ProcessRecord;
} // :cond_2
/* move/from16 v11, p4 */
/* .line 203 */
} // .end local v7 # "preloadProcessData":Lmiui/process/PreloadProcessData;
} // .end local v8 # "data":Lmiui/process/PreloadProcessData;
} // :cond_3
} // :goto_1
/* .line 181 */
} // :cond_4
/* move/from16 v11, p4 */
/* .line 204 */
} // :goto_2
/* monitor-exit v4 */
/* .line 205 */
} // .end local v0 # "mColdStartCount":I
} // .end local v5 # "protectCount":I
/* :catchall_0 */
/* move-exception v0 */
/* move/from16 v11, p4 */
} // :goto_3
/* monitor-exit v4 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* throw v0 */
/* :catchall_1 */
/* move-exception v0 */
} // .end method
public void systemReady ( ) {
/* .locals 3 */
/* .line 122 */
v0 = this.mPms;
(( com.android.server.am.ProcessManagerService ) v0 ).getProcessPolicy ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessManagerService;->getProcessPolicy()Lcom/android/server/am/ProcessPolicy;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.am.ProcessPolicy ) v0 ).getWhiteList ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessPolicy;->getWhiteList(I)Ljava/util/List;
/* .line 123 */
/* .local v0, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v2 = com.android.server.am.ProcessStarter.sProcRestartWhiteList;
/* .line 124 */
v2 = com.android.server.am.ProcessStarter.sProcRestartCallerWhiteList;
/* .line 125 */
/* const-class v2, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
com.android.server.LocalServices .getService ( v2 );
/* check-cast v2, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
this.mSmartPowerService = v2;
/* .line 126 */
/* iput-boolean v1, p0, Lcom/android/server/am/ProcessStarter;->mSystemReady:Z */
/* .line 127 */
return;
} // .end method
