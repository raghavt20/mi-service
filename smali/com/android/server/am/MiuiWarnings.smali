.class public Lcom/android/server/am/MiuiWarnings;
.super Ljava/lang/Object;
.source "MiuiWarnings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/MiuiWarnings$NoPreloadHolder;,
        Lcom/android/server/am/MiuiWarnings$WarningCallback;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mService:Lcom/android/server/am/ActivityManagerService;


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/am/MiuiWarnings-IA;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/MiuiWarnings;-><init>()V

    return-void
.end method

.method private checkService()V
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/android/server/am/MiuiWarnings;->mService:Lcom/android/server/am/ActivityManagerService;

    if-nez v0, :cond_0

    .line 42
    const-string v0, "activity"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/ActivityManagerService;

    iput-object v0, p0, Lcom/android/server/am/MiuiWarnings;->mService:Lcom/android/server/am/ActivityManagerService;

    .line 44
    :cond_0
    return-void
.end method

.method public static getInstance()Lcom/android/server/am/MiuiWarnings;
    .locals 1

    .line 23
    invoke-static {}, Lcom/android/server/am/MiuiWarnings$NoPreloadHolder;->-$$Nest$sfgetINSTANCE()Lcom/android/server/am/MiuiWarnings;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public init(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .line 27
    iput-object p1, p0, Lcom/android/server/am/MiuiWarnings;->mContext:Landroid/content/Context;

    .line 28
    return-void
.end method

.method public showWarningDialog(Ljava/lang/String;Lcom/android/server/am/MiuiWarnings$WarningCallback;)Z
    .locals 2
    .param p1, "packageLabel"    # Ljava/lang/String;
    .param p2, "callback"    # Lcom/android/server/am/MiuiWarnings$WarningCallback;

    .line 31
    invoke-direct {p0}, Lcom/android/server/am/MiuiWarnings;->checkService()V

    .line 32
    iget-object v0, p0, Lcom/android/server/am/MiuiWarnings;->mService:Lcom/android/server/am/ActivityManagerService;

    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mAtmInternal:Lcom/android/server/wm/ActivityTaskManagerInternal;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerInternal;->canShowErrorDialogs()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    new-instance v0, Lcom/android/server/am/MiuiWarningDialog;

    iget-object v1, p0, Lcom/android/server/am/MiuiWarnings;->mContext:Landroid/content/Context;

    invoke-direct {v0, p1, v1, p2}, Lcom/android/server/am/MiuiWarningDialog;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/android/server/am/MiuiWarnings$WarningCallback;)V

    .line 34
    .local v0, "dialog":Landroid/app/Dialog;
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 35
    const/4 v1, 0x1

    return v1

    .line 37
    .end local v0    # "dialog":Landroid/app/Dialog;
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
