.class Lcom/android/server/am/ScoutMemoryError$1;
.super Ljava/lang/Object;
.source "ScoutMemoryError.java"

# interfaces
.implements Lcom/android/server/am/MiuiWarnings$WarningCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/ScoutMemoryError;->showMemoryErrorDialog(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ScoutMemoryError;

.field final synthetic val$app:Lcom/android/server/am/ProcessRecord;

.field final synthetic val$reason:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/server/am/ScoutMemoryError;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/ScoutMemoryError;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 69
    iput-object p1, p0, Lcom/android/server/am/ScoutMemoryError$1;->this$0:Lcom/android/server/am/ScoutMemoryError;

    iput-object p2, p0, Lcom/android/server/am/ScoutMemoryError$1;->val$app:Lcom/android/server/am/ProcessRecord;

    iput-object p3, p0, Lcom/android/server/am/ScoutMemoryError$1;->val$reason:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallback(Z)V
    .locals 3
    .param p1, "positive"    # Z

    .line 72
    const-string v0, "ScoutMemoryError"

    if-eqz p1, :cond_0

    .line 73
    const-string/jumbo v1, "showMemoryErrorDialog ok"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    iget-object v0, p0, Lcom/android/server/am/ScoutMemoryError$1;->this$0:Lcom/android/server/am/ScoutMemoryError;

    iget-object v1, p0, Lcom/android/server/am/ScoutMemoryError$1;->val$app:Lcom/android/server/am/ProcessRecord;

    iget-object v2, p0, Lcom/android/server/am/ScoutMemoryError$1;->val$reason:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/android/server/am/ScoutMemoryError;->scheduleCrashApp(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)Z

    goto :goto_0

    .line 76
    :cond_0
    const-string/jumbo v1, "showMemoryErrorDialog cancel"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    :goto_0
    return-void
.end method
