.class Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
.super Ljava/lang/Object;
.source "MemoryStandardProcessControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/MemoryStandardProcessControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MemoryStandard"
.end annotation


# instance fields
.field public final mParent:Ljava/lang/String;

.field public final mProcesses:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final mPss:J

.field final synthetic this$0:Lcom/android/server/am/MemoryStandardProcessControl;


# direct methods
.method public constructor <init>(Lcom/android/server/am/MemoryStandardProcessControl;JLjava/util/List;Ljava/lang/String;)V
    .locals 0
    .param p2, "pss"    # J
    .param p5, "parent"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1285
    .local p4, "processes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1283
    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    iput-object p1, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;->mProcesses:Ljava/util/Set;

    .line 1286
    iput-wide p2, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;->mPss:J

    .line 1287
    iput-object p5, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;->mParent:Ljava/lang/String;

    .line 1288
    invoke-interface {p1, p4}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1289
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .line 1293
    iget-wide v0, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;->mPss:J

    .line 1294
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;->mProcesses:Ljava/util/Set;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;->mParent:Ljava/lang/String;

    filled-new-array {v0, v1, v2}, [Ljava/lang/Object;

    move-result-object v0

    .line 1293
    const-string/jumbo v1, "{ pss: %d, processes: %s, parent: %s }"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
