public class com.android.server.am.AppStateManager$AppState$RunningProcess extends com.miui.server.smartpower.IAppState$IRunningProcess {
	 /* .source "AppStateManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/AppStateManager$AppState; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x1 */
/* name = "RunningProcess" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;, */
/* Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;, */
/* Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord; */
/* } */
} // .end annotation
/* # instance fields */
private java.lang.String LOG_TAG;
private Integer mAdj;
private java.lang.String mAdjType;
private Integer mAmsProcState;
private Boolean mAudioActive;
private Integer mAudioBehavier;
private Long mBackgroundCount;
private Long mBackgroundDuration;
private Boolean mBleActive;
private Integer mCamBehavier;
private Boolean mCameraActive;
public final java.util.concurrent.atomic.AtomicLong mCurCpuTime;
private volatile Boolean mDependencyProtected;
private Boolean mDisplayActive;
private Integer mDisplayBehavier;
private Long mForegroundCount;
private Long mForegroundDuration;
private Boolean mGpsActive;
private Integer mGpsBehavier;
private Boolean mHasActivity;
private Boolean mHasFgService;
private Long mHibernatonCount;
private Long mHibernatonDuration;
private Integer mHistoryIndexNext;
private Long mIdleCount;
private Long mIdleDuration;
private Boolean mIs64BitProcess;
private Boolean mIsKilled;
private Boolean mIsMainProc;
public final java.util.concurrent.atomic.AtomicLong mLastCpuTime;
private Long mLastInteractiveTimeMills;
private Long mLastPss;
private Integer mLastRecordState;
private Long mLastStateTimeMills;
private Long mLastSwapPss;
private Integer mLastTaskId;
private Boolean mNetActive;
private Integer mNetBehavier;
private java.lang.String mPackageName;
private Integer mPid;
private com.android.server.am.AppStateManager$AppState$RunningProcess$ProcessPriorityScore mProcPriorityScore;
private java.lang.String mProcessName;
private com.android.server.am.ProcessRecord mProcessRecord;
private final android.util.ArrayMap mProviderDependProcs;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.am.AppStateManager$AppState$RunningProcess$AppPowerResourceCallback mResourcesCallback;
private final android.util.ArrayMap mServiceDependProcs;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mState;
private com.android.server.am.AppStateManager$AppState$RunningProcess$StateChangeRecord mStateChangeHistory;
private final java.lang.Object mStateLock;
private Integer mUid;
private Integer mUserId;
private final android.util.ArraySet mVisibleActivities;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/ref/WeakReference<", */
/* "Lcom/android/server/wm/ActivityRecord;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private final android.util.ArraySet mVisibleWindows;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/ref/WeakReference<", */
/* "Lcom/android/server/policy/WindowManagerPolicy$WindowState;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
final com.android.server.am.AppStateManager$AppState this$1; //synthetic
/* # direct methods */
static java.lang.String -$$Nest$fgetLOG_TAG ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.LOG_TAG;
} // .end method
static Integer -$$Nest$fgetmAdj ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAdj:I */
} // .end method
static Integer -$$Nest$fgetmAmsProcState ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAmsProcState:I */
} // .end method
static Integer -$$Nest$fgetmAudioBehavier ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAudioBehavier:I */
} // .end method
static Integer -$$Nest$fgetmCamBehavier ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mCamBehavier:I */
} // .end method
static Integer -$$Nest$fgetmDisplayBehavier ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mDisplayBehavier:I */
} // .end method
static Integer -$$Nest$fgetmGpsBehavier ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mGpsBehavier:I */
} // .end method
static Boolean -$$Nest$fgetmIs64BitProcess ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIs64BitProcess:Z */
} // .end method
static Long -$$Nest$fgetmLastPss ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastPss:J */
/* return-wide v0 */
} // .end method
static Integer -$$Nest$fgetmNetBehavier ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mNetBehavier:I */
} // .end method
static java.lang.String -$$Nest$fgetmPackageName ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPackageName;
} // .end method
static Integer -$$Nest$fgetmPid ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mPid:I */
} // .end method
static java.lang.String -$$Nest$fgetmProcessName ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mProcessName;
} // .end method
static com.android.server.am.ProcessRecord -$$Nest$fgetmProcessRecord ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mProcessRecord;
} // .end method
static android.util.ArrayMap -$$Nest$fgetmProviderDependProcs ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mProviderDependProcs;
} // .end method
static com.android.server.am.AppStateManager$AppState$RunningProcess$AppPowerResourceCallback -$$Nest$fgetmResourcesCallback ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mResourcesCallback;
} // .end method
static android.util.ArrayMap -$$Nest$fgetmServiceDependProcs ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mServiceDependProcs;
} // .end method
static Integer -$$Nest$fgetmState ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I */
} // .end method
static java.lang.Object -$$Nest$fgetmStateLock ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mStateLock;
} // .end method
static Integer -$$Nest$fgetmUid ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mUid:I */
} // .end method
static Integer -$$Nest$fgetmUserId ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mUserId:I */
} // .end method
static void -$$Nest$fputmAudioActive ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAudioActive:Z */
return;
} // .end method
static void -$$Nest$fputmAudioBehavier ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAudioBehavier:I */
return;
} // .end method
static void -$$Nest$fputmBleActive ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mBleActive:Z */
return;
} // .end method
static void -$$Nest$fputmCamBehavier ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mCamBehavier:I */
return;
} // .end method
static void -$$Nest$fputmCameraActive ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mCameraActive:Z */
return;
} // .end method
static void -$$Nest$fputmDisplayActive ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mDisplayActive:Z */
return;
} // .end method
static void -$$Nest$fputmDisplayBehavier ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mDisplayBehavier:I */
return;
} // .end method
static void -$$Nest$fputmGpsActive ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mGpsActive:Z */
return;
} // .end method
static void -$$Nest$fputmGpsBehavier ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mGpsBehavier:I */
return;
} // .end method
static void -$$Nest$fputmLastTaskId ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastTaskId:I */
return;
} // .end method
static void -$$Nest$fputmNetActive ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mNetActive:Z */
return;
} // .end method
static void -$$Nest$fputmNetBehavier ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mNetBehavier:I */
return;
} // .end method
static void -$$Nest$mbecomeActiveIfNeeded ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->becomeActiveIfNeeded(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$mbecomeInactiveIfNeeded ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->becomeInactiveIfNeeded(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$mcomponentEnd ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->componentEnd(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$mcomponentStart ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->componentStart(Ljava/lang/String;)V */
return;
} // .end method
static java.util.ArrayList -$$Nest$mgetHistoryInfos ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getHistoryInfos(J)Ljava/util/ArrayList; */
} // .end method
static void -$$Nest$mmoveToStateLocked ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, Integer p1, java.lang.String p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->moveToStateLocked(ILjava/lang/String;)V */
return;
} // .end method
static void -$$Nest$monActivityForegroundLocked ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->onActivityForegroundLocked(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$monAddToWhiteList ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->onAddToWhiteList()V */
return;
} // .end method
static void -$$Nest$monBackupChanged ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->onBackupChanged(Z)V */
return;
} // .end method
static void -$$Nest$monBackupServiceAppChanged ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->onBackupServiceAppChanged(Z)V */
return;
} // .end method
static void -$$Nest$monInputMethodShow ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->onInputMethodShow()V */
return;
} // .end method
static void -$$Nest$monMediaKey ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->onMediaKey(J)V */
return;
} // .end method
static void -$$Nest$monRemoveFromWhiteList ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->onRemoveFromWhiteList()V */
return;
} // .end method
static void -$$Nest$monReportPowerFrozenSignal ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->onReportPowerFrozenSignal(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$monSendPendingIntent ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->onSendPendingIntent(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$mprocessKilledLocked ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->processKilledLocked()V */
return;
} // .end method
static void -$$Nest$msendBecomeInactiveMsg ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->sendBecomeInactiveMsg(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$msetActivityVisible ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, com.android.server.wm.ActivityRecord p1, Boolean p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->setActivityVisible(Lcom/android/server/wm/ActivityRecord;Z)V */
return;
} // .end method
static void -$$Nest$msetForeground ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->setForeground(Z)V */
return;
} // .end method
static void -$$Nest$msetWindowVisible ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, com.android.server.policy.WindowManagerPolicy$WindowState p1, android.view.WindowManager$LayoutParams p2, Boolean p3 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->setWindowVisible(Lcom/android/server/policy/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;Z)V */
return;
} // .end method
static void -$$Nest$mupdateProcessInfo ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, com.android.server.am.ProcessRecord p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->updateProcessInfo(Lcom/android/server/am/ProcessRecord;)V */
return;
} // .end method
static void -$$Nest$mupdateProviderDepends ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, com.android.server.am.ProcessRecord p1, Boolean p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->updateProviderDepends(Lcom/android/server/am/ProcessRecord;Z)V */
return;
} // .end method
static void -$$Nest$mupdateServiceDepends ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, com.android.server.am.ProcessRecord p1, Boolean p2, Boolean p3 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->updateServiceDepends(Lcom/android/server/am/ProcessRecord;ZZ)V */
return;
} // .end method
static void -$$Nest$mupdateVisibility ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->updateVisibility()V */
return;
} // .end method
static Boolean -$$Nest$mwhiteListVideoVisible ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->whiteListVideoVisible()Z */
} // .end method
private com.android.server.am.AppStateManager$AppState$RunningProcess ( ) {
/* .locals 5 */
/* .param p1, "this$1" # Lcom/android/server/am/AppStateManager$AppState; */
/* .param p2, "record" # Lcom/android/server/am/ProcessRecord; */
/* .line 1986 */
this.this$1 = p1;
/* invoke-direct {p0, p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;-><init>(Lcom/miui/server/smartpower/IAppState;)V */
/* .line 1912 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mUid:I */
/* .line 1913 */
/* iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mUserId:I */
/* .line 1914 */
/* iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mPid:I */
/* .line 1915 */
/* const-wide/16 v1, -0x1 */
/* iput-wide v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastPss:J */
/* .line 1916 */
/* iput-wide v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastSwapPss:J */
/* .line 1917 */
final String v1 = "SmartPower"; // const-string v1, "SmartPower"
this.LOG_TAG = v1;
/* .line 1922 */
/* const/16 v1, -0x2710 */
/* iput v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAdj:I */
/* .line 1923 */
/* const/16 v1, 0x14 */
/* iput v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAmsProcState:I */
/* .line 1924 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHasActivity:Z */
/* .line 1925 */
/* iput-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHasFgService:Z */
/* .line 1926 */
/* iput-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAudioActive:Z */
/* .line 1927 */
/* iput-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mGpsActive:Z */
/* .line 1928 */
/* iput-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mNetActive:Z */
/* .line 1929 */
/* iput-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mBleActive:Z */
/* .line 1930 */
/* iput-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mCameraActive:Z */
/* .line 1931 */
/* iput-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mDisplayActive:Z */
/* .line 1932 */
/* iput-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIsMainProc:Z */
/* .line 1933 */
/* iput v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAudioBehavier:I */
/* .line 1934 */
/* iput v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mGpsBehavier:I */
/* .line 1935 */
/* iput v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mNetBehavier:I */
/* .line 1936 */
/* iput v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mCamBehavier:I */
/* .line 1937 */
/* iput-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mDependencyProtected:Z */
/* .line 1938 */
/* iput v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mDisplayBehavier:I */
/* .line 1940 */
int v2 = 1; // const/4 v2, 0x1
/* iput-boolean v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIs64BitProcess:Z */
/* .line 1942 */
/* iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I */
/* .line 1943 */
/* new-instance v3, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore; */
/* invoke-direct {v3, p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;-><init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V */
this.mProcPriorityScore = v3;
/* .line 1945 */
/* iput-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIsKilled:Z */
/* .line 1946 */
/* new-instance v3, Ljava/lang/Object; */
/* invoke-direct {v3}, Ljava/lang/Object;-><init>()V */
this.mStateLock = v3;
/* .line 1950 */
/* new-instance v3, Landroid/util/ArrayMap; */
/* invoke-direct {v3}, Landroid/util/ArrayMap;-><init>()V */
this.mServiceDependProcs = v3;
/* .line 1951 */
/* new-instance v3, Landroid/util/ArrayMap; */
/* invoke-direct {v3}, Landroid/util/ArrayMap;-><init>()V */
this.mProviderDependProcs = v3;
/* .line 1953 */
/* new-instance v3, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback; */
int v4 = 0; // const/4 v4, 0x0
/* invoke-direct {v3, p0, v4}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;-><init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback-IA;)V */
this.mResourcesCallback = v3;
/* .line 1955 */
/* new-instance v3, Landroid/util/ArraySet; */
/* invoke-direct {v3}, Landroid/util/ArraySet;-><init>()V */
this.mVisibleWindows = v3;
/* .line 1957 */
/* new-instance v3, Landroid/util/ArraySet; */
/* invoke-direct {v3}, Landroid/util/ArraySet;-><init>()V */
this.mVisibleActivities = v3;
/* .line 1960 */
v3 = com.android.server.am.AppStateManager .-$$Nest$sfgetHISTORY_SIZE ( );
/* new-array v3, v3, [Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord; */
this.mStateChangeHistory = v3;
/* .line 1961 */
/* iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastRecordState:I */
/* .line 1962 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v3 */
/* iput-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastStateTimeMills:J */
/* .line 1963 */
/* iput v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHistoryIndexNext:I */
/* .line 1965 */
/* const-wide/16 v3, 0x0 */
/* iput-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mBackgroundDuration:J */
/* .line 1966 */
/* iput-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mForegroundDuration:J */
/* .line 1967 */
/* iput-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIdleDuration:J */
/* .line 1968 */
/* iput-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHibernatonDuration:J */
/* .line 1970 */
/* iput-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mBackgroundCount:J */
/* .line 1971 */
/* iput-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mForegroundCount:J */
/* .line 1972 */
/* iput-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIdleCount:J */
/* .line 1973 */
/* iput-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHibernatonCount:J */
/* .line 1974 */
/* iput-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastInteractiveTimeMills:J */
/* .line 1975 */
/* iput v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastTaskId:I */
/* .line 1980 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicLong; */
/* invoke-direct {v0, v3, v4}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V */
this.mLastCpuTime = v0;
/* .line 1985 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicLong; */
/* invoke-direct {v0, v3, v4}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V */
this.mCurCpuTime = v0;
/* .line 1987 */
/* invoke-direct {p0, p2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->updateProcessInfo(Lcom/android/server/am/ProcessRecord;)V */
/* .line 1988 */
v0 = com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmForeground ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1989 */
/* invoke-direct {p0, v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->setForeground(Z)V */
/* .line 1991 */
} // :cond_0
/* invoke-direct {p0, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->setForeground(Z)V */
/* .line 1993 */
} // :goto_0
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I */
/* iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastRecordState:I */
/* .line 1994 */
return;
} // .end method
 com.android.server.am.AppStateManager$AppState$RunningProcess ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;-><init>(Lcom/android/server/am/AppStateManager$AppState;Lcom/android/server/am/ProcessRecord;)V */
return;
} // .end method
private void addToHistory ( com.android.server.am.AppStateManager$AppState$RunningProcess$StateChangeRecord p0 ) {
/* .locals 5 */
/* .param p1, "record" # Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord; */
/* .line 2584 */
v0 = this.mStateChangeHistory;
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHistoryIndexNext:I */
/* aput-object p1, v0, v1 */
/* .line 2585 */
int v0 = 1; // const/4 v0, 0x1
v2 = com.android.server.am.AppStateManager .-$$Nest$sfgetHISTORY_SIZE ( );
v0 = com.android.server.am.AppStateManager .-$$Nest$smringAdvance ( v1,v0,v2 );
/* iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHistoryIndexNext:I */
/* .line 2586 */
/* sget-boolean v0, Lcom/android/server/am/AppStateManager;->DEBUG:Z */
/* if-nez v0, :cond_0 */
v0 = com.android.server.am.AppStateManager$AppState$RunningProcess$StateChangeRecord .-$$Nest$fgetmNewState ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2587 */
v0 = this.LOG_TAG;
(( com.android.server.am.AppStateManager$AppState$RunningProcess$StateChangeRecord ) p1 ).toString ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 2590 */
} // :cond_0
v0 = com.android.server.am.AppStateManager$AppState$RunningProcess$StateChangeRecord .-$$Nest$fgetmOldState ( p1 );
int v1 = 6; // const/4 v1, 0x6
/* if-ne v0, v1, :cond_1 */
/* .line 2591 */
/* iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIdleDuration:J */
com.android.server.am.AppStateManager$AppState$RunningProcess$StateChangeRecord .-$$Nest$fgetmInterval ( p1 );
/* move-result-wide v2 */
/* add-long/2addr v0, v2 */
/* iput-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIdleDuration:J */
/* .line 2592 */
} // :cond_1
v0 = com.android.server.am.AppStateManager$AppState$RunningProcess$StateChangeRecord .-$$Nest$fgetmOldState ( p1 );
int v1 = 7; // const/4 v1, 0x7
/* if-ne v0, v1, :cond_2 */
/* .line 2593 */
/* iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHibernatonDuration:J */
com.android.server.am.AppStateManager$AppState$RunningProcess$StateChangeRecord .-$$Nest$fgetmInterval ( p1 );
/* move-result-wide v2 */
/* add-long/2addr v0, v2 */
/* iput-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHibernatonDuration:J */
/* .line 2594 */
} // :cond_2
v0 = com.android.server.am.AppStateManager$AppState$RunningProcess$StateChangeRecord .-$$Nest$fgetmOldState ( p1 );
int v1 = 3; // const/4 v1, 0x3
/* if-lez v0, :cond_3 */
v0 = com.android.server.am.AppStateManager$AppState$RunningProcess$StateChangeRecord .-$$Nest$fgetmOldState ( p1 );
/* if-ge v0, v1, :cond_3 */
/* .line 2596 */
/* iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mForegroundDuration:J */
com.android.server.am.AppStateManager$AppState$RunningProcess$StateChangeRecord .-$$Nest$fgetmInterval ( p1 );
/* move-result-wide v2 */
/* add-long/2addr v0, v2 */
/* iput-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mForegroundDuration:J */
/* .line 2597 */
} // :cond_3
v0 = com.android.server.am.AppStateManager$AppState$RunningProcess$StateChangeRecord .-$$Nest$fgetmOldState ( p1 );
/* if-lt v0, v1, :cond_4 */
/* .line 2598 */
/* iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mBackgroundDuration:J */
com.android.server.am.AppStateManager$AppState$RunningProcess$StateChangeRecord .-$$Nest$fgetmInterval ( p1 );
/* move-result-wide v2 */
/* add-long/2addr v0, v2 */
/* iput-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mBackgroundDuration:J */
/* .line 2600 */
} // :cond_4
} // :goto_0
v0 = com.android.server.am.AppStateManager$AppState$RunningProcess$StateChangeRecord .-$$Nest$fgetmNewState ( p1 );
/* const-wide/16 v1, 0x1 */
/* packed-switch v0, :pswitch_data_0 */
/* :pswitch_0 */
/* .line 2605 */
/* :pswitch_1 */
/* iget-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHibernatonCount:J */
/* add-long/2addr v3, v1 */
/* iput-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHibernatonCount:J */
/* .line 2606 */
/* .line 2602 */
/* :pswitch_2 */
/* iget-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIdleCount:J */
/* add-long/2addr v3, v1 */
/* iput-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIdleCount:J */
/* .line 2603 */
/* .line 2608 */
/* :pswitch_3 */
/* iget-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mBackgroundCount:J */
/* add-long/2addr v3, v1 */
/* iput-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mBackgroundCount:J */
/* .line 2609 */
/* .line 2611 */
/* :pswitch_4 */
/* iget-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mForegroundCount:J */
/* add-long/2addr v3, v1 */
/* iput-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mForegroundCount:J */
/* .line 2614 */
} // :goto_1
return;
/* :pswitch_data_0 */
/* .packed-switch 0x2 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
private void becomeActiveIfNeeded ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .line 2470 */
v0 = this.mStateLock;
/* monitor-enter v0 */
/* .line 2471 */
try { // :try_start_0
/* sget-boolean v1, Lcom/android/server/am/AppStateManager;->sEnable:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I */
int v2 = 3; // const/4 v2, 0x3
/* if-le v1, v2, :cond_0 */
/* .line 2472 */
/* invoke-direct {p0, v2, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->moveToStateLocked(ILjava/lang/String;)V */
/* .line 2473 */
/* iget-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mNetActive:Z */
/* if-nez v1, :cond_0 */
/* .line 2474 */
v1 = this.this$1;
com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmHandler ( v1 );
/* new-instance v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$5; */
/* invoke-direct {v2, p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$5;-><init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V */
(( com.android.server.am.AppStateManager$AppState$MyHandler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->post(Ljava/lang/Runnable;)Z
/* .line 2484 */
} // :cond_0
/* monitor-exit v0 */
/* .line 2485 */
return;
/* .line 2484 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void becomeInactiveIfNeeded ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .line 2488 */
v0 = this.mStateLock;
/* monitor-enter v0 */
/* .line 2489 */
try { // :try_start_0
/* sget-boolean v1, Lcom/android/server/am/AppStateManager;->sEnable:Z */
int v2 = 3; // const/4 v2, 0x3
if ( v1 != null) { // if-eqz v1, :cond_b
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I */
/* if-ne v1, v2, :cond_b */
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAdj:I */
/* if-ltz v1, :cond_b */
/* .line 2491 */
/* iget-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAudioActive:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 2492 */
/* sget-boolean v1, Lcom/android/server/am/AppStateManager;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 2493 */
v1 = this.LOG_TAG;
/* const-string/jumbo v2, "skip inactive for audio active" */
android.util.Slog .d ( v1,v2 );
/* .line 2495 */
} // :cond_0
/* monitor-exit v0 */
return;
/* .line 2497 */
} // :cond_1
/* iget-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mBleActive:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 2498 */
/* sget-boolean v1, Lcom/android/server/am/AppStateManager;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 2499 */
v1 = this.LOG_TAG;
/* const-string/jumbo v2, "skip inactive for bluetooth active" */
android.util.Slog .d ( v1,v2 );
/* .line 2501 */
} // :cond_2
/* monitor-exit v0 */
return;
/* .line 2503 */
} // :cond_3
/* iget-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mGpsActive:Z */
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 2504 */
/* sget-boolean v1, Lcom/android/server/am/AppStateManager;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 2505 */
v1 = this.LOG_TAG;
/* const-string/jumbo v2, "skip inactive for GPS active" */
android.util.Slog .d ( v1,v2 );
/* .line 2507 */
} // :cond_4
/* monitor-exit v0 */
return;
/* .line 2509 */
} // :cond_5
/* iget-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mNetActive:Z */
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 2510 */
/* sget-boolean v1, Lcom/android/server/am/AppStateManager;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 2511 */
v1 = this.LOG_TAG;
/* const-string/jumbo v2, "skip inactive for net active" */
android.util.Slog .d ( v1,v2 );
/* .line 2513 */
} // :cond_6
/* monitor-exit v0 */
return;
/* .line 2515 */
} // :cond_7
v1 = this.this$1;
v1 = com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmIsBackupServiceApp ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_9
/* .line 2516 */
/* sget-boolean v1, Lcom/android/server/am/AppStateManager;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 2517 */
v1 = this.LOG_TAG;
/* const-string/jumbo v2, "skip inactive for backing up" */
android.util.Slog .d ( v1,v2 );
/* .line 2519 */
} // :cond_8
/* monitor-exit v0 */
return;
/* .line 2521 */
} // :cond_9
v1 = this.this$1;
v1 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmSmartPowerPolicyManager ( v1 );
v2 = this.mPackageName;
/* iget v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mUid:I */
v1 = (( com.miui.server.smartpower.SmartPowerPolicyManager ) v1 ).needCheckNet ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->needCheckNet(Ljava/lang/String;I)Z
if ( v1 != null) { // if-eqz v1, :cond_a
/* .line 2522 */
v1 = this.this$1;
com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmHandler ( v1 );
/* new-instance v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$6; */
/* invoke-direct {v2, p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$6;-><init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V */
(( com.android.server.am.AppStateManager$AppState$MyHandler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->post(Ljava/lang/Runnable;)Z
/* .line 2531 */
} // :cond_a
int v1 = 4; // const/4 v1, 0x4
/* invoke-direct {p0, v1, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->moveToStateLocked(ILjava/lang/String;)V */
/* .line 2532 */
} // :cond_b
/* sget-boolean v1, Lcom/android/server/am/AppStateManager;->sEnable:Z */
if ( v1 != null) { // if-eqz v1, :cond_c
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I */
/* if-ne v1, v2, :cond_c */
/* .line 2533 */
/* sget-boolean v1, Lcom/android/server/am/AppStateManager;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_c
/* .line 2534 */
v1 = this.LOG_TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "skip inactive state(" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I */
com.android.server.am.AppStateManager .processStateToString ( v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ") adj("; // const-string v3, ") adj("
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAdj:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ") R("; // const-string v3, ") R("
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ")"; // const-string v3, ")"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 2538 */
} // :cond_c
} // :goto_0
/* monitor-exit v0 */
/* .line 2539 */
return;
/* .line 2538 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void componentEnd ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "content" # Ljava/lang/String; */
/* .line 2377 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->sendBecomeInactiveMsg(Ljava/lang/String;)V */
/* .line 2378 */
return;
} // .end method
private void componentStart ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "content" # Ljava/lang/String; */
/* .line 2371 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I */
int v1 = 7; // const/4 v1, 0x7
/* if-ne v0, v1, :cond_0 */
/* .line 2372 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->becomeActiveIfNeeded(Ljava/lang/String;)V */
/* .line 2374 */
} // :cond_0
return;
} // .end method
private java.util.ArrayList getHistoryInfos ( Long p0 ) {
/* .locals 6 */
/* .param p1, "sinceUptime" # J */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(J)", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 2617 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 2618 */
/* .local v0, "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;>;" */
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHistoryIndexNext:I */
v2 = com.android.server.am.AppStateManager .-$$Nest$sfgetHISTORY_SIZE ( );
int v3 = -1; // const/4 v3, -0x1
v1 = com.android.server.am.AppStateManager .-$$Nest$smringAdvance ( v1,v3,v2 );
/* .line 2620 */
/* .local v1, "index":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
v4 = com.android.server.am.AppStateManager .-$$Nest$sfgetHISTORY_SIZE ( );
/* if-ge v2, v4, :cond_1 */
/* .line 2621 */
v4 = this.mStateChangeHistory;
/* aget-object v4, v4, v1 */
if ( v4 != null) { // if-eqz v4, :cond_1
com.android.server.am.AppStateManager$AppState$RunningProcess$StateChangeRecord .-$$Nest$fgetmTimeStamp ( v4 );
/* move-result-wide v4 */
/* cmp-long v4, v4, p1 */
/* if-gez v4, :cond_0 */
/* .line 2623 */
/* .line 2625 */
} // :cond_0
v4 = this.mStateChangeHistory;
/* aget-object v4, v4, v1 */
(( java.util.ArrayList ) v0 ).add ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 2626 */
v4 = com.android.server.am.AppStateManager .-$$Nest$sfgetHISTORY_SIZE ( );
v1 = com.android.server.am.AppStateManager .-$$Nest$smringAdvance ( v1,v3,v4 );
/* .line 2620 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 2628 */
} // .end local v2 # "i":I
} // :cond_1
} // :goto_1
} // .end method
private Boolean hasVisibleActivity ( ) {
/* .locals 2 */
/* .line 2296 */
v0 = this.mVisibleActivities;
/* monitor-enter v0 */
/* .line 2297 */
try { // :try_start_0
v1 = this.mVisibleActivities;
v1 = (( android.util.ArraySet ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/ArraySet;->size()I
/* if-lez v1, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* monitor-exit v0 */
/* .line 2298 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private Boolean hasVisibleWindow ( ) {
/* .locals 2 */
/* .line 2290 */
v0 = this.mVisibleWindows;
/* monitor-enter v0 */
/* .line 2291 */
try { // :try_start_0
v1 = this.mVisibleWindows;
v1 = (( android.util.ArraySet ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/ArraySet;->size()I
/* if-lez v1, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* monitor-exit v0 */
/* .line 2292 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private Boolean is32BitProcess ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 5 */
/* .param p1, "proc" # Lcom/android/server/am/ProcessRecord; */
/* .line 2070 */
v0 = this.info;
v0 = this.primaryCpuAbi;
/* .line 2071 */
/* .local v0, "primaryCpuAbi":Ljava/lang/String; */
v1 = this.info;
v1 = this.secondaryCpuAbi;
/* .line 2072 */
/* .local v1, "secondaryCpuAbi":Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* if-nez v0, :cond_0 */
/* if-nez v1, :cond_0 */
/* .line 2073 */
/* .line 2074 */
} // :cond_0
final String v3 = "arm64"; // const-string v3, "arm64"
if ( v0 != null) { // if-eqz v0, :cond_1
v4 = (( java.lang.String ) v0 ).startsWith ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 2075 */
/* .line 2076 */
} // :cond_1
if ( v1 != null) { // if-eqz v1, :cond_2
v3 = (( java.lang.String ) v1 ).startsWith ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 2077 */
/* .line 2079 */
} // :cond_2
int v2 = 1; // const/4 v2, 0x1
} // .end method
private void moveToStateLocked ( Integer p0, java.lang.String p1 ) {
/* .locals 7 */
/* .param p1, "targetState" # I */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 2542 */
/* sget-boolean v0, Lcom/android/server/am/AppStateManager;->sEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I */
/* if-ne p1, v0, :cond_0 */
/* .line 2545 */
} // :cond_0
v0 = this.this$1;
v0 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmSmartPowerPolicyManager ( v0 );
v0 = (( com.miui.server.smartpower.SmartPowerPolicyManager ) v0 ).isProcessHibernationWhiteList ( p0 ); // invoke-virtual {v0, p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isProcessHibernationWhiteList(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Z
/* .line 2546 */
/* .local v0, "isWhiteList":Z */
int v1 = 7; // const/4 v1, 0x7
/* if-ne p1, v1, :cond_1 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2547 */
return;
/* .line 2549 */
} // :cond_1
final String v1 = "moveToStateLocked"; // const-string v1, "moveToStateLocked"
/* const-wide/32 v2, 0x20000 */
android.os.Trace .traceBegin ( v2,v3,v1 );
/* .line 2550 */
v1 = this.this$1;
v1 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmProcessStateCallbacks ( v1 );
/* monitor-enter v1 */
/* .line 2551 */
try { // :try_start_0
v4 = this.this$1;
v4 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmProcessStateCallbacks ( v4 );
(( java.util.ArrayList ) v4 ).iterator ( ); // invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_2
/* check-cast v5, Lcom/android/server/am/AppStateManager$IProcessStateCallback; */
/* .line 2552 */
/* .local v5, "callback":Lcom/android/server/am/AppStateManager$IProcessStateCallback; */
/* iget v6, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I */
/* .line 2553 */
} // .end local v5 # "callback":Lcom/android/server/am/AppStateManager$IProcessStateCallback;
/* .line 2554 */
} // :cond_2
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 2555 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->stateChangeToRecord(ILjava/lang/String;)V */
/* .line 2556 */
/* iput p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I */
/* .line 2558 */
v1 = this.this$1;
com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmHandler ( v1 );
int v4 = 2; // const/4 v4, 0x2
(( com.android.server.am.AppStateManager$AppState$MyHandler ) v1 ).removeMessages ( v4 ); // invoke-virtual {v1, v4}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->removeMessages(I)V
/* .line 2559 */
v1 = this.this$1;
com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmHandler ( v1 );
(( com.android.server.am.AppStateManager$AppState$MyHandler ) v1 ).obtainMessage ( v4, p2 ); // invoke-virtual {v1, v4, p2}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 2560 */
/* .local v1, "msg":Landroid/os/Message; */
v4 = this.this$1;
com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmHandler ( v4 );
(( com.android.server.am.AppStateManager$AppState$MyHandler ) v4 ).sendMessage ( v1 ); // invoke-virtual {v4, v1}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 2561 */
/* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->reportDependChangedIfNeeded()V */
/* .line 2563 */
android.os.Trace .traceEnd ( v2,v3 );
/* .line 2564 */
return;
/* .line 2554 */
} // .end local v1 # "msg":Landroid/os/Message;
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
/* .line 2543 */
} // .end local v0 # "isWhiteList":Z
} // :cond_3
} // :goto_1
return;
} // .end method
private void onActivityForegroundLocked ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 2431 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "start activity " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2432 */
/* .local v0, "reason":Ljava/lang/String; */
/* invoke-direct {p0, v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->becomeActiveIfNeeded(Ljava/lang/String;)V */
/* .line 2433 */
return;
} // .end method
private void onAddToWhiteList ( ) {
/* .locals 2 */
/* .line 2381 */
/* sget-boolean v0, Lcom/android/server/am/AppStateManager;->DEBUG:Z */
final String v1 = "add to whitelist"; // const-string v1, "add to whitelist"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2382 */
v0 = this.LOG_TAG;
android.util.Slog .w ( v0,v1 );
/* .line 2384 */
} // :cond_0
/* invoke-direct {p0, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->becomeActiveIfNeeded(Ljava/lang/String;)V */
/* .line 2385 */
return;
} // .end method
private void onBackupChanged ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "active" # Z */
/* .line 2348 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 2349 */
final String v0 = "backup start"; // const-string v0, "backup start"
/* invoke-direct {p0, v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->becomeActiveIfNeeded(Ljava/lang/String;)V */
/* .line 2351 */
} // :cond_0
final String v0 = "backup end"; // const-string v0, "backup end"
/* invoke-direct {p0, v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->sendBecomeInactiveMsg(Ljava/lang/String;)V */
/* .line 2353 */
} // :goto_0
return;
} // .end method
private void onBackupServiceAppChanged ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "active" # Z */
/* .line 2356 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 2357 */
final String v0 = "backup service connected"; // const-string v0, "backup service connected"
/* invoke-direct {p0, v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->becomeActiveIfNeeded(Ljava/lang/String;)V */
/* .line 2359 */
} // :cond_0
final String v0 = "backup service disconnected"; // const-string v0, "backup service disconnected"
/* invoke-direct {p0, v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->sendBecomeInactiveMsg(Ljava/lang/String;)V */
/* .line 2361 */
} // :goto_0
/* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->reportDependChangedIfNeeded()V */
/* .line 2362 */
return;
} // .end method
private void onInputMethodShow ( ) {
/* .locals 3 */
/* .line 2339 */
v0 = this.LOG_TAG;
/* const-string/jumbo v1, "show input method" */
android.util.Slog .d ( v0,v1 );
/* .line 2340 */
v0 = this.mStateLock;
/* monitor-enter v0 */
/* .line 2341 */
try { // :try_start_0
v1 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) p0 ).isKilled ( ); // invoke-virtual {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isKilled()Z
/* if-nez v1, :cond_0 */
/* .line 2342 */
/* const-string/jumbo v1, "show input method" */
int v2 = 1; // const/4 v2, 0x1
/* invoke-direct {p0, v2, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->moveToStateLocked(ILjava/lang/String;)V */
/* .line 2344 */
} // :cond_0
/* monitor-exit v0 */
/* .line 2345 */
return;
/* .line 2344 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void onMediaKey ( Long p0 ) {
/* .locals 2 */
/* .param p1, "timeStamp" # J */
/* .line 2333 */
v0 = this.LOG_TAG;
final String v1 = "press media key"; // const-string v1, "press media key"
android.util.Slog .d ( v0,v1 );
/* .line 2334 */
/* iput-wide p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastInteractiveTimeMills:J */
/* .line 2335 */
/* invoke-direct {p0, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->becomeActiveIfNeeded(Ljava/lang/String;)V */
/* .line 2336 */
return;
} // .end method
private void onRemoveFromWhiteList ( ) {
/* .locals 2 */
/* .line 2388 */
/* sget-boolean v0, Lcom/android/server/am/AppStateManager;->DEBUG:Z */
final String v1 = "remove from whitelist"; // const-string v1, "remove from whitelist"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2389 */
v0 = this.LOG_TAG;
android.util.Slog .w ( v0,v1 );
/* .line 2391 */
} // :cond_0
/* invoke-direct {p0, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->sendBecomeInactiveMsg(Ljava/lang/String;)V */
/* .line 2392 */
return;
} // .end method
private void onReportPowerFrozenSignal ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .line 2425 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I */
int v1 = 7; // const/4 v1, 0x7
/* if-ne v0, v1, :cond_0 */
/* .line 2426 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->sendBecomeActiveMsg(Ljava/lang/String;)V */
/* .line 2428 */
} // :cond_0
return;
} // .end method
private void onSendPendingIntent ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "content" # Ljava/lang/String; */
/* .line 2365 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I */
int v1 = 7; // const/4 v1, 0x7
/* if-ne v0, v1, :cond_0 */
/* .line 2366 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->becomeActiveIfNeeded(Ljava/lang/String;)V */
/* .line 2368 */
} // :cond_0
return;
} // .end method
private void processKilledLocked ( ) {
/* .locals 10 */
/* .line 2084 */
v0 = this.mStateLock;
/* monitor-enter v0 */
/* .line 2085 */
try { // :try_start_0
final String v1 = "process died "; // const-string v1, "process died "
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {p0, v2, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->moveToStateLocked(ILjava/lang/String;)V */
/* .line 2086 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 2087 */
/* iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mDependencyProtected:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2088 */
v0 = this.mServiceDependProcs;
/* monitor-enter v0 */
/* .line 2089 */
try { // :try_start_1
v1 = this.mServiceDependProcs;
(( android.util.ArrayMap ) v1 ).values ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo; */
/* .line 2090 */
/* .local v2, "processInfo":Lcom/android/server/am/AppStateManager$AppState$DependProcInfo; */
v3 = this.this$1;
v3 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmSmartPowerPolicyManager ( v3 );
int v5 = 0; // const/4 v5, 0x0
v6 = this.mProcessName;
com.android.server.am.AppStateManager$AppState$DependProcInfo .-$$Nest$fgetmProcessName ( v2 );
v8 = com.android.server.am.AppStateManager$AppState$DependProcInfo .-$$Nest$fgetmUid ( v2 );
v9 = com.android.server.am.AppStateManager$AppState$DependProcInfo .-$$Nest$fgetmPid ( v2 );
/* invoke-virtual/range {v4 ..v9}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->onDependChanged(ZLjava/lang/String;Ljava/lang/String;II)V */
/* .line 2094 */
} // .end local v2 # "processInfo":Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;
/* .line 2095 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
/* .line 2097 */
} // :cond_1
} // :goto_1
/* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->reset()V */
/* .line 2098 */
return;
/* .line 2086 */
/* :catchall_1 */
/* move-exception v1 */
try { // :try_start_2
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* throw v1 */
} // .end method
private void reportDependChangedIfNeeded ( ) {
/* .locals 10 */
/* .line 2241 */
v0 = this.this$1;
v0 = com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmIsBackupServiceApp ( v0 );
/* if-nez v0, :cond_2 */
/* .line 2242 */
v0 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) p0 ).isForeground ( ); // invoke-virtual {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isForeground()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mUid:I */
/* const/16 v1, 0x3e8 */
/* if-ne v0, v1, :cond_0 */
v0 = /* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->hasVisibleActivity()Z */
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_0
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // :cond_2
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 2243 */
/* .local v0, "isConnected":Z */
} // :goto_1
/* iget-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mDependencyProtected:Z */
/* if-eq v0, v1, :cond_5 */
/* .line 2244 */
v7 = this.mServiceDependProcs;
/* monitor-enter v7 */
/* .line 2245 */
try { // :try_start_0
/* iget-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mDependencyProtected:Z */
/* if-eq v0, v1, :cond_4 */
/* .line 2246 */
v1 = this.mServiceDependProcs;
(( android.util.ArrayMap ) v1 ).values ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v1 = } // :goto_2
if ( v1 != null) { // if-eqz v1, :cond_3
/* check-cast v1, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo; */
/* move-object v9, v1 */
/* .line 2247 */
/* .local v9, "processInfo":Lcom/android/server/am/AppStateManager$AppState$DependProcInfo; */
v1 = this.this$1;
v1 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmSmartPowerPolicyManager ( v1 );
v3 = this.mProcessName;
com.android.server.am.AppStateManager$AppState$DependProcInfo .-$$Nest$fgetmProcessName ( v9 );
v5 = com.android.server.am.AppStateManager$AppState$DependProcInfo .-$$Nest$fgetmUid ( v9 );
v6 = com.android.server.am.AppStateManager$AppState$DependProcInfo .-$$Nest$fgetmPid ( v9 );
/* move v2, v0 */
/* invoke-virtual/range {v1 ..v6}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->onDependChanged(ZLjava/lang/String;Ljava/lang/String;II)V */
/* .line 2251 */
} // .end local v9 # "processInfo":Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;
/* .line 2252 */
} // :cond_3
/* iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mDependencyProtected:Z */
/* .line 2254 */
} // :cond_4
/* monitor-exit v7 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v7 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 2256 */
} // :cond_5
} // :goto_3
return;
} // .end method
private void reset ( ) {
/* .locals 4 */
/* .line 2101 */
v0 = this.mVisibleWindows;
/* monitor-enter v0 */
/* .line 2102 */
try { // :try_start_0
v1 = this.mVisibleWindows;
(( android.util.ArraySet ) v1 ).clear ( ); // invoke-virtual {v1}, Landroid/util/ArraySet;->clear()V
/* .line 2103 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_3 */
/* .line 2104 */
v1 = this.mVisibleActivities;
/* monitor-enter v1 */
/* .line 2105 */
try { // :try_start_1
v0 = this.mVisibleActivities;
(( android.util.ArraySet ) v0 ).clear ( ); // invoke-virtual {v0}, Landroid/util/ArraySet;->clear()V
/* .line 2106 */
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_2 */
/* .line 2107 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAudioActive:Z */
/* .line 2108 */
/* iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mGpsActive:Z */
/* .line 2109 */
/* iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mNetActive:Z */
/* .line 2110 */
/* iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mDisplayActive:Z */
/* .line 2111 */
/* iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mCameraActive:Z */
/* .line 2112 */
/* iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAudioBehavier:I */
/* .line 2113 */
/* iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mGpsBehavier:I */
/* .line 2114 */
/* iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mNetBehavier:I */
/* .line 2115 */
/* iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mCamBehavier:I */
/* .line 2116 */
v1 = this.mLastCpuTime;
/* const-wide/16 v2, 0x0 */
(( java.util.concurrent.atomic.AtomicLong ) v1 ).set ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V
/* .line 2117 */
v1 = this.mCurCpuTime;
(( java.util.concurrent.atomic.AtomicLong ) v1 ).set ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V
/* .line 2118 */
/* iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mDisplayBehavier:I */
/* .line 2119 */
v1 = this.this$1;
com.android.server.am.AppStateManager$AppState .-$$Nest$fputmTopAppCount ( v1,v0 );
/* .line 2120 */
v1 = this.this$1;
com.android.server.am.AppStateManager$AppState .-$$Nest$fputmFreeFormCount ( v1,v0 );
/* .line 2121 */
v1 = this.this$1;
com.android.server.am.AppStateManager$AppState .-$$Nest$fputmOverlayCount ( v1,v0 );
/* .line 2122 */
/* iput-wide v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastPss:J */
/* .line 2123 */
/* iput-wide v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastSwapPss:J */
/* .line 2124 */
v1 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) p0 ).isMainProc ( ); // invoke-virtual {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isMainProc()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 2125 */
v1 = this.this$1;
com.android.server.am.AppStateManager$AppState .-$$Nest$fputmAppSwitchFgCount ( v1,v0 );
/* .line 2127 */
} // :cond_0
v0 = this.this$1;
com.android.server.am.AppStateManager$AppState .-$$Nest$mupdateCurrentResourceBehavier ( v0 );
/* .line 2128 */
v0 = this.mServiceDependProcs;
/* monitor-enter v0 */
/* .line 2129 */
try { // :try_start_2
v1 = this.mServiceDependProcs;
(( android.util.ArrayMap ) v1 ).clear ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->clear()V
/* .line 2130 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 2131 */
v1 = this.mProviderDependProcs;
/* monitor-enter v1 */
/* .line 2132 */
try { // :try_start_3
v0 = this.mProviderDependProcs;
(( android.util.ArrayMap ) v0 ).clear ( ); // invoke-virtual {v0}, Landroid/util/ArrayMap;->clear()V
/* .line 2133 */
/* monitor-exit v1 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 2135 */
v0 = this.this$1;
com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmHandler ( v0 );
/* new-instance v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$2; */
/* invoke-direct {v1, p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$2;-><init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V */
(( com.android.server.am.AppStateManager$AppState$MyHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->post(Ljava/lang/Runnable;)Z
/* .line 2160 */
return;
/* .line 2133 */
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_4
/* monitor-exit v1 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* throw v0 */
/* .line 2130 */
/* :catchall_1 */
/* move-exception v1 */
try { // :try_start_5
/* monitor-exit v0 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* throw v1 */
/* .line 2106 */
/* :catchall_2 */
/* move-exception v0 */
try { // :try_start_6
/* monitor-exit v1 */
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_2 */
/* throw v0 */
/* .line 2103 */
/* :catchall_3 */
/* move-exception v1 */
try { // :try_start_7
/* monitor-exit v0 */
/* :try_end_7 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_3 */
/* throw v1 */
} // .end method
private void sendBecomeActiveMsg ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "resson" # Ljava/lang/String; */
/* .line 2416 */
v0 = this.this$1;
com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmHandler ( v0 );
/* new-instance v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$4; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$4;-><init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V */
(( com.android.server.am.AppStateManager$AppState$MyHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->post(Ljava/lang/Runnable;)Z
/* .line 2422 */
return;
} // .end method
private void sendBecomeInactiveMsg ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "resson" # Ljava/lang/String; */
/* .line 2407 */
v0 = this.this$1;
com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmHandler ( v0 );
/* new-instance v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$3; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$3;-><init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V */
(( com.android.server.am.AppStateManager$AppState$MyHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->post(Ljava/lang/Runnable;)Z
/* .line 2413 */
return;
} // .end method
private void setActivityVisible ( com.android.server.wm.ActivityRecord p0, Boolean p1 ) {
/* .locals 5 */
/* .param p1, "record" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "visible" # Z */
/* .line 2163 */
/* sget-boolean v0, Lcom/android/server/am/AppStateManager;->DEBUG_PROC:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2164 */
v0 = this.LOG_TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( com.android.server.wm.ActivityRecord ) p1 ).toString ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " onActivityVisibilityChanged "; // const-string v2, " onActivityVisibilityChanged "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 2166 */
} // :cond_0
v0 = this.mVisibleActivities;
/* monitor-enter v0 */
/* .line 2167 */
int v1 = 0; // const/4 v1, 0x0
/* .line 2168 */
/* .local v1, "recordRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/android/server/wm/ActivityRecord;>;" */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
try { // :try_start_0
v3 = this.mVisibleActivities;
v3 = (( android.util.ArraySet ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/ArraySet;->size()I
/* if-ge v2, v3, :cond_2 */
/* .line 2169 */
v3 = this.mVisibleActivities;
(( android.util.ArraySet ) v3 ).valueAt ( v2 ); // invoke-virtual {v3, v2}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/ref/WeakReference; */
/* .line 2170 */
/* .local v3, "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/android/server/wm/ActivityRecord;>;" */
(( java.lang.ref.WeakReference ) v3 ).get ( ); // invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* if-ne v4, p1, :cond_1 */
/* .line 2171 */
/* move-object v1, v3 */
/* .line 2172 */
/* .line 2168 */
} // .end local v3 # "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/android/server/wm/ActivityRecord;>;"
} // :cond_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 2175 */
} // .end local v2 # "i":I
} // :cond_2
} // :goto_1
if ( p2 != null) { // if-eqz p2, :cond_4
/* if-nez v1, :cond_4 */
/* .line 2176 */
v2 = this.mVisibleActivities;
/* new-instance v3, Ljava/lang/ref/WeakReference; */
/* invoke-direct {v3, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V */
(( android.util.ArraySet ) v2 ).add ( v3 ); // invoke-virtual {v2, v3}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 2177 */
v2 = (( com.android.server.wm.ActivityRecord ) p1 ).inFreeformWindowingMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->inFreeformWindowingMode()Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 2178 */
v2 = this.this$1;
v3 = com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmFreeFormCount ( v2 );
/* add-int/lit8 v3, v3, 0x1 */
com.android.server.am.AppStateManager$AppState .-$$Nest$fputmFreeFormCount ( v2,v3 );
/* .line 2180 */
} // :cond_3
v2 = this.this$1;
v3 = com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmTopAppCount ( v2 );
/* add-int/lit8 v3, v3, 0x1 */
com.android.server.am.AppStateManager$AppState .-$$Nest$fputmTopAppCount ( v2,v3 );
/* .line 2182 */
} // :cond_4
/* if-nez p2, :cond_6 */
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 2183 */
v2 = this.mVisibleActivities;
(( android.util.ArraySet ) v2 ).remove ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/ArraySet;->remove(Ljava/lang/Object;)Z
/* .line 2184 */
v2 = (( com.android.server.wm.ActivityRecord ) p1 ).inFreeformWindowingMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->inFreeformWindowingMode()Z
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 2185 */
v2 = this.this$1;
v3 = com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmFreeFormCount ( v2 );
/* add-int/lit8 v3, v3, -0x1 */
com.android.server.am.AppStateManager$AppState .-$$Nest$fputmFreeFormCount ( v2,v3 );
/* .line 2187 */
} // :cond_5
v2 = this.this$1;
v3 = com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmTopAppCount ( v2 );
/* add-int/lit8 v3, v3, -0x1 */
com.android.server.am.AppStateManager$AppState .-$$Nest$fputmTopAppCount ( v2,v3 );
/* .line 2190 */
} // .end local v1 # "recordRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/android/server/wm/ActivityRecord;>;"
} // :cond_6
} // :goto_2
/* monitor-exit v0 */
/* .line 2191 */
return;
/* .line 2190 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void setForeground ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "foreground" # Z */
/* .line 2318 */
v0 = this.mStateLock;
/* monitor-enter v0 */
/* .line 2319 */
try { // :try_start_0
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I */
/* if-nez v1, :cond_0 */
/* monitor-exit v0 */
return;
/* .line 2320 */
} // :cond_0
/* sget-boolean v1, Lcom/android/server/am/AppStateManager;->DEBUG_PROC:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 2321 */
v1 = this.LOG_TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "set foreground " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 2323 */
} // :cond_1
/* if-nez p1, :cond_2 */
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I */
int v2 = 3; // const/4 v2, 0x3
/* if-ge v1, v2, :cond_2 */
/* .line 2324 */
final String v1 = "become background"; // const-string v1, "become background"
/* invoke-direct {p0, v2, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->moveToStateLocked(ILjava/lang/String;)V */
/* .line 2325 */
final String v1 = "become background"; // const-string v1, "become background"
/* invoke-direct {p0, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->becomeInactiveIfNeeded(Ljava/lang/String;)V */
/* .line 2326 */
} // :cond_2
if ( p1 != null) { // if-eqz p1, :cond_3
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I */
int v2 = 2; // const/4 v2, 0x2
/* if-le v1, v2, :cond_3 */
/* .line 2327 */
final String v1 = "become foreground"; // const-string v1, "become foreground"
/* invoke-direct {p0, v2, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->moveToStateLocked(ILjava/lang/String;)V */
/* .line 2329 */
} // :cond_3
} // :goto_0
/* monitor-exit v0 */
/* .line 2330 */
return;
/* .line 2329 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void setWindowVisible ( com.android.server.policy.WindowManagerPolicy$WindowState p0, android.view.WindowManager$LayoutParams p1, Boolean p2 ) {
/* .locals 5 */
/* .param p1, "win" # Lcom/android/server/policy/WindowManagerPolicy$WindowState; */
/* .param p2, "attrs" # Landroid/view/WindowManager$LayoutParams; */
/* .param p3, "visible" # Z */
/* .line 2195 */
/* sget-boolean v0, Lcom/android/server/am/AppStateManager;->DEBUG_PROC:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2196 */
v0 = this.LOG_TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.Object ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " onWindowVisibilityChanged "; // const-string v2, " onWindowVisibilityChanged "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 2198 */
} // :cond_0
v0 = this.mVisibleWindows;
/* monitor-enter v0 */
/* .line 2199 */
int v1 = 0; // const/4 v1, 0x0
/* .line 2200 */
/* .local v1, "winRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/android/server/policy/WindowManagerPolicy$WindowState;>;" */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
try { // :try_start_0
v3 = this.mVisibleWindows;
v3 = (( android.util.ArraySet ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/ArraySet;->size()I
/* if-ge v2, v3, :cond_2 */
/* .line 2201 */
v3 = this.mVisibleWindows;
/* .line 2202 */
(( android.util.ArraySet ) v3 ).valueAt ( v2 ); // invoke-virtual {v3, v2}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/ref/WeakReference; */
/* .line 2203 */
/* .local v3, "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/android/server/policy/WindowManagerPolicy$WindowState;>;" */
(( java.lang.ref.WeakReference ) v3 ).get ( ); // invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* if-ne v4, p1, :cond_1 */
/* .line 2204 */
/* move-object v1, v3 */
/* .line 2205 */
/* .line 2200 */
} // .end local v3 # "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/android/server/policy/WindowManagerPolicy$WindowState;>;"
} // :cond_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 2208 */
} // .end local v2 # "i":I
} // :cond_2
} // :goto_1
/* const/16 v2, 0x7f6 */
if ( p3 != null) { // if-eqz p3, :cond_3
/* if-nez v1, :cond_3 */
/* .line 2209 */
v3 = this.mVisibleWindows;
/* new-instance v4, Ljava/lang/ref/WeakReference; */
/* invoke-direct {v4, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V */
(( android.util.ArraySet ) v3 ).add ( v4 ); // invoke-virtual {v3, v4}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 2210 */
/* iget v3, p2, Landroid/view/WindowManager$LayoutParams;->type:I */
/* if-ne v3, v2, :cond_4 */
/* .line 2211 */
v2 = (( android.view.WindowManager$LayoutParams ) p2 ).isFullscreen ( ); // invoke-virtual {p2}, Landroid/view/WindowManager$LayoutParams;->isFullscreen()Z
/* if-nez v2, :cond_4 */
/* .line 2212 */
v2 = this.this$1;
v3 = com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmOverlayCount ( v2 );
/* add-int/lit8 v3, v3, 0x1 */
com.android.server.am.AppStateManager$AppState .-$$Nest$fputmOverlayCount ( v2,v3 );
/* .line 2214 */
} // :cond_3
/* if-nez p3, :cond_4 */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 2215 */
v3 = this.mVisibleWindows;
(( android.util.ArraySet ) v3 ).remove ( v1 ); // invoke-virtual {v3, v1}, Landroid/util/ArraySet;->remove(Ljava/lang/Object;)Z
/* .line 2216 */
/* iget v3, p2, Landroid/view/WindowManager$LayoutParams;->type:I */
/* if-ne v3, v2, :cond_4 */
/* .line 2217 */
v2 = (( android.view.WindowManager$LayoutParams ) p2 ).isFullscreen ( ); // invoke-virtual {p2}, Landroid/view/WindowManager$LayoutParams;->isFullscreen()Z
/* if-nez v2, :cond_4 */
/* .line 2218 */
v2 = this.this$1;
v3 = com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmOverlayCount ( v2 );
/* add-int/lit8 v3, v3, -0x1 */
com.android.server.am.AppStateManager$AppState .-$$Nest$fputmOverlayCount ( v2,v3 );
/* .line 2221 */
} // .end local v1 # "winRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/android/server/policy/WindowManagerPolicy$WindowState;>;"
} // :cond_4
} // :goto_2
/* monitor-exit v0 */
/* .line 2222 */
return;
/* .line 2221 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void stateChangeToRecord ( Integer p0, java.lang.String p1 ) {
/* .locals 16 */
/* .param p1, "targetState" # I */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 2567 */
/* move-object/from16 v9, p0 */
/* move/from16 v10, p1 */
/* sget-boolean v0, Lcom/android/server/am/AppStateManager;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2568 */
v11 = this.LOG_TAG;
/* new-instance v12, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord; */
/* iget v2, v9, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I */
/* const-wide/16 v4, 0x0 */
/* iget v7, v9, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAdj:I */
int v8 = 0; // const/4 v8, 0x0
/* move-object v0, v12 */
/* move-object/from16 v1, p0 */
/* move/from16 v3, p1 */
/* move-object/from16 v6, p2 */
/* invoke-direct/range {v0 ..v8}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;-><init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;IIJLjava/lang/String;ILcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord-IA;)V */
/* .line 2569 */
(( com.android.server.am.AppStateManager$AppState$RunningProcess$StateChangeRecord ) v12 ).toString ( ); // invoke-virtual {v12}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->toString()Ljava/lang/String;
/* .line 2568 */
android.util.Slog .i ( v11,v0 );
/* .line 2571 */
} // :cond_0
/* iget v0, v9, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastRecordState:I */
/* if-eq v10, v0, :cond_1 */
com.android.server.am.AppStateManager .-$$Nest$sfgetmStatesNeedToRecord ( );
/* invoke-static/range {p1 ..p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer; */
v0 = (( android.util.ArraySet ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2572 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v11 */
/* .line 2573 */
/* .local v11, "now":J */
/* iget-wide v0, v9, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastStateTimeMills:J */
/* sub-long v13, v11, v0 */
/* .line 2574 */
/* .local v13, "interval":J */
/* new-instance v15, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord; */
/* iget v2, v9, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastRecordState:I */
/* iget v7, v9, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAdj:I */
int v8 = 0; // const/4 v8, 0x0
/* move-object v0, v15 */
/* move-object/from16 v1, p0 */
/* move/from16 v3, p1 */
/* move-wide v4, v13 */
/* move-object/from16 v6, p2 */
/* invoke-direct/range {v0 ..v8}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;-><init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;IIJLjava/lang/String;ILcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord-IA;)V */
/* .line 2576 */
/* .local v0, "record":Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord; */
/* invoke-direct {v9, v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->addToHistory(Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;)V */
/* .line 2577 */
/* iput-wide v11, v9, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastStateTimeMills:J */
/* .line 2578 */
/* iput v10, v9, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastRecordState:I */
/* .line 2580 */
} // .end local v0 # "record":Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;
} // .end local v11 # "now":J
} // .end local v13 # "interval":J
} // :cond_1
return;
} // .end method
private void updateActivityVisiblity ( ) {
/* .locals 6 */
/* .line 2272 */
int v0 = 0; // const/4 v0, 0x0
/* .line 2273 */
/* .local v0, "inSplitMode":Z */
v1 = this.mVisibleActivities;
/* monitor-enter v1 */
/* .line 2274 */
try { // :try_start_0
v2 = this.mVisibleActivities;
v2 = (( android.util.ArraySet ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/ArraySet;->size()I
/* if-lez v2, :cond_3 */
/* .line 2275 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
v3 = this.mVisibleActivities;
v3 = (( android.util.ArraySet ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/ArraySet;->size()I
/* if-ge v2, v3, :cond_2 */
/* .line 2276 */
v3 = this.mVisibleActivities;
(( android.util.ArraySet ) v3 ).valueAt ( v2 ); // invoke-virtual {v3, v2}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/ref/WeakReference; */
(( java.lang.ref.WeakReference ) v3 ).get ( ); // invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/wm/ActivityRecord; */
/* .line 2277 */
/* .local v3, "activity":Lcom/android/server/wm/ActivityRecord; */
/* if-nez v3, :cond_0 */
/* .line 2278 */
v4 = this.mVisibleActivities;
/* add-int/lit8 v5, v2, -0x1 */
} // .end local v2 # "i":I
/* .local v5, "i":I */
(( android.util.ArraySet ) v4 ).removeAt ( v2 ); // invoke-virtual {v4, v2}, Landroid/util/ArraySet;->removeAt(I)Ljava/lang/Object;
/* move v2, v5 */
/* .line 2279 */
} // .end local v5 # "i":I
/* .restart local v2 # "i":I */
} // :cond_0
v4 = (( com.android.server.wm.ActivityRecord ) v3 ).inSplitScreenWindowingMode ( ); // invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->inSplitScreenWindowingMode()Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 2280 */
int v0 = 1; // const/4 v0, 0x1
/* .line 2275 */
} // .end local v3 # "activity":Lcom/android/server/wm/ActivityRecord;
} // :cond_1
} // :goto_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 2283 */
} // .end local v2 # "i":I
} // :cond_2
v2 = this.this$1;
com.android.server.am.AppStateManager$AppState .-$$Nest$fputmInSplitMode ( v2,v0 );
/* .line 2285 */
} // :cond_3
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 2286 */
v1 = this.this$1;
com.android.server.am.AppStateManager$AppState .-$$Nest$fputmInSplitMode ( v1,v0 );
/* .line 2287 */
return;
/* .line 2285 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
} // .end method
private void updateProcessInfo ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 4 */
/* .param p1, "record" # Lcom/android/server/am/ProcessRecord; */
/* .line 1997 */
/* iget v0, p1, Lcom/android/server/am/ProcessRecord;->userId:I */
/* iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mUserId:I */
/* .line 1998 */
/* iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
/* iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mUid:I */
/* .line 1999 */
v0 = this.processName;
this.mProcessName = v0;
/* .line 2000 */
v0 = this.info;
v0 = this.packageName;
this.mPackageName = v0;
/* .line 2001 */
this.mProcessRecord = p1;
/* .line 2002 */
v0 = this.mState;
v0 = (( com.android.server.am.ProcessStateRecord ) v0 ).getSetAdj ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getSetAdj()I
/* iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAdj:I */
/* .line 2003 */
v0 = this.mState;
v0 = (( com.android.server.am.ProcessStateRecord ) v0 ).getSetProcState ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getSetProcState()I
/* iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAmsProcState:I */
/* .line 2004 */
v0 = this.mProfile;
(( com.android.server.am.ProcessProfileRecord ) v0 ).getLastPss ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastPss:J */
/* .line 2005 */
v0 = this.mProfile;
(( com.android.server.am.ProcessProfileRecord ) v0 ).getLastSwapPss ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessProfileRecord;->getLastSwapPss()J
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastSwapPss:J */
/* .line 2006 */
v0 = (( com.android.server.am.ProcessRecord ) p1 ).isKilled ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isKilled()Z
/* iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIsKilled:Z */
/* .line 2007 */
v0 = this.mState;
(( com.android.server.am.ProcessStateRecord ) v0 ).getAdjType ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getAdjType()Ljava/lang/String;
this.mAdjType = v0;
/* .line 2008 */
v0 = (( com.android.server.am.ProcessRecord ) p1 ).hasActivities ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->hasActivities()Z
/* iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHasActivity:Z */
/* .line 2009 */
v0 = this.mServices;
v0 = (( com.android.server.am.ProcessServiceRecord ) v0 ).hasForegroundServices ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessServiceRecord;->hasForegroundServices()Z
/* iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHasFgService:Z */
/* .line 2010 */
v0 = this.info;
v0 = this.packageName;
v1 = this.processName;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIsMainProc:Z */
/* .line 2012 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->is32BitProcess(Lcom/android/server/am/ProcessRecord;)Z */
int v1 = 1; // const/4 v1, 0x1
/* xor-int/2addr v0, v1 */
/* iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIs64BitProcess:Z */
/* .line 2013 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mPid:I */
/* iget v2, p1, Lcom/android/server/am/ProcessRecord;->mPid:I */
/* if-eq v0, v2, :cond_3 */
/* .line 2014 */
/* iget v0, p1, Lcom/android/server/am/ProcessRecord;->mPid:I */
/* iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mPid:I */
/* .line 2015 */
/* iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIsMainProc:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2016 */
v0 = this.this$1;
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
com.android.server.am.AppStateManager$AppState .-$$Nest$fputmMainProcStartTime ( v0,v2,v3 );
/* .line 2018 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "SmartPower: "; // const-string v2, "SmartPower: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mProcessName;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "/"; // const-string v2, "/"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mUid:I */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = "("; // const-string v2, "("
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mPid:I */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ")"; // const-string v2, ")"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
this.LOG_TAG = v0;
/* .line 2019 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mPid:I */
/* if-lez v0, :cond_3 */
/* iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIsKilled:Z */
/* if-nez v0, :cond_3 */
/* .line 2020 */
v0 = this.mStateLock;
/* monitor-enter v0 */
/* .line 2021 */
try { // :try_start_0
v2 = this.this$1;
v2 = com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmForeground ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_1
/* iget v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I */
/* if-eq v2, v1, :cond_1 */
/* .line 2022 */
final String v1 = "process start "; // const-string v1, "process start "
int v2 = 2; // const/4 v2, 0x2
/* invoke-direct {p0, v2, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->moveToStateLocked(ILjava/lang/String;)V */
/* .line 2023 */
} // :cond_1
v1 = this.this$1;
v1 = com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmForeground ( v1 );
/* if-nez v1, :cond_2 */
/* .line 2024 */
final String v1 = "process start "; // const-string v1, "process start "
int v2 = 3; // const/4 v2, 0x3
/* invoke-direct {p0, v2, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->moveToStateLocked(ILjava/lang/String;)V */
/* .line 2026 */
} // :cond_2
} // :goto_0
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 2027 */
v0 = this.this$1;
com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmHandler ( v0 );
/* new-instance v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;-><init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Lcom/android/server/am/ProcessRecord;)V */
(( com.android.server.am.AppStateManager$AppState$MyHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->post(Ljava/lang/Runnable;)Z
/* .line 2026 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
/* .line 2056 */
} // :cond_3
} // :goto_1
/* iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIsKilled:Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 2057 */
/* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->processKilledLocked()V */
/* .line 2059 */
} // :cond_4
v0 = this.mProcPriorityScore;
com.android.server.am.AppStateManager$AppState$RunningProcess$ProcessPriorityScore .-$$Nest$mupdatePriorityScore ( v0 );
/* .line 2062 */
} // :goto_2
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAdj:I */
/* const/16 v1, 0x64 */
/* if-lt v0, v1, :cond_5 */
/* .line 2063 */
final String v0 = "adj below visible"; // const-string v0, "adj below visible"
/* invoke-direct {p0, v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->becomeInactiveIfNeeded(Ljava/lang/String;)V */
/* .line 2064 */
} // :cond_5
/* if-gez v0, :cond_6 */
/* .line 2065 */
final String v0 = "adj above foreground"; // const-string v0, "adj above foreground"
/* invoke-direct {p0, v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->becomeActiveIfNeeded(Ljava/lang/String;)V */
/* .line 2067 */
} // :cond_6
} // :goto_3
return;
} // .end method
private void updateProviderDepends ( com.android.server.am.ProcessRecord p0, Boolean p1 ) {
/* .locals 10 */
/* .param p1, "host" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "isConnect" # Z */
/* .line 2457 */
/* iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mUid:I */
/* if-eq v0, v1, :cond_1 */
/* .line 2458 */
v0 = this.mProviderDependProcs;
/* monitor-enter v0 */
/* .line 2459 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 2460 */
try { // :try_start_0
v1 = this.mProviderDependProcs;
v2 = this.processName;
/* new-instance v9, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo; */
v4 = this.this$1;
/* iget v5, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
/* iget v6, p1, Lcom/android/server/am/ProcessRecord;->mPid:I */
v7 = this.processName;
int v8 = 0; // const/4 v8, 0x0
/* move-object v3, v9 */
/* invoke-direct/range {v3 ..v8}, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;-><init>(Lcom/android/server/am/AppStateManager$AppState;IILjava/lang/String;Lcom/android/server/am/AppStateManager$AppState$DependProcInfo-IA;)V */
(( android.util.ArrayMap ) v1 ).put ( v2, v9 ); // invoke-virtual {v1, v2, v9}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 2463 */
} // :cond_0
v1 = this.mProviderDependProcs;
v2 = this.processName;
(( android.util.ArrayMap ) v1 ).remove ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 2465 */
} // :goto_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 2467 */
} // :cond_1
} // :goto_1
return;
} // .end method
private void updateServiceDepends ( com.android.server.am.ProcessRecord p0, Boolean p1, Boolean p2 ) {
/* .locals 10 */
/* .param p1, "service" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "isConnect" # Z */
/* .param p3, "fgService" # Z */
/* .line 2437 */
/* iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mUid:I */
/* if-eq v0, v1, :cond_2 */
/* .line 2438 */
v0 = this.mServiceDependProcs;
/* monitor-enter v0 */
/* .line 2439 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 2440 */
try { // :try_start_0
v1 = this.mServiceDependProcs;
v2 = this.processName;
/* new-instance v9, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo; */
v4 = this.this$1;
/* iget v5, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
/* iget v6, p1, Lcom/android/server/am/ProcessRecord;->mPid:I */
v7 = this.processName;
int v8 = 0; // const/4 v8, 0x0
/* move-object v3, v9 */
/* invoke-direct/range {v3 ..v8}, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;-><init>(Lcom/android/server/am/AppStateManager$AppState;IILjava/lang/String;Lcom/android/server/am/AppStateManager$AppState$DependProcInfo-IA;)V */
(( android.util.ArrayMap ) v1 ).put ( v2, v9 ); // invoke-virtual {v1, v2, v9}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 2443 */
} // :cond_0
v1 = this.mServiceDependProcs;
v2 = this.processName;
(( android.util.ArrayMap ) v1 ).remove ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 2445 */
} // :goto_0
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 2446 */
/* iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mDependencyProtected:Z */
/* if-nez v0, :cond_1 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mUid:I */
/* const/16 v1, 0x3e8 */
/* if-ne v0, v1, :cond_2 */
/* if-nez p3, :cond_1 */
/* if-nez p2, :cond_2 */
/* .line 2448 */
} // :cond_1
v0 = this.this$1;
v0 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmSmartPowerPolicyManager ( v0 );
v3 = this.mProcessName;
v4 = this.processName;
/* iget v5, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
/* iget v6, p1, Lcom/android/server/am/ProcessRecord;->mPid:I */
/* move v2, p2 */
/* invoke-virtual/range {v1 ..v6}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->onDependChanged(ZLjava/lang/String;Ljava/lang/String;II)V */
/* .line 2445 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
/* .line 2454 */
} // :cond_2
} // :goto_1
return;
} // .end method
private void updateVisibility ( ) {
/* .locals 3 */
/* .line 2225 */
/* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->updateActivityVisiblity()V */
/* .line 2226 */
/* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->updateWindowVisiblity()V */
/* .line 2227 */
v0 = this.mStateLock;
/* monitor-enter v0 */
/* .line 2228 */
try { // :try_start_0
v1 = /* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->hasVisibleWindow()Z */
/* if-nez v1, :cond_1 */
v1 = /* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->hasVisibleActivity()Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 2233 */
} // :cond_0
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I */
if ( v1 != null) { // if-eqz v1, :cond_2
int v2 = 2; // const/4 v2, 0x2
/* if-ge v1, v2, :cond_2 */
/* .line 2234 */
final String v1 = "become invisible"; // const-string v1, "become invisible"
/* invoke-direct {p0, v2, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->moveToStateLocked(ILjava/lang/String;)V */
/* .line 2229 */
} // :cond_1
} // :goto_0
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I */
int v2 = 1; // const/4 v2, 0x1
/* if-eq v1, v2, :cond_2 */
/* .line 2230 */
final String v1 = "become visible"; // const-string v1, "become visible"
/* invoke-direct {p0, v2, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->moveToStateLocked(ILjava/lang/String;)V */
/* .line 2237 */
} // :cond_2
} // :goto_1
/* monitor-exit v0 */
/* .line 2238 */
return;
/* .line 2237 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void updateWindowVisiblity ( ) {
/* .locals 5 */
/* .line 2259 */
v0 = this.mVisibleWindows;
/* monitor-enter v0 */
/* .line 2260 */
try { // :try_start_0
v1 = this.mVisibleWindows;
v1 = (( android.util.ArraySet ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/ArraySet;->size()I
/* if-lez v1, :cond_1 */
/* .line 2261 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = this.mVisibleWindows;
v2 = (( android.util.ArraySet ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/ArraySet;->size()I
/* if-ge v1, v2, :cond_1 */
/* .line 2262 */
v2 = this.mVisibleWindows;
(( android.util.ArraySet ) v2 ).valueAt ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/ref/WeakReference; */
(( java.lang.ref.WeakReference ) v2 ).get ( ); // invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/policy/WindowManagerPolicy$WindowState; */
/* .line 2263 */
/* .local v2, "win":Lcom/android/server/policy/WindowManagerPolicy$WindowState; */
/* if-nez v2, :cond_0 */
/* .line 2264 */
v3 = this.mVisibleWindows;
/* add-int/lit8 v4, v1, -0x1 */
} // .end local v1 # "i":I
/* .local v4, "i":I */
(( android.util.ArraySet ) v3 ).removeAt ( v1 ); // invoke-virtual {v3, v1}, Landroid/util/ArraySet;->removeAt(I)Ljava/lang/Object;
/* move v1, v4 */
/* .line 2261 */
} // .end local v2 # "win":Lcom/android/server/policy/WindowManagerPolicy$WindowState;
} // .end local v4 # "i":I
/* .restart local v1 # "i":I */
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 2268 */
} // .end local v1 # "i":I
} // :cond_1
/* monitor-exit v0 */
/* .line 2269 */
return;
/* .line 2268 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private Boolean whiteListVideoVisible ( ) {
/* .locals 6 */
/* .line 2302 */
v0 = this.mVisibleActivities;
/* monitor-enter v0 */
/* .line 2303 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
try { // :try_start_0
v2 = this.mVisibleActivities;
v2 = (( android.util.ArraySet ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/ArraySet;->size()I
/* if-ge v1, v2, :cond_2 */
/* .line 2304 */
v2 = this.mVisibleActivities;
(( android.util.ArraySet ) v2 ).valueAt ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/ref/WeakReference; */
(( java.lang.ref.WeakReference ) v2 ).get ( ); // invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/wm/ActivityRecord; */
/* .line 2305 */
/* .local v2, "activity":Lcom/android/server/wm/ActivityRecord; */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 2306 */
v3 = this.this$1;
v3 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmWhiteListVideoActivities ( v3 );
(( android.util.ArraySet ) v3 ).iterator ( ); // invoke-virtual {v3}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;
v4 = } // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_1
/* check-cast v4, Ljava/lang/String; */
/* .line 2307 */
/* .local v4, "videoActivity":Ljava/lang/String; */
(( com.android.server.wm.ActivityRecord ) v2 ).toString ( ); // invoke-virtual {v2}, Lcom/android/server/wm/ActivityRecord;->toString()Ljava/lang/String;
v5 = (( java.lang.String ) v5 ).contains ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 2308 */
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 2310 */
} // .end local v4 # "videoActivity":Ljava/lang/String;
} // :cond_0
/* .line 2303 */
} // .end local v2 # "activity":Lcom/android/server/wm/ActivityRecord;
} // :cond_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 2313 */
} // .end local v1 # "i":I
} // :cond_2
/* monitor-exit v0 */
/* .line 2314 */
int v0 = 0; // const/4 v0, 0x0
/* .line 2313 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
/* # virtual methods */
public Long addAndGetCurCpuTime ( Long p0 ) {
/* .locals 2 */
/* .param p1, "cpuTime" # J */
/* .line 2826 */
v0 = this.mCurCpuTime;
(( java.util.concurrent.atomic.AtomicLong ) v0 ).addAndGet ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
public Boolean canHibernate ( ) {
/* .locals 1 */
/* .line 2809 */
v0 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) p0 ).isProcessPerceptible ( ); // invoke-virtual {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isProcessPerceptible()Z
/* if-nez v0, :cond_0 */
v0 = this.this$1;
v0 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmSmartPowerPolicyManager ( v0 );
/* .line 2810 */
v0 = (( com.miui.server.smartpower.SmartPowerPolicyManager ) v0 ).isProcessHibernationWhiteList ( p0 ); // invoke-virtual {v0, p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isProcessHibernationWhiteList(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Z
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 2809 */
} // :goto_0
} // .end method
public Boolean compareAndSetLastCpuTime ( Long p0, Long p1 ) {
/* .locals 1 */
/* .param p1, "oldTime" # J */
/* .param p3, "cpuTime" # J */
/* .line 2831 */
v0 = this.mLastCpuTime;
v0 = (( java.util.concurrent.atomic.AtomicLong ) v0 ).compareAndSet ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Ljava/util/concurrent/atomic/AtomicLong;->compareAndSet(JJ)Z
} // .end method
public void dump ( java.io.PrintWriter p0, java.lang.String[] p1, Integer p2 ) {
/* .locals 20 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "args" # [Ljava/lang/String; */
/* .param p3, "opti" # I */
/* .line 2850 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " name="; // const-string v3, " name="
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mProcessName;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "("; // const-string v3, "("
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mPid:I */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ") state="; // const-string v3, ") state="
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I */
/* .line 2851 */
com.android.server.am.AppStateManager .processStateToString ( v3 );
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2850 */
(( java.io.PrintWriter ) v2 ).println ( v0 ); // invoke-virtual {v2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2852 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " pkg="; // const-string v3, " pkg="
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mPackageName;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " adj="; // const-string v3, " adj="
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAdj:I */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " pri="; // const-string v3, " pri="
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 2854 */
v3 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPriorityScore()I */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " usage="; // const-string v3, " usage="
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 2855 */
v3 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getUsageLevel()I */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2852 */
(( java.io.PrintWriter ) v2 ).println ( v0 ); // invoke-virtual {v2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2856 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " audioActive="; // const-string v3, " audioActive="
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAudioActive:Z */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = " gpsActive="; // const-string v3, " gpsActive="
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mGpsActive:Z */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = " bleActive="; // const-string v3, " bleActive="
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mBleActive:Z */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = " netActive="; // const-string v3, " netActive="
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mNetActive:Z */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v2 ).println ( v0 ); // invoke-virtual {v2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2858 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v3 */
/* iget-wide v5, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastStateTimeMills:J */
/* sub-long/2addr v3, v5 */
/* const-wide/16 v5, 0x3e8 */
/* div-long/2addr v3, v5 */
/* .line 2860 */
/* .local v3, "currentDuration":J */
/* iget-wide v7, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mForegroundDuration:J */
/* div-long/2addr v7, v5 */
/* .line 2861 */
/* .local v7, "foregroundDuration":J */
/* iget-wide v9, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mBackgroundDuration:J */
/* div-long/2addr v9, v5 */
/* .line 2862 */
/* .local v9, "backgroundDuration":J */
/* iget-wide v11, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIdleDuration:J */
/* div-long/2addr v11, v5 */
/* .line 2863 */
/* .local v11, "idleDuration":J */
/* iget-wide v13, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHibernatonDuration:J */
/* div-long/2addr v13, v5 */
/* .line 2864 */
/* .local v13, "hibernationDuration":J */
/* iget v0, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I */
int v5 = 6; // const/4 v5, 0x6
/* if-ne v0, v5, :cond_0 */
/* .line 2865 */
/* add-long/2addr v11, v3 */
/* .line 2866 */
} // :cond_0
int v5 = 7; // const/4 v5, 0x7
/* if-ne v0, v5, :cond_1 */
/* .line 2867 */
/* add-long/2addr v13, v3 */
/* .line 2868 */
} // :cond_1
int v5 = 3; // const/4 v5, 0x3
if ( v0 != null) { // if-eqz v0, :cond_2
/* if-ge v0, v5, :cond_2 */
/* .line 2869 */
/* add-long/2addr v7, v3 */
/* .line 2870 */
} // :cond_2
/* if-lt v0, v5, :cond_3 */
/* .line 2871 */
/* add-long/2addr v9, v3 */
/* .line 2873 */
} // :cond_3
} // :goto_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " f("; // const-string v5, " f("
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v7, v8 ); // invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v5 = ":"; // const-string v5, ":"
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v5, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mForegroundCount:J */
(( java.lang.StringBuilder ) v0 ).append ( v5, v6 ); // invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v5 = ") b("; // const-string v5, ") b("
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v9, v10 ); // invoke-virtual {v0, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v5 = ":"; // const-string v5, ":"
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v5, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mBackgroundCount:J */
(( java.lang.StringBuilder ) v0 ).append ( v5, v6 ); // invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v5 = ") i("; // const-string v5, ") i("
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v11, v12 ); // invoke-virtual {v0, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v5 = ":"; // const-string v5, ":"
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v5, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIdleCount:J */
(( java.lang.StringBuilder ) v0 ).append ( v5, v6 ); // invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v5 = ") h("; // const-string v5, ") h("
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v13, v14 ); // invoke-virtual {v0, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v5 = ":"; // const-string v5, ":"
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v5, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHibernatonCount:J */
(( java.lang.StringBuilder ) v0 ).append ( v5, v6 ); // invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v5 = ")"; // const-string v5, ")"
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v2 ).println ( v0 ); // invoke-virtual {v2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2878 */
v5 = this.mVisibleActivities;
/* monitor-enter v5 */
/* .line 2879 */
try { // :try_start_0
v0 = this.mVisibleActivities;
v0 = (( android.util.ArraySet ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/ArraySet;->size()I
/* if-lez v0, :cond_6 */
/* .line 2880 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = " visible activities size="; // const-string v6, " visible activities size="
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.mVisibleActivities;
/* .line 2881 */
v6 = (( android.util.ArraySet ) v6 ).size ( ); // invoke-virtual {v6}, Landroid/util/ArraySet;->size()I
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2880 */
(( java.io.PrintWriter ) v2 ).println ( v0 ); // invoke-virtual {v2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2882 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_1
v6 = this.mVisibleActivities;
v6 = (( android.util.ArraySet ) v6 ).size ( ); // invoke-virtual {v6}, Landroid/util/ArraySet;->size()I
/* if-ge v0, v6, :cond_5 */
/* .line 2883 */
v6 = this.mVisibleActivities;
(( android.util.ArraySet ) v6 ).valueAt ( v0 ); // invoke-virtual {v6, v0}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;
/* check-cast v6, Ljava/lang/ref/WeakReference; */
(( java.lang.ref.WeakReference ) v6 ).get ( ); // invoke-virtual {v6}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v6, Lcom/android/server/wm/ActivityRecord; */
/* .line 2884 */
/* .local v6, "record":Lcom/android/server/wm/ActivityRecord; */
if ( v6 != null) { // if-eqz v6, :cond_4
/* .line 2885 */
/* new-instance v15, Ljava/lang/StringBuilder; */
/* invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* move-wide/from16 v16, v3 */
} // .end local v3 # "currentDuration":J
/* .local v16, "currentDuration":J */
try { // :try_start_1
final String v3 = " "; // const-string v3, " "
(( java.lang.StringBuilder ) v15 ).append ( v3 ); // invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v6 ); // invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v2 ).println ( v3 ); // invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2884 */
} // .end local v16 # "currentDuration":J
/* .restart local v3 # "currentDuration":J */
} // :cond_4
/* move-wide/from16 v16, v3 */
/* .line 2882 */
} // .end local v3 # "currentDuration":J
} // .end local v6 # "record":Lcom/android/server/wm/ActivityRecord;
/* .restart local v16 # "currentDuration":J */
} // :goto_2
/* add-int/lit8 v0, v0, 0x1 */
/* move-wide/from16 v3, v16 */
} // .end local v16 # "currentDuration":J
/* .restart local v3 # "currentDuration":J */
} // :cond_5
/* move-wide/from16 v16, v3 */
} // .end local v3 # "currentDuration":J
/* .restart local v16 # "currentDuration":J */
/* .line 2879 */
} // .end local v0 # "i":I
} // .end local v16 # "currentDuration":J
/* .restart local v3 # "currentDuration":J */
} // :cond_6
/* move-wide/from16 v16, v3 */
/* .line 2889 */
} // .end local v3 # "currentDuration":J
/* .restart local v16 # "currentDuration":J */
} // :goto_3
/* monitor-exit v5 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_2 */
/* .line 2891 */
v3 = this.mVisibleWindows;
/* monitor-enter v3 */
/* .line 2892 */
try { // :try_start_2
v0 = this.mVisibleWindows;
v0 = (( android.util.ArraySet ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/ArraySet;->size()I
/* if-lez v0, :cond_8 */
/* .line 2893 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " visible windows size="; // const-string v4, " visible windows size="
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mVisibleWindows;
v4 = (( android.util.ArraySet ) v4 ).size ( ); // invoke-virtual {v4}, Landroid/util/ArraySet;->size()I
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v2 ).println ( v0 ); // invoke-virtual {v2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2894 */
int v0 = 0; // const/4 v0, 0x0
/* .restart local v0 # "i":I */
} // :goto_4
v4 = this.mVisibleWindows;
v4 = (( android.util.ArraySet ) v4 ).size ( ); // invoke-virtual {v4}, Landroid/util/ArraySet;->size()I
/* if-ge v0, v4, :cond_8 */
/* .line 2895 */
v4 = this.mVisibleWindows;
(( android.util.ArraySet ) v4 ).valueAt ( v0 ); // invoke-virtual {v4, v0}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;
/* check-cast v4, Ljava/lang/ref/WeakReference; */
(( java.lang.ref.WeakReference ) v4 ).get ( ); // invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v4, Lcom/android/server/policy/WindowManagerPolicy$WindowState; */
/* .line 2896 */
/* .local v4, "win":Lcom/android/server/policy/WindowManagerPolicy$WindowState; */
if ( v4 != null) { // if-eqz v4, :cond_7
/* .line 2897 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = " "; // const-string v6, " "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v2 ).println ( v5 ); // invoke-virtual {v2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2894 */
} // .end local v4 # "win":Lcom/android/server/policy/WindowManagerPolicy$WindowState;
} // :cond_7
/* add-int/lit8 v0, v0, 0x1 */
/* .line 2901 */
} // .end local v0 # "i":I
} // :cond_8
/* monitor-exit v3 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 2903 */
/* new-instance v0, Ljava/text/SimpleDateFormat; */
final String v3 = "HH:mm:ss.SSS"; // const-string v3, "HH:mm:ss.SSS"
/* invoke-direct {v0, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V */
/* .line 2905 */
/* .local v0, "formater":Ljava/text/SimpleDateFormat; */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
/* sget-wide v5, Lcom/miui/app/smartpower/SmartPowerSettings;->MAX_HISTORY_REPORT_DURATION:J */
/* sub-long/2addr v3, v5 */
/* invoke-direct {v1, v3, v4}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getHistoryInfos(J)Ljava/util/ArrayList; */
/* .line 2907 */
/* .local v3, "history":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;>;" */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " state change history:"; // const-string v5, " state change history:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = (( java.util.ArrayList ) v3 ).size ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v2 ).println ( v4 ); // invoke-virtual {v2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2908 */
(( java.util.ArrayList ) v3 ).iterator ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v5 = } // :goto_5
if ( v5 != null) { // if-eqz v5, :cond_9
/* check-cast v5, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord; */
/* .line 2909 */
/* .local v5, "record":Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v15 = " "; // const-string v15, " "
(( java.lang.StringBuilder ) v6 ).append ( v15 ); // invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* new-instance v15, Ljava/util/Date; */
/* .line 2910 */
/* move-object/from16 v18, v3 */
/* move-object/from16 v19, v4 */
} // .end local v3 # "history":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;>;"
/* .local v18, "history":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;>;" */
com.android.server.am.AppStateManager$AppState$RunningProcess$StateChangeRecord .-$$Nest$mgetTimeStamp ( v5 );
/* move-result-wide v3 */
/* invoke-direct {v15, v3, v4}, Ljava/util/Date;-><init>(J)V */
(( java.text.SimpleDateFormat ) v0 ).format ( v15 ); // invoke-virtual {v0, v15}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
(( java.lang.StringBuilder ) v6 ).append ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ": "; // const-string v4, ": "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2909 */
(( java.io.PrintWriter ) v2 ).println ( v3 ); // invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2911 */
} // .end local v5 # "record":Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;
/* move-object/from16 v3, v18 */
/* move-object/from16 v4, v19 */
/* .line 2912 */
} // .end local v18 # "history":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;>;"
/* .restart local v3 # "history":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;>;" */
} // :cond_9
return;
/* .line 2901 */
} // .end local v0 # "formater":Ljava/text/SimpleDateFormat;
} // .end local v3 # "history":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;>;"
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_3
/* monitor-exit v3 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* throw v0 */
/* .line 2889 */
} // .end local v16 # "currentDuration":J
/* .local v3, "currentDuration":J */
/* :catchall_1 */
/* move-exception v0 */
/* move-wide/from16 v16, v3 */
} // .end local v3 # "currentDuration":J
/* .restart local v16 # "currentDuration":J */
} // :goto_6
try { // :try_start_4
/* monitor-exit v5 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_2 */
/* throw v0 */
/* :catchall_2 */
/* move-exception v0 */
} // .end method
public Integer getAdj ( ) {
/* .locals 1 */
/* .line 2668 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAdj:I */
} // .end method
public java.lang.String getAdjType ( ) {
/* .locals 1 */
/* .line 2683 */
v0 = this.mAdjType;
} // .end method
public Integer getAmsProcState ( ) {
/* .locals 1 */
/* .line 2678 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAmsProcState:I */
} // .end method
public Long getCurCpuTime ( ) {
/* .locals 2 */
/* .line 2841 */
v0 = this.mCurCpuTime;
(( java.util.concurrent.atomic.AtomicLong ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
public Integer getCurrentState ( ) {
/* .locals 1 */
/* .line 2673 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I */
} // .end method
public Long getLastCpuTime ( ) {
/* .locals 2 */
/* .line 2846 */
v0 = this.mLastCpuTime;
(( java.util.concurrent.atomic.AtomicLong ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
public Integer getLastTaskId ( ) {
/* .locals 1 */
/* .line 2687 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastTaskId:I */
} // .end method
public java.lang.String getPackageName ( ) {
/* .locals 1 */
/* .line 2658 */
v0 = this.mPackageName;
} // .end method
public Integer getPid ( ) {
/* .locals 1 */
/* .line 2648 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mPid:I */
} // .end method
public Integer getPriorityScore ( ) {
/* .locals 1 */
/* .line 2799 */
v0 = this.mProcPriorityScore;
v0 = com.android.server.am.AppStateManager$AppState$RunningProcess$ProcessPriorityScore .-$$Nest$fgetmPriorityScore ( v0 );
} // .end method
public java.lang.String getProcessName ( ) {
/* .locals 1 */
/* .line 2653 */
v0 = this.mProcessName;
} // .end method
public com.android.server.am.ProcessRecord getProcessRecord ( ) {
/* .locals 1 */
/* .line 2663 */
v0 = this.mProcessRecord;
} // .end method
public Long getPss ( ) {
/* .locals 4 */
/* .line 2783 */
/* iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastPss:J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
/* if-nez v0, :cond_0 */
/* .line 2784 */
(( com.android.server.am.AppStateManager$AppState$RunningProcess ) p0 ).updatePss ( ); // invoke-virtual {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->updatePss()V
/* .line 2786 */
} // :cond_0
/* iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastPss:J */
/* return-wide v0 */
} // .end method
public Long getSwapPss ( ) {
/* .locals 4 */
/* .line 2791 */
/* iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastPss:J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
/* if-nez v0, :cond_0 */
/* .line 2792 */
(( com.android.server.am.AppStateManager$AppState$RunningProcess ) p0 ).updatePss ( ); // invoke-virtual {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->updatePss()V
/* .line 2794 */
} // :cond_0
/* iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastSwapPss:J */
/* return-wide v0 */
} // .end method
public Integer getUid ( ) {
/* .locals 1 */
/* .line 2643 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mUid:I */
} // .end method
public Integer getUsageLevel ( ) {
/* .locals 1 */
/* .line 2804 */
v0 = this.mProcPriorityScore;
v0 = com.android.server.am.AppStateManager$AppState$RunningProcess$ProcessPriorityScore .-$$Nest$fgetmUsageLevel ( v0 );
} // .end method
public Integer getUserId ( ) {
/* .locals 1 */
/* .line 2638 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mUserId:I */
} // .end method
public Boolean hasActivity ( ) {
/* .locals 1 */
/* .line 2761 */
/* iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHasActivity:Z */
} // .end method
public Boolean hasForegrundService ( ) {
/* .locals 1 */
/* .line 2766 */
/* iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHasFgService:Z */
} // .end method
public Boolean inCurrentStack ( ) {
/* .locals 1 */
/* .line 2710 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean inSplitMode ( ) {
/* .locals 1 */
/* .line 2706 */
v0 = this.this$1;
v0 = com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmInSplitMode ( v0 );
} // .end method
public Boolean is64BitProcess ( ) {
/* .locals 1 */
/* .line 2724 */
/* iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIs64BitProcess:Z */
} // .end method
public Boolean isAutoStartApp ( ) {
/* .locals 1 */
/* .line 2732 */
v0 = this.this$1;
v0 = com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmIsAutoStartApp ( v0 );
} // .end method
public Boolean isDependsProvidersProcess ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "dependsProcName" # Ljava/lang/String; */
/* .line 2403 */
v0 = this.mProviderDependProcs;
v0 = (( android.util.ArrayMap ) v0 ).containsKey ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z
} // .end method
public Boolean isDependsServiceProcess ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "dependsProcName" # Ljava/lang/String; */
/* .line 2396 */
v0 = this.mServiceDependProcs;
/* monitor-enter v0 */
/* .line 2397 */
try { // :try_start_0
v1 = this.mServiceDependProcs;
v1 = (( android.util.ArrayMap ) v1 ).containsKey ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z
/* monitor-exit v0 */
/* .line 2398 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isForeground ( ) {
/* .locals 1 */
/* .line 2715 */
v0 = this.this$1;
v0 = com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmForeground ( v0 );
} // .end method
public Boolean isHibernation ( ) {
/* .locals 2 */
/* .line 2742 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I */
int v1 = 7; // const/4 v1, 0x7
/* if-ne v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isIdle ( ) {
/* .locals 2 */
/* .line 2737 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I */
int v1 = 5; // const/4 v1, 0x5
/* if-le v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isKilled ( ) {
/* .locals 1 */
/* .line 2747 */
/* iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIsKilled:Z */
/* if-nez v0, :cond_1 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I */
/* if-nez v0, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
public Boolean isLastInterActive ( ) {
/* .locals 6 */
/* .line 2692 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 2693 */
/* .local v0, "now":J */
/* iget-wide v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastInteractiveTimeMills:J */
/* sub-long v2, v0, v2 */
/* sget-wide v4, Lcom/miui/app/smartpower/SmartPowerSettings;->DEF_LAST_INTER_ACTIVE_DURATION:J */
/* cmp-long v2, v2, v4 */
/* if-ltz v2, :cond_1 */
v2 = this.this$1;
com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmLastUidInteractiveTimeMills ( v2 );
/* move-result-wide v2 */
/* sub-long v2, v0, v2 */
/* sget-wide v4, Lcom/miui/app/smartpower/SmartPowerSettings;->DEF_LAST_INTER_ACTIVE_DURATION:J */
/* cmp-long v2, v2, v4 */
/* if-gez v2, :cond_0 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // :cond_1
} // :goto_0
int v2 = 1; // const/4 v2, 0x1
} // :goto_1
} // .end method
public Boolean isMainProc ( ) {
/* .locals 1 */
/* .line 2633 */
/* iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIsMainProc:Z */
} // .end method
public Boolean isProcessPerceptible ( ) {
/* .locals 3 */
/* .line 2752 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I */
int v1 = 1; // const/4 v1, 0x1
/* if-eq v0, v1, :cond_1 */
int v2 = 2; // const/4 v2, 0x2
/* if-eq v0, v2, :cond_1 */
/* iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAudioActive:Z */
/* if-nez v0, :cond_1 */
/* iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mGpsActive:Z */
/* if-nez v0, :cond_1 */
/* iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mNetActive:Z */
/* if-nez v0, :cond_1 */
/* iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mBleActive:Z */
/* if-nez v0, :cond_1 */
v0 = this.this$1;
v0 = com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmIsBackupServiceApp ( v0 );
/* if-nez v0, :cond_1 */
/* .line 2756 */
v0 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) p0 ).isLastInterActive ( ); // invoke-virtual {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isLastInterActive()Z
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :cond_1
} // :goto_0
/* nop */
/* .line 2752 */
} // :goto_1
} // .end method
public Boolean isSystemApp ( ) {
/* .locals 1 */
/* .line 2720 */
v0 = this.this$1;
v0 = com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmSystemApp ( v0 );
} // .end method
public Boolean isSystemSignature ( ) {
/* .locals 1 */
/* .line 2728 */
v0 = this.this$1;
v0 = com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmSystemSignature ( v0 );
} // .end method
public Boolean isVisible ( ) {
/* .locals 2 */
/* .line 2701 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
public void setLastCpuTime ( Long p0 ) {
/* .locals 1 */
/* .param p1, "cpuTime" # J */
/* .line 2836 */
v0 = this.mLastCpuTime;
(( java.util.concurrent.atomic.AtomicLong ) v0 ).set ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V
/* .line 2837 */
return;
} // .end method
public void setPss ( Long p0, Long p1 ) {
/* .locals 0 */
/* .param p1, "pss" # J */
/* .param p3, "swapPss" # J */
/* .line 2777 */
/* iput-wide p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastPss:J */
/* .line 2778 */
/* iput-wide p3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastSwapPss:J */
/* .line 2779 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 2815 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
v1 = this.mProcessName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "/"; // const-string v1, "/"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mUid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "("; // const-string v1, "("
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mPid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ") state="; // const-string v1, ") state="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I */
/* .line 2816 */
com.android.server.am.AppStateManager .processStateToString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " adj="; // const-string v1, " adj="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAdj:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " pri="; // const-string v1, " pri="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 2818 */
v1 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) p0 ).getPriorityScore ( ); // invoke-virtual {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPriorityScore()I
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " usage="; // const-string v1, " usage="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 2819 */
v1 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) p0 ).getUsageLevel ( ); // invoke-virtual {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getUsageLevel()I
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " isKilled="; // const-string v1, " isKilled="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIsKilled:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " is64Bit="; // const-string v1, " is64Bit="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIs64BitProcess:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2815 */
} // .end method
public void updatePss ( ) {
/* .locals 3 */
/* .line 2771 */
int v0 = 3; // const/4 v0, 0x3
/* new-array v0, v0, [J */
/* .line 2772 */
/* .local v0, "swapTmp":[J */
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mPid:I */
int v2 = 0; // const/4 v2, 0x0
android.os.Debug .getPss ( v1,v0,v2 );
/* move-result-wide v1 */
/* iput-wide v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastPss:J */
/* .line 2773 */
int v1 = 1; // const/4 v1, 0x1
/* aget-wide v1, v0, v1 */
/* iput-wide v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastSwapPss:J */
/* .line 2774 */
return;
} // .end method
