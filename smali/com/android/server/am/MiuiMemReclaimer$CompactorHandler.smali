.class Lcom/android/server/am/MiuiMemReclaimer$CompactorHandler;
.super Landroid/os/Handler;
.source "MiuiMemReclaimer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/MiuiMemReclaimer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CompactorHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/MiuiMemReclaimer;


# direct methods
.method public constructor <init>(Lcom/android/server/am/MiuiMemReclaimer;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 130
    iput-object p1, p0, Lcom/android/server/am/MiuiMemReclaimer$CompactorHandler;->this$0:Lcom/android/server/am/MiuiMemReclaimer;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .line 134
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 145
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0

    .line 142
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/am/MiuiMemReclaimer$CompactorHandler;->this$0:Lcom/android/server/am/MiuiMemReclaimer;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1}, Lcom/android/server/am/MiuiMemReclaimer;->-$$Nest$mperformCompactProcesses(Lcom/android/server/am/MiuiMemReclaimer;I)V

    .line 143
    goto :goto_0

    .line 139
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/am/MiuiMemReclaimer$CompactorHandler;->this$0:Lcom/android/server/am/MiuiMemReclaimer;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1, v2}, Lcom/android/server/am/MiuiMemReclaimer;->-$$Nest$mperformCompactProcess(Lcom/android/server/am/MiuiMemReclaimer;Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)V

    .line 140
    goto :goto_0

    .line 136
    :pswitch_2
    iget v0, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0}, Lcom/android/server/am/MiuiMemReclaimer;->reclaimPage(I)V

    .line 137
    nop

    .line 148
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
