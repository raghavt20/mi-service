class com.android.server.am.MemoryFreezeStubImpl$2 extends android.database.ContentObserver {
	 /* .source "MemoryFreezeStubImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/am/MemoryFreezeStubImpl;->registerCloudObserver(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.am.MemoryFreezeStubImpl this$0; //synthetic
/* # direct methods */
 com.android.server.am.MemoryFreezeStubImpl$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/MemoryFreezeStubImpl; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 304 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 1 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 307 */
if ( p2 != null) { // if-eqz p2, :cond_0
	 final String v0 = "cloud_memFreeze_control"; // const-string v0, "cloud_memFreeze_control"
	 android.provider.Settings$System .getUriFor ( v0 );
	 v0 = 	 (( android.net.Uri ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 308 */
		 v0 = this.this$0;
		 (( com.android.server.am.MemoryFreezeStubImpl ) v0 ).updateCloudControlParas ( ); // invoke-virtual {v0}, Lcom/android/server/am/MemoryFreezeStubImpl;->updateCloudControlParas()V
		 /* .line 310 */
	 } // :cond_0
	 return;
} // .end method
