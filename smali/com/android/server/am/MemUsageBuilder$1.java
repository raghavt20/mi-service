class com.android.server.am.MemUsageBuilder$1 implements java.util.Comparator {
	 /* .source "MemUsageBuilder.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/am/MemUsageBuilder;->prepare()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Object;", */
/* "Ljava/util/Comparator<", */
/* "Lcom/android/server/am/ProcessMemInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* # instance fields */
final com.android.server.am.MemUsageBuilder this$0; //synthetic
/* # direct methods */
 com.android.server.am.MemUsageBuilder$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/MemUsageBuilder; */
/* .line 131 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public Integer compare ( com.android.server.am.ProcessMemInfo p0, com.android.server.am.ProcessMemInfo p1 ) {
/* .locals 6 */
/* .param p1, "lhs" # Lcom/android/server/am/ProcessMemInfo; */
/* .param p2, "rhs" # Lcom/android/server/am/ProcessMemInfo; */
/* .line 133 */
/* iget v0, p1, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I */
/* iget v1, p2, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I */
int v2 = -1; // const/4 v2, -0x1
int v3 = 1; // const/4 v3, 0x1
/* if-eq v0, v1, :cond_1 */
/* .line 134 */
/* iget v0, p1, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I */
/* iget v1, p2, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I */
/* if-ge v0, v1, :cond_0 */
} // :cond_0
/* move v2, v3 */
} // :goto_0
/* .line 136 */
} // :cond_1
/* iget-wide v0, p1, Lcom/android/server/am/ProcessMemInfo;->pss:J */
/* iget-wide v4, p2, Lcom/android/server/am/ProcessMemInfo;->pss:J */
/* cmp-long v0, v0, v4 */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 137 */
/* iget-wide v0, p1, Lcom/android/server/am/ProcessMemInfo;->pss:J */
/* iget-wide v4, p2, Lcom/android/server/am/ProcessMemInfo;->pss:J */
/* cmp-long v0, v0, v4 */
/* if-gez v0, :cond_2 */
/* move v2, v3 */
} // :cond_2
/* .line 139 */
} // :cond_3
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer compare ( java.lang.Object p0, java.lang.Object p1 ) { //bridge//synthethic
/* .locals 0 */
/* .line 131 */
/* check-cast p1, Lcom/android/server/am/ProcessMemInfo; */
/* check-cast p2, Lcom/android/server/am/ProcessMemInfo; */
p1 = (( com.android.server.am.MemUsageBuilder$1 ) p0 ).compare ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/am/MemUsageBuilder$1;->compare(Lcom/android/server/am/ProcessMemInfo;Lcom/android/server/am/ProcessMemInfo;)I
} // .end method
