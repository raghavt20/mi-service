.class public final Lcom/android/server/am/SystemPressureControllerStub$$;
.super Ljava/lang/Object;
.source "SystemPressureControllerStub$$.java"

# interfaces
.implements Lcom/miui/base/MiuiStubRegistry$ImplProviderManifest;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final collectImplProviders(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/base/MiuiStubRegistry$ImplProvider<",
            "*>;>;)V"
        }
    .end annotation

    .line 22
    .local p1, "outProviders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/base/MiuiStubRegistry$ImplProvider<*>;>;"
    new-instance v0, Lcom/android/server/am/PreloadAppControllerImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/am/PreloadAppControllerImpl$Provider;-><init>()V

    const-string v1, "com.android.server.am.PreloadAppControllerStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    new-instance v0, Lcom/android/server/am/ProcessListStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/am/ProcessListStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.am.ProcessListStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    new-instance v0, Lcom/android/server/am/MiuiProcessStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/am/MiuiProcessStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.am.MiuiProcessStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    new-instance v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.am.MiuiBoosterUtilsStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    new-instance v0, Lcom/android/server/am/MimdManagerServiceImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/am/MimdManagerServiceImpl$Provider;-><init>()V

    const-string v1, "com.android.server.am.MimdManagerServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    new-instance v0, Lcom/android/server/am/MemoryStandardProcessControl$Provider;

    invoke-direct {v0}, Lcom/android/server/am/MemoryStandardProcessControl$Provider;-><init>()V

    const-string v1, "com.android.server.am.MemoryStandardProcessControlStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    new-instance v0, Lcom/android/server/wm/RealTimeModeControllerImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/RealTimeModeControllerImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.RealTimeModeControllerStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    new-instance v0, Lcom/android/server/appcacheopt/AppCacheOptimizer$Provider;

    invoke-direct {v0}, Lcom/android/server/appcacheopt/AppCacheOptimizer$Provider;-><init>()V

    const-string v1, "com.android.server.appcacheopt.AppCacheOptimizerStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    new-instance v0, Lcom/android/server/am/OomAdjusterImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/am/OomAdjusterImpl$Provider;-><init>()V

    const-string v1, "com.android.server.am.OomAdjusterStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    new-instance v0, Lcom/android/server/am/SystemPressureController$Provider;

    invoke-direct {v0}, Lcom/android/server/am/SystemPressureController$Provider;-><init>()V

    const-string v1, "com.android.server.am.SystemPressureControllerStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    new-instance v0, Lcom/android/server/am/MiuiMemoryInfoImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/am/MiuiMemoryInfoImpl$Provider;-><init>()V

    const-string v1, "com.android.server.am.MiuiMemoryInfoStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    new-instance v0, Lcom/android/server/am/ProcessProphetImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/am/ProcessProphetImpl$Provider;-><init>()V

    const-string v1, "com.android.server.am.ProcessProphetStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    new-instance v0, Lcom/android/server/am/SchedBoostManagerInternalStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/am/SchedBoostManagerInternalStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.am.SchedBoostManagerInternalStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    new-instance v0, Lcom/android/server/wm/MiuiFreezeImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/MiuiFreezeImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.MiuiFreezeStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    new-instance v0, Lcom/android/server/am/MemoryFreezeStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/am/MemoryFreezeStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.am.MemoryFreezeStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    new-instance v0, Lcom/android/server/wm/PreloadStateManagerImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/PreloadStateManagerImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.PreloadStateManagerStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    return-void
.end method
