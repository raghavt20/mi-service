.class public Lcom/android/server/am/ProcMemCleanerStatistics;
.super Ljava/lang/Object;
.source "ProcMemCleanerStatistics.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;
    }
.end annotation


# static fields
.field private static final DEBUG:Z

.field public static final EVENT_TAGS:I = 0x13ba0

.field private static final MAX_KILL_REPORT_COUNTS:I

.field public static final MAX_RECENTLY_KILL_REPORT_TIME:J = 0x2710L

.field public static final REASON_CLEAN_UP_MEM:Ljava/lang/String; = "clean_up_mem"

.field public static final REASON_KILL_BG_PROC:Ljava/lang/String; = "kill_bg_proc"

.field private static final TAG:Ljava/lang/String; = "ProcessMemCleanerStatistics"

.field public static final TYPE_FORCE_STOP:I = 0x2

.field public static final TYPE_KILL_NONE:I = 0x0

.field public static final TYPE_KILL_PROC:I = 0x1

.field private static sCommonUsedPackageList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sInstance:Lcom/android/server/am/ProcMemCleanerStatistics;


# instance fields
.field private mDateformat:Ljava/text/SimpleDateFormat;

.field private final mKillInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mLastKillProc:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mPreCleanUpTime:J

.field private mTotalKillsProc:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 21
    sget-boolean v0, Landroid/os/spc/PressureStateSettings;->DEBUG_ALL:Z

    sput-boolean v0, Lcom/android/server/am/ProcMemCleanerStatistics;->DEBUG:Z

    .line 24
    if-eqz v0, :cond_0

    const/16 v0, 0x3e8

    goto :goto_0

    :cond_0
    const/16 v0, 0x12c

    :goto_0
    sput v0, Lcom/android/server/am/ProcMemCleanerStatistics;->MAX_KILL_REPORT_COUNTS:I

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/am/ProcMemCleanerStatistics;->sCommonUsedPackageList:Ljava/util/List;

    .line 45
    const-string v1, "com.tencent.mm"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/ProcMemCleanerStatistics;->mKillInfos:Ljava/util/List;

    .line 37
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/am/ProcMemCleanerStatistics;->mPreCleanUpTime:J

    .line 38
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/ProcMemCleanerStatistics;->mLastKillProc:Landroid/util/ArrayMap;

    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/am/ProcMemCleanerStatistics;->mTotalKillsProc:I

    .line 49
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "HH:mm:ss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/am/ProcMemCleanerStatistics;->mDateformat:Ljava/text/SimpleDateFormat;

    .line 50
    return-void
.end method

.method private debugAppGroupToString()V
    .locals 5

    .line 128
    iget-object v0, p0, Lcom/android/server/am/ProcMemCleanerStatistics;->mLastKillProc:Landroid/util/ArrayMap;

    invoke-virtual {v0}, Landroid/util/ArrayMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 129
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "key:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " value:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/ProcMemCleanerStatistics;->mDateformat:Ljava/text/SimpleDateFormat;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ProcessMemCleanerStatistics"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
    goto :goto_0

    .line 131
    :cond_0
    return-void
.end method

.method private genLastKillKey(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1
    .param p1, "procName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance()Lcom/android/server/am/ProcMemCleanerStatistics;
    .locals 1

    .line 53
    sget-object v0, Lcom/android/server/am/ProcMemCleanerStatistics;->sInstance:Lcom/android/server/am/ProcMemCleanerStatistics;

    if-nez v0, :cond_0

    .line 54
    new-instance v0, Lcom/android/server/am/ProcMemCleanerStatistics;

    invoke-direct {v0}, Lcom/android/server/am/ProcMemCleanerStatistics;-><init>()V

    sput-object v0, Lcom/android/server/am/ProcMemCleanerStatistics;->sInstance:Lcom/android/server/am/ProcMemCleanerStatistics;

    .line 56
    :cond_0
    sget-object v0, Lcom/android/server/am/ProcMemCleanerStatistics;->sInstance:Lcom/android/server/am/ProcMemCleanerStatistics;

    return-object v0
.end method

.method public static isCommonUsedApp(Ljava/lang/String;)Z
    .locals 1
    .param p0, "pckName"    # Ljava/lang/String;

    .line 60
    sget-object v0, Lcom/android/server/am/ProcMemCleanerStatistics;->sCommonUsedPackageList:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public checkLastKillTime(J)V
    .locals 4
    .param p1, "nowTime"    # J

    .line 80
    sget-boolean v0, Lcom/android/server/am/ProcMemCleanerStatistics;->DEBUG:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/server/am/ProcMemCleanerStatistics;->debugAppGroupToString()V

    .line 82
    :cond_0
    iget-wide v0, p0, Lcom/android/server/am/ProcMemCleanerStatistics;->mPreCleanUpTime:J

    sub-long v0, p1, v0

    const-wide/16 v2, 0x2710

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 83
    iget-object v0, p0, Lcom/android/server/am/ProcMemCleanerStatistics;->mLastKillProc:Landroid/util/ArrayMap;

    invoke-virtual {v0}, Landroid/util/ArrayMap;->clear()V

    .line 85
    :cond_1
    iput-wide p1, p0, Lcom/android/server/am/ProcMemCleanerStatistics;->mPreCleanUpTime:J

    .line 86
    return-void
.end method

.method public dumpKillInfo(Ljava/io/PrintWriter;)V
    .locals 5
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 118
    const-string v0, "ProcessMemCleanerStatistics KillInfo"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/android/server/am/ProcMemCleanerStatistics;->mKillInfos:Ljava/util/List;

    monitor-enter v0

    .line 120
    :try_start_0
    const-string/jumbo v1, "total=%s last=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lcom/android/server/am/ProcMemCleanerStatistics;->mTotalKillsProc:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/android/server/am/ProcMemCleanerStatistics;->mKillInfos:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 121
    iget-object v1, p0, Lcom/android/server/am/ProcMemCleanerStatistics;->mKillInfos:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;

    .line 122
    .local v2, "i":Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;
    invoke-virtual {v2}, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 123
    .end local v2    # "i":Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;
    goto :goto_0

    .line 124
    :cond_0
    monitor-exit v0

    .line 125
    return-void

    .line 124
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isLastKillProcess(Ljava/lang/String;IJ)Z
    .locals 7
    .param p1, "procName"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "nowTime"    # J

    .line 68
    iget-object v0, p0, Lcom/android/server/am/ProcMemCleanerStatistics;->mLastKillProc:Landroid/util/ArrayMap;

    invoke-direct {p0, p1, p2}, Lcom/android/server/am/ProcMemCleanerStatistics;->genLastKillKey(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/ArrayMap;->indexOfKey(Ljava/lang/Object;)I

    move-result v0

    .line 69
    .local v0, "index":I
    if-ltz v0, :cond_1

    .line 70
    iget-object v1, p0, Lcom/android/server/am/ProcMemCleanerStatistics;->mLastKillProc:Landroid/util/ArrayMap;

    invoke-virtual {v1, v0}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 71
    .local v1, "value":J
    sub-long v3, v1, p3

    const-wide/16 v5, 0x2710

    cmp-long v3, v3, v5

    if-gtz v3, :cond_0

    .line 72
    const/4 v3, 0x1

    return v3

    .line 74
    :cond_0
    iget-object v3, p0, Lcom/android/server/am/ProcMemCleanerStatistics;->mLastKillProc:Landroid/util/ArrayMap;

    invoke-direct {p0, p1, p2}, Lcom/android/server/am/ProcMemCleanerStatistics;->genLastKillKey(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    .end local v1    # "value":J
    :cond_1
    const/4 v1, 0x0

    return v1
.end method

.method public reportEvent(ILcom/miui/server/smartpower/IAppState$IRunningProcess;JLjava/lang/String;)V
    .locals 6
    .param p1, "eventType"    # I
    .param p2, "app"    # Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .param p3, "pss"    # J
    .param p5, "killReason"    # Ljava/lang/String;

    .line 90
    new-instance v0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;-><init>(Lcom/android/server/am/ProcMemCleanerStatistics;Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo-IA;)V

    .line 91
    .local v0, "info":Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;
    invoke-virtual {p2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUid()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->-$$Nest$fputuid(Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;I)V

    .line 92
    invoke-virtual {p2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->-$$Nest$fputname(Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;Ljava/lang/String;)V

    .line 93
    invoke-virtual {p2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPid()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->-$$Nest$fputpid(Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;I)V

    .line 94
    invoke-static {v0, p1}, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->-$$Nest$fputkillType(Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;I)V

    .line 95
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->-$$Nest$fputtime(Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;J)V

    .line 96
    invoke-static {p2}, Lcom/android/server/am/ProcessMemoryCleaner;->getProcPriority(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->-$$Nest$fputpriority(Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;I)V

    .line 97
    invoke-static {v0, p3, p4}, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->-$$Nest$fputpss(Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;J)V

    .line 98
    invoke-static {v0, p5}, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->-$$Nest$fputkillReason(Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;Ljava/lang/String;)V

    .line 100
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 101
    iget-object v2, p0, Lcom/android/server/am/ProcMemCleanerStatistics;->mLastKillProc:Landroid/util/ArrayMap;

    invoke-virtual {p2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUid()I

    move-result v4

    invoke-direct {p0, v3, v4}, Lcom/android/server/am/ProcMemCleanerStatistics;->genLastKillKey(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->-$$Nest$fgettime(Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    :cond_0
    iget-object v2, p0, Lcom/android/server/am/ProcMemCleanerStatistics;->mKillInfos:Ljava/util/List;

    monitor-enter v2

    .line 104
    :try_start_0
    iget-object v3, p0, Lcom/android/server/am/ProcMemCleanerStatistics;->mKillInfos:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    iget-object v3, p0, Lcom/android/server/am/ProcMemCleanerStatistics;->mKillInfos:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    sget v4, Lcom/android/server/am/ProcMemCleanerStatistics;->MAX_KILL_REPORT_COUNTS:I

    if-lt v3, v4, :cond_1

    .line 106
    iget-object v3, p0, Lcom/android/server/am/ProcMemCleanerStatistics;->mKillInfos:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 108
    :cond_1
    iget v3, p0, Lcom/android/server/am/ProcMemCleanerStatistics;->mTotalKillsProc:I

    add-int/2addr v3, v1

    iput v3, p0, Lcom/android/server/am/ProcMemCleanerStatistics;->mTotalKillsProc:I

    .line 109
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111
    sget-boolean v1, Lcom/android/server/am/ProcessMemoryCleaner;->DEBUG:Z

    if-eqz v1, :cond_2

    .line 112
    const-string v1, "ProcessMemoryCleaner"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "spckill:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "spckill:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const v2, 0x13ba0

    invoke-static {v2, v1}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 115
    return-void

    .line 109
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
