.class Lcom/android/server/am/MemoryControlJobService$1;
.super Ljava/lang/Object;
.source "MemoryControlJobService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/MemoryControlJobService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/MemoryControlJobService;


# direct methods
.method constructor <init>(Lcom/android/server/am/MemoryControlJobService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/MemoryControlJobService;

    .line 29
    iput-object p1, p0, Lcom/android/server/am/MemoryControlJobService$1;->this$0:Lcom/android/server/am/MemoryControlJobService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 32
    const-string v0, "MemoryStandardProcessControl"

    const-string v1, "Complete callback"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    iget-object v0, p0, Lcom/android/server/am/MemoryControlJobService$1;->this$0:Lcom/android/server/am/MemoryControlJobService;

    iget-object v0, v0, Lcom/android/server/am/MemoryControlJobService;->mFinishCallback:Ljava/lang/Runnable;

    monitor-enter v0

    .line 34
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/MemoryControlJobService$1;->this$0:Lcom/android/server/am/MemoryControlJobService;

    invoke-static {v1}, Lcom/android/server/am/MemoryControlJobService;->-$$Nest$fgetmStarted(Lcom/android/server/am/MemoryControlJobService;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 36
    iget-object v1, p0, Lcom/android/server/am/MemoryControlJobService$1;->this$0:Lcom/android/server/am/MemoryControlJobService;

    invoke-static {v1}, Lcom/android/server/am/MemoryControlJobService;->-$$Nest$fgetmJobParams(Lcom/android/server/am/MemoryControlJobService;)Landroid/app/job/JobParameters;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/android/server/am/MemoryControlJobService;->jobFinished(Landroid/app/job/JobParameters;Z)V

    .line 37
    iget-object v1, p0, Lcom/android/server/am/MemoryControlJobService$1;->this$0:Lcom/android/server/am/MemoryControlJobService;

    invoke-static {v1, v3}, Lcom/android/server/am/MemoryControlJobService;->-$$Nest$fputmStarted(Lcom/android/server/am/MemoryControlJobService;Z)V

    .line 39
    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 41
    invoke-static {}, Lcom/android/server/am/MemoryControlJobService;->-$$Nest$sfgetsContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/am/MemoryControlJobService;->killSchedule(Landroid/content/Context;)V

    .line 42
    return-void

    .line 39
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
