.class public final Lcom/android/server/am/ActivityManagerServiceStub$$;
.super Ljava/lang/Object;
.source "ActivityManagerServiceStub$$.java"

# interfaces
.implements Lcom/miui/base/MiuiStubRegistry$ImplProviderManifest;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final collectImplProviders(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/base/MiuiStubRegistry$ImplProvider<",
            "*>;>;)V"
        }
    .end annotation

    .line 67
    .local p1, "outProviders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/base/MiuiStubRegistry$ImplProvider<*>;>;"
    new-instance v0, Lcom/android/server/am/AppNotRespondingDialogImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/am/AppNotRespondingDialogImpl$Provider;-><init>()V

    const-string v1, "com.android.server.am.AppNotRespondingDialogStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    new-instance v0, Lcom/android/server/am/ProcessRecordImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/am/ProcessRecordImpl$Provider;-><init>()V

    const-string v1, "com.android.server.am.ProcessRecordStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    new-instance v0, Lcom/android/server/wm/WindowStateAnimatorImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/WindowStateAnimatorImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.WindowStateAnimatorStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    new-instance v0, Lcom/android/server/wm/ImmersiveModeConfirmationImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/ImmersiveModeConfirmationImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.ImmersiveModeConfirmationStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    new-instance v0, Lcom/android/server/wm/TalkbackWatermark$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/TalkbackWatermark$Provider;-><init>()V

    const-string v1, "com.android.server.wm.TalkbackWatermarkStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    new-instance v0, Lcom/android/server/wm/DimmerImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/DimmerImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.DimmerStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    new-instance v0, Lcom/android/server/wm/OpenBrowserWithUrlImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/OpenBrowserWithUrlImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.OpenBrowserWithUrlStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    new-instance v0, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.ActivityTaskManagerServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    new-instance v0, Lcom/android/server/am/ActivityManagerServiceImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/am/ActivityManagerServiceImpl$Provider;-><init>()V

    const-string v1, "com.android.server.am.ActivityManagerServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    new-instance v0, Lcom/android/server/policy/PhoneWindowManagerStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/policy/PhoneWindowManagerStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.policy.PhoneWindowManagerStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    new-instance v0, Lcom/android/server/am/ActiveServiceManagementImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/am/ActiveServiceManagementImpl$Provider;-><init>()V

    const-string v1, "com.android.server.am.ActiveServiceManagementStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    new-instance v0, Lcom/android/server/am/PendingIntentRecordImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/am/PendingIntentRecordImpl$Provider;-><init>()V

    const-string v1, "com.android.server.am.PendingIntentRecordStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    new-instance v0, Lcom/android/server/am/AppErrorsImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/am/AppErrorsImpl$Provider;-><init>()V

    const-string v1, "com.android.server.am.AppErrorsStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    new-instance v0, Lcom/android/server/input/InputShellCommandStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/input/InputShellCommandStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.input.InputShellCommandStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    new-instance v0, Lcom/android/server/wm/DisplayPolicyStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/DisplayPolicyStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.DisplayPolicyStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    new-instance v0, Lcom/android/server/wm/LetterboxImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/LetterboxImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.LetterboxStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    new-instance v0, Lcom/android/server/wm/MirrorActiveUidsImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/MirrorActiveUidsImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.MirrorActiveUidsStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    new-instance v0, Lcom/android/server/am/PendingIntentControllerStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/am/PendingIntentControllerStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.am.PendingIntentControllerStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    new-instance v0, Lcom/android/server/wm/MiuiScreenProjectionServiceExStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/MiuiScreenProjectionServiceExStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.IMiuiScreenProjectionServiceExStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    new-instance v0, Lcom/android/server/wm/MiuiFreeformUtilImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/MiuiFreeformUtilImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.MiuiFreeformUtilStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    new-instance v0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.input.KeyboardCombinationManagerStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    new-instance v0, Lcom/android/server/wm/TaskSnapshotControllerInjectorImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/TaskSnapshotControllerInjectorImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.TaskSnapshotControllerInjectorStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    new-instance v0, Lcom/android/server/wm/MiuiMiPerfStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/MiuiMiPerfStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.MiuiMiPerfStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    new-instance v0, Lcom/android/server/wm/MiuiDragAndDropStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/MiuiDragAndDropStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.MiuiDragAndDropStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    new-instance v0, Lcom/android/server/wm/ActivityRecordImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/ActivityRecordImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.ActivityRecordStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    new-instance v0, Lcom/android/server/am/AutoStartManagerServiceStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/am/AutoStartManagerServiceStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.am.AutoStartManagerServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    new-instance v0, Lcom/android/server/wm/MiuiMultiWindowServiceImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/MiuiMultiWindowServiceImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.MiuiMultiWindowServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    new-instance v0, Lcom/android/server/wm/MiuiRefreshRatePolicy$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/MiuiRefreshRatePolicy$Provider;-><init>()V

    const-string v1, "com.android.server.wm.RefreshRatePolicyStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    new-instance v0, Lcom/android/server/wm/MiuiOrientationImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/MiuiOrientationImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.MiuiOrientationStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    new-instance v0, Lcom/android/server/wm/FindDeviceLockWindowImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/FindDeviceLockWindowImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.FindDeviceLockWindowStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    new-instance v0, Lcom/android/server/wm/MiuiDragStateImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/MiuiDragStateImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.DragStateStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    new-instance v0, Lcom/android/server/wm/AsyncRotationControllerImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/AsyncRotationControllerImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.AsyncRotationControllerStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    new-instance v0, Lcom/android/server/wm/MiuiFreeFormManagerService$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/MiuiFreeFormManagerService$Provider;-><init>()V

    const-string v1, "com.android.server.wm.MiuiFreeFormManagerServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    new-instance v0, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/TaskSnapshotPersisterInjectorImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.TaskSnapshotPersisterInjectorStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    new-instance v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/ScreenRotationAnimationImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.ScreenRotationAnimationStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    new-instance v0, Lcom/android/server/am/MutableActivityManagerShellCommandStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/am/MutableActivityManagerShellCommandStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.am.MutableActivityManagerShellCommandStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    new-instance v0, Lcom/android/server/wm/MiuiMultiTaskManager$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/MiuiMultiTaskManager$Provider;-><init>()V

    const-string v1, "com.android.server.wm.MiuiMultiTaskManagerStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    new-instance v0, Lcom/android/server/wm/MiuiDisplayReportImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/MiuiDisplayReportImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.MiuiDisplayReportStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    new-instance v0, Lcom/android/server/input/InputManagerServiceStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/input/InputManagerServiceStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.input.InputManagerServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    new-instance v0, Lcom/android/server/wm/MIUIWatermark$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/MIUIWatermark$Provider;-><init>()V

    const-string v1, "com.android.server.wm.MIUIWatermarkStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    new-instance v0, Lcom/android/server/wm/LaunchParamsControllerImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/LaunchParamsControllerImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.LaunchParamsControllerStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    new-instance v0, Lcom/android/server/am/AppErrorDialogImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/am/AppErrorDialogImpl$Provider;-><init>()V

    const-string v1, "com.android.server.am.AppErrorDialogStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    new-instance v0, Lcom/android/server/wm/TaskStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/TaskStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.TaskStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    new-instance v0, Lcom/android/server/wm/DragDropControllerImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/DragDropControllerImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.DragDropControllerStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    new-instance v0, Lcom/android/server/wm/DisplayContentStubImpl$MutableDisplayContentImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/DisplayContentStubImpl$MutableDisplayContentImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.DisplayContentStub$MutableDisplayContentStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    new-instance v0, Lcom/android/server/wm/OrientationSensorJudgeImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/OrientationSensorJudgeImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.OrientationSensorJudgeStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    new-instance v0, Lcom/android/server/wm/TaskStubImpl$MutableTaskStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/TaskStubImpl$MutableTaskStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.TaskStub$MutableTaskStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    new-instance v0, Lcom/android/server/wm/WindowManagerServiceImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/WindowManagerServiceImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.WindowManagerServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    new-instance v0, Lcom/android/server/wm/WallpaperWindowTokenImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/WallpaperWindowTokenImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.WallpaperWindowTokenStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    new-instance v0, Lcom/android/server/wm/WindowContainerImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/WindowContainerImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.WindowContainerStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    new-instance v0, Lcom/android/server/am/ContentProviderRecordImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/am/ContentProviderRecordImpl$Provider;-><init>()V

    const-string v1, "com.android.server.am.ContentProviderRecordStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    new-instance v0, Lcom/android/server/wm/WallpaperControllerImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/WallpaperControllerImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.WallpaperControllerStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    new-instance v0, Lcom/android/server/wm/ActivityTaskSupervisorImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/ActivityTaskSupervisorImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.ActivityTaskSupervisorStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    new-instance v0, Lcom/android/server/am/MutableActiveServicesStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/am/MutableActiveServicesStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.am.MutableActiveServicesStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    new-instance v0, Lcom/android/server/wm/DisplayContentStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/DisplayContentStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.DisplayContentStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    new-instance v0, Lcom/android/server/wm/ActivityStarterImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/ActivityStarterImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.ActivityStarterStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    new-instance v0, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/ActivityRecordImpl$MutableActivityRecordImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.ActivityRecordStub$MutableActivityRecordStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    new-instance v0, Lcom/android/server/wm/WindowStateStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/WindowStateStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.WindowStateStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    new-instance v0, Lcom/android/server/am/BroadcastQueueModernStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/am/BroadcastQueueModernStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.am.BroadcastQueueModernStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    new-instance v0, Lcom/android/server/wm/InsetsPolicyStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/InsetsPolicyStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.InsetsPolicyStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    new-instance v0, Lcom/android/server/wm/AppRTWmsImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/AppRTWmsImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.AppRTWmsStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    new-instance v0, Lcom/android/server/wm/MiuiMirrorDragEventImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/wm/MiuiMirrorDragEventImpl$Provider;-><init>()V

    const-string v1, "com.android.server.wm.MiuiMirrorDragEventStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    return-void
.end method
