class com.android.server.am.ProcessMemoryCleaner$H extends android.os.Handler {
	 /* .source "ProcessMemoryCleaner.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/ProcessMemoryCleaner; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "H" */
} // .end annotation
/* # instance fields */
private java.util.List mPadSmallWindowWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
final com.android.server.am.ProcessMemoryCleaner this$0; //synthetic
/* # direct methods */
public com.android.server.am.ProcessMemoryCleaner$H ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 737 */
this.this$0 = p1;
/* .line 738 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 735 */
/* new-instance p1, Ljava/util/ArrayList; */
/* invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V */
this.mPadSmallWindowWhiteList = p1;
/* .line 739 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 4 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 743 */
/* invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V */
/* .line 744 */
/* iget v0, p1, Landroid/os/Message;->what:I */
final String v1 = "ProcessMemoryCleaner"; // const-string v1, "ProcessMemoryCleaner"
/* packed-switch v0, :pswitch_data_0 */
/* goto/16 :goto_0 */
/* .line 754 */
/* :pswitch_0 */
try { // :try_start_0
v0 = this.this$0;
v2 = this.obj;
/* check-cast v2, Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
com.android.server.am.ProcessMemoryCleaner .-$$Nest$mcheckBackgroundProcCompact ( v0,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 757 */
/* .line 755 */
/* :catch_0 */
/* move-exception v0 */
/* .line 756 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "checkBackgroundProcCompact failed: "; // const-string v3, "checkBackgroundProcCompact failed: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 758 */
} // .end local v0 # "e":Ljava/lang/Exception;
/* .line 764 */
/* :pswitch_1 */
v0 = this.mPadSmallWindowWhiteList;
v1 = this.obj;
/* check-cast v1, Ljava/lang/String; */
/* .line 765 */
v0 = this.this$0;
final String v1 = "PadSmallWindowClean"; // const-string v1, "PadSmallWindowClean"
v2 = this.mPadSmallWindowWhiteList;
/* const/16 v3, 0x31f */
com.android.server.am.ProcessMemoryCleaner .-$$Nest$mkillProcessByMinAdj ( v0,v3,v1,v2 );
/* .line 767 */
v0 = this.mPadSmallWindowWhiteList;
/* .line 768 */
/* .line 760 */
/* :pswitch_2 */
v0 = this.this$0;
com.android.server.am.ProcessMemoryCleaner .-$$Nest$fgetmContext ( v0 );
com.android.server.am.ProcessMemoryCleaner .-$$Nest$mregisterCloudObserver ( v0,v1 );
/* .line 761 */
v0 = this.this$0;
com.android.server.am.ProcessMemoryCleaner .-$$Nest$mupdateCloudControlData ( v0 );
/* .line 762 */
/* .line 747 */
/* :pswitch_3 */
try { // :try_start_1
v0 = this.this$0;
v2 = this.obj;
/* check-cast v2, Ljava/lang/String; */
/* iget v3, p1, Landroid/os/Message;->arg1:I */
com.android.server.am.ProcessMemoryCleaner .-$$Nest$mcheckBackgroundAppException ( v0,v2,v3 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 750 */
/* .line 748 */
/* :catch_1 */
/* move-exception v0 */
/* .line 749 */
/* .restart local v0 # "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "checkBackgroundAppException failed: "; // const-string v3, "checkBackgroundAppException failed: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 751 */
} // .end local v0 # "e":Ljava/lang/Exception;
/* nop */
/* .line 772 */
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
