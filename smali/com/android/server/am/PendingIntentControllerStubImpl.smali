.class public Lcom/android/server/am/PendingIntentControllerStubImpl;
.super Ljava/lang/Object;
.source "PendingIntentControllerStubImpl.java"

# interfaces
.implements Lcom/android/server/am/PendingIntentControllerStub;


# static fields
.field private static final DEFAULT_PENDINGINTENT_ERROR_THRESHOLD:I = 0x2710

.field private static final TAG:Ljava/lang/String; = "PendingIntentControllerStubImpl"


# instance fields
.field public PENDINGINTENT_ERROR_THRESHOLD:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/16 v0, 0x2710

    iput v0, p0, Lcom/android/server/am/PendingIntentControllerStubImpl;->PENDINGINTENT_ERROR_THRESHOLD:I

    return-void
.end method


# virtual methods
.method public checkIsTooManyPendingIntent(IILcom/android/server/am/PendingIntentRecord;Landroid/util/SparseIntArray;)V
    .locals 4
    .param p1, "newCount"    # I
    .param p2, "uid"    # I
    .param p3, "pir"    # Lcom/android/server/am/PendingIntentRecord;
    .param p4, "intentsPerUid"    # Landroid/util/SparseIntArray;

    .line 19
    iget v0, p0, Lcom/android/server/am/PendingIntentControllerStubImpl;->PENDINGINTENT_ERROR_THRESHOLD:I

    if-lt p1, v0, :cond_1

    .line 20
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Too many PendingIntent created for uid "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", aborting "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p3, Lcom/android/server/am/PendingIntentRecord;->key:Lcom/android/server/am/PendingIntentRecord$Key;

    .line 21
    invoke-virtual {v1}, Lcom/android/server/am/PendingIntentRecord$Key;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 22
    .local v0, "msg":Ljava/lang/String;
    const-string v1, "PendingIntentControllerStubImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 25
    const/16 v2, 0x3e8

    if-ne p2, v2, :cond_0

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 26
    const-string v2, "Too many PendingIntent created for system_server"

    invoke-static {v1, v2}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 28
    :cond_0
    add-int/lit8 v1, p1, -0x1

    invoke-virtual {p4, p2, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 29
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 32
    .end local v0    # "msg":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void
.end method
