.class public Lcom/android/server/am/MemoryControlCloud;
.super Ljava/lang/Object;
.source "MemoryControlCloud.java"


# static fields
.field private static final CLOUD_MEMORY_STANDARD_APPHEAP_ENABLE:Ljava/lang/String; = "cloud_memory_standard_appheap_enable"

.field private static final CLOUD_MEMORY_STANDARD_ENABLE:Ljava/lang/String; = "cloud_memory_standard_enable"

.field private static final CLOUD_MEMORY_STANDARD_PROC_LIST:Ljava/lang/String; = "cloud_memory_standard_proc_list"

.field private static final CLOUD_MEMORY_STANDARD_PROC_LIST_JSON:Ljava/lang/String; = "cloud_memory_standard_proc_list_json"

.field private static final CLOUD_MEMORY_STANDRAD_APPHEAP_LIST_JSON:Ljava/lang/String; = "cloud_memory_standard_appheap_list_json"

.field private static final CLOUD_MRMORY_STANDARD_APPHEAP_HANDLE_TIME:Ljava/lang/String; = "cloud_memory_standard_appheap_handle_time"

.field private static final CLOUD_SCREEN_OFF_STRATEGY:Ljava/lang/String; = "cloud_memory_standard_screen_off_strategy"

.field private static final TAG:Ljava/lang/String; = "MemoryStandardProcessControl"


# instance fields
.field private mspc:Lcom/android/server/am/MemoryStandardProcessControl;


# direct methods
.method static bridge synthetic -$$Nest$fgetmspc(Lcom/android/server/am/MemoryControlCloud;)Lcom/android/server/am/MemoryStandardProcessControl;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/MemoryControlCloud;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    return-object p0
.end method

.method constructor <init>()V
    .locals 1

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/am/MemoryControlCloud;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    .line 27
    return-void
.end method

.method private getCloudAppHeapHandleTime(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 171
    const-string v0, "MemoryStandardProcessControl"

    invoke-direct {p0, p1}, Lcom/android/server/am/MemoryControlCloud;->registerCloudAppHeapHandleTimeObserver(Landroid/content/Context;)V

    .line 172
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "cloud_memory_standard_appheap_handle_time"

    const/4 v3, -0x2

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 175
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 177
    .local v1, "str":Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 178
    .local v2, "handleTime":J
    iget-object v4, p0, Lcom/android/server/am/MemoryControlCloud;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    iput-wide v2, v4, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapHandleTime:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 181
    .end local v1    # "str":Ljava/lang/String;
    .end local v2    # "handleTime":J
    goto :goto_0

    .line 179
    :catch_0
    move-exception v1

    .line 180
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cloud CLOUD_MRMORY_STANDARD_APPHEAP_HANDLE_TIME check failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cloud app heap handle time received: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/am/MemoryControlCloud;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    iget-wide v2, v2, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapHandleTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    :cond_0
    return-void
.end method

.method private getCloudAppHeapList(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 240
    invoke-direct {p0, p1}, Lcom/android/server/am/MemoryControlCloud;->registerCloudAppHeapListObserver(Landroid/content/Context;)V

    .line 241
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "cloud_memory_standard_appheap_list_json"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 243
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 245
    .local v0, "str":Ljava/lang/String;
    if-eqz v0, :cond_1

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 248
    :cond_0
    iget-object v1, p0, Lcom/android/server/am/MemoryControlCloud;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    invoke-virtual {v1, v0}, Lcom/android/server/am/MemoryStandardProcessControl;->callUpdateCloudAppHeapConfig(Ljava/lang/String;)V

    goto :goto_1

    .line 246
    :cond_1
    :goto_0
    return-void

    .line 250
    .end local v0    # "str":Ljava/lang/String;
    :cond_2
    :goto_1
    return-void
.end method

.method private getCloudProcList(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 207
    invoke-direct {p0, p1}, Lcom/android/server/am/MemoryControlCloud;->registerCloudProcListObserver(Landroid/content/Context;)V

    .line 208
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "cloud_memory_standard_proc_list_json"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 210
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 212
    .local v0, "str":Ljava/lang/String;
    if-eqz v0, :cond_1

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 215
    :cond_0
    iget-object v1, p0, Lcom/android/server/am/MemoryControlCloud;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    invoke-virtual {v1, v0}, Lcom/android/server/am/MemoryStandardProcessControl;->callUpdateCloudConfig(Ljava/lang/String;)V

    goto :goto_1

    .line 213
    :cond_1
    :goto_0
    return-void

    .line 217
    .end local v0    # "str":Ljava/lang/String;
    :cond_2
    :goto_1
    return-void
.end method

.method private getCloudSwitch(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .line 108
    invoke-direct {p0, p1}, Lcom/android/server/am/MemoryControlCloud;->registerCloudEnableObserver(Landroid/content/Context;)V

    .line 109
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "cloud_memory_standard_enable"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    const-string v4, "MemoryStandardProcessControl"

    if-eqz v0, :cond_0

    .line 112
    :try_start_0
    iget-object v0, p0, Lcom/android/server/am/MemoryControlCloud;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    .line 113
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-static {v5, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 112
    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/android/server/am/MemoryStandardProcessControl;->mEnable:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    goto :goto_0

    .line 115
    :catch_0
    move-exception v0

    .line 116
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cloud CLOUD_MEMORY_STANDARD_ENABLE check failed: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    iget-object v1, p0, Lcom/android/server/am/MemoryControlCloud;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    iput-boolean v3, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mEnable:Z

    .line 119
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "set enable state from database: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/MemoryControlCloud;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    iget-boolean v1, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "cloud_memory_standard_screen_off_strategy"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 124
    :try_start_1
    iget-object v0, p0, Lcom/android/server/am/MemoryControlCloud;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    .line 125
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-static {v5, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 124
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenOffStrategy:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 131
    goto :goto_1

    .line 127
    :catch_1
    move-exception v0

    .line 128
    .restart local v0    # "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cloud mScreenOffStrategy check failed: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    iget-object v1, p0, Lcom/android/server/am/MemoryControlCloud;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    iput v3, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenOffStrategy:I

    .line 130
    iget-object v1, p0, Lcom/android/server/am/MemoryControlCloud;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    iput-boolean v3, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mEnable:Z

    .line 132
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "set screen off strategy from database: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/MemoryControlCloud;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    iget v1, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenOffStrategy:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "cloud_memory_standard_appheap_enable"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 137
    :try_start_2
    iget-object v0, p0, Lcom/android/server/am/MemoryControlCloud;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    .line 138
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-static {v5, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 137
    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapEnable:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 143
    goto :goto_2

    .line 140
    :catch_2
    move-exception v0

    .line 141
    .restart local v0    # "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cloud CLOUD_MEMORY_STANDARD_APPHEAP_ENABLE check failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    iget-object v1, p0, Lcom/android/server/am/MemoryControlCloud;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    iput-boolean v3, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapEnable:Z

    .line 144
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "set appheap enable state from database: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/MemoryControlCloud;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    iget-boolean v1, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    :cond_2
    return-void
.end method

.method private registerCloudAppHeapHandleTimeObserver(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 149
    new-instance v0, Lcom/android/server/am/MemoryControlCloud$4;

    iget-object v1, p0, Lcom/android/server/am/MemoryControlCloud;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    iget-object v1, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mHandler:Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;

    invoke-direct {v0, p0, v1, p1}, Lcom/android/server/am/MemoryControlCloud$4;-><init>(Lcom/android/server/am/MemoryControlCloud;Landroid/os/Handler;Landroid/content/Context;)V

    .line 165
    .local v0, "observer":Landroid/database/ContentObserver;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 166
    const-string v2, "cloud_memory_standard_appheap_handle_time"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 165
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 168
    return-void
.end method

.method private registerCloudAppHeapListObserver(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 221
    new-instance v0, Lcom/android/server/am/MemoryControlCloud$6;

    iget-object v1, p0, Lcom/android/server/am/MemoryControlCloud;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    iget-object v1, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mHandler:Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;

    invoke-direct {v0, p0, v1, p1}, Lcom/android/server/am/MemoryControlCloud$6;-><init>(Lcom/android/server/am/MemoryControlCloud;Landroid/os/Handler;Landroid/content/Context;)V

    .line 234
    .local v0, "observer":Landroid/database/ContentObserver;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 235
    const-string v2, "cloud_memory_standard_appheap_list_json"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 234
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 237
    return-void
.end method

.method private registerCloudEnableObserver(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .line 39
    new-instance v0, Lcom/android/server/am/MemoryControlCloud$1;

    iget-object v1, p0, Lcom/android/server/am/MemoryControlCloud;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    iget-object v1, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mHandler:Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;

    invoke-direct {v0, p0, v1, p1}, Lcom/android/server/am/MemoryControlCloud$1;-><init>(Lcom/android/server/am/MemoryControlCloud;Landroid/os/Handler;Landroid/content/Context;)V

    .line 61
    .local v0, "enableObserver":Landroid/database/ContentObserver;
    new-instance v1, Lcom/android/server/am/MemoryControlCloud$2;

    iget-object v2, p0, Lcom/android/server/am/MemoryControlCloud;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    iget-object v2, v2, Lcom/android/server/am/MemoryStandardProcessControl;->mHandler:Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;

    invoke-direct {v1, p0, v2, p1}, Lcom/android/server/am/MemoryControlCloud$2;-><init>(Lcom/android/server/am/MemoryControlCloud;Landroid/os/Handler;Landroid/content/Context;)V

    .line 79
    .local v1, "strategyObserver":Landroid/database/ContentObserver;
    new-instance v2, Lcom/android/server/am/MemoryControlCloud$3;

    iget-object v3, p0, Lcom/android/server/am/MemoryControlCloud;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    iget-object v3, v3, Lcom/android/server/am/MemoryStandardProcessControl;->mHandler:Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;

    invoke-direct {v2, p0, v3, p1}, Lcom/android/server/am/MemoryControlCloud$3;-><init>(Lcom/android/server/am/MemoryControlCloud;Landroid/os/Handler;Landroid/content/Context;)V

    .line 96
    .local v2, "appheapObserver":Landroid/database/ContentObserver;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 97
    const-string v4, "cloud_memory_standard_enable"

    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 96
    const/4 v5, 0x0

    const/4 v6, -0x2

    invoke-virtual {v3, v4, v5, v0, v6}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 99
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 100
    const-string v4, "cloud_memory_standard_screen_off_strategy"

    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 99
    invoke-virtual {v3, v4, v5, v1, v6}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 102
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 103
    const-string v4, "cloud_memory_standard_appheap_enable"

    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 102
    invoke-virtual {v3, v4, v5, v2, v6}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 105
    return-void
.end method

.method private registerCloudProcListObserver(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 188
    new-instance v0, Lcom/android/server/am/MemoryControlCloud$5;

    iget-object v1, p0, Lcom/android/server/am/MemoryControlCloud;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    iget-object v1, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mHandler:Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;

    invoke-direct {v0, p0, v1, p1}, Lcom/android/server/am/MemoryControlCloud$5;-><init>(Lcom/android/server/am/MemoryControlCloud;Landroid/os/Handler;Landroid/content/Context;)V

    .line 201
    .local v0, "observer":Landroid/database/ContentObserver;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 202
    const-string v2, "cloud_memory_standard_proc_list_json"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 201
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 204
    return-void
.end method


# virtual methods
.method public initCloud(Landroid/content/Context;Lcom/android/server/am/MemoryStandardProcessControl;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mspc"    # Lcom/android/server/am/MemoryStandardProcessControl;

    .line 30
    iput-object p2, p0, Lcom/android/server/am/MemoryControlCloud;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    .line 31
    invoke-direct {p0, p1}, Lcom/android/server/am/MemoryControlCloud;->getCloudSwitch(Landroid/content/Context;)V

    .line 32
    invoke-direct {p0, p1}, Lcom/android/server/am/MemoryControlCloud;->getCloudProcList(Landroid/content/Context;)V

    .line 33
    invoke-direct {p0, p1}, Lcom/android/server/am/MemoryControlCloud;->getCloudAppHeapList(Landroid/content/Context;)V

    .line 34
    invoke-direct {p0, p1}, Lcom/android/server/am/MemoryControlCloud;->getCloudAppHeapHandleTime(Landroid/content/Context;)V

    .line 35
    const/4 v0, 0x1

    return v0
.end method
