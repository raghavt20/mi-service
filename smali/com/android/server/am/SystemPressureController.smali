.class public final Lcom/android/server/am/SystemPressureController;
.super Lcom/android/server/am/SystemPressureControllerStub;
.source "SystemPressureController.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.am.SystemPressureControllerStub$$"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/SystemPressureController$H;,
        Lcom/android/server/am/SystemPressureController$ScreenStateReceiver;
    }
.end annotation


# static fields
.field private static final IS_ENABLE_RECLAIM:Z

.field private static final MSG_CLEAN_UP_MEMLVL_PROCESS:I = 0x1

.field private static final MSG_REGISTER_CLOUD_OBSERVER:I = 0x3

.field private static final MSG_RESET_STAERTING_APP_STATE:I = 0x2

.field private static final MSG_UPDATE_SCREEN_STATE:I = 0x4

.field private static final PAD_SMALL_WINDOW_CLEAN_ENABLE:Z

.field private static final PREF_SHIELDER_PROC:Ljava/lang/String; = "perf_shielder_Proc"

.field private static final PREF_SHIELDER_PROC_CLOUD_MOUDLE:Ljava/lang/String; = "perf_process"

.field private static final PROC_GAME_LIST_CLOUD:Ljava/lang/String; = "perf_proc_game_List"

.field private static final PROC_GAME_OOM_CLOUD_ENABLE:Ljava/lang/String; = "perf_game_oom_enable"

.field private static final RECLAIM_EVENT_NODE:Ljava/lang/String; = "/sys/kernel/mi_reclaim/event"

.field public static final STAERT_APP_TIMEOUT:I = 0x7d0

.field public static final TAG:Ljava/lang/String; = "SystemPressureControl"

.field public static final THREAD_GROUP_FOREGROUND:I = 0x1

.field private static final TOTAL_MEMORY:J

.field private static mKillPkgPermList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mKillProcPermList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sGameAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sInstance:Lcom/android/server/am/SystemPressureController;


# instance fields
.field private mAms:Lcom/android/server/am/ActivityManagerService;

.field private mContext:Landroid/content/Context;

.field private mForegroundPackageName:Ljava/lang/String;

.field private mGameOomEnable:Z

.field private mHandler:Lcom/android/server/am/SystemPressureController$H;

.field private mHandlerTh:Landroid/os/HandlerThread;

.field private mIsStartingApp:Z

.field private mLastProcessCleanTimeMillis:J

.field private mPms:Lcom/android/server/am/ProcessManagerService;

.field mPowerCleaner:Lcom/android/server/am/ProcessPowerCleaner;

.field private mProcessCleanHandlerTh:Landroid/os/HandlerThread;

.field mProcessCleaner:Lcom/android/server/am/ProcessMemoryCleaner;

.field mProcessGameCleaner:Lcom/android/server/am/ProcessGameCleaner;

.field mSceneCleaner:Lcom/android/server/am/ProcessSceneCleaner;

.field private mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

.field public mStartingAppLock:Ljava/lang/Object;

.field private mSystemReady:Z

.field private final mThermalTempListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/server/am/ThermalTempListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/am/SystemPressureController;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/SystemPressureController;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/am/SystemPressureController;)Lcom/android/server/am/SystemPressureController$H;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/SystemPressureController;->mHandler:Lcom/android/server/am/SystemPressureController$H;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mcleanUpMemory(Lcom/android/server/am/SystemPressureController;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/am/SystemPressureController;->cleanUpMemory(J)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mregisterCloudObserver(Lcom/android/server/am/SystemPressureController;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/SystemPressureController;->registerCloudObserver(Landroid/content/Context;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mresetStartingAppState(Lcom/android/server/am/SystemPressureController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/SystemPressureController;->resetStartingAppState()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateCloudControlData(Lcom/android/server/am/SystemPressureController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/SystemPressureController;->updateCloudControlData()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 7

    .line 71
    invoke-static {}, Landroid/os/Process;->getTotalMemory()J

    move-result-wide v0

    const/16 v2, 0x1e

    shr-long/2addr v0, v2

    sput-wide v0, Lcom/android/server/am/SystemPressureController;->TOTAL_MEMORY:J

    .line 86
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sput-object v2, Lcom/android/server/am/SystemPressureController;->sGameAppList:Ljava/util/List;

    .line 89
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sput-object v2, Lcom/android/server/am/SystemPressureController;->mKillProcPermList:Ljava/util/List;

    .line 98
    const-string v3, "com.miui.powerkeeper:ui"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    sget-object v2, Lcom/android/server/am/SystemPressureController;->mKillProcPermList:Ljava/util/List;

    const-string v3, "com.miui.powerkeeper"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    sget-object v2, Lcom/android/server/am/SystemPressureController;->mKillProcPermList:Ljava/util/List;

    const-string v3, "com.miui.cleanmaster"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 101
    sget-object v2, Lcom/android/server/am/SystemPressureController;->mKillProcPermList:Ljava/util/List;

    const-string v3, "com.miui.voiceassist"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    sget-object v2, Lcom/android/server/am/SystemPressureController;->mKillProcPermList:Ljava/util/List;

    const-string/jumbo v3, "system"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sput-object v2, Lcom/android/server/am/SystemPressureController;->mKillPkgPermList:Ljava/util/List;

    .line 107
    const-string v3, "com.miui.home"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    sget-object v2, Lcom/android/server/am/SystemPressureController;->mKillPkgPermList:Ljava/util/List;

    const-string v3, "com.mi.android.globallauncher"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    sget-object v2, Lcom/android/server/am/SystemPressureController;->mKillPkgPermList:Ljava/util/List;

    const-string v3, "com.miui.securitycenter"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    sget-object v2, Lcom/android/server/am/SystemPressureController;->mKillPkgPermList:Ljava/util/List;

    const-string v3, "com.miui.cleaner"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    const-wide/16 v2, 0x8

    cmp-long v2, v0, v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-gez v2, :cond_0

    move v2, v3

    goto :goto_0

    :cond_0
    move v2, v4

    :goto_0
    sput-boolean v2, Lcom/android/server/am/SystemPressureController;->IS_ENABLE_RECLAIM:Z

    .line 117
    const-wide/16 v5, 0x6

    cmp-long v0, v0, v5

    if-gez v0, :cond_1

    goto :goto_1

    :cond_1
    move v3, v4

    :goto_1
    sput-boolean v3, Lcom/android/server/am/SystemPressureController;->PAD_SMALL_WINDOW_CLEAN_ENABLE:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 50
    invoke-direct {p0}, Lcom/android/server/am/SystemPressureControllerStub;-><init>()V

    .line 67
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "SystemPressureControlTh"

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/android/server/am/SystemPressureController;->mHandlerTh:Landroid/os/HandlerThread;

    .line 69
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "ProcessCleanTh"

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/android/server/am/SystemPressureController;->mProcessCleanHandlerTh:Landroid/os/HandlerThread;

    .line 87
    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/am/SystemPressureController;->mForegroundPackageName:Ljava/lang/String;

    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/am/SystemPressureController;->mGameOomEnable:Z

    .line 90
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/SystemPressureController;->mThermalTempListeners:Ljava/util/ArrayList;

    .line 92
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/am/SystemPressureController;->mSystemReady:Z

    .line 94
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/android/server/am/SystemPressureController;->mStartingAppLock:Ljava/lang/Object;

    .line 95
    iput-boolean v0, p0, Lcom/android/server/am/SystemPressureController;->mIsStartingApp:Z

    return-void
.end method

.method private checkKillProcPermission(I)Z
    .locals 3
    .param p1, "callingPid"    # I

    .line 662
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mPms:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;

    move-result-object v0

    .line 663
    .local v0, "app":Lcom/android/server/am/ProcessRecord;
    sget-object v1, Lcom/android/server/am/SystemPressureController;->mKillProcPermList:Ljava/util/List;

    iget-object v2, v0, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/android/server/am/SystemPressureController;->mKillPkgPermList:Ljava/util/List;

    iget-object v2, v0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 664
    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 667
    :cond_0
    const/4 v1, 0x0

    return v1

    .line 665
    :cond_1
    :goto_0
    const/4 v1, 0x1

    return v1
.end method

.method private cleanUpMemory(J)V
    .locals 9
    .param p1, "targetReleaseMem"    # J

    .line 307
    iget-boolean v0, p0, Lcom/android/server/am/SystemPressureController;->mSystemReady:Z

    if-nez v0, :cond_0

    .line 308
    return-void

    .line 311
    :cond_0
    const/16 v0, 0x1b

    new-array v0, v0, [J

    .line 312
    .local v0, "meminfo":[J
    invoke-static {v0}, Landroid/os/Debug;->getMemInfo([J)V

    .line 313
    new-instance v1, Landroid/os/spc/MemoryCleanInfo;

    invoke-direct {v1}, Landroid/os/spc/MemoryCleanInfo;-><init>()V

    .line 314
    .local v1, "cInfo":Landroid/os/spc/MemoryCleanInfo;
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, v1, Landroid/os/spc/MemoryCleanInfo;->time:J

    .line 315
    const/4 v2, 0x1

    aget-wide v3, v0, v2

    iput-wide v3, v1, Landroid/os/spc/MemoryCleanInfo;->beforeMemFree:J

    .line 316
    const/4 v3, 0x3

    aget-wide v4, v0, v3

    aget-wide v6, v0, v2

    add-long/2addr v4, v6

    iput-wide v4, v1, Landroid/os/spc/MemoryCleanInfo;->beforeMemAvail:J

    .line 317
    const/4 v4, 0x0

    aget-wide v5, v0, v4

    iput-wide v5, v1, Landroid/os/spc/MemoryCleanInfo;->memTotal:J

    .line 318
    const/16 v5, 0x8

    aget-wide v5, v0, v5

    iput-wide v5, v1, Landroid/os/spc/MemoryCleanInfo;->swapTotal:J

    .line 321
    :try_start_0
    iget-object v5, p0, Lcom/android/server/am/SystemPressureController;->mProcessCleaner:Lcom/android/server/am/ProcessMemoryCleaner;

    invoke-virtual {v5, p1, p2}, Lcom/android/server/am/ProcessMemoryCleaner;->scanProcessAndCleanUpMemory(J)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 324
    goto :goto_0

    .line 322
    :catch_0
    move-exception v5

    .line 323
    .local v5, "e":Ljava/lang/Exception;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "scanProcessAndCleanUpMemory failed: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "SystemPressureControl"

    invoke-static {v7, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    .end local v5    # "e":Ljava/lang/Exception;
    :goto_0
    invoke-static {v0}, Landroid/os/Debug;->getMemInfo([J)V

    .line 326
    aget-wide v5, v0, v2

    iput-wide v5, v1, Landroid/os/spc/MemoryCleanInfo;->afterMemFree:J

    .line 327
    aget-wide v5, v0, v3

    aget-wide v7, v0, v2

    add-long/2addr v5, v7

    iput-wide v5, v1, Landroid/os/spc/MemoryCleanInfo;->afterMemAvail:J

    .line 329
    :try_start_1
    sget-boolean v5, Lcom/android/server/am/ProcessMemoryCleaner;->DEBUG:Z

    if-eqz v5, :cond_1

    .line 330
    const-string v5, "ProcessMemoryCleaner"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "memory clean info: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Landroid/os/spc/MemoryCleanInfo;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    :cond_1
    const/4 v5, 0x7

    new-array v5, v5, [Ljava/lang/Object;

    iget-wide v6, v1, Landroid/os/spc/MemoryCleanInfo;->memTotal:J

    .line 333
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v4

    iget-wide v6, v1, Landroid/os/spc/MemoryCleanInfo;->swapTotal:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v5, v2

    iget-wide v6, v1, Landroid/os/spc/MemoryCleanInfo;->beforeMemFree:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v4, 0x2

    aput-object v2, v5, v4

    iget-wide v6, v1, Landroid/os/spc/MemoryCleanInfo;->beforeMemAvail:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v5, v3

    iget-wide v2, v1, Landroid/os/spc/MemoryCleanInfo;->afterMemFree:J

    .line 334
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x4

    aput-object v2, v5, v3

    iget-wide v2, v1, Landroid/os/spc/MemoryCleanInfo;->afterMemAvail:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x5

    aput-object v2, v5, v3

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x6

    aput-object v2, v5, v3

    .line 332
    const v2, 0x13ba0

    invoke-static {v2, v5}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 337
    goto :goto_1

    .line 335
    :catch_1
    move-exception v2

    .line 336
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 338
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method

.method public static getInstance()Lcom/android/server/am/SystemPressureController;
    .locals 1

    .line 157
    const-class v0, Lcom/android/server/am/SystemPressureControllerStub;

    invoke-static {v0}, Lcom/miui/base/MiuiStubUtil;->getImpl(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/SystemPressureController;

    return-object v0
.end method

.method private killPackage(Lcom/android/server/am/ProcessRecord;ILjava/lang/String;)J
    .locals 4
    .param p1, "proc"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "minAdj"    # I
    .param p3, "reason"    # Ljava/lang/String;

    .line 679
    iget-boolean v0, p0, Lcom/android/server/am/SystemPressureController;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 680
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mProcessCleaner:Lcom/android/server/am/ProcessMemoryCleaner;

    iget-object v1, p0, Lcom/android/server/am/SystemPressureController;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 681
    invoke-interface {v1, v2, v3}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->getRunningProcess(ILjava/lang/String;)Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    move-result-object v1

    .line 680
    invoke-virtual {v0, v1, p2, p3}, Lcom/android/server/am/ProcessMemoryCleaner;->killPackage(Lcom/miui/server/smartpower/IAppState$IRunningProcess;ILjava/lang/String;)J

    move-result-wide v0

    return-wide v0

    .line 684
    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method private killPackage(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)J
    .locals 4
    .param p1, "proc"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "reason"    # Ljava/lang/String;

    .line 671
    iget-boolean v0, p0, Lcom/android/server/am/SystemPressureController;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 672
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mProcessCleaner:Lcom/android/server/am/ProcessMemoryCleaner;

    iget-object v1, p0, Lcom/android/server/am/SystemPressureController;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 673
    invoke-interface {v1, v2, v3}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->getRunningProcess(ILjava/lang/String;)Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    move-result-object v1

    .line 672
    invoke-virtual {v0, v1, p2}, Lcom/android/server/am/ProcessMemoryCleaner;->killPackage(Lcom/miui/server/smartpower/IAppState$IRunningProcess;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0

    .line 675
    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method private killProcess(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)J
    .locals 4
    .param p1, "proc"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "reason"    # Ljava/lang/String;

    .line 431
    iget-boolean v0, p0, Lcom/android/server/am/SystemPressureController;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 432
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mProcessCleaner:Lcom/android/server/am/ProcessMemoryCleaner;

    iget-object v1, p0, Lcom/android/server/am/SystemPressureController;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 433
    invoke-interface {v1, v2, v3}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->getRunningProcess(ILjava/lang/String;)Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    move-result-object v1

    .line 432
    invoke-virtual {v0, v1, p2}, Lcom/android/server/am/ProcessMemoryCleaner;->killProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0

    .line 434
    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method private static native nDumpCpuLimit(Ljava/io/PrintWriter;)V
.end method

.method private native nEndPressureMonitor()V
.end method

.method private static native nGetBackgroundCpuPolicy()Ljava/lang/String;
.end method

.method private static native nGetCpuUsage(ILjava/lang/String;)J
.end method

.method private native nInit()V
.end method

.method private native nStartPressureMonitor()V
.end method

.method private registerCloudObserver(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 341
    new-instance v0, Lcom/android/server/am/SystemPressureController$1;

    iget-object v1, p0, Lcom/android/server/am/SystemPressureController;->mHandler:Lcom/android/server/am/SystemPressureController$H;

    invoke-direct {v0, p0, v1}, Lcom/android/server/am/SystemPressureController$1;-><init>(Lcom/android/server/am/SystemPressureController;Landroid/os/Handler;)V

    .line 351
    .local v0, "observer":Landroid/database/ContentObserver;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 352
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v2

    .line 351
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 354
    return-void
.end method

.method private registerScreenStateReceiver()V
    .locals 3

    .line 723
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 724
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 725
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 726
    iget-object v1, p0, Lcom/android/server/am/SystemPressureController;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/android/server/am/SystemPressureController$ScreenStateReceiver;

    invoke-direct {v2, p0}, Lcom/android/server/am/SystemPressureController$ScreenStateReceiver;-><init>(Lcom/android/server/am/SystemPressureController;)V

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 727
    return-void
.end method

.method private resetStartingAppState()V
    .locals 2

    .line 580
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/server/am/SystemPressureController;->setAppStartingMode(Z)V

    .line 581
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mStartingAppLock:Ljava/lang/Object;

    monitor-enter v0

    .line 582
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/SystemPressureController;->mStartingAppLock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 583
    monitor-exit v0

    .line 584
    return-void

    .line 583
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private updateCloudControlData()V
    .locals 11

    .line 357
    const-string v0, "SystemPressureControl"

    iget-object v1, p0, Lcom/android/server/am/SystemPressureController;->mContext:Landroid/content/Context;

    .line 358
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "perf_process"

    invoke-static {v1, v2}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataList(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 360
    .local v1, "cloudDataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;"
    if-nez v1, :cond_0

    .line 361
    return-void

    .line 363
    :cond_0
    const-string v2, ""

    .line 364
    .local v2, "data":Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 365
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    const-string v5, "perf_shielder_Proc"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 366
    .local v4, "cd":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 367
    move-object v2, v4

    .line 368
    goto :goto_1

    .line 364
    .end local v4    # "cd":Ljava/lang/String;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 374
    .end local v3    # "i":I
    :cond_2
    :goto_1
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 376
    .local v3, "jsonObject":Lorg/json/JSONObject;
    sget-object v4, Lcom/android/server/am/SystemPressureController;->sGameAppList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 377
    sget-object v4, Lcom/android/server/am/SystemPressureController;->sGameAppList:Ljava/util/List;

    iget-object v5, p0, Lcom/android/server/am/SystemPressureController;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 378
    const v6, 0x110300e6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    .line 377
    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 379
    const-string v4, "perf_proc_game_List"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 380
    .local v4, "gameString":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 381
    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 382
    .local v5, "appArray":[Ljava/lang/String;
    array-length v6, v5

    const/4 v7, 0x0

    :goto_2
    if-ge v7, v6, :cond_4

    aget-object v8, v5, v7

    .line 383
    .local v8, "appPackageName":Ljava/lang/String;
    sget-object v9, Lcom/android/server/am/SystemPressureController;->sGameAppList:Ljava/util/List;

    invoke-interface {v9, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 384
    sget-object v9, Lcom/android/server/am/SystemPressureController;->sGameAppList:Ljava/util/List;

    invoke-interface {v9, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 385
    sget-boolean v9, Landroid/os/spc/PressureStateSettings;->DEBUG_ALL:Z

    if-eqz v9, :cond_3

    .line 386
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "sGameList cloud set received: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v0, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    .end local v8    # "appPackageName":Ljava/lang/String;
    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 392
    .end local v5    # "appArray":[Ljava/lang/String;
    :cond_4
    const-string v5, "perf_game_oom_enable"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 393
    .local v5, "gameOomString":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 394
    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/android/server/am/SystemPressureController;->mGameOomEnable:Z

    .line 395
    sget-boolean v6, Landroid/os/spc/PressureStateSettings;->DEBUG_ALL:Z

    if-eqz v6, :cond_5

    .line 396
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mGameOomEnable cloud set received: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 401
    .end local v3    # "jsonObject":Lorg/json/JSONObject;
    .end local v4    # "gameString":Ljava/lang/String;
    .end local v5    # "gameOomString":Ljava/lang/String;
    :cond_5
    goto :goto_3

    .line 399
    :catch_0
    move-exception v3

    .line 400
    .local v3, "e":Lorg/json/JSONException;
    const-string/jumbo v4, "updateCloudData error :"

    invoke-static {v0, v4, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 402
    .end local v3    # "e":Lorg/json/JSONException;
    :goto_3
    return-void
.end method

.method private writeToNode(Ljava/lang/String;I)V
    .locals 6
    .param p1, "node"    # Ljava/lang/String;
    .param p2, "value"    # I

    .line 481
    const-string v0, "SystemPressureControl"

    const/4 v1, 0x0

    .line 482
    .local v1, "writer":Ljava/io/FileWriter;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 483
    .local v2, "file":Ljava/io/File;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 484
    .local v3, "errMsg":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    .line 485
    return-void

    .line 487
    :cond_0
    :try_start_0
    new-instance v4, Ljava/io/FileWriter;

    invoke-direct {v4, v2}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    move-object v1, v4

    .line 488
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 492
    nop

    .line 494
    :try_start_1
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 497
    :goto_0
    goto :goto_1

    .line 495
    :catch_0
    move-exception v4

    .line 496
    .local v4, "e":Ljava/io/IOException;
    invoke-static {v0, v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .end local v4    # "e":Ljava/io/IOException;
    goto :goto_0

    .line 492
    :catchall_0
    move-exception v4

    goto :goto_2

    .line 489
    :catch_1
    move-exception v4

    .line 490
    .restart local v4    # "e":Ljava/io/IOException;
    :try_start_2
    invoke-static {v0, v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 492
    nop

    .end local v4    # "e":Ljava/io/IOException;
    if-eqz v1, :cond_1

    .line 494
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 500
    :cond_1
    :goto_1
    return-void

    .line 492
    :goto_2
    if-eqz v1, :cond_2

    .line 494
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 497
    goto :goto_3

    .line 495
    :catch_2
    move-exception v5

    .line 496
    .local v5, "e":Ljava/io/IOException;
    invoke-static {v0, v3, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 499
    .end local v5    # "e":Ljava/io/IOException;
    :cond_2
    :goto_3
    throw v4
.end method


# virtual methods
.method public KillProcessForPadSmallWindowMode(Ljava/lang/String;)V
    .locals 1
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 419
    sget-boolean v0, Lcom/android/server/am/SystemPressureController;->PAD_SMALL_WINDOW_CLEAN_ENABLE:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/am/SystemPressureController;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 420
    invoke-virtual {p0}, Lcom/android/server/am/SystemPressureController;->cleanAllSubProcess()V

    .line 421
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mProcessCleaner:Lcom/android/server/am/ProcessMemoryCleaner;

    invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessMemoryCleaner;->KillProcessForPadSmallWindowMode(Ljava/lang/String;)V

    .line 423
    :cond_0
    return-void
.end method

.method public activityResumeUnchecked(Lcom/android/server/am/ControllerActivityInfo;)V
    .locals 1
    .param p1, "activityInfo"    # Lcom/android/server/am/ControllerActivityInfo;

    .line 574
    iget-boolean v0, p0, Lcom/android/server/am/SystemPressureController;->mIsStartingApp:Z

    if-eqz v0, :cond_0

    .line 575
    invoke-direct {p0}, Lcom/android/server/am/SystemPressureController;->resetStartingAppState()V

    .line 577
    :cond_0
    return-void
.end method

.method public activityStartBeforeLocked(Lcom/android/server/am/ControllerActivityInfo;)V
    .locals 2
    .param p1, "activityInfo"    # Lcom/android/server/am/ControllerActivityInfo;

    .line 553
    iget-object v0, p1, Lcom/android/server/am/ControllerActivityInfo;->launchPkg:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/server/am/SystemPressureController;->mForegroundPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 554
    return-void

    .line 556
    :cond_0
    iget-boolean v0, p1, Lcom/android/server/am/ControllerActivityInfo;->isCloudStart:Z

    if-eqz v0, :cond_1

    .line 557
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/server/am/SystemPressureController;->setAppStartingMode(Z)V

    .line 559
    :cond_1
    return-void
.end method

.method public cleanAllSubProcess()V
    .locals 6

    .line 295
    sget-boolean v0, Landroid/os/spc/PressureStateSettings;->PROCESS_CLEANER_ENABLED:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mHandler:Lcom/android/server/am/SystemPressureController$H;

    .line 296
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/am/SystemPressureController$H;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 297
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/android/server/am/SystemPressureController;->mLastProcessCleanTimeMillis:J

    sub-long/2addr v2, v4

    sget-wide v4, Landroid/os/spc/PressureStateSettings;->PROC_CLEAN_MIN_INTERVAL_MS:J

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mHandler:Lcom/android/server/am/SystemPressureController$H;

    invoke-virtual {v0, v1}, Lcom/android/server/am/SystemPressureController$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 300
    .local v0, "msg":Landroid/os/Message;
    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 301
    iget-object v1, p0, Lcom/android/server/am/SystemPressureController;->mHandler:Lcom/android/server/am/SystemPressureController$H;

    invoke-virtual {v1, v0}, Lcom/android/server/am/SystemPressureController$H;->sendMessage(Landroid/os/Message;)Z

    .line 302
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/server/am/SystemPressureController;->mLastProcessCleanTimeMillis:J

    .line 304
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method public compactBackgroundProcess(ILjava/lang/String;)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "processName"    # Ljava/lang/String;

    .line 468
    iget-boolean v0, p0, Lcom/android/server/am/SystemPressureController;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 469
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mProcessCleaner:Lcom/android/server/am/ProcessMemoryCleaner;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/am/ProcessMemoryCleaner;->compactBackgroundProcess(ILjava/lang/String;)V

    .line 471
    :cond_0
    return-void
.end method

.method public compactBackgroundProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;Z)V
    .locals 1
    .param p1, "proc"    # Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .param p2, "isDelayed"    # Z

    .line 475
    iget-boolean v0, p0, Lcom/android/server/am/SystemPressureController;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 476
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mProcessCleaner:Lcom/android/server/am/ProcessMemoryCleaner;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/am/ProcessMemoryCleaner;->compactBackgroundProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;Z)V

    .line 478
    :cond_0
    return-void
.end method

.method public dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
    .locals 9
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "args"    # [Ljava/lang/String;
    .param p3, "opti"    # I

    .line 207
    const-string/jumbo v0, "system pressure controller:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 209
    :try_start_0
    array-length v0, p2

    if-ge p3, v0, :cond_d

    .line 210
    aget-object v0, p2, p3

    .line 211
    .local v0, "parm":Ljava/lang/String;
    const-string v1, "cleanproc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 212
    sget-wide v1, Landroid/os/spc/PressureStateSettings;->PROC_CLEAN_PSS_KB:J

    invoke-direct {p0, v1, v2}, Lcom/android/server/am/SystemPressureController;->cleanUpMemory(J)V

    goto/16 :goto_2

    .line 213
    :cond_0
    const-string v1, "configs"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 214
    invoke-virtual {p0, p1}, Lcom/android/server/am/SystemPressureController;->dumpConfigs(Ljava/io/PrintWriter;)V

    goto/16 :goto_2

    .line 215
    :cond_1
    const-string v1, "cleanrecord"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/android/server/am/SystemPressureController;->mSystemReady:Z

    if-eqz v1, :cond_2

    .line 216
    iget-object v1, p0, Lcom/android/server/am/SystemPressureController;->mProcessCleaner:Lcom/android/server/am/ProcessMemoryCleaner;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessMemoryCleaner;->getProcMemStat()Lcom/android/server/am/ProcMemCleanerStatistics;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/server/am/ProcMemCleanerStatistics;->dumpKillInfo(Ljava/io/PrintWriter;)V

    goto/16 :goto_2

    .line 217
    :cond_2
    const-string/jumbo v1, "thermalcleanlevel1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 218
    new-instance v1, Lmiui/process/ProcessConfig;

    const/16 v2, 0x13

    invoke-direct {v1, v2}, Lmiui/process/ProcessConfig;-><init>(I)V

    .line 220
    .local v1, "config":Lmiui/process/ProcessConfig;
    invoke-virtual {p0, v1}, Lcom/android/server/am/SystemPressureController;->powerKillProcess(Lmiui/process/ProcessConfig;)Z

    .line 221
    nop

    .end local v1    # "config":Lmiui/process/ProcessConfig;
    goto/16 :goto_2

    :cond_3
    const-string/jumbo v1, "thermalcleanlevel2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 222
    new-instance v1, Lmiui/process/ProcessConfig;

    const/16 v2, 0x14

    invoke-direct {v1, v2}, Lmiui/process/ProcessConfig;-><init>(I)V

    .line 224
    .restart local v1    # "config":Lmiui/process/ProcessConfig;
    invoke-virtual {p0, v1}, Lcom/android/server/am/SystemPressureController;->powerKillProcess(Lmiui/process/ProcessConfig;)Z

    .line 225
    nop

    .end local v1    # "config":Lmiui/process/ProcessConfig;
    goto/16 :goto_2

    :cond_4
    const-string v1, "lockoffclean"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_5

    .line 226
    iget-object v1, p0, Lcom/android/server/am/SystemPressureController;->mPowerCleaner:Lcom/android/server/am/ProcessPowerCleaner;

    if-eqz v1, :cond_d

    .line 227
    invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessPowerCleaner;->setLockOffCleanTestEnable(Z)V

    .line 228
    iget-object v1, p0, Lcom/android/server/am/SystemPressureController;->mPowerCleaner:Lcom/android/server/am/ProcessPowerCleaner;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessPowerCleaner;->handleAutoLockOff()V

    goto/16 :goto_2

    .line 230
    :cond_5
    const-string v1, "idlecleanapp"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v3, "_"

    if-eqz v1, :cond_9

    .line 231
    :try_start_1
    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 232
    .local v1, "cleanString":Ljava/lang/String;
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 233
    .local v2, "cleanArray":[Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v4, v2

    if-ge v3, v4, :cond_8

    .line 234
    aget-object v4, v2, v3

    .line 235
    invoke-static {v4}, Lmiui/process/ProcessManager;->getRunningProcessInfoByPackageName(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 236
    .local v4, "infoList":Ljava/util/List;, "Ljava/util/List<Lmiui/process/RunningProcessInfo;>;"
    if-eqz v4, :cond_7

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 237
    goto :goto_1

    .line 239
    :cond_6
    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lmiui/process/RunningProcessInfo;

    iget v5, v5, Lmiui/process/RunningProcessInfo;->mUid:I

    .line 240
    .local v5, "uid":I
    new-instance v6, Lmiui/process/ProcessConfig;

    aget-object v7, v2, v3

    const/16 v8, 0xd

    invoke-direct {v6, v8, v7, v5}, Lmiui/process/ProcessConfig;-><init>(ILjava/lang/String;I)V

    .line 242
    .local v6, "config":Lmiui/process/ProcessConfig;
    invoke-virtual {p0, v6}, Lcom/android/server/am/SystemPressureController;->powerKillProcess(Lmiui/process/ProcessConfig;)Z

    .line 233
    .end local v4    # "infoList":Ljava/util/List;, "Ljava/util/List<Lmiui/process/RunningProcessInfo;>;"
    .end local v5    # "uid":I
    .end local v6    # "config":Lmiui/process/ProcessConfig;
    :cond_7
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 244
    .end local v1    # "cleanString":Ljava/lang/String;
    .end local v2    # "cleanArray":[Ljava/lang/String;
    .end local v3    # "i":I
    :cond_8
    goto :goto_2

    :cond_9
    const-string/jumbo v1, "systemabnormal"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 245
    new-instance v1, Lmiui/process/ProcessConfig;

    const/16 v2, 0x10

    invoke-direct {v1, v2}, Lmiui/process/ProcessConfig;-><init>(I)V

    .line 247
    .local v1, "config":Lmiui/process/ProcessConfig;
    invoke-virtual {p0, v1}, Lcom/android/server/am/SystemPressureController;->powerKillProcess(Lmiui/process/ProcessConfig;)Z

    .line 248
    nop

    .end local v1    # "config":Lmiui/process/ProcessConfig;
    goto :goto_2

    :cond_a
    const-string v1, "forceclean"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 249
    new-instance v1, Lmiui/process/ProcessConfig;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Lmiui/process/ProcessConfig;-><init>(I)V

    .line 251
    .restart local v1    # "config":Lmiui/process/ProcessConfig;
    invoke-virtual {p0, v1}, Lcom/android/server/am/SystemPressureController;->sceneKillProcess(Lmiui/process/ProcessConfig;)Z

    .line 252
    nop

    .end local v1    # "config":Lmiui/process/ProcessConfig;
    goto :goto_2

    :cond_b
    const-string v1, "removePerm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 253
    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 254
    .local v1, "name":Ljava/lang/String;
    sget-object v2, Lcom/android/server/am/SystemPressureController;->mKillPkgPermList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 255
    sget-object v2, Lcom/android/server/am/SystemPressureController;->mKillPkgPermList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_2

    .line 256
    :cond_c
    sget-object v2, Lcom/android/server/am/SystemPressureController;->mKillProcPermList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 257
    sget-object v2, Lcom/android/server/am/SystemPressureController;->mKillProcPermList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 263
    .end local v0    # "parm":Ljava/lang/String;
    .end local v1    # "name":Ljava/lang/String;
    :cond_d
    :goto_2
    goto :goto_3

    .line 261
    :catch_0
    move-exception v0

    .line 262
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 264
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_3
    return-void
.end method

.method protected dumpConfigs(Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 272
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DEBUG_ALL="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Landroid/os/spc/PressureStateSettings;->DEBUG_ALL:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 273
    const-string v0, "-----------start of proc cleaner configs-----------"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 274
    const-string v0, "PROCESS_CLEANER_ENABLED = "

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 275
    sget-boolean v0, Landroid/os/spc/PressureStateSettings;->PROCESS_CLEANER_ENABLED:Z

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 276
    const-string v0, "PROC_CLEAN_MIN_INTERVAL_MS = "

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 277
    sget-wide v0, Landroid/os/spc/PressureStateSettings;->PROC_CLEAN_MIN_INTERVAL_MS:J

    invoke-virtual {p1, v0, v1}, Ljava/io/PrintWriter;->println(J)V

    .line 278
    const-string v0, "-----------end of proc cleaner configs-----------"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 279
    return-void
.end method

.method public finishBooting()V
    .locals 2

    .line 201
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mHandler:Lcom/android/server/am/SystemPressureController$H;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/android/server/am/SystemPressureController$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 202
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/am/SystemPressureController;->mHandler:Lcom/android/server/am/SystemPressureController$H;

    invoke-virtual {v1, v0}, Lcom/android/server/am/SystemPressureController$H;->sendMessage(Landroid/os/Message;)Z

    .line 203
    return-void
.end method

.method public foregroundActivityChangedLocked(Lcom/android/server/am/ControllerActivityInfo;)V
    .locals 3
    .param p1, "activityInfo"    # Lcom/android/server/am/ControllerActivityInfo;

    .line 562
    iget-object v0, p1, Lcom/android/server/am/ControllerActivityInfo;->launchPkg:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/server/am/SystemPressureController;->mForegroundPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 563
    return-void

    .line 565
    :cond_0
    iget-object v0, p1, Lcom/android/server/am/ControllerActivityInfo;->launchPkg:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/server/am/SystemPressureController;->mForegroundPackageName:Ljava/lang/String;

    .line 566
    sget-boolean v0, Landroid/os/spc/PressureStateSettings;->PROCESS_CLEANER_ENABLED:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/am/SystemPressureController;->mSystemReady:Z

    if-eqz v0, :cond_1

    .line 567
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mProcessCleaner:Lcom/android/server/am/ProcessMemoryCleaner;

    invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessMemoryCleaner;->foregroundActivityChanged(Lcom/android/server/am/ControllerActivityInfo;)V

    .line 570
    :cond_1
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mProcessGameCleaner:Lcom/android/server/am/ProcessGameCleaner;

    iget-object v1, p1, Lcom/android/server/am/ControllerActivityInfo;->launchPkg:Ljava/lang/String;

    iget v2, p1, Lcom/android/server/am/ControllerActivityInfo;->launchPid:I

    invoke-virtual {v0, v1, v2}, Lcom/android/server/am/ProcessGameCleaner;->foregroundInfoChanged(Ljava/lang/String;I)V

    .line 571
    return-void
.end method

.method public getBackgroundCpuPolicy()Ljava/lang/String;
    .locals 1

    .line 711
    invoke-static {}, Lcom/android/server/am/SystemPressureController;->nGetBackgroundCpuPolicy()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCpuUsage(ILjava/lang/String;)J
    .locals 2
    .param p1, "pid"    # I
    .param p2, "processName"    # Ljava/lang/String;

    .line 707
    invoke-static {p1, p2}, Lcom/android/server/am/SystemPressureController;->nGetCpuUsage(ILjava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getForegroundPackageName()Ljava/lang/String;
    .locals 1

    .line 268
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mForegroundPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getGameOomEnable()Z
    .locals 1

    .line 405
    iget-boolean v0, p0, Lcom/android/server/am/SystemPressureController;->mGameOomEnable:Z

    return v0
.end method

.method public init(Landroid/content/Context;Lcom/android/server/am/ActivityManagerService;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ams"    # Lcom/android/server/am/ActivityManagerService;

    .line 163
    iput-object p1, p0, Lcom/android/server/am/SystemPressureController;->mContext:Landroid/content/Context;

    .line 164
    iput-object p2, p0, Lcom/android/server/am/SystemPressureController;->mAms:Lcom/android/server/am/ActivityManagerService;

    .line 165
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mHandlerTh:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 166
    new-instance v0, Lcom/android/server/am/SystemPressureController$H;

    iget-object v1, p0, Lcom/android/server/am/SystemPressureController;->mHandlerTh:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/am/SystemPressureController$H;-><init>(Lcom/android/server/am/SystemPressureController;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/am/SystemPressureController;->mHandler:Lcom/android/server/am/SystemPressureController$H;

    .line 167
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mHandlerTh:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getThreadId()I

    move-result v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/Process;->setThreadGroupAndCpuset(II)V

    .line 168
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mProcessCleanHandlerTh:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 169
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mProcessCleanHandlerTh:Landroid/os/HandlerThread;

    .line 170
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getThreadId()I

    move-result v0

    .line 169
    invoke-static {v0, v1}, Landroid/os/Process;->setThreadGroupAndCpuset(II)V

    .line 172
    new-instance v0, Lcom/android/server/am/ProcessMemoryCleaner;

    iget-object v1, p0, Lcom/android/server/am/SystemPressureController;->mAms:Lcom/android/server/am/ActivityManagerService;

    invoke-direct {v0, v1}, Lcom/android/server/am/ProcessMemoryCleaner;-><init>(Lcom/android/server/am/ActivityManagerService;)V

    iput-object v0, p0, Lcom/android/server/am/SystemPressureController;->mProcessCleaner:Lcom/android/server/am/ProcessMemoryCleaner;

    .line 173
    new-instance v0, Lcom/android/server/am/ProcessGameCleaner;

    iget-object v1, p0, Lcom/android/server/am/SystemPressureController;->mAms:Lcom/android/server/am/ActivityManagerService;

    invoke-direct {v0, v1}, Lcom/android/server/am/ProcessGameCleaner;-><init>(Lcom/android/server/am/ActivityManagerService;)V

    iput-object v0, p0, Lcom/android/server/am/SystemPressureController;->mProcessGameCleaner:Lcom/android/server/am/ProcessGameCleaner;

    .line 174
    sget-boolean v0, Landroid/os/spc/PressureStateSettings;->DEBUG_ALL:Z

    if-eqz v0, :cond_0

    const-string v0, "SystemPressureControl"

    const-string v1, "init SystemPressureController"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    :cond_0
    return-void
.end method

.method public isAudioOrGPSApp(I)Z
    .locals 2
    .param p1, "uid"    # I

    .line 588
    iget-boolean v0, p0, Lcom/android/server/am/SystemPressureController;->mSystemReady:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 589
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-interface {v0, p1}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->isAppAudioActive(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    .line 590
    invoke-interface {v0, p1}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->isAppGpsActive(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 589
    :cond_1
    return v1

    .line 592
    :cond_2
    return v1
.end method

.method public isAudioOrGPSProc(II)Z
    .locals 2
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 596
    iget-boolean v0, p0, Lcom/android/server/am/SystemPressureController;->mSystemReady:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 597
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-interface {v0, p1, p2}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->isAppAudioActive(II)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    .line 598
    invoke-interface {v0, p1, p2}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->isAppGpsActive(II)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 597
    :cond_1
    return v1

    .line 600
    :cond_2
    return v1
.end method

.method public isCtsModeEnable()Z
    .locals 1

    .line 527
    sget-boolean v0, Lcom/miui/app/smartpower/SmartPowerPolicyConstants;->TESTSUITSPECIFIC:Z

    return v0
.end method

.method public isForceStopEnable(Lcom/android/server/am/ProcessRecord;I)Z
    .locals 1
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "policy"    # I

    .line 604
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mPms:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/am/ProcessManagerService;->isForceStopEnable(Lcom/android/server/am/ProcessRecord;I)Z

    move-result v0

    return v0
.end method

.method public isGameApp(Ljava/lang/String;)Z
    .locals 1
    .param p1, "pckName"    # Ljava/lang/String;

    .line 545
    sget-object v0, Lcom/android/server/am/SystemPressureController;->sGameAppList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isStartingApp()Z
    .locals 1

    .line 549
    iget-boolean v0, p0, Lcom/android/server/am/SystemPressureController;->mIsStartingApp:Z

    return v0
.end method

.method public kill(Lmiui/process/ProcessConfig;)Z
    .locals 5
    .param p1, "config"    # Lmiui/process/ProcessConfig;

    .line 614
    iget-boolean v0, p0, Lcom/android/server/am/SystemPressureController;->mSystemReady:Z

    const/4 v1, 0x0

    const-string v2, "SystemPressureControl"

    if-nez v0, :cond_0

    .line 615
    const-string v0, "The system is not ready"

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 616
    return v1

    .line 618
    :cond_0
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    .line 619
    .local v0, "callingPid":I
    invoke-direct {p0, v0}, Lcom/android/server/am/SystemPressureController;->checkKillProcPermission(I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 620
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Permission Denial: ProcessManager.kill() from pid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 621
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", uid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", procName="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/am/SystemPressureController;->mPms:Lcom/android/server/am/ProcessManagerService;

    .line 622
    invoke-virtual {v4, v0}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;

    move-result-object v4

    iget-object v4, v4, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", pkgName="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/am/SystemPressureController;->mPms:Lcom/android/server/am/ProcessManagerService;

    .line 623
    invoke-virtual {v4, v0}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;

    move-result-object v4

    iget-object v4, v4, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 624
    .local v3, "msg":Ljava/lang/String;
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 625
    return v1

    .line 627
    .end local v3    # "msg":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getPolicy()I

    move-result v1

    .line 628
    .local v1, "policy":I
    const/4 v3, 0x0

    .line 629
    .local v3, "success":Z
    packed-switch v1, :pswitch_data_0

    .line 656
    :pswitch_0
    const-string/jumbo v4, "unKnown policy"

    invoke-static {v2, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 653
    :pswitch_1
    iget-object v2, p0, Lcom/android/server/am/SystemPressureController;->mPms:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v2, p1}, Lcom/android/server/am/ProcessManagerService;->killAllBackgroundExceptLocked(Lmiui/process/ProcessConfig;)Z

    move-result v3

    .line 654
    goto :goto_0

    .line 650
    :pswitch_2
    invoke-virtual {p0, p1}, Lcom/android/server/am/SystemPressureController;->powerKillProcess(Lmiui/process/ProcessConfig;)Z

    move-result v3

    .line 651
    goto :goto_0

    .line 641
    :pswitch_3
    invoke-virtual {p0, p1}, Lcom/android/server/am/SystemPressureController;->sceneKillProcess(Lmiui/process/ProcessConfig;)Z

    move-result v3

    .line 642
    goto :goto_0

    .line 631
    :pswitch_4
    const-class v2, Lcom/miui/app/SpeedTestModeServiceInternal;

    invoke-static {v2}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/app/SpeedTestModeServiceInternal;

    invoke-interface {v2}, Lcom/miui/app/SpeedTestModeServiceInternal;->reportOneKeyCleanEvent()V

    .line 632
    invoke-virtual {p0, p1}, Lcom/android/server/am/SystemPressureController;->sceneKillProcess(Lmiui/process/ProcessConfig;)Z

    move-result v3

    .line 633
    nop

    .line 658
    :goto_0
    return v3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public killProcess(Lcom/android/server/am/ProcessRecord;ILjava/lang/String;)J
    .locals 4
    .param p1, "proc"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "minAdj"    # I
    .param p3, "reason"    # Ljava/lang/String;

    .line 443
    iget-boolean v0, p0, Lcom/android/server/am/SystemPressureController;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 444
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mProcessCleaner:Lcom/android/server/am/ProcessMemoryCleaner;

    iget-object v1, p0, Lcom/android/server/am/SystemPressureController;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 445
    invoke-interface {v1, v2, v3}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->getRunningProcess(ILjava/lang/String;)Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    move-result-object v1

    .line 444
    invoke-virtual {v0, v1, p2, p3}, Lcom/android/server/am/ProcessMemoryCleaner;->killProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;ILjava/lang/String;)J

    move-result-wide v0

    return-wide v0

    .line 447
    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public notifyCpuExceptionEvents(I)V
    .locals 1
    .param p1, "type"    # I

    .line 694
    iget-boolean v0, p0, Lcom/android/server/am/SystemPressureController;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 695
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-interface {v0, p1}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->onCpuExceptionEvents(I)V

    .line 697
    :cond_0
    return-void
.end method

.method public notifyCpuPressureEvents(I)V
    .locals 1
    .param p1, "level"    # I

    .line 688
    iget-boolean v0, p0, Lcom/android/server/am/SystemPressureController;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 689
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-interface {v0, p1}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->onCpuPressureEvents(I)V

    .line 691
    :cond_0
    return-void
.end method

.method public notifyThermalTempChange(I)V
    .locals 2
    .param p1, "temp"    # I

    .line 700
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "thermal temperature "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SystemPressureControl"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 701
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mThermalTempListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/am/ThermalTempListener;

    .line 702
    .local v1, "listener":Lcom/android/server/am/ThermalTempListener;
    invoke-interface {v1, p1}, Lcom/android/server/am/ThermalTempListener;->onThermalTempChange(I)V

    .line 703
    .end local v1    # "listener":Lcom/android/server/am/ThermalTempListener;
    goto :goto_0

    .line 704
    :cond_0
    return-void
.end method

.method public onApplyOomAdjLocked(Lcom/android/server/am/ProcessRecord;)V
    .locals 1
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 608
    iget-boolean v0, p0, Lcom/android/server/am/SystemPressureController;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 609
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mProcessCleaner:Lcom/android/server/am/ProcessMemoryCleaner;

    invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessMemoryCleaner;->onApplyOomAdjLocked(Lcom/android/server/am/ProcessRecord;)V

    .line 611
    :cond_0
    return-void
.end method

.method public onSystemReady()V
    .locals 4

    .line 179
    const-string v0, "ProcessManager"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/ProcessManagerService;

    iput-object v0, p0, Lcom/android/server/am/SystemPressureController;->mPms:Lcom/android/server/am/ProcessManagerService;

    .line 180
    invoke-static {}, Lcom/android/server/am/OomAdjusterImpl;->getInstance()Lcom/android/server/am/OomAdjusterImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/am/OomAdjusterImpl;->onSystemReady()V

    .line 181
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mProcessCleaner:Lcom/android/server/am/ProcessMemoryCleaner;

    iget-object v1, p0, Lcom/android/server/am/SystemPressureController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/am/SystemPressureController;->mPms:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v0, v1, v2}, Lcom/android/server/am/ProcessMemoryCleaner;->systemReady(Landroid/content/Context;Lcom/android/server/am/ProcessManagerService;)V

    .line 182
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mProcessGameCleaner:Lcom/android/server/am/ProcessGameCleaner;

    iget-object v1, p0, Lcom/android/server/am/SystemPressureController;->mPms:Lcom/android/server/am/ProcessManagerService;

    iget-object v2, p0, Lcom/android/server/am/SystemPressureController;->mProcessCleanHandlerTh:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/server/am/ProcessGameCleaner;->onSystemReady(Lcom/android/server/am/ProcessManagerService;Landroid/os/Looper;)V

    .line 184
    :try_start_0
    invoke-direct {p0}, Lcom/android/server/am/SystemPressureController;->nInit()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 187
    goto :goto_0

    .line 185
    :catch_0
    move-exception v0

    .line 186
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 189
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    iput-object v0, p0, Lcom/android/server/am/SystemPressureController;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    .line 190
    new-instance v0, Lcom/android/server/am/ProcessPowerCleaner;

    iget-object v1, p0, Lcom/android/server/am/SystemPressureController;->mAms:Lcom/android/server/am/ActivityManagerService;

    invoke-direct {v0, v1}, Lcom/android/server/am/ProcessPowerCleaner;-><init>(Lcom/android/server/am/ActivityManagerService;)V

    iput-object v0, p0, Lcom/android/server/am/SystemPressureController;->mPowerCleaner:Lcom/android/server/am/ProcessPowerCleaner;

    .line 191
    iget-object v1, p0, Lcom/android/server/am/SystemPressureController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/am/SystemPressureController;->mPms:Lcom/android/server/am/ProcessManagerService;

    iget-object v3, p0, Lcom/android/server/am/SystemPressureController;->mProcessCleanHandlerTh:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/am/ProcessPowerCleaner;->systemReady(Landroid/content/Context;Lcom/android/server/am/ProcessManagerService;Landroid/os/Looper;)V

    .line 192
    new-instance v0, Lcom/android/server/am/ProcessSceneCleaner;

    iget-object v1, p0, Lcom/android/server/am/SystemPressureController;->mAms:Lcom/android/server/am/ActivityManagerService;

    invoke-direct {v0, v1}, Lcom/android/server/am/ProcessSceneCleaner;-><init>(Lcom/android/server/am/ActivityManagerService;)V

    iput-object v0, p0, Lcom/android/server/am/SystemPressureController;->mSceneCleaner:Lcom/android/server/am/ProcessSceneCleaner;

    .line 193
    iget-object v1, p0, Lcom/android/server/am/SystemPressureController;->mPms:Lcom/android/server/am/ProcessManagerService;

    iget-object v2, p0, Lcom/android/server/am/SystemPressureController;->mProcessCleanHandlerTh:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/SystemPressureController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/am/ProcessSceneCleaner;->systemReady(Lcom/android/server/am/ProcessManagerService;Landroid/os/Looper;Landroid/content/Context;)V

    .line 194
    invoke-direct {p0}, Lcom/android/server/am/SystemPressureController;->nStartPressureMonitor()V

    .line 195
    invoke-direct {p0}, Lcom/android/server/am/SystemPressureController;->registerScreenStateReceiver()V

    .line 196
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/am/SystemPressureController;->mSystemReady:Z

    .line 197
    return-void
.end method

.method public performCompaction(Ljava/lang/String;I)V
    .locals 1
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "pid"    # I

    .line 504
    const-class v0, Lcom/android/server/am/MiuiMemoryServiceInternal;

    .line 505
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/MiuiMemoryServiceInternal;

    .line 506
    .local v0, "memService":Lcom/android/server/am/MiuiMemoryServiceInternal;
    if-nez v0, :cond_0

    .line 507
    return-void

    .line 509
    :cond_0
    invoke-interface {v0, p1, p2}, Lcom/android/server/am/MiuiMemoryServiceInternal;->performCompaction(Ljava/lang/String;I)V

    .line 510
    return-void
.end method

.method public powerKillProcess(Lmiui/process/ProcessConfig;)Z
    .locals 1
    .param p1, "config"    # Lmiui/process/ProcessConfig;

    .line 531
    iget-boolean v0, p0, Lcom/android/server/am/SystemPressureController;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 532
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mPowerCleaner:Lcom/android/server/am/ProcessPowerCleaner;

    invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessPowerCleaner;->powerKillProcess(Lmiui/process/ProcessConfig;)Z

    move-result v0

    return v0

    .line 534
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public reclaimPage()V
    .locals 1

    .line 452
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/server/am/SystemPressureController;->reclaimPage(I)V

    .line 453
    return-void
.end method

.method public reclaimPage(I)V
    .locals 1
    .param p1, "reclaimSizeMB"    # I

    .line 457
    sget-boolean v0, Lcom/android/server/am/SystemPressureController;->IS_ENABLE_RECLAIM:Z

    if-eqz v0, :cond_0

    .line 458
    const-string v0, "/sys/kernel/mi_reclaim/event"

    invoke-direct {p0, v0, p1}, Lcom/android/server/am/SystemPressureController;->writeToNode(Ljava/lang/String;I)V

    .line 459
    :cond_0
    return-void
.end method

.method public registerThermalTempListener(Lcom/android/server/am/ThermalTempListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/android/server/am/ThermalTempListener;

    .line 410
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mThermalTempListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 411
    return-void
.end method

.method public sceneKillProcess(Lmiui/process/ProcessConfig;)Z
    .locals 1
    .param p1, "config"    # Lmiui/process/ProcessConfig;

    .line 538
    iget-boolean v0, p0, Lcom/android/server/am/SystemPressureController;->mSystemReady:Z

    if-eqz v0, :cond_0

    .line 539
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mSceneCleaner:Lcom/android/server/am/ProcessSceneCleaner;

    invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessSceneCleaner;->sceneKillProcess(Lmiui/process/ProcessConfig;)Z

    move-result v0

    return v0

    .line 541
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public setAppStartingMode(Z)V
    .locals 4
    .param p1, "appStarting"    # Z

    .line 513
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mHandler:Lcom/android/server/am/SystemPressureController$H;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/server/am/SystemPressureController$H;->removeMessages(I)V

    .line 514
    iput-boolean p1, p0, Lcom/android/server/am/SystemPressureController;->mIsStartingApp:Z

    .line 515
    if-eqz p1, :cond_0

    .line 516
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mHandler:Lcom/android/server/am/SystemPressureController$H;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/am/SystemPressureController$H;->sendEmptyMessageDelayed(IJ)Z

    .line 518
    :cond_0
    const-class v0, Lcom/android/server/am/MiuiMemoryServiceInternal;

    .line 519
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/MiuiMemoryServiceInternal;

    .line 520
    .local v0, "memService":Lcom/android/server/am/MiuiMemoryServiceInternal;
    if-eqz v0, :cond_1

    .line 521
    invoke-interface {v0, p1}, Lcom/android/server/am/MiuiMemoryServiceInternal;->setAppStartingMode(Z)V

    .line 523
    :cond_1
    return-void
.end method

.method public switchSPTMReclaim(Z)V
    .locals 2
    .param p1, "isEnable"    # Z

    .line 463
    sget-boolean v0, Lcom/android/server/am/SystemPressureController;->IS_ENABLE_RECLAIM:Z

    if-eqz v0, :cond_1

    .line 464
    if-eqz p1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "/sys/kernel/mi_reclaim/event"

    invoke-direct {p0, v1, v0}, Lcom/android/server/am/SystemPressureController;->writeToNode(Ljava/lang/String;I)V

    .line 465
    :cond_1
    return-void
.end method

.method public triggerProcessClean()V
    .locals 6

    .line 283
    sget-boolean v0, Landroid/os/spc/PressureStateSettings;->PROCESS_CLEANER_ENABLED:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mHandler:Lcom/android/server/am/SystemPressureController$H;

    .line 284
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/am/SystemPressureController$H;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 285
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/android/server/am/SystemPressureController;->mLastProcessCleanTimeMillis:J

    sub-long/2addr v2, v4

    sget-wide v4, Landroid/os/spc/PressureStateSettings;->PROC_CLEAN_MIN_INTERVAL_MS:J

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    .line 287
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mHandler:Lcom/android/server/am/SystemPressureController$H;

    invoke-virtual {v0, v1}, Lcom/android/server/am/SystemPressureController$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 288
    .local v0, "msg":Landroid/os/Message;
    sget-wide v1, Landroid/os/spc/PressureStateSettings;->PROC_CLEAN_PSS_KB:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 289
    iget-object v1, p0, Lcom/android/server/am/SystemPressureController;->mHandler:Lcom/android/server/am/SystemPressureController$H;

    invoke-virtual {v1, v0}, Lcom/android/server/am/SystemPressureController$H;->sendMessage(Landroid/os/Message;)Z

    .line 290
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/server/am/SystemPressureController;->mLastProcessCleanTimeMillis:J

    .line 292
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method public unRegisterThermalTemListener(Lcom/android/server/am/ThermalTempListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/android/server/am/ThermalTempListener;

    .line 415
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController;->mThermalTempListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 416
    return-void
.end method

.method public updateScreenState(Z)V
    .locals 0
    .param p1, "screenOn"    # Z

    .line 715
    if-eqz p1, :cond_0

    .line 716
    invoke-direct {p0}, Lcom/android/server/am/SystemPressureController;->nStartPressureMonitor()V

    goto :goto_0

    .line 718
    :cond_0
    invoke-direct {p0}, Lcom/android/server/am/SystemPressureController;->nEndPressureMonitor()V

    .line 720
    :goto_0
    return-void
.end method
