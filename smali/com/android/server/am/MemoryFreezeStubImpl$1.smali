.class Lcom/android/server/am/MemoryFreezeStubImpl$1;
.super Landroid/database/ContentObserver;
.source "MemoryFreezeStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/MemoryFreezeStubImpl;->registerMiuiFreezeObserver(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/MemoryFreezeStubImpl;


# direct methods
.method constructor <init>(Lcom/android/server/am/MemoryFreezeStubImpl;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/MemoryFreezeStubImpl;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 290
    iput-object p1, p0, Lcom/android/server/am/MemoryFreezeStubImpl$1;->this$0:Lcom/android/server/am/MemoryFreezeStubImpl;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 1
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 293
    if-eqz p2, :cond_0

    const-string v0, "miui_freeze"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$1;->this$0:Lcom/android/server/am/MemoryFreezeStubImpl;

    invoke-virtual {v0}, Lcom/android/server/am/MemoryFreezeStubImpl;->updateMiuiFreezeInfo()V

    .line 296
    :cond_0
    return-void
.end method
