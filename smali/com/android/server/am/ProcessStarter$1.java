class com.android.server.am.ProcessStarter$1 implements java.lang.Runnable {
	 /* .source "ProcessStarter.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/am/ProcessStarter;->recordDiedProcessIfNeeded(Ljava/lang/String;Ljava/lang/String;I)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.am.ProcessStarter this$0; //synthetic
final java.lang.String val$pkgName; //synthetic
final java.lang.String val$processName; //synthetic
final Integer val$uid; //synthetic
/* # direct methods */
 com.android.server.am.ProcessStarter$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/ProcessStarter; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 290 */
this.this$0 = p1;
this.val$pkgName = p2;
/* iput p3, p0, Lcom/android/server/am/ProcessStarter$1;->val$uid:I */
this.val$processName = p4;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 7 */
/* .line 293 */
v0 = this.this$0;
com.android.server.am.ProcessStarter .-$$Nest$fgetmPms ( v0 );
v1 = this.val$pkgName;
/* iget v2, p0, Lcom/android/server/am/ProcessStarter$1;->val$uid:I */
v0 = (( com.android.server.am.ProcessManagerService ) v0 ).isAllowAutoStart ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/am/ProcessManagerService;->isAllowAutoStart(Ljava/lang/String;I)Z
/* if-nez v0, :cond_0 */
/* .line 294 */
final String v0 = "ProcessStarter"; // const-string v0, "ProcessStarter"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "process "; // const-string v2, "process "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.val$processName;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " No restart permission!"; // const-string v2, " No restart permission!"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 295 */
return;
/* .line 297 */
} // :cond_0
v0 = this.this$0;
com.android.server.am.ProcessStarter .-$$Nest$fgetmProcessDiedRecordMap ( v0 );
v1 = this.val$processName;
/* check-cast v0, Ljava/lang/Integer; */
/* .line 298 */
/* .local v0, "value":Ljava/lang/Integer; */
if ( v0 != null) { // if-eqz v0, :cond_1
v1 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
int v2 = 5; // const/4 v2, 0x5
/* if-lt v1, v2, :cond_1 */
/* .line 299 */
final String v1 = "ProcessStarter"; // const-string v1, "ProcessStarter"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "process "; // const-string v3, "process "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.val$processName;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " frequent died!"; // const-string v3, " frequent died!"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 300 */
return;
/* .line 302 */
} // :cond_1
v1 = this.this$0;
com.android.server.am.ProcessStarter .-$$Nest$fgetmAms ( v1 );
/* monitor-enter v1 */
/* .line 303 */
try { // :try_start_0
v2 = this.this$0;
v3 = this.val$pkgName;
v4 = this.val$processName;
/* iget v5, p0, Lcom/android/server/am/ProcessStarter$1;->val$uid:I */
/* .line 304 */
v5 = android.os.UserHandle .getUserId ( v5 );
int v6 = 2; // const/4 v6, 0x2
com.android.server.am.ProcessStarter .makeHostingTypeFromFlag ( v6 );
/* .line 303 */
com.android.server.am.ProcessStarter .-$$Nest$mstartProcessLocked ( v2,v3,v4,v5,v6 );
/* .line 306 */
/* monitor-exit v1 */
/* .line 307 */
return;
/* .line 306 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
