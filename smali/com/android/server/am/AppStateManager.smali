.class public Lcom/android/server/am/AppStateManager;
.super Ljava/lang/Object;
.source "AppStateManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/AppStateManager$ActiveApps;,
        Lcom/android/server/am/AppStateManager$CurrentTaskStack;,
        Lcom/android/server/am/AppStateManager$MainHandler;,
        Lcom/android/server/am/AppStateManager$PowerFrozenCallback;,
        Lcom/android/server/am/AppStateManager$AppState;,
        Lcom/android/server/am/AppStateManager$TaskRecord;,
        Lcom/android/server/am/AppStateManager$IProcessStateCallback;
    }
.end annotation


# static fields
.field private static final APP_STATE_BACKGROUND:I = 0x2

.field private static final APP_STATE_DIED:I = 0x0

.field private static final APP_STATE_FOREGROUND:I = 0x1

.field private static final APP_STATE_HIBERNATION:I = 0x6

.field private static final APP_STATE_IDLE:I = 0x5

.field private static final APP_STATE_INACTIVE:I = 0x3

.field private static final APP_STATE_MAINTENANCE:I = 0x4

.field private static final APP_STATE_NONE:I = -0x1

.field public static final DEBUG:Z

.field public static final DEBUG_PROC:Z

.field private static final HISTORY_SIZE:I

.field private static final MSG_CLEAR_APPSTATES_BY_USERID:I = 0x3

.field private static final MSG_HIBERNATE_ALL:I = 0x1

.field private static final MSG_HIBERNATE_ALL_INACTIVE:I = 0x2

.field private static final PROCESS_STATE_BACKGROUND:I = 0x3

.field private static final PROCESS_STATE_DIED:I = 0x0

.field private static final PROCESS_STATE_HIBERNATION:I = 0x7

.field private static final PROCESS_STATE_IDLE:I = 0x6

.field private static final PROCESS_STATE_INACTIVE:I = 0x4

.field private static final PROCESS_STATE_INVISIBLE:I = 0x2

.field private static final PROCESS_STATE_MAINTENANCE:I = 0x5

.field private static final PROCESS_STATE_NONE:I = -0x1

.field private static final PROCESS_STATE_VISIBLE:I = 0x1

.field public static final REASON_ADD_TO_WHITELIST:Ljava/lang/String; = "add to whitelist"

.field public static final REASON_ADJ_ABOVE_FG:Ljava/lang/String; = "adj above foreground"

.field public static final REASON_ADJ_BELOW_VISIBLE:Ljava/lang/String; = "adj below visible"

.field public static final REASON_ALARM_END:Ljava/lang/String; = "alarm end"

.field public static final REASON_ALARM_START:Ljava/lang/String; = "alarm start"

.field public static final REASON_APP_START:Ljava/lang/String; = "app start"

.field public static final REASON_BACKUP_END:Ljava/lang/String; = "backup end"

.field public static final REASON_BACKUP_SERVICE_CONNECTED:Ljava/lang/String; = "backup service connected"

.field public static final REASON_BACKUP_SERVICE_DISCONNECTED:Ljava/lang/String; = "backup service disconnected"

.field public static final REASON_BACKUP_START:Ljava/lang/String; = "backup start"

.field public static final REASON_BECOME_BACKGROUND:Ljava/lang/String; = "become background"

.field public static final REASON_BECOME_FOREGROUND:Ljava/lang/String; = "become foreground"

.field public static final REASON_BECOME_INVISIBLE:Ljava/lang/String; = "become invisible"

.field public static final REASON_BECOME_VISIBLE:Ljava/lang/String; = "become visible"

.field public static final REASON_BROADCAST_END:Ljava/lang/String; = "broadcast end "

.field public static final REASON_BROADCAST_START:Ljava/lang/String; = "broadcast start "

.field public static final REASON_CONTENT_PROVIDER_END:Ljava/lang/String; = "content provider end "

.field public static final REASON_CONTENT_PROVIDER_START:Ljava/lang/String; = "content provider start "

.field public static final REASON_HIBERNATE_ALL:Ljava/lang/String; = "hibernate all "

.field public static final REASON_PENDING_INTENT:Ljava/lang/String; = "pending intent "

.field public static final REASON_PERIODIC_ACTIVE:Ljava/lang/String; = "periodic active"

.field public static final REASON_PRESS_MEDIA_KEY:Ljava/lang/String; = "press media key"

.field public static final REASON_PROCESS_DIED:Ljava/lang/String; = "process died "

.field public static final REASON_PROCESS_START:Ljava/lang/String; = "process start "

.field public static final REASON_RECEIVE_BINDER_STATE:Ljava/lang/String; = "receive binder state"

.field public static final REASON_RECEIVE_BINDER_TRANS:Ljava/lang/String; = "receive binder trans"

.field public static final REASON_RECEIVE_KILL_SIGNAL:Ljava/lang/String; = "receive kill signal"

.field public static final REASON_RECEIVE_NET_REQUEST:Ljava/lang/String; = "receive net request"

.field public static final REASON_RECEIVE_OTHER_THAW_REQUEST:Ljava/lang/String; = "receive other thaw request"

.field public static final REASON_REMOVE_FROM_WHITELIST:Ljava/lang/String; = "remove from whitelist"

.field public static final REASON_RESOURCE_ACTIVE:Ljava/lang/String; = "resource active "

.field public static final REASON_RESOURCE_INACTIVE:Ljava/lang/String; = "resource inactive "

.field public static final REASON_SERVICE_CEANGE:Ljava/lang/String; = "service "

.field public static final REASON_SHOW_INPUTMETHOD:Ljava/lang/String; = "show input method"

.field public static final REASON_START_ACTIVITY:Ljava/lang/String; = "start activity "

.field public static final TAG:Ljava/lang/String; = "SmartPower"

.field private static final mStatesNeedToRecord:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static sEnable:Z


# instance fields
.field private mAMS:Lcom/android/server/am/ActivityManagerService;

.field private final mActiveApps:Lcom/android/server/am/AppStateManager$ActiveApps;

.field private final mAppLock:Ljava/lang/Object;

.field private mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

.field private mContext:Landroid/content/Context;

.field private final mCurrentTaskStack:Lcom/android/server/am/AppStateManager$CurrentTaskStack;

.field private mHoldScreenUid:I

.field private final mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mLooper:Landroid/os/Looper;

.field private mMainHandler:Lcom/android/server/am/AppStateManager$MainHandler;

.field private final mPendingInteractiveUids:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mPkms:Landroid/content/pm/PackageManager;

.field private mPmInternal:Lcom/miui/server/process/ProcessManagerInternal;

.field private mPowerFrozenManager:Lcom/miui/server/smartpower/PowerFrozenManager;

.field private mProcessPolicy:Lcom/android/server/am/IProcessPolicy;

.field private final mProcessStateCallbacks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/server/am/AppStateManager$IProcessStateCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

.field private mSmartScenarioManager:Lcom/miui/server/smartpower/SmartScenarioManager;

.field private final mWhiteListVideoActivities:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetmAMS(Lcom/android/server/am/AppStateManager;)Lcom/android/server/am/ActivityManagerService;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/AppStateManager;->mAMS:Lcom/android/server/am/ActivityManagerService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmActiveApps(Lcom/android/server/am/AppStateManager;)Lcom/android/server/am/AppStateManager$ActiveApps;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/AppStateManager;->mActiveApps:Lcom/android/server/am/AppStateManager$ActiveApps;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmAppLock(Lcom/android/server/am/AppStateManager;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/AppStateManager;->mAppLock:Ljava/lang/Object;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmAppPowerResourceManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/AppPowerResourceManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/AppStateManager;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/am/AppStateManager;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/AppStateManager;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHoldScreenUid(Lcom/android/server/am/AppStateManager;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/AppStateManager;->mHoldScreenUid:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLooper(Lcom/android/server/am/AppStateManager;)Landroid/os/Looper;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/AppStateManager;->mLooper:Landroid/os/Looper;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMainHandler(Lcom/android/server/am/AppStateManager;)Lcom/android/server/am/AppStateManager$MainHandler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/AppStateManager;->mMainHandler:Lcom/android/server/am/AppStateManager$MainHandler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPendingInteractiveUids(Lcom/android/server/am/AppStateManager;)Landroid/util/SparseArray;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/AppStateManager;->mPendingInteractiveUids:Landroid/util/SparseArray;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPkms(Lcom/android/server/am/AppStateManager;)Landroid/content/pm/PackageManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/AppStateManager;->mPkms:Landroid/content/pm/PackageManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmProcessPolicy(Lcom/android/server/am/AppStateManager;)Lcom/android/server/am/IProcessPolicy;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/AppStateManager;->mProcessPolicy:Lcom/android/server/am/IProcessPolicy;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmProcessStateCallbacks(Lcom/android/server/am/AppStateManager;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/AppStateManager;->mProcessStateCallbacks:Ljava/util/ArrayList;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSmartPowerPolicyManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/SmartPowerPolicyManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/AppStateManager;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSmartScenarioManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/SmartScenarioManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/AppStateManager;->mSmartScenarioManager:Lcom/miui/server/smartpower/SmartScenarioManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmWhiteListVideoActivities(Lcom/android/server/am/AppStateManager;)Landroid/util/ArraySet;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/AppStateManager;->mWhiteListVideoActivities:Landroid/util/ArraySet;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmPkms(Lcom/android/server/am/AppStateManager;Landroid/content/pm/PackageManager;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/am/AppStateManager;->mPkms:Landroid/content/pm/PackageManager;

    return-void
.end method

.method static bridge synthetic -$$Nest$monReportPowerFrozenSignal(Lcom/android/server/am/AppStateManager;IILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/am/AppStateManager;->onReportPowerFrozenSignal(IILjava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monReportPowerFrozenSignal(Lcom/android/server/am/AppStateManager;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/am/AppStateManager;->onReportPowerFrozenSignal(ILjava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetHISTORY_SIZE()I
    .locals 1

    sget v0, Lcom/android/server/am/AppStateManager;->HISTORY_SIZE:I

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetmStatesNeedToRecord()Landroid/util/ArraySet;
    .locals 1

    sget-object v0, Lcom/android/server/am/AppStateManager;->mStatesNeedToRecord:Landroid/util/ArraySet;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$smparseScenatioActions(I)Ljava/util/List;
    .locals 0

    invoke-static {p0}, Lcom/android/server/am/AppStateManager;->parseScenatioActions(I)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$smringAdvance(III)I
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/android/server/am/AppStateManager;->ringAdvance(III)I

    move-result p0

    return p0
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 56
    sget-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->DEBUG_ALL:Z

    sput-boolean v0, Lcom/android/server/am/AppStateManager;->DEBUG:Z

    .line 58
    sget-boolean v1, Lcom/miui/app/smartpower/SmartPowerSettings;->APP_STATE_ENABLE:Z

    sput-boolean v1, Lcom/android/server/am/AppStateManager;->sEnable:Z

    .line 60
    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->DEBUG_PROC:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    sput-boolean v0, Lcom/android/server/am/AppStateManager;->DEBUG_PROC:Z

    .line 175
    sget v0, Lcom/miui/app/smartpower/SmartPowerSettings;->MAX_HISTORY_REPORT_SIZE:I

    sput v0, Lcom/android/server/am/AppStateManager;->HISTORY_SIZE:I

    .line 183
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    sput-object v0, Lcom/android/server/am/AppStateManager;->mStatesNeedToRecord:Landroid/util/ArraySet;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/android/server/am/ActivityManagerService;Lcom/miui/server/smartpower/PowerFrozenManager;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "looper"    # Landroid/os/Looper;
    .param p3, "ams"    # Lcom/android/server/am/ActivityManagerService;
    .param p4, "powerFrozenManager"    # Lcom/miui/server/smartpower/PowerFrozenManager;

    .line 202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177
    new-instance v0, Lcom/android/server/am/AppStateManager$ActiveApps;

    invoke-direct {v0, p0}, Lcom/android/server/am/AppStateManager$ActiveApps;-><init>(Lcom/android/server/am/AppStateManager;)V

    iput-object v0, p0, Lcom/android/server/am/AppStateManager;->mActiveApps:Lcom/android/server/am/AppStateManager$ActiveApps;

    .line 178
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/AppStateManager;->mAppLock:Ljava/lang/Object;

    .line 195
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/AppStateManager;->mProcessStateCallbacks:Ljava/util/ArrayList;

    .line 196
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/AppStateManager;->mPendingInteractiveUids:Landroid/util/SparseArray;

    .line 198
    new-instance v0, Lcom/android/server/am/AppStateManager$CurrentTaskStack;

    invoke-direct {v0, p0}, Lcom/android/server/am/AppStateManager$CurrentTaskStack;-><init>(Lcom/android/server/am/AppStateManager;)V

    iput-object v0, p0, Lcom/android/server/am/AppStateManager;->mCurrentTaskStack:Lcom/android/server/am/AppStateManager$CurrentTaskStack;

    .line 199
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/am/AppStateManager;->mHoldScreenUid:I

    .line 200
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/AppStateManager;->mWhiteListVideoActivities:Landroid/util/ArraySet;

    .line 238
    new-instance v1, Lcom/android/server/am/AppStateManager$1;

    invoke-direct {v1, p0}, Lcom/android/server/am/AppStateManager$1;-><init>(Lcom/android/server/am/AppStateManager;)V

    iput-object v1, p0, Lcom/android/server/am/AppStateManager;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    .line 203
    iput-object p2, p0, Lcom/android/server/am/AppStateManager;->mLooper:Landroid/os/Looper;

    .line 204
    new-instance v1, Lcom/android/server/am/AppStateManager$MainHandler;

    iget-object v2, p0, Lcom/android/server/am/AppStateManager;->mLooper:Landroid/os/Looper;

    invoke-direct {v1, p0, v2}, Lcom/android/server/am/AppStateManager$MainHandler;-><init>(Lcom/android/server/am/AppStateManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/server/am/AppStateManager;->mMainHandler:Lcom/android/server/am/AppStateManager$MainHandler;

    .line 205
    iput-object p1, p0, Lcom/android/server/am/AppStateManager;->mContext:Landroid/content/Context;

    .line 206
    iput-object p3, p0, Lcom/android/server/am/AppStateManager;->mAMS:Lcom/android/server/am/ActivityManagerService;

    .line 207
    iput-object p4, p0, Lcom/android/server/am/AppStateManager;->mPowerFrozenManager:Lcom/miui/server/smartpower/PowerFrozenManager;

    .line 208
    new-instance v1, Lcom/android/server/am/AppStateManager$PowerFrozenCallback;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/server/am/AppStateManager$PowerFrozenCallback;-><init>(Lcom/android/server/am/AppStateManager;Lcom/android/server/am/AppStateManager$PowerFrozenCallback-IA;)V

    invoke-virtual {p4, v1}, Lcom/miui/server/smartpower/PowerFrozenManager;->registerFrozenCallback(Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;)V

    .line 209
    sget-object v1, Lcom/android/server/am/AppStateManager;->mStatesNeedToRecord:Landroid/util/ArraySet;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 210
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 211
    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 212
    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 213
    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 214
    sget-boolean v2, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_FROZEN_ENABLE:Z

    if-nez v2, :cond_0

    sget-boolean v2, Lcom/android/server/am/AppStateManager;->DEBUG:Z

    if-eqz v2, :cond_1

    .line 215
    :cond_0
    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 218
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x110300d1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 220
    .local v1, "activities":[Ljava/lang/String;
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/util/ArraySet;->addAll(Ljava/util/Collection;)Z

    .line 221
    return-void
.end method

.method public static appStateToString(I)Ljava/lang/String;
    .locals 1
    .param p0, "state"    # I

    .line 116
    packed-switch p0, :pswitch_data_0

    .line 132
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 130
    :pswitch_0
    const-string v0, "hibernation"

    return-object v0

    .line 128
    :pswitch_1
    const-string v0, "idle"

    return-object v0

    .line 126
    :pswitch_2
    const-string v0, "maintenance"

    return-object v0

    .line 124
    :pswitch_3
    const-string v0, "inactive"

    return-object v0

    .line 122
    :pswitch_4
    const-string v0, "background"

    return-object v0

    .line 120
    :pswitch_5
    const-string v0, "foreground"

    return-object v0

    .line 118
    :pswitch_6
    const-string v0, "died"

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private isFrozenForUid(I)Z
    .locals 1
    .param p1, "uid"    # I

    .line 3375
    iget-object v0, p0, Lcom/android/server/am/AppStateManager;->mPowerFrozenManager:Lcom/miui/server/smartpower/PowerFrozenManager;

    invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/PowerFrozenManager;->isFrozenForUid(I)Z

    move-result v0

    return v0
.end method

.method public static isSystemApp(Lcom/android/server/am/ProcessRecord;)Z
    .locals 3
    .param p0, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 775
    const/4 v0, 0x0

    if-eqz p0, :cond_2

    iget-object v1, p0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    if-eqz v1, :cond_2

    .line 776
    iget-object v1, p0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit16 v1, v1, 0x81

    if-nez v1, :cond_0

    const/16 v1, 0x3e8

    iget v2, p0, Lcom/android/server/am/ProcessRecord;->uid:I

    if-ne v1, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0

    .line 780
    :cond_2
    return v0
.end method

.method private onReportPowerFrozenSignal(IILjava/lang/String;)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "reason"    # Ljava/lang/String;

    .line 724
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 725
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 726
    invoke-static {v0, p2, p3}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$monReportPowerFrozenSignal(Lcom/android/server/am/AppStateManager$AppState;ILjava/lang/String;)V

    .line 728
    :cond_0
    return-void
.end method

.method private onReportPowerFrozenSignal(ILjava/lang/String;)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "reason"    # Ljava/lang/String;

    .line 717
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 718
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 719
    invoke-static {v0, p2}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$monReportPowerFrozenSignal(Lcom/android/server/am/AppStateManager$AppState;Ljava/lang/String;)V

    .line 721
    :cond_0
    return-void
.end method

.method private static parseScenatioActions(I)Ljava/util/List;
    .locals 3
    .param p0, "scenarioAction"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 878
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 879
    .local v0, "actions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v1, 0x1

    .line 880
    .local v1, "action":I
    :goto_0
    if-gt v1, p0, :cond_1

    .line 881
    and-int v2, v1, p0

    if-eqz v2, :cond_0

    .line 882
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 884
    :cond_0
    shl-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 886
    :cond_1
    return-object v0
.end method

.method public static processStateToString(I)Ljava/lang/String;
    .locals 1
    .param p0, "state"    # I

    .line 93
    packed-switch p0, :pswitch_data_0

    .line 111
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 109
    :pswitch_0
    const-string v0, "hibernation"

    return-object v0

    .line 107
    :pswitch_1
    const-string v0, "idle"

    return-object v0

    .line 105
    :pswitch_2
    const-string v0, "maintenance"

    return-object v0

    .line 103
    :pswitch_3
    const-string v0, "inactive"

    return-object v0

    .line 101
    :pswitch_4
    const-string v0, "background"

    return-object v0

    .line 99
    :pswitch_5
    const-string v0, "invisible"

    return-object v0

    .line 97
    :pswitch_6
    const-string/jumbo v0, "visible"

    return-object v0

    .line 95
    :pswitch_7
    const-string v0, "died"

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private removeAppIfNeeded(Lcom/android/server/am/AppStateManager$AppState;)V
    .locals 3
    .param p1, "appState"    # Lcom/android/server/am/AppStateManager$AppState;

    .line 476
    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getUid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->isIsolated(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 477
    iget-object v0, p0, Lcom/android/server/am/AppStateManager;->mAppLock:Ljava/lang/Object;

    monitor-enter v0

    .line 478
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager;->mActiveApps:Lcom/android/server/am/AppStateManager$ActiveApps;

    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getUid()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/server/am/AppStateManager$ActiveApps;->remove(I)V

    .line 479
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 481
    :cond_0
    :goto_0
    return-void
.end method

.method private static ringAdvance(III)I
    .locals 2
    .param p0, "origin"    # I
    .param p1, "increment"    # I
    .param p2, "size"    # I

    .line 873
    add-int v0, p0, p1

    rem-int/2addr v0, p2

    .line 874
    .local v0, "index":I
    if-gez v0, :cond_0

    add-int v1, v0, p2

    goto :goto_0

    :cond_0
    move v1, v0

    :goto_0
    return v1
.end method


# virtual methods
.method public activityStartBeforeLocked(Ljava/lang/String;IILjava/lang/String;Z)V
    .locals 10
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "fromPid"    # I
    .param p3, "toUid"    # I
    .param p4, "packageName"    # Ljava/lang/String;
    .param p5, "isColdStart"    # Z

    .line 655
    if-lez p2, :cond_0

    invoke-virtual {p0}, Lcom/android/server/am/AppStateManager;->getHomeProcessPidLocked()I

    move-result v0

    if-ne v0, p2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 656
    .local v0, "startByHome":Z
    :goto_0
    invoke-virtual {p0, p3}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v1

    .line 657
    .local v1, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-nez v1, :cond_1

    if-eqz v0, :cond_1

    .line 658
    iget-object v2, p0, Lcom/android/server/am/AppStateManager;->mAppLock:Ljava/lang/Object;

    monitor-enter v2

    .line 659
    :try_start_0
    new-instance v9, Lcom/android/server/am/AppStateManager$AppState;

    iget-object v7, p0, Lcom/android/server/am/AppStateManager;->mContext:Landroid/content/Context;

    const/4 v8, 0x0

    move-object v3, v9

    move-object v4, p0

    move v5, p3

    move-object v6, p4

    invoke-direct/range {v3 .. v8}, Lcom/android/server/am/AppStateManager$AppState;-><init>(Lcom/android/server/am/AppStateManager;ILjava/lang/String;Landroid/content/Context;Lcom/android/server/am/AppStateManager$AppState-IA;)V

    move-object v1, v9

    .line 660
    iget-object v3, p0, Lcom/android/server/am/AppStateManager;->mActiveApps:Lcom/android/server/am/AppStateManager$ActiveApps;

    invoke-virtual {v3, p3, v1}, Lcom/android/server/am/AppStateManager$ActiveApps;->put(ILcom/android/server/am/AppStateManager$AppState;)V

    .line 661
    monitor-exit v2

    goto :goto_1

    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 663
    :cond_1
    :goto_1
    if-eqz v1, :cond_2

    .line 664
    invoke-static {v1, p1}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$monActivityForegroundLocked(Lcom/android/server/am/AppStateManager$AppState;Ljava/lang/String;)V

    .line 666
    :cond_2
    if-eqz v0, :cond_3

    .line 667
    const-string v2, "app start"

    invoke-virtual {p0, v1, v2}, Lcom/android/server/am/AppStateManager;->hibernateAllInactive(Lcom/android/server/am/AppStateManager$AppState;Ljava/lang/String;)V

    .line 669
    :cond_3
    return-void
.end method

.method public componentEndLocked(ILjava/lang/String;)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "content"    # Ljava/lang/String;

    .line 517
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 518
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 519
    invoke-static {v0, p2}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$mcomponentEnd(Lcom/android/server/am/AppStateManager$AppState;Ljava/lang/String;)V

    .line 521
    :cond_0
    return-void
.end method

.method public componentEndLocked(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
    .locals 1
    .param p1, "processRecord"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "content"    # Ljava/lang/String;

    .line 497
    iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 498
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 499
    invoke-static {v0, p1, p2}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$mcomponentEnd(Lcom/android/server/am/AppStateManager$AppState;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V

    .line 501
    :cond_0
    return-void
.end method

.method public componentStartLocked(ILjava/lang/String;)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "content"    # Ljava/lang/String;

    .line 507
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 508
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 509
    invoke-static {v0, p2}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$mcomponentStart(Lcom/android/server/am/AppStateManager$AppState;Ljava/lang/String;)V

    .line 511
    :cond_0
    return-void
.end method

.method public componentStartLocked(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
    .locals 1
    .param p1, "processRecord"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "content"    # Ljava/lang/String;

    .line 487
    iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 488
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 489
    invoke-static {v0, p1, p2}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$mcomponentStart(Lcom/android/server/am/AppStateManager$AppState;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V

    .line 491
    :cond_0
    return-void
.end method

.method public dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
    .locals 5
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "args"    # [Ljava/lang/String;
    .param p3, "opti"    # I

    .line 417
    iget-object v0, p0, Lcom/android/server/am/AppStateManager;->mAppLock:Ljava/lang/Object;

    monitor-enter v0

    .line 419
    add-int/lit8 v1, p3, 0x1

    :try_start_0
    array-length v2, p2

    if-ge v1, v2, :cond_2

    .line 420
    aget-object v1, p2, p3

    .line 421
    .local v1, "parm":Ljava/lang/String;
    const/4 v2, 0x0

    .line 422
    .local v2, "dpUid":I
    const-string/jumbo v3, "uid"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "-u"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 423
    :cond_0
    add-int/lit8 v3, p3, 0x1

    aget-object v3, p2, v3

    .line 424
    .local v3, "uidStr":Ljava/lang/String;
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    move v2, v4

    .line 426
    .end local v3    # "uidStr":Ljava/lang/String;
    :cond_1
    iget-object v3, p0, Lcom/android/server/am/AppStateManager;->mActiveApps:Lcom/android/server/am/AppStateManager$ActiveApps;

    invoke-virtual {v3, p1, p2, p3, v2}, Lcom/android/server/am/AppStateManager$ActiveApps;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;II)V

    .line 427
    .end local v1    # "parm":Ljava/lang/String;
    .end local v2    # "dpUid":I
    goto :goto_0

    .line 428
    :cond_2
    iget-object v1, p0, Lcom/android/server/am/AppStateManager;->mActiveApps:Lcom/android/server/am/AppStateManager$ActiveApps;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, p2, p3, v2}, Lcom/android/server/am/AppStateManager$ActiveApps;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 432
    :goto_0
    goto :goto_1

    .line 433
    :catchall_0
    move-exception v1

    goto :goto_2

    .line 430
    :catch_0
    move-exception v1

    .line 431
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 433
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    monitor-exit v0

    .line 434
    return-void

    .line 433
    :goto_2
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public dumpStack(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
    .locals 1
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "args"    # [Ljava/lang/String;
    .param p3, "opti"    # I

    .line 437
    iget-object v0, p0, Lcom/android/server/am/AppStateManager;->mCurrentTaskStack:Lcom/android/server/am/AppStateManager$CurrentTaskStack;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/am/AppStateManager$CurrentTaskStack;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V

    .line 438
    return-void
.end method

.method public getAllAppState()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/smartpower/IAppState;",
            ">;"
        }
    .end annotation

    .line 340
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 341
    .local v0, "appList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState;>;"
    iget-object v1, p0, Lcom/android/server/am/AppStateManager;->mAppLock:Ljava/lang/Object;

    monitor-enter v1

    .line 342
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/am/AppStateManager;->mActiveApps:Lcom/android/server/am/AppStateManager$ActiveApps;

    invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$ActiveApps;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 343
    iget-object v3, p0, Lcom/android/server/am/AppStateManager;->mActiveApps:Lcom/android/server/am/AppStateManager$ActiveApps;

    invoke-virtual {v3, v2}, Lcom/android/server/am/AppStateManager$ActiveApps;->valueAt(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v3

    .line 344
    .local v3, "app":Lcom/android/server/am/AppStateManager$AppState;
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 342
    nop

    .end local v3    # "app":Lcom/android/server/am/AppStateManager$AppState;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 346
    .end local v2    # "i":I
    :cond_0
    monitor-exit v1

    .line 347
    return-object v0

    .line 346
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
    .locals 2
    .param p1, "uid"    # I

    .line 354
    iget-object v0, p0, Lcom/android/server/am/AppStateManager;->mAppLock:Ljava/lang/Object;

    monitor-enter v0

    .line 355
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager;->mActiveApps:Lcom/android/server/am/AppStateManager$ActiveApps;

    invoke-virtual {v1, p1}, Lcom/android/server/am/AppStateManager$ActiveApps;->get(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v1

    monitor-exit v0

    return-object v1

    .line 356
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getAppState(Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/android/server/am/AppStateManager$AppState;",
            ">;"
        }
    .end annotation

    .line 363
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 364
    .local v0, "apps":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/AppStateManager$AppState;>;"
    iget-object v1, p0, Lcom/android/server/am/AppStateManager;->mAppLock:Ljava/lang/Object;

    monitor-enter v1

    .line 365
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/am/AppStateManager;->mActiveApps:Lcom/android/server/am/AppStateManager$ActiveApps;

    invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$ActiveApps;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 366
    iget-object v3, p0, Lcom/android/server/am/AppStateManager;->mActiveApps:Lcom/android/server/am/AppStateManager$ActiveApps;

    invoke-virtual {v3, v2}, Lcom/android/server/am/AppStateManager$ActiveApps;->valueAt(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v3

    .line 367
    .local v3, "app":Lcom/android/server/am/AppStateManager$AppState;
    invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 368
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 365
    .end local v3    # "app":Lcom/android/server/am/AppStateManager$AppState;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 371
    .end local v2    # "i":I
    :cond_1
    monitor-exit v1

    .line 372
    return-object v0

    .line 371
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public getHomeProcessPidLocked()I
    .locals 2

    .line 672
    iget-object v0, p0, Lcom/android/server/am/AppStateManager;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mAtmInternal:Lcom/android/server/wm/ActivityTaskManagerInternal;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerInternal;->getHomeProcess()Lcom/android/server/wm/WindowProcessController;

    move-result-object v0

    .line 673
    .local v0, "homeProcess":Lcom/android/server/wm/WindowProcessController;
    if-eqz v0, :cond_0

    .line 674
    invoke-virtual {v0}, Lcom/android/server/wm/WindowProcessController;->getPid()I

    move-result v1

    return v1

    .line 676
    :cond_0
    const/4 v1, -0x1

    return v1
.end method

.method getLruProcesses()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/smartpower/IAppState$IRunningProcess;",
            ">;"
        }
    .end annotation

    .line 750
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 751
    .local v0, "lruProcesses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
    iget-object v1, p0, Lcom/android/server/am/AppStateManager;->mAppLock:Ljava/lang/Object;

    monitor-enter v1

    .line 752
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/am/AppStateManager;->mActiveApps:Lcom/android/server/am/AppStateManager$ActiveApps;

    invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$ActiveApps;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 753
    iget-object v3, p0, Lcom/android/server/am/AppStateManager;->mActiveApps:Lcom/android/server/am/AppStateManager$ActiveApps;

    invoke-virtual {v3, v2}, Lcom/android/server/am/AppStateManager$ActiveApps;->valueAt(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v3

    .line 754
    .local v3, "app":Lcom/android/server/am/AppStateManager$AppState;
    invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcessList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 752
    nop

    .end local v3    # "app":Lcom/android/server/am/AppStateManager$AppState;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 756
    .end local v2    # "i":I
    :cond_0
    monitor-exit v1

    .line 757
    return-object v0

    .line 756
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method getLruProcesses(ILjava/lang/String;)Ljava/util/ArrayList;
    .locals 4
    .param p1, "uid"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/smartpower/IAppState$IRunningProcess;",
            ">;"
        }
    .end annotation

    .line 764
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 765
    .local v0, "processes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
    iget-object v1, p0, Lcom/android/server/am/AppStateManager;->mAppLock:Ljava/lang/Object;

    monitor-enter v1

    .line 766
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/AppStateManager;->mActiveApps:Lcom/android/server/am/AppStateManager$ActiveApps;

    invoke-virtual {v2, p1}, Lcom/android/server/am/AppStateManager$ActiveApps;->get(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v2

    .line 767
    .local v2, "app":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v2, :cond_0

    .line 768
    invoke-virtual {v2, p2}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcessList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 770
    .end local v2    # "app":Lcom/android/server/am/AppStateManager$AppState;
    :cond_0
    monitor-exit v1

    .line 771
    return-object v0

    .line 770
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public getProcessState(ILjava/lang/String;)I
    .locals 2
    .param p1, "uid"    # I
    .param p2, "processName"    # Ljava/lang/String;

    .line 376
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 377
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 378
    invoke-virtual {v0, p2}, Lcom/android/server/am/AppStateManager$AppState;->getProcessState(Ljava/lang/String;)I

    move-result v1

    return v1

    .line 380
    :cond_0
    const/4 v1, -0x1

    return v1
.end method

.method public getRunningProcess(II)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    .locals 2
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 392
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 393
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 394
    invoke-virtual {v0, p2}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v1

    return-object v1

    .line 396
    :cond_0
    const/4 v1, 0x0

    return-object v1
.end method

.method public getRunningProcess(ILjava/lang/String;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    .locals 2
    .param p1, "uid"    # I
    .param p2, "processName"    # Ljava/lang/String;

    .line 384
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 385
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 386
    invoke-virtual {v0, p2}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(Ljava/lang/String;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v1

    return-object v1

    .line 388
    :cond_0
    const/4 v1, 0x0

    return-object v1
.end method

.method public hibernateAllIfNeeded(Ljava/lang/String;)V
    .locals 2
    .param p1, "reason"    # Ljava/lang/String;

    .line 795
    iget-object v0, p0, Lcom/android/server/am/AppStateManager;->mMainHandler:Lcom/android/server/am/AppStateManager$MainHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/android/server/am/AppStateManager$MainHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 796
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/am/AppStateManager;->mMainHandler:Lcom/android/server/am/AppStateManager$MainHandler;

    invoke-virtual {v1, v0}, Lcom/android/server/am/AppStateManager$MainHandler;->sendMessage(Landroid/os/Message;)Z

    .line 797
    return-void
.end method

.method public hibernateAllInactive(Lcom/android/server/am/AppStateManager$AppState;Ljava/lang/String;)V
    .locals 3
    .param p1, "startingApp"    # Lcom/android/server/am/AppStateManager$AppState;
    .param p2, "reason"    # Ljava/lang/String;

    .line 800
    iget-object v0, p0, Lcom/android/server/am/AppStateManager;->mMainHandler:Lcom/android/server/am/AppStateManager$MainHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/server/am/AppStateManager$MainHandler;->removeMessages(I)V

    .line 801
    iget-object v0, p0, Lcom/android/server/am/AppStateManager;->mMainHandler:Lcom/android/server/am/AppStateManager$MainHandler;

    invoke-virtual {v0, v1, p1}, Lcom/android/server/am/AppStateManager$MainHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 802
    .local v0, "msg":Landroid/os/Message;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 803
    .local v1, "data":Landroid/os/Bundle;
    const-string v2, "reason"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 804
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 805
    iget-object v2, p0, Lcom/android/server/am/AppStateManager;->mMainHandler:Lcom/android/server/am/AppStateManager$MainHandler;

    invoke-virtual {v2, v0}, Lcom/android/server/am/AppStateManager$MainHandler;->sendMessage(Landroid/os/Message;)Z

    .line 806
    return-void
.end method

.method init(Lcom/miui/server/smartpower/AppPowerResourceManager;Lcom/miui/server/smartpower/SmartPowerPolicyManager;Lcom/miui/server/smartpower/SmartScenarioManager;)V
    .locals 7
    .param p1, "appPowerResourceManager"    # Lcom/miui/server/smartpower/AppPowerResourceManager;
    .param p2, "smartPowerPolicyManager"    # Lcom/miui/server/smartpower/SmartPowerPolicyManager;
    .param p3, "smartScenarioManager"    # Lcom/miui/server/smartpower/SmartScenarioManager;

    .line 226
    iput-object p1, p0, Lcom/android/server/am/AppStateManager;->mAppPowerResourceManager:Lcom/miui/server/smartpower/AppPowerResourceManager;

    .line 227
    iput-object p2, p0, Lcom/android/server/am/AppStateManager;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    .line 228
    iput-object p3, p0, Lcom/android/server/am/AppStateManager;->mSmartScenarioManager:Lcom/miui/server/smartpower/SmartScenarioManager;

    .line 229
    invoke-static {}, Lcom/miui/server/process/ProcessManagerInternal;->getInstance()Lcom/miui/server/process/ProcessManagerInternal;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/AppStateManager;->mPmInternal:Lcom/miui/server/process/ProcessManagerInternal;

    .line 230
    iget-object v0, p0, Lcom/android/server/am/AppStateManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/AppStateManager;->mPkms:Landroid/content/pm/PackageManager;

    .line 231
    iget-object v0, p0, Lcom/android/server/am/AppStateManager;->mPmInternal:Lcom/miui/server/process/ProcessManagerInternal;

    invoke-virtual {v0}, Lcom/miui/server/process/ProcessManagerInternal;->getProcessPolicy()Lcom/android/server/am/IProcessPolicy;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/AppStateManager;->mProcessPolicy:Lcom/android/server/am/IProcessPolicy;

    .line 233
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 234
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.USER_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 235
    iget-object v1, p0, Lcom/android/server/am/AppStateManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/am/AppStateManager;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v4, v0

    invoke-virtual/range {v1 .. v6}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 236
    return-void
.end method

.method public isHomeProcessTopLocked()Z
    .locals 5

    .line 680
    invoke-virtual {p0}, Lcom/android/server/am/AppStateManager;->getHomeProcessPidLocked()I

    move-result v0

    .line 681
    .local v0, "homePid":I
    const/4 v1, 0x0

    if-lez v0, :cond_1

    .line 683
    iget-object v2, p0, Lcom/android/server/am/AppStateManager;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mPidsSelfLocked:Lcom/android/server/am/ActivityManagerService$PidMap;

    monitor-enter v2

    .line 684
    :try_start_0
    iget-object v3, p0, Lcom/android/server/am/AppStateManager;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mPidsSelfLocked:Lcom/android/server/am/ActivityManagerService$PidMap;

    invoke-virtual {v3, v0}, Lcom/android/server/am/ActivityManagerService$PidMap;->get(I)Lcom/android/server/am/ProcessRecord;

    move-result-object v3

    .line 685
    .local v3, "proc":Lcom/android/server/am/ProcessRecord;
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 686
    if-eqz v3, :cond_1

    .line 687
    iget-object v2, v3, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v2}, Lcom/android/server/am/ProcessStateRecord;->getCurProcState()I

    move-result v2

    const/4 v4, 0x2

    if-ne v2, v4, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1

    .line 685
    .end local v3    # "proc":Lcom/android/server/am/ProcessRecord;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 691
    :cond_1
    return v1
.end method

.method public isProcessIdle(II)Z
    .locals 2
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 290
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 291
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 292
    invoke-virtual {v0, p2}, Lcom/android/server/am/AppStateManager$AppState;->isProcessIdle(I)Z

    move-result v1

    return v1

    .line 294
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public isProcessInTaskStack(II)Z
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 318
    iget-object v0, p0, Lcom/android/server/am/AppStateManager;->mCurrentTaskStack:Lcom/android/server/am/AppStateManager$CurrentTaskStack;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/am/AppStateManager$CurrentTaskStack;->isProcessInTaskStack(II)Z

    move-result v0

    return v0
.end method

.method public isProcessInTaskStack(Ljava/lang/String;)Z
    .locals 1
    .param p1, "procName"    # Ljava/lang/String;

    .line 322
    iget-object v0, p0, Lcom/android/server/am/AppStateManager;->mCurrentTaskStack:Lcom/android/server/am/AppStateManager$CurrentTaskStack;

    invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager$CurrentTaskStack;->isProcessInTaskStack(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isProcessKilled(II)Z
    .locals 2
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 329
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 330
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 331
    invoke-virtual {v0, p2}, Lcom/android/server/am/AppStateManager$AppState;->isProcessKilled(I)Z

    move-result v1

    return v1

    .line 333
    :cond_0
    const/4 v1, 0x1

    return v1
.end method

.method public isProcessPerceptible(II)Z
    .locals 2
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 302
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 303
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 304
    invoke-virtual {v0, p2}, Lcom/android/server/am/AppStateManager$AppState;->isProcessPerceptible(I)Z

    move-result v1

    return v1

    .line 306
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public isProcessPerceptible(ILjava/lang/String;)Z
    .locals 2
    .param p1, "uid"    # I
    .param p2, "processName"    # Ljava/lang/String;

    .line 310
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 311
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 312
    invoke-virtual {v0, p2}, Lcom/android/server/am/AppStateManager$AppState;->isProcessPerceptible(Ljava/lang/String;)Z

    move-result v1

    return v1

    .line 314
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public isRecentThawed(IJ)Z
    .locals 6
    .param p1, "uid"    # I
    .param p2, "sinceUptime"    # J

    .line 731
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 732
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_1

    .line 733
    nop

    .line 734
    invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcessList()Ljava/util/ArrayList;

    move-result-object v1

    .line 735
    .local v1, "runningProcessList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 736
    .local v3, "process":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    move-object v4, v3

    check-cast v4, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 737
    invoke-static {v4, p2, p3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$mgetHistoryInfos(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;J)Ljava/util/ArrayList;

    move-result-object v4

    .line 738
    .local v4, "history":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 739
    const/4 v2, 0x1

    return v2

    .line 741
    .end local v3    # "process":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .end local v4    # "history":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;>;"
    :cond_0
    goto :goto_0

    .line 743
    .end local v1    # "runningProcessList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
    :cond_1
    const/4 v1, 0x0

    return v1
.end method

.method public isSystemApp(I)Z
    .locals 2
    .param p1, "uid"    # I

    .line 784
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 785
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 786
    invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState;->isSystemApp()Z

    move-result v1

    return v1

    .line 788
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public isUidIdle(I)Z
    .locals 2
    .param p1, "uid"    # I

    .line 257
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 258
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 259
    invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState;->isIdle()Z

    move-result v1

    return v1

    .line 261
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public isUidInactive(I)Z
    .locals 2
    .param p1, "uid"    # I

    .line 268
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 269
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 270
    invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState;->isInactive()Z

    move-result v1

    return v1

    .line 272
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public isUidVisible(I)Z
    .locals 2
    .param p1, "uid"    # I

    .line 279
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 280
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 281
    invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState;->isVsible()Z

    move-result v1

    return v1

    .line 283
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public onAddToWhiteList(I)V
    .locals 1
    .param p1, "uid"    # I

    .line 555
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 556
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 557
    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$monAddToWhiteList(Lcom/android/server/am/AppStateManager$AppState;)V

    .line 559
    :cond_0
    return-void
.end method

.method public onAddToWhiteList(II)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 562
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 563
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 564
    invoke-static {v0, p2}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$monAddToWhiteList(Lcom/android/server/am/AppStateManager$AppState;I)V

    .line 566
    :cond_0
    return-void
.end method

.method public onApplyOomAdjLocked(Lcom/android/server/am/ProcessRecord;)V
    .locals 1
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 618
    iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 619
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 620
    invoke-static {v0, p1}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$monApplyOomAdjLocked(Lcom/android/server/am/AppStateManager$AppState;Lcom/android/server/am/ProcessRecord;)V

    .line 622
    :cond_0
    return-void
.end method

.method public onBackupChanged(ZLcom/android/server/am/ProcessRecord;)V
    .locals 1
    .param p1, "active"    # Z
    .param p2, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 604
    iget v0, p2, Lcom/android/server/am/ProcessRecord;->uid:I

    invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 605
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 606
    invoke-static {v0, p1, p2}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$monBackupChanged(Lcom/android/server/am/AppStateManager$AppState;ZLcom/android/server/am/ProcessRecord;)V

    .line 608
    :cond_0
    return-void
.end method

.method public onBackupServiceAppChanged(ZII)V
    .locals 1
    .param p1, "active"    # Z
    .param p2, "uid"    # I
    .param p3, "pid"    # I

    .line 611
    invoke-virtual {p0, p2}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 612
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 613
    invoke-static {v0, p1, p3}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$monBackupServiceAppChanged(Lcom/android/server/am/AppStateManager$AppState;ZI)V

    .line 615
    :cond_0
    return-void
.end method

.method public onForegroundActivityChangedLocked(IIILjava/lang/String;IILjava/lang/String;)V
    .locals 3
    .param p1, "fromPid"    # I
    .param p2, "toUid"    # I
    .param p3, "toPid"    # I
    .param p4, "name"    # Ljava/lang/String;
    .param p5, "taskId"    # I
    .param p6, "callingUid"    # I
    .param p7, "callingPkg"    # Ljava/lang/String;

    .line 696
    invoke-virtual {p0, p2}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 697
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 698
    invoke-static {v0, p4}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$monActivityForegroundLocked(Lcom/android/server/am/AppStateManager$AppState;Ljava/lang/String;)V

    .line 699
    invoke-virtual {v0, p3}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v1

    .line 700
    .local v1, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    if-eqz v1, :cond_0

    .line 701
    iget-object v2, p0, Lcom/android/server/am/AppStateManager;->mCurrentTaskStack:Lcom/android/server/am/AppStateManager$CurrentTaskStack;

    invoke-virtual {v2, p5, v1, p6, p7}, Lcom/android/server/am/AppStateManager$CurrentTaskStack;->updateIfNeeded(ILcom/android/server/am/AppStateManager$AppState$RunningProcess;ILjava/lang/String;)V

    .line 702
    invoke-static {v1, p5}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fputmLastTaskId(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;I)V

    .line 705
    .end local v1    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    :cond_0
    return-void
.end method

.method public onHoldScreenUidChanged(IZ)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "hold"    # Z

    .line 642
    if-eqz p2, :cond_0

    .line 643
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    goto :goto_0

    .line 645
    .end local v0    # "appState":Lcom/android/server/am/AppStateManager$AppState;
    :cond_0
    iget v0, p0, Lcom/android/server/am/AppStateManager;->mHoldScreenUid:I

    invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 647
    .restart local v0    # "appState":Lcom/android/server/am/AppStateManager$AppState;
    :goto_0
    iput p1, p0, Lcom/android/server/am/AppStateManager;->mHoldScreenUid:I

    .line 648
    if-eqz v0, :cond_1

    .line 649
    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$mupdateCurrentResourceBehavier(Lcom/android/server/am/AppStateManager$AppState;)V

    .line 651
    :cond_1
    return-void
.end method

.method public onInputMethodShow(I)V
    .locals 1
    .param p1, "uid"    # I

    .line 597
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 598
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 599
    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$monInputMethodShow(Lcom/android/server/am/AppStateManager$AppState;)V

    .line 601
    :cond_0
    return-void
.end method

.method public onMediaKey(I)V
    .locals 1
    .param p1, "uid"    # I

    .line 590
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 591
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 592
    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$monMediaKey(Lcom/android/server/am/AppStateManager$AppState;)V

    .line 594
    :cond_0
    return-void
.end method

.method public onMediaKey(II)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 583
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 584
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 585
    invoke-static {v0, p2}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$monMediaKey(Lcom/android/server/am/AppStateManager$AppState;I)V

    .line 587
    :cond_0
    return-void
.end method

.method public onRemoveFromWhiteList(I)V
    .locals 1
    .param p1, "uid"    # I

    .line 569
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 570
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 571
    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$monRemoveFromWhiteList(Lcom/android/server/am/AppStateManager$AppState;)V

    .line 573
    :cond_0
    return-void
.end method

.method public onRemoveFromWhiteList(II)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 576
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 577
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 578
    invoke-static {v0, p2}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$monRemoveFromWhiteList(Lcom/android/server/am/AppStateManager$AppState;I)V

    .line 580
    :cond_0
    return-void
.end method

.method public onSendPendingIntent(ILjava/lang/String;)V
    .locals 5
    .param p1, "uid"    # I
    .param p2, "typeName"    # Ljava/lang/String;

    .line 524
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 525
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 526
    invoke-static {v0, p2}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$monSendPendingIntent(Lcom/android/server/am/AppStateManager$AppState;Ljava/lang/String;)V

    goto :goto_0

    .line 528
    :cond_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager;->mPendingInteractiveUids:Landroid/util/SparseArray;

    monitor-enter v1

    .line 529
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/AppStateManager;->mPendingInteractiveUids:Landroid/util/SparseArray;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, p1, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 530
    monitor-exit v1

    .line 532
    :goto_0
    return-void

    .line 530
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public processKilledLocked(Lcom/android/server/am/ProcessRecord;)V
    .locals 1
    .param p1, "record"    # Lcom/android/server/am/ProcessRecord;

    .line 453
    iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 454
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 455
    invoke-static {v0, p1}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$mprocessKilledLocked(Lcom/android/server/am/AppStateManager$AppState;Lcom/android/server/am/ProcessRecord;)V

    .line 456
    invoke-direct {p0, v0}, Lcom/android/server/am/AppStateManager;->removeAppIfNeeded(Lcom/android/server/am/AppStateManager$AppState;)V

    .line 458
    :cond_0
    return-void
.end method

.method public processStartedLocked(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
    .locals 9
    .param p1, "record"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "packageName"    # Ljava/lang/String;

    .line 442
    iget-object v0, p0, Lcom/android/server/am/AppStateManager;->mAppLock:Ljava/lang/Object;

    monitor-enter v0

    .line 443
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager;->mActiveApps:Lcom/android/server/am/AppStateManager$ActiveApps;

    iget v2, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    invoke-virtual {v1, v2}, Lcom/android/server/am/AppStateManager$ActiveApps;->get(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v1

    .line 444
    .local v1, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-nez v1, :cond_0

    .line 445
    new-instance v8, Lcom/android/server/am/AppStateManager$AppState;

    iget v4, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    iget-object v6, p0, Lcom/android/server/am/AppStateManager;->mContext:Landroid/content/Context;

    const/4 v7, 0x0

    move-object v2, v8

    move-object v3, p0

    move-object v5, p2

    invoke-direct/range {v2 .. v7}, Lcom/android/server/am/AppStateManager$AppState;-><init>(Lcom/android/server/am/AppStateManager;ILjava/lang/String;Landroid/content/Context;Lcom/android/server/am/AppStateManager$AppState-IA;)V

    move-object v1, v8

    .line 446
    iget-object v2, p0, Lcom/android/server/am/AppStateManager;->mActiveApps:Lcom/android/server/am/AppStateManager$ActiveApps;

    iget v3, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    invoke-virtual {v2, v3, v1}, Lcom/android/server/am/AppStateManager$ActiveApps;->put(ILcom/android/server/am/AppStateManager$AppState;)V

    .line 448
    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 449
    invoke-static {v1, p1}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$mprocessStartedLocked(Lcom/android/server/am/AppStateManager$AppState;Lcom/android/server/am/ProcessRecord;)V

    .line 450
    return-void

    .line 448
    .end local v1    # "appState":Lcom/android/server/am/AppStateManager$AppState;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public providerConnectionChangedLocked(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;Z)V
    .locals 5
    .param p1, "client"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "host"    # Lcom/android/server/am/ProcessRecord;
    .param p3, "isConnect"    # Z

    .line 544
    iget-object v0, p0, Lcom/android/server/am/AppStateManager;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    iget v1, p2, Lcom/android/server/am/ProcessRecord;->uid:I

    iget v2, p2, Lcom/android/server/am/ProcessRecord;->mPid:I

    iget-object v3, p2, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v4, p2, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isProcessHibernationWhiteList(IILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 546
    return-void

    .line 548
    :cond_0
    iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 549
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_1

    .line 550
    invoke-static {v0, p1, p2, p3}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$mupdateProviderDepends(Lcom/android/server/am/AppStateManager$AppState;Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;Z)V

    .line 552
    :cond_1
    return-void
.end method

.method public registerAppStateListener(Lcom/android/server/am/AppStateManager$IProcessStateCallback;)Z
    .locals 2
    .param p1, "callback"    # Lcom/android/server/am/AppStateManager$IProcessStateCallback;

    .line 3379
    iget-object v0, p0, Lcom/android/server/am/AppStateManager;->mProcessStateCallbacks:Ljava/util/ArrayList;

    monitor-enter v0

    .line 3380
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager;->mProcessStateCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3381
    iget-object v1, p0, Lcom/android/server/am/AppStateManager;->mProcessStateCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3382
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 3384
    :cond_0
    monitor-exit v0

    const/4 v0, 0x0

    return v0

    .line 3385
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public serviceConnectionChangedLocked(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;ZJ)V
    .locals 7
    .param p1, "client"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "service"    # Lcom/android/server/am/ProcessRecord;
    .param p3, "isConnect"    # Z
    .param p4, "flags"    # J

    .line 536
    iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 537
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 538
    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-wide v5, p4

    invoke-static/range {v1 .. v6}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$mupdateServiceDepends(Lcom/android/server/am/AppStateManager$AppState;Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;ZJ)V

    .line 540
    :cond_0
    return-void
.end method

.method public setActivityVisible(ILjava/lang/String;Lcom/android/server/wm/ActivityRecord;Z)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "processName"    # Ljava/lang/String;
    .param p3, "record"    # Lcom/android/server/wm/ActivityRecord;
    .param p4, "visible"    # Z

    .line 626
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 627
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 628
    invoke-static {v0, p2, p3, p4}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$msetActivityVisible(Lcom/android/server/am/AppStateManager$AppState;Ljava/lang/String;Lcom/android/server/wm/ActivityRecord;Z)V

    .line 630
    :cond_0
    return-void
.end method

.method public setProcessPss(Lcom/android/server/am/ProcessRecord;JJ)V
    .locals 2
    .param p1, "record"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "pss"    # J
    .param p4, "swapPss"    # J

    .line 469
    iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/android/server/am/AppStateManager;->getRunningProcess(ILjava/lang/String;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    move-result-object v0

    .line 470
    .local v0, "runProc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    if-eqz v0, :cond_0

    .line 471
    invoke-virtual {v0, p2, p3, p4, p5}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->setPss(JJ)V

    .line 473
    :cond_0
    return-void
.end method

.method public setWindowVisible(IILcom/android/server/policy/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;Z)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "win"    # Lcom/android/server/policy/WindowManagerPolicy$WindowState;
    .param p4, "attrs"    # Landroid/view/WindowManager$LayoutParams;
    .param p5, "visible"    # Z

    .line 634
    invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 635
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 636
    invoke-static {v0, p2, p3, p4, p5}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$msetWindowVisible(Lcom/android/server/am/AppStateManager$AppState;ILcom/android/server/policy/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;Z)V

    .line 638
    :cond_0
    return-void
.end method

.method public unRegisterAppStateListener(Lcom/android/server/am/AppStateManager$IProcessStateCallback;)V
    .locals 2
    .param p1, "callback"    # Lcom/android/server/am/AppStateManager$IProcessStateCallback;

    .line 3389
    iget-object v0, p0, Lcom/android/server/am/AppStateManager;->mProcessStateCallbacks:Ljava/util/ArrayList;

    monitor-enter v0

    .line 3390
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager;->mProcessStateCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 3391
    monitor-exit v0

    .line 3392
    return-void

    .line 3391
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public updateAllAppUsageStats()Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/smartpower/IAppState;",
            ">;"
        }
    .end annotation

    .line 400
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 401
    .local v0, "appList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState;>;"
    iget-object v1, p0, Lcom/android/server/am/AppStateManager;->mAppLock:Ljava/lang/Object;

    monitor-enter v1

    .line 402
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/am/AppStateManager;->mActiveApps:Lcom/android/server/am/AppStateManager$ActiveApps;

    invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$ActiveApps;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 403
    iget-object v3, p0, Lcom/android/server/am/AppStateManager;->mActiveApps:Lcom/android/server/am/AppStateManager$ActiveApps;

    invoke-virtual {v3, v2}, Lcom/android/server/am/AppStateManager$ActiveApps;->valueAt(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v3

    .line 404
    .local v3, "app":Lcom/miui/server/smartpower/IAppState;
    iget-object v4, p0, Lcom/android/server/am/AppStateManager;->mSmartPowerPolicyManager:Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    .line 405
    invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->getUsageStatsInfo(Ljava/lang/String;)Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;

    move-result-object v4

    .line 406
    .local v4, "usageStats":Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;
    if-eqz v4, :cond_0

    .line 407
    invoke-virtual {v4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->getAppLaunchCount()I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/miui/server/smartpower/IAppState;->setLaunchCount(I)V

    .line 408
    invoke-virtual {v4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->getTotalTimeInForeground()J

    move-result-wide v5

    invoke-virtual {v3, v5, v6}, Lcom/miui/server/smartpower/IAppState;->setTotalTimeInForeground(J)V

    .line 410
    :cond_0
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 402
    nop

    .end local v3    # "app":Lcom/miui/server/smartpower/IAppState;
    .end local v4    # "usageStats":Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 412
    .end local v2    # "i":I
    :cond_1
    monitor-exit v1

    .line 413
    return-object v0

    .line 412
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public updateProcessLocked(Lcom/android/server/am/ProcessRecord;)V
    .locals 1
    .param p1, "record"    # Lcom/android/server/am/ProcessRecord;

    .line 461
    iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v0

    .line 462
    .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v0, :cond_0

    .line 463
    invoke-static {v0, p1}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$mupdateProcessLocked(Lcom/android/server/am/AppStateManager$AppState;Lcom/android/server/am/ProcessRecord;)V

    .line 464
    invoke-direct {p0, v0}, Lcom/android/server/am/AppStateManager;->removeAppIfNeeded(Lcom/android/server/am/AppStateManager$AppState;)V

    .line 466
    :cond_0
    return-void
.end method

.method public updateVisibilityLocked(Landroid/util/ArraySet;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/ArraySet<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 708
    .local p1, "uids":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/Integer;>;"
    invoke-virtual {p1}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 709
    .local v1, "uid":I
    invoke-virtual {p0, v1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;

    move-result-object v2

    .line 710
    .local v2, "appState":Lcom/android/server/am/AppStateManager$AppState;
    if-eqz v2, :cond_0

    .line 711
    invoke-static {v2}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$mupdateVisibility(Lcom/android/server/am/AppStateManager$AppState;)V

    .line 713
    .end local v1    # "uid":I
    .end local v2    # "appState":Lcom/android/server/am/AppStateManager$AppState;
    :cond_0
    goto :goto_0

    .line 714
    :cond_1
    return-void
.end method
