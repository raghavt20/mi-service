class com.android.server.am.PeriodicCleanerService$PackageUseInfo {
	 /* .source "PeriodicCleanerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/PeriodicCleanerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "PackageUseInfo" */
} // .end annotation
/* # instance fields */
private Long mBackgroundTime;
private Boolean mFgTrimDone;
private java.lang.String mPackageName;
private Integer mUid;
private Integer mUserId;
final com.android.server.am.PeriodicCleanerService this$0; //synthetic
/* # direct methods */
static Long -$$Nest$fgetmBackgroundTime ( com.android.server.am.PeriodicCleanerService$PackageUseInfo p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mBackgroundTime:J */
/* return-wide v0 */
} // .end method
static Boolean -$$Nest$fgetmFgTrimDone ( com.android.server.am.PeriodicCleanerService$PackageUseInfo p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mFgTrimDone:Z */
} // .end method
static java.lang.String -$$Nest$fgetmPackageName ( com.android.server.am.PeriodicCleanerService$PackageUseInfo p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPackageName;
} // .end method
static Integer -$$Nest$fgetmUid ( com.android.server.am.PeriodicCleanerService$PackageUseInfo p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mUid:I */
} // .end method
static Integer -$$Nest$fgetmUserId ( com.android.server.am.PeriodicCleanerService$PackageUseInfo p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mUserId:I */
} // .end method
static void -$$Nest$fputmBackgroundTime ( com.android.server.am.PeriodicCleanerService$PackageUseInfo p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-wide p1, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mBackgroundTime:J */
return;
} // .end method
static void -$$Nest$fputmFgTrimDone ( com.android.server.am.PeriodicCleanerService$PackageUseInfo p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mFgTrimDone:Z */
return;
} // .end method
public com.android.server.am.PeriodicCleanerService$PackageUseInfo ( ) {
/* .locals 0 */
/* .param p2, "uid" # I */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .param p4, "backgroundTime" # J */
/* .line 1885 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 1886 */
this.mPackageName = p3;
/* .line 1887 */
p1 = android.os.UserHandle .getUserId ( p2 );
/* iput p1, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mUserId:I */
/* .line 1888 */
/* iput p2, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mUid:I */
/* .line 1889 */
/* iput-wide p4, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mBackgroundTime:J */
/* .line 1890 */
int p1 = 0; // const/4 p1, 0x0
/* iput-boolean p1, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mFgTrimDone:Z */
/* .line 1891 */
return;
} // .end method
/* # virtual methods */
public Boolean equals ( java.lang.Object p0 ) {
/* .locals 4 */
/* .param p1, "obj" # Ljava/lang/Object; */
/* .line 1906 */
int v0 = 1; // const/4 v0, 0x1
/* if-ne p0, p1, :cond_0 */
/* .line 1907 */
/* .line 1910 */
} // :cond_0
/* instance-of v1, p1, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1911 */
/* move-object v1, p1 */
/* check-cast v1, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo; */
/* .line 1912 */
/* .local v1, "another":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo; */
/* iget v2, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mUid:I */
/* iget v3, v1, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mUid:I */
/* if-ne v2, v3, :cond_1 */
v2 = this.mPackageName;
v3 = this.mPackageName;
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
	 /* .line 1913 */
	 /* .line 1916 */
} // .end local v1 # "another":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public java.lang.String toString ( ) {
/* .locals 4 */
/* .line 1894 */
/* new-instance v0, Ljava/text/SimpleDateFormat; */
/* const-string/jumbo v1, "yyyy-MM-dd HH:mm:ss" */
/* invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V */
/* .line 1895 */
/* .local v0, "dateformat":Ljava/text/SimpleDateFormat; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "u" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mUserId:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = "/"; // const-string v2, "/"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mUid:I */
v2 = android.os.UserHandle .getAppId ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ":"; // const-string v2, ":"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mPackageName;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1896 */
/* iget-boolean v3, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mFgTrimDone:Z */
if ( v3 != null) { // if-eqz v3, :cond_0
final String v3 = "(trimed)"; // const-string v3, "(trimed)"
} // :cond_0
final String v3 = "(non-trimed)"; // const-string v3, "(non-trimed)"
} // :goto_0
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mBackgroundTime:J */
/* .line 1897 */
java.lang.Long .valueOf ( v2,v3 );
(( java.text.SimpleDateFormat ) v0 ).format ( v2 ); // invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1895 */
} // .end method
public void updateUid ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 1901 */
/* iput p1, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mUid:I */
/* .line 1902 */
v0 = android.os.UserHandle .getUserId ( p1 );
/* iput v0, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mUserId:I */
/* .line 1903 */
return;
} // .end method
