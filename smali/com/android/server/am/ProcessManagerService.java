public class com.android.server.am.ProcessManagerService extends miui.process.ProcessManagerNative {
	 /* .source "ProcessManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/am/ProcessManagerService$MainHandler;, */
	 /* Lcom/android/server/am/ProcessManagerService$LocalService;, */
	 /* Lcom/android/server/am/ProcessManagerService$ProcessMangaerShellCommand;, */
	 /* Lcom/android/server/am/ProcessManagerService$Lifecycle; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String BINDER_MONITOR_FD_PATH;
private static final Boolean DEBUG;
public static final java.lang.String DEVICE;
static final Integer MAX_PROCESS_CONFIG_HISTORY;
static final Integer RESTORE_AI_PROCESSES_INFO_MSG;
static final Integer SKIP_PRELOAD_COUNT_LIMIT;
private static final java.lang.String TAG;
static final Integer USER_OWNER;
static final Integer USER_XSPACE;
/* # instance fields */
private android.view.accessibility.AccessibilityManager mAccessibilityManager;
private com.android.server.am.ActivityManagerService mActivityManagerService;
private android.app.AppOpsManager mAppOpsManager;
private java.io.FileWriter mBinderDelayWriter;
private android.content.Context mContext;
private android.hardware.display.DisplayManagerInternal mDisplayManagerInternal;
private com.android.server.wm.ForegroundInfoManager mForegroundInfoManager;
final com.android.server.am.ProcessManagerService$MainHandler mHandler;
Integer mHistoryNext;
private final com.miui.server.process.ProcessManagerInternal mInternal;
private java.util.ArrayList mLruProcesses;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/am/ProcessRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.am.MiuiApplicationThreadManager mMiuiApplicationThreadManager;
private android.app.INotificationManager mNotificationManager;
private com.android.internal.app.IPerfShielder mPerfService;
private android.content.pm.PackageManager mPkms;
private com.android.server.am.PreloadAppControllerImpl mPreloadAppController;
final miui.process.ProcessConfig mProcessConfigHistory;
private com.android.server.am.ProcessKiller mProcessKiller;
private com.android.server.am.ProcessPolicy mProcessPolicy;
private com.android.server.am.ProcessStarter mProcessStarter;
private com.miui.server.rtboost.SchedBoostManagerInternal mSchedBoostService;
final com.android.server.ServiceThread mServiceThread;
private com.miui.app.smartpower.SmartPowerServiceInternal mSmartPowerService;
private java.util.Set mSystemSignatures;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Landroid/content/pm/Signature;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.android.server.am.ActivityManagerService -$$Nest$fgetmActivityManagerService ( com.android.server.am.ProcessManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mActivityManagerService;
} // .end method
static java.io.FileWriter -$$Nest$fgetmBinderDelayWriter ( com.android.server.am.ProcessManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mBinderDelayWriter;
} // .end method
static com.android.server.wm.ForegroundInfoManager -$$Nest$fgetmForegroundInfoManager ( com.android.server.am.ProcessManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mForegroundInfoManager;
} // .end method
static com.android.server.am.MiuiApplicationThreadManager -$$Nest$fgetmMiuiApplicationThreadManager ( com.android.server.am.ProcessManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMiuiApplicationThreadManager;
} // .end method
static com.android.server.am.PreloadAppControllerImpl -$$Nest$fgetmPreloadAppController ( com.android.server.am.ProcessManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPreloadAppController;
} // .end method
static com.android.server.am.ProcessPolicy -$$Nest$fgetmProcessPolicy ( com.android.server.am.ProcessManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mProcessPolicy;
} // .end method
static com.android.server.am.ProcessStarter -$$Nest$fgetmProcessStarter ( com.android.server.am.ProcessManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mProcessStarter;
} // .end method
static miui.process.RunningProcessInfo -$$Nest$mgenerateRunningProcessInfo ( com.android.server.am.ProcessManagerService p0, com.android.server.am.ProcessRecord p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessManagerService;->generateRunningProcessInfo(Lcom/android/server/am/ProcessRecord;)Lmiui/process/RunningProcessInfo; */
} // .end method
static Boolean -$$Nest$misAppHasForegroundServices ( com.android.server.am.ProcessManagerService p0, com.android.server.am.ProcessRecord p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessManagerService;->isAppHasForegroundServices(Lcom/android/server/am/ProcessRecord;)Z */
} // .end method
static void -$$Nest$mnotifyAmsProcessKill ( com.android.server.am.ProcessManagerService p0, com.android.server.am.ProcessRecord p1, java.lang.String p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/am/ProcessManagerService;->notifyAmsProcessKill(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$mnotifyProcessDied ( com.android.server.am.ProcessManagerService p0, com.android.server.am.ProcessRecord p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessManagerService;->notifyProcessDied(Lcom/android/server/am/ProcessRecord;)V */
return;
} // .end method
static void -$$Nest$mnotifyProcessStarted ( com.android.server.am.ProcessManagerService p0, com.android.server.am.ProcessRecord p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessManagerService;->notifyProcessStarted(Lcom/android/server/am/ProcessRecord;)V */
return;
} // .end method
static com.android.server.am.ProcessManagerService ( ) {
/* .locals 1 */
/* .line 159 */
v0 = miui.os.Build.DEVICE;
(( java.lang.String ) v0 ).toLowerCase ( ); // invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
return;
} // .end method
public com.android.server.am.ProcessManagerService ( ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 181 */
/* invoke-direct {p0}, Lmiui/process/ProcessManagerNative;-><init>()V */
/* .line 152 */
/* const/16 v0, 0x1e */
/* new-array v0, v0, [Lmiui/process/ProcessConfig; */
this.mProcessConfigHistory = v0;
/* .line 153 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/am/ProcessManagerService;->mHistoryNext:I */
/* .line 182 */
this.mContext = p1;
/* .line 183 */
final String v0 = "appops"; // const-string v0, "appops"
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/AppOpsManager; */
this.mAppOpsManager = v0;
/* .line 184 */
android.app.NotificationManager .getService ( );
this.mNotificationManager = v0;
/* .line 185 */
android.app.ActivityManagerNative .getDefault ( );
/* check-cast v0, Lcom/android/server/am/ActivityManagerService; */
this.mActivityManagerService = v0;
/* .line 187 */
v0 = this.mContext;
final String v1 = "accessibility"; // const-string v1, "accessibility"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/view/accessibility/AccessibilityManager; */
this.mAccessibilityManager = v0;
/* .line 188 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
this.mPkms = v0;
/* .line 190 */
v0 = this.mActivityManagerService;
v0 = this.mProcessList;
(( com.android.server.am.ProcessList ) v0 ).getLruProcessesLOSP ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessList;->getLruProcessesLOSP()Ljava/util/ArrayList;
this.mLruProcesses = v0;
/* .line 192 */
/* new-instance v0, Lcom/android/server/ServiceThread; */
final String v1 = "ProcessManager"; // const-string v1, "ProcessManager"
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v0, v1, v2, v2}, Lcom/android/server/ServiceThread;-><init>(Ljava/lang/String;IZ)V */
this.mServiceThread = v0;
/* .line 193 */
(( com.android.server.ServiceThread ) v0 ).start ( ); // invoke-virtual {v0}, Lcom/android/server/ServiceThread;->start()V
/* .line 194 */
/* new-instance v1, Lcom/android/server/am/ProcessManagerService$MainHandler; */
(( com.android.server.ServiceThread ) v0 ).getLooper ( ); // invoke-virtual {v0}, Lcom/android/server/ServiceThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v1, p0, v2}, Lcom/android/server/am/ProcessManagerService$MainHandler;-><init>(Lcom/android/server/am/ProcessManagerService;Landroid/os/Looper;)V */
this.mHandler = v1;
/* .line 196 */
com.android.server.am.PreloadAppControllerImpl .getInstance ( );
this.mPreloadAppController = v2;
/* .line 197 */
v3 = this.mActivityManagerService;
(( com.android.server.am.PreloadAppControllerImpl ) v2 ).init ( v3, p0, v0 ); // invoke-virtual {v2, v3, p0, v0}, Lcom/android/server/am/PreloadAppControllerImpl;->init(Lcom/android/server/am/ActivityManagerService;Lcom/android/server/am/ProcessManagerService;Lcom/android/server/ServiceThread;)V
/* .line 198 */
/* new-instance v2, Lcom/android/server/am/ProcessKiller; */
v3 = this.mActivityManagerService;
/* invoke-direct {v2, v3}, Lcom/android/server/am/ProcessKiller;-><init>(Lcom/android/server/am/ActivityManagerService;)V */
this.mProcessKiller = v2;
/* .line 199 */
/* new-instance v2, Lcom/android/server/am/ProcessPolicy; */
v3 = this.mActivityManagerService;
v4 = this.mAccessibilityManager;
/* invoke-direct {v2, p0, v3, v4, v0}, Lcom/android/server/am/ProcessPolicy;-><init>(Lcom/android/server/am/ProcessManagerService;Lcom/android/server/am/ActivityManagerService;Landroid/view/accessibility/AccessibilityManager;Lcom/android/server/ServiceThread;)V */
this.mProcessPolicy = v2;
/* .line 201 */
/* new-instance v0, Lcom/android/server/am/ProcessStarter; */
v2 = this.mActivityManagerService;
/* invoke-direct {v0, p0, v2, v1}, Lcom/android/server/am/ProcessStarter;-><init>(Lcom/android/server/am/ProcessManagerService;Lcom/android/server/am/ActivityManagerService;Landroid/os/Handler;)V */
this.mProcessStarter = v0;
/* .line 203 */
com.android.server.power.stats.BatteryStatsManagerStub .getInstance ( );
v1 = this.mProcessPolicy;
/* .line 205 */
/* new-instance v0, Lcom/android/server/am/MiuiApplicationThreadManager; */
v1 = this.mActivityManagerService;
/* invoke-direct {v0, v1}, Lcom/android/server/am/MiuiApplicationThreadManager;-><init>(Lcom/android/server/am/ActivityManagerService;)V */
this.mMiuiApplicationThreadManager = v0;
/* .line 206 */
/* new-instance v0, Lcom/android/server/wm/ForegroundInfoManager; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/ForegroundInfoManager;-><init>(Lcom/android/server/am/ProcessManagerService;)V */
this.mForegroundInfoManager = v0;
/* .line 207 */
(( com.android.server.am.ProcessManagerService ) p0 ).systemReady ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->systemReady()V
/* .line 209 */
/* new-instance v0, Lcom/android/server/am/ProcessManagerService$LocalService; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/android/server/am/ProcessManagerService$LocalService;-><init>(Lcom/android/server/am/ProcessManagerService;Lcom/android/server/am/ProcessManagerService$LocalService-IA;)V */
this.mInternal = v0;
/* .line 210 */
/* const-class v1, Lcom/miui/server/process/ProcessManagerInternal; */
com.android.server.LocalServices .addService ( v1,v0 );
/* .line 211 */
/* const-class v0, Landroid/hardware/display/DisplayManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Landroid/hardware/display/DisplayManagerInternal; */
this.mDisplayManagerInternal = v0;
/* .line 212 */
/* const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
this.mSmartPowerService = v0;
/* .line 213 */
(( com.android.server.am.ProcessManagerService ) p0 ).probeCgroupVersion ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->probeCgroupVersion()V
/* .line 214 */
return;
} // .end method
private void addConfigToHistory ( miui.process.ProcessConfig p0 ) {
/* .locals 3 */
/* .param p1, "config" # Lmiui/process/ProcessConfig; */
/* .line 1525 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
(( miui.process.ProcessConfig ) p1 ).setKillingClockTime ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Lmiui/process/ProcessConfig;->setKillingClockTime(J)V
/* .line 1526 */
/* iget v0, p0, Lcom/android/server/am/ProcessManagerService;->mHistoryNext:I */
int v1 = 1; // const/4 v1, 0x1
/* const/16 v2, 0x1e */
v0 = /* invoke-direct {p0, v0, v1, v2}, Lcom/android/server/am/ProcessManagerService;->ringAdvance(III)I */
/* iput v0, p0, Lcom/android/server/am/ProcessManagerService;->mHistoryNext:I */
/* .line 1527 */
v1 = this.mProcessConfigHistory;
/* aput-object p1, v1, v0 */
/* .line 1528 */
return;
} // .end method
private void fillRunningProcessInfoList ( java.util.List p0, com.android.server.am.ProcessRecord p1 ) {
/* .locals 2 */
/* .param p2, "app" # Lcom/android/server/am/ProcessRecord; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lmiui/process/RunningProcessInfo;", */
/* ">;", */
/* "Lcom/android/server/am/ProcessRecord;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 1158 */
/* .local p1, "infoList":Ljava/util/List;, "Ljava/util/List<Lmiui/process/RunningProcessInfo;>;" */
/* invoke-direct {p0, p2}, Lcom/android/server/am/ProcessManagerService;->generateRunningProcessInfo(Lcom/android/server/am/ProcessRecord;)Lmiui/process/RunningProcessInfo; */
/* .line 1159 */
/* .local v0, "info":Lmiui/process/RunningProcessInfo; */
v1 = if ( v0 != null) { // if-eqz v0, :cond_0
/* if-nez v1, :cond_0 */
/* .line 1160 */
/* .line 1162 */
} // :cond_0
return;
} // .end method
private miui.process.ActiveUidInfo generateActiveUidInfoLocked ( com.android.server.am.ProcessRecord p0, com.android.server.am.ProcessPolicy$ActiveUidRecord p1 ) {
/* .locals 3 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "activeUidRecord" # Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
/* .line 1250 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1251 */
/* .local v0, "info":Lmiui/process/ActiveUidInfo; */
if ( p1 != null) { // if-eqz p1, :cond_0
(( com.android.server.am.ProcessRecord ) p1 ).getThread ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = this.mErrorState;
/* .line 1252 */
v1 = (( com.android.server.am.ProcessErrorStateRecord ) v1 ).isCrashing ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessErrorStateRecord;->isCrashing()Z
/* if-nez v1, :cond_0 */
v1 = this.mErrorState;
v1 = (( com.android.server.am.ProcessErrorStateRecord ) v1 ).isNotResponding ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessErrorStateRecord;->isNotResponding()Z
/* if-nez v1, :cond_0 */
/* .line 1253 */
/* new-instance v1, Lmiui/process/ActiveUidInfo; */
/* invoke-direct {v1}, Lmiui/process/ActiveUidInfo;-><init>()V */
/* move-object v0, v1 */
/* .line 1254 */
v1 = this.info;
v1 = this.packageName;
this.packageName = v1;
/* .line 1255 */
/* iget v1, p2, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->uid:I */
/* iput v1, v0, Lmiui/process/ActiveUidInfo;->uid:I */
/* .line 1256 */
/* iget v1, p2, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->flag:I */
/* iput v1, v0, Lmiui/process/ActiveUidInfo;->flag:I */
/* .line 1257 */
v1 = this.mState;
v1 = (( com.android.server.am.ProcessStateRecord ) v1 ).getCurAdj ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I
/* iput v1, v0, Lmiui/process/ActiveUidInfo;->curAdj:I */
/* .line 1258 */
v1 = this.mState;
v1 = (( com.android.server.am.ProcessStateRecord ) v1 ).getCurProcState ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessStateRecord;->getCurProcState()I
/* iput v1, v0, Lmiui/process/ActiveUidInfo;->curProcState:I */
/* .line 1259 */
v1 = this.mServices;
v1 = (( com.android.server.am.ProcessServiceRecord ) v1 ).hasForegroundServices ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessServiceRecord;->hasForegroundServices()Z
/* iput-boolean v1, v0, Lmiui/process/ActiveUidInfo;->foregroundServices:Z */
/* .line 1260 */
(( com.android.server.am.ProcessRecord ) p1 ).getUidRecord ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getUidRecord()Lcom/android/server/am/UidRecord;
(( com.android.server.am.UidRecord ) v1 ).getLastBackgroundTime ( ); // invoke-virtual {v1}, Lcom/android/server/am/UidRecord;->getLastBackgroundTime()J
/* move-result-wide v1 */
/* iput-wide v1, v0, Lmiui/process/ActiveUidInfo;->lastBackgroundTime:J */
/* .line 1261 */
(( com.android.server.am.ProcessRecord ) p1 ).getUidRecord ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getUidRecord()Lcom/android/server/am/UidRecord;
v1 = (( com.android.server.am.UidRecord ) v1 ).getNumOfProcs ( ); // invoke-virtual {v1}, Lcom/android/server/am/UidRecord;->getNumOfProcs()I
/* iput v1, v0, Lmiui/process/ActiveUidInfo;->numProcs:I */
/* .line 1262 */
(( com.android.server.am.ProcessRecord ) p1 ).getPackageList ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPackageList()[Ljava/lang/String;
this.pkgList = v1;
/* .line 1264 */
} // :cond_0
} // .end method
private miui.process.RunningProcessInfo generateRunningProcessInfo ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 2 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 1165 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1166 */
/* .local v0, "info":Lmiui/process/RunningProcessInfo; */
if ( p1 != null) { // if-eqz p1, :cond_2
(( com.android.server.am.ProcessRecord ) p1 ).getThread ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
if ( v1 != null) { // if-eqz v1, :cond_2
v1 = this.mErrorState;
v1 = (( com.android.server.am.ProcessErrorStateRecord ) v1 ).isCrashing ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessErrorStateRecord;->isCrashing()Z
/* if-nez v1, :cond_2 */
v1 = this.mErrorState;
v1 = (( com.android.server.am.ProcessErrorStateRecord ) v1 ).isNotResponding ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessErrorStateRecord;->isNotResponding()Z
/* if-nez v1, :cond_2 */
/* .line 1167 */
/* new-instance v1, Lmiui/process/RunningProcessInfo; */
/* invoke-direct {v1}, Lmiui/process/RunningProcessInfo;-><init>()V */
/* move-object v0, v1 */
/* .line 1168 */
v1 = this.processName;
this.mProcessName = v1;
/* .line 1169 */
v1 = (( com.android.server.am.ProcessRecord ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I
/* iput v1, v0, Lmiui/process/RunningProcessInfo;->mPid:I */
/* .line 1170 */
/* iget v1, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
/* iput v1, v0, Lmiui/process/RunningProcessInfo;->mUid:I */
/* .line 1171 */
v1 = this.mState;
v1 = (( com.android.server.am.ProcessStateRecord ) v1 ).getCurAdj ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I
/* iput v1, v0, Lmiui/process/RunningProcessInfo;->mAdj:I */
/* .line 1172 */
v1 = this.mState;
v1 = (( com.android.server.am.ProcessStateRecord ) v1 ).getCurProcState ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessStateRecord;->getCurProcState()I
/* iput v1, v0, Lmiui/process/RunningProcessInfo;->mProcState:I */
/* .line 1173 */
v1 = this.mState;
v1 = (( com.android.server.am.ProcessStateRecord ) v1 ).hasForegroundActivities ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessStateRecord;->hasForegroundActivities()Z
/* iput-boolean v1, v0, Lmiui/process/RunningProcessInfo;->mHasForegroundActivities:Z */
/* .line 1174 */
v1 = this.mServices;
v1 = (( com.android.server.am.ProcessServiceRecord ) v1 ).hasForegroundServices ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessServiceRecord;->hasForegroundServices()Z
/* iput-boolean v1, v0, Lmiui/process/RunningProcessInfo;->mHasForegroundServices:Z */
/* .line 1175 */
(( com.android.server.am.ProcessRecord ) p1 ).getPackageList ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPackageList()[Ljava/lang/String;
this.mPkgList = v1;
/* .line 1176 */
/* iget-boolean v1, v0, Lmiui/process/RunningProcessInfo;->mHasForegroundServices:Z */
/* if-nez v1, :cond_1 */
v1 = this.mServices;
/* .line 1177 */
v1 = (( com.android.server.am.ProcessServiceRecord ) v1 ).getForegroundServiceTypes ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessServiceRecord;->getForegroundServiceTypes()I
/* and-int/lit8 v1, v1, 0x8 */
if ( v1 != null) { // if-eqz v1, :cond_0
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :cond_1
} // :goto_0
int v1 = 1; // const/4 v1, 0x1
} // :goto_1
/* iput-boolean v1, v0, Lmiui/process/RunningProcessInfo;->mLocationForeground:Z */
/* .line 1180 */
} // :cond_2
} // .end method
private java.util.List getAppNotificationWithFlag ( java.lang.String p0, Integer p1, Integer p2 ) {
/* .locals 7 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .param p3, "flags" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "II)", */
/* "Ljava/util/List<", */
/* "Landroid/service/notification/StatusBarNotification;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1184 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 1186 */
/* .local v0, "notificationList":Ljava/util/List;, "Ljava/util/List<Landroid/service/notification/StatusBarNotification;>;" */
try { // :try_start_0
v1 = this.mNotificationManager;
/* .line 1187 */
v2 = android.os.UserHandle .getUserId ( p2 );
/* .line 1188 */
/* .local v1, "notificaionList":Landroid/content/pm/ParceledListSlice;, "Landroid/content/pm/ParceledListSlice<Landroid/service/notification/StatusBarNotification;>;" */
(( android.content.pm.ParceledListSlice ) v1 ).getList ( ); // invoke-virtual {v1}, Landroid/content/pm/ParceledListSlice;->getList()Ljava/util/List;
/* .line 1189 */
/* .local v2, "notifications":Ljava/util/List;, "Ljava/util/List<Landroid/service/notification/StatusBarNotification;>;" */
v3 = if ( v2 != null) { // if-eqz v2, :cond_3
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1192 */
} // :cond_0
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_2
/* check-cast v4, Landroid/service/notification/StatusBarNotification; */
/* .line 1193 */
/* .local v4, "statusBarNotification":Landroid/service/notification/StatusBarNotification; */
if ( v4 != null) { // if-eqz v4, :cond_1
(( android.service.notification.StatusBarNotification ) v4 ).getNotification ( ); // invoke-virtual {v4}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 1194 */
(( android.service.notification.StatusBarNotification ) v4 ).getNotification ( ); // invoke-virtual {v4}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;
/* .line 1195 */
/* .local v5, "notification":Landroid/app/Notification; */
/* iget v6, v5, Landroid/app/Notification;->flags:I */
/* and-int/2addr v6, p3 */
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 1196 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1199 */
} // .end local v4 # "statusBarNotification":Landroid/service/notification/StatusBarNotification;
} // .end local v5 # "notification":Landroid/app/Notification;
} // :cond_1
/* .line 1202 */
} // .end local v1 # "notificaionList":Landroid/content/pm/ParceledListSlice;, "Landroid/content/pm/ParceledListSlice<Landroid/service/notification/StatusBarNotification;>;"
} // .end local v2 # "notifications":Ljava/util/List;, "Ljava/util/List<Landroid/service/notification/StatusBarNotification;>;"
} // :cond_2
/* .line 1190 */
/* .restart local v1 # "notificaionList":Landroid/content/pm/ParceledListSlice;, "Landroid/content/pm/ParceledListSlice<Landroid/service/notification/StatusBarNotification;>;" */
/* .restart local v2 # "notifications":Ljava/util/List;, "Ljava/util/List<Landroid/service/notification/StatusBarNotification;>;" */
} // :cond_3
} // :goto_1
/* .line 1200 */
} // .end local v1 # "notificaionList":Landroid/content/pm/ParceledListSlice;, "Landroid/content/pm/ParceledListSlice<Landroid/service/notification/StatusBarNotification;>;"
} // .end local v2 # "notifications":Ljava/util/List;, "Ljava/util/List<Landroid/service/notification/StatusBarNotification;>;"
/* :catch_0 */
/* move-exception v1 */
/* .line 1203 */
} // :goto_2
} // .end method
private java.lang.String getKillReason ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "policy" # I */
/* .line 429 */
int v0 = 0; // const/4 v0, 0x0
/* .line 430 */
/* .local v0, "reason":Ljava/lang/String; */
/* packed-switch p1, :pswitch_data_0 */
/* .line 480 */
/* :pswitch_0 */
final String v0 = "Unknown"; // const-string v0, "Unknown"
/* .line 477 */
/* :pswitch_1 */
final String v0 = "DisplaySizeChanged"; // const-string v0, "DisplaySizeChanged"
/* .line 478 */
/* .line 474 */
/* :pswitch_2 */
final String v0 = "AutoLockOffCleanByPriority"; // const-string v0, "AutoLockOffCleanByPriority"
/* .line 475 */
/* .line 471 */
/* :pswitch_3 */
final String v0 = "AutoSystemAbnormalClean"; // const-string v0, "AutoSystemAbnormalClean"
/* .line 472 */
/* .line 468 */
/* :pswitch_4 */
final String v0 = "AutoLockOffClean"; // const-string v0, "AutoLockOffClean"
/* .line 469 */
/* .line 465 */
/* :pswitch_5 */
final String v0 = "AutoSleepClean"; // const-string v0, "AutoSleepClean"
/* .line 466 */
/* .line 462 */
/* :pswitch_6 */
final String v0 = "AutoIdleKill"; // const-string v0, "AutoIdleKill"
/* .line 463 */
/* .line 459 */
/* :pswitch_7 */
final String v0 = "AutoThermalKill"; // const-string v0, "AutoThermalKill"
/* .line 460 */
/* .line 456 */
/* :pswitch_8 */
final String v0 = "AutoPowerKill"; // const-string v0, "AutoPowerKill"
/* .line 457 */
/* .line 453 */
/* :pswitch_9 */
final String v0 = "UserDefined"; // const-string v0, "UserDefined"
/* .line 454 */
/* .line 450 */
/* :pswitch_a */
final String v0 = "SwipeUpClean"; // const-string v0, "SwipeUpClean"
/* .line 451 */
/* .line 438 */
/* :pswitch_b */
final String v0 = "GarbageClean"; // const-string v0, "GarbageClean"
/* .line 439 */
/* .line 447 */
/* :pswitch_c */
final String v0 = "OptimizationClean"; // const-string v0, "OptimizationClean"
/* .line 448 */
/* .line 444 */
/* :pswitch_d */
final String v0 = "GameClean"; // const-string v0, "GameClean"
/* .line 445 */
/* .line 441 */
/* :pswitch_e */
final String v0 = "LockScreenClean"; // const-string v0, "LockScreenClean"
/* .line 442 */
/* .line 435 */
/* :pswitch_f */
final String v0 = "ForceClean"; // const-string v0, "ForceClean"
/* .line 436 */
/* .line 432 */
/* :pswitch_10 */
final String v0 = "OneKeyClean"; // const-string v0, "OneKeyClean"
/* .line 433 */
/* nop */
/* .line 482 */
} // :goto_0
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_10 */
/* :pswitch_f */
/* :pswitch_e */
/* :pswitch_d */
/* :pswitch_c */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
private java.lang.String getPackageNameByPid ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "pid" # I */
/* .line 683 */
(( com.android.server.am.ProcessManagerService ) p0 ).getProcessRecordByPid ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;
/* .line 684 */
/* .local v0, "processRecord":Lcom/android/server/am/ProcessRecord; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 685 */
v1 = this.info;
v1 = this.packageName;
/* .line 687 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
private void increaseRecordCount ( java.lang.String p0, java.util.Map p1 ) {
/* .locals 2 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 774 */
/* .local p2, "recordMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;" */
/* check-cast v0, Ljava/lang/Integer; */
/* .line 775 */
/* .local v0, "expCount":Ljava/lang/Integer; */
/* if-nez v0, :cond_0 */
/* .line 776 */
int v1 = 0; // const/4 v1, 0x0
java.lang.Integer .valueOf ( v1 );
/* .line 778 */
} // :cond_0
v1 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* add-int/lit8 v1, v1, 0x1 */
java.lang.Integer .valueOf ( v1 );
/* move-object v0, v1 */
/* .line 779 */
return;
} // .end method
private Boolean isAppHasForegroundServices ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 2 */
/* .param p1, "processRecord" # Lcom/android/server/am/ProcessRecord; */
/* .line 768 */
v0 = this.mActivityManagerService;
/* monitor-enter v0 */
/* .line 769 */
try { // :try_start_0
v1 = this.mServices;
v1 = (( com.android.server.am.ProcessServiceRecord ) v1 ).hasForegroundServices ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessServiceRecord;->hasForegroundServices()Z
/* monitor-exit v0 */
/* .line 770 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private Boolean isPackageInList ( java.lang.String p0, Integer p1 ) {
/* .locals 6 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "flags" # I */
/* .line 640 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 641 */
/* .line 645 */
} // :cond_0
v1 = miui.enterprise.ApplicationHelperStub .getInstance ( );
int v2 = 1; // const/4 v2, 0x1
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 646 */
/* .line 650 */
} // :cond_1
v1 = this.mProcessPolicy;
(( com.android.server.am.ProcessPolicy ) v1 ).getWhiteList ( p2 ); // invoke-virtual {v1, p2}, Lcom/android/server/am/ProcessPolicy;->getWhiteList(I)Ljava/util/List;
/* .line 651 */
/* .local v1, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_3
/* check-cast v4, Ljava/lang/String; */
/* .line 652 */
/* .local v4, "item":Ljava/lang/String; */
v5 = (( java.lang.String ) p1 ).equals ( v4 ); // invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 653 */
/* .line 655 */
} // .end local v4 # "item":Ljava/lang/String;
} // :cond_2
/* .line 656 */
} // :cond_3
} // .end method
private Boolean isSystemApp ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "pid" # I */
/* .line 660 */
(( com.android.server.am.ProcessManagerService ) p0 ).getProcessRecordByPid ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;
/* .line 661 */
/* .local v0, "processRecord":Lcom/android/server/am/ProcessRecord; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 662 */
v1 = /* invoke-direct {p0, v0}, Lcom/android/server/am/ProcessManagerService;->isSystemApp(Lcom/android/server/am/ProcessRecord;)Z */
/* .line 664 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
private Boolean isSystemApp ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 2 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 668 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_1
v1 = this.info;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 669 */
v1 = this.info;
/* iget v1, v1, Landroid/content/pm/ApplicationInfo;->flags:I */
/* and-int/lit16 v1, v1, 0x81 */
if ( v1 != null) { // if-eqz v1, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
/* .line 672 */
} // :cond_1
} // .end method
private Boolean isUidSystem ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 677 */
/* const v0, 0x186a0 */
/* rem-int/2addr p1, v0 */
/* .line 679 */
/* const/16 v0, 0x2710 */
/* if-ge p1, v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private void notifyAmsProcessKill ( com.android.server.am.ProcessRecord p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 1051 */
v0 = (( com.android.server.am.ProcessRecord ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I
java.lang.Integer .valueOf ( v0 );
v1 = this.info;
/* iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I */
java.lang.Integer .valueOf ( v1 );
v2 = this.info;
v2 = this.packageName;
v3 = this.processName;
/* filled-new-array {v0, v1, v2, v3}, [Ljava/lang/Object; */
final String v1 = "notifyProcessDied"; // const-string v1, "notifyProcessDied"
com.android.server.camera.CameraOpt .callMethod ( v1,v0 );
/* .line 1053 */
v0 = this.processName;
/* .line 1054 */
/* .local v0, "processName":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( p2 );
/* if-nez v1, :cond_1 */
v1 = android.text.TextUtils .isEmpty ( v0 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1057 */
} // :cond_0
v1 = this.mPreloadAppController;
(( com.android.server.am.PreloadAppControllerImpl ) v1 ).onProcessKilled ( p2, v0 ); // invoke-virtual {v1, p2, v0}, Lcom/android/server/am/PreloadAppControllerImpl;->onProcessKilled(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1058 */
v1 = this.mProcessStarter;
(( com.android.server.am.ProcessStarter ) v1 ).recordKillProcessIfNeeded ( v0, p2 ); // invoke-virtual {v1, v0, p2}, Lcom/android/server/am/ProcessStarter;->recordKillProcessIfNeeded(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1060 */
/* const-class v1, Lcom/miui/server/migard/MiGardInternal; */
com.android.server.LocalServices .getService ( v1 );
/* check-cast v1, Lcom/miui/server/migard/MiGardInternal; */
/* .line 1061 */
/* .local v1, "migard":Lcom/miui/server/migard/MiGardInternal; */
/* iget v2, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
v3 = (( com.android.server.am.ProcessRecord ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I
v4 = this.info;
v4 = this.packageName;
(( com.miui.server.migard.MiGardInternal ) v1 ).onProcessKilled ( v2, v3, v4, p2 ); // invoke-virtual {v1, v2, v3, v4, p2}, Lcom/miui/server/migard/MiGardInternal;->onProcessKilled(IILjava/lang/String;Ljava/lang/String;)V
/* .line 1062 */
com.android.server.am.MiProcessTracker .getInstance ( );
(( com.android.server.am.MiProcessTracker ) v2 ).recordAmKillProcess ( p1, p2 ); // invoke-virtual {v2, p1, p2}, Lcom/android/server/am/MiProcessTracker;->recordAmKillProcess(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
/* .line 1063 */
return;
/* .line 1055 */
} // .end local v1 # "migard":Lcom/miui/server/migard/MiGardInternal;
} // :cond_1
} // :goto_0
return;
} // .end method
private void notifyProcessDied ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 8 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 1080 */
if ( p1 != null) { // if-eqz p1, :cond_2
v0 = this.info;
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget-boolean v0, p1, Lcom/android/server/am/ProcessRecord;->isolated:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1083 */
} // :cond_0
v0 = this.info;
v0 = this.packageName;
/* .line 1084 */
/* .local v0, "packageName":Ljava/lang/String; */
/* iget v1, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
/* .line 1085 */
/* .local v1, "uid":I */
v2 = this.processName;
/* .line 1086 */
/* .local v2, "processName":Ljava/lang/String; */
com.android.server.am.OomAdjusterImpl .getInstance ( );
(( com.android.server.am.OomAdjusterImpl ) v3 ).notifyProcessDied ( p1 ); // invoke-virtual {v3, p1}, Lcom/android/server/am/OomAdjusterImpl;->notifyProcessDied(Lcom/android/server/am/ProcessRecord;)V
/* .line 1087 */
v3 = this.mProcessStarter;
(( com.android.server.am.ProcessStarter ) v3 ).restartCameraIfNeeded ( v0, v2, v1 ); // invoke-virtual {v3, v0, v2, v1}, Lcom/android/server/am/ProcessStarter;->restartCameraIfNeeded(Ljava/lang/String;Ljava/lang/String;I)V
/* .line 1088 */
v3 = this.mProcessStarter;
(( com.android.server.am.ProcessStarter ) v3 ).recordDiedProcessIfNeeded ( v0, v2, v1 ); // invoke-virtual {v3, v0, v2, v1}, Lcom/android/server/am/ProcessStarter;->recordDiedProcessIfNeeded(Ljava/lang/String;Ljava/lang/String;I)V
/* .line 1089 */
/* const-class v3, Lcom/miui/server/migard/MiGardInternal; */
com.android.server.LocalServices .getService ( v3 );
/* check-cast v3, Lcom/miui/server/migard/MiGardInternal; */
/* .line 1090 */
/* .local v3, "miGardInternal":Lcom/miui/server/migard/MiGardInternal; */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1091 */
v4 = (( com.android.server.am.ProcessRecord ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I
(( com.miui.server.migard.MiGardInternal ) v3 ).notifyProcessDied ( v4 ); // invoke-virtual {v3, v4}, Lcom/miui/server/migard/MiGardInternal;->notifyProcessDied(I)V
/* .line 1093 */
} // :cond_1
/* iget v4, p1, Lcom/android/server/am/ProcessRecord;->mPid:I */
int v5 = 0; // const/4 v5, 0x0
(( com.android.server.am.ProcessManagerService ) p0 ).enableBinderMonitor ( v4, v5 ); // invoke-virtual {p0, v4, v5}, Lcom/android/server/am/ProcessManagerService;->enableBinderMonitor(II)V
/* .line 1095 */
v4 = (( com.android.server.am.ProcessRecord ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I
java.lang.Integer .valueOf ( v4 );
v5 = this.info;
/* iget v5, v5, Landroid/content/pm/ApplicationInfo;->uid:I */
java.lang.Integer .valueOf ( v5 );
v6 = this.info;
v6 = this.packageName;
v7 = this.processName;
/* filled-new-array {v4, v5, v6, v7}, [Ljava/lang/Object; */
final String v5 = "notifyProcessDied"; // const-string v5, "notifyProcessDied"
com.android.server.camera.CameraOpt .callMethod ( v5,v4 );
/* .line 1097 */
return;
/* .line 1081 */
} // .end local v0 # "packageName":Ljava/lang/String;
} // .end local v1 # "uid":I
} // .end local v2 # "processName":Ljava/lang/String;
} // .end local v3 # "miGardInternal":Lcom/miui/server/migard/MiGardInternal;
} // :cond_2
} // :goto_0
return;
} // .end method
private void notifyProcessStarted ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 5 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 1045 */
/* const-class v0, Lcom/miui/server/migard/MiGardInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/server/migard/MiGardInternal; */
/* .line 1046 */
/* .local v0, "migard":Lcom/miui/server/migard/MiGardInternal; */
/* iget v1, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
v2 = (( com.android.server.am.ProcessRecord ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I
v3 = this.info;
v3 = this.packageName;
v4 = this.callerPackage;
(( com.miui.server.migard.MiGardInternal ) v0 ).onProcessStart ( v1, v2, v3, v4 ); // invoke-virtual {v0, v1, v2, v3, v4}, Lcom/miui/server/migard/MiGardInternal;->onProcessStart(IILjava/lang/String;Ljava/lang/String;)V
/* .line 1047 */
return;
} // .end method
private void reduceRecordCountDelay ( java.lang.String p0, java.util.Map p1, Long p2 ) {
/* .locals 2 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .param p3, "delay" # J */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;J)V" */
/* } */
} // .end annotation
/* .line 783 */
/* .local p2, "recordMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;" */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/am/ProcessManagerService$1; */
/* invoke-direct {v1, p0, p2, p1}, Lcom/android/server/am/ProcessManagerService$1;-><init>(Lcom/android/server/am/ProcessManagerService;Ljava/util/Map;Ljava/lang/String;)V */
(( com.android.server.am.ProcessManagerService$MainHandler ) v0 ).postDelayed ( v1, p3, p4 ); // invoke-virtual {v0, v1, p3, p4}, Lcom/android/server/am/ProcessManagerService$MainHandler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 798 */
return;
} // .end method
private final Integer ringAdvance ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "x" # I */
/* .param p2, "increment" # I */
/* .param p3, "ringSize" # I */
/* .line 1518 */
/* add-int/2addr p1, p2 */
/* .line 1519 */
/* if-gez p1, :cond_0 */
/* add-int/lit8 v0, p3, -0x1 */
/* .line 1520 */
} // :cond_0
/* if-lt p1, p3, :cond_1 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1521 */
} // :cond_1
} // .end method
/* # virtual methods */
public void addMiuiApplicationThread ( miui.process.IMiuiApplicationThread p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "applicationThread" # Lmiui/process/IMiuiApplicationThread; */
/* .param p2, "pid" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 995 */
v0 = android.os.Binder .getCallingPid ( );
/* if-ne v0, p2, :cond_0 */
/* .line 1001 */
v0 = this.mMiuiApplicationThreadManager;
(( com.android.server.am.MiuiApplicationThreadManager ) v0 ).addMiuiApplicationThread ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/am/MiuiApplicationThreadManager;->addMiuiApplicationThread(Lmiui/process/IMiuiApplicationThread;I)V
/* .line 1002 */
return;
/* .line 996 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Permission Denial: ProcessManager.addMiuiApplicationThread() from pid="; // const-string v1, "Permission Denial: ProcessManager.addMiuiApplicationThread() from pid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 997 */
v1 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", uid="; // const-string v1, ", uid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 998 */
/* .local v0, "msg":Ljava/lang/String; */
final String v1 = "ProcessManager"; // const-string v1, "ProcessManager"
android.util.Slog .w ( v1,v0 );
/* .line 999 */
/* new-instance v1, Ljava/lang/SecurityException; */
/* invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
public void beginSchedThreads ( Integer[] p0, Long p1, Integer p2, Integer p3 ) {
/* .locals 9 */
/* .param p1, "tids" # [I */
/* .param p2, "duration" # J */
/* .param p4, "pid" # I */
/* .param p5, "mode" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1546 */
v0 = (( com.android.server.am.ProcessManagerService ) p0 ).checkPermission ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z
final String v1 = "ProcessManager"; // const-string v1, "ProcessManager"
/* if-nez v0, :cond_1 */
v0 = (( com.android.server.am.ProcessManagerService ) p0 ).checkSystemSignature ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkSystemSignature()Z
/* if-nez v0, :cond_1 */
int v0 = 3; // const/4 v0, 0x3
/* if-ne p5, v0, :cond_0 */
/* .line 1548 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Permission Denial: ProcessManager.beginSchedThreads() from pid="; // const-string v2, "Permission Denial: ProcessManager.beginSchedThreads() from pid="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1549 */
v2 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", uid="; // const-string v2, ", uid="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1550 */
/* .local v0, "msg":Ljava/lang/String; */
android.util.Slog .d ( v1,v0 );
/* .line 1551 */
/* new-instance v1, Ljava/lang/SecurityException; */
/* invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
/* .line 1553 */
} // .end local v0 # "msg":Ljava/lang/String;
} // :cond_1
} // :goto_0
v0 = this.mSchedBoostService;
/* if-nez v0, :cond_2 */
/* .line 1554 */
/* const-class v0, Lcom/miui/server/rtboost/SchedBoostManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/server/rtboost/SchedBoostManagerInternal; */
this.mSchedBoostService = v0;
/* .line 1556 */
} // :cond_2
v0 = this.mSchedBoostService;
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 1557 */
if ( p1 != null) { // if-eqz p1, :cond_6
/* array-length v0, p1 */
int v2 = 1; // const/4 v2, 0x1
/* if-ge v0, v2, :cond_3 */
/* .line 1560 */
} // :cond_3
/* if-gez p4, :cond_4 */
/* .line 1561 */
p4 = android.os.Binder .getCallingPid ( );
/* .line 1563 */
} // :cond_4
/* invoke-direct {p0, p4}, Lcom/android/server/am/ProcessManagerService;->getPackageNameByPid(I)Ljava/lang/String; */
/* .line 1565 */
/* .local v0, "pkgName":Ljava/lang/String; */
v8 = com.android.server.wm.RealTimeModeControllerStub .get ( );
/* .line 1566 */
/* .local v8, "isEnabled":Z */
/* if-nez v8, :cond_5 */
/* .line 1567 */
final String v2 = "beginSchedThreads is not Enabled"; // const-string v2, "beginSchedThreads is not Enabled"
android.util.Slog .d ( v1,v2 );
/* .line 1568 */
return;
/* .line 1571 */
} // :cond_5
v2 = this.mSchedBoostService;
/* move-object v3, p1 */
/* move-wide v4, p2 */
/* move-object v6, v0 */
/* move v7, p5 */
/* invoke-interface/range {v2 ..v7}, Lcom/miui/server/rtboost/SchedBoostManagerInternal;->beginSchedThreads([IJLjava/lang/String;I)V */
/* .line 1558 */
} // .end local v0 # "pkgName":Ljava/lang/String;
} // .end local v8 # "isEnabled":Z
} // :cond_6
} // :goto_1
return;
/* .line 1573 */
} // :cond_7
} // :goto_2
return;
} // .end method
Boolean checkPermission ( ) {
/* .locals 5 */
/* .line 486 */
v0 = android.os.Binder .getCallingPid ( );
/* .line 487 */
/* .local v0, "callingPid":I */
v1 = android.os.Binder .getCallingUid ( );
/* .line 488 */
/* .local v1, "callingUid":I */
/* const/16 v2, 0x2710 */
int v3 = 1; // const/4 v3, 0x1
/* if-ge v1, v2, :cond_0 */
/* .line 489 */
/* .line 492 */
} // :cond_0
(( com.android.server.am.ProcessManagerService ) p0 ).getProcessRecordByPid ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;
/* .line 493 */
/* .local v2, "app":Lcom/android/server/am/ProcessRecord; */
v4 = /* invoke-direct {p0, v2}, Lcom/android/server/am/ProcessManagerService;->isSystemApp(Lcom/android/server/am/ProcessRecord;)Z */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 494 */
/* .line 497 */
} // :cond_1
int v3 = 0; // const/4 v3, 0x0
} // .end method
Boolean checkSystemSignature ( ) {
/* .locals 2 */
/* .line 501 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 502 */
/* .local v0, "callingUid":I */
v1 = v1 = this.mSmartPowerService;
} // .end method
protected void dump ( java.io.FileDescriptor p0, java.io.PrintWriter p1, java.lang.String[] p2 ) {
/* .locals 6 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .param p3, "args" # [Ljava/lang/String; */
/* .line 1630 */
v0 = (( com.android.server.am.ProcessManagerService ) p0 ).checkPermission ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z
/* if-nez v0, :cond_0 */
/* .line 1631 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Permission Denial: can\'t dump ProcessManager from pid="; // const-string v1, "Permission Denial: can\'t dump ProcessManager from pid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1632 */
v1 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", uid="; // const-string v1, ", uid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1633 */
v1 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1631 */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1634 */
return;
/* .line 1637 */
} // :cond_0
final String v0 = "Process Config:"; // const-string v0, "Process Config:"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1638 */
/* iget v0, p0, Lcom/android/server/am/ProcessManagerService;->mHistoryNext:I */
/* .line 1639 */
/* .local v0, "lastIndex":I */
/* iget v1, p0, Lcom/android/server/am/ProcessManagerService;->mHistoryNext:I */
/* .line 1640 */
/* .local v1, "ringIndex":I */
int v2 = 0; // const/4 v2, 0x0
/* .line 1642 */
/* .local v2, "i":I */
} // :cond_1
int v3 = -1; // const/4 v3, -0x1
/* if-ne v1, v3, :cond_2 */
/* .line 1643 */
} // :cond_2
v4 = this.mProcessConfigHistory;
/* aget-object v4, v4, v1 */
/* .line 1644 */
/* .local v4, "config":Lmiui/process/ProcessConfig; */
/* if-nez v4, :cond_3 */
/* .line 1645 */
} // :cond_3
final String v5 = " #"; // const-string v5, " #"
(( java.io.PrintWriter ) p2 ).print ( v5 ); // invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1646 */
(( java.io.PrintWriter ) p2 ).print ( v2 ); // invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(I)V
/* .line 1647 */
final String v5 = ": "; // const-string v5, ": "
(( java.io.PrintWriter ) p2 ).print ( v5 ); // invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1648 */
(( miui.process.ProcessConfig ) v4 ).toString ( ); // invoke-virtual {v4}, Lmiui/process/ProcessConfig;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v5 ); // invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1649 */
/* const/16 v5, 0x1e */
v1 = /* invoke-direct {p0, v1, v3, v5}, Lcom/android/server/am/ProcessManagerService;->ringAdvance(III)I */
/* .line 1650 */
/* nop */
} // .end local v4 # "config":Lmiui/process/ProcessConfig;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1651 */
/* if-ne v1, v0, :cond_1 */
/* .line 1653 */
} // :goto_0
v3 = this.mForegroundInfoManager;
final String v4 = " "; // const-string v4, " "
(( com.android.server.wm.ForegroundInfoManager ) v3 ).dump ( p2, v4 ); // invoke-virtual {v3, p2, v4}, Lcom/android/server/wm/ForegroundInfoManager;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
/* .line 1654 */
v3 = this.mProcessPolicy;
(( com.android.server.am.ProcessPolicy ) v3 ).dump ( p2, v4 ); // invoke-virtual {v3, p2, v4}, Lcom/android/server/am/ProcessPolicy;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
/* .line 1655 */
return;
} // .end method
public void enableBinderMonitor ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "pid" # I */
/* .param p2, "enable" # I */
/* .line 1066 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/am/ProcessManagerService$6; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/android/server/am/ProcessManagerService$6;-><init>(Lcom/android/server/am/ProcessManagerService;II)V */
(( com.android.server.am.ProcessManagerService$MainHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessManagerService$MainHandler;->post(Ljava/lang/Runnable;)Z
/* .line 1077 */
return;
} // .end method
public void enableHomeSchedBoost ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enable" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1532 */
v0 = (( com.android.server.am.ProcessManagerService ) p0 ).checkPermission ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z
/* if-nez v0, :cond_0 */
/* .line 1533 */
final String v0 = "ProcessManager"; // const-string v0, "ProcessManager"
final String v1 = "Permission Denial: can\'t enable Home Sched Boost"; // const-string v1, "Permission Denial: can\'t enable Home Sched Boost"
android.util.Slog .e ( v0,v1 );
/* .line 1536 */
} // :cond_0
/* const-class v0, Lcom/miui/server/rtboost/SchedBoostManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/server/rtboost/SchedBoostManagerInternal; */
/* .line 1537 */
/* .local v0, "si":Lcom/miui/server/rtboost/SchedBoostManagerInternal; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1538 */
/* .line 1540 */
} // :cond_1
return;
} // .end method
public void foregroundInfoChanged ( java.lang.String p0, android.content.ComponentName p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "foregroundPackageName" # Ljava/lang/String; */
/* .param p2, "component" # Landroid/content/ComponentName; */
/* .param p3, "resultToProc" # Ljava/lang/String; */
/* .line 845 */
v0 = this.mProcessStarter;
(( com.android.server.am.ProcessStarter ) v0 ).foregroundActivityChanged ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessStarter;->foregroundActivityChanged(Ljava/lang/String;)V
/* .line 846 */
com.android.server.am.OomAdjusterImpl .getInstance ( );
(( com.android.server.am.OomAdjusterImpl ) v0 ).foregroundInfoChanged ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/am/OomAdjusterImpl;->foregroundInfoChanged(Ljava/lang/String;Landroid/content/ComponentName;Ljava/lang/String;)V
/* .line 848 */
return;
} // .end method
public Boolean frequentlyKilledForPreload ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1285 */
v0 = this.mProcessStarter;
v0 = (( com.android.server.am.ProcessStarter ) v0 ).frequentlyKilledForPreload ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessStarter;->frequentlyKilledForPreload(Ljava/lang/String;)Z
} // .end method
public java.util.List getActiveUidInfo ( Integer p0 ) {
/* .locals 11 */
/* .param p1, "flag" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Lmiui/process/ActiveUidInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1207 */
v0 = (( com.android.server.am.ProcessManagerService ) p0 ).checkPermission ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 1214 */
v0 = this.mProcessPolicy;
(( com.android.server.am.ProcessPolicy ) v0 ).getActiveUidRecordList ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessPolicy;->getActiveUidRecordList(I)Ljava/util/List;
/* .line 1215 */
/* .local v0, "activeUidRecords":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;>;" */
/* new-instance v1, Landroid/util/SparseArray; */
/* invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V */
/* .line 1217 */
/* .local v1, "activeUidInfos":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lmiui/process/ActiveUidInfo;>;" */
v2 = this.mActivityManagerService;
/* monitor-enter v2 */
/* .line 1218 */
try { // :try_start_0
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_4
/* check-cast v4, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
/* .line 1219 */
/* .local v4, "r":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
/* iget v5, v4, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->uid:I */
(( com.android.server.am.ProcessManagerService ) p0 ).getProcessRecordByUid ( v5 ); // invoke-virtual {p0, v5}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordByUid(I)Ljava/util/List;
/* .line 1220 */
/* .local v5, "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;" */
v7 = } // :goto_1
if ( v7 != null) { // if-eqz v7, :cond_3
/* check-cast v7, Lcom/android/server/am/ProcessRecord; */
/* .line 1222 */
/* .local v7, "app":Lcom/android/server/am/ProcessRecord; */
/* iget v8, v4, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->uid:I */
(( android.util.SparseArray ) v1 ).get ( v8 ); // invoke-virtual {v1, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v8, Lmiui/process/ActiveUidInfo; */
/* move-object v9, v8 */
/* .local v9, "activeUidInfo":Lmiui/process/ActiveUidInfo; */
if ( v8 != null) { // if-eqz v8, :cond_1
/* .line 1224 */
v8 = this.mState;
v8 = (( com.android.server.am.ProcessStateRecord ) v8 ).getCurAdj ( ); // invoke-virtual {v8}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I
/* iget v10, v9, Lmiui/process/ActiveUidInfo;->curAdj:I */
/* if-ge v8, v10, :cond_0 */
/* .line 1225 */
v8 = this.mState;
v8 = (( com.android.server.am.ProcessStateRecord ) v8 ).getCurAdj ( ); // invoke-virtual {v8}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I
/* iput v8, v9, Lmiui/process/ActiveUidInfo;->curAdj:I */
/* .line 1227 */
} // :cond_0
v8 = this.mState;
v8 = (( com.android.server.am.ProcessStateRecord ) v8 ).getCurProcState ( ); // invoke-virtual {v8}, Lcom/android/server/am/ProcessStateRecord;->getCurProcState()I
/* iget v10, v9, Lmiui/process/ActiveUidInfo;->curProcState:I */
/* if-ge v8, v10, :cond_2 */
/* .line 1228 */
v8 = this.mState;
v8 = (( com.android.server.am.ProcessStateRecord ) v8 ).getCurProcState ( ); // invoke-virtual {v8}, Lcom/android/server/am/ProcessStateRecord;->getCurProcState()I
/* iput v8, v9, Lmiui/process/ActiveUidInfo;->curProcState:I */
/* .line 1231 */
} // :cond_1
/* invoke-direct {p0, v7, v4}, Lcom/android/server/am/ProcessManagerService;->generateActiveUidInfoLocked(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;)Lmiui/process/ActiveUidInfo; */
/* .line 1232 */
} // .end local v9 # "activeUidInfo":Lmiui/process/ActiveUidInfo;
/* .local v8, "activeUidInfo":Lmiui/process/ActiveUidInfo; */
if ( v8 != null) { // if-eqz v8, :cond_2
/* .line 1233 */
/* iget v9, v4, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->uid:I */
(( android.util.SparseArray ) v1 ).put ( v9, v8 ); // invoke-virtual {v1, v9, v8}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 1236 */
} // .end local v7 # "app":Lcom/android/server/am/ProcessRecord;
} // .end local v8 # "activeUidInfo":Lmiui/process/ActiveUidInfo;
} // :cond_2
} // :goto_2
/* .line 1237 */
} // .end local v4 # "r":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;
} // .end local v5 # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
} // :cond_3
/* .line 1239 */
} // :cond_4
/* new-instance v3, Ljava/util/ArrayList; */
/* invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V */
/* .line 1241 */
/* .local v3, "activeUidInfoList":Ljava/util/List;, "Ljava/util/List<Lmiui/process/ActiveUidInfo;>;" */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_3
v5 = (( android.util.SparseArray ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/SparseArray;->size()I
/* if-ge v4, v5, :cond_5 */
/* .line 1242 */
(( android.util.SparseArray ) v1 ).valueAt ( v4 ); // invoke-virtual {v1, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v5, Lmiui/process/ActiveUidInfo; */
/* .line 1241 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 1245 */
} // .end local v4 # "i":I
} // :cond_5
/* monitor-exit v2 */
/* .line 1246 */
} // .end local v3 # "activeUidInfoList":Ljava/util/List;, "Ljava/util/List<Lmiui/process/ActiveUidInfo;>;"
/* :catchall_0 */
/* move-exception v3 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v3 */
/* .line 1208 */
} // .end local v0 # "activeUidRecords":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;>;"
} // .end local v1 # "activeUidInfos":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lmiui/process/ActiveUidInfo;>;"
} // :cond_6
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Permission Denial: ProcessManager.getActiveUidInfo() from pid="; // const-string v1, "Permission Denial: ProcessManager.getActiveUidInfo() from pid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1209 */
v1 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", uid="; // const-string v1, ", uid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1210 */
/* .local v0, "msg":Ljava/lang/String; */
final String v1 = "ProcessManager"; // const-string v1, "ProcessManager"
android.util.Slog .w ( v1,v0 );
/* .line 1211 */
/* new-instance v1, Ljava/lang/SecurityException; */
/* invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
public miui.process.IMiuiApplicationThread getForegroundApplicationThread ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1006 */
v0 = (( com.android.server.am.ProcessManagerService ) p0 ).checkPermission ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1012 */
v0 = com.android.server.wm.WindowProcessUtils .getTopRunningPidLocked ( );
/* .line 1013 */
/* .local v0, "pid":I */
v1 = this.mMiuiApplicationThreadManager;
(( com.android.server.am.MiuiApplicationThreadManager ) v1 ).getMiuiApplicationThread ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/am/MiuiApplicationThreadManager;->getMiuiApplicationThread(I)Lmiui/process/IMiuiApplicationThread;
/* .line 1007 */
} // .end local v0 # "pid":I
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Permission Denial: ProcessManager.getForegroundApplicationThread() from pid="; // const-string v1, "Permission Denial: ProcessManager.getForegroundApplicationThread() from pid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1008 */
v1 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", uid="; // const-string v1, ", uid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1009 */
/* .local v0, "msg":Ljava/lang/String; */
final String v1 = "ProcessManager"; // const-string v1, "ProcessManager"
android.util.Slog .w ( v1,v0 );
/* .line 1010 */
/* new-instance v1, Ljava/lang/SecurityException; */
/* invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
public miui.process.ForegroundInfo getForegroundInfo ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 984 */
v0 = (( com.android.server.am.ProcessManagerService ) p0 ).checkPermission ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 990 */
v0 = this.mForegroundInfoManager;
(( com.android.server.wm.ForegroundInfoManager ) v0 ).getForegroundInfo ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ForegroundInfoManager;->getForegroundInfo()Lmiui/process/ForegroundInfo;
/* .line 985 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Permission Denial: ProcessManager.unregisterForegroundInfoListener() from pid="; // const-string v1, "Permission Denial: ProcessManager.unregisterForegroundInfoListener() from pid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 986 */
v1 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", uid="; // const-string v1, ", uid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 987 */
/* .local v0, "msg":Ljava/lang/String; */
final String v1 = "ProcessManager"; // const-string v1, "ProcessManager"
android.util.Slog .w ( v1,v0 );
/* .line 988 */
/* new-instance v1, Ljava/lang/SecurityException; */
/* invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
public com.miui.server.process.ProcessManagerInternal getInternal ( ) {
/* .locals 1 */
/* .line 218 */
v0 = this.mInternal;
} // .end method
protected java.lang.String getKillReason ( miui.process.ProcessConfig p0 ) {
/* .locals 2 */
/* .param p1, "config" # Lmiui/process/ProcessConfig; */
/* .line 420 */
v0 = (( miui.process.ProcessConfig ) p1 ).getPolicy ( ); // invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getPolicy()I
/* .line 421 */
/* .local v0, "policy":I */
/* const/16 v1, 0xa */
/* if-ne v0, v1, :cond_0 */
(( miui.process.ProcessConfig ) p1 ).getReason ( ); // invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getReason()Ljava/lang/String;
v1 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v1, :cond_0 */
/* .line 422 */
(( miui.process.ProcessConfig ) p1 ).getReason ( ); // invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getReason()Ljava/lang/String;
/* .line 424 */
} // :cond_0
/* invoke-direct {p0, v0}, Lcom/android/server/am/ProcessManagerService;->getKillReason(I)Ljava/lang/String; */
} // .end method
public java.util.List getLockedApplication ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 532 */
v0 = this.mProcessPolicy;
(( com.android.server.am.ProcessPolicy ) v0 ).getLockedApplication ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessPolicy;->getLockedApplication(I)Ljava/util/List;
} // .end method
public com.android.server.am.ProcessKiller getProcessKiller ( ) {
/* .locals 1 */
/* .line 270 */
v0 = this.mProcessKiller;
} // .end method
com.android.server.am.ProcessPolicy getProcessPolicy ( ) {
/* .locals 1 */
/* .line 266 */
v0 = this.mProcessPolicy;
} // .end method
public com.android.server.am.ProcessRecord getProcessRecord ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .line 712 */
v0 = this.mActivityManagerService;
/* monitor-enter v0 */
/* .line 713 */
try { // :try_start_0
v1 = this.mLruProcesses;
v1 = (( java.util.ArrayList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
/* add-int/lit8 v1, v1, -0x1 */
/* .local v1, "i":I */
} // :goto_0
/* if-ltz v1, :cond_1 */
/* .line 714 */
v2 = this.mLruProcesses;
(( java.util.ArrayList ) v2 ).get ( v1 ); // invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/am/ProcessRecord; */
/* .line 715 */
/* .local v2, "app":Lcom/android/server/am/ProcessRecord; */
(( com.android.server.am.ProcessRecord ) v2 ).getThread ( ); // invoke-virtual {v2}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
if ( v3 != null) { // if-eqz v3, :cond_0
v3 = this.processName;
v3 = (( java.lang.String ) v3 ).equals ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 716 */
/* monitor-exit v0 */
/* .line 713 */
} // .end local v2 # "app":Lcom/android/server/am/ProcessRecord;
} // :cond_0
/* add-int/lit8 v1, v1, -0x1 */
/* .line 719 */
} // .end local v1 # "i":I
} // :cond_1
/* monitor-exit v0 */
/* .line 720 */
int v0 = 0; // const/4 v0, 0x0
/* .line 719 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public com.android.server.am.ProcessRecord getProcessRecord ( java.lang.String p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 698 */
v0 = this.mActivityManagerService;
/* monitor-enter v0 */
/* .line 699 */
try { // :try_start_0
v1 = this.mLruProcesses;
v1 = (( java.util.ArrayList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
/* add-int/lit8 v1, v1, -0x1 */
/* .local v1, "i":I */
} // :goto_0
/* if-ltz v1, :cond_1 */
/* .line 700 */
v2 = this.mLruProcesses;
(( java.util.ArrayList ) v2 ).get ( v1 ); // invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/am/ProcessRecord; */
/* .line 701 */
/* .local v2, "app":Lcom/android/server/am/ProcessRecord; */
(( com.android.server.am.ProcessRecord ) v2 ).getThread ( ); // invoke-virtual {v2}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
if ( v3 != null) { // if-eqz v3, :cond_0
v3 = this.processName;
v3 = (( java.lang.String ) v3 ).equals ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* iget v3, v2, Lcom/android/server/am/ProcessRecord;->userId:I */
/* if-ne v3, p2, :cond_0 */
/* .line 703 */
/* monitor-exit v0 */
/* .line 699 */
} // .end local v2 # "app":Lcom/android/server/am/ProcessRecord;
} // :cond_0
/* add-int/lit8 v1, v1, -0x1 */
/* .line 706 */
} // .end local v1 # "i":I
} // :cond_1
/* monitor-exit v0 */
/* .line 707 */
int v0 = 0; // const/4 v0, 0x0
/* .line 706 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public com.android.server.am.ProcessRecord getProcessRecordByPid ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "pid" # I */
/* .line 691 */
v0 = this.mActivityManagerService;
v0 = this.mPidsSelfLocked;
/* monitor-enter v0 */
/* .line 692 */
try { // :try_start_0
v1 = this.mActivityManagerService;
v1 = this.mPidsSelfLocked;
(( com.android.server.am.ActivityManagerService$PidMap ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/am/ActivityManagerService$PidMap;->get(I)Lcom/android/server/am/ProcessRecord;
/* monitor-exit v0 */
/* .line 693 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public java.util.List getProcessRecordByUid ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "uid" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/am/ProcessRecord;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 754 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 755 */
/* .local v0, "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;" */
v1 = this.mActivityManagerService;
/* monitor-enter v1 */
/* .line 756 */
try { // :try_start_0
v2 = this.mLruProcesses;
v2 = (( java.util.ArrayList ) v2 ).size ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->size()I
/* add-int/lit8 v2, v2, -0x1 */
/* .local v2, "i":I */
} // :goto_0
/* if-ltz v2, :cond_1 */
/* .line 757 */
v3 = this.mLruProcesses;
(( java.util.ArrayList ) v3 ).get ( v2 ); // invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/am/ProcessRecord; */
/* .line 758 */
/* .local v3, "app":Lcom/android/server/am/ProcessRecord; */
(( com.android.server.am.ProcessRecord ) v3 ).getThread ( ); // invoke-virtual {v3}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
if ( v4 != null) { // if-eqz v4, :cond_0
v4 = this.info;
/* iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I */
/* if-ne v4, p1, :cond_0 */
/* .line 759 */
/* .line 756 */
} // .end local v3 # "app":Lcom/android/server/am/ProcessRecord;
} // :cond_0
/* add-int/lit8 v2, v2, -0x1 */
/* .line 762 */
} // .end local v2 # "i":I
} // :cond_1
/* monitor-exit v1 */
/* .line 763 */
/* .line 762 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public java.util.List getProcessRecordList ( java.lang.String p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "I)", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/am/ProcessRecord;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 725 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 726 */
/* .local v0, "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;" */
v1 = this.mActivityManagerService;
/* monitor-enter v1 */
/* .line 727 */
try { // :try_start_0
v2 = this.mLruProcesses;
v2 = (( java.util.ArrayList ) v2 ).size ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->size()I
/* add-int/lit8 v2, v2, -0x1 */
/* .local v2, "i":I */
} // :goto_0
/* if-ltz v2, :cond_1 */
/* .line 728 */
v3 = this.mLruProcesses;
(( java.util.ArrayList ) v3 ).get ( v2 ); // invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/am/ProcessRecord; */
/* .line 729 */
/* .local v3, "app":Lcom/android/server/am/ProcessRecord; */
(( com.android.server.am.ProcessRecord ) v3 ).getThread ( ); // invoke-virtual {v3}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
if ( v4 != null) { // if-eqz v4, :cond_0
(( com.android.server.am.ProcessRecord ) v3 ).getPkgList ( ); // invoke-virtual {v3}, Lcom/android/server/am/ProcessRecord;->getPkgList()Lcom/android/server/am/PackageList;
v4 = (( com.android.server.am.PackageList ) v4 ).containsKey ( p1 ); // invoke-virtual {v4, p1}, Lcom/android/server/am/PackageList;->containsKey(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_0
/* iget v4, v3, Lcom/android/server/am/ProcessRecord;->userId:I */
/* if-ne v4, p2, :cond_0 */
/* .line 731 */
/* .line 727 */
} // .end local v3 # "app":Lcom/android/server/am/ProcessRecord;
} // :cond_0
/* add-int/lit8 v2, v2, -0x1 */
/* .line 734 */
} // .end local v2 # "i":I
} // :cond_1
/* monitor-exit v1 */
/* .line 735 */
/* .line 734 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public java.util.List getProcessRecordListByPackageAndUid ( java.lang.String p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "I)", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/am/ProcessRecord;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 740 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 741 */
/* .local v0, "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;" */
v1 = this.mActivityManagerService;
/* monitor-enter v1 */
/* .line 742 */
try { // :try_start_0
v2 = this.mLruProcesses;
v2 = (( java.util.ArrayList ) v2 ).size ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->size()I
/* add-int/lit8 v2, v2, -0x1 */
/* .local v2, "i":I */
} // :goto_0
/* if-ltz v2, :cond_1 */
/* .line 743 */
v3 = this.mLruProcesses;
(( java.util.ArrayList ) v3 ).get ( v2 ); // invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/am/ProcessRecord; */
/* .line 744 */
/* .local v3, "app":Lcom/android/server/am/ProcessRecord; */
(( com.android.server.am.ProcessRecord ) v3 ).getThread ( ); // invoke-virtual {v3}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
if ( v4 != null) { // if-eqz v4, :cond_0
(( com.android.server.am.ProcessRecord ) v3 ).getPkgList ( ); // invoke-virtual {v3}, Lcom/android/server/am/ProcessRecord;->getPkgList()Lcom/android/server/am/PackageList;
v4 = (( com.android.server.am.PackageList ) v4 ).containsKey ( p1 ); // invoke-virtual {v4, p1}, Lcom/android/server/am/PackageList;->containsKey(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_0
v4 = this.info;
/* iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I */
/* if-ne v4, p2, :cond_0 */
/* .line 746 */
/* .line 742 */
} // .end local v3 # "app":Lcom/android/server/am/ProcessRecord;
} // :cond_0
/* add-int/lit8 v2, v2, -0x1 */
/* .line 749 */
} // .end local v2 # "i":I
} // :cond_1
/* monitor-exit v1 */
/* .line 750 */
/* .line 749 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public Integer getRenderThreadTidByPid ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "pid" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1606 */
v0 = (( com.android.server.am.ProcessManagerService ) p0 ).checkPermission ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z
final String v1 = "ProcessManager"; // const-string v1, "ProcessManager"
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1614 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "getRenderThreadTidByPid, caller="; // const-string v2, "getRenderThreadTidByPid, caller="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v0 );
/* .line 1617 */
v0 = android.os.Process .myPid ( );
/* if-ne p1, v0, :cond_0 */
/* .line 1618 */
com.android.server.am.ProcessListStubImpl .getInstance ( );
v0 = (( com.android.server.am.ProcessListStubImpl ) v0 ).getSystemRenderThreadTid ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessListStubImpl;->getSystemRenderThreadTid()I
/* .line 1621 */
} // :cond_0
(( com.android.server.am.ProcessManagerService ) p0 ).getProcessRecordByPid ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;
/* .line 1622 */
/* .local v0, "proc":Lcom/android/server/am/ProcessRecord; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1623 */
v1 = (( com.android.server.am.ProcessRecord ) v0 ).getRenderThreadTid ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessRecord;->getRenderThreadTid()I
/* .line 1625 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
/* .line 1607 */
} // .end local v0 # "proc":Lcom/android/server/am/ProcessRecord;
} // :cond_2
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Permission Denial: ProcessManager.updateConfig() from pid="; // const-string v2, "Permission Denial: ProcessManager.updateConfig() from pid="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1608 */
v2 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", uid="; // const-string v2, ", uid="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1609 */
/* .local v0, "msg":Ljava/lang/String; */
android.util.Slog .w ( v1,v0 );
/* .line 1610 */
/* new-instance v1, Ljava/lang/SecurityException; */
/* invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
public java.util.List getRunningProcessInfo ( Integer p0, Integer p1, java.lang.String p2, java.lang.String p3, Integer p4 ) {
/* .locals 5 */
/* .param p1, "pid" # I */
/* .param p2, "uid" # I */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .param p4, "processName" # Ljava/lang/String; */
/* .param p5, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(II", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* "I)", */
/* "Ljava/util/List<", */
/* "Lmiui/process/RunningProcessInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1102 */
v0 = (( com.android.server.am.ProcessManagerService ) p0 ).checkPermission ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 1110 */
/* if-gtz p5, :cond_0 */
/* .line 1111 */
p5 = android.os.UserHandle .getCallingUserId ( );
/* move v0, p5 */
/* .line 1110 */
} // :cond_0
/* move v0, p5 */
/* .line 1114 */
} // .end local p5 # "userId":I
/* .local v0, "userId":I */
} // :goto_0
/* new-instance p5, Ljava/util/ArrayList; */
/* invoke-direct {p5}, Ljava/util/ArrayList;-><init>()V */
/* move-object v1, p5 */
/* .line 1115 */
/* .local v1, "processInfoList":Ljava/util/List;, "Ljava/util/List<Lmiui/process/RunningProcessInfo;>;" */
v2 = this.mActivityManagerService;
/* monitor-enter v2 */
/* .line 1117 */
/* if-lez p1, :cond_1 */
/* .line 1118 */
try { // :try_start_0
(( com.android.server.am.ProcessManagerService ) p0 ).getProcessRecordByPid ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;
/* .line 1119 */
/* .local p5, "app":Lcom/android/server/am/ProcessRecord; */
/* invoke-direct {p0, v1, p5}, Lcom/android/server/am/ProcessManagerService;->fillRunningProcessInfoList(Ljava/util/List;Lcom/android/server/am/ProcessRecord;)V */
/* .line 1120 */
/* monitor-exit v2 */
/* .line 1124 */
} // .end local p5 # "app":Lcom/android/server/am/ProcessRecord;
} // :cond_1
p5 = android.text.TextUtils .isEmpty ( p4 );
/* if-nez p5, :cond_2 */
/* .line 1125 */
(( com.android.server.am.ProcessManagerService ) p0 ).getProcessRecord ( p4, v0 ); // invoke-virtual {p0, p4, v0}, Lcom/android/server/am/ProcessManagerService;->getProcessRecord(Ljava/lang/String;I)Lcom/android/server/am/ProcessRecord;
/* .line 1126 */
/* .restart local p5 # "app":Lcom/android/server/am/ProcessRecord; */
/* invoke-direct {p0, v1, p5}, Lcom/android/server/am/ProcessManagerService;->fillRunningProcessInfoList(Ljava/util/List;Lcom/android/server/am/ProcessRecord;)V */
/* .line 1127 */
/* monitor-exit v2 */
/* .line 1131 */
} // .end local p5 # "app":Lcom/android/server/am/ProcessRecord;
} // :cond_2
p5 = android.text.TextUtils .isEmpty ( p3 );
/* if-nez p5, :cond_4 */
/* if-lez p2, :cond_4 */
/* .line 1132 */
(( com.android.server.am.ProcessManagerService ) p0 ).getProcessRecordListByPackageAndUid ( p3, p2 ); // invoke-virtual {p0, p3, p2}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordListByPackageAndUid(Ljava/lang/String;I)Ljava/util/List;
/* .line 1133 */
/* .local p5, "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;" */
v4 = } // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_3
/* check-cast v4, Lcom/android/server/am/ProcessRecord; */
/* .line 1134 */
/* .local v4, "app":Lcom/android/server/am/ProcessRecord; */
/* invoke-direct {p0, v1, v4}, Lcom/android/server/am/ProcessManagerService;->fillRunningProcessInfoList(Ljava/util/List;Lcom/android/server/am/ProcessRecord;)V */
/* .line 1135 */
} // .end local v4 # "app":Lcom/android/server/am/ProcessRecord;
/* .line 1136 */
} // :cond_3
/* monitor-exit v2 */
/* .line 1139 */
} // .end local p5 # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
} // :cond_4
p5 = android.text.TextUtils .isEmpty ( p3 );
/* if-nez p5, :cond_5 */
/* .line 1140 */
(( com.android.server.am.ProcessManagerService ) p0 ).getProcessRecordList ( p3, v0 ); // invoke-virtual {p0, p3, v0}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordList(Ljava/lang/String;I)Ljava/util/List;
/* .line 1141 */
/* .restart local p5 # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;" */
v4 = } // :goto_2
if ( v4 != null) { // if-eqz v4, :cond_5
/* check-cast v4, Lcom/android/server/am/ProcessRecord; */
/* .line 1142 */
/* .restart local v4 # "app":Lcom/android/server/am/ProcessRecord; */
/* invoke-direct {p0, v1, v4}, Lcom/android/server/am/ProcessManagerService;->fillRunningProcessInfoList(Ljava/util/List;Lcom/android/server/am/ProcessRecord;)V */
/* .line 1143 */
} // .end local v4 # "app":Lcom/android/server/am/ProcessRecord;
/* .line 1146 */
} // .end local p5 # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
} // :cond_5
/* if-lez p2, :cond_6 */
/* .line 1147 */
(( com.android.server.am.ProcessManagerService ) p0 ).getProcessRecordByUid ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordByUid(I)Ljava/util/List;
/* .line 1148 */
/* .restart local p5 # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;" */
v4 = } // :goto_3
if ( v4 != null) { // if-eqz v4, :cond_6
/* check-cast v4, Lcom/android/server/am/ProcessRecord; */
/* .line 1149 */
/* .restart local v4 # "app":Lcom/android/server/am/ProcessRecord; */
/* invoke-direct {p0, v1, v4}, Lcom/android/server/am/ProcessManagerService;->fillRunningProcessInfoList(Ljava/util/List;Lcom/android/server/am/ProcessRecord;)V */
/* .line 1150 */
} // .end local v4 # "app":Lcom/android/server/am/ProcessRecord;
/* .line 1152 */
} // .end local p5 # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
} // :cond_6
/* monitor-exit v2 */
/* .line 1154 */
/* .line 1152 */
/* :catchall_0 */
/* move-exception p5 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw p5 */
/* .line 1103 */
} // .end local v0 # "userId":I
} // .end local v1 # "processInfoList":Ljava/util/List;, "Ljava/util/List<Lmiui/process/RunningProcessInfo;>;"
/* .local p5, "userId":I */
} // :cond_7
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Permission Denial: ProcessManager.getRunningProcessInfo() from pid="; // const-string v1, "Permission Denial: ProcessManager.getRunningProcessInfo() from pid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1104 */
v1 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", uid="; // const-string v1, ", uid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1105 */
/* .local v0, "msg":Ljava/lang/String; */
final String v1 = "ProcessManager"; // const-string v1, "ProcessManager"
android.util.Slog .w ( v1,v0 );
/* .line 1106 */
/* new-instance v1, Ljava/lang/SecurityException; */
/* invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
Boolean isAllowAutoStart ( java.lang.String p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 618 */
v0 = this.mAppOpsManager;
/* const/16 v1, 0x2718 */
v0 = (( android.app.AppOpsManager ) v0 ).checkOpNoThrow ( v1, p2, p1 ); // invoke-virtual {v0, v1, p2, p1}, Landroid/app/AppOpsManager;->checkOpNoThrow(IILjava/lang/String;)I
/* .line 619 */
/* .local v0, "mode":I */
/* if-nez v0, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
Boolean isAppDisabledForPkms ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 626 */
v0 = this.mPkms;
/* .line 627 */
v1 = android.os.UserHandle .myUserId ( );
v0 = (( android.content.pm.PackageManager ) v0 ).isPackageSuspendedForUser ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->isPackageSuspendedForUser(Ljava/lang/String;I)Z
/* .line 628 */
/* .local v0, "isSuspend":Z */
v1 = this.mPkms;
/* .line 629 */
v1 = (( android.content.pm.PackageManager ) v1 ).getApplicationEnabledSetting ( p1 ); // invoke-virtual {v1, p1}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I
/* .line 630 */
/* .local v1, "mode":I */
int v2 = 1; // const/4 v2, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 631 */
/* .line 633 */
} // :cond_0
int v3 = 2; // const/4 v3, 0x2
/* if-eq v1, v3, :cond_2 */
int v3 = 3; // const/4 v3, 0x3
/* if-eq v1, v3, :cond_2 */
int v3 = 4; // const/4 v3, 0x4
/* if-ne v1, v3, :cond_1 */
} // :cond_1
int v2 = 0; // const/4 v2, 0x0
} // :cond_2
} // :goto_0
} // .end method
public Boolean isForceStopEnable ( com.android.server.am.ProcessRecord p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "policy" # I */
/* .line 585 */
/* const/16 v0, 0xd */
int v1 = 1; // const/4 v1, 0x1
/* if-ne p2, v0, :cond_0 */
/* .line 586 */
/* .line 590 */
} // :cond_0
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 591 */
/* .line 595 */
} // :cond_1
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessManagerService;->isSystemApp(Lcom/android/server/am/ProcessRecord;)Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 596 */
/* .line 600 */
} // :cond_2
v0 = this.info;
v0 = this.packageName;
v3 = this.info;
/* iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I */
v0 = (( com.android.server.am.ProcessManagerService ) p0 ).isAllowAutoStart ( v0, v3 ); // invoke-virtual {p0, v0, v3}, Lcom/android/server/am/ProcessManagerService;->isAllowAutoStart(Ljava/lang/String;I)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 601 */
/* .line 605 */
} // :cond_3
v0 = this.info;
v0 = this.packageName;
/* const/16 v3, 0x22 */
v0 = /* invoke-direct {p0, v0, v3}, Lcom/android/server/am/ProcessManagerService;->isPackageInList(Ljava/lang/String;I)Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 607 */
/* .line 610 */
} // :cond_4
} // .end method
Boolean isInWhiteList ( com.android.server.am.ProcessRecord p0, Integer p1, Integer p2 ) {
/* .locals 5 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "userId" # I */
/* .param p3, "policy" # I */
/* .line 341 */
/* iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
/* const/16 v1, 0x3ea */
v1 = android.os.UserHandle .getAppId ( v1 );
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v1, :cond_0 */
/* .line 342 */
/* .line 346 */
} // :cond_0
v0 = this.info;
v0 = this.packageName;
final String v1 = "com.android.cts"; // const-string v1, "com.android.cts"
v0 = (( java.lang.String ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_c */
v0 = this.info;
v0 = this.packageName;
/* .line 347 */
final String v1 = "android.devicepolicy.cts"; // const-string v1, "android.devicepolicy.cts"
v0 = (( java.lang.String ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* goto/16 :goto_2 */
/* .line 351 */
} // :cond_1
v0 = this.mProcessPolicy;
v0 = (( com.android.server.am.ProcessPolicy ) v0 ).getPolicyFlags ( p3 ); // invoke-virtual {v0, p3}, Lcom/android/server/am/ProcessPolicy;->getPolicyFlags(I)I
/* .line 352 */
/* .local v0, "flags":I */
/* sget-boolean v1, Lcom/miui/enterprise/settings/EnterpriseSettings;->ENTERPRISE_ACTIVATED:Z */
/* if-nez v1, :cond_2 */
/* sget-boolean v1, Lmiui/enterprise/EnterpriseManagerStub;->ENTERPRISE_ACTIVATED:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 354 */
} // :cond_2
/* or-int/lit16 v0, v0, 0x1000 */
/* .line 356 */
} // :cond_3
/* packed-switch p3, :pswitch_data_0 */
/* :pswitch_0 */
/* goto/16 :goto_1 */
/* .line 405 */
/* :pswitch_1 */
v1 = this.info;
v1 = this.packageName;
v1 = /* invoke-direct {p0, v1, v0}, Lcom/android/server/am/ProcessManagerService;->isPackageInList(Ljava/lang/String;I)Z */
/* if-nez v1, :cond_4 */
v1 = this.mProcessPolicy;
v3 = this.processName;
/* .line 406 */
v1 = (( com.android.server.am.ProcessPolicy ) v1 ).isInProcessStaticWhiteList ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/am/ProcessPolicy;->isInProcessStaticWhiteList(Ljava/lang/String;)Z
/* if-nez v1, :cond_4 */
v1 = this.mProcessPolicy;
/* .line 407 */
v1 = (( com.android.server.am.ProcessPolicy ) v1 ).isProcessImportant ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/am/ProcessPolicy;->isProcessImportant(Lcom/android/server/am/ProcessRecord;)Z
/* if-nez v1, :cond_4 */
v1 = this.mProcessPolicy;
v3 = this.info;
v3 = this.packageName;
/* .line 408 */
v1 = (( com.android.server.am.ProcessPolicy ) v1 ).isInExtraPackageList ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/am/ProcessPolicy;->isInExtraPackageList(Ljava/lang/String;)Z
/* if-nez v1, :cond_4 */
v1 = this.mProcessPolicy;
v3 = this.processName;
/* .line 409 */
v1 = (( com.android.server.am.ProcessPolicy ) v1 ).isInSystemCleanWhiteList ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/am/ProcessPolicy;->isInSystemCleanWhiteList(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_b
/* .line 410 */
} // :cond_4
/* .line 403 */
/* :pswitch_2 */
v1 = this.mProcessPolicy;
v2 = this.processName;
v1 = (( com.android.server.am.ProcessPolicy ) v1 ).isInDisplaySizeWhiteList ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessPolicy;->isInDisplaySizeWhiteList(Ljava/lang/String;)Z
/* .line 358 */
/* :pswitch_3 */
int v0 = 5; // const/4 v0, 0x5
/* .line 360 */
/* sget-boolean v1, Lcom/miui/enterprise/settings/EnterpriseSettings;->ENTERPRISE_ACTIVATED:Z */
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 361 */
/* or-int/lit16 v0, v0, 0x1000 */
/* .line 364 */
} // :cond_5
v1 = this.info;
v1 = this.packageName;
v1 = /* invoke-direct {p0, v1, v0}, Lcom/android/server/am/ProcessManagerService;->isPackageInList(Ljava/lang/String;I)Z */
/* if-nez v1, :cond_6 */
v1 = this.mProcessPolicy;
v3 = this.processName;
/* .line 365 */
v1 = (( com.android.server.am.ProcessPolicy ) v1 ).isInProcessStaticWhiteList ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/am/ProcessPolicy;->isInProcessStaticWhiteList(Ljava/lang/String;)Z
/* if-nez v1, :cond_6 */
v1 = this.mProcessPolicy;
/* .line 366 */
v1 = (( com.android.server.am.ProcessPolicy ) v1 ).isProcessImportant ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/am/ProcessPolicy;->isProcessImportant(Lcom/android/server/am/ProcessRecord;)Z
/* if-nez v1, :cond_6 */
v1 = this.mProcessPolicy;
v3 = this.info;
v3 = this.packageName;
v4 = this.info;
/* iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 367 */
v1 = (( com.android.server.am.ProcessPolicy ) v1 ).isFastBootEnable ( v3, v4, v2 ); // invoke-virtual {v1, v3, v4, v2}, Lcom/android/server/am/ProcessPolicy;->isFastBootEnable(Ljava/lang/String;IZ)Z
if ( v1 != null) { // if-eqz v1, :cond_b
/* .line 368 */
} // :cond_6
/* .line 380 */
/* :pswitch_4 */
int v0 = 5; // const/4 v0, 0x5
/* .line 382 */
/* sget-boolean v1, Lcom/miui/enterprise/settings/EnterpriseSettings;->ENTERPRISE_ACTIVATED:Z */
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 383 */
/* or-int/lit16 v0, v0, 0x1000 */
/* .line 386 */
} // :cond_7
v1 = this.info;
v1 = this.packageName;
v1 = /* invoke-direct {p0, v1, v0}, Lcom/android/server/am/ProcessManagerService;->isPackageInList(Ljava/lang/String;I)Z */
/* if-nez v1, :cond_a */
v1 = this.mProcessPolicy;
v3 = this.processName;
/* .line 387 */
v1 = (( com.android.server.am.ProcessPolicy ) v1 ).isInProcessStaticWhiteList ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/am/ProcessPolicy;->isInProcessStaticWhiteList(Ljava/lang/String;)Z
/* if-nez v1, :cond_a */
v1 = this.mProcessPolicy;
v3 = this.info;
v3 = this.packageName;
/* .line 388 */
v1 = (( com.android.server.am.ProcessPolicy ) v1 ).isLockedApplication ( v3, p2 ); // invoke-virtual {v1, v3, p2}, Lcom/android/server/am/ProcessPolicy;->isLockedApplication(Ljava/lang/String;I)Z
/* if-nez v1, :cond_a */
v1 = this.mProcessPolicy;
/* .line 389 */
v1 = (( com.android.server.am.ProcessPolicy ) v1 ).isProcessImportant ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/am/ProcessPolicy;->isProcessImportant(Lcom/android/server/am/ProcessRecord;)Z
/* if-nez v1, :cond_a */
v1 = this.mProcessPolicy;
v3 = this.info;
v3 = this.packageName;
v4 = this.info;
/* iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 390 */
v1 = (( com.android.server.am.ProcessPolicy ) v1 ).isFastBootEnable ( v3, v4, v2 ); // invoke-virtual {v1, v3, v4, v2}, Lcom/android/server/am/ProcessPolicy;->isFastBootEnable(Ljava/lang/String;IZ)Z
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 396 */
} // :cond_8
/* :pswitch_5 */
v1 = this.info;
v1 = this.packageName;
v1 = /* invoke-direct {p0, v1, v0}, Lcom/android/server/am/ProcessManagerService;->isPackageInList(Ljava/lang/String;I)Z */
/* if-nez v1, :cond_9 */
v1 = this.mProcessPolicy;
v3 = this.processName;
/* .line 397 */
v1 = (( com.android.server.am.ProcessPolicy ) v1 ).isInProcessStaticWhiteList ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/am/ProcessPolicy;->isInProcessStaticWhiteList(Ljava/lang/String;)Z
/* if-nez v1, :cond_9 */
v1 = this.mProcessPolicy;
/* .line 398 */
v1 = (( com.android.server.am.ProcessPolicy ) v1 ).isProcessImportant ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/am/ProcessPolicy;->isProcessImportant(Lcom/android/server/am/ProcessRecord;)Z
if ( v1 != null) { // if-eqz v1, :cond_b
/* .line 399 */
} // :cond_9
/* .line 391 */
} // :cond_a
} // :goto_0
/* .line 416 */
} // :cond_b
} // :goto_1
int v1 = 0; // const/4 v1, 0x0
/* .line 348 */
} // .end local v0 # "flags":I
} // :cond_c
} // :goto_2
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_4 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_4 */
/* :pswitch_4 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_4 */
/* :pswitch_0 */
/* :pswitch_4 */
/* :pswitch_0 */
/* :pswitch_2 */
/* :pswitch_5 */
/* :pswitch_5 */
/* :pswitch_1 */
/* :pswitch_4 */
} // .end packed-switch
} // .end method
public Boolean isLockedApplication ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 544 */
v0 = this.mProcessPolicy;
v0 = (( com.android.server.am.ProcessPolicy ) v0 ).isLockedApplication ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/am/ProcessPolicy;->isLockedApplication(Ljava/lang/String;I)Z
} // .end method
public Boolean isTrimMemoryEnable ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 614 */
/* const/16 v0, 0x10 */
v0 = /* invoke-direct {p0, p1, v0}, Lcom/android/server/am/ProcessManagerService;->isPackageInList(Ljava/lang/String;I)Z */
/* xor-int/lit8 v0, v0, 0x1 */
} // .end method
public Boolean kill ( miui.process.ProcessConfig p0 ) {
/* .locals 3 */
/* .param p1, "config" # Lmiui/process/ProcessConfig; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 281 */
v0 = (( com.android.server.am.ProcessManagerService ) p0 ).checkPermission ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z
/* if-nez v0, :cond_0 */
/* .line 282 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Permission Denial: ProcessManager.kill() from pid="; // const-string v1, "Permission Denial: ProcessManager.kill() from pid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 283 */
v1 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", uid="; // const-string v1, ", uid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 284 */
/* .local v0, "msg":Ljava/lang/String; */
final String v1 = "ProcessManager"; // const-string v1, "ProcessManager"
android.util.Slog .w ( v1,v0 );
/* .line 285 */
int v1 = 0; // const/4 v1, 0x0
/* .line 288 */
} // .end local v0 # "msg":Ljava/lang/String;
} // :cond_0
/* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessManagerService;->addConfigToHistory(Lmiui/process/ProcessConfig;)V */
/* .line 289 */
v0 = this.mProcessPolicy;
v1 = this.mContext;
v2 = android.os.UserHandle .getCallingUserId ( );
(( com.android.server.am.ProcessPolicy ) v0 ).resetWhiteList ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/am/ProcessPolicy;->resetWhiteList(Landroid/content/Context;I)V
/* .line 290 */
com.android.server.am.SystemPressureController .getInstance ( );
v0 = (( com.android.server.am.SystemPressureController ) v0 ).kill ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/SystemPressureController;->kill(Lmiui/process/ProcessConfig;)Z
/* .line 291 */
/* .local v0, "success":Z */
com.android.server.am.ProcessRecordStub .get ( );
/* .line 292 */
} // .end method
public Boolean killAllBackgroundExceptLocked ( miui.process.ProcessConfig p0 ) {
/* .locals 13 */
/* .param p1, "config" # Lmiui/process/ProcessConfig; */
/* .line 296 */
v0 = (( miui.process.ProcessConfig ) p1 ).isPriorityInvalid ( ); // invoke-virtual {p1}, Lmiui/process/ProcessConfig;->isPriorityInvalid()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 297 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "priority:"; // const-string v1, "priority:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = (( miui.process.ProcessConfig ) p1 ).getPriority ( ); // invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getPriority()I
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " is invalid"; // const-string v1, " is invalid"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 298 */
/* .local v0, "msg":Ljava/lang/String; */
final String v1 = "ProcessManager"; // const-string v1, "ProcessManager"
android.util.Slog .w ( v1,v0 );
/* .line 299 */
int v1 = 0; // const/4 v1, 0x0
/* .line 301 */
} // .end local v0 # "msg":Ljava/lang/String;
} // :cond_0
v0 = (( miui.process.ProcessConfig ) p1 ).getPriority ( ); // invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getPriority()I
/* .line 302 */
/* .local v0, "maxProcState":I */
(( miui.process.ProcessConfig ) p1 ).getReason ( ); // invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getReason()Ljava/lang/String;
v1 = android.text.TextUtils .isEmpty ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 303 */
v1 = (( miui.process.ProcessConfig ) p1 ).getPolicy ( ); // invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getPolicy()I
/* invoke-direct {p0, v1}, Lcom/android/server/am/ProcessManagerService;->getKillReason(I)Ljava/lang/String; */
} // :cond_1
(( miui.process.ProcessConfig ) p1 ).getReason ( ); // invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getReason()Ljava/lang/String;
/* .line 304 */
/* .local v1, "killReason":Ljava/lang/String; */
} // :goto_0
v2 = this.mActivityManagerService;
/* monitor-enter v2 */
/* .line 305 */
try { // :try_start_0
/* new-instance v3, Ljava/util/ArrayList; */
/* invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V */
/* .line 306 */
/* .local v3, "procs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;" */
v4 = this.mActivityManagerService;
v4 = this.mProcessList;
/* .line 307 */
/* .local v4, "processList":Lcom/android/server/am/ProcessList; */
(( com.android.server.am.ProcessList ) v4 ).getProcessNamesLOSP ( ); // invoke-virtual {v4}, Lcom/android/server/am/ProcessList;->getProcessNamesLOSP()Lcom/android/server/am/ProcessList$MyProcessMap;
(( com.android.server.am.ProcessList$MyProcessMap ) v5 ).getMap ( ); // invoke-virtual {v5}, Lcom/android/server/am/ProcessList$MyProcessMap;->getMap()Landroid/util/ArrayMap;
v5 = (( android.util.ArrayMap ) v5 ).size ( ); // invoke-virtual {v5}, Landroid/util/ArrayMap;->size()I
/* .line 308 */
/* .local v5, "NP":I */
int v6 = 0; // const/4 v6, 0x0
/* .local v6, "ip":I */
} // :goto_1
/* if-ge v6, v5, :cond_7 */
/* .line 309 */
(( com.android.server.am.ProcessList ) v4 ).getProcessNamesLOSP ( ); // invoke-virtual {v4}, Lcom/android/server/am/ProcessList;->getProcessNamesLOSP()Lcom/android/server/am/ProcessList$MyProcessMap;
(( com.android.server.am.ProcessList$MyProcessMap ) v7 ).getMap ( ); // invoke-virtual {v7}, Lcom/android/server/am/ProcessList$MyProcessMap;->getMap()Landroid/util/ArrayMap;
(( android.util.ArrayMap ) v7 ).valueAt ( v6 ); // invoke-virtual {v7, v6}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;
/* check-cast v7, Landroid/util/SparseArray; */
/* .line 310 */
/* .local v7, "apps":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/am/ProcessRecord;>;" */
v8 = (( android.util.SparseArray ) v7 ).size ( ); // invoke-virtual {v7}, Landroid/util/SparseArray;->size()I
/* .line 311 */
/* .local v8, "NA":I */
int v9 = 0; // const/4 v9, 0x0
/* .local v9, "ia":I */
} // :goto_2
/* if-ge v9, v8, :cond_6 */
/* .line 312 */
(( android.util.SparseArray ) v7 ).valueAt ( v9 ); // invoke-virtual {v7, v9}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v10, Lcom/android/server/am/ProcessRecord; */
/* .line 313 */
/* .local v10, "app":Lcom/android/server/am/ProcessRecord; */
v11 = (( com.android.server.am.ProcessRecord ) v10 ).isRemoved ( ); // invoke-virtual {v10}, Lcom/android/server/am/ProcessRecord;->isRemoved()Z
/* if-nez v11, :cond_4 */
/* if-ltz v0, :cond_2 */
v11 = this.mState;
/* .line 314 */
v11 = (( com.android.server.am.ProcessStateRecord ) v11 ).getSetProcState ( ); // invoke-virtual {v11}, Lcom/android/server/am/ProcessStateRecord;->getSetProcState()I
/* if-le v11, v0, :cond_3 */
} // :cond_2
v11 = this.mState;
/* .line 315 */
v11 = (( com.android.server.am.ProcessStateRecord ) v11 ).hasShownUi ( ); // invoke-virtual {v11}, Lcom/android/server/am/ProcessStateRecord;->hasShownUi()Z
/* if-nez v11, :cond_4 */
/* .line 316 */
} // :cond_3
com.android.server.display.DisplayManagerServiceStub .getInstance ( );
v12 = this.processName;
/* .line 317 */
v11 = (( com.android.server.display.DisplayManagerServiceStub ) v11 ).isInResolutionSwitchBlackList ( v12 ); // invoke-virtual {v11, v12}, Lcom/android/server/display/DisplayManagerServiceStub;->isInResolutionSwitchBlackList(Ljava/lang/String;)Z
if ( v11 != null) { // if-eqz v11, :cond_5
/* .line 318 */
} // :cond_4
(( java.util.ArrayList ) v3 ).add ( v10 ); // invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 311 */
} // .end local v10 # "app":Lcom/android/server/am/ProcessRecord;
} // :cond_5
/* add-int/lit8 v9, v9, 0x1 */
/* .line 308 */
} // .end local v7 # "apps":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/am/ProcessRecord;>;"
} // .end local v8 # "NA":I
} // .end local v9 # "ia":I
} // :cond_6
/* add-int/lit8 v6, v6, 0x1 */
/* .line 322 */
} // .end local v6 # "ip":I
} // :cond_7
v6 = (( java.util.ArrayList ) v3 ).size ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
/* .line 323 */
/* .local v6, "N":I */
int v7 = 0; // const/4 v7, 0x0
/* .local v7, "i":I */
} // :goto_3
int v8 = 1; // const/4 v8, 0x1
/* if-ge v7, v6, :cond_9 */
/* .line 324 */
(( java.util.ArrayList ) v3 ).get ( v7 ); // invoke-virtual {v3, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v9, Lcom/android/server/am/ProcessRecord; */
/* .line 328 */
/* .local v9, "app":Lcom/android/server/am/ProcessRecord; */
com.android.server.display.DisplayManagerServiceStub .getInstance ( );
v11 = this.processName;
/* .line 329 */
v10 = (( com.android.server.display.DisplayManagerServiceStub ) v10 ).isInResolutionSwitchProtectList ( v11 ); // invoke-virtual {v10, v11}, Lcom/android/server/display/DisplayManagerServiceStub;->isInResolutionSwitchProtectList(Ljava/lang/String;)Z
if ( v10 != null) { // if-eqz v10, :cond_8
/* .line 330 */
/* .line 332 */
} // :cond_8
v10 = this.mProcessKiller;
(( com.android.server.am.ProcessKiller ) v10 ).forceStopPackage ( v9, v1, v8 ); // invoke-virtual {v10, v9, v1, v8}, Lcom/android/server/am/ProcessKiller;->forceStopPackage(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Z)V
/* .line 323 */
} // .end local v9 # "app":Lcom/android/server/am/ProcessRecord;
} // :goto_4
/* add-int/lit8 v7, v7, 0x1 */
/* .line 334 */
} // .end local v3 # "procs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;"
} // .end local v4 # "processList":Lcom/android/server/am/ProcessList;
} // .end local v5 # "NP":I
} // .end local v6 # "N":I
} // .end local v7 # "i":I
} // :cond_9
/* monitor-exit v2 */
/* .line 335 */
/* .line 334 */
/* :catchall_0 */
/* move-exception v3 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v3 */
} // .end method
public Integer killPreloadApp ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1281 */
v0 = this.mPreloadAppController;
v0 = (( com.android.server.am.PreloadAppControllerImpl ) v0 ).killPreloadApp ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/PreloadAppControllerImpl;->killPreloadApp(Ljava/lang/String;)I
} // .end method
public void notifyActivityChanged ( android.content.ComponentName p0 ) {
/* .locals 2 */
/* .param p1, "curActivityComponent" # Landroid/content/ComponentName; */
/* .line 1035 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/am/ProcessManagerService$5; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/am/ProcessManagerService$5;-><init>(Lcom/android/server/am/ProcessManagerService;Landroid/content/ComponentName;)V */
(( com.android.server.am.ProcessManagerService$MainHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessManagerService$MainHandler;->post(Ljava/lang/Runnable;)Z
/* .line 1042 */
return;
} // .end method
public void notifyBluetoothEvent ( Boolean p0, Integer p1, Integer p2, Integer p3, java.lang.String p4, Integer p5 ) {
/* .locals 7 */
/* .param p1, "isConnect" # Z */
/* .param p2, "bleType" # I */
/* .param p3, "uid" # I */
/* .param p4, "pid" # I */
/* .param p5, "pkg" # Ljava/lang/String; */
/* .param p6, "data" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1595 */
v0 = this.mSmartPowerService;
/* move v1, p1 */
/* move v2, p2 */
/* move v3, p3 */
/* move v4, p4 */
/* move-object v5, p5 */
/* move v6, p6 */
/* invoke-interface/range {v0 ..v6}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->onBluetoothEvent(ZIIILjava/lang/String;I)V */
/* .line 1596 */
return;
} // .end method
public void notifyForegroundInfoChanged ( com.android.server.wm.FgActivityChangedInfo p0 ) {
/* .locals 2 */
/* .param p1, "fgInfo" # Lcom/android/server/wm/FgActivityChangedInfo; */
/* .line 1017 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/am/ProcessManagerService$3; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/am/ProcessManagerService$3;-><init>(Lcom/android/server/am/ProcessManagerService;Lcom/android/server/wm/FgActivityChangedInfo;)V */
(( com.android.server.am.ProcessManagerService$MainHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessManagerService$MainHandler;->post(Ljava/lang/Runnable;)Z
/* .line 1023 */
return;
} // .end method
public void notifyForegroundWindowChanged ( com.android.server.wm.FgWindowChangedInfo p0 ) {
/* .locals 2 */
/* .param p1, "fgInfo" # Lcom/android/server/wm/FgWindowChangedInfo; */
/* .line 1026 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/am/ProcessManagerService$4; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/am/ProcessManagerService$4;-><init>(Lcom/android/server/am/ProcessManagerService;Lcom/android/server/wm/FgWindowChangedInfo;)V */
(( com.android.server.am.ProcessManagerService$MainHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessManagerService$MainHandler;->post(Ljava/lang/Runnable;)Z
/* .line 1032 */
return;
} // .end method
public void onShellCommand ( java.io.FileDescriptor p0, java.io.FileDescriptor p1, java.io.FileDescriptor p2, java.lang.String[] p3, android.os.ShellCallback p4, android.os.ResultReceiver p5 ) {
/* .locals 8 */
/* .param p1, "in" # Ljava/io/FileDescriptor; */
/* .param p2, "out" # Ljava/io/FileDescriptor; */
/* .param p3, "err" # Ljava/io/FileDescriptor; */
/* .param p4, "args" # [Ljava/lang/String; */
/* .param p5, "callback" # Landroid/os/ShellCallback; */
/* .param p6, "resultReceiver" # Landroid/os/ResultReceiver; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1292 */
/* new-instance v0, Lcom/android/server/am/ProcessManagerService$ProcessMangaerShellCommand; */
/* invoke-direct {v0, p0}, Lcom/android/server/am/ProcessManagerService$ProcessMangaerShellCommand;-><init>(Lcom/android/server/am/ProcessManagerService;)V */
/* .line 1293 */
/* move-object v1, p0 */
/* move-object v2, p1 */
/* move-object v3, p2 */
/* move-object v4, p3 */
/* move-object v5, p4 */
/* move-object v6, p5 */
/* move-object v7, p6 */
/* invoke-virtual/range {v0 ..v7}, Lcom/android/server/am/ProcessManagerService$ProcessMangaerShellCommand;->exec(Landroid/os/Binder;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)I */
/* .line 1294 */
return;
} // .end method
public Boolean protectCurrentProcess ( Boolean p0, Integer p1 ) {
/* .locals 6 */
/* .param p1, "isProtected" # Z */
/* .param p2, "timeout" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 852 */
v0 = android.os.Binder .getCallingPid ( );
(( com.android.server.am.ProcessManagerService ) p0 ).getProcessRecordByPid ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;
/* .line 853 */
/* .local v0, "app":Lcom/android/server/am/ProcessRecord; */
if ( v0 != null) { // if-eqz v0, :cond_1
v1 = this.mProcessPolicy;
v2 = this.info;
v2 = this.packageName;
v1 = (( com.android.server.am.ProcessPolicy ) v1 ).isInAppProtectList ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessPolicy;->isInAppProtectList(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 860 */
v1 = this.mProcessPolicy;
v1 = (( com.android.server.am.ProcessPolicy ) v1 ).protectCurrentProcess ( v0, p1 ); // invoke-virtual {v1, v0, p1}, Lcom/android/server/am/ProcessPolicy;->protectCurrentProcess(Lcom/android/server/am/ProcessRecord;Z)Z
/* .line 861 */
/* .local v1, "success":Z */
if ( p1 != null) { // if-eqz p1, :cond_0
/* if-lez p2, :cond_0 */
/* .line 862 */
v2 = this.mHandler;
/* new-instance v3, Lcom/android/server/am/ProcessManagerService$2; */
/* invoke-direct {v3, p0, v0}, Lcom/android/server/am/ProcessManagerService$2;-><init>(Lcom/android/server/am/ProcessManagerService;Lcom/android/server/am/ProcessRecord;)V */
/* int-to-long v4, p2 */
(( com.android.server.am.ProcessManagerService$MainHandler ) v2 ).postDelayed ( v3, v4, v5 ); // invoke-virtual {v2, v3, v4, v5}, Lcom/android/server/am/ProcessManagerService$MainHandler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 870 */
} // :cond_0
/* .line 854 */
} // .end local v1 # "success":Z
} // :cond_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Permission Denial: ProcessManager.protectCurrentProcess() from pid="; // const-string v2, "Permission Denial: ProcessManager.protectCurrentProcess() from pid="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 855 */
v2 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", uid="; // const-string v2, ", uid="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 856 */
/* .local v1, "msg":Ljava/lang/String; */
final String v2 = "ProcessManager"; // const-string v2, "ProcessManager"
android.util.Slog .w ( v2,v1 );
/* .line 857 */
/* new-instance v2, Ljava/lang/SecurityException; */
/* invoke-direct {v2, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v2 */
} // .end method
public void registerActivityChangeListener ( java.util.List p0, java.util.List p1, miui.process.IActivityChangeListener p2 ) {
/* .locals 2 */
/* .param p3, "listener" # Lmiui/process/IActivityChangeListener; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Lmiui/process/IActivityChangeListener;", */
/* ")V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 961 */
/* .local p1, "targetPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* .local p2, "targetActivities":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = (( com.android.server.am.ProcessManagerService ) p0 ).checkPermission ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 967 */
v0 = this.mForegroundInfoManager;
/* .line 968 */
(( com.android.server.wm.ForegroundInfoManager ) v0 ).registerActivityChangeListener ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/wm/ForegroundInfoManager;->registerActivityChangeListener(Ljava/util/List;Ljava/util/List;Lmiui/process/IActivityChangeListener;)V
/* .line 969 */
return;
/* .line 962 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Permission Denial: ProcessManager.registerActivityChangeListener() from pid="; // const-string v1, "Permission Denial: ProcessManager.registerActivityChangeListener() from pid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 963 */
v1 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", uid="; // const-string v1, ", uid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 964 */
/* .local v0, "msg":Ljava/lang/String; */
final String v1 = "ProcessManager"; // const-string v1, "ProcessManager"
android.util.Slog .w ( v1,v0 );
/* .line 965 */
/* new-instance v1, Ljava/lang/SecurityException; */
/* invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
public void registerForegroundInfoListener ( miui.process.IForegroundInfoListener p0 ) {
/* .locals 3 */
/* .param p1, "listener" # Lmiui/process/IForegroundInfoListener; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 905 */
v0 = (( com.android.server.am.ProcessManagerService ) p0 ).checkPermission ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z
final String v1 = "ProcessManager"; // const-string v1, "ProcessManager"
/* if-nez v0, :cond_1 */
v0 = (( com.android.server.am.ProcessManagerService ) p0 ).checkSystemSignature ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkSystemSignature()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 906 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Permission Denial: ProcessManager.registerForegroundInfoListener() from pid="; // const-string v2, "Permission Denial: ProcessManager.registerForegroundInfoListener() from pid="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 907 */
v2 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", uid="; // const-string v2, ", uid="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 908 */
/* .local v0, "msg":Ljava/lang/String; */
android.util.Slog .w ( v1,v0 );
/* .line 909 */
/* new-instance v1, Ljava/lang/SecurityException; */
/* invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
/* .line 912 */
} // .end local v0 # "msg":Ljava/lang/String;
} // :cond_1
} // :goto_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "registerForegroundInfoListener, caller="; // const-string v2, "registerForegroundInfoListener, caller="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", listener="; // const-string v2, ", listener="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v1,v0 );
/* .line 915 */
v0 = this.mForegroundInfoManager;
(( com.android.server.wm.ForegroundInfoManager ) v0 ).registerForegroundInfoListener ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/ForegroundInfoManager;->registerForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V
/* .line 916 */
return;
} // .end method
public void registerForegroundWindowListener ( miui.process.IForegroundWindowListener p0 ) {
/* .locals 3 */
/* .param p1, "listener" # Lmiui/process/IForegroundWindowListener; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 933 */
v0 = (( com.android.server.am.ProcessManagerService ) p0 ).checkPermission ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z
final String v1 = "ProcessManager"; // const-string v1, "ProcessManager"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 940 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "registerForegroundWindowListener, caller="; // const-string v2, "registerForegroundWindowListener, caller="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", listener="; // const-string v2, ", listener="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v1,v0 );
/* .line 943 */
v0 = this.mForegroundInfoManager;
(( com.android.server.wm.ForegroundInfoManager ) v0 ).registerForegroundWindowListener ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/ForegroundInfoManager;->registerForegroundWindowListener(Lmiui/process/IForegroundWindowListener;)V
/* .line 944 */
return;
/* .line 934 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Permission Denial: ProcessManager.registerForegroundWindowListener() from pid="; // const-string v2, "Permission Denial: ProcessManager.registerForegroundWindowListener() from pid="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 935 */
v2 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", uid="; // const-string v2, ", uid="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 936 */
/* .local v0, "msg":Ljava/lang/String; */
android.util.Slog .w ( v1,v0 );
/* .line 937 */
/* new-instance v1, Ljava/lang/SecurityException; */
/* invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
public void registerPreloadCallback ( miui.process.IPreloadCallback p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "callback" # Lmiui/process/IPreloadCallback; */
/* .param p2, "type" # I */
/* .line 1269 */
v0 = this.mPreloadAppController;
(( com.android.server.am.PreloadAppControllerImpl ) v0 ).registerPreloadCallback ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/am/PreloadAppControllerImpl;->registerPreloadCallback(Lmiui/process/IPreloadCallback;I)V
/* .line 1270 */
return;
} // .end method
public void reportGameScene ( java.lang.String p0, Integer p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "gameScene" # I */
/* .param p3, "appState" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1578 */
v0 = (( com.android.server.am.ProcessManagerService ) p0 ).checkPermission ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z
final String v1 = "ProcessManager"; // const-string v1, "ProcessManager"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1585 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "pms : packageName = "; // const-string v2, "pms : packageName = "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " gameScene ="; // const-string v2, " gameScene ="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " appState = "; // const-string v2, " appState = "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v0 );
/* .line 1588 */
com.android.server.am.OomAdjusterImpl .getInstance ( );
/* .line 1589 */
(( com.android.server.am.OomAdjusterImpl ) v0 ).updateGameSceneRecordMap ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/am/OomAdjusterImpl;->updateGameSceneRecordMap(Ljava/lang/String;II)V
/* .line 1590 */
return;
/* .line 1579 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Permission Denial: ProcessManager.reportGameScene() from pid="; // const-string v2, "Permission Denial: ProcessManager.reportGameScene() from pid="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1580 */
v2 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", uid="; // const-string v2, ", uid="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1581 */
/* .local v0, "msg":Ljava/lang/String; */
android.util.Slog .w ( v1,v0 );
/* .line 1582 */
/* new-instance v1, Ljava/lang/SecurityException; */
/* invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
public void reportTrackStatus ( Integer p0, Integer p1, Integer p2, Boolean p3 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "sessionId" # I */
/* .param p4, "isMuted" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1601 */
v0 = this.mSmartPowerService;
/* .line 1602 */
return;
} // .end method
public void setProcessMaxAdjLock ( Integer p0, com.android.server.am.ProcessRecord p1, Integer p2, Integer p3 ) {
/* .locals 2 */
/* .param p1, "userId" # I */
/* .param p2, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p3, "maxAdj" # I */
/* .param p4, "maxProcState" # I */
/* .line 876 */
/* if-nez p2, :cond_0 */
/* .line 877 */
return;
/* .line 879 */
} // :cond_0
/* if-le p3, v0, :cond_1 */
v0 = this.mProcessPolicy;
v1 = this.info;
v1 = this.packageName;
/* .line 880 */
v0 = (( com.android.server.am.ProcessPolicy ) v0 ).isLockedApplication ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Lcom/android/server/am/ProcessPolicy;->isLockedApplication(Ljava/lang/String;I)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 881 */
/* .line 882 */
/* .line 884 */
} // :cond_1
v0 = this.mState;
(( com.android.server.am.ProcessStateRecord ) v0 ).setMaxAdj ( p3 ); // invoke-virtual {v0, p3}, Lcom/android/server/am/ProcessStateRecord;->setMaxAdj(I)V
/* .line 885 */
com.android.server.am.IProcessPolicy .setAppMaxProcState ( p2,p4 );
/* .line 886 */
return;
} // .end method
public void shutdown ( ) {
/* .locals 0 */
/* .line 263 */
return;
} // .end method
public Boolean skipCurrentProcessInBackup ( com.android.server.am.ProcessRecord p0, java.lang.String p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "userId" # I */
/* .line 556 */
v0 = this.mContext;
miui.app.backup.BackupManager .getBackupManager ( v0 );
/* .line 557 */
/* .local v0, "backupManager":Lmiui/app/backup/BackupManager; */
v1 = (( miui.app.backup.BackupManager ) v0 ).getState ( ); // invoke-virtual {v0}, Lmiui/app/backup/BackupManager;->getState()I
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 558 */
(( miui.app.backup.BackupManager ) v0 ).getCurrentRunningPackage ( ); // invoke-virtual {v0}, Lmiui/app/backup/BackupManager;->getCurrentRunningPackage()Ljava/lang/String;
/* .line 559 */
/* .local v1, "curRunningPkg":Ljava/lang/String; */
v2 = android.text.TextUtils .isEmpty ( p2 );
/* if-nez v2, :cond_0 */
v2 = (( java.lang.String ) p2 ).equals ( v1 ); // invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_1 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_3
/* .line 560 */
(( com.android.server.am.ProcessRecord ) p1 ).getThread ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
if ( v2 != null) { // if-eqz v2, :cond_3
(( com.android.server.am.ProcessRecord ) p1 ).getPkgList ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPkgList()Lcom/android/server/am/PackageList;
v2 = (( com.android.server.am.PackageList ) v2 ).containsKey ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/am/PackageList;->containsKey(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* iget v2, p1, Lcom/android/server/am/ProcessRecord;->userId:I */
/* if-ne v2, p3, :cond_3 */
/* .line 562 */
} // :cond_1
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "skip kill:" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
if ( p1 != null) { // if-eqz p1, :cond_2
v3 = this.processName;
} // :cond_2
/* move-object v3, p2 */
} // :goto_0
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " for Backup app"; // const-string v3, " for Backup app"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "ProcessManager"; // const-string v3, "ProcessManager"
android.util.Log .i ( v3,v2 );
/* .line 563 */
int v2 = 1; // const/4 v2, 0x1
/* .line 566 */
} // .end local v1 # "curRunningPkg":Ljava/lang/String;
} // :cond_3
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Integer startPreloadApp ( java.lang.String p0, Boolean p1, Boolean p2, miui.process.LifecycleConfig p3 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "ignoreMemory" # Z */
/* .param p3, "sync" # Z */
/* .param p4, "config" # Lmiui/process/LifecycleConfig; */
/* .line 1275 */
v0 = this.mPreloadAppController;
(( com.android.server.am.PreloadAppControllerImpl ) v0 ).preloadAppEnqueue ( p1, p2, p4 ); // invoke-virtual {v0, p1, p2, p4}, Lcom/android/server/am/PreloadAppControllerImpl;->preloadAppEnqueue(Ljava/lang/String;ZLmiui/process/LifecycleConfig;)V
/* .line 1276 */
/* const/16 v0, 0x1f4 */
} // .end method
public Integer startProcesses ( java.util.List p0, Integer p1, Boolean p2, Integer p3, Integer p4 ) {
/* .locals 8 */
/* .param p2, "startProcessCount" # I */
/* .param p3, "ignoreMemory" # Z */
/* .param p4, "userId" # I */
/* .param p5, "flag" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lmiui/process/PreloadProcessData;", */
/* ">;IZII)I" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 814 */
/* .local p1, "dataList":Ljava/util/List;, "Ljava/util/List<Lmiui/process/PreloadProcessData;>;" */
v0 = (( com.android.server.am.ProcessManagerService ) p0 ).checkPermission ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z
final String v1 = "ProcessManager"; // const-string v1, "ProcessManager"
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 820 */
v0 = if ( p1 != null) { // if-eqz p1, :cond_4
if ( v0 != null) { // if-eqz v0, :cond_4
v0 = /* .line 823 */
/* if-lt v0, p2, :cond_3 */
/* .line 826 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p3, :cond_0 */
v2 = com.android.server.am.ProcessUtils .isLowMemory ( );
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 827 */
final String v2 = "low memory! skip start process!"; // const-string v2, "low memory! skip start process!"
android.util.Slog .w ( v1,v2 );
/* .line 828 */
/* .line 830 */
} // :cond_0
/* if-gtz p2, :cond_1 */
/* .line 831 */
/* const-string/jumbo v2, "startProcessCount <= 0, skip start process!" */
android.util.Slog .w ( v1,v2 );
/* .line 832 */
/* .line 835 */
} // :cond_1
v1 = this.mProcessStarter;
v1 = (( com.android.server.am.ProcessStarter ) v1 ).isAllowPreloadProcess ( p1, p5 ); // invoke-virtual {v1, p1, p5}, Lcom/android/server/am/ProcessStarter;->isAllowPreloadProcess(Ljava/util/List;I)Z
/* if-nez v1, :cond_2 */
/* .line 836 */
/* .line 839 */
} // :cond_2
v2 = this.mProcessStarter;
/* move-object v3, p1 */
/* move v4, p2 */
/* move v5, p3 */
/* move v6, p4 */
/* move v7, p5 */
v0 = /* invoke-virtual/range {v2 ..v7}, Lcom/android/server/am/ProcessStarter;->startProcesses(Ljava/util/List;IZII)I */
/* .line 824 */
} // :cond_3
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
final String v1 = "illegal start number!"; // const-string v1, "illegal start number!"
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
/* .line 821 */
} // :cond_4
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
final String v1 = "packageNames cannot be null!"; // const-string v1, "packageNames cannot be null!"
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
/* .line 815 */
} // :cond_5
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Permission Denial: ProcessManager.startMutiProcesses() from pid="; // const-string v2, "Permission Denial: ProcessManager.startMutiProcesses() from pid="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 816 */
v2 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", uid="; // const-string v2, ", uid="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 817 */
/* .local v0, "msg":Ljava/lang/String; */
android.util.Slog .w ( v1,v0 );
/* .line 818 */
/* new-instance v1, Ljava/lang/SecurityException; */
/* invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
protected void systemReady ( ) {
/* .locals 2 */
/* .line 222 */
/* nop */
/* .line 223 */
final String v0 = "perfshielder"; // const-string v0, "perfshielder"
android.os.ServiceManager .getService ( v0 );
/* .line 222 */
com.android.internal.app.IPerfShielder$Stub .asInterface ( v0 );
this.mPerfService = v0;
/* .line 224 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 225 */
/* check-cast v0, Lcom/miui/server/PerfShielderService; */
(( com.miui.server.PerfShielderService ) v0 ).systemReady ( ); // invoke-virtual {v0}, Lcom/miui/server/PerfShielderService;->systemReady()V
/* .line 227 */
} // :cond_0
v0 = this.mProcessPolicy;
v1 = this.mContext;
(( com.android.server.am.ProcessPolicy ) v0 ).systemReady ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessPolicy;->systemReady(Landroid/content/Context;)V
/* .line 228 */
v0 = this.mProcessStarter;
(( com.android.server.am.ProcessStarter ) v0 ).systemReady ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStarter;->systemReady()V
/* .line 229 */
com.android.server.am.MiProcessTracker .getInstance ( );
v1 = this.mContext;
(( com.android.server.am.MiProcessTracker ) v0 ).systemReady ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/MiProcessTracker;->systemReady(Landroid/content/Context;)V
/* .line 231 */
try { // :try_start_0
/* new-instance v0, Ljava/io/FileWriter; */
final String v1 = "/proc/mi_log/binder_delay"; // const-string v1, "/proc/mi_log/binder_delay"
/* invoke-direct {v0, v1}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V */
this.mBinderDelayWriter = v0;
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 234 */
/* .line 232 */
/* :catch_0 */
/* move-exception v0 */
/* .line 233 */
/* .local v0, "e":Ljava/io/IOException; */
(( java.io.IOException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
/* .line 235 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_0
return;
} // .end method
public void unregisterActivityChangeListener ( miui.process.IActivityChangeListener p0 ) {
/* .locals 2 */
/* .param p1, "listener" # Lmiui/process/IActivityChangeListener; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 973 */
v0 = (( com.android.server.am.ProcessManagerService ) p0 ).checkPermission ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 979 */
v0 = this.mForegroundInfoManager;
(( com.android.server.wm.ForegroundInfoManager ) v0 ).unregisterActivityChangeListener ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/ForegroundInfoManager;->unregisterActivityChangeListener(Lmiui/process/IActivityChangeListener;)V
/* .line 980 */
return;
/* .line 974 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Permission Denial: ProcessManager.unregisterActivityChangeListener() from pid="; // const-string v1, "Permission Denial: ProcessManager.unregisterActivityChangeListener() from pid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 975 */
v1 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", uid="; // const-string v1, ", uid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 976 */
/* .local v0, "msg":Ljava/lang/String; */
final String v1 = "ProcessManager"; // const-string v1, "ProcessManager"
android.util.Slog .w ( v1,v0 );
/* .line 977 */
/* new-instance v1, Ljava/lang/SecurityException; */
/* invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
public void unregisterForegroundInfoListener ( miui.process.IForegroundInfoListener p0 ) {
/* .locals 2 */
/* .param p1, "listener" # Lmiui/process/IForegroundInfoListener; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 921 */
v0 = (( com.android.server.am.ProcessManagerService ) p0 ).checkPermission ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z
/* if-nez v0, :cond_1 */
v0 = (( com.android.server.am.ProcessManagerService ) p0 ).checkSystemSignature ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkSystemSignature()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 922 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Permission Denial: ProcessManager.unregisterForegroundInfoListener() from pid="; // const-string v1, "Permission Denial: ProcessManager.unregisterForegroundInfoListener() from pid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 923 */
v1 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", uid="; // const-string v1, ", uid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 924 */
/* .local v0, "msg":Ljava/lang/String; */
final String v1 = "ProcessManager"; // const-string v1, "ProcessManager"
android.util.Slog .w ( v1,v0 );
/* .line 925 */
/* new-instance v1, Ljava/lang/SecurityException; */
/* invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
/* .line 927 */
} // .end local v0 # "msg":Ljava/lang/String;
} // :cond_1
} // :goto_0
v0 = this.mForegroundInfoManager;
(( com.android.server.wm.ForegroundInfoManager ) v0 ).unregisterForegroundInfoListener ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/ForegroundInfoManager;->unregisterForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V
/* .line 928 */
return;
} // .end method
public void unregisterForegroundWindowListener ( miui.process.IForegroundWindowListener p0 ) {
/* .locals 2 */
/* .param p1, "listener" # Lmiui/process/IForegroundWindowListener; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 949 */
v0 = (( com.android.server.am.ProcessManagerService ) p0 ).checkPermission ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 955 */
v0 = this.mForegroundInfoManager;
(( com.android.server.wm.ForegroundInfoManager ) v0 ).unregisterForegroundWindowListener ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/wm/ForegroundInfoManager;->unregisterForegroundWindowListener(Lmiui/process/IForegroundWindowListener;)V
/* .line 956 */
return;
/* .line 950 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Permission Denial: ProcessManager.unregisterForegroundWindowListener() from pid="; // const-string v1, "Permission Denial: ProcessManager.unregisterForegroundWindowListener() from pid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 951 */
v1 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", uid="; // const-string v1, ", uid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 952 */
/* .local v0, "msg":Ljava/lang/String; */
final String v1 = "ProcessManager"; // const-string v1, "ProcessManager"
android.util.Slog .w ( v1,v0 );
/* .line 953 */
/* new-instance v1, Ljava/lang/SecurityException; */
/* invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
public void updateApplicationLockedState ( java.lang.String p0, Integer p1, Boolean p2 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .param p3, "isLocked" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 515 */
v0 = (( com.android.server.am.ProcessManagerService ) p0 ).checkPermission ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 521 */
v0 = this.mProcessPolicy;
v1 = this.mContext;
(( com.android.server.am.ProcessPolicy ) v0 ).updateApplicationLockedState ( v1, p2, p1, p3 ); // invoke-virtual {v0, v1, p2, p1, p3}, Lcom/android/server/am/ProcessPolicy;->updateApplicationLockedState(Landroid/content/Context;ILjava/lang/String;Z)V
/* .line 522 */
return;
/* .line 516 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Permission Denial: ProcessManager.updateApplicationLockedState() from pid="; // const-string v1, "Permission Denial: ProcessManager.updateApplicationLockedState() from pid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 517 */
v1 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", uid="; // const-string v1, ", uid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 518 */
/* .local v0, "msg":Ljava/lang/String; */
final String v1 = "ProcessManager"; // const-string v1, "ProcessManager"
android.util.Slog .w ( v1,v0 );
/* .line 519 */
/* new-instance v1, Ljava/lang/SecurityException; */
/* invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
public void updateCloudData ( miui.process.ProcessCloudData p0 ) {
/* .locals 2 */
/* .param p1, "cloudData" # Lmiui/process/ProcessCloudData; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 890 */
v0 = (( com.android.server.am.ProcessManagerService ) p0 ).checkPermission ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 896 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 899 */
v0 = this.mProcessPolicy;
(( com.android.server.am.ProcessPolicy ) v0 ).updateCloudData ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessPolicy;->updateCloudData(Lmiui/process/ProcessCloudData;)V
/* .line 900 */
return;
/* .line 897 */
} // :cond_0
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
final String v1 = "cloudData cannot be null!"; // const-string v1, "cloudData cannot be null!"
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
/* .line 891 */
} // :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Permission Denial: ProcessManager.updateCloudWhiteList() from pid="; // const-string v1, "Permission Denial: ProcessManager.updateCloudWhiteList() from pid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 892 */
v1 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", uid="; // const-string v1, ", uid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 893 */
/* .local v0, "msg":Ljava/lang/String; */
final String v1 = "ProcessManager"; // const-string v1, "ProcessManager"
android.util.Slog .w ( v1,v0 );
/* .line 894 */
/* new-instance v1, Ljava/lang/SecurityException; */
/* invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
public void updateConfig ( miui.process.ProcessConfig p0 ) {
/* .locals 2 */
/* .param p1, "config" # Lmiui/process/ProcessConfig; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 802 */
v0 = (( com.android.server.am.ProcessManagerService ) p0 ).checkPermission ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 809 */
return;
/* .line 803 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Permission Denial: ProcessManager.updateConfig() from pid="; // const-string v1, "Permission Denial: ProcessManager.updateConfig() from pid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 804 */
v1 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", uid="; // const-string v1, ", uid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 805 */
/* .local v0, "msg":Ljava/lang/String; */
final String v1 = "ProcessManager"; // const-string v1, "ProcessManager"
android.util.Slog .w ( v1,v0 );
/* .line 806 */
/* new-instance v1, Ljava/lang/SecurityException; */
/* invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
