.class public Lcom/android/server/am/ProcessProphetCloud;
.super Ljava/lang/Object;
.source "ProcessProphetCloud.java"


# static fields
.field private static final CLOUD_PROCPROPHET_EMPTYMEM_THRESH:Ljava/lang/String; = "cloud_procprophet_emptymem_thresh"

.field private static final CLOUD_PROCPROPHET_ENABLE:Ljava/lang/String; = "cloud_procprophet_enable"

.field private static final CLOUD_PROCPROPHET_INTERVAL_TIME:Ljava/lang/String; = "cloud_procprophet_interval_time"

.field private static final CLOUD_PROCPROPHET_LAUNCH_BLACKLIST:Ljava/lang/String; = "cloud_procprophet_launch_blacklist"

.field private static final CLOUD_PROCPROPHET_MEMPRES_THRESH:Ljava/lang/String; = "cloud_procprophet_mempres_thresh"

.field private static final CLOUD_PROCPROPHET_STARTFREQ_THRESH:Ljava/lang/String; = "cloud_procprophet_startfreq_thresh"

.field private static final CLOUD_PROCPROPHET_START_LIMITLIST:Ljava/lang/String; = "cloud_procprophet_start_limitlist"

.field private static DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "ProcessProphetCloud"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mProcessProphetImpl:Lcom/android/server/am/ProcessProphetImpl;

.field private mProcessProphetModel:Lcom/android/server/am/ProcessProphetModel;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 40
    nop

    .line 41
    const-string v0, "persist.sys.procprophet.debug"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/am/ProcessProphetCloud;->DEBUG:Z

    .line 40
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/am/ProcessProphetCloud;->mProcessProphetImpl:Lcom/android/server/am/ProcessProphetImpl;

    .line 37
    iput-object v0, p0, Lcom/android/server/am/ProcessProphetCloud;->mProcessProphetModel:Lcom/android/server/am/ProcessProphetModel;

    .line 38
    iput-object v0, p0, Lcom/android/server/am/ProcessProphetCloud;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public getCloudAttribute(Ljava/lang/String;)Ljava/lang/Long;
    .locals 10
    .param p1, "operations"    # Ljava/lang/String;

    .line 286
    const-string v0, "close io failed: "

    const-string v1, "ProcessProphetCloud"

    const/4 v2, 0x0

    .line 287
    .local v2, "sReader":Ljava/io/StringReader;
    const/4 v3, 0x0

    .line 288
    .local v3, "jReader":Lcom/google/gson/stream/JsonReader;
    invoke-static {}, Landroid/os/Process;->getTotalMemory()J

    move-result-wide v4

    const-wide/32 v6, 0x40000000

    div-long/2addr v4, v6

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    long-to-int v4, v4

    .line 290
    .local v4, "totalNameGB":I
    const/4 v5, 0x4

    if-gt v4, v5, :cond_0

    .line 291
    const/4 v4, 0x4

    .line 293
    :cond_0
    const/16 v5, 0xc

    if-lt v4, v5, :cond_1

    .line 294
    const/16 v4, 0xc

    .line 298
    :cond_1
    :try_start_0
    new-instance v5, Ljava/io/StringReader;

    invoke-direct {v5, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    move-object v2, v5

    .line 299
    new-instance v5, Lcom/google/gson/stream/JsonReader;

    invoke-direct {v5, v2}, Lcom/google/gson/stream/JsonReader;-><init>(Ljava/io/Reader;)V

    move-object v3, v5

    .line 300
    invoke-virtual {v3}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 301
    :goto_0
    invoke-virtual {v3}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 302
    invoke-virtual {v3}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 303
    .local v5, "nameNumb":I
    invoke-virtual {v3}, Lcom/google/gson/stream/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v6

    .line 304
    .local v6, "valueStr":Ljava/lang/String;
    if-ne v4, v5, :cond_2

    .line 305
    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 312
    nop

    .line 314
    :try_start_1
    invoke-virtual {v3}, Lcom/google/gson/stream/JsonReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 317
    goto :goto_1

    .line 315
    :catch_0
    move-exception v8

    .line 316
    .local v8, "e":Ljava/io/IOException;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    .end local v8    # "e":Ljava/io/IOException;
    :goto_1
    nop

    .line 320
    invoke-virtual {v2}, Ljava/io/StringReader;->close()V

    .line 305
    return-object v7

    .line 307
    .end local v5    # "nameNumb":I
    .end local v6    # "valueStr":Ljava/lang/String;
    :cond_2
    goto :goto_0

    .line 308
    :cond_3
    :try_start_2
    invoke-virtual {v3}, Lcom/google/gson/stream/JsonReader;->endObject()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 312
    nop

    .line 314
    :try_start_3
    invoke-virtual {v3}, Lcom/google/gson/stream/JsonReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 317
    goto :goto_2

    .line 315
    :catch_1
    move-exception v5

    .line 316
    .local v5, "e":Ljava/io/IOException;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    .end local v5    # "e":Ljava/io/IOException;
    :goto_2
    nop

    .line 320
    :goto_3
    invoke-virtual {v2}, Ljava/io/StringReader;->close()V

    goto :goto_5

    .line 312
    :catchall_0
    move-exception v5

    goto :goto_6

    .line 309
    :catch_2
    move-exception v5

    .line 310
    .restart local v5    # "e":Ljava/io/IOException;
    :try_start_4
    const-string v6, "json reader error"

    invoke-static {v1, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 312
    nop

    .end local v5    # "e":Ljava/io/IOException;
    if-eqz v3, :cond_4

    .line 314
    :try_start_5
    invoke-virtual {v3}, Lcom/google/gson/stream/JsonReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 317
    goto :goto_4

    .line 315
    :catch_3
    move-exception v5

    .line 316
    .restart local v5    # "e":Ljava/io/IOException;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    .end local v5    # "e":Ljava/io/IOException;
    :cond_4
    :goto_4
    if-eqz v2, :cond_5

    .line 320
    goto :goto_3

    .line 323
    :cond_5
    :goto_5
    const-wide/16 v0, -0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0

    .line 312
    :goto_6
    if-eqz v3, :cond_6

    .line 314
    :try_start_6
    invoke-virtual {v3}, Lcom/google/gson/stream/JsonReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 317
    goto :goto_7

    .line 315
    :catch_4
    move-exception v6

    .line 316
    .local v6, "e":Ljava/io/IOException;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    .end local v6    # "e":Ljava/io/IOException;
    :cond_6
    :goto_7
    if-eqz v2, :cond_7

    .line 320
    invoke-virtual {v2}, Ljava/io/StringReader;->close()V

    .line 322
    :cond_7
    throw v5
.end method

.method public initCloud(Lcom/android/server/am/ProcessProphetImpl;Lcom/android/server/am/ProcessProphetModel;Landroid/content/Context;)V
    .locals 2
    .param p1, "ppi"    # Lcom/android/server/am/ProcessProphetImpl;
    .param p2, "ppm"    # Lcom/android/server/am/ProcessProphetModel;
    .param p3, "context"    # Landroid/content/Context;

    .line 44
    iput-object p1, p0, Lcom/android/server/am/ProcessProphetCloud;->mProcessProphetImpl:Lcom/android/server/am/ProcessProphetImpl;

    .line 45
    iput-object p2, p0, Lcom/android/server/am/ProcessProphetCloud;->mProcessProphetModel:Lcom/android/server/am/ProcessProphetModel;

    .line 46
    iput-object p3, p0, Lcom/android/server/am/ProcessProphetCloud;->mContext:Landroid/content/Context;

    .line 47
    const-string v0, "ProcessProphetCloud"

    if-eqz p1, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    .line 51
    :cond_0
    const-string v1, "initCloud success"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    return-void

    .line 48
    :cond_1
    :goto_0
    const-string v1, "initCloud failure."

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    return-void
.end method

.method public registerProcProphetCloudObserver()V
    .locals 5

    .line 56
    new-instance v0, Lcom/android/server/am/ProcessProphetCloud$1;

    iget-object v1, p0, Lcom/android/server/am/ProcessProphetCloud;->mProcessProphetImpl:Lcom/android/server/am/ProcessProphetImpl;

    iget-object v1, v1, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/am/ProcessProphetCloud$1;-><init>(Lcom/android/server/am/ProcessProphetCloud;Landroid/os/Handler;)V

    .line 105
    .local v0, "observer":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetCloud;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 106
    const-string v2, "cloud_procprophet_enable"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 105
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 109
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetCloud;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 110
    const-string v2, "cloud_procprophet_interval_time"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 109
    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 113
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetCloud;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 114
    const-string v2, "cloud_procprophet_startfreq_thresh"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 113
    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 117
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetCloud;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 118
    const-string v2, "cloud_procprophet_mempres_thresh"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 117
    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 121
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetCloud;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 122
    const-string v2, "cloud_procprophet_emptymem_thresh"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 121
    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 125
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetCloud;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 126
    const-string v2, "cloud_procprophet_start_limitlist"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 125
    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 129
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetCloud;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 130
    const-string v2, "cloud_procprophet_launch_blacklist"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 129
    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 132
    return-void
.end method

.method public updateEmptyMemThresCloudControlParas()V
    .locals 7

    .line 226
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetCloud;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "cloud_procprophet_emptymem_thresh"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 228
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetCloud;->mContext:Landroid/content/Context;

    .line 229
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 228
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 232
    .local v0, "emptyMemThresStr":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 233
    invoke-virtual {p0, v0}, Lcom/android/server/am/ProcessProphetCloud;->getCloudAttribute(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    .line 234
    .local v1, "tmpEmptylong":Ljava/lang/Long;
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    const-string v3, "ProcessProphetCloud"

    if-nez v2, :cond_0

    .line 235
    const-string v2, "getCloudAttribute operations is error , return -1"

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    return-void

    .line 238
    :cond_0
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetCloud;->mProcessProphetImpl:Lcom/android/server/am/ProcessProphetImpl;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-string v6, "emptyMem"

    invoke-virtual {v2, v4, v5, v6}, Lcom/android/server/am/ProcessProphetImpl;->updateImplThreshold(JLjava/lang/String;)V

    .line 239
    sget-boolean v2, Lcom/android/server/am/ProcessProphetCloud;->DEBUG:Z

    if-eqz v2, :cond_1

    .line 240
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "receive info from cloud: Empty Process Memory Threshold - "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    .end local v0    # "emptyMemThresStr":Ljava/lang/String;
    .end local v1    # "tmpEmptylong":Ljava/lang/Long;
    :cond_1
    return-void
.end method

.method public updateEnableCloudControlParas()V
    .locals 4

    .line 147
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetCloud;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "cloud_procprophet_enable"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetCloud;->mContext:Landroid/content/Context;

    .line 150
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 149
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 153
    .local v0, "enableStr":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 154
    new-instance v1, Ljava/lang/Boolean;

    invoke-direct {v1, v0}, Ljava/lang/Boolean;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 155
    .local v1, "enableB":Ljava/lang/Boolean;
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetCloud;->mProcessProphetImpl:Lcom/android/server/am/ProcessProphetImpl;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/android/server/am/ProcessProphetImpl;->updateEnable(Z)V

    .line 156
    sget-boolean v2, Lcom/android/server/am/ProcessProphetCloud;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 157
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "receive info from cloud: ProcessProphetEnable - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ProcessProphetCloud"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    .end local v0    # "enableStr":Ljava/lang/String;
    .end local v1    # "enableB":Ljava/lang/Boolean;
    :cond_0
    return-void
.end method

.method public updateLaunchProcCloudControlParas()V
    .locals 4

    .line 267
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetCloud;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "cloud_procprophet_launch_blacklist"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetCloud;->mContext:Landroid/content/Context;

    .line 270
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 269
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 273
    .local v0, "luchBLStr":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 274
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 275
    .local v1, "arrStr":[Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetCloud;->mProcessProphetImpl:Lcom/android/server/am/ProcessProphetImpl;

    const-string v3, "blakclist"

    invoke-virtual {v2, v1, v3}, Lcom/android/server/am/ProcessProphetImpl;->updateList([Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    sget-boolean v2, Lcom/android/server/am/ProcessProphetCloud;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 277
    const-string v2, "ProcessProphetCloud"

    const-string v3, "receive info from cloud: LaunchProcBlackList"

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    .end local v0    # "luchBLStr":Ljava/lang/String;
    .end local v1    # "arrStr":[Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public updateMemThresCloudControlParas()V
    .locals 7

    .line 203
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetCloud;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "cloud_procprophet_mempres_thresh"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 205
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetCloud;->mContext:Landroid/content/Context;

    .line 206
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 205
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 209
    .local v0, "memThresStr":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 210
    invoke-virtual {p0, v0}, Lcom/android/server/am/ProcessProphetCloud;->getCloudAttribute(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    .line 211
    .local v1, "tmpMemlong":Ljava/lang/Long;
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    const-string v3, "ProcessProphetCloud"

    if-nez v2, :cond_0

    .line 212
    const-string v2, "getCloudAttribute operations is error , return -1"

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    return-void

    .line 215
    :cond_0
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetCloud;->mProcessProphetImpl:Lcom/android/server/am/ProcessProphetImpl;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-string v6, "memPress"

    invoke-virtual {v2, v4, v5, v6}, Lcom/android/server/am/ProcessProphetImpl;->updateImplThreshold(JLjava/lang/String;)V

    .line 216
    sget-boolean v2, Lcom/android/server/am/ProcessProphetCloud;->DEBUG:Z

    if-eqz v2, :cond_1

    .line 217
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "receive info from cloud: Memory Pressure Threshold - "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    .end local v0    # "memThresStr":Ljava/lang/String;
    .end local v1    # "tmpMemlong":Ljava/lang/Long;
    :cond_1
    return-void
.end method

.method public updateProcProphetCloudControlParas()V
    .locals 0

    .line 136
    invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetCloud;->updateEnableCloudControlParas()V

    .line 137
    invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetCloud;->updateTrackTimeCloudControlParas()V

    .line 138
    invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetCloud;->updateStartProcFreqThresCloudControlParas()V

    .line 139
    invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetCloud;->updateMemThresCloudControlParas()V

    .line 140
    invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetCloud;->updateEmptyMemThresCloudControlParas()V

    .line 141
    invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetCloud;->updateStartProcLimitCloudControlParas()V

    .line 142
    invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetCloud;->updateLaunchProcCloudControlParas()V

    .line 143
    return-void
.end method

.method public updateStartProcFreqThresCloudControlParas()V
    .locals 5

    .line 184
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetCloud;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "cloud_procprophet_startfreq_thresh"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetCloud;->mContext:Landroid/content/Context;

    .line 187
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 186
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 190
    .local v0, "probThresStr":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 191
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 192
    .local v1, "probThress":[Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetCloud;->mProcessProphetModel:Lcom/android/server/am/ProcessProphetModel;

    invoke-virtual {v2, v1}, Lcom/android/server/am/ProcessProphetModel;->updateModelThreshold([Ljava/lang/String;)V

    .line 193
    sget-boolean v2, Lcom/android/server/am/ProcessProphetCloud;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 194
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "receive info from cloud: Process Frequent Threshold - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v3, v1, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v4, 0x1

    aget-object v4, v1, v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x2

    aget-object v3, v1, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ProcessProphetCloud"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    .end local v0    # "probThresStr":Ljava/lang/String;
    .end local v1    # "probThress":[Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public updateStartProcLimitCloudControlParas()V
    .locals 4

    .line 249
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetCloud;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "cloud_procprophet_start_limitlist"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetCloud;->mContext:Landroid/content/Context;

    .line 252
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 251
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 255
    .local v0, "procLimitStr":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 256
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 257
    .local v1, "arrStr":[Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetCloud;->mProcessProphetImpl:Lcom/android/server/am/ProcessProphetImpl;

    const-string/jumbo v3, "whitelist"

    invoke-virtual {v2, v1, v3}, Lcom/android/server/am/ProcessProphetImpl;->updateList([Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    sget-boolean v2, Lcom/android/server/am/ProcessProphetCloud;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 259
    const-string v2, "ProcessProphetCloud"

    const-string v3, "receive info from cloud: StartProcLimitList"

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    .end local v0    # "procLimitStr":Ljava/lang/String;
    .end local v1    # "arrStr":[Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public updateTrackTimeCloudControlParas()V
    .locals 4

    .line 165
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetCloud;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "cloud_procprophet_interval_time"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetCloud;->mContext:Landroid/content/Context;

    .line 168
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 167
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 171
    .local v0, "intervalStr":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 172
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 173
    .local v1, "valTime":I
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetCloud;->mProcessProphetImpl:Lcom/android/server/am/ProcessProphetImpl;

    invoke-virtual {v2, v1}, Lcom/android/server/am/ProcessProphetImpl;->updateTrackInterval(I)V

    .line 174
    sget-boolean v2, Lcom/android/server/am/ProcessProphetCloud;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 175
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "receive info from cloud: ProcessProphet-Onetrack interval - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ProcessProphetCloud"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    .end local v0    # "intervalStr":Ljava/lang/String;
    .end local v1    # "valTime":I
    :cond_0
    return-void
.end method
