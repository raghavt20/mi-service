public class com.android.server.am.GameProcessCompactor$GameProcessCompactorConfig implements com.android.server.am.IGameProcessAction$IGameProcessActionConfig {
	 /* .source "GameProcessCompactor.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/GameProcessCompactor; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "GameProcessCompactorConfig" */
} // .end annotation
/* # static fields */
private static final java.lang.String COMPACTOR_INCOME_PCT;
private static final java.lang.String COMPACTOR_INTERVAL;
private static final java.lang.String COMPACT_MAX_NUM;
private static final java.lang.String CONFIG_PRIO;
private static final Integer DEFAULT_MAX_NUM;
private static final Integer DEFAULT_PRIO;
private static final java.lang.String MAX_ADJ;
private static final java.lang.String MIN_ADJ;
private static final java.lang.String SKIP_ACTIVE;
private static final java.lang.String SKIP_FOREGROUND;
private static final java.lang.String WHITE_LIST;
/* # instance fields */
Integer mCompactIncomePctThreshold;
Integer mCompactIntervalThreshold;
Integer mCompactMaxNum;
Integer mMaxAdj;
Integer mMinAdj;
Integer mPrio;
Boolean mSkipActive;
Boolean mSkipForeground;
java.util.List mWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public com.android.server.am.GameProcessCompactor$GameProcessCompactorConfig ( ) {
/* .locals 2 */
/* .line 155 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 170 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mPrio:I */
/* .line 171 */
/* const/16 v1, -0x2710 */
/* iput v1, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mMinAdj:I */
/* .line 172 */
/* const/16 v1, 0x3e9 */
/* iput v1, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mMaxAdj:I */
/* .line 173 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
this.mWhiteList = v1;
/* .line 174 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mSkipForeground:Z */
/* .line 175 */
/* iput-boolean v1, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mSkipActive:Z */
/* .line 176 */
/* iput v0, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mCompactIntervalThreshold:I */
/* .line 177 */
/* iput v0, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mCompactIncomePctThreshold:I */
/* .line 178 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mCompactMaxNum:I */
return;
} // .end method
/* # virtual methods */
public void addWhiteList ( java.util.List p0, Boolean p1 ) {
/* .locals 1 */
/* .param p2, "append" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;Z)V" */
/* } */
} // .end annotation
/* .line 230 */
/* .local p1, "wl":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 231 */
v0 = this.mWhiteList;
/* .line 233 */
} // :cond_0
v0 = this.mWhiteList;
/* .line 234 */
} // :goto_0
v0 = this.mWhiteList;
/* .line 235 */
return;
} // .end method
public Integer getPrio ( ) {
/* .locals 1 */
/* .line 225 */
/* iget v0, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mPrio:I */
} // .end method
public void initFromJSON ( org.json.JSONObject p0 ) {
/* .locals 11 */
/* .param p1, "obj" # Lorg/json/JSONObject; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Lorg/json/JSONException; */
/* } */
} // .end annotation
/* .line 185 */
/* const-string/jumbo v0, "white-list" */
final String v1 = "compact-max-num"; // const-string v1, "compact-max-num"
/* const-string/jumbo v2, "skip-foreground" */
/* const-string/jumbo v3, "skip-active" */
final String v4 = "compactor-income-pct"; // const-string v4, "compactor-income-pct"
final String v5 = "compactor-interval"; // const-string v5, "compactor-interval"
final String v6 = "max-adj"; // const-string v6, "max-adj"
final String v7 = "min-adj"; // const-string v7, "min-adj"
final String v8 = "prio"; // const-string v8, "prio"
try { // :try_start_0
v9 = (( org.json.JSONObject ) p1 ).has ( v8 ); // invoke-virtual {p1, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
int v10 = 0; // const/4 v10, 0x0
if ( v9 != null) { // if-eqz v9, :cond_0
/* .line 186 */
v8 = (( org.json.JSONObject ) p1 ).optInt ( v8, v10 ); // invoke-virtual {p1, v8, v10}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I
/* iput v8, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mPrio:I */
/* .line 188 */
} // :cond_0
v8 = (( org.json.JSONObject ) p1 ).has ( v7 ); // invoke-virtual {p1, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v8 != null) { // if-eqz v8, :cond_1
/* .line 189 */
/* const/16 v8, -0x2710 */
v7 = (( org.json.JSONObject ) p1 ).optInt ( v7, v8 ); // invoke-virtual {p1, v7, v8}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I
/* iput v7, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mMinAdj:I */
/* .line 191 */
} // :cond_1
v7 = (( org.json.JSONObject ) p1 ).has ( v6 ); // invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v7 != null) { // if-eqz v7, :cond_2
/* .line 192 */
/* const/16 v7, 0x3e9 */
v6 = (( org.json.JSONObject ) p1 ).optInt ( v6, v7 ); // invoke-virtual {p1, v6, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I
/* iput v6, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mMaxAdj:I */
/* .line 194 */
} // :cond_2
v6 = (( org.json.JSONObject ) p1 ).has ( v5 ); // invoke-virtual {p1, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 195 */
v5 = (( org.json.JSONObject ) p1 ).optInt ( v5, v10 ); // invoke-virtual {p1, v5, v10}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I
/* iput v5, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mCompactIntervalThreshold:I */
/* .line 197 */
} // :cond_3
v5 = (( org.json.JSONObject ) p1 ).has ( v4 ); // invoke-virtual {p1, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 198 */
v4 = (( org.json.JSONObject ) p1 ).optInt ( v4, v10 ); // invoke-virtual {p1, v4, v10}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I
/* iput v4, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mCompactIncomePctThreshold:I */
/* .line 200 */
} // :cond_4
v4 = (( org.json.JSONObject ) p1 ).has ( v3 ); // invoke-virtual {p1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
int v5 = 1; // const/4 v5, 0x1
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 201 */
v3 = (( org.json.JSONObject ) p1 ).optBoolean ( v3, v5 ); // invoke-virtual {p1, v3, v5}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z
/* iput-boolean v3, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mSkipActive:Z */
/* .line 203 */
} // :cond_5
v3 = (( org.json.JSONObject ) p1 ).has ( v2 ); // invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v3 != null) { // if-eqz v3, :cond_6
/* .line 204 */
v2 = (( org.json.JSONObject ) p1 ).optBoolean ( v2, v5 ); // invoke-virtual {p1, v2, v5}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z
/* iput-boolean v2, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mSkipForeground:Z */
/* .line 206 */
} // :cond_6
v2 = (( org.json.JSONObject ) p1 ).has ( v1 ); // invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 207 */
int v2 = -1; // const/4 v2, -0x1
v1 = (( org.json.JSONObject ) p1 ).optInt ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I
/* iput v1, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mCompactMaxNum:I */
/* .line 209 */
} // :cond_7
v1 = (( org.json.JSONObject ) p1 ).has ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_9
/* .line 210 */
(( org.json.JSONObject ) p1 ).optJSONArray ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 211 */
/* .local v0, "tmpArray":Lorg/json/JSONArray; */
if ( v0 != null) { // if-eqz v0, :cond_9
/* .line 212 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = (( org.json.JSONArray ) v0 ).length ( ); // invoke-virtual {v0}, Lorg/json/JSONArray;->length()I
/* if-ge v1, v2, :cond_9 */
/* .line 213 */
v2 = (( org.json.JSONArray ) v0 ).isNull ( v1 ); // invoke-virtual {v0, v1}, Lorg/json/JSONArray;->isNull(I)Z
/* if-nez v2, :cond_8 */
/* .line 214 */
v2 = this.mWhiteList;
(( org.json.JSONArray ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 212 */
} // :cond_8
/* add-int/lit8 v1, v1, 0x1 */
/* .line 220 */
} // .end local v0 # "tmpArray":Lorg/json/JSONArray;
} // .end local v1 # "i":I
} // :cond_9
/* nop */
/* .line 221 */
return;
/* .line 218 */
/* :catch_0 */
/* move-exception v0 */
/* .line 219 */
/* .local v0, "e":Lorg/json/JSONException; */
/* throw v0 */
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 239 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 240 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
final String v1 = "prio="; // const-string v1, "prio="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 241 */
/* iget v1, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mPrio:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 242 */
final String v1 = ", minAdj="; // const-string v1, ", minAdj="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 243 */
/* iget v1, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mMinAdj:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 244 */
final String v1 = ", maxAdj="; // const-string v1, ", maxAdj="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 245 */
/* iget v1, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mMaxAdj:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 246 */
final String v1 = ", compactInterval="; // const-string v1, ", compactInterval="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 247 */
/* iget v1, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mCompactIntervalThreshold:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 248 */
final String v1 = ", compactIncomePct="; // const-string v1, ", compactIncomePct="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 249 */
/* iget v1, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mCompactIncomePctThreshold:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 250 */
final String v1 = ", skipForeground="; // const-string v1, ", skipForeground="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 251 */
/* iget-boolean v1, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mSkipForeground:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
/* .line 252 */
final String v1 = ", skipActive="; // const-string v1, ", skipActive="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 253 */
/* iget-boolean v1, p0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mSkipActive:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
/* .line 254 */
final String v1 = ", whiteList="; // const-string v1, ", whiteList="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 255 */
v1 = this.mWhiteList;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* .line 256 */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
