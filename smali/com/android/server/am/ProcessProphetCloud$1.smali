.class Lcom/android/server/am/ProcessProphetCloud$1;
.super Landroid/database/ContentObserver;
.source "ProcessProphetCloud.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/ProcessProphetCloud;->registerProcProphetCloudObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ProcessProphetCloud;


# direct methods
.method constructor <init>(Lcom/android/server/am/ProcessProphetCloud;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/ProcessProphetCloud;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 56
    iput-object p1, p0, Lcom/android/server/am/ProcessProphetCloud$1;->this$0:Lcom/android/server/am/ProcessProphetCloud;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 1
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 60
    if-eqz p2, :cond_0

    .line 61
    const-string v0, "cloud_procprophet_enable"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 60
    invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetCloud$1;->this$0:Lcom/android/server/am/ProcessProphetCloud;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetCloud;->updateEnableCloudControlParas()V

    .line 63
    return-void

    .line 66
    :cond_0
    if-eqz p2, :cond_1

    .line 67
    const-string v0, "cloud_procprophet_interval_time"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 66
    invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 68
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetCloud$1;->this$0:Lcom/android/server/am/ProcessProphetCloud;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetCloud;->updateTrackTimeCloudControlParas()V

    .line 69
    return-void

    .line 72
    :cond_1
    if-eqz p2, :cond_2

    .line 73
    const-string v0, "cloud_procprophet_startfreq_thresh"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 72
    invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 74
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetCloud$1;->this$0:Lcom/android/server/am/ProcessProphetCloud;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetCloud;->updateStartProcFreqThresCloudControlParas()V

    .line 75
    return-void

    .line 78
    :cond_2
    if-eqz p2, :cond_3

    .line 79
    const-string v0, "cloud_procprophet_mempres_thresh"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 78
    invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 80
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetCloud$1;->this$0:Lcom/android/server/am/ProcessProphetCloud;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetCloud;->updateMemThresCloudControlParas()V

    .line 81
    return-void

    .line 84
    :cond_3
    if-eqz p2, :cond_4

    .line 85
    const-string v0, "cloud_procprophet_emptymem_thresh"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 84
    invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 86
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetCloud$1;->this$0:Lcom/android/server/am/ProcessProphetCloud;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetCloud;->updateEmptyMemThresCloudControlParas()V

    .line 87
    return-void

    .line 90
    :cond_4
    if-eqz p2, :cond_5

    .line 91
    const-string v0, "cloud_procprophet_start_limitlist"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 90
    invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 92
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetCloud$1;->this$0:Lcom/android/server/am/ProcessProphetCloud;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetCloud;->updateStartProcLimitCloudControlParas()V

    .line 93
    return-void

    .line 96
    :cond_5
    if-eqz p2, :cond_6

    .line 97
    const-string v0, "cloud_procprophet_launch_blacklist"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 96
    invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 98
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetCloud$1;->this$0:Lcom/android/server/am/ProcessProphetCloud;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetCloud;->updateLaunchProcCloudControlParas()V

    .line 99
    return-void

    .line 101
    :cond_6
    return-void
.end method
