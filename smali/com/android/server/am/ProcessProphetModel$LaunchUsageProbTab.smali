.class public final Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;
.super Ljava/lang/Object;
.source "ProcessProphetModel.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ProcessProphetModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LaunchUsageProbTab"
.end annotation


# instance fields
.field probTabs:[Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

.field weekendTab:Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;


# direct methods
.method constructor <init>(Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;)V
    .locals 9
    .param p1, "oneDay"    # Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;
    .param p2, "weekendDay"    # Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;

    .line 938
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 933
    const/16 v0, 0x18

    new-array v1, v0, [Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    iput-object v1, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;->probTabs:[Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    .line 934
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;->weekendTab:Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    .line 939
    new-instance v1, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    invoke-direct {v1}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;-><init>()V

    .line 940
    .local v1, "weekendUseInfo":Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;
    const/4 v2, 0x0

    .local v2, "hourOfDay":I
    :goto_0
    if-ge v2, v0, :cond_2

    .line 941
    new-instance v3, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    invoke-direct {v3}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;-><init>()V

    .line 942
    .local v3, "useInfoMapList":Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;
    invoke-direct {p0, p2, v1, v2}, Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;->mergeUseInfo(Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;I)V

    .line 943
    invoke-direct {p0, p1, v3, v2}, Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;->mergeUseInfo(Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;I)V

    .line 947
    add-int/lit8 v4, v2, 0x1

    if-lt v4, v0, :cond_0

    .line 948
    iget-object v4, p1, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;->launchUsageInThisDay:[Ljava/util/HashMap;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    .local v4, "usageInNextHour":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    goto :goto_1

    .line 950
    .end local v4    # "usageInNextHour":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_0
    iget-object v4, p1, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;->launchUsageInThisDay:[Ljava/util/HashMap;

    add-int/lit8 v5, v2, 0x1

    aget-object v4, v4, v5

    .line 953
    .restart local v4    # "usageInNextHour":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    :goto_1
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v5

    if-lez v5, :cond_1

    .line 954
    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 955
    .local v6, "pkg":Ljava/lang/String;
    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-double v7, v7

    invoke-virtual {v3, v6, v7, v8}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->update(Ljava/lang/String;D)V

    .line 956
    .end local v6    # "pkg":Ljava/lang/String;
    goto :goto_2

    .line 958
    :cond_1
    iget-object v5, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;->probTabs:[Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    aput-object v3, v5, v2

    .line 940
    .end local v3    # "useInfoMapList":Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;
    .end local v4    # "usageInNextHour":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 960
    .end local v2    # "hourOfDay":I
    :cond_2
    iput-object v1, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;->weekendTab:Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    .line 961
    return-void
.end method

.method private mergeUseInfo(Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;I)V
    .locals 5
    .param p1, "launchInfoDaily"    # Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;
    .param p2, "uInfoMapList"    # Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;
    .param p3, "hour"    # I

    .line 966
    iget-object v0, p1, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;->launchUsageInThisDay:[Ljava/util/HashMap;

    aget-object v0, v0, p3

    .line 967
    .local v0, "usageInThisHour":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 968
    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 969
    .local v2, "pkg":Ljava/lang/String;
    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-double v3, v3

    invoke-virtual {p2, v2, v3, v4}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->update(Ljava/lang/String;D)V

    .line 970
    .end local v2    # "pkg":Ljava/lang/String;
    goto :goto_0

    .line 972
    :cond_0
    return-void
.end method


# virtual methods
.method public dump()V
    .locals 6

    .line 1030
    const-string v0, "Dumping Launch Usage Prob Tab."

    const-string v1, "ProcessProphetModel"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1031
    const/4 v0, 0x0

    .local v0, "hourOfDay":I
    :goto_0
    const/16 v2, 0x18

    if-ge v0, v2, :cond_1

    .line 1032
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;->probTabs:[Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    aget-object v2, v2, v0

    .line 1033
    .local v2, "useInfoMapList":Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;
    if-eqz v2, :cond_0

    .line 1034
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\thourOfDay="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1035
    .local v3, "dumpRes":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->toProbString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1036
    invoke-static {v1, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1031
    .end local v2    # "useInfoMapList":Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;
    .end local v3    # "dumpRes":Ljava/lang/String;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1040
    .end local v0    # "hourOfDay":I
    :cond_1
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;->weekendTab:Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    if-eqz v0, :cond_2

    .line 1041
    const-string v0, "\tweekendOfDay:"

    .line 1042
    .local v0, "dumpRes":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;->weekendTab:Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    invoke-virtual {v3}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->toProbString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1043
    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1045
    .end local v0    # "dumpRes":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method public dumpNow()V
    .locals 7

    .line 1049
    const-string v0, "Dumping Launch Usage Prob Tab for now."

    const-string v1, "ProcessProphetModel"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1050
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1051
    .local v0, "calendar":Ljava/util/Calendar;
    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 1053
    .local v2, "clock":I
    iget-object v3, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;->probTabs:[Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    aget-object v3, v3, v2

    .line 1054
    .local v3, "useInfoMapList":Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;
    if-eqz v3, :cond_0

    .line 1055
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\tclock="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1056
    .local v4, "dumpRes":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->toProbString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1057
    invoke-static {v1, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1060
    .end local v4    # "dumpRes":Ljava/lang/String;
    :cond_0
    iget-object v4, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;->weekendTab:Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    if-eqz v4, :cond_1

    .line 1061
    const-string v4, "\tweekendOfDay:"

    .line 1062
    .restart local v4    # "dumpRes":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;->weekendTab:Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    invoke-virtual {v6}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->toProbString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1063
    invoke-static {v1, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1065
    .end local v4    # "dumpRes":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public getCurProbTab()Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;
    .locals 3

    .line 1020
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1021
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-static {v0}, Lcom/android/server/am/ProcessProphetModel;->-$$Nest$smisWeekend(Ljava/util/Calendar;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1022
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;->weekendTab:Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    return-object v1

    .line 1024
    :cond_0
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 1025
    .local v1, "clock":I
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;->probTabs:[Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    aget-object v2, v2, v1

    return-object v2
.end method

.method public getProb(Ljava/lang/String;)Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
    .locals 6
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 976
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 977
    .local v0, "calendar":Ljava/util/Calendar;
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 978
    .local v1, "clock":I
    const/4 v2, 0x0

    .line 979
    .local v2, "useInfoMapList":Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;
    invoke-static {v0}, Lcom/android/server/am/ProcessProphetModel;->-$$Nest$smisWeekend(Ljava/util/Calendar;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 980
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;->weekendTab:Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    goto :goto_0

    .line 982
    :cond_0
    iget-object v3, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;->probTabs:[Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    aget-object v2, v3, v1

    .line 984
    :goto_0
    if-eqz v2, :cond_1

    .line 985
    new-instance v3, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;

    invoke-virtual {v2, p1}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->getProb(Ljava/lang/String;)D

    move-result-wide v4

    invoke-direct {v3, p1, v4, v5}, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;-><init>(Ljava/lang/String;D)V

    return-object v3

    .line 987
    :cond_1
    new-instance v3, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;

    const-wide/16 v4, 0x0

    invoke-direct {v3, p1, v4, v5}, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;-><init>(Ljava/lang/String;D)V

    return-object v3
.end method

.method public getTopProb()Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
    .locals 1

    .line 993
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;->getTopProb(I)Lcom/android/server/am/ProcessProphetModel$PkgValuePair;

    move-result-object v0

    return-object v0
.end method

.method public getTopProb(I)Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
    .locals 3
    .param p1, "i"    # I

    .line 998
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 999
    .local v0, "calendar":Ljava/util/Calendar;
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 1000
    .local v1, "clock":I
    invoke-static {v0}, Lcom/android/server/am/ProcessProphetModel;->-$$Nest$smisWeekend(Ljava/util/Calendar;)Z

    move-result v2

    invoke-virtual {p0, v1, p1, v2}, Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;->getTopProb(IIZ)Lcom/android/server/am/ProcessProphetModel$PkgValuePair;

    move-result-object v2

    .line 1001
    .local v2, "top":Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
    return-object v2
.end method

.method public getTopProb(IIZ)Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
    .locals 2
    .param p1, "hourOfDay"    # I
    .param p2, "top"    # I
    .param p3, "isWeekend"    # Z

    .line 1006
    const/4 v0, 0x0

    .line 1007
    .local v0, "useInfoMapList":Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;
    if-eqz p3, :cond_0

    .line 1008
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;->weekendTab:Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    goto :goto_0

    .line 1010
    :cond_0
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;->probTabs:[Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    aget-object v0, v1, p1

    .line 1012
    :goto_0
    if-nez v0, :cond_1

    .line 1013
    const/4 v1, 0x0

    return-object v1

    .line 1015
    :cond_1
    invoke-virtual {v0, p2}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->getTopProb(I)Lcom/android/server/am/ProcessProphetModel$PkgValuePair;

    move-result-object v1

    return-object v1
.end method
