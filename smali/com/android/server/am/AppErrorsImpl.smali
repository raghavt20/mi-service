.class public Lcom/android/server/am/AppErrorsImpl;
.super Lcom/android/server/am/AppErrorsStub;
.source "AppErrorsImpl.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Lcom/android/server/am/AppErrorsStub;-><init>()V

    return-void
.end method


# virtual methods
.method public handleShowAppErrorUI(Landroid/content/Context;Lcom/android/server/am/ProcessErrorStateRecord;Lcom/android/server/am/AppErrorDialog$Data;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "errState"    # Lcom/android/server/am/ProcessErrorStateRecord;
    .param p3, "data"    # Lcom/android/server/am/AppErrorDialog$Data;

    .line 13
    invoke-static {p1}, Landroid/provider/MiuiSettings$Secure;->isForceCloseDialogEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 14
    invoke-virtual {p2}, Lcom/android/server/am/ProcessErrorStateRecord;->getDialogController()Lcom/android/server/am/ErrorDialogController;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/android/server/am/ErrorDialogController;->showCrashDialogs(Lcom/android/server/am/AppErrorDialog$Data;)V

    .line 15
    return-void

    .line 17
    :cond_0
    iget-object v0, p3, Lcom/android/server/am/AppErrorDialog$Data;->result:Lcom/android/server/am/AppErrorResult;

    if-eqz v0, :cond_1

    .line 18
    iget-object v0, p3, Lcom/android/server/am/AppErrorDialog$Data;->result:Lcom/android/server/am/AppErrorResult;

    sget v1, Lcom/android/server/am/AppErrorDialog;->CANT_SHOW:I

    invoke-virtual {v0, v1}, Lcom/android/server/am/AppErrorResult;->set(I)V

    .line 20
    :cond_1
    return-void
.end method
