class com.android.server.am.AppStateManager$CurrentTaskStack {
	 /* .source "AppStateManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/AppStateManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "CurrentTaskStack" */
} // .end annotation
/* # instance fields */
private final java.util.Stack mTaskRecords;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Stack<", */
/* "Lcom/android/server/am/AppStateManager$TaskRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.am.AppStateManager$TaskRecord mTopTask;
final com.android.server.am.AppStateManager this$0; //synthetic
/* # direct methods */
 com.android.server.am.AppStateManager$CurrentTaskStack ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/android/server/am/AppStateManager; */
/* .line 3452 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 3453 */
/* new-instance v0, Ljava/util/Stack; */
/* invoke-direct {v0}, Ljava/util/Stack;-><init>()V */
this.mTaskRecords = v0;
return;
} // .end method
/* # virtual methods */
void dump ( java.io.PrintWriter p0, java.lang.String[] p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "args" # [Ljava/lang/String; */
/* .param p3, "opti" # I */
/* .line 3496 */
final String v0 = "#tasks"; // const-string v0, "#tasks"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3497 */
v0 = this.mTaskRecords;
/* monitor-enter v0 */
/* .line 3498 */
try { // :try_start_0
v1 = this.mTaskRecords;
(( java.util.Stack ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Lcom/android/server/am/AppStateManager$TaskRecord; */
/* .line 3499 */
/* .local v2, "task":Lcom/android/server/am/AppStateManager$TaskRecord; */
(( com.android.server.am.AppStateManager$TaskRecord ) v2 ).dump ( p1, p2, p3 ); // invoke-virtual {v2, p1, p2, p3}, Lcom/android/server/am/AppStateManager$TaskRecord;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
/* .line 3500 */
} // .end local v2 # "task":Lcom/android/server/am/AppStateManager$TaskRecord;
/* .line 3501 */
} // :cond_0
/* monitor-exit v0 */
/* .line 3502 */
return;
/* .line 3501 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
Boolean isProcessInTaskStack ( Integer p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 3474 */
v0 = this.mTaskRecords;
/* monitor-enter v0 */
/* .line 3475 */
try { // :try_start_0
v1 = this.mTaskRecords;
(( java.util.Stack ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/android/server/am/AppStateManager$TaskRecord; */
/* .line 3476 */
/* .local v2, "task":Lcom/android/server/am/AppStateManager$TaskRecord; */
v3 = (( com.android.server.am.AppStateManager$TaskRecord ) v2 ).isProcessInTask ( p1, p2 ); // invoke-virtual {v2, p1, p2}, Lcom/android/server/am/AppStateManager$TaskRecord;->isProcessInTask(II)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 3477 */
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 3479 */
} // .end local v2 # "task":Lcom/android/server/am/AppStateManager$TaskRecord;
} // :cond_0
/* .line 3480 */
} // :cond_1
/* monitor-exit v0 */
/* .line 3481 */
int v0 = 0; // const/4 v0, 0x0
/* .line 3480 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
Boolean isProcessInTaskStack ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "prcName" # Ljava/lang/String; */
/* .line 3485 */
v0 = this.mTaskRecords;
/* monitor-enter v0 */
/* .line 3486 */
try { // :try_start_0
v1 = this.mTaskRecords;
(( java.util.Stack ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/android/server/am/AppStateManager$TaskRecord; */
/* .line 3487 */
/* .local v2, "task":Lcom/android/server/am/AppStateManager$TaskRecord; */
v3 = (( com.android.server.am.AppStateManager$TaskRecord ) v2 ).isProcessInTask ( p1 ); // invoke-virtual {v2, p1}, Lcom/android/server/am/AppStateManager$TaskRecord;->isProcessInTask(Ljava/lang/String;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 3488 */
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 3490 */
} // .end local v2 # "task":Lcom/android/server/am/AppStateManager$TaskRecord;
} // :cond_0
/* .line 3491 */
} // :cond_1
/* monitor-exit v0 */
/* .line 3492 */
int v0 = 0; // const/4 v0, 0x0
/* .line 3491 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
void updateIfNeeded ( Integer p0, com.android.server.am.AppStateManager$AppState$RunningProcess p1, Integer p2, java.lang.String p3 ) {
/* .locals 3 */
/* .param p1, "taskId" # I */
/* .param p2, "proc" # Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .param p3, "launchedFromUid" # I */
/* .param p4, "launchedFromPkg" # Ljava/lang/String; */
/* .line 3458 */
v0 = this.mTaskRecords;
/* monitor-enter v0 */
/* .line 3459 */
try { // :try_start_0
v1 = this.mTopTask;
if ( v1 != null) { // if-eqz v1, :cond_0
/* iget v1, v1, Lcom/android/server/am/AppStateManager$TaskRecord;->mTaskId:I */
/* if-eq v1, p1, :cond_2 */
/* .line 3460 */
} // :cond_0
v1 = this.mTopTask;
if ( v1 != null) { // if-eqz v1, :cond_1
/* iget v1, v1, Lcom/android/server/am/AppStateManager$TaskRecord;->mLastUid:I */
/* if-eq p3, v1, :cond_1 */
v1 = this.mTopTask;
v1 = this.mLastPkg;
/* .line 3461 */
v1 = (( java.lang.String ) v1 ).equals ( p4 ); // invoke-virtual {v1, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_1 */
/* .line 3462 */
v1 = this.mTaskRecords;
(( java.util.Stack ) v1 ).clear ( ); // invoke-virtual {v1}, Ljava/util/Stack;->clear()V
/* .line 3464 */
} // :cond_1
/* new-instance v1, Lcom/android/server/am/AppStateManager$TaskRecord; */
v2 = this.this$0;
/* invoke-direct {v1, v2, p1}, Lcom/android/server/am/AppStateManager$TaskRecord;-><init>(Lcom/android/server/am/AppStateManager;I)V */
this.mTopTask = v1;
/* .line 3465 */
v2 = this.mTaskRecords;
(( java.util.Stack ) v2 ).push ( v1 ); // invoke-virtual {v2, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 3467 */
} // :cond_2
v1 = this.mTopTask;
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 3468 */
(( com.android.server.am.AppStateManager$TaskRecord ) v1 ).addProcess ( p2 ); // invoke-virtual {v1, p2}, Lcom/android/server/am/AppStateManager$TaskRecord;->addProcess(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V
/* .line 3470 */
} // :cond_3
/* monitor-exit v0 */
/* .line 3471 */
return;
/* .line 3470 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
