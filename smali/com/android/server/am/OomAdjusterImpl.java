public class com.android.server.am.OomAdjusterImpl extends com.android.server.am.OomAdjusterStub {
	 /* .source "OomAdjusterImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String ADJ_TYPE_BIND_IMPROVE;
public static final java.lang.String ADJ_TYPE_CAMERA_IMPROVE;
public static final java.lang.String ADJ_TYPE_GAME_IMPROVE;
public static final java.lang.String ADJ_TYPE_GAME_IMPROVE_DOWNLOAD;
public static final java.lang.String ADJ_TYPE_GAME_IMPROVE_PAYMENT;
public static final java.lang.String ADJ_TYPE_PREVIOUS_IMPROVE;
public static final java.lang.String ADJ_TYPE_PREVIOUS_IMPROVE_SCALE;
public static final java.lang.String ADJ_TYPE_RESIDENT_IMPROVE;
public static final java.lang.String ADJ_TYPE_WIDGET_DEGENERATE;
private static final Integer BACKGROUND_APP_COUNT_LIMIT;
private static final Integer GAME_APP_BACKGROUND_STATE;
private static final Integer GAME_APP_TOP_TO_BACKGROUND_STATE;
private static final Integer GAME_SCENE_DEFAULT_STATE;
private static final Integer GAME_SCENE_DOWNLOAD_STATE;
private static final Integer GAME_SCENE_PLAYING_STATE;
private static final Integer GAME_SCENE_WATCHING_STATE;
private static final Integer HIGH_MEMORY_RESIDENT_APP_COUNT;
private static Boolean IMPROVE_RESIDENT_APP_ADJ_ENABLE;
public static Boolean LIMIT_BIND_VEISIBLE_ENABLED;
private static final Integer LOW_MEMORY_RESIDENT_APP_COUNT;
private static final Integer MAX_APP_WEEK_LAUNCH_COUNT;
private static final Integer MAX_APP_WEEK_TOP_TIME;
private static final Integer MAX_GAME_DOWNLOAD_TIME;
private static final Integer MAX_PAYMENT_SCENE_TIME;
private static final Integer MAX_PREVIOUS_GAME_TIME;
private static final Integer MAX_PREVIOUS_PROTECT_TIME;
private static final Integer MAX_PREVIOUS_TIME;
private static final Integer MIDDLE_MEMORY_RESIDENT_APP_COUNT;
private static final java.lang.String PACKAGE_NAME_CAMERA;
private static final java.lang.String PACKAGE_NAME_MAGE_PAYMENT_SDK;
private static final java.lang.String PACKAGE_NAME_WECHAT;
private static final Integer PREVIOUS_APP_CRITICAL_ADJ;
private static final Integer PREVIOUS_APP_MAJOR_ADJ;
private static final Integer PREVIOUS_APP_MINOR_ADJ;
private static final Integer PREVIOUS_PROTECT_CRITICAL_COUNT;
private static java.lang.String QQ_PLAYER;
private static final Integer RESIDENT_APP_COUNT;
private static final Integer RESIDENT_APP_COUNT_LIMIT;
private static Boolean SCALE_BACKGROUND_APP_ADJ_ENABLE;
private static final Long TOTAL_MEMORY;
private static final Boolean UNTRUSTEDAPP_BG_ENABLED;
private static final Integer WIDGET_PROTECT_TIME;
private static com.android.server.am.OomAdjusterImpl$RunningAppRecordMap mRunningGameMap;
private static final android.util.ArraySet mWidgetProcBCWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List sPaymentAppList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List sPreviousBackgroundAppList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List sPreviousResidentAppList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.Map sSubProcessAdjBindList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List skipMoveCgroupList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private java.lang.String mForegroundPkg;
private java.lang.String mForegroundResultToProc;
private com.miui.app.smartpower.SmartPowerServiceInternal mSmartPowerService;
/* # direct methods */
static com.android.server.am.OomAdjusterImpl ( ) {
/* .locals 6 */
/* .line 61 */
/* .line 72 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
/* .line 78 */
/* nop */
/* .line 79 */
final String v1 = "persist.sys.spc.bindvisible.enabled"; // const-string v1, "persist.sys.spc.bindvisible.enabled"
int v2 = 1; // const/4 v2, 0x1
v1 = android.os.SystemProperties .getBoolean ( v1,v2 );
com.android.server.am.OomAdjusterImpl.LIMIT_BIND_VEISIBLE_ENABLED = (v1!= 0);
/* .line 83 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 85 */
final String v2 = "com.tencent.mm"; // const-string v2, "com.tencent.mm"
/* .line 86 */
v1 = com.android.server.am.OomAdjusterImpl.skipMoveCgroupList;
final String v3 = "com.tencent.mobileqq"; // const-string v3, "com.tencent.mobileqq"
/* .line 90 */
/* new-instance v1, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap; */
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {v1, v3}, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;-><init>(Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap-IA;)V */
/* .line 91 */
/* new-instance v1, Ljava/util/HashMap; */
/* invoke-direct {v1}, Ljava/util/HashMap;-><init>()V */
/* .line 93 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 94 */
v1 = com.android.server.am.OomAdjusterImpl .getBackgroundAppCount ( );
/* .line 96 */
/* nop */
/* .line 97 */
final String v1 = "persist.sys.spc.scale.backgorund.app.enable"; // const-string v1, "persist.sys.spc.scale.backgorund.app.enable"
int v3 = 0; // const/4 v3, 0x0
v1 = android.os.SystemProperties .getBoolean ( v1,v3 );
com.android.server.am.OomAdjusterImpl.SCALE_BACKGROUND_APP_ADJ_ENABLE = (v1!= 0);
/* .line 98 */
/* nop */
/* .line 99 */
final String v1 = "persist.sys.spc.resident.app.enable"; // const-string v1, "persist.sys.spc.resident.app.enable"
v1 = android.os.SystemProperties .getBoolean ( v1,v3 );
com.android.server.am.OomAdjusterImpl.IMPROVE_RESIDENT_APP_ADJ_ENABLE = (v1!= 0);
/* .line 100 */
/* nop */
/* .line 101 */
final String v1 = "persist.sys.miui.resident.app.count"; // const-string v1, "persist.sys.miui.resident.app.count"
v1 = android.os.SystemProperties .getInt ( v1,v3 );
/* .line 105 */
android.os.Process .getTotalMemory ( );
/* move-result-wide v3 */
/* const/16 v5, 0x1e */
/* shr-long/2addr v3, v5 */
/* sput-wide v3, Lcom/android/server/am/OomAdjusterImpl;->TOTAL_MEMORY:J */
/* .line 106 */
/* add-int/lit8 v3, v1, 0xa */
/* .line 107 */
/* add-int/lit8 v3, v1, 0x5 */
/* .line 108 */
/* add-int/lit8 v1, v1, 0x2 */
/* .line 109 */
v1 = com.android.server.am.OomAdjusterImpl .getResidentAppCount ( );
/* .line 110 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 151 */
final String v1 = "com.tencent.qqmusic:QQPlayerService"; // const-string v1, "com.tencent.qqmusic:QQPlayerService"
/* .line 154 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 156 */
final String v1 = "android.appwidget.action.APPWIDGET_UPDATE"; // const-string v1, "android.appwidget.action.APPWIDGET_UPDATE"
(( android.util.ArraySet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 157 */
final String v1 = "miui.appwidget.action.APPWIDGET_UPDATE"; // const-string v1, "miui.appwidget.action.APPWIDGET_UPDATE"
(( android.util.ArraySet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 159 */
v0 = com.android.server.am.OomAdjusterImpl.sPaymentAppList;
/* .line 160 */
v0 = com.android.server.am.OomAdjusterImpl.sPaymentAppList;
final String v1 = "com.eg.android.AlipayGphone"; // const-string v1, "com.eg.android.AlipayGphone"
/* .line 161 */
v0 = com.android.server.am.OomAdjusterImpl.sPaymentAppList;
final String v1 = "com.xiaomi.gamecenter.sdk.service"; // const-string v1, "com.xiaomi.gamecenter.sdk.service"
/* .line 162 */
return;
} // .end method
public com.android.server.am.OomAdjusterImpl ( ) {
/* .locals 1 */
/* .line 41 */
/* invoke-direct {p0}, Lcom/android/server/am/OomAdjusterStub;-><init>()V */
/* .line 88 */
final String v0 = ""; // const-string v0, ""
this.mForegroundPkg = v0;
/* .line 89 */
this.mForegroundResultToProc = v0;
return;
} // .end method
private Boolean computeOomAdjForBackgroundApp ( com.android.server.am.ProcessRecord p0, Integer p1 ) {
/* .locals 9 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "procState" # I */
/* .line 204 */
/* invoke-direct {p0}, Lcom/android/server/am/OomAdjusterImpl;->updateBackgroundAppList()V */
/* .line 205 */
v0 = com.android.server.am.OomAdjusterImpl.sPreviousBackgroundAppList;
v0 = v1 = this.processName;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 206 */
v0 = com.android.server.am.OomAdjusterImpl.sPreviousBackgroundAppList;
v0 = v1 = this.processName;
/* .line 207 */
/* .local v0, "index":I */
/* const/16 v1, -0x2710 */
/* .line 208 */
/* .local v1, "tempAdj":I */
/* if-ge v0, v2, :cond_0 */
/* .line 209 */
/* mul-int/lit8 v2, v0, 0x2 */
/* add-int/lit16 v2, v2, 0x2bc */
} // .end local v1 # "tempAdj":I
/* .local v2, "tempAdj":I */
/* .line 211 */
} // .end local v2 # "tempAdj":I
/* .restart local v1 # "tempAdj":I */
} // :cond_0
/* mul-int/lit8 v2, v2, 0x2 */
/* add-int/lit16 v2, v2, 0x2bc */
/* .line 213 */
} // .end local v1 # "tempAdj":I
/* .restart local v2 # "tempAdj":I */
} // :goto_0
v4 = this.mState;
int v5 = 0; // const/4 v5, 0x0
final String v8 = "ADJ_TYPE_PREVIOUS_IMPROVE_SCALE"; // const-string v8, "ADJ_TYPE_PREVIOUS_IMPROVE_SCALE"
/* move-object v3, p0 */
/* move v6, v2 */
/* move v7, p2 */
/* invoke-direct/range {v3 ..v8}, Lcom/android/server/am/OomAdjusterImpl;->modifyProcessRecordAdj(Lcom/android/server/am/ProcessStateRecord;ZIILjava/lang/String;)V */
/* .line 215 */
int v1 = 1; // const/4 v1, 0x1
/* .line 217 */
} // .end local v0 # "index":I
} // .end local v2 # "tempAdj":I
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean computeOomAdjForGameApp ( com.android.server.am.ProcessRecord p0, Integer p1 ) {
/* .locals 12 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "procState" # I */
/* .line 350 */
int v0 = 0; // const/4 v0, 0x0
/* .line 351 */
/* .local v0, "isChange":Z */
v7 = this.mState;
/* .line 352 */
/* .local v7, "appState":Lcom/android/server/am/ProcessStateRecord; */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
(( com.android.server.am.ProcessStateRecord ) v7 ).getLastTopTime ( ); // invoke-virtual {v7}, Lcom/android/server/am/ProcessStateRecord;->getLastTopTime()J
/* move-result-wide v3 */
/* sub-long v8, v1, v3 */
/* .line 353 */
/* .local v8, "backgroundTime":J */
com.android.server.am.SystemPressureController .getInstance ( );
v2 = this.info;
v2 = this.packageName;
v1 = (( com.android.server.am.SystemPressureController ) v1 ).isGameApp ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/SystemPressureController;->isGameApp(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 354 */
v1 = com.android.server.am.MemoryFreezeStub .getInstance ( );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 355 */
int v3 = 0; // const/4 v3, 0x0
/* const/16 v4, 0x2bd */
final String v6 = "game-improveAdj(memory-freeze)"; // const-string v6, "game-improveAdj(memory-freeze)"
/* move-object v1, p0 */
/* move-object v2, v7 */
/* move v5, p2 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/am/OomAdjusterImpl;->modifyProcessRecordAdj(Lcom/android/server/am/ProcessStateRecord;ZIILjava/lang/String;)V */
/* .line 357 */
int v0 = 1; // const/4 v0, 0x1
/* .line 358 */
} // :cond_0
/* const-wide/32 v1, 0x927c0 */
/* cmp-long v1, v8, v1 */
/* if-gtz v1, :cond_3 */
/* .line 359 */
v1 = com.android.server.am.OomAdjusterImpl.mRunningGameMap;
v10 = (( com.android.server.am.OomAdjusterImpl$RunningAppRecordMap ) v1 ).size ( ); // invoke-virtual {v1}, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;->size()I
/* .line 360 */
/* .local v10, "gameCount":I */
v1 = com.android.server.am.OomAdjusterImpl.mRunningGameMap;
v2 = this.info;
v2 = this.packageName;
v11 = (( com.android.server.am.OomAdjusterImpl$RunningAppRecordMap ) v1 ).getIndexForKey ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;->getIndexForKey(Ljava/lang/String;)I
/* .line 361 */
/* .local v11, "gameIndex":I */
/* if-le v10, v1, :cond_1 */
/* if-ge v11, v1, :cond_2 */
/* .line 363 */
} // :cond_1
int v3 = 0; // const/4 v3, 0x0
/* const/16 v4, 0x2bd */
final String v6 = "game-improve"; // const-string v6, "game-improve"
/* move-object v1, p0 */
/* move-object v2, v7 */
/* move v5, p2 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/am/OomAdjusterImpl;->modifyProcessRecordAdj(Lcom/android/server/am/ProcessStateRecord;ZIILjava/lang/String;)V */
/* .line 365 */
int v0 = 1; // const/4 v0, 0x1
/* .line 367 */
} // .end local v10 # "gameCount":I
} // .end local v11 # "gameIndex":I
} // :cond_2
} // :cond_3
/* const-wide/32 v1, 0x124f80 */
/* cmp-long v1, v8, v1 */
/* if-gtz v1, :cond_4 */
/* .line 369 */
v1 = com.android.server.am.OomAdjusterImpl.mRunningGameMap;
v2 = this.info;
v2 = this.packageName;
(( com.android.server.am.OomAdjusterImpl$RunningAppRecordMap ) v1 ).getForKey ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;->getForKey(Ljava/lang/String;)Ljava/lang/Integer;
v10 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* .line 370 */
/* .local v10, "gameScene":I */
/* packed-switch v10, :pswitch_data_0 */
/* .line 378 */
/* :pswitch_0 */
/* .line 372 */
/* :pswitch_1 */
int v3 = 0; // const/4 v3, 0x0
/* const/16 v4, 0x2be */
final String v6 = "game-improve-download"; // const-string v6, "game-improve-download"
/* move-object v1, p0 */
/* move-object v2, v7 */
/* move v5, p2 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/am/OomAdjusterImpl;->modifyProcessRecordAdj(Lcom/android/server/am/ProcessStateRecord;ZIILjava/lang/String;)V */
/* .line 374 */
int v0 = 1; // const/4 v0, 0x1
/* .line 375 */
/* nop */
/* .line 382 */
} // .end local v10 # "gameScene":I
} // :goto_0
/* .line 383 */
} // :cond_4
v1 = com.android.server.am.OomAdjusterImpl.mRunningGameMap;
v2 = this.info;
v2 = this.packageName;
(( com.android.server.am.OomAdjusterImpl$RunningAppRecordMap ) v1 ).removeForKey ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;->removeForKey(Ljava/lang/String;)V
/* .line 386 */
} // :cond_5
} // :goto_1
/* :pswitch_data_0 */
/* .packed-switch 0x2 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private Boolean computeOomAdjForPaymentScene ( com.android.server.am.ProcessRecord p0, Integer p1 ) {
/* .locals 9 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "procState" # I */
/* .line 414 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
v2 = this.mState;
(( com.android.server.am.ProcessStateRecord ) v2 ).getLastTopTime ( ); // invoke-virtual {v2}, Lcom/android/server/am/ProcessStateRecord;->getLastTopTime()J
/* move-result-wide v2 */
/* sub-long/2addr v0, v2 */
/* .line 415 */
/* .local v0, "backgroundTime":J */
/* sget-boolean v2, Lcom/miui/app/smartpower/SmartPowerSettings;->GAME_PAY_PROTECT_ENABLED:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 416 */
v2 = (( com.android.server.am.OomAdjusterImpl ) p0 ).isPaymentScene ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/OomAdjusterImpl;->isPaymentScene(Lcom/android/server/am/ProcessRecord;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* const-wide/32 v2, 0x1d4c0 */
/* cmp-long v2, v0, v2 */
/* if-gtz v2, :cond_0 */
/* .line 417 */
v4 = this.mState;
int v5 = 0; // const/4 v5, 0x0
/* const/16 v6, 0x190 */
final String v8 = "game-improve-payment"; // const-string v8, "game-improve-payment"
/* move-object v3, p0 */
/* move v7, p2 */
/* invoke-direct/range {v3 ..v8}, Lcom/android/server/am/OomAdjusterImpl;->modifyProcessRecordAdj(Lcom/android/server/am/ProcessStateRecord;ZIILjava/lang/String;)V */
/* .line 419 */
int v2 = 1; // const/4 v2, 0x1
/* .line 421 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // .end method
private Boolean computeOomAdjForResidentApp ( com.android.server.am.ProcessRecord p0, Integer p1 ) {
/* .locals 7 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "procState" # I */
/* .line 318 */
v0 = this.mSmartPowerService;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 319 */
(( com.android.server.am.OomAdjusterImpl ) p0 ).updateResidentAppList ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/am/OomAdjusterImpl;->updateResidentAppList(Ljava/util/List;)V
/* .line 321 */
} // :cond_0
v0 = com.android.server.am.OomAdjusterImpl.sPreviousResidentAppList;
v0 = v1 = this.processName;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 322 */
v2 = this.mState;
int v3 = 0; // const/4 v3, 0x0
/* const/16 v4, 0x2da */
final String v6 = "resident-improveAdj"; // const-string v6, "resident-improveAdj"
/* move-object v1, p0 */
/* move v5, p2 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/am/OomAdjusterImpl;->modifyProcessRecordAdj(Lcom/android/server/am/ProcessStateRecord;ZIILjava/lang/String;)V */
/* .line 324 */
int v0 = 1; // const/4 v0, 0x1
/* .line 326 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private final Boolean computePreviousAdj ( com.android.server.am.ProcessRecord p0, Integer p1, Integer p2 ) {
/* .locals 10 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "procState" # I */
/* .param p3, "adj" # I */
/* .line 173 */
/* sget-boolean v0, Lcom/miui/app/smartpower/SmartPowerPolicyConstants;->TESTSUITSPECIFIC:Z */
/* if-nez v0, :cond_4 */
/* .line 174 */
v0 = (( com.android.server.am.ProcessRecord ) p1 ).hasActivities ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->hasActivities()Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
/* .line 175 */
v0 = android.os.Process .isIsolated ( v0 );
/* if-nez v0, :cond_4 */
/* const/16 v0, 0x2bc */
/* if-le p3, v0, :cond_4 */
/* .line 177 */
v0 = this.mState;
/* .line 178 */
/* .local v0, "appState":Lcom/android/server/am/ProcessStateRecord; */
v1 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/am/OomAdjusterImpl;->computeOomAdjForPaymentScene(Lcom/android/server/am/ProcessRecord;I)Z */
int v7 = 1; // const/4 v7, 0x1
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 179 */
/* .line 182 */
} // :cond_0
v1 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/am/OomAdjusterImpl;->computeOomAdjForGameApp(Lcom/android/server/am/ProcessRecord;I)Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 183 */
/* .line 185 */
} // :cond_1
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
(( com.android.server.am.ProcessStateRecord ) v0 ).getLastTopTime ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getLastTopTime()J
/* move-result-wide v3 */
/* sub-long v8, v1, v3 */
/* .line 186 */
/* .local v8, "backgroundTime":J */
/* const-wide/32 v1, 0x493e0 */
/* cmp-long v1, v8, v1 */
/* if-gtz v1, :cond_3 */
/* .line 187 */
/* sget-boolean v1, Lcom/android/server/am/OomAdjusterImpl;->SCALE_BACKGROUND_APP_ADJ_ENABLE:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 188 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/am/OomAdjusterImpl;->computeOomAdjForBackgroundApp(Lcom/android/server/am/ProcessRecord;I)Z */
/* .line 189 */
/* .line 191 */
} // :cond_2
int v3 = 0; // const/4 v3, 0x0
/* const/16 v4, 0x2be */
final String v6 = "previous-improve"; // const-string v6, "previous-improve"
/* move-object v1, p0 */
/* move-object v2, v0 */
/* move v5, p2 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/am/OomAdjusterImpl;->modifyProcessRecordAdj(Lcom/android/server/am/ProcessStateRecord;ZIILjava/lang/String;)V */
/* .line 193 */
/* .line 195 */
} // :cond_3
/* sget-boolean v1, Lcom/android/server/am/OomAdjusterImpl;->IMPROVE_RESIDENT_APP_ADJ_ENABLE:Z */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 196 */
v1 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/am/OomAdjusterImpl;->computeOomAdjForResidentApp(Lcom/android/server/am/ProcessRecord;I)Z */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 197 */
/* .line 200 */
} // .end local v0 # "appState":Lcom/android/server/am/ProcessStateRecord;
} // .end local v8 # "backgroundTime":J
} // :cond_4
int v0 = 0; // const/4 v0, 0x0
} // .end method
private final Boolean computeWidgetAdj ( com.android.server.am.ProcessRecord p0, Integer p1 ) {
/* .locals 10 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "adj" # I */
/* .line 291 */
v0 = this.processName;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = this.processName;
final String v2 = ":widgetProvider"; // const-string v2, ":widgetProvider"
v0 = (( java.lang.String ) v0 ).endsWith ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 292 */
/* const-wide/16 v2, 0x0 */
/* if-nez p2, :cond_1 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/am/OomAdjusterImpl;->isRunningWidgetBroadcast(Lcom/android/server/am/ProcessRecord;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 294 */
v0 = this.mState;
(( com.android.server.am.ProcessStateRecord ) v0 ).getLastSwitchToTopTime ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getLastSwitchToTopTime()J
/* move-result-wide v4 */
/* cmp-long v0, v4, v2 */
/* if-gtz v0, :cond_0 */
/* .line 295 */
v0 = this.mState;
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v2 */
(( com.android.server.am.ProcessStateRecord ) v0 ).setLastSwitchToTopTime ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lcom/android/server/am/ProcessStateRecord;->setLastSwitchToTopTime(J)V
/* .line 297 */
/* .line 298 */
} // :cond_0
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v2 */
v0 = this.mState;
/* .line 299 */
(( com.android.server.am.ProcessStateRecord ) v0 ).getLastSwitchToTopTime ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getLastSwitchToTopTime()J
/* move-result-wide v4 */
/* sub-long/2addr v2, v4 */
/* const-wide/16 v4, 0xbb8 */
/* cmp-long v0, v2, v4 */
/* if-gez v0, :cond_2 */
/* .line 301 */
/* .line 305 */
} // :cond_1
v0 = this.mState;
(( com.android.server.am.ProcessStateRecord ) v0 ).setLastSwitchToTopTime ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lcom/android/server/am/ProcessStateRecord;->setLastSwitchToTopTime(J)V
/* .line 308 */
} // :cond_2
v0 = this.mState;
/* .line 309 */
/* .local v0, "prcState":Lcom/android/server/am/ProcessStateRecord; */
v6 = (( com.android.server.am.ProcessStateRecord ) v0 ).isCached ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->isCached()Z
/* const/16 v7, 0x3e7 */
/* const/16 v8, 0x13 */
/* const-string/jumbo v9, "widget-degenerate" */
/* move-object v4, p0 */
/* move-object v5, v0 */
/* invoke-direct/range {v4 ..v9}, Lcom/android/server/am/OomAdjusterImpl;->modifyProcessRecordAdj(Lcom/android/server/am/ProcessStateRecord;ZIILjava/lang/String;)V */
/* .line 312 */
int v1 = 1; // const/4 v1, 0x1
/* .line 314 */
} // .end local v0 # "prcState":Lcom/android/server/am/ProcessStateRecord;
} // :cond_3
} // .end method
private static Integer getBackgroundAppCount ( ) {
/* .locals 6 */
/* .line 113 */
/* const-wide/32 v0, 0x40000000 */
/* .line 114 */
/* .local v0, "GB":J */
android.os.Process .getTotalMemory ( );
/* move-result-wide v2 */
/* .line 115 */
/* .local v2, "totalMemByte":J */
/* const-wide/16 v4, 0xc */
/* mul-long/2addr v4, v0 */
/* cmp-long v4, v2, v4 */
/* if-lez v4, :cond_0 */
/* .line 116 */
/* const/16 v4, 0xf */
/* .line 117 */
} // :cond_0
/* const-wide/16 v4, 0x8 */
/* mul-long/2addr v4, v0 */
/* cmp-long v4, v2, v4 */
/* if-lez v4, :cond_1 */
/* .line 118 */
/* const/16 v4, 0xa */
/* .line 120 */
} // :cond_1
int v4 = 0; // const/4 v4, 0x0
} // .end method
public static com.android.server.am.OomAdjusterImpl getInstance ( ) {
/* .locals 1 */
/* .line 165 */
com.android.server.am.OomAdjusterStub .getInstance ( );
/* check-cast v0, Lcom/android/server/am/OomAdjusterImpl; */
} // .end method
private static Integer getResidentAppCount ( ) {
/* .locals 5 */
/* .line 124 */
/* sget-wide v0, Lcom/android/server/am/OomAdjusterImpl;->TOTAL_MEMORY:J */
/* const-wide/16 v2, 0x6 */
/* cmp-long v2, v0, v2 */
/* const-wide/16 v3, 0x8 */
/* if-lez v2, :cond_0 */
/* cmp-long v2, v0, v3 */
/* if-gtz v2, :cond_0 */
/* .line 125 */
/* .line 126 */
} // :cond_0
/* cmp-long v2, v0, v3 */
/* const-wide/16 v3, 0xc */
/* if-lez v2, :cond_1 */
/* cmp-long v2, v0, v3 */
/* if-gtz v2, :cond_1 */
/* .line 127 */
/* .line 128 */
} // :cond_1
/* cmp-long v0, v0, v3 */
/* if-lez v0, :cond_2 */
/* .line 129 */
/* .line 131 */
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void improveOomAdjForAudioProcess ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 4 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 390 */
v0 = com.android.server.am.OomAdjusterImpl.QQ_PLAYER;
v1 = this.processName;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 391 */
v0 = this.mState;
/* .line 392 */
/* .local v0, "prcState":Lcom/android/server/am/ProcessStateRecord; */
com.android.server.am.SystemPressureController .getInstance ( );
/* iget v2, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
v3 = (( com.android.server.am.ProcessRecord ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I
v1 = (( com.android.server.am.SystemPressureController ) v1 ).isAudioOrGPSProc ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/am/SystemPressureController;->isAudioOrGPSProc(II)Z
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = this.mServices;
/* .line 393 */
v1 = (( com.android.server.am.ProcessServiceRecord ) v1 ).hasForegroundServices ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessServiceRecord;->hasForegroundServices()Z
/* if-nez v1, :cond_0 */
/* .line 394 */
/* const/16 v1, 0xc8 */
(( com.android.server.am.ProcessStateRecord ) v0 ).setMaxAdj ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessStateRecord;->setMaxAdj(I)V
/* .line 396 */
} // :cond_0
(( com.android.server.am.ProcessStateRecord ) v0 ).setMaxAdj ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessStateRecord;->setMaxAdj(I)V
/* .line 399 */
} // .end local v0 # "prcState":Lcom/android/server/am/ProcessStateRecord;
} // :cond_1
} // :goto_0
return;
} // .end method
private Boolean improveOomAdjForCamera ( com.android.server.am.ProcessRecord p0, com.android.server.am.ProcessRecord p1, Integer p2, Integer p3 ) {
/* .locals 7 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "topApp" # Lcom/android/server/am/ProcessRecord; */
/* .param p3, "adj" # I */
/* .param p4, "procState" # I */
/* .line 331 */
if ( p2 != null) { // if-eqz p2, :cond_0
v0 = this.processName;
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x190 */
/* if-le p3, v0, :cond_0 */
/* if-eq p1, p2, :cond_0 */
/* .line 334 */
v0 = (( com.android.server.am.ProcessRecord ) p1 ).hasActivities ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->hasActivities()Z
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.processName;
/* .line 335 */
final String v1 = "com.android.camera"; // const-string v1, "com.android.camera"
v0 = android.text.TextUtils .equals ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.processName;
v1 = this.mForegroundResultToProc;
/* .line 336 */
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 337 */
v2 = this.mState;
v0 = this.mState;
v3 = (( com.android.server.am.ProcessStateRecord ) v0 ).isCached ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->isCached()Z
/* const/16 v4, 0x190 */
/* const/16 v5, 0xd */
final String v6 = "camera-improve"; // const-string v6, "camera-improve"
/* move-object v1, p0 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/am/OomAdjusterImpl;->modifyProcessRecordAdj(Lcom/android/server/am/ProcessStateRecord;ZIILjava/lang/String;)V */
/* .line 340 */
int v0 = 1; // const/4 v0, 0x1
/* .line 342 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public static Boolean isCacheProcessState ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "procState" # I */
/* .line 595 */
/* const/16 v0, 0x10 */
/* if-eq p0, v0, :cond_1 */
/* const/16 v0, 0x11 */
/* if-eq p0, v0, :cond_1 */
/* const/16 v0, 0x12 */
/* if-ne p0, v0, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
private Boolean isProcessPerceptible ( Integer p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "processName" # Ljava/lang/String; */
/* .line 760 */
v0 = this.mSmartPowerService;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = /* .line 761 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 760 */
} // :goto_0
} // .end method
private Boolean isRunningWidgetBroadcast ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 4 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 278 */
com.android.server.am.ActivityManagerServiceImpl .getInstance ( );
v1 = this.processName;
/* iget v2, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
/* .line 279 */
(( com.android.server.am.ActivityManagerServiceImpl ) v0 ).getBroadcastProcessQueue ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/am/ActivityManagerServiceImpl;->getBroadcastProcessQueue(Ljava/lang/String;I)Lcom/android/server/am/BroadcastProcessQueue;
/* .line 280 */
/* .local v0, "brcProcessQueue":Lcom/android/server/am/BroadcastProcessQueue; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( com.android.server.am.BroadcastProcessQueue ) v0 ).isActive ( ); // invoke-virtual {v0}, Lcom/android/server/am/BroadcastProcessQueue;->isActive()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 281 */
(( com.android.server.am.BroadcastProcessQueue ) v0 ).getActive ( ); // invoke-virtual {v0}, Lcom/android/server/am/BroadcastProcessQueue;->getActive()Lcom/android/server/am/BroadcastRecord;
/* .line 282 */
/* .local v1, "brc":Lcom/android/server/am/BroadcastRecord; */
if ( v1 != null) { // if-eqz v1, :cond_0
v2 = this.intent;
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = com.android.server.am.OomAdjusterImpl.mWidgetProcBCWhiteList;
v3 = this.intent;
/* .line 283 */
(( android.content.Intent ) v3 ).getAction ( ); // invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v2 = (( android.util.ArraySet ) v2 ).contains ( v3 ); // invoke-virtual {v2, v3}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 284 */
int v2 = 1; // const/4 v2, 0x1
/* .line 287 */
} // .end local v1 # "brc":Lcom/android/server/am/BroadcastRecord;
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
private Boolean isSpecialApp ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 263 */
final String v0 = "com.android.camera"; // const-string v0, "com.android.camera"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
final String v0 = "com.tencent.mm"; // const-string v0, "com.tencent.mm"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 264 */
com.android.server.am.SystemPressureController .getInstance ( );
v0 = (( com.android.server.am.SystemPressureController ) v0 ).isGameApp ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/SystemPressureController;->isGameApp(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 263 */
} // :goto_1
} // .end method
private void modifyProcessRecordAdj ( com.android.server.am.ProcessStateRecord p0, Boolean p1, Integer p2, Integer p3, java.lang.String p4 ) {
/* .locals 0 */
/* .param p1, "prcState" # Lcom/android/server/am/ProcessStateRecord; */
/* .param p2, "cached" # Z */
/* .param p3, "CurRawAdj" # I */
/* .param p4, "procState" # I */
/* .param p5, "AdjType" # Ljava/lang/String; */
/* .line 426 */
(( com.android.server.am.ProcessStateRecord ) p1 ).setCached ( p2 ); // invoke-virtual {p1, p2}, Lcom/android/server/am/ProcessStateRecord;->setCached(Z)V
/* .line 427 */
(( com.android.server.am.ProcessStateRecord ) p1 ).setCurRawAdj ( p3 ); // invoke-virtual {p1, p3}, Lcom/android/server/am/ProcessStateRecord;->setCurRawAdj(I)V
/* .line 428 */
(( com.android.server.am.ProcessStateRecord ) p1 ).setCurRawProcState ( p4 ); // invoke-virtual {p1, p4}, Lcom/android/server/am/ProcessStateRecord;->setCurRawProcState(I)V
/* .line 429 */
(( com.android.server.am.ProcessStateRecord ) p1 ).setAdjType ( p5 ); // invoke-virtual {p1, p5}, Lcom/android/server/am/ProcessStateRecord;->setAdjType(Ljava/lang/String;)V
/* .line 430 */
return;
} // .end method
private void updateBackgroundAppList ( ) {
/* .locals 7 */
/* .line 221 */
v0 = this.mSmartPowerService;
/* .line 222 */
/* .local v0, "runningAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;" */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 223 */
/* .local v1, "backgroundRunningAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
(( java.util.ArrayList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 224 */
/* .local v3, "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v3 ).getProcessName ( ); // invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;
v4 = (( java.lang.String ) v4 ).equals ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 225 */
v4 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v3 ).hasActivity ( ); // invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->hasActivity()Z
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 226 */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v3 ).getProcessName ( ); // invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;
(( java.util.ArrayList ) v1 ).add ( v4 ); // invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 228 */
} // .end local v3 # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
} // :cond_0
/* .line 229 */
} // :cond_1
v2 = this.mSmartPowerService;
/* .line 230 */
/* .local v2, "AppStateList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/IAppState;>;" */
/* new-instance v3, Ljava/util/ArrayList; */
/* invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V */
/* .line 231 */
/* .local v3, "backgroundAppStateList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/IAppState;>;" */
v5 = } // :goto_1
if ( v5 != null) { // if-eqz v5, :cond_3
/* check-cast v5, Lcom/miui/server/smartpower/IAppState; */
/* .line 232 */
/* .local v5, "appState":Lcom/miui/server/smartpower/IAppState; */
(( com.miui.server.smartpower.IAppState ) v5 ).getPackageName ( ); // invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState;->getPackageName()Ljava/lang/String;
v6 = (( java.util.ArrayList ) v1 ).contains ( v6 ); // invoke-virtual {v1, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 233 */
v6 = (( com.miui.server.smartpower.IAppState ) v5 ).isSystemApp ( ); // invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState;->isSystemApp()Z
/* if-nez v6, :cond_2 */
/* .line 234 */
(( com.miui.server.smartpower.IAppState ) v5 ).getPackageName ( ); // invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState;->getPackageName()Ljava/lang/String;
v6 = /* invoke-direct {p0, v6}, Lcom/android/server/am/OomAdjusterImpl;->isSpecialApp(Ljava/lang/String;)Z */
/* if-nez v6, :cond_2 */
/* .line 235 */
/* .line 237 */
} // .end local v5 # "appState":Lcom/miui/server/smartpower/IAppState;
} // :cond_2
/* .line 238 */
} // :cond_3
/* new-instance v4, Lcom/android/server/am/OomAdjusterImpl$1; */
/* invoke-direct {v4, p0}, Lcom/android/server/am/OomAdjusterImpl$1;-><init>(Lcom/android/server/am/OomAdjusterImpl;)V */
java.util.Collections .sort ( v3,v4 );
/* .line 252 */
v4 = com.android.server.am.OomAdjusterImpl.sPreviousBackgroundAppList;
/* .line 253 */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
v5 = } // :goto_2
/* if-ge v4, v5, :cond_5 */
/* .line 254 */
/* check-cast v5, Lcom/miui/server/smartpower/IAppState; */
(( com.miui.server.smartpower.IAppState ) v5 ).getPackageName ( ); // invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState;->getPackageName()Ljava/lang/String;
/* .line 255 */
/* .local v5, "theProcess":Ljava/lang/String; */
v6 = v6 = com.android.server.am.OomAdjusterImpl.sPreviousBackgroundAppList;
if ( v6 != null) { // if-eqz v6, :cond_4
/* .line 256 */
/* .line 258 */
} // :cond_4
v6 = com.android.server.am.OomAdjusterImpl.sPreviousBackgroundAppList;
/* .line 253 */
} // .end local v5 # "theProcess":Ljava/lang/String;
} // :goto_3
/* add-int/lit8 v4, v4, 0x1 */
/* .line 260 */
} // .end local v4 # "i":I
} // :cond_5
return;
} // .end method
/* # virtual methods */
public void applyOomAdjLocked ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 1 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 556 */
v0 = this.mSmartPowerService;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 557 */
/* .line 559 */
} // :cond_0
return;
} // .end method
public void compactBackgroundProcess ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 6 */
/* .param p1, "proc" # Lcom/android/server/am/ProcessRecord; */
/* .line 602 */
v0 = this.mState;
/* .line 603 */
/* .local v0, "state":Lcom/android/server/am/ProcessStateRecord; */
v1 = (( com.android.server.am.ProcessStateRecord ) v0 ).getCurProcState ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getCurProcState()I
v2 = (( com.android.server.am.ProcessStateRecord ) v0 ).getSetProcState ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getSetProcState()I
/* if-eq v1, v2, :cond_0 */
/* .line 604 */
v1 = (( com.android.server.am.ProcessStateRecord ) v0 ).getSetProcState ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getSetProcState()I
/* .line 605 */
/* .local v1, "oldState":I */
v2 = (( com.android.server.am.ProcessStateRecord ) v0 ).getCurProcState ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getCurProcState()I
/* .line 606 */
/* .local v2, "newState":I */
v3 = com.android.server.am.OomAdjusterImpl .isCacheProcessState ( v1 );
/* if-nez v3, :cond_0 */
v3 = com.android.server.am.OomAdjusterImpl .isCacheProcessState ( v2 );
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 607 */
com.android.server.am.SystemPressureController .getInstance ( );
/* iget v4, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
v5 = this.processName;
(( com.android.server.am.SystemPressureController ) v3 ).compactBackgroundProcess ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Lcom/android/server/am/SystemPressureController;->compactBackgroundProcess(ILjava/lang/String;)V
/* .line 611 */
} // .end local v1 # "oldState":I
} // .end local v2 # "newState":I
} // :cond_0
return;
} // .end method
public Integer computeBindServiceAdj ( com.android.server.am.ProcessRecord p0, Integer p1, com.android.server.am.ConnectionRecord p2 ) {
/* .locals 15 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "adj" # I */
/* .param p3, "connectService" # Lcom/android/server/am/ConnectionRecord; */
/* .line 563 */
/* move-object/from16 v0, p1 */
/* move-object/from16 v1, p3 */
v2 = this.binding;
v2 = this.client;
/* .line 564 */
/* .local v2, "client":Lcom/android/server/am/ProcessRecord; */
v3 = this.mState;
/* .line 565 */
/* .local v3, "cstate":Lcom/android/server/am/ProcessStateRecord; */
v4 = this.processName;
v4 = com.miui.server.process.ProcessManagerInternal .checkCtsProcess ( v4 );
/* const/16 v5, 0x64 */
/* if-nez v4, :cond_8 */
/* sget-boolean v4, Lcom/android/server/am/OomAdjusterImpl;->LIMIT_BIND_VEISIBLE_ENABLED:Z */
/* if-nez v4, :cond_0 */
/* move-object v9, p0 */
/* .line 570 */
} // :cond_0
v4 = (( com.android.server.am.ProcessStateRecord ) v3 ).getCurRawAdj ( ); // invoke-virtual {v3}, Lcom/android/server/am/ProcessStateRecord;->getCurRawAdj()I
/* .line 571 */
/* .local v4, "clientAdj":I */
v6 = this.info;
/* iget v6, v6, Landroid/content/pm/ApplicationInfo;->flags:I */
/* and-int/lit16 v6, v6, 0x81 */
if ( v6 != null) { // if-eqz v6, :cond_1
int v6 = 1; // const/4 v6, 0x1
} // :cond_1
int v6 = 0; // const/4 v6, 0x0
/* .line 573 */
/* .local v6, "isSystem":Z */
} // :goto_0
/* iget-wide v7, v1, Lcom/android/server/am/ConnectionRecord;->flags:J */
/* const-wide/32 v9, 0x6000000 */
/* and-long/2addr v7, v9 */
/* const-wide/16 v9, 0x0 */
/* cmp-long v7, v7, v9 */
/* const/16 v8, 0xc8 */
/* if-nez v7, :cond_6 */
/* if-gez v4, :cond_2 */
/* move-object v9, p0 */
/* .line 583 */
} // :cond_2
/* iget-wide v11, v1, Lcom/android/server/am/ConnectionRecord;->flags:J */
/* const-wide/16 v13, 0x1 */
/* and-long/2addr v11, v13 */
/* cmp-long v5, v11, v9 */
if ( v5 != null) { // if-eqz v5, :cond_4
/* if-ge v4, v8, :cond_4 */
/* iget v5, v2, Lcom/android/server/am/ProcessRecord;->uid:I */
v7 = this.processName;
/* .line 585 */
/* move-object v9, p0 */
v5 = /* invoke-direct {p0, v5, v7}, Lcom/android/server/am/OomAdjusterImpl;->isProcessPerceptible(ILjava/lang/String;)Z */
/* if-nez v5, :cond_3 */
/* sget-boolean v5, Lcom/miui/app/smartpower/SmartPowerPolicyConstants;->TESTSUITSPECIFIC:Z */
if ( v5 != null) { // if-eqz v5, :cond_5
/* .line 588 */
} // :cond_3
/* const/16 v5, 0xfa */
} // .end local p2 # "adj":I
/* .local v5, "adj":I */
/* .line 583 */
} // .end local v5 # "adj":I
/* .restart local p2 # "adj":I */
} // :cond_4
/* move-object v9, p0 */
/* .line 591 */
} // :cond_5
/* move/from16 v5, p2 */
/* .line 573 */
} // :cond_6
/* move-object v9, p0 */
/* .line 578 */
} // :goto_1
if ( v6 != null) { // if-eqz v6, :cond_7
/* .line 579 */
v5 = java.lang.Math .max ( v4,v5 );
} // .end local p2 # "adj":I
/* .restart local v5 # "adj":I */
/* .line 581 */
} // .end local v5 # "adj":I
/* .restart local p2 # "adj":I */
} // :cond_7
v5 = java.lang.Math .max ( v4,v8 );
/* .line 591 */
} // .end local p2 # "adj":I
/* .restart local v5 # "adj":I */
} // :goto_2
/* .line 565 */
} // .end local v4 # "clientAdj":I
} // .end local v5 # "adj":I
} // .end local v6 # "isSystem":Z
/* .restart local p2 # "adj":I */
} // :cond_8
/* move-object v9, p0 */
/* .line 567 */
} // :goto_3
v4 = (( com.android.server.am.ProcessStateRecord ) v3 ).getCurRawAdj ( ); // invoke-virtual {v3}, Lcom/android/server/am/ProcessStateRecord;->getCurRawAdj()I
v4 = java.lang.Math .max ( v4,v5 );
} // .end method
public Boolean computeOomAdjLocked ( com.android.server.am.ProcessRecord p0, com.android.server.am.ProcessRecord p1, Integer p2, Integer p3, Boolean p4 ) {
/* .locals 5 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "topApp" # Lcom/android/server/am/ProcessRecord; */
/* .param p3, "adj" # I */
/* .param p4, "procState" # I */
/* .param p5, "cycleReEval" # Z */
/* .line 435 */
/* sget-boolean v0, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_OOM_ADJ:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 436 */
/* if-ne p1, p2, :cond_0 */
/* const-string/jumbo v0, "true" */
} // :cond_0
final String v0 = "false"; // const-string v0, "false"
/* .line 437 */
/* .local v0, "isTop":Ljava/lang/String; */
} // :goto_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "computeOomAdjLocked processName = "; // const-string v2, "computeOomAdjLocked processName = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.processName;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " rawAdj="; // const-string v2, " rawAdj="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " maxAdj="; // const-string v2, " maxAdj="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mState;
/* .line 439 */
v2 = (( com.android.server.am.ProcessStateRecord ) v2 ).getMaxAdj ( ); // invoke-virtual {v2}, Lcom/android/server/am/ProcessStateRecord;->getMaxAdj()I
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " procState="; // const-string v2, " procState="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p4 ); // invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " maxProcState="; // const-string v2, " maxProcState="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mState;
/* iget v2, v2, Lcom/android/server/am/ProcessStateRecord;->mMaxProcState:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " adjType="; // const-string v2, " adjType="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mState;
/* .line 442 */
(( com.android.server.am.ProcessStateRecord ) v2 ).getAdjType ( ); // invoke-virtual {v2}, Lcom/android/server/am/ProcessStateRecord;->getAdjType()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " isTop="; // const-string v2, " isTop="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " cycleReEval="; // const-string v2, " cycleReEval="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p5 ); // invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 437 */
final String v2 = "ActivityManager"; // const-string v2, "ActivityManager"
android.util.Slog .i ( v2,v1 );
/* .line 446 */
} // .end local v0 # "isTop":Ljava/lang/String;
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* if-gez p3, :cond_2 */
/* .line 447 */
/* .line 450 */
} // :cond_2
v1 = this.mSmartPowerService;
int v2 = 1; // const/4 v2, 0x1
if ( v1 != null) { // if-eqz v1, :cond_3
v1 = this.info;
/* iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I */
/* const/16 v3, 0x2710 */
/* if-le v1, v3, :cond_3 */
/* const/16 v1, 0x2bc */
/* if-le p3, v1, :cond_3 */
v1 = this.info;
v1 = this.packageName;
v3 = this.processName;
/* .line 452 */
v1 = (( com.android.server.am.OomAdjusterImpl ) p0 ).isImportantSubProc ( v1, v3 ); // invoke-virtual {p0, v1, v3}, Lcom/android/server/am/OomAdjusterImpl;->isImportantSubProc(Ljava/lang/String;Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 453 */
v1 = this.mSmartPowerService;
v3 = this.info;
/* iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 454 */
/* .local v1, "appState":Lcom/miui/server/smartpower/IAppState; */
if ( v1 != null) { // if-eqz v1, :cond_3
v3 = this.info;
v3 = this.packageName;
(( com.miui.server.smartpower.IAppState ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Lcom/miui/server/smartpower/IAppState;->getPackageName()Ljava/lang/String;
v3 = (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 455 */
v0 = this.mState;
v3 = (( com.miui.server.smartpower.IAppState ) v1 ).getMainProcAdj ( ); // invoke-virtual {v1}, Lcom/miui/server/smartpower/IAppState;->getMainProcAdj()I
/* const/16 v4, 0xc8 */
v3 = java.lang.Math .max ( v3,v4 );
(( com.android.server.am.ProcessStateRecord ) v0 ).setCurRawAdj ( v3 ); // invoke-virtual {v0, v3}, Lcom/android/server/am/ProcessStateRecord;->setCurRawAdj(I)V
/* .line 457 */
v0 = this.mState;
final String v3 = "bind-fix"; // const-string v3, "bind-fix"
(( com.android.server.am.ProcessStateRecord ) v0 ).setAdjType ( v3 ); // invoke-virtual {v0, v3}, Lcom/android/server/am/ProcessStateRecord;->setAdjType(Ljava/lang/String;)V
/* .line 458 */
/* .line 461 */
} // .end local v1 # "appState":Lcom/miui/server/smartpower/IAppState;
} // :cond_3
v1 = /* invoke-direct {p0, p1, p3}, Lcom/android/server/am/OomAdjusterImpl;->computeWidgetAdj(Lcom/android/server/am/ProcessRecord;I)Z */
/* if-nez v1, :cond_4 */
/* .line 462 */
v1 = /* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/am/OomAdjusterImpl;->improveOomAdjForCamera(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;II)Z */
if ( v1 != null) { // if-eqz v1, :cond_5
} // :cond_4
/* move v0, v2 */
/* .line 463 */
/* .local v0, "isChangeAdj":Z */
} // :cond_5
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 464 */
/* .line 466 */
} // :cond_6
v1 = (( com.android.server.am.ProcessRecord ) p1 ).isKilled ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isKilled()Z
/* if-nez v1, :cond_9 */
v1 = (( com.android.server.am.ProcessRecord ) p1 ).isKilledByAm ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isKilledByAm()Z
/* if-nez v1, :cond_9 */
(( com.android.server.am.ProcessRecord ) p1 ).getThread ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
if ( v1 != null) { // if-eqz v1, :cond_9
/* if-lez p3, :cond_9 */
/* .line 468 */
/* packed-switch p4, :pswitch_data_0 */
/* :pswitch_0 */
/* .line 480 */
/* :pswitch_1 */
/* const/16 v1, 0x320 */
/* if-le p3, v1, :cond_8 */
/* .line 481 */
v2 = com.android.server.am.ProcessProphetStub .getInstance ( );
if ( v2 != null) { // if-eqz v2, :cond_8
/* .line 482 */
v2 = this.mState;
(( com.android.server.am.ProcessStateRecord ) v2 ).setCurRawAdj ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/am/ProcessStateRecord;->setCurRawAdj(I)V
/* .line 483 */
v1 = this.mState;
final String v2 = "proc-prophet"; // const-string v2, "proc-prophet"
(( com.android.server.am.ProcessStateRecord ) v1 ).setAdjType ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessStateRecord;->setAdjType(Ljava/lang/String;)V
/* .line 484 */
int v0 = 1; // const/4 v0, 0x1
/* .line 474 */
/* :pswitch_2 */
/* const/16 v1, 0x3e9 */
/* if-ge p3, v1, :cond_7 */
v1 = this.mServices;
/* .line 475 */
v1 = (( com.android.server.am.ProcessServiceRecord ) v1 ).hasAboveClient ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessServiceRecord;->hasAboveClient()Z
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 476 */
} // :cond_7
v0 = /* invoke-direct {p0, p1, p4, p3}, Lcom/android/server/am/OomAdjusterImpl;->computePreviousAdj(Lcom/android/server/am/ProcessRecord;II)Z */
/* .line 490 */
} // :cond_8
} // :goto_1
/* invoke-direct {p0, p1}, Lcom/android/server/am/OomAdjusterImpl;->improveOomAdjForAudioProcess(Lcom/android/server/am/ProcessRecord;)V */
/* .line 492 */
} // :cond_9
/* :pswitch_data_0 */
/* .packed-switch 0xa */
/* :pswitch_2 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_2 */
/* :pswitch_2 */
/* :pswitch_2 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
public void dumpPreviousBackgroundApps ( java.io.PrintWriter p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "prefix" # Ljava/lang/String; */
/* .line 268 */
v0 = v0 = com.android.server.am.OomAdjusterImpl.sPreviousBackgroundAppList;
/* if-lez v0, :cond_0 */
/* .line 269 */
final String v0 = "BGAL:"; // const-string v0, "BGAL:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 270 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = v1 = com.android.server.am.OomAdjusterImpl.sPreviousBackgroundAppList;
/* if-ge v0, v1, :cond_0 */
/* .line 271 */
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 272 */
v1 = com.android.server.am.OomAdjusterImpl.sPreviousBackgroundAppList;
/* check-cast v1, Ljava/lang/String; */
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 270 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 275 */
} // .end local v0 # "i":I
} // :cond_0
return;
} // .end method
public void dumpPreviousResidentApps ( java.io.PrintWriter p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "prefix" # Ljava/lang/String; */
/* .line 528 */
v0 = v0 = com.android.server.am.OomAdjusterImpl.sPreviousResidentAppList;
/* if-lez v0, :cond_0 */
/* .line 529 */
final String v0 = "PRAL:"; // const-string v0, "PRAL:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 530 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = v1 = com.android.server.am.OomAdjusterImpl.sPreviousResidentAppList;
/* if-ge v0, v1, :cond_0 */
/* .line 531 */
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 532 */
v1 = com.android.server.am.OomAdjusterImpl.sPreviousResidentAppList;
/* check-cast v1, Ljava/lang/String; */
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 530 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 535 */
} // .end local v0 # "i":I
} // :cond_0
return;
} // .end method
public void foregroundInfoChanged ( java.lang.String p0, android.content.ComponentName p1, java.lang.String p2 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "component" # Landroid/content/ComponentName; */
/* .param p3, "resultToProc" # Ljava/lang/String; */
/* .line 689 */
v0 = this.mForegroundPkg;
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 690 */
com.android.server.am.SystemPressureController .getInstance ( );
v0 = (( com.android.server.am.SystemPressureController ) v0 ).isGameApp ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/SystemPressureController;->isGameApp(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 691 */
v0 = com.android.server.am.OomAdjusterImpl.mRunningGameMap;
int v1 = 0; // const/4 v1, 0x0
java.lang.Integer .valueOf ( v1 );
(( com.android.server.am.OomAdjusterImpl$RunningAppRecordMap ) v0 ).put ( v1, p1, v2 ); // invoke-virtual {v0, v1, p1, v2}, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;->put(ILjava/lang/String;Ljava/lang/Integer;)V
/* .line 693 */
} // :cond_0
this.mForegroundPkg = p1;
/* .line 694 */
this.mForegroundResultToProc = p3;
/* .line 696 */
} // :cond_1
return;
} // .end method
public Boolean isImportantSubProc ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "procName" # Ljava/lang/String; */
/* .line 538 */
v0 = com.android.server.am.OomAdjusterImpl.sSubProcessAdjBindList;
/* check-cast v0, Ljava/util/List; */
/* .line 539 */
/* .local v0, "procList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v1 = if ( v0 != null) { // if-eqz v0, :cond_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 540 */
int v1 = 1; // const/4 v1, 0x1
/* .line 542 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean isPaymentScene ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 3 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 402 */
v0 = com.android.server.am.OomAdjusterImpl.sPaymentAppList;
v0 = v1 = this.mForegroundPkg;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mSmartPowerService;
v1 = this.info;
/* iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 403 */
v0 = v2 = (( com.android.server.am.ProcessRecord ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mSmartPowerService;
/* .line 404 */
v0 = final String v1 = "com.xiaomi.gamecenter.sdk.service"; // const-string v1, "com.xiaomi.gamecenter.sdk.service"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 405 */
int v0 = 1; // const/4 v0, 0x1
/* .line 407 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isSetUntrustedBgGroup ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 1 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 618 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void notifyProcessDied ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 2 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 699 */
v0 = this.info;
v0 = this.packageName;
v1 = this.processName;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 700 */
v0 = com.android.server.am.OomAdjusterImpl.mRunningGameMap;
v1 = this.info;
v1 = this.packageName;
(( com.android.server.am.OomAdjusterImpl$RunningAppRecordMap ) v0 ).removeForKey ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;->removeForKey(Ljava/lang/String;)V
/* .line 702 */
} // :cond_0
return;
} // .end method
public void onSystemReady ( ) {
/* .locals 1 */
/* .line 169 */
/* const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
this.mSmartPowerService = v0;
/* .line 170 */
return;
} // .end method
public void resetProperThreadPriority ( com.android.server.am.ProcessRecord p0, Integer p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "tid" # I */
/* .param p3, "prio" # I */
/* .line 650 */
com.android.server.wm.RealTimeModeControllerImpl .get ( );
v0 = (( com.android.server.wm.RealTimeModeControllerImpl ) v0 ).checkThreadBoost ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/wm/RealTimeModeControllerImpl;->checkThreadBoost(I)Z
/* .line 651 */
/* .local v0, "boosted":Z */
if ( p2 != null) { // if-eqz p2, :cond_0
/* if-nez v0, :cond_0 */
/* .line 652 */
/* invoke-super {p0, p1, p2, p3}, Lcom/android/server/am/OomAdjusterStub;->resetProperThreadPriority(Lcom/android/server/am/ProcessRecord;II)V */
/* .line 654 */
} // :cond_0
com.android.server.wm.RealTimeModeControllerImpl .get ( );
/* filled-new-array {p2}, [I */
/* .line 655 */
(( com.android.server.wm.RealTimeModeControllerImpl ) v1 ).setThreadSavedPriority ( v2, p3 ); // invoke-virtual {v1, v2, p3}, Lcom/android/server/wm/RealTimeModeControllerImpl;->setThreadSavedPriority([II)V
/* .line 656 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
v2 = this.processName;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " boosting, skip reset priority"; // const-string v2, " boosting, skip reset priority"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "SchedBoost"; // const-string v2, "SchedBoost"
android.util.Slog .d ( v2,v1 );
/* .line 659 */
} // :goto_0
return;
} // .end method
public void setProperThreadPriority ( com.android.server.am.ProcessRecord p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 3 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "pid" # I */
/* .param p3, "renderThreadTid" # I */
/* .param p4, "prio" # I */
/* .line 636 */
com.android.server.wm.RealTimeModeControllerImpl .get ( );
v0 = (( com.android.server.wm.RealTimeModeControllerImpl ) v0 ).checkThreadBoost ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/wm/RealTimeModeControllerImpl;->checkThreadBoost(I)Z
/* .line 637 */
/* .local v0, "boosted":Z */
/* if-nez v0, :cond_0 */
/* .line 638 */
/* invoke-super {p0, p1, p2, p3, p4}, Lcom/android/server/am/OomAdjusterStub;->setProperThreadPriority(Lcom/android/server/am/ProcessRecord;III)V */
/* .line 640 */
} // :cond_0
com.android.server.wm.RealTimeModeControllerImpl .get ( );
/* filled-new-array {p2, p3}, [I */
(( com.android.server.wm.RealTimeModeControllerImpl ) v1 ).setThreadSavedPriority ( v2, p4 ); // invoke-virtual {v1, v2, p4}, Lcom/android/server/wm/RealTimeModeControllerImpl;->setThreadSavedPriority([II)V
/* .line 642 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
v2 = this.processName;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " already boosted, skip boost priority"; // const-string v2, " already boosted, skip boost priority"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "SchedBoost"; // const-string v2, "SchedBoost"
android.util.Slog .d ( v2,v1 );
/* .line 645 */
} // :goto_0
return;
} // .end method
public Boolean shouldSkipDueToBind ( com.android.server.am.ProcessRecord p0, com.android.server.am.ProcessRecord p1 ) {
/* .locals 2 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "client" # Lcom/android/server/am/ProcessRecord; */
/* .line 547 */
v0 = this.info;
v0 = this.packageName;
v1 = this.processName;
v0 = (( com.android.server.am.OomAdjusterImpl ) p0 ).isImportantSubProc ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/am/OomAdjusterImpl;->isImportantSubProc(Ljava/lang/String;Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.processName;
v1 = this.info;
v1 = this.packageName;
/* .line 548 */
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 549 */
int v0 = 1; // const/4 v0, 0x1
/* .line 551 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void updateGameSceneRecordMap ( java.lang.String p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "gameScene" # I */
/* .param p3, "appState" # I */
/* .line 622 */
/* packed-switch p3, :pswitch_data_0 */
/* :pswitch_0 */
/* .line 625 */
/* :pswitch_1 */
v0 = com.android.server.am.OomAdjusterImpl.mRunningGameMap;
java.lang.Integer .valueOf ( p2 );
(( com.android.server.am.OomAdjusterImpl$RunningAppRecordMap ) v0 ).put ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;->put(Ljava/lang/String;Ljava/lang/Integer;)V
/* .line 626 */
/* nop */
/* .line 630 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x2 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
public void updateProcessAdjBindList ( java.lang.String p0 ) {
/* .locals 8 */
/* .param p1, "data" # Ljava/lang/String; */
/* .line 663 */
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_4 */
/* .line 664 */
v0 = com.android.server.am.OomAdjusterImpl.sSubProcessAdjBindList;
/* .line 665 */
final String v0 = ","; // const-string v0, ","
(( java.lang.String ) p1 ).split ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 666 */
/* .local v0, "pkgString":[Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* array-length v2, v0 */
/* if-ge v1, v2, :cond_3 */
/* .line 667 */
/* aget-object v2, v0, v1 */
v2 = android.text.TextUtils .isEmpty ( v2 );
/* if-nez v2, :cond_2 */
/* .line 668 */
/* aget-object v2, v0, v1 */
final String v3 = "-"; // const-string v3, "-"
(( java.lang.String ) v2 ).split ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 669 */
/* .local v2, "appString":[Ljava/lang/String; */
/* array-length v3, v2 */
int v4 = 2; // const/4 v4, 0x2
/* if-ne v3, v4, :cond_2 */
int v3 = 0; // const/4 v3, 0x0
/* aget-object v4, v2, v3 */
v4 = android.text.TextUtils .isEmpty ( v4 );
/* if-nez v4, :cond_2 */
int v4 = 1; // const/4 v4, 0x1
/* aget-object v5, v2, v4 */
/* .line 670 */
v5 = android.text.TextUtils .isEmpty ( v5 );
/* if-nez v5, :cond_2 */
/* .line 671 */
/* aget-object v4, v2, v4 */
final String v5 = "\\+"; // const-string v5, "\\+"
(( java.lang.String ) v4 ).split ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 672 */
/* .local v4, "procString":[Ljava/lang/String; */
/* new-instance v5, Ljava/util/ArrayList; */
/* invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V */
/* .line 673 */
/* .local v5, "procList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
int v6 = 0; // const/4 v6, 0x0
/* .local v6, "j":I */
} // :goto_1
/* array-length v7, v4 */
/* if-ge v6, v7, :cond_1 */
/* .line 674 */
/* aget-object v7, v4, v6 */
v7 = android.text.TextUtils .isEmpty ( v7 );
/* if-nez v7, :cond_0 */
/* .line 675 */
/* aget-object v7, v4, v6 */
/* .line 673 */
} // :cond_0
/* add-int/lit8 v6, v6, 0x1 */
/* .line 678 */
} // .end local v6 # "j":I
} // :cond_1
v6 = com.android.server.am.OomAdjusterImpl.sSubProcessAdjBindList;
/* aget-object v3, v2, v3 */
/* .line 666 */
} // .end local v2 # "appString":[Ljava/lang/String;
} // .end local v4 # "procString":[Ljava/lang/String;
} // .end local v5 # "procList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_2
/* add-int/lit8 v1, v1, 0x1 */
/* .line 682 */
} // .end local v1 # "i":I
} // :cond_3
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "subprocess adj bind list cloud control received: " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "ProcCloudControl"; // const-string v2, "ProcCloudControl"
android.util.Slog .d ( v2,v1 );
/* .line 685 */
} // .end local v0 # "pkgString":[Ljava/lang/String;
} // :cond_4
return;
} // .end method
public void updateResidentAppList ( java.util.List p0 ) {
/* .locals 7 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/smartpower/IAppState;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 496 */
/* .local p1, "appList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/IAppState;>;" */
/* if-nez p1, :cond_0 */
/* .line 497 */
return;
/* .line 499 */
} // :cond_0
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 500 */
/* .local v0, "residentList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/IAppState;>;" */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Lcom/miui/server/smartpower/IAppState; */
/* .line 501 */
/* .local v2, "appState":Lcom/miui/server/smartpower/IAppState; */
v3 = (( com.miui.server.smartpower.IAppState ) v2 ).isSystemApp ( ); // invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState;->isSystemApp()Z
/* if-nez v3, :cond_1 */
(( com.miui.server.smartpower.IAppState ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState;->getPackageName()Ljava/lang/String;
v3 = /* invoke-direct {p0, v3}, Lcom/android/server/am/OomAdjusterImpl;->isSpecialApp(Ljava/lang/String;)Z */
/* if-nez v3, :cond_1 */
/* .line 502 */
v3 = (( com.miui.server.smartpower.IAppState ) v2 ).hasActivityApp ( ); // invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState;->hasActivityApp()Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 503 */
v3 = (( com.miui.server.smartpower.IAppState ) v2 ).getLaunchCount ( ); // invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState;->getLaunchCount()I
/* const/16 v4, 0x15 */
/* if-le v3, v4, :cond_1 */
/* .line 504 */
(( com.miui.server.smartpower.IAppState ) v2 ).getTotalTimeInForeground ( ); // invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState;->getTotalTimeInForeground()J
/* move-result-wide v3 */
/* const-wide/32 v5, 0x6ddd00 */
/* cmp-long v3, v3, v5 */
/* if-lez v3, :cond_1 */
/* .line 505 */
/* .line 507 */
} // .end local v2 # "appState":Lcom/miui/server/smartpower/IAppState;
} // :cond_1
/* .line 508 */
} // :cond_2
/* new-instance v1, Lcom/android/server/am/OomAdjusterImpl$2; */
/* invoke-direct {v1, p0}, Lcom/android/server/am/OomAdjusterImpl$2;-><init>(Lcom/android/server/am/OomAdjusterImpl;)V */
java.util.Collections .sort ( v0,v1 );
/* .line 521 */
v1 = com.android.server.am.OomAdjusterImpl.sPreviousResidentAppList;
/* .line 522 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
v3 = } // :goto_1
v2 = java.lang.Math .min ( v2,v3 );
/* if-ge v1, v2, :cond_3 */
/* .line 523 */
v2 = com.android.server.am.OomAdjusterImpl.sPreviousResidentAppList;
/* check-cast v3, Lcom/miui/server/smartpower/IAppState; */
(( com.miui.server.smartpower.IAppState ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState;->getPackageName()Ljava/lang/String;
/* .line 522 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 525 */
} // .end local v1 # "i":I
} // :cond_3
return;
} // .end method
