public class com.android.server.am.ProcessPolicy implements com.android.server.power.stats.BatteryStatsManagerStub$ActiveCallback implements com.android.server.am.IProcessPolicy {
	 /* .source "ProcessPolicy.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/am/ProcessPolicy$ActiveUpdateHandler;, */
	 /* Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Boolean DEBUG;
public static final Boolean DEBUG_ACTIVE;
private static final Long DEFAULT_FASTBOOT_THRESHOLDKB;
private static final Boolean DYNAMIC_LIST_CHECK_ADJ;
private static final java.lang.String JSON_KEY_PACKAGE_NAMES;
private static final java.lang.String JSON_KEY_USER_ID;
private static final Integer MSG_UPDATE_AUDIO_OFF;
private static final Integer MSG_UPDATE_STOP_GPS;
private static final Integer PERCEPTIBLE_APP_ADJ;
private static final java.lang.String PKG_MIUI_CONTENT_CATCHER;
private static final java.lang.String PKG_MIUI_VOICETRIGGER;
private static final Integer PRIORITY_LEVEL_HEAVY_WEIGHT;
private static final Integer PRIORITY_LEVEL_PERCEPTIBLE;
private static final Integer PRIORITY_LEVEL_UNKNOWN;
private static final Integer PRIORITY_LEVEL_VISIBLE;
private static final java.lang.String SETTINGS_MIUI_CCONTENT_CATCHER_ENABLE;
private static final android.net.Uri SETTINGS_MIUI_CCONTENT_CATCHER_URL;
private static final java.lang.String SETTINGS_MIUI_VOICETRIGGER_ENABLE;
private static final android.net.Uri SETTINGS_MIUI_VOICETRIGGER_ENABLE_URL;
public static final java.lang.String TAG;
private static final Long UPDATE_AUDIO_OFF_DELAY;
private static final Long UPDATE_STOP_GPS_DELAY;
private static android.util.SparseArray sActiveUidList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.Map sAppProtectMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.Map sBoundFgServiceProtectMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List sCloudWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List sDisableForceStopList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List sDisableTrimList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List sDisplaySizeBlackList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List sDisplaySizeProtectList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.HashMap sDynamicWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Boolean;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List sEnableCallProtectList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List sEnterpriseAppList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List sExtraPackageWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.Map sFastBootAppMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List sFgServiceCheckList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List sHeapBlackList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.Object sLock;
private static java.util.HashMap sLockedApplicationList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
static java.util.List sLowMemKillProcReasons;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List sNeedTraceList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.HashMap sOneKeyCleanWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private static android.util.SparseArray sPolicyFlagsList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static final android.util.SparseArray sProcessPriorityMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Landroid/util/Pair<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/Integer;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List sProcessStaticWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List sSecretlyProtectAppList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List sStaticWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List sSystemCleanWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static android.util.SparseArray sTempInactiveAudioList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static android.util.SparseArray sTempInactiveGPSList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
static java.util.List sUserKillProcReasons;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List sUserRequestCleanWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private android.view.accessibility.AccessibilityManager mAccessibilityManager;
private com.android.server.am.ProcessPolicy$ActiveUpdateHandler mActiveUpdateHandler;
private com.android.server.am.ActivityManagerService mActivityManagerService;
private android.content.Context mContext;
private com.android.server.am.ProcessManagerService mProcessManagerService;
private final android.database.ContentObserver mProcessPolicyObserver;
/* # direct methods */
static void -$$Nest$msaveLockedAppIntoSettings ( com.android.server.am.ProcessPolicy p0, android.content.Context p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessPolicy;->saveLockedAppIntoSettings(Landroid/content/Context;)V */
return;
} // .end method
static void -$$Nest$mupdateContentCatcherWhitelist ( com.android.server.am.ProcessPolicy p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/ProcessPolicy;->updateContentCatcherWhitelist()V */
return;
} // .end method
static void -$$Nest$mupdateVoiceTriggerWhitelist ( com.android.server.am.ProcessPolicy p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/ProcessPolicy;->updateVoiceTriggerWhitelist()V */
return;
} // .end method
static android.net.Uri -$$Nest$sfgetSETTINGS_MIUI_CCONTENT_CATCHER_URL ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.am.ProcessPolicy.SETTINGS_MIUI_CCONTENT_CATCHER_URL;
} // .end method
static android.net.Uri -$$Nest$sfgetSETTINGS_MIUI_VOICETRIGGER_ENABLE_URL ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.am.ProcessPolicy.SETTINGS_MIUI_VOICETRIGGER_ENABLE_URL;
} // .end method
static android.util.SparseArray -$$Nest$sfgetsActiveUidList ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.am.ProcessPolicy.sActiveUidList;
} // .end method
static java.lang.Object -$$Nest$sfgetsLock ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.am.ProcessPolicy.sLock;
} // .end method
static android.util.SparseArray -$$Nest$sfgetsTempInactiveAudioList ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.am.ProcessPolicy.sTempInactiveAudioList;
} // .end method
static android.util.SparseArray -$$Nest$sfgetsTempInactiveGPSList ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.am.ProcessPolicy.sTempInactiveGPSList;
} // .end method
static com.android.server.am.ProcessPolicy ( ) {
/* .locals 8 */
/* .line 83 */
/* nop */
/* .line 84 */
/* const-string/jumbo v0, "voice_trigger_enabled" */
android.provider.Settings$Global .getUriFor ( v0 );
/* .line 86 */
/* nop */
/* .line 87 */
final String v0 = "open_content_extension_clipboard_mode"; // const-string v0, "open_content_extension_clipboard_mode"
android.provider.Settings$System .getUriFor ( v0 );
/* .line 92 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 96 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 99 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 102 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 104 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 106 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 109 */
v0 = com.android.server.am.ProcessPolicy.sHeapBlackList;
final String v1 = "com.ss.android.ugc.aweme"; // const-string v1, "com.ss.android.ugc.aweme"
/* .line 110 */
v0 = com.android.server.am.ProcessPolicy.sOneKeyCleanWhiteList;
/* new-instance v1, Ljava/util/ArrayList; */
final String v2 = "com.xiaomi.mslgrdp:multiwindow.rdp"; // const-string v2, "com.xiaomi.mslgrdp:multiwindow.rdp"
/* filled-new-array {v2}, [Ljava/lang/String; */
java.util.Arrays .asList ( v2 );
/* invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
final String v2 = "com.xiaomi.mslgrdp"; // const-string v2, "com.xiaomi.mslgrdp"
(( java.util.HashMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 115 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 119 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 122 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 125 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 128 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 136 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 138 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 139 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 142 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 146 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 149 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 152 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 155 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 158 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 161 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 164 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 167 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
/* .line 170 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
/* .line 173 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
/* .line 176 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
/* .line 183 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 193 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 195 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
/* .line 197 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
/* .line 213 */
/* const/16 v1, 0xc8 */
/* .line 217 */
v1 = com.android.server.am.ProcessPolicy.sUserKillProcReasons;
final String v2 = "OneKeyClean"; // const-string v2, "OneKeyClean"
/* .line 218 */
v1 = com.android.server.am.ProcessPolicy.sUserKillProcReasons;
final String v2 = "ForceClean"; // const-string v2, "ForceClean"
/* .line 219 */
v1 = com.android.server.am.ProcessPolicy.sUserKillProcReasons;
final String v2 = "GarbageClean"; // const-string v2, "GarbageClean"
/* .line 220 */
v1 = com.android.server.am.ProcessPolicy.sUserKillProcReasons;
final String v2 = "GameClean"; // const-string v2, "GameClean"
/* .line 221 */
v1 = com.android.server.am.ProcessPolicy.sUserKillProcReasons;
final String v2 = "OptimizationClean"; // const-string v2, "OptimizationClean"
/* .line 222 */
v1 = com.android.server.am.ProcessPolicy.sUserKillProcReasons;
final String v2 = "SwipeUpClean"; // const-string v2, "SwipeUpClean"
/* .line 223 */
v1 = com.android.server.am.ProcessPolicy.sUserKillProcReasons;
final String v2 = "UserDefined"; // const-string v2, "UserDefined"
/* .line 224 */
v1 = com.android.server.am.ProcessPolicy.sLowMemKillProcReasons;
final String v2 = "anr"; // const-string v2, "anr"
/* .line 225 */
v1 = com.android.server.am.ProcessPolicy.sLowMemKillProcReasons;
final String v2 = "crash"; // const-string v2, "crash"
/* .line 226 */
v1 = com.android.server.am.ProcessPolicy.sLowMemKillProcReasons;
final String v2 = "MiuiMemoryService"; // const-string v2, "MiuiMemoryService"
/* .line 227 */
v1 = com.android.server.am.ProcessPolicy.sLowMemKillProcReasons;
final String v2 = "lowMemory"; // const-string v2, "lowMemory"
/* .line 229 */
com.android.server.am.IProcessPolicy .getMemoryThresholdForFastBooApp ( );
(( java.lang.Long ) v1 ).longValue ( ); // invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
/* move-result-wide v1 */
/* .line 230 */
/* .local v1, "memoryThreshold":J */
v3 = com.android.server.am.ProcessPolicy.sFastBootAppMap;
final String v4 = "com.tencent.mm"; // const-string v4, "com.tencent.mm"
java.lang.Long .valueOf ( v1,v2 );
/* .line 231 */
v3 = com.android.server.am.ProcessPolicy.sFastBootAppMap;
final String v4 = "com.tencent.mobileqq"; // const-string v4, "com.tencent.mobileqq"
java.lang.Long .valueOf ( v1,v2 );
/* .line 233 */
int v3 = -1; // const/4 v3, -0x1
v4 = com.android.server.am.ProcessUtils.PRIORITY_UNKNOW;
(( android.util.SparseArray ) v0 ).put ( v3, v4 ); // invoke-virtual {v0, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 234 */
v3 = com.android.server.am.ProcessUtils.PRIORITY_VISIBLE;
int v4 = 1; // const/4 v4, 0x1
(( android.util.SparseArray ) v0 ).put ( v4, v3 ); // invoke-virtual {v0, v4, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 235 */
v3 = com.android.server.am.ProcessUtils.PRIORITY_PERCEPTIBLE;
int v5 = 2; // const/4 v5, 0x2
(( android.util.SparseArray ) v0 ).put ( v5, v3 ); // invoke-virtual {v0, v5, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 236 */
v3 = com.android.server.am.ProcessUtils.PRIORITY_HEAVY;
int v6 = 3; // const/4 v6, 0x3
(( android.util.SparseArray ) v0 ).put ( v6, v3 ); // invoke-virtual {v0, v6, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 239 */
v0 = com.android.server.am.ProcessPolicy.sAppProtectMap;
final String v3 = "com.miui.bugreport"; // const-string v3, "com.miui.bugreport"
java.lang.Integer .valueOf ( v6 );
/* .line 240 */
v0 = com.android.server.am.ProcessPolicy.sAppProtectMap;
final String v3 = "com.miui.virtualsim"; // const-string v3, "com.miui.virtualsim"
java.lang.Integer .valueOf ( v6 );
/* .line 241 */
v0 = com.android.server.am.ProcessPolicy.sAppProtectMap;
final String v3 = "com.miui.touchassistant"; // const-string v3, "com.miui.touchassistant"
java.lang.Integer .valueOf ( v6 );
/* .line 242 */
v0 = com.android.server.am.ProcessPolicy.sAppProtectMap;
final String v3 = "com.xiaomi.joyose"; // const-string v3, "com.xiaomi.joyose"
java.lang.Integer .valueOf ( v6 );
/* .line 243 */
v0 = com.android.server.am.ProcessPolicy.sAppProtectMap;
final String v3 = "com.miui.tsmclient"; // const-string v3, "com.miui.tsmclient"
java.lang.Integer .valueOf ( v6 );
/* .line 244 */
v0 = com.android.server.am.ProcessPolicy.sAppProtectMap;
final String v3 = "com.miui.powerkeeper"; // const-string v3, "com.miui.powerkeeper"
java.lang.Integer .valueOf ( v6 );
/* .line 247 */
v0 = com.android.server.am.ProcessPolicy.sBoundFgServiceProtectMap;
final String v3 = "com.milink.service"; // const-string v3, "com.milink.service"
final String v7 = "com.xiaomi.miplay_client"; // const-string v7, "com.xiaomi.miplay_client"
/* .line 250 */
v0 = com.android.server.am.ProcessPolicy.sPolicyFlagsList;
/* .line 251 */
/* const/16 v3, 0x2005 */
java.lang.Integer .valueOf ( v3 );
/* .line 250 */
(( android.util.SparseArray ) v0 ).put ( v4, v7 ); // invoke-virtual {v0, v4, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 253 */
v0 = com.android.server.am.ProcessPolicy.sPolicyFlagsList;
/* .line 254 */
java.lang.Integer .valueOf ( v3 );
/* .line 253 */
int v4 = 7; // const/4 v4, 0x7
(( android.util.SparseArray ) v0 ).put ( v4, v3 ); // invoke-virtual {v0, v4, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 256 */
v0 = com.android.server.am.ProcessPolicy.sPolicyFlagsList;
/* .line 257 */
int v3 = 5; // const/4 v3, 0x5
java.lang.Integer .valueOf ( v3 );
/* .line 256 */
int v7 = 4; // const/4 v7, 0x4
(( android.util.SparseArray ) v0 ).put ( v7, v4 ); // invoke-virtual {v0, v7, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 258 */
v0 = com.android.server.am.ProcessPolicy.sPolicyFlagsList;
/* .line 259 */
java.lang.Integer .valueOf ( v3 );
/* .line 258 */
(( android.util.SparseArray ) v0 ).put ( v3, v4 ); // invoke-virtual {v0, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 260 */
v0 = com.android.server.am.ProcessPolicy.sPolicyFlagsList;
/* .line 261 */
java.lang.Integer .valueOf ( v3 );
/* .line 260 */
/* const/16 v7, 0xe */
(( android.util.SparseArray ) v0 ).put ( v7, v4 ); // invoke-virtual {v0, v7, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 262 */
v0 = com.android.server.am.ProcessPolicy.sPolicyFlagsList;
/* .line 263 */
java.lang.Integer .valueOf ( v3 );
/* .line 262 */
(( android.util.SparseArray ) v0 ).put ( v6, v4 ); // invoke-virtual {v0, v6, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 264 */
v0 = com.android.server.am.ProcessPolicy.sPolicyFlagsList;
/* .line 265 */
java.lang.Integer .valueOf ( v3 );
/* .line 264 */
int v6 = 6; // const/4 v6, 0x6
(( android.util.SparseArray ) v0 ).put ( v6, v4 ); // invoke-virtual {v0, v6, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 266 */
v0 = com.android.server.am.ProcessPolicy.sPolicyFlagsList;
/* .line 267 */
java.lang.Integer .valueOf ( v3 );
/* .line 266 */
/* const/16 v6, 0x10 */
(( android.util.SparseArray ) v0 ).put ( v6, v4 ); // invoke-virtual {v0, v6, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 268 */
v0 = com.android.server.am.ProcessPolicy.sPolicyFlagsList;
/* .line 269 */
java.lang.Integer .valueOf ( v3 );
/* .line 268 */
/* const/16 v6, 0x16 */
(( android.util.SparseArray ) v0 ).put ( v6, v4 ); // invoke-virtual {v0, v6, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 270 */
v0 = com.android.server.am.ProcessPolicy.sPolicyFlagsList;
/* .line 271 */
java.lang.Integer .valueOf ( v3 );
/* .line 270 */
/* const/16 v6, 0x13 */
(( android.util.SparseArray ) v0 ).put ( v6, v4 ); // invoke-virtual {v0, v6, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 272 */
v0 = com.android.server.am.ProcessPolicy.sPolicyFlagsList;
/* .line 273 */
java.lang.Integer .valueOf ( v3 );
/* .line 272 */
/* const/16 v6, 0x14 */
(( android.util.SparseArray ) v0 ).put ( v6, v4 ); // invoke-virtual {v0, v6, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 274 */
v0 = com.android.server.am.ProcessPolicy.sPolicyFlagsList;
/* .line 275 */
java.lang.Integer .valueOf ( v3 );
/* .line 274 */
(( android.util.SparseArray ) v0 ).put ( v5, v3 ); // invoke-virtual {v0, v5, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 276 */
v0 = com.android.server.am.ProcessPolicy.sPolicyFlagsList;
/* .line 277 */
/* const/16 v3, 0x35 */
java.lang.Integer .valueOf ( v3 );
/* .line 276 */
/* const/16 v4, 0x15 */
(( android.util.SparseArray ) v0 ).put ( v4, v3 ); // invoke-virtual {v0, v4, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 279 */
} // .end local v1 # "memoryThreshold":J
return;
} // .end method
public com.android.server.am.ProcessPolicy ( ) {
/* .locals 2 */
/* .param p1, "processManagerService" # Lcom/android/server/am/ProcessManagerService; */
/* .param p2, "ams" # Lcom/android/server/am/ActivityManagerService; */
/* .param p3, "accessibilityManager" # Landroid/view/accessibility/AccessibilityManager; */
/* .param p4, "thread" # Lcom/android/server/ServiceThread; */
/* .line 403 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 389 */
/* new-instance v0, Lcom/android/server/am/ProcessPolicy$1; */
v1 = this.mActiveUpdateHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/am/ProcessPolicy$1;-><init>(Lcom/android/server/am/ProcessPolicy;Landroid/os/Handler;)V */
this.mProcessPolicyObserver = v0;
/* .line 404 */
this.mProcessManagerService = p1;
/* .line 405 */
this.mActivityManagerService = p2;
/* .line 406 */
this.mAccessibilityManager = p3;
/* .line 407 */
/* new-instance v0, Lcom/android/server/am/ProcessPolicy$ActiveUpdateHandler; */
(( com.android.server.ServiceThread ) p4 ).getLooper ( ); // invoke-virtual {p4}, Lcom/android/server/ServiceThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/am/ProcessPolicy$ActiveUpdateHandler;-><init>(Lcom/android/server/am/ProcessPolicy;Landroid/os/Looper;)V */
this.mActiveUpdateHandler = v0;
/* .line 408 */
return;
} // .end method
private static Boolean isLiteOrMiddle ( ) {
/* .locals 1 */
/* .line 475 */
/* sget-boolean v0, Lmiui/util/DeviceLevel;->IS_MIUI_LITE_VERSION:Z */
/* if-nez v0, :cond_1 */
/* sget-boolean v0, Lmiui/util/DeviceLevel;->IS_MIUI_MIDDLE_VERSION:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
private Boolean isLockedApplicationForUserId ( java.lang.String p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 821 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 822 */
/* .line 824 */
} // :cond_0
v1 = com.android.server.am.ProcessPolicy.sLockedApplicationList;
java.lang.Integer .valueOf ( p2 );
(( java.util.HashMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/util/Set; */
/* .line 825 */
/* .local v1, "lockedApplication":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 826 */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_2
/* check-cast v3, Ljava/lang/String; */
/* .line 827 */
/* .local v3, "item":Ljava/lang/String; */
v4 = (( java.lang.String ) v3 ).equals ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 828 */
int v0 = 1; // const/4 v0, 0x1
/* .line 830 */
} // .end local v3 # "item":Ljava/lang/String;
} // :cond_1
/* .line 832 */
} // :cond_2
} // .end method
private void loadLockedAppFromSettings ( android.content.Context p0 ) {
/* .locals 11 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 773 */
v0 = com.android.server.am.ProcessPolicy.sLock;
/* monitor-enter v0 */
/* .line 774 */
try { // :try_start_0
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "locked_apps"; // const-string v2, "locked_apps"
android.provider.MiuiSettings$System .getString ( v1,v2 );
/* .line 777 */
/* .local v1, "jsonFormatText":Ljava/lang/String; */
v2 = android.text.TextUtils .isEmpty ( v1 );
if ( v2 != null) { // if-eqz v2, :cond_0
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
return;
/* .line 780 */
} // :cond_0
try { // :try_start_1
/* new-instance v2, Lorg/json/JSONArray; */
/* invoke-direct {v2, v1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V */
/* .line 781 */
/* .local v2, "userSpaceArray":Lorg/json/JSONArray; */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "spaceIndex":I */
} // :goto_0
v4 = (( org.json.JSONArray ) v2 ).length ( ); // invoke-virtual {v2}, Lorg/json/JSONArray;->length()I
/* if-ge v3, v4, :cond_2 */
/* .line 782 */
(( org.json.JSONArray ) v2 ).get ( v3 ); // invoke-virtual {v2, v3}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;
/* check-cast v4, Lorg/json/JSONObject; */
/* .line 784 */
/* .local v4, "userSpaceObject":Lorg/json/JSONObject; */
/* const-string/jumbo v5, "u" */
v5 = (( org.json.JSONObject ) v4 ).getInt ( v5 ); // invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* .line 785 */
/* .local v5, "userId":I */
final String v6 = "pkgs"; // const-string v6, "pkgs"
/* .line 786 */
(( org.json.JSONObject ) v4 ).getJSONArray ( v6 ); // invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 787 */
/* .local v6, "packageNameArray":Lorg/json/JSONArray; */
/* new-instance v7, Ljava/util/HashSet; */
/* invoke-direct {v7}, Ljava/util/HashSet;-><init>()V */
/* .line 788 */
/* .local v7, "packageNameSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
int v8 = 0; // const/4 v8, 0x0
/* .local v8, "pkgIndex":I */
} // :goto_1
v9 = (( org.json.JSONArray ) v6 ).length ( ); // invoke-virtual {v6}, Lorg/json/JSONArray;->length()I
/* if-ge v8, v9, :cond_1 */
/* .line 789 */
(( org.json.JSONArray ) v6 ).getString ( v8 ); // invoke-virtual {v6, v8}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
/* .line 788 */
/* add-int/lit8 v8, v8, 0x1 */
/* .line 791 */
} // .end local v8 # "pkgIndex":I
} // :cond_1
v8 = com.android.server.am.ProcessPolicy.sLockedApplicationList;
java.lang.Integer .valueOf ( v5 );
(( java.util.HashMap ) v8 ).put ( v9, v7 ); // invoke-virtual {v8, v9, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 793 */
final String v8 = "ProcessManager"; // const-string v8, "ProcessManager"
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "loadLockedAppFromSettings userId:"; // const-string v10, "loadLockedAppFromSettings userId:"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v5 ); // invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = "-pkgNames:"; // const-string v10, "-pkgNames:"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 794 */
java.util.Arrays .toString ( v10 );
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 793 */
android.util.Log .d ( v8,v9 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 781 */
/* nop */
} // .end local v4 # "userSpaceObject":Lorg/json/JSONObject;
} // .end local v5 # "userId":I
} // .end local v6 # "packageNameArray":Lorg/json/JSONArray;
} // .end local v7 # "packageNameSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
/* add-int/lit8 v3, v3, 0x1 */
/* .line 799 */
} // .end local v2 # "userSpaceArray":Lorg/json/JSONArray;
} // .end local v3 # "spaceIndex":I
} // :cond_2
/* .line 796 */
/* :catch_0 */
/* move-exception v2 */
/* .line 797 */
/* .local v2, "e":Ljava/lang/Exception; */
try { // :try_start_2
final String v3 = "ProcessManager"; // const-string v3, "ProcessManager"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "loadLockedApp failed: "; // const-string v5, "loadLockedApp failed: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v3,v4 );
/* .line 798 */
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* .line 800 */
} // .end local v1 # "jsonFormatText":Ljava/lang/String;
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_2
/* monitor-exit v0 */
/* .line 801 */
return;
/* .line 800 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
private void registerContentObserver ( ) {
/* .locals 4 */
/* .line 480 */
/* invoke-direct {p0}, Lcom/android/server/am/ProcessPolicy;->updateVoiceTriggerWhitelist()V */
/* .line 481 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v1 = com.android.server.am.ProcessPolicy.SETTINGS_MIUI_VOICETRIGGER_ENABLE_URL;
v2 = this.mProcessPolicyObserver;
int v3 = 1; // const/4 v3, 0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 485 */
/* invoke-direct {p0}, Lcom/android/server/am/ProcessPolicy;->updateContentCatcherWhitelist()V */
/* .line 486 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v1 = com.android.server.am.ProcessPolicy.SETTINGS_MIUI_CCONTENT_CATCHER_URL;
v2 = this.mProcessPolicyObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 488 */
return;
} // .end method
private void removeDefaultLockedAppIfExists ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 718 */
v0 = com.android.server.am.ProcessPolicy.sLockedApplicationList;
/* const/16 v1, -0x64 */
java.lang.Integer .valueOf ( v1 );
(( java.util.HashMap ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/util/Set; */
/* .line 719 */
/* .local v0, "defaultLockedApps":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
v1 = if ( v0 != null) { // if-eqz v0, :cond_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 720 */
/* .line 722 */
} // :cond_0
return;
} // .end method
private void saveLockedAppIntoSettings ( android.content.Context p0 ) {
/* .locals 8 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 748 */
v0 = com.android.server.am.ProcessPolicy.sLock;
/* monitor-enter v0 */
/* .line 749 */
try { // :try_start_0
/* new-instance v1, Lorg/json/JSONArray; */
/* invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 751 */
/* .local v1, "userSpaceArray":Lorg/json/JSONArray; */
try { // :try_start_1
v2 = com.android.server.am.ProcessPolicy.sLockedApplicationList;
(( java.util.HashMap ) v2 ).keySet ( ); // invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_0
/* check-cast v3, Ljava/lang/Integer; */
/* .line 752 */
/* .local v3, "userId":Ljava/lang/Integer; */
/* new-instance v4, Lorg/json/JSONObject; */
/* invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V */
/* .line 753 */
/* .local v4, "userSpaceObject":Lorg/json/JSONObject; */
/* const-string/jumbo v5, "u" */
(( org.json.JSONObject ) v4 ).put ( v5, v3 ); // invoke-virtual {v4, v5, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 754 */
final String v5 = "pkgs"; // const-string v5, "pkgs"
/* new-instance v6, Lorg/json/JSONArray; */
v7 = com.android.server.am.ProcessPolicy.sLockedApplicationList;
/* .line 755 */
(( java.util.HashMap ) v7 ).get ( v3 ); // invoke-virtual {v7, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v7, Ljava/util/Collection; */
/* invoke-direct {v6, v7}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V */
/* .line 754 */
(( org.json.JSONObject ) v4 ).put ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 756 */
(( org.json.JSONArray ) v1 ).put ( v4 ); // invoke-virtual {v1, v4}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
/* .line 757 */
/* nop */
} // .end local v3 # "userId":Ljava/lang/Integer;
} // .end local v4 # "userSpaceObject":Lorg/json/JSONObject;
/* .line 760 */
} // :cond_0
final String v2 = "ProcessManager"; // const-string v2, "ProcessManager"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "saveLockedAppIntoSettings:"; // const-string v4, "saveLockedAppIntoSettings:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( org.json.JSONArray ) v1 ).toString ( ); // invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v3 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 765 */
/* .line 762 */
/* :catch_0 */
/* move-exception v2 */
/* .line 763 */
/* .local v2, "e":Ljava/lang/Exception; */
try { // :try_start_2
final String v3 = "ProcessManager"; // const-string v3, "ProcessManager"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "saveLockedAppIntoSettings failed: "; // const-string v5, "saveLockedAppIntoSettings failed: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v3,v4 );
/* .line 764 */
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* .line 767 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_1
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v3 = "locked_apps"; // const-string v3, "locked_apps"
/* .line 768 */
(( org.json.JSONArray ) v1 ).toString ( ); // invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;
/* .line 767 */
android.provider.MiuiSettings$System .putString ( v2,v3,v4 );
/* .line 769 */
/* nop */
} // .end local v1 # "userSpaceArray":Lorg/json/JSONArray;
/* monitor-exit v0 */
/* .line 770 */
return;
/* .line 769 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
private void updateAppProtectMap ( miui.process.ProcessCloudData p0 ) {
/* .locals 5 */
/* .param p1, "cloudData" # Lmiui/process/ProcessCloudData; */
/* .line 876 */
(( miui.process.ProcessCloudData ) p1 ).getAppProtectMap ( ); // invoke-virtual {p1}, Lmiui/process/ProcessCloudData;->getAppProtectMap()Ljava/util/Map;
/* .line 877 */
/* .local v0, "appProtectMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;" */
v1 = com.android.server.am.ProcessPolicy.sLock;
/* monitor-enter v1 */
/* .line 878 */
if ( v0 != null) { // if-eqz v0, :cond_0
v2 = try { // :try_start_0
/* if-nez v2, :cond_0 */
v2 = com.android.server.am.ProcessPolicy.sAppProtectMap;
v2 = /* .line 879 */
/* if-nez v2, :cond_0 */
/* .line 880 */
v2 = com.android.server.am.ProcessPolicy.sAppProtectMap;
/* .line 881 */
v2 = com.android.server.am.ProcessPolicy.sAppProtectMap;
/* .line 882 */
final String v2 = "ProcessManager"; // const-string v2, "ProcessManager"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "update AP:" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = com.android.server.am.ProcessPolicy.sAppProtectMap;
java.util.Arrays .toString ( v4 );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v3 );
/* .line 888 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 883 */
} // :cond_0
v2 = if ( v0 != null) { // if-eqz v0, :cond_1
if ( v2 != null) { // if-eqz v2, :cond_2
} // :cond_1
v2 = com.android.server.am.ProcessPolicy.sAppProtectMap;
v2 = /* .line 884 */
/* if-nez v2, :cond_2 */
/* .line 885 */
v2 = com.android.server.am.ProcessPolicy.sAppProtectMap;
/* .line 886 */
final String v2 = "ProcessManager"; // const-string v2, "ProcessManager"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "update AP:" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = com.android.server.am.ProcessPolicy.sAppProtectMap;
java.util.Arrays .toString ( v4 );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v3 );
/* .line 888 */
} // :cond_2
} // :goto_0
/* monitor-exit v1 */
/* .line 889 */
return;
/* .line 888 */
} // :goto_1
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
private void updateApplicationLockedState ( java.lang.String p0, Integer p1, Boolean p2 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .param p3, "isLocked" # Z */
/* .line 702 */
v0 = com.android.server.am.ProcessPolicy.sLock;
/* monitor-enter v0 */
/* .line 703 */
try { // :try_start_0
v1 = com.android.server.am.ProcessPolicy.sLockedApplicationList;
java.lang.Integer .valueOf ( p2 );
(( java.util.HashMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/util/Set; */
/* .line 704 */
/* .local v1, "lockedApplication":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* if-nez v1, :cond_0 */
/* .line 705 */
/* new-instance v2, Ljava/util/HashSet; */
/* invoke-direct {v2}, Ljava/util/HashSet;-><init>()V */
/* move-object v1, v2 */
/* .line 706 */
v2 = com.android.server.am.ProcessPolicy.sLockedApplicationList;
java.lang.Integer .valueOf ( p2 );
(( java.util.HashMap ) v2 ).put ( v3, v1 ); // invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 708 */
} // :cond_0
if ( p3 != null) { // if-eqz p3, :cond_1
/* .line 709 */
/* .line 711 */
} // :cond_1
/* .line 712 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessPolicy;->removeDefaultLockedAppIfExists(Ljava/lang/String;)V */
/* .line 714 */
} // .end local v1 # "lockedApplication":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
} // :goto_0
/* monitor-exit v0 */
/* .line 715 */
return;
/* .line 714 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void updateCloudWhiteList ( miui.process.ProcessCloudData p0 ) {
/* .locals 5 */
/* .param p1, "cloudData" # Lmiui/process/ProcessCloudData; */
/* .line 860 */
(( miui.process.ProcessCloudData ) p1 ).getCloudWhiteList ( ); // invoke-virtual {p1}, Lmiui/process/ProcessCloudData;->getCloudWhiteList()Ljava/util/List;
/* .line 861 */
/* .local v0, "cloudWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v1 = com.android.server.am.ProcessPolicy.sLock;
/* monitor-enter v1 */
/* .line 862 */
if ( v0 != null) { // if-eqz v0, :cond_0
v2 = try { // :try_start_0
/* if-nez v2, :cond_0 */
v2 = com.android.server.am.ProcessPolicy.sCloudWhiteList;
v2 = /* .line 863 */
/* if-nez v2, :cond_0 */
/* .line 864 */
v2 = com.android.server.am.ProcessPolicy.sCloudWhiteList;
/* .line 865 */
v2 = com.android.server.am.ProcessPolicy.sCloudWhiteList;
/* .line 866 */
final String v2 = "ProcessManager"; // const-string v2, "ProcessManager"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "update CL:" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = com.android.server.am.ProcessPolicy.sCloudWhiteList;
java.util.Arrays .toString ( v4 );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v3 );
/* .line 872 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 867 */
} // :cond_0
v2 = if ( v0 != null) { // if-eqz v0, :cond_1
if ( v2 != null) { // if-eqz v2, :cond_2
} // :cond_1
v2 = com.android.server.am.ProcessPolicy.sCloudWhiteList;
v2 = /* .line 868 */
/* if-nez v2, :cond_2 */
/* .line 869 */
v2 = com.android.server.am.ProcessPolicy.sCloudWhiteList;
/* .line 870 */
final String v2 = "ProcessManager"; // const-string v2, "ProcessManager"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "update CL:" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = com.android.server.am.ProcessPolicy.sCloudWhiteList;
java.util.Arrays .toString ( v4 );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v3 );
/* .line 872 */
} // :cond_2
} // :goto_0
/* monitor-exit v1 */
/* .line 873 */
return;
/* .line 872 */
} // :goto_1
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
private void updateContentCatcherWhitelist ( ) {
/* .locals 3 */
/* .line 506 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 507 */
final String v0 = "ProcessManager"; // const-string v0, "ProcessManager"
/* const-string/jumbo v1, "update taplus clipboard whitelist error, build international" */
android.util.Slog .i ( v0,v1 );
/* .line 508 */
return;
/* .line 510 */
} // :cond_0
v0 = this.mContext;
/* .line 511 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 510 */
final String v1 = "open_content_extension_clipboard_mode"; // const-string v1, "open_content_extension_clipboard_mode"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$System .getInt ( v0,v1,v2 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_1 */
/* move v2, v1 */
} // :cond_1
/* move v0, v2 */
/* .line 512 */
/* .local v0, "isEnabled":Z */
v1 = com.android.server.am.ProcessPolicy.sProcessStaticWhiteList;
final String v2 = "com.miui.contentcatcher"; // const-string v2, "com.miui.contentcatcher"
/* .line 513 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 514 */
v1 = com.android.server.am.ProcessPolicy.sProcessStaticWhiteList;
/* .line 516 */
} // :cond_2
return;
} // .end method
private void updateFastBootList ( miui.process.ProcessCloudData p0 ) {
/* .locals 10 */
/* .param p1, "cloudData" # Lmiui/process/ProcessCloudData; */
/* .line 892 */
(( miui.process.ProcessCloudData ) p1 ).getFastBootList ( ); // invoke-virtual {p1}, Lmiui/process/ProcessCloudData;->getFastBootList()Ljava/util/List;
/* .line 894 */
/* .local v0, "fastBootList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v1 = com.android.server.am.ProcessPolicy.sLock;
/* monitor-enter v1 */
/* .line 895 */
try { // :try_start_0
v2 = com.android.server.am.ProcessPolicy.sFastBootAppMap;
/* .line 896 */
/* .local v2, "oldFastBootSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_2 */
/* .line 897 */
v3 = if ( v0 != null) { // if-eqz v0, :cond_2
/* if-nez v3, :cond_2 */
/* new-instance v3, Ljava/util/HashSet; */
/* invoke-direct {v3, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V */
v3 = /* .line 898 */
/* if-nez v3, :cond_2 */
/* .line 899 */
/* monitor-enter v1 */
/* .line 900 */
try { // :try_start_1
/* new-instance v3, Ljava/util/HashMap; */
/* invoke-direct {v3}, Ljava/util/HashMap;-><init>()V */
/* .line 901 */
/* .local v3, "temp":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;" */
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_1
/* check-cast v5, Ljava/lang/String; */
/* .line 902 */
/* .local v5, "packageName":Ljava/lang/String; */
v6 = com.android.server.am.ProcessPolicy.sFastBootAppMap;
/* check-cast v6, Ljava/lang/Long; */
(( java.lang.Long ) v6 ).longValue ( ); // invoke-virtual {v6}, Ljava/lang/Long;->longValue()J
/* move-result-wide v6 */
/* .line 903 */
/* .local v6, "thresholdKb":J */
/* const-wide/16 v8, 0x0 */
/* cmp-long v8, v6, v8 */
/* if-lez v8, :cond_0 */
/* move-wide v8, v6 */
} // :cond_0
/* const-wide/32 v8, 0x1f400000 */
} // :goto_1
java.lang.Long .valueOf ( v8,v9 );
/* .line 904 */
/* nop */
} // .end local v5 # "packageName":Ljava/lang/String;
} // .end local v6 # "thresholdKb":J
/* .line 905 */
} // :cond_1
v4 = com.android.server.am.ProcessPolicy.sFastBootAppMap;
/* .line 906 */
v4 = com.android.server.am.ProcessPolicy.sFastBootAppMap;
/* .line 907 */
} // .end local v3 # "temp":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 908 */
final String v1 = "ProcessManager"; // const-string v1, "ProcessManager"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "update FA:" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = com.android.server.am.ProcessPolicy.sFastBootAppMap;
java.util.Arrays .toString ( v4 );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v3 );
/* .line 907 */
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_2
/* monitor-exit v1 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v3 */
/* .line 909 */
} // :cond_2
v3 = if ( v0 != null) { // if-eqz v0, :cond_3
if ( v3 != null) { // if-eqz v3, :cond_4
} // :cond_3
v3 = v3 = com.android.server.am.ProcessPolicy.sFastBootAppMap;
/* if-nez v3, :cond_4 */
/* .line 910 */
/* monitor-enter v1 */
/* .line 911 */
try { // :try_start_3
v3 = com.android.server.am.ProcessPolicy.sFastBootAppMap;
/* .line 912 */
/* monitor-exit v1 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* .line 913 */
final String v1 = "ProcessManager"; // const-string v1, "ProcessManager"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "update FA:" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = com.android.server.am.ProcessPolicy.sFastBootAppMap;
java.util.Arrays .toString ( v4 );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v3 );
/* .line 912 */
/* :catchall_1 */
/* move-exception v3 */
try { // :try_start_4
/* monitor-exit v1 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* throw v3 */
/* .line 915 */
} // :cond_4
} // :goto_2
return;
/* .line 896 */
} // .end local v2 # "oldFastBootSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
/* :catchall_2 */
/* move-exception v2 */
try { // :try_start_5
/* monitor-exit v1 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_2 */
/* throw v2 */
} // .end method
private void updateMaxAdjLocked ( com.android.server.am.ProcessRecord p0, Integer p1, Boolean p2 ) {
/* .locals 1 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "targetMaxAdj" # I */
/* .param p3, "protect" # Z */
/* .line 1068 */
v0 = (( com.android.server.am.ProcessRecord ) p1 ).isPersistent ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isPersistent()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1069 */
return;
/* .line 1072 */
} // :cond_0
if ( p3 != null) { // if-eqz p3, :cond_1
v0 = this.mState;
v0 = (( com.android.server.am.ProcessStateRecord ) v0 ).getMaxAdj ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getMaxAdj()I
/* if-le v0, p2, :cond_1 */
/* .line 1073 */
v0 = this.mState;
(( com.android.server.am.ProcessStateRecord ) v0 ).setMaxAdj ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/am/ProcessStateRecord;->setMaxAdj(I)V
/* .line 1074 */
} // :cond_1
/* if-nez p3, :cond_2 */
v0 = this.mState;
v0 = (( com.android.server.am.ProcessStateRecord ) v0 ).getMaxAdj ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getMaxAdj()I
/* if-ge v0, p2, :cond_2 */
/* .line 1075 */
v0 = this.mState;
(( com.android.server.am.ProcessStateRecord ) v0 ).setMaxAdj ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/am/ProcessStateRecord;->setMaxAdj(I)V
/* .line 1077 */
} // :cond_2
} // :goto_0
return;
} // .end method
private void updateMaxProcStateLocked ( com.android.server.am.ProcessRecord p0, Integer p1, Boolean p2 ) {
/* .locals 1 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "targetMaxProcState" # I */
/* .param p3, "protect" # Z */
/* .line 1084 */
if ( p3 != null) { // if-eqz p3, :cond_0
v0 = com.android.server.am.IProcessPolicy .getAppMaxProcState ( p1 );
/* if-le v0, p2, :cond_0 */
/* .line 1085 */
com.android.server.am.IProcessPolicy .setAppMaxProcState ( p1,p2 );
/* .line 1086 */
} // :cond_0
/* if-nez p3, :cond_1 */
v0 = com.android.server.am.IProcessPolicy .getAppMaxProcState ( p1 );
/* if-ge v0, p2, :cond_1 */
/* .line 1087 */
com.android.server.am.IProcessPolicy .setAppMaxProcState ( p1,p2 );
/* .line 1089 */
} // :cond_1
} // :goto_0
return;
} // .end method
private void updateProcessPriority ( com.android.server.am.ProcessRecord p0, Integer p1, Boolean p2 ) {
/* .locals 3 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "priorityLevel" # I */
/* .param p3, "protect" # Z */
/* .line 1053 */
v0 = com.android.server.am.ProcessPolicy.sProcessPriorityMap;
(( android.util.SparseArray ) v0 ).get ( p2 ); // invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Landroid/util/Pair; */
/* .line 1054 */
/* .local v0, "priorityPair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;" */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1055 */
if ( p3 != null) { // if-eqz p3, :cond_0
v1 = this.first;
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
} // :cond_0
/* const/16 v1, 0x3e9 */
/* .line 1056 */
/* .local v1, "targetMaxAdj":I */
} // :goto_0
/* invoke-direct {p0, p1, v1, p3}, Lcom/android/server/am/ProcessPolicy;->updateMaxAdjLocked(Lcom/android/server/am/ProcessRecord;IZ)V */
/* .line 1058 */
if ( p3 != null) { // if-eqz p3, :cond_1
v2 = this.second;
/* check-cast v2, Ljava/lang/Integer; */
v2 = (( java.lang.Integer ) v2 ).intValue ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
} // :cond_1
/* const/16 v2, 0x14 */
/* .line 1059 */
/* .local v2, "targetMaxProcState":I */
} // :goto_1
/* invoke-direct {p0, p1, v2, p3}, Lcom/android/server/am/ProcessPolicy;->updateMaxProcStateLocked(Lcom/android/server/am/ProcessRecord;IZ)V */
/* .line 1061 */
} // .end local v1 # "targetMaxAdj":I
} // .end local v2 # "targetMaxProcState":I
} // :cond_2
return;
} // .end method
private void updateSecretlyProtectAppList ( miui.process.ProcessCloudData p0 ) {
/* .locals 5 */
/* .param p1, "cloudData" # Lmiui/process/ProcessCloudData; */
/* .line 918 */
(( miui.process.ProcessCloudData ) p1 ).getSecretlyProtectAppList ( ); // invoke-virtual {p1}, Lmiui/process/ProcessCloudData;->getSecretlyProtectAppList()Ljava/util/List;
/* .line 919 */
/* .local v0, "secretlyProtectAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v1 = com.android.server.am.ProcessPolicy.sLock;
/* monitor-enter v1 */
/* .line 920 */
if ( v0 != null) { // if-eqz v0, :cond_0
v2 = try { // :try_start_0
/* if-nez v2, :cond_0 */
v2 = com.android.server.am.ProcessPolicy.sSecretlyProtectAppList;
v2 = /* .line 921 */
/* if-nez v2, :cond_0 */
/* .line 922 */
v2 = com.android.server.am.ProcessPolicy.sSecretlyProtectAppList;
/* .line 923 */
v2 = com.android.server.am.ProcessPolicy.sSecretlyProtectAppList;
/* .line 924 */
final String v2 = "ProcessManager"; // const-string v2, "ProcessManager"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "update SPAL:" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = com.android.server.am.ProcessPolicy.sSecretlyProtectAppList;
java.util.Arrays .toString ( v4 );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v3 );
/* .line 930 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 925 */
} // :cond_0
v2 = if ( v0 != null) { // if-eqz v0, :cond_1
if ( v2 != null) { // if-eqz v2, :cond_2
} // :cond_1
v2 = com.android.server.am.ProcessPolicy.sSecretlyProtectAppList;
v2 = /* .line 926 */
/* if-nez v2, :cond_2 */
/* .line 927 */
v2 = com.android.server.am.ProcessPolicy.sSecretlyProtectAppList;
/* .line 928 */
final String v2 = "ProcessManager"; // const-string v2, "ProcessManager"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "update SPAL:" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = com.android.server.am.ProcessPolicy.sSecretlyProtectAppList;
java.util.Arrays .toString ( v4 );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v3 );
/* .line 930 */
} // :cond_2
} // :goto_0
/* monitor-exit v1 */
/* .line 931 */
return;
/* .line 930 */
} // :goto_1
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
private void updateVoiceTriggerWhitelist ( ) {
/* .locals 5 */
/* .line 491 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
final String v1 = "ProcessManager"; // const-string v1, "ProcessManager"
int v2 = 1; // const/4 v2, 0x1
/* if-nez v0, :cond_3 */
v0 = com.android.server.am.ProcessPolicy .isLiteOrMiddle ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 496 */
} // :cond_0
v0 = this.mContext;
/* .line 497 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 496 */
/* const-string/jumbo v3, "voice_trigger_enabled" */
int v4 = 0; // const/4 v4, 0x0
v0 = android.provider.Settings$Global .getInt ( v0,v3,v4 );
/* if-ne v0, v2, :cond_1 */
} // :cond_1
/* move v2, v4 */
} // :goto_0
/* move v0, v2 */
/* .line 498 */
/* .local v0, "isEnabled":Z */
v2 = com.android.server.am.ProcessPolicy.sProcessStaticWhiteList;
final String v3 = "com.miui.voicetrigger"; // const-string v3, "com.miui.voicetrigger"
/* .line 499 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 500 */
v2 = com.android.server.am.ProcessPolicy.sProcessStaticWhiteList;
/* .line 502 */
} // :cond_2
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "update voicetrigger whitelist, enabled " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* .line 503 */
return;
/* .line 492 */
} // .end local v0 # "isEnabled":Z
} // :cond_3
} // :goto_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "update voicetrigger whitelist error, build " */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v3, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = " isProtect "; // const-string v3, " isProtect "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 493 */
v3 = com.android.server.am.ProcessPolicy .isLiteOrMiddle ( );
/* xor-int/2addr v2, v3 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 492 */
android.util.Slog .i ( v1,v0 );
/* .line 494 */
return;
} // .end method
/* # virtual methods */
public void addWhiteList ( Integer p0, java.util.List p1, Boolean p2 ) {
/* .locals 5 */
/* .param p1, "flag" # I */
/* .param p3, "append" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;Z)V" */
/* } */
} // .end annotation
/* .line 562 */
/* .local p2, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = com.android.server.am.ProcessPolicy.sLock;
/* monitor-enter v0 */
/* .line 563 */
/* and-int/lit8 v1, p1, 0x1 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 564 */
try { // :try_start_0
v1 = com.android.server.am.ProcessPolicy.sStaticWhiteList;
/* .local v1, "targetWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* .line 585 */
} // .end local v1 # "targetWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
/* :catchall_0 */
/* move-exception v1 */
/* .line 565 */
} // :cond_0
/* and-int/lit8 v1, p1, 0x4 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 566 */
v1 = com.android.server.am.ProcessPolicy.sCloudWhiteList;
/* .restart local v1 # "targetWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* .line 567 */
} // .end local v1 # "targetWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_1
/* and-int/lit8 v1, p1, 0x10 */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 568 */
v1 = com.android.server.am.ProcessPolicy.sDisableTrimList;
/* .restart local v1 # "targetWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* .line 569 */
} // .end local v1 # "targetWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_2
/* and-int/lit8 v1, p1, 0x20 */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 570 */
v1 = com.android.server.am.ProcessPolicy.sDisableForceStopList;
/* .restart local v1 # "targetWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* .line 572 */
} // .end local v1 # "targetWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_3
/* sget-boolean v1, Lcom/miui/enterprise/settings/EnterpriseSettings;->ENTERPRISE_ACTIVATED:Z */
if ( v1 != null) { // if-eqz v1, :cond_4
/* and-int/lit16 v1, p1, 0x1000 */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 574 */
v1 = com.android.server.am.ProcessPolicy.sEnterpriseAppList;
/* .restart local v1 # "targetWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* .line 577 */
} // .end local v1 # "targetWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_4
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 578 */
/* .restart local v1 # "targetWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
final String v2 = "ProcessManager"; // const-string v2, "ProcessManager"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "addWhiteList with unknown flag="; // const-string v4, "addWhiteList with unknown flag="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* .line 581 */
} // :goto_0
/* if-nez p3, :cond_5 */
/* .line 582 */
/* .line 584 */
} // :cond_5
/* .line 585 */
/* monitor-exit v0 */
/* .line 586 */
return;
/* .line 585 */
} // .end local v1 # "targetWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :goto_1
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
void dump ( java.io.PrintWriter p0, java.lang.String p1 ) {
/* .locals 6 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "prefix" # Ljava/lang/String; */
/* .line 1234 */
final String v0 = "Process Policy:"; // const-string v0, "Process Policy:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1236 */
v0 = com.android.server.am.ProcessPolicy.sDynamicWhiteList;
v0 = (( java.util.HashMap ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->size()I
/* if-lez v0, :cond_0 */
/* .line 1237 */
final String v0 = "DY:"; // const-string v0, "DY:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1238 */
v0 = com.android.server.am.ProcessPolicy.sDynamicWhiteList;
(( java.util.HashMap ) v0 ).keySet ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;
/* .line 1239 */
/* .local v0, "dynamic":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* .line 1240 */
/* .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;" */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1241 */
/* check-cast v2, Ljava/lang/String; */
/* .line 1242 */
/* .local v2, "pkg":Ljava/lang/String; */
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1243 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " : "; // const-string v4, " : "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = com.android.server.am.ProcessPolicy.sDynamicWhiteList;
(( java.util.HashMap ) v4 ).get ( v2 ); // invoke-virtual {v4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1244 */
} // .end local v2 # "pkg":Ljava/lang/String;
/* .line 1247 */
} // .end local v0 # "dynamic":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
} // .end local v1 # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
} // :cond_0
v0 = v0 = com.android.server.am.ProcessPolicy.sCloudWhiteList;
/* if-lez v0, :cond_1 */
/* .line 1248 */
final String v0 = "CL:"; // const-string v0, "CL:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1249 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_1
v1 = v1 = com.android.server.am.ProcessPolicy.sCloudWhiteList;
/* if-ge v0, v1, :cond_1 */
/* .line 1250 */
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1251 */
v1 = com.android.server.am.ProcessPolicy.sCloudWhiteList;
/* check-cast v1, Ljava/lang/String; */
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1249 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 1255 */
} // .end local v0 # "i":I
} // :cond_1
v0 = com.android.server.am.ProcessPolicy.sLockedApplicationList;
v0 = (( java.util.HashMap ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->size()I
/* if-lez v0, :cond_3 */
/* .line 1256 */
final String v0 = "LO:"; // const-string v0, "LO:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1257 */
v0 = com.android.server.am.ProcessPolicy.sLockedApplicationList;
(( java.util.HashMap ) v0 ).keySet ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;
/* .line 1258 */
/* .local v0, "userIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
v2 = } // :goto_2
if ( v2 != null) { // if-eqz v2, :cond_3
/* check-cast v2, Ljava/lang/Integer; */
/* .line 1259 */
/* .local v2, "userId":Ljava/lang/Integer; */
v3 = com.android.server.am.ProcessPolicy.sLockedApplicationList;
(( java.util.HashMap ) v3 ).get ( v2 ); // invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Ljava/util/Set; */
/* .line 1260 */
/* .local v3, "lockedApplication":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1261 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "userId=" */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v4 ); // invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1262 */
v5 = } // :goto_3
if ( v5 != null) { // if-eqz v5, :cond_2
/* check-cast v5, Ljava/lang/String; */
/* .line 1263 */
/* .local v5, "app":Ljava/lang/String; */
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1264 */
(( java.io.PrintWriter ) p1 ).println ( v5 ); // invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1265 */
} // .end local v5 # "app":Ljava/lang/String;
/* .line 1266 */
} // .end local v2 # "userId":Ljava/lang/Integer;
} // .end local v3 # "lockedApplication":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
} // :cond_2
/* .line 1269 */
} // .end local v0 # "userIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
} // :cond_3
v0 = v0 = com.android.server.am.ProcessPolicy.sFastBootAppMap;
/* if-lez v0, :cond_4 */
/* .line 1270 */
final String v0 = "FA:"; // const-string v0, "FA:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1271 */
v0 = com.android.server.am.ProcessPolicy.sFastBootAppMap;
v1 = } // :goto_4
if ( v1 != null) { // if-eqz v1, :cond_4
/* check-cast v1, Ljava/lang/String; */
/* .line 1272 */
/* .local v1, "protectedPackage":Ljava/lang/String; */
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1273 */
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1274 */
} // .end local v1 # "protectedPackage":Ljava/lang/String;
/* .line 1277 */
} // :cond_4
/* sget-boolean v0, Lcom/miui/enterprise/settings/EnterpriseSettings;->ENTERPRISE_ACTIVATED:Z */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 1278 */
final String v0 = "EP Activated: true"; // const-string v0, "EP Activated: true"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1279 */
v0 = v0 = com.android.server.am.ProcessPolicy.sEnterpriseAppList;
/* if-lez v0, :cond_5 */
/* .line 1280 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_5
v1 = v1 = com.android.server.am.ProcessPolicy.sEnterpriseAppList;
/* if-ge v0, v1, :cond_5 */
/* .line 1281 */
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1282 */
v1 = com.android.server.am.ProcessPolicy.sEnterpriseAppList;
/* check-cast v1, Ljava/lang/String; */
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1280 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 1288 */
} // .end local v0 # "i":I
} // :cond_5
v0 = v0 = com.android.server.am.ProcessPolicy.sSecretlyProtectAppList;
/* if-lez v0, :cond_6 */
/* .line 1289 */
final String v0 = "SPAL:"; // const-string v0, "SPAL:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1290 */
int v0 = 0; // const/4 v0, 0x0
/* .restart local v0 # "i":I */
} // :goto_6
v1 = v1 = com.android.server.am.ProcessPolicy.sSecretlyProtectAppList;
/* if-ge v0, v1, :cond_6 */
/* .line 1291 */
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1292 */
v1 = com.android.server.am.ProcessPolicy.sSecretlyProtectAppList;
/* check-cast v1, Ljava/lang/String; */
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1290 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 1296 */
} // .end local v0 # "i":I
} // :cond_6
v0 = com.android.server.am.ProcessPolicy.sActiveUidList;
v0 = (( android.util.SparseArray ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/SparseArray;->size()I
/* if-lez v0, :cond_7 */
/* .line 1297 */
final String v0 = "ACU:"; // const-string v0, "ACU:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1298 */
int v0 = 0; // const/4 v0, 0x0
/* .restart local v0 # "i":I */
} // :goto_7
v1 = com.android.server.am.ProcessPolicy.sActiveUidList;
v1 = (( android.util.SparseArray ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/SparseArray;->size()I
/* if-ge v0, v1, :cond_7 */
/* .line 1299 */
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1300 */
v1 = com.android.server.am.ProcessPolicy.sActiveUidList;
(( android.util.SparseArray ) v1 ).valueAt ( v0 ); // invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
/* .line 1298 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 1304 */
} // .end local v0 # "i":I
} // :cond_7
com.android.server.am.OomAdjusterImpl .getInstance ( );
(( com.android.server.am.OomAdjusterImpl ) v0 ).dumpPreviousBackgroundApps ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/am/OomAdjusterImpl;->dumpPreviousBackgroundApps(Ljava/io/PrintWriter;Ljava/lang/String;)V
/* .line 1306 */
com.android.server.am.OomAdjusterImpl .getInstance ( );
(( com.android.server.am.OomAdjusterImpl ) v0 ).dumpPreviousResidentApps ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/am/OomAdjusterImpl;->dumpPreviousResidentApps(Ljava/io/PrintWriter;Ljava/lang/String;)V
/* .line 1307 */
return;
} // .end method
public java.util.List getActiveUidList ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "flag" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1105 */
v0 = com.android.server.am.ProcessPolicy.sLock;
/* monitor-enter v0 */
/* .line 1106 */
try { // :try_start_0
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 1107 */
/* .local v1, "records":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
v2 = com.android.server.am.ProcessPolicy.sActiveUidList;
v2 = (( android.util.SparseArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/SparseArray;->size()I
/* add-int/lit8 v2, v2, -0x1 */
/* .local v2, "i":I */
} // :goto_0
/* if-ltz v2, :cond_1 */
/* .line 1108 */
v3 = com.android.server.am.ProcessPolicy.sActiveUidList;
(( android.util.SparseArray ) v3 ).valueAt ( v2 ); // invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
/* .line 1109 */
/* .local v3, "r":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
/* iget v4, v3, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->flag:I */
/* and-int/2addr v4, p1 */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 1110 */
/* iget v4, v3, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->uid:I */
java.lang.Integer .valueOf ( v4 );
/* .line 1107 */
} // .end local v3 # "r":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;
} // :cond_0
/* add-int/lit8 v2, v2, -0x1 */
/* .line 1113 */
} // .end local v2 # "i":I
} // :cond_1
/* monitor-exit v0 */
/* .line 1114 */
} // .end local v1 # "records":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public java.util.List getActiveUidRecordList ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "flag" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1092 */
v0 = com.android.server.am.ProcessPolicy.sLock;
/* monitor-enter v0 */
/* .line 1093 */
try { // :try_start_0
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 1094 */
/* .local v1, "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;>;" */
v2 = com.android.server.am.ProcessPolicy.sActiveUidList;
v2 = (( android.util.SparseArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/SparseArray;->size()I
/* add-int/lit8 v2, v2, -0x1 */
/* .local v2, "i":I */
} // :goto_0
/* if-ltz v2, :cond_1 */
/* .line 1095 */
v3 = com.android.server.am.ProcessPolicy.sActiveUidList;
(( android.util.SparseArray ) v3 ).valueAt ( v2 ); // invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
/* .line 1096 */
/* .local v3, "r":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
/* iget v4, v3, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->flag:I */
/* and-int/2addr v4, p1 */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 1097 */
/* .line 1094 */
} // .end local v3 # "r":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;
} // :cond_0
/* add-int/lit8 v2, v2, -0x1 */
/* .line 1100 */
} // .end local v2 # "i":I
} // :cond_1
/* monitor-exit v0 */
/* .line 1101 */
} // .end local v1 # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;>;"
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public java.util.List getLockedApplication ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 836 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 837 */
/* .local v0, "lockedApps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v1 = com.android.server.am.ProcessPolicy.sLockedApplicationList;
java.lang.Integer .valueOf ( p1 );
(( java.util.HashMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/util/Set; */
/* .line 838 */
/* .local v1, "userApps":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
v2 = if ( v1 != null) { // if-eqz v1, :cond_0
/* if-lez v2, :cond_0 */
/* .line 839 */
/* .line 841 */
} // :cond_0
v2 = com.android.server.am.ProcessPolicy.sLockedApplicationList;
/* const/16 v3, -0x64 */
java.lang.Integer .valueOf ( v3 );
(( java.util.HashMap ) v2 ).get ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Ljava/util/Collection; */
/* .line 842 */
} // .end method
public java.util.HashMap getOneKeyCleanWhiteList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
/* .line 977 */
v0 = com.android.server.am.ProcessPolicy.sOneKeyCleanWhiteList;
} // .end method
public Integer getPolicyFlags ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "policy" # I */
/* .line 1118 */
v0 = com.android.server.am.ProcessPolicy.sPolicyFlagsList;
v0 = (( android.util.SparseArray ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->contains(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1119 */
v0 = com.android.server.am.ProcessPolicy.sPolicyFlagsList;
(( android.util.SparseArray ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Integer; */
v0 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* .line 1121 */
} // :cond_0
int v0 = -1; // const/4 v0, -0x1
} // .end method
public java.util.List getWhiteList ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "flags" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 519 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 521 */
/* .local v0, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v1 = com.android.server.am.ProcessPolicy.sLock;
/* monitor-enter v1 */
/* .line 522 */
/* and-int/lit8 v2, p1, 0x1 */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 523 */
try { // :try_start_0
v2 = com.android.server.am.ProcessPolicy.sStaticWhiteList;
/* .line 556 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 525 */
} // :cond_0
} // :goto_0
/* and-int/lit8 v2, p1, 0x2 */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 526 */
v2 = com.android.server.am.ProcessPolicy.sDynamicWhiteList;
(( java.util.HashMap ) v2 ).keySet ( ); // invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;
/* .line 528 */
} // :cond_1
/* and-int/lit8 v2, p1, 0x4 */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 529 */
v2 = com.android.server.am.ProcessPolicy.sCloudWhiteList;
/* .line 531 */
} // :cond_2
/* and-int/lit8 v2, p1, 0x10 */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 532 */
v2 = com.android.server.am.ProcessPolicy.sDisableTrimList;
/* .line 534 */
} // :cond_3
/* and-int/lit8 v2, p1, 0x20 */
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 535 */
v2 = com.android.server.am.ProcessPolicy.sDisableForceStopList;
/* .line 537 */
} // :cond_4
/* and-int/lit8 v2, p1, 0x40 */
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 538 */
v2 = com.android.server.am.ProcessPolicy.sEnableCallProtectList;
/* .line 540 */
} // :cond_5
/* and-int/lit16 v2, p1, 0x80 */
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 541 */
v2 = com.android.server.am.ProcessPolicy.sNeedTraceList;
/* .line 543 */
} // :cond_6
/* and-int/lit16 v2, p1, 0x400 */
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 544 */
v2 = com.android.server.am.ProcessPolicy.sSecretlyProtectAppList;
/* .line 546 */
} // :cond_7
/* and-int/lit16 v2, p1, 0x800 */
if ( v2 != null) { // if-eqz v2, :cond_8
/* .line 547 */
v2 = com.android.server.am.ProcessPolicy.sFastBootAppMap;
/* .line 549 */
} // :cond_8
/* and-int/lit16 v2, p1, 0x2000 */
if ( v2 != null) { // if-eqz v2, :cond_9
/* .line 550 */
v2 = com.android.server.am.ProcessPolicy.sUserRequestCleanWhiteList;
/* .line 552 */
} // :cond_9
/* sget-boolean v2, Lcom/miui/enterprise/settings/EnterpriseSettings;->ENTERPRISE_ACTIVATED:Z */
if ( v2 != null) { // if-eqz v2, :cond_a
/* and-int/lit16 v2, p1, 0x1000 */
if ( v2 != null) { // if-eqz v2, :cond_a
/* .line 554 */
v2 = com.android.server.am.ProcessPolicy.sEnterpriseAppList;
/* .line 556 */
} // :cond_a
/* monitor-exit v1 */
/* .line 557 */
/* .line 556 */
} // :goto_1
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public Boolean isFastBootEnable ( java.lang.String p0, Integer p1, Boolean p2 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .param p3, "checkPss" # Z */
/* .line 960 */
/* if-lez p2, :cond_0 */
v0 = (( com.android.server.am.ProcessPolicy ) p0 ).isInFastBootList ( p1, p2, p3 ); // invoke-virtual {p0, p1, p2, p3}, Lcom/android/server/am/ProcessPolicy;->isInFastBootList(Ljava/lang/String;IZ)Z
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mProcessManagerService;
/* .line 961 */
v0 = (( com.android.server.am.ProcessManagerService ) v0 ).isAllowAutoStart ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/am/ProcessManagerService;->isAllowAutoStart(Ljava/lang/String;I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 960 */
} // :goto_0
} // .end method
public Boolean isInAppProtectList ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 981 */
v0 = com.android.server.am.ProcessPolicy.sLock;
/* monitor-enter v0 */
/* .line 982 */
try { // :try_start_0
v1 = v1 = com.android.server.am.ProcessPolicy.sAppProtectMap;
/* monitor-exit v0 */
/* .line 983 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isInDisplaySizeBlackList ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .line 999 */
v0 = com.android.server.am.ProcessPolicy.sLock;
/* monitor-enter v0 */
/* .line 1000 */
try { // :try_start_0
v1 = v1 = com.android.server.am.ProcessPolicy.sDisplaySizeBlackList;
/* monitor-exit v0 */
/* .line 1001 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isInDisplaySizeWhiteList ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .line 993 */
v0 = com.android.server.am.ProcessPolicy.sLock;
/* monitor-enter v0 */
/* .line 994 */
try { // :try_start_0
v1 = v1 = com.android.server.am.ProcessPolicy.sDisplaySizeProtectList;
/* monitor-exit v0 */
/* .line 995 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public android.util.Pair isInDynamicList ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 5 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/android/server/am/ProcessRecord;", */
/* ")", */
/* "Landroid/util/Pair<", */
/* "Ljava/lang/Boolean;", */
/* "Ljava/lang/Boolean;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 948 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 949 */
v0 = com.android.server.am.ProcessPolicy.sLock;
/* monitor-enter v0 */
/* .line 950 */
try { // :try_start_0
v1 = this.info;
v1 = this.packageName;
/* .line 951 */
/* .local v1, "packageName":Ljava/lang/String; */
v2 = com.android.server.am.ProcessPolicy.sDynamicWhiteList;
v2 = (( java.util.HashMap ) v2 ).keySet ( ); // invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 952 */
/* new-instance v2, Landroid/util/Pair; */
v3 = java.lang.Boolean.TRUE;
v4 = com.android.server.am.ProcessPolicy.sDynamicWhiteList;
(( java.util.HashMap ) v4 ).get ( v1 ); // invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v4, Ljava/lang/Boolean; */
/* invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
/* monitor-exit v0 */
/* .line 954 */
} // .end local v1 # "packageName":Ljava/lang/String;
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 956 */
} // :cond_1
} // :goto_0
/* new-instance v0, Landroid/util/Pair; */
v1 = java.lang.Boolean.FALSE;
v2 = java.lang.Boolean.FALSE;
/* invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
} // .end method
public Boolean isInExtraPackageList ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1017 */
v0 = com.android.server.am.ProcessPolicy.sLock;
/* monitor-enter v0 */
/* .line 1018 */
try { // :try_start_0
v1 = v1 = com.android.server.am.ProcessPolicy.sExtraPackageWhiteList;
/* monitor-exit v0 */
/* .line 1019 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isInFastBootList ( java.lang.String p0, Integer p1, Boolean p2 ) {
/* .locals 7 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .param p3, "checkPss" # Z */
/* .line 965 */
if ( p3 != null) { // if-eqz p3, :cond_0
v0 = android.os.UserHandle .getUserId ( p2 );
com.android.server.am.ProcessUtils .getPackageLastPss ( p1,v0 );
/* move-result-wide v0 */
} // :cond_0
/* const-wide/16 v0, 0x0 */
/* .line 966 */
/* .local v0, "pss":J */
} // :goto_0
v2 = com.android.server.am.ProcessPolicy.sLock;
/* monitor-enter v2 */
/* .line 967 */
try { // :try_start_0
v3 = v3 = com.android.server.am.ProcessPolicy.sFastBootAppMap;
/* .line 968 */
/* .local v3, "res":Z */
if ( v3 != null) { // if-eqz v3, :cond_1
if ( p3 != null) { // if-eqz p3, :cond_1
v4 = com.android.server.am.ProcessPolicy.sFastBootAppMap;
/* check-cast v4, Ljava/lang/Long; */
(( java.lang.Long ) v4 ).longValue ( ); // invoke-virtual {v4}, Ljava/lang/Long;->longValue()J
/* move-result-wide v4 */
/* cmp-long v4, v0, v4 */
/* if-lez v4, :cond_1 */
/* .line 969 */
final String v4 = "ProcessManager"; // const-string v4, "ProcessManager"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "ignore fast boot, caused pkg:"; // const-string v6, "ignore fast boot, caused pkg:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " is too large, with pss:"; // const-string v6, " is too large, with pss:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0, v1 ); // invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .w ( v4,v5 );
/* .line 970 */
int v3 = 0; // const/4 v3, 0x0
/* .line 972 */
} // :cond_1
/* monitor-exit v2 */
/* .line 973 */
} // .end local v3 # "res":Z
/* :catchall_0 */
/* move-exception v3 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v3 */
} // .end method
public Boolean isInProcessStaticWhiteList ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .line 987 */
v0 = com.android.server.am.ProcessPolicy.sLock;
/* monitor-enter v0 */
/* .line 988 */
try { // :try_start_0
v1 = v1 = com.android.server.am.ProcessPolicy.sProcessStaticWhiteList;
/* monitor-exit v0 */
/* .line 989 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isInSecretlyProtectList ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .line 1005 */
v0 = com.android.server.am.ProcessPolicy.sLock;
/* monitor-enter v0 */
/* .line 1006 */
try { // :try_start_0
v1 = v1 = com.android.server.am.ProcessPolicy.sSecretlyProtectAppList;
/* monitor-exit v0 */
/* .line 1007 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isInSystemCleanWhiteList ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .line 1011 */
v0 = com.android.server.am.ProcessPolicy.sLock;
/* monitor-enter v0 */
/* .line 1012 */
try { // :try_start_0
v1 = v1 = com.android.server.am.ProcessPolicy.sSystemCleanWhiteList;
/* monitor-exit v0 */
/* .line 1013 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isLockedApplication ( java.lang.String p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 811 */
v0 = com.android.server.am.ProcessPolicy.sLock;
/* monitor-enter v0 */
/* .line 812 */
try { // :try_start_0
v1 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/am/ProcessPolicy;->isLockedApplicationForUserId(Ljava/lang/String;I)Z */
/* if-nez v1, :cond_1 */
/* .line 813 */
/* const/16 v1, -0x64 */
v1 = /* invoke-direct {p0, p1, v1}, Lcom/android/server/am/ProcessPolicy;->isLockedApplicationForUserId(Ljava/lang/String;I)Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 816 */
} // :cond_0
/* monitor-exit v0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 814 */
} // :cond_1
} // :goto_0
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 817 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isProcessImportant ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 3 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 935 */
(( com.android.server.am.ProcessPolicy ) p0 ).isInDynamicList ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessPolicy;->isInDynamicList(Lcom/android/server/am/ProcessRecord;)Landroid/util/Pair;
/* .line 936 */
/* .local v0, "isInDynamicPair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Boolean;Ljava/lang/Boolean;>;" */
v1 = this.first;
/* check-cast v1, Ljava/lang/Boolean; */
v1 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
if ( v1 != null) { // if-eqz v1, :cond_1
v1 = this.second;
/* check-cast v1, Ljava/lang/Boolean; */
v1 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = this.mServices;
/* .line 937 */
v1 = (( com.android.server.am.ProcessServiceRecord ) v1 ).hasForegroundServices ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessServiceRecord;->hasForegroundServices()Z
/* if-nez v1, :cond_0 */
v1 = this.mState;
v1 = (( com.android.server.am.ProcessStateRecord ) v1 ).getCurAdj ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I
/* if-gt v1, v2, :cond_1 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
/* .line 936 */
} // :goto_0
} // .end method
public void noteAudioOffLocked ( Integer p0 ) {
/* .locals 6 */
/* .param p1, "uid" # I */
/* .line 1152 */
v0 = android.os.UserHandle .isApp ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1153 */
v0 = com.android.server.am.ProcessPolicy.sLock;
/* monitor-enter v0 */
/* .line 1154 */
try { // :try_start_0
v1 = com.android.server.am.ProcessPolicy.sActiveUidList;
(( android.util.SparseArray ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
/* .line 1155 */
/* .local v1, "r":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1156 */
v2 = com.android.server.am.ProcessPolicy.sTempInactiveAudioList;
(( android.util.SparseArray ) v2 ).put ( p1, v1 ); // invoke-virtual {v2, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 1157 */
v2 = this.mActiveUpdateHandler;
int v3 = 1; // const/4 v3, 0x1
(( com.android.server.am.ProcessPolicy$ActiveUpdateHandler ) v2 ).obtainMessage ( v3, v1 ); // invoke-virtual {v2, v3, v1}, Lcom/android/server/am/ProcessPolicy$ActiveUpdateHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 1158 */
/* .local v2, "msg":Landroid/os/Message; */
v3 = this.mActiveUpdateHandler;
/* const-wide/16 v4, 0x258 */
(( com.android.server.am.ProcessPolicy$ActiveUpdateHandler ) v3 ).sendMessageDelayed ( v2, v4, v5 ); // invoke-virtual {v3, v2, v4, v5}, Lcom/android/server/am/ProcessPolicy$ActiveUpdateHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 1160 */
final String v3 = "ProcessManager"; // const-string v3, "ProcessManager"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "add temp remove audio inactive uid : "; // const-string v5, "add temp remove audio inactive uid : "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v4 );
/* .line 1163 */
} // .end local v1 # "r":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;
} // .end local v2 # "msg":Landroid/os/Message;
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 1165 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void noteAudioOnLocked ( Integer p0 ) {
/* .locals 6 */
/* .param p1, "uid" # I */
/* .line 1126 */
v0 = android.os.UserHandle .isApp ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1127 */
v0 = com.android.server.am.ProcessPolicy.sLock;
/* monitor-enter v0 */
/* .line 1128 */
try { // :try_start_0
v1 = com.android.server.am.ProcessPolicy.sTempInactiveAudioList;
(( android.util.SparseArray ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
/* .line 1129 */
/* .local v1, "temp":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
int v2 = 1; // const/4 v2, 0x1
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1130 */
v3 = this.mActiveUpdateHandler;
(( com.android.server.am.ProcessPolicy$ActiveUpdateHandler ) v3 ).removeMessages ( v2, v1 ); // invoke-virtual {v3, v2, v1}, Lcom/android/server/am/ProcessPolicy$ActiveUpdateHandler;->removeMessages(ILjava/lang/Object;)V
/* .line 1131 */
v2 = com.android.server.am.ProcessPolicy.sTempInactiveAudioList;
(( android.util.SparseArray ) v2 ).remove ( p1 ); // invoke-virtual {v2, p1}, Landroid/util/SparseArray;->remove(I)V
/* .line 1133 */
final String v2 = "ProcessManager"; // const-string v2, "ProcessManager"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "remove temp audio active uid : "; // const-string v4, "remove temp audio active uid : "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 1136 */
} // :cond_0
v3 = com.android.server.am.ProcessPolicy.sActiveUidList;
(( android.util.SparseArray ) v3 ).get ( p1 ); // invoke-virtual {v3, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
/* .line 1137 */
/* .local v3, "r":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
/* if-nez v3, :cond_1 */
/* .line 1138 */
/* new-instance v4, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
/* invoke-direct {v4, p1}, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;-><init>(I)V */
/* move-object v3, v4 */
/* .line 1140 */
} // :cond_1
/* iget v4, v3, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->flag:I */
/* or-int/2addr v2, v4 */
/* iput v2, v3, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->flag:I */
/* .line 1141 */
v2 = com.android.server.am.ProcessPolicy.sActiveUidList;
(( android.util.SparseArray ) v2 ).put ( p1, v3 ); // invoke-virtual {v2, p1, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 1143 */
final String v2 = "ProcessManager"; // const-string v2, "ProcessManager"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "add audio active uid : "; // const-string v5, "add audio active uid : "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v4 );
/* .line 1146 */
} // .end local v1 # "temp":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;
} // .end local v3 # "r":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;
} // :goto_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 1148 */
} // :cond_2
} // :goto_1
return;
} // .end method
public void noteResetAudioLocked ( ) {
/* .locals 7 */
/* .line 1169 */
v0 = com.android.server.am.ProcessPolicy.sLock;
/* monitor-enter v0 */
/* .line 1170 */
try { // :try_start_0
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 1171 */
/* .local v1, "removed":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;>;" */
v2 = com.android.server.am.ProcessPolicy.sActiveUidList;
v2 = (( android.util.SparseArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/SparseArray;->size()I
/* .line 1172 */
/* .local v2, "N":I */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
/* if-ge v3, v2, :cond_1 */
/* .line 1173 */
v4 = com.android.server.am.ProcessPolicy.sActiveUidList;
(( android.util.SparseArray ) v4 ).valueAt ( v3 ); // invoke-virtual {v4, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v4, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
/* .line 1174 */
/* .local v4, "r":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
/* iget v5, v4, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->flag:I */
/* and-int/lit8 v5, v5, -0x2 */
/* iput v5, v4, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->flag:I */
/* .line 1175 */
/* iget v5, v4, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->flag:I */
/* if-nez v5, :cond_0 */
/* .line 1176 */
/* .line 1172 */
} // .end local v4 # "r":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;
} // :cond_0
/* add-int/lit8 v3, v3, 0x1 */
/* .line 1180 */
} // .end local v3 # "i":I
} // :cond_1
v4 = } // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_2
/* check-cast v4, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
/* .line 1181 */
/* .restart local v4 # "r":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
v5 = com.android.server.am.ProcessPolicy.sActiveUidList;
/* iget v6, v4, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->uid:I */
(( android.util.SparseArray ) v5 ).remove ( v6 ); // invoke-virtual {v5, v6}, Landroid/util/SparseArray;->remove(I)V
/* .line 1182 */
} // .end local v4 # "r":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;
/* .line 1185 */
} // :cond_2
final String v3 = "ProcessManager"; // const-string v3, "ProcessManager"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " noteResetAudioLocked removed ActiveUids : "; // const-string v5, " noteResetAudioLocked removed ActiveUids : "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .toString ( v5 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v4 );
/* .line 1187 */
/* nop */
} // .end local v1 # "removed":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;>;"
} // .end local v2 # "N":I
/* monitor-exit v0 */
/* .line 1188 */
return;
/* .line 1187 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void noteStartGpsLocked ( Integer p0 ) {
/* .locals 6 */
/* .param p1, "uid" # I */
/* .line 1192 */
v0 = android.os.UserHandle .isApp ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1193 */
v0 = com.android.server.am.ProcessPolicy.sLock;
/* monitor-enter v0 */
/* .line 1194 */
try { // :try_start_0
v1 = com.android.server.am.ProcessPolicy.sTempInactiveGPSList;
(( android.util.SparseArray ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
/* .line 1195 */
/* .local v1, "temp":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
int v2 = 2; // const/4 v2, 0x2
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1196 */
v3 = this.mActiveUpdateHandler;
(( com.android.server.am.ProcessPolicy$ActiveUpdateHandler ) v3 ).removeMessages ( v2, v1 ); // invoke-virtual {v3, v2, v1}, Lcom/android/server/am/ProcessPolicy$ActiveUpdateHandler;->removeMessages(ILjava/lang/Object;)V
/* .line 1197 */
v2 = com.android.server.am.ProcessPolicy.sTempInactiveGPSList;
(( android.util.SparseArray ) v2 ).remove ( p1 ); // invoke-virtual {v2, p1}, Landroid/util/SparseArray;->remove(I)V
/* .line 1199 */
final String v2 = "ProcessManager"; // const-string v2, "ProcessManager"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "remove temp gps active uid : "; // const-string v4, "remove temp gps active uid : "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 1202 */
} // :cond_0
v3 = com.android.server.am.ProcessPolicy.sActiveUidList;
(( android.util.SparseArray ) v3 ).get ( p1 ); // invoke-virtual {v3, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
/* .line 1203 */
/* .local v3, "r":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
/* if-nez v3, :cond_1 */
/* .line 1204 */
/* new-instance v4, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
/* invoke-direct {v4, p1}, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;-><init>(I)V */
/* move-object v3, v4 */
/* .line 1206 */
} // :cond_1
/* iget v4, v3, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->flag:I */
/* or-int/2addr v2, v4 */
/* iput v2, v3, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->flag:I */
/* .line 1207 */
v2 = com.android.server.am.ProcessPolicy.sActiveUidList;
(( android.util.SparseArray ) v2 ).put ( p1, v3 ); // invoke-virtual {v2, p1, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 1209 */
final String v2 = "ProcessManager"; // const-string v2, "ProcessManager"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "add gps active uid : "; // const-string v5, "add gps active uid : "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v4 );
/* .line 1212 */
} // .end local v1 # "temp":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;
} // .end local v3 # "r":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;
} // :goto_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 1214 */
} // :cond_2
} // :goto_1
return;
} // .end method
public void noteStopGpsLocked ( Integer p0 ) {
/* .locals 6 */
/* .param p1, "uid" # I */
/* .line 1218 */
v0 = android.os.UserHandle .isApp ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1219 */
v0 = com.android.server.am.ProcessPolicy.sLock;
/* monitor-enter v0 */
/* .line 1220 */
try { // :try_start_0
v1 = com.android.server.am.ProcessPolicy.sActiveUidList;
(( android.util.SparseArray ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
/* .line 1221 */
/* .local v1, "r":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1222 */
v2 = com.android.server.am.ProcessPolicy.sTempInactiveGPSList;
(( android.util.SparseArray ) v2 ).put ( p1, v1 ); // invoke-virtual {v2, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 1223 */
v2 = this.mActiveUpdateHandler;
int v3 = 2; // const/4 v3, 0x2
(( com.android.server.am.ProcessPolicy$ActiveUpdateHandler ) v2 ).obtainMessage ( v3, v1 ); // invoke-virtual {v2, v3, v1}, Lcom/android/server/am/ProcessPolicy$ActiveUpdateHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 1224 */
/* .local v2, "msg":Landroid/os/Message; */
v3 = this.mActiveUpdateHandler;
/* const-wide/16 v4, 0x3e8 */
(( com.android.server.am.ProcessPolicy$ActiveUpdateHandler ) v3 ).sendMessageDelayed ( v2, v4, v5 ); // invoke-virtual {v3, v2, v4, v5}, Lcom/android/server/am/ProcessPolicy$ActiveUpdateHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 1226 */
final String v3 = "ProcessManager"; // const-string v3, "ProcessManager"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "add temp remove gps inactive uid : "; // const-string v5, "add temp remove gps inactive uid : "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v4 );
/* .line 1229 */
} // .end local v1 # "r":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;
} // .end local v2 # "msg":Landroid/os/Message;
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 1231 */
} // :cond_1
} // :goto_0
return;
} // .end method
protected void promoteLockedApp ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 7 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 726 */
v0 = (( com.android.server.am.ProcessRecord ) p1 ).isPersistent ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isPersistent()Z
final String v1 = "ProcessManager"; // const-string v1, "ProcessManager"
/* if-nez v0, :cond_3 */
v0 = this.processName;
v0 = (( com.android.server.am.ProcessPolicy ) p0 ).isInSecretlyProtectList ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/am/ProcessPolicy;->isInSecretlyProtectList(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 733 */
} // :cond_0
v0 = this.processName;
/* iget v2, p1, Lcom/android/server/am/ProcessRecord;->userId:I */
v0 = (( com.android.server.am.ProcessPolicy ) p0 ).isLockedApplication ( v0, v2 ); // invoke-virtual {p0, v0, v2}, Lcom/android/server/am/ProcessPolicy;->isLockedApplication(Ljava/lang/String;I)Z
/* .line 735 */
/* .local v0, "isLocked":Z */
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_1
/* const/16 v2, 0x3e9 */
/* .line 736 */
/* .local v2, "targetMaxAdj":I */
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 737 */
} // :cond_2
/* const/16 v3, 0x14 */
/* .line 739 */
/* .local v3, "targetMaxProcState":I */
} // :goto_1
/* invoke-direct {p0, p1, v2, v0}, Lcom/android/server/am/ProcessPolicy;->updateMaxAdjLocked(Lcom/android/server/am/ProcessRecord;IZ)V */
/* .line 740 */
/* invoke-direct {p0, p1, v3, v0}, Lcom/android/server/am/ProcessPolicy;->updateMaxProcStateLocked(Lcom/android/server/am/ProcessRecord;IZ)V */
/* .line 742 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "promoteLockedApp:"; // const-string v5, "promoteLockedApp:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v5 = ", set "; // const-string v5, ", set "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.processName;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " maxAdj to "; // const-string v5, " maxAdj to "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.mState;
/* .line 743 */
v5 = (( com.android.server.am.ProcessStateRecord ) v5 ).getMaxAdj ( ); // invoke-virtual {v5}, Lcom/android/server/am/ProcessStateRecord;->getMaxAdj()I
int v6 = 0; // const/4 v6, 0x0
com.android.server.am.ProcessList .makeOomAdjString ( v5,v6 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ", maxProcState to + "; // const-string v5, ", maxProcState to + "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 744 */
v5 = com.android.server.am.IProcessPolicy .getAppMaxProcState ( p1 );
com.android.server.am.ProcessList .makeProcStateString ( v5 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 742 */
android.util.Slog .d ( v1,v4 );
/* .line 745 */
return;
/* .line 728 */
} // .end local v0 # "isLocked":Z
} // .end local v2 # "targetMaxAdj":I
} // .end local v3 # "targetMaxProcState":I
} // :cond_3
} // :goto_2
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "do not promote "; // const-string v2, "do not promote "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.processName;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v0 );
/* .line 730 */
return;
} // .end method
public Boolean protectCurrentProcess ( com.android.server.am.ProcessRecord p0, Boolean p1 ) {
/* .locals 5 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "isProtected" # Z */
/* .line 1031 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_2
v1 = this.info;
/* if-nez v1, :cond_0 */
/* .line 1035 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 1036 */
/* .local v1, "priorityLevel":Ljava/lang/Integer; */
v2 = com.android.server.am.ProcessPolicy.sLock;
/* monitor-enter v2 */
/* .line 1037 */
try { // :try_start_0
v3 = com.android.server.am.ProcessPolicy.sAppProtectMap;
v4 = this.info;
v4 = this.packageName;
/* check-cast v3, Ljava/lang/Integer; */
/* move-object v1, v3 */
/* .line 1038 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1040 */
/* if-nez v1, :cond_1 */
/* .line 1041 */
/* .line 1044 */
} // :cond_1
v2 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* invoke-direct {p0, p1, v2, p2}, Lcom/android/server/am/ProcessPolicy;->updateProcessPriority(Lcom/android/server/am/ProcessRecord;IZ)V */
/* .line 1045 */
final String v2 = "ProcessManager"; // const-string v2, "ProcessManager"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "protectCurrentProcess:"; // const-string v4, "protectCurrentProcess:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = ", set "; // const-string v4, ", set "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.processName;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " maxAdj to "; // const-string v4, " maxAdj to "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mState;
/* .line 1046 */
v4 = (( com.android.server.am.ProcessStateRecord ) v4 ).getMaxAdj ( ); // invoke-virtual {v4}, Lcom/android/server/am/ProcessStateRecord;->getMaxAdj()I
com.android.server.am.ProcessList .makeOomAdjString ( v4,v0 );
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ", maxProcState to + "; // const-string v3, ", maxProcState to + "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1047 */
v3 = com.android.server.am.IProcessPolicy .getAppMaxProcState ( p1 );
com.android.server.am.ProcessList .makeProcStateString ( v3 );
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1045 */
android.util.Slog .d ( v2,v0 );
/* .line 1049 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1038 */
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_1
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v0 */
/* .line 1032 */
} // .end local v1 # "priorityLevel":Ljava/lang/Integer;
} // :cond_2
} // :goto_0
} // .end method
public void resetWhiteList ( android.content.Context p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "userId" # I */
/* .line 681 */
(( com.android.server.am.ProcessPolicy ) p0 ).updateDynamicWhiteList ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/am/ProcessPolicy;->updateDynamicWhiteList(Landroid/content/Context;I)Ljava/util/HashMap;
/* .line 682 */
return;
} // .end method
public void systemReady ( android.content.Context p0 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 416 */
this.mContext = p1;
/* .line 417 */
v0 = com.android.server.am.ProcessPolicy.sLock;
/* monitor-enter v0 */
/* .line 418 */
try { // :try_start_0
/* new-instance v1, Ljava/util/ArrayList; */
/* .line 419 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v3, 0x110300ad */
(( android.content.res.Resources ) v2 ).getStringArray ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 418 */
java.util.Arrays .asList ( v2 );
/* invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 420 */
/* new-instance v1, Ljava/util/ArrayList; */
/* .line 421 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v3, 0x110300bb */
(( android.content.res.Resources ) v2 ).getStringArray ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 420 */
java.util.Arrays .asList ( v2 );
/* invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 425 */
/* sget-boolean v1, Lmiui/util/DeviceLevel;->IS_MIUI_LITE_VERSION:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 426 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 427 */
/* const v2, 0x110300ae */
(( android.content.res.Resources ) v1 ).getStringArray ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 428 */
/* .local v1, "pckWhiteList":[Ljava/lang/String; */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 429 */
/* const v3, 0x110300bc */
(( android.content.res.Resources ) v2 ).getStringArray ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .local v2, "prcWhiteList":[Ljava/lang/String; */
/* .line 431 */
} // .end local v1 # "pckWhiteList":[Ljava/lang/String;
} // .end local v2 # "prcWhiteList":[Ljava/lang/String;
} // :cond_0
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 432 */
/* const v2, 0x110300ac */
(( android.content.res.Resources ) v1 ).getStringArray ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 433 */
/* .restart local v1 # "pckWhiteList":[Ljava/lang/String; */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 434 */
/* const v3, 0x110300ba */
(( android.content.res.Resources ) v2 ).getStringArray ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 436 */
/* .restart local v2 # "prcWhiteList":[Ljava/lang/String; */
} // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 437 */
v3 = com.android.server.am.ProcessPolicy.sStaticWhiteList;
/* new-instance v4, Ljava/util/ArrayList; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 438 */
} // :cond_1
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 439 */
v3 = com.android.server.am.ProcessPolicy.sProcessStaticWhiteList;
/* new-instance v4, Ljava/util/ArrayList; */
java.util.Arrays .asList ( v2 );
/* invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 441 */
} // :cond_2
/* nop */
/* .line 442 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v4, 0x110300b5 */
(( android.content.res.Resources ) v3 ).getStringArray ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 441 */
java.util.Arrays .asList ( v3 );
/* .line 443 */
/* nop */
/* .line 444 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v4, 0x110300b4 */
(( android.content.res.Resources ) v3 ).getStringArray ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 443 */
java.util.Arrays .asList ( v3 );
/* .line 445 */
/* nop */
/* .line 446 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v4, 0x110300a6 */
(( android.content.res.Resources ) v3 ).getStringArray ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 445 */
java.util.Arrays .asList ( v3 );
/* .line 447 */
/* new-instance v3, Ljava/util/ArrayList; */
/* .line 448 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v5, 0x110300b9 */
(( android.content.res.Resources ) v4 ).getStringArray ( v5 ); // invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 447 */
java.util.Arrays .asList ( v4 );
/* invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 449 */
/* nop */
/* .line 450 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v4, 0x110300b8 */
(( android.content.res.Resources ) v3 ).getStringArray ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 449 */
java.util.Arrays .asList ( v3 );
/* .line 451 */
/* nop */
/* .line 452 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v4, 0x110300b7 */
(( android.content.res.Resources ) v3 ).getStringArray ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 451 */
java.util.Arrays .asList ( v3 );
/* .line 453 */
/* nop */
/* .line 454 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v4, 0x110300b6 */
(( android.content.res.Resources ) v3 ).getStringArray ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 453 */
java.util.Arrays .asList ( v3 );
/* .line 455 */
/* new-instance v3, Ljava/util/ArrayList; */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 456 */
/* const v5, 0x110300af */
(( android.content.res.Resources ) v4 ).getStringArray ( v5 ); // invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 455 */
java.util.Arrays .asList ( v4 );
/* invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 457 */
/* new-instance v3, Ljava/util/ArrayList; */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 458 */
/* const v5, 0x110300bd */
(( android.content.res.Resources ) v4 ).getStringArray ( v5 ); // invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 457 */
java.util.Arrays .asList ( v4 );
/* invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 459 */
} // .end local v1 # "pckWhiteList":[Ljava/lang/String;
} // .end local v2 # "prcWhiteList":[Ljava/lang/String;
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 461 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessPolicy;->loadLockedAppFromSettings(Landroid/content/Context;)V */
/* .line 463 */
final String v0 = "com.jeejen.family.miui"; // const-string v0, "com.jeejen.family.miui"
/* const/16 v1, -0x64 */
int v2 = 1; // const/4 v2, 0x1
/* invoke-direct {p0, v0, v1, v2}, Lcom/android/server/am/ProcessPolicy;->updateApplicationLockedState(Ljava/lang/String;IZ)V */
/* .line 465 */
v0 = android.os.spc.PressureStateSettings.WHITE_LIST_PKG;
v0 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v0, :cond_3 */
/* .line 466 */
v0 = android.os.spc.PressureStateSettings.WHITE_LIST_PKG;
final String v1 = ","; // const-string v1, ","
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 467 */
/* .local v0, "pckWhiteList":[Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_3
/* array-length v1, v0 */
/* if-lez v1, :cond_3 */
/* .line 468 */
java.util.Arrays .asList ( v0 );
/* .line 471 */
} // .end local v0 # "pckWhiteList":[Ljava/lang/String;
} // :cond_3
/* invoke-direct {p0}, Lcom/android/server/am/ProcessPolicy;->registerContentObserver()V */
/* .line 472 */
return;
/* .line 459 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
public void updateApplicationLockedState ( android.content.Context p0, Integer p1, java.lang.String p2, Boolean p3 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "userId" # I */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .param p4, "isLocked" # Z */
/* .line 686 */
/* invoke-direct {p0, p3, p2, p4}, Lcom/android/server/am/ProcessPolicy;->updateApplicationLockedState(Ljava/lang/String;IZ)V */
/* .line 687 */
com.android.internal.os.BackgroundThread .getHandler ( );
/* new-instance v1, Lcom/android/server/am/ProcessPolicy$2; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/am/ProcessPolicy$2;-><init>(Lcom/android/server/am/ProcessPolicy;Landroid/content/Context;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 694 */
v0 = this.mProcessManagerService;
(( com.android.server.am.ProcessManagerService ) v0 ).getProcessRecord ( p3, p2 ); // invoke-virtual {v0, p3, p2}, Lcom/android/server/am/ProcessManagerService;->getProcessRecord(Ljava/lang/String;I)Lcom/android/server/am/ProcessRecord;
/* .line 695 */
/* .local v0, "targetApp":Lcom/android/server/am/ProcessRecord; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 696 */
(( com.android.server.am.ProcessPolicy ) p0 ).promoteLockedApp ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/am/ProcessPolicy;->promoteLockedApp(Lcom/android/server/am/ProcessRecord;)V
/* .line 698 */
} // :cond_0
return;
} // .end method
public void updateCloudData ( miui.process.ProcessCloudData p0 ) {
/* .locals 0 */
/* .param p1, "cloudData" # Lmiui/process/ProcessCloudData; */
/* .line 847 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessPolicy;->updateCloudWhiteList(Lmiui/process/ProcessCloudData;)V */
/* .line 850 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessPolicy;->updateAppProtectMap(Lmiui/process/ProcessCloudData;)V */
/* .line 853 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessPolicy;->updateFastBootList(Lmiui/process/ProcessCloudData;)V */
/* .line 856 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessPolicy;->updateSecretlyProtectAppList(Lmiui/process/ProcessCloudData;)V */
/* .line 857 */
return;
} // .end method
public java.util.HashMap updateDynamicWhiteList ( android.content.Context p0, Integer p1 ) {
/* .locals 17 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/content/Context;", */
/* "I)", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Boolean;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 589 */
/* move-object/from16 v1, p0 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* move-object v2, v0 */
/* .line 593 */
/* .local v2, "activeWhiteList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;" */
/* invoke-static/range {p1 ..p1}, Lcom/android/server/am/ProcessUtils;->getActiveWallpaperPackage(Landroid/content/Context;)Ljava/lang/String; */
/* move-object v3, v0 */
/* .local v3, "wallpaperPkg":Ljava/lang/String; */
int v4 = 1; // const/4 v4, 0x1
/* .line 594 */
java.lang.Boolean .valueOf ( v4 );
/* .line 593 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 594 */
(( java.util.HashMap ) v2 ).put ( v3, v5 ); // invoke-virtual {v2, v3, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 599 */
} // :cond_0
/* invoke-static/range {p1 ..p1}, Lcom/android/server/am/ProcessUtils;->getDefaultInputMethod(Landroid/content/Context;)Ljava/lang/String; */
/* move-object v6, v0 */
/* .local v6, "inputMethodPkg":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 600 */
(( java.util.HashMap ) v2 ).put ( v6, v5 ); // invoke-virtual {v2, v6, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 605 */
} // :cond_1
/* invoke-static/range {p1 ..p1}, Lcom/android/server/am/ProcessUtils;->getActiveTtsEngine(Landroid/content/Context;)Ljava/lang/String; */
/* move-object v7, v0 */
/* .local v7, "ttsEngine":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 606 */
(( java.util.HashMap ) v2 ).put ( v7, v5 ); // invoke-virtual {v2, v7, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 610 */
} // :cond_2
v0 = com.android.server.am.ProcessUtils .isPhoneWorking ( );
int v8 = 0; // const/4 v8, 0x0
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 611 */
/* const-string/jumbo v0, "telecom" */
/* .line 612 */
/* move-object/from16 v9, p1 */
(( android.content.Context ) v9 ).getSystemService ( v0 ); // invoke-virtual {v9, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/telecom/TelecomManager; */
/* .line 613 */
/* .local v0, "telecomm":Landroid/telecom/TelecomManager; */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 614 */
(( android.telecom.TelecomManager ) v0 ).getDefaultDialerPackage ( ); // invoke-virtual {v0}, Landroid/telecom/TelecomManager;->getDefaultDialerPackage()Ljava/lang/String;
final String v11 = "com.android.contacts"; // const-string v11, "com.android.contacts"
v10 = (( java.lang.String ) v10 ).equals ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v10, :cond_3 */
/* .line 615 */
final String v10 = "com.qualcomm.qtil.btdsda"; // const-string v10, "com.qualcomm.qtil.btdsda"
java.lang.Boolean .valueOf ( v8 );
(( java.util.HashMap ) v2 ).put ( v10, v11 ); // invoke-virtual {v2, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 616 */
(( android.telecom.TelecomManager ) v0 ).getDefaultDialerPackage ( ); // invoke-virtual {v0}, Landroid/telecom/TelecomManager;->getDefaultDialerPackage()Ljava/lang/String;
(( java.util.HashMap ) v2 ).put ( v10, v5 ); // invoke-virtual {v2, v10, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 618 */
} // :cond_3
final String v10 = "com.qualcomm.qtil.btdsda"; // const-string v10, "com.qualcomm.qtil.btdsda"
java.lang.Boolean .valueOf ( v8 );
(( java.util.HashMap ) v2 ).put ( v10, v11 ); // invoke-virtual {v2, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 619 */
final String v10 = "com.android.incallui"; // const-string v10, "com.android.incallui"
(( java.util.HashMap ) v2 ).put ( v10, v5 ); // invoke-virtual {v2, v10, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 610 */
} // .end local v0 # "telecomm":Landroid/telecom/TelecomManager;
} // :cond_4
/* move-object/from16 v9, p1 */
/* .line 624 */
} // :goto_0
final String v0 = "com.miui.voip"; // const-string v0, "com.miui.voip"
(( java.util.HashMap ) v2 ).put ( v0, v5 ); // invoke-virtual {v2, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 627 */
v10 = this.mActivityManagerService;
/* monitor-enter v10 */
/* .line 628 */
try { // :try_start_0
v0 = com.android.server.am.ProcessPolicy.sFgServiceCheckList;
v11 = } // :goto_1
if ( v11 != null) { // if-eqz v11, :cond_7
/* check-cast v11, Ljava/lang/String; */
/* .line 629 */
/* .local v11, "packageName":Ljava/lang/String; */
v12 = this.mProcessManagerService;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 630 */
/* move/from16 v13, p2 */
try { // :try_start_1
(( com.android.server.am.ProcessManagerService ) v12 ).getProcessRecordList ( v11, v13 ); // invoke-virtual {v12, v11, v13}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordList(Ljava/lang/String;I)Ljava/util/List;
/* .line 631 */
/* .local v12, "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;" */
v15 = } // :goto_2
if ( v15 != null) { // if-eqz v15, :cond_6
/* check-cast v15, Lcom/android/server/am/ProcessRecord; */
/* .line 632 */
/* .local v15, "app":Lcom/android/server/am/ProcessRecord; */
if ( v15 != null) { // if-eqz v15, :cond_5
v4 = this.mServices;
v4 = (( com.android.server.am.ProcessServiceRecord ) v4 ).hasForegroundServices ( ); // invoke-virtual {v4}, Lcom/android/server/am/ProcessServiceRecord;->hasForegroundServices()Z
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 633 */
(( java.util.HashMap ) v2 ).put ( v11, v5 ); // invoke-virtual {v2, v11, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 634 */
v4 = v4 = com.android.server.am.ProcessPolicy.sBoundFgServiceProtectMap;
if ( v4 != null) { // if-eqz v4, :cond_6
/* .line 635 */
v4 = com.android.server.am.ProcessPolicy.sBoundFgServiceProtectMap;
/* check-cast v4, Ljava/lang/String; */
java.lang.Boolean .valueOf ( v8 );
(( java.util.HashMap ) v2 ).put ( v4, v14 ); // invoke-virtual {v2, v4, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 639 */
} // .end local v15 # "app":Lcom/android/server/am/ProcessRecord;
} // :cond_5
int v4 = 1; // const/4 v4, 0x1
/* .line 640 */
} // .end local v11 # "packageName":Ljava/lang/String;
} // .end local v12 # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
} // :cond_6
} // :goto_3
int v4 = 1; // const/4 v4, 0x1
/* .line 641 */
} // :cond_7
/* move/from16 v13, p2 */
/* monitor-exit v10 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_2 */
/* .line 644 */
v0 = this.mAccessibilityManager;
/* .line 645 */
int v4 = -1; // const/4 v4, -0x1
(( android.view.accessibility.AccessibilityManager ) v0 ).getEnabledAccessibilityServiceList ( v4 ); // invoke-virtual {v0, v4}, Landroid/view/accessibility/AccessibilityManager;->getEnabledAccessibilityServiceList(I)Ljava/util/List;
/* .line 646 */
/* .local v4, "accessibilityList":Ljava/util/List;, "Ljava/util/List<Landroid/accessibilityservice/AccessibilityServiceInfo;>;" */
v0 = if ( v4 != null) { // if-eqz v4, :cond_f
/* if-nez v0, :cond_f */
/* .line 647 */
v10 = } // :goto_4
if ( v10 != null) { // if-eqz v10, :cond_f
/* check-cast v10, Landroid/accessibilityservice/AccessibilityServiceInfo; */
/* .line 648 */
/* .local v10, "info":Landroid/accessibilityservice/AccessibilityServiceInfo; */
if ( v10 != null) { // if-eqz v10, :cond_d
(( android.accessibilityservice.AccessibilityServiceInfo ) v10 ).getId ( ); // invoke-virtual {v10}, Landroid/accessibilityservice/AccessibilityServiceInfo;->getId()Ljava/lang/String;
v11 = android.text.TextUtils .isEmpty ( v11 );
/* if-nez v11, :cond_d */
/* .line 649 */
(( android.accessibilityservice.AccessibilityServiceInfo ) v10 ).getId ( ); // invoke-virtual {v10}, Landroid/accessibilityservice/AccessibilityServiceInfo;->getId()Ljava/lang/String;
android.content.ComponentName .unflattenFromString ( v11 );
/* .line 650 */
/* .local v11, "componentName":Landroid/content/ComponentName; */
if ( v11 != null) { // if-eqz v11, :cond_8
(( android.content.ComponentName ) v11 ).getPackageName ( ); // invoke-virtual {v11}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
} // :cond_8
int v12 = 0; // const/4 v12, 0x0
/* .line 651 */
/* .local v12, "pkg":Ljava/lang/String; */
} // :goto_5
if ( v12 != null) { // if-eqz v12, :cond_c
v14 = (( java.util.HashMap ) v2 ).containsKey ( v12 ); // invoke-virtual {v2, v12}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
/* if-nez v14, :cond_c */
/* .line 652 */
(( android.accessibilityservice.AccessibilityServiceInfo ) v10 ).getResolveInfo ( ); // invoke-virtual {v10}, Landroid/accessibilityservice/AccessibilityServiceInfo;->getResolveInfo()Landroid/content/pm/ResolveInfo;
v14 = this.serviceInfo;
v14 = this.applicationInfo;
/* .line 653 */
/* .local v14, "app":Landroid/content/pm/ApplicationInfo; */
v15 = (( android.content.pm.ApplicationInfo ) v14 ).isSystemApp ( ); // invoke-virtual {v14}, Landroid/content/pm/ApplicationInfo;->isSystemApp()Z
/* if-nez v15, :cond_a */
v15 = (( android.content.pm.ApplicationInfo ) v14 ).isUpdatedSystemApp ( ); // invoke-virtual {v14}, Landroid/content/pm/ApplicationInfo;->isUpdatedSystemApp()Z
if ( v15 != null) { // if-eqz v15, :cond_9
} // :cond_9
/* move v15, v8 */
} // :cond_a
} // :goto_6
int v15 = 1; // const/4 v15, 0x1
/* .line 657 */
/* .local v15, "isSystemApp":Z */
} // :goto_7
if ( v15 != null) { // if-eqz v15, :cond_b
/* .line 658 */
/* move-object/from16 v16, v0 */
java.lang.Boolean .valueOf ( v8 );
(( java.util.HashMap ) v2 ).put ( v12, v0 ); // invoke-virtual {v2, v12, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 659 */
} // :cond_b
/* move-object/from16 v16, v0 */
/* iget v0, v10, Landroid/accessibilityservice/AccessibilityServiceInfo;->feedbackType:I */
/* and-int/lit8 v0, v0, 0x7 */
if ( v0 != null) { // if-eqz v0, :cond_e
/* .line 662 */
(( android.content.ComponentName ) v11 ).getPackageName ( ); // invoke-virtual {v11}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
(( java.util.HashMap ) v2 ).put ( v0, v5 ); // invoke-virtual {v2, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 651 */
} // .end local v14 # "app":Landroid/content/pm/ApplicationInfo;
} // .end local v15 # "isSystemApp":Z
} // :cond_c
/* move-object/from16 v16, v0 */
/* .line 648 */
} // .end local v11 # "componentName":Landroid/content/ComponentName;
} // .end local v12 # "pkg":Ljava/lang/String;
} // :cond_d
/* move-object/from16 v16, v0 */
/* .line 666 */
} // .end local v10 # "info":Landroid/accessibilityservice/AccessibilityServiceInfo;
} // :cond_e
} // :goto_8
/* move-object/from16 v0, v16 */
/* .line 670 */
} // :cond_f
final String v0 = "ProcessManager"; // const-string v0, "ProcessManager"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v8, "update DY:" */
(( java.lang.StringBuilder ) v5 ).append ( v8 ); // invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.util.HashMap ) v2 ).keySet ( ); // invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;
java.util.Arrays .toString ( v8 );
(( java.lang.StringBuilder ) v5 ).append ( v8 ); // invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v5 );
/* .line 673 */
v5 = com.android.server.am.ProcessPolicy.sLock;
/* monitor-enter v5 */
/* .line 674 */
try { // :try_start_2
/* .line 675 */
/* monitor-exit v5 */
/* .line 676 */
/* .line 675 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit v5 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v0 */
/* .line 641 */
} // .end local v4 # "accessibilityList":Ljava/util/List;, "Ljava/util/List<Landroid/accessibilityservice/AccessibilityServiceInfo;>;"
/* :catchall_1 */
/* move-exception v0 */
/* move/from16 v13, p2 */
} // :goto_9
try { // :try_start_3
/* monitor-exit v10 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_2 */
/* throw v0 */
/* :catchall_2 */
/* move-exception v0 */
} // .end method
public void updateSystemCleanWhiteList ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .line 1023 */
v0 = com.android.server.am.ProcessPolicy.sLock;
/* monitor-enter v0 */
/* .line 1024 */
try { // :try_start_0
v1 = v1 = com.android.server.am.ProcessPolicy.sSystemCleanWhiteList;
/* if-nez v1, :cond_0 */
/* .line 1025 */
v1 = com.android.server.am.ProcessPolicy.sSystemCleanWhiteList;
/* .line 1027 */
} // :cond_0
/* monitor-exit v0 */
/* .line 1028 */
return;
/* .line 1027 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
