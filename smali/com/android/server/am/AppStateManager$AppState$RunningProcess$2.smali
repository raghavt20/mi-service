.class Lcom/android/server/am/AppStateManager$AppState$RunningProcess$2;
.super Ljava/lang/Object;
.source "AppStateManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->reset()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;


# direct methods
.method constructor <init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V
    .locals 0
    .param p1, "this$2"    # Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 2135
    iput-object p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$2;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 2138
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$2;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmState(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 2139
    return-void

    .line 2141
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$2;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmAppPowerResourceManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/AppPowerResourceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$2;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmResourcesCallback(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$2;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmUid(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v2

    const/4 v3, 0x2

    invoke-virtual {v0, v3, v1, v2}, Lcom/miui/server/smartpower/AppPowerResourceManager;->unRegisterCallback(ILcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;I)V

    .line 2143
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$2;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmAppPowerResourceManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/AppPowerResourceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$2;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmResourcesCallback(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$2;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmUid(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v2

    iget-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$2;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmPid(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v3

    const/4 v4, 0x1

    invoke-virtual {v0, v4, v1, v2, v3}, Lcom/miui/server/smartpower/AppPowerResourceManager;->unRegisterCallback(ILcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;II)V

    .line 2146
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$2;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmAppPowerResourceManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/AppPowerResourceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$2;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmResourcesCallback(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$2;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmUid(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v2

    iget-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$2;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmPid(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v3

    const/4 v4, 0x5

    invoke-virtual {v0, v4, v1, v2, v3}, Lcom/miui/server/smartpower/AppPowerResourceManager;->unRegisterCallback(ILcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;II)V

    .line 2149
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$2;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmAppPowerResourceManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/AppPowerResourceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$2;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmResourcesCallback(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$2;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmUid(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v2

    iget-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$2;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmPid(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v3

    const/4 v4, 0x3

    invoke-virtual {v0, v4, v1, v2, v3}, Lcom/miui/server/smartpower/AppPowerResourceManager;->unRegisterCallback(ILcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;II)V

    .line 2152
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$2;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmAppPowerResourceManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/AppPowerResourceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$2;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmResourcesCallback(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$2;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmUid(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v2

    iget-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$2;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmPid(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v3

    const/4 v4, 0x6

    invoke-virtual {v0, v4, v1, v2, v3}, Lcom/miui/server/smartpower/AppPowerResourceManager;->unRegisterCallback(ILcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;II)V

    .line 2155
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$2;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmAppPowerResourceManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/AppPowerResourceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$2;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmResourcesCallback(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$2;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmUid(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v2

    const/4 v3, 0x7

    invoke-virtual {v0, v3, v1, v2}, Lcom/miui/server/smartpower/AppPowerResourceManager;->unRegisterCallback(ILcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;I)V

    .line 2158
    return-void
.end method
