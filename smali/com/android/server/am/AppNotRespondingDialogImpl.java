public class com.android.server.am.AppNotRespondingDialogImpl extends com.android.server.am.AppNotRespondingDialogStub {
	 /* .source "AppNotRespondingDialogImpl.java" */
	 /* # direct methods */
	 public com.android.server.am.AppNotRespondingDialogImpl ( ) {
		 /* .locals 0 */
		 /* .line 9 */
		 /* invoke-direct {p0}, Lcom/android/server/am/AppNotRespondingDialogStub;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 Boolean onCreate ( com.android.server.am.AppNotRespondingDialog p0 ) {
		 /* .locals 2 */
		 /* .param p1, "dialog" # Lcom/android/server/am/AppNotRespondingDialog; */
		 /* .line 28 */
		 int v0 = -1; // const/4 v0, -0x1
		 (( com.android.server.am.AppNotRespondingDialog ) p1 ).getButton ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/am/AppNotRespondingDialog;->getButton(I)Landroid/widget/Button;
		 /* const v1, 0x10201d8 */
		 (( android.widget.Button ) v0 ).setId ( v1 ); // invoke-virtual {v0, v1}, Landroid/widget/Button;->setId(I)V
		 /* .line 29 */
		 int v0 = -2; // const/4 v0, -0x2
		 (( com.android.server.am.AppNotRespondingDialog ) p1 ).getButton ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/am/AppNotRespondingDialog;->getButton(I)Landroid/widget/Button;
		 /* const v1, 0x10201dc */
		 (( android.widget.Button ) v0 ).setId ( v1 ); // invoke-virtual {v0, v1}, Landroid/widget/Button;->setId(I)V
		 /* .line 30 */
		 int v0 = -3; // const/4 v0, -0x3
		 (( com.android.server.am.AppNotRespondingDialog ) p1 ).getButton ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/am/AppNotRespondingDialog;->getButton(I)Landroid/widget/Button;
		 /* const v1, 0x10201da */
		 (( android.widget.Button ) v0 ).setId ( v1 ); // invoke-virtual {v0, v1}, Landroid/widget/Button;->setId(I)V
		 /* .line 31 */
		 int v0 = 1; // const/4 v0, 0x1
	 } // .end method
	 void onInit ( com.android.server.am.AppNotRespondingDialog p0, Boolean p1, android.os.Message p2, android.os.Message p3, android.os.Message p4 ) {
		 /* .locals 3 */
		 /* .param p1, "dialog" # Lcom/android/server/am/AppNotRespondingDialog; */
		 /* .param p2, "hasErrorReceiver" # Z */
		 /* .param p3, "forceCloseMsg" # Landroid/os/Message; */
		 /* .param p4, "waitMsg" # Landroid/os/Message; */
		 /* .param p5, "waitAndReportMsg" # Landroid/os/Message; */
		 /* .line 12 */
		 (( com.android.server.am.AppNotRespondingDialog ) p1 ).getContext ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppNotRespondingDialog;->getContext()Landroid/content/Context;
		 (( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
		 /* .line 13 */
		 /* .local v0, "res":Landroid/content/res/Resources; */
		 /* nop */
		 /* .line 14 */
		 /* const v1, 0x110f001b */
		 (( android.content.res.Resources ) v0 ).getText ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;
		 /* .line 13 */
		 int v2 = -1; // const/4 v2, -0x1
		 (( com.android.server.am.AppNotRespondingDialog ) p1 ).setButton ( v2, v1, p3 ); // invoke-virtual {p1, v2, v1, p3}, Lcom/android/server/am/AppNotRespondingDialog;->setButton(ILjava/lang/CharSequence;Landroid/os/Message;)V
		 /* .line 16 */
		 /* nop */
		 /* .line 17 */
		 /* const v1, 0x110f003a */
		 (( android.content.res.Resources ) v0 ).getText ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;
		 /* .line 16 */
		 int v2 = -2; // const/4 v2, -0x2
		 (( com.android.server.am.AppNotRespondingDialog ) p1 ).setButton ( v2, v1, p4 ); // invoke-virtual {p1, v2, v1, p4}, Lcom/android/server/am/AppNotRespondingDialog;->setButton(ILjava/lang/CharSequence;Landroid/os/Message;)V
		 /* .line 20 */
		 if ( p2 != null) { // if-eqz p2, :cond_0
			 /* .line 21 */
			 /* nop */
			 /* .line 22 */
			 /* const v1, 0x110f0028 */
			 (( android.content.res.Resources ) v0 ).getText ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;
			 /* .line 21 */
			 int v2 = -3; // const/4 v2, -0x3
			 (( com.android.server.am.AppNotRespondingDialog ) p1 ).setButton ( v2, v1, p5 ); // invoke-virtual {p1, v2, v1, p5}, Lcom/android/server/am/AppNotRespondingDialog;->setButton(ILjava/lang/CharSequence;Landroid/os/Message;)V
			 /* .line 25 */
		 } // :cond_0
		 return;
	 } // .end method
