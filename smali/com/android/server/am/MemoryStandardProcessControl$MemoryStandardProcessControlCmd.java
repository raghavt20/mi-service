class com.android.server.am.MemoryStandardProcessControl$MemoryStandardProcessControlCmd extends android.os.ShellCommand {
	 /* .source "MemoryStandardProcessControl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/MemoryStandardProcessControl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "MemoryStandardProcessControlCmd" */
} // .end annotation
/* # instance fields */
final com.android.server.am.MemoryStandardProcessControl this$0; //synthetic
/* # direct methods */
private com.android.server.am.MemoryStandardProcessControl$MemoryStandardProcessControlCmd ( ) {
/* .locals 0 */
/* .line 1137 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/os/ShellCommand;-><init>()V */
return;
} // .end method
 com.android.server.am.MemoryStandardProcessControl$MemoryStandardProcessControlCmd ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;-><init>(Lcom/android/server/am/MemoryStandardProcessControl;)V */
return;
} // .end method
/* # virtual methods */
public Integer onCommand ( java.lang.String p0 ) {
/* .locals 13 */
/* .param p1, "cmd" # Ljava/lang/String; */
/* .line 1141 */
/* if-nez p1, :cond_0 */
/* .line 1142 */
v0 = (( com.android.server.am.MemoryStandardProcessControl$MemoryStandardProcessControlCmd ) p0 ).handleDefaultCommands ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->handleDefaultCommands(Ljava/lang/String;)I
/* .line 1144 */
} // :cond_0
(( com.android.server.am.MemoryStandardProcessControl$MemoryStandardProcessControlCmd ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->getOutPrintWriter()Ljava/io/PrintWriter;
/* .line 1146 */
/* .local v0, "pw":Ljava/io/PrintWriter; */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
v2 = (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
int v3 = 2; // const/4 v3, 0x2
int v4 = 3; // const/4 v4, 0x3
int v5 = 1; // const/4 v5, 0x1
/* sparse-switch v2, :sswitch_data_0 */
} // :cond_1
/* :sswitch_0 */
/* const-string/jumbo v2, "standard" */
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* move v2, v3 */
/* :sswitch_1 */
final String v2 = "debug"; // const-string v2, "debug"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
	 int v2 = 4; // const/4 v2, 0x4
	 /* :sswitch_2 */
	 final String v2 = "dump"; // const-string v2, "dump"
	 v2 = 	 (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v2 != null) { // if-eqz v2, :cond_1
		 /* move v2, v1 */
		 /* :sswitch_3 */
		 final String v2 = "pressure"; // const-string v2, "pressure"
		 v2 = 		 (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v2 != null) { // if-eqz v2, :cond_1
			 /* move v2, v5 */
			 /* :sswitch_4 */
			 final String v2 = "enable"; // const-string v2, "enable"
			 v2 = 			 (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 /* :try_end_0 */
			 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
			 if ( v2 != null) { // if-eqz v2, :cond_1
				 /* move v2, v4 */
			 } // :goto_0
			 int v2 = -1; // const/4 v2, -0x1
		 } // :goto_1
		 final String v6 = "false"; // const-string v6, "false"
		 /* const-string/jumbo v7, "true" */
		 /* packed-switch v2, :pswitch_data_0 */
		 /* .line 1217 */
		 try { // :try_start_1
			 v1 = 			 (( com.android.server.am.MemoryStandardProcessControl$MemoryStandardProcessControlCmd ) p0 ).handleDefaultCommands ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->handleDefaultCommands(Ljava/lang/String;)I
			 /* goto/16 :goto_4 */
			 /* .line 1204 */
			 /* :pswitch_0 */
			 (( com.android.server.am.MemoryStandardProcessControl$MemoryStandardProcessControlCmd ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->getNextArgRequired()Ljava/lang/String;
			 /* .line 1205 */
			 /* .local v2, "str":Ljava/lang/String; */
			 v3 = 			 (( java.lang.String ) v7 ).equals ( v2 ); // invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 if ( v3 != null) { // if-eqz v3, :cond_2
				 /* .line 1206 */
				 com.android.server.am.MemoryStandardProcessControl.DEBUG = (v5!= 0);
				 /* .line 1207 */
				 v3 = this.this$0;
				 com.android.server.am.MemoryStandardProcessControl .-$$Nest$mdebugModifyKillStrategy ( v3 );
				 /* .line 1208 */
				 final String v3 = "debug on, enable mspc and screen off kill"; // const-string v3, "debug on, enable mspc and screen off kill"
				 (( java.io.PrintWriter ) v0 ).println ( v3 ); // invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
				 /* goto/16 :goto_3 */
				 /* .line 1209 */
			 } // :cond_2
			 v3 = 			 (( java.lang.String ) v6 ).equals ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 if ( v3 != null) { // if-eqz v3, :cond_8
				 /* .line 1210 */
				 com.android.server.am.MemoryStandardProcessControl.DEBUG = (v1!= 0);
				 /* .line 1211 */
				 v3 = this.this$0;
				 /* iput v1, v3, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenOffStrategy:I */
				 /* .line 1212 */
				 final String v3 = "debug off, disable mspc and screen off kill"; // const-string v3, "debug off, disable mspc and screen off kill"
				 (( java.io.PrintWriter ) v0 ).println ( v3 ); // invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
				 /* goto/16 :goto_3 */
				 /* .line 1193 */
			 } // .end local v2 # "str":Ljava/lang/String;
			 /* :pswitch_1 */
			 (( com.android.server.am.MemoryStandardProcessControl$MemoryStandardProcessControlCmd ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->getNextArgRequired()Ljava/lang/String;
			 /* .line 1194 */
			 /* .restart local v2 # "str":Ljava/lang/String; */
			 v3 = 			 (( java.lang.String ) v7 ).equals ( v2 ); // invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 if ( v3 != null) { // if-eqz v3, :cond_3
				 /* .line 1195 */
				 v3 = this.this$0;
				 /* iput-boolean v5, v3, Lcom/android/server/am/MemoryStandardProcessControl;->mEnable:Z */
				 /* .line 1196 */
				 final String v3 = "mspc enabled"; // const-string v3, "mspc enabled"
				 (( java.io.PrintWriter ) v0 ).println ( v3 ); // invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
				 /* goto/16 :goto_3 */
				 /* .line 1197 */
			 } // :cond_3
			 v3 = 			 (( java.lang.String ) v6 ).equals ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 if ( v3 != null) { // if-eqz v3, :cond_8
				 /* .line 1198 */
				 v3 = this.this$0;
				 /* iput-boolean v1, v3, Lcom/android/server/am/MemoryStandardProcessControl;->mEnable:Z */
				 /* .line 1199 */
				 final String v3 = "mspc disabled"; // const-string v3, "mspc disabled"
				 (( java.io.PrintWriter ) v0 ).println ( v3 ); // invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
				 /* goto/16 :goto_3 */
				 /* .line 1164 */
			 } // .end local v2 # "str":Ljava/lang/String;
			 /* :pswitch_2 */
			 (( com.android.server.am.MemoryStandardProcessControl$MemoryStandardProcessControlCmd ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->getNextArgRequired()Ljava/lang/String;
			 /* .line 1165 */
			 /* .local v2, "mode":Ljava/lang/String; */
			 final String v6 = "list"; // const-string v6, "list"
			 v6 = 			 (( java.lang.String ) v2 ).equals ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 if ( v6 != null) { // if-eqz v6, :cond_4
				 /* .line 1167 */
				 v3 = this.this$0;
				 v3 = this.mHandler;
				 v4 = this.this$0;
				 v4 = this.mHandler;
				 int v5 = 6; // const/4 v5, 0x6
				 (( com.android.server.am.MemoryStandardProcessControl$MyHandler ) v4 ).obtainMessage ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->obtainMessage(I)Landroid/os/Message;
				 (( com.android.server.am.MemoryStandardProcessControl$MyHandler ) v3 ).sendMessage ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->sendMessage(Landroid/os/Message;)Z
				 /* .line 1168 */
				 final String v3 = "please check logcat"; // const-string v3, "please check logcat"
				 (( java.io.PrintWriter ) v0 ).println ( v3 ); // invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
				 /* goto/16 :goto_3 */
				 /* .line 1169 */
			 } // :cond_4
			 final String v6 = "add"; // const-string v6, "add"
			 v6 = 			 (( java.lang.String ) v2 ).equals ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 /* :try_end_1 */
			 /* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
			 final String v7 = ","; // const-string v7, ","
			 if ( v6 != null) { // if-eqz v6, :cond_5
				 /* .line 1171 */
				 try { // :try_start_2
					 (( com.android.server.am.MemoryStandardProcessControl$MemoryStandardProcessControlCmd ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->getNextArgRequired()Ljava/lang/String;
					 (( java.lang.String ) v4 ).split ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
					 /* .line 1172 */
					 /* .local v4, "strs":[Ljava/lang/String; */
					 /* aget-object v9, v4, v1 */
					 /* .line 1173 */
					 /* .local v9, "packageName":Ljava/lang/String; */
					 /* aget-object v5, v4, v5 */
					 java.lang.Long .parseLong ( v5 );
					 /* move-result-wide v7 */
					 /* .line 1174 */
					 /* .local v7, "pss":J */
					 /* new-instance v5, Ljava/util/ArrayList; */
					 /* invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V */
					 /* .line 1175 */
					 /* .local v5, "processes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
					 /* aget-object v3, v4, v3 */
					 /* .line 1176 */
					 v3 = this.this$0;
					 v6 = this.mMemoryStandardMap;
					 int v11 = 0; // const/4 v11, 0x0
					 /* move-object v10, v5 */
					 /* invoke-virtual/range {v6 ..v11}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->updateMemoryStandard(JLjava/lang/String;Ljava/util/List;Ljava/lang/String;)V */
				 } // .end local v4 # "strs":[Ljava/lang/String;
			 } // .end local v5 # "processes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
		 } // .end local v7 # "pss":J
	 } // .end local v9 # "packageName":Ljava/lang/String;
	 /* .line 1177 */
} // :cond_5
final String v6 = "add-native"; // const-string v6, "add-native"
v6 = (( java.lang.String ) v2 ).equals ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_7
	 /* .line 1179 */
	 (( com.android.server.am.MemoryStandardProcessControl$MemoryStandardProcessControlCmd ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->getNextArgRequired()Ljava/lang/String;
	 (( java.lang.String ) v6 ).split ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
	 /* .line 1180 */
	 /* .local v6, "strs":[Ljava/lang/String; */
	 /* aget-object v10, v6, v1 */
	 /* .line 1181 */
	 /* .local v10, "packageName":Ljava/lang/String; */
	 /* aget-object v5, v6, v5 */
	 java.lang.Long .parseLong ( v5 );
	 /* move-result-wide v8 */
	 /* .line 1182 */
	 /* .local v8, "pss":J */
	 /* new-instance v5, Ljava/util/ArrayList; */
	 /* invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V */
	 /* .line 1183 */
	 /* .restart local v5 # "processes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
	 /* aget-object v3, v6, v3 */
	 /* .line 1184 */
	 int v3 = 0; // const/4 v3, 0x0
	 /* .line 1185 */
	 /* .local v3, "parent":Ljava/lang/String; */
	 /* array-length v7, v6 */
	 /* if-le v7, v4, :cond_6 */
	 /* .line 1186 */
	 /* aget-object v4, v6, v4 */
	 /* move-object v3, v4 */
	 /* .line 1188 */
} // :cond_6
v4 = this.this$0;
v7 = this.mMemoryStandardMap;
/* move-object v11, v5 */
/* move-object v12, v3 */
/* invoke-virtual/range {v7 ..v12}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->updateNativeMemoryStandard(JLjava/lang/String;Ljava/util/List;Ljava/lang/String;)V */
/* .line 1189 */
} // .end local v3 # "parent":Ljava/lang/String;
} // .end local v5 # "processes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v6 # "strs":[Ljava/lang/String;
} // .end local v8 # "pss":J
} // .end local v10 # "packageName":Ljava/lang/String;
/* .line 1177 */
} // :cond_7
} // :goto_2
/* .line 1153 */
} // .end local v2 # "mode":Ljava/lang/String;
/* :pswitch_3 */
(( com.android.server.am.MemoryStandardProcessControl$MemoryStandardProcessControlCmd ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->getNextArgRequired()Ljava/lang/String;
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_1 */
/* .line 1154 */
/* .local v2, "str":Ljava/lang/String; */
int v3 = 0; // const/4 v3, 0x0
/* .line 1156 */
/* .local v3, "presssureState":I */
try { // :try_start_3
v4 = java.lang.Integer .parseInt ( v2 );
/* move v3, v4 */
/* .line 1157 */
v4 = this.this$0;
(( com.android.server.am.MemoryStandardProcessControl ) v4 ).reportMemPressure ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/am/MemoryStandardProcessControl;->reportMemPressure(I)V
/* :try_end_3 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_1 */
/* .line 1160 */
/* .line 1158 */
/* :catch_0 */
/* move-exception v4 */
/* .line 1159 */
/* .local v4, "e":Ljava/lang/NumberFormatException; */
try { // :try_start_4
final String v5 = "pressure is invalid"; // const-string v5, "pressure is invalid"
(( java.io.PrintWriter ) v0 ).println ( v5 ); // invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1161 */
} // .end local v4 # "e":Ljava/lang/NumberFormatException;
/* .line 1148 */
} // .end local v2 # "str":Ljava/lang/String;
} // .end local v3 # "presssureState":I
/* :pswitch_4 */
v2 = this.this$0;
v2 = this.mHandler;
v3 = this.this$0;
v3 = this.mHandler;
int v4 = 5; // const/4 v4, 0x5
(( com.android.server.am.MemoryStandardProcessControl$MyHandler ) v3 ).obtainMessage ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->obtainMessage(I)Landroid/os/Message;
(( com.android.server.am.MemoryStandardProcessControl$MyHandler ) v2 ).sendMessage ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->sendMessage(Landroid/os/Message;)Z
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_1 */
/* .line 1149 */
/* nop */
/* .line 1222 */
} // :cond_8
} // :goto_3
/* .line 1217 */
} // :goto_4
/* .line 1219 */
/* :catch_1 */
/* move-exception v2 */
/* .line 1220 */
/* .local v2, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Error occurred.Check logcat for details."; // const-string v4, "Error occurred.Check logcat for details."
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v2 ).getMessage ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v0 ).println ( v3 ); // invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1221 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Error running shell command! "; // const-string v4, "Error running shell command! "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "ShellCommand"; // const-string v4, "ShellCommand"
android.util.Slog .e ( v4,v3 );
/* .line 1223 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_5
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x4d6ada7d -> :sswitch_4 */
/* -0x4c11e9bb -> :sswitch_3 */
/* 0x2f39f4 -> :sswitch_2 */
/* 0x5b09653 -> :sswitch_1 */
/* 0x4e3d1ebd -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void onHelp ( ) {
/* .locals 2 */
/* .line 1228 */
(( com.android.server.am.MemoryStandardProcessControl$MemoryStandardProcessControlCmd ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->getOutPrintWriter()Ljava/io/PrintWriter;
/* .line 1229 */
/* .local v0, "pw":Ljava/io/PrintWriter; */
final String v1 = "Memory Standard Process Control commands:"; // const-string v1, "Memory Standard Process Control commands:"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1230 */
return;
} // .end method
