.class public final Lcom/android/server/am/SmartPowerService$Lifecycle;
.super Lcom/android/server/SystemService;
.source "SmartPowerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/SmartPowerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Lifecycle"
.end annotation


# instance fields
.field private final mService:Lcom/android/server/am/SmartPowerService;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 200
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 201
    invoke-static {}, Lcom/android/server/am/SmartPowerServiceStub;->getInstance()Lcom/android/server/am/SmartPowerServiceStub;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/SmartPowerService;

    iput-object v0, p0, Lcom/android/server/am/SmartPowerService$Lifecycle;->mService:Lcom/android/server/am/SmartPowerService;

    .line 202
    invoke-virtual {v0, p1}, Lcom/android/server/am/SmartPowerService;->systemReady(Landroid/content/Context;)V

    .line 203
    return-void
.end method


# virtual methods
.method public onBootPhase(I)V
    .locals 1
    .param p1, "phase"    # I

    .line 212
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/android/server/am/SmartPowerService$Lifecycle;->mService:Lcom/android/server/am/SmartPowerService;

    invoke-static {v0}, Lcom/android/server/am/SmartPowerService;->-$$Nest$mupdateCloudControlParas(Lcom/android/server/am/SmartPowerService;)V

    .line 215
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 2

    .line 207
    const-string/jumbo v0, "smartpower"

    iget-object v1, p0, Lcom/android/server/am/SmartPowerService$Lifecycle;->mService:Lcom/android/server/am/SmartPowerService;

    invoke-virtual {p0, v0, v1}, Lcom/android/server/am/SmartPowerService$Lifecycle;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 208
    return-void
.end method
