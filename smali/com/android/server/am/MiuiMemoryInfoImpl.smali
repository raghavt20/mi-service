.class public Lcom/android/server/am/MiuiMemoryInfoImpl;
.super Ljava/lang/Object;
.source "MiuiMemoryInfoImpl.java"

# interfaces
.implements Lcom/android/server/am/MiuiMemoryInfoStub;


# static fields
.field private static final TAG:Ljava/lang/String; = "MiuiMemoryInfoImpl"

.field static sAms:Lcom/android/server/am/ActivityManagerService;

.field private static sTotalMem:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 16
    invoke-static {}, Landroid/os/Process;->getTotalMemory()J

    move-result-wide v0

    sput-wide v0, Lcom/android/server/am/MiuiMemoryInfoImpl;->sTotalMem:J

    .line 36
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/am/MiuiMemoryInfoImpl;->sAms:Lcom/android/server/am/ActivityManagerService;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getCachePss()J
    .locals 8

    .line 38
    sget-object v0, Lcom/android/server/am/MiuiMemoryInfoImpl;->sAms:Lcom/android/server/am/ActivityManagerService;

    if-nez v0, :cond_0

    .line 39
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    .line 40
    .local v0, "ams":Landroid/app/IActivityManager;
    instance-of v1, v0, Lcom/android/server/am/ActivityManagerService;

    if-eqz v1, :cond_0

    .line 41
    move-object v1, v0

    check-cast v1, Lcom/android/server/am/ActivityManagerService;

    sput-object v1, Lcom/android/server/am/MiuiMemoryInfoImpl;->sAms:Lcom/android/server/am/ActivityManagerService;

    .line 44
    .end local v0    # "ams":Landroid/app/IActivityManager;
    :cond_0
    const-wide/16 v0, 0x0

    .line 45
    .local v0, "cachePss":J
    sget-object v2, Lcom/android/server/am/MiuiMemoryInfoImpl;->sAms:Lcom/android/server/am/ActivityManagerService;

    if-eqz v2, :cond_2

    .line 46
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v4, v3}, Lcom/android/server/am/ActivityManagerService;->collectProcesses(Ljava/io/PrintWriter;IZ[Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 47
    .local v2, "procs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;"
    if-eqz v2, :cond_2

    .line 48
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/am/ProcessRecord;

    .line 49
    .local v4, "proc":Lcom/android/server/am/ProcessRecord;
    sget-object v5, Lcom/android/server/am/MiuiMemoryInfoImpl;->sAms:Lcom/android/server/am/ActivityManagerService;

    iget-object v5, v5, Lcom/android/server/am/ActivityManagerService;->mProcLock:Lcom/android/server/am/ActivityManagerGlobalLock;

    monitor-enter v5

    .line 50
    :try_start_0
    iget-object v6, v4, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v6}, Lcom/android/server/am/ProcessStateRecord;->getSetProcState()I

    move-result v6

    const/16 v7, 0xf

    if-lt v6, v7, :cond_1

    .line 51
    iget-object v6, v4, Lcom/android/server/am/ProcessRecord;->mProfile:Lcom/android/server/am/ProcessProfileRecord;

    invoke-virtual {v6}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J

    move-result-wide v6

    add-long/2addr v0, v6

    .line 53
    :cond_1
    monitor-exit v5

    .line 54
    .end local v4    # "proc":Lcom/android/server/am/ProcessRecord;
    goto :goto_0

    .line 53
    .restart local v4    # "proc":Lcom/android/server/am/ProcessRecord;
    :catchall_0
    move-exception v3

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 57
    .end local v2    # "procs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;"
    .end local v4    # "proc":Lcom/android/server/am/ProcessRecord;
    :cond_2
    const-wide/16 v2, 0x400

    mul-long/2addr v2, v0

    return-wide v2
.end method

.method public static getCachedLostRam()J
    .locals 2

    .line 62
    invoke-static {}, Lcom/android/server/am/MiuiMemoryInfoImpl;->getNativeCachedLostMemory()J

    move-result-wide v0

    return-wide v0
.end method

.method private static native getNativeCachedLostMemory()J
.end method


# virtual methods
.method public getFreeMemory()J
    .locals 14

    .line 67
    new-instance v0, Lcom/android/internal/util/MemInfoReader;

    invoke-direct {v0}, Lcom/android/internal/util/MemInfoReader;-><init>()V

    .line 68
    .local v0, "minfo":Lcom/android/internal/util/MemInfoReader;
    invoke-virtual {v0}, Lcom/android/internal/util/MemInfoReader;->readMemInfo()V

    .line 69
    invoke-virtual {v0}, Lcom/android/internal/util/MemInfoReader;->getRawInfo()[J

    move-result-object v1

    .line 70
    .local v1, "rawInfo":[J
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MEMINFO_KRECLAIMABLE: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xf

    aget-wide v4, v1, v3

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", MEMINFO_SLAB_RECLAIMABLE: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v4, 0x6

    aget-wide v5, v1, v4

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", MEMINFO_BUFFERS: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v5, 0x2

    aget-wide v6, v1, v5

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ", MEMINFO_CACHED: "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v6, 0x3

    aget-wide v7, v1, v6

    invoke-virtual {v2, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, ", MEMINFO_FREE: "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v7, 0x1

    aget-wide v8, v1, v7

    invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v8, "MiuiMemoryInfoImpl"

    invoke-static {v8, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    aget-wide v2, v1, v3

    .line 76
    .local v2, "kReclaimable":J
    const-wide/16 v9, 0x0

    cmp-long v9, v2, v9

    if-nez v9, :cond_0

    .line 77
    aget-wide v2, v1, v4

    .line 79
    :cond_0
    aget-wide v4, v1, v5

    add-long/2addr v4, v2

    aget-wide v9, v1, v6

    add-long/2addr v4, v9

    aget-wide v6, v1, v7

    add-long/2addr v4, v6

    const-wide/16 v6, 0x400

    mul-long/2addr v4, v6

    .line 81
    .local v4, "cache":J
    invoke-static {}, Lcom/android/server/am/MiuiMemoryInfoImpl;->getCachedLostRam()J

    move-result-wide v6

    .line 82
    .local v6, "lostCache":J
    add-long v9, v4, v6

    invoke-static {}, Lcom/android/server/am/MiuiMemoryInfoImpl;->getCachePss()J

    move-result-wide v11

    add-long/2addr v9, v11

    .line 83
    .local v9, "free":J
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "cache: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", cachePss: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {}, Lcom/android/server/am/MiuiMemoryInfoImpl;->getCachePss()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", lostcache: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v8, v11}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    sget-wide v11, Lcom/android/server/am/MiuiMemoryInfoImpl;->sTotalMem:J

    cmp-long v8, v9, v11

    if-ltz v8, :cond_1

    .line 86
    add-long v9, v4, v6

    .line 88
    :cond_1
    return-wide v9
.end method

.method public getMoreCachedSizeKb(Lcom/android/internal/util/MemInfoReader;)J
    .locals 7
    .param p1, "infos"    # Lcom/android/internal/util/MemInfoReader;

    .line 24
    invoke-virtual {p1}, Lcom/android/internal/util/MemInfoReader;->getRawInfo()[J

    move-result-object v0

    .line 25
    .local v0, "rawInfo":[J
    const/16 v1, 0xf

    aget-wide v1, v0, v1

    .line 29
    .local v1, "kReclaimable":J
    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-nez v3, :cond_0

    .line 30
    const/4 v3, 0x6

    aget-wide v1, v0, v3

    .line 32
    :cond_0
    const/4 v3, 0x2

    aget-wide v3, v0, v3

    add-long/2addr v3, v1

    const/4 v5, 0x3

    aget-wide v5, v0, v5

    add-long/2addr v3, v5

    return-wide v3
.end method
