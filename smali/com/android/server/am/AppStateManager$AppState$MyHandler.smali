.class Lcom/android/server/am/AppStateManager$AppState$MyHandler;
.super Landroid/os/Handler;
.source "AppStateManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/AppStateManager$AppState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/server/am/AppStateManager$AppState;


# direct methods
.method constructor <init>(Lcom/android/server/am/AppStateManager$AppState;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 1893
    iput-object p1, p0, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    .line 1894
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1895
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .line 1899
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 1900
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1905
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$mupdateAppState(Lcom/android/server/am/AppStateManager$AppState;Ljava/lang/String;)V

    goto :goto_0

    .line 1902
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$mstepAppState(Lcom/android/server/am/AppStateManager$AppState;Ljava/lang/String;)V

    .line 1903
    nop

    .line 1908
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
