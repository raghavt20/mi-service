public class com.android.server.am.ProcessPowerCleaner extends com.android.server.am.ProcessCleanerBase {
	 /* .source "ProcessPowerCleaner.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/am/ProcessPowerCleaner$ScreenStatusReceiver;, */
	 /* Lcom/android/server/am/ProcessPowerCleaner$H; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Float ACTIVE_PROCESS_MIX_THRESHOLD;
private static final Float ACTIVE_PROCESS_THRESHOLD_RATE;
private static final Double BACKGROUND_PROCESS_SINGLE_CPU_RATIO;
private static final Integer CHECK_CPU_GROWTH_RATE;
private static final Integer CHECK_CPU_MAX_TIME_MS;
private static final java.lang.String DESK_CLOCK_PROCESS_NAME;
private static final Integer SCREEN_OFF_DELAYED_TIME;
private static final Integer SCREEN_OFF_FROZEN_DELAYED_TIME;
private static final java.lang.String SCREEN_OFF_FROZEN_REASON;
private static final Integer SCREEN_OFF_START_CPU_CHECK_TIME;
private static final java.lang.String TAG;
private static final Integer THERMAL_KILL_ALL_PROCESS_MINADJ;
/* # instance fields */
private com.android.server.am.ActivityManagerService mAMS;
private Float mActiveProcessThreshold;
private Integer mCheckCPUTime;
private android.content.Context mContext;
private com.android.server.am.ProcessPowerCleaner$H mHandler;
private Boolean mIsScreenOffState;
private Boolean mLockOffCleanTestEnable;
private Float mLockOffCleanTestThreshold;
private com.android.server.am.ProcessManagerService mPMS;
private com.android.server.am.ProcessPolicy mProcessPolicy;
private com.android.server.am.ProcessPowerCleaner$ScreenStatusReceiver mScreenStatusReceiver;
private com.android.server.am.SystemPressureController mSysPressureCtrl;
/* # direct methods */
static android.content.Context -$$Nest$fgetmContext ( com.android.server.am.ProcessPowerCleaner p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContext;
} // .end method
static com.android.server.am.ProcessPowerCleaner$H -$$Nest$fgetmHandler ( com.android.server.am.ProcessPowerCleaner p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mHandler;
} // .end method
static void -$$Nest$fputmIsScreenOffState ( com.android.server.am.ProcessPowerCleaner p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/am/ProcessPowerCleaner;->mIsScreenOffState:Z */
	 return;
} // .end method
static void -$$Nest$mhandleKillAll ( com.android.server.am.ProcessPowerCleaner p0, miui.process.ProcessConfig p1, Boolean p2 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1, p2}, Lcom/android/server/am/ProcessPowerCleaner;->handleKillAll(Lmiui/process/ProcessConfig;Z)V */
	 return;
} // .end method
static void -$$Nest$mhandleScreenOffEvent ( com.android.server.am.ProcessPowerCleaner p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/am/ProcessPowerCleaner;->handleScreenOffEvent()V */
	 return;
} // .end method
static void -$$Nest$mhandleThermalKillProc ( com.android.server.am.ProcessPowerCleaner p0, miui.process.ProcessConfig p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessPowerCleaner;->handleThermalKillProc(Lmiui/process/ProcessConfig;)V */
	 return;
} // .end method
static void -$$Nest$mpowerFrozenAll ( com.android.server.am.ProcessPowerCleaner p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/am/ProcessPowerCleaner;->powerFrozenAll()V */
	 return;
} // .end method
static void -$$Nest$mresetLockOffConfig ( com.android.server.am.ProcessPowerCleaner p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/am/ProcessPowerCleaner;->resetLockOffConfig()V */
	 return;
} // .end method
static void -$$Nest$mupdateCloudControlParas ( com.android.server.am.ProcessPowerCleaner p0, android.content.Context p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessPowerCleaner;->updateCloudControlParas(Landroid/content/Context;)V */
	 return;
} // .end method
static com.android.server.am.ProcessPowerCleaner ( ) {
	 /* .locals 2 */
	 /* .line 41 */
	 /* int-to-float v0, v0 */
	 /* const/high16 v1, 0x3f800000 # 1.0f */
	 /* div-float/2addr v0, v1 */
	 return;
} // .end method
public com.android.server.am.ProcessPowerCleaner ( ) {
	 /* .locals 2 */
	 /* .param p1, "ams" # Lcom/android/server/am/ActivityManagerService; */
	 /* .line 66 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessCleanerBase;-><init>(Lcom/android/server/am/ActivityManagerService;)V */
	 /* .line 50 */
	 /* int-to-float v0, v0 */
	 /* const/high16 v1, 0x3f800000 # 1.0f */
	 /* div-float/2addr v0, v1 */
	 /* iput v0, p0, Lcom/android/server/am/ProcessPowerCleaner;->mActiveProcessThreshold:F */
	 /* .line 52 */
	 /* const v0, 0x3a83126f # 0.001f */
	 /* iput v0, p0, Lcom/android/server/am/ProcessPowerCleaner;->mLockOffCleanTestThreshold:F */
	 /* .line 53 */
	 /* const v0, 0x927c0 */
	 /* iput v0, p0, Lcom/android/server/am/ProcessPowerCleaner;->mCheckCPUTime:I */
	 /* .line 54 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput-boolean v0, p0, Lcom/android/server/am/ProcessPowerCleaner;->mIsScreenOffState:Z */
	 /* .line 55 */
	 /* iput-boolean v0, p0, Lcom/android/server/am/ProcessPowerCleaner;->mLockOffCleanTestEnable:Z */
	 /* .line 67 */
	 this.mAMS = p1;
	 /* .line 68 */
	 return;
} // .end method
private void cleanAllSubProcess ( ) {
	 /* .locals 1 */
	 /* .line 362 */
	 v0 = this.mSysPressureCtrl;
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 363 */
		 (( com.android.server.am.SystemPressureController ) v0 ).cleanAllSubProcess ( ); // invoke-virtual {v0}, Lcom/android/server/am/SystemPressureController;->cleanAllSubProcess()V
		 /* .line 365 */
	 } // :cond_0
	 return;
} // .end method
private void handleKillAll ( miui.process.ProcessConfig p0, Boolean p1 ) {
	 /* .locals 23 */
	 /* .param p1, "config" # Lmiui/process/ProcessConfig; */
	 /* .param p2, "isKillSystemProc" # Z */
	 /* .line 170 */
	 /* move-object/from16 v9, p0 */
	 /* move-object/from16 v10, p1 */
	 v0 = this.mPMS;
	 /* if-nez v0, :cond_0 */
	 /* .line 171 */
	 return;
	 /* .line 173 */
} // :cond_0
v11 = /* invoke-virtual/range {p1 ..p1}, Lmiui/process/ProcessConfig;->getPolicy()I */
/* .line 174 */
/* .local v11, "policy":I */
v0 = /* invoke-virtual/range {p1 ..p1}, Lmiui/process/ProcessConfig;->getPolicy()I */
(( com.android.server.am.ProcessPowerCleaner ) v9 ).getKillReason ( v0 ); // invoke-virtual {v9, v0}, Lcom/android/server/am/ProcessPowerCleaner;->getKillReason(I)Ljava/lang/String;
/* .line 176 */
/* .local v12, "reason":Ljava/lang/String; */
v0 = this.mAMS;
v0 = this.mActivityTaskManager;
com.android.server.wm.WindowProcessUtils .getPerceptibleRecentAppList ( v0 );
/* .line 178 */
/* .local v13, "fgTaskPackageMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/String;>;" */
v0 = this.mProcessPolicy;
(( com.android.server.am.ProcessPowerCleaner ) v9 ).getProcessPolicyWhiteList ( v10, v0 ); // invoke-virtual {v9, v10, v0}, Lcom/android/server/am/ProcessPowerCleaner;->getProcessPolicyWhiteList(Lmiui/process/ProcessConfig;Lcom/android/server/am/ProcessPolicy;)Ljava/util/List;
/* .line 179 */
/* .local v14, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
if ( v13 != null) { // if-eqz v13, :cond_1
	 if ( v14 != null) { // if-eqz v14, :cond_1
		 /* .line 180 */
		 /* .line 182 */
	 } // :cond_1
	 v0 = this.mProcessPolicy;
	 (( com.android.server.am.ProcessPowerCleaner ) v9 ).removeTasksIfNeeded ( v10, v0, v14, v13 ); // invoke-virtual {v9, v10, v0, v14, v13}, Lcom/android/server/am/ProcessPowerCleaner;->removeTasksIfNeeded(Lmiui/process/ProcessConfig;Lcom/android/server/am/ProcessPolicy;Ljava/util/List;Ljava/util/Map;)V
	 /* .line 183 */
	 v0 = this.mSmartPowerService;
	 /* .line 184 */
	 /* .local v15, "appList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState;>;" */
	 if ( v15 != null) { // if-eqz v15, :cond_a
		 v0 = 		 (( java.util.ArrayList ) v15 ).isEmpty ( ); // invoke-virtual {v15}, Ljava/util/ArrayList;->isEmpty()Z
		 if ( v0 != null) { // if-eqz v0, :cond_2
			 /* goto/16 :goto_3 */
			 /* .line 187 */
		 } // :cond_2
		 (( java.util.ArrayList ) v15 ).iterator ( ); // invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
	 } // :cond_3
} // :goto_0
v0 = /* invoke-interface/range {v16 ..v16}, Ljava/util/Iterator;->hasNext()Z */
if ( v0 != null) { // if-eqz v0, :cond_9
	 /* invoke-interface/range {v16 ..v16}, Ljava/util/Iterator;->next()Ljava/lang/Object; */
	 /* move-object/from16 v17, v0 */
	 /* check-cast v17, Lcom/miui/server/smartpower/IAppState; */
	 /* .line 188 */
	 /* .local v17, "appState":Lcom/miui/server/smartpower/IAppState; */
	 v0 = 	 /* invoke-virtual/range {v17 ..v17}, Lcom/miui/server/smartpower/IAppState;->getPackageName()Ljava/lang/String; */
	 if ( v0 != null) { // if-eqz v0, :cond_4
		 /* .line 189 */
	 } // :cond_4
	 /* const/16 v0, 0x14 */
	 /* if-ne v11, v0, :cond_5 */
	 /* .line 190 */
	 v0 = 	 /* invoke-virtual/range {v17 ..v17}, Lcom/miui/server/smartpower/IAppState;->getAdj()I */
	 /* if-lez v0, :cond_3 */
	 /* .line 191 */
	 v0 = 	 /* invoke-virtual/range {v17 ..v17}, Lcom/miui/server/smartpower/IAppState;->isSystemApp()Z */
	 if ( v0 != null) { // if-eqz v0, :cond_5
		 /* .line 192 */
		 /* .line 194 */
	 } // :cond_5
	 /* invoke-virtual/range {v17 ..v17}, Lcom/miui/server/smartpower/IAppState;->getRunningProcessList()Ljava/util/ArrayList; */
	 /* .line 195 */
	 /* .local v18, "runningAppList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;" */
	 /* invoke-interface/range {v18 ..v18}, Ljava/util/List;->iterator()Ljava/util/Iterator; */
} // :goto_1
v0 = /* invoke-interface/range {v19 ..v19}, Ljava/util/Iterator;->hasNext()Z */
if ( v0 != null) { // if-eqz v0, :cond_8
	 /* invoke-interface/range {v19 ..v19}, Ljava/util/Iterator;->next()Ljava/lang/Object; */
	 /* move-object v8, v0 */
	 /* check-cast v8, Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
	 /* .line 196 */
	 /* .local v8, "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
	 (( com.miui.server.smartpower.IAppState$IRunningProcess ) v8 ).getProcessRecord ( ); // invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;
	 /* .line 197 */
	 /* .local v20, "app":Lcom/android/server/am/ProcessRecord; */
	 v0 = 	 (( com.miui.server.smartpower.IAppState$IRunningProcess ) v8 ).getAdj ( ); // invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I
	 /* if-lez v0, :cond_7 */
	 /* .line 198 */
	 v0 = 	 (( com.android.server.am.ProcessPowerCleaner ) v9 ).isCurrentProcessInBackup ( v8 ); // invoke-virtual {v9, v8}, Lcom/android/server/am/ProcessPowerCleaner;->isCurrentProcessInBackup(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z
	 /* if-nez v0, :cond_6 */
	 /* .line 199 */
	 int v4 = 1; // const/4 v4, 0x1
	 v5 = this.mPMS;
	 final String v6 = "ProcessPowerCleaner"; // const-string v6, "ProcessPowerCleaner"
	 v7 = this.mHandler;
	 v3 = this.mContext;
	 /* move-object/from16 v0, p0 */
	 /* move-object/from16 v1, v20 */
	 /* move v2, v11 */
	 /* move-object/from16 v21, v3 */
	 /* move-object v3, v12 */
	 /* move-object/from16 v22, v8 */
} // .end local v8 # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
/* .local v22, "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* move-object/from16 v8, v21 */
/* invoke-virtual/range {v0 ..v8}, Lcom/android/server/am/ProcessPowerCleaner;->killOnce(Lcom/android/server/am/ProcessRecord;ILjava/lang/String;ZLcom/android/server/am/ProcessManagerService;Ljava/lang/String;Landroid/os/Handler;Landroid/content/Context;)V */
/* .line 198 */
} // .end local v22 # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
/* .restart local v8 # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
} // :cond_6
/* move-object/from16 v22, v8 */
} // .end local v8 # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
/* .restart local v22 # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 197 */
} // .end local v22 # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
/* .restart local v8 # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
} // :cond_7
/* move-object/from16 v22, v8 */
/* .line 201 */
} // .end local v8 # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
} // .end local v20 # "app":Lcom/android/server/am/ProcessRecord;
} // :goto_2
/* .line 202 */
} // .end local v17 # "appState":Lcom/miui/server/smartpower/IAppState;
} // .end local v18 # "runningAppList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
} // :cond_8
/* .line 203 */
} // :cond_9
return;
/* .line 185 */
} // :cond_a
} // :goto_3
return;
} // .end method
private Boolean handleKillApp ( miui.process.ProcessConfig p0 ) {
/* .locals 17 */
/* .param p1, "config" # Lmiui/process/ProcessConfig; */
/* .line 206 */
/* move-object/from16 v9, p0 */
v0 = /* invoke-virtual/range {p1 ..p1}, Lmiui/process/ProcessConfig;->isUidInvalid()Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 207 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "uid:" */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = /* invoke-virtual/range {p1 ..p1}, Lmiui/process/ProcessConfig;->getUserId()I */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " is invalid"; // const-string v2, " is invalid"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "ProcessPowerCleaner"; // const-string v2, "ProcessPowerCleaner"
android.util.Slog .w ( v2,v0 );
/* .line 208 */
/* .line 210 */
} // :cond_0
/* invoke-virtual/range {p1 ..p1}, Lmiui/process/ProcessConfig;->getKillingPackage()Ljava/lang/String; */
/* .line 211 */
/* .local v10, "packageName":Ljava/lang/String; */
v11 = /* invoke-virtual/range {p1 ..p1}, Lmiui/process/ProcessConfig;->getUid()I */
/* .line 212 */
/* .local v11, "uid":I */
v12 = /* invoke-virtual/range {p1 ..p1}, Lmiui/process/ProcessConfig;->getPolicy()I */
/* .line 214 */
/* .local v12, "policy":I */
v0 = android.text.TextUtils .isEmpty ( v10 );
/* if-nez v0, :cond_4 */
v0 = android.os.UserHandle .isApp ( v11 );
/* if-nez v0, :cond_1 */
/* .line 218 */
} // :cond_1
v0 = this.mSmartPowerService;
/* .line 219 */
/* .line 220 */
/* .local v13, "runningAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;" */
(( java.util.ArrayList ) v13 ).iterator ( ); // invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v0 = } // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_3
/* move-object v15, v0 */
/* check-cast v15, Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 221 */
/* .local v15, "runningProcess":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v15 ).getProcessRecord ( ); // invoke-virtual {v15}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;
/* .line 222 */
/* .local v16, "app":Lcom/android/server/am/ProcessRecord; */
v0 = (( com.android.server.am.ProcessPowerCleaner ) v9 ).isCurrentProcessInBackup ( v15 ); // invoke-virtual {v9, v15}, Lcom/android/server/am/ProcessPowerCleaner;->isCurrentProcessInBackup(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z
/* if-nez v0, :cond_2 */
/* .line 223 */
(( com.android.server.am.ProcessPowerCleaner ) v9 ).getKillReason ( v12 ); // invoke-virtual {v9, v12}, Lcom/android/server/am/ProcessPowerCleaner;->getKillReason(I)Ljava/lang/String;
int v4 = 1; // const/4 v4, 0x1
v5 = this.mPMS;
final String v6 = "ProcessPowerCleaner"; // const-string v6, "ProcessPowerCleaner"
v7 = this.mHandler;
v8 = this.mContext;
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, v16 */
/* move v2, v12 */
/* invoke-virtual/range {v0 ..v8}, Lcom/android/server/am/ProcessPowerCleaner;->killOnce(Lcom/android/server/am/ProcessRecord;ILjava/lang/String;ZLcom/android/server/am/ProcessManagerService;Ljava/lang/String;Landroid/os/Handler;Landroid/content/Context;)V */
/* .line 225 */
} // .end local v15 # "runningProcess":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
} // .end local v16 # "app":Lcom/android/server/am/ProcessRecord;
} // :cond_2
/* .line 226 */
} // :cond_3
int v0 = 1; // const/4 v0, 0x1
/* .line 215 */
} // .end local v13 # "runningAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
} // :cond_4
} // :goto_1
} // .end method
private void handleScreenOffEvent ( ) {
/* .locals 4 */
/* .line 317 */
v0 = this.mSmartPowerService;
int v1 = 1; // const/4 v1, 0x1
/* .line 318 */
/* invoke-direct {p0}, Lcom/android/server/am/ProcessPowerCleaner;->cleanAllSubProcess()V */
/* .line 319 */
/* invoke-direct {p0}, Lcom/android/server/am/ProcessPowerCleaner;->powerFrozenAll()V */
/* .line 321 */
v0 = this.mHandler;
int v1 = 4; // const/4 v1, 0x4
(( com.android.server.am.ProcessPowerCleaner$H ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessPowerCleaner$H;->obtainMessage(I)Landroid/os/Message;
/* .line 322 */
/* .local v0, "message":Landroid/os/Message; */
v1 = this.mHandler;
/* const-wide/32 v2, 0x16e360 */
(( com.android.server.am.ProcessPowerCleaner$H ) v1 ).sendMessageDelayed ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/am/ProcessPowerCleaner$H;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 323 */
return;
} // .end method
private void handleThermalKillProc ( miui.process.ProcessConfig p0 ) {
/* .locals 16 */
/* .param p1, "config" # Lmiui/process/ProcessConfig; */
/* .line 230 */
/* move-object/from16 v9, p0 */
v0 = this.mSmartPowerService;
/* .line 231 */
/* .local v10, "appList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState;>;" */
if ( v10 != null) { // if-eqz v10, :cond_6
v0 = (( java.util.ArrayList ) v10 ).isEmpty ( ); // invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 235 */
} // :cond_0
(( java.util.ArrayList ) v10 ).iterator ( ); // invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
} // :cond_1
v0 = } // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_5
/* move-object v12, v0 */
/* check-cast v12, Lcom/miui/server/smartpower/IAppState; */
/* .line 236 */
/* .local v12, "appState":Lcom/miui/server/smartpower/IAppState; */
v0 = (( com.miui.server.smartpower.IAppState ) v12 ).isSystemApp ( ); // invoke-virtual {v12}, Lcom/miui/server/smartpower/IAppState;->isSystemApp()Z
/* if-nez v0, :cond_1 */
/* .line 237 */
v0 = (( com.miui.server.smartpower.IAppState ) v12 ).getAdj ( ); // invoke-virtual {v12}, Lcom/miui/server/smartpower/IAppState;->getAdj()I
/* const/16 v1, 0xc8 */
/* if-gt v0, v1, :cond_2 */
/* .line 238 */
/* .line 240 */
} // :cond_2
(( com.miui.server.smartpower.IAppState ) v12 ).getRunningProcessList ( ); // invoke-virtual {v12}, Lcom/miui/server/smartpower/IAppState;->getRunningProcessList()Ljava/util/ArrayList;
/* .line 241 */
/* .local v13, "runningAppList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;" */
v0 = } // :goto_1
if ( v0 != null) { // if-eqz v0, :cond_4
/* move-object v15, v0 */
/* check-cast v15, Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 242 */
/* .local v15, "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
v0 = (( com.android.server.am.ProcessPowerCleaner ) v9 ).isCurrentProcessInBackup ( v15 ); // invoke-virtual {v9, v15}, Lcom/android/server/am/ProcessPowerCleaner;->isCurrentProcessInBackup(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z
/* if-nez v0, :cond_3 */
/* .line 243 */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v15 ).getProcessRecord ( ); // invoke-virtual {v15}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;
v2 = /* invoke-virtual/range {p1 ..p1}, Lmiui/process/ProcessConfig;->getPolicy()I */
/* .line 244 */
v0 = /* invoke-virtual/range {p1 ..p1}, Lmiui/process/ProcessConfig;->getPolicy()I */
(( com.android.server.am.ProcessPowerCleaner ) v9 ).getKillReason ( v0 ); // invoke-virtual {v9, v0}, Lcom/android/server/am/ProcessPowerCleaner;->getKillReason(I)Ljava/lang/String;
int v4 = 1; // const/4 v4, 0x1
v5 = this.mPMS;
final String v6 = "ProcessPowerCleaner"; // const-string v6, "ProcessPowerCleaner"
v7 = this.mHandler;
v8 = this.mContext;
/* .line 243 */
/* move-object/from16 v0, p0 */
/* invoke-virtual/range {v0 ..v8}, Lcom/android/server/am/ProcessPowerCleaner;->killOnce(Lcom/android/server/am/ProcessRecord;ILjava/lang/String;ZLcom/android/server/am/ProcessManagerService;Ljava/lang/String;Landroid/os/Handler;Landroid/content/Context;)V */
/* .line 246 */
} // .end local v15 # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
} // :cond_3
/* .line 247 */
} // .end local v12 # "appState":Lcom/miui/server/smartpower/IAppState;
} // .end local v13 # "runningAppList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
} // :cond_4
/* .line 248 */
} // :cond_5
return;
/* .line 232 */
} // :cond_6
} // :goto_2
return;
} // .end method
private void killActiveProcess ( Float p0, Integer p1, java.util.List p2 ) {
/* .locals 28 */
/* .param p1, "threshold" # F */
/* .param p2, "policy" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(FI", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 276 */
/* .local p3, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* move-object/from16 v10, p0 */
/* move/from16 v11, p1 */
/* move/from16 v12, p2 */
v0 = this.mSmartPowerService;
int v13 = 0; // const/4 v13, 0x0
/* move-result-wide v14 */
/* .line 277 */
/* .local v14, "uptimeSince":J */
/* const-wide/16 v0, 0x0 */
/* cmp-long v0, v14, v0 */
/* if-gtz v0, :cond_0 */
/* .line 278 */
return;
/* .line 280 */
} // :cond_0
v0 = this.mSmartPowerService;
/* .line 281 */
/* .local v16, "appStates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState;>;" */
/* invoke-virtual/range {v16 ..v16}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator; */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_a
/* move-object/from16 v17, v1 */
/* check-cast v17, Lcom/miui/server/smartpower/IAppState; */
/* .line 282 */
/* .local v17, "appInfo":Lcom/miui/server/smartpower/IAppState; */
/* invoke-virtual/range {v17 ..v17}, Lcom/miui/server/smartpower/IAppState;->getRunningProcessList()Ljava/util/ArrayList; */
/* .line 283 */
/* .local v18, "procs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;" */
v1 = /* invoke-virtual/range {v17 ..v17}, Lcom/miui/server/smartpower/IAppState;->getAdj()I */
/* const/16 v9, 0xc8 */
/* if-le v1, v9, :cond_1 */
int v1 = 1; // const/4 v1, 0x1
/* move v5, v1 */
} // :cond_1
/* move v5, v13 */
/* .line 284 */
/* .local v5, "isForceStop":Z */
} // :goto_1
/* invoke-virtual/range {v18 ..v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator; */
} // :goto_2
v1 = /* invoke-interface/range {v19 ..v19}, Ljava/util/Iterator;->hasNext()Z */
if ( v1 != null) { // if-eqz v1, :cond_9
/* invoke-interface/range {v19 ..v19}, Ljava/util/Iterator;->next()Ljava/lang/Object; */
/* move-object v8, v1 */
/* check-cast v8, Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 285 */
/* .local v8, "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v8 ).getCurCpuTime ( ); // invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getCurCpuTime()J
/* move-result-wide v6 */
/* .line 286 */
/* .local v6, "curCpuTime":J */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v8 ).getLastCpuTime ( ); // invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getLastCpuTime()J
/* move-result-wide v20 */
/* .line 287 */
/* .local v20, "lastCpuTime":J */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v8 ).setLastCpuTime ( v6, v7 ); // invoke-virtual {v8, v6, v7}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->setLastCpuTime(J)V
/* .line 288 */
v1 = this.mAMS;
/* monitor-enter v1 */
/* .line 289 */
/* nop */
/* .line 290 */
try { // :try_start_0
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v8 ).getProcessRecord ( ); // invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;
v3 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v8 ).getUid ( ); // invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUid()I
v4 = this.mPMS;
/* .line 289 */
v2 = (( com.android.server.am.ProcessPowerCleaner ) v10 ).isInWhiteListLock ( v2, v3, v12, v4 ); // invoke-virtual {v10, v2, v3, v12, v4}, Lcom/android/server/am/ProcessPowerCleaner;->isInWhiteListLock(Lcom/android/server/am/ProcessRecord;IILcom/android/server/am/ProcessManagerService;)Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 291 */
try { // :try_start_1
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 293 */
/* :catchall_0 */
/* move-exception v0 */
/* move-wide/from16 v26, v6 */
/* move-object v10, v8 */
/* goto/16 :goto_4 */
} // :cond_2
try { // :try_start_2
/* monitor-exit v1 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 294 */
v1 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v8 ).getUid ( ); // invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUid()I
v1 = (( com.android.server.am.ProcessPowerCleaner ) v10 ).isAudioOrGPSApp ( v1 ); // invoke-virtual {v10, v1}, Lcom/android/server/am/ProcessPowerCleaner;->isAudioOrGPSApp(I)Z
/* if-nez v1, :cond_8 */
/* .line 295 */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v8 ).getPackageName ( ); // invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;
v1 = /* move-object/from16 v4, p3 */
/* if-nez v1, :cond_7 */
/* .line 296 */
v1 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v8 ).getAdj ( ); // invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I
/* if-lt v1, v9, :cond_6 */
/* .line 297 */
v1 = (( com.android.server.am.ProcessPowerCleaner ) v10 ).isCurrentProcessInBackup ( v8 ); // invoke-virtual {v10, v8}, Lcom/android/server/am/ProcessPowerCleaner;->isCurrentProcessInBackup(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z
/* if-nez v1, :cond_5 */
/* .line 298 */
v1 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v8 ).hasForegrundService ( ); // invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->hasForegrundService()Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 299 */
/* .line 301 */
} // :cond_3
/* sub-long v1, v6, v20 */
v3 = this.mSmartPowerService;
v3 = /* .line 302 */
/* int-to-long v9, v3 */
/* div-long v9, v1, v9 */
/* .line 303 */
/* .local v9, "cpuUsedTime":J */
/* long-to-float v1, v9 */
/* const/high16 v2, 0x42c80000 # 100.0f */
/* mul-float/2addr v1, v2 */
/* long-to-float v3, v14 */
/* div-float/2addr v1, v3 */
/* cmpl-float v1, v1, v11 */
/* if-ltz v1, :cond_4 */
/* .line 304 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* move-object/from16 v3, p0 */
(( com.android.server.am.ProcessPowerCleaner ) v3 ).getKillReason ( v12 ); // invoke-virtual {v3, v12}, Lcom/android/server/am/ProcessPowerCleaner;->getKillReason(I)Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v13 ); // invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v13 = " over "; // const-string v13, " over "
(( java.lang.StringBuilder ) v1 ).append ( v13 ); // invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 305 */
android.util.TimeUtils .formatDuration ( v14,v15 );
(( java.lang.StringBuilder ) v1 ).append ( v13 ); // invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v13 = " used "; // const-string v13, " used "
(( java.lang.StringBuilder ) v1 ).append ( v13 ); // invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 306 */
android.util.TimeUtils .formatDuration ( v9,v10 );
(( java.lang.StringBuilder ) v1 ).append ( v13 ); // invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v13 = " ("; // const-string v13, " ("
(( java.lang.StringBuilder ) v1 ).append ( v13 ); // invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* long-to-float v13, v9 */
/* mul-float/2addr v13, v2 */
/* long-to-float v2, v14 */
/* div-float/2addr v13, v2 */
(( java.lang.StringBuilder ) v1 ).append ( v13 ); // invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v2 = "%) threshold "; // const-string v2, "%) threshold "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v11 ); // invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 309 */
/* .local v13, "reason":Ljava/lang/String; */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v8 ).getProcessRecord ( ); // invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;
v1 = this.mPMS;
final String v22 = "ProcessPowerCleaner"; // const-string v22, "ProcessPowerCleaner"
/* move-object/from16 v23, v0 */
v0 = this.mHandler;
/* move-wide/from16 v24, v9 */
} // .end local v9 # "cpuUsedTime":J
/* .local v24, "cpuUsedTime":J */
v9 = this.mContext;
/* move-object v10, v1 */
/* move-object/from16 v1, p0 */
/* move/from16 v3, p2 */
/* move-object v4, v13 */
/* move-wide/from16 v26, v6 */
} // .end local v6 # "curCpuTime":J
/* .local v26, "curCpuTime":J */
/* move-object v6, v10 */
/* move-object/from16 v7, v22 */
/* move-object v10, v8 */
} // .end local v8 # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
/* .local v10, "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* move-object v8, v0 */
/* const/16 v0, 0xc8 */
/* invoke-virtual/range {v1 ..v9}, Lcom/android/server/am/ProcessPowerCleaner;->killOnce(Lcom/android/server/am/ProcessRecord;ILjava/lang/String;ZLcom/android/server/am/ProcessManagerService;Ljava/lang/String;Landroid/os/Handler;Landroid/content/Context;)V */
/* .line 303 */
} // .end local v10 # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
} // .end local v13 # "reason":Ljava/lang/String;
} // .end local v24 # "cpuUsedTime":J
} // .end local v26 # "curCpuTime":J
/* .restart local v6 # "curCpuTime":J */
/* .restart local v8 # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .restart local v9 # "cpuUsedTime":J */
} // :cond_4
/* move-object/from16 v23, v0 */
/* move-wide/from16 v26, v6 */
/* move-wide/from16 v24, v9 */
/* const/16 v0, 0xc8 */
/* move-object v10, v8 */
/* .line 312 */
} // .end local v6 # "curCpuTime":J
} // .end local v8 # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
} // .end local v9 # "cpuUsedTime":J
} // .end local v20 # "lastCpuTime":J
} // :goto_3
int v13 = 0; // const/4 v13, 0x0
/* move-object/from16 v10, p0 */
/* move v9, v0 */
/* move-object/from16 v0, v23 */
/* goto/16 :goto_2 */
/* .line 297 */
/* .restart local v6 # "curCpuTime":J */
/* .restart local v8 # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .restart local v20 # "lastCpuTime":J */
} // :cond_5
/* move-object/from16 v23, v0 */
/* move-wide/from16 v26, v6 */
/* move-object v10, v8 */
/* move v0, v9 */
} // .end local v6 # "curCpuTime":J
} // .end local v8 # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
/* .restart local v10 # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .restart local v26 # "curCpuTime":J */
int v13 = 0; // const/4 v13, 0x0
/* move-object/from16 v10, p0 */
/* move-object/from16 v0, v23 */
/* goto/16 :goto_2 */
/* .line 296 */
} // .end local v10 # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
} // .end local v26 # "curCpuTime":J
/* .restart local v6 # "curCpuTime":J */
/* .restart local v8 # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
} // :cond_6
/* move-object/from16 v23, v0 */
/* move-wide/from16 v26, v6 */
/* move-object v10, v8 */
/* move v0, v9 */
} // .end local v6 # "curCpuTime":J
} // .end local v8 # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
/* .restart local v10 # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .restart local v26 # "curCpuTime":J */
int v13 = 0; // const/4 v13, 0x0
/* move-object/from16 v10, p0 */
/* move-object/from16 v0, v23 */
/* goto/16 :goto_2 */
/* .line 295 */
} // .end local v10 # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
} // .end local v26 # "curCpuTime":J
/* .restart local v6 # "curCpuTime":J */
/* .restart local v8 # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
} // :cond_7
/* move-object/from16 v23, v0 */
/* move-wide/from16 v26, v6 */
/* move-object v10, v8 */
/* move v0, v9 */
} // .end local v6 # "curCpuTime":J
} // .end local v8 # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
/* .restart local v10 # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .restart local v26 # "curCpuTime":J */
int v13 = 0; // const/4 v13, 0x0
/* move-object/from16 v10, p0 */
/* move-object/from16 v0, v23 */
/* goto/16 :goto_2 */
/* .line 294 */
} // .end local v10 # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
} // .end local v26 # "curCpuTime":J
/* .restart local v6 # "curCpuTime":J */
/* .restart local v8 # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
} // :cond_8
/* move-object/from16 v23, v0 */
/* move-wide/from16 v26, v6 */
/* move-object v10, v8 */
/* move v0, v9 */
} // .end local v6 # "curCpuTime":J
} // .end local v8 # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
/* .restart local v10 # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .restart local v26 # "curCpuTime":J */
int v13 = 0; // const/4 v13, 0x0
/* move-object/from16 v10, p0 */
/* move-object/from16 v0, v23 */
/* goto/16 :goto_2 */
/* .line 293 */
} // .end local v10 # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
} // .end local v26 # "curCpuTime":J
/* .restart local v6 # "curCpuTime":J */
/* .restart local v8 # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* :catchall_1 */
/* move-exception v0 */
/* move-wide/from16 v26, v6 */
/* move-object v10, v8 */
} // .end local v6 # "curCpuTime":J
} // .end local v8 # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
/* .restart local v10 # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .restart local v26 # "curCpuTime":J */
} // :goto_4
try { // :try_start_3
/* monitor-exit v1 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_2 */
/* throw v0 */
/* :catchall_2 */
/* move-exception v0 */
/* .line 284 */
} // .end local v10 # "procInfo":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
} // .end local v20 # "lastCpuTime":J
} // .end local v26 # "curCpuTime":J
} // :cond_9
/* move-object/from16 v23, v0 */
/* .line 313 */
} // .end local v5 # "isForceStop":Z
} // .end local v17 # "appInfo":Lcom/miui/server/smartpower/IAppState;
} // .end local v18 # "procs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
int v13 = 0; // const/4 v13, 0x0
/* move-object/from16 v10, p0 */
/* goto/16 :goto_0 */
/* .line 314 */
} // :cond_a
return;
} // .end method
private void powerFrozenAll ( ) {
/* .locals 4 */
/* .line 326 */
/* sget-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_FROZEN_ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mSmartPowerService;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 327 */
v0 = this.mSmartPowerService;
final String v1 = "lock off frozen"; // const-string v1, "lock off frozen"
/* .line 328 */
final String v0 = "ProcessPowerCleaner"; // const-string v0, "ProcessPowerCleaner"
final String v1 = "Frozen: lock off frozen"; // const-string v1, "Frozen: lock off frozen"
android.util.Slog .d ( v0,v1 );
/* .line 329 */
v0 = this.mHandler;
int v1 = 5; // const/4 v1, 0x5
(( com.android.server.am.ProcessPowerCleaner$H ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessPowerCleaner$H;->obtainMessage(I)Landroid/os/Message;
/* .line 330 */
/* .local v0, "message":Landroid/os/Message; */
v1 = this.mHandler;
/* const-wide/32 v2, 0x2bf20 */
(( com.android.server.am.ProcessPowerCleaner$H ) v1 ).sendMessageDelayed ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/am/ProcessPowerCleaner$H;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 332 */
} // .end local v0 # "message":Landroid/os/Message;
} // :cond_0
return;
} // .end method
private void registerCloudObserver ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 345 */
/* new-instance v0, Lcom/android/server/am/ProcessPowerCleaner$1; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/am/ProcessPowerCleaner$1;-><init>(Lcom/android/server/am/ProcessPowerCleaner;Landroid/os/Handler;)V */
/* .line 354 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "perf_proc_power"; // const-string v2, "perf_proc_power"
android.provider.Settings$System .getUriFor ( v2 );
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 356 */
return;
} // .end method
private void resetLockOffConfig ( ) {
/* .locals 2 */
/* .line 368 */
v0 = this.mHandler;
/* const/16 v1, 0x10 */
(( com.android.server.am.ProcessPowerCleaner$H ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessPowerCleaner$H;->removeMessages(I)V
/* .line 369 */
v0 = this.mHandler;
int v1 = 4; // const/4 v1, 0x4
(( com.android.server.am.ProcessPowerCleaner$H ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessPowerCleaner$H;->removeMessages(I)V
/* .line 370 */
v0 = this.mHandler;
int v1 = 5; // const/4 v1, 0x5
(( com.android.server.am.ProcessPowerCleaner$H ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessPowerCleaner$H;->removeMessages(I)V
/* .line 371 */
/* const/high16 v0, 0x40000000 # 2.0f */
/* iput v0, p0, Lcom/android/server/am/ProcessPowerCleaner;->mActiveProcessThreshold:F */
/* .line 372 */
/* const v0, 0x927c0 */
/* iput v0, p0, Lcom/android/server/am/ProcessPowerCleaner;->mCheckCPUTime:I */
/* .line 373 */
return;
} // .end method
private void updateCloudControlParas ( android.content.Context p0 ) {
/* .locals 0 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 359 */
return;
} // .end method
/* # virtual methods */
android.os.Message createMessage ( Integer p0, miui.process.ProcessConfig p1, android.os.Handler p2 ) {
/* .locals 1 */
/* .param p1, "event" # I */
/* .param p2, "config" # Lmiui/process/ProcessConfig; */
/* .param p3, "handler" # Landroid/os/Handler; */
/* .line 335 */
(( android.os.Handler ) p3 ).obtainMessage ( p1 ); // invoke-virtual {p3, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 336 */
/* .local v0, "msg":Landroid/os/Message; */
this.obj = p2;
/* .line 337 */
} // .end method
public void handleAutoLockOff ( ) {
/* .locals 6 */
/* .line 251 */
v0 = this.mAMS;
v0 = this.mActivityTaskManager;
com.android.server.wm.WindowProcessUtils .getPerceptibleRecentAppList ( v0 );
/* .line 253 */
/* .local v0, "fgTaskPackageMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/String;>;" */
/* invoke-direct {p0}, Lcom/android/server/am/ProcessPowerCleaner;->cleanAllSubProcess()V */
/* .line 254 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 255 */
/* .local v1, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
final String v2 = "com.android.deskclock"; // const-string v2, "com.android.deskclock"
/* .line 256 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 257 */
/* .line 259 */
} // :cond_0
/* iget-boolean v2, p0, Lcom/android/server/am/ProcessPowerCleaner;->mLockOffCleanTestEnable:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* iget v2, p0, Lcom/android/server/am/ProcessPowerCleaner;->mLockOffCleanTestThreshold:F */
/* .line 260 */
} // :cond_1
/* iget v2, p0, Lcom/android/server/am/ProcessPowerCleaner;->mActiveProcessThreshold:F */
} // :goto_0
/* nop */
/* .line 259 */
/* const/16 v3, 0x16 */
/* invoke-direct {p0, v2, v3, v1}, Lcom/android/server/am/ProcessPowerCleaner;->killActiveProcess(FILjava/util/List;)V */
/* .line 262 */
/* iget-boolean v2, p0, Lcom/android/server/am/ProcessPowerCleaner;->mLockOffCleanTestEnable:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 263 */
int v2 = 0; // const/4 v2, 0x0
/* iput-boolean v2, p0, Lcom/android/server/am/ProcessPowerCleaner;->mLockOffCleanTestEnable:Z */
/* .line 264 */
return;
/* .line 267 */
} // :cond_2
v2 = this.mHandler;
int v3 = 4; // const/4 v3, 0x4
(( com.android.server.am.ProcessPowerCleaner$H ) v2 ).removeMessages ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/am/ProcessPowerCleaner$H;->removeMessages(I)V
/* .line 268 */
v2 = this.mHandler;
(( com.android.server.am.ProcessPowerCleaner$H ) v2 ).obtainMessage ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/am/ProcessPowerCleaner$H;->obtainMessage(I)Landroid/os/Message;
/* .line 269 */
/* .local v2, "message":Landroid/os/Message; */
v3 = this.mHandler;
/* iget v4, p0, Lcom/android/server/am/ProcessPowerCleaner;->mCheckCPUTime:I */
/* int-to-long v4, v4 */
(( com.android.server.am.ProcessPowerCleaner$H ) v3 ).sendMessageDelayed ( v2, v4, v5 ); // invoke-virtual {v3, v2, v4, v5}, Lcom/android/server/am/ProcessPowerCleaner$H;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 270 */
/* iget v3, p0, Lcom/android/server/am/ProcessPowerCleaner;->mActiveProcessThreshold:F */
/* const/high16 v4, 0x3f400000 # 0.75f */
/* mul-float/2addr v3, v4 */
v3 = java.lang.Math .max ( v3,v4 );
/* iput v3, p0, Lcom/android/server/am/ProcessPowerCleaner;->mActiveProcessThreshold:F */
/* .line 272 */
/* iget v3, p0, Lcom/android/server/am/ProcessPowerCleaner;->mCheckCPUTime:I */
/* mul-int/lit8 v3, v3, 0x2 */
/* const v4, 0x36ee80 */
v3 = java.lang.Math .min ( v3,v4 );
/* iput v3, p0, Lcom/android/server/am/ProcessPowerCleaner;->mCheckCPUTime:I */
/* .line 273 */
return;
} // .end method
public Boolean powerKillProcess ( miui.process.ProcessConfig p0 ) {
/* .locals 3 */
/* .param p1, "config" # Lmiui/process/ProcessConfig; */
/* .line 144 */
v0 = this.mHandler;
if ( v0 != null) { // if-eqz v0, :cond_1
/* if-nez p1, :cond_0 */
/* .line 147 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 148 */
/* .local v0, "success":Z */
v1 = (( miui.process.ProcessConfig ) p1 ).getPolicy ( ); // invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getPolicy()I
/* packed-switch v1, :pswitch_data_0 */
/* :pswitch_0 */
/* .line 150 */
/* :pswitch_1 */
v1 = this.mHandler;
int v2 = 1; // const/4 v2, 0x1
(( com.android.server.am.ProcessPowerCleaner ) p0 ).createMessage ( v2, p1, v1 ); // invoke-virtual {p0, v2, p1, v1}, Lcom/android/server/am/ProcessPowerCleaner;->createMessage(ILmiui/process/ProcessConfig;Landroid/os/Handler;)Landroid/os/Message;
(( com.android.server.am.ProcessPowerCleaner$H ) v1 ).sendMessage ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessPowerCleaner$H;->sendMessage(Landroid/os/Message;)Z
/* .line 151 */
int v0 = 1; // const/4 v0, 0x1
/* .line 152 */
/* .line 161 */
/* :pswitch_2 */
v1 = this.mHandler;
int v2 = 2; // const/4 v2, 0x2
(( com.android.server.am.ProcessPowerCleaner ) p0 ).createMessage ( v2, p1, v1 ); // invoke-virtual {p0, v2, p1, v1}, Lcom/android/server/am/ProcessPowerCleaner;->createMessage(ILmiui/process/ProcessConfig;Landroid/os/Handler;)Landroid/os/Message;
(( com.android.server.am.ProcessPowerCleaner$H ) v1 ).sendMessage ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessPowerCleaner$H;->sendMessage(Landroid/os/Message;)Z
/* .line 162 */
/* .line 156 */
/* :pswitch_3 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessPowerCleaner;->handleKillApp(Lmiui/process/ProcessConfig;)Z */
/* .line 157 */
/* nop */
/* .line 166 */
} // :goto_0
/* .line 145 */
} // .end local v0 # "success":Z
} // :cond_1
} // :goto_1
int v0 = 0; // const/4 v0, 0x0
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0xb */
/* :pswitch_3 */
/* :pswitch_3 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_0 */
/* :pswitch_2 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_1 */
/* :pswitch_2 */
} // .end packed-switch
} // .end method
public void setLockOffCleanTestEnable ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "lockOffCleanTestEnable" # Z */
/* .line 341 */
/* iput-boolean p1, p0, Lcom/android/server/am/ProcessPowerCleaner;->mLockOffCleanTestEnable:Z */
/* .line 342 */
return;
} // .end method
public void systemReady ( android.content.Context p0, com.android.server.am.ProcessManagerService p1, android.os.Looper p2 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "pms" # Lcom/android/server/am/ProcessManagerService; */
/* .param p3, "myLooper" # Landroid/os/Looper; */
/* .line 71 */
/* invoke-super {p0, p1, p2}, Lcom/android/server/am/ProcessCleanerBase;->systemReady(Landroid/content/Context;Lcom/android/server/am/ProcessManagerService;)V */
/* .line 72 */
this.mPMS = p2;
/* .line 73 */
(( com.android.server.am.ProcessManagerService ) p2 ).getProcessPolicy ( ); // invoke-virtual {p2}, Lcom/android/server/am/ProcessManagerService;->getProcessPolicy()Lcom/android/server/am/ProcessPolicy;
this.mProcessPolicy = v0;
/* .line 74 */
com.android.server.am.SystemPressureController .getInstance ( );
this.mSysPressureCtrl = v0;
/* .line 75 */
this.mContext = p1;
/* .line 76 */
/* const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
this.mSmartPowerService = v0;
/* .line 78 */
/* new-instance v0, Lcom/android/server/am/ProcessPowerCleaner$ScreenStatusReceiver; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/android/server/am/ProcessPowerCleaner$ScreenStatusReceiver;-><init>(Lcom/android/server/am/ProcessPowerCleaner;Lcom/android/server/am/ProcessPowerCleaner$ScreenStatusReceiver-IA;)V */
this.mScreenStatusReceiver = v0;
/* .line 79 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 80 */
/* .local v0, "screenStatus":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.SCREEN_ON"; // const-string v1, "android.intent.action.SCREEN_ON"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 81 */
final String v1 = "android.intent.action.SCREEN_OFF"; // const-string v1, "android.intent.action.SCREEN_OFF"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 82 */
v1 = this.mScreenStatusReceiver;
(( android.content.Context ) p1 ).registerReceiver ( v1, v0 ); // invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 84 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessPowerCleaner;->registerCloudObserver(Landroid/content/Context;)V */
/* .line 85 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessPowerCleaner;->updateCloudControlParas(Landroid/content/Context;)V */
/* .line 87 */
/* new-instance v1, Lcom/android/server/am/ProcessPowerCleaner$H; */
/* invoke-direct {v1, p0, p3}, Lcom/android/server/am/ProcessPowerCleaner$H;-><init>(Lcom/android/server/am/ProcessPowerCleaner;Landroid/os/Looper;)V */
this.mHandler = v1;
/* .line 88 */
return;
} // .end method
