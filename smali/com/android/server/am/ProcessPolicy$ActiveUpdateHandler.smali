.class Lcom/android/server/am/ProcessPolicy$ActiveUpdateHandler;
.super Landroid/os/Handler;
.source "ProcessPolicy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ProcessPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ActiveUpdateHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ProcessPolicy;


# direct methods
.method public constructor <init>(Lcom/android/server/am/ProcessPolicy;Landroid/os/Looper;)V
    .locals 2
    .param p1, "this$0"    # Lcom/android/server/am/ProcessPolicy;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 295
    iput-object p1, p0, Lcom/android/server/am/ProcessPolicy$ActiveUpdateHandler;->this$0:Lcom/android/server/am/ProcessPolicy;

    .line 296
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p2, v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;Z)V

    .line 297
    return-void
.end method

.method private checkRemoveActiveUid(Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;I)V
    .locals 4
    .param p1, "uidRecord"    # Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;
    .param p2, "flag"    # I

    .line 316
    if-eqz p1, :cond_1

    .line 317
    invoke-static {}, Lcom/android/server/am/ProcessPolicy;->-$$Nest$sfgetsLock()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 318
    packed-switch p2, :pswitch_data_0

    .line 326
    :try_start_0
    monitor-exit v0

    goto :goto_1

    .line 323
    :pswitch_0
    invoke-static {}, Lcom/android/server/am/ProcessPolicy;->-$$Nest$sfgetsTempInactiveGPSList()Landroid/util/SparseArray;

    move-result-object v1

    iget v2, p1, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->uid:I

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->remove(I)V

    .line 324
    goto :goto_0

    .line 320
    :pswitch_1
    invoke-static {}, Lcom/android/server/am/ProcessPolicy;->-$$Nest$sfgetsTempInactiveAudioList()Landroid/util/SparseArray;

    move-result-object v1

    iget v2, p1, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->uid:I

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->remove(I)V

    .line 321
    nop

    .line 329
    :goto_0
    iget v1, p1, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->flag:I

    not-int v2, p2

    and-int/2addr v1, v2

    iput v1, p1, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->flag:I

    .line 330
    iget v1, p1, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->flag:I

    if-nez v1, :cond_0

    .line 331
    invoke-static {}, Lcom/android/server/am/ProcessPolicy;->-$$Nest$sfgetsActiveUidList()Landroid/util/SparseArray;

    move-result-object v1

    iget v2, p1, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->uid:I

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->remove(I)V

    .line 333
    const-string v1, "ProcessManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "real remove inactive uid : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->uid:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " flag : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    :cond_0
    monitor-exit v0

    goto :goto_3

    :catchall_0
    move-exception v1

    goto :goto_2

    .line 326
    :goto_1
    return-void

    .line 336
    :goto_2
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 338
    :cond_1
    :goto_3
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .line 301
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;

    .line 302
    .local v0, "uidRecord":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 307
    :pswitch_0
    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lcom/android/server/am/ProcessPolicy$ActiveUpdateHandler;->checkRemoveActiveUid(Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;I)V

    .line 308
    goto :goto_0

    .line 304
    :pswitch_1
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/android/server/am/ProcessPolicy$ActiveUpdateHandler;->checkRemoveActiveUid(Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;I)V

    .line 305
    nop

    .line 313
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
