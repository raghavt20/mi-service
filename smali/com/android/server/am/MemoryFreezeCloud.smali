.class public Lcom/android/server/am/MemoryFreezeCloud;
.super Ljava/lang/Object;
.source "MemoryFreezeCloud.java"


# static fields
.field private static final CLOUD_MEMFREEZE_POLICY:Ljava/lang/String; = "cloud_memory_freeze_policy"

.field private static final CLOUD_MEMFREEZE_WHITE_LIST:Ljava/lang/String; = "cloud_memory_freeze_whitelist"

.field private static final FILE_PATH:Ljava/lang/String; = "/data/system/mfz.xml"

.field private static final MEMFREEZE_POLICY_PROP:Ljava/lang/String; = "persist.sys.mfz.mem_policy"

.field private static final TAG:Ljava/lang/String; = "MFZCloud"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mMemoryFreezeStubImpl:Lcom/android/server/am/MemoryFreezeStubImpl;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/am/MemoryFreezeCloud;->mMemoryFreezeStubImpl:Lcom/android/server/am/MemoryFreezeStubImpl;

    .line 51
    iput-object v0, p0, Lcom/android/server/am/MemoryFreezeCloud;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public getTotalPhysicalRam()I
    .locals 8

    .line 131
    :try_start_0
    const-string v0, "miui.util.HardwareInfo"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 132
    .local v0, "clazz":Ljava/lang/Class;
    const-string v1, "getTotalPhysicalMemory"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 133
    .local v1, "method":Ljava/lang/reflect/Method;
    invoke-virtual {v1, v2, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 134
    .local v2, "totalPhysicalMemory":J
    long-to-double v4, v2

    const-wide/high16 v6, 0x4090000000000000L    # 1024.0

    div-double/2addr v4, v6

    div-double/2addr v4, v6

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    double-to-int v4, v4

    .line 135
    .local v4, "memory_G":I
    return v4

    .line 136
    .end local v0    # "clazz":Ljava/lang/Class;
    .end local v1    # "method":Ljava/lang/reflect/Method;
    .end local v2    # "totalPhysicalMemory":J
    .end local v4    # "memory_G":I
    :catch_0
    move-exception v0

    .line 137
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MFZCloud"

    const-string v2, "Mobile Memory not found!"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    .end local v0    # "e":Ljava/lang/Exception;
    const/4 v0, -0x1

    return v0
.end method

.method public initCloud(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 54
    invoke-static {}, Lcom/android/server/am/MemoryFreezeStubImpl;->getInstance()Lcom/android/server/am/MemoryFreezeStubImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/MemoryFreezeCloud;->mMemoryFreezeStubImpl:Lcom/android/server/am/MemoryFreezeStubImpl;

    .line 55
    iput-object p1, p0, Lcom/android/server/am/MemoryFreezeCloud;->mContext:Landroid/content/Context;

    .line 56
    const-string v1, "MFZCloud"

    if-nez v0, :cond_0

    .line 57
    const-string v0, "initCloud failure"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    :cond_0
    const-string v0, "initCloud success"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    return-void
.end method

.method public registerCloudWhiteList(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 63
    new-instance v0, Lcom/android/server/am/MemoryFreezeCloud$1;

    iget-object v1, p0, Lcom/android/server/am/MemoryFreezeCloud;->mMemoryFreezeStubImpl:Lcom/android/server/am/MemoryFreezeStubImpl;

    iget-object v1, v1, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/am/MemoryFreezeCloud$1;-><init>(Lcom/android/server/am/MemoryFreezeCloud;Landroid/os/Handler;)V

    .line 71
    .local v0, "observer":Landroid/database/ContentObserver;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 72
    const-string v2, "cloud_memory_freeze_whitelist"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 71
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 73
    return-void
.end method

.method public registerMemfreezeOperation(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 76
    new-instance v0, Lcom/android/server/am/MemoryFreezeCloud$2;

    iget-object v1, p0, Lcom/android/server/am/MemoryFreezeCloud;->mMemoryFreezeStubImpl:Lcom/android/server/am/MemoryFreezeStubImpl;

    iget-object v1, v1, Lcom/android/server/am/MemoryFreezeStubImpl;->mHandler:Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/am/MemoryFreezeCloud$2;-><init>(Lcom/android/server/am/MemoryFreezeCloud;Landroid/os/Handler;)V

    .line 85
    .local v0, "observer":Landroid/database/ContentObserver;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 86
    const-string v2, "cloud_memory_freeze_policy"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 85
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 87
    return-void
.end method

.method public saveToXml(Ljava/lang/String;)V
    .locals 13
    .param p1, "operations"    # Ljava/lang/String;

    .line 90
    const-string v0, "designated-package"

    const-string v1, "contents"

    const-string v2, "UTF-8"

    const-string v3, "\n"

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    const-string v5, "MFZCloud"

    if-eqz v4, :cond_0

    .line 91
    const-string v0, "saveToXml operations is empty !"

    invoke-static {v5, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    return-void

    .line 94
    :cond_0
    new-instance v4, Ljava/io/File;

    const-string v6, "/data/system/mfz.xml"

    invoke-direct {v4, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 95
    .local v4, "file":Ljava/io/File;
    const/4 v6, 0x0

    .line 96
    .local v6, "out":Ljava/io/OutputStream;
    const-string v7, ","

    invoke-virtual {p1, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 98
    .local v7, "operation":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Landroid/util/Xml;->newSerializer()Lorg/xmlpull/v1/XmlSerializer;

    move-result-object v8

    .line 99
    .local v8, "serializer":Lorg/xmlpull/v1/XmlSerializer;
    new-instance v9, Ljava/io/FileOutputStream;

    invoke-direct {v9, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    move-object v6, v9

    .line 100
    invoke-interface {v8, v6, v2}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 101
    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-interface {v8, v2, v9}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 102
    invoke-interface {v8, v3}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 103
    const/4 v2, 0x0

    invoke-interface {v8, v2, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 104
    array-length v9, v7

    const/4 v10, 0x0

    :goto_0
    if-ge v10, v9, :cond_1

    aget-object v11, v7, v10

    .line 105
    .local v11, "str":Ljava/lang/String;
    const-string v12, "\n\t"

    invoke-interface {v8, v12}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 106
    invoke-interface {v8, v2, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 107
    const-string v12, "name"

    invoke-interface {v8, v2, v12, v11}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 108
    invoke-interface {v8, v2, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 104
    nop

    .end local v11    # "str":Ljava/lang/String;
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 110
    :cond_1
    invoke-interface {v8, v3}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 111
    invoke-interface {v8, v2, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 112
    invoke-interface {v8, v3}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 113
    invoke-interface {v8}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 114
    invoke-virtual {v6}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119
    .end local v8    # "serializer":Lorg/xmlpull/v1/XmlSerializer;
    nop

    .line 120
    :try_start_1
    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 124
    :cond_2
    :goto_1
    goto :goto_2

    .line 122
    :catch_0
    move-exception v0

    .line 123
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 125
    .end local v0    # "e":Ljava/lang/Exception;
    goto :goto_2

    .line 118
    :catchall_0
    move-exception v0

    goto :goto_3

    .line 115
    :catch_1
    move-exception v0

    .line 116
    .restart local v0    # "e":Ljava/lang/Exception;
    :try_start_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Save xml error"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 119
    .end local v0    # "e":Ljava/lang/Exception;
    if-eqz v6, :cond_2

    .line 120
    :try_start_3
    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    .line 127
    :goto_2
    return-void

    .line 119
    :goto_3
    if-eqz v6, :cond_3

    .line 120
    :try_start_4
    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_4

    .line 122
    :catch_2
    move-exception v1

    .line 123
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    .line 124
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_3
    :goto_4
    nop

    .line 125
    :goto_5
    throw v0
.end method

.method public setParameter(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "operations"    # Ljava/lang/String;

    .line 143
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    const-string v0, "MFZCloud"

    const-string/jumbo v1, "setParameter operations is empty , set as default value!"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    const-string v0, "2048,3072,4096"

    return-object v0

    .line 147
    :cond_0
    const-string v0, "\""

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 148
    const-string v0, "\\},"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 149
    .local v0, "arr":[Ljava/lang/String;
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 150
    .local v2, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v3, 0x0

    aget-object v4, v0, v3

    aget-object v5, v0, v3

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x1

    invoke-virtual {v4, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v3

    .line 151
    array-length v4, v0

    sub-int/2addr v4, v6

    array-length v5, v0

    sub-int/2addr v5, v6

    aget-object v5, v0, v5

    array-length v7, v0

    sub-int/2addr v7, v6

    aget-object v7, v0, v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x2

    invoke-virtual {v5, v3, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v4

    .line 152
    array-length v4, v0

    move v5, v3

    :goto_0
    if-ge v5, v4, :cond_1

    aget-object v7, v0, v5

    .line 153
    .local v7, "s":Ljava/lang/String;
    const-string v8, "\\}"

    invoke-virtual {v7, v8, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    .line 154
    const-string v8, ":\\{"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    aget-object v9, v9, v3

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    aget-object v8, v8, v6

    invoke-interface {v2, v9, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    .end local v7    # "s":Ljava/lang/String;
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 156
    :cond_1
    invoke-virtual {p0}, Lcom/android/server/am/MemoryFreezeCloud;->getTotalPhysicalRam()I

    move-result v1

    const-string v3, ":"

    sparse-switch v1, :sswitch_data_0

    .line 173
    const-string v1, "default"

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v6

    return-object v1

    .line 170
    :sswitch_0
    const-string v1, "24"

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v6

    return-object v1

    .line 167
    :sswitch_1
    const-string v1, "16"

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v6

    return-object v1

    .line 164
    :sswitch_2
    const-string v1, "12"

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v6

    return-object v1

    .line 161
    :sswitch_3
    const-string v1, "8"

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v6

    return-object v1

    .line 158
    :sswitch_4
    const-string v1, "6"

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v6

    return-object v1

    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_4
        0x8 -> :sswitch_3
        0xc -> :sswitch_2
        0x10 -> :sswitch_1
        0x18 -> :sswitch_0
    .end sparse-switch
.end method

.method public updateCloudWhiteList()V
    .locals 3

    .line 179
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeCloud;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "cloud_memory_freeze_whitelist"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeCloud;->mContext:Landroid/content/Context;

    .line 182
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 181
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 185
    .local v0, "enableStr":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 186
    invoke-virtual {p0, v0}, Lcom/android/server/am/MemoryFreezeCloud;->saveToXml(Ljava/lang/String;)V

    .line 189
    .end local v0    # "enableStr":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public updateMemfreezeOperation()V
    .locals 3

    .line 192
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeCloud;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "cloud_memory_freeze_policy"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeCloud;->mContext:Landroid/content/Context;

    .line 195
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 194
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 198
    .local v0, "policy":Ljava/lang/String;
    const-string v1, "persist.sys.mfz.mem_policy"

    invoke-virtual {p0, v0}, Lcom/android/server/am/MemoryFreezeCloud;->setParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    .end local v0    # "policy":Ljava/lang/String;
    :cond_0
    return-void
.end method
