class com.android.server.am.ProcessManagerService$3 implements java.lang.Runnable {
	 /* .source "ProcessManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/am/ProcessManagerService;->notifyForegroundInfoChanged(Lcom/android/server/wm/FgActivityChangedInfo;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.am.ProcessManagerService this$0; //synthetic
final com.android.server.wm.FgActivityChangedInfo val$fgInfo; //synthetic
/* # direct methods */
 com.android.server.am.ProcessManagerService$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/ProcessManagerService; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 1017 */
this.this$0 = p1;
this.val$fgInfo = p2;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 2 */
/* .line 1020 */
v0 = this.this$0;
com.android.server.am.ProcessManagerService .-$$Nest$fgetmForegroundInfoManager ( v0 );
v1 = this.val$fgInfo;
(( com.android.server.wm.ForegroundInfoManager ) v0 ).notifyForegroundInfoChanged ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/ForegroundInfoManager;->notifyForegroundInfoChanged(Lcom/android/server/wm/FgActivityChangedInfo;)V
/* .line 1021 */
return;
} // .end method
