.class public final Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
.super Ljava/lang/Object;
.source "ProcessProphetModel.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ProcessProphetModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PkgValuePair"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Comparable<",
        "Lcom/android/server/am/ProcessProphetModel$PkgValuePair;",
        ">;"
    }
.end annotation


# instance fields
.field pkgName:Ljava/lang/String;

.field value:D


# direct methods
.method constructor <init>(Ljava/lang/String;D)V
    .locals 2
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "value"    # D

    .line 543
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 541
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;->pkgName:Ljava/lang/String;

    .line 542
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;->value:D

    .line 544
    iput-object p1, p0, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;->pkgName:Ljava/lang/String;

    .line 545
    iput-wide p2, p0, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;->value:D

    .line 546
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/android/server/am/ProcessProphetModel$PkgValuePair;)I
    .locals 5
    .param p1, "p"    # Lcom/android/server/am/ProcessProphetModel$PkgValuePair;

    .line 549
    iget-wide v0, p0, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;->value:D

    iget-wide v2, p1, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;->value:D

    cmpl-double v4, v0, v2

    if-lez v4, :cond_0

    .line 550
    const/4 v0, -0x1

    return v0

    .line 551
    :cond_0
    cmpg-double v0, v0, v2

    if-gez v0, :cond_1

    .line 552
    const/4 v0, 0x1

    return v0

    .line 554
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 540
    check-cast p1, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;

    invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;->compareTo(Lcom/android/server/am/ProcessProphetModel$PkgValuePair;)I

    move-result p1

    return p1
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 559
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;->pkgName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/text/DecimalFormat;

    const-string v2, "#.####"

    invoke-direct {v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;->value:D

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
