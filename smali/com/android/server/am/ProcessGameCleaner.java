public class com.android.server.am.ProcessGameCleaner {
	 /* .source "ProcessGameCleaner.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/am/ProcessGameCleaner$H; */
	 /* } */
} // .end annotation
/* # static fields */
private static Integer CLOD_START_FOR_GAME_DALAYED;
private static Integer HOT_START_FOR_GAME_DALAYED;
private static final java.lang.String TAG;
/* # instance fields */
private com.android.server.am.ActivityManagerService mAms;
private com.android.server.am.ProcessGameCleaner$H mHandler;
private java.lang.reflect.Method mMethodUpdateOomForGame;
private com.android.server.am.ProcessManagerService mPms;
private com.android.server.am.SystemPressureController mSystemPressureCtl;
/* # direct methods */
static void -$$Nest$muploadLmkdOomForGame ( com.android.server.am.ProcessGameCleaner p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessGameCleaner;->uploadLmkdOomForGame(Z)V */
	 return;
} // .end method
static com.android.server.am.ProcessGameCleaner ( ) {
	 /* .locals 1 */
	 /* .line 18 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 19 */
	 /* const/16 v0, 0x7530 */
	 return;
} // .end method
public com.android.server.am.ProcessGameCleaner ( ) {
	 /* .locals 0 */
	 /* .param p1, "ams" # Lcom/android/server/am/ActivityManagerService; */
	 /* .line 27 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 28 */
	 this.mAms = p1;
	 /* .line 29 */
	 return;
} // .end method
private Boolean isColdStartApp ( Integer p0 ) {
	 /* .locals 5 */
	 /* .param p1, "pid" # I */
	 /* .line 58 */
	 v0 = this.mPms;
	 (( com.android.server.am.ProcessManagerService ) v0 ).getProcessRecordByPid ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;
	 /* .line 59 */
	 /* .local v0, "proc":Lcom/android/server/am/ProcessRecord; */
	 v1 = this.mState;
	 (( com.android.server.am.ProcessStateRecord ) v1 ).getLastTopTime ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessStateRecord;->getLastTopTime()J
	 /* move-result-wide v1 */
	 /* const-wide/16 v3, 0x0 */
	 /* cmp-long v1, v1, v3 */
	 /* if-nez v1, :cond_0 */
	 /* .line 60 */
	 int v1 = 1; // const/4 v1, 0x1
	 /* .line 62 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
private void uploadLmkdOomForGame ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "isEnable" # Z */
/* .line 67 */
try { // :try_start_0
	 v0 = this.mMethodUpdateOomForGame;
	 v1 = this.mAms;
	 int v2 = 1; // const/4 v2, 0x1
	 /* new-array v2, v2, [Ljava/lang/Object; */
	 java.lang.Boolean .valueOf ( p1 );
	 int v4 = 0; // const/4 v4, 0x0
	 /* aput-object v3, v2, v4 */
	 (( java.lang.reflect.Method ) v0 ).invoke ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 70 */
	 /* .line 68 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 69 */
	 /* .local v0, "e":Ljava/lang/Exception; */
	 final String v1 = "ProcessGameCleaner"; // const-string v1, "ProcessGameCleaner"
	 final String v2 = "game oom update error!"; // const-string v2, "game oom update error!"
	 android.util.Slog .w ( v1,v2 );
	 /* .line 71 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void foregroundInfoChanged ( java.lang.String p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "pckName" # Ljava/lang/String; */
/* .param p2, "pid" # I */
/* .line 40 */
v0 = this.mMethodUpdateOomForGame;
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = this.mAms;
/* if-nez v0, :cond_0 */
/* .line 43 */
} // :cond_0
v0 = this.mHandler;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.am.ProcessGameCleaner$H ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessGameCleaner$H;->removeMessages(I)V
/* .line 44 */
v0 = this.mSystemPressureCtl;
v0 = (( com.android.server.am.SystemPressureController ) v0 ).isGameApp ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/SystemPressureController;->isGameApp(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 45 */
v0 = this.mHandler;
(( com.android.server.am.ProcessGameCleaner$H ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessGameCleaner$H;->obtainMessage(I)Landroid/os/Message;
/* .line 46 */
/* .local v0, "msg":Landroid/os/Message; */
v2 = /* invoke-direct {p0, p2}, Lcom/android/server/am/ProcessGameCleaner;->isColdStartApp(I)Z */
/* .line 47 */
/* .local v2, "isClodApp":Z */
java.lang.Boolean .valueOf ( v1 );
this.obj = v1;
/* .line 48 */
v1 = this.mHandler;
/* .line 49 */
if ( v2 != null) { // if-eqz v2, :cond_1
} // :cond_1
} // :goto_0
/* int-to-long v3, v3 */
/* .line 48 */
(( com.android.server.am.ProcessGameCleaner$H ) v1 ).sendMessageDelayed ( v0, v3, v4 ); // invoke-virtual {v1, v0, v3, v4}, Lcom/android/server/am/ProcessGameCleaner$H;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 50 */
} // .end local v0 # "msg":Landroid/os/Message;
} // .end local v2 # "isClodApp":Z
/* .line 51 */
} // :cond_2
v0 = this.mHandler;
(( com.android.server.am.ProcessGameCleaner$H ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessGameCleaner$H;->obtainMessage(I)Landroid/os/Message;
/* .line 52 */
/* .restart local v0 # "msg":Landroid/os/Message; */
int v1 = 0; // const/4 v1, 0x0
java.lang.Boolean .valueOf ( v1 );
this.obj = v1;
/* .line 53 */
v1 = this.mHandler;
(( com.android.server.am.ProcessGameCleaner$H ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/am/ProcessGameCleaner$H;->sendMessage(Landroid/os/Message;)Z
/* .line 55 */
} // .end local v0 # "msg":Landroid/os/Message;
} // :goto_1
return;
/* .line 41 */
} // :cond_3
} // :goto_2
return;
} // .end method
public void onSystemReady ( com.android.server.am.ProcessManagerService p0, android.os.Looper p1 ) {
/* .locals 3 */
/* .param p1, "pms" # Lcom/android/server/am/ProcessManagerService; */
/* .param p2, "myLooper" # Landroid/os/Looper; */
/* .line 32 */
v0 = this.mAms;
/* .line 33 */
(( java.lang.Object ) v0 ).getClass ( ); // invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
v1 = java.lang.Boolean.TYPE;
/* filled-new-array {v1}, [Ljava/lang/Class; */
/* .line 32 */
/* const-string/jumbo v2, "updateOomForGame" */
miui.util.ReflectionUtils .tryFindMethodExact ( v0,v2,v1 );
this.mMethodUpdateOomForGame = v0;
/* .line 34 */
com.android.server.am.SystemPressureController .getInstance ( );
this.mSystemPressureCtl = v0;
/* .line 35 */
this.mPms = p1;
/* .line 36 */
/* new-instance v0, Lcom/android/server/am/ProcessGameCleaner$H; */
/* invoke-direct {v0, p0, p2}, Lcom/android/server/am/ProcessGameCleaner$H;-><init>(Lcom/android/server/am/ProcessGameCleaner;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 37 */
return;
} // .end method
