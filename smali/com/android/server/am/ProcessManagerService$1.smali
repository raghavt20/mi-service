.class Lcom/android/server/am/ProcessManagerService$1;
.super Ljava/lang/Object;
.source "ProcessManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/ProcessManagerService;->reduceRecordCountDelay(Ljava/lang/String;Ljava/util/Map;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ProcessManagerService;

.field final synthetic val$processName:Ljava/lang/String;

.field final synthetic val$recordMap:Ljava/util/Map;


# direct methods
.method constructor <init>(Lcom/android/server/am/ProcessManagerService;Ljava/util/Map;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/ProcessManagerService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 783
    iput-object p1, p0, Lcom/android/server/am/ProcessManagerService$1;->this$0:Lcom/android/server/am/ProcessManagerService;

    iput-object p2, p0, Lcom/android/server/am/ProcessManagerService$1;->val$recordMap:Ljava/util/Map;

    iput-object p3, p0, Lcom/android/server/am/ProcessManagerService$1;->val$processName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 786
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$1;->val$recordMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService$1;->val$processName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 787
    .local v0, "count":Ljava/lang/Integer;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gtz v1, :cond_0

    goto :goto_1

    .line 790
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 791
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gtz v1, :cond_1

    .line 792
    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService$1;->val$recordMap:Ljava/util/Map;

    iget-object v2, p0, Lcom/android/server/am/ProcessManagerService$1;->val$processName:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 794
    :cond_1
    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService$1;->val$recordMap:Ljava/util/Map;

    iget-object v2, p0, Lcom/android/server/am/ProcessManagerService$1;->val$processName:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 796
    :goto_0
    return-void

    .line 788
    :cond_2
    :goto_1
    return-void
.end method
