.class Lcom/android/server/am/PeriodicCleanerService$CleanInfo;
.super Ljava/lang/Object;
.source "PeriodicCleanerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/PeriodicCleanerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CleanInfo"
.end annotation


# instance fields
.field private mAgingThresHold:I

.field private mCleanList:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mCleanTime:J

.field private mPressure:I

.field private mReason:Ljava/lang/String;

.field private mValid:Z

.field final synthetic this$0:Lcom/android/server/am/PeriodicCleanerService;


# direct methods
.method static bridge synthetic -$$Nest$fgetmAgingThresHold(Lcom/android/server/am/PeriodicCleanerService$CleanInfo;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->mAgingThresHold:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmCleanList(Lcom/android/server/am/PeriodicCleanerService$CleanInfo;)Landroid/util/SparseArray;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->mCleanList:Landroid/util/SparseArray;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmCleanTime(Lcom/android/server/am/PeriodicCleanerService$CleanInfo;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->mCleanTime:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmPressure(Lcom/android/server/am/PeriodicCleanerService$CleanInfo;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->mPressure:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmReason(Lcom/android/server/am/PeriodicCleanerService$CleanInfo;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->mReason:Ljava/lang/String;

    return-object p0
.end method

.method constructor <init>(Lcom/android/server/am/PeriodicCleanerService;JIILjava/lang/String;)V
    .locals 0
    .param p2, "currentTime"    # J
    .param p4, "pressure"    # I
    .param p5, "agingThresHold"    # I
    .param p6, "reason"    # Ljava/lang/String;

    .line 1964
    iput-object p1, p0, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->this$0:Lcom/android/server/am/PeriodicCleanerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1961
    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->mValid:Z

    .line 1962
    new-instance p1, Landroid/util/SparseArray;

    invoke-direct {p1}, Landroid/util/SparseArray;-><init>()V

    iput-object p1, p0, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->mCleanList:Landroid/util/SparseArray;

    .line 1965
    iput-wide p2, p0, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->mCleanTime:J

    .line 1966
    iput p4, p0, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->mPressure:I

    .line 1967
    iput p5, p0, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->mAgingThresHold:I

    .line 1968
    iput-object p6, p0, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->mReason:Ljava/lang/String;

    .line 1969
    return-void
.end method


# virtual methods
.method public addCleanList(ILjava/util/List;)V
    .locals 2
    .param p1, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1972
    .local p2, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->mCleanList:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1973
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Already exists old mapping for user "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PeriodicCleaner"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1975
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->mCleanList:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1976
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->mValid:Z

    .line 1977
    return-void
.end method

.method public isValid()Z
    .locals 1

    .line 1980
    iget-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->mValid:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1984
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->mCleanTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->mCleanList:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
