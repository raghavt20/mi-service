.class Lcom/android/server/am/ProcessGameCleaner$H;
.super Landroid/os/Handler;
.source "ProcessGameCleaner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ProcessGameCleaner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "H"
.end annotation


# static fields
.field static final MSG_MEMORY_CLEAN_FOR_GAME:I = 0x1


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ProcessGameCleaner;


# direct methods
.method public constructor <init>(Lcom/android/server/am/ProcessGameCleaner;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 77
    iput-object p1, p0, Lcom/android/server/am/ProcessGameCleaner$H;->this$0:Lcom/android/server/am/ProcessGameCleaner;

    .line 78
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 79
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .line 83
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 84
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 86
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/am/ProcessGameCleaner$H;->this$0:Lcom/android/server/am/ProcessGameCleaner;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/am/ProcessGameCleaner;->-$$Nest$muploadLmkdOomForGame(Lcom/android/server/am/ProcessGameCleaner;Z)V

    .line 87
    nop

    .line 91
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
