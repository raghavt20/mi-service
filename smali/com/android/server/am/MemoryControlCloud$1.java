class com.android.server.am.MemoryControlCloud$1 extends android.database.ContentObserver {
	 /* .source "MemoryControlCloud.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/am/MemoryControlCloud;->registerCloudEnableObserver(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.am.MemoryControlCloud this$0; //synthetic
final android.content.Context val$context; //synthetic
/* # direct methods */
 com.android.server.am.MemoryControlCloud$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/MemoryControlCloud; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 39 */
this.this$0 = p1;
this.val$context = p3;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 6 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 42 */
final String v0 = "MemoryStandardProcessControl"; // const-string v0, "MemoryStandardProcessControl"
if ( p2 != null) { // if-eqz p2, :cond_2
	 /* .line 43 */
	 final String v1 = "cloud_memory_standard_enable"; // const-string v1, "cloud_memory_standard_enable"
	 android.provider.Settings$System .getUriFor ( v1 );
	 v2 = 	 (( android.net.Uri ) p2 ).equals ( v2 ); // invoke-virtual {p2, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
	 if ( v2 != null) { // if-eqz v2, :cond_2
		 /* .line 44 */
		 v2 = this.val$context;
		 (( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
		 int v3 = -2; // const/4 v3, -0x2
		 android.provider.Settings$System .getStringForUser ( v2,v1,v3 );
		 /* .line 46 */
		 /* .local v2, "str":Ljava/lang/String; */
		 if ( v2 != null) { // if-eqz v2, :cond_1
			 final String v4 = ""; // const-string v4, ""
			 v4 = 			 (( java.lang.String ) v2 ).equals ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 if ( v4 != null) { // if-eqz v4, :cond_0
				 /* .line 50 */
			 } // :cond_0
			 try { // :try_start_0
				 v4 = this.this$0;
				 com.android.server.am.MemoryControlCloud .-$$Nest$fgetmspc ( v4 );
				 v5 = this.val$context;
				 /* .line 51 */
				 (( android.content.Context ) v5 ).getContentResolver ( ); // invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
				 android.provider.Settings$System .getStringForUser ( v5,v1,v3 );
				 /* .line 50 */
				 v1 = 				 java.lang.Boolean .parseBoolean ( v1 );
				 /* iput-boolean v1, v4, Lcom/android/server/am/MemoryStandardProcessControl;->mEnable:Z */
				 /* :try_end_0 */
				 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
				 /* .line 56 */
				 /* .line 53 */
				 /* :catch_0 */
				 /* move-exception v1 */
				 /* .line 54 */
				 /* .local v1, "e":Ljava/lang/Exception; */
				 /* new-instance v3, Ljava/lang/StringBuilder; */
				 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
				 final String v4 = "cloud CLOUD_MEMORY_STANDARD_ENABLE check failed: "; // const-string v4, "cloud CLOUD_MEMORY_STANDARD_ENABLE check failed: "
				 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
				 android.util.Slog .e ( v0,v3 );
				 /* .line 55 */
				 v3 = this.this$0;
				 com.android.server.am.MemoryControlCloud .-$$Nest$fgetmspc ( v3 );
				 int v4 = 0; // const/4 v4, 0x0
				 /* iput-boolean v4, v3, Lcom/android/server/am/MemoryStandardProcessControl;->mEnable:Z */
				 /* .line 57 */
			 } // .end local v1 # "e":Ljava/lang/Exception;
		 } // :goto_0
		 /* new-instance v1, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v3 = "cloud control set received: "; // const-string v3, "cloud control set received: "
		 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v3 = this.this$0;
		 com.android.server.am.MemoryControlCloud .-$$Nest$fgetmspc ( v3 );
		 /* iget-boolean v3, v3, Lcom/android/server/am/MemoryStandardProcessControl;->mEnable:Z */
		 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Slog .w ( v0,v1 );
		 /* .line 47 */
	 } // :cond_1
} // :goto_1
return;
/* .line 59 */
} // .end local v2 # "str":Ljava/lang/String;
} // :cond_2
} // :goto_2
return;
} // .end method
