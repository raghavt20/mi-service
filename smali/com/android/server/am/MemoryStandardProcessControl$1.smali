.class Lcom/android/server/am/MemoryStandardProcessControl$1;
.super Landroid/content/BroadcastReceiver;
.source "MemoryStandardProcessControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/MemoryStandardProcessControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/MemoryStandardProcessControl;


# direct methods
.method constructor <init>(Lcom/android/server/am/MemoryStandardProcessControl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/MemoryStandardProcessControl;

    .line 794
    iput-object p1, p0, Lcom/android/server/am/MemoryStandardProcessControl$1;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 797
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 798
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eqz v1, :cond_0

    .line 799
    iget-object v1, p0, Lcom/android/server/am/MemoryStandardProcessControl$1;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    iput v3, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenState:I

    .line 800
    iget-object v1, p0, Lcom/android/server/am/MemoryStandardProcessControl$1;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    iget v1, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenOffStrategy:I

    if-ne v1, v3, :cond_1

    .line 801
    iget-object v1, p0, Lcom/android/server/am/MemoryStandardProcessControl$1;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    iget-object v1, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mHandler:Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;

    invoke-virtual {v1, v2}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->removeMessages(I)V

    goto :goto_0

    .line 803
    :cond_0
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 804
    iget-object v1, p0, Lcom/android/server/am/MemoryStandardProcessControl$1;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    iput v2, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenState:I

    .line 805
    iget-object v1, p0, Lcom/android/server/am/MemoryStandardProcessControl$1;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    iget v1, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenOffStrategy:I

    if-ne v1, v3, :cond_1

    .line 806
    iget-object v1, p0, Lcom/android/server/am/MemoryStandardProcessControl$1;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    iget-object v1, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mHandler:Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;

    const/4 v2, 0x3

    const-wide/32 v3, 0xea60

    invoke-virtual {v1, v2, v3, v4}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 809
    :cond_1
    :goto_0
    return-void
.end method
