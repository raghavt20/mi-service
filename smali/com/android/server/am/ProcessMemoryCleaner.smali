.class public Lcom/android/server/am/ProcessMemoryCleaner;
.super Lcom/android/server/am/ProcessCleanerBase;
.source "ProcessMemoryCleaner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/ProcessMemoryCleaner$H;
    }
.end annotation


# static fields
.field private static final AUDIO_PROC_PAUSE_PROTECT_TIME:I = 0x493e0

.field private static final CAMERA_PACKAGE_NAME:Ljava/lang/String; = "com.android.camera"

.field private static final COMM_USED_PSS_LIMIT_HIGH_FACTOR:D = 1.33

.field private static final COMM_USED_PSS_LIMIT_LITE_FACTOR:D = 0.67

.field private static final COMM_USED_PSS_LIMIT_USUAL_FACTOR:D = 1.0

.field public static final DEBUG:Z

.field private static final DEF_MIN_KILL_PROC_ADJ:I = 0xc8

.field private static final HOME_PACKAGE_NAME:Ljava/lang/String; = "com.miui.home"

.field private static final KILLING_PROC_REASON:Ljava/lang/String; = "MiuiMemoryService"

.field private static final KILL_PROC_COUNT_LIMIT:I = 0xa

.field private static final MEM_EXCEPTION_TH_HIGHH_FACTOR:D = 2.0

.field private static final MEM_EXCEPTION_TH_HIGHL_FACTOR:D = 1.25

.field private static final MEM_EXCEPTION_TH_HIGHM_FACTOR:D = 1.5

.field private static final MEM_EXCEPTION_TH_LITE_FACTOR:D = 0.625

.field private static final MEM_EXCEPTION_TH_MID_FACTOR:D = 0.75

.field private static final MEM_EXCEPTION_TH_USUAL_FACTOR:D = 1.0

.field private static final MSG_APP_SWITCH_BG_EXCEPTION:I = 0x1

.field private static final MSG_PAD_SMALL_WINDOW_CLEAN:I = 0x3

.field private static final MSG_PROCESS_BG_COMPACT:I = 0x4

.field private static final MSG_REGISTER_CLOUD_OBSERVER:I = 0x2

.field private static final PAD_SMALL_WINDOW_CLEAN_TIME:J = 0x1388L

.field private static final PERF_PROC_THRESHOLD_CLOUD_KEY:Ljava/lang/String; = "perf_proc_threshold"

.field private static final PERF_PROC_THRESHOLD_CLOUD_MOUDLE:Ljava/lang/String; = "perf_process"

.field private static final PROCESS_PRIORITY_FACTOR:I = 0x3e8

.field private static final PROC_BG_COMPACT_DELAY_TIME:I = 0xbb8

.field private static final PROC_COMM_PSS_LIMIT_CLOUD:Ljava/lang/String; = "perf_proc_common_pss_limit"

.field private static final PROC_MEM_EXCEPTION_THRESHOLD_CLOUD:Ljava/lang/String; = "perf_proc_mem_exception_threshold"

.field private static final PROC_PROTECT_LIST_CLOUD:Ljava/lang/String; = "perf_proc_protect_list"

.field private static final PROC_SWITCH_BG_DELAY_TIME_CLOUD:Ljava/lang/String; = "perf_proc_switch_Bg_time"

.field private static final RAM_SIZE_1GB:J = 0x40000000L

.field private static final REASON_PAD_SMALL_WINDOW_CLEAN:Ljava/lang/String; = "PadSmallWindowClean"

.field private static final SUB_PROCESS_ADJ_BIND_LIST_CLOUD:Ljava/lang/String; = "perf_proc_adj_bind_list"

.field public static final TAG:Ljava/lang/String; = "ProcessMemoryCleaner"

.field private static final TOTAL_MEMEORY_GB:J


# instance fields
.field private mAMS:Lcom/android/server/am/ActivityManagerService;

.field private mAppSwitchBgExceptDelayTime:I

.field private mCommonUsedPssLimitKB:J

.field private mContext:Landroid/content/Context;

.field private mForegroundPkg:Ljava/lang/String;

.field private mForegroundUid:I

.field private mHandler:Lcom/android/server/am/ProcessMemoryCleaner$H;

.field private mHandlerTh:Landroid/os/HandlerThread;

.field private mLastPadSmallWindowUpdateTime:J

.field private mMemExceptionThresholdKB:J

.field private mMiuiMemoryService:Lcom/android/server/am/MiuiMemoryServiceInternal;

.field private mPMS:Lcom/android/server/am/ProcessManagerService;

.field private mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

.field private mStatistics:Lcom/android/server/am/ProcMemCleanerStatistics;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/am/ProcessMemoryCleaner;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mcheckBackgroundAppException(Lcom/android/server/am/ProcessMemoryCleaner;Ljava/lang/String;I)I
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/am/ProcessMemoryCleaner;->checkBackgroundAppException(Ljava/lang/String;I)I

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mcheckBackgroundProcCompact(Lcom/android/server/am/ProcessMemoryCleaner;Lcom/miui/server/smartpower/IAppState$IRunningProcess;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessMemoryCleaner;->checkBackgroundProcCompact(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mkillProcessByMinAdj(Lcom/android/server/am/ProcessMemoryCleaner;ILjava/lang/String;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/am/ProcessMemoryCleaner;->killProcessByMinAdj(ILjava/lang/String;Ljava/util/List;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mregisterCloudObserver(Lcom/android/server/am/ProcessMemoryCleaner;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessMemoryCleaner;->registerCloudObserver(Landroid/content/Context;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateCloudControlData(Lcom/android/server/am/ProcessMemoryCleaner;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/ProcessMemoryCleaner;->updateCloudControlData()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 4

    .line 38
    sget-boolean v0, Landroid/os/spc/PressureStateSettings;->DEBUG_ALL:Z

    sput-boolean v0, Lcom/android/server/am/ProcessMemoryCleaner;->DEBUG:Z

    .line 51
    invoke-static {}, Landroid/os/Process;->getTotalMemory()J

    move-result-wide v0

    const-wide/32 v2, 0x40000000

    div-long/2addr v0, v2

    sput-wide v0, Lcom/android/server/am/ProcessMemoryCleaner;->TOTAL_MEMEORY_GB:J

    return-void
.end method

.method public constructor <init>(Lcom/android/server/am/ActivityManagerService;)V
    .locals 3
    .param p1, "ams"    # Lcom/android/server/am/ActivityManagerService;

    .line 115
    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessCleanerBase;-><init>(Lcom/android/server/am/ActivityManagerService;)V

    .line 88
    const/16 v0, 0x7530

    iput v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mAppSwitchBgExceptDelayTime:I

    .line 90
    sget-wide v0, Landroid/os/spc/PressureStateSettings;->PROC_COMMON_USED_PSS_LIMIT_KB:J

    iput-wide v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mCommonUsedPssLimitKB:J

    .line 101
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "ProcessMemoryCleanerTh"

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mHandlerTh:Landroid/os/HandlerThread;

    .line 108
    sget-wide v0, Landroid/os/spc/PressureStateSettings;->PROC_MEM_EXCEPTION_PSS_LIMIT_KB:J

    iput-wide v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mMemExceptionThresholdKB:J

    .line 110
    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mForegroundPkg:Ljava/lang/String;

    .line 111
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mForegroundUid:I

    .line 112
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mLastPadSmallWindowUpdateTime:J

    .line 116
    iput-object p1, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mAMS:Lcom/android/server/am/ActivityManagerService;

    .line 117
    return-void
.end method

.method private checkBackgroundAppException(Ljava/lang/String;I)I
    .locals 18
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 673
    move-object/from16 v0, p0

    const/4 v1, 0x0

    .line 675
    .local v1, "killType":I
    invoke-static {}, Lcom/android/server/am/SystemPressureController;->getInstance()Lcom/android/server/am/SystemPressureController;

    move-result-object v2

    move-object/from16 v3, p1

    invoke-virtual {v2, v3}, Lcom/android/server/am/SystemPressureController;->isGameApp(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 676
    return v1

    .line 678
    :cond_0
    const-wide/16 v4, 0x0

    .line 679
    .local v4, "mainPss":J
    const-wide/16 v6, 0x0

    .line 680
    .local v6, "subPss":J
    const-wide/16 v8, 0x0

    .line 681
    .local v8, "subPssWithActivity":J
    const/4 v2, 0x0

    .line 682
    .local v2, "mainProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    invoke-direct/range {p0 .. p2}, Lcom/android/server/am/ProcessMemoryCleaner;->getProcessGroup(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v10

    .line 683
    .local v10, "procGroup":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 684
    .local v12, "proc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    invoke-virtual {v12}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->updatePss()V

    .line 685
    invoke-virtual {v12}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 686
    move-object v2, v12

    .line 687
    invoke-virtual {v12}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPss()J

    move-result-wide v4

    goto :goto_1

    .line 688
    :cond_1
    invoke-virtual {v12}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->hasActivity()Z

    move-result v13

    if-eqz v13, :cond_2

    .line 689
    invoke-virtual {v12}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPss()J

    move-result-wide v13

    add-long/2addr v8, v13

    goto :goto_1

    .line 691
    :cond_2
    invoke-virtual {v12}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPss()J

    move-result-wide v13

    add-long/2addr v6, v13

    .line 693
    .end local v12    # "proc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    :goto_1
    goto :goto_0

    .line 694
    :cond_3
    if-nez v2, :cond_4

    return v1

    .line 696
    :cond_4
    invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/android/server/am/ProcMemCleanerStatistics;->isCommonUsedApp(Ljava/lang/String;)Z

    move-result v11

    const-string v12, "ProcessMemoryCleaner"

    const-string v13, "kill_bg_proc"

    if-eqz v11, :cond_6

    .line 697
    iget-wide v14, v0, Lcom/android/server/am/ProcessMemoryCleaner;->mCommonUsedPssLimitKB:J

    cmp-long v11, v4, v14

    if-ltz v11, :cond_5

    .line 698
    invoke-virtual {v0, v2, v13}, Lcom/android/server/am/ProcessMemoryCleaner;->killPackage(Lcom/miui/server/smartpower/IAppState$IRunningProcess;Ljava/lang/String;)J

    .line 699
    const/4 v1, 0x2

    .line 700
    sget-boolean v11, Lcom/android/server/am/ProcessMemoryCleaner;->DEBUG:Z

    if-eqz v11, :cond_5

    .line 701
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "common used app takes up too much memory: "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 702
    invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 701
    invoke-static {v12, v11}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 704
    :cond_5
    return v1

    .line 707
    :cond_6
    add-long v14, v4, v6

    add-long/2addr v14, v8

    move-wide/from16 v16, v8

    .end local v8    # "subPssWithActivity":J
    .local v16, "subPssWithActivity":J
    iget-wide v8, v0, Lcom/android/server/am/ProcessMemoryCleaner;->mMemExceptionThresholdKB:J

    cmp-long v8, v14, v8

    if-lez v8, :cond_7

    .line 708
    iget-object v8, v0, Lcom/android/server/am/ProcessMemoryCleaner;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    .line 709
    invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUid()I

    move-result v9

    invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;

    move-result-object v11

    .line 708
    invoke-interface {v8, v9, v11}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->getRunningProcess(ILjava/lang/String;)Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    move-result-object v8

    invoke-virtual {v0, v8, v13}, Lcom/android/server/am/ProcessMemoryCleaner;->killPackage(Lcom/miui/server/smartpower/IAppState$IRunningProcess;Ljava/lang/String;)J

    .line 711
    const/4 v1, 0x2

    .line 712
    sget-boolean v8, Lcom/android/server/am/ProcessMemoryCleaner;->DEBUG:Z

    if-eqz v8, :cond_a

    .line 713
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "app takes up too much memory: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ". pss:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " sub:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v12, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 716
    :cond_7
    sget-wide v8, Lcom/miui/app/smartpower/SmartPowerSettings;->PROC_MEM_LVL1_PSS_LIMIT_KB:J

    cmp-long v8, v6, v8

    if-ltz v8, :cond_a

    .line 718
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 719
    .local v9, "proc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    invoke-virtual {v9}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_8

    invoke-virtual {v9}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->hasActivity()Z

    move-result v11

    if-nez v11, :cond_8

    .line 720
    iget-object v11, v0, Lcom/android/server/am/ProcessMemoryCleaner;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    .line 721
    invoke-virtual {v9}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUid()I

    move-result v14

    invoke-virtual {v9}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v11, v14, v15}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->getRunningProcess(ILjava/lang/String;)Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    move-result-object v11

    .line 720
    invoke-virtual {v0, v11, v13}, Lcom/android/server/am/ProcessMemoryCleaner;->killProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;Ljava/lang/String;)J

    .line 724
    .end local v9    # "proc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    :cond_8
    goto :goto_2

    .line 725
    :cond_9
    const/4 v1, 0x1

    .line 726
    sget-boolean v8, Lcom/android/server/am/ProcessMemoryCleaner;->DEBUG:Z

    if-eqz v8, :cond_a

    .line 727
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "subprocess takes up too much memory: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 728
    invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ". sub:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 727
    invoke-static {v12, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 730
    :cond_a
    :goto_3
    return v1
.end method

.method private checkBackgroundProcCompact(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)V
    .locals 3
    .param p1, "proc"    # Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 599
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getCurrentState()I

    move-result v0

    const/4 v1, 0x4

    if-lt v0, v1, :cond_1

    .line 600
    const/16 v0, 0x64

    invoke-direct {p0, p1, v0}, Lcom/android/server/am/ProcessMemoryCleaner;->checkRunningProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 601
    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessMemoryCleaner;->isNeedCompact(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mMiuiMemoryService:Lcom/android/server/am/MiuiMemoryServiceInternal;

    .line 602
    invoke-interface {v0, p1, v1}, Lcom/android/server/am/MiuiMemoryServiceInternal;->isCompactNeeded(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 604
    sget-boolean v0, Lcom/android/server/am/ProcessMemoryCleaner;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 605
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Compact memory: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " pss:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 606
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPss()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " swap:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getSwapPss()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 605
    const-string v1, "ProcessMemoryCleaner"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 607
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessMemoryCleaner;->compactProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)V

    .line 609
    :cond_1
    return-void
.end method

.method private checkRunningProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)Z
    .locals 1
    .param p1, "runningProc"    # Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .param p2, "minAdj"    # I

    .line 310
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/am/ProcessMemoryCleaner;->checkProcessDied(Lcom/android/server/am/ProcessRecord;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 311
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I

    move-result v0

    if-le v0, p2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 310
    :goto_0
    return v0
.end method

.method private cleanUpMemory(Ljava/util/List;J)Z
    .locals 12
    .param p2, "targetReleaseMem"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/miui/server/smartpower/IAppState$IRunningProcess;",
            ">;J)Z"
        }
    .end annotation

    .line 220
    .local p1, "runningProcList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
    new-instance v0, Lcom/android/server/am/ProcessMemoryCleaner$1;

    invoke-direct {v0, p0}, Lcom/android/server/am/ProcessMemoryCleaner$1;-><init>(Lcom/android/server/am/ProcessMemoryCleaner;)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 234
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 235
    .local v0, "nowTime":J
    iget-object v2, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mStatistics:Lcom/android/server/am/ProcMemCleanerStatistics;

    invoke-virtual {v2, v0, v1}, Lcom/android/server/am/ProcMemCleanerStatistics;->checkLastKillTime(J)V

    .line 236
    sget-boolean v2, Lcom/android/server/am/ProcessMemoryCleaner;->DEBUG:Z

    if-eqz v2, :cond_0

    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessMemoryCleaner;->debugAppGroupToString(Ljava/util/List;)V

    .line 238
    :cond_0
    const-wide/16 v2, 0x0

    .line 239
    .local v2, "releasePssByProcClean":J
    const/4 v4, 0x0

    .line 240
    .local v4, "killProcCount":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    const/4 v7, 0x0

    if-eqz v6, :cond_9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 241
    .local v6, "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    iget-object v8, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mStatistics:Lcom/android/server/am/ProcMemCleanerStatistics;

    invoke-virtual {v6}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;

    move-result-object v9

    .line 242
    invoke-virtual {v6}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUid()I

    move-result v10

    .line 241
    invoke-virtual {v8, v9, v10, v0, v1}, Lcom/android/server/am/ProcMemCleanerStatistics;->isLastKillProcess(Ljava/lang/String;IJ)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 243
    goto :goto_0

    .line 246
    :cond_1
    invoke-virtual {v6}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I

    move-result v8

    const/16 v9, 0x384

    if-lt v8, v9, :cond_2

    .line 247
    invoke-direct {p0, v6}, Lcom/android/server/am/ProcessMemoryCleaner;->isIsolatedProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 248
    goto :goto_0

    .line 250
    :cond_2
    invoke-virtual {v6}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->hasActivity()Z

    move-result v8

    const/4 v9, 0x1

    if-eqz v8, :cond_3

    .line 251
    return v9

    .line 253
    :cond_3
    const-string v8, "clean_up_mem"

    invoke-virtual {p0, v6, v7, v8}, Lcom/android/server/am/ProcessMemoryCleaner;->killProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;ILjava/lang/String;)J

    move-result-wide v7

    .line 255
    .local v7, "pss":J
    const-wide/16 v10, 0x0

    cmp-long v10, p2, v10

    if-gtz v10, :cond_4

    .line 256
    goto :goto_0

    .line 258
    :cond_4
    sget-boolean v10, Landroid/os/spc/PressureStateSettings;->ONLY_KILL_ONE_PKG:Z

    if-eqz v10, :cond_6

    invoke-virtual {v6}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->hasActivity()Z

    move-result v10

    if-eqz v10, :cond_6

    .line 259
    sget-boolean v5, Lcom/android/server/am/ProcessMemoryCleaner;->DEBUG:Z

    if-eqz v5, :cond_5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "----skip kill: "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-direct {p0, v6}, Lcom/android/server/am/ProcessMemoryCleaner;->processToString(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v10, "ProcessMemoryCleaner"

    invoke-static {v10, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    :cond_5
    return v9

    .line 262
    :cond_6
    add-long/2addr v2, v7

    .line 263
    cmp-long v10, v2, p2

    if-gez v10, :cond_8

    add-int/lit8 v4, v4, 0x1

    const/16 v10, 0xa

    if-lt v4, v10, :cond_7

    goto :goto_1

    .line 267
    .end local v6    # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .end local v7    # "pss":J
    :cond_7
    goto :goto_0

    .line 265
    .restart local v6    # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .restart local v7    # "pss":J
    :cond_8
    :goto_1
    return v9

    .line 268
    .end local v6    # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .end local v7    # "pss":J
    :cond_9
    return v7
.end method

.method private compactProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)V
    .locals 2
    .param p1, "processInfo"    # Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 272
    iget-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mMiuiMemoryService:Lcom/android/server/am/MiuiMemoryServiceInternal;

    const/4 v1, 0x4

    invoke-interface {v0, p1, v1}, Lcom/android/server/am/MiuiMemoryServiceInternal;->runProcCompaction(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)V

    .line 274
    return-void
.end method

.method private computeCommonUsedAppPssThreshold(J)V
    .locals 4
    .param p1, "threshold"    # J

    .line 660
    sget-wide v0, Lcom/android/server/am/ProcessMemoryCleaner;->TOTAL_MEMEORY_GB:J

    const-wide/16 v2, 0x6

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    .line 661
    long-to-double v0, p1

    const-wide v2, 0x3fe570a3d70a3d71L    # 0.67

    mul-double/2addr v0, v2

    double-to-long v0, v0

    iput-wide v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mCommonUsedPssLimitKB:J

    goto :goto_0

    .line 663
    :cond_0
    const-wide/16 v2, 0x8

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    .line 664
    long-to-double v0, p1

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v0, v2

    double-to-long v0, v0

    iput-wide v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mCommonUsedPssLimitKB:J

    goto :goto_0

    .line 667
    :cond_1
    long-to-double v0, p1

    const-wide v2, 0x3ff547ae147ae148L    # 1.33

    mul-double/2addr v0, v2

    double-to-long v0, v0

    iput-wide v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mCommonUsedPssLimitKB:J

    .line 670
    :goto_0
    return-void
.end method

.method private computeMemExceptionThreshold(J)V
    .locals 4
    .param p1, "threshold"    # J

    .line 644
    sget-wide v0, Lcom/android/server/am/ProcessMemoryCleaner;->TOTAL_MEMEORY_GB:J

    const-wide/16 v2, 0x5

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 645
    long-to-double v0, p1

    const-wide/high16 v2, 0x3fe4000000000000L    # 0.625

    mul-double/2addr v0, v2

    double-to-long v0, v0

    iput-wide v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mMemExceptionThresholdKB:J

    goto :goto_0

    .line 646
    :cond_0
    const-wide/16 v2, 0x6

    cmp-long v2, v0, v2

    if-gtz v2, :cond_1

    .line 647
    long-to-double v0, p1

    const-wide/high16 v2, 0x3fe8000000000000L    # 0.75

    mul-double/2addr v0, v2

    double-to-long v0, v0

    iput-wide v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mMemExceptionThresholdKB:J

    goto :goto_0

    .line 648
    :cond_1
    const-wide/16 v2, 0x8

    cmp-long v2, v0, v2

    if-gtz v2, :cond_2

    .line 649
    long-to-double v0, p1

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v0, v2

    double-to-long v0, v0

    iput-wide v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mMemExceptionThresholdKB:J

    goto :goto_0

    .line 650
    :cond_2
    const-wide/16 v2, 0xc

    cmp-long v2, v0, v2

    if-gtz v2, :cond_3

    .line 651
    long-to-double v0, p1

    const-wide/high16 v2, 0x3ff4000000000000L    # 1.25

    mul-double/2addr v0, v2

    double-to-long v0, v0

    iput-wide v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mMemExceptionThresholdKB:J

    goto :goto_0

    .line 652
    :cond_3
    const-wide/16 v2, 0x10

    cmp-long v0, v0, v2

    if-gtz v0, :cond_4

    .line 653
    long-to-double v0, p1

    const-wide/high16 v2, 0x3ff8000000000000L    # 1.5

    mul-double/2addr v0, v2

    double-to-long v0, v0

    iput-wide v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mMemExceptionThresholdKB:J

    goto :goto_0

    .line 655
    :cond_4
    long-to-double v0, p1

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    mul-double/2addr v0, v2

    double-to-long v0, v0

    iput-wide v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mMemExceptionThresholdKB:J

    .line 657
    :goto_0
    return-void
.end method

.method private containInWhiteList(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z
    .locals 4
    .param p1, "proc"    # Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 277
    iget-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mPMS:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;

    move-result-object v1

    const/16 v2, 0x15

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Lcom/android/server/am/ProcessManagerService;->isInWhiteList(Lcom/android/server/am/ProcessRecord;II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 279
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/server/process/ProcessManagerInternal;->checkCtsProcess(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v3, 0x1

    .line 277
    :cond_1
    return v3
.end method

.method private debugAppGroupToString(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/miui/server/smartpower/IAppState$IRunningProcess;",
            ">;)V"
        }
    .end annotation

    .line 531
    .local p1, "runningProcList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 532
    .local v1, "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    invoke-direct {p0, v1}, Lcom/android/server/am/ProcessMemoryCleaner;->processToString(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Ljava/lang/String;

    move-result-object v2

    .line 533
    .local v2, "deg":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ProcessInfo: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ProcessMemoryCleaner"

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 534
    .end local v1    # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .end local v2    # "deg":Ljava/lang/String;
    goto :goto_0

    .line 535
    :cond_0
    return-void
.end method

.method private getKillReason(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Ljava/lang/String;
    .locals 2
    .param p1, "proc"    # Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 448
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MiuiMemoryService("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdjType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getProcPriority(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)I
    .locals 2
    .param p0, "runningProc"    # Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 141
    invoke-virtual {p0}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    invoke-virtual {p0}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPriorityScore()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private getProcessGroup(Ljava/lang/String;I)Ljava/util/List;
    .locals 13
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List<",
            "Lcom/miui/server/smartpower/IAppState$IRunningProcess;",
            ">;"
        }
    .end annotation

    .line 284
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 285
    .local v0, "procs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
    iget-object v1, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mAMS:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v1

    .line 286
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    .line 287
    .local v2, "procList":Lcom/android/server/am/ProcessList;
    invoke-virtual {v2}, Lcom/android/server/am/ProcessList;->getProcessNamesLOSP()Lcom/android/server/am/ProcessList$MyProcessMap;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/server/am/ProcessList$MyProcessMap;->getMap()Landroid/util/ArrayMap;

    move-result-object v3

    invoke-virtual {v3}, Landroid/util/ArrayMap;->size()I

    move-result v3

    .line 288
    .local v3, "NP":I
    const/4 v4, 0x0

    .local v4, "ip":I
    :goto_0
    if-ge v4, v3, :cond_4

    .line 289
    nop

    .line 290
    invoke-virtual {v2}, Lcom/android/server/am/ProcessList;->getProcessNamesLOSP()Lcom/android/server/am/ProcessList$MyProcessMap;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/server/am/ProcessList$MyProcessMap;->getMap()Landroid/util/ArrayMap;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/util/SparseArray;

    .line 291
    .local v5, "apps":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/am/ProcessRecord;>;"
    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    move-result v6

    .line 292
    .local v6, "NA":I
    const/4 v7, 0x0

    .local v7, "ia":I
    :goto_1
    if-ge v7, v6, :cond_3

    .line 293
    invoke-virtual {v5, v7}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/server/am/ProcessRecord;

    .line 294
    .local v8, "app":Lcom/android/server/am/ProcessRecord;
    invoke-virtual {v8}, Lcom/android/server/am/ProcessRecord;->getPkgDeps()Landroid/util/ArraySet;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 295
    invoke-virtual {v8}, Lcom/android/server/am/ProcessRecord;->getPkgDeps()Landroid/util/ArraySet;

    move-result-object v9

    invoke-virtual {v9, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    const/4 v9, 0x1

    goto :goto_2

    :cond_0
    const/4 v9, 0x0

    .line 296
    .local v9, "isDep":Z
    :goto_2
    iget v10, v8, Lcom/android/server/am/ProcessRecord;->uid:I

    invoke-static {v10}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v10

    invoke-static {p2}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v11

    if-eq v10, v11, :cond_1

    .line 297
    goto :goto_3

    .line 299
    :cond_1
    invoke-virtual {v8}, Lcom/android/server/am/ProcessRecord;->getPkgList()Lcom/android/server/am/PackageList;

    move-result-object v10

    invoke-virtual {v10, p1}, Lcom/android/server/am/PackageList;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    if-nez v9, :cond_2

    .line 300
    goto :goto_3

    .line 302
    :cond_2
    iget-object v10, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    iget v11, v8, Lcom/android/server/am/ProcessRecord;->uid:I

    iget-object v12, v8, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-interface {v10, v11, v12}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->getRunningProcess(ILjava/lang/String;)Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 292
    .end local v8    # "app":Lcom/android/server/am/ProcessRecord;
    .end local v9    # "isDep":Z
    :goto_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 288
    .end local v5    # "apps":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/am/ProcessRecord;>;"
    .end local v6    # "NA":I
    .end local v7    # "ia":I
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 305
    .end local v2    # "procList":Lcom/android/server/am/ProcessList;
    .end local v3    # "NP":I
    .end local v4    # "ip":I
    :cond_4
    monitor-exit v1

    .line 306
    return-object v0

    .line 305
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private isAutoStartProcess(Lcom/miui/server/smartpower/IAppState;Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z
    .locals 2
    .param p1, "appState"    # Lcom/miui/server/smartpower/IAppState;
    .param p2, "runningProc"    # Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 180
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState;->isAutoStartApp()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I

    move-result v0

    const/16 v1, 0x320

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isImportantSubProc(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "procName"    # Ljava/lang/String;

    .line 200
    invoke-static {}, Lcom/android/server/am/OomAdjusterImpl;->getInstance()Lcom/android/server/am/OomAdjusterImpl;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/server/am/OomAdjusterImpl;->isImportantSubProc(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private isIsolatedByAdj(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z
    .locals 1
    .param p1, "runningProc"    # Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 190
    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessMemoryCleaner;->isIsolatedProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z

    move-result v0

    return v0
.end method

.method private isIsolatedProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z
    .locals 3
    .param p1, "runningProc"    # Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 194
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->isIsolated(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 195
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 196
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":sandboxed_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 195
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 194
    :goto_1
    return v0
.end method

.method private isLastMusicPlayProcess(I)Z
    .locals 6
    .param p1, "pid"    # I

    .line 212
    iget-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-interface {v0, p1}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->getLastMusicPlayTimeStamp(I)J

    move-result-wide v0

    .line 213
    .local v0, "lastMusicPlayTime":J
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    const-wide/32 v4, 0x493e0

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    .line 214
    const/4 v2, 0x1

    return v2

    .line 216
    :cond_0
    const/4 v2, 0x0

    return v2
.end method

.method private isLaunchCameraForThirdApp(Lcom/android/server/am/ControllerActivityInfo;)Z
    .locals 2
    .param p1, "info"    # Lcom/android/server/am/ControllerActivityInfo;

    .line 553
    iget-object v0, p1, Lcom/android/server/am/ControllerActivityInfo;->fromPkg:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/server/am/ControllerActivityInfo;->launchPkg:Ljava/lang/String;

    .line 554
    const-string v1, "com.android.camera"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p1, Lcom/android/server/am/ControllerActivityInfo;->formUid:I

    .line 555
    invoke-direct {p0, v0}, Lcom/android/server/am/ProcessMemoryCleaner;->isSystemApp(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 556
    const/4 v0, 0x1

    return v0

    .line 558
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private isNeedCompact(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z
    .locals 4
    .param p1, "proc"    # Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 612
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUid()I

    move-result v0

    const/16 v1, 0x3e8

    if-le v0, v1, :cond_1

    .line 613
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->hasActivity()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;

    move-result-object v0

    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUserId()I

    move-result v1

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mPMS:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/android/server/am/ProcessMemoryCleaner;->isInWhiteListLock(Lcom/android/server/am/ProcessRecord;IILcom/android/server/am/ProcessManagerService;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 615
    :cond_0
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPss()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getSwapPss()J

    move-result-wide v2

    sub-long/2addr v0, v2

    sget-wide v2, Lcom/android/server/am/MiuiMemReclaimer;->ANON_RSS_LIMIT_KB:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 612
    :goto_0
    return v0
.end method

.method private isRunningComponent(Lcom/android/server/am/ProcessRecord;)Z
    .locals 1
    .param p1, "proc"    # Lcom/android/server/am/ProcessRecord;

    .line 476
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mServices:Lcom/android/server/am/ProcessServiceRecord;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessServiceRecord;->numberOfExecutingServices()I

    move-result v0

    if-gtz v0, :cond_1

    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mReceivers:Lcom/android/server/am/ProcessReceiverRecord;

    .line 477
    invoke-virtual {v0}, Lcom/android/server/am/ProcessReceiverRecord;->numberOfCurReceivers()I

    move-result v0

    if-lez v0, :cond_0

    goto :goto_0

    .line 480
    :cond_0
    const/4 v0, 0x0

    return v0

    .line 478
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method private isSystemApp(I)Z
    .locals 2
    .param p1, "uid"    # I

    .line 562
    iget-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-interface {v0, p1}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->getAppState(I)Lcom/miui/server/smartpower/IAppState;

    move-result-object v0

    .line 563
    .local v0, "appState":Lcom/miui/server/smartpower/IAppState;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/server/smartpower/IAppState;->isSystemApp()Z

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private isSystemHighPrioProc(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z
    .locals 2
    .param p1, "runningProc"    # Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 204
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I

    move-result v0

    const/16 v1, 0xc8

    if-gt v0, v1, :cond_0

    .line 205
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->isSystemApp()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 206
    const/4 v0, 0x1

    return v0

    .line 208
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private killProcessByMinAdj(ILjava/lang/String;Ljava/util/List;)V
    .locals 7
    .param p1, "minAdj"    # I
    .param p2, "reason"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 496
    .local p3, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/16 v0, 0x12c

    if-gt p1, v0, :cond_0

    .line 497
    const/16 p1, 0x12c

    .line 499
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-interface {v0}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->getLruProcesses()Ljava/util/ArrayList;

    move-result-object v0

    .line 500
    .local v0, "runningAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 501
    .local v1, "canForcePkg":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 502
    .local v3, "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 503
    invoke-direct {p0, v3, p1}, Lcom/android/server/am/ProcessMemoryCleaner;->checkRunningProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)Z

    move-result v4

    if-nez v4, :cond_2

    .line 504
    goto :goto_0

    .line 506
    :cond_2
    invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 507
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 509
    :cond_3
    iget-object v4, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mAMS:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v4

    .line 510
    :try_start_0
    invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;

    move-result-object v5

    invoke-virtual {p0, v5, p2}, Lcom/android/server/am/ProcessMemoryCleaner;->killApplicationLock(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V

    .line 511
    monitor-exit v4

    .line 513
    .end local v3    # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    :goto_1
    goto :goto_0

    .line 511
    .restart local v3    # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    :catchall_0
    move-exception v2

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 514
    .end local v3    # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    :cond_4
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 515
    .local v3, "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    invoke-static {}, Lcom/android/server/am/SystemPressureController;->getInstance()Lcom/android/server/am/SystemPressureController;

    move-result-object v4

    .line 516
    invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/android/server/am/SystemPressureController;->isForceStopEnable(Lcom/android/server/am/ProcessRecord;I)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 517
    invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUserId()I

    move-result v5

    invoke-virtual {p0, v4, v5, p2}, Lcom/android/server/am/ProcessMemoryCleaner;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_3

    .line 519
    :cond_5
    iget-object v4, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mAMS:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v4

    .line 520
    :try_start_1
    invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;

    move-result-object v5

    invoke-virtual {p0, v5, p2}, Lcom/android/server/am/ProcessMemoryCleaner;->killApplicationLock(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V

    .line 521
    monitor-exit v4

    .line 523
    .end local v3    # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    :goto_3
    goto :goto_2

    .line 521
    .restart local v3    # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    :catchall_1
    move-exception v2

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v2

    .line 524
    .end local v3    # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    :cond_6
    return-void
.end method

.method private logCloudControlParas(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/String;

    .line 408
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "sync cloud control "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ProcessMemoryCleaner"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    return-void
.end method

.method private processToString(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Ljava/lang/String;
    .locals 3
    .param p1, "runningProc"    # Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 538
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 539
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "pck="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 540
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 541
    const-string v1, ", prcName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 542
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 543
    const-string v1, ", priority="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 544
    invoke-static {p1}, Lcom/android/server/am/ProcessMemoryCleaner;->getProcPriority(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 545
    const-string v1, ", hasAct="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 546
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->hasActivity()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 547
    const-string v1, ", pss="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 548
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPss()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 549
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private registerCloudObserver(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 315
    new-instance v0, Lcom/android/server/am/ProcessMemoryCleaner$2;

    iget-object v1, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mHandler:Lcom/android/server/am/ProcessMemoryCleaner$H;

    invoke-direct {v0, p0, v1}, Lcom/android/server/am/ProcessMemoryCleaner$2;-><init>(Lcom/android/server/am/ProcessMemoryCleaner;Landroid/os/Handler;)V

    .line 325
    .local v0, "observer":Landroid/database/ContentObserver;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 326
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v2

    .line 325
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 327
    return-void
.end method

.method private sendAppSwitchBgExceptionMsg(Lcom/android/server/am/ControllerActivityInfo;)V
    .locals 5
    .param p1, "info"    # Lcom/android/server/am/ControllerActivityInfo;

    .line 619
    iget-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mHandler:Lcom/android/server/am/ProcessMemoryCleaner$H;

    iget-object v1, p1, Lcom/android/server/am/ControllerActivityInfo;->launchPkg:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/android/server/am/ProcessMemoryCleaner$H;->hasEqualMessages(ILjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 620
    iget-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mHandler:Lcom/android/server/am/ProcessMemoryCleaner$H;

    iget-object v1, p1, Lcom/android/server/am/ControllerActivityInfo;->launchPkg:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Lcom/android/server/am/ProcessMemoryCleaner$H;->removeMessages(ILjava/lang/Object;)V

    .line 622
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    .line 623
    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessPolicy;->getPolicyFlags(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessPolicy;->getWhiteList(I)Ljava/util/List;

    move-result-object v0

    .line 624
    .local v0, "packageWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mForegroundPkg:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    iget-object v3, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mForegroundPkg:Ljava/lang/String;

    .line 625
    invoke-virtual {v1, v3}, Lcom/android/server/am/ProcessPolicy;->isInExtraPackageList(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 626
    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessMemoryCleaner;->isLaunchCameraForThirdApp(Lcom/android/server/am/ControllerActivityInfo;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 627
    invoke-static {}, Lcom/android/server/am/SystemPressureController;->getInstance()Lcom/android/server/am/SystemPressureController;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mForegroundPkg:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/android/server/am/SystemPressureController;->isGameApp(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 628
    iget-object v1, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mHandler:Lcom/android/server/am/ProcessMemoryCleaner$H;

    iget-object v3, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mForegroundPkg:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/android/server/am/ProcessMemoryCleaner$H;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 629
    .local v1, "msg":Landroid/os/Message;
    iget-object v2, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mForegroundPkg:Ljava/lang/String;

    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 630
    iget v2, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mForegroundUid:I

    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 631
    iget-object v2, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mHandler:Lcom/android/server/am/ProcessMemoryCleaner$H;

    iget v3, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mAppSwitchBgExceptDelayTime:I

    int-to-long v3, v3

    invoke-virtual {v2, v1, v3, v4}, Lcom/android/server/am/ProcessMemoryCleaner$H;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 633
    .end local v1    # "msg":Landroid/os/Message;
    :cond_1
    return-void
.end method

.method private sendGlobalCompactMsg(Lcom/android/server/am/ControllerActivityInfo;)V
    .locals 2
    .param p1, "info"    # Lcom/android/server/am/ControllerActivityInfo;

    .line 636
    iget-object v0, p1, Lcom/android/server/am/ControllerActivityInfo;->launchPkg:Ljava/lang/String;

    const-string v1, "com.miui.home"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 637
    iget-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mMiuiMemoryService:Lcom/android/server/am/MiuiMemoryServiceInternal;

    if-eqz v0, :cond_0

    .line 638
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/android/server/am/MiuiMemoryServiceInternal;->runGlobalCompaction(I)V

    .line 641
    :cond_0
    return-void
.end method

.method private updateAppSwitchBgDelayTime(Lorg/json/JSONObject;)V
    .locals 3
    .param p1, "jsonObject"    # Lorg/json/JSONObject;

    .line 365
    const-string v0, "perf_proc_switch_Bg_time"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 366
    .local v1, "SwitchBgTime":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 367
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mAppSwitchBgExceptDelayTime:I

    .line 368
    sget-boolean v2, Lcom/android/server/am/ProcessMemoryCleaner;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 369
    invoke-direct {p0, v0, v1}, Lcom/android/server/am/ProcessMemoryCleaner;->logCloudControlParas(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    :cond_0
    return-void
.end method

.method private updateCloudControlData()V
    .locals 6

    .line 330
    iget-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mContext:Landroid/content/Context;

    .line 331
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "perf_process"

    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataList(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 333
    .local v0, "cloudDataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;"
    if-nez v0, :cond_0

    .line 334
    return-void

    .line 336
    :cond_0
    const-string v1, ""

    .line 337
    .local v1, "data":Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 338
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    const-string v4, "perf_proc_threshold"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 339
    .local v3, "cd":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 340
    move-object v1, v3

    .line 341
    goto :goto_1

    .line 337
    .end local v3    # "cd":Ljava/lang/String;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 345
    .end local v2    # "i":I
    :cond_2
    :goto_1
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 346
    .local v2, "jsonObject":Lorg/json/JSONObject;
    invoke-direct {p0, v2}, Lcom/android/server/am/ProcessMemoryCleaner;->updateAppSwitchBgDelayTime(Lorg/json/JSONObject;)V

    .line 347
    invoke-direct {p0, v2}, Lcom/android/server/am/ProcessMemoryCleaner;->updateCommonUsedPssLimitKB(Lorg/json/JSONObject;)V

    .line 348
    invoke-direct {p0, v2}, Lcom/android/server/am/ProcessMemoryCleaner;->updateMemExceptionThresholdKB(Lorg/json/JSONObject;)V

    .line 349
    invoke-direct {p0, v2}, Lcom/android/server/am/ProcessMemoryCleaner;->updateProtectProcessList(Lorg/json/JSONObject;)V

    .line 350
    invoke-direct {p0, v2}, Lcom/android/server/am/ProcessMemoryCleaner;->updateProcessAdjBindList(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 353
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    goto :goto_2

    .line 351
    :catch_0
    move-exception v2

    .line 352
    .local v2, "e":Lorg/json/JSONException;
    const-string v3, "ProcessMemoryCleaner"

    const-string/jumbo v4, "updateCloudData error :"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 354
    .end local v2    # "e":Lorg/json/JSONException;
    :goto_2
    return-void
.end method

.method private updateCommonUsedPssLimitKB(Lorg/json/JSONObject;)V
    .locals 4
    .param p1, "jsonObject"    # Lorg/json/JSONObject;

    .line 375
    const-string v0, "perf_proc_common_pss_limit"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 376
    .local v1, "pssLimitKB":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 377
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/android/server/am/ProcessMemoryCleaner;->computeCommonUsedAppPssThreshold(J)V

    .line 378
    sget-boolean v2, Lcom/android/server/am/ProcessMemoryCleaner;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 379
    invoke-direct {p0, v0, v1}, Lcom/android/server/am/ProcessMemoryCleaner;->logCloudControlParas(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    :cond_0
    return-void
.end method

.method private updateMemExceptionThresholdKB(Lorg/json/JSONObject;)V
    .locals 4
    .param p1, "jsonObject"    # Lorg/json/JSONObject;

    .line 385
    const-string v0, "perf_proc_mem_exception_threshold"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 386
    .local v1, "memExceptionThresholdKB":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 387
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/android/server/am/ProcessMemoryCleaner;->computeMemExceptionThreshold(J)V

    .line 388
    sget-boolean v2, Lcom/android/server/am/ProcessMemoryCleaner;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 389
    invoke-direct {p0, v0, v1}, Lcom/android/server/am/ProcessMemoryCleaner;->logCloudControlParas(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    :cond_0
    return-void
.end method

.method private updateProcessAdjBindList(Lorg/json/JSONObject;)V
    .locals 3
    .param p1, "jsonObject"    # Lorg/json/JSONObject;

    .line 357
    const-string v0, "perf_proc_adj_bind_list"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 358
    .local v1, "procString":Ljava/lang/String;
    invoke-static {}, Lcom/android/server/am/OomAdjusterImpl;->getInstance()Lcom/android/server/am/OomAdjusterImpl;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/android/server/am/OomAdjusterImpl;->updateProcessAdjBindList(Ljava/lang/String;)V

    .line 359
    sget-boolean v2, Lcom/android/server/am/ProcessMemoryCleaner;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 360
    invoke-direct {p0, v0, v1}, Lcom/android/server/am/ProcessMemoryCleaner;->logCloudControlParas(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    :cond_0
    return-void
.end method

.method private updateProtectProcessList(Lorg/json/JSONObject;)V
    .locals 7
    .param p1, "jsonObject"    # Lorg/json/JSONObject;

    .line 395
    const-string v0, "perf_proc_protect_list"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 396
    .local v1, "processString":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 397
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 398
    .local v2, "processArray":[Ljava/lang/String;
    array-length v3, v2

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_0

    aget-object v5, v2, v4

    .line 399
    .local v5, "processName":Ljava/lang/String;
    iget-object v6, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    invoke-virtual {v6, v5}, Lcom/android/server/am/ProcessPolicy;->updateSystemCleanWhiteList(Ljava/lang/String;)V

    .line 398
    .end local v5    # "processName":Ljava/lang/String;
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 401
    :cond_0
    sget-boolean v3, Lcom/android/server/am/ProcessMemoryCleaner;->DEBUG:Z

    if-eqz v3, :cond_1

    .line 402
    invoke-direct {p0, v0, v1}, Lcom/android/server/am/ProcessMemoryCleaner;->logCloudControlParas(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    .end local v2    # "processArray":[Ljava/lang/String;
    :cond_1
    return-void
.end method


# virtual methods
.method public KillProcessForPadSmallWindowMode(Ljava/lang/String;)V
    .locals 8
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 484
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 485
    .local v0, "nowTime":J
    iget-object v2, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mHandler:Lcom/android/server/am/ProcessMemoryCleaner$H;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/android/server/am/ProcessMemoryCleaner$H;->hasMessages(I)Z

    move-result v2

    if-nez v2, :cond_1

    iget-wide v4, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mLastPadSmallWindowUpdateTime:J

    sub-long v4, v0, v4

    const-wide/16 v6, 0x1388

    cmp-long v2, v4, v6

    if-gez v2, :cond_0

    goto :goto_0

    .line 489
    :cond_0
    iput-wide v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mLastPadSmallWindowUpdateTime:J

    .line 490
    iget-object v2, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mHandler:Lcom/android/server/am/ProcessMemoryCleaner$H;

    invoke-virtual {v2, v3}, Lcom/android/server/am/ProcessMemoryCleaner$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 491
    .local v2, "msg":Landroid/os/Message;
    iput-object p1, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 492
    iget-object v3, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mHandler:Lcom/android/server/am/ProcessMemoryCleaner$H;

    invoke-virtual {v3, v2}, Lcom/android/server/am/ProcessMemoryCleaner$H;->sendMessage(Landroid/os/Message;)Z

    .line 493
    return-void

    .line 487
    .end local v2    # "msg":Landroid/os/Message;
    :cond_1
    :goto_0
    return-void
.end method

.method public compactBackgroundProcess(ILjava/lang/String;)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "processName"    # Ljava/lang/String;

    .line 579
    iget-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-interface {v0, p1, p2}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->getRunningProcess(ILjava/lang/String;)Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    move-result-object v0

    .line 580
    .local v0, "proc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/server/am/ProcessMemoryCleaner;->compactBackgroundProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;Z)V

    .line 581
    return-void
.end method

.method public compactBackgroundProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;Z)V
    .locals 4
    .param p1, "proc"    # Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .param p2, "isDelayed"    # Z

    .line 584
    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessMemoryCleaner;->isNeedCompact(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 585
    const/4 v0, 0x4

    if-nez p2, :cond_0

    .line 586
    iget-object v1, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mHandler:Lcom/android/server/am/ProcessMemoryCleaner$H;

    invoke-virtual {v1, v0, p1}, Lcom/android/server/am/ProcessMemoryCleaner$H;->removeMessages(ILjava/lang/Object;)V

    .line 587
    iget-object v1, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mHandler:Lcom/android/server/am/ProcessMemoryCleaner$H;

    invoke-virtual {v1, v0, p1}, Lcom/android/server/am/ProcessMemoryCleaner$H;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 588
    .local v0, "msg":Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 589
    iget-object v1, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mHandler:Lcom/android/server/am/ProcessMemoryCleaner$H;

    invoke-virtual {v1, v0}, Lcom/android/server/am/ProcessMemoryCleaner$H;->sendMessage(Landroid/os/Message;)Z

    .end local v0    # "msg":Landroid/os/Message;
    goto :goto_0

    .line 590
    :cond_0
    iget-object v1, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mHandler:Lcom/android/server/am/ProcessMemoryCleaner$H;

    invoke-virtual {v1, v0, p1}, Lcom/android/server/am/ProcessMemoryCleaner$H;->hasMessages(ILjava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 591
    iget-object v1, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mHandler:Lcom/android/server/am/ProcessMemoryCleaner$H;

    invoke-virtual {v1, v0, p1}, Lcom/android/server/am/ProcessMemoryCleaner$H;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 592
    .restart local v0    # "msg":Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 593
    iget-object v1, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mHandler:Lcom/android/server/am/ProcessMemoryCleaner$H;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/am/ProcessMemoryCleaner$H;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_1

    .line 590
    .end local v0    # "msg":Landroid/os/Message;
    :cond_1
    :goto_0
    nop

    .line 596
    :cond_2
    :goto_1
    return-void
.end method

.method public foregroundActivityChanged(Lcom/android/server/am/ControllerActivityInfo;)V
    .locals 2
    .param p1, "info"    # Lcom/android/server/am/ControllerActivityInfo;

    .line 570
    iget-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mHandler:Lcom/android/server/am/ProcessMemoryCleaner$H;

    if-nez v0, :cond_0

    return-void

    .line 571
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mMiuiMemoryService:Lcom/android/server/am/MiuiMemoryServiceInternal;

    iget v1, p1, Lcom/android/server/am/ControllerActivityInfo;->launchPid:I

    invoke-interface {v0, v1}, Lcom/android/server/am/MiuiMemoryServiceInternal;->interruptProcCompaction(I)V

    .line 572
    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessMemoryCleaner;->sendAppSwitchBgExceptionMsg(Lcom/android/server/am/ControllerActivityInfo;)V

    .line 573
    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessMemoryCleaner;->sendGlobalCompactMsg(Lcom/android/server/am/ControllerActivityInfo;)V

    .line 574
    iget-object v0, p1, Lcom/android/server/am/ControllerActivityInfo;->launchPkg:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mForegroundPkg:Ljava/lang/String;

    .line 575
    iget v0, p1, Lcom/android/server/am/ControllerActivityInfo;->launchUid:I

    iput v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mForegroundUid:I

    .line 576
    return-void
.end method

.method public getProcMemStat()Lcom/android/server/am/ProcMemCleanerStatistics;
    .locals 1

    .line 527
    iget-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mStatistics:Lcom/android/server/am/ProcMemCleanerStatistics;

    return-object v0
.end method

.method public killPackage(Lcom/miui/server/smartpower/IAppState$IRunningProcess;ILjava/lang/String;)J
    .locals 15
    .param p1, "proc"    # Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .param p2, "minAdj"    # I
    .param p3, "reason"    # Ljava/lang/String;

    .line 419
    move-object v0, p0

    move/from16 v1, p2

    invoke-virtual/range {p1 .. p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;

    move-result-object v2

    const-wide/16 v3, 0x0

    if-nez v2, :cond_0

    .line 420
    return-wide v3

    .line 423
    :cond_0
    const-wide/16 v5, 0x0

    .line 424
    .local v5, "appCurPss":J
    iget-object v2, v0, Lcom/android/server/am/ProcessMemoryCleaner;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    .line 425
    invoke-virtual/range {p1 .. p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUid()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v2, v7, v8}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->getLruProcesses(ILjava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 426
    .local v2, "runningAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
    const/4 v7, 0x0

    .line 427
    .local v7, "mainProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 428
    .local v9, "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    invoke-virtual {v9}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPss()J

    move-result-wide v10

    add-long/2addr v5, v10

    .line 429
    invoke-direct {p0, v9, v1}, Lcom/android/server/am/ProcessMemoryCleaner;->checkRunningProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)Z

    move-result v10

    if-nez v10, :cond_1

    .line 430
    return-wide v3

    .line 432
    :cond_1
    invoke-virtual {v9}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 433
    move-object v7, v9

    .line 435
    .end local v9    # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    :cond_2
    goto :goto_0

    .line 437
    :cond_3
    if-eqz v7, :cond_4

    invoke-direct {p0, v7, v1}, Lcom/android/server/am/ProcessMemoryCleaner;->checkRunningProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 438
    invoke-virtual {v7}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUserId()I

    move-result v4

    .line 439
    invoke-direct {p0, v7}, Lcom/android/server/am/ProcessMemoryCleaner;->getKillReason(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Ljava/lang/String;

    move-result-object v8

    .line 438
    invoke-virtual {p0, v3, v4, v8}, Lcom/android/server/am/ProcessMemoryCleaner;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V

    .line 440
    iget-object v9, v0, Lcom/android/server/am/ProcessMemoryCleaner;->mStatistics:Lcom/android/server/am/ProcMemCleanerStatistics;

    const/4 v10, 0x2

    move-object v11, v7

    move-wide v12, v5

    move-object/from16 v14, p3

    invoke-virtual/range {v9 .. v14}, Lcom/android/server/am/ProcMemCleanerStatistics;->reportEvent(ILcom/miui/server/smartpower/IAppState$IRunningProcess;JLjava/lang/String;)V

    .line 442
    return-wide v5

    .line 444
    :cond_4
    return-wide v3
.end method

.method public killPackage(Lcom/miui/server/smartpower/IAppState$IRunningProcess;Ljava/lang/String;)J
    .locals 2
    .param p1, "runningProc"    # Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .param p2, "reason"    # Ljava/lang/String;

    .line 415
    const/16 v0, 0xc8

    invoke-virtual {p0, p1, v0, p2}, Lcom/android/server/am/ProcessMemoryCleaner;->killPackage(Lcom/miui/server/smartpower/IAppState$IRunningProcess;ILjava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public killProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;ILjava/lang/String;)J
    .locals 10
    .param p1, "runningProc"    # Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .param p2, "minAdj"    # I
    .param p3, "reason"    # Ljava/lang/String;

    .line 460
    invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessMemoryCleaner;->isCurrentProcessInBackup(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z

    move-result v0

    const-wide/16 v1, 0x0

    if-eqz v0, :cond_0

    .line 461
    return-wide v1

    .line 463
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mAMS:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v0

    .line 464
    :try_start_0
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;

    move-result-object v3

    .line 465
    .local v3, "proc":Lcom/android/server/am/ProcessRecord;
    invoke-direct {p0, p1, p2}, Lcom/android/server/am/ProcessMemoryCleaner;->checkRunningProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-direct {p0, v3}, Lcom/android/server/am/ProcessMemoryCleaner;->isRunningComponent(Lcom/android/server/am/ProcessRecord;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 466
    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessMemoryCleaner;->getKillReason(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v3, v1}, Lcom/android/server/am/ProcessMemoryCleaner;->killApplicationLock(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V

    .line 467
    iget-object v4, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mStatistics:Lcom/android/server/am/ProcMemCleanerStatistics;

    const/4 v5, 0x1

    .line 468
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPss()J

    move-result-wide v7

    .line 467
    move-object v6, p1

    move-object v9, p3

    invoke-virtual/range {v4 .. v9}, Lcom/android/server/am/ProcMemCleanerStatistics;->reportEvent(ILcom/miui/server/smartpower/IAppState$IRunningProcess;JLjava/lang/String;)V

    .line 469
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPss()J

    move-result-wide v1

    monitor-exit v0

    return-wide v1

    .line 471
    .end local v3    # "proc":Lcom/android/server/am/ProcessRecord;
    :cond_1
    monitor-exit v0

    .line 472
    return-wide v1

    .line 471
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public killProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;Ljava/lang/String;)J
    .locals 2
    .param p1, "runningProc"    # Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .param p2, "reason"    # Ljava/lang/String;

    .line 452
    const/16 v0, 0xc8

    invoke-virtual {p0, p1, v0, p2}, Lcom/android/server/am/ProcessMemoryCleaner;->killProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;ILjava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public onApplyOomAdjLocked(Lcom/android/server/am/ProcessRecord;)V
    .locals 2
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 184
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getSetAdj()I

    move-result v0

    if-gtz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mMiuiMemoryService:Lcom/android/server/am/MiuiMemoryServiceInternal;

    iget v1, p1, Lcom/android/server/am/ProcessRecord;->mPid:I

    invoke-interface {v0, v1}, Lcom/android/server/am/MiuiMemoryServiceInternal;->interruptProcCompaction(I)V

    .line 187
    :cond_0
    return-void
.end method

.method public onBootPhase()V
    .locals 2

    .line 136
    iget-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mHandler:Lcom/android/server/am/ProcessMemoryCleaner$H;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessMemoryCleaner$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 137
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mHandler:Lcom/android/server/am/ProcessMemoryCleaner$H;

    invoke-virtual {v1, v0}, Lcom/android/server/am/ProcessMemoryCleaner$H;->sendMessage(Landroid/os/Message;)Z

    .line 138
    return-void
.end method

.method public scanProcessAndCleanUpMemory(J)Z
    .locals 10
    .param p1, "targetReleaseMem"    # J

    .line 145
    iget-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 146
    const/4 v0, 0x0

    return v0

    .line 148
    :cond_0
    sget-boolean v0, Lcom/android/server/am/ProcessMemoryCleaner;->DEBUG:Z

    if-eqz v0, :cond_1

    const-string v0, "ProcessMemoryCleaner"

    const-string v1, "Start clean up memory....."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 151
    .local v0, "runningProcList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
    iget-object v1, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    .line 152
    invoke-interface {v1}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->getAllAppState()Ljava/util/ArrayList;

    move-result-object v1

    .line 153
    .local v1, "appStateList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/smartpower/IAppState;

    .line 154
    .local v3, "appState":Lcom/miui/server/smartpower/IAppState;
    invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState;->isVsible()Z

    move-result v4

    if-nez v4, :cond_4

    .line 155
    invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState;->getRunningProcessList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 156
    .local v5, "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I

    move-result v6

    if-lez v6, :cond_3

    .line 157
    invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I

    move-result v6

    const/16 v7, 0xc8

    if-lt v6, v7, :cond_2

    .line 158
    invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I

    move-result v6

    const/16 v7, 0xfa

    if-le v6, v7, :cond_3

    .line 159
    :cond_2
    invoke-direct {p0, v5}, Lcom/android/server/am/ProcessMemoryCleaner;->isIsolatedByAdj(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 160
    invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v6

    .line 161
    invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;

    move-result-object v7

    .line 160
    invoke-direct {p0, v6, v7}, Lcom/android/server/am/ProcessMemoryCleaner;->isImportantSubProc(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 162
    invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->isProcessPerceptible()Z

    move-result v6

    if-nez v6, :cond_3

    .line 163
    invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->hasForegrundService()Z

    move-result v6

    if-nez v6, :cond_3

    .line 164
    invoke-direct {p0, v5}, Lcom/android/server/am/ProcessMemoryCleaner;->isSystemHighPrioProc(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 165
    invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPid()I

    move-result v6

    invoke-direct {p0, v6}, Lcom/android/server/am/ProcessMemoryCleaner;->isLastMusicPlayProcess(I)Z

    move-result v6

    if-nez v6, :cond_3

    .line 166
    invoke-direct {p0, v5}, Lcom/android/server/am/ProcessMemoryCleaner;->containInWhiteList(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 167
    invoke-direct {p0, v3, v5}, Lcom/android/server/am/ProcessMemoryCleaner;->isAutoStartProcess(Lcom/miui/server/smartpower/IAppState;Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z

    move-result v6

    if-nez v6, :cond_3

    iget-object v6, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    .line 169
    invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;

    move-result-object v8

    .line 168
    const/16 v9, 0x1d8

    invoke-interface {v6, v9, v7, v8}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->isProcessWhiteList(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 170
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    .end local v5    # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    :cond_3
    goto :goto_1

    .line 174
    .end local v3    # "appState":Lcom/miui/server/smartpower/IAppState;
    :cond_4
    goto/16 :goto_0

    .line 175
    :cond_5
    invoke-direct {p0, v0, p1, p2}, Lcom/android/server/am/ProcessMemoryCleaner;->cleanUpMemory(Ljava/util/List;J)Z

    move-result v2

    return v2
.end method

.method public systemReady(Landroid/content/Context;Lcom/android/server/am/ProcessManagerService;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pms"    # Lcom/android/server/am/ProcessManagerService;

    .line 120
    invoke-super {p0, p1, p2}, Lcom/android/server/am/ProcessCleanerBase;->systemReady(Landroid/content/Context;Lcom/android/server/am/ProcessManagerService;)V

    .line 121
    sget-boolean v0, Lcom/android/server/am/ProcessMemoryCleaner;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "ProcessMemoryCleaner"

    const-string v1, "ProcessesCleaner init"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    :cond_0
    iput-object p1, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mContext:Landroid/content/Context;

    .line 123
    iput-object p2, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mPMS:Lcom/android/server/am/ProcessManagerService;

    .line 124
    iget-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mHandlerTh:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 125
    new-instance v0, Lcom/android/server/am/ProcessMemoryCleaner$H;

    iget-object v1, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mHandlerTh:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/am/ProcessMemoryCleaner$H;-><init>(Lcom/android/server/am/ProcessMemoryCleaner;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mHandler:Lcom/android/server/am/ProcessMemoryCleaner$H;

    .line 126
    iget-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mHandlerTh:Landroid/os/HandlerThread;

    .line 127
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getThreadId()I

    move-result v0

    .line 126
    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/Process;->setThreadGroupAndCpuset(II)V

    .line 128
    iget-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mPMS:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessManagerService;->getProcessPolicy()Lcom/android/server/am/ProcessPolicy;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    .line 129
    const-class v0, Lcom/android/server/am/MiuiMemoryServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/MiuiMemoryServiceInternal;

    iput-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mMiuiMemoryService:Lcom/android/server/am/MiuiMemoryServiceInternal;

    .line 130
    invoke-static {}, Lcom/android/server/am/ProcMemCleanerStatistics;->getInstance()Lcom/android/server/am/ProcMemCleanerStatistics;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mStatistics:Lcom/android/server/am/ProcMemCleanerStatistics;

    .line 131
    sget-wide v0, Landroid/os/spc/PressureStateSettings;->PROC_MEM_EXCEPTION_PSS_LIMIT_KB:J

    invoke-direct {p0, v0, v1}, Lcom/android/server/am/ProcessMemoryCleaner;->computeMemExceptionThreshold(J)V

    .line 132
    iget-wide v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mCommonUsedPssLimitKB:J

    invoke-direct {p0, v0, v1}, Lcom/android/server/am/ProcessMemoryCleaner;->computeCommonUsedAppPssThreshold(J)V

    .line 133
    return-void
.end method
