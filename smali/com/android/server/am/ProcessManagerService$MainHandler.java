class com.android.server.am.ProcessManagerService$MainHandler extends android.os.Handler {
	 /* .source "ProcessManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/ProcessManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "MainHandler" */
} // .end annotation
/* # instance fields */
final com.android.server.am.ProcessManagerService this$0; //synthetic
/* # direct methods */
public com.android.server.am.ProcessManagerService$MainHandler ( ) {
/* .locals 2 */
/* .param p1, "this$0" # Lcom/android/server/am/ProcessManagerService; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 243 */
this.this$0 = p1;
/* .line 244 */
int v0 = 0; // const/4 v0, 0x0
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {p0, p2, v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;Z)V */
/* .line 245 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 3 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 249 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 251 */
/* :pswitch_0 */
v0 = this.this$0;
com.android.server.am.ProcessManagerService .-$$Nest$fgetmActivityManagerService ( v0 );
/* monitor-enter v0 */
/* .line 252 */
try { // :try_start_0
	 v1 = this.this$0;
	 com.android.server.am.ProcessManagerService .-$$Nest$fgetmProcessStarter ( v1 );
	 /* iget v2, p1, Landroid/os/Message;->arg1:I */
	 (( com.android.server.am.ProcessStarter ) v1 ).restoreLastProcessesInfoLocked ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessStarter;->restoreLastProcessesInfoLocked(I)V
	 /* .line 253 */
	 /* monitor-exit v0 */
	 /* .line 254 */
	 /* .line 253 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
	 /* .line 258 */
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
