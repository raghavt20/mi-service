.class Lcom/android/server/am/PeriodicCleanerService$MyHandler;
.super Landroid/os/Handler;
.source "PeriodicCleanerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/PeriodicCleanerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/PeriodicCleanerService;


# direct methods
.method public constructor <init>(Lcom/android/server/am/PeriodicCleanerService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 1921
    iput-object p1, p0, Lcom/android/server/am/PeriodicCleanerService$MyHandler;->this$0:Lcom/android/server/am/PeriodicCleanerService;

    .line 1922
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1923
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .line 1927
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 1947
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/am/PeriodicCleanerService$MyHandler;->this$0:Lcom/android/server/am/PeriodicCleanerService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/android/server/am/PeriodicCleanerService;->-$$Nest$mreportCleanProcess(Lcom/android/server/am/PeriodicCleanerService;IILjava/lang/String;)V

    .line 1948
    goto :goto_0

    .line 1943
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/am/PeriodicCleanerService$MyHandler;->this$0:Lcom/android/server/am/PeriodicCleanerService;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/server/am/PeriodicCleanerService;->-$$Nest$mreportStartProcess(Lcom/android/server/am/PeriodicCleanerService;Ljava/lang/String;)V

    .line 1944
    goto :goto_0

    .line 1939
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/am/PeriodicCleanerService$MyHandler;->this$0:Lcom/android/server/am/PeriodicCleanerService;

    invoke-static {v0}, Lcom/android/server/am/PeriodicCleanerService;->-$$Nest$mhandleScreenOff(Lcom/android/server/am/PeriodicCleanerService;)V

    .line 1940
    goto :goto_0

    .line 1935
    :pswitch_4
    iget-object v0, p0, Lcom/android/server/am/PeriodicCleanerService$MyHandler;->this$0:Lcom/android/server/am/PeriodicCleanerService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1}, Lcom/android/server/am/PeriodicCleanerService;->-$$Nest$mreportMemPressure(Lcom/android/server/am/PeriodicCleanerService;I)V

    .line 1936
    goto :goto_0

    .line 1929
    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Lcom/android/server/am/PeriodicCleanerService$MyEvent;

    if-eqz v0, :cond_0

    .line 1930
    iget-object v0, p0, Lcom/android/server/am/PeriodicCleanerService$MyHandler;->this$0:Lcom/android/server/am/PeriodicCleanerService;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/android/server/am/PeriodicCleanerService$MyEvent;

    invoke-static {v0, v1}, Lcom/android/server/am/PeriodicCleanerService;->-$$Nest$mreportEvent(Lcom/android/server/am/PeriodicCleanerService;Lcom/android/server/am/PeriodicCleanerService$MyEvent;)V

    .line 1953
    :cond_0
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
