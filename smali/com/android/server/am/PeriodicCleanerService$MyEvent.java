class com.android.server.am.PeriodicCleanerService$MyEvent {
	 /* .source "PeriodicCleanerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/PeriodicCleanerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "MyEvent" */
} // .end annotation
/* # instance fields */
public java.lang.String mClass;
public Integer mEventType;
public Boolean mFullScreen;
public java.lang.String mPackage;
public Integer mUid;
final com.android.server.am.PeriodicCleanerService this$0; //synthetic
/* # direct methods */
public com.android.server.am.PeriodicCleanerService$MyEvent ( ) {
/* .locals 0 */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "className" # Ljava/lang/String; */
/* .param p4, "uid" # I */
/* .param p5, "event" # I */
/* .param p6, "fullscreen" # Z */
/* .line 1996 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 1997 */
this.mPackage = p2;
/* .line 1998 */
this.mClass = p3;
/* .line 1999 */
/* iput p5, p0, Lcom/android/server/am/PeriodicCleanerService$MyEvent;->mEventType:I */
/* .line 2000 */
/* iput-boolean p6, p0, Lcom/android/server/am/PeriodicCleanerService$MyEvent;->mFullScreen:Z */
/* .line 2001 */
/* iput p4, p0, Lcom/android/server/am/PeriodicCleanerService$MyEvent;->mUid:I */
/* .line 2002 */
return;
} // .end method
