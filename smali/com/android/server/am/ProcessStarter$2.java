class com.android.server.am.ProcessStarter$2 implements java.lang.Runnable {
	 /* .source "ProcessStarter.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/am/ProcessStarter;->restartCameraIfNeeded(Ljava/lang/String;Ljava/lang/String;I)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.am.ProcessStarter this$0; //synthetic
final java.lang.String val$packageName; //synthetic
final java.lang.String val$processName; //synthetic
final Integer val$uid; //synthetic
/* # direct methods */
 com.android.server.am.ProcessStarter$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/ProcessStarter; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 341 */
this.this$0 = p1;
this.val$packageName = p2;
/* iput p3, p0, Lcom/android/server/am/ProcessStarter$2;->val$uid:I */
this.val$processName = p4;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 7 */
/* .line 344 */
v0 = this.this$0;
com.android.server.am.ProcessStarter .-$$Nest$fgetmPms ( v0 );
v1 = this.val$packageName;
/* iget v2, p0, Lcom/android/server/am/ProcessStarter$2;->val$uid:I */
v0 = (( com.android.server.am.ProcessManagerService ) v0 ).isAllowAutoStart ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/am/ProcessManagerService;->isAllowAutoStart(Ljava/lang/String;I)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 345 */
v0 = this.this$0;
v0 = this.mKilledProcessRecordMap;
v1 = this.val$processName;
/* check-cast v0, Ljava/lang/Integer; */
/* .line 346 */
/* .local v0, "killCount":Ljava/lang/Integer; */
if ( v0 != null) { // if-eqz v0, :cond_0
	 v1 = 	 (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
	 int v2 = 3; // const/4 v2, 0x3
	 /* if-lt v1, v2, :cond_0 */
	 /* .line 347 */
	 v1 = 	 com.android.server.am.ProcessUtils .isLowMemory ( );
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 348 */
		 final String v1 = "ProcessStarter"; // const-string v1, "ProcessStarter"
		 /* new-instance v2, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
		 /* const-string/jumbo v3, "skip restart " */
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v3 = this.val$processName;
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 final String v3 = " due to too much kill in low memory condition!"; // const-string v3, " due to too much kill in low memory condition!"
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Log .w ( v1,v2 );
		 /* .line 350 */
		 return;
		 /* .line 352 */
	 } // :cond_0
	 v1 = this.this$0;
	 com.android.server.am.ProcessStarter .-$$Nest$fgetmAms ( v1 );
	 /* monitor-enter v1 */
	 /* .line 353 */
	 try { // :try_start_0
		 v2 = this.this$0;
		 v3 = this.val$packageName;
		 v4 = this.val$processName;
		 /* iget v5, p0, Lcom/android/server/am/ProcessStarter$2;->val$uid:I */
		 /* .line 354 */
		 v5 = 		 android.os.UserHandle .getUserId ( v5 );
		 int v6 = 2; // const/4 v6, 0x2
		 com.android.server.am.ProcessStarter .makeHostingTypeFromFlag ( v6 );
		 /* .line 353 */
		 com.android.server.am.ProcessStarter .-$$Nest$mstartProcessLocked ( v2,v3,v4,v5,v6 );
		 /* .line 356 */
		 /* monitor-exit v1 */
		 /* :catchall_0 */
		 /* move-exception v2 */
		 /* monitor-exit v1 */
		 /* :try_end_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
		 /* throw v2 */
		 /* .line 358 */
	 } // .end local v0 # "killCount":Ljava/lang/Integer;
} // :cond_1
} // :goto_0
return;
} // .end method
