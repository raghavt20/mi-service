.class Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;
.super Ljava/lang/Object;
.source "PeriodicCleanerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/PeriodicCleanerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PackageUseInfo"
.end annotation


# instance fields
.field private mBackgroundTime:J

.field private mFgTrimDone:Z

.field private mPackageName:Ljava/lang/String;

.field private mUid:I

.field private mUserId:I

.field final synthetic this$0:Lcom/android/server/am/PeriodicCleanerService;


# direct methods
.method static bridge synthetic -$$Nest$fgetmBackgroundTime(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mBackgroundTime:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmFgTrimDone(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mFgTrimDone:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmPackageName(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mPackageName:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmUid(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mUid:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmUserId(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mUserId:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmBackgroundTime(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;J)V
    .locals 0

    iput-wide p1, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mBackgroundTime:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmFgTrimDone(Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mFgTrimDone:Z

    return-void
.end method

.method public constructor <init>(Lcom/android/server/am/PeriodicCleanerService;ILjava/lang/String;J)V
    .locals 0
    .param p2, "uid"    # I
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "backgroundTime"    # J

    .line 1885
    iput-object p1, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->this$0:Lcom/android/server/am/PeriodicCleanerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1886
    iput-object p3, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mPackageName:Ljava/lang/String;

    .line 1887
    invoke-static {p2}, Landroid/os/UserHandle;->getUserId(I)I

    move-result p1

    iput p1, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mUserId:I

    .line 1888
    iput p2, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mUid:I

    .line 1889
    iput-wide p4, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mBackgroundTime:J

    .line 1890
    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mFgTrimDone:Z

    .line 1891
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .line 1906
    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    .line 1907
    return v0

    .line 1910
    :cond_0
    instance-of v1, p1, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;

    if-eqz v1, :cond_1

    .line 1911
    move-object v1, p1

    check-cast v1, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;

    .line 1912
    .local v1, "another":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;
    iget v2, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mUid:I

    iget v3, v1, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mUid:I

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mPackageName:Ljava/lang/String;

    iget-object v3, v1, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1913
    return v0

    .line 1916
    .end local v1    # "another":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1894
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy-MM-dd HH:mm:ss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1895
    .local v0, "dateformat":Ljava/text/SimpleDateFormat;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "u"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mUserId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mUid:I

    invoke-static {v2}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1896
    iget-boolean v3, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mFgTrimDone:Z

    if-eqz v3, :cond_0

    const-string v3, "(trimed)"

    goto :goto_0

    :cond_0
    const-string v3, "(non-trimed)"

    :goto_0
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mBackgroundTime:J

    .line 1897
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1895
    return-object v1
.end method

.method public updateUid(I)V
    .locals 1
    .param p1, "uid"    # I

    .line 1901
    iput p1, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mUid:I

    .line 1902
    invoke-static {p1}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v0

    iput v0, p0, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->mUserId:I

    .line 1903
    return-void
.end method
