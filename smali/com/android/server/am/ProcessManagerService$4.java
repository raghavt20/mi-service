class com.android.server.am.ProcessManagerService$4 implements java.lang.Runnable {
	 /* .source "ProcessManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/am/ProcessManagerService;->notifyForegroundWindowChanged(Lcom/android/server/wm/FgWindowChangedInfo;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.am.ProcessManagerService this$0; //synthetic
final com.android.server.wm.FgWindowChangedInfo val$fgInfo; //synthetic
/* # direct methods */
 com.android.server.am.ProcessManagerService$4 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/ProcessManagerService; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 1026 */
this.this$0 = p1;
this.val$fgInfo = p2;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 2 */
/* .line 1029 */
v0 = this.this$0;
com.android.server.am.ProcessManagerService .-$$Nest$fgetmForegroundInfoManager ( v0 );
v1 = this.val$fgInfo;
(( com.android.server.wm.ForegroundInfoManager ) v0 ).notifyForegroundWindowChanged ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/ForegroundInfoManager;->notifyForegroundWindowChanged(Lcom/android/server/wm/FgWindowChangedInfo;)V
/* .line 1030 */
return;
} // .end method
