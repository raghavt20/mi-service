public class com.android.server.am.GameProcessCompactor implements com.android.server.am.IGameProcessAction {
	 /* .source "GameProcessCompactor.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;, */
	 /* Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Boolean DEBUG;
private static final java.lang.String TAG;
/* # instance fields */
private com.android.server.am.GameProcessCompactor$GameProcessCompactorConfig mConfig;
private com.android.server.am.GameMemoryReclaimer mReclaimer;
/* # direct methods */
static java.lang.String -$$Nest$sfgetTAG ( ) { //bridge//synthethic
	 /* .locals 1 */
	 v0 = com.android.server.am.GameProcessCompactor.TAG;
} // .end method
static com.android.server.am.GameProcessCompactor ( ) {
	 /* .locals 1 */
	 /* .line 31 */
	 /* const-class v0, Lcom/android/server/am/GameProcessCompactor; */
	 (( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
	 return;
} // .end method
 com.android.server.am.GameProcessCompactor ( ) {
	 /* .locals 0 */
	 /* .param p1, "reclaimer" # Lcom/android/server/am/GameMemoryReclaimer; */
	 /* .param p2, "cfg" # Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig; */
	 /* .line 37 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 38 */
	 this.mReclaimer = p1;
	 /* .line 39 */
	 this.mConfig = p2;
	 /* .line 40 */
	 return;
} // .end method
/* # virtual methods */
public Long doAction ( Long p0 ) {
	 /* .locals 15 */
	 /* .param p1, "need" # J */
	 /* .line 72 */
	 /* move-object v0, p0 */
	 int v1 = 0; // const/4 v1, 0x0
	 /* .line 77 */
	 /* .local v1, "num":I */
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v3 = "doCompact:"; // const-string v3, "doCompact:"
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v3 = this.mConfig;
	 /* iget v3, v3, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mPrio:I */
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 /* const-wide/32 v3, 0x80000 */
	 android.os.Trace .traceBegin ( v3,v4,v2 );
	 /* .line 78 */
	 android.os.SystemClock .uptimeMillis ( );
	 /* move-result-wide v5 */
	 /* .line 79 */
	 /* .local v5, "time":J */
	 /* const-wide/16 v7, 0x0 */
	 /* .line 80 */
	 /* .local v7, "reclaim":J */
	 v2 = this.mReclaimer;
	 (( com.android.server.am.GameMemoryReclaimer ) v2 ).filterProcessInfos ( p0 ); // invoke-virtual {v2, p0}, Lcom/android/server/am/GameMemoryReclaimer;->filterProcessInfos(Lcom/android/server/am/IGameProcessAction;)Ljava/util/List;
	 /* .line 81 */
	 /* .local v2, "pidList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;>;" */
		 v9 = 	 if ( v2 != null) { // if-eqz v2, :cond_1
		 /* if-lez v9, :cond_1 */
		 /* .line 82 */
	 v10 = 	 } // :goto_0
	 if ( v10 != null) { // if-eqz v10, :cond_1
		 /* check-cast v10, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo; */
		 /* .line 83 */
		 /* .local v10, "compInfo":Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo; */
		 (( com.android.server.am.GameProcessCompactor$ProcessCompactInfo ) v10 ).compact ( p0 ); // invoke-virtual {v10, p0}, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->compact(Lcom/android/server/am/GameProcessCompactor;)J
		 /* move-result-wide v11 */
		 /* .line 84 */
		 /* .local v11, "income":J */
		 /* const-wide/16 v13, 0x0 */
		 /* cmp-long v13, v11, v13 */
		 /* if-ltz v13, :cond_0 */
		 /* .line 85 */
		 /* add-int/lit8 v1, v1, 0x1 */
		 /* .line 86 */
		 /* add-long/2addr v7, v11 */
		 /* .line 87 */
		 /* cmp-long v13, v7, p1 */
		 /* if-gez v13, :cond_1 */
		 v13 = this.mConfig;
		 /* iget v13, v13, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mCompactMaxNum:I */
		 /* if-lez v13, :cond_0 */
		 v13 = this.mConfig;
		 /* iget v13, v13, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mCompactMaxNum:I */
		 /* if-lt v1, v13, :cond_0 */
		 /* .line 89 */
		 /* .line 91 */
	 } // .end local v10 # "compInfo":Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;
} // :cond_0
/* .line 93 */
} // .end local v11 # "income":J
} // :cond_1
} // :goto_1
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v9 */
/* sub-long/2addr v9, v5 */
/* .line 94 */
} // .end local v5 # "time":J
/* .local v9, "time":J */
v5 = com.android.server.am.GameProcessCompactor.TAG;
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "compact "; // const-string v11, "compact "
(( java.lang.StringBuilder ) v6 ).append ( v11 ); // invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v1 ); // invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v11 = " processes, and reclaim mem: "; // const-string v11, " processes, and reclaim mem: "
(( java.lang.StringBuilder ) v6 ).append ( v11 ); // invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v11 = ", during "; // const-string v11, ", during "
(( java.lang.StringBuilder ) v6 ).append ( v11 ); // invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v9, v10 ); // invoke-virtual {v6, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v11 = "ms"; // const-string v11, "ms"
(( java.lang.StringBuilder ) v6 ).append ( v11 ); // invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v5,v6 );
/* .line 95 */
android.os.Trace .traceEnd ( v3,v4 );
/* .line 96 */
/* return-wide v7 */
} // .end method
Long getIncomePctThreshold ( ) {
/* .locals 2 */
/* .line 104 */
v0 = this.mConfig;
/* iget v0, v0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mCompactIncomePctThreshold:I */
/* int-to-long v0, v0 */
/* return-wide v0 */
} // .end method
Long getIntervalThreshold ( ) {
/* .locals 2 */
/* .line 100 */
v0 = this.mConfig;
/* iget v0, v0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mCompactIntervalThreshold:I */
/* int-to-long v0, v0 */
/* return-wide v0 */
} // .end method
Integer getPrio ( ) {
/* .locals 1 */
/* .line 108 */
v0 = this.mConfig;
/* iget v0, v0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mPrio:I */
} // .end method
public Boolean shouldSkip ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 4 */
/* .param p1, "proc" # Lcom/android/server/am/ProcessRecord; */
/* .line 44 */
v0 = this.mConfig;
/* iget-boolean v0, v0, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mSkipActive:Z */
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 45 */
com.miui.server.process.ProcessManagerInternal .getInstance ( );
/* .line 46 */
(( com.miui.server.process.ProcessManagerInternal ) v0 ).getProcessPolicy ( ); // invoke-virtual {v0}, Lcom/miui/server/process/ProcessManagerInternal;->getProcessPolicy()Lcom/android/server/am/IProcessPolicy;
/* iget v2, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
v0 = java.lang.Integer .valueOf ( v2 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 47 */
/* .line 51 */
} // :cond_0
try { // :try_start_0
com.miui.server.process.ProcessManagerInternal .getInstance ( );
(( com.miui.server.process.ProcessManagerInternal ) v0 ).getForegroundInfo ( ); // invoke-virtual {v0}, Lcom/miui/server/process/ProcessManagerInternal;->getForegroundInfo()Lmiui/process/ForegroundInfo;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 54 */
/* .local v0, "foregroundInfo":Lmiui/process/ForegroundInfo; */
/* .line 52 */
} // .end local v0 # "foregroundInfo":Lmiui/process/ForegroundInfo;
/* :catch_0 */
/* move-exception v0 */
/* .line 53 */
/* .local v0, "ex":Landroid/os/RemoteException; */
int v2 = 0; // const/4 v2, 0x0
/* move-object v0, v2 */
/* .line 55 */
/* .local v0, "foregroundInfo":Lmiui/process/ForegroundInfo; */
} // :goto_0
/* if-nez v0, :cond_1 */
/* .line 56 */
/* .line 58 */
} // :cond_1
/* iget v2, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
/* iget v3, v0, Lmiui/process/ForegroundInfo;->mForegroundUid:I */
/* if-eq v2, v3, :cond_8 */
/* iget v2, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
/* iget v3, v0, Lmiui/process/ForegroundInfo;->mMultiWindowForegroundUid:I */
/* if-ne v2, v3, :cond_2 */
/* .line 61 */
} // :cond_2
v2 = this.mConfig;
v2 = this.mWhiteList;
v3 = this.info;
v2 = v3 = this.packageName;
/* if-nez v2, :cond_7 */
v2 = this.mConfig;
v2 = this.mWhiteList;
v2 = v3 = this.processName;
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 63 */
} // :cond_3
v2 = this.mState;
v2 = (( com.android.server.am.ProcessStateRecord ) v2 ).getSetAdj ( ); // invoke-virtual {v2}, Lcom/android/server/am/ProcessStateRecord;->getSetAdj()I
v3 = this.mConfig;
/* iget v3, v3, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mMinAdj:I */
/* if-le v2, v3, :cond_6 */
v2 = this.mState;
v2 = (( com.android.server.am.ProcessStateRecord ) v2 ).getSetAdj ( ); // invoke-virtual {v2}, Lcom/android/server/am/ProcessStateRecord;->getSetAdj()I
v3 = this.mConfig;
/* iget v3, v3, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mMaxAdj:I */
/* if-le v2, v3, :cond_4 */
/* .line 65 */
} // :cond_4
v2 = this.mConfig;
/* iget-boolean v2, v2, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->mSkipForeground:Z */
if ( v2 != null) { // if-eqz v2, :cond_5
(( com.android.server.am.ProcessRecord ) p1 ).getWindowProcessController ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getWindowProcessController()Lcom/android/server/wm/WindowProcessController;
v2 = (( com.android.server.wm.WindowProcessController ) v2 ).isInterestingToUser ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowProcessController;->isInterestingToUser()Z
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 66 */
/* .line 67 */
} // :cond_5
int v1 = 0; // const/4 v1, 0x0
/* .line 64 */
} // :cond_6
} // :goto_1
/* .line 62 */
} // :cond_7
} // :goto_2
/* .line 60 */
} // :cond_8
} // :goto_3
} // .end method
