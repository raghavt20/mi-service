.class Lcom/android/server/am/ProcessMemoryCleaner$H;
.super Landroid/os/Handler;
.source "ProcessMemoryCleaner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ProcessMemoryCleaner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "H"
.end annotation


# instance fields
.field private mPadSmallWindowWhiteList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/server/am/ProcessMemoryCleaner;


# direct methods
.method public constructor <init>(Lcom/android/server/am/ProcessMemoryCleaner;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 737
    iput-object p1, p0, Lcom/android/server/am/ProcessMemoryCleaner$H;->this$0:Lcom/android/server/am/ProcessMemoryCleaner;

    .line 738
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 735
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/android/server/am/ProcessMemoryCleaner$H;->mPadSmallWindowWhiteList:Ljava/util/List;

    .line 739
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .line 743
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 744
    iget v0, p1, Landroid/os/Message;->what:I

    const-string v1, "ProcessMemoryCleaner"

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 754
    :pswitch_0
    :try_start_0
    iget-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner$H;->this$0:Lcom/android/server/am/ProcessMemoryCleaner;

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    invoke-static {v0, v2}, Lcom/android/server/am/ProcessMemoryCleaner;->-$$Nest$mcheckBackgroundProcCompact(Lcom/android/server/am/ProcessMemoryCleaner;Lcom/miui/server/smartpower/IAppState$IRunningProcess;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 757
    goto :goto_0

    .line 755
    :catch_0
    move-exception v0

    .line 756
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "checkBackgroundProcCompact failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 758
    .end local v0    # "e":Ljava/lang/Exception;
    goto :goto_0

    .line 764
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner$H;->mPadSmallWindowWhiteList:Ljava/util/List;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 765
    iget-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner$H;->this$0:Lcom/android/server/am/ProcessMemoryCleaner;

    const-string v1, "PadSmallWindowClean"

    iget-object v2, p0, Lcom/android/server/am/ProcessMemoryCleaner$H;->mPadSmallWindowWhiteList:Ljava/util/List;

    const/16 v3, 0x31f

    invoke-static {v0, v3, v1, v2}, Lcom/android/server/am/ProcessMemoryCleaner;->-$$Nest$mkillProcessByMinAdj(Lcom/android/server/am/ProcessMemoryCleaner;ILjava/lang/String;Ljava/util/List;)V

    .line 767
    iget-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner$H;->mPadSmallWindowWhiteList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 768
    goto :goto_0

    .line 760
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner$H;->this$0:Lcom/android/server/am/ProcessMemoryCleaner;

    invoke-static {v0}, Lcom/android/server/am/ProcessMemoryCleaner;->-$$Nest$fgetmContext(Lcom/android/server/am/ProcessMemoryCleaner;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/am/ProcessMemoryCleaner;->-$$Nest$mregisterCloudObserver(Lcom/android/server/am/ProcessMemoryCleaner;Landroid/content/Context;)V

    .line 761
    iget-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner$H;->this$0:Lcom/android/server/am/ProcessMemoryCleaner;

    invoke-static {v0}, Lcom/android/server/am/ProcessMemoryCleaner;->-$$Nest$mupdateCloudControlData(Lcom/android/server/am/ProcessMemoryCleaner;)V

    .line 762
    goto :goto_0

    .line 747
    :pswitch_3
    :try_start_1
    iget-object v0, p0, Lcom/android/server/am/ProcessMemoryCleaner$H;->this$0:Lcom/android/server/am/ProcessMemoryCleaner;

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v2, v3}, Lcom/android/server/am/ProcessMemoryCleaner;->-$$Nest$mcheckBackgroundAppException(Lcom/android/server/am/ProcessMemoryCleaner;Ljava/lang/String;I)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 750
    goto :goto_0

    .line 748
    :catch_1
    move-exception v0

    .line 749
    .restart local v0    # "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "checkBackgroundAppException failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 751
    .end local v0    # "e":Ljava/lang/Exception;
    nop

    .line 772
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
