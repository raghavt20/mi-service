public class com.android.server.am.AppStateManager {
	 /* .source "AppStateManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/am/AppStateManager$ActiveApps;, */
	 /* Lcom/android/server/am/AppStateManager$CurrentTaskStack;, */
	 /* Lcom/android/server/am/AppStateManager$MainHandler;, */
	 /* Lcom/android/server/am/AppStateManager$PowerFrozenCallback;, */
	 /* Lcom/android/server/am/AppStateManager$AppState;, */
	 /* Lcom/android/server/am/AppStateManager$TaskRecord;, */
	 /* Lcom/android/server/am/AppStateManager$IProcessStateCallback; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer APP_STATE_BACKGROUND;
private static final Integer APP_STATE_DIED;
private static final Integer APP_STATE_FOREGROUND;
private static final Integer APP_STATE_HIBERNATION;
private static final Integer APP_STATE_IDLE;
private static final Integer APP_STATE_INACTIVE;
private static final Integer APP_STATE_MAINTENANCE;
private static final Integer APP_STATE_NONE;
public static final Boolean DEBUG;
public static final Boolean DEBUG_PROC;
private static final Integer HISTORY_SIZE;
private static final Integer MSG_CLEAR_APPSTATES_BY_USERID;
private static final Integer MSG_HIBERNATE_ALL;
private static final Integer MSG_HIBERNATE_ALL_INACTIVE;
private static final Integer PROCESS_STATE_BACKGROUND;
private static final Integer PROCESS_STATE_DIED;
private static final Integer PROCESS_STATE_HIBERNATION;
private static final Integer PROCESS_STATE_IDLE;
private static final Integer PROCESS_STATE_INACTIVE;
private static final Integer PROCESS_STATE_INVISIBLE;
private static final Integer PROCESS_STATE_MAINTENANCE;
private static final Integer PROCESS_STATE_NONE;
private static final Integer PROCESS_STATE_VISIBLE;
public static final java.lang.String REASON_ADD_TO_WHITELIST;
public static final java.lang.String REASON_ADJ_ABOVE_FG;
public static final java.lang.String REASON_ADJ_BELOW_VISIBLE;
public static final java.lang.String REASON_ALARM_END;
public static final java.lang.String REASON_ALARM_START;
public static final java.lang.String REASON_APP_START;
public static final java.lang.String REASON_BACKUP_END;
public static final java.lang.String REASON_BACKUP_SERVICE_CONNECTED;
public static final java.lang.String REASON_BACKUP_SERVICE_DISCONNECTED;
public static final java.lang.String REASON_BACKUP_START;
public static final java.lang.String REASON_BECOME_BACKGROUND;
public static final java.lang.String REASON_BECOME_FOREGROUND;
public static final java.lang.String REASON_BECOME_INVISIBLE;
public static final java.lang.String REASON_BECOME_VISIBLE;
public static final java.lang.String REASON_BROADCAST_END;
public static final java.lang.String REASON_BROADCAST_START;
public static final java.lang.String REASON_CONTENT_PROVIDER_END;
public static final java.lang.String REASON_CONTENT_PROVIDER_START;
public static final java.lang.String REASON_HIBERNATE_ALL;
public static final java.lang.String REASON_PENDING_INTENT;
public static final java.lang.String REASON_PERIODIC_ACTIVE;
public static final java.lang.String REASON_PRESS_MEDIA_KEY;
public static final java.lang.String REASON_PROCESS_DIED;
public static final java.lang.String REASON_PROCESS_START;
public static final java.lang.String REASON_RECEIVE_BINDER_STATE;
public static final java.lang.String REASON_RECEIVE_BINDER_TRANS;
public static final java.lang.String REASON_RECEIVE_KILL_SIGNAL;
public static final java.lang.String REASON_RECEIVE_NET_REQUEST;
public static final java.lang.String REASON_RECEIVE_OTHER_THAW_REQUEST;
public static final java.lang.String REASON_REMOVE_FROM_WHITELIST;
public static final java.lang.String REASON_RESOURCE_ACTIVE;
public static final java.lang.String REASON_RESOURCE_INACTIVE;
public static final java.lang.String REASON_SERVICE_CEANGE;
public static final java.lang.String REASON_SHOW_INPUTMETHOD;
public static final java.lang.String REASON_START_ACTIVITY;
public static final java.lang.String TAG;
private static final android.util.ArraySet mStatesNeedToRecord;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static Boolean sEnable;
/* # instance fields */
private com.android.server.am.ActivityManagerService mAMS;
private final com.android.server.am.AppStateManager$ActiveApps mActiveApps;
private final java.lang.Object mAppLock;
private com.miui.server.smartpower.AppPowerResourceManager mAppPowerResourceManager;
private android.content.Context mContext;
private final com.android.server.am.AppStateManager$CurrentTaskStack mCurrentTaskStack;
private Integer mHoldScreenUid;
private final android.content.BroadcastReceiver mIntentReceiver;
private android.os.Looper mLooper;
private com.android.server.am.AppStateManager$MainHandler mMainHandler;
private final android.util.SparseArray mPendingInteractiveUids;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.content.pm.PackageManager mPkms;
private com.miui.server.process.ProcessManagerInternal mPmInternal;
private com.miui.server.smartpower.PowerFrozenManager mPowerFrozenManager;
private com.android.server.am.IProcessPolicy mProcessPolicy;
private final java.util.ArrayList mProcessStateCallbacks;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/am/AppStateManager$IProcessStateCallback;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.miui.server.smartpower.SmartPowerPolicyManager mSmartPowerPolicyManager;
private com.miui.server.smartpower.SmartScenarioManager mSmartScenarioManager;
private final android.util.ArraySet mWhiteListVideoActivities;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.android.server.am.ActivityManagerService -$$Nest$fgetmAMS ( com.android.server.am.AppStateManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mAMS;
} // .end method
static com.android.server.am.AppStateManager$ActiveApps -$$Nest$fgetmActiveApps ( com.android.server.am.AppStateManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mActiveApps;
} // .end method
static java.lang.Object -$$Nest$fgetmAppLock ( com.android.server.am.AppStateManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mAppLock;
} // .end method
static com.miui.server.smartpower.AppPowerResourceManager -$$Nest$fgetmAppPowerResourceManager ( com.android.server.am.AppStateManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mAppPowerResourceManager;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.am.AppStateManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static Integer -$$Nest$fgetmHoldScreenUid ( com.android.server.am.AppStateManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/AppStateManager;->mHoldScreenUid:I */
} // .end method
static android.os.Looper -$$Nest$fgetmLooper ( com.android.server.am.AppStateManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mLooper;
} // .end method
static com.android.server.am.AppStateManager$MainHandler -$$Nest$fgetmMainHandler ( com.android.server.am.AppStateManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMainHandler;
} // .end method
static android.util.SparseArray -$$Nest$fgetmPendingInteractiveUids ( com.android.server.am.AppStateManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPendingInteractiveUids;
} // .end method
static android.content.pm.PackageManager -$$Nest$fgetmPkms ( com.android.server.am.AppStateManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPkms;
} // .end method
static com.android.server.am.IProcessPolicy -$$Nest$fgetmProcessPolicy ( com.android.server.am.AppStateManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mProcessPolicy;
} // .end method
static java.util.ArrayList -$$Nest$fgetmProcessStateCallbacks ( com.android.server.am.AppStateManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mProcessStateCallbacks;
} // .end method
static com.miui.server.smartpower.SmartPowerPolicyManager -$$Nest$fgetmSmartPowerPolicyManager ( com.android.server.am.AppStateManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mSmartPowerPolicyManager;
} // .end method
static com.miui.server.smartpower.SmartScenarioManager -$$Nest$fgetmSmartScenarioManager ( com.android.server.am.AppStateManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mSmartScenarioManager;
} // .end method
static android.util.ArraySet -$$Nest$fgetmWhiteListVideoActivities ( com.android.server.am.AppStateManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mWhiteListVideoActivities;
} // .end method
static void -$$Nest$fputmPkms ( com.android.server.am.AppStateManager p0, android.content.pm.PackageManager p1 ) { //bridge//synthethic
/* .locals 0 */
this.mPkms = p1;
return;
} // .end method
static void -$$Nest$monReportPowerFrozenSignal ( com.android.server.am.AppStateManager p0, Integer p1, Integer p2, java.lang.String p3 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/am/AppStateManager;->onReportPowerFrozenSignal(IILjava/lang/String;)V */
return;
} // .end method
static void -$$Nest$monReportPowerFrozenSignal ( com.android.server.am.AppStateManager p0, Integer p1, java.lang.String p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/am/AppStateManager;->onReportPowerFrozenSignal(ILjava/lang/String;)V */
return;
} // .end method
static Integer -$$Nest$sfgetHISTORY_SIZE ( ) { //bridge//synthethic
/* .locals 1 */
} // .end method
static android.util.ArraySet -$$Nest$sfgetmStatesNeedToRecord ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.am.AppStateManager.mStatesNeedToRecord;
} // .end method
static java.util.List -$$Nest$smparseScenatioActions ( Integer p0 ) { //bridge//synthethic
/* .locals 0 */
com.android.server.am.AppStateManager .parseScenatioActions ( p0 );
} // .end method
static Integer -$$Nest$smringAdvance ( Integer p0, Integer p1, Integer p2 ) { //bridge//synthethic
/* .locals 0 */
p0 = com.android.server.am.AppStateManager .ringAdvance ( p0,p1,p2 );
} // .end method
static com.android.server.am.AppStateManager ( ) {
/* .locals 2 */
/* .line 56 */
/* sget-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->DEBUG_ALL:Z */
com.android.server.am.AppStateManager.DEBUG = (v0!= 0);
/* .line 58 */
/* sget-boolean v1, Lcom/miui/app/smartpower/SmartPowerSettings;->APP_STATE_ENABLE:Z */
com.android.server.am.AppStateManager.sEnable = (v1!= 0);
/* .line 60 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* sget-boolean v0, Lcom/miui/app/smartpower/SmartPowerSettings;->DEBUG_PROC:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
com.android.server.am.AppStateManager.DEBUG_PROC = (v0!= 0);
/* .line 175 */
/* .line 183 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
return;
} // .end method
 com.android.server.am.AppStateManager ( ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .param p3, "ams" # Lcom/android/server/am/ActivityManagerService; */
/* .param p4, "powerFrozenManager" # Lcom/miui/server/smartpower/PowerFrozenManager; */
/* .line 202 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 177 */
/* new-instance v0, Lcom/android/server/am/AppStateManager$ActiveApps; */
/* invoke-direct {v0, p0}, Lcom/android/server/am/AppStateManager$ActiveApps;-><init>(Lcom/android/server/am/AppStateManager;)V */
this.mActiveApps = v0;
/* .line 178 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mAppLock = v0;
/* .line 195 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mProcessStateCallbacks = v0;
/* .line 196 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mPendingInteractiveUids = v0;
/* .line 198 */
/* new-instance v0, Lcom/android/server/am/AppStateManager$CurrentTaskStack; */
/* invoke-direct {v0, p0}, Lcom/android/server/am/AppStateManager$CurrentTaskStack;-><init>(Lcom/android/server/am/AppStateManager;)V */
this.mCurrentTaskStack = v0;
/* .line 199 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/am/AppStateManager;->mHoldScreenUid:I */
/* .line 200 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
this.mWhiteListVideoActivities = v0;
/* .line 238 */
/* new-instance v1, Lcom/android/server/am/AppStateManager$1; */
/* invoke-direct {v1, p0}, Lcom/android/server/am/AppStateManager$1;-><init>(Lcom/android/server/am/AppStateManager;)V */
this.mIntentReceiver = v1;
/* .line 203 */
this.mLooper = p2;
/* .line 204 */
/* new-instance v1, Lcom/android/server/am/AppStateManager$MainHandler; */
v2 = this.mLooper;
/* invoke-direct {v1, p0, v2}, Lcom/android/server/am/AppStateManager$MainHandler;-><init>(Lcom/android/server/am/AppStateManager;Landroid/os/Looper;)V */
this.mMainHandler = v1;
/* .line 205 */
this.mContext = p1;
/* .line 206 */
this.mAMS = p3;
/* .line 207 */
this.mPowerFrozenManager = p4;
/* .line 208 */
/* new-instance v1, Lcom/android/server/am/AppStateManager$PowerFrozenCallback; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v1, p0, v2}, Lcom/android/server/am/AppStateManager$PowerFrozenCallback;-><init>(Lcom/android/server/am/AppStateManager;Lcom/android/server/am/AppStateManager$PowerFrozenCallback-IA;)V */
(( com.miui.server.smartpower.PowerFrozenManager ) p4 ).registerFrozenCallback ( v1 ); // invoke-virtual {p4, v1}, Lcom/miui/server/smartpower/PowerFrozenManager;->registerFrozenCallback(Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;)V
/* .line 209 */
v1 = com.android.server.am.AppStateManager.mStatesNeedToRecord;
int v2 = 0; // const/4 v2, 0x0
java.lang.Integer .valueOf ( v2 );
(( android.util.ArraySet ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 210 */
int v2 = 1; // const/4 v2, 0x1
java.lang.Integer .valueOf ( v2 );
(( android.util.ArraySet ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 211 */
int v2 = 2; // const/4 v2, 0x2
java.lang.Integer .valueOf ( v2 );
(( android.util.ArraySet ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 212 */
int v2 = 3; // const/4 v2, 0x3
java.lang.Integer .valueOf ( v2 );
(( android.util.ArraySet ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 213 */
int v2 = 6; // const/4 v2, 0x6
java.lang.Integer .valueOf ( v2 );
(( android.util.ArraySet ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 214 */
/* sget-boolean v2, Lcom/miui/app/smartpower/SmartPowerSettings;->PROP_FROZEN_ENABLE:Z */
/* if-nez v2, :cond_0 */
/* sget-boolean v2, Lcom/android/server/am/AppStateManager;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 215 */
} // :cond_0
int v2 = 7; // const/4 v2, 0x7
java.lang.Integer .valueOf ( v2 );
(( android.util.ArraySet ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 218 */
} // :cond_1
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v2, 0x110300d1 */
(( android.content.res.Resources ) v1 ).getStringArray ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 220 */
/* .local v1, "activities":[Ljava/lang/String; */
java.util.Arrays .asList ( v1 );
(( android.util.ArraySet ) v0 ).addAll ( v2 ); // invoke-virtual {v0, v2}, Landroid/util/ArraySet;->addAll(Ljava/util/Collection;)Z
/* .line 221 */
return;
} // .end method
public static java.lang.String appStateToString ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "state" # I */
/* .line 116 */
/* packed-switch p0, :pswitch_data_0 */
/* .line 132 */
java.lang.Integer .toString ( p0 );
/* .line 130 */
/* :pswitch_0 */
final String v0 = "hibernation"; // const-string v0, "hibernation"
/* .line 128 */
/* :pswitch_1 */
final String v0 = "idle"; // const-string v0, "idle"
/* .line 126 */
/* :pswitch_2 */
final String v0 = "maintenance"; // const-string v0, "maintenance"
/* .line 124 */
/* :pswitch_3 */
final String v0 = "inactive"; // const-string v0, "inactive"
/* .line 122 */
/* :pswitch_4 */
final String v0 = "background"; // const-string v0, "background"
/* .line 120 */
/* :pswitch_5 */
final String v0 = "foreground"; // const-string v0, "foreground"
/* .line 118 */
/* :pswitch_6 */
final String v0 = "died"; // const-string v0, "died"
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private Boolean isFrozenForUid ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 3375 */
v0 = this.mPowerFrozenManager;
v0 = (( com.miui.server.smartpower.PowerFrozenManager ) v0 ).isFrozenForUid ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/smartpower/PowerFrozenManager;->isFrozenForUid(I)Z
} // .end method
public static Boolean isSystemApp ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 3 */
/* .param p0, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 775 */
int v0 = 0; // const/4 v0, 0x0
if ( p0 != null) { // if-eqz p0, :cond_2
v1 = this.info;
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 776 */
v1 = this.info;
/* iget v1, v1, Landroid/content/pm/ApplicationInfo;->flags:I */
/* and-int/lit16 v1, v1, 0x81 */
/* if-nez v1, :cond_0 */
/* const/16 v1, 0x3e8 */
/* iget v2, p0, Lcom/android/server/am/ProcessRecord;->uid:I */
/* if-ne v1, v2, :cond_1 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
/* .line 780 */
} // :cond_2
} // .end method
private void onReportPowerFrozenSignal ( Integer p0, Integer p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "reason" # Ljava/lang/String; */
/* .line 724 */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 725 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 726 */
com.android.server.am.AppStateManager$AppState .-$$Nest$monReportPowerFrozenSignal ( v0,p2,p3 );
/* .line 728 */
} // :cond_0
return;
} // .end method
private void onReportPowerFrozenSignal ( Integer p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 717 */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 718 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 719 */
com.android.server.am.AppStateManager$AppState .-$$Nest$monReportPowerFrozenSignal ( v0,p2 );
/* .line 721 */
} // :cond_0
return;
} // .end method
private static java.util.List parseScenatioActions ( Integer p0 ) {
/* .locals 3 */
/* .param p0, "scenarioAction" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 878 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 879 */
/* .local v0, "actions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
int v1 = 1; // const/4 v1, 0x1
/* .line 880 */
/* .local v1, "action":I */
} // :goto_0
/* if-gt v1, p0, :cond_1 */
/* .line 881 */
/* and-int v2, v1, p0 */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 882 */
java.lang.Integer .valueOf ( v1 );
/* .line 884 */
} // :cond_0
/* shl-int/lit8 v1, v1, 0x1 */
/* .line 886 */
} // :cond_1
} // .end method
public static java.lang.String processStateToString ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "state" # I */
/* .line 93 */
/* packed-switch p0, :pswitch_data_0 */
/* .line 111 */
java.lang.Integer .toString ( p0 );
/* .line 109 */
/* :pswitch_0 */
final String v0 = "hibernation"; // const-string v0, "hibernation"
/* .line 107 */
/* :pswitch_1 */
final String v0 = "idle"; // const-string v0, "idle"
/* .line 105 */
/* :pswitch_2 */
final String v0 = "maintenance"; // const-string v0, "maintenance"
/* .line 103 */
/* :pswitch_3 */
final String v0 = "inactive"; // const-string v0, "inactive"
/* .line 101 */
/* :pswitch_4 */
final String v0 = "background"; // const-string v0, "background"
/* .line 99 */
/* :pswitch_5 */
final String v0 = "invisible"; // const-string v0, "invisible"
/* .line 97 */
/* :pswitch_6 */
/* const-string/jumbo v0, "visible" */
/* .line 95 */
/* :pswitch_7 */
final String v0 = "died"; // const-string v0, "died"
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void removeAppIfNeeded ( com.android.server.am.AppStateManager$AppState p0 ) {
/* .locals 3 */
/* .param p1, "appState" # Lcom/android/server/am/AppStateManager$AppState; */
/* .line 476 */
v0 = (( com.android.server.am.AppStateManager$AppState ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getUid()I
v0 = android.os.Process .isIsolated ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( com.android.server.am.AppStateManager$AppState ) p1 ).isAlive ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->isAlive()Z
/* if-nez v0, :cond_0 */
/* .line 477 */
v0 = this.mAppLock;
/* monitor-enter v0 */
/* .line 478 */
try { // :try_start_0
v1 = this.mActiveApps;
v2 = (( com.android.server.am.AppStateManager$AppState ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState;->getUid()I
(( com.android.server.am.AppStateManager$ActiveApps ) v1 ).remove ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/AppStateManager$ActiveApps;->remove(I)V
/* .line 479 */
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 481 */
} // :cond_0
} // :goto_0
return;
} // .end method
private static Integer ringAdvance ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p0, "origin" # I */
/* .param p1, "increment" # I */
/* .param p2, "size" # I */
/* .line 873 */
/* add-int v0, p0, p1 */
/* rem-int/2addr v0, p2 */
/* .line 874 */
/* .local v0, "index":I */
/* if-gez v0, :cond_0 */
/* add-int v1, v0, p2 */
} // :cond_0
/* move v1, v0 */
} // :goto_0
} // .end method
/* # virtual methods */
public void activityStartBeforeLocked ( java.lang.String p0, Integer p1, Integer p2, java.lang.String p3, Boolean p4 ) {
/* .locals 10 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "fromPid" # I */
/* .param p3, "toUid" # I */
/* .param p4, "packageName" # Ljava/lang/String; */
/* .param p5, "isColdStart" # Z */
/* .line 655 */
/* if-lez p2, :cond_0 */
v0 = (( com.android.server.am.AppStateManager ) p0 ).getHomeProcessPidLocked ( ); // invoke-virtual {p0}, Lcom/android/server/am/AppStateManager;->getHomeProcessPidLocked()I
/* if-ne v0, p2, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 656 */
/* .local v0, "startByHome":Z */
} // :goto_0
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( p3 ); // invoke-virtual {p0, p3}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 657 */
/* .local v1, "appState":Lcom/android/server/am/AppStateManager$AppState; */
/* if-nez v1, :cond_1 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 658 */
v2 = this.mAppLock;
/* monitor-enter v2 */
/* .line 659 */
try { // :try_start_0
/* new-instance v9, Lcom/android/server/am/AppStateManager$AppState; */
v7 = this.mContext;
int v8 = 0; // const/4 v8, 0x0
/* move-object v3, v9 */
/* move-object v4, p0 */
/* move v5, p3 */
/* move-object v6, p4 */
/* invoke-direct/range {v3 ..v8}, Lcom/android/server/am/AppStateManager$AppState;-><init>(Lcom/android/server/am/AppStateManager;ILjava/lang/String;Landroid/content/Context;Lcom/android/server/am/AppStateManager$AppState-IA;)V */
/* move-object v1, v9 */
/* .line 660 */
v3 = this.mActiveApps;
(( com.android.server.am.AppStateManager$ActiveApps ) v3 ).put ( p3, v1 ); // invoke-virtual {v3, p3, v1}, Lcom/android/server/am/AppStateManager$ActiveApps;->put(ILcom/android/server/am/AppStateManager$AppState;)V
/* .line 661 */
/* monitor-exit v2 */
/* :catchall_0 */
/* move-exception v3 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v3 */
/* .line 663 */
} // :cond_1
} // :goto_1
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 664 */
com.android.server.am.AppStateManager$AppState .-$$Nest$monActivityForegroundLocked ( v1,p1 );
/* .line 666 */
} // :cond_2
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 667 */
final String v2 = "app start"; // const-string v2, "app start"
(( com.android.server.am.AppStateManager ) p0 ).hibernateAllInactive ( v1, v2 ); // invoke-virtual {p0, v1, v2}, Lcom/android/server/am/AppStateManager;->hibernateAllInactive(Lcom/android/server/am/AppStateManager$AppState;Ljava/lang/String;)V
/* .line 669 */
} // :cond_3
return;
} // .end method
public void componentEndLocked ( Integer p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "content" # Ljava/lang/String; */
/* .line 517 */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 518 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 519 */
com.android.server.am.AppStateManager$AppState .-$$Nest$mcomponentEnd ( v0,p2 );
/* .line 521 */
} // :cond_0
return;
} // .end method
public void componentEndLocked ( com.android.server.am.ProcessRecord p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "processRecord" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "content" # Ljava/lang/String; */
/* .line 497 */
/* iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 498 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 499 */
com.android.server.am.AppStateManager$AppState .-$$Nest$mcomponentEnd ( v0,p1,p2 );
/* .line 501 */
} // :cond_0
return;
} // .end method
public void componentStartLocked ( Integer p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "content" # Ljava/lang/String; */
/* .line 507 */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 508 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 509 */
com.android.server.am.AppStateManager$AppState .-$$Nest$mcomponentStart ( v0,p2 );
/* .line 511 */
} // :cond_0
return;
} // .end method
public void componentStartLocked ( com.android.server.am.ProcessRecord p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "processRecord" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "content" # Ljava/lang/String; */
/* .line 487 */
/* iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 488 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 489 */
com.android.server.am.AppStateManager$AppState .-$$Nest$mcomponentStart ( v0,p1,p2 );
/* .line 491 */
} // :cond_0
return;
} // .end method
public void dump ( java.io.PrintWriter p0, java.lang.String[] p1, Integer p2 ) {
/* .locals 5 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "args" # [Ljava/lang/String; */
/* .param p3, "opti" # I */
/* .line 417 */
v0 = this.mAppLock;
/* monitor-enter v0 */
/* .line 419 */
/* add-int/lit8 v1, p3, 0x1 */
try { // :try_start_0
/* array-length v2, p2 */
/* if-ge v1, v2, :cond_2 */
/* .line 420 */
/* aget-object v1, p2, p3 */
/* .line 421 */
/* .local v1, "parm":Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* .line 422 */
/* .local v2, "dpUid":I */
/* const-string/jumbo v3, "uid" */
v3 = (( java.lang.String ) v1 ).contains ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v3, :cond_0 */
final String v3 = "-u"; // const-string v3, "-u"
v3 = (( java.lang.String ) v1 ).contains ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 423 */
} // :cond_0
/* add-int/lit8 v3, p3, 0x1 */
/* aget-object v3, p2, v3 */
/* .line 424 */
/* .local v3, "uidStr":Ljava/lang/String; */
v4 = java.lang.Integer .parseInt ( v3 );
/* move v2, v4 */
/* .line 426 */
} // .end local v3 # "uidStr":Ljava/lang/String;
} // :cond_1
v3 = this.mActiveApps;
(( com.android.server.am.AppStateManager$ActiveApps ) v3 ).dump ( p1, p2, p3, v2 ); // invoke-virtual {v3, p1, p2, p3, v2}, Lcom/android/server/am/AppStateManager$ActiveApps;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;II)V
/* .line 427 */
} // .end local v1 # "parm":Ljava/lang/String;
} // .end local v2 # "dpUid":I
/* .line 428 */
} // :cond_2
v1 = this.mActiveApps;
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.am.AppStateManager$ActiveApps ) v1 ).dump ( p1, p2, p3, v2 ); // invoke-virtual {v1, p1, p2, p3, v2}, Lcom/android/server/am/AppStateManager$ActiveApps;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;II)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 432 */
} // :goto_0
/* .line 433 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 430 */
/* :catch_0 */
/* move-exception v1 */
/* .line 431 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_1
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 433 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
/* monitor-exit v0 */
/* .line 434 */
return;
/* .line 433 */
} // :goto_2
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
public void dumpStack ( java.io.PrintWriter p0, java.lang.String[] p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "args" # [Ljava/lang/String; */
/* .param p3, "opti" # I */
/* .line 437 */
v0 = this.mCurrentTaskStack;
(( com.android.server.am.AppStateManager$CurrentTaskStack ) v0 ).dump ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/am/AppStateManager$CurrentTaskStack;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
/* .line 438 */
return;
} // .end method
public java.util.ArrayList getAllAppState ( ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/miui/server/smartpower/IAppState;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 340 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 341 */
/* .local v0, "appList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState;>;" */
v1 = this.mAppLock;
/* monitor-enter v1 */
/* .line 342 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
try { // :try_start_0
v3 = this.mActiveApps;
v3 = (( com.android.server.am.AppStateManager$ActiveApps ) v3 ).size ( ); // invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$ActiveApps;->size()I
/* if-ge v2, v3, :cond_0 */
/* .line 343 */
v3 = this.mActiveApps;
(( com.android.server.am.AppStateManager$ActiveApps ) v3 ).valueAt ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/server/am/AppStateManager$ActiveApps;->valueAt(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 344 */
/* .local v3, "app":Lcom/android/server/am/AppStateManager$AppState; */
(( java.util.ArrayList ) v0 ).add ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 342 */
/* nop */
} // .end local v3 # "app":Lcom/android/server/am/AppStateManager$AppState;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 346 */
} // .end local v2 # "i":I
} // :cond_0
/* monitor-exit v1 */
/* .line 347 */
/* .line 346 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public com.android.server.am.AppStateManager$AppState getAppState ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 354 */
v0 = this.mAppLock;
/* monitor-enter v0 */
/* .line 355 */
try { // :try_start_0
v1 = this.mActiveApps;
(( com.android.server.am.AppStateManager$ActiveApps ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/am/AppStateManager$ActiveApps;->get(I)Lcom/android/server/am/AppStateManager$AppState;
/* monitor-exit v0 */
/* .line 356 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public java.util.List getAppState ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/am/AppStateManager$AppState;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 363 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 364 */
/* .local v0, "apps":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/AppStateManager$AppState;>;" */
v1 = this.mAppLock;
/* monitor-enter v1 */
/* .line 365 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
try { // :try_start_0
v3 = this.mActiveApps;
v3 = (( com.android.server.am.AppStateManager$ActiveApps ) v3 ).size ( ); // invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$ActiveApps;->size()I
/* if-ge v2, v3, :cond_1 */
/* .line 366 */
v3 = this.mActiveApps;
(( com.android.server.am.AppStateManager$ActiveApps ) v3 ).valueAt ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/server/am/AppStateManager$ActiveApps;->valueAt(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 367 */
/* .local v3, "app":Lcom/android/server/am/AppStateManager$AppState; */
(( com.android.server.am.AppStateManager$AppState ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState;->getPackageName()Ljava/lang/String;
v4 = (( java.lang.String ) v4 ).equals ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 368 */
/* .line 365 */
} // .end local v3 # "app":Lcom/android/server/am/AppStateManager$AppState;
} // :cond_0
/* add-int/lit8 v2, v2, 0x1 */
/* .line 371 */
} // .end local v2 # "i":I
} // :cond_1
/* monitor-exit v1 */
/* .line 372 */
/* .line 371 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public Integer getHomeProcessPidLocked ( ) {
/* .locals 2 */
/* .line 672 */
v0 = this.mAMS;
v0 = this.mAtmInternal;
(( com.android.server.wm.ActivityTaskManagerInternal ) v0 ).getHomeProcess ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerInternal;->getHomeProcess()Lcom/android/server/wm/WindowProcessController;
/* .line 673 */
/* .local v0, "homeProcess":Lcom/android/server/wm/WindowProcessController; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 674 */
v1 = (( com.android.server.wm.WindowProcessController ) v0 ).getPid ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowProcessController;->getPid()I
/* .line 676 */
} // :cond_0
int v1 = -1; // const/4 v1, -0x1
} // .end method
java.util.ArrayList getLruProcesses ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/miui/server/smartpower/IAppState$IRunningProcess;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 750 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 751 */
/* .local v0, "lruProcesses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;" */
v1 = this.mAppLock;
/* monitor-enter v1 */
/* .line 752 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
try { // :try_start_0
v3 = this.mActiveApps;
v3 = (( com.android.server.am.AppStateManager$ActiveApps ) v3 ).size ( ); // invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$ActiveApps;->size()I
/* if-ge v2, v3, :cond_0 */
/* .line 753 */
v3 = this.mActiveApps;
(( com.android.server.am.AppStateManager$ActiveApps ) v3 ).valueAt ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/server/am/AppStateManager$ActiveApps;->valueAt(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 754 */
/* .local v3, "app":Lcom/android/server/am/AppStateManager$AppState; */
(( com.android.server.am.AppStateManager$AppState ) v3 ).getRunningProcessList ( ); // invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcessList()Ljava/util/ArrayList;
(( java.util.ArrayList ) v0 ).addAll ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
/* .line 752 */
/* nop */
} // .end local v3 # "app":Lcom/android/server/am/AppStateManager$AppState;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 756 */
} // .end local v2 # "i":I
} // :cond_0
/* monitor-exit v1 */
/* .line 757 */
/* .line 756 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
java.util.ArrayList getLruProcesses ( Integer p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/miui/server/smartpower/IAppState$IRunningProcess;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 764 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 765 */
/* .local v0, "processes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;" */
v1 = this.mAppLock;
/* monitor-enter v1 */
/* .line 766 */
try { // :try_start_0
v2 = this.mActiveApps;
(( com.android.server.am.AppStateManager$ActiveApps ) v2 ).get ( p1 ); // invoke-virtual {v2, p1}, Lcom/android/server/am/AppStateManager$ActiveApps;->get(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 767 */
/* .local v2, "app":Lcom/android/server/am/AppStateManager$AppState; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 768 */
(( com.android.server.am.AppStateManager$AppState ) v2 ).getRunningProcessList ( p2 ); // invoke-virtual {v2, p2}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcessList(Ljava/lang/String;)Ljava/util/ArrayList;
(( java.util.ArrayList ) v0 ).addAll ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
/* .line 770 */
} // .end local v2 # "app":Lcom/android/server/am/AppStateManager$AppState;
} // :cond_0
/* monitor-exit v1 */
/* .line 771 */
/* .line 770 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public Integer getProcessState ( Integer p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "processName" # Ljava/lang/String; */
/* .line 376 */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 377 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 378 */
v1 = (( com.android.server.am.AppStateManager$AppState ) v0 ).getProcessState ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/am/AppStateManager$AppState;->getProcessState(Ljava/lang/String;)I
/* .line 380 */
} // :cond_0
int v1 = -1; // const/4 v1, -0x1
} // .end method
public com.android.server.am.AppStateManager$AppState$RunningProcess getRunningProcess ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 392 */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 393 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 394 */
(( com.android.server.am.AppStateManager$AppState ) v0 ).getRunningProcess ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 396 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public com.android.server.am.AppStateManager$AppState$RunningProcess getRunningProcess ( Integer p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "processName" # Ljava/lang/String; */
/* .line 384 */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 385 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 386 */
(( com.android.server.am.AppStateManager$AppState ) v0 ).getRunningProcess ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(Ljava/lang/String;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 388 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public void hibernateAllIfNeeded ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .line 795 */
v0 = this.mMainHandler;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.am.AppStateManager$MainHandler ) v0 ).obtainMessage ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Lcom/android/server/am/AppStateManager$MainHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 796 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.mMainHandler;
(( com.android.server.am.AppStateManager$MainHandler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/am/AppStateManager$MainHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 797 */
return;
} // .end method
public void hibernateAllInactive ( com.android.server.am.AppStateManager$AppState p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "startingApp" # Lcom/android/server/am/AppStateManager$AppState; */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 800 */
v0 = this.mMainHandler;
int v1 = 2; // const/4 v1, 0x2
(( com.android.server.am.AppStateManager$MainHandler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/AppStateManager$MainHandler;->removeMessages(I)V
/* .line 801 */
v0 = this.mMainHandler;
(( com.android.server.am.AppStateManager$MainHandler ) v0 ).obtainMessage ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Lcom/android/server/am/AppStateManager$MainHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 802 */
/* .local v0, "msg":Landroid/os/Message; */
/* new-instance v1, Landroid/os/Bundle; */
/* invoke-direct {v1}, Landroid/os/Bundle;-><init>()V */
/* .line 803 */
/* .local v1, "data":Landroid/os/Bundle; */
final String v2 = "reason"; // const-string v2, "reason"
(( android.os.Bundle ) v1 ).putString ( v2, p2 ); // invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 804 */
(( android.os.Message ) v0 ).setData ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
/* .line 805 */
v2 = this.mMainHandler;
(( com.android.server.am.AppStateManager$MainHandler ) v2 ).sendMessage ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/am/AppStateManager$MainHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 806 */
return;
} // .end method
void init ( com.miui.server.smartpower.AppPowerResourceManager p0, com.miui.server.smartpower.SmartPowerPolicyManager p1, com.miui.server.smartpower.SmartScenarioManager p2 ) {
/* .locals 7 */
/* .param p1, "appPowerResourceManager" # Lcom/miui/server/smartpower/AppPowerResourceManager; */
/* .param p2, "smartPowerPolicyManager" # Lcom/miui/server/smartpower/SmartPowerPolicyManager; */
/* .param p3, "smartScenarioManager" # Lcom/miui/server/smartpower/SmartScenarioManager; */
/* .line 226 */
this.mAppPowerResourceManager = p1;
/* .line 227 */
this.mSmartPowerPolicyManager = p2;
/* .line 228 */
this.mSmartScenarioManager = p3;
/* .line 229 */
com.miui.server.process.ProcessManagerInternal .getInstance ( );
this.mPmInternal = v0;
/* .line 230 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
this.mPkms = v0;
/* .line 231 */
v0 = this.mPmInternal;
(( com.miui.server.process.ProcessManagerInternal ) v0 ).getProcessPolicy ( ); // invoke-virtual {v0}, Lcom/miui/server/process/ProcessManagerInternal;->getProcessPolicy()Lcom/android/server/am/IProcessPolicy;
this.mProcessPolicy = v0;
/* .line 233 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 234 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.USER_REMOVED"; // const-string v1, "android.intent.action.USER_REMOVED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 235 */
v1 = this.mContext;
v2 = this.mIntentReceiver;
v3 = android.os.UserHandle.ALL;
int v5 = 0; // const/4 v5, 0x0
int v6 = 0; // const/4 v6, 0x0
/* move-object v4, v0 */
/* invoke-virtual/range {v1 ..v6}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent; */
/* .line 236 */
return;
} // .end method
public Boolean isHomeProcessTopLocked ( ) {
/* .locals 5 */
/* .line 680 */
v0 = (( com.android.server.am.AppStateManager ) p0 ).getHomeProcessPidLocked ( ); // invoke-virtual {p0}, Lcom/android/server/am/AppStateManager;->getHomeProcessPidLocked()I
/* .line 681 */
/* .local v0, "homePid":I */
int v1 = 0; // const/4 v1, 0x0
/* if-lez v0, :cond_1 */
/* .line 683 */
v2 = this.mAMS;
v2 = this.mPidsSelfLocked;
/* monitor-enter v2 */
/* .line 684 */
try { // :try_start_0
v3 = this.mAMS;
v3 = this.mPidsSelfLocked;
(( com.android.server.am.ActivityManagerService$PidMap ) v3 ).get ( v0 ); // invoke-virtual {v3, v0}, Lcom/android/server/am/ActivityManagerService$PidMap;->get(I)Lcom/android/server/am/ProcessRecord;
/* .line 685 */
/* .local v3, "proc":Lcom/android/server/am/ProcessRecord; */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 686 */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 687 */
v2 = this.mState;
v2 = (( com.android.server.am.ProcessStateRecord ) v2 ).getCurProcState ( ); // invoke-virtual {v2}, Lcom/android/server/am/ProcessStateRecord;->getCurProcState()I
int v4 = 2; // const/4 v4, 0x2
/* if-ne v2, v4, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
/* .line 685 */
} // .end local v3 # "proc":Lcom/android/server/am/ProcessRecord;
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
/* .line 691 */
} // :cond_1
} // .end method
public Boolean isProcessIdle ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 290 */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 291 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 292 */
v1 = (( com.android.server.am.AppStateManager$AppState ) v0 ).isProcessIdle ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/am/AppStateManager$AppState;->isProcessIdle(I)Z
/* .line 294 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean isProcessInTaskStack ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 318 */
v0 = this.mCurrentTaskStack;
v0 = (( com.android.server.am.AppStateManager$CurrentTaskStack ) v0 ).isProcessInTaskStack ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/am/AppStateManager$CurrentTaskStack;->isProcessInTaskStack(II)Z
} // .end method
public Boolean isProcessInTaskStack ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "procName" # Ljava/lang/String; */
/* .line 322 */
v0 = this.mCurrentTaskStack;
v0 = (( com.android.server.am.AppStateManager$CurrentTaskStack ) v0 ).isProcessInTaskStack ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/AppStateManager$CurrentTaskStack;->isProcessInTaskStack(Ljava/lang/String;)Z
} // .end method
public Boolean isProcessKilled ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 329 */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 330 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 331 */
v1 = (( com.android.server.am.AppStateManager$AppState ) v0 ).isProcessKilled ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/am/AppStateManager$AppState;->isProcessKilled(I)Z
/* .line 333 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
} // .end method
public Boolean isProcessPerceptible ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 302 */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 303 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 304 */
v1 = (( com.android.server.am.AppStateManager$AppState ) v0 ).isProcessPerceptible ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/am/AppStateManager$AppState;->isProcessPerceptible(I)Z
/* .line 306 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean isProcessPerceptible ( Integer p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "processName" # Ljava/lang/String; */
/* .line 310 */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 311 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 312 */
v1 = (( com.android.server.am.AppStateManager$AppState ) v0 ).isProcessPerceptible ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/am/AppStateManager$AppState;->isProcessPerceptible(Ljava/lang/String;)Z
/* .line 314 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean isRecentThawed ( Integer p0, Long p1 ) {
/* .locals 6 */
/* .param p1, "uid" # I */
/* .param p2, "sinceUptime" # J */
/* .line 731 */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 732 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 733 */
/* nop */
/* .line 734 */
(( com.android.server.am.AppStateManager$AppState ) v0 ).getRunningProcessList ( ); // invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcessList()Ljava/util/ArrayList;
/* .line 735 */
/* .local v1, "runningProcessList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;" */
(( java.util.ArrayList ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 736 */
/* .local v3, "process":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* move-object v4, v3 */
/* check-cast v4, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 737 */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$mgetHistoryInfos ( v4,p2,p3 );
/* .line 738 */
/* .local v4, "history":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;>;" */
v5 = (( java.util.ArrayList ) v4 ).size ( ); // invoke-virtual {v4}, Ljava/util/ArrayList;->size()I
/* if-lez v5, :cond_0 */
/* .line 739 */
int v2 = 1; // const/4 v2, 0x1
/* .line 741 */
} // .end local v3 # "process":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
} // .end local v4 # "history":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;>;"
} // :cond_0
/* .line 743 */
} // .end local v1 # "runningProcessList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean isSystemApp ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 784 */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 785 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 786 */
v1 = (( com.android.server.am.AppStateManager$AppState ) v0 ).isSystemApp ( ); // invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState;->isSystemApp()Z
/* .line 788 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean isUidIdle ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 257 */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 258 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 259 */
v1 = (( com.android.server.am.AppStateManager$AppState ) v0 ).isIdle ( ); // invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState;->isIdle()Z
/* .line 261 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean isUidInactive ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 268 */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 269 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 270 */
v1 = (( com.android.server.am.AppStateManager$AppState ) v0 ).isInactive ( ); // invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState;->isInactive()Z
/* .line 272 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean isUidVisible ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 279 */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 280 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 281 */
v1 = (( com.android.server.am.AppStateManager$AppState ) v0 ).isVsible ( ); // invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState;->isVsible()Z
/* .line 283 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public void onAddToWhiteList ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 555 */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 556 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 557 */
com.android.server.am.AppStateManager$AppState .-$$Nest$monAddToWhiteList ( v0 );
/* .line 559 */
} // :cond_0
return;
} // .end method
public void onAddToWhiteList ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 562 */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 563 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 564 */
com.android.server.am.AppStateManager$AppState .-$$Nest$monAddToWhiteList ( v0,p2 );
/* .line 566 */
} // :cond_0
return;
} // .end method
public void onApplyOomAdjLocked ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 1 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 618 */
/* iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 619 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 620 */
com.android.server.am.AppStateManager$AppState .-$$Nest$monApplyOomAdjLocked ( v0,p1 );
/* .line 622 */
} // :cond_0
return;
} // .end method
public void onBackupChanged ( Boolean p0, com.android.server.am.ProcessRecord p1 ) {
/* .locals 1 */
/* .param p1, "active" # Z */
/* .param p2, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 604 */
/* iget v0, p2, Lcom/android/server/am/ProcessRecord;->uid:I */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 605 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 606 */
com.android.server.am.AppStateManager$AppState .-$$Nest$monBackupChanged ( v0,p1,p2 );
/* .line 608 */
} // :cond_0
return;
} // .end method
public void onBackupServiceAppChanged ( Boolean p0, Integer p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "active" # Z */
/* .param p2, "uid" # I */
/* .param p3, "pid" # I */
/* .line 611 */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 612 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 613 */
com.android.server.am.AppStateManager$AppState .-$$Nest$monBackupServiceAppChanged ( v0,p1,p3 );
/* .line 615 */
} // :cond_0
return;
} // .end method
public void onForegroundActivityChangedLocked ( Integer p0, Integer p1, Integer p2, java.lang.String p3, Integer p4, Integer p5, java.lang.String p6 ) {
/* .locals 3 */
/* .param p1, "fromPid" # I */
/* .param p2, "toUid" # I */
/* .param p3, "toPid" # I */
/* .param p4, "name" # Ljava/lang/String; */
/* .param p5, "taskId" # I */
/* .param p6, "callingUid" # I */
/* .param p7, "callingPkg" # Ljava/lang/String; */
/* .line 696 */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 697 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 698 */
com.android.server.am.AppStateManager$AppState .-$$Nest$monActivityForegroundLocked ( v0,p4 );
/* .line 699 */
(( com.android.server.am.AppStateManager$AppState ) v0 ).getRunningProcess ( p3 ); // invoke-virtual {v0, p3}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 700 */
/* .local v1, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 701 */
v2 = this.mCurrentTaskStack;
(( com.android.server.am.AppStateManager$CurrentTaskStack ) v2 ).updateIfNeeded ( p5, v1, p6, p7 ); // invoke-virtual {v2, p5, v1, p6, p7}, Lcom/android/server/am/AppStateManager$CurrentTaskStack;->updateIfNeeded(ILcom/android/server/am/AppStateManager$AppState$RunningProcess;ILjava/lang/String;)V
/* .line 702 */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fputmLastTaskId ( v1,p5 );
/* .line 705 */
} // .end local v1 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
} // :cond_0
return;
} // .end method
public void onHoldScreenUidChanged ( Integer p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "hold" # Z */
/* .line 642 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 643 */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
/* .line 645 */
} // .end local v0 # "appState":Lcom/android/server/am/AppStateManager$AppState;
} // :cond_0
/* iget v0, p0, Lcom/android/server/am/AppStateManager;->mHoldScreenUid:I */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 647 */
/* .restart local v0 # "appState":Lcom/android/server/am/AppStateManager$AppState; */
} // :goto_0
/* iput p1, p0, Lcom/android/server/am/AppStateManager;->mHoldScreenUid:I */
/* .line 648 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 649 */
com.android.server.am.AppStateManager$AppState .-$$Nest$mupdateCurrentResourceBehavier ( v0 );
/* .line 651 */
} // :cond_1
return;
} // .end method
public void onInputMethodShow ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 597 */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 598 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 599 */
com.android.server.am.AppStateManager$AppState .-$$Nest$monInputMethodShow ( v0 );
/* .line 601 */
} // :cond_0
return;
} // .end method
public void onMediaKey ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 590 */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 591 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 592 */
com.android.server.am.AppStateManager$AppState .-$$Nest$monMediaKey ( v0 );
/* .line 594 */
} // :cond_0
return;
} // .end method
public void onMediaKey ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 583 */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 584 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 585 */
com.android.server.am.AppStateManager$AppState .-$$Nest$monMediaKey ( v0,p2 );
/* .line 587 */
} // :cond_0
return;
} // .end method
public void onRemoveFromWhiteList ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 569 */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 570 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 571 */
com.android.server.am.AppStateManager$AppState .-$$Nest$monRemoveFromWhiteList ( v0 );
/* .line 573 */
} // :cond_0
return;
} // .end method
public void onRemoveFromWhiteList ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 576 */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 577 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 578 */
com.android.server.am.AppStateManager$AppState .-$$Nest$monRemoveFromWhiteList ( v0,p2 );
/* .line 580 */
} // :cond_0
return;
} // .end method
public void onSendPendingIntent ( Integer p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "uid" # I */
/* .param p2, "typeName" # Ljava/lang/String; */
/* .line 524 */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 525 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 526 */
com.android.server.am.AppStateManager$AppState .-$$Nest$monSendPendingIntent ( v0,p2 );
/* .line 528 */
} // :cond_0
v1 = this.mPendingInteractiveUids;
/* monitor-enter v1 */
/* .line 529 */
try { // :try_start_0
v2 = this.mPendingInteractiveUids;
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v3 */
java.lang.Long .valueOf ( v3,v4 );
(( android.util.SparseArray ) v2 ).append ( p1, v3 ); // invoke-virtual {v2, p1, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V
/* .line 530 */
/* monitor-exit v1 */
/* .line 532 */
} // :goto_0
return;
/* .line 530 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public void processKilledLocked ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 1 */
/* .param p1, "record" # Lcom/android/server/am/ProcessRecord; */
/* .line 453 */
/* iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 454 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 455 */
com.android.server.am.AppStateManager$AppState .-$$Nest$mprocessKilledLocked ( v0,p1 );
/* .line 456 */
/* invoke-direct {p0, v0}, Lcom/android/server/am/AppStateManager;->removeAppIfNeeded(Lcom/android/server/am/AppStateManager$AppState;)V */
/* .line 458 */
} // :cond_0
return;
} // .end method
public void processStartedLocked ( com.android.server.am.ProcessRecord p0, java.lang.String p1 ) {
/* .locals 9 */
/* .param p1, "record" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 442 */
v0 = this.mAppLock;
/* monitor-enter v0 */
/* .line 443 */
try { // :try_start_0
v1 = this.mActiveApps;
/* iget v2, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
(( com.android.server.am.AppStateManager$ActiveApps ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/AppStateManager$ActiveApps;->get(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 444 */
/* .local v1, "appState":Lcom/android/server/am/AppStateManager$AppState; */
/* if-nez v1, :cond_0 */
/* .line 445 */
/* new-instance v8, Lcom/android/server/am/AppStateManager$AppState; */
/* iget v4, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
v6 = this.mContext;
int v7 = 0; // const/4 v7, 0x0
/* move-object v2, v8 */
/* move-object v3, p0 */
/* move-object v5, p2 */
/* invoke-direct/range {v2 ..v7}, Lcom/android/server/am/AppStateManager$AppState;-><init>(Lcom/android/server/am/AppStateManager;ILjava/lang/String;Landroid/content/Context;Lcom/android/server/am/AppStateManager$AppState-IA;)V */
/* move-object v1, v8 */
/* .line 446 */
v2 = this.mActiveApps;
/* iget v3, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
(( com.android.server.am.AppStateManager$ActiveApps ) v2 ).put ( v3, v1 ); // invoke-virtual {v2, v3, v1}, Lcom/android/server/am/AppStateManager$ActiveApps;->put(ILcom/android/server/am/AppStateManager$AppState;)V
/* .line 448 */
} // :cond_0
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 449 */
com.android.server.am.AppStateManager$AppState .-$$Nest$mprocessStartedLocked ( v1,p1 );
/* .line 450 */
return;
/* .line 448 */
} // .end local v1 # "appState":Lcom/android/server/am/AppStateManager$AppState;
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
public void providerConnectionChangedLocked ( com.android.server.am.ProcessRecord p0, com.android.server.am.ProcessRecord p1, Boolean p2 ) {
/* .locals 5 */
/* .param p1, "client" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "host" # Lcom/android/server/am/ProcessRecord; */
/* .param p3, "isConnect" # Z */
/* .line 544 */
v0 = this.mSmartPowerPolicyManager;
/* iget v1, p2, Lcom/android/server/am/ProcessRecord;->uid:I */
/* iget v2, p2, Lcom/android/server/am/ProcessRecord;->mPid:I */
v3 = this.info;
v3 = this.packageName;
v4 = this.processName;
v0 = (( com.miui.server.smartpower.SmartPowerPolicyManager ) v0 ).isProcessHibernationWhiteList ( v1, v2, v3, v4 ); // invoke-virtual {v0, v1, v2, v3, v4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isProcessHibernationWhiteList(IILjava/lang/String;Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 546 */
return;
/* .line 548 */
} // :cond_0
/* iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 549 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 550 */
com.android.server.am.AppStateManager$AppState .-$$Nest$mupdateProviderDepends ( v0,p1,p2,p3 );
/* .line 552 */
} // :cond_1
return;
} // .end method
public Boolean registerAppStateListener ( com.android.server.am.AppStateManager$IProcessStateCallback p0 ) {
/* .locals 2 */
/* .param p1, "callback" # Lcom/android/server/am/AppStateManager$IProcessStateCallback; */
/* .line 3379 */
v0 = this.mProcessStateCallbacks;
/* monitor-enter v0 */
/* .line 3380 */
try { // :try_start_0
v1 = this.mProcessStateCallbacks;
v1 = (( java.util.ArrayList ) v1 ).contains ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v1, :cond_0 */
/* .line 3381 */
v1 = this.mProcessStateCallbacks;
(( java.util.ArrayList ) v1 ).add ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 3382 */
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 3384 */
} // :cond_0
/* monitor-exit v0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 3385 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void serviceConnectionChangedLocked ( com.android.server.am.ProcessRecord p0, com.android.server.am.ProcessRecord p1, Boolean p2, Long p3 ) {
/* .locals 7 */
/* .param p1, "client" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "service" # Lcom/android/server/am/ProcessRecord; */
/* .param p3, "isConnect" # Z */
/* .param p4, "flags" # J */
/* .line 536 */
/* iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 537 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 538 */
/* move-object v1, v0 */
/* move-object v2, p1 */
/* move-object v3, p2 */
/* move v4, p3 */
/* move-wide v5, p4 */
/* invoke-static/range {v1 ..v6}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$mupdateServiceDepends(Lcom/android/server/am/AppStateManager$AppState;Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;ZJ)V */
/* .line 540 */
} // :cond_0
return;
} // .end method
public void setActivityVisible ( Integer p0, java.lang.String p1, com.android.server.wm.ActivityRecord p2, Boolean p3 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "processName" # Ljava/lang/String; */
/* .param p3, "record" # Lcom/android/server/wm/ActivityRecord; */
/* .param p4, "visible" # Z */
/* .line 626 */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 627 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 628 */
com.android.server.am.AppStateManager$AppState .-$$Nest$msetActivityVisible ( v0,p2,p3,p4 );
/* .line 630 */
} // :cond_0
return;
} // .end method
public void setProcessPss ( com.android.server.am.ProcessRecord p0, Long p1, Long p2 ) {
/* .locals 2 */
/* .param p1, "record" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "pss" # J */
/* .param p4, "swapPss" # J */
/* .line 469 */
/* iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
v1 = this.processName;
(( com.android.server.am.AppStateManager ) p0 ).getRunningProcess ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/am/AppStateManager;->getRunningProcess(ILjava/lang/String;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 470 */
/* .local v0, "runProc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 471 */
(( com.android.server.am.AppStateManager$AppState$RunningProcess ) v0 ).setPss ( p2, p3, p4, p5 ); // invoke-virtual {v0, p2, p3, p4, p5}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->setPss(JJ)V
/* .line 473 */
} // :cond_0
return;
} // .end method
public void setWindowVisible ( Integer p0, Integer p1, com.android.server.policy.WindowManagerPolicy$WindowState p2, android.view.WindowManager$LayoutParams p3, Boolean p4 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "win" # Lcom/android/server/policy/WindowManagerPolicy$WindowState; */
/* .param p4, "attrs" # Landroid/view/WindowManager$LayoutParams; */
/* .param p5, "visible" # Z */
/* .line 634 */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 635 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 636 */
com.android.server.am.AppStateManager$AppState .-$$Nest$msetWindowVisible ( v0,p2,p3,p4,p5 );
/* .line 638 */
} // :cond_0
return;
} // .end method
public void unRegisterAppStateListener ( com.android.server.am.AppStateManager$IProcessStateCallback p0 ) {
/* .locals 2 */
/* .param p1, "callback" # Lcom/android/server/am/AppStateManager$IProcessStateCallback; */
/* .line 3389 */
v0 = this.mProcessStateCallbacks;
/* monitor-enter v0 */
/* .line 3390 */
try { // :try_start_0
v1 = this.mProcessStateCallbacks;
(( java.util.ArrayList ) v1 ).remove ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
/* .line 3391 */
/* monitor-exit v0 */
/* .line 3392 */
return;
/* .line 3391 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public java.util.ArrayList updateAllAppUsageStats ( ) {
/* .locals 7 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/miui/server/smartpower/IAppState;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 400 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 401 */
/* .local v0, "appList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState;>;" */
v1 = this.mAppLock;
/* monitor-enter v1 */
/* .line 402 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
try { // :try_start_0
v3 = this.mActiveApps;
v3 = (( com.android.server.am.AppStateManager$ActiveApps ) v3 ).size ( ); // invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$ActiveApps;->size()I
/* if-ge v2, v3, :cond_1 */
/* .line 403 */
v3 = this.mActiveApps;
(( com.android.server.am.AppStateManager$ActiveApps ) v3 ).valueAt ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/server/am/AppStateManager$ActiveApps;->valueAt(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 404 */
/* .local v3, "app":Lcom/miui/server/smartpower/IAppState; */
v4 = this.mSmartPowerPolicyManager;
/* .line 405 */
(( com.miui.server.smartpower.IAppState ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState;->getPackageName()Ljava/lang/String;
(( com.miui.server.smartpower.SmartPowerPolicyManager ) v4 ).getUsageStatsInfo ( v5 ); // invoke-virtual {v4, v5}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->getUsageStatsInfo(Ljava/lang/String;)Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;
/* .line 406 */
/* .local v4, "usageStats":Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo; */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 407 */
v5 = (( com.miui.server.smartpower.SmartPowerPolicyManager$UsageStatsInfo ) v4 ).getAppLaunchCount ( ); // invoke-virtual {v4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->getAppLaunchCount()I
(( com.miui.server.smartpower.IAppState ) v3 ).setLaunchCount ( v5 ); // invoke-virtual {v3, v5}, Lcom/miui/server/smartpower/IAppState;->setLaunchCount(I)V
/* .line 408 */
(( com.miui.server.smartpower.SmartPowerPolicyManager$UsageStatsInfo ) v4 ).getTotalTimeInForeground ( ); // invoke-virtual {v4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->getTotalTimeInForeground()J
/* move-result-wide v5 */
(( com.miui.server.smartpower.IAppState ) v3 ).setTotalTimeInForeground ( v5, v6 ); // invoke-virtual {v3, v5, v6}, Lcom/miui/server/smartpower/IAppState;->setTotalTimeInForeground(J)V
/* .line 410 */
} // :cond_0
(( java.util.ArrayList ) v0 ).add ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 402 */
/* nop */
} // .end local v3 # "app":Lcom/miui/server/smartpower/IAppState;
} // .end local v4 # "usageStats":Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 412 */
} // .end local v2 # "i":I
} // :cond_1
/* monitor-exit v1 */
/* .line 413 */
/* .line 412 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public void updateProcessLocked ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 1 */
/* .param p1, "record" # Lcom/android/server/am/ProcessRecord; */
/* .line 461 */
/* iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 462 */
/* .local v0, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 463 */
com.android.server.am.AppStateManager$AppState .-$$Nest$mupdateProcessLocked ( v0,p1 );
/* .line 464 */
/* invoke-direct {p0, v0}, Lcom/android/server/am/AppStateManager;->removeAppIfNeeded(Lcom/android/server/am/AppStateManager$AppState;)V */
/* .line 466 */
} // :cond_0
return;
} // .end method
public void updateVisibilityLocked ( android.util.ArraySet p0 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/Integer;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 708 */
/* .local p1, "uids":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/Integer;>;" */
(( android.util.ArraySet ) p1 ).iterator ( ); // invoke-virtual {p1}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* .line 709 */
/* .local v1, "uid":I */
(( com.android.server.am.AppStateManager ) p0 ).getAppState ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/am/AppStateManager;->getAppState(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 710 */
/* .local v2, "appState":Lcom/android/server/am/AppStateManager$AppState; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 711 */
com.android.server.am.AppStateManager$AppState .-$$Nest$mupdateVisibility ( v2 );
/* .line 713 */
} // .end local v1 # "uid":I
} // .end local v2 # "appState":Lcom/android/server/am/AppStateManager$AppState;
} // :cond_0
/* .line 714 */
} // :cond_1
return;
} // .end method
