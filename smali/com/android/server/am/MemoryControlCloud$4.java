class com.android.server.am.MemoryControlCloud$4 extends android.database.ContentObserver {
	 /* .source "MemoryControlCloud.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/am/MemoryControlCloud;->registerCloudAppHeapHandleTimeObserver(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.am.MemoryControlCloud this$0; //synthetic
final android.content.Context val$context; //synthetic
/* # direct methods */
 com.android.server.am.MemoryControlCloud$4 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/MemoryControlCloud; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 149 */
this.this$0 = p1;
this.val$context = p3;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 5 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 152 */
final String v0 = "MemoryStandardProcessControl"; // const-string v0, "MemoryStandardProcessControl"
if ( p2 != null) { // if-eqz p2, :cond_0
	 final String v1 = "cloud_memory_standard_appheap_handle_time"; // const-string v1, "cloud_memory_standard_appheap_handle_time"
	 android.provider.Settings$System .getUriFor ( v1 );
	 v2 = 	 (( android.net.Uri ) p2 ).equals ( v2 ); // invoke-virtual {p2, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 /* .line 154 */
		 try { // :try_start_0
			 v2 = this.val$context;
			 (( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
			 int v3 = -2; // const/4 v3, -0x2
			 android.provider.Settings$System .getStringForUser ( v2,v1,v3 );
			 /* .line 156 */
			 /* .local v1, "str":Ljava/lang/String; */
			 java.lang.Long .parseLong ( v1 );
			 /* move-result-wide v2 */
			 /* .line 157 */
			 /* .local v2, "handleTime":J */
			 v4 = this.this$0;
			 com.android.server.am.MemoryControlCloud .-$$Nest$fgetmspc ( v4 );
			 /* iput-wide v2, v4, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapHandleTime:J */
			 /* :try_end_0 */
			 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 160 */
		 } // .end local v1 # "str":Ljava/lang/String;
	 } // .end local v2 # "handleTime":J
	 /* .line 158 */
	 /* :catch_0 */
	 /* move-exception v1 */
	 /* .line 159 */
	 /* .local v1, "e":Ljava/lang/Exception; */
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v3 = "cloud CLOUD_MRMORY_STANDARD_APPHEAP_HANDLE_TIME check failed: "; // const-string v3, "cloud CLOUD_MRMORY_STANDARD_APPHEAP_HANDLE_TIME check failed: "
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .e ( v0,v2 );
	 /* .line 161 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "set app heap handle time from database: " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
com.android.server.am.MemoryControlCloud .-$$Nest$fgetmspc ( v2 );
/* iget-wide v2, v2, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapHandleTime:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v1 );
/* .line 163 */
} // :cond_0
return;
} // .end method
