.class public Lcom/android/server/am/PreloadAppControllerImpl;
.super Ljava/lang/Object;
.source "PreloadAppControllerImpl.java"

# interfaces
.implements Lcom/android/server/am/PreloadAppControllerStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;,
        Lcom/android/server/am/PreloadAppControllerImpl$CallbackDeathRecipient;
    }
.end annotation


# static fields
.field public static final APP_NOT_PRELOAD:I = -0x1e3

.field public static final DEBUG:Z

.field private static ENABLE:Z = false

.field private static final FLAG_DISPLAY_ID_SHIFT:I = 0x8

.field private static final MAX_PRELOAD_COUNT:I = 0xa

.field public static final MIN_TOTAL_MEMORY:I = 0x6

.field public static final PACKAGE_NAME_EMPTY:I = -0x1ef

.field public static final PERMISSION_DENIED:I = -0x1e8

.field public static final PREFIX:Ljava/lang/String; = "preloadApp"

.field public static final START_ALREADY_LOAD_APP:I = -0x1f1

.field public static final START_CONFIG_NULL:I = -0x1e5

.field public static final START_ERROR:I = -0x1f4

.field public static final START_FAIL_SPEED_TEST:I = -0x1e4

.field public static final START_FORBIDDEN_LOW_MEMORY:I = -0x1ee

.field public static final START_FORBIDDEN_SYSTEM_APP:I = -0x1f2

.field public static final START_FREQUENTLY_KILLED:I = -0x1e7

.field public static final START_GREEZE_IS_DISABLE:I = -0x1e9

.field public static final START_LAUNCH_INTENT_NOT_FOUND:I = -0x1ed

.field public static final START_NO_INTERCEPT:I = 0x1f4

.field public static final START_PACKAGE_NAME_IN_BLACK_LIST:I = -0x1eb

.field public static final START_PACKAGE_NAME_NOT_FOUND:I = -0x1f3

.field public static final START_PERSISTENT_APP:I = -0x1f0

.field public static final START_PRELOAD_IS_DISABLE:I = -0x1e6

.field public static final START_SWITCH_PRELOAD_STATE_ERROR:I = -0x1ea

.field public static final START_TOP_APP_IN_BLACK_LIST:I = -0x1ec

.field private static final TAG:Ljava/lang/String; = "PreloadAppControllerImpl"

.field private static volatile mPreloadLifecycleState:I

.field private static sBlackList:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final sDefaultSchedAffinity:[I

.field private static sInstance:Lcom/android/server/am/PreloadAppControllerImpl;

.field public static final sPreloadSchedAffinity:[I

.field private static sRelaunchBlackList:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sTopAppBlackList:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sWindowBlackList:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

.field private mAllPreloadAppInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/server/wm/PreloadLifecycle;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;

.field private final mPreloadingAppLock:Ljava/lang/Object;

.field private mProcessManagerService:Lcom/android/server/am/ProcessManagerService;

.field private volatile mSpeedTestState:Z


# direct methods
.method static bridge synthetic -$$Nest$mremovePreloadAppInfo(Lcom/android/server/am/PreloadAppControllerImpl;Ljava/lang/String;)Lcom/android/server/wm/PreloadLifecycle;
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/PreloadAppControllerImpl;->removePreloadAppInfo(Ljava/lang/String;)Lcom/android/server/wm/PreloadLifecycle;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mstartPreloadApp(Lcom/android/server/am/PreloadAppControllerImpl;Lcom/android/server/wm/PreloadLifecycle;)I
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/PreloadAppControllerImpl;->startPreloadApp(Lcom/android/server/wm/PreloadLifecycle;)I

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$munRegistPreloadCallback(Lcom/android/server/am/PreloadAppControllerImpl;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/PreloadAppControllerImpl;->unRegistPreloadCallback(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfputmPreloadLifecycleState(I)V
    .locals 0

    sput p0, Lcom/android/server/am/PreloadAppControllerImpl;->mPreloadLifecycleState:I

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 69
    nop

    .line 70
    const-string v0, "persist.sys.preload.debug"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    .line 71
    nop

    .line 72
    const-string v0, "persist.sys.preload.enable"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/am/PreloadAppControllerImpl;->ENABLE:Z

    .line 75
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/server/am/PreloadAppControllerImpl;->sDefaultSchedAffinity:[I

    .line 76
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/server/am/PreloadAppControllerImpl;->sPreloadSchedAffinity:[I

    .line 77
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/server/am/PreloadAppControllerImpl;->sWindowBlackList:Ljava/util/Set;

    .line 78
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/server/am/PreloadAppControllerImpl;->sBlackList:Ljava/util/Set;

    .line 79
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/server/am/PreloadAppControllerImpl;->sTopAppBlackList:Ljava/util/Set;

    .line 80
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/server/am/PreloadAppControllerImpl;->sRelaunchBlackList:Ljava/util/Set;

    .line 85
    sget-object v0, Lcom/android/server/am/PreloadAppControllerImpl;->sBlackList:Ljava/util/Set;

    const-string v1, "com.xiaomi.youpin"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 86
    sget-object v0, Lcom/android/server/am/PreloadAppControllerImpl;->sBlackList:Ljava/util/Set;

    const-string v1, "com.ss.android.ugc.live"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 87
    sget-object v0, Lcom/android/server/am/PreloadAppControllerImpl;->sWindowBlackList:Ljava/util/Set;

    const-string v1, "com.miui.systemAdSolution"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 88
    sget-object v0, Lcom/android/server/am/PreloadAppControllerImpl;->sTopAppBlackList:Ljava/util/Set;

    const-string v1, "com.android.camera"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 89
    sget-object v0, Lcom/android/server/am/PreloadAppControllerImpl;->sRelaunchBlackList:Ljava/util/Set;

    const-string v1, "com.ss.android.ugc"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 90
    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
        0x7
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x1
        0x2
        0x3
        0x4
        0x5
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mPreloadingAppLock:Ljava/lang/Object;

    .line 95
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mAllPreloadAppInfos:Ljava/util/List;

    return-void
.end method

.method private createPreloadLifeCycle(ZLjava/lang/String;Lmiui/process/LifecycleConfig;)Lcom/android/server/wm/PreloadLifecycle;
    .locals 7
    .param p1, "ignoreMemory"    # Z
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "config"    # Lmiui/process/LifecycleConfig;

    .line 250
    new-instance v0, Lcom/android/server/wm/PreloadLifecycle;

    invoke-direct {v0, p1, p2, p3}, Lcom/android/server/wm/PreloadLifecycle;-><init>(ZLjava/lang/String;Lmiui/process/LifecycleConfig;)V

    .line 251
    .local v0, "lifecycle":Lcom/android/server/wm/PreloadLifecycle;
    sget-boolean v1, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 252
    const-string v1, "PreloadAppControllerImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "received preloadApp request, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    :cond_0
    iget-object v1, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mPreloadingAppLock:Ljava/lang/Object;

    monitor-enter v1

    .line 255
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mAllPreloadAppInfos:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 256
    .local v2, "size":I
    const/16 v3, 0xa

    const/4 v4, 0x0

    if-lt v2, v3, :cond_1

    .line 257
    const-string v3, "PreloadAppControllerImpl"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " applications have been preloaded, no more preload"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    monitor-exit v1

    return-object v4

    .line 260
    :cond_1
    invoke-static {p2}, Lcom/android/server/am/PreloadAppControllerImpl;->getUidFromPackageName(Ljava/lang/String;)I

    move-result v3

    .line 261
    .local v3, "uid":I
    if-gez v3, :cond_2

    .line 262
    iget-object v4, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mAllPreloadAppInfos:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 263
    monitor-exit v1

    return-object v0

    .line 265
    :cond_2
    iget-object v5, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    iget-object v5, v5, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    .line 266
    invoke-virtual {v5}, Lcom/android/server/am/ProcessList;->getProcessNamesLOSP()Lcom/android/server/am/ProcessList$MyProcessMap;

    move-result-object v5

    invoke-virtual {v5, p2, v3}, Lcom/android/server/am/ProcessList$MyProcessMap;->get(Ljava/lang/String;I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/am/ProcessRecord;

    .line 267
    .local v5, "app":Lcom/android/server/am/ProcessRecord;
    if-nez v5, :cond_3

    .line 268
    monitor-exit v1

    return-object v0

    .line 271
    .end local v2    # "size":I
    .end local v3    # "uid":I
    .end local v5    # "app":Lcom/android/server/am/ProcessRecord;
    :cond_3
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 272
    const-string v1, "PreloadAppControllerImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "preloadApp: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " already exits, skip it"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    return-object v4

    .line 271
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public static getInstance()Lcom/android/server/am/PreloadAppControllerImpl;
    .locals 1

    .line 125
    const-class v0, Lcom/android/server/am/PreloadAppControllerStub;

    invoke-static {v0}, Lcom/miui/base/MiuiStubUtil;->getImpl(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/PreloadAppControllerImpl;

    return-object v0
.end method

.method public static getPackageName(I)Ljava/lang/String;
    .locals 4
    .param p0, "uid"    # I

    .line 289
    sget-object v0, Lcom/android/server/am/PreloadAppControllerImpl;->sInstance:Lcom/android/server/am/PreloadAppControllerImpl;

    if-eqz v0, :cond_2

    .line 290
    iget-object v0, v0, Lcom/android/server/am/PreloadAppControllerImpl;->mPreloadingAppLock:Ljava/lang/Object;

    monitor-enter v0

    .line 291
    :try_start_0
    sget-object v1, Lcom/android/server/am/PreloadAppControllerImpl;->sInstance:Lcom/android/server/am/PreloadAppControllerImpl;

    iget-object v1, v1, Lcom/android/server/am/PreloadAppControllerImpl;->mAllPreloadAppInfos:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/PreloadLifecycle;

    .line 292
    .local v2, "lifecycle":Lcom/android/server/wm/PreloadLifecycle;
    invoke-virtual {v2}, Lcom/android/server/wm/PreloadLifecycle;->getUid()I

    move-result v3

    if-ne v3, p0, :cond_0

    .line 293
    invoke-virtual {v2}, Lcom/android/server/wm/PreloadLifecycle;->getPackageName()Ljava/lang/String;

    move-result-object v1

    monitor-exit v0

    return-object v1

    .line 295
    .end local v2    # "lifecycle":Lcom/android/server/wm/PreloadLifecycle;
    :cond_0
    goto :goto_0

    .line 296
    :cond_1
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 298
    :cond_2
    :goto_1
    const/4 v0, 0x0

    return-object v0
.end method

.method private getPreloadType(Ljava/lang/String;)I
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .line 315
    iget-object v0, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mPreloadingAppLock:Ljava/lang/Object;

    monitor-enter v0

    .line 316
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mAllPreloadAppInfos:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/PreloadLifecycle;

    .line 317
    .local v2, "lifecycle":Lcom/android/server/wm/PreloadLifecycle;
    invoke-virtual {v2}, Lcom/android/server/wm/PreloadLifecycle;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 318
    invoke-virtual {v2}, Lcom/android/server/wm/PreloadLifecycle;->getPreloadType()I

    move-result v1

    monitor-exit v0

    return v1

    .line 320
    .end local v2    # "lifecycle":Lcom/android/server/wm/PreloadLifecycle;
    :cond_0
    goto :goto_0

    .line 321
    :cond_1
    monitor-exit v0

    .line 322
    const/4 v0, -0x1

    return v0

    .line 321
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static getUidFromPackageName(Ljava/lang/String;)I
    .locals 4
    .param p0, "packageName"    # Ljava/lang/String;

    .line 302
    sget-object v0, Lcom/android/server/am/PreloadAppControllerImpl;->sInstance:Lcom/android/server/am/PreloadAppControllerImpl;

    if-eqz v0, :cond_2

    if-eqz p0, :cond_2

    .line 303
    iget-object v0, v0, Lcom/android/server/am/PreloadAppControllerImpl;->mPreloadingAppLock:Ljava/lang/Object;

    monitor-enter v0

    .line 304
    :try_start_0
    sget-object v1, Lcom/android/server/am/PreloadAppControllerImpl;->sInstance:Lcom/android/server/am/PreloadAppControllerImpl;

    iget-object v1, v1, Lcom/android/server/am/PreloadAppControllerImpl;->mAllPreloadAppInfos:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/PreloadLifecycle;

    .line 305
    .local v2, "lifecycle":Lcom/android/server/wm/PreloadLifecycle;
    invoke-virtual {v2}, Lcom/android/server/wm/PreloadLifecycle;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 306
    invoke-virtual {v2}, Lcom/android/server/wm/PreloadLifecycle;->getUid()I

    move-result v1

    monitor-exit v0

    return v1

    .line 308
    .end local v2    # "lifecycle":Lcom/android/server/wm/PreloadLifecycle;
    :cond_0
    goto :goto_0

    .line 309
    :cond_1
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 311
    :cond_2
    :goto_1
    const/4 v0, -0x1

    return v0
.end method

.method public static inBlackList(Ljava/lang/String;)Z
    .locals 1
    .param p0, "packageName"    # Ljava/lang/String;

    .line 447
    sget-object v0, Lcom/android/server/am/PreloadAppControllerImpl;->sBlackList:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static inRelaunchBlackList(Ljava/lang/String;)Z
    .locals 4
    .param p0, "packageName"    # Ljava/lang/String;

    .line 459
    const/4 v0, 0x0

    if-nez p0, :cond_0

    .line 460
    return v0

    .line 463
    :cond_0
    sget-object v1, Lcom/android/server/am/PreloadAppControllerImpl;->sRelaunchBlackList:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 464
    .local v2, "appName":Ljava/lang/String;
    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 465
    const/4 v0, 0x1

    return v0

    .line 467
    .end local v2    # "appName":Ljava/lang/String;
    :cond_1
    goto :goto_0

    .line 468
    :cond_2
    return v0
.end method

.method public static inTopAppBlackList(Ljava/lang/String;)Z
    .locals 1
    .param p0, "packageName"    # Ljava/lang/String;

    .line 455
    sget-object v0, Lcom/android/server/am/PreloadAppControllerImpl;->sTopAppBlackList:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static inWindowBlackList(Ljava/lang/String;)Z
    .locals 1
    .param p0, "packageName"    # Ljava/lang/String;

    .line 451
    sget-object v0, Lcom/android/server/am/PreloadAppControllerImpl;->sWindowBlackList:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private initPreloadEnableState()V
    .locals 4

    .line 115
    invoke-static {}, Landroid/os/Process;->getTotalMemory()J

    move-result-wide v0

    const/16 v2, 0x1e

    shr-long/2addr v0, v2

    const-wide/16 v2, 0x6

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 116
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/server/am/PreloadAppControllerImpl;->ENABLE:Z

    .line 117
    const-string v0, "PreloadAppControllerImpl"

    const-string v1, "preloadApp low total memory, disable"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    return-void

    .line 121
    :cond_0
    sget-boolean v0, Lcom/android/server/am/PreloadAppControllerImpl;->ENABLE:Z

    invoke-static {}, Lcom/android/server/wm/PreloadStateManagerImpl;->checkEnablePreload()Z

    move-result v1

    and-int/2addr v0, v1

    sput-boolean v0, Lcom/android/server/am/PreloadAppControllerImpl;->ENABLE:Z

    .line 122
    return-void
.end method

.method public static isEnable()Z
    .locals 1

    .line 129
    sget-boolean v0, Lcom/android/server/am/PreloadAppControllerImpl;->ENABLE:Z

    return v0
.end method

.method public static queryFreezeTimeoutFromUid(I)J
    .locals 5
    .param p0, "uid"    # I

    .line 498
    sget-object v0, Lcom/android/server/am/PreloadAppControllerImpl;->sInstance:Lcom/android/server/am/PreloadAppControllerImpl;

    if-eqz v0, :cond_2

    .line 499
    iget-object v0, v0, Lcom/android/server/am/PreloadAppControllerImpl;->mPreloadingAppLock:Ljava/lang/Object;

    monitor-enter v0

    .line 500
    :try_start_0
    sget-object v1, Lcom/android/server/am/PreloadAppControllerImpl;->sInstance:Lcom/android/server/am/PreloadAppControllerImpl;

    iget-object v1, v1, Lcom/android/server/am/PreloadAppControllerImpl;->mAllPreloadAppInfos:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/PreloadLifecycle;

    .line 501
    .local v2, "lifecycle":Lcom/android/server/wm/PreloadLifecycle;
    invoke-virtual {v2}, Lcom/android/server/wm/PreloadLifecycle;->getUid()I

    move-result v3

    if-ne v3, p0, :cond_0

    .line 502
    invoke-virtual {v2}, Lcom/android/server/wm/PreloadLifecycle;->getFreezeTimeout()J

    move-result-wide v3

    monitor-exit v0

    return-wide v3

    .line 504
    .end local v2    # "lifecycle":Lcom/android/server/wm/PreloadLifecycle;
    :cond_0
    goto :goto_0

    .line 505
    :cond_1
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 507
    :cond_2
    :goto_1
    const-wide/16 v0, 0x5208

    return-wide v0
.end method

.method public static queryKillTimeoutFromUid(I)J
    .locals 5
    .param p0, "uid"    # I

    .line 485
    sget-object v0, Lcom/android/server/am/PreloadAppControllerImpl;->sInstance:Lcom/android/server/am/PreloadAppControllerImpl;

    if-eqz v0, :cond_2

    .line 486
    iget-object v0, v0, Lcom/android/server/am/PreloadAppControllerImpl;->mPreloadingAppLock:Ljava/lang/Object;

    monitor-enter v0

    .line 487
    :try_start_0
    sget-object v1, Lcom/android/server/am/PreloadAppControllerImpl;->sInstance:Lcom/android/server/am/PreloadAppControllerImpl;

    iget-object v1, v1, Lcom/android/server/am/PreloadAppControllerImpl;->mAllPreloadAppInfos:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/PreloadLifecycle;

    .line 488
    .local v2, "lifecycle":Lcom/android/server/wm/PreloadLifecycle;
    invoke-virtual {v2}, Lcom/android/server/wm/PreloadLifecycle;->getUid()I

    move-result v3

    if-ne v3, p0, :cond_0

    .line 489
    invoke-virtual {v2}, Lcom/android/server/wm/PreloadLifecycle;->getKillTimeout()J

    move-result-wide v3

    monitor-exit v0

    return-wide v3

    .line 491
    .end local v2    # "lifecycle":Lcom/android/server/wm/PreloadLifecycle;
    :cond_0
    goto :goto_0

    .line 492
    :cond_1
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 494
    :cond_2
    :goto_1
    const-wide/32 v0, 0x493e0

    return-wide v0
.end method

.method public static querySchedAffinityFromUid(I)[I
    .locals 4
    .param p0, "uid"    # I

    .line 472
    sget-object v0, Lcom/android/server/am/PreloadAppControllerImpl;->sInstance:Lcom/android/server/am/PreloadAppControllerImpl;

    if-eqz v0, :cond_2

    .line 473
    iget-object v0, v0, Lcom/android/server/am/PreloadAppControllerImpl;->mPreloadingAppLock:Ljava/lang/Object;

    monitor-enter v0

    .line 474
    :try_start_0
    sget-object v1, Lcom/android/server/am/PreloadAppControllerImpl;->sInstance:Lcom/android/server/am/PreloadAppControllerImpl;

    iget-object v1, v1, Lcom/android/server/am/PreloadAppControllerImpl;->mAllPreloadAppInfos:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/PreloadLifecycle;

    .line 475
    .local v2, "lifecycle":Lcom/android/server/wm/PreloadLifecycle;
    invoke-virtual {v2}, Lcom/android/server/wm/PreloadLifecycle;->getUid()I

    move-result v3

    if-ne v3, p0, :cond_0

    .line 476
    invoke-virtual {v2}, Lcom/android/server/wm/PreloadLifecycle;->getPreloadSchedAffinity()[I

    move-result-object v1

    monitor-exit v0

    return-object v1

    .line 478
    .end local v2    # "lifecycle":Lcom/android/server/wm/PreloadLifecycle;
    :cond_0
    goto :goto_0

    .line 479
    :cond_1
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 481
    :cond_2
    :goto_1
    sget-object v0, Lcom/android/server/am/PreloadAppControllerImpl;->sPreloadSchedAffinity:[I

    return-object v0
.end method

.method public static queryStopTimeoutFromUid(I)J
    .locals 5
    .param p0, "uid"    # I

    .line 511
    sget-object v0, Lcom/android/server/am/PreloadAppControllerImpl;->sInstance:Lcom/android/server/am/PreloadAppControllerImpl;

    if-eqz v0, :cond_2

    .line 512
    iget-object v0, v0, Lcom/android/server/am/PreloadAppControllerImpl;->mPreloadingAppLock:Ljava/lang/Object;

    monitor-enter v0

    .line 513
    :try_start_0
    sget-object v1, Lcom/android/server/am/PreloadAppControllerImpl;->sInstance:Lcom/android/server/am/PreloadAppControllerImpl;

    iget-object v1, v1, Lcom/android/server/am/PreloadAppControllerImpl;->mAllPreloadAppInfos:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/PreloadLifecycle;

    .line 514
    .local v2, "lifecycle":Lcom/android/server/wm/PreloadLifecycle;
    invoke-virtual {v2}, Lcom/android/server/wm/PreloadLifecycle;->getUid()I

    move-result v3

    if-ne v3, p0, :cond_0

    .line 515
    invoke-virtual {v2}, Lcom/android/server/wm/PreloadLifecycle;->getStopTimeout()J

    move-result-wide v3

    monitor-exit v0

    return-wide v3

    .line 517
    .end local v2    # "lifecycle":Lcom/android/server/wm/PreloadLifecycle;
    :cond_0
    goto :goto_0

    .line 518
    :cond_1
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 520
    :cond_2
    :goto_1
    const-wide/16 v0, 0x4e20

    return-wide v0
.end method

.method private realStartPreloadApp(Ljava/lang/String;Lcom/android/server/wm/PreloadLifecycle;Landroid/content/pm/ApplicationInfo;)I
    .locals 23
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "lifecycle"    # Lcom/android/server/wm/PreloadLifecycle;
    .param p3, "info"    # Landroid/content/pm/ApplicationInfo;

    .line 388
    move-object/from16 v0, p0

    move-object/from16 v11, p1

    iget-object v1, v0, Lcom/android/server/am/PreloadAppControllerImpl;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    iget-object v1, v1, Lcom/android/server/am/ActivityManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v11}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v10

    .line 390
    .local v10, "intent":Landroid/content/Intent;
    if-eqz v10, :cond_3

    .line 391
    sget-boolean v1, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    const-string v12, "PreloadAppControllerImpl"

    if-eqz v1, :cond_0

    .line 392
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "preloadAppstart app: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v12, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v10, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 395
    invoke-static {}, Landroid/app/ActivityOptions;->makeBasic()Landroid/app/ActivityOptions;

    move-result-object v9

    .line 396
    .local v9, "options":Landroid/app/ActivityOptions;
    invoke-static/range {p1 .. p1}, Lcom/android/server/wm/PreloadStateManagerImpl;->getOrCreatePreloadDisplayId(Ljava/lang/String;)I

    move-result v8

    .line 397
    .local v8, "displayId":I
    move-object/from16 v7, p2

    invoke-virtual {v7, v8}, Lcom/android/server/wm/PreloadLifecycle;->setDisplayId(I)V

    .line 398
    invoke-virtual {v9, v8}, Landroid/app/ActivityOptions;->setLaunchDisplayId(I)Landroid/app/ActivityOptions;

    .line 400
    iget-object v1, v0, Lcom/android/server/am/PreloadAppControllerImpl;->mHandler:Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;

    new-instance v2, Lcom/android/server/am/PreloadAppControllerImpl$1;

    invoke-direct {v2, v0}, Lcom/android/server/am/PreloadAppControllerImpl$1;-><init>(Lcom/android/server/am/PreloadAppControllerImpl;)V

    const-wide/16 v3, 0xbb8

    invoke-virtual {v1, v2, v3, v4}, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 407
    const-string v1, "preloadApp PreloadLifecycle.PRELOAD_SOON"

    invoke-static {v12, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    const/4 v1, 0x2

    sput v1, Lcom/android/server/am/PreloadAppControllerImpl;->mPreloadLifecycleState:I

    .line 409
    invoke-static {}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->getInstance()Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->getSPTMCloudEnableNew()Z

    move-result v1

    if-nez v1, :cond_1

    .line 410
    iget-object v12, v0, Lcom/android/server/am/PreloadAppControllerImpl;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    .line 411
    invoke-virtual {v9}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v22

    .line 410
    move-object v15, v10

    invoke-virtual/range {v12 .. v22}, Lcom/android/server/am/ActivityManagerService;->startActivity(Landroid/app/IApplicationThread;Ljava/lang/String;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILandroid/app/ProfilerInfo;Landroid/os/Bundle;)I

    move-result v1

    return v1

    .line 413
    :cond_1
    iget-object v1, v0, Lcom/android/server/am/PreloadAppControllerImpl;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    const/4 v4, 0x0

    const/4 v5, 0x0

    new-instance v6, Lcom/android/server/am/HostingRecord;

    .line 415
    invoke-virtual/range {p2 .. p2}, Lcom/android/server/wm/PreloadLifecycle;->getPreloadType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v6, v2, v11}, Lcom/android/server/am/HostingRecord;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    .line 417
    invoke-static {}, Lcom/android/server/am/ActivityManagerServiceStub;->get()Lcom/android/server/am/ActivityManagerServiceStub;

    move-result-object v2

    .line 418
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/android/server/am/ActivityManagerServiceStub;->getPackageNameByPid(I)Ljava/lang/String;

    move-result-object v16

    .line 413
    move-object/from16 v2, p1

    move-object/from16 v3, p3

    move v7, v13

    move v13, v8

    .end local v8    # "displayId":I
    .local v13, "displayId":I
    move v8, v14

    move-object v14, v9

    .end local v9    # "options":Landroid/app/ActivityOptions;
    .local v14, "options":Landroid/app/ActivityOptions;
    move v9, v15

    move-object v15, v10

    .end local v10    # "intent":Landroid/content/Intent;
    .local v15, "intent":Landroid/content/Intent;
    move-object/from16 v10, v16

    invoke-virtual/range {v1 .. v10}, Lcom/android/server/am/ActivityManagerService;->startProcessLocked(Ljava/lang/String;Landroid/content/pm/ApplicationInfo;ZILcom/android/server/am/HostingRecord;IZZLjava/lang/String;)Lcom/android/server/am/ProcessRecord;

    move-result-object v1

    if-nez v1, :cond_2

    .line 419
    const-string v1, "error in startProcessLocked!"

    invoke-static {v12, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    const/16 v1, -0x1e4

    return v1

    .line 422
    :cond_2
    const-string v1, "AMS startProcessLocked success"

    invoke-static {v12, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    const/4 v1, 0x0

    return v1

    .line 428
    .end local v13    # "displayId":I
    .end local v14    # "options":Landroid/app/ActivityOptions;
    .end local v15    # "intent":Landroid/content/Intent;
    .restart local v10    # "intent":Landroid/content/Intent;
    :cond_3
    const/16 v1, -0x1ed

    return v1
.end method

.method private removePreloadAppInfo(I)Lcom/android/server/wm/PreloadLifecycle;
    .locals 5
    .param p1, "uid"    # I

    .line 349
    sget-boolean v0, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 350
    const-string v0, "PreloadAppControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "preloadApp removePreloadAppInfo uid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mPreloadingAppLock:Ljava/lang/Object;

    monitor-enter v0

    .line 353
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mAllPreloadAppInfos:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 354
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/wm/PreloadLifecycle;>;"
    const/4 v2, 0x0

    .line 355
    .local v2, "removeItem":Lcom/android/server/wm/PreloadLifecycle;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 356
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/wm/PreloadLifecycle;

    .line 357
    .local v3, "p":Lcom/android/server/wm/PreloadLifecycle;
    invoke-virtual {v3}, Lcom/android/server/wm/PreloadLifecycle;->getUid()I

    move-result v4

    if-ne v4, p1, :cond_1

    .line 358
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 359
    move-object v2, v3

    .line 361
    .end local v3    # "p":Lcom/android/server/wm/PreloadLifecycle;
    :cond_1
    goto :goto_0

    .line 362
    :cond_2
    monitor-exit v0

    return-object v2

    .line 363
    .end local v1    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/wm/PreloadLifecycle;>;"
    .end local v2    # "removeItem":Lcom/android/server/wm/PreloadLifecycle;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private removePreloadAppInfo(Ljava/lang/String;)Lcom/android/server/wm/PreloadLifecycle;
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .line 367
    if-nez p1, :cond_0

    .line 368
    const/4 v0, 0x0

    return-object v0

    .line 370
    :cond_0
    sget-boolean v0, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 371
    const-string v0, "PreloadAppControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "preloadApp removePreloadAppInfo packageName:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    :cond_1
    iget-object v0, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mPreloadingAppLock:Ljava/lang/Object;

    monitor-enter v0

    .line 374
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mAllPreloadAppInfos:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 375
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/wm/PreloadLifecycle;>;"
    const/4 v2, 0x0

    .line 376
    .local v2, "removeItem":Lcom/android/server/wm/PreloadLifecycle;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 377
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/wm/PreloadLifecycle;

    .line 378
    .local v3, "p":Lcom/android/server/wm/PreloadLifecycle;
    invoke-virtual {v3}, Lcom/android/server/wm/PreloadLifecycle;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 379
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 380
    move-object v2, v3

    .line 382
    .end local v3    # "p":Lcom/android/server/wm/PreloadLifecycle;
    :cond_2
    goto :goto_0

    .line 383
    :cond_3
    monitor-exit v0

    return-object v2

    .line 384
    .end local v1    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/wm/PreloadLifecycle;>;"
    .end local v2    # "removeItem":Lcom/android/server/wm/PreloadLifecycle;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private startPreloadApp(Lcom/android/server/wm/PreloadLifecycle;)I
    .locals 13
    .param p1, "lifecycle"    # Lcom/android/server/wm/PreloadLifecycle;

    .line 143
    invoke-virtual {p1}, Lcom/android/server/wm/PreloadLifecycle;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 144
    .local v0, "packageName":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/android/server/wm/PreloadLifecycle;->isIgnoreMemory()Z

    move-result v1

    .line 145
    .local v1, "ignoreMemory":Z
    invoke-virtual {p1}, Lcom/android/server/wm/PreloadLifecycle;->getConfig()Lmiui/process/LifecycleConfig;

    move-result-object v2

    .line 146
    .local v2, "config":Lmiui/process/LifecycleConfig;
    sget-boolean v3, Lcom/android/server/am/PreloadAppControllerImpl;->ENABLE:Z

    const-string v4, "PreloadAppControllerImpl"

    if-nez v3, :cond_1

    .line 147
    sget-boolean v3, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 148
    const-string v3, "preloadApp is disable!!!"

    invoke-static {v4, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    :cond_0
    const/16 v3, -0x1e6

    return v3

    .line 153
    :cond_1
    iget-boolean v3, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mSpeedTestState:Z

    const/4 v5, 0x1

    if-eqz v3, :cond_3

    .line 154
    invoke-virtual {p1}, Lcom/android/server/wm/PreloadLifecycle;->getPreloadType()I

    move-result v3

    if-eq v3, v5, :cond_3

    .line 155
    sget-boolean v3, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    if-eqz v3, :cond_2

    .line 156
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "preloadApp fail! mSpeedTestState="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v5, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mSpeedTestState:Z

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    :cond_2
    const/16 v3, -0x1e4

    return v3

    .line 161
    :cond_3
    iget-object v3, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mProcessManagerService:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v3}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z

    move-result v3

    if-nez v3, :cond_5

    .line 162
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " Permission Denial: from pid="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 163
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", uid="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 164
    .local v3, "msg":Ljava/lang/String;
    sget-boolean v5, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    if-eqz v5, :cond_4

    .line 165
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "preloadApp"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    :cond_4
    const/16 v4, -0x1e8

    return v4

    .line 169
    .end local v3    # "msg":Ljava/lang/String;
    :cond_5
    if-nez v0, :cond_6

    .line 170
    const/16 v3, -0x1ef

    return v3

    .line 172
    :cond_6
    invoke-static {v0}, Lcom/android/server/am/PreloadAppControllerImpl;->inBlackList(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 173
    const/16 v3, -0x1eb

    return v3

    .line 175
    :cond_7
    if-nez v2, :cond_8

    .line 176
    const/16 v3, -0x1e5

    return v3

    .line 178
    :cond_8
    iget-object v3, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mProcessManagerService:Lcom/android/server/am/ProcessManagerService;

    .line 179
    invoke-virtual {v3, v0}, Lcom/android/server/am/ProcessManagerService;->frequentlyKilledForPreload(Ljava/lang/String;)Z

    move-result v3

    const-string v6, "preloadApp skip "

    if-eqz v3, :cond_a

    invoke-virtual {v2}, Lmiui/process/LifecycleConfig;->forceStart()Z

    move-result v3

    if-nez v3, :cond_a

    .line 180
    sget-boolean v3, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    if-eqz v3, :cond_9

    .line 181
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", because of errors or killed by user before"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    :cond_9
    const/16 v3, -0x1e7

    return v3

    .line 186
    :cond_a
    if-nez v1, :cond_c

    invoke-static {}, Lcom/android/server/am/ProcessUtils;->isLowMemory()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 187
    sget-boolean v3, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    if-eqz v3, :cond_b

    .line 188
    const-string v3, "low memory! skip preloadApp!"

    invoke-static {v4, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    :cond_b
    const/16 v3, -0x1ee

    return v3

    .line 193
    :cond_c
    iget-object v3, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mAtmInternal:Lcom/android/server/wm/ActivityTaskManagerInternal;

    invoke-virtual {v3}, Lcom/android/server/wm/ActivityTaskManagerInternal;->getTopApp()Lcom/android/server/wm/WindowProcessController;

    move-result-object v3

    .line 194
    .local v3, "wpc":Lcom/android/server/wm/WindowProcessController;
    if-eqz v3, :cond_d

    iget-object v7, v3, Lcom/android/server/wm/WindowProcessController;->mOwner:Ljava/lang/Object;

    check-cast v7, Lcom/android/server/am/ProcessRecord;

    goto :goto_0

    :cond_d
    const/4 v7, 0x0

    .line 196
    .local v7, "r":Lcom/android/server/am/ProcessRecord;
    :goto_0
    if-eqz v7, :cond_f

    iget-object v8, v7, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-static {v8}, Lcom/android/server/am/PreloadAppControllerImpl;->inTopAppBlackList(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_f

    .line 197
    sget-boolean v5, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    if-eqz v5, :cond_e

    .line 198
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "because topApp is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v7, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    :cond_e
    const/16 v4, -0x1ec

    return v4

    .line 203
    :cond_f
    const/4 v6, 0x0

    .line 205
    .local v6, "info":Landroid/content/pm/ApplicationInfo;
    const/16 v8, -0x1f3

    :try_start_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v9

    const-wide/16 v10, 0x400

    const/4 v12, 0x0

    invoke-interface {v9, v0, v10, v11, v12}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;JI)Landroid/content/pm/ApplicationInfo;

    move-result-object v9
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v6, v9

    .line 212
    nop

    .line 214
    if-nez v6, :cond_10

    .line 215
    return v8

    .line 217
    :cond_10
    iget v8, v6, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/2addr v5, v8

    if-lez v5, :cond_11

    .line 218
    const/16 v4, -0x1f2

    return v4

    .line 221
    :cond_11
    iget-object v5, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    iget-object v5, v5, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    .line 222
    invoke-virtual {v5}, Lcom/android/server/am/ProcessList;->getProcessNamesLOSP()Lcom/android/server/am/ProcessList$MyProcessMap;

    move-result-object v5

    iget v8, v6, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v5, v0, v8}, Lcom/android/server/am/ProcessList$MyProcessMap;->get(Ljava/lang/String;I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/am/ProcessRecord;

    .line 224
    .local v5, "app":Lcom/android/server/am/ProcessRecord;
    if-eqz v5, :cond_16

    .line 225
    invoke-virtual {v5}, Lcom/android/server/am/ProcessRecord;->isPersistent()Z

    move-result v8

    const-string v9, "preloadApp: "

    if-eqz v8, :cond_13

    .line 226
    sget-boolean v8, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    if-eqz v8, :cond_12

    .line 227
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " is persistent, skip!"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    :cond_12
    const/16 v4, -0x1f0

    return v4

    .line 231
    :cond_13
    invoke-static {}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->getInstance()Lcom/miui/server/sptm/SpeedTestModeServiceImpl;

    move-result-object v8

    invoke-virtual {v8}, Lcom/miui/server/sptm/SpeedTestModeServiceImpl;->isSpeedTestMode()Z

    move-result v8

    if-nez v8, :cond_15

    .line 232
    sget-boolean v8, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    if-eqz v8, :cond_14

    .line 233
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " already exits, skip it"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    :cond_14
    const/16 v4, -0x1f1

    return v4

    .line 238
    :cond_15
    iget-object v4, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    iget-object v8, v5, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v8, v8, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget v9, v5, Lcom/android/server/am/ProcessRecord;->userId:I

    const-string v10, "preload app exits"

    invoke-virtual {v4, v8, v9, v12, v10}, Lcom/android/server/am/ActivityManagerService;->forceStopPackage(Ljava/lang/String;IILjava/lang/String;)V

    .line 243
    :cond_16
    iget v4, v6, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {p1, v4}, Lcom/android/server/wm/PreloadLifecycle;->setUid(I)V

    .line 244
    invoke-direct {p0, v0, p1, v6}, Lcom/android/server/am/PreloadAppControllerImpl;->realStartPreloadApp(Ljava/lang/String;Lcom/android/server/wm/PreloadLifecycle;Landroid/content/pm/ApplicationInfo;)I

    move-result v4

    return v4

    .line 207
    .end local v5    # "app":Lcom/android/server/am/ProcessRecord;
    :catch_0
    move-exception v5

    .line 208
    .local v5, "e":Landroid/os/RemoteException;
    sget-boolean v9, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    if-eqz v9, :cond_17

    .line 209
    const-string v9, "preloadApp error in getApplicationInfo!"

    invoke-static {v4, v9, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 211
    :cond_17
    return v8
.end method

.method private unRegistPreloadCallback(I)V
    .locals 2
    .param p1, "type"    # I

    .line 619
    iget-object v0, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mHandler:Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 620
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 621
    iget-object v1, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mHandler:Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;

    invoke-virtual {v1, v0}, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->sendMessage(Landroid/os/Message;)Z

    .line 622
    return-void
.end method


# virtual methods
.method public getPreloadingApps()Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 277
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 278
    .local v0, "packages":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mPreloadingAppLock:Ljava/lang/Object;

    monitor-enter v1

    .line 279
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mAllPreloadAppInfos:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/wm/PreloadLifecycle;

    .line 280
    .local v3, "lifecycle":Lcom/android/server/wm/PreloadLifecycle;
    invoke-virtual {v3}, Lcom/android/server/wm/PreloadLifecycle;->isAlreadyPreloaded()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 281
    invoke-virtual {v3}, Lcom/android/server/wm/PreloadLifecycle;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 283
    .end local v3    # "lifecycle":Lcom/android/server/wm/PreloadLifecycle;
    :cond_0
    goto :goto_0

    .line 284
    :cond_1
    monitor-exit v1

    .line 285
    return-object v0

    .line 284
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public init(Lcom/android/server/am/ActivityManagerService;Lcom/android/server/am/ProcessManagerService;Lcom/android/server/ServiceThread;)V
    .locals 2
    .param p1, "ams"    # Lcom/android/server/am/ActivityManagerService;
    .param p2, "processManagerService"    # Lcom/android/server/am/ProcessManagerService;
    .param p3, "thread"    # Lcom/android/server/ServiceThread;

    .line 102
    iget-object v0, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mHandler:Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;

    if-eqz v0, :cond_0

    .line 103
    return-void

    .line 106
    :cond_0
    invoke-static {}, Lcom/android/server/am/PreloadAppControllerImpl;->getInstance()Lcom/android/server/am/PreloadAppControllerImpl;

    move-result-object v0

    sput-object v0, Lcom/android/server/am/PreloadAppControllerImpl;->sInstance:Lcom/android/server/am/PreloadAppControllerImpl;

    .line 107
    iput-object p1, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    .line 108
    iput-object p2, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mProcessManagerService:Lcom/android/server/am/ProcessManagerService;

    .line 109
    new-instance v0, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;

    invoke-virtual {p3}, Lcom/android/server/ServiceThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;-><init>(Lcom/android/server/am/PreloadAppControllerImpl;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mHandler:Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;

    .line 111
    invoke-direct {p0}, Lcom/android/server/am/PreloadAppControllerImpl;->initPreloadEnableState()V

    .line 112
    return-void
.end method

.method public interceptWindowInjector(Landroid/view/WindowManager$LayoutParams;)Z
    .locals 3
    .param p1, "attrs"    # Landroid/view/WindowManager$LayoutParams;

    .line 433
    iget-object v0, p1, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/server/am/PreloadAppControllerImpl;->inWindowBlackList(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 434
    sget-boolean v0, Lcom/android/server/am/PreloadAppControllerImpl;->DEBUG:Z

    const-string v1, "PreloadAppControllerImpl"

    if-eqz v0, :cond_0

    .line 435
    const-string v0, "preloadApp inWindowBlackList"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    :cond_0
    sget v0, Lcom/android/server/am/PreloadAppControllerImpl;->mPreloadLifecycleState:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    .line 439
    const-string v0, "preloadApp window should add to PreloadDisplay"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    const/4 v0, 0x1

    return v0

    .line 443
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public killPreloadApp(Ljava/lang/String;)I
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .line 639
    iget-object v0, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mProcessManagerService:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessManagerService;->checkPermission()Z

    move-result v0

    if-nez v0, :cond_0

    .line 640
    const/16 v0, -0x1e8

    return v0

    .line 643
    :cond_0
    if-nez p1, :cond_1

    .line 644
    const/16 v0, -0x1ef

    return v0

    .line 647
    :cond_1
    iget-object v0, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mPreloadingAppLock:Ljava/lang/Object;

    monitor-enter v0

    .line 648
    :try_start_0
    invoke-static {p1}, Lcom/android/server/am/PreloadAppControllerImpl;->getUidFromPackageName(Ljava/lang/String;)I

    move-result v1

    .line 649
    .local v1, "uid":I
    if-gez v1, :cond_2

    .line 650
    monitor-exit v0

    const/16 v0, -0x1e3

    return v0

    .line 652
    :cond_2
    const-string v2, "PreloadAppControllerImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " advance kill preloadApp "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 653
    invoke-static {p1, v1}, Lcom/android/server/wm/PreloadStateManagerImpl;->killPreloadApp(Ljava/lang/String;I)V

    .line 654
    .end local v1    # "uid":I
    monitor-exit v0

    .line 656
    const/16 v0, 0x1f4

    return v0

    .line 654
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onPreloadAppKilled(I)V
    .locals 0
    .param p1, "uid"    # I

    .line 335
    invoke-direct {p0, p1}, Lcom/android/server/am/PreloadAppControllerImpl;->removePreloadAppInfo(I)Lcom/android/server/wm/PreloadLifecycle;

    .line 336
    return-void
.end method

.method public onPreloadAppStarted(ILjava/lang/String;I)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "pid"    # I

    .line 326
    invoke-direct {p0, p1}, Lcom/android/server/am/PreloadAppControllerImpl;->removePreloadAppInfo(I)Lcom/android/server/wm/PreloadLifecycle;

    .line 327
    iget-object v0, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mHandler:Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 328
    .local v0, "msg":Landroid/os/Message;
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 329
    iput p3, v0, Landroid/os/Message;->arg1:I

    .line 330
    invoke-direct {p0, p2}, Lcom/android/server/am/PreloadAppControllerImpl;->getPreloadType(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Landroid/os/Message;->arg2:I

    .line 331
    iget-object v1, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mHandler:Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;

    invoke-virtual {v1, v0}, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->sendMessage(Landroid/os/Message;)Z

    .line 332
    return-void
.end method

.method public onProcessKilled(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "reason"    # Ljava/lang/String;
    .param p2, "processName"    # Ljava/lang/String;

    .line 339
    const-string/jumbo v0, "timeout_kill_preloadApp"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 340
    invoke-static {p2}, Lcom/android/server/am/PreloadAppControllerImpl;->getUidFromPackageName(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/android/server/wm/PreloadStateManagerImpl;->enableAudio(I)V

    .line 342
    :cond_0
    return-void
.end method

.method protected preloadAppEnqueue(Ljava/lang/String;ZLmiui/process/LifecycleConfig;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "ignoreMemory"    # Z
    .param p3, "config"    # Lmiui/process/LifecycleConfig;

    .line 134
    invoke-direct {p0, p2, p1, p3}, Lcom/android/server/am/PreloadAppControllerImpl;->createPreloadLifeCycle(ZLjava/lang/String;Lmiui/process/LifecycleConfig;)Lcom/android/server/wm/PreloadLifecycle;

    move-result-object v0

    .line 135
    .local v0, "lifecycle":Lcom/android/server/wm/PreloadLifecycle;
    if-eqz v0, :cond_0

    .line 136
    iget-object v1, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mHandler:Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    .line 137
    .local v1, "msg":Landroid/os/Message;
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 138
    iget-object v2, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mHandler:Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;

    invoke-virtual {v2, v1}, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->sendMessage(Landroid/os/Message;)Z

    .line 140
    .end local v1    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method public registerPreloadCallback(Lmiui/process/IPreloadCallback;I)V
    .locals 4
    .param p1, "callback"    # Lmiui/process/IPreloadCallback;
    .param p2, "type"    # I

    .line 626
    const-string v0, "PreloadAppControllerImpl"

    :try_start_0
    invoke-interface {p1}, Lmiui/process/IPreloadCallback;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    new-instance v2, Lcom/android/server/am/PreloadAppControllerImpl$CallbackDeathRecipient;

    invoke-direct {v2, p0, p2}, Lcom/android/server/am/PreloadAppControllerImpl$CallbackDeathRecipient;-><init>(Lcom/android/server/am/PreloadAppControllerImpl;I)V

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 630
    nop

    .line 631
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "registPreloadCallback , type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "pid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 632
    iget-object v0, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mHandler:Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 633
    .local v0, "msg":Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 634
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 635
    iget-object v1, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mHandler:Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;

    invoke-virtual {v1, v0}, Lcom/android/server/am/PreloadAppControllerImpl$PreloadHandler;->sendMessage(Landroid/os/Message;)Z

    .line 636
    return-void

    .line 627
    .end local v0    # "msg":Landroid/os/Message;
    :catch_0
    move-exception v1

    .line 628
    .local v1, "e":Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "registPreloadCallback fail due to linkToDeath, type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 629
    return-void
.end method

.method public setSpeedTestState(Z)V
    .locals 0
    .param p1, "speedTestState"    # Z

    .line 345
    iput-boolean p1, p0, Lcom/android/server/am/PreloadAppControllerImpl;->mSpeedTestState:Z

    .line 346
    return-void
.end method
