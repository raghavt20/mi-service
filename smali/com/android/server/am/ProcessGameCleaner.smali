.class public Lcom/android/server/am/ProcessGameCleaner;
.super Ljava/lang/Object;
.source "ProcessGameCleaner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/ProcessGameCleaner$H;
    }
.end annotation


# static fields
.field private static CLOD_START_FOR_GAME_DALAYED:I = 0x0

.field private static HOT_START_FOR_GAME_DALAYED:I = 0x0

.field private static final TAG:Ljava/lang/String; = "ProcessGameCleaner"


# instance fields
.field private mAms:Lcom/android/server/am/ActivityManagerService;

.field private mHandler:Lcom/android/server/am/ProcessGameCleaner$H;

.field private mMethodUpdateOomForGame:Ljava/lang/reflect/Method;

.field private mPms:Lcom/android/server/am/ProcessManagerService;

.field private mSystemPressureCtl:Lcom/android/server/am/SystemPressureController;


# direct methods
.method static bridge synthetic -$$Nest$muploadLmkdOomForGame(Lcom/android/server/am/ProcessGameCleaner;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessGameCleaner;->uploadLmkdOomForGame(Z)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 18
    const/4 v0, 0x0

    sput v0, Lcom/android/server/am/ProcessGameCleaner;->HOT_START_FOR_GAME_DALAYED:I

    .line 19
    const/16 v0, 0x7530

    sput v0, Lcom/android/server/am/ProcessGameCleaner;->CLOD_START_FOR_GAME_DALAYED:I

    return-void
.end method

.method public constructor <init>(Lcom/android/server/am/ActivityManagerService;)V
    .locals 0
    .param p1, "ams"    # Lcom/android/server/am/ActivityManagerService;

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/android/server/am/ProcessGameCleaner;->mAms:Lcom/android/server/am/ActivityManagerService;

    .line 29
    return-void
.end method

.method private isColdStartApp(I)Z
    .locals 5
    .param p1, "pid"    # I

    .line 58
    iget-object v0, p0, Lcom/android/server/am/ProcessGameCleaner;->mPms:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;

    move-result-object v0

    .line 59
    .local v0, "proc":Lcom/android/server/am/ProcessRecord;
    iget-object v1, v0, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessStateRecord;->getLastTopTime()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    .line 60
    const/4 v1, 0x1

    return v1

    .line 62
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method private uploadLmkdOomForGame(Z)V
    .locals 5
    .param p1, "isEnable"    # Z

    .line 67
    :try_start_0
    iget-object v0, p0, Lcom/android/server/am/ProcessGameCleaner;->mMethodUpdateOomForGame:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/android/server/am/ProcessGameCleaner;->mAms:Lcom/android/server/am/ActivityManagerService;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    goto :goto_0

    .line 68
    :catch_0
    move-exception v0

    .line 69
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ProcessGameCleaner"

    const-string v2, "game oom update error!"

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method


# virtual methods
.method public foregroundInfoChanged(Ljava/lang/String;I)V
    .locals 5
    .param p1, "pckName"    # Ljava/lang/String;
    .param p2, "pid"    # I

    .line 40
    iget-object v0, p0, Lcom/android/server/am/ProcessGameCleaner;->mMethodUpdateOomForGame:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/server/am/ProcessGameCleaner;->mAms:Lcom/android/server/am/ActivityManagerService;

    if-nez v0, :cond_0

    goto :goto_2

    .line 43
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ProcessGameCleaner;->mHandler:Lcom/android/server/am/ProcessGameCleaner$H;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessGameCleaner$H;->removeMessages(I)V

    .line 44
    iget-object v0, p0, Lcom/android/server/am/ProcessGameCleaner;->mSystemPressureCtl:Lcom/android/server/am/SystemPressureController;

    invoke-virtual {v0, p1}, Lcom/android/server/am/SystemPressureController;->isGameApp(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 45
    iget-object v0, p0, Lcom/android/server/am/ProcessGameCleaner;->mHandler:Lcom/android/server/am/ProcessGameCleaner$H;

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessGameCleaner$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 46
    .local v0, "msg":Landroid/os/Message;
    invoke-direct {p0, p2}, Lcom/android/server/am/ProcessGameCleaner;->isColdStartApp(I)Z

    move-result v2

    .line 47
    .local v2, "isClodApp":Z
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 48
    iget-object v1, p0, Lcom/android/server/am/ProcessGameCleaner;->mHandler:Lcom/android/server/am/ProcessGameCleaner$H;

    .line 49
    if-eqz v2, :cond_1

    sget v3, Lcom/android/server/am/ProcessGameCleaner;->CLOD_START_FOR_GAME_DALAYED:I

    goto :goto_0

    :cond_1
    sget v3, Lcom/android/server/am/ProcessGameCleaner;->HOT_START_FOR_GAME_DALAYED:I

    :goto_0
    int-to-long v3, v3

    .line 48
    invoke-virtual {v1, v0, v3, v4}, Lcom/android/server/am/ProcessGameCleaner$H;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 50
    .end local v0    # "msg":Landroid/os/Message;
    .end local v2    # "isClodApp":Z
    goto :goto_1

    .line 51
    :cond_2
    iget-object v0, p0, Lcom/android/server/am/ProcessGameCleaner;->mHandler:Lcom/android/server/am/ProcessGameCleaner$H;

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessGameCleaner$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 52
    .restart local v0    # "msg":Landroid/os/Message;
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 53
    iget-object v1, p0, Lcom/android/server/am/ProcessGameCleaner;->mHandler:Lcom/android/server/am/ProcessGameCleaner$H;

    invoke-virtual {v1, v0}, Lcom/android/server/am/ProcessGameCleaner$H;->sendMessage(Landroid/os/Message;)Z

    .line 55
    .end local v0    # "msg":Landroid/os/Message;
    :goto_1
    return-void

    .line 41
    :cond_3
    :goto_2
    return-void
.end method

.method public onSystemReady(Lcom/android/server/am/ProcessManagerService;Landroid/os/Looper;)V
    .locals 3
    .param p1, "pms"    # Lcom/android/server/am/ProcessManagerService;
    .param p2, "myLooper"    # Landroid/os/Looper;

    .line 32
    iget-object v0, p0, Lcom/android/server/am/ProcessGameCleaner;->mAms:Lcom/android/server/am/ActivityManagerService;

    .line 33
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    filled-new-array {v1}, [Ljava/lang/Class;

    move-result-object v1

    .line 32
    const-string/jumbo v2, "updateOomForGame"

    invoke-static {v0, v2, v1}, Lmiui/util/ReflectionUtils;->tryFindMethodExact(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/ProcessGameCleaner;->mMethodUpdateOomForGame:Ljava/lang/reflect/Method;

    .line 34
    invoke-static {}, Lcom/android/server/am/SystemPressureController;->getInstance()Lcom/android/server/am/SystemPressureController;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/ProcessGameCleaner;->mSystemPressureCtl:Lcom/android/server/am/SystemPressureController;

    .line 35
    iput-object p1, p0, Lcom/android/server/am/ProcessGameCleaner;->mPms:Lcom/android/server/am/ProcessManagerService;

    .line 36
    new-instance v0, Lcom/android/server/am/ProcessGameCleaner$H;

    invoke-direct {v0, p0, p2}, Lcom/android/server/am/ProcessGameCleaner$H;-><init>(Lcom/android/server/am/ProcessGameCleaner;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/am/ProcessGameCleaner;->mHandler:Lcom/android/server/am/ProcessGameCleaner$H;

    .line 37
    return-void
.end method
