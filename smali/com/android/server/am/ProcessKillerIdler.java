public class com.android.server.am.ProcessKillerIdler extends android.app.job.JobService {
	 /* .source "ProcessKillerIdler.java" */
	 /* # static fields */
	 static final Long APP_MEM_THRESHOLD;
	 static final Long BACKUP_APP_MEM_THRESHOLD;
	 static final Long BACKUP_MEM_THRESHOLD;
	 static final Long CHECK_FREE_MEM_TIME;
	 private static Integer PROCESS_KILL_JOB_ID;
	 private static final java.lang.String TAG;
	 static final java.util.ArrayList blackList;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/ArrayList<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private static android.content.ComponentName cm;
/* # instance fields */
private com.android.server.am.ActivityManagerService mAm;
/* # direct methods */
static com.android.server.am.ProcessKillerIdler ( ) {
/* .locals 3 */
/* .line 20 */
/* new-instance v0, Landroid/content/ComponentName; */
/* .line 21 */
/* const-class v1, Lcom/android/server/am/ProcessKillerIdler; */
(( java.lang.Class ) v1 ).getName ( ); // invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;
final String v2 = "android"; // const-string v2, "android"
/* invoke-direct {v0, v2, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 23 */
/* const/16 v0, 0x64 */
/* .line 30 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 38 */
final String v1 = "com.tencent.mm"; // const-string v1, "com.tencent.mm"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 39 */
final String v1 = "com.tencent.mobileqq"; // const-string v1, "com.tencent.mobileqq"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 40 */
return;
} // .end method
public com.android.server.am.ProcessKillerIdler ( ) {
/* .locals 1 */
/* .line 17 */
/* invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V */
/* .line 42 */
/* nop */
/* .line 43 */
android.app.ActivityManagerNative .getDefault ( );
/* check-cast v0, Lcom/android/server/am/ActivityManagerService; */
this.mAm = v0;
/* .line 42 */
return;
} // .end method
public static void schedule ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 100 */
final String v0 = "jobscheduler"; // const-string v0, "jobscheduler"
(( android.content.Context ) p0 ).getSystemService ( v0 ); // invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/job/JobScheduler; */
/* .line 102 */
/* .local v0, "jobScheduler":Landroid/app/job/JobScheduler; */
/* new-instance v1, Landroid/app/job/JobInfo$Builder; */
v3 = com.android.server.am.ProcessKillerIdler.cm;
/* invoke-direct {v1, v2, v3}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V */
/* .line 103 */
/* .local v1, "builder":Landroid/app/job/JobInfo$Builder; */
/* const-wide/32 v2, 0x1499700 */
(( android.app.job.JobInfo$Builder ) v1 ).setMinimumLatency ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/app/job/JobInfo$Builder;->setMinimumLatency(J)Landroid/app/job/JobInfo$Builder;
/* .line 104 */
(( android.app.job.JobInfo$Builder ) v1 ).build ( ); // invoke-virtual {v1}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;
(( android.app.job.JobScheduler ) v0 ).schedule ( v2 ); // invoke-virtual {v0, v2}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I
/* .line 105 */
return;
} // .end method
/* # virtual methods */
public Boolean onStartJob ( android.app.job.JobParameters p0 ) {
/* .locals 17 */
/* .param p1, "params" # Landroid/app/job/JobParameters; */
/* .line 47 */
/* move-object/from16 v1, p0 */
final String v0 = "ProcessKillerIdler"; // const-string v0, "ProcessKillerIdler"
final String v2 = "ProcessKillerIdler onStartJob"; // const-string v2, "ProcessKillerIdler onStartJob"
android.util.Slog .w ( v0,v2 );
/* .line 48 */
v2 = this.mAm;
/* monitor-enter v2 */
/* .line 49 */
try { // :try_start_0
	 v0 = this.mAm;
	 /* const/16 v3, 0x12c */
	 int v4 = 0; // const/4 v4, 0x0
	 com.android.server.am.ProcessUtils .getProcessListByAdj ( v0,v3,v4 );
	 /* .line 51 */
	 v3 = 	 /* .local v0, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;" */
	 /* .line 52 */
	 /* .local v3, "N":I */
	 /* const-wide/16 v4, 0x0 */
	 /* .line 53 */
	 /* .local v4, "totalBackupProcsMem":J */
	 /* new-instance v6, Ljava/util/ArrayList; */
	 /* invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V */
	 /* .line 55 */
	 /* .local v6, "backupProcs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;" */
	 int v7 = 0; // const/4 v7, 0x0
	 /* .local v7, "i":I */
} // :goto_0
int v8 = 3; // const/4 v8, 0x3
int v9 = 1; // const/4 v9, 0x1
/* const-wide/16 v10, 0x400 */
/* if-ge v7, v3, :cond_2 */
/* .line 56 */
/* check-cast v12, Lcom/android/server/am/ProcessRecord; */
/* .line 57 */
/* .local v12, "app":Lcom/android/server/am/ProcessRecord; */
v13 = com.android.server.am.ProcessKillerIdler.blackList;
v14 = this.processName;
v13 = (( java.util.ArrayList ) v13 ).contains ( v14 ); // invoke-virtual {v13, v14}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v13 != null) { // if-eqz v13, :cond_0
	 v13 = this.mProfile;
	 /* .line 58 */
	 (( com.android.server.am.ProcessProfileRecord ) v13 ).getLastPss ( ); // invoke-virtual {v13}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J
	 /* move-result-wide v13 */
	 /* div-long/2addr v13, v10 */
	 /* const-wide/16 v15, 0x12c */
	 /* cmp-long v13, v13, v15 */
	 /* if-lez v13, :cond_0 */
	 /* .line 60 */
	 final String v13 = "ProcessKillerIdler"; // const-string v13, "ProcessKillerIdler"
	 /* new-instance v14, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v15 = "killing process "; // const-string v15, "killing process "
	 (( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v15 = this.processName;
	 (( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 final String v15 = " pid "; // const-string v15, " pid "
	 (( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* .line 61 */
	 v15 = 	 (( com.android.server.am.ProcessRecord ) v12 ).getPid ( ); // invoke-virtual {v12}, Lcom/android/server/am/ProcessRecord;->getPid()I
	 (( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v15 = " size "; // const-string v15, " size "
	 (( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v15 = this.mProfile;
	 (( com.android.server.am.ProcessProfileRecord ) v15 ).getLastPss ( ); // invoke-virtual {v15}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J
	 /* move-result-wide v15 */
	 /* div-long v10, v15, v10 */
	 (( java.lang.StringBuilder ) v14 ).append ( v10, v11 ); // invoke-virtual {v14, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 /* .line 60 */
	 android.util.Slog .w ( v13,v10 );
	 /* .line 62 */
	 final String v10 = "low mem kill"; // const-string v10, "low mem kill"
	 (( com.android.server.am.ProcessRecord ) v12 ).killLocked ( v10, v8, v9 ); // invoke-virtual {v12, v10, v8, v9}, Lcom/android/server/am/ProcessRecord;->killLocked(Ljava/lang/String;IZ)V
	 /* .line 64 */
	 /* .line 67 */
} // :cond_0
v8 = this.mState;
v8 = (( com.android.server.am.ProcessStateRecord ) v8 ).getSetAdj ( ); // invoke-virtual {v8}, Lcom/android/server/am/ProcessStateRecord;->getSetAdj()I
/* const/16 v9, 0x190 */
/* if-ne v8, v9, :cond_1 */
/* .line 68 */
v8 = this.mProfile;
(( com.android.server.am.ProcessProfileRecord ) v8 ).getLastPss ( ); // invoke-virtual {v8}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J
/* move-result-wide v8 */
/* add-long/2addr v4, v8 */
/* .line 69 */
(( java.util.ArrayList ) v6 ).add ( v12 ); // invoke-virtual {v6, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 55 */
} // .end local v12 # "app":Lcom/android/server/am/ProcessRecord;
} // :cond_1
} // :goto_1
/* add-int/lit8 v7, v7, 0x1 */
/* .line 74 */
} // .end local v7 # "i":I
} // :cond_2
/* div-long v12, v4, v10 */
/* const-wide/16 v14, 0x3e8 */
/* cmp-long v7, v12, v14 */
/* if-ltz v7, :cond_4 */
/* .line 75 */
int v7 = 0; // const/4 v7, 0x0
/* .restart local v7 # "i":I */
} // :goto_2
v12 = (( java.util.ArrayList ) v6 ).size ( ); // invoke-virtual {v6}, Ljava/util/ArrayList;->size()I
/* if-ge v7, v12, :cond_4 */
/* .line 76 */
(( java.util.ArrayList ) v6 ).get ( v7 ); // invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v12, Lcom/android/server/am/ProcessRecord; */
/* .line 77 */
/* .restart local v12 # "app":Lcom/android/server/am/ProcessRecord; */
v13 = this.mProfile;
(( com.android.server.am.ProcessProfileRecord ) v13 ).getLastPss ( ); // invoke-virtual {v13}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J
/* move-result-wide v13 */
/* div-long/2addr v13, v10 */
/* const-wide/16 v15, 0x64 */
/* cmp-long v13, v13, v15 */
/* if-lez v13, :cond_3 */
/* .line 79 */
final String v13 = "ProcessKillerIdler"; // const-string v13, "ProcessKillerIdler"
/* new-instance v14, Ljava/lang/StringBuilder; */
/* invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V */
final String v15 = "killing process "; // const-string v15, "killing process "
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v15 = this.processName;
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v15 = " pid "; // const-string v15, " pid "
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 80 */
v15 = (( com.android.server.am.ProcessRecord ) v12 ).getPid ( ); // invoke-virtual {v12}, Lcom/android/server/am/ProcessRecord;->getPid()I
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v15 = " size "; // const-string v15, " size "
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v15 = this.mProfile;
(( com.android.server.am.ProcessProfileRecord ) v15 ).getLastPss ( ); // invoke-virtual {v15}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J
/* move-result-wide v15 */
/* div-long v8, v15, v10 */
(( java.lang.StringBuilder ) v14 ).append ( v8, v9 ); // invoke-virtual {v14, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v9 = " reason: backup procs\' totalMem is too big, need to kill big mem proc"; // const-string v9, " reason: backup procs\' totalMem is too big, need to kill big mem proc"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 79 */
android.util.Slog .w ( v13,v8 );
/* .line 82 */
final String v8 = "low mem kill"; // const-string v8, "low mem kill"
int v9 = 3; // const/4 v9, 0x3
int v13 = 1; // const/4 v13, 0x1
(( com.android.server.am.ProcessRecord ) v12 ).killLocked ( v8, v9, v13 ); // invoke-virtual {v12, v8, v9, v13}, Lcom/android/server/am/ProcessRecord;->killLocked(Ljava/lang/String;IZ)V
/* .line 77 */
} // :cond_3
/* move v13, v9 */
/* move v9, v8 */
/* .line 75 */
} // .end local v12 # "app":Lcom/android/server/am/ProcessRecord;
} // :goto_3
/* add-int/lit8 v7, v7, 0x1 */
/* move v8, v9 */
/* move v9, v13 */
/* .line 86 */
} // .end local v0 # "procs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
} // .end local v3 # "N":I
} // .end local v4 # "totalBackupProcsMem":J
} // .end local v6 # "backupProcs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;"
} // .end local v7 # "i":I
} // :cond_4
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 88 */
int v0 = 0; // const/4 v0, 0x0
/* move-object/from16 v3, p1 */
(( com.android.server.am.ProcessKillerIdler ) v1 ).jobFinished ( v3, v0 ); // invoke-virtual {v1, v3, v0}, Lcom/android/server/am/ProcessKillerIdler;->jobFinished(Landroid/app/job/JobParameters;Z)V
/* .line 89 */
/* invoke-static/range {p0 ..p0}, Lcom/android/server/am/ProcessKillerIdler;->schedule(Landroid/content/Context;)V */
/* .line 90 */
/* .line 86 */
/* :catchall_0 */
/* move-exception v0 */
/* move-object/from16 v3, p1 */
} // :goto_4
try { // :try_start_1
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* throw v0 */
/* :catchall_1 */
/* move-exception v0 */
} // .end method
public Boolean onStopJob ( android.app.job.JobParameters p0 ) {
/* .locals 2 */
/* .param p1, "params" # Landroid/app/job/JobParameters; */
/* .line 95 */
final String v0 = "ProcessKillerIdler"; // const-string v0, "ProcessKillerIdler"
final String v1 = "ProcessKillerIdler onStopJob"; // const-string v1, "ProcessKillerIdler onStopJob"
android.util.Slog .w ( v0,v1 );
/* .line 96 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
