class com.android.server.am.ProcessProphetImpl$ProcessProphetShellCmd extends android.os.ShellCommand {
	 /* .source "ProcessProphetImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/ProcessProphetImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "ProcessProphetShellCmd" */
} // .end annotation
/* # instance fields */
final com.android.server.am.ProcessProphetImpl this$0; //synthetic
/* # direct methods */
private com.android.server.am.ProcessProphetImpl$ProcessProphetShellCmd ( ) {
/* .locals 0 */
/* .line 1179 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/os/ShellCommand;-><init>()V */
return;
} // .end method
 com.android.server.am.ProcessProphetImpl$ProcessProphetShellCmd ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;-><init>(Lcom/android/server/am/ProcessProphetImpl;)V */
return;
} // .end method
/* # virtual methods */
public Integer onCommand ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "cmd" # Ljava/lang/String; */
/* .line 1183 */
/* if-nez p1, :cond_0 */
/* .line 1184 */
v0 = (( com.android.server.am.ProcessProphetImpl$ProcessProphetShellCmd ) p0 ).handleDefaultCommands ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->handleDefaultCommands(Ljava/lang/String;)I
/* .line 1186 */
} // :cond_0
(( com.android.server.am.ProcessProphetImpl$ProcessProphetShellCmd ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->getOutPrintWriter()Ljava/io/PrintWriter;
/* .line 1188 */
/* .local v0, "pw":Ljava/io/PrintWriter; */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
v2 = (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
int v3 = 3; // const/4 v3, 0x3
int v4 = 2; // const/4 v4, 0x2
int v5 = 4; // const/4 v5, 0x4
/* sparse-switch v2, :sswitch_data_0 */
} // :cond_1
/* goto/16 :goto_0 */
/* :sswitch_0 */
/* const-string/jumbo v2, "start" */
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
int v2 = 1; // const/4 v2, 0x1
/* goto/16 :goto_1 */
/* :sswitch_1 */
final String v2 = "btoff"; // const-string v2, "btoff"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
	 int v2 = 5; // const/4 v2, 0x5
	 /* goto/16 :goto_1 */
	 /* :sswitch_2 */
	 final String v2 = "idle"; // const-string v2, "idle"
	 v2 = 	 (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v2 != null) { // if-eqz v2, :cond_1
		 /* move v2, v3 */
		 /* :sswitch_3 */
		 final String v2 = "dump"; // const-string v2, "dump"
		 v2 = 		 (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v2 != null) { // if-eqz v2, :cond_1
			 /* move v2, v1 */
			 /* :sswitch_4 */
			 final String v2 = "bton"; // const-string v2, "bton"
			 v2 = 			 (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 if ( v2 != null) { // if-eqz v2, :cond_1
				 /* move v2, v5 */
				 /* :sswitch_5 */
				 /* const-string/jumbo v2, "testLU" */
				 v2 = 				 (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
				 if ( v2 != null) { // if-eqz v2, :cond_1
					 int v2 = 7; // const/4 v2, 0x7
					 /* :sswitch_6 */
					 /* const-string/jumbo v2, "testCP" */
					 v2 = 					 (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
					 if ( v2 != null) { // if-eqz v2, :cond_1
						 /* const/16 v2, 0x9 */
						 /* :sswitch_7 */
						 /* const-string/jumbo v2, "testBT" */
						 v2 = 						 (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
						 if ( v2 != null) { // if-eqz v2, :cond_1
							 /* const/16 v2, 0x8 */
							 /* :sswitch_8 */
							 /* const-string/jumbo v2, "testMTBF" */
							 v2 = 							 (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
							 if ( v2 != null) { // if-eqz v2, :cond_1
								 /* const/16 v2, 0xa */
								 /* :sswitch_9 */
								 final String v2 = "pressure"; // const-string v2, "pressure"
								 v2 = 								 (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
								 if ( v2 != null) { // if-eqz v2, :cond_1
									 int v2 = 6; // const/4 v2, 0x6
									 /* :sswitch_a */
									 final String v2 = "force-start"; // const-string v2, "force-start"
									 v2 = 									 (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
									 if ( v2 != null) { // if-eqz v2, :cond_1
										 /* move v2, v4 */
									 } // :goto_0
									 int v2 = -1; // const/4 v2, -0x1
								 } // :goto_1
								 /* packed-switch v2, :pswitch_data_0 */
								 /* .line 1242 */
								 v1 = 								 (( com.android.server.am.ProcessProphetImpl$ProcessProphetShellCmd ) p0 ).handleDefaultCommands ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->handleDefaultCommands(Ljava/lang/String;)I
								 /* goto/16 :goto_3 */
								 /* .line 1238 */
								 /* :pswitch_0 */
								 v2 = this.this$0;
								 (( com.android.server.am.ProcessProphetImpl ) v2 ).testMTBF ( ); // invoke-virtual {v2}, Lcom/android/server/am/ProcessProphetImpl;->testMTBF()V
								 /* .line 1239 */
								 /* goto/16 :goto_2 */
								 /* .line 1232 */
								 /* :pswitch_1 */
								 v2 = this.this$0;
								 v2 = 								 (( com.android.server.am.ProcessProphetImpl ) v2 ).isEnable ( ); // invoke-virtual {v2}, Lcom/android/server/am/ProcessProphetImpl;->isEnable()Z
								 if ( v2 != null) { // if-eqz v2, :cond_2
									 /* .line 1233 */
									 v2 = this.this$0;
									 com.android.server.am.ProcessProphetImpl .-$$Nest$fgetmProcessProphetModel ( v2 );
									 (( com.android.server.am.ProcessProphetImpl$ProcessProphetShellCmd ) p0 ).peekNextArg ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->peekNextArg()Ljava/lang/String;
									 (( com.android.server.am.ProcessProphetModel ) v2 ).testCP ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/am/ProcessProphetModel;->testCP(Ljava/lang/String;)V
									 /* goto/16 :goto_2 */
									 /* .line 1226 */
									 /* :pswitch_2 */
									 v2 = this.this$0;
									 v2 = 									 (( com.android.server.am.ProcessProphetImpl ) v2 ).isEnable ( ); // invoke-virtual {v2}, Lcom/android/server/am/ProcessProphetImpl;->isEnable()Z
									 if ( v2 != null) { // if-eqz v2, :cond_2
										 /* .line 1227 */
										 v2 = this.this$0;
										 (( com.android.server.am.ProcessProphetImpl$ProcessProphetShellCmd ) p0 ).peekNextArg ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->peekNextArg()Ljava/lang/String;
										 (( com.android.server.am.ProcessProphetImpl ) v2 ).testBT ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/am/ProcessProphetImpl;->testBT(Ljava/lang/String;)V
										 /* .line 1220 */
										 /* :pswitch_3 */
										 v2 = this.this$0;
										 v2 = 										 (( com.android.server.am.ProcessProphetImpl ) v2 ).isEnable ( ); // invoke-virtual {v2}, Lcom/android/server/am/ProcessProphetImpl;->isEnable()Z
										 if ( v2 != null) { // if-eqz v2, :cond_2
											 /* .line 1221 */
											 v2 = this.this$0;
											 (( com.android.server.am.ProcessProphetImpl$ProcessProphetShellCmd ) p0 ).peekNextArg ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->peekNextArg()Ljava/lang/String;
											 (( com.android.server.am.ProcessProphetImpl ) v2 ).testLU ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/am/ProcessProphetImpl;->testLU(Ljava/lang/String;)V
											 /* .line 1216 */
											 /* :pswitch_4 */
											 v2 = this.this$0;
											 (( com.android.server.am.ProcessProphetImpl ) v2 ).reportMemPressure ( v5 ); // invoke-virtual {v2, v5}, Lcom/android/server/am/ProcessProphetImpl;->reportMemPressure(I)V
											 /* .line 1217 */
											 /* .line 1211 */
											 /* :pswitch_5 */
											 v2 = this.this$0;
											 v2 = this.mHandler;
											 (( com.android.server.am.ProcessProphetImpl$MyHandler ) v2 ).obtainMessage ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;
											 /* .line 1212 */
											 /* .local v2, "msg":Landroid/os/Message; */
											 v3 = this.this$0;
											 v3 = this.mHandler;
											 (( com.android.server.am.ProcessProphetImpl$MyHandler ) v3 ).sendMessage ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z
											 /* .line 1213 */
											 /* .line 1206 */
										 } // .end local v2 # "msg":Landroid/os/Message;
										 /* :pswitch_6 */
										 v2 = this.this$0;
										 v2 = this.mHandler;
										 final String v3 = "cmd"; // const-string v3, "cmd"
										 (( com.android.server.am.ProcessProphetImpl$MyHandler ) v2 ).obtainMessage ( v4, v3 ); // invoke-virtual {v2, v4, v3}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
										 /* .line 1207 */
										 /* .restart local v2 # "msg":Landroid/os/Message; */
										 v3 = this.this$0;
										 v3 = this.mHandler;
										 (( com.android.server.am.ProcessProphetImpl$MyHandler ) v3 ).sendMessage ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z
										 /* .line 1208 */
										 /* .line 1202 */
									 } // .end local v2 # "msg":Landroid/os/Message;
									 /* :pswitch_7 */
									 v2 = this.this$0;
									 (( com.android.server.am.ProcessProphetImpl ) v2 ).notifyIdleUpdate ( ); // invoke-virtual {v2}, Lcom/android/server/am/ProcessProphetImpl;->notifyIdleUpdate()V
									 /* .line 1203 */
									 /* .line 1198 */
									 /* :pswitch_8 */
									 v2 = this.this$0;
									 (( com.android.server.am.ProcessProphetImpl$ProcessProphetShellCmd ) p0 ).peekNextArg ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->peekNextArg()Ljava/lang/String;
									 com.android.server.am.ProcessProphetImpl .-$$Nest$mstartEmptyProc ( v2,v3 );
									 /* .line 1199 */
									 /* .line 1194 */
									 /* :pswitch_9 */
									 v2 = this.this$0;
									 (( com.android.server.am.ProcessProphetImpl$ProcessProphetShellCmd ) p0 ).peekNextArg ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->peekNextArg()Ljava/lang/String;
									 com.android.server.am.ProcessProphetImpl .-$$Nest$mtryToStartEmptyProc ( v2,v3 );
									 /* .line 1195 */
									 /* .line 1190 */
									 /* :pswitch_a */
									 v2 = this.this$0;
									 v2 = this.mHandler;
									 v3 = this.this$0;
									 v3 = this.mHandler;
									 /* const/16 v4, 0xd */
									 (( com.android.server.am.ProcessProphetImpl$MyHandler ) v3 ).obtainMessage ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;
									 (( com.android.server.am.ProcessProphetImpl$MyHandler ) v2 ).sendMessage ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z
									 /* :try_end_0 */
									 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
									 /* .line 1191 */
									 /* nop */
									 /* .line 1247 */
								 } // :cond_2
							 } // :goto_2
							 /* .line 1242 */
						 } // :goto_3
						 /* .line 1244 */
						 /* :catch_0 */
						 /* move-exception v2 */
						 /* .line 1245 */
						 /* .local v2, "e":Ljava/lang/Exception; */
						 /* new-instance v3, Ljava/lang/StringBuilder; */
						 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
						 final String v4 = "Error occurred.Check logcat for details."; // const-string v4, "Error occurred.Check logcat for details."
						 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
						 (( java.lang.Exception ) v2 ).getMessage ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
						 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
						 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
						 (( java.io.PrintWriter ) v0 ).println ( v3 ); // invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
						 /* .line 1246 */
						 /* new-instance v3, Ljava/lang/StringBuilder; */
						 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
						 final String v4 = "Error running shell command! "; // const-string v4, "Error running shell command! "
						 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
						 (( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
						 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
						 final String v4 = "ShellCommand"; // const-string v4, "ShellCommand"
						 android.util.Slog .e ( v4,v3 );
						 /* .line 1248 */
					 } // .end local v2 # "e":Ljava/lang/Exception;
				 } // :goto_4
				 /* :sswitch_data_0 */
				 /* .sparse-switch */
				 /* -0x5c50b900 -> :sswitch_a */
				 /* -0x4c11e9bb -> :sswitch_9 */
				 /* -0x445e1043 -> :sswitch_8 */
				 /* -0x34488f9c -> :sswitch_7 */
				 /* -0x34488f81 -> :sswitch_6 */
				 /* -0x34488e65 -> :sswitch_5 */
				 /* 0x2e4db1 -> :sswitch_4 */
				 /* 0x2f39f4 -> :sswitch_3 */
				 /* 0x313fd4 -> :sswitch_2 */
				 /* 0x59b67dd -> :sswitch_1 */
				 /* 0x68ac462 -> :sswitch_0 */
			 } // .end sparse-switch
			 /* :pswitch_data_0 */
			 /* .packed-switch 0x0 */
			 /* :pswitch_a */
			 /* :pswitch_9 */
			 /* :pswitch_8 */
			 /* :pswitch_7 */
			 /* :pswitch_6 */
			 /* :pswitch_5 */
			 /* :pswitch_4 */
			 /* :pswitch_3 */
			 /* :pswitch_2 */
			 /* :pswitch_1 */
			 /* :pswitch_0 */
		 } // .end packed-switch
	 } // .end method
	 public void onHelp ( ) {
		 /* .locals 2 */
		 /* .line 1253 */
		 (( com.android.server.am.ProcessProphetImpl$ProcessProphetShellCmd ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->getOutPrintWriter()Ljava/io/PrintWriter;
		 /* .line 1254 */
		 /* .local v0, "pw":Ljava/io/PrintWriter; */
		 final String v1 = "Process Prophet commands:"; // const-string v1, "Process Prophet commands:"
		 (( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
		 /* .line 1255 */
		 return;
	 } // .end method
