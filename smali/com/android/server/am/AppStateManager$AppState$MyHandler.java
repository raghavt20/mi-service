class com.android.server.am.AppStateManager$AppState$MyHandler extends android.os.Handler {
	 /* .source "AppStateManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/AppStateManager$AppState; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "MyHandler" */
} // .end annotation
/* # instance fields */
final com.android.server.am.AppStateManager$AppState this$1; //synthetic
/* # direct methods */
 com.android.server.am.AppStateManager$AppState$MyHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 1893 */
this.this$1 = p1;
/* .line 1894 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 1895 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 2 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 1899 */
/* invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V */
/* .line 1900 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 1905 */
/* :pswitch_0 */
v0 = this.this$1;
v1 = this.obj;
/* check-cast v1, Ljava/lang/String; */
com.android.server.am.AppStateManager$AppState .-$$Nest$mupdateAppState ( v0,v1 );
/* .line 1902 */
/* :pswitch_1 */
v0 = this.this$1;
v1 = this.obj;
/* check-cast v1, Ljava/lang/String; */
com.android.server.am.AppStateManager$AppState .-$$Nest$mstepAppState ( v0,v1 );
/* .line 1903 */
/* nop */
/* .line 1908 */
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
