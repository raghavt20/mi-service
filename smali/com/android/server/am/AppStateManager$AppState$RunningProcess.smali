.class public Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
.super Lcom/miui/server/smartpower/IAppState$IRunningProcess;
.source "AppStateManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/AppStateManager$AppState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RunningProcess"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;,
        Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;,
        Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;
    }
.end annotation


# instance fields
.field private LOG_TAG:Ljava/lang/String;

.field private mAdj:I

.field private mAdjType:Ljava/lang/String;

.field private mAmsProcState:I

.field private mAudioActive:Z

.field private mAudioBehavier:I

.field private mBackgroundCount:J

.field private mBackgroundDuration:J

.field private mBleActive:Z

.field private mCamBehavier:I

.field private mCameraActive:Z

.field public final mCurCpuTime:Ljava/util/concurrent/atomic/AtomicLong;

.field private volatile mDependencyProtected:Z

.field private mDisplayActive:Z

.field private mDisplayBehavier:I

.field private mForegroundCount:J

.field private mForegroundDuration:J

.field private mGpsActive:Z

.field private mGpsBehavier:I

.field private mHasActivity:Z

.field private mHasFgService:Z

.field private mHibernatonCount:J

.field private mHibernatonDuration:J

.field private mHistoryIndexNext:I

.field private mIdleCount:J

.field private mIdleDuration:J

.field private mIs64BitProcess:Z

.field private mIsKilled:Z

.field private mIsMainProc:Z

.field public final mLastCpuTime:Ljava/util/concurrent/atomic/AtomicLong;

.field private mLastInteractiveTimeMills:J

.field private mLastPss:J

.field private mLastRecordState:I

.field private mLastStateTimeMills:J

.field private mLastSwapPss:J

.field private mLastTaskId:I

.field private mNetActive:Z

.field private mNetBehavier:I

.field private mPackageName:Ljava/lang/String;

.field private mPid:I

.field private mProcPriorityScore:Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;

.field private mProcessName:Ljava/lang/String;

.field private mProcessRecord:Lcom/android/server/am/ProcessRecord;

.field private final mProviderDependProcs:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mResourcesCallback:Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;

.field private final mServiceDependProcs:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mState:I

.field private mStateChangeHistory:[Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;

.field private final mStateLock:Ljava/lang/Object;

.field private mUid:I

.field private mUserId:I

.field private final mVisibleActivities:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Ljava/lang/ref/WeakReference<",
            "Lcom/android/server/wm/ActivityRecord;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mVisibleWindows:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Ljava/lang/ref/WeakReference<",
            "Lcom/android/server/policy/WindowManagerPolicy$WindowState;",
            ">;>;"
        }
    .end annotation
.end field

.field final synthetic this$1:Lcom/android/server/am/AppStateManager$AppState;


# direct methods
.method static bridge synthetic -$$Nest$fgetLOG_TAG(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->LOG_TAG:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmAdj(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAdj:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmAmsProcState(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAmsProcState:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmAudioBehavier(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAudioBehavier:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmCamBehavier(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mCamBehavier:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmDisplayBehavier(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mDisplayBehavier:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmGpsBehavier(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mGpsBehavier:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIs64BitProcess(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIs64BitProcess:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastPss(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastPss:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmNetBehavier(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mNetBehavier:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmPackageName(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mPackageName:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPid(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mPid:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmProcessName(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mProcessName:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmProcessRecord(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Lcom/android/server/am/ProcessRecord;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mProcessRecord:Lcom/android/server/am/ProcessRecord;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmProviderDependProcs(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Landroid/util/ArrayMap;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mProviderDependProcs:Landroid/util/ArrayMap;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmResourcesCallback(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mResourcesCallback:Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmServiceDependProcs(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Landroid/util/ArrayMap;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mServiceDependProcs:Landroid/util/ArrayMap;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmState(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmStateLock(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mStateLock:Ljava/lang/Object;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmUid(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mUid:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmUserId(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mUserId:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmAudioActive(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAudioActive:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmAudioBehavier(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAudioBehavier:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmBleActive(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mBleActive:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmCamBehavier(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mCamBehavier:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmCameraActive(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mCameraActive:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmDisplayActive(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mDisplayActive:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmDisplayBehavier(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mDisplayBehavier:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmGpsActive(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mGpsActive:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmGpsBehavier(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mGpsBehavier:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastTaskId(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastTaskId:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmNetActive(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mNetActive:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmNetBehavier(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mNetBehavier:I

    return-void
.end method

.method static bridge synthetic -$$Nest$mbecomeActiveIfNeeded(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->becomeActiveIfNeeded(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mbecomeInactiveIfNeeded(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->becomeInactiveIfNeeded(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mcomponentEnd(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->componentEnd(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mcomponentStart(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->componentStart(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetHistoryInfos(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;J)Ljava/util/ArrayList;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getHistoryInfos(J)Ljava/util/ArrayList;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mmoveToStateLocked(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->moveToStateLocked(ILjava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monActivityForegroundLocked(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->onActivityForegroundLocked(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monAddToWhiteList(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->onAddToWhiteList()V

    return-void
.end method

.method static bridge synthetic -$$Nest$monBackupChanged(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->onBackupChanged(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monBackupServiceAppChanged(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->onBackupServiceAppChanged(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monInputMethodShow(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->onInputMethodShow()V

    return-void
.end method

.method static bridge synthetic -$$Nest$monMediaKey(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->onMediaKey(J)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monRemoveFromWhiteList(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->onRemoveFromWhiteList()V

    return-void
.end method

.method static bridge synthetic -$$Nest$monReportPowerFrozenSignal(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->onReportPowerFrozenSignal(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monSendPendingIntent(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->onSendPendingIntent(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mprocessKilledLocked(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->processKilledLocked()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msendBecomeInactiveMsg(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->sendBecomeInactiveMsg(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetActivityVisible(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Lcom/android/server/wm/ActivityRecord;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->setActivityVisible(Lcom/android/server/wm/ActivityRecord;Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetForeground(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->setForeground(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetWindowVisible(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Lcom/android/server/policy/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;Z)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->setWindowVisible(Lcom/android/server/policy/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateProcessInfo(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Lcom/android/server/am/ProcessRecord;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->updateProcessInfo(Lcom/android/server/am/ProcessRecord;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateProviderDepends(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Lcom/android/server/am/ProcessRecord;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->updateProviderDepends(Lcom/android/server/am/ProcessRecord;Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateServiceDepends(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Lcom/android/server/am/ProcessRecord;ZZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->updateServiceDepends(Lcom/android/server/am/ProcessRecord;ZZ)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateVisibility(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->updateVisibility()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mwhiteListVideoVisible(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->whiteListVideoVisible()Z

    move-result p0

    return p0
.end method

.method private constructor <init>(Lcom/android/server/am/AppStateManager$AppState;Lcom/android/server/am/ProcessRecord;)V
    .locals 5
    .param p1, "this$1"    # Lcom/android/server/am/AppStateManager$AppState;
    .param p2, "record"    # Lcom/android/server/am/ProcessRecord;

    .line 1986
    iput-object p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-direct {p0, p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;-><init>(Lcom/miui/server/smartpower/IAppState;)V

    .line 1912
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mUid:I

    .line 1913
    iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mUserId:I

    .line 1914
    iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mPid:I

    .line 1915
    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastPss:J

    .line 1916
    iput-wide v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastSwapPss:J

    .line 1917
    const-string v1, "SmartPower"

    iput-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->LOG_TAG:Ljava/lang/String;

    .line 1922
    const/16 v1, -0x2710

    iput v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAdj:I

    .line 1923
    const/16 v1, 0x14

    iput v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAmsProcState:I

    .line 1924
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHasActivity:Z

    .line 1925
    iput-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHasFgService:Z

    .line 1926
    iput-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAudioActive:Z

    .line 1927
    iput-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mGpsActive:Z

    .line 1928
    iput-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mNetActive:Z

    .line 1929
    iput-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mBleActive:Z

    .line 1930
    iput-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mCameraActive:Z

    .line 1931
    iput-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mDisplayActive:Z

    .line 1932
    iput-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIsMainProc:Z

    .line 1933
    iput v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAudioBehavier:I

    .line 1934
    iput v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mGpsBehavier:I

    .line 1935
    iput v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mNetBehavier:I

    .line 1936
    iput v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mCamBehavier:I

    .line 1937
    iput-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mDependencyProtected:Z

    .line 1938
    iput v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mDisplayBehavier:I

    .line 1940
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIs64BitProcess:Z

    .line 1942
    iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I

    .line 1943
    new-instance v3, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;

    invoke-direct {v3, p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;-><init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V

    iput-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mProcPriorityScore:Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;

    .line 1945
    iput-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIsKilled:Z

    .line 1946
    new-instance v3, Ljava/lang/Object;

    invoke-direct {v3}, Ljava/lang/Object;-><init>()V

    iput-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mStateLock:Ljava/lang/Object;

    .line 1950
    new-instance v3, Landroid/util/ArrayMap;

    invoke-direct {v3}, Landroid/util/ArrayMap;-><init>()V

    iput-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mServiceDependProcs:Landroid/util/ArrayMap;

    .line 1951
    new-instance v3, Landroid/util/ArrayMap;

    invoke-direct {v3}, Landroid/util/ArrayMap;-><init>()V

    iput-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mProviderDependProcs:Landroid/util/ArrayMap;

    .line 1953
    new-instance v3, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;-><init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback-IA;)V

    iput-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mResourcesCallback:Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;

    .line 1955
    new-instance v3, Landroid/util/ArraySet;

    invoke-direct {v3}, Landroid/util/ArraySet;-><init>()V

    iput-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleWindows:Landroid/util/ArraySet;

    .line 1957
    new-instance v3, Landroid/util/ArraySet;

    invoke-direct {v3}, Landroid/util/ArraySet;-><init>()V

    iput-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleActivities:Landroid/util/ArraySet;

    .line 1960
    invoke-static {}, Lcom/android/server/am/AppStateManager;->-$$Nest$sfgetHISTORY_SIZE()I

    move-result v3

    new-array v3, v3, [Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;

    iput-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mStateChangeHistory:[Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;

    .line 1961
    iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastRecordState:I

    .line 1962
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastStateTimeMills:J

    .line 1963
    iput v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHistoryIndexNext:I

    .line 1965
    const-wide/16 v3, 0x0

    iput-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mBackgroundDuration:J

    .line 1966
    iput-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mForegroundDuration:J

    .line 1967
    iput-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIdleDuration:J

    .line 1968
    iput-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHibernatonDuration:J

    .line 1970
    iput-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mBackgroundCount:J

    .line 1971
    iput-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mForegroundCount:J

    .line 1972
    iput-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIdleCount:J

    .line 1973
    iput-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHibernatonCount:J

    .line 1974
    iput-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastInteractiveTimeMills:J

    .line 1975
    iput v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastTaskId:I

    .line 1980
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v3, v4}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastCpuTime:Ljava/util/concurrent/atomic/AtomicLong;

    .line 1985
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v3, v4}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mCurCpuTime:Ljava/util/concurrent/atomic/AtomicLong;

    .line 1987
    invoke-direct {p0, p2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->updateProcessInfo(Lcom/android/server/am/ProcessRecord;)V

    .line 1988
    invoke-static {p1}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmForeground(Lcom/android/server/am/AppStateManager$AppState;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1989
    invoke-direct {p0, v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->setForeground(Z)V

    goto :goto_0

    .line 1991
    :cond_0
    invoke-direct {p0, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->setForeground(Z)V

    .line 1993
    :goto_0
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I

    iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastRecordState:I

    .line 1994
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/am/AppStateManager$AppState;Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/AppStateManager$AppState$RunningProcess-IA;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;-><init>(Lcom/android/server/am/AppStateManager$AppState;Lcom/android/server/am/ProcessRecord;)V

    return-void
.end method

.method private addToHistory(Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;)V
    .locals 5
    .param p1, "record"    # Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;

    .line 2584
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mStateChangeHistory:[Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;

    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHistoryIndexNext:I

    aput-object p1, v0, v1

    .line 2585
    const/4 v0, 0x1

    invoke-static {}, Lcom/android/server/am/AppStateManager;->-$$Nest$sfgetHISTORY_SIZE()I

    move-result v2

    invoke-static {v1, v0, v2}, Lcom/android/server/am/AppStateManager;->-$$Nest$smringAdvance(III)I

    move-result v0

    iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHistoryIndexNext:I

    .line 2586
    sget-boolean v0, Lcom/android/server/am/AppStateManager;->DEBUG:Z

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->-$$Nest$fgetmNewState(Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 2587
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->LOG_TAG:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2590
    :cond_0
    invoke-static {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->-$$Nest$fgetmOldState(Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;)I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    .line 2591
    iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIdleDuration:J

    invoke-static {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->-$$Nest$fgetmInterval(Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;)J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIdleDuration:J

    goto :goto_0

    .line 2592
    :cond_1
    invoke-static {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->-$$Nest$fgetmOldState(Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;)I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_2

    .line 2593
    iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHibernatonDuration:J

    invoke-static {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->-$$Nest$fgetmInterval(Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;)J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHibernatonDuration:J

    goto :goto_0

    .line 2594
    :cond_2
    invoke-static {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->-$$Nest$fgetmOldState(Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;)I

    move-result v0

    const/4 v1, 0x3

    if-lez v0, :cond_3

    invoke-static {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->-$$Nest$fgetmOldState(Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;)I

    move-result v0

    if-ge v0, v1, :cond_3

    .line 2596
    iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mForegroundDuration:J

    invoke-static {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->-$$Nest$fgetmInterval(Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;)J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mForegroundDuration:J

    goto :goto_0

    .line 2597
    :cond_3
    invoke-static {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->-$$Nest$fgetmOldState(Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;)I

    move-result v0

    if-lt v0, v1, :cond_4

    .line 2598
    iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mBackgroundDuration:J

    invoke-static {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->-$$Nest$fgetmInterval(Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;)J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mBackgroundDuration:J

    .line 2600
    :cond_4
    :goto_0
    invoke-static {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->-$$Nest$fgetmNewState(Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;)I

    move-result v0

    const-wide/16 v1, 0x1

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_1

    .line 2605
    :pswitch_1
    iget-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHibernatonCount:J

    add-long/2addr v3, v1

    iput-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHibernatonCount:J

    .line 2606
    goto :goto_1

    .line 2602
    :pswitch_2
    iget-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIdleCount:J

    add-long/2addr v3, v1

    iput-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIdleCount:J

    .line 2603
    goto :goto_1

    .line 2608
    :pswitch_3
    iget-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mBackgroundCount:J

    add-long/2addr v3, v1

    iput-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mBackgroundCount:J

    .line 2609
    goto :goto_1

    .line 2611
    :pswitch_4
    iget-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mForegroundCount:J

    add-long/2addr v3, v1

    iput-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mForegroundCount:J

    .line 2614
    :goto_1
    return-void

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private becomeActiveIfNeeded(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .line 2470
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 2471
    :try_start_0
    sget-boolean v1, Lcom/android/server/am/AppStateManager;->sEnable:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I

    const/4 v2, 0x3

    if-le v1, v2, :cond_0

    .line 2472
    invoke-direct {p0, v2, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->moveToStateLocked(ILjava/lang/String;)V

    .line 2473
    iget-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mNetActive:Z

    if-nez v1, :cond_0

    .line 2474
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmHandler(Lcom/android/server/am/AppStateManager$AppState;)Lcom/android/server/am/AppStateManager$AppState$MyHandler;

    move-result-object v1

    new-instance v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$5;

    invoke-direct {v2, p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$5;-><init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V

    invoke-virtual {v1, v2}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->post(Ljava/lang/Runnable;)Z

    .line 2484
    :cond_0
    monitor-exit v0

    .line 2485
    return-void

    .line 2484
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private becomeInactiveIfNeeded(Ljava/lang/String;)V
    .locals 4
    .param p1, "reason"    # Ljava/lang/String;

    .line 2488
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 2489
    :try_start_0
    sget-boolean v1, Lcom/android/server/am/AppStateManager;->sEnable:Z

    const/4 v2, 0x3

    if-eqz v1, :cond_b

    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I

    if-ne v1, v2, :cond_b

    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAdj:I

    if-ltz v1, :cond_b

    .line 2491
    iget-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAudioActive:Z

    if-eqz v1, :cond_1

    .line 2492
    sget-boolean v1, Lcom/android/server/am/AppStateManager;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 2493
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "skip inactive for audio active"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2495
    :cond_0
    monitor-exit v0

    return-void

    .line 2497
    :cond_1
    iget-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mBleActive:Z

    if-eqz v1, :cond_3

    .line 2498
    sget-boolean v1, Lcom/android/server/am/AppStateManager;->DEBUG:Z

    if-eqz v1, :cond_2

    .line 2499
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "skip inactive for bluetooth active"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2501
    :cond_2
    monitor-exit v0

    return-void

    .line 2503
    :cond_3
    iget-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mGpsActive:Z

    if-eqz v1, :cond_5

    .line 2504
    sget-boolean v1, Lcom/android/server/am/AppStateManager;->DEBUG:Z

    if-eqz v1, :cond_4

    .line 2505
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "skip inactive for GPS active"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2507
    :cond_4
    monitor-exit v0

    return-void

    .line 2509
    :cond_5
    iget-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mNetActive:Z

    if-eqz v1, :cond_7

    .line 2510
    sget-boolean v1, Lcom/android/server/am/AppStateManager;->DEBUG:Z

    if-eqz v1, :cond_6

    .line 2511
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "skip inactive for net active"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2513
    :cond_6
    monitor-exit v0

    return-void

    .line 2515
    :cond_7
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmIsBackupServiceApp(Lcom/android/server/am/AppStateManager$AppState;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2516
    sget-boolean v1, Lcom/android/server/am/AppStateManager;->DEBUG:Z

    if-eqz v1, :cond_8

    .line 2517
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "skip inactive for backing up"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2519
    :cond_8
    monitor-exit v0

    return-void

    .line 2521
    :cond_9
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-object v1, v1, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmSmartPowerPolicyManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mPackageName:Ljava/lang/String;

    iget v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mUid:I

    invoke-virtual {v1, v2, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->needCheckNet(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 2522
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmHandler(Lcom/android/server/am/AppStateManager$AppState;)Lcom/android/server/am/AppStateManager$AppState$MyHandler;

    move-result-object v1

    new-instance v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$6;

    invoke-direct {v2, p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$6;-><init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V

    invoke-virtual {v1, v2}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->post(Ljava/lang/Runnable;)Z

    .line 2531
    :cond_a
    const/4 v1, 0x4

    invoke-direct {p0, v1, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->moveToStateLocked(ILjava/lang/String;)V

    goto :goto_0

    .line 2532
    :cond_b
    sget-boolean v1, Lcom/android/server/am/AppStateManager;->sEnable:Z

    if-eqz v1, :cond_c

    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I

    if-ne v1, v2, :cond_c

    .line 2533
    sget-boolean v1, Lcom/android/server/am/AppStateManager;->DEBUG:Z

    if-eqz v1, :cond_c

    .line 2534
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "skip inactive state("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I

    invoke-static {v3}, Lcom/android/server/am/AppStateManager;->processStateToString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") adj("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAdj:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") R("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2538
    :cond_c
    :goto_0
    monitor-exit v0

    .line 2539
    return-void

    .line 2538
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private componentEnd(Ljava/lang/String;)V
    .locals 0
    .param p1, "content"    # Ljava/lang/String;

    .line 2377
    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->sendBecomeInactiveMsg(Ljava/lang/String;)V

    .line 2378
    return-void
.end method

.method private componentStart(Ljava/lang/String;)V
    .locals 2
    .param p1, "content"    # Ljava/lang/String;

    .line 2371
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    .line 2372
    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->becomeActiveIfNeeded(Ljava/lang/String;)V

    .line 2374
    :cond_0
    return-void
.end method

.method private getHistoryInfos(J)Ljava/util/ArrayList;
    .locals 6
    .param p1, "sinceUptime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/ArrayList<",
            "Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;",
            ">;"
        }
    .end annotation

    .line 2617
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2618
    .local v0, "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;>;"
    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHistoryIndexNext:I

    invoke-static {}, Lcom/android/server/am/AppStateManager;->-$$Nest$sfgetHISTORY_SIZE()I

    move-result v2

    const/4 v3, -0x1

    invoke-static {v1, v3, v2}, Lcom/android/server/am/AppStateManager;->-$$Nest$smringAdvance(III)I

    move-result v1

    .line 2620
    .local v1, "index":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-static {}, Lcom/android/server/am/AppStateManager;->-$$Nest$sfgetHISTORY_SIZE()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 2621
    iget-object v4, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mStateChangeHistory:[Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;

    aget-object v4, v4, v1

    if-eqz v4, :cond_1

    invoke-static {v4}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->-$$Nest$fgetmTimeStamp(Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;)J

    move-result-wide v4

    cmp-long v4, v4, p1

    if-gez v4, :cond_0

    .line 2623
    goto :goto_1

    .line 2625
    :cond_0
    iget-object v4, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mStateChangeHistory:[Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;

    aget-object v4, v4, v1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2626
    invoke-static {}, Lcom/android/server/am/AppStateManager;->-$$Nest$sfgetHISTORY_SIZE()I

    move-result v4

    invoke-static {v1, v3, v4}, Lcom/android/server/am/AppStateManager;->-$$Nest$smringAdvance(III)I

    move-result v1

    .line 2620
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2628
    .end local v2    # "i":I
    :cond_1
    :goto_1
    return-object v0
.end method

.method private hasVisibleActivity()Z
    .locals 2

    .line 2296
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleActivities:Landroid/util/ArraySet;

    monitor-enter v0

    .line 2297
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleActivities:Landroid/util/ArraySet;

    invoke-virtual {v1}, Landroid/util/ArraySet;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    monitor-exit v0

    return v1

    .line 2298
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private hasVisibleWindow()Z
    .locals 2

    .line 2290
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleWindows:Landroid/util/ArraySet;

    monitor-enter v0

    .line 2291
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleWindows:Landroid/util/ArraySet;

    invoke-virtual {v1}, Landroid/util/ArraySet;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    monitor-exit v0

    return v1

    .line 2292
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private is32BitProcess(Lcom/android/server/am/ProcessRecord;)Z
    .locals 5
    .param p1, "proc"    # Lcom/android/server/am/ProcessRecord;

    .line 2070
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->primaryCpuAbi:Ljava/lang/String;

    .line 2071
    .local v0, "primaryCpuAbi":Ljava/lang/String;
    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->secondaryCpuAbi:Ljava/lang/String;

    .line 2072
    .local v1, "secondaryCpuAbi":Ljava/lang/String;
    const/4 v2, 0x0

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    .line 2073
    return v2

    .line 2074
    :cond_0
    const-string v3, "arm64"

    if-eqz v0, :cond_1

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2075
    return v2

    .line 2076
    :cond_1
    if-eqz v1, :cond_2

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2077
    return v2

    .line 2079
    :cond_2
    const/4 v2, 0x1

    return v2
.end method

.method private moveToStateLocked(ILjava/lang/String;)V
    .locals 7
    .param p1, "targetState"    # I
    .param p2, "reason"    # Ljava/lang/String;

    .line 2542
    sget-boolean v0, Lcom/android/server/am/AppStateManager;->sEnable:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I

    if-ne p1, v0, :cond_0

    goto :goto_1

    .line 2545
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmSmartPowerPolicyManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isProcessHibernationWhiteList(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Z

    move-result v0

    .line 2546
    .local v0, "isWhiteList":Z
    const/4 v1, 0x7

    if-ne p1, v1, :cond_1

    if-eqz v0, :cond_1

    .line 2547
    return-void

    .line 2549
    :cond_1
    const-string v1, "moveToStateLocked"

    const-wide/32 v2, 0x20000

    invoke-static {v2, v3, v1}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 2550
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-object v1, v1, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmProcessStateCallbacks(Lcom/android/server/am/AppStateManager;)Ljava/util/ArrayList;

    move-result-object v1

    monitor-enter v1

    .line 2551
    :try_start_0
    iget-object v4, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-object v4, v4, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v4}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmProcessStateCallbacks(Lcom/android/server/am/AppStateManager;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/am/AppStateManager$IProcessStateCallback;

    .line 2552
    .local v5, "callback":Lcom/android/server/am/AppStateManager$IProcessStateCallback;
    iget v6, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I

    invoke-interface {v5, p0, v6, p1, p2}, Lcom/android/server/am/AppStateManager$IProcessStateCallback;->onProcessStateChanged(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;IILjava/lang/String;)V

    .line 2553
    .end local v5    # "callback":Lcom/android/server/am/AppStateManager$IProcessStateCallback;
    goto :goto_0

    .line 2554
    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2555
    invoke-direct {p0, p1, p2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->stateChangeToRecord(ILjava/lang/String;)V

    .line 2556
    iput p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I

    .line 2558
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmHandler(Lcom/android/server/am/AppStateManager$AppState;)Lcom/android/server/am/AppStateManager$AppState$MyHandler;

    move-result-object v1

    const/4 v4, 0x2

    invoke-virtual {v1, v4}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->removeMessages(I)V

    .line 2559
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmHandler(Lcom/android/server/am/AppStateManager$AppState;)Lcom/android/server/am/AppStateManager$AppState$MyHandler;

    move-result-object v1

    invoke-virtual {v1, v4, p2}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 2560
    .local v1, "msg":Landroid/os/Message;
    iget-object v4, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v4}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmHandler(Lcom/android/server/am/AppStateManager$AppState;)Lcom/android/server/am/AppStateManager$AppState$MyHandler;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->sendMessage(Landroid/os/Message;)Z

    .line 2561
    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->reportDependChangedIfNeeded()V

    .line 2563
    invoke-static {v2, v3}, Landroid/os/Trace;->traceEnd(J)V

    .line 2564
    return-void

    .line 2554
    .end local v1    # "msg":Landroid/os/Message;
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 2543
    .end local v0    # "isWhiteList":Z
    :cond_3
    :goto_1
    return-void
.end method

.method private onActivityForegroundLocked(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .line 2431
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "start activity "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2432
    .local v0, "reason":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->becomeActiveIfNeeded(Ljava/lang/String;)V

    .line 2433
    return-void
.end method

.method private onAddToWhiteList()V
    .locals 2

    .line 2381
    sget-boolean v0, Lcom/android/server/am/AppStateManager;->DEBUG:Z

    const-string v1, "add to whitelist"

    if-eqz v0, :cond_0

    .line 2382
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->LOG_TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2384
    :cond_0
    invoke-direct {p0, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->becomeActiveIfNeeded(Ljava/lang/String;)V

    .line 2385
    return-void
.end method

.method private onBackupChanged(Z)V
    .locals 1
    .param p1, "active"    # Z

    .line 2348
    if-eqz p1, :cond_0

    .line 2349
    const-string v0, "backup start"

    invoke-direct {p0, v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->becomeActiveIfNeeded(Ljava/lang/String;)V

    goto :goto_0

    .line 2351
    :cond_0
    const-string v0, "backup end"

    invoke-direct {p0, v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->sendBecomeInactiveMsg(Ljava/lang/String;)V

    .line 2353
    :goto_0
    return-void
.end method

.method private onBackupServiceAppChanged(Z)V
    .locals 1
    .param p1, "active"    # Z

    .line 2356
    if-eqz p1, :cond_0

    .line 2357
    const-string v0, "backup service connected"

    invoke-direct {p0, v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->becomeActiveIfNeeded(Ljava/lang/String;)V

    goto :goto_0

    .line 2359
    :cond_0
    const-string v0, "backup service disconnected"

    invoke-direct {p0, v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->sendBecomeInactiveMsg(Ljava/lang/String;)V

    .line 2361
    :goto_0
    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->reportDependChangedIfNeeded()V

    .line 2362
    return-void
.end method

.method private onInputMethodShow()V
    .locals 3

    .line 2339
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v1, "show input method"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2340
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 2341
    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isKilled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2342
    const-string/jumbo v1, "show input method"

    const/4 v2, 0x1

    invoke-direct {p0, v2, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->moveToStateLocked(ILjava/lang/String;)V

    .line 2344
    :cond_0
    monitor-exit v0

    .line 2345
    return-void

    .line 2344
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private onMediaKey(J)V
    .locals 2
    .param p1, "timeStamp"    # J

    .line 2333
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->LOG_TAG:Ljava/lang/String;

    const-string v1, "press media key"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2334
    iput-wide p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastInteractiveTimeMills:J

    .line 2335
    invoke-direct {p0, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->becomeActiveIfNeeded(Ljava/lang/String;)V

    .line 2336
    return-void
.end method

.method private onRemoveFromWhiteList()V
    .locals 2

    .line 2388
    sget-boolean v0, Lcom/android/server/am/AppStateManager;->DEBUG:Z

    const-string v1, "remove from whitelist"

    if-eqz v0, :cond_0

    .line 2389
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->LOG_TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2391
    :cond_0
    invoke-direct {p0, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->sendBecomeInactiveMsg(Ljava/lang/String;)V

    .line 2392
    return-void
.end method

.method private onReportPowerFrozenSignal(Ljava/lang/String;)V
    .locals 2
    .param p1, "reason"    # Ljava/lang/String;

    .line 2425
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    .line 2426
    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->sendBecomeActiveMsg(Ljava/lang/String;)V

    .line 2428
    :cond_0
    return-void
.end method

.method private onSendPendingIntent(Ljava/lang/String;)V
    .locals 2
    .param p1, "content"    # Ljava/lang/String;

    .line 2365
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    .line 2366
    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->becomeActiveIfNeeded(Ljava/lang/String;)V

    .line 2368
    :cond_0
    return-void
.end method

.method private processKilledLocked()V
    .locals 10

    .line 2084
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 2085
    :try_start_0
    const-string v1, "process died "

    const/4 v2, 0x0

    invoke-direct {p0, v2, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->moveToStateLocked(ILjava/lang/String;)V

    .line 2086
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2087
    iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mDependencyProtected:Z

    if-eqz v0, :cond_1

    .line 2088
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mServiceDependProcs:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 2089
    :try_start_1
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mServiceDependProcs:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;

    .line 2090
    .local v2, "processInfo":Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;
    iget-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-object v3, v3, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v3}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmSmartPowerPolicyManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    move-result-object v4

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mProcessName:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;->-$$Nest$fgetmProcessName(Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v2}, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;->-$$Nest$fgetmUid(Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;)I

    move-result v8

    invoke-static {v2}, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;->-$$Nest$fgetmPid(Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;)I

    move-result v9

    invoke-virtual/range {v4 .. v9}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->onDependChanged(ZLjava/lang/String;Ljava/lang/String;II)V

    .line 2094
    .end local v2    # "processInfo":Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;
    goto :goto_0

    .line 2095
    :cond_0
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 2097
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->reset()V

    .line 2098
    return-void

    .line 2086
    :catchall_1
    move-exception v1

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1
.end method

.method private reportDependChangedIfNeeded()V
    .locals 10

    .line 2241
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmIsBackupServiceApp(Lcom/android/server/am/AppStateManager$AppState;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2242
    invoke-virtual {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isForeground()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mUid:I

    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->hasVisibleActivity()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, 0x1

    .line 2243
    .local v0, "isConnected":Z
    :goto_1
    iget-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mDependencyProtected:Z

    if-eq v0, v1, :cond_5

    .line 2244
    iget-object v7, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mServiceDependProcs:Landroid/util/ArrayMap;

    monitor-enter v7

    .line 2245
    :try_start_0
    iget-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mDependencyProtected:Z

    if-eq v0, v1, :cond_4

    .line 2246
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mServiceDependProcs:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;

    move-object v9, v1

    .line 2247
    .local v9, "processInfo":Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-object v1, v1, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmSmartPowerPolicyManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mProcessName:Ljava/lang/String;

    invoke-static {v9}, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;->-$$Nest$fgetmProcessName(Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v9}, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;->-$$Nest$fgetmUid(Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;)I

    move-result v5

    invoke-static {v9}, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;->-$$Nest$fgetmPid(Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;)I

    move-result v6

    move v2, v0

    invoke-virtual/range {v1 .. v6}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->onDependChanged(ZLjava/lang/String;Ljava/lang/String;II)V

    .line 2251
    .end local v9    # "processInfo":Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;
    goto :goto_2

    .line 2252
    :cond_3
    iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mDependencyProtected:Z

    .line 2254
    :cond_4
    monitor-exit v7

    goto :goto_3

    :catchall_0
    move-exception v1

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 2256
    :cond_5
    :goto_3
    return-void
.end method

.method private reset()V
    .locals 4

    .line 2101
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleWindows:Landroid/util/ArraySet;

    monitor-enter v0

    .line 2102
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleWindows:Landroid/util/ArraySet;

    invoke-virtual {v1}, Landroid/util/ArraySet;->clear()V

    .line 2103
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 2104
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleActivities:Landroid/util/ArraySet;

    monitor-enter v1

    .line 2105
    :try_start_1
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleActivities:Landroid/util/ArraySet;

    invoke-virtual {v0}, Landroid/util/ArraySet;->clear()V

    .line 2106
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 2107
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAudioActive:Z

    .line 2108
    iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mGpsActive:Z

    .line 2109
    iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mNetActive:Z

    .line 2110
    iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mDisplayActive:Z

    .line 2111
    iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mCameraActive:Z

    .line 2112
    iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAudioBehavier:I

    .line 2113
    iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mGpsBehavier:I

    .line 2114
    iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mNetBehavier:I

    .line 2115
    iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mCamBehavier:I

    .line 2116
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastCpuTime:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 2117
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mCurCpuTime:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 2118
    iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mDisplayBehavier:I

    .line 2119
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v1, v0}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fputmTopAppCount(Lcom/android/server/am/AppStateManager$AppState;I)V

    .line 2120
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v1, v0}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fputmFreeFormCount(Lcom/android/server/am/AppStateManager$AppState;I)V

    .line 2121
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v1, v0}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fputmOverlayCount(Lcom/android/server/am/AppStateManager$AppState;I)V

    .line 2122
    iput-wide v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastPss:J

    .line 2123
    iput-wide v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastSwapPss:J

    .line 2124
    invoke-virtual {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isMainProc()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2125
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v1, v0}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fputmAppSwitchFgCount(Lcom/android/server/am/AppStateManager$AppState;I)V

    .line 2127
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$mupdateCurrentResourceBehavier(Lcom/android/server/am/AppStateManager$AppState;)V

    .line 2128
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mServiceDependProcs:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 2129
    :try_start_2
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mServiceDependProcs:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->clear()V

    .line 2130
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2131
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mProviderDependProcs:Landroid/util/ArrayMap;

    monitor-enter v1

    .line 2132
    :try_start_3
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mProviderDependProcs:Landroid/util/ArrayMap;

    invoke-virtual {v0}, Landroid/util/ArrayMap;->clear()V

    .line 2133
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2135
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmHandler(Lcom/android/server/am/AppStateManager$AppState;)Lcom/android/server/am/AppStateManager$AppState$MyHandler;

    move-result-object v0

    new-instance v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$2;

    invoke-direct {v1, p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$2;-><init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V

    invoke-virtual {v0, v1}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->post(Ljava/lang/Runnable;)Z

    .line 2160
    return-void

    .line 2133
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    .line 2130
    :catchall_1
    move-exception v1

    :try_start_5
    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v1

    .line 2106
    :catchall_2
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v0

    .line 2103
    :catchall_3
    move-exception v1

    :try_start_7
    monitor-exit v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v1
.end method

.method private sendBecomeActiveMsg(Ljava/lang/String;)V
    .locals 2
    .param p1, "resson"    # Ljava/lang/String;

    .line 2416
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmHandler(Lcom/android/server/am/AppStateManager$AppState;)Lcom/android/server/am/AppStateManager$AppState$MyHandler;

    move-result-object v0

    new-instance v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$4;

    invoke-direct {v1, p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$4;-><init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->post(Ljava/lang/Runnable;)Z

    .line 2422
    return-void
.end method

.method private sendBecomeInactiveMsg(Ljava/lang/String;)V
    .locals 2
    .param p1, "resson"    # Ljava/lang/String;

    .line 2407
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmHandler(Lcom/android/server/am/AppStateManager$AppState;)Lcom/android/server/am/AppStateManager$AppState$MyHandler;

    move-result-object v0

    new-instance v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$3;

    invoke-direct {v1, p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$3;-><init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->post(Ljava/lang/Runnable;)Z

    .line 2413
    return-void
.end method

.method private setActivityVisible(Lcom/android/server/wm/ActivityRecord;Z)V
    .locals 5
    .param p1, "record"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "visible"    # Z

    .line 2163
    sget-boolean v0, Lcom/android/server/am/AppStateManager;->DEBUG_PROC:Z

    if-eqz v0, :cond_0

    .line 2164
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onActivityVisibilityChanged "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2166
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleActivities:Landroid/util/ArraySet;

    monitor-enter v0

    .line 2167
    const/4 v1, 0x0

    .line 2168
    .local v1, "recordRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/android/server/wm/ActivityRecord;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleActivities:Landroid/util/ArraySet;

    invoke-virtual {v3}, Landroid/util/ArraySet;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 2169
    iget-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleActivities:Landroid/util/ArraySet;

    invoke-virtual {v3, v2}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/WeakReference;

    .line 2170
    .local v3, "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/android/server/wm/ActivityRecord;>;"
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    if-ne v4, p1, :cond_1

    .line 2171
    move-object v1, v3

    .line 2172
    goto :goto_1

    .line 2168
    .end local v3    # "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/android/server/wm/ActivityRecord;>;"
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2175
    .end local v2    # "i":I
    :cond_2
    :goto_1
    if-eqz p2, :cond_4

    if-nez v1, :cond_4

    .line 2176
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleActivities:Landroid/util/ArraySet;

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 2177
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->inFreeformWindowingMode()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2178
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v2}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmFreeFormCount(Lcom/android/server/am/AppStateManager$AppState;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v2, v3}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fputmFreeFormCount(Lcom/android/server/am/AppStateManager$AppState;I)V

    goto :goto_2

    .line 2180
    :cond_3
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v2}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmTopAppCount(Lcom/android/server/am/AppStateManager$AppState;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v2, v3}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fputmTopAppCount(Lcom/android/server/am/AppStateManager$AppState;I)V

    goto :goto_2

    .line 2182
    :cond_4
    if-nez p2, :cond_6

    if-eqz v1, :cond_6

    .line 2183
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleActivities:Landroid/util/ArraySet;

    invoke-virtual {v2, v1}, Landroid/util/ArraySet;->remove(Ljava/lang/Object;)Z

    .line 2184
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->inFreeformWindowingMode()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2185
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v2}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmFreeFormCount(Lcom/android/server/am/AppStateManager$AppState;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v2, v3}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fputmFreeFormCount(Lcom/android/server/am/AppStateManager$AppState;I)V

    goto :goto_2

    .line 2187
    :cond_5
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v2}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmTopAppCount(Lcom/android/server/am/AppStateManager$AppState;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v2, v3}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fputmTopAppCount(Lcom/android/server/am/AppStateManager$AppState;I)V

    .line 2190
    .end local v1    # "recordRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/android/server/wm/ActivityRecord;>;"
    :cond_6
    :goto_2
    monitor-exit v0

    .line 2191
    return-void

    .line 2190
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private setForeground(Z)V
    .locals 4
    .param p1, "foreground"    # Z

    .line 2318
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 2319
    :try_start_0
    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I

    if-nez v1, :cond_0

    monitor-exit v0

    return-void

    .line 2320
    :cond_0
    sget-boolean v1, Lcom/android/server/am/AppStateManager;->DEBUG_PROC:Z

    if-eqz v1, :cond_1

    .line 2321
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "set foreground "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2323
    :cond_1
    if-nez p1, :cond_2

    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I

    const/4 v2, 0x3

    if-ge v1, v2, :cond_2

    .line 2324
    const-string v1, "become background"

    invoke-direct {p0, v2, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->moveToStateLocked(ILjava/lang/String;)V

    .line 2325
    const-string v1, "become background"

    invoke-direct {p0, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->becomeInactiveIfNeeded(Ljava/lang/String;)V

    goto :goto_0

    .line 2326
    :cond_2
    if-eqz p1, :cond_3

    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I

    const/4 v2, 0x2

    if-le v1, v2, :cond_3

    .line 2327
    const-string v1, "become foreground"

    invoke-direct {p0, v2, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->moveToStateLocked(ILjava/lang/String;)V

    .line 2329
    :cond_3
    :goto_0
    monitor-exit v0

    .line 2330
    return-void

    .line 2329
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private setWindowVisible(Lcom/android/server/policy/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;Z)V
    .locals 5
    .param p1, "win"    # Lcom/android/server/policy/WindowManagerPolicy$WindowState;
    .param p2, "attrs"    # Landroid/view/WindowManager$LayoutParams;
    .param p3, "visible"    # Z

    .line 2195
    sget-boolean v0, Lcom/android/server/am/AppStateManager;->DEBUG_PROC:Z

    if-eqz v0, :cond_0

    .line 2196
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onWindowVisibilityChanged "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2198
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleWindows:Landroid/util/ArraySet;

    monitor-enter v0

    .line 2199
    const/4 v1, 0x0

    .line 2200
    .local v1, "winRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/android/server/policy/WindowManagerPolicy$WindowState;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleWindows:Landroid/util/ArraySet;

    invoke-virtual {v3}, Landroid/util/ArraySet;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 2201
    iget-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleWindows:Landroid/util/ArraySet;

    .line 2202
    invoke-virtual {v3, v2}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/WeakReference;

    .line 2203
    .local v3, "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/android/server/policy/WindowManagerPolicy$WindowState;>;"
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    if-ne v4, p1, :cond_1

    .line 2204
    move-object v1, v3

    .line 2205
    goto :goto_1

    .line 2200
    .end local v3    # "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/android/server/policy/WindowManagerPolicy$WindowState;>;"
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2208
    .end local v2    # "i":I
    :cond_2
    :goto_1
    const/16 v2, 0x7f6

    if-eqz p3, :cond_3

    if-nez v1, :cond_3

    .line 2209
    iget-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleWindows:Landroid/util/ArraySet;

    new-instance v4, Ljava/lang/ref/WeakReference;

    invoke-direct {v4, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v3, v4}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 2210
    iget v3, p2, Landroid/view/WindowManager$LayoutParams;->type:I

    if-ne v3, v2, :cond_4

    .line 2211
    invoke-virtual {p2}, Landroid/view/WindowManager$LayoutParams;->isFullscreen()Z

    move-result v2

    if-nez v2, :cond_4

    .line 2212
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v2}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmOverlayCount(Lcom/android/server/am/AppStateManager$AppState;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v2, v3}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fputmOverlayCount(Lcom/android/server/am/AppStateManager$AppState;I)V

    goto :goto_2

    .line 2214
    :cond_3
    if-nez p3, :cond_4

    if-eqz v1, :cond_4

    .line 2215
    iget-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleWindows:Landroid/util/ArraySet;

    invoke-virtual {v3, v1}, Landroid/util/ArraySet;->remove(Ljava/lang/Object;)Z

    .line 2216
    iget v3, p2, Landroid/view/WindowManager$LayoutParams;->type:I

    if-ne v3, v2, :cond_4

    .line 2217
    invoke-virtual {p2}, Landroid/view/WindowManager$LayoutParams;->isFullscreen()Z

    move-result v2

    if-nez v2, :cond_4

    .line 2218
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v2}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmOverlayCount(Lcom/android/server/am/AppStateManager$AppState;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v2, v3}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fputmOverlayCount(Lcom/android/server/am/AppStateManager$AppState;I)V

    .line 2221
    .end local v1    # "winRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/android/server/policy/WindowManagerPolicy$WindowState;>;"
    :cond_4
    :goto_2
    monitor-exit v0

    .line 2222
    return-void

    .line 2221
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private stateChangeToRecord(ILjava/lang/String;)V
    .locals 16
    .param p1, "targetState"    # I
    .param p2, "reason"    # Ljava/lang/String;

    .line 2567
    move-object/from16 v9, p0

    move/from16 v10, p1

    sget-boolean v0, Lcom/android/server/am/AppStateManager;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 2568
    iget-object v11, v9, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->LOG_TAG:Ljava/lang/String;

    new-instance v12, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;

    iget v2, v9, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I

    const-wide/16 v4, 0x0

    iget v7, v9, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAdj:I

    const/4 v8, 0x0

    move-object v0, v12

    move-object/from16 v1, p0

    move/from16 v3, p1

    move-object/from16 v6, p2

    invoke-direct/range {v0 .. v8}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;-><init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;IIJLjava/lang/String;ILcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord-IA;)V

    .line 2569
    invoke-virtual {v12}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2568
    invoke-static {v11, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2571
    :cond_0
    iget v0, v9, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastRecordState:I

    if-eq v10, v0, :cond_1

    invoke-static {}, Lcom/android/server/am/AppStateManager;->-$$Nest$sfgetmStatesNeedToRecord()Landroid/util/ArraySet;

    move-result-object v0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2572
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v11

    .line 2573
    .local v11, "now":J
    iget-wide v0, v9, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastStateTimeMills:J

    sub-long v13, v11, v0

    .line 2574
    .local v13, "interval":J
    new-instance v15, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;

    iget v2, v9, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastRecordState:I

    iget v7, v9, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAdj:I

    const/4 v8, 0x0

    move-object v0, v15

    move-object/from16 v1, p0

    move/from16 v3, p1

    move-wide v4, v13

    move-object/from16 v6, p2

    invoke-direct/range {v0 .. v8}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;-><init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;IIJLjava/lang/String;ILcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord-IA;)V

    .line 2576
    .local v0, "record":Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;
    invoke-direct {v9, v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->addToHistory(Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;)V

    .line 2577
    iput-wide v11, v9, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastStateTimeMills:J

    .line 2578
    iput v10, v9, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastRecordState:I

    .line 2580
    .end local v0    # "record":Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;
    .end local v11    # "now":J
    .end local v13    # "interval":J
    :cond_1
    return-void
.end method

.method private updateActivityVisiblity()V
    .locals 6

    .line 2272
    const/4 v0, 0x0

    .line 2273
    .local v0, "inSplitMode":Z
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleActivities:Landroid/util/ArraySet;

    monitor-enter v1

    .line 2274
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleActivities:Landroid/util/ArraySet;

    invoke-virtual {v2}, Landroid/util/ArraySet;->size()I

    move-result v2

    if-lez v2, :cond_3

    .line 2275
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleActivities:Landroid/util/ArraySet;

    invoke-virtual {v3}, Landroid/util/ArraySet;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 2276
    iget-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleActivities:Landroid/util/ArraySet;

    invoke-virtual {v3, v2}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/wm/ActivityRecord;

    .line 2277
    .local v3, "activity":Lcom/android/server/wm/ActivityRecord;
    if-nez v3, :cond_0

    .line 2278
    iget-object v4, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleActivities:Landroid/util/ArraySet;

    add-int/lit8 v5, v2, -0x1

    .end local v2    # "i":I
    .local v5, "i":I
    invoke-virtual {v4, v2}, Landroid/util/ArraySet;->removeAt(I)Ljava/lang/Object;

    move v2, v5

    goto :goto_1

    .line 2279
    .end local v5    # "i":I
    .restart local v2    # "i":I
    :cond_0
    invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->inSplitScreenWindowingMode()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2280
    const/4 v0, 0x1

    .line 2275
    .end local v3    # "activity":Lcom/android/server/wm/ActivityRecord;
    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2283
    .end local v2    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v2, v0}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fputmInSplitMode(Lcom/android/server/am/AppStateManager$AppState;Z)V

    .line 2285
    :cond_3
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2286
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v1, v0}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fputmInSplitMode(Lcom/android/server/am/AppStateManager$AppState;Z)V

    .line 2287
    return-void

    .line 2285
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method private updateProcessInfo(Lcom/android/server/am/ProcessRecord;)V
    .locals 4
    .param p1, "record"    # Lcom/android/server/am/ProcessRecord;

    .line 1997
    iget v0, p1, Lcom/android/server/am/ProcessRecord;->userId:I

    iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mUserId:I

    .line 1998
    iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mUid:I

    .line 1999
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mProcessName:Ljava/lang/String;

    .line 2000
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mPackageName:Ljava/lang/String;

    .line 2001
    iput-object p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mProcessRecord:Lcom/android/server/am/ProcessRecord;

    .line 2002
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getSetAdj()I

    move-result v0

    iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAdj:I

    .line 2003
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getSetProcState()I

    move-result v0

    iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAmsProcState:I

    .line 2004
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mProfile:Lcom/android/server/am/ProcessProfileRecord;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastPss:J

    .line 2005
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mProfile:Lcom/android/server/am/ProcessProfileRecord;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessProfileRecord;->getLastSwapPss()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastSwapPss:J

    .line 2006
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isKilled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIsKilled:Z

    .line 2007
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getAdjType()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAdjType:Ljava/lang/String;

    .line 2008
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->hasActivities()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHasActivity:Z

    .line 2009
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mServices:Lcom/android/server/am/ProcessServiceRecord;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessServiceRecord;->hasForegroundServices()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHasFgService:Z

    .line 2010
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIsMainProc:Z

    .line 2012
    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->is32BitProcess(Lcom/android/server/am/ProcessRecord;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIs64BitProcess:Z

    .line 2013
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mPid:I

    iget v2, p1, Lcom/android/server/am/ProcessRecord;->mPid:I

    if-eq v0, v2, :cond_3

    .line 2014
    iget v0, p1, Lcom/android/server/am/ProcessRecord;->mPid:I

    iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mPid:I

    .line 2015
    iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIsMainProc:Z

    if-eqz v0, :cond_0

    .line 2016
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fputmMainProcStartTime(Lcom/android/server/am/AppStateManager$AppState;J)V

    .line 2018
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SmartPower: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mProcessName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mUid:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mPid:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->LOG_TAG:Ljava/lang/String;

    .line 2019
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mPid:I

    if-lez v0, :cond_3

    iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIsKilled:Z

    if-nez v0, :cond_3

    .line 2020
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 2021
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v2}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmForeground(Lcom/android/server/am/AppStateManager$AppState;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I

    if-eq v2, v1, :cond_1

    .line 2022
    const-string v1, "process start "

    const/4 v2, 0x2

    invoke-direct {p0, v2, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->moveToStateLocked(ILjava/lang/String;)V

    goto :goto_0

    .line 2023
    :cond_1
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmForeground(Lcom/android/server/am/AppStateManager$AppState;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2024
    const-string v1, "process start "

    const/4 v2, 0x3

    invoke-direct {p0, v2, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->moveToStateLocked(ILjava/lang/String;)V

    .line 2026
    :cond_2
    :goto_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2027
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmHandler(Lcom/android/server/am/AppStateManager$AppState;)Lcom/android/server/am/AppStateManager$AppState$MyHandler;

    move-result-object v0

    new-instance v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;

    invoke-direct {v1, p0, p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;-><init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Lcom/android/server/am/ProcessRecord;)V

    invoke-virtual {v0, v1}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    .line 2026
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 2056
    :cond_3
    :goto_1
    iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIsKilled:Z

    if-eqz v0, :cond_4

    .line 2057
    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->processKilledLocked()V

    goto :goto_2

    .line 2059
    :cond_4
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mProcPriorityScore:Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->-$$Nest$mupdatePriorityScore(Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;)V

    .line 2062
    :goto_2
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAdj:I

    const/16 v1, 0x64

    if-lt v0, v1, :cond_5

    .line 2063
    const-string v0, "adj below visible"

    invoke-direct {p0, v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->becomeInactiveIfNeeded(Ljava/lang/String;)V

    goto :goto_3

    .line 2064
    :cond_5
    if-gez v0, :cond_6

    .line 2065
    const-string v0, "adj above foreground"

    invoke-direct {p0, v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->becomeActiveIfNeeded(Ljava/lang/String;)V

    .line 2067
    :cond_6
    :goto_3
    return-void
.end method

.method private updateProviderDepends(Lcom/android/server/am/ProcessRecord;Z)V
    .locals 10
    .param p1, "host"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "isConnect"    # Z

    .line 2457
    iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mUid:I

    if-eq v0, v1, :cond_1

    .line 2458
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mProviderDependProcs:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 2459
    if-eqz p2, :cond_0

    .line 2460
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mProviderDependProcs:Landroid/util/ArrayMap;

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    new-instance v9, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;

    iget-object v4, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget v5, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    iget v6, p1, Lcom/android/server/am/ProcessRecord;->mPid:I

    iget-object v7, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    const/4 v8, 0x0

    move-object v3, v9

    invoke-direct/range {v3 .. v8}, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;-><init>(Lcom/android/server/am/AppStateManager$AppState;IILjava/lang/String;Lcom/android/server/am/AppStateManager$AppState$DependProcInfo-IA;)V

    invoke-virtual {v1, v2, v9}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2463
    :cond_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mProviderDependProcs:Landroid/util/ArrayMap;

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2465
    :goto_0
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 2467
    :cond_1
    :goto_1
    return-void
.end method

.method private updateServiceDepends(Lcom/android/server/am/ProcessRecord;ZZ)V
    .locals 10
    .param p1, "service"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "isConnect"    # Z
    .param p3, "fgService"    # Z

    .line 2437
    iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mUid:I

    if-eq v0, v1, :cond_2

    .line 2438
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mServiceDependProcs:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 2439
    if-eqz p2, :cond_0

    .line 2440
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mServiceDependProcs:Landroid/util/ArrayMap;

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    new-instance v9, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;

    iget-object v4, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget v5, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    iget v6, p1, Lcom/android/server/am/ProcessRecord;->mPid:I

    iget-object v7, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    const/4 v8, 0x0

    move-object v3, v9

    invoke-direct/range {v3 .. v8}, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;-><init>(Lcom/android/server/am/AppStateManager$AppState;IILjava/lang/String;Lcom/android/server/am/AppStateManager$AppState$DependProcInfo-IA;)V

    invoke-virtual {v1, v2, v9}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2443
    :cond_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mServiceDependProcs:Landroid/util/ArrayMap;

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2445
    :goto_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2446
    iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mDependencyProtected:Z

    if-nez v0, :cond_1

    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mUid:I

    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_2

    if-nez p3, :cond_1

    if-nez p2, :cond_2

    .line 2448
    :cond_1
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmSmartPowerPolicyManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mProcessName:Ljava/lang/String;

    iget-object v4, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    iget v5, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    iget v6, p1, Lcom/android/server/am/ProcessRecord;->mPid:I

    move v2, p2

    invoke-virtual/range {v1 .. v6}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->onDependChanged(ZLjava/lang/String;Ljava/lang/String;II)V

    goto :goto_1

    .line 2445
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 2454
    :cond_2
    :goto_1
    return-void
.end method

.method private updateVisibility()V
    .locals 3

    .line 2225
    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->updateActivityVisiblity()V

    .line 2226
    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->updateWindowVisiblity()V

    .line 2227
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 2228
    :try_start_0
    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->hasVisibleWindow()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->hasVisibleActivity()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 2233
    :cond_0
    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I

    if-eqz v1, :cond_2

    const/4 v2, 0x2

    if-ge v1, v2, :cond_2

    .line 2234
    const-string v1, "become invisible"

    invoke-direct {p0, v2, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->moveToStateLocked(ILjava/lang/String;)V

    goto :goto_1

    .line 2229
    :cond_1
    :goto_0
    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    .line 2230
    const-string v1, "become visible"

    invoke-direct {p0, v2, v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->moveToStateLocked(ILjava/lang/String;)V

    .line 2237
    :cond_2
    :goto_1
    monitor-exit v0

    .line 2238
    return-void

    .line 2237
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private updateWindowVisiblity()V
    .locals 5

    .line 2259
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleWindows:Landroid/util/ArraySet;

    monitor-enter v0

    .line 2260
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleWindows:Landroid/util/ArraySet;

    invoke-virtual {v1}, Landroid/util/ArraySet;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 2261
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleWindows:Landroid/util/ArraySet;

    invoke-virtual {v2}, Landroid/util/ArraySet;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 2262
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleWindows:Landroid/util/ArraySet;

    invoke-virtual {v2, v1}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    .line 2263
    .local v2, "win":Lcom/android/server/policy/WindowManagerPolicy$WindowState;
    if-nez v2, :cond_0

    .line 2264
    iget-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleWindows:Landroid/util/ArraySet;

    add-int/lit8 v4, v1, -0x1

    .end local v1    # "i":I
    .local v4, "i":I
    invoke-virtual {v3, v1}, Landroid/util/ArraySet;->removeAt(I)Ljava/lang/Object;

    move v1, v4

    .line 2261
    .end local v2    # "win":Lcom/android/server/policy/WindowManagerPolicy$WindowState;
    .end local v4    # "i":I
    .restart local v1    # "i":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2268
    .end local v1    # "i":I
    :cond_1
    monitor-exit v0

    .line 2269
    return-void

    .line 2268
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private whiteListVideoVisible()Z
    .locals 6

    .line 2302
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleActivities:Landroid/util/ArraySet;

    monitor-enter v0

    .line 2303
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleActivities:Landroid/util/ArraySet;

    invoke-virtual {v2}, Landroid/util/ArraySet;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 2304
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleActivities:Landroid/util/ArraySet;

    invoke-virtual {v2, v1}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/ActivityRecord;

    .line 2305
    .local v2, "activity":Lcom/android/server/wm/ActivityRecord;
    if-eqz v2, :cond_1

    .line 2306
    iget-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-object v3, v3, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v3}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmWhiteListVideoActivities(Lcom/android/server/am/AppStateManager;)Landroid/util/ArraySet;

    move-result-object v3

    invoke-virtual {v3}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 2307
    .local v4, "videoActivity":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/android/server/wm/ActivityRecord;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2308
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 2310
    .end local v4    # "videoActivity":Ljava/lang/String;
    :cond_0
    goto :goto_1

    .line 2303
    .end local v2    # "activity":Lcom/android/server/wm/ActivityRecord;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2313
    .end local v1    # "i":I
    :cond_2
    monitor-exit v0

    .line 2314
    const/4 v0, 0x0

    return v0

    .line 2313
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method public addAndGetCurCpuTime(J)J
    .locals 2
    .param p1, "cpuTime"    # J

    .line 2826
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mCurCpuTime:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public canHibernate()Z
    .locals 1

    .line 2809
    invoke-virtual {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isProcessPerceptible()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmSmartPowerPolicyManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/SmartPowerPolicyManager;

    move-result-object v0

    .line 2810
    invoke-virtual {v0, p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isProcessHibernationWhiteList(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 2809
    :goto_0
    return v0
.end method

.method public compareAndSetLastCpuTime(JJ)Z
    .locals 1
    .param p1, "oldTime"    # J
    .param p3, "cpuTime"    # J

    .line 2831
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastCpuTime:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, p1, p2, p3, p4}, Ljava/util/concurrent/atomic/AtomicLong;->compareAndSet(JJ)Z

    move-result v0

    return v0
.end method

.method public dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
    .locals 20
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "args"    # [Ljava/lang/String;
    .param p3, "opti"    # I

    .line 2850
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "        name="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mProcessName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mPid:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")  state="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I

    .line 2851
    invoke-static {v3}, Lcom/android/server/am/AppStateManager;->processStateToString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2850
    invoke-virtual {v2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2852
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "            pkg="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " adj="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAdj:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " pri="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 2854
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPriorityScore()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " usage="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 2855
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getUsageLevel()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2852
    invoke-virtual {v2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2856
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "            audioActive="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v3, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAudioActive:Z

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " gpsActive="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v3, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mGpsActive:Z

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " bleActive="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v3, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mBleActive:Z

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " netActive="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v3, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mNetActive:Z

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2858
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    iget-wide v5, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastStateTimeMills:J

    sub-long/2addr v3, v5

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    .line 2860
    .local v3, "currentDuration":J
    iget-wide v7, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mForegroundDuration:J

    div-long/2addr v7, v5

    .line 2861
    .local v7, "foregroundDuration":J
    iget-wide v9, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mBackgroundDuration:J

    div-long/2addr v9, v5

    .line 2862
    .local v9, "backgroundDuration":J
    iget-wide v11, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIdleDuration:J

    div-long/2addr v11, v5

    .line 2863
    .local v11, "idleDuration":J
    iget-wide v13, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHibernatonDuration:J

    div-long/2addr v13, v5

    .line 2864
    .local v13, "hibernationDuration":J
    iget v0, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I

    const/4 v5, 0x6

    if-ne v0, v5, :cond_0

    .line 2865
    add-long/2addr v11, v3

    goto :goto_0

    .line 2866
    :cond_0
    const/4 v5, 0x7

    if-ne v0, v5, :cond_1

    .line 2867
    add-long/2addr v13, v3

    goto :goto_0

    .line 2868
    :cond_1
    const/4 v5, 0x3

    if-eqz v0, :cond_2

    if-ge v0, v5, :cond_2

    .line 2869
    add-long/2addr v7, v3

    goto :goto_0

    .line 2870
    :cond_2
    if-lt v0, v5, :cond_3

    .line 2871
    add-long/2addr v9, v3

    .line 2873
    :cond_3
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "            f("

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ":"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v5, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mForegroundCount:J

    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ") b("

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ":"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v5, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mBackgroundCount:J

    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ") i("

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ":"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v5, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIdleCount:J

    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ") h("

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ":"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v5, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHibernatonCount:J

    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ")"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2878
    iget-object v5, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleActivities:Landroid/util/ArraySet;

    monitor-enter v5

    .line 2879
    :try_start_0
    iget-object v0, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleActivities:Landroid/util/ArraySet;

    invoke-virtual {v0}, Landroid/util/ArraySet;->size()I

    move-result v0

    if-lez v0, :cond_6

    .line 2880
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "            visible activities size="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v6, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleActivities:Landroid/util/ArraySet;

    .line 2881
    invoke-virtual {v6}, Landroid/util/ArraySet;->size()I

    move-result v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2880
    invoke-virtual {v2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2882
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v6, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleActivities:Landroid/util/ArraySet;

    invoke-virtual {v6}, Landroid/util/ArraySet;->size()I

    move-result v6

    if-ge v0, v6, :cond_5

    .line 2883
    iget-object v6, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleActivities:Landroid/util/ArraySet;

    invoke-virtual {v6, v0}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/ref/WeakReference;

    invoke-virtual {v6}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/wm/ActivityRecord;

    .line 2884
    .local v6, "record":Lcom/android/server/wm/ActivityRecord;
    if-eqz v6, :cond_4

    .line 2885
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-wide/from16 v16, v3

    .end local v3    # "currentDuration":J
    .local v16, "currentDuration":J
    :try_start_1
    const-string v3, "            "

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_2

    .line 2884
    .end local v16    # "currentDuration":J
    .restart local v3    # "currentDuration":J
    :cond_4
    move-wide/from16 v16, v3

    .line 2882
    .end local v3    # "currentDuration":J
    .end local v6    # "record":Lcom/android/server/wm/ActivityRecord;
    .restart local v16    # "currentDuration":J
    :goto_2
    add-int/lit8 v0, v0, 0x1

    move-wide/from16 v3, v16

    goto :goto_1

    .end local v16    # "currentDuration":J
    .restart local v3    # "currentDuration":J
    :cond_5
    move-wide/from16 v16, v3

    .end local v3    # "currentDuration":J
    .restart local v16    # "currentDuration":J
    goto :goto_3

    .line 2879
    .end local v0    # "i":I
    .end local v16    # "currentDuration":J
    .restart local v3    # "currentDuration":J
    :cond_6
    move-wide/from16 v16, v3

    .line 2889
    .end local v3    # "currentDuration":J
    .restart local v16    # "currentDuration":J
    :goto_3
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 2891
    iget-object v3, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleWindows:Landroid/util/ArraySet;

    monitor-enter v3

    .line 2892
    :try_start_2
    iget-object v0, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleWindows:Landroid/util/ArraySet;

    invoke-virtual {v0}, Landroid/util/ArraySet;->size()I

    move-result v0

    if-lez v0, :cond_8

    .line 2893
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "            visible windows size="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleWindows:Landroid/util/ArraySet;

    invoke-virtual {v4}, Landroid/util/ArraySet;->size()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2894
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_4
    iget-object v4, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleWindows:Landroid/util/ArraySet;

    invoke-virtual {v4}, Landroid/util/ArraySet;->size()I

    move-result v4

    if-ge v0, v4, :cond_8

    .line 2895
    iget-object v4, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mVisibleWindows:Landroid/util/ArraySet;

    invoke-virtual {v4, v0}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    .line 2896
    .local v4, "win":Lcom/android/server/policy/WindowManagerPolicy$WindowState;
    if-eqz v4, :cond_7

    .line 2897
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "            "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2894
    .end local v4    # "win":Lcom/android/server/policy/WindowManagerPolicy$WindowState;
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 2901
    .end local v0    # "i":I
    :cond_8
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2903
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v3, "HH:mm:ss.SSS"

    invoke-direct {v0, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 2905
    .local v0, "formater":Ljava/text/SimpleDateFormat;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sget-wide v5, Lcom/miui/app/smartpower/SmartPowerSettings;->MAX_HISTORY_REPORT_DURATION:J

    sub-long/2addr v3, v5

    invoke-direct {v1, v3, v4}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getHistoryInfos(J)Ljava/util/ArrayList;

    move-result-object v3

    .line 2907
    .local v3, "history":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;>;"
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "            state change history:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2908
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;

    .line 2909
    .local v5, "record":Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "            "

    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    new-instance v15, Ljava/util/Date;

    .line 2910
    move-object/from16 v18, v3

    move-object/from16 v19, v4

    .end local v3    # "history":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;>;"
    .local v18, "history":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;>;"
    invoke-static {v5}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;->-$$Nest$mgetTimeStamp(Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;)J

    move-result-wide v3

    invoke-direct {v15, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v15}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2909
    invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2911
    .end local v5    # "record":Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;
    move-object/from16 v3, v18

    move-object/from16 v4, v19

    goto :goto_5

    .line 2912
    .end local v18    # "history":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;>;"
    .restart local v3    # "history":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;>;"
    :cond_9
    return-void

    .line 2901
    .end local v0    # "formater":Ljava/text/SimpleDateFormat;
    .end local v3    # "history":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/AppStateManager$AppState$RunningProcess$StateChangeRecord;>;"
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 2889
    .end local v16    # "currentDuration":J
    .local v3, "currentDuration":J
    :catchall_1
    move-exception v0

    move-wide/from16 v16, v3

    .end local v3    # "currentDuration":J
    .restart local v16    # "currentDuration":J
    :goto_6
    :try_start_4
    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v0

    :catchall_2
    move-exception v0

    goto :goto_6
.end method

.method public getAdj()I
    .locals 1

    .line 2668
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAdj:I

    return v0
.end method

.method public getAdjType()Ljava/lang/String;
    .locals 1

    .line 2683
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAdjType:Ljava/lang/String;

    return-object v0
.end method

.method public getAmsProcState()I
    .locals 1

    .line 2678
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAmsProcState:I

    return v0
.end method

.method public getCurCpuTime()J
    .locals 2

    .line 2841
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mCurCpuTime:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public getCurrentState()I
    .locals 1

    .line 2673
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I

    return v0
.end method

.method public getLastCpuTime()J
    .locals 2

    .line 2846
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastCpuTime:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public getLastTaskId()I
    .locals 1

    .line 2687
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastTaskId:I

    return v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .line 2658
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getPid()I
    .locals 1

    .line 2648
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mPid:I

    return v0
.end method

.method public getPriorityScore()I
    .locals 1

    .line 2799
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mProcPriorityScore:Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->-$$Nest$fgetmPriorityScore(Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;)I

    move-result v0

    return v0
.end method

.method public getProcessName()Ljava/lang/String;
    .locals 1

    .line 2653
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mProcessName:Ljava/lang/String;

    return-object v0
.end method

.method public getProcessRecord()Lcom/android/server/am/ProcessRecord;
    .locals 1

    .line 2663
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mProcessRecord:Lcom/android/server/am/ProcessRecord;

    return-object v0
.end method

.method public getPss()J
    .locals 4

    .line 2783
    iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastPss:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2784
    invoke-virtual {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->updatePss()V

    .line 2786
    :cond_0
    iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastPss:J

    return-wide v0
.end method

.method public getSwapPss()J
    .locals 4

    .line 2791
    iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastPss:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2792
    invoke-virtual {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->updatePss()V

    .line 2794
    :cond_0
    iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastSwapPss:J

    return-wide v0
.end method

.method public getUid()I
    .locals 1

    .line 2643
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mUid:I

    return v0
.end method

.method public getUsageLevel()I
    .locals 1

    .line 2804
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mProcPriorityScore:Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->-$$Nest$fgetmUsageLevel(Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;)I

    move-result v0

    return v0
.end method

.method public getUserId()I
    .locals 1

    .line 2638
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mUserId:I

    return v0
.end method

.method public hasActivity()Z
    .locals 1

    .line 2761
    iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHasActivity:Z

    return v0
.end method

.method public hasForegrundService()Z
    .locals 1

    .line 2766
    iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mHasFgService:Z

    return v0
.end method

.method public inCurrentStack()Z
    .locals 1

    .line 2710
    const/4 v0, 0x0

    return v0
.end method

.method public inSplitMode()Z
    .locals 1

    .line 2706
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmInSplitMode(Lcom/android/server/am/AppStateManager$AppState;)Z

    move-result v0

    return v0
.end method

.method public is64BitProcess()Z
    .locals 1

    .line 2724
    iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIs64BitProcess:Z

    return v0
.end method

.method public isAutoStartApp()Z
    .locals 1

    .line 2732
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmIsAutoStartApp(Lcom/android/server/am/AppStateManager$AppState;)Z

    move-result v0

    return v0
.end method

.method public isDependsProvidersProcess(Ljava/lang/String;)Z
    .locals 1
    .param p1, "dependsProcName"    # Ljava/lang/String;

    .line 2403
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mProviderDependProcs:Landroid/util/ArrayMap;

    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isDependsServiceProcess(Ljava/lang/String;)Z
    .locals 2
    .param p1, "dependsProcName"    # Ljava/lang/String;

    .line 2396
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mServiceDependProcs:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 2397
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mServiceDependProcs:Landroid/util/ArrayMap;

    invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    monitor-exit v0

    return v1

    .line 2398
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isForeground()Z
    .locals 1

    .line 2715
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmForeground(Lcom/android/server/am/AppStateManager$AppState;)Z

    move-result v0

    return v0
.end method

.method public isHibernation()Z
    .locals 2

    .line 2742
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isIdle()Z
    .locals 2

    .line 2737
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I

    const/4 v1, 0x5

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isKilled()Z
    .locals 1

    .line 2747
    iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIsKilled:Z

    if-nez v0, :cond_1

    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isLastInterActive()Z
    .locals 6

    .line 2692
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 2693
    .local v0, "now":J
    iget-wide v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastInteractiveTimeMills:J

    sub-long v2, v0, v2

    sget-wide v4, Lcom/miui/app/smartpower/SmartPowerSettings;->DEF_LAST_INTER_ACTIVE_DURATION:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v2}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmLastUidInteractiveTimeMills(Lcom/android/server/am/AppStateManager$AppState;)J

    move-result-wide v2

    sub-long v2, v0, v2

    sget-wide v4, Lcom/miui/app/smartpower/SmartPowerSettings;->DEF_LAST_INTER_ACTIVE_DURATION:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v2, 0x1

    :goto_1
    return v2
.end method

.method public isMainProc()Z
    .locals 1

    .line 2633
    iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIsMainProc:Z

    return v0
.end method

.method public isProcessPerceptible()Z
    .locals 3

    .line 2752
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAudioActive:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mGpsActive:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mNetActive:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mBleActive:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmIsBackupServiceApp(Lcom/android/server/am/AppStateManager$AppState;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2756
    invoke-virtual {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isLastInterActive()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    nop

    .line 2752
    :goto_1
    return v1
.end method

.method public isSystemApp()Z
    .locals 1

    .line 2720
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmSystemApp(Lcom/android/server/am/AppStateManager$AppState;)Z

    move-result v0

    return v0
.end method

.method public isSystemSignature()Z
    .locals 1

    .line 2728
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmSystemSignature(Lcom/android/server/am/AppStateManager$AppState;)Z

    move-result v0

    return v0
.end method

.method public isVisible()Z
    .locals 2

    .line 2701
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public setLastCpuTime(J)V
    .locals 1
    .param p1, "cpuTime"    # J

    .line 2836
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastCpuTime:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 2837
    return-void
.end method

.method public setPss(JJ)V
    .locals 0
    .param p1, "pss"    # J
    .param p3, "swapPss"    # J

    .line 2777
    iput-wide p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastPss:J

    .line 2778
    iput-wide p3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastSwapPss:J

    .line 2779
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 2815
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mProcessName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mUid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mPid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mState:I

    .line 2816
    invoke-static {v1}, Lcom/android/server/am/AppStateManager;->processStateToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " adj="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mAdj:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " pri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 2818
    invoke-virtual {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPriorityScore()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " usage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 2819
    invoke-virtual {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getUsageLevel()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isKilled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIsKilled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is64Bit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mIs64BitProcess:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2815
    return-object v0
.end method

.method public updatePss()V
    .locals 3

    .line 2771
    const/4 v0, 0x3

    new-array v0, v0, [J

    .line 2772
    .local v0, "swapTmp":[J
    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mPid:I

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/os/Debug;->getPss(I[J[J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastPss:J

    .line 2773
    const/4 v1, 0x1

    aget-wide v1, v0, v1

    iput-wide v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->mLastSwapPss:J

    .line 2774
    return-void
.end method
