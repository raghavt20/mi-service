public class com.android.server.am.ProcessProphetImpl$MyReceiver extends android.content.BroadcastReceiver {
	 /* .source "ProcessProphetImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/ProcessProphetImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x1 */
/* name = "MyReceiver" */
} // .end annotation
/* # instance fields */
final com.android.server.am.ProcessProphetImpl this$0; //synthetic
/* # direct methods */
public com.android.server.am.ProcessProphetImpl$MyReceiver ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/ProcessProphetImpl; */
/* .line 1061 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 1064 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 1065 */
/* .local v0, "action":Ljava/lang/String; */
final String v1 = "android.intent.action.USER_PRESENT"; // const-string v1, "android.intent.action.USER_PRESENT"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 1066 */
	 v1 = this.this$0;
	 v1 = this.mHandler;
	 v2 = this.this$0;
	 v2 = this.mHandler;
	 int v3 = 1; // const/4 v3, 0x1
	 (( com.android.server.am.ProcessProphetImpl$MyHandler ) v2 ).obtainMessage ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;
	 (( com.android.server.am.ProcessProphetImpl$MyHandler ) v1 ).sendMessage ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z
	 /* .line 1067 */
	 v1 = 	 com.android.server.am.ProcessProphetImpl .-$$Nest$sfgetDEBUG ( );
	 if ( v1 != null) { // if-eqz v1, :cond_3
		 /* .line 1068 */
		 final String v1 = "ProcessProphet"; // const-string v1, "ProcessProphet"
		 /* const-string/jumbo v2, "user present." */
		 android.util.Slog .d ( v1,v2 );
		 /* .line 1070 */
	 } // :cond_0
	 final String v1 = "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"; // const-string v1, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"
	 v1 = 	 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_3
		 /* .line 1072 */
		 final String v1 = "android.bluetooth.profile.extra.STATE"; // const-string v1, "android.bluetooth.profile.extra.STATE"
		 int v2 = 0; // const/4 v2, 0x0
		 v1 = 		 (( android.content.Intent ) p2 ).getIntExtra ( v1, v2 ); // invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
		 /* .line 1073 */
		 /* .local v1, "state":I */
		 int v2 = 2; // const/4 v2, 0x2
		 /* if-ne v1, v2, :cond_1 */
		 /* .line 1074 */
		 final String v3 = "android.bluetooth.device.extra.DEVICE"; // const-string v3, "android.bluetooth.device.extra.DEVICE"
		 (( android.content.Intent ) p2 ).getParcelableExtra ( v3 ); // invoke-virtual {p2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;
		 /* check-cast v3, Landroid/bluetooth/BluetoothDevice; */
		 /* .line 1075 */
		 /* .local v3, "device":Landroid/bluetooth/BluetoothDevice; */
		 (( android.bluetooth.BluetoothDevice ) v3 ).getName ( ); // invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;
		 /* .line 1076 */
		 /* .local v4, "deviceName":Ljava/lang/String; */
		 v5 = this.this$0;
		 v5 = this.mHandler;
		 (( com.android.server.am.ProcessProphetImpl$MyHandler ) v5 ).obtainMessage ( v2, v4 ); // invoke-virtual {v5, v2, v4}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
		 /* .line 1077 */
		 /* .local v2, "msg":Landroid/os/Message; */
		 v5 = this.this$0;
		 v5 = this.mHandler;
		 (( com.android.server.am.ProcessProphetImpl$MyHandler ) v5 ).sendMessage ( v2 ); // invoke-virtual {v5, v2}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z
	 } // .end local v2 # "msg":Landroid/os/Message;
} // .end local v3 # "device":Landroid/bluetooth/BluetoothDevice;
} // .end local v4 # "deviceName":Ljava/lang/String;
/* .line 1078 */
} // :cond_1
/* if-nez v1, :cond_2 */
/* .line 1079 */
v2 = this.this$0;
v2 = this.mHandler;
int v3 = 3; // const/4 v3, 0x3
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v2 ).obtainMessage ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;
/* .line 1080 */
/* .restart local v2 # "msg":Landroid/os/Message; */
v3 = this.this$0;
v3 = this.mHandler;
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v3 ).sendMessage ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 1078 */
} // .end local v2 # "msg":Landroid/os/Message;
} // :cond_2
} // :goto_0
/* nop */
/* .line 1083 */
} // .end local v1 # "state":I
} // :cond_3
} // :goto_1
return;
} // .end method
