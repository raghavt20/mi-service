public class com.android.server.am.ProcessProphetImpl$MyHandler extends android.os.Handler {
	 /* .source "ProcessProphetImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/ProcessProphetImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x1 */
/* name = "MyHandler" */
} // .end annotation
/* # instance fields */
final com.android.server.am.ProcessProphetImpl this$0; //synthetic
/* # direct methods */
public com.android.server.am.ProcessProphetImpl$MyHandler ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/ProcessProphetImpl; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 1087 */
this.this$0 = p1;
/* .line 1088 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 1089 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 4 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 1093 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* :pswitch_0 */
/* goto/16 :goto_0 */
/* .line 1134 */
/* :pswitch_1 */
v0 = this.this$0;
com.android.server.am.ProcessProphetImpl .-$$Nest$fgetmProcessProphetCloud ( v0 );
(( com.android.server.am.ProcessProphetCloud ) v0 ).registerProcProphetCloudObserver ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetCloud;->registerProcProphetCloudObserver()V
/* .line 1135 */
v0 = this.this$0;
com.android.server.am.ProcessProphetImpl .-$$Nest$fgetmProcessProphetCloud ( v0 );
(( com.android.server.am.ProcessProphetCloud ) v0 ).updateProcProphetCloudControlParas ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetCloud;->updateProcProphetCloudControlParas()V
/* .line 1136 */
/* goto/16 :goto_0 */
/* .line 1131 */
/* :pswitch_2 */
v0 = this.this$0;
(( com.android.server.am.ProcessProphetImpl ) v0 ).testMTBF ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetImpl;->testMTBF()V
/* .line 1132 */
/* goto/16 :goto_0 */
/* .line 1128 */
/* :pswitch_3 */
v0 = this.this$0;
com.android.server.am.ProcessProphetImpl .-$$Nest$mhandleDump ( v0 );
/* .line 1129 */
/* .line 1125 */
/* :pswitch_4 */
v0 = this.this$0;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
com.android.server.am.ProcessProphetImpl .-$$Nest$mhandleMemPressure ( v0,v1 );
/* .line 1126 */
/* .line 1122 */
/* :pswitch_5 */
v0 = this.this$0;
com.android.server.am.ProcessProphetImpl .-$$Nest$mhandleIdleUpdate ( v0 );
/* .line 1123 */
/* .line 1119 */
/* :pswitch_6 */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Lcom/android/server/am/ProcessRecord; */
com.android.server.am.ProcessProphetImpl .-$$Nest$mhandleKillProc ( v0,v1 );
/* .line 1120 */
/* .line 1116 */
/* :pswitch_7 */
v0 = this.this$0;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
com.android.server.am.ProcessProphetImpl .-$$Nest$mhandleCheckEmptyProcs ( v0,v1 );
/* .line 1117 */
/* .line 1113 */
/* :pswitch_8 */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Lcom/android/server/am/ProcessRecord; */
/* iget v2, p1, Landroid/os/Message;->arg1:I */
com.android.server.am.ProcessProphetImpl .-$$Nest$mhandleProcStarted ( v0,v1,v2 );
/* .line 1114 */
/* .line 1110 */
/* :pswitch_9 */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Ljava/lang/CharSequence; */
com.android.server.am.ProcessProphetImpl .-$$Nest$mhandleCopy ( v0,v1 );
/* .line 1111 */
/* .line 1107 */
/* :pswitch_a */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Ljava/lang/String; */
/* iget v2, p1, Landroid/os/Message;->arg1:I */
/* iget v3, p1, Landroid/os/Message;->arg2:I */
com.android.server.am.ProcessProphetImpl .-$$Nest$mhandleLaunchEvent ( v0,v1,v2,v3 );
/* .line 1108 */
/* .line 1104 */
/* :pswitch_b */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Ljava/lang/String; */
com.android.server.am.ProcessProphetImpl .-$$Nest$mhandleAudioFocusChanged ( v0,v1 );
/* .line 1105 */
/* .line 1101 */
/* :pswitch_c */
v0 = this.this$0;
com.android.server.am.ProcessProphetImpl .-$$Nest$mhandleBluetoothDisConnected ( v0 );
/* .line 1102 */
/* .line 1098 */
/* :pswitch_d */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Ljava/lang/String; */
com.android.server.am.ProcessProphetImpl .-$$Nest$mhandleBluetoothConnected ( v0,v1 );
/* .line 1099 */
/* .line 1095 */
/* :pswitch_e */
v0 = this.this$0;
com.android.server.am.ProcessProphetImpl .-$$Nest$mhandleUnlock ( v0 );
/* .line 1096 */
/* nop */
/* .line 1140 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_e */
/* :pswitch_d */
/* :pswitch_c */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_0 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
