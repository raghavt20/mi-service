.class public Lcom/android/server/am/OomAdjusterImpl;
.super Lcom/android/server/am/OomAdjusterStub;
.source "OomAdjusterImpl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;
    }
.end annotation


# static fields
.field public static final ADJ_TYPE_BIND_IMPROVE:Ljava/lang/String; = "bind-fix"

.field public static final ADJ_TYPE_CAMERA_IMPROVE:Ljava/lang/String; = "camera-improve"

.field public static final ADJ_TYPE_GAME_IMPROVE:Ljava/lang/String; = "game-improve"

.field public static final ADJ_TYPE_GAME_IMPROVE_DOWNLOAD:Ljava/lang/String; = "game-improve-download"

.field public static final ADJ_TYPE_GAME_IMPROVE_PAYMENT:Ljava/lang/String; = "game-improve-payment"

.field public static final ADJ_TYPE_PREVIOUS_IMPROVE:Ljava/lang/String; = "previous-improve"

.field public static final ADJ_TYPE_PREVIOUS_IMPROVE_SCALE:Ljava/lang/String; = "previous-improve-scale"

.field public static final ADJ_TYPE_RESIDENT_IMPROVE:Ljava/lang/String; = "resident-improveAdj"

.field public static final ADJ_TYPE_WIDGET_DEGENERATE:Ljava/lang/String; = "widget-degenerate"

.field private static final BACKGROUND_APP_COUNT_LIMIT:I

.field private static final GAME_APP_BACKGROUND_STATE:I = 0x2

.field private static final GAME_APP_TOP_TO_BACKGROUND_STATE:I = 0x4

.field private static final GAME_SCENE_DEFAULT_STATE:I = 0x0

.field private static final GAME_SCENE_DOWNLOAD_STATE:I = 0x2

.field private static final GAME_SCENE_PLAYING_STATE:I = 0x3

.field private static final GAME_SCENE_WATCHING_STATE:I = 0x4

.field private static final HIGH_MEMORY_RESIDENT_APP_COUNT:I

.field private static IMPROVE_RESIDENT_APP_ADJ_ENABLE:Z = false

.field public static LIMIT_BIND_VEISIBLE_ENABLED:Z = false

.field private static final LOW_MEMORY_RESIDENT_APP_COUNT:I

.field private static final MAX_APP_WEEK_LAUNCH_COUNT:I = 0x15

.field private static final MAX_APP_WEEK_TOP_TIME:I = 0x6ddd00

.field private static final MAX_GAME_DOWNLOAD_TIME:I = 0x124f80

.field private static final MAX_PAYMENT_SCENE_TIME:I = 0x1d4c0

.field private static final MAX_PREVIOUS_GAME_TIME:I = 0x927c0

.field private static final MAX_PREVIOUS_PROTECT_TIME:I = 0x124f80

.field private static final MAX_PREVIOUS_TIME:I = 0x493e0

.field private static final MIDDLE_MEMORY_RESIDENT_APP_COUNT:I

.field private static final PACKAGE_NAME_CAMERA:Ljava/lang/String; = "com.android.camera"

.field private static final PACKAGE_NAME_MAGE_PAYMENT_SDK:Ljava/lang/String; = "com.xiaomi.gamecenter.sdk.service"

.field private static final PACKAGE_NAME_WECHAT:Ljava/lang/String; = "com.tencent.mm"

.field private static final PREVIOUS_APP_CRITICAL_ADJ:I = 0x2bd

.field private static final PREVIOUS_APP_MAJOR_ADJ:I = 0x2be

.field private static final PREVIOUS_APP_MINOR_ADJ:I = 0x2da

.field private static final PREVIOUS_PROTECT_CRITICAL_COUNT:I

.field private static QQ_PLAYER:Ljava/lang/String; = null

.field private static final RESIDENT_APP_COUNT:I

.field private static final RESIDENT_APP_COUNT_LIMIT:I

.field private static SCALE_BACKGROUND_APP_ADJ_ENABLE:Z = false

.field private static final TOTAL_MEMORY:J

.field private static final UNTRUSTEDAPP_BG_ENABLED:Z = false

.field private static final WIDGET_PROTECT_TIME:I = 0xbb8

.field private static mRunningGameMap:Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;

.field private static final mWidgetProcBCWhiteList:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sPaymentAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sPreviousBackgroundAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sPreviousResidentAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sSubProcessAdjBindList:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private static skipMoveCgroupList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mForegroundPkg:Ljava/lang/String;

.field private mForegroundResultToProc:Ljava/lang/String;

.field private mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 61
    sget v0, Landroid/os/spc/PressureStateSettings;->PREVIOUS_PROTECT_CRITICAL_COUNT:I

    sput v0, Lcom/android/server/am/OomAdjusterImpl;->PREVIOUS_PROTECT_CRITICAL_COUNT:I

    .line 72
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    sput-object v0, Lcom/android/server/am/OomAdjusterImpl;->mWidgetProcBCWhiteList:Landroid/util/ArraySet;

    .line 78
    nop

    .line 79
    const-string v1, "persist.sys.spc.bindvisible.enabled"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/android/server/am/OomAdjusterImpl;->LIMIT_BIND_VEISIBLE_ENABLED:Z

    .line 83
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/android/server/am/OomAdjusterImpl;->skipMoveCgroupList:Ljava/util/List;

    .line 85
    const-string v2, "com.tencent.mm"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    sget-object v1, Lcom/android/server/am/OomAdjusterImpl;->skipMoveCgroupList:Ljava/util/List;

    const-string v3, "com.tencent.mobileqq"

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    new-instance v1, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;

    const/4 v3, 0x0

    invoke-direct {v1, v3}, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;-><init>(Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap-IA;)V

    sput-object v1, Lcom/android/server/am/OomAdjusterImpl;->mRunningGameMap:Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;

    .line 91
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/android/server/am/OomAdjusterImpl;->sSubProcessAdjBindList:Ljava/util/Map;

    .line 93
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/android/server/am/OomAdjusterImpl;->sPreviousBackgroundAppList:Ljava/util/List;

    .line 94
    invoke-static {}, Lcom/android/server/am/OomAdjusterImpl;->getBackgroundAppCount()I

    move-result v1

    sput v1, Lcom/android/server/am/OomAdjusterImpl;->BACKGROUND_APP_COUNT_LIMIT:I

    .line 96
    nop

    .line 97
    const-string v1, "persist.sys.spc.scale.backgorund.app.enable"

    const/4 v3, 0x0

    invoke-static {v1, v3}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/android/server/am/OomAdjusterImpl;->SCALE_BACKGROUND_APP_ADJ_ENABLE:Z

    .line 98
    nop

    .line 99
    const-string v1, "persist.sys.spc.resident.app.enable"

    invoke-static {v1, v3}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/android/server/am/OomAdjusterImpl;->IMPROVE_RESIDENT_APP_ADJ_ENABLE:Z

    .line 100
    nop

    .line 101
    const-string v1, "persist.sys.miui.resident.app.count"

    invoke-static {v1, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    sput v1, Lcom/android/server/am/OomAdjusterImpl;->RESIDENT_APP_COUNT:I

    .line 105
    invoke-static {}, Landroid/os/Process;->getTotalMemory()J

    move-result-wide v3

    const/16 v5, 0x1e

    shr-long/2addr v3, v5

    sput-wide v3, Lcom/android/server/am/OomAdjusterImpl;->TOTAL_MEMORY:J

    .line 106
    add-int/lit8 v3, v1, 0xa

    sput v3, Lcom/android/server/am/OomAdjusterImpl;->HIGH_MEMORY_RESIDENT_APP_COUNT:I

    .line 107
    add-int/lit8 v3, v1, 0x5

    sput v3, Lcom/android/server/am/OomAdjusterImpl;->MIDDLE_MEMORY_RESIDENT_APP_COUNT:I

    .line 108
    add-int/lit8 v1, v1, 0x2

    sput v1, Lcom/android/server/am/OomAdjusterImpl;->LOW_MEMORY_RESIDENT_APP_COUNT:I

    .line 109
    invoke-static {}, Lcom/android/server/am/OomAdjusterImpl;->getResidentAppCount()I

    move-result v1

    sput v1, Lcom/android/server/am/OomAdjusterImpl;->RESIDENT_APP_COUNT_LIMIT:I

    .line 110
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/android/server/am/OomAdjusterImpl;->sPreviousResidentAppList:Ljava/util/List;

    .line 151
    const-string v1, "com.tencent.qqmusic:QQPlayerService"

    sput-object v1, Lcom/android/server/am/OomAdjusterImpl;->QQ_PLAYER:Ljava/lang/String;

    .line 154
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/android/server/am/OomAdjusterImpl;->sPaymentAppList:Ljava/util/List;

    .line 156
    const-string v1, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-virtual {v0, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 157
    const-string v1, "miui.appwidget.action.APPWIDGET_UPDATE"

    invoke-virtual {v0, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 159
    sget-object v0, Lcom/android/server/am/OomAdjusterImpl;->sPaymentAppList:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 160
    sget-object v0, Lcom/android/server/am/OomAdjusterImpl;->sPaymentAppList:Ljava/util/List;

    const-string v1, "com.eg.android.AlipayGphone"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 161
    sget-object v0, Lcom/android/server/am/OomAdjusterImpl;->sPaymentAppList:Ljava/util/List;

    const-string v1, "com.xiaomi.gamecenter.sdk.service"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 41
    invoke-direct {p0}, Lcom/android/server/am/OomAdjusterStub;-><init>()V

    .line 88
    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/am/OomAdjusterImpl;->mForegroundPkg:Ljava/lang/String;

    .line 89
    iput-object v0, p0, Lcom/android/server/am/OomAdjusterImpl;->mForegroundResultToProc:Ljava/lang/String;

    return-void
.end method

.method private computeOomAdjForBackgroundApp(Lcom/android/server/am/ProcessRecord;I)Z
    .locals 9
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "procState"    # I

    .line 204
    invoke-direct {p0}, Lcom/android/server/am/OomAdjusterImpl;->updateBackgroundAppList()V

    .line 205
    sget-object v0, Lcom/android/server/am/OomAdjusterImpl;->sPreviousBackgroundAppList:Ljava/util/List;

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 206
    sget-object v0, Lcom/android/server/am/OomAdjusterImpl;->sPreviousBackgroundAppList:Ljava/util/List;

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 207
    .local v0, "index":I
    const/16 v1, -0x2710

    .line 208
    .local v1, "tempAdj":I
    sget v2, Lcom/android/server/am/OomAdjusterImpl;->BACKGROUND_APP_COUNT_LIMIT:I

    if-ge v0, v2, :cond_0

    .line 209
    mul-int/lit8 v2, v0, 0x2

    add-int/lit16 v2, v2, 0x2bc

    .end local v1    # "tempAdj":I
    .local v2, "tempAdj":I
    goto :goto_0

    .line 211
    .end local v2    # "tempAdj":I
    .restart local v1    # "tempAdj":I
    :cond_0
    mul-int/lit8 v2, v2, 0x2

    add-int/lit16 v2, v2, 0x2bc

    .line 213
    .end local v1    # "tempAdj":I
    .restart local v2    # "tempAdj":I
    :goto_0
    iget-object v4, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    const/4 v5, 0x0

    const-string v8, "ADJ_TYPE_PREVIOUS_IMPROVE_SCALE"

    move-object v3, p0

    move v6, v2

    move v7, p2

    invoke-direct/range {v3 .. v8}, Lcom/android/server/am/OomAdjusterImpl;->modifyProcessRecordAdj(Lcom/android/server/am/ProcessStateRecord;ZIILjava/lang/String;)V

    .line 215
    const/4 v1, 0x1

    return v1

    .line 217
    .end local v0    # "index":I
    .end local v2    # "tempAdj":I
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method private computeOomAdjForGameApp(Lcom/android/server/am/ProcessRecord;I)Z
    .locals 12
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "procState"    # I

    .line 350
    const/4 v0, 0x0

    .line 351
    .local v0, "isChange":Z
    iget-object v7, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    .line 352
    .local v7, "appState":Lcom/android/server/am/ProcessStateRecord;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    invoke-virtual {v7}, Lcom/android/server/am/ProcessStateRecord;->getLastTopTime()J

    move-result-wide v3

    sub-long v8, v1, v3

    .line 353
    .local v8, "backgroundTime":J
    invoke-static {}, Lcom/android/server/am/SystemPressureController;->getInstance()Lcom/android/server/am/SystemPressureController;

    move-result-object v1

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/server/am/SystemPressureController;->isGameApp(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 354
    invoke-static {}, Lcom/android/server/am/MemoryFreezeStub;->getInstance()Lcom/android/server/am/MemoryFreezeStub;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/android/server/am/MemoryFreezeStub;->isNeedProtect(Lcom/android/server/am/ProcessRecord;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 355
    const/4 v3, 0x0

    const/16 v4, 0x2bd

    const-string v6, "game-improveAdj(memory-freeze)"

    move-object v1, p0

    move-object v2, v7

    move v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/android/server/am/OomAdjusterImpl;->modifyProcessRecordAdj(Lcom/android/server/am/ProcessStateRecord;ZIILjava/lang/String;)V

    .line 357
    const/4 v0, 0x1

    goto :goto_1

    .line 358
    :cond_0
    const-wide/32 v1, 0x927c0

    cmp-long v1, v8, v1

    if-gtz v1, :cond_3

    .line 359
    sget-object v1, Lcom/android/server/am/OomAdjusterImpl;->mRunningGameMap:Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;

    invoke-virtual {v1}, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;->size()I

    move-result v10

    .line 360
    .local v10, "gameCount":I
    sget-object v1, Lcom/android/server/am/OomAdjusterImpl;->mRunningGameMap:Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;->getIndexForKey(Ljava/lang/String;)I

    move-result v11

    .line 361
    .local v11, "gameIndex":I
    sget v1, Lcom/android/server/am/OomAdjusterImpl;->PREVIOUS_PROTECT_CRITICAL_COUNT:I

    if-le v10, v1, :cond_1

    if-ge v11, v1, :cond_2

    .line 363
    :cond_1
    const/4 v3, 0x0

    const/16 v4, 0x2bd

    const-string v6, "game-improve"

    move-object v1, p0

    move-object v2, v7

    move v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/android/server/am/OomAdjusterImpl;->modifyProcessRecordAdj(Lcom/android/server/am/ProcessStateRecord;ZIILjava/lang/String;)V

    .line 365
    const/4 v0, 0x1

    .line 367
    .end local v10    # "gameCount":I
    .end local v11    # "gameIndex":I
    :cond_2
    goto :goto_1

    :cond_3
    const-wide/32 v1, 0x124f80

    cmp-long v1, v8, v1

    if-gtz v1, :cond_4

    .line 369
    sget-object v1, Lcom/android/server/am/OomAdjusterImpl;->mRunningGameMap:Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;->getForKey(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 370
    .local v10, "gameScene":I
    packed-switch v10, :pswitch_data_0

    goto :goto_0

    .line 378
    :pswitch_0
    goto :goto_0

    .line 372
    :pswitch_1
    const/4 v3, 0x0

    const/16 v4, 0x2be

    const-string v6, "game-improve-download"

    move-object v1, p0

    move-object v2, v7

    move v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/android/server/am/OomAdjusterImpl;->modifyProcessRecordAdj(Lcom/android/server/am/ProcessStateRecord;ZIILjava/lang/String;)V

    .line 374
    const/4 v0, 0x1

    .line 375
    nop

    .line 382
    .end local v10    # "gameScene":I
    :goto_0
    goto :goto_1

    .line 383
    :cond_4
    sget-object v1, Lcom/android/server/am/OomAdjusterImpl;->mRunningGameMap:Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;->removeForKey(Ljava/lang/String;)V

    .line 386
    :cond_5
    :goto_1
    return v0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private computeOomAdjForPaymentScene(Lcom/android/server/am/ProcessRecord;I)Z
    .locals 9
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "procState"    # I

    .line 414
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v2}, Lcom/android/server/am/ProcessStateRecord;->getLastTopTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 415
    .local v0, "backgroundTime":J
    sget-boolean v2, Lcom/miui/app/smartpower/SmartPowerSettings;->GAME_PAY_PROTECT_ENABLED:Z

    if-eqz v2, :cond_0

    .line 416
    invoke-virtual {p0, p1}, Lcom/android/server/am/OomAdjusterImpl;->isPaymentScene(Lcom/android/server/am/ProcessRecord;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-wide/32 v2, 0x1d4c0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    .line 417
    iget-object v4, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    const/4 v5, 0x0

    const/16 v6, 0x190

    const-string v8, "game-improve-payment"

    move-object v3, p0

    move v7, p2

    invoke-direct/range {v3 .. v8}, Lcom/android/server/am/OomAdjusterImpl;->modifyProcessRecordAdj(Lcom/android/server/am/ProcessStateRecord;ZIILjava/lang/String;)V

    .line 419
    const/4 v2, 0x1

    return v2

    .line 421
    :cond_0
    const/4 v2, 0x0

    return v2
.end method

.method private computeOomAdjForResidentApp(Lcom/android/server/am/ProcessRecord;I)Z
    .locals 7
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "procState"    # I

    .line 318
    iget-object v0, p0, Lcom/android/server/am/OomAdjusterImpl;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    if-eqz v0, :cond_0

    .line 319
    invoke-interface {v0}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->updateAllAppUsageStats()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/am/OomAdjusterImpl;->updateResidentAppList(Ljava/util/List;)V

    .line 321
    :cond_0
    sget-object v0, Lcom/android/server/am/OomAdjusterImpl;->sPreviousResidentAppList:Ljava/util/List;

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 322
    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    const/4 v3, 0x0

    const/16 v4, 0x2da

    const-string v6, "resident-improveAdj"

    move-object v1, p0

    move v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/android/server/am/OomAdjusterImpl;->modifyProcessRecordAdj(Lcom/android/server/am/ProcessStateRecord;ZIILjava/lang/String;)V

    .line 324
    const/4 v0, 0x1

    return v0

    .line 326
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method private final computePreviousAdj(Lcom/android/server/am/ProcessRecord;II)Z
    .locals 10
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "procState"    # I
    .param p3, "adj"    # I

    .line 173
    sget-boolean v0, Lcom/miui/app/smartpower/SmartPowerPolicyConstants;->TESTSUITSPECIFIC:Z

    if-nez v0, :cond_4

    .line 174
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->hasActivities()Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    .line 175
    invoke-static {v0}, Landroid/os/Process;->isIsolated(I)Z

    move-result v0

    if-nez v0, :cond_4

    const/16 v0, 0x2bc

    if-le p3, v0, :cond_4

    .line 177
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    .line 178
    .local v0, "appState":Lcom/android/server/am/ProcessStateRecord;
    invoke-direct {p0, p1, p2}, Lcom/android/server/am/OomAdjusterImpl;->computeOomAdjForPaymentScene(Lcom/android/server/am/ProcessRecord;I)Z

    move-result v1

    const/4 v7, 0x1

    if-eqz v1, :cond_0

    .line 179
    return v7

    .line 182
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/android/server/am/OomAdjusterImpl;->computeOomAdjForGameApp(Lcom/android/server/am/ProcessRecord;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 183
    return v7

    .line 185
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getLastTopTime()J

    move-result-wide v3

    sub-long v8, v1, v3

    .line 186
    .local v8, "backgroundTime":J
    const-wide/32 v1, 0x493e0

    cmp-long v1, v8, v1

    if-gtz v1, :cond_3

    .line 187
    sget-boolean v1, Lcom/android/server/am/OomAdjusterImpl;->SCALE_BACKGROUND_APP_ADJ_ENABLE:Z

    if-eqz v1, :cond_2

    .line 188
    invoke-direct {p0, p1, p2}, Lcom/android/server/am/OomAdjusterImpl;->computeOomAdjForBackgroundApp(Lcom/android/server/am/ProcessRecord;I)Z

    .line 189
    return v7

    .line 191
    :cond_2
    const/4 v3, 0x0

    const/16 v4, 0x2be

    const-string v6, "previous-improve"

    move-object v1, p0

    move-object v2, v0

    move v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/android/server/am/OomAdjusterImpl;->modifyProcessRecordAdj(Lcom/android/server/am/ProcessStateRecord;ZIILjava/lang/String;)V

    .line 193
    return v7

    .line 195
    :cond_3
    sget-boolean v1, Lcom/android/server/am/OomAdjusterImpl;->IMPROVE_RESIDENT_APP_ADJ_ENABLE:Z

    if-eqz v1, :cond_4

    .line 196
    invoke-direct {p0, p1, p2}, Lcom/android/server/am/OomAdjusterImpl;->computeOomAdjForResidentApp(Lcom/android/server/am/ProcessRecord;I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 197
    return v7

    .line 200
    .end local v0    # "appState":Lcom/android/server/am/ProcessStateRecord;
    .end local v8    # "backgroundTime":J
    :cond_4
    const/4 v0, 0x0

    return v0
.end method

.method private final computeWidgetAdj(Lcom/android/server/am/ProcessRecord;I)Z
    .locals 10
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "adj"    # I

    .line 291
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    const-string v2, ":widgetProvider"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 292
    const-wide/16 v2, 0x0

    if-nez p2, :cond_1

    invoke-direct {p0, p1}, Lcom/android/server/am/OomAdjusterImpl;->isRunningWidgetBroadcast(Lcom/android/server/am/ProcessRecord;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 294
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getLastSwitchToTopTime()J

    move-result-wide v4

    cmp-long v0, v4, v2

    if-gtz v0, :cond_0

    .line 295
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/android/server/am/ProcessStateRecord;->setLastSwitchToTopTime(J)V

    .line 297
    return v1

    .line 298
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    .line 299
    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getLastSwitchToTopTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0xbb8

    cmp-long v0, v2, v4

    if-gez v0, :cond_2

    .line 301
    return v1

    .line 305
    :cond_1
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v0, v2, v3}, Lcom/android/server/am/ProcessStateRecord;->setLastSwitchToTopTime(J)V

    .line 308
    :cond_2
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    .line 309
    .local v0, "prcState":Lcom/android/server/am/ProcessStateRecord;
    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->isCached()Z

    move-result v6

    const/16 v7, 0x3e7

    const/16 v8, 0x13

    const-string/jumbo v9, "widget-degenerate"

    move-object v4, p0

    move-object v5, v0

    invoke-direct/range {v4 .. v9}, Lcom/android/server/am/OomAdjusterImpl;->modifyProcessRecordAdj(Lcom/android/server/am/ProcessStateRecord;ZIILjava/lang/String;)V

    .line 312
    const/4 v1, 0x1

    return v1

    .line 314
    .end local v0    # "prcState":Lcom/android/server/am/ProcessStateRecord;
    :cond_3
    return v1
.end method

.method private static getBackgroundAppCount()I
    .locals 6

    .line 113
    const-wide/32 v0, 0x40000000

    .line 114
    .local v0, "GB":J
    invoke-static {}, Landroid/os/Process;->getTotalMemory()J

    move-result-wide v2

    .line 115
    .local v2, "totalMemByte":J
    const-wide/16 v4, 0xc

    mul-long/2addr v4, v0

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    .line 116
    const/16 v4, 0xf

    return v4

    .line 117
    :cond_0
    const-wide/16 v4, 0x8

    mul-long/2addr v4, v0

    cmp-long v4, v2, v4

    if-lez v4, :cond_1

    .line 118
    const/16 v4, 0xa

    return v4

    .line 120
    :cond_1
    const/4 v4, 0x0

    return v4
.end method

.method public static getInstance()Lcom/android/server/am/OomAdjusterImpl;
    .locals 1

    .line 165
    invoke-static {}, Lcom/android/server/am/OomAdjusterStub;->getInstance()Lcom/android/server/am/OomAdjusterStub;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/OomAdjusterImpl;

    return-object v0
.end method

.method private static getResidentAppCount()I
    .locals 5

    .line 124
    sget-wide v0, Lcom/android/server/am/OomAdjusterImpl;->TOTAL_MEMORY:J

    const-wide/16 v2, 0x6

    cmp-long v2, v0, v2

    const-wide/16 v3, 0x8

    if-lez v2, :cond_0

    cmp-long v2, v0, v3

    if-gtz v2, :cond_0

    .line 125
    sget v0, Lcom/android/server/am/OomAdjusterImpl;->LOW_MEMORY_RESIDENT_APP_COUNT:I

    return v0

    .line 126
    :cond_0
    cmp-long v2, v0, v3

    const-wide/16 v3, 0xc

    if-lez v2, :cond_1

    cmp-long v2, v0, v3

    if-gtz v2, :cond_1

    .line 127
    sget v0, Lcom/android/server/am/OomAdjusterImpl;->MIDDLE_MEMORY_RESIDENT_APP_COUNT:I

    return v0

    .line 128
    :cond_1
    cmp-long v0, v0, v3

    if-lez v0, :cond_2

    .line 129
    sget v0, Lcom/android/server/am/OomAdjusterImpl;->HIGH_MEMORY_RESIDENT_APP_COUNT:I

    return v0

    .line 131
    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method private improveOomAdjForAudioProcess(Lcom/android/server/am/ProcessRecord;)V
    .locals 4
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 390
    sget-object v0, Lcom/android/server/am/OomAdjusterImpl;->QQ_PLAYER:Ljava/lang/String;

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 391
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    .line 392
    .local v0, "prcState":Lcom/android/server/am/ProcessStateRecord;
    invoke-static {}, Lcom/android/server/am/SystemPressureController;->getInstance()Lcom/android/server/am/SystemPressureController;

    move-result-object v1

    iget v2, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/android/server/am/SystemPressureController;->isAudioOrGPSProc(II)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->mServices:Lcom/android/server/am/ProcessServiceRecord;

    .line 393
    invoke-virtual {v1}, Lcom/android/server/am/ProcessServiceRecord;->hasForegroundServices()Z

    move-result v1

    if-nez v1, :cond_0

    .line 394
    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessStateRecord;->setMaxAdj(I)V

    goto :goto_0

    .line 396
    :cond_0
    sget v1, Lmiui/process/ProcessManager;->LOCKED_MAX_ADJ:I

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessStateRecord;->setMaxAdj(I)V

    .line 399
    .end local v0    # "prcState":Lcom/android/server/am/ProcessStateRecord;
    :cond_1
    :goto_0
    return-void
.end method

.method private improveOomAdjForCamera(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;II)Z
    .locals 7
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "topApp"    # Lcom/android/server/am/ProcessRecord;
    .param p3, "adj"    # I
    .param p4, "procState"    # I

    .line 331
    if-eqz p2, :cond_0

    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/16 v0, 0x190

    if-le p3, v0, :cond_0

    if-eq p1, p2, :cond_0

    .line 334
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->hasActivities()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 335
    const-string v1, "com.android.camera"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/server/am/OomAdjusterImpl;->mForegroundResultToProc:Ljava/lang/String;

    .line 336
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337
    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->isCached()Z

    move-result v3

    const/16 v4, 0x190

    const/16 v5, 0xd

    const-string v6, "camera-improve"

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/android/server/am/OomAdjusterImpl;->modifyProcessRecordAdj(Lcom/android/server/am/ProcessStateRecord;ZIILjava/lang/String;)V

    .line 340
    const/4 v0, 0x1

    return v0

    .line 342
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public static isCacheProcessState(I)Z
    .locals 1
    .param p0, "procState"    # I

    .line 595
    const/16 v0, 0x10

    if-eq p0, v0, :cond_1

    const/16 v0, 0x11

    if-eq p0, v0, :cond_1

    const/16 v0, 0x12

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private isProcessPerceptible(ILjava/lang/String;)Z
    .locals 1
    .param p1, "uid"    # I
    .param p2, "processName"    # Ljava/lang/String;

    .line 760
    iget-object v0, p0, Lcom/android/server/am/OomAdjusterImpl;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    if-eqz v0, :cond_0

    .line 761
    invoke-interface {v0, p1, p2}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->isProcessPerceptible(ILjava/lang/String;)Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 760
    :goto_0
    return v0
.end method

.method private isRunningWidgetBroadcast(Lcom/android/server/am/ProcessRecord;)Z
    .locals 4
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 278
    invoke-static {}, Lcom/android/server/am/ActivityManagerServiceImpl;->getInstance()Lcom/android/server/am/ActivityManagerServiceImpl;

    move-result-object v0

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    iget v2, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    .line 279
    invoke-virtual {v0, v1, v2}, Lcom/android/server/am/ActivityManagerServiceImpl;->getBroadcastProcessQueue(Ljava/lang/String;I)Lcom/android/server/am/BroadcastProcessQueue;

    move-result-object v0

    .line 280
    .local v0, "brcProcessQueue":Lcom/android/server/am/BroadcastProcessQueue;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/server/am/BroadcastProcessQueue;->isActive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 281
    invoke-virtual {v0}, Lcom/android/server/am/BroadcastProcessQueue;->getActive()Lcom/android/server/am/BroadcastRecord;

    move-result-object v1

    .line 282
    .local v1, "brc":Lcom/android/server/am/BroadcastRecord;
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/android/server/am/OomAdjusterImpl;->mWidgetProcBCWhiteList:Landroid/util/ArraySet;

    iget-object v3, v1, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    .line 283
    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 284
    const/4 v2, 0x1

    return v2

    .line 287
    .end local v1    # "brc":Lcom/android/server/am/BroadcastRecord;
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method private isSpecialApp(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 263
    const-string v0, "com.android.camera"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "com.tencent.mm"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 264
    invoke-static {}, Lcom/android/server/am/SystemPressureController;->getInstance()Lcom/android/server/am/SystemPressureController;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/am/SystemPressureController;->isGameApp(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 263
    :goto_1
    return v0
.end method

.method private modifyProcessRecordAdj(Lcom/android/server/am/ProcessStateRecord;ZIILjava/lang/String;)V
    .locals 0
    .param p1, "prcState"    # Lcom/android/server/am/ProcessStateRecord;
    .param p2, "cached"    # Z
    .param p3, "CurRawAdj"    # I
    .param p4, "procState"    # I
    .param p5, "AdjType"    # Ljava/lang/String;

    .line 426
    invoke-virtual {p1, p2}, Lcom/android/server/am/ProcessStateRecord;->setCached(Z)V

    .line 427
    invoke-virtual {p1, p3}, Lcom/android/server/am/ProcessStateRecord;->setCurRawAdj(I)V

    .line 428
    invoke-virtual {p1, p4}, Lcom/android/server/am/ProcessStateRecord;->setCurRawProcState(I)V

    .line 429
    invoke-virtual {p1, p5}, Lcom/android/server/am/ProcessStateRecord;->setAdjType(Ljava/lang/String;)V

    .line 430
    return-void
.end method

.method private updateBackgroundAppList()V
    .locals 7

    .line 221
    iget-object v0, p0, Lcom/android/server/am/OomAdjusterImpl;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-interface {v0}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->getLruProcesses()Ljava/util/ArrayList;

    move-result-object v0

    .line 222
    .local v0, "runningAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 223
    .local v1, "backgroundRunningAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 224
    .local v3, "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 225
    invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->hasActivity()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 226
    invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 228
    .end local v3    # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    :cond_0
    goto :goto_0

    .line 229
    :cond_1
    iget-object v2, p0, Lcom/android/server/am/OomAdjusterImpl;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-interface {v2}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->getAllAppState()Ljava/util/ArrayList;

    move-result-object v2

    .line 230
    .local v2, "AppStateList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/IAppState;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 231
    .local v3, "backgroundAppStateList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/IAppState;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/miui/server/smartpower/IAppState;

    .line 232
    .local v5, "appState":Lcom/miui/server/smartpower/IAppState;
    invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 233
    invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState;->isSystemApp()Z

    move-result v6

    if-nez v6, :cond_2

    .line 234
    invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/android/server/am/OomAdjusterImpl;->isSpecialApp(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 235
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 237
    .end local v5    # "appState":Lcom/miui/server/smartpower/IAppState;
    :cond_2
    goto :goto_1

    .line 238
    :cond_3
    new-instance v4, Lcom/android/server/am/OomAdjusterImpl$1;

    invoke-direct {v4, p0}, Lcom/android/server/am/OomAdjusterImpl$1;-><init>(Lcom/android/server/am/OomAdjusterImpl;)V

    invoke-static {v3, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 252
    sget-object v4, Lcom/android/server/am/OomAdjusterImpl;->sPreviousBackgroundAppList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 253
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_5

    .line 254
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/miui/server/smartpower/IAppState;

    invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 255
    .local v5, "theProcess":Ljava/lang/String;
    sget-object v6, Lcom/android/server/am/OomAdjusterImpl;->sPreviousBackgroundAppList:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 256
    goto :goto_3

    .line 258
    :cond_4
    sget-object v6, Lcom/android/server/am/OomAdjusterImpl;->sPreviousBackgroundAppList:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 253
    .end local v5    # "theProcess":Ljava/lang/String;
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 260
    .end local v4    # "i":I
    :cond_5
    return-void
.end method


# virtual methods
.method public applyOomAdjLocked(Lcom/android/server/am/ProcessRecord;)V
    .locals 1
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 556
    iget-object v0, p0, Lcom/android/server/am/OomAdjusterImpl;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    if-eqz v0, :cond_0

    .line 557
    invoke-interface {v0, p1}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->applyOomAdjLocked(Lcom/android/server/am/ProcessRecord;)V

    .line 559
    :cond_0
    return-void
.end method

.method public compactBackgroundProcess(Lcom/android/server/am/ProcessRecord;)V
    .locals 6
    .param p1, "proc"    # Lcom/android/server/am/ProcessRecord;

    .line 602
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    .line 603
    .local v0, "state":Lcom/android/server/am/ProcessStateRecord;
    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getCurProcState()I

    move-result v1

    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getSetProcState()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 604
    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getSetProcState()I

    move-result v1

    .line 605
    .local v1, "oldState":I
    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getCurProcState()I

    move-result v2

    .line 606
    .local v2, "newState":I
    invoke-static {v1}, Lcom/android/server/am/OomAdjusterImpl;->isCacheProcessState(I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v2}, Lcom/android/server/am/OomAdjusterImpl;->isCacheProcessState(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 607
    invoke-static {}, Lcom/android/server/am/SystemPressureController;->getInstance()Lcom/android/server/am/SystemPressureController;

    move-result-object v3

    iget v4, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    iget-object v5, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/android/server/am/SystemPressureController;->compactBackgroundProcess(ILjava/lang/String;)V

    .line 611
    .end local v1    # "oldState":I
    .end local v2    # "newState":I
    :cond_0
    return-void
.end method

.method public computeBindServiceAdj(Lcom/android/server/am/ProcessRecord;ILcom/android/server/am/ConnectionRecord;)I
    .locals 15
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "adj"    # I
    .param p3, "connectService"    # Lcom/android/server/am/ConnectionRecord;

    .line 563
    move-object/from16 v0, p1

    move-object/from16 v1, p3

    iget-object v2, v1, Lcom/android/server/am/ConnectionRecord;->binding:Lcom/android/server/am/AppBindRecord;

    iget-object v2, v2, Lcom/android/server/am/AppBindRecord;->client:Lcom/android/server/am/ProcessRecord;

    .line 564
    .local v2, "client":Lcom/android/server/am/ProcessRecord;
    iget-object v3, v2, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    .line 565
    .local v3, "cstate":Lcom/android/server/am/ProcessStateRecord;
    iget-object v4, v0, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-static {v4}, Lcom/miui/server/process/ProcessManagerInternal;->checkCtsProcess(Ljava/lang/String;)Z

    move-result v4

    const/16 v5, 0x64

    if-nez v4, :cond_8

    sget-boolean v4, Lcom/android/server/am/OomAdjusterImpl;->LIMIT_BIND_VEISIBLE_ENABLED:Z

    if-nez v4, :cond_0

    move-object v9, p0

    goto :goto_3

    .line 570
    :cond_0
    invoke-virtual {v3}, Lcom/android/server/am/ProcessStateRecord;->getCurRawAdj()I

    move-result v4

    .line 571
    .local v4, "clientAdj":I
    iget-object v6, v0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget v6, v6, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit16 v6, v6, 0x81

    if-eqz v6, :cond_1

    const/4 v6, 0x1

    goto :goto_0

    :cond_1
    const/4 v6, 0x0

    .line 573
    .local v6, "isSystem":Z
    :goto_0
    iget-wide v7, v1, Lcom/android/server/am/ConnectionRecord;->flags:J

    const-wide/32 v9, 0x6000000

    and-long/2addr v7, v9

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    const/16 v8, 0xc8

    if-nez v7, :cond_6

    if-gez v4, :cond_2

    move-object v9, p0

    goto :goto_1

    .line 583
    :cond_2
    iget-wide v11, v1, Lcom/android/server/am/ConnectionRecord;->flags:J

    const-wide/16 v13, 0x1

    and-long/2addr v11, v13

    cmp-long v5, v11, v9

    if-eqz v5, :cond_4

    if-ge v4, v8, :cond_4

    iget v5, v2, Lcom/android/server/am/ProcessRecord;->uid:I

    iget-object v7, v2, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 585
    move-object v9, p0

    invoke-direct {p0, v5, v7}, Lcom/android/server/am/OomAdjusterImpl;->isProcessPerceptible(ILjava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    sget-boolean v5, Lcom/miui/app/smartpower/SmartPowerPolicyConstants;->TESTSUITSPECIFIC:Z

    if-eqz v5, :cond_5

    .line 588
    :cond_3
    const/16 v5, 0xfa

    .end local p2    # "adj":I
    .local v5, "adj":I
    goto :goto_2

    .line 583
    .end local v5    # "adj":I
    .restart local p2    # "adj":I
    :cond_4
    move-object v9, p0

    .line 591
    :cond_5
    move/from16 v5, p2

    goto :goto_2

    .line 573
    :cond_6
    move-object v9, p0

    .line 578
    :goto_1
    if-eqz v6, :cond_7

    .line 579
    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    .end local p2    # "adj":I
    .restart local v5    # "adj":I
    goto :goto_2

    .line 581
    .end local v5    # "adj":I
    .restart local p2    # "adj":I
    :cond_7
    invoke-static {v4, v8}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 591
    .end local p2    # "adj":I
    .restart local v5    # "adj":I
    :goto_2
    return v5

    .line 565
    .end local v4    # "clientAdj":I
    .end local v5    # "adj":I
    .end local v6    # "isSystem":Z
    .restart local p2    # "adj":I
    :cond_8
    move-object v9, p0

    .line 567
    :goto_3
    invoke-virtual {v3}, Lcom/android/server/am/ProcessStateRecord;->getCurRawAdj()I

    move-result v4

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    return v4
.end method

.method public computeOomAdjLocked(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;IIZ)Z
    .locals 5
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "topApp"    # Lcom/android/server/am/ProcessRecord;
    .param p3, "adj"    # I
    .param p4, "procState"    # I
    .param p5, "cycleReEval"    # Z

    .line 435
    sget-boolean v0, Lcom/android/server/am/ActivityManagerDebugConfig;->DEBUG_OOM_ADJ:Z

    if-eqz v0, :cond_1

    .line 436
    if-ne p1, p2, :cond_0

    const-string/jumbo v0, "true"

    goto :goto_0

    :cond_0
    const-string v0, "false"

    .line 437
    .local v0, "isTop":Ljava/lang/String;
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "computeOomAdjLocked processName = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " rawAdj="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " maxAdj="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    .line 439
    invoke-virtual {v2}, Lcom/android/server/am/ProcessStateRecord;->getMaxAdj()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " procState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " maxProcState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    iget v2, v2, Lcom/android/server/am/ProcessStateRecord;->mMaxProcState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " adjType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    .line 442
    invoke-virtual {v2}, Lcom/android/server/am/ProcessStateRecord;->getAdjType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " isTop="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cycleReEval="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 437
    const-string v2, "ActivityManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 446
    .end local v0    # "isTop":Ljava/lang/String;
    :cond_1
    const/4 v0, 0x0

    if-gez p3, :cond_2

    .line 447
    return v0

    .line 450
    :cond_2
    iget-object v1, p0, Lcom/android/server/am/OomAdjusterImpl;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    const/4 v2, 0x1

    if-eqz v1, :cond_3

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    const/16 v3, 0x2710

    if-le v1, v3, :cond_3

    const/16 v1, 0x2bc

    if-le p3, v1, :cond_3

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 452
    invoke-virtual {p0, v1, v3}, Lcom/android/server/am/OomAdjusterImpl;->isImportantSubProc(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 453
    iget-object v1, p0, Lcom/android/server/am/OomAdjusterImpl;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-interface {v1, v3}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->getAppState(I)Lcom/miui/server/smartpower/IAppState;

    move-result-object v1

    .line 454
    .local v1, "appState":Lcom/miui/server/smartpower/IAppState;
    if-eqz v1, :cond_3

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/miui/server/smartpower/IAppState;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 455
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v1}, Lcom/miui/server/smartpower/IAppState;->getMainProcAdj()I

    move-result v3

    const/16 v4, 0xc8

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/android/server/am/ProcessStateRecord;->setCurRawAdj(I)V

    .line 457
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    const-string v3, "bind-fix"

    invoke-virtual {v0, v3}, Lcom/android/server/am/ProcessStateRecord;->setAdjType(Ljava/lang/String;)V

    .line 458
    return v2

    .line 461
    .end local v1    # "appState":Lcom/miui/server/smartpower/IAppState;
    :cond_3
    invoke-direct {p0, p1, p3}, Lcom/android/server/am/OomAdjusterImpl;->computeWidgetAdj(Lcom/android/server/am/ProcessRecord;I)Z

    move-result v1

    if-nez v1, :cond_4

    .line 462
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/am/OomAdjusterImpl;->improveOomAdjForCamera(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;II)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_4
    move v0, v2

    .line 463
    .local v0, "isChangeAdj":Z
    :cond_5
    if-eqz v0, :cond_6

    .line 464
    return v2

    .line 466
    :cond_6
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isKilled()Z

    move-result v1

    if-nez v1, :cond_9

    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isKilledByAm()Z

    move-result v1

    if-nez v1, :cond_9

    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v1

    if-eqz v1, :cond_9

    if-lez p3, :cond_9

    .line 468
    packed-switch p4, :pswitch_data_0

    :pswitch_0
    goto :goto_1

    .line 480
    :pswitch_1
    const/16 v1, 0x320

    if-le p3, v1, :cond_8

    .line 481
    invoke-static {}, Lcom/android/server/am/ProcessProphetStub;->getInstance()Lcom/android/server/am/ProcessProphetStub;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/android/server/am/ProcessProphetStub;->isNeedProtect(Lcom/android/server/am/ProcessRecord;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 482
    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v2, v1}, Lcom/android/server/am/ProcessStateRecord;->setCurRawAdj(I)V

    .line 483
    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    const-string v2, "proc-prophet"

    invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessStateRecord;->setAdjType(Ljava/lang/String;)V

    .line 484
    const/4 v0, 0x1

    goto :goto_1

    .line 474
    :pswitch_2
    const/16 v1, 0x3e9

    if-ge p3, v1, :cond_7

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->mServices:Lcom/android/server/am/ProcessServiceRecord;

    .line 475
    invoke-virtual {v1}, Lcom/android/server/am/ProcessServiceRecord;->hasAboveClient()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 476
    :cond_7
    invoke-direct {p0, p1, p4, p3}, Lcom/android/server/am/OomAdjusterImpl;->computePreviousAdj(Lcom/android/server/am/ProcessRecord;II)Z

    move-result v0

    .line 490
    :cond_8
    :goto_1
    invoke-direct {p0, p1}, Lcom/android/server/am/OomAdjusterImpl;->improveOomAdjForAudioProcess(Lcom/android/server/am/ProcessRecord;)V

    .line 492
    :cond_9
    return v0

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public dumpPreviousBackgroundApps(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 2
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "prefix"    # Ljava/lang/String;

    .line 268
    sget-object v0, Lcom/android/server/am/OomAdjusterImpl;->sPreviousBackgroundAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 269
    const-string v0, "BGAL:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 270
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/android/server/am/OomAdjusterImpl;->sPreviousBackgroundAppList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 271
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 272
    sget-object v1, Lcom/android/server/am/OomAdjusterImpl;->sPreviousBackgroundAppList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 270
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 275
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method public dumpPreviousResidentApps(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 2
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "prefix"    # Ljava/lang/String;

    .line 528
    sget-object v0, Lcom/android/server/am/OomAdjusterImpl;->sPreviousResidentAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 529
    const-string v0, "PRAL:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 530
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/android/server/am/OomAdjusterImpl;->sPreviousResidentAppList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 531
    invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 532
    sget-object v1, Lcom/android/server/am/OomAdjusterImpl;->sPreviousResidentAppList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 530
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 535
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method public foregroundInfoChanged(Ljava/lang/String;Landroid/content/ComponentName;Ljava/lang/String;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "component"    # Landroid/content/ComponentName;
    .param p3, "resultToProc"    # Ljava/lang/String;

    .line 689
    iget-object v0, p0, Lcom/android/server/am/OomAdjusterImpl;->mForegroundPkg:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 690
    invoke-static {}, Lcom/android/server/am/SystemPressureController;->getInstance()Lcom/android/server/am/SystemPressureController;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/am/SystemPressureController;->isGameApp(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 691
    sget-object v0, Lcom/android/server/am/OomAdjusterImpl;->mRunningGameMap:Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;->put(ILjava/lang/String;Ljava/lang/Integer;)V

    .line 693
    :cond_0
    iput-object p1, p0, Lcom/android/server/am/OomAdjusterImpl;->mForegroundPkg:Ljava/lang/String;

    .line 694
    iput-object p3, p0, Lcom/android/server/am/OomAdjusterImpl;->mForegroundResultToProc:Ljava/lang/String;

    .line 696
    :cond_1
    return-void
.end method

.method public isImportantSubProc(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "procName"    # Ljava/lang/String;

    .line 538
    sget-object v0, Lcom/android/server/am/OomAdjusterImpl;->sSubProcessAdjBindList:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 539
    .local v0, "procList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 540
    const/4 v1, 0x1

    return v1

    .line 542
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public isPaymentScene(Lcom/android/server/am/ProcessRecord;)Z
    .locals 3
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 402
    sget-object v0, Lcom/android/server/am/OomAdjusterImpl;->sPaymentAppList:Ljava/util/List;

    iget-object v1, p0, Lcom/android/server/am/OomAdjusterImpl;->mForegroundPkg:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/am/OomAdjusterImpl;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 403
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->isProcessInTaskStack(II)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/am/OomAdjusterImpl;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    .line 404
    const-string v1, "com.xiaomi.gamecenter.sdk.service"

    invoke-interface {v0, v1}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->isProcessInTaskStack(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 405
    const/4 v0, 0x1

    return v0

    .line 407
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isSetUntrustedBgGroup(Lcom/android/server/am/ProcessRecord;)Z
    .locals 1
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 618
    const/4 v0, 0x0

    return v0
.end method

.method public notifyProcessDied(Lcom/android/server/am/ProcessRecord;)V
    .locals 2
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 699
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 700
    sget-object v0, Lcom/android/server/am/OomAdjusterImpl;->mRunningGameMap:Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;->removeForKey(Ljava/lang/String;)V

    .line 702
    :cond_0
    return-void
.end method

.method public onSystemReady()V
    .locals 1

    .line 169
    const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    iput-object v0, p0, Lcom/android/server/am/OomAdjusterImpl;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    .line 170
    return-void
.end method

.method public resetProperThreadPriority(Lcom/android/server/am/ProcessRecord;II)V
    .locals 3
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "tid"    # I
    .param p3, "prio"    # I

    .line 650
    invoke-static {}, Lcom/android/server/wm/RealTimeModeControllerImpl;->get()Lcom/android/server/wm/RealTimeModeControllerImpl;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/server/wm/RealTimeModeControllerImpl;->checkThreadBoost(I)Z

    move-result v0

    .line 651
    .local v0, "boosted":Z
    if-eqz p2, :cond_0

    if-nez v0, :cond_0

    .line 652
    invoke-super {p0, p1, p2, p3}, Lcom/android/server/am/OomAdjusterStub;->resetProperThreadPriority(Lcom/android/server/am/ProcessRecord;II)V

    goto :goto_0

    .line 654
    :cond_0
    invoke-static {}, Lcom/android/server/wm/RealTimeModeControllerImpl;->get()Lcom/android/server/wm/RealTimeModeControllerImpl;

    move-result-object v1

    filled-new-array {p2}, [I

    move-result-object v2

    .line 655
    invoke-virtual {v1, v2, p3}, Lcom/android/server/wm/RealTimeModeControllerImpl;->setThreadSavedPriority([II)V

    .line 656
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " boosting, skip reset priority"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SchedBoost"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 659
    :goto_0
    return-void
.end method

.method public setProperThreadPriority(Lcom/android/server/am/ProcessRecord;III)V
    .locals 3
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "pid"    # I
    .param p3, "renderThreadTid"    # I
    .param p4, "prio"    # I

    .line 636
    invoke-static {}, Lcom/android/server/wm/RealTimeModeControllerImpl;->get()Lcom/android/server/wm/RealTimeModeControllerImpl;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/server/wm/RealTimeModeControllerImpl;->checkThreadBoost(I)Z

    move-result v0

    .line 637
    .local v0, "boosted":Z
    if-nez v0, :cond_0

    .line 638
    invoke-super {p0, p1, p2, p3, p4}, Lcom/android/server/am/OomAdjusterStub;->setProperThreadPriority(Lcom/android/server/am/ProcessRecord;III)V

    goto :goto_0

    .line 640
    :cond_0
    invoke-static {}, Lcom/android/server/wm/RealTimeModeControllerImpl;->get()Lcom/android/server/wm/RealTimeModeControllerImpl;

    move-result-object v1

    filled-new-array {p2, p3}, [I

    move-result-object v2

    invoke-virtual {v1, v2, p4}, Lcom/android/server/wm/RealTimeModeControllerImpl;->setThreadSavedPriority([II)V

    .line 642
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " already boosted, skip boost priority"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SchedBoost"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 645
    :goto_0
    return-void
.end method

.method public shouldSkipDueToBind(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;)Z
    .locals 2
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "client"    # Lcom/android/server/am/ProcessRecord;

    .line 547
    iget-object v0, p2, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v1, p2, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/android/server/am/OomAdjusterImpl;->isImportantSubProc(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 548
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 549
    const/4 v0, 0x1

    return v0

    .line 551
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public updateGameSceneRecordMap(Ljava/lang/String;II)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "gameScene"    # I
    .param p3, "appState"    # I

    .line 622
    packed-switch p3, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 625
    :pswitch_1
    sget-object v0, Lcom/android/server/am/OomAdjusterImpl;->mRunningGameMap:Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/android/server/am/OomAdjusterImpl$RunningAppRecordMap;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 626
    nop

    .line 630
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public updateProcessAdjBindList(Ljava/lang/String;)V
    .locals 8
    .param p1, "data"    # Ljava/lang/String;

    .line 663
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 664
    sget-object v0, Lcom/android/server/am/OomAdjusterImpl;->sSubProcessAdjBindList:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 665
    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 666
    .local v0, "pkgString":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_3

    .line 667
    aget-object v2, v0, v1

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 668
    aget-object v2, v0, v1

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 669
    .local v2, "appString":[Ljava/lang/String;
    array-length v3, v2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    const/4 v3, 0x0

    aget-object v4, v2, v3

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    const/4 v4, 0x1

    aget-object v5, v2, v4

    .line 670
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 671
    aget-object v4, v2, v4

    const-string v5, "\\+"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 672
    .local v4, "procString":[Ljava/lang/String;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 673
    .local v5, "procList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_1
    array-length v7, v4

    if-ge v6, v7, :cond_1

    .line 674
    aget-object v7, v4, v6

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 675
    aget-object v7, v4, v6

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 673
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 678
    .end local v6    # "j":I
    :cond_1
    sget-object v6, Lcom/android/server/am/OomAdjusterImpl;->sSubProcessAdjBindList:Ljava/util/Map;

    aget-object v3, v2, v3

    invoke-interface {v6, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 666
    .end local v2    # "appString":[Ljava/lang/String;
    .end local v4    # "procString":[Ljava/lang/String;
    .end local v5    # "procList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 682
    .end local v1    # "i":I
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "subprocess adj bind list cloud control received: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ProcCloudControl"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 685
    .end local v0    # "pkgString":[Ljava/lang/String;
    :cond_4
    return-void
.end method

.method public updateResidentAppList(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/miui/server/smartpower/IAppState;",
            ">;)V"
        }
    .end annotation

    .line 496
    .local p1, "appList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/IAppState;>;"
    if-nez p1, :cond_0

    .line 497
    return-void

    .line 499
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 500
    .local v0, "residentList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/IAppState;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/IAppState;

    .line 501
    .local v2, "appState":Lcom/miui/server/smartpower/IAppState;
    invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState;->isSystemApp()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/server/am/OomAdjusterImpl;->isSpecialApp(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 502
    invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState;->hasActivityApp()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 503
    invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState;->getLaunchCount()I

    move-result v3

    const/16 v4, 0x15

    if-le v3, v4, :cond_1

    .line 504
    invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState;->getTotalTimeInForeground()J

    move-result-wide v3

    const-wide/32 v5, 0x6ddd00

    cmp-long v3, v3, v5

    if-lez v3, :cond_1

    .line 505
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 507
    .end local v2    # "appState":Lcom/miui/server/smartpower/IAppState;
    :cond_1
    goto :goto_0

    .line 508
    :cond_2
    new-instance v1, Lcom/android/server/am/OomAdjusterImpl$2;

    invoke-direct {v1, p0}, Lcom/android/server/am/OomAdjusterImpl$2;-><init>(Lcom/android/server/am/OomAdjusterImpl;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 521
    sget-object v1, Lcom/android/server/am/OomAdjusterImpl;->sPreviousResidentAppList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 522
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    sget v2, Lcom/android/server/am/OomAdjusterImpl;->RESIDENT_APP_COUNT_LIMIT:I

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 523
    sget-object v2, Lcom/android/server/am/OomAdjusterImpl;->sPreviousResidentAppList:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/smartpower/IAppState;

    invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 522
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 525
    .end local v1    # "i":I
    :cond_3
    return-void
.end method
