.class public abstract Lcom/android/server/am/ProcessCleanerBase;
.super Ljava/lang/Object;
.source "ProcessCleanerBase.java"


# static fields
.field private static final DESK_CLOCK_PROCESS_NAME:Ljava/lang/String; = "com.android.deskclock"

.field private static final HOME_PROCESS_NAME:Ljava/lang/String; = "com.miui.home"

.field private static final RADIO_PROCESS_NAME:Ljava/lang/String; = "com.miui.fmservice:remote"

.field private static final RADIO_TURN_OFF_INTENT:Ljava/lang/String; = "miui.intent.action.TURN_OFF"

.field public static final SMART_POWER_PROTECT_APP_FLAGS:I = 0x1d8

.field private static final TAG:Ljava/lang/String; = "ProcessCleanerBase"


# instance fields
.field protected mAMS:Lcom/android/server/am/ActivityManagerService;

.field protected mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;


# direct methods
.method public constructor <init>(Lcom/android/server/am/ActivityManagerService;)V
    .locals 0
    .param p1, "ams"    # Lcom/android/server/am/ActivityManagerService;

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/android/server/am/ProcessCleanerBase;->mAMS:Lcom/android/server/am/ActivityManagerService;

    .line 50
    return-void
.end method

.method public static isSystemApp(Lcom/android/server/am/ProcessRecord;)Z
    .locals 3
    .param p0, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 57
    const/4 v0, 0x0

    if-eqz p0, :cond_2

    iget-object v1, p0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    if-eqz v1, :cond_2

    .line 58
    iget-object v1, p0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit16 v1, v1, 0x81

    if-nez v1, :cond_0

    const/16 v1, 0x3e8

    iget v2, p0, Lcom/android/server/am/ProcessRecord;->uid:I

    if-ne v1, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0

    .line 62
    :cond_2
    return v0
.end method

.method private printKillLog(Lcom/android/server/am/ProcessRecord;ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "killLevel"    # I
    .param p3, "reason"    # Ljava/lang/String;
    .param p4, "killTag"    # Ljava/lang/String;

    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p2}, Lcom/android/server/am/ProcessCleanerBase;->killLevelToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 138
    const/16 v1, 0x68

    if-ne p2, v1, :cond_0

    .line 139
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " pkgName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " procName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 140
    .local v0, "levelString":Ljava/lang/String;
    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    .line 141
    invoke-virtual {v2}, Lcom/android/server/am/ProcessStateRecord;->getCurProcState()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    filled-new-array {v1, v2}, [Ljava/lang/Object;

    move-result-object v1

    .line 140
    const-string v2, "AS:%d%d"

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 142
    .local v1, "info":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " info="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p4, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    return-void
.end method


# virtual methods
.method checkKillFMApp(Lcom/android/server/am/ProcessRecord;Landroid/os/Handler;Landroid/content/Context;)Z
    .locals 2
    .param p1, "proc"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "context"    # Landroid/content/Context;

    .line 146
    if-eqz p2, :cond_0

    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    const-string v1, "com.miui.fmservice:remote"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.TURN_OFF"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 148
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 149
    new-instance v1, Lcom/android/server/am/ProcessCleanerBase$1;

    invoke-direct {v1, p0, p3, v0, p1}, Lcom/android/server/am/ProcessCleanerBase$1;-><init>(Lcom/android/server/am/ProcessCleanerBase;Landroid/content/Context;Landroid/content/Intent;Lcom/android/server/am/ProcessRecord;)V

    invoke-virtual {p2, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 155
    const/4 v1, 0x1

    return v1

    .line 157
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method protected checkProcessDied(Lcom/android/server/am/ProcessRecord;)Z
    .locals 2
    .param p1, "proc"    # Lcom/android/server/am/ProcessRecord;

    .line 131
    iget-object v0, p0, Lcom/android/server/am/ProcessCleanerBase;->mAMS:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v0

    .line 132
    if-eqz p1, :cond_1

    :try_start_0
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isKilled()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 133
    :catchall_0
    move-exception v1

    goto :goto_2

    .line 132
    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    monitor-exit v0

    return v1

    .line 133
    :goto_2
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I
    .param p3, "reason"    # Ljava/lang/String;

    .line 262
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "forceStopPackage pck:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " r:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-wide/16 v1, 0x40

    invoke-static {v1, v2, v0}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 264
    const-class v0, Landroid/app/ActivityManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManagerInternal;

    .line 265
    invoke-virtual {v0, p1, p2, p3}, Landroid/app/ActivityManagerInternal;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V

    .line 266
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    .line 267
    return-void
.end method

.method getKillReason(I)Ljava/lang/String;
    .locals 1
    .param p1, "policy"    # I

    .line 330
    const/4 v0, 0x0

    .line 331
    .local v0, "reason":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 381
    :pswitch_0
    const-string v0, "Unknown"

    goto :goto_0

    .line 351
    :pswitch_1
    const-string v0, "ScreenOffCPUCheckKill"

    .line 352
    goto :goto_0

    .line 336
    :pswitch_2
    const-string v0, "AutoThermalKillAll2"

    .line 337
    goto :goto_0

    .line 333
    :pswitch_3
    const-string v0, "AutoThermalKillAll1"

    .line 334
    goto :goto_0

    .line 354
    :pswitch_4
    const-string v0, "AutoSystemAbnormalClean"

    .line 355
    goto :goto_0

    .line 348
    :pswitch_5
    const-string v0, "AutoSleepClean"

    .line 349
    goto :goto_0

    .line 345
    :pswitch_6
    const-string v0, "AutoIdleKill"

    .line 346
    goto :goto_0

    .line 342
    :pswitch_7
    const-string v0, "AutoThermalKill"

    .line 343
    goto :goto_0

    .line 339
    :pswitch_8
    const-string v0, "AutoPowerKill"

    .line 340
    goto :goto_0

    .line 378
    :pswitch_9
    const-string v0, "UserDefined"

    .line 379
    goto :goto_0

    .line 360
    :pswitch_a
    const-string v0, "SwipeUpClean"

    .line 361
    goto :goto_0

    .line 375
    :pswitch_b
    const-string v0, "GarbageClean"

    .line 376
    goto :goto_0

    .line 366
    :pswitch_c
    const-string v0, "OptimizationClean"

    .line 367
    goto :goto_0

    .line 363
    :pswitch_d
    const-string v0, "GameClean"

    .line 364
    goto :goto_0

    .line 372
    :pswitch_e
    const-string v0, "LockScreenClean"

    .line 373
    goto :goto_0

    .line 369
    :pswitch_f
    const-string v0, "ForceClean"

    .line 370
    goto :goto_0

    .line 357
    :pswitch_10
    const-string v0, "OneKeyClean"

    .line 358
    nop

    .line 383
    :goto_0
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method getProcessPolicyWhiteList(Lmiui/process/ProcessConfig;Lcom/android/server/am/ProcessPolicy;)Ljava/util/List;
    .locals 3
    .param p1, "config"    # Lmiui/process/ProcessConfig;
    .param p2, "mProcessPolicy"    # Lcom/android/server/am/ProcessPolicy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/process/ProcessConfig;",
            "Lcom/android/server/am/ProcessPolicy;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 285
    invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getPolicy()I

    move-result v0

    .line 286
    .local v0, "policy":I
    invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getWhiteList()Ljava/util/List;

    move-result-object v1

    .line 287
    .local v1, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v1, :cond_0

    .line 288
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object v1, v2

    .line 290
    :cond_0
    const/16 v2, 0xe

    if-eq v0, v2, :cond_2

    const/16 v2, 0x10

    if-eq v0, v2, :cond_2

    const/16 v2, 0xf

    if-ne v0, v2, :cond_1

    goto :goto_0

    .line 294
    :cond_1
    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    .line 295
    const-string v2, "com.miui.home"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 293
    :cond_2
    :goto_0
    const-string v2, "com.android.deskclock"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 297
    :cond_3
    :goto_1
    invoke-virtual {p0, p1, v1}, Lcom/android/server/am/ProcessCleanerBase;->printWhiteList(Lmiui/process/ProcessConfig;Ljava/util/List;)V

    .line 298
    return-object v1
.end method

.method isAudioOrGPSApp(I)Z
    .locals 1
    .param p1, "uid"    # I

    .line 230
    invoke-static {}, Lcom/android/server/am/SystemPressureController;->getInstance()Lcom/android/server/am/SystemPressureController;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/am/SystemPressureController;->isAudioOrGPSApp(I)Z

    move-result v0

    return v0
.end method

.method isAudioOrGPSProc(II)Z
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 234
    invoke-static {}, Lcom/android/server/am/SystemPressureController;->getInstance()Lcom/android/server/am/SystemPressureController;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/server/am/SystemPressureController;->isAudioOrGPSProc(II)Z

    move-result v0

    return v0
.end method

.method isCurrentProcessInBackup(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z
    .locals 2
    .param p1, "runningProc"    # Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 204
    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/android/server/am/ProcessCleanerBase;->isCurrentProcessInBackup(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method isCurrentProcessInBackup(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "processName"    # Ljava/lang/String;

    .line 208
    iget-object v0, p0, Lcom/android/server/am/ProcessCleanerBase;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    const/16 v1, 0x40

    invoke-interface {v0, v1, p1, p2}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->isProcessWhiteList(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    const/4 v0, 0x1

    return v0

    .line 212
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method isForceStopEnable(Lcom/android/server/am/ProcessRecord;ILcom/android/server/am/ProcessManagerService;)Z
    .locals 1
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "policy"    # I
    .param p3, "pms"    # Lcom/android/server/am/ProcessManagerService;

    .line 226
    invoke-virtual {p3, p1, p2}, Lcom/android/server/am/ProcessManagerService;->isForceStopEnable(Lcom/android/server/am/ProcessRecord;I)Z

    move-result v0

    return v0
.end method

.method isInWhiteListLock(Lcom/android/server/am/ProcessRecord;IILcom/android/server/am/ProcessManagerService;)Z
    .locals 1
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "userId"    # I
    .param p3, "policy"    # I
    .param p4, "pms"    # Lcom/android/server/am/ProcessManagerService;

    .line 222
    invoke-virtual {p4, p1, p2, p3}, Lcom/android/server/am/ProcessManagerService;->isInWhiteList(Lcom/android/server/am/ProcessRecord;II)Z

    move-result v0

    return v0
.end method

.method isTrimMemoryEnable(Ljava/lang/String;Lcom/android/server/am/ProcessManagerService;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "pms"    # Lcom/android/server/am/ProcessManagerService;

    .line 216
    invoke-virtual {p2, p1}, Lcom/android/server/am/ProcessManagerService;->isTrimMemoryEnable(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method killApplicationLock(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
    .locals 4
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "reason"    # Ljava/lang/String;

    .line 271
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "killLocked pid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 272
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " r:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 271
    const-wide/16 v1, 0x40

    invoke-static {v1, v2, v0}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 273
    const/16 v0, 0xd

    const/4 v3, 0x1

    invoke-virtual {p1, p2, v0, v3}, Lcom/android/server/am/ProcessRecord;->killLocked(Ljava/lang/String;IZ)V

    .line 274
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    .line 275
    return-void
.end method

.method killBackgroundApplication(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
    .locals 5
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "reason"    # Ljava/lang/String;

    .line 278
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "killBackgroundProcesses pid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 279
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " r:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 278
    const-wide/16 v1, 0x40

    invoke-static {v1, v2, v0}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 280
    iget-object v0, p0, Lcom/android/server/am/ProcessCleanerBase;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget v4, p1, Lcom/android/server/am/ProcessRecord;->userId:I

    invoke-virtual {v0, v3, v4, p2}, Lcom/android/server/am/ActivityManagerService;->killBackgroundProcesses(Ljava/lang/String;ILjava/lang/String;)V

    .line 281
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    .line 282
    return-void
.end method

.method killLevelToString(I)Ljava/lang/String;
    .locals 1
    .param p1, "level"    # I

    .line 387
    const-string v0, ""

    .line 388
    .local v0, "killLevel":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 399
    :pswitch_0
    const-string v0, "force-stop"

    .line 400
    goto :goto_0

    .line 396
    :pswitch_1
    const-string v0, "kill"

    .line 397
    goto :goto_0

    .line 393
    :pswitch_2
    const-string v0, "kill-background"

    .line 394
    goto :goto_0

    .line 390
    :pswitch_3
    const-string/jumbo v0, "trim-memory"

    .line 391
    goto :goto_0

    .line 402
    :pswitch_4
    const-string v0, "none"

    .line 403
    nop

    .line 406
    :goto_0
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method killOnce(Lcom/android/server/am/ProcessRecord;ILjava/lang/String;ZLcom/android/server/am/ProcessManagerService;Ljava/lang/String;Landroid/os/Handler;Landroid/content/Context;)V
    .locals 14
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "policy"    # I
    .param p3, "killReason"    # Ljava/lang/String;
    .param p4, "canForceStop"    # Z
    .param p5, "pms"    # Lcom/android/server/am/ProcessManagerService;
    .param p6, "killTag"    # Ljava/lang/String;
    .param p7, "handler"    # Landroid/os/Handler;
    .param p8, "context"    # Landroid/content/Context;

    .line 68
    move-object v7, p0

    move-object v8, p1

    move/from16 v9, p2

    move-object/from16 v10, p5

    if-nez v8, :cond_0

    .line 69
    return-void

    .line 71
    :cond_0
    const/16 v1, 0x64

    .line 73
    .local v1, "killLevel":I
    :try_start_0
    iget-object v2, v7, Lcom/android/server/am/ProcessCleanerBase;->mAMS:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 75
    :try_start_1
    iget-object v0, v7, Lcom/android/server/am/ProcessCleanerBase;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mActivityTaskManager:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v3, v8, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/android/server/wm/WindowProcessUtils;->isRunningOnCarDisplay(Lcom/android/server/wm/ActivityTaskManagerService;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 77
    monitor-exit v2

    return-void

    .line 80
    :cond_1
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isPersistent()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, v8, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    .line 81
    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getSetAdj()I

    move-result v0

    if-ltz v0, :cond_5

    iget v0, v8, Lcom/android/server/am/ProcessRecord;->userId:I

    .line 82
    invoke-virtual {p0, p1, v0, v9, v10}, Lcom/android/server/am/ProcessCleanerBase;->isInWhiteListLock(Lcom/android/server/am/ProcessRecord;IILcom/android/server/am/ProcessManagerService;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 87
    :cond_2
    invoke-virtual {p0, p1, v9, v10}, Lcom/android/server/am/ProcessCleanerBase;->isForceStopEnable(Lcom/android/server/am/ProcessRecord;ILcom/android/server/am/ProcessManagerService;)Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz p4, :cond_3

    .line 88
    const/16 v0, 0x68

    move v11, v0

    .end local v1    # "killLevel":I
    .local v0, "killLevel":I
    goto :goto_1

    .line 89
    .end local v0    # "killLevel":I
    .restart local v1    # "killLevel":I
    :cond_3
    const/4 v0, 0x3

    if-ne v9, v0, :cond_4

    .line 90
    const/16 v0, 0x66

    move v11, v0

    .end local v1    # "killLevel":I
    .restart local v0    # "killLevel":I
    goto :goto_1

    .line 92
    .end local v0    # "killLevel":I
    .restart local v1    # "killLevel":I
    :cond_4
    const/16 v0, 0x67

    move v11, v0

    .end local v1    # "killLevel":I
    .restart local v0    # "killLevel":I
    goto :goto_1

    .line 83
    .end local v0    # "killLevel":I
    .restart local v1    # "killLevel":I
    :cond_5
    :goto_0
    iget-object v0, v8, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->hasOverlayUi()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, v8, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->hasTopUi()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, v8, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->hasShownUi()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, v8, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 84
    invoke-virtual {p0, v0, v10}, Lcom/android/server/am/ProcessCleanerBase;->isTrimMemoryEnable(Ljava/lang/String;Lcom/android/server/am/ProcessManagerService;)Z

    move-result v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v0, :cond_6

    .line 85
    const/16 v0, 0x65

    move v11, v0

    .end local v1    # "killLevel":I
    .restart local v0    # "killLevel":I
    goto :goto_1

    .line 94
    .end local v0    # "killLevel":I
    .restart local v1    # "killLevel":I
    :cond_6
    move v11, v1

    .end local v1    # "killLevel":I
    .local v11, "killLevel":I
    :goto_1
    const/16 v0, 0x65

    if-le v11, v0, :cond_7

    .line 95
    move-object/from16 v12, p3

    move-object/from16 v13, p6

    :try_start_2
    invoke-direct {p0, p1, v11, v12, v13}, Lcom/android/server/am/ProcessCleanerBase;->printKillLog(Lcom/android/server/am/ProcessRecord;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 94
    :cond_7
    move-object/from16 v12, p3

    move-object/from16 v13, p6

    .line 97
    :goto_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 98
    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p3

    move v4, v11

    move-object/from16 v5, p7

    move-object/from16 v6, p8

    :try_start_3
    invoke-virtual/range {v1 .. v6}, Lcom/android/server/am/ProcessCleanerBase;->killOnce(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;ILandroid/os/Handler;Landroid/content/Context;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 101
    goto :goto_5

    .line 99
    :catch_0
    move-exception v0

    move v1, v11

    goto :goto_4

    .line 97
    :catchall_0
    move-exception v0

    move v1, v11

    goto :goto_3

    .end local v11    # "killLevel":I
    .restart local v1    # "killLevel":I
    :catchall_1
    move-exception v0

    move-object/from16 v12, p3

    move-object/from16 v13, p6

    :goto_3
    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .end local v1    # "killLevel":I
    .end local p0    # "this":Lcom/android/server/am/ProcessCleanerBase;
    .end local p1    # "app":Lcom/android/server/am/ProcessRecord;
    .end local p2    # "policy":I
    .end local p3    # "killReason":Ljava/lang/String;
    .end local p4    # "canForceStop":Z
    .end local p5    # "pms":Lcom/android/server/am/ProcessManagerService;
    .end local p6    # "killTag":Ljava/lang/String;
    .end local p7    # "handler":Landroid/os/Handler;
    .end local p8    # "context":Landroid/content/Context;
    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    .line 99
    .restart local v1    # "killLevel":I
    .restart local p0    # "this":Lcom/android/server/am/ProcessCleanerBase;
    .restart local p1    # "app":Lcom/android/server/am/ProcessRecord;
    .restart local p2    # "policy":I
    .restart local p3    # "killReason":Ljava/lang/String;
    .restart local p4    # "canForceStop":Z
    .restart local p5    # "pms":Lcom/android/server/am/ProcessManagerService;
    .restart local p6    # "killTag":Ljava/lang/String;
    .restart local p7    # "handler":Landroid/os/Handler;
    .restart local p8    # "context":Landroid/content/Context;
    :catch_1
    move-exception v0

    goto :goto_4

    .line 97
    :catchall_2
    move-exception v0

    goto :goto_3

    .line 99
    :catch_2
    move-exception v0

    move-object/from16 v12, p3

    move-object/from16 v13, p6

    .line 100
    .local v0, "e":Ljava/lang/Exception;
    :goto_4
    const-string v2, "ProcessCleanerBase"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "killOnce:reason "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v11, v1

    .line 102
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "killLevel":I
    .restart local v11    # "killLevel":I
    :goto_5
    return-void
.end method

.method killOnce(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;ILandroid/os/Handler;Landroid/content/Context;)V
    .locals 4
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "reason"    # Ljava/lang/String;
    .param p3, "killLevel"    # I
    .param p4, "handler"    # Landroid/os/Handler;
    .param p5, "context"    # Landroid/content/Context;

    .line 107
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessCleanerBase;->checkProcessDied(Lcom/android/server/am/ProcessRecord;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    return-void

    .line 111
    :cond_0
    invoke-virtual {p0, p1, p4, p5}, Lcom/android/server/am/ProcessCleanerBase;->checkKillFMApp(Lcom/android/server/am/ProcessRecord;Landroid/os/Handler;Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    return-void

    .line 114
    :cond_1
    const/16 v0, 0x65

    if-ne p3, v0, :cond_2

    .line 115
    invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessCleanerBase;->trimMemory(Lcom/android/server/am/ProcessRecord;)V

    goto :goto_0

    .line 116
    :cond_2
    const/16 v0, 0x66

    if-ne p3, v0, :cond_3

    .line 117
    invoke-virtual {p0, p1, p2}, Lcom/android/server/am/ProcessCleanerBase;->killBackgroundApplication(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V

    goto :goto_0

    .line 118
    :cond_3
    const/16 v0, 0x67

    if-ne p3, v0, :cond_4

    .line 119
    iget-object v0, p0, Lcom/android/server/am/ProcessCleanerBase;->mAMS:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    :try_start_1
    invoke-virtual {p0, p1, p2}, Lcom/android/server/am/ProcessCleanerBase;->killApplicationLock(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V

    .line 121
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local p0    # "this":Lcom/android/server/am/ProcessCleanerBase;
    .end local p1    # "app":Lcom/android/server/am/ProcessRecord;
    .end local p2    # "reason":Ljava/lang/String;
    .end local p3    # "killLevel":I
    .end local p4    # "handler":Landroid/os/Handler;
    .end local p5    # "context":Landroid/content/Context;
    :try_start_2
    throw v1

    .line 122
    .restart local p0    # "this":Lcom/android/server/am/ProcessCleanerBase;
    .restart local p1    # "app":Lcom/android/server/am/ProcessRecord;
    .restart local p2    # "reason":Ljava/lang/String;
    .restart local p3    # "killLevel":I
    .restart local p4    # "handler":Landroid/os/Handler;
    .restart local p5    # "context":Landroid/content/Context;
    :cond_4
    const/16 v0, 0x68

    if-ne p3, v0, :cond_5

    .line 123
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget v1, p1, Lcom/android/server/am/ProcessRecord;->userId:I

    invoke-virtual {p0, v0, v1, p2}, Lcom/android/server/am/ProcessCleanerBase;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 127
    :cond_5
    :goto_0
    goto :goto_1

    .line 125
    :catch_0
    move-exception v0

    .line 126
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ProcessCleanerBase"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "killOnce:reason "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method

.method printWhiteList(Lmiui/process/ProcessConfig;Ljava/util/List;)V
    .locals 4
    .param p1, "config"    # Lmiui/process/ProcessConfig;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/process/ProcessConfig;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 319
    .local p2, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-boolean v0, Landroid/os/spc/PressureStateSettings;->DEBUG_ALL:Z

    if-nez v0, :cond_0

    .line 320
    return-void

    .line 322
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getPolicy()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/android/server/am/ProcessCleanerBase;->getKillReason(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " whiteList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 323
    .local v0, "info":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 324
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 323
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 326
    .end local v1    # "i":I
    :cond_1
    const-string v1, "ProcessCleanerBase"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    return-void
.end method

.method removeAllTasks(ILcom/android/server/am/ProcessManagerService;)V
    .locals 5
    .param p1, "userId"    # I
    .param p2, "pms"    # Lcom/android/server/am/ProcessManagerService;

    .line 161
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 163
    .local v0, "token":J
    :try_start_0
    invoke-virtual {p2}, Lcom/android/server/am/ProcessManagerService;->getInternal()Lcom/miui/server/process/ProcessManagerInternal;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/ProcessCleanerBase;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mActivityTaskManager:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-static {v2, p1, v3}, Lcom/android/server/wm/WindowProcessUtils;->removeAllTasks(Lcom/miui/server/process/ProcessManagerInternal;ILcom/android/server/wm/ActivityTaskManagerService;)V

    .line 164
    if-nez p1, :cond_0

    .line 165
    invoke-virtual {p2}, Lcom/android/server/am/ProcessManagerService;->getInternal()Lcom/miui/server/process/ProcessManagerInternal;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/ProcessCleanerBase;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mActivityTaskManager:Lcom/android/server/wm/ActivityTaskManagerService;

    const/16 v4, 0x3e7

    invoke-static {v2, v4, v3}, Lcom/android/server/wm/WindowProcessUtils;->removeAllTasks(Lcom/miui/server/process/ProcessManagerInternal;ILcom/android/server/wm/ActivityTaskManagerService;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 168
    :cond_0
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 169
    nop

    .line 170
    return-void

    .line 168
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 169
    throw v2
.end method

.method removeTaskIfNeeded(I)V
    .locals 3
    .param p1, "taskId"    # I

    .line 185
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 187
    .local v0, "token":J
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/ProcessCleanerBase;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mActivityTaskManager:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-static {p1, v2}, Lcom/android/server/wm/WindowProcessUtils;->removeTask(ILcom/android/server/wm/ActivityTaskManagerService;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 189
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 190
    nop

    .line 191
    return-void

    .line 189
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 190
    throw v2
.end method

.method removeTasksIfNeeded(Ljava/util/List;Ljava/util/Set;Ljava/util/List;Ljava/util/List;Lcom/android/server/am/ProcessPolicy;)V
    .locals 9
    .param p5, "procPolicy"    # Lcom/android/server/am/ProcessPolicy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/android/server/am/ProcessPolicy;",
            ")V"
        }
    .end annotation

    .line 175
    .local p1, "taskIdList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .local p2, "whiteTaskSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .local p3, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p4, "whiteListTaskId":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 177
    .local v0, "token":J
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/ProcessCleanerBase;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget-object v6, v2, Lcom/android/server/am/ActivityManagerService;->mActivityTaskManager:Lcom/android/server/wm/ActivityTaskManagerService;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p5

    move-object v7, p3

    move-object v8, p4

    invoke-static/range {v3 .. v8}, Lcom/android/server/wm/WindowProcessUtils;->removeTasks(Ljava/util/List;Ljava/util/Set;Lcom/android/server/am/IProcessPolicy;Lcom/android/server/wm/ActivityTaskManagerService;Ljava/util/List;Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 180
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 181
    nop

    .line 182
    return-void

    .line 180
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 181
    throw v2
.end method

.method removeTasksIfNeeded(Lmiui/process/ProcessConfig;Lcom/android/server/am/ProcessPolicy;Ljava/util/List;Ljava/util/Map;)V
    .locals 10
    .param p1, "config"    # Lmiui/process/ProcessConfig;
    .param p2, "mProcessPolicy"    # Lcom/android/server/am/ProcessPolicy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/process/ProcessConfig;",
            "Lcom/android/server/am/ProcessPolicy;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 303
    .local p3, "policyWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p4, "fgTaskPackageMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/String;>;"
    invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getWhiteListTaskId()Ljava/util/List;

    move-result-object v9

    .line 304
    .local v9, "whiteListTaskId":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-virtual {p1}, Lmiui/process/ProcessConfig;->isRemoveTaskNeeded()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getRemovingTaskIdList()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 305
    if-eqz p4, :cond_0

    invoke-interface {p4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move-object v2, v0

    .line 306
    .local v2, "fgTaskIdSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getPolicy()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 307
    iget-object v0, p0, Lcom/android/server/am/ProcessCleanerBase;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mActivityTaskManager:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-static {v0}, Lcom/android/server/wm/WindowProcessUtils;->getAllTaskIdList(Lcom/android/server/wm/ActivityTaskManagerService;)Ljava/util/List;

    move-result-object v6

    .line 309
    .local v6, "taskList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    move-object v0, p0

    move-object v1, v6

    move-object v3, p3

    move-object v4, v9

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/android/server/am/ProcessCleanerBase;->removeTasksIfNeeded(Ljava/util/List;Ljava/util/Set;Ljava/util/List;Ljava/util/List;Lcom/android/server/am/ProcessPolicy;)V

    .line 311
    .end local v6    # "taskList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    goto :goto_1

    .line 312
    :cond_1
    invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getRemovingTaskIdList()Ljava/util/List;

    move-result-object v4

    move-object v3, p0

    move-object v5, v2

    move-object v6, p3

    move-object v7, v9

    move-object v8, p2

    invoke-virtual/range {v3 .. v8}, Lcom/android/server/am/ProcessCleanerBase;->removeTasksIfNeeded(Ljava/util/List;Ljava/util/Set;Ljava/util/List;Ljava/util/List;Lcom/android/server/am/ProcessPolicy;)V

    .line 316
    .end local v2    # "fgTaskIdSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :cond_2
    :goto_1
    return-void
.end method

.method removeTasksInPackages(Ljava/util/List;ILcom/android/server/am/ProcessPolicy;)V
    .locals 3
    .param p2, "userId"    # I
    .param p3, "procPolicy"    # Lcom/android/server/am/ProcessPolicy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;I",
            "Lcom/android/server/am/ProcessPolicy;",
            ")V"
        }
    .end annotation

    .line 194
    .local p1, "packages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 196
    .local v0, "token":J
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/ProcessCleanerBase;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mActivityTaskManager:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-static {p1, p2, p3, v2}, Lcom/android/server/wm/WindowProcessUtils;->removeTasksInPackages(Ljava/util/List;ILcom/android/server/am/IProcessPolicy;Lcom/android/server/wm/ActivityTaskManagerService;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 199
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 200
    nop

    .line 201
    return-void

    .line 199
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 200
    throw v2
.end method

.method scheduleTrimMemory(Lcom/android/server/am/ProcessRecord;I)V
    .locals 2
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "level"    # I

    .line 250
    iget-object v0, p0, Lcom/android/server/am/ProcessCleanerBase;->mAMS:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v0

    .line 251
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessCleanerBase;->checkProcessDied(Lcom/android/server/am/ProcessRecord;)Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 253
    :try_start_1
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v1

    invoke-interface {v1, p2}, Landroid/app/IApplicationThread;->scheduleTrimMemory(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 256
    goto :goto_0

    .line 254
    :catch_0
    move-exception v1

    .line 255
    .local v1, "e":Landroid/os/RemoteException;
    :try_start_2
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 258
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_0
    monitor-exit v0

    .line 259
    return-void

    .line 258
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public systemReady(Landroid/content/Context;Lcom/android/server/am/ProcessManagerService;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pms"    # Lcom/android/server/am/ProcessManagerService;

    .line 53
    const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    iput-object v0, p0, Lcom/android/server/am/ProcessCleanerBase;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    .line 54
    return-void
.end method

.method trimMemory(Lcom/android/server/am/ProcessRecord;)V
    .locals 2
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 238
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getWindowProcessController()Lcom/android/server/wm/WindowProcessController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/WindowProcessController;->isInterestingToUser()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 239
    return-void

    .line 242
    :cond_0
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-string v1, "android"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 243
    const/16 v0, 0x3c

    invoke-virtual {p0, p1, v0}, Lcom/android/server/am/ProcessCleanerBase;->scheduleTrimMemory(Lcom/android/server/am/ProcessRecord;I)V

    goto :goto_0

    .line 245
    :cond_1
    const/16 v0, 0x50

    invoke-virtual {p0, p1, v0}, Lcom/android/server/am/ProcessCleanerBase;->scheduleTrimMemory(Lcom/android/server/am/ProcessRecord;I)V

    .line 247
    :goto_0
    return-void
.end method
