class com.android.server.am.PeriodicCleanerService$1 extends android.database.ContentObserver {
	 /* .source "PeriodicCleanerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/am/PeriodicCleanerService;->registerCloudObserver(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.am.PeriodicCleanerService this$0; //synthetic
final android.content.Context val$context; //synthetic
/* # direct methods */
 com.android.server.am.PeriodicCleanerService$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/PeriodicCleanerService; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 331 */
this.this$0 = p1;
this.val$context = p3;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 4 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 334 */
if ( p2 != null) { // if-eqz p2, :cond_0
	 final String v0 = "cloud_periodic_enable"; // const-string v0, "cloud_periodic_enable"
	 android.provider.Settings$System .getUriFor ( v0 );
	 v1 = 	 (( android.net.Uri ) p2 ).equals ( v1 ); // invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 335 */
		 v1 = this.this$0;
		 v2 = this.val$context;
		 /* .line 336 */
		 (( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
		 int v3 = -2; // const/4 v3, -0x2
		 android.provider.Settings$System .getStringForUser ( v2,v0,v3 );
		 /* .line 335 */
		 v0 = 		 java.lang.Boolean .parseBoolean ( v0 );
		 com.android.server.am.PeriodicCleanerService .-$$Nest$fputmEnable ( v1,v0 );
		 /* .line 338 */
		 v0 = this.this$0;
		 v1 = 		 com.android.server.am.PeriodicCleanerService .-$$Nest$fgetmEnable ( v0 );
		 com.android.server.am.PeriodicCleanerService .-$$Nest$fputmReady ( v0,v1 );
		 /* .line 339 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v1 = "cloud control set received: "; // const-string v1, "cloud control set received: "
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v1 = this.this$0;
		 v1 = 		 com.android.server.am.PeriodicCleanerService .-$$Nest$fgetmEnable ( v1 );
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 final String v1 = "PeriodicCleaner"; // const-string v1, "PeriodicCleaner"
		 android.util.Slog .w ( v1,v0 );
		 /* .line 341 */
	 } // :cond_0
	 return;
} // .end method
