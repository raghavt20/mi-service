class com.android.server.am.MiuiApplicationThreadManager {
	 /* .source "MiuiApplicationThreadManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/am/MiuiApplicationThreadManager$CallBack; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
/* # instance fields */
private com.android.server.am.ActivityManagerService mActivityManagerService;
private android.util.SparseArray mMiuiApplicationThreads;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lmiui/process/IMiuiApplicationThread;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public com.android.server.am.MiuiApplicationThreadManager ( ) {
/* .locals 1 */
/* .param p1, "ams" # Lcom/android/server/am/ActivityManagerService; */
/* .line 21 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 18 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mMiuiApplicationThreads = v0;
/* .line 22 */
this.mActivityManagerService = p1;
/* .line 23 */
return;
} // .end method
/* # virtual methods */
public synchronized void addMiuiApplicationThread ( miui.process.IMiuiApplicationThread p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "applicationThread" # Lmiui/process/IMiuiApplicationThread; */
/* .param p2, "pid" # I */
/* monitor-enter p0 */
/* .line 26 */
try { // :try_start_0
v0 = this.mMiuiApplicationThreads;
(( android.util.SparseArray ) v0 ).put ( p2, p1 ); // invoke-virtual {v0, p2, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 28 */
try { // :try_start_1
	 /* new-instance v0, Lcom/android/server/am/MiuiApplicationThreadManager$CallBack; */
	 /* invoke-direct {v0, p0, p2, p1}, Lcom/android/server/am/MiuiApplicationThreadManager$CallBack;-><init>(Lcom/android/server/am/MiuiApplicationThreadManager;ILmiui/process/IMiuiApplicationThread;)V */
	 /* .line 29 */
	 /* .local v0, "callback":Lcom/android/server/am/MiuiApplicationThreadManager$CallBack; */
	 int v2 = 0; // const/4 v2, 0x0
	 /* :try_end_1 */
	 /* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
	 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
	 /* .line 32 */
} // .end local v0 # "callback":Lcom/android/server/am/MiuiApplicationThreadManager$CallBack;
/* .line 30 */
} // .end local p0 # "this":Lcom/android/server/am/MiuiApplicationThreadManager;
/* :catch_0 */
/* move-exception v0 */
/* .line 31 */
/* .local v0, "e":Landroid/os/RemoteException; */
try { // :try_start_2
final String v1 = "ProcessManager"; // const-string v1, "ProcessManager"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "process:"; // const-string v3, "process:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " is dead"; // const-string v3, " is dead"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .w ( v1,v2 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 33 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
/* monitor-exit p0 */
return;
/* .line 25 */
} // .end local p1 # "applicationThread":Lmiui/process/IMiuiApplicationThread;
} // .end local p2 # "pid":I
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
public synchronized miui.process.IMiuiApplicationThread getMiuiApplicationThread ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "pid" # I */
/* monitor-enter p0 */
/* .line 40 */
if ( p1 != null) { // if-eqz p1, :cond_0
try { // :try_start_0
v0 = this.mMiuiApplicationThreads;
(( android.util.SparseArray ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Lmiui/process/IMiuiApplicationThread; */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 40 */
} // .end local p0 # "this":Lcom/android/server/am/MiuiApplicationThreadManager;
} // .end local p1 # "pid":I
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
/* .line 40 */
/* .restart local p0 # "this":Lcom/android/server/am/MiuiApplicationThreadManager; */
/* .restart local p1 # "pid":I */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
/* monitor-exit p0 */
} // .end method
public synchronized void removeMiuiApplicationThread ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "pid" # I */
/* monitor-enter p0 */
/* .line 36 */
try { // :try_start_0
v0 = this.mMiuiApplicationThreads;
(( android.util.SparseArray ) v0 ).remove ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 37 */
/* monitor-exit p0 */
return;
/* .line 35 */
} // .end local p0 # "this":Lcom/android/server/am/MiuiApplicationThreadManager;
} // .end local p1 # "pid":I
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
