.class public Lcom/android/server/am/MemUsageBuilder;
.super Ljava/lang/Object;
.source "MemUsageBuilder.java"


# static fields
.field private static final DUMP_DMABUF_THRESHOLD:J = 0x100000L

.field private static final DUMP_GPUMemory_THRESHOLD:J = 0x100000L

.field static final DUMP_MEM_BUCKETS:[J


# instance fields
.field cachedPss:J

.field fullJava:Ljava/lang/String;

.field fullNative:Ljava/lang/String;

.field private final mAppProfiler:Lcom/android/server/am/AppProfiler;

.field private final memInfos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/server/am/ProcessMemInfo;",
            ">;"
        }
    .end annotation
.end field

.field private scoutInfo:Lcom/android/server/am/ScoutMeminfo;

.field shortNative:Ljava/lang/String;

.field stack:Ljava/lang/String;

.field subject:Ljava/lang/String;

.field summary:Ljava/lang/String;

.field final title:Ljava/lang/String;

.field topProcs:Ljava/lang/String;

.field totalMemtrack:J

.field totalMemtrackGl:J

.field totalMemtrackGraphics:J

.field totalPss:J

.field totalSwapPss:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 35
    const/16 v0, 0x17

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/server/am/MemUsageBuilder;->DUMP_MEM_BUCKETS:[J

    return-void

    :array_0
    .array-data 8
        0x1400
        0x1c00
        0x2800
        0x3c00
        0x5000
        0x7800
        0xa000
        0x14000
        0x1e000
        0x28000
        0x32000
        0x3e800
        0x4b000
        0x57800
        0x64000
        0x7d000
        0x96000
        0xc8000
        0x100000
        0x200000
        0x500000
        0xa00000
        0x1400000
    .end array-data
.end method

.method constructor <init>(Lcom/android/server/am/AppProfiler;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 2
    .param p1, "appProfiler"    # Lcom/android/server/am/AppProfiler;
    .param p3, "title"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/server/am/AppProfiler;",
            "Ljava/util/ArrayList<",
            "Lcom/android/server/am/ProcessMemInfo;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 68
    .local p2, "memInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessMemInfo;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrackGraphics:J

    .line 53
    iput-wide v0, p0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrackGl:J

    .line 54
    iput-wide v0, p0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J

    .line 55
    iput-wide v0, p0, Lcom/android/server/am/MemUsageBuilder;->totalSwapPss:J

    .line 56
    iput-wide v0, p0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrack:J

    .line 57
    iput-wide v0, p0, Lcom/android/server/am/MemUsageBuilder;->cachedPss:J

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/am/MemUsageBuilder;->subject:Ljava/lang/String;

    .line 61
    iput-object v0, p0, Lcom/android/server/am/MemUsageBuilder;->stack:Ljava/lang/String;

    .line 62
    iput-object v0, p0, Lcom/android/server/am/MemUsageBuilder;->fullNative:Ljava/lang/String;

    .line 63
    iput-object v0, p0, Lcom/android/server/am/MemUsageBuilder;->shortNative:Ljava/lang/String;

    .line 64
    iput-object v0, p0, Lcom/android/server/am/MemUsageBuilder;->fullJava:Ljava/lang/String;

    .line 65
    iput-object v0, p0, Lcom/android/server/am/MemUsageBuilder;->summary:Ljava/lang/String;

    .line 66
    iput-object v0, p0, Lcom/android/server/am/MemUsageBuilder;->topProcs:Ljava/lang/String;

    .line 69
    iput-object p2, p0, Lcom/android/server/am/MemUsageBuilder;->memInfos:Ljava/util/ArrayList;

    .line 70
    iput-object p1, p0, Lcom/android/server/am/MemUsageBuilder;->mAppProfiler:Lcom/android/server/am/AppProfiler;

    .line 71
    iput-object p3, p0, Lcom/android/server/am/MemUsageBuilder;->title:Ljava/lang/String;

    .line 72
    invoke-direct {p0}, Lcom/android/server/am/MemUsageBuilder;->prepare()V

    .line 73
    return-void
.end method

.method static appendBasicMemEntry(Ljava/lang/StringBuilder;IIJJLjava/lang/String;)V
    .locals 2
    .param p0, "sb"    # Ljava/lang/StringBuilder;
    .param p1, "oomAdj"    # I
    .param p2, "procState"    # I
    .param p3, "pss"    # J
    .param p5, "memtrack"    # J
    .param p7, "name"    # Ljava/lang/String;

    .line 485
    const-string v0, "  "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 486
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/android/server/am/ProcessList;->makeOomAdjString(IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 487
    const/16 v0, 0x20

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 488
    invoke-static {p2}, Lcom/android/server/am/ProcessList;->makeProcStateString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 489
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 490
    invoke-static {p0, p3, p4}, Lcom/android/server/am/ProcessList;->appendRamKb(Ljava/lang/StringBuilder;J)V

    .line 491
    const-string v0, ": "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 492
    invoke-virtual {p0, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 493
    const-wide/16 v0, 0x0

    cmp-long v0, p5, v0

    if-lez v0, :cond_0

    .line 494
    const-string v0, " ("

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 495
    invoke-static {p5, p6}, Lcom/android/server/am/ActivityManagerService;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 496
    const-string v0, " memtrack)"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 498
    :cond_0
    return-void
.end method

.method static final appendMemBucket(Ljava/lang/StringBuilder;JLjava/lang/String;Z)V
    .locals 10
    .param p0, "out"    # Ljava/lang/StringBuilder;
    .param p1, "memKB"    # J
    .param p3, "label"    # Ljava/lang/String;
    .param p4, "stackLike"    # Z

    .line 516
    const/16 v0, 0x2e

    invoke-virtual {p3, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 517
    .local v0, "start":I
    if-ltz v0, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 518
    :cond_0
    const/4 v0, 0x0

    .line 519
    :goto_0
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v1

    .line 520
    .local v1, "end":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    sget-object v3, Lcom/android/server/am/MemUsageBuilder;->DUMP_MEM_BUCKETS:[J

    array-length v4, v3

    const-string v5, "MB."

    const-string v6, "MB "

    const-wide/16 v7, 0x400

    if-ge v2, v4, :cond_3

    .line 521
    aget-wide v3, v3, v2

    cmp-long v9, v3, p1

    if-ltz v9, :cond_2

    .line 522
    div-long/2addr v3, v7

    .line 523
    .local v3, "bucket":J
    invoke-virtual {p0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 524
    if-eqz p4, :cond_1

    goto :goto_2

    :cond_1
    move-object v5, v6

    :goto_2
    invoke-virtual {p0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 525
    invoke-virtual {p0, p3, v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    .line 526
    return-void

    .line 520
    .end local v3    # "bucket":J
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 529
    .end local v2    # "i":I
    :cond_3
    div-long v2, p1, v7

    invoke-virtual {p0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 530
    if-eqz p4, :cond_4

    goto :goto_3

    :cond_4
    move-object v5, v6

    :goto_3
    invoke-virtual {p0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 531
    invoke-virtual {p0, p3, v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    .line 532
    return-void
.end method

.method static appendMemInfo(Ljava/lang/StringBuilder;Lcom/android/server/am/ProcessMemInfo;)V
    .locals 8
    .param p0, "sb"    # Ljava/lang/StringBuilder;
    .param p1, "mi"    # Lcom/android/server/am/ProcessMemInfo;

    .line 501
    iget v1, p1, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I

    iget v2, p1, Lcom/android/server/am/ProcessMemInfo;->procState:I

    iget-wide v3, p1, Lcom/android/server/am/ProcessMemInfo;->pss:J

    iget-wide v5, p1, Lcom/android/server/am/ProcessMemInfo;->memtrack:J

    iget-object v7, p1, Lcom/android/server/am/ProcessMemInfo;->name:Ljava/lang/String;

    move-object v0, p0

    invoke-static/range {v0 .. v7}, Lcom/android/server/am/MemUsageBuilder;->appendBasicMemEntry(Ljava/lang/StringBuilder;IIJJLjava/lang/String;)V

    .line 502
    const-string v0, " (pid "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 503
    iget v0, p1, Lcom/android/server/am/ProcessMemInfo;->pid:I

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 504
    const-string v0, ") "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 505
    iget-object v0, p1, Lcom/android/server/am/ProcessMemInfo;->adjType:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 506
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 507
    iget-object v1, p1, Lcom/android/server/am/ProcessMemInfo;->adjReason:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 508
    const-string v1, "                      "

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 509
    iget-object v1, p1, Lcom/android/server/am/ProcessMemInfo;->adjReason:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 510
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 512
    :cond_0
    return-void
.end method

.method private calculateCachedPss()V
    .locals 7

    .line 146
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v1, p0, Lcom/android/server/am/MemUsageBuilder;->memInfos:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, "size":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 147
    iget-object v2, p0, Lcom/android/server/am/MemUsageBuilder;->memInfos:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/ProcessMemInfo;

    .line 148
    .local v2, "mi":Lcom/android/server/am/ProcessMemInfo;
    iget v3, v2, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I

    const/16 v4, 0x384

    if-lt v3, v4, :cond_0

    .line 149
    iget-wide v3, p0, Lcom/android/server/am/MemUsageBuilder;->cachedPss:J

    iget-wide v5, v2, Lcom/android/server/am/ProcessMemInfo;->pss:J

    add-long/2addr v3, v5

    iput-wide v3, p0, Lcom/android/server/am/MemUsageBuilder;->cachedPss:J

    .line 146
    .end local v2    # "mi":Lcom/android/server/am/ProcessMemInfo;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 152
    .end local v0    # "i":I
    .end local v1    # "size":I
    :cond_1
    return-void
.end method

.method static synthetic lambda$buildSummary$1([JLcom/android/server/am/ScoutMeminfo;)V
    .locals 0
    .param p0, "infos"    # [J
    .param p1, "scout"    # Lcom/android/server/am/ScoutMeminfo;

    .line 332
    invoke-virtual {p1, p0}, Lcom/android/server/am/ScoutMeminfo;->setInfos([J)V

    return-void
.end method

.method static synthetic lambda$buildSummary$10(JLcom/android/server/am/ScoutMeminfo;)V
    .locals 0
    .param p0, "gpuUsage"    # J
    .param p2, "scout"    # Lcom/android/server/am/ScoutMeminfo;

    .line 418
    invoke-virtual {p2, p0, p1}, Lcom/android/server/am/ScoutMeminfo;->setGpuUsage(J)V

    return-void
.end method

.method static synthetic lambda$buildSummary$11(JLcom/android/server/am/ScoutMeminfo;)V
    .locals 0
    .param p0, "gpuPrivateUsage"    # J
    .param p2, "scout"    # Lcom/android/server/am/ScoutMeminfo;

    .line 419
    invoke-virtual {p2, p0, p1}, Lcom/android/server/am/ScoutMeminfo;->setGpuPrivateUsage(J)V

    return-void
.end method

.method static synthetic lambda$buildSummary$12(Lcom/android/internal/util/MemInfoReader;Lcom/android/server/am/ScoutMeminfo;)V
    .locals 0
    .param p0, "memInfo"    # Lcom/android/internal/util/MemInfoReader;
    .param p1, "scout"    # Lcom/android/server/am/ScoutMeminfo;

    .line 435
    invoke-virtual {p1, p0}, Lcom/android/server/am/ScoutMeminfo;->setMeminfo(Lcom/android/internal/util/MemInfoReader;)V

    return-void
.end method

.method static synthetic lambda$buildSummary$13(JLcom/android/server/am/ScoutMeminfo;)V
    .locals 0
    .param p0, "finalKernelUsed"    # J
    .param p2, "scout"    # Lcom/android/server/am/ScoutMeminfo;

    .line 436
    invoke-virtual {p2, p0, p1}, Lcom/android/server/am/ScoutMeminfo;->setKernelUsed(J)V

    return-void
.end method

.method static synthetic lambda$buildSummary$2([JLcom/android/server/am/ScoutMeminfo;)V
    .locals 0
    .param p0, "ksm"    # [J
    .param p1, "scout"    # Lcom/android/server/am/ScoutMeminfo;

    .line 333
    invoke-virtual {p1, p0}, Lcom/android/server/am/ScoutMeminfo;->setKsm([J)V

    return-void
.end method

.method static synthetic lambda$buildSummary$3(JLcom/android/server/am/ScoutMeminfo;)V
    .locals 0
    .param p0, "ionHeap"    # J
    .param p2, "scout"    # Lcom/android/server/am/ScoutMeminfo;

    .line 342
    invoke-virtual {p2, p0, p1}, Lcom/android/server/am/ScoutMeminfo;->setIonHeap(J)V

    return-void
.end method

.method static synthetic lambda$buildSummary$4(JLcom/android/server/am/ScoutMeminfo;)V
    .locals 0
    .param p0, "ionPool"    # J
    .param p2, "scout"    # Lcom/android/server/am/ScoutMeminfo;

    .line 343
    invoke-virtual {p2, p0, p1}, Lcom/android/server/am/ScoutMeminfo;->setIonPool(J)V

    return-void
.end method

.method static synthetic lambda$buildSummary$5(JLcom/android/server/am/ScoutMeminfo;)V
    .locals 0
    .param p0, "dmabufMapped"    # J
    .param p2, "scout"    # Lcom/android/server/am/ScoutMeminfo;

    .line 344
    invoke-virtual {p2, p0, p1}, Lcom/android/server/am/ScoutMeminfo;->setDmabufMapped(J)V

    return-void
.end method

.method static synthetic lambda$buildSummary$6(JLcom/android/server/am/ScoutMeminfo;)V
    .locals 0
    .param p0, "totalExportedDmabuf"    # J
    .param p2, "scout"    # Lcom/android/server/am/ScoutMeminfo;

    .line 371
    invoke-virtual {p2, p0, p1}, Lcom/android/server/am/ScoutMeminfo;->setTotalExportedDmabuf(J)V

    return-void
.end method

.method static synthetic lambda$buildSummary$7(JLcom/android/server/am/ScoutMeminfo;)V
    .locals 0
    .param p0, "totalExportedDmabufHeap"    # J
    .param p2, "scout"    # Lcom/android/server/am/ScoutMeminfo;

    .line 381
    invoke-virtual {p2, p0, p1}, Lcom/android/server/am/ScoutMeminfo;->setTotalExportedDmabufHeap(J)V

    return-void
.end method

.method static synthetic lambda$buildSummary$8(JLcom/android/server/am/ScoutMeminfo;)V
    .locals 0
    .param p0, "totalDmabufHeapPool"    # J
    .param p2, "scout"    # Lcom/android/server/am/ScoutMeminfo;

    .line 389
    invoke-virtual {p2, p0, p1}, Lcom/android/server/am/ScoutMeminfo;->setTotalDmabufHeapPool(J)V

    return-void
.end method

.method static synthetic lambda$buildSummary$9(JLcom/android/server/am/ScoutMeminfo;)V
    .locals 0
    .param p0, "gpuDmaBufUsage"    # J
    .param p2, "scout"    # Lcom/android/server/am/ScoutMeminfo;

    .line 409
    invoke-virtual {p2, p0, p1}, Lcom/android/server/am/ScoutMeminfo;->setGpuDmaBufUsage(J)V

    return-void
.end method

.method static synthetic lambda$prepare$0(Lcom/android/internal/os/ProcessCpuTracker$Stats;)Z
    .locals 4
    .param p0, "st"    # Lcom/android/internal/os/ProcessCpuTracker$Stats;

    .line 99
    iget-wide v0, p0, Lcom/android/internal/os/ProcessCpuTracker$Stats;->vsize:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private prepare()V
    .locals 22

    .line 90
    move-object/from16 v0, p0

    new-instance v1, Landroid/util/SparseArray;

    iget-object v2, v0, Lcom/android/server/am/MemUsageBuilder;->memInfos:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-direct {v1, v2}, Landroid/util/SparseArray;-><init>(I)V

    .line 91
    .local v1, "infoMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/am/ProcessMemInfo;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    iget-object v3, v0, Lcom/android/server/am/MemUsageBuilder;->memInfos:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    .local v3, "size":I
    :goto_0
    if-ge v2, v3, :cond_0

    .line 92
    iget-object v4, v0, Lcom/android/server/am/MemUsageBuilder;->memInfos:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/am/ProcessMemInfo;

    .line 93
    .local v4, "mi":Lcom/android/server/am/ProcessMemInfo;
    iget v5, v4, Lcom/android/server/am/ProcessMemInfo;->pid:I

    invoke-virtual {v1, v5, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 91
    .end local v4    # "mi":Lcom/android/server/am/ProcessMemInfo;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 95
    .end local v2    # "i":I
    .end local v3    # "size":I
    :cond_0
    iget-object v2, v0, Lcom/android/server/am/MemUsageBuilder;->mAppProfiler:Lcom/android/server/am/AppProfiler;

    invoke-virtual {v2}, Lcom/android/server/am/AppProfiler;->updateCpuStatsNow()V

    .line 96
    const/4 v2, 0x4

    new-array v2, v2, [J

    .line 97
    .local v2, "memtrackTmp":[J
    const/4 v3, 0x2

    new-array v4, v3, [J

    .line 99
    .local v4, "swaptrackTmp":[J
    iget-object v5, v0, Lcom/android/server/am/MemUsageBuilder;->mAppProfiler:Lcom/android/server/am/AppProfiler;

    new-instance v6, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda13;

    invoke-direct {v6}, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda13;-><init>()V

    invoke-virtual {v5, v6}, Lcom/android/server/am/AppProfiler;->getCpuStats(Ljava/util/function/Predicate;)Ljava/util/List;

    move-result-object v5

    .line 100
    .local v5, "stats":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;"
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    .line 101
    .local v6, "statsCount":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    const/4 v8, 0x0

    const-wide/16 v9, 0x0

    const/4 v11, 0x1

    if-ge v7, v6, :cond_3

    .line 102
    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/android/internal/os/ProcessCpuTracker$Stats;

    .line 103
    .local v12, "st":Lcom/android/internal/os/ProcessCpuTracker$Stats;
    iget v13, v12, Lcom/android/internal/os/ProcessCpuTracker$Stats;->pid:I

    invoke-static {v13, v4, v2}, Landroid/os/Debug;->getPss(I[J[J)J

    move-result-wide v13

    .line 104
    .local v13, "pss":J
    cmp-long v9, v13, v9

    if-lez v9, :cond_2

    .line 105
    iget v9, v12, Lcom/android/internal/os/ProcessCpuTracker$Stats;->pid:I

    invoke-virtual {v1, v9}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v9

    if-gez v9, :cond_1

    .line 106
    new-instance v9, Lcom/android/server/am/ProcessMemInfo;

    iget-object v10, v12, Lcom/android/internal/os/ProcessCpuTracker$Stats;->name:Ljava/lang/String;

    iget v15, v12, Lcom/android/internal/os/ProcessCpuTracker$Stats;->pid:I

    const/16 v18, -0x3e8

    const/16 v19, -0x1

    const-string v20, "native"

    const/16 v21, 0x0

    move/from16 v17, v15

    move-object v15, v9

    move-object/from16 v16, v10

    invoke-direct/range {v15 .. v21}, Lcom/android/server/am/ProcessMemInfo;-><init>(Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;)V

    .line 108
    .local v9, "mi":Lcom/android/server/am/ProcessMemInfo;
    iput-wide v13, v9, Lcom/android/server/am/ProcessMemInfo;->pss:J

    .line 109
    move-object v15, v5

    move/from16 v16, v6

    .end local v5    # "stats":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;"
    .end local v6    # "statsCount":I
    .local v15, "stats":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;"
    .local v16, "statsCount":I
    aget-wide v5, v4, v11

    iput-wide v5, v9, Lcom/android/server/am/ProcessMemInfo;->swapPss:J

    .line 110
    aget-wide v5, v2, v8

    iput-wide v5, v9, Lcom/android/server/am/ProcessMemInfo;->memtrack:J

    .line 111
    iget-wide v5, v0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrackGraphics:J

    aget-wide v10, v2, v11

    add-long/2addr v5, v10

    iput-wide v5, v0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrackGraphics:J

    .line 112
    iget-wide v5, v0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrackGl:J

    aget-wide v10, v2, v3

    add-long/2addr v5, v10

    iput-wide v5, v0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrackGl:J

    .line 113
    iget-object v5, v0, Lcom/android/server/am/MemUsageBuilder;->memInfos:Ljava/util/ArrayList;

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 105
    .end local v9    # "mi":Lcom/android/server/am/ProcessMemInfo;
    .end local v15    # "stats":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;"
    .end local v16    # "statsCount":I
    .restart local v5    # "stats":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;"
    .restart local v6    # "statsCount":I
    :cond_1
    move-object v15, v5

    move/from16 v16, v6

    .end local v5    # "stats":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;"
    .end local v6    # "statsCount":I
    .restart local v15    # "stats":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;"
    .restart local v16    # "statsCount":I
    goto :goto_2

    .line 104
    .end local v15    # "stats":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;"
    .end local v16    # "statsCount":I
    .restart local v5    # "stats":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;"
    .restart local v6    # "statsCount":I
    :cond_2
    move-object v15, v5

    move/from16 v16, v6

    .line 101
    .end local v5    # "stats":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;"
    .end local v6    # "statsCount":I
    .end local v12    # "st":Lcom/android/internal/os/ProcessCpuTracker$Stats;
    .end local v13    # "pss":J
    .restart local v15    # "stats":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;"
    .restart local v16    # "statsCount":I
    :goto_2
    add-int/lit8 v7, v7, 0x1

    move-object v5, v15

    move/from16 v6, v16

    goto :goto_1

    .end local v15    # "stats":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;"
    .end local v16    # "statsCount":I
    .restart local v5    # "stats":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;"
    .restart local v6    # "statsCount":I
    :cond_3
    move-object v15, v5

    move/from16 v16, v6

    .line 118
    .end local v5    # "stats":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;"
    .end local v6    # "statsCount":I
    .end local v7    # "i":I
    .restart local v15    # "stats":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/os/ProcessCpuTracker$Stats;>;"
    .restart local v16    # "statsCount":I
    const/4 v5, 0x0

    .local v5, "i":I
    iget-object v6, v0, Lcom/android/server/am/MemUsageBuilder;->memInfos:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    .local v6, "size":I
    :goto_3
    if-ge v5, v6, :cond_5

    .line 119
    iget-object v7, v0, Lcom/android/server/am/MemUsageBuilder;->memInfos:Ljava/util/ArrayList;

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/server/am/ProcessMemInfo;

    .line 120
    .local v7, "mi":Lcom/android/server/am/ProcessMemInfo;
    iget-wide v12, v7, Lcom/android/server/am/ProcessMemInfo;->pss:J

    cmp-long v12, v12, v9

    if-nez v12, :cond_4

    .line 121
    iget v12, v7, Lcom/android/server/am/ProcessMemInfo;->pid:I

    invoke-static {v12, v4, v2}, Landroid/os/Debug;->getPss(I[J[J)J

    move-result-wide v12

    iput-wide v12, v7, Lcom/android/server/am/ProcessMemInfo;->pss:J

    .line 122
    aget-wide v12, v4, v11

    iput-wide v12, v7, Lcom/android/server/am/ProcessMemInfo;->swapPss:J

    .line 123
    aget-wide v12, v2, v8

    iput-wide v12, v7, Lcom/android/server/am/ProcessMemInfo;->memtrack:J

    .line 124
    iget-wide v12, v0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrackGraphics:J

    aget-wide v17, v2, v11

    add-long v12, v12, v17

    iput-wide v12, v0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrackGraphics:J

    .line 125
    iget-wide v12, v0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrackGl:J

    aget-wide v17, v2, v3

    add-long v12, v12, v17

    iput-wide v12, v0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrackGl:J

    .line 127
    :cond_4
    iget-wide v12, v0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J

    move-object/from16 v17, v4

    .end local v4    # "swaptrackTmp":[J
    .local v17, "swaptrackTmp":[J
    iget-wide v3, v7, Lcom/android/server/am/ProcessMemInfo;->pss:J

    add-long/2addr v12, v3

    iput-wide v12, v0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J

    .line 128
    iget-wide v3, v0, Lcom/android/server/am/MemUsageBuilder;->totalSwapPss:J

    iget-wide v12, v7, Lcom/android/server/am/ProcessMemInfo;->swapPss:J

    add-long/2addr v3, v12

    iput-wide v3, v0, Lcom/android/server/am/MemUsageBuilder;->totalSwapPss:J

    .line 129
    iget-wide v3, v0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrack:J

    iget-wide v12, v7, Lcom/android/server/am/ProcessMemInfo;->memtrack:J

    add-long/2addr v3, v12

    iput-wide v3, v0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrack:J

    .line 118
    .end local v7    # "mi":Lcom/android/server/am/ProcessMemInfo;
    add-int/lit8 v5, v5, 0x1

    move-object/from16 v4, v17

    const/4 v3, 0x2

    goto :goto_3

    .end local v17    # "swaptrackTmp":[J
    .restart local v4    # "swaptrackTmp":[J
    :cond_5
    move-object/from16 v17, v4

    .line 131
    .end local v4    # "swaptrackTmp":[J
    .end local v5    # "i":I
    .end local v6    # "size":I
    .restart local v17    # "swaptrackTmp":[J
    iget-object v3, v0, Lcom/android/server/am/MemUsageBuilder;->memInfos:Ljava/util/ArrayList;

    new-instance v4, Lcom/android/server/am/MemUsageBuilder$1;

    invoke-direct {v4, v0}, Lcom/android/server/am/MemUsageBuilder$1;-><init>(Lcom/android/server/am/MemUsageBuilder;)V

    invoke-static {v3, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 142
    invoke-direct/range {p0 .. p0}, Lcom/android/server/am/MemUsageBuilder;->calculateCachedPss()V

    .line 143
    return-void
.end method


# virtual methods
.method buildAll()V
    .locals 1

    .line 80
    invoke-virtual {p0}, Lcom/android/server/am/MemUsageBuilder;->buildSubject()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/MemUsageBuilder;->subject:Ljava/lang/String;

    .line 81
    invoke-virtual {p0}, Lcom/android/server/am/MemUsageBuilder;->buildStack()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/MemUsageBuilder;->stack:Ljava/lang/String;

    .line 82
    invoke-virtual {p0}, Lcom/android/server/am/MemUsageBuilder;->buildFullNative()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/MemUsageBuilder;->fullNative:Ljava/lang/String;

    .line 83
    invoke-virtual {p0}, Lcom/android/server/am/MemUsageBuilder;->buildShortNative()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/MemUsageBuilder;->shortNative:Ljava/lang/String;

    .line 84
    invoke-virtual {p0}, Lcom/android/server/am/MemUsageBuilder;->buildFullJava()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/MemUsageBuilder;->fullJava:Ljava/lang/String;

    .line 85
    invoke-virtual {p0}, Lcom/android/server/am/MemUsageBuilder;->buildSummary()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/MemUsageBuilder;->summary:Ljava/lang/String;

    .line 86
    invoke-virtual {p0}, Lcom/android/server/am/MemUsageBuilder;->buildTopProcs()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/MemUsageBuilder;->topProcs:Ljava/lang/String;

    .line 87
    return-void
.end method

.method buildFullJava()Ljava/lang/String;
    .locals 6

    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x400

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 156
    .local v0, "fullJavaBuilder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    iget-object v2, p0, Lcom/android/server/am/MemUsageBuilder;->memInfos:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    .local v2, "size":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 157
    iget-object v3, p0, Lcom/android/server/am/MemUsageBuilder;->memInfos:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/ProcessMemInfo;

    .line 158
    .local v3, "mi":Lcom/android/server/am/ProcessMemInfo;
    iget v4, v3, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I

    const/16 v5, -0x3e8

    if-eq v4, v5, :cond_0

    .line 159
    invoke-static {v0, v3}, Lcom/android/server/am/MemUsageBuilder;->appendMemInfo(Ljava/lang/StringBuilder;Lcom/android/server/am/ProcessMemInfo;)V

    .line 156
    .end local v3    # "mi":Lcom/android/server/am/ProcessMemInfo;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 162
    .end local v1    # "i":I
    .end local v2    # "size":I
    :cond_1
    const-string v1, "           "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    iget-wide v1, p0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J

    invoke-static {v0, v1, v2}, Lcom/android/server/am/ProcessList;->appendRamKb(Ljava/lang/StringBuilder;J)V

    .line 164
    const-string v1, ": TOTAL"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    iget-wide v1, p0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrack:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_2

    .line 166
    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    iget-wide v1, p0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrack:J

    invoke-static {v1, v2}, Lcom/android/server/am/ActivityManagerService;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    const-string v1, " memtrack)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    :cond_2
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method buildFullNative()Ljava/lang/String;
    .locals 6

    .line 204
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x400

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 205
    .local v0, "fullNativeBuilder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    iget-object v2, p0, Lcom/android/server/am/MemUsageBuilder;->memInfos:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    .local v2, "size":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 206
    iget-object v3, p0, Lcom/android/server/am/MemUsageBuilder;->memInfos:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/ProcessMemInfo;

    .line 207
    .local v3, "mi":Lcom/android/server/am/ProcessMemInfo;
    iget v4, v3, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I

    const/16 v5, -0x3e8

    if-ne v4, v5, :cond_0

    .line 208
    invoke-static {v0, v3}, Lcom/android/server/am/MemUsageBuilder;->appendMemInfo(Ljava/lang/StringBuilder;Lcom/android/server/am/ProcessMemInfo;)V

    .line 205
    .end local v3    # "mi":Lcom/android/server/am/ProcessMemInfo;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 211
    .end local v1    # "i":I
    .end local v2    # "size":I
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method buildShortNative()Ljava/lang/String;
    .locals 18

    .line 175
    move-object/from16 v0, p0

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x400

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 176
    .local v1, "shortNativeBuilder":Ljava/lang/StringBuilder;
    const-wide/16 v2, 0x0

    .line 177
    .local v2, "extraNativeRam":J
    const-wide/16 v4, 0x0

    .line 178
    .local v4, "extraNativeMemtrack":J
    const/4 v6, 0x0

    .local v6, "i":I
    iget-object v7, v0, Lcom/android/server/am/MemUsageBuilder;->memInfos:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v11

    move-wide v12, v2

    move-wide v14, v4

    move v2, v6

    .end local v4    # "extraNativeMemtrack":J
    .end local v6    # "i":I
    .local v2, "i":I
    .local v11, "size":I
    .local v12, "extraNativeRam":J
    .local v14, "extraNativeMemtrack":J
    :goto_0
    if-ge v2, v11, :cond_3

    .line 179
    iget-object v3, v0, Lcom/android/server/am/MemUsageBuilder;->memInfos:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v10, v3

    check-cast v10, Lcom/android/server/am/ProcessMemInfo;

    .line 181
    .local v10, "mi":Lcom/android/server/am/ProcessMemInfo;
    iget v3, v10, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I

    const/16 v4, -0x3e8

    if-ne v3, v4, :cond_1

    .line 183
    iget-wide v3, v10, Lcom/android/server/am/ProcessMemInfo;->pss:J

    const-wide/16 v5, 0x200

    cmp-long v3, v3, v5

    if-ltz v3, :cond_0

    .line 184
    invoke-static {v1, v10}, Lcom/android/server/am/MemUsageBuilder;->appendMemInfo(Ljava/lang/StringBuilder;Lcom/android/server/am/ProcessMemInfo;)V

    goto :goto_1

    .line 186
    :cond_0
    iget-wide v3, v10, Lcom/android/server/am/ProcessMemInfo;->pss:J

    add-long/2addr v12, v3

    .line 187
    iget-wide v3, v10, Lcom/android/server/am/ProcessMemInfo;->memtrack:J

    add-long/2addr v14, v3

    goto :goto_1

    .line 192
    :cond_1
    const-wide/16 v3, 0x0

    cmp-long v3, v12, v3

    if-lez v3, :cond_2

    .line 193
    const/16 v4, -0x3e8

    const/4 v5, -0x1

    const-string v16, "(Other native)"

    move-object v3, v1

    move-wide v6, v12

    move-wide v8, v14

    move-object/from16 v17, v10

    .end local v10    # "mi":Lcom/android/server/am/ProcessMemInfo;
    .local v17, "mi":Lcom/android/server/am/ProcessMemInfo;
    move-object/from16 v10, v16

    invoke-static/range {v3 .. v10}, Lcom/android/server/am/MemUsageBuilder;->appendBasicMemEntry(Ljava/lang/StringBuilder;IIJJLjava/lang/String;)V

    .line 195
    const/16 v3, 0xa

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 196
    const-wide/16 v3, 0x0

    move-wide v12, v3

    .end local v12    # "extraNativeRam":J
    .local v3, "extraNativeRam":J
    goto :goto_1

    .line 192
    .end local v3    # "extraNativeRam":J
    .end local v17    # "mi":Lcom/android/server/am/ProcessMemInfo;
    .restart local v10    # "mi":Lcom/android/server/am/ProcessMemInfo;
    .restart local v12    # "extraNativeRam":J
    :cond_2
    move-object/from16 v17, v10

    .line 178
    .end local v10    # "mi":Lcom/android/server/am/ProcessMemInfo;
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 200
    .end local v2    # "i":I
    .end local v11    # "size":I
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method buildStack()Ljava/lang/String;
    .locals 11

    .line 215
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 216
    .local v0, "stack":Ljava/lang/StringBuilder;
    iget-wide v1, p0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J

    const-string/jumbo v3, "total"

    const/4 v4, 0x1

    invoke-static {v0, v1, v2, v3, v4}, Lcom/android/server/am/MemUsageBuilder;->appendMemBucket(Ljava/lang/StringBuilder;JLjava/lang/String;Z)V

    .line 217
    const/high16 v1, -0x80000000

    .line 218
    .local v1, "lastOomAdj":I
    const/4 v2, 0x1

    .line 219
    .local v2, "firstLine":Z
    const/4 v3, 0x0

    .local v3, "i":I
    iget-object v5, p0, Lcom/android/server/am/MemUsageBuilder;->memInfos:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    .local v5, "size":I
    :goto_0
    if-ge v3, v5, :cond_8

    .line 220
    iget-object v6, p0, Lcom/android/server/am/MemUsageBuilder;->memInfos:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/am/ProcessMemInfo;

    .line 221
    .local v6, "mi":Lcom/android/server/am/ProcessMemInfo;
    iget v7, v6, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I

    const/16 v8, -0x3e8

    if-eq v7, v8, :cond_7

    iget v7, v6, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I

    const/16 v8, 0x1f4

    if-lt v7, v8, :cond_0

    iget v7, v6, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I

    const/16 v8, 0x258

    if-eq v7, v8, :cond_0

    iget v7, v6, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I

    const/16 v8, 0x2bc

    if-ne v7, v8, :cond_7

    .line 225
    :cond_0
    iget v7, v6, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I

    const-string v8, ":"

    const-string v9, "$"

    if-eq v1, v7, :cond_3

    .line 226
    iget v1, v6, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I

    .line 227
    iget v7, v6, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I

    if-ltz v7, :cond_2

    .line 228
    if-eqz v2, :cond_1

    .line 229
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 230
    const/4 v2, 0x0

    .line 232
    :cond_1
    const-string v7, "\n\t at "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 234
    :cond_2
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 237
    :cond_3
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 239
    :goto_1
    iget-wide v9, v6, Lcom/android/server/am/ProcessMemInfo;->pss:J

    iget-object v7, v6, Lcom/android/server/am/ProcessMemInfo;->name:Ljava/lang/String;

    invoke-static {v0, v9, v10, v7, v4}, Lcom/android/server/am/MemUsageBuilder;->appendMemBucket(Ljava/lang/StringBuilder;JLjava/lang/String;Z)V

    .line 240
    iget v7, v6, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I

    if-ltz v7, :cond_7

    add-int/lit8 v7, v3, 0x1

    if-ge v7, v5, :cond_4

    iget-object v7, p0, Lcom/android/server/am/MemUsageBuilder;->memInfos:Ljava/util/ArrayList;

    add-int/lit8 v9, v3, 0x1

    .line 241
    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/server/am/ProcessMemInfo;

    iget v7, v7, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I

    if-eq v7, v1, :cond_7

    .line 242
    :cond_4
    const-string v7, "("

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    const/4 v7, 0x0

    .local v7, "k":I
    :goto_2
    sget-object v9, Lcom/android/server/am/ActivityManagerService;->DUMP_MEM_OOM_ADJ:[I

    array-length v9, v9

    if-ge v7, v9, :cond_6

    .line 244
    sget-object v9, Lcom/android/server/am/ActivityManagerService;->DUMP_MEM_OOM_ADJ:[I

    aget v9, v9, v7

    iget v10, v6, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I

    if-ne v9, v10, :cond_5

    .line 245
    sget-object v9, Lcom/android/server/am/ActivityManagerService;->DUMP_MEM_OOM_LABEL:[Ljava/lang/String;

    aget-object v9, v9, v7

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 246
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    sget-object v9, Lcom/android/server/am/ActivityManagerService;->DUMP_MEM_OOM_ADJ:[I

    aget v9, v9, v7

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 243
    :cond_5
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 250
    .end local v7    # "k":I
    :cond_6
    const-string v7, ")"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    .end local v6    # "mi":Lcom/android/server/am/ProcessMemInfo;
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 254
    .end local v3    # "i":I
    .end local v5    # "size":I
    :cond_8
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method buildSubject()Ljava/lang/String;
    .locals 9

    .line 258
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 259
    .local v0, "tag":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/android/server/am/MemUsageBuilder;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " -- "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    iget-wide v1, p0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J

    const-string/jumbo v3, "total"

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/android/server/am/MemUsageBuilder;->appendMemBucket(Ljava/lang/StringBuilder;JLjava/lang/String;Z)V

    .line 262
    const/high16 v1, -0x80000000

    .line 263
    .local v1, "lastOomAdj":I
    const/4 v2, 0x0

    .local v2, "i":I
    iget-object v3, p0, Lcom/android/server/am/MemUsageBuilder;->memInfos:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    .local v3, "size":I
    :goto_0
    if-ge v2, v3, :cond_4

    .line 264
    iget-object v5, p0, Lcom/android/server/am/MemUsageBuilder;->memInfos:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/am/ProcessMemInfo;

    .line 266
    .local v5, "mi":Lcom/android/server/am/ProcessMemInfo;
    iget v6, v5, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I

    const/16 v7, -0x3e8

    if-eq v6, v7, :cond_3

    iget v6, v5, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I

    const/16 v7, 0x1f4

    if-lt v6, v7, :cond_0

    iget v6, v5, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I

    const/16 v7, 0x258

    if-eq v6, v7, :cond_0

    iget v6, v5, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I

    const/16 v7, 0x2bc

    if-ne v6, v7, :cond_3

    .line 270
    :cond_0
    iget v6, v5, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I

    if-eq v1, v6, :cond_1

    .line 271
    iget v1, v5, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I

    .line 272
    iget v6, v5, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I

    if-gtz v6, :cond_2

    .line 273
    const-string v6, " / "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 276
    :cond_1
    const-string v6, " "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    :cond_2
    :goto_1
    iget v6, v5, Lcom/android/server/am/ProcessMemInfo;->oomAdj:I

    if-gtz v6, :cond_3

    .line 279
    iget-wide v6, v5, Lcom/android/server/am/ProcessMemInfo;->pss:J

    iget-object v8, v5, Lcom/android/server/am/ProcessMemInfo;->name:Ljava/lang/String;

    invoke-static {v0, v6, v7, v8, v4}, Lcom/android/server/am/MemUsageBuilder;->appendMemBucket(Ljava/lang/StringBuilder;JLjava/lang/String;Z)V

    .line 263
    .end local v5    # "mi":Lcom/android/server/am/ProcessMemInfo;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 283
    .end local v2    # "i":I
    .end local v3    # "size":I
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method buildSummary()Ljava/lang/String;
    .locals 30

    .line 287
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/am/MemUsageBuilder;->scoutInfo:Lcom/android/server/am/ScoutMeminfo;

    invoke-static {v1}, Ljava/util/Optional;->ofNullable(Ljava/lang/Object;)Ljava/util/Optional;

    move-result-object v1

    .line 288
    .local v1, "scoutInfo":Ljava/util/Optional;, "Ljava/util/Optional<Lcom/android/server/am/ScoutMeminfo;>;"
    new-instance v2, Lcom/android/internal/util/MemInfoReader;

    invoke-direct {v2}, Lcom/android/internal/util/MemInfoReader;-><init>()V

    .line 289
    .local v2, "memInfo":Lcom/android/internal/util/MemInfoReader;
    invoke-virtual {v2}, Lcom/android/internal/util/MemInfoReader;->readMemInfo()V

    .line 290
    invoke-virtual {v2}, Lcom/android/internal/util/MemInfoReader;->getRawInfo()[J

    move-result-object v3

    .line 291
    .local v3, "infos":[J
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 292
    .local v5, "dumpDmabufInfo":Ljava/lang/Boolean;
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    .line 294
    .local v6, "dumpGpuInfo":Ljava/lang/Boolean;
    new-instance v7, Ljava/lang/StringBuilder;

    const/16 v8, 0x400

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 295
    .local v7, "memInfoBuilder":Ljava/lang/StringBuilder;
    invoke-static {v3}, Landroid/os/Debug;->getMemInfo([J)V

    .line 296
    const-string v8, "  MemInfo: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 297
    const/4 v8, 0x5

    aget-wide v8, v3, v8

    invoke-static {v8, v9}, Lcom/android/server/am/ActivityManagerService;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " slab, "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 298
    const/4 v8, 0x4

    aget-wide v8, v3, v8

    invoke-static {v8, v9}, Lcom/android/server/am/ActivityManagerService;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " shmem, "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    const/16 v8, 0xc

    aget-wide v8, v3, v8

    invoke-static {v8, v9}, Lcom/android/server/am/ActivityManagerService;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 300
    const-string v9, " vm alloc, "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301
    const/16 v8, 0xd

    aget-wide v8, v3, v8

    invoke-static {v8, v9}, Lcom/android/server/am/ActivityManagerService;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 302
    const-string v9, " page tables "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    const/16 v8, 0xe

    aget-wide v8, v3, v8

    invoke-static {v8, v9}, Lcom/android/server/am/ActivityManagerService;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 304
    const-string v9, " kernel stack\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 305
    const-string v8, "           "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 306
    const/4 v8, 0x2

    aget-wide v9, v3, v8

    invoke-static {v9, v10}, Lcom/android/server/am/ActivityManagerService;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " buffers, "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 307
    const/4 v9, 0x3

    aget-wide v10, v3, v9

    invoke-static {v10, v11}, Lcom/android/server/am/ActivityManagerService;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " cached, "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 308
    const/16 v10, 0xb

    aget-wide v10, v3, v10

    invoke-static {v10, v11}, Lcom/android/server/am/ActivityManagerService;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " mapped, "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 309
    const/4 v10, 0x1

    aget-wide v11, v3, v10

    invoke-static {v11, v12}, Lcom/android/server/am/ActivityManagerService;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " free\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 310
    const/16 v11, 0xa

    aget-wide v12, v3, v11

    const-wide/16 v14, 0x0

    cmp-long v12, v12, v14

    if-eqz v12, :cond_0

    .line 311
    const-string v12, "  ZRAM: "

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 312
    aget-wide v11, v3, v11

    invoke-static {v11, v12}, Lcom/android/server/am/ActivityManagerService;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 313
    const-string v11, " RAM, "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 314
    const/16 v11, 0x8

    aget-wide v11, v3, v11

    invoke-static {v11, v12}, Lcom/android/server/am/ActivityManagerService;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 315
    const-string v11, " swap total, "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 316
    const/16 v11, 0x9

    aget-wide v11, v3, v11

    invoke-static {v11, v12}, Lcom/android/server/am/ActivityManagerService;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 317
    const-string v11, " swap free\n"

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 319
    :cond_0
    invoke-static {}, Lcom/android/server/am/ActivityManagerService;->getKsmInfo()[J

    move-result-object v11

    .line 320
    .local v11, "ksm":[J
    aget-wide v12, v11, v10

    cmp-long v12, v12, v14

    if-nez v12, :cond_1

    aget-wide v12, v11, v4

    cmp-long v12, v12, v14

    if-nez v12, :cond_1

    aget-wide v12, v11, v8

    cmp-long v12, v12, v14

    if-nez v12, :cond_1

    aget-wide v12, v11, v9

    cmp-long v12, v12, v14

    if-eqz v12, :cond_2

    .line 322
    :cond_1
    const-string v12, "  KSM: "

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 323
    aget-wide v12, v11, v10

    invoke-static {v12, v13}, Lcom/android/server/am/ActivityManagerService;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 324
    const-string v12, " saved from shared "

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 325
    aget-wide v12, v11, v4

    invoke-static {v12, v13}, Lcom/android/server/am/ActivityManagerService;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 326
    const-string v4, "\n       "

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 327
    aget-wide v12, v11, v8

    invoke-static {v12, v13}, Lcom/android/server/am/ActivityManagerService;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 328
    const-string v4, " unshared; "

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 329
    aget-wide v8, v11, v9

    invoke-static {v8, v9}, Lcom/android/server/am/ActivityManagerService;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 330
    const-string v4, " volatile\n"

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 332
    :cond_2
    new-instance v4, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda0;

    invoke-direct {v4, v3}, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda0;-><init>([J)V

    invoke-virtual {v1, v4}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V

    .line 333
    new-instance v4, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda4;

    invoke-direct {v4, v11}, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda4;-><init>([J)V

    invoke-virtual {v1, v4}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V

    .line 334
    const-string v4, "  Free RAM: "

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 335
    iget-wide v8, v0, Lcom/android/server/am/MemUsageBuilder;->cachedPss:J

    invoke-virtual {v2}, Lcom/android/internal/util/MemInfoReader;->getCachedSizeKb()J

    move-result-wide v12

    add-long/2addr v8, v12

    .line 336
    invoke-virtual {v2}, Lcom/android/internal/util/MemInfoReader;->getFreeSizeKb()J

    move-result-wide v12

    add-long/2addr v8, v12

    .line 335
    invoke-static {v8, v9}, Lcom/android/server/am/ActivityManagerService;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337
    const-string v4, "\n"

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 338
    invoke-virtual {v2}, Lcom/android/internal/util/MemInfoReader;->getKernelUsedSizeKb()J

    move-result-wide v8

    .line 339
    .local v8, "kernelUsed":J
    invoke-static {}, Landroid/os/Debug;->getIonHeapsSizeKb()J

    move-result-wide v12

    .line 340
    .local v12, "ionHeap":J
    move-object/from16 v16, v11

    .end local v11    # "ksm":[J
    .local v16, "ksm":[J
    invoke-static {}, Landroid/os/Debug;->getIonPoolsSizeKb()J

    move-result-wide v10

    .line 341
    .local v10, "ionPool":J
    invoke-static {}, Landroid/os/Debug;->getDmabufMappedSizeKb()J

    move-result-wide v14

    .line 342
    .local v14, "dmabufMapped":J
    move-object/from16 v19, v3

    .end local v3    # "infos":[J
    .local v19, "infos":[J
    new-instance v3, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda5;

    invoke-direct {v3, v12, v13}, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda5;-><init>(J)V

    invoke-virtual {v1, v3}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V

    .line 343
    new-instance v3, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda6;

    invoke-direct {v3, v10, v11}, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda6;-><init>(J)V

    invoke-virtual {v1, v3}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V

    .line 344
    new-instance v3, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda7;

    invoke-direct {v3, v14, v15}, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda7;-><init>(J)V

    invoke-virtual {v1, v3}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V

    .line 345
    const-wide/16 v17, 0x0

    cmp-long v3, v12, v17

    const-wide/32 v20, 0x100000

    if-ltz v3, :cond_3

    cmp-long v3, v10, v17

    if-ltz v3, :cond_3

    .line 346
    sub-long v22, v12, v14

    .line 347
    .local v22, "ionUnmapped":J
    const-string v3, "       ION: "

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 348
    add-long v24, v12, v10

    invoke-static/range {v24 .. v25}, Lcom/android/server/am/ActivityManagerService;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 349
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 350
    add-long v8, v8, v22

    .line 354
    move-object v3, v5

    move-object/from16 v24, v6

    .end local v5    # "dumpDmabufInfo":Ljava/lang/Boolean;
    .end local v6    # "dumpGpuInfo":Ljava/lang/Boolean;
    .local v3, "dumpDmabufInfo":Ljava/lang/Boolean;
    .local v24, "dumpGpuInfo":Ljava/lang/Boolean;
    iget-wide v5, v0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J

    move-wide/from16 v25, v8

    .end local v8    # "kernelUsed":J
    .local v25, "kernelUsed":J
    iget-wide v8, v0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrackGraphics:J

    sub-long/2addr v5, v8

    iput-wide v5, v0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J

    .line 355
    add-long/2addr v5, v14

    iput-wide v5, v0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J

    .line 356
    .end local v22    # "ionUnmapped":J
    move-object v5, v3

    move-wide/from16 v28, v10

    move-wide/from16 v8, v25

    goto/16 :goto_1

    .line 345
    .end local v3    # "dumpDmabufInfo":Ljava/lang/Boolean;
    .end local v24    # "dumpGpuInfo":Ljava/lang/Boolean;
    .end local v25    # "kernelUsed":J
    .restart local v5    # "dumpDmabufInfo":Ljava/lang/Boolean;
    .restart local v6    # "dumpGpuInfo":Ljava/lang/Boolean;
    .restart local v8    # "kernelUsed":J
    :cond_3
    move-object v3, v5

    move-object/from16 v24, v6

    .line 357
    .end local v5    # "dumpDmabufInfo":Ljava/lang/Boolean;
    .end local v6    # "dumpGpuInfo":Ljava/lang/Boolean;
    .restart local v3    # "dumpDmabufInfo":Ljava/lang/Boolean;
    .restart local v24    # "dumpGpuInfo":Ljava/lang/Boolean;
    invoke-static {}, Landroid/os/Debug;->getDmabufTotalExportedKb()J

    move-result-wide v5

    .line 358
    .local v5, "totalExportedDmabuf":J
    const-wide/16 v17, 0x0

    cmp-long v22, v5, v17

    if-ltz v22, :cond_5

    .line 359
    sub-long v22, v5, v14

    .line 360
    .local v22, "dmabufUnmapped":J
    move-object/from16 v25, v3

    .end local v3    # "dumpDmabufInfo":Ljava/lang/Boolean;
    .local v25, "dumpDmabufInfo":Ljava/lang/Boolean;
    const-string v3, "DMA-BUF: "

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 361
    invoke-static {v5, v6}, Lcom/android/server/am/ActivityManagerService;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 362
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 363
    cmp-long v3, v22, v20

    if-lez v3, :cond_4

    .line 364
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v25

    .line 367
    :cond_4
    add-long v8, v8, v22

    .line 369
    move-wide/from16 v26, v8

    .end local v8    # "kernelUsed":J
    .local v26, "kernelUsed":J
    iget-wide v8, v0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J

    move-wide/from16 v28, v10

    .end local v10    # "ionPool":J
    .local v28, "ionPool":J
    iget-wide v10, v0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrackGraphics:J

    sub-long/2addr v8, v10

    iput-wide v8, v0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J

    .line 370
    add-long/2addr v8, v14

    iput-wide v8, v0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J

    .line 371
    new-instance v3, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda8;

    invoke-direct {v3, v5, v6}, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda8;-><init>(J)V

    invoke-virtual {v1, v3}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V

    move-wide/from16 v8, v26

    goto :goto_0

    .line 358
    .end local v22    # "dmabufUnmapped":J
    .end local v25    # "dumpDmabufInfo":Ljava/lang/Boolean;
    .end local v26    # "kernelUsed":J
    .end local v28    # "ionPool":J
    .restart local v3    # "dumpDmabufInfo":Ljava/lang/Boolean;
    .restart local v8    # "kernelUsed":J
    .restart local v10    # "ionPool":J
    :cond_5
    move-object/from16 v25, v3

    move-wide/from16 v28, v10

    .line 375
    .end local v3    # "dumpDmabufInfo":Ljava/lang/Boolean;
    .end local v10    # "ionPool":J
    .restart local v25    # "dumpDmabufInfo":Ljava/lang/Boolean;
    .restart local v28    # "ionPool":J
    :goto_0
    invoke-static {}, Landroid/os/Debug;->getDmabufHeapTotalExportedKb()J

    move-result-wide v10

    .line 376
    .local v10, "totalExportedDmabufHeap":J
    const-wide/16 v17, 0x0

    cmp-long v3, v10, v17

    if-ltz v3, :cond_6

    .line 377
    const-string v3, "DMA-BUF Heap: "

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 378
    invoke-static {v10, v11}, Lcom/android/server/am/ActivityManagerService;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 379
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 380
    new-instance v3, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda9;

    invoke-direct {v3, v10, v11}, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda9;-><init>(J)V

    invoke-virtual {v1, v3}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V

    .line 384
    :cond_6
    move-wide/from16 v22, v5

    .end local v5    # "totalExportedDmabuf":J
    .local v22, "totalExportedDmabuf":J
    invoke-static {}, Landroid/os/Debug;->getDmabufHeapPoolsSizeKb()J

    move-result-wide v5

    .line 385
    .local v5, "totalDmabufHeapPool":J
    const-wide/16 v17, 0x0

    cmp-long v3, v5, v17

    if-ltz v3, :cond_7

    .line 386
    const-string v3, "DMA-BUF Heaps pool: "

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 387
    invoke-static {v5, v6}, Lcom/android/server/am/ActivityManagerService;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 388
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 389
    new-instance v3, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda10;

    invoke-direct {v3, v5, v6}, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda10;-><init>(J)V

    invoke-virtual {v1, v3}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V

    .line 393
    .end local v5    # "totalDmabufHeapPool":J
    .end local v10    # "totalExportedDmabufHeap":J
    .end local v22    # "totalExportedDmabuf":J
    :cond_7
    move-object/from16 v5, v25

    .end local v25    # "dumpDmabufInfo":Ljava/lang/Boolean;
    .local v5, "dumpDmabufInfo":Ljava/lang/Boolean;
    :goto_1
    invoke-static {}, Landroid/os/Debug;->getGpuTotalUsageKb()J

    move-result-wide v10

    .line 394
    .local v10, "gpuUsage":J
    const-wide/16 v17, 0x0

    cmp-long v3, v10, v17

    if-ltz v3, :cond_a

    .line 395
    move-wide/from16 v22, v12

    .end local v12    # "ionHeap":J
    .local v22, "ionHeap":J
    invoke-static {}, Landroid/os/Debug;->getGpuPrivateMemoryKb()J

    move-result-wide v12

    .line 396
    .local v12, "gpuPrivateUsage":J
    cmp-long v3, v12, v17

    if-ltz v3, :cond_8

    .line 397
    move-wide/from16 v17, v14

    .end local v14    # "dmabufMapped":J
    .local v17, "dmabufMapped":J
    sub-long v14, v10, v12

    .line 398
    .local v14, "gpuDmaBufUsage":J
    const-string v3, "      GPU: "

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 399
    invoke-static {v10, v11}, Lcom/android/server/am/ActivityManagerService;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 400
    const-string v3, " ("

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 401
    invoke-static {v14, v15}, Lcom/android/server/am/ActivityManagerService;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 402
    const-string v3, " dmabuf + "

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 403
    invoke-static {v12, v13}, Lcom/android/server/am/ActivityManagerService;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 404
    const-string v3, " private)\n"

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 407
    move-object v3, v5

    .end local v5    # "dumpDmabufInfo":Ljava/lang/Boolean;
    .restart local v3    # "dumpDmabufInfo":Ljava/lang/Boolean;
    iget-wide v5, v0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J

    move-object/from16 v25, v2

    move-object/from16 v26, v3

    .end local v2    # "memInfo":Lcom/android/internal/util/MemInfoReader;
    .end local v3    # "dumpDmabufInfo":Ljava/lang/Boolean;
    .local v25, "memInfo":Lcom/android/internal/util/MemInfoReader;
    .local v26, "dumpDmabufInfo":Ljava/lang/Boolean;
    iget-wide v2, v0, Lcom/android/server/am/MemUsageBuilder;->totalMemtrackGl:J

    sub-long/2addr v5, v2

    iput-wide v5, v0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J

    .line 408
    add-long/2addr v8, v12

    .line 409
    new-instance v2, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda11;

    invoke-direct {v2, v14, v15}, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda11;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V

    .line 410
    .end local v14    # "gpuDmaBufUsage":J
    move-object/from16 v6, v24

    goto :goto_2

    .line 411
    .end local v17    # "dmabufMapped":J
    .end local v25    # "memInfo":Lcom/android/internal/util/MemInfoReader;
    .end local v26    # "dumpDmabufInfo":Ljava/lang/Boolean;
    .restart local v2    # "memInfo":Lcom/android/internal/util/MemInfoReader;
    .restart local v5    # "dumpDmabufInfo":Ljava/lang/Boolean;
    .local v14, "dmabufMapped":J
    :cond_8
    move-object/from16 v25, v2

    move-object/from16 v26, v5

    move-wide/from16 v17, v14

    .end local v2    # "memInfo":Lcom/android/internal/util/MemInfoReader;
    .end local v5    # "dumpDmabufInfo":Ljava/lang/Boolean;
    .end local v14    # "dmabufMapped":J
    .restart local v17    # "dmabufMapped":J
    .restart local v25    # "memInfo":Lcom/android/internal/util/MemInfoReader;
    .restart local v26    # "dumpDmabufInfo":Ljava/lang/Boolean;
    const-string v2, "       GPU: "

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412
    invoke-static {v10, v11}, Lcom/android/server/am/ActivityManagerService;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 413
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 414
    cmp-long v2, v10, v20

    if-lez v2, :cond_9

    .line 415
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    move-object v6, v2

    .end local v24    # "dumpGpuInfo":Ljava/lang/Boolean;
    .local v2, "dumpGpuInfo":Ljava/lang/Boolean;
    goto :goto_2

    .line 414
    .end local v2    # "dumpGpuInfo":Ljava/lang/Boolean;
    .restart local v24    # "dumpGpuInfo":Ljava/lang/Boolean;
    :cond_9
    move-object/from16 v6, v24

    .line 418
    .end local v24    # "dumpGpuInfo":Ljava/lang/Boolean;
    .restart local v6    # "dumpGpuInfo":Ljava/lang/Boolean;
    :goto_2
    new-instance v2, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda12;

    invoke-direct {v2, v10, v11}, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda12;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V

    .line 419
    new-instance v2, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda1;

    invoke-direct {v2, v12, v13}, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda1;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V

    goto :goto_3

    .line 394
    .end local v6    # "dumpGpuInfo":Ljava/lang/Boolean;
    .end local v17    # "dmabufMapped":J
    .end local v22    # "ionHeap":J
    .end local v25    # "memInfo":Lcom/android/internal/util/MemInfoReader;
    .end local v26    # "dumpDmabufInfo":Ljava/lang/Boolean;
    .local v2, "memInfo":Lcom/android/internal/util/MemInfoReader;
    .restart local v5    # "dumpDmabufInfo":Ljava/lang/Boolean;
    .local v12, "ionHeap":J
    .restart local v14    # "dmabufMapped":J
    .restart local v24    # "dumpGpuInfo":Ljava/lang/Boolean;
    :cond_a
    move-object/from16 v25, v2

    move-object/from16 v26, v5

    move-wide/from16 v22, v12

    move-wide/from16 v17, v14

    .end local v2    # "memInfo":Lcom/android/internal/util/MemInfoReader;
    .end local v5    # "dumpDmabufInfo":Ljava/lang/Boolean;
    .end local v12    # "ionHeap":J
    .end local v14    # "dmabufMapped":J
    .restart local v17    # "dmabufMapped":J
    .restart local v22    # "ionHeap":J
    .restart local v25    # "memInfo":Lcom/android/internal/util/MemInfoReader;
    .restart local v26    # "dumpDmabufInfo":Ljava/lang/Boolean;
    move-object/from16 v6, v24

    .line 421
    .end local v24    # "dumpGpuInfo":Ljava/lang/Boolean;
    .restart local v6    # "dumpGpuInfo":Ljava/lang/Boolean;
    :goto_3
    const-string v2, "  Used RAM: "

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422
    iget-wide v2, v0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J

    iget-wide v12, v0, Lcom/android/server/am/MemUsageBuilder;->cachedPss:J

    sub-long/2addr v2, v12

    add-long/2addr v2, v8

    invoke-static {v2, v3}, Lcom/android/server/am/ActivityManagerService;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 424
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 428
    const-string v2, "  Lost RAM: "

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 429
    invoke-virtual/range {v25 .. v25}, Lcom/android/internal/util/MemInfoReader;->getTotalSizeKb()J

    move-result-wide v2

    iget-wide v12, v0, Lcom/android/server/am/MemUsageBuilder;->totalPss:J

    iget-wide v14, v0, Lcom/android/server/am/MemUsageBuilder;->totalSwapPss:J

    sub-long/2addr v12, v14

    sub-long/2addr v2, v12

    .line 430
    invoke-virtual/range {v25 .. v25}, Lcom/android/internal/util/MemInfoReader;->getFreeSizeKb()J

    move-result-wide v12

    sub-long/2addr v2, v12

    invoke-virtual/range {v25 .. v25}, Lcom/android/internal/util/MemInfoReader;->getCachedSizeKb()J

    move-result-wide v12

    sub-long/2addr v2, v12

    sub-long/2addr v2, v8

    .line 431
    invoke-virtual/range {v25 .. v25}, Lcom/android/internal/util/MemInfoReader;->getZramTotalSizeKb()J

    move-result-wide v12

    sub-long/2addr v2, v12

    .line 429
    invoke-static {v2, v3}, Lcom/android/server/am/ActivityManagerService;->stringifyKBSize(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 432
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 434
    move-wide v2, v8

    .line 435
    .local v2, "finalKernelUsed":J
    new-instance v5, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda2;

    move-object/from16 v12, v25

    .end local v25    # "memInfo":Lcom/android/internal/util/MemInfoReader;
    .local v12, "memInfo":Lcom/android/internal/util/MemInfoReader;
    invoke-direct {v5, v12}, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda2;-><init>(Lcom/android/internal/util/MemInfoReader;)V

    invoke-virtual {v1, v5}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V

    .line 436
    new-instance v5, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda3;

    invoke-direct {v5, v2, v3}, Lcom/android/server/am/MemUsageBuilder$$ExternalSyntheticLambda3;-><init>(J)V

    invoke-virtual {v1, v5}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V

    .line 437
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 438
    invoke-static {}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->getInstance()Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->getDmabufUsageInfo()Ljava/lang/String;

    move-result-object v5

    .line 439
    .local v5, "dmabufInfo":Ljava/lang/String;
    if-eqz v5, :cond_b

    .line 440
    const-string v13, "\n\nDMABUF usage Info:\n"

    invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 441
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 442
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 445
    .end local v5    # "dmabufInfo":Ljava/lang/String;
    :cond_b
    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_c

    .line 446
    invoke-static {}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->getInstance()Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->getGpuMemoryUsageInfo()Ljava/lang/String;

    move-result-object v5

    .line 447
    .local v5, "gpuInfo":Ljava/lang/String;
    if-eqz v5, :cond_c

    .line 448
    const-string v13, "GPU usage Info:\n"

    invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 449
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 450
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 453
    .end local v5    # "gpuInfo":Ljava/lang/String;
    :cond_c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method buildTopProcs()Ljava/lang/String;
    .locals 9

    .line 457
    iget-object v0, p0, Lcom/android/server/am/MemUsageBuilder;->memInfos:Ljava/util/ArrayList;

    new-instance v1, Lcom/android/server/am/MemUsageBuilder$2;

    invoke-direct {v1, p0}, Lcom/android/server/am/MemUsageBuilder$2;-><init>(Lcom/android/server/am/MemUsageBuilder;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 465
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 466
    .local v0, "sb":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Number of processes: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/am/MemUsageBuilder;->memInfos:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 467
    const/4 v1, 0x0

    .local v1, "i":I
    iget-object v3, p0, Lcom/android/server/am/MemUsageBuilder;->memInfos:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    .local v3, "size":I
    :goto_0
    if-ge v1, v3, :cond_0

    const/16 v4, 0xa

    if-ge v1, v4, :cond_0

    .line 468
    iget-object v4, p0, Lcom/android/server/am/MemUsageBuilder;->memInfos:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/am/ProcessMemInfo;

    .line 469
    .local v4, "mi":Lcom/android/server/am/ProcessMemInfo;
    new-instance v5, Landroid/os/Debug$MemoryInfo;

    invoke-direct {v5}, Landroid/os/Debug$MemoryInfo;-><init>()V

    .line 470
    .local v5, "info":Landroid/os/Debug$MemoryInfo;
    iget v6, v4, Lcom/android/server/am/ProcessMemInfo;->pid:I

    invoke-static {v6, v5}, Landroid/os/Debug;->getMemoryInfo(ILandroid/os/Debug$MemoryInfo;)Z

    .line 471
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "TOP "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    add-int/lit8 v7, v1, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 472
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "    Pid:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v4, Lcom/android/server/am/ProcessMemInfo;->pid:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 473
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "    Process:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v4, Lcom/android/server/am/ProcessMemInfo;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 474
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "    Pss:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-wide v7, v4, Lcom/android/server/am/ProcessMemInfo;->pss:J

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 475
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "    Java Heap Pss:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v5, Landroid/os/Debug$MemoryInfo;->dalvikPss:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 476
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "    Native Heap Pss:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v5, Landroid/os/Debug$MemoryInfo;->nativePss:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 477
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "    Graphics Pss:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v5}, Landroid/os/Debug$MemoryInfo;->getSummaryGraphics()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 478
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 467
    .end local v4    # "mi":Lcom/android/server/am/ProcessMemInfo;
    .end local v5    # "info":Landroid/os/Debug$MemoryInfo;
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 480
    .end local v1    # "i":I
    .end local v3    # "size":I
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method setScoutInfo(Lcom/android/server/am/ScoutMeminfo;)V
    .locals 0
    .param p1, "scoutInfo"    # Lcom/android/server/am/ScoutMeminfo;

    .line 76
    iput-object p1, p0, Lcom/android/server/am/MemUsageBuilder;->scoutInfo:Lcom/android/server/am/ScoutMeminfo;

    .line 77
    return-void
.end method
