public class com.android.server.am.MemoryFreezeCloud {
	 /* .source "MemoryFreezeCloud.java" */
	 /* # static fields */
	 private static final java.lang.String CLOUD_MEMFREEZE_POLICY;
	 private static final java.lang.String CLOUD_MEMFREEZE_WHITE_LIST;
	 private static final java.lang.String FILE_PATH;
	 private static final java.lang.String MEMFREEZE_POLICY_PROP;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private android.content.Context mContext;
	 private com.android.server.am.MemoryFreezeStubImpl mMemoryFreezeStubImpl;
	 /* # direct methods */
	 public com.android.server.am.MemoryFreezeCloud ( ) {
		 /* .locals 1 */
		 /* .line 43 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 50 */
		 int v0 = 0; // const/4 v0, 0x0
		 this.mMemoryFreezeStubImpl = v0;
		 /* .line 51 */
		 this.mContext = v0;
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Integer getTotalPhysicalRam ( ) {
		 /* .locals 8 */
		 /* .line 131 */
		 try { // :try_start_0
			 final String v0 = "miui.util.HardwareInfo"; // const-string v0, "miui.util.HardwareInfo"
			 java.lang.Class .forName ( v0 );
			 /* .line 132 */
			 /* .local v0, "clazz":Ljava/lang/Class; */
			 final String v1 = "getTotalPhysicalMemory"; // const-string v1, "getTotalPhysicalMemory"
			 int v2 = 0; // const/4 v2, 0x0
			 (( java.lang.Class ) v0 ).getMethod ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
			 /* .line 133 */
			 /* .local v1, "method":Ljava/lang/reflect/Method; */
			 (( java.lang.reflect.Method ) v1 ).invoke ( v2, v2 ); // invoke-virtual {v1, v2, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
			 /* check-cast v2, Ljava/lang/Long; */
			 (( java.lang.Long ) v2 ).longValue ( ); // invoke-virtual {v2}, Ljava/lang/Long;->longValue()J
			 /* move-result-wide v2 */
			 /* .line 134 */
			 /* .local v2, "totalPhysicalMemory":J */
			 /* long-to-double v4, v2 */
			 /* const-wide/high16 v6, 0x4090000000000000L # 1024.0 */
			 /* div-double/2addr v4, v6 */
			 /* div-double/2addr v4, v6 */
			 /* div-double/2addr v4, v6 */
			 java.lang.Math .ceil ( v4,v5 );
			 /* move-result-wide v4 */
			 /* :try_end_0 */
			 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* double-to-int v4, v4 */
			 /* .line 135 */
			 /* .local v4, "memory_G":I */
			 /* .line 136 */
		 } // .end local v0 # "clazz":Ljava/lang/Class;
	 } // .end local v1 # "method":Ljava/lang/reflect/Method;
} // .end local v2 # "totalPhysicalMemory":J
} // .end local v4 # "memory_G":I
/* :catch_0 */
/* move-exception v0 */
/* .line 137 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "MFZCloud"; // const-string v1, "MFZCloud"
final String v2 = "Mobile Memory not found!"; // const-string v2, "Mobile Memory not found!"
android.util.Slog .e ( v1,v2 );
/* .line 139 */
} // .end local v0 # "e":Ljava/lang/Exception;
int v0 = -1; // const/4 v0, -0x1
} // .end method
public void initCloud ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 54 */
com.android.server.am.MemoryFreezeStubImpl .getInstance ( );
this.mMemoryFreezeStubImpl = v0;
/* .line 55 */
this.mContext = p1;
/* .line 56 */
final String v1 = "MFZCloud"; // const-string v1, "MFZCloud"
/* if-nez v0, :cond_0 */
/* .line 57 */
final String v0 = "initCloud failure"; // const-string v0, "initCloud failure"
android.util.Slog .e ( v1,v0 );
/* .line 59 */
} // :cond_0
final String v0 = "initCloud success"; // const-string v0, "initCloud success"
android.util.Slog .i ( v1,v0 );
/* .line 60 */
return;
} // .end method
public void registerCloudWhiteList ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 63 */
/* new-instance v0, Lcom/android/server/am/MemoryFreezeCloud$1; */
v1 = this.mMemoryFreezeStubImpl;
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/am/MemoryFreezeCloud$1;-><init>(Lcom/android/server/am/MemoryFreezeCloud;Landroid/os/Handler;)V */
/* .line 71 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 72 */
final String v2 = "cloud_memory_freeze_whitelist"; // const-string v2, "cloud_memory_freeze_whitelist"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 71 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 73 */
return;
} // .end method
public void registerMemfreezeOperation ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 76 */
/* new-instance v0, Lcom/android/server/am/MemoryFreezeCloud$2; */
v1 = this.mMemoryFreezeStubImpl;
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/am/MemoryFreezeCloud$2;-><init>(Lcom/android/server/am/MemoryFreezeCloud;Landroid/os/Handler;)V */
/* .line 85 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 86 */
final String v2 = "cloud_memory_freeze_policy"; // const-string v2, "cloud_memory_freeze_policy"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 85 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 87 */
return;
} // .end method
public void saveToXml ( java.lang.String p0 ) {
/* .locals 13 */
/* .param p1, "operations" # Ljava/lang/String; */
/* .line 90 */
final String v0 = "designated-package"; // const-string v0, "designated-package"
final String v1 = "contents"; // const-string v1, "contents"
final String v2 = "UTF-8"; // const-string v2, "UTF-8"
final String v3 = "\n"; // const-string v3, "\n"
v4 = android.text.TextUtils .isEmpty ( p1 );
final String v5 = "MFZCloud"; // const-string v5, "MFZCloud"
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 91 */
final String v0 = "saveToXml operations is empty !"; // const-string v0, "saveToXml operations is empty !"
android.util.Slog .e ( v5,v0 );
/* .line 92 */
return;
/* .line 94 */
} // :cond_0
/* new-instance v4, Ljava/io/File; */
final String v6 = "/data/system/mfz.xml"; // const-string v6, "/data/system/mfz.xml"
/* invoke-direct {v4, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 95 */
/* .local v4, "file":Ljava/io/File; */
int v6 = 0; // const/4 v6, 0x0
/* .line 96 */
/* .local v6, "out":Ljava/io/OutputStream; */
final String v7 = ","; // const-string v7, ","
(( java.lang.String ) p1 ).split ( v7 ); // invoke-virtual {p1, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 98 */
/* .local v7, "operation":[Ljava/lang/String; */
try { // :try_start_0
android.util.Xml .newSerializer ( );
/* .line 99 */
/* .local v8, "serializer":Lorg/xmlpull/v1/XmlSerializer; */
/* new-instance v9, Ljava/io/FileOutputStream; */
/* invoke-direct {v9, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V */
/* move-object v6, v9 */
/* .line 100 */
/* .line 101 */
int v9 = 1; // const/4 v9, 0x1
java.lang.Boolean .valueOf ( v9 );
/* .line 102 */
/* .line 103 */
int v2 = 0; // const/4 v2, 0x0
/* .line 104 */
/* array-length v9, v7 */
int v10 = 0; // const/4 v10, 0x0
} // :goto_0
/* if-ge v10, v9, :cond_1 */
/* aget-object v11, v7, v10 */
/* .line 105 */
/* .local v11, "str":Ljava/lang/String; */
final String v12 = "\n\t"; // const-string v12, "\n\t"
/* .line 106 */
/* .line 107 */
final String v12 = "name"; // const-string v12, "name"
/* .line 108 */
/* .line 104 */
/* nop */
} // .end local v11 # "str":Ljava/lang/String;
/* add-int/lit8 v10, v10, 0x1 */
/* .line 110 */
} // :cond_1
/* .line 111 */
/* .line 112 */
/* .line 113 */
/* .line 114 */
(( java.io.OutputStream ) v6 ).flush ( ); // invoke-virtual {v6}, Ljava/io/OutputStream;->flush()V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 119 */
} // .end local v8 # "serializer":Lorg/xmlpull/v1/XmlSerializer;
/* nop */
/* .line 120 */
try { // :try_start_1
(( java.io.OutputStream ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/OutputStream;->close()V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 124 */
} // :cond_2
} // :goto_1
/* .line 122 */
/* :catch_0 */
/* move-exception v0 */
/* .line 123 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 125 */
} // .end local v0 # "e":Ljava/lang/Exception;
/* .line 118 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 115 */
/* :catch_1 */
/* move-exception v0 */
/* .line 116 */
/* .restart local v0 # "e":Ljava/lang/Exception; */
try { // :try_start_2
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Save xml error"; // const-string v2, "Save xml error"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).getMessage ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v5,v1 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 119 */
} // .end local v0 # "e":Ljava/lang/Exception;
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 120 */
try { // :try_start_3
(( java.io.OutputStream ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/OutputStream;->close()V
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 127 */
} // :goto_2
return;
/* .line 119 */
} // :goto_3
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 120 */
try { // :try_start_4
(( java.io.OutputStream ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/OutputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_2 */
/* .line 122 */
/* :catch_2 */
/* move-exception v1 */
/* .line 123 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 124 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :cond_3
} // :goto_4
/* nop */
/* .line 125 */
} // :goto_5
/* throw v0 */
} // .end method
public java.lang.String setParameter ( java.lang.String p0 ) {
/* .locals 10 */
/* .param p1, "operations" # Ljava/lang/String; */
/* .line 143 */
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 144 */
final String v0 = "MFZCloud"; // const-string v0, "MFZCloud"
/* const-string/jumbo v1, "setParameter operations is empty , set as default value!" */
android.util.Slog .e ( v0,v1 );
/* .line 145 */
final String v0 = "2048,3072,4096"; // const-string v0, "2048,3072,4096"
/* .line 147 */
} // :cond_0
final String v0 = "\""; // const-string v0, "\""
final String v1 = ""; // const-string v1, ""
(( java.lang.String ) p1 ).replaceAll ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
/* .line 148 */
final String v0 = "\\},"; // const-string v0, "\\},"
(( java.lang.String ) p1 ).split ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 149 */
/* .local v0, "arr":[Ljava/lang/String; */
/* new-instance v2, Ljava/util/HashMap; */
/* invoke-direct {v2}, Ljava/util/HashMap;-><init>()V */
/* .line 150 */
/* .local v2, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
int v3 = 0; // const/4 v3, 0x0
/* aget-object v4, v0, v3 */
/* aget-object v5, v0, v3 */
v5 = (( java.lang.String ) v5 ).length ( ); // invoke-virtual {v5}, Ljava/lang/String;->length()I
int v6 = 1; // const/4 v6, 0x1
(( java.lang.String ) v4 ).substring ( v6, v5 ); // invoke-virtual {v4, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* aput-object v4, v0, v3 */
/* .line 151 */
/* array-length v4, v0 */
/* sub-int/2addr v4, v6 */
/* array-length v5, v0 */
/* sub-int/2addr v5, v6 */
/* aget-object v5, v0, v5 */
/* array-length v7, v0 */
/* sub-int/2addr v7, v6 */
/* aget-object v7, v0, v7 */
v7 = (( java.lang.String ) v7 ).length ( ); // invoke-virtual {v7}, Ljava/lang/String;->length()I
/* add-int/lit8 v7, v7, -0x2 */
(( java.lang.String ) v5 ).substring ( v3, v7 ); // invoke-virtual {v5, v3, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* aput-object v5, v0, v4 */
/* .line 152 */
/* array-length v4, v0 */
/* move v5, v3 */
} // :goto_0
/* if-ge v5, v4, :cond_1 */
/* aget-object v7, v0, v5 */
/* .line 153 */
/* .local v7, "s":Ljava/lang/String; */
final String v8 = "\\}"; // const-string v8, "\\}"
(( java.lang.String ) v7 ).replace ( v8, v1 ); // invoke-virtual {v7, v8, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
/* .line 154 */
final String v8 = ":\\{"; // const-string v8, ":\\{"
(( java.lang.String ) v7 ).split ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* aget-object v9, v9, v3 */
(( java.lang.String ) v7 ).split ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* aget-object v8, v8, v6 */
/* .line 152 */
} // .end local v7 # "s":Ljava/lang/String;
/* add-int/lit8 v5, v5, 0x1 */
/* .line 156 */
} // :cond_1
v1 = (( com.android.server.am.MemoryFreezeCloud ) p0 ).getTotalPhysicalRam ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemoryFreezeCloud;->getTotalPhysicalRam()I
final String v3 = ":"; // const-string v3, ":"
/* sparse-switch v1, :sswitch_data_0 */
/* .line 173 */
final String v1 = "default"; // const-string v1, "default"
/* check-cast v1, Ljava/lang/String; */
(( java.lang.String ) v1 ).split ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* aget-object v1, v1, v6 */
/* .line 170 */
/* :sswitch_0 */
final String v1 = "24"; // const-string v1, "24"
/* check-cast v1, Ljava/lang/String; */
(( java.lang.String ) v1 ).split ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* aget-object v1, v1, v6 */
/* .line 167 */
/* :sswitch_1 */
final String v1 = "16"; // const-string v1, "16"
/* check-cast v1, Ljava/lang/String; */
(( java.lang.String ) v1 ).split ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* aget-object v1, v1, v6 */
/* .line 164 */
/* :sswitch_2 */
final String v1 = "12"; // const-string v1, "12"
/* check-cast v1, Ljava/lang/String; */
(( java.lang.String ) v1 ).split ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* aget-object v1, v1, v6 */
/* .line 161 */
/* :sswitch_3 */
final String v1 = "8"; // const-string v1, "8"
/* check-cast v1, Ljava/lang/String; */
(( java.lang.String ) v1 ).split ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* aget-object v1, v1, v6 */
/* .line 158 */
/* :sswitch_4 */
final String v1 = "6"; // const-string v1, "6"
/* check-cast v1, Ljava/lang/String; */
(( java.lang.String ) v1 ).split ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* aget-object v1, v1, v6 */
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x6 -> :sswitch_4 */
/* 0x8 -> :sswitch_3 */
/* 0xc -> :sswitch_2 */
/* 0x10 -> :sswitch_1 */
/* 0x18 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public void updateCloudWhiteList ( ) {
/* .locals 3 */
/* .line 179 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "cloud_memory_freeze_whitelist"; // const-string v1, "cloud_memory_freeze_whitelist"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 181 */
v0 = this.mContext;
/* .line 182 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 181 */
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 185 */
/* .local v0, "enableStr":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v1, :cond_0 */
/* .line 186 */
(( com.android.server.am.MemoryFreezeCloud ) p0 ).saveToXml ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/am/MemoryFreezeCloud;->saveToXml(Ljava/lang/String;)V
/* .line 189 */
} // .end local v0 # "enableStr":Ljava/lang/String;
} // :cond_0
return;
} // .end method
public void updateMemfreezeOperation ( ) {
/* .locals 3 */
/* .line 192 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "cloud_memory_freeze_policy"; // const-string v1, "cloud_memory_freeze_policy"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 194 */
v0 = this.mContext;
/* .line 195 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 194 */
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 198 */
/* .local v0, "policy":Ljava/lang/String; */
final String v1 = "persist.sys.mfz.mem_policy"; // const-string v1, "persist.sys.mfz.mem_policy"
(( com.android.server.am.MemoryFreezeCloud ) p0 ).setParameter ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/am/MemoryFreezeCloud;->setParameter(Ljava/lang/String;)Ljava/lang/String;
android.os.SystemProperties .set ( v1,v2 );
/* .line 200 */
} // .end local v0 # "policy":Ljava/lang/String;
} // :cond_0
return;
} // .end method
