.class Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;
.super Ljava/lang/Object;
.source "MiuiBatteryStatsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MiuiBatteryStatsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BatteryTempLevelInfo"
.end annotation


# instance fields
.field private mHighLevelCount:I

.field private mLastSatisfyTempLevelCondition:Z

.field private mlowLevelCount:I

.field final synthetic this$0:Lcom/android/server/MiuiBatteryStatsService;


# direct methods
.method static bridge synthetic -$$Nest$mcycleCheck(Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->cycleCheck()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mdataReset(Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->dataReset()V

    return-void
.end method

.method constructor <init>(Lcom/android/server/MiuiBatteryStatsService;)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/server/MiuiBatteryStatsService;

    .line 1401
    iput-object p1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1402
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mHighLevelCount:I

    .line 1403
    iput v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mlowLevelCount:I

    .line 1404
    iput-boolean v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mLastSatisfyTempLevelCondition:Z

    return-void
.end method

.method private cycleCheck()V
    .locals 7

    .line 1407
    const/4 v0, 0x0

    .line 1408
    .local v0, "btValue":I
    const/4 v1, 0x0

    .line 1409
    .local v1, "tlValue":I
    iget-object v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/util/IMiCharge;->getBatteryTbat()Ljava/lang/String;

    move-result-object v2

    .line 1410
    .local v2, "bt":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1411
    iget-object v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1413
    :cond_0
    iget-object v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmPlugType(Lcom/android/server/MiuiBatteryStatsService;)I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 1414
    iget-object v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v3

    const-string/jumbo v4, "wlscharge_control_limit"

    invoke-virtual {v3, v4}, Lmiui/util/IMiCharge;->getMiChargePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1415
    .local v3, "tl":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1416
    iget-object v4, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v4}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1418
    .end local v3    # "tl":Ljava/lang/String;
    :cond_1
    goto :goto_0

    .line 1419
    :cond_2
    iget-object v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge;

    move-result-object v3

    invoke-virtual {v3}, Lmiui/util/IMiCharge;->getBatteryThermaLevel()Ljava/lang/String;

    move-result-object v3

    .line 1420
    .restart local v3    # "tl":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 1421
    iget-object v4, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v4}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1424
    .end local v3    # "tl":Ljava/lang/String;
    :cond_3
    :goto_0
    iget-object v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryStatsService;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1425
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "batteryTbatValue = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " thermalLevelValue = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "MiuiBatteryStatsService"

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1427
    :cond_4
    const/16 v3, 0x1ae

    const/4 v4, 0x1

    if-lt v0, v3, :cond_5

    const/16 v3, 0x1d6

    if-gt v0, v3, :cond_5

    const/16 v3, 0xd

    if-lt v1, v3, :cond_5

    .line 1428
    iget v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mHighLevelCount:I

    add-int/2addr v3, v4

    iput v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mHighLevelCount:I

    .line 1430
    :cond_5
    iget v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mHighLevelCount:I

    const/16 v5, 0xa

    const/4 v6, 0x0

    if-lt v3, v5, :cond_6

    .line 1431
    invoke-static {v4}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$sfputmIsSatisfyTempLevelCondition(Z)V

    .line 1432
    iput v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mHighLevelCount:I

    .line 1434
    :cond_6
    invoke-static {}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$sfgetmIsSatisfyTempLevelCondition()Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0x17c

    if-gt v0, v3, :cond_7

    const/16 v3, 0x8

    if-gt v1, v3, :cond_7

    .line 1435
    iget v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mlowLevelCount:I

    add-int/2addr v3, v4

    iput v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mlowLevelCount:I

    .line 1437
    :cond_7
    iget v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mlowLevelCount:I

    if-lt v3, v5, :cond_8

    .line 1438
    invoke-static {v6}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$sfputmIsSatisfyTempLevelCondition(Z)V

    .line 1439
    iput v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mlowLevelCount:I

    .line 1442
    :cond_8
    invoke-static {}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$sfgetmIsSatisfyTempLevelCondition()Z

    move-result v3

    if-eqz v3, :cond_9

    iget-boolean v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mLastSatisfyTempLevelCondition:Z

    if-eqz v3, :cond_a

    :cond_9
    invoke-static {}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$sfgetmIsSatisfyTempLevelCondition()Z

    move-result v3

    if-nez v3, :cond_b

    iget-boolean v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mLastSatisfyTempLevelCondition:Z

    if-eqz v3, :cond_b

    .line 1444
    :cond_a
    iget-object v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v3

    invoke-static {v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->-$$Nest$msendAdjustVolBroadcast(Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;)V

    .line 1445
    invoke-static {}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$sfgetmIsSatisfyTempLevelCondition()Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mLastSatisfyTempLevelCondition:Z

    .line 1447
    :cond_b
    return-void
.end method

.method private dataReset()V
    .locals 1

    .line 1450
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mHighLevelCount:I

    .line 1451
    iput v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mlowLevelCount:I

    .line 1452
    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$sfputmIsSatisfyTempLevelCondition(Z)V

    .line 1453
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryStatsService;)Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->-$$Nest$msendAdjustVolBroadcast(Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;)V

    .line 1454
    return-void
.end method
