public class com.android.server.DarkModeStatusTracker {
	 /* .source "DarkModeStatusTracker.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/DarkModeStatusTracker$SettingsObserver; */
	 /* } */
} // .end annotation
/* # static fields */
public static Boolean DEBUG;
private static final Long ONE_MINUTE;
private static volatile com.android.server.DarkModeStatusTracker sInstance;
/* # instance fields */
private final java.lang.String TAG;
private android.app.AlarmManager$OnAlarmListener mAlarmListener;
private android.app.AlarmManager mAlarmManager;
private android.content.Context mContext;
private android.os.Handler mDarkModeStatusHandler;
private com.android.server.DarkModeStauesEvent mDarkModeSwitchEvent;
private com.android.server.ForceDarkAppListManager mForceDarkAppListManager;
private android.content.pm.PackageManager mPackageManager;
private android.database.ContentObserver mSettingsObserver;
/* # direct methods */
static android.content.Context -$$Nest$fgetmContext ( com.android.server.DarkModeStatusTracker p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContext;
} // .end method
static void -$$Nest$msetUploadDarkModeSwitchAlarm ( com.android.server.DarkModeStatusTracker p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/DarkModeStatusTracker;->setUploadDarkModeSwitchAlarm(Z)V */
	 return;
} // .end method
static void -$$Nest$muploadDarkModeStatusEvent ( com.android.server.DarkModeStatusTracker p0, java.lang.String p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/DarkModeStatusTracker;->uploadDarkModeStatusEvent(Ljava/lang/String;)V */
	 return;
} // .end method
static com.android.server.DarkModeStatusTracker ( ) {
	 /* .locals 1 */
	 /* .line 76 */
	 int v0 = 0; // const/4 v0, 0x0
	 com.android.server.DarkModeStatusTracker.DEBUG = (v0!= 0);
	 return;
} // .end method
public com.android.server.DarkModeStatusTracker ( ) {
	 /* .locals 1 */
	 /* .line 74 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 75 */
	 final String v0 = "DarkModeStatusTracker"; // const-string v0, "DarkModeStatusTracker"
	 this.TAG = v0;
	 /* .line 307 */
	 /* new-instance v0, Lcom/android/server/DarkModeStatusTracker$2; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/DarkModeStatusTracker$2;-><init>(Lcom/android/server/DarkModeStatusTracker;)V */
	 this.mAlarmListener = v0;
	 return;
} // .end method
public static com.android.server.DarkModeStatusTracker getIntance ( ) {
	 /* .locals 2 */
	 /* .line 89 */
	 v0 = com.android.server.DarkModeStatusTracker.sInstance;
	 /* if-nez v0, :cond_1 */
	 /* .line 90 */
	 /* const-class v0, Lcom/android/server/DarkModeStatusTracker; */
	 /* monitor-enter v0 */
	 /* .line 91 */
	 try { // :try_start_0
		 v1 = com.android.server.DarkModeStatusTracker.sInstance;
		 /* if-nez v1, :cond_0 */
		 /* .line 92 */
		 /* new-instance v1, Lcom/android/server/DarkModeStatusTracker; */
		 /* invoke-direct {v1}, Lcom/android/server/DarkModeStatusTracker;-><init>()V */
		 /* .line 94 */
	 } // :cond_0
	 /* monitor-exit v0 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
	 /* .line 96 */
} // :cond_1
} // :goto_0
v0 = com.android.server.DarkModeStatusTracker.sInstance;
} // .end method
private java.util.List initDarkModeAppList ( ) {
/* .locals 9 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 113 */
v0 = this.mForceDarkAppListManager;
/* const-wide/16 v1, 0x0 */
v3 = android.os.UserHandle .myUserId ( );
(( com.android.server.ForceDarkAppListManager ) v0 ).getDarkModeAppList ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/ForceDarkAppListManager;->getDarkModeAppList(JI)Lcom/miui/darkmode/DarkModeAppData;
/* .line 114 */
/* .local v0, "mDarkModeAppData":Lcom/miui/darkmode/DarkModeAppData; */
(( com.miui.darkmode.DarkModeAppData ) v0 ).getDarkModeAppDetailInfoList ( ); // invoke-virtual {v0}, Lcom/miui/darkmode/DarkModeAppData;->getDarkModeAppDetailInfoList()Ljava/util/List;
/* .line 115 */
/* .local v1, "appInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/darkmode/DarkModeAppDetailInfo;>;" */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* .line 116 */
v3 = /* .local v2, "appList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* if-nez v3, :cond_1 */
/* .line 119 */
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_1
/* check-cast v4, Lcom/miui/darkmode/DarkModeAppDetailInfo; */
/* .line 121 */
/* .local v4, "info":Lcom/miui/darkmode/DarkModeAppDetailInfo; */
try { // :try_start_0
/* new-instance v5, Ljava/util/HashMap; */
/* invoke-direct {v5}, Ljava/util/HashMap;-><init>()V */
/* .line 122 */
/* .local v5, "appStatus":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
v6 = this.mPackageManager;
(( com.miui.darkmode.DarkModeAppDetailInfo ) v4 ).getPkgName ( ); // invoke-virtual {v4}, Lcom/miui/darkmode/DarkModeAppDetailInfo;->getPkgName()Ljava/lang/String;
int v8 = 0; // const/4 v8, 0x0
(( android.content.pm.PackageManager ) v6 ).getPackageInfo ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
v6 = this.applicationInfo;
v7 = this.mPackageManager;
/* .line 123 */
(( android.content.pm.ApplicationInfo ) v6 ).loadLabel ( v7 ); // invoke-virtual {v6, v7}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
/* .line 124 */
/* .local v6, "appName":Ljava/lang/String; */
final String v7 = "app_package_name"; // const-string v7, "app_package_name"
(( com.miui.darkmode.DarkModeAppDetailInfo ) v4 ).getPkgName ( ); // invoke-virtual {v4}, Lcom/miui/darkmode/DarkModeAppDetailInfo;->getPkgName()Ljava/lang/String;
/* .line 125 */
final String v7 = "app_name"; // const-string v7, "app_name"
/* .line 126 */
final String v7 = "app_switch_status"; // const-string v7, "app_switch_status"
v8 = (( com.miui.darkmode.DarkModeAppDetailInfo ) v4 ).isEnabled ( ); // invoke-virtual {v4}, Lcom/miui/darkmode/DarkModeAppDetailInfo;->isEnabled()Z
if ( v8 != null) { // if-eqz v8, :cond_0
/* const-string/jumbo v8, "\u5f00" */
} // :cond_0
/* const-string/jumbo v8, "\u5173" */
} // :goto_1
/* .line 127 */
(( java.lang.Object ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;
/* :try_end_0 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 130 */
/* .line 128 */
} // .end local v5 # "appStatus":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
} // .end local v6 # "appName":Ljava/lang/String;
/* :catch_0 */
/* move-exception v5 */
/* .line 129 */
/* .local v5, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
(( android.content.pm.PackageManager$NameNotFoundException ) v5 ).printStackTrace ( ); // invoke-virtual {v5}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V
/* .line 131 */
} // .end local v4 # "info":Lcom/miui/darkmode/DarkModeAppDetailInfo;
} // .end local v5 # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
} // :goto_2
/* .line 133 */
} // :cond_1
} // .end method
private void putTimeModeStatus ( com.android.server.DarkModeStauesEvent p0 ) {
/* .locals 3 */
/* .param p1, "event" # Lcom/android/server/DarkModeStauesEvent; */
/* .line 276 */
v0 = this.mContext;
v0 = com.android.server.DarkModeTimeModeHelper .isDarkModeTimeEnable ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* const-string/jumbo v0, "\u5f00" */
} // :cond_0
/* const-string/jumbo v0, "\u5173" */
} // :goto_0
(( com.android.server.DarkModeStauesEvent ) p1 ).setTimeModeStatus ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setTimeModeStatus(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 277 */
v0 = this.mContext;
v0 = com.android.server.DarkModeTimeModeHelper .isDarkModeTimeEnable ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 278 */
v0 = this.mContext;
v0 = com.android.server.DarkModeTimeModeHelper .isSuntimeType ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 279 */
/* const-string/jumbo v0, "\u65e5\u51fa\u65e5\u843d\u6a21\u5f0f" */
} // :cond_1
/* const-string/jumbo v0, "\u81ea\u5b9a\u4e49\u65f6\u95f4" */
/* .line 278 */
} // :goto_1
(( com.android.server.DarkModeStauesEvent ) p1 ).setTimeModePattern ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setTimeModePattern(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 280 */
v0 = this.mContext;
v0 = com.android.server.DarkModeTimeModeHelper .isSuntimeType ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 281 */
v0 = this.mContext;
v0 = com.android.server.DarkModeTimeModeHelper .getSunSetTime ( v0 );
} // :cond_2
v0 = this.mContext;
v0 = com.android.server.DarkModeTimeModeHelper .getDarkModeStartTime ( v0 );
/* .line 282 */
/* .local v0, "startTime":I */
} // :goto_2
v1 = this.mContext;
v1 = com.android.server.DarkModeTimeModeHelper .isSuntimeType ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 283 */
v1 = this.mContext;
v1 = com.android.server.DarkModeTimeModeHelper .getSunRiseTime ( v1 );
} // :cond_3
v1 = this.mContext;
v1 = com.android.server.DarkModeTimeModeHelper .getDarkModeEndTime ( v1 );
/* .line 284 */
/* .local v1, "endTime":I */
} // :goto_3
com.android.server.DarkModeTimeModeHelper .getTimeInString ( v0 );
(( com.android.server.DarkModeStauesEvent ) p1 ).setBeginTime ( v2 ); // invoke-virtual {p1, v2}, Lcom/android/server/DarkModeStauesEvent;->setBeginTime(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 285 */
com.android.server.DarkModeTimeModeHelper .getTimeInString ( v1 );
(( com.android.server.DarkModeStauesEvent ) p1 ).setEndTime ( v2 ); // invoke-virtual {p1, v2}, Lcom/android/server/DarkModeStauesEvent;->setEndTime(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 287 */
} // .end local v0 # "startTime":I
} // .end local v1 # "endTime":I
} // :cond_4
return;
} // .end method
private void registerSettingsObserver ( ) {
/* .locals 5 */
/* .line 137 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 138 */
/* .local v0, "observer":Landroid/content/ContentResolver; */
final String v1 = "dark_mode_enable"; // const-string v1, "dark_mode_enable"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
int v3 = 0; // const/4 v3, 0x0
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 140 */
final String v1 = "dark_mode_time_enable"; // const-string v1, "dark_mode_time_enable"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 142 */
final String v1 = "dark_mode_time_type"; // const-string v1, "dark_mode_time_type"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 144 */
final String v1 = "dark_mode_contrast_enable"; // const-string v1, "dark_mode_contrast_enable"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 146 */
final String v1 = "last_app_dark_mode_pkg"; // const-string v1, "last_app_dark_mode_pkg"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 148 */
android.provider.MiuiSettings$SettingsCloudData .getCloudDataNotifyUri ( );
/* new-instance v2, Lcom/android/server/DarkModeStatusTracker$1; */
/* .line 149 */
com.android.server.MiuiBgThread .getHandler ( );
/* invoke-direct {v2, p0, v4}, Lcom/android/server/DarkModeStatusTracker$1;-><init>(Lcom/android/server/DarkModeStatusTracker;Landroid/os/Handler;)V */
/* .line 148 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 157 */
return;
} // .end method
private void setUploadDarkModeSwitchAlarm ( Boolean p0 ) {
/* .locals 11 */
/* .param p1, "init" # Z */
/* .line 296 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* .line 298 */
/* .local v0, "nowTime":J */
/* sget-boolean v2, Lcom/android/server/DarkModeStatusTracker;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* const-wide/32 v2, 0xea60 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
/* const-wide/32 v2, 0x1b7740 */
/* .line 299 */
} // :cond_1
/* const-wide/32 v2, 0x5265c00 */
} // :goto_0
/* add-long/2addr v2, v0 */
/* .line 300 */
/* .local v2, "nextTime":J */
v4 = this.mAlarmManager;
int v5 = 1; // const/4 v5, 0x1
/* const-string/jumbo v8, "upload_dark_mode_switch" */
v9 = this.mAlarmListener;
v10 = this.mDarkModeStatusHandler;
/* move-wide v6, v2 */
/* invoke-virtual/range {v4 ..v10}, Landroid/app/AlarmManager;->setExact(IJLjava/lang/String;Landroid/app/AlarmManager$OnAlarmListener;Landroid/os/Handler;)V */
/* .line 302 */
/* sget-boolean v4, Lcom/android/server/DarkModeStatusTracker;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 303 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "setUploadDarkModeSwitchAlarm nextTime = " */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
android.util.TimeUtils .formatDuration ( v2,v3 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "DarkModeStatusTracker"; // const-string v5, "DarkModeStatusTracker"
android.util.Slog .d ( v5,v4 );
/* .line 305 */
} // :cond_2
return;
} // .end method
private void updateAppSetting ( com.android.server.DarkModeStauesEvent p0 ) {
/* .locals 7 */
/* .param p1, "event" # Lcom/android/server/DarkModeStauesEvent; */
/* .line 237 */
/* const-string/jumbo v0, "setting" */
(( com.android.server.DarkModeStauesEvent ) p1 ).setEventName ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setEventName(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 238 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "last_app_dark_mode_pkg"; // const-string v1, "last_app_dark_mode_pkg"
android.provider.Settings$System .getString ( v0,v1 );
/* .line 239 */
/* .local v0, "appSetting":Ljava/lang/String; */
final String v1 = "DarkModeStatusTracker"; // const-string v1, "DarkModeStatusTracker"
/* if-nez v0, :cond_0 */
/* .line 240 */
final String v2 = "get app setting error"; // const-string v2, "get app setting error"
android.util.Slog .i ( v1,v2 );
/* .line 241 */
return;
/* .line 243 */
} // :cond_0
final String v2 = ":"; // const-string v2, ":"
(( java.lang.String ) v0 ).split ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 244 */
/* .local v2, "appInfo":[Ljava/lang/String; */
if ( v2 != null) { // if-eqz v2, :cond_3
/* array-length v3, v2 */
int v4 = 2; // const/4 v4, 0x2
/* if-eq v3, v4, :cond_1 */
/* .line 248 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
/* aget-object v3, v2, v1 */
/* .line 249 */
/* .local v3, "appPkg":Ljava/lang/String; */
int v4 = 1; // const/4 v4, 0x1
/* aget-object v4, v2, v4 */
/* .line 250 */
/* .local v4, "appEnable":Ljava/lang/String; */
int v5 = 0; // const/4 v5, 0x0
/* .line 252 */
/* .local v5, "appName":Ljava/lang/String; */
try { // :try_start_0
v6 = this.mPackageManager;
(( android.content.pm.PackageManager ) v6 ).getPackageInfo ( v3, v1 ); // invoke-virtual {v6, v3, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
v1 = this.applicationInfo;
v6 = this.mPackageManager;
(( android.content.pm.ApplicationInfo ) v1 ).loadLabel ( v6 ); // invoke-virtual {v1, v6}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
/* :try_end_0 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v5, v1 */
/* .line 255 */
/* .line 253 */
/* :catch_0 */
/* move-exception v1 */
/* .line 254 */
/* .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
(( android.content.pm.PackageManager$NameNotFoundException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V
/* .line 256 */
} // .end local v1 # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
} // :goto_0
(( com.android.server.DarkModeStauesEvent ) p1 ).setAppName ( v5 ); // invoke-virtual {p1, v5}, Lcom/android/server/DarkModeStauesEvent;->setAppName(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 257 */
(( com.android.server.DarkModeStauesEvent ) p1 ).setAppPkg ( v3 ); // invoke-virtual {p1, v3}, Lcom/android/server/DarkModeStauesEvent;->setAppPkg(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 258 */
/* const-string/jumbo v1, "true" */
v1 = (( java.lang.String ) v1 ).equals ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* const-string/jumbo v1, "\u5f00" */
} // :cond_2
/* const-string/jumbo v1, "\u5173" */
} // :goto_1
(( com.android.server.DarkModeStauesEvent ) p1 ).setAppEnable ( v1 ); // invoke-virtual {p1, v1}, Lcom/android/server/DarkModeStauesEvent;->setAppEnable(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 259 */
return;
/* .line 245 */
} // .end local v3 # "appPkg":Ljava/lang/String;
} // .end local v4 # "appEnable":Ljava/lang/String;
} // .end local v5 # "appName":Ljava/lang/String;
} // :cond_3
} // :goto_2
final String v3 = "get app setting info error"; // const-string v3, "get app setting info error"
android.util.Slog .i ( v1,v3 );
/* .line 246 */
return;
} // .end method
private void updateAppStatus ( com.android.server.DarkModeStauesEvent p0 ) {
/* .locals 1 */
/* .param p1, "event" # Lcom/android/server/DarkModeStauesEvent; */
/* .line 213 */
v0 = this.mContext;
v0 = com.android.server.DarkModeTimeModeHelper .isDarkModeOpen ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 214 */
/* const-string/jumbo v0, "status" */
(( com.android.server.DarkModeStauesEvent ) p1 ).setEventName ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setEventName(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 215 */
/* invoke-direct {p0}, Lcom/android/server/DarkModeStatusTracker;->initDarkModeAppList()Ljava/util/List; */
(( com.android.server.DarkModeStauesEvent ) p1 ).setAppList ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setAppList(Ljava/util/List;)Lcom/android/server/DarkModeStauesEvent;
/* .line 217 */
} // :cond_0
return;
} // .end method
private void updateAutoSwitch ( com.android.server.DarkModeStauesEvent p0 ) {
/* .locals 1 */
/* .param p1, "event" # Lcom/android/server/DarkModeStauesEvent; */
/* .line 270 */
final String v0 = "auto_switch"; // const-string v0, "auto_switch"
(( com.android.server.DarkModeStauesEvent ) p1 ).setEventName ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setEventName(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 271 */
v0 = this.mContext;
v0 = com.android.server.DarkModeTimeModeHelper .isDarkModeEnable ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 272 */
/* const-string/jumbo v0, "\u4ece\u6d45\u5230\u6df1" */
} // :cond_0
/* const-string/jumbo v0, "\u4ece\u6df1\u5230\u6d45" */
/* .line 271 */
} // :goto_0
(( com.android.server.DarkModeStauesEvent ) p1 ).setAutoSwitch ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setAutoSwitch(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 273 */
return;
} // .end method
private void updateContrastSetting ( com.android.server.DarkModeStauesEvent p0 ) {
/* .locals 1 */
/* .param p1, "event" # Lcom/android/server/DarkModeStauesEvent; */
/* .line 229 */
/* const-string/jumbo v0, "setting" */
(( com.android.server.DarkModeStauesEvent ) p1 ).setEventName ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setEventName(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 230 */
v0 = this.mContext;
v0 = com.android.server.DarkModeTimeModeHelper .isDarkModeOpen ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 231 */
v0 = this.mContext;
v0 = com.android.server.DarkModeTimeModeHelper .isDarkModeContrastEnable ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 232 */
/* const-string/jumbo v0, "\u5f00" */
} // :cond_0
/* const-string/jumbo v0, "\u5173" */
/* .line 231 */
} // :goto_0
(( com.android.server.DarkModeStauesEvent ) p1 ).setContrastStatus ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setContrastStatus(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 234 */
} // :cond_1
return;
} // .end method
private void updateDarkModeSetting ( com.android.server.DarkModeStauesEvent p0 ) {
/* .locals 3 */
/* .param p1, "event" # Lcom/android/server/DarkModeStauesEvent; */
/* .line 262 */
/* const-string/jumbo v0, "setting" */
(( com.android.server.DarkModeStauesEvent ) p1 ).setEventName ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setEventName(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 263 */
v0 = this.mContext;
v0 = com.android.server.DarkModeTimeModeHelper .isDarkModeEnable ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 264 */
/* const-string/jumbo v0, "\u5f00" */
} // :cond_0
/* const-string/jumbo v0, "\u5173" */
/* .line 263 */
} // :goto_0
(( com.android.server.DarkModeStauesEvent ) p1 ).setDarkModeStatus ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setDarkModeStatus(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 265 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "open_dark_mode_channel"; // const-string v1, "open_dark_mode_channel"
int v2 = 0; // const/4 v2, 0x0
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
(( com.android.server.DarkModeStauesEvent ) p1 ).setSettingChannel ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setSettingChannel(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 267 */
return;
} // .end method
private void updateDarkModeStatus ( com.android.server.DarkModeStauesEvent p0 ) {
/* .locals 3 */
/* .param p1, "event" # Lcom/android/server/DarkModeStauesEvent; */
/* .line 202 */
/* const-string/jumbo v0, "status" */
(( com.android.server.DarkModeStauesEvent ) p1 ).setEventName ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setEventName(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 203 */
v0 = this.mContext;
v0 = com.android.server.DarkModeTimeModeHelper .isDarkModeEnable ( v0 );
/* const-string/jumbo v1, "\u5f00" */
/* const-string/jumbo v2, "\u5173" */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 204 */
/* move-object v0, v1 */
} // :cond_0
/* move-object v0, v2 */
/* .line 203 */
} // :goto_0
(( com.android.server.DarkModeStauesEvent ) p1 ).setDarkModeStatus ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setDarkModeStatus(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 205 */
/* invoke-direct {p0, p1}, Lcom/android/server/DarkModeStatusTracker;->putTimeModeStatus(Lcom/android/server/DarkModeStauesEvent;)V */
/* .line 206 */
v0 = this.mContext;
v0 = com.android.server.DarkModeTimeModeHelper .isDarkModeOpen ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 207 */
v0 = this.mContext;
v0 = com.android.server.DarkModeTimeModeHelper .isDarkModeContrastEnable ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 208 */
} // :cond_1
/* move-object v1, v2 */
/* .line 207 */
} // :goto_1
(( com.android.server.DarkModeStauesEvent ) p1 ).setContrastStatus ( v1 ); // invoke-virtual {p1, v1}, Lcom/android/server/DarkModeStauesEvent;->setContrastStatus(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 210 */
} // :cond_2
return;
} // .end method
private com.android.server.DarkModeStauesEvent updateSwitchEvent ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "tip" # Ljava/lang/String; */
/* .line 169 */
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 170 */
int v0 = 0; // const/4 v0, 0x0
/* .line 172 */
} // :cond_0
/* new-instance v0, Lcom/android/server/DarkModeStauesEvent; */
/* invoke-direct {v0}, Lcom/android/server/DarkModeStauesEvent;-><init>()V */
(( com.android.server.DarkModeStauesEvent ) v0 ).setTip ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/DarkModeStauesEvent;->setTip(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 173 */
/* .local v0, "event":Lcom/android/server/DarkModeStauesEvent; */
v1 = (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
/* sparse-switch v1, :sswitch_data_0 */
} // :cond_1
/* :sswitch_0 */
final String v1 = "577.5.0.1.23096"; // const-string v1, "577.5.0.1.23096"
v1 = (( java.lang.String ) p1 ).equals ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
int v1 = 6; // const/4 v1, 0x6
/* :sswitch_1 */
final String v1 = "577.3.2.1.23093"; // const-string v1, "577.3.2.1.23093"
v1 = (( java.lang.String ) p1 ).equals ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
int v1 = 3; // const/4 v1, 0x3
/* :sswitch_2 */
final String v1 = "577.1.2.1.23090"; // const-string v1, "577.1.2.1.23090"
v1 = (( java.lang.String ) p1 ).equals ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
int v1 = 1; // const/4 v1, 0x1
/* :sswitch_3 */
final String v1 = "577.3.3.1.23094"; // const-string v1, "577.3.3.1.23094"
v1 = (( java.lang.String ) p1 ).equals ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
int v1 = 4; // const/4 v1, 0x4
/* :sswitch_4 */
final String v1 = "577.4.0.1.23106"; // const-string v1, "577.4.0.1.23106"
v1 = (( java.lang.String ) p1 ).equals ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
int v1 = 5; // const/4 v1, 0x5
/* :sswitch_5 */
final String v1 = "577.2.0.1.23091"; // const-string v1, "577.2.0.1.23091"
v1 = (( java.lang.String ) p1 ).equals ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
int v1 = 2; // const/4 v1, 0x2
/* :sswitch_6 */
final String v1 = "577.1.1.1.23089"; // const-string v1, "577.1.1.1.23089"
v1 = (( java.lang.String ) p1 ).equals ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
int v1 = -1; // const/4 v1, -0x1
} // :goto_1
/* packed-switch v1, :pswitch_data_0 */
/* .line 193 */
/* :pswitch_0 */
/* invoke-direct {p0, v0}, Lcom/android/server/DarkModeStatusTracker;->updateAutoSwitch(Lcom/android/server/DarkModeStauesEvent;)V */
/* .line 194 */
/* .line 190 */
/* :pswitch_1 */
/* invoke-direct {p0, v0}, Lcom/android/server/DarkModeStatusTracker;->updateDarkModeSetting(Lcom/android/server/DarkModeStauesEvent;)V */
/* .line 191 */
/* .line 187 */
/* :pswitch_2 */
/* invoke-direct {p0, v0}, Lcom/android/server/DarkModeStatusTracker;->updateAppSetting(Lcom/android/server/DarkModeStauesEvent;)V */
/* .line 188 */
/* .line 184 */
/* :pswitch_3 */
/* invoke-direct {p0, v0}, Lcom/android/server/DarkModeStatusTracker;->updateContrastSetting(Lcom/android/server/DarkModeStauesEvent;)V */
/* .line 185 */
/* .line 181 */
/* :pswitch_4 */
/* invoke-direct {p0, v0}, Lcom/android/server/DarkModeStatusTracker;->updateTimeModeSetting(Lcom/android/server/DarkModeStauesEvent;)V */
/* .line 182 */
/* .line 178 */
/* :pswitch_5 */
/* invoke-direct {p0, v0}, Lcom/android/server/DarkModeStatusTracker;->updateAppStatus(Lcom/android/server/DarkModeStauesEvent;)V */
/* .line 179 */
/* .line 175 */
/* :pswitch_6 */
/* invoke-direct {p0, v0}, Lcom/android/server/DarkModeStatusTracker;->updateDarkModeStatus(Lcom/android/server/DarkModeStauesEvent;)V */
/* .line 176 */
/* nop */
/* .line 198 */
} // :goto_2
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x5ce9f380 -> :sswitch_6 */
/* -0x5c49afa9 -> :sswitch_5 */
/* -0x32804778 -> :sswitch_4 */
/* -0xa97afe2 -> :sswitch_3 */
/* 0x375a7b97 -> :sswitch_2 */
/* 0x6123e11c -> :sswitch_1 */
/* 0x6264689f -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void updateTimeModeSetting ( com.android.server.DarkModeStauesEvent p0 ) {
/* .locals 2 */
/* .param p1, "event" # Lcom/android/server/DarkModeStauesEvent; */
/* .line 220 */
/* const-string/jumbo v0, "setting" */
(( com.android.server.DarkModeStauesEvent ) p1 ).setEventName ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setEventName(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 221 */
/* invoke-direct {p0, p1}, Lcom/android/server/DarkModeStatusTracker;->putTimeModeStatus(Lcom/android/server/DarkModeStauesEvent;)V */
/* .line 222 */
v0 = this.mContext;
v0 = com.android.server.DarkModeTimeModeHelper .isDarkModeTimeEnable ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mContext;
/* .line 223 */
v0 = com.android.server.DarkModeTimeModeHelper .isSuntimeType ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 224 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "open_sun_time_channel"; // const-string v1, "open_sun_time_channel"
android.provider.Settings$System .getString ( v0,v1 );
(( com.android.server.DarkModeStauesEvent ) p1 ).setSettingChannel ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/DarkModeStauesEvent;->setSettingChannel(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 226 */
} // :cond_0
return;
} // .end method
private void uploadDarkModeStatusEvent ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "eventTip" # Ljava/lang/String; */
/* .line 161 */
/* invoke-direct {p0, p1}, Lcom/android/server/DarkModeStatusTracker;->updateSwitchEvent(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent; */
this.mDarkModeSwitchEvent = v0;
/* .line 162 */
v1 = this.mContext;
/* invoke-direct {p0, v1, v0}, Lcom/android/server/DarkModeStatusTracker;->uploadSwitchToOnetrack(Landroid/content/Context;Lcom/android/server/DarkModeEvent;)V */
/* .line 163 */
/* sget-boolean v0, Lcom/android/server/DarkModeStatusTracker;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 164 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "uploadDarkModeStatusEvent " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mDarkModeSwitchEvent;
(( com.android.server.DarkModeStauesEvent ) v1 ).toString ( ); // invoke-virtual {v1}, Lcom/android/server/DarkModeStauesEvent;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "DarkModeStatusTracker"; // const-string v1, "DarkModeStatusTracker"
android.util.Slog .d ( v1,v0 );
/* .line 166 */
} // :cond_0
return;
} // .end method
private void uploadSwitchToOnetrack ( android.content.Context p0, com.android.server.DarkModeEvent p1 ) {
/* .locals 0 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "event" # Lcom/android/server/DarkModeEvent; */
/* .line 290 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 291 */
com.android.server.DarkModeOneTrackHelper .uploadToOneTrack ( p1,p2 );
/* .line 293 */
} // :cond_0
return;
} // .end method
/* # virtual methods */
public void init ( android.content.Context p0, com.android.server.ForceDarkAppListManager p1 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "forceDarkAppListManager" # Lcom/android/server/ForceDarkAppListManager; */
/* .line 100 */
this.mContext = p1;
/* .line 101 */
/* new-instance v0, Landroid/os/Handler; */
com.android.server.MiuiBgThread .getHandler ( );
(( android.os.Handler ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.mDarkModeStatusHandler = v0;
/* .line 102 */
/* new-instance v0, Lcom/android/server/DarkModeStatusTracker$SettingsObserver; */
v1 = this.mDarkModeStatusHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/DarkModeStatusTracker$SettingsObserver;-><init>(Lcom/android/server/DarkModeStatusTracker;Landroid/os/Handler;)V */
this.mSettingsObserver = v0;
/* .line 103 */
v0 = this.mContext;
final String v1 = "alarm"; // const-string v1, "alarm"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/AlarmManager; */
this.mAlarmManager = v0;
/* .line 104 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
this.mPackageManager = v0;
/* .line 105 */
this.mForceDarkAppListManager = p2;
/* .line 106 */
/* invoke-direct {p0}, Lcom/android/server/DarkModeStatusTracker;->registerSettingsObserver()V */
/* .line 107 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, v0}, Lcom/android/server/DarkModeStatusTracker;->setUploadDarkModeSwitchAlarm(Z)V */
/* .line 108 */
com.android.server.DarkModeSuggestProvider .getInstance ( );
v1 = this.mContext;
/* .line 109 */
(( com.android.server.DarkModeSuggestProvider ) v0 ).updateCloudDataForDisableRegion ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/DarkModeSuggestProvider;->updateCloudDataForDisableRegion(Landroid/content/Context;)Ljava/util/Set;
/* .line 108 */
com.android.server.DarkModeOneTrackHelper .setDataDisableRegion ( v0 );
/* .line 110 */
return;
} // .end method
