class com.android.server.MiuiRestoreManagerService$TransferDataTask implements java.lang.Runnable {
	 /* .source "MiuiRestoreManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/MiuiRestoreManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "TransferDataTask" */
} // .end annotation
/* # instance fields */
final miui.app.backup.ITransferDataCallback callback;
final Boolean copy;
final Boolean isExternal;
final java.lang.String src;
final java.lang.String targetBase;
final Integer targetMode;
final java.lang.String targetPkgName;
final java.lang.String targetRelative;
final com.android.server.MiuiRestoreManagerService this$0; //synthetic
final Integer userId;
/* # direct methods */
public com.android.server.MiuiRestoreManagerService$TransferDataTask ( ) {
/* .locals 0 */
/* .param p2, "src" # Ljava/lang/String; */
/* .param p3, "targetBase" # Ljava/lang/String; */
/* .param p4, "targetRelative" # Ljava/lang/String; */
/* .param p5, "targetPkgName" # Ljava/lang/String; */
/* .param p6, "targetMode" # I */
/* .param p7, "copy" # Z */
/* .param p8, "userId" # I */
/* .param p9, "isExternal" # Z */
/* .param p10, "callback" # Lmiui/app/backup/ITransferDataCallback; */
/* .line 492 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 493 */
this.src = p2;
/* .line 494 */
this.targetBase = p3;
/* .line 495 */
this.targetRelative = p4;
/* .line 496 */
this.targetPkgName = p5;
/* .line 497 */
/* iput p6, p0, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->targetMode:I */
/* .line 498 */
/* iput-boolean p7, p0, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->copy:Z */
/* .line 499 */
/* iput p8, p0, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->userId:I */
/* .line 500 */
/* iput-boolean p9, p0, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->isExternal:Z */
/* .line 501 */
this.callback = p10;
/* .line 502 */
return;
} // .end method
private android.system.StructStat parseStructStatFromList ( java.util.List p0 ) {
/* .locals 27 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)", */
/* "Landroid/system/StructStat;" */
/* } */
} // .end annotation
/* .line 573 */
/* .local p1, "stat":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* move-object/from16 v0, p1 */
/* new-instance v25, Landroid/system/StructStat; */
/* move-object/from16 v1, v25 */
int v2 = 0; // const/4 v2, 0x0
/* check-cast v2, Ljava/lang/String; */
java.lang.Long .parseLong ( v2 );
/* move-result-wide v2 */
int v4 = 1; // const/4 v4, 0x1
/* check-cast v4, Ljava/lang/String; */
java.lang.Long .parseLong ( v4 );
/* move-result-wide v4 */
/* .line 574 */
int v6 = 2; // const/4 v6, 0x2
/* check-cast v6, Ljava/lang/String; */
java.lang.Long .parseLong ( v6 );
/* move-result-wide v6 */
/* long-to-int v6, v6 */
int v7 = 3; // const/4 v7, 0x3
/* check-cast v7, Ljava/lang/String; */
java.lang.Long .parseLong ( v7 );
/* move-result-wide v7 */
int v9 = 4; // const/4 v9, 0x4
/* check-cast v9, Ljava/lang/String; */
java.lang.Long .parseLong ( v9 );
/* move-result-wide v9 */
/* long-to-int v9, v9 */
/* .line 575 */
int v10 = 5; // const/4 v10, 0x5
/* check-cast v10, Ljava/lang/String; */
java.lang.Long .parseLong ( v10 );
/* move-result-wide v10 */
/* long-to-int v10, v10 */
int v11 = 6; // const/4 v11, 0x6
/* check-cast v11, Ljava/lang/String; */
java.lang.Long .parseLong ( v11 );
/* move-result-wide v11 */
int v13 = 7; // const/4 v13, 0x7
/* check-cast v13, Ljava/lang/String; */
java.lang.Long .parseLong ( v13 );
/* move-result-wide v13 */
/* .line 576 */
/* const/16 v15, 0x8 */
/* check-cast v15, Ljava/lang/String; */
java.lang.Long .parseLong ( v15 );
/* move-result-wide v15 */
/* move-object/from16 v26, v1 */
/* const/16 v1, 0x9 */
/* check-cast v1, Ljava/lang/String; */
java.lang.Long .parseLong ( v1 );
/* move-result-wide v17 */
/* const/16 v1, 0xa */
/* check-cast v1, Ljava/lang/String; */
java.lang.Long .parseLong ( v1 );
/* move-result-wide v19 */
/* .line 577 */
/* const/16 v1, 0xb */
/* check-cast v1, Ljava/lang/String; */
java.lang.Long .parseLong ( v1 );
/* move-result-wide v21 */
/* const/16 v1, 0xc */
/* check-cast v1, Ljava/lang/String; */
java.lang.Long .parseLong ( v1 );
/* move-result-wide v23 */
/* move-object/from16 v1, v26 */
/* invoke-direct/range {v1 ..v24}, Landroid/system/StructStat;-><init>(JJIJIIJJJJJJJ)V */
/* .line 573 */
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 18 */
/* .line 508 */
/* move-object/from16 v1, p0 */
final String v2 = "MiuiRestoreManagerService"; // const-string v2, "MiuiRestoreManagerService"
try { // :try_start_0
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 509 */
/* .local v0, "tmpStatList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v3 = this.this$0;
com.android.server.MiuiRestoreManagerService .-$$Nest$fgetmInstaller ( v3 );
v4 = this.src;
v3 = (( com.android.server.pm.Installer ) v3 ).getDataFileStat ( v4, v0 ); // invoke-virtual {v3, v4, v0}, Lcom/android/server/pm/Installer;->getDataFileStat(Ljava/lang/String;Ljava/util/List;)I
/* .line 510 */
/* .local v3, "errorCode":I */
/* if-nez v3, :cond_7 */
/* .line 511 */
int v4 = 0; // const/4 v4, 0x0
/* .line 512 */
/* .local v4, "preStat":Landroid/system/StructStat; */
/* iget-boolean v5, v1, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->copy:Z */
/* if-nez v5, :cond_0 */
/* iget-boolean v5, v1, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->isExternal:Z */
if ( v5 != null) { // if-eqz v5, :cond_1
	 /* .line 513 */
} // :cond_0
/* invoke-direct {v1, v0}, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->parseStructStatFromList(Ljava/util/List;)Landroid/system/StructStat; */
/* move-object v4, v5 */
/* .line 516 */
} // :cond_1
android.app.AppGlobals .getPackageManager ( );
v6 = this.targetPkgName;
/* iget v7, v1, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->userId:I */
/* const-wide/16 v8, 0x0 */
/* .line 519 */
/* .local v5, "info":Landroid/content/pm/ApplicationInfo; */
if ( v5 != null) { // if-eqz v5, :cond_6
/* .line 522 */
v6 = this.seInfo;
if ( v6 != null) { // if-eqz v6, :cond_5
	 /* .line 526 */
	 /* iget v12, v5, Landroid/content/pm/ApplicationInfo;->uid:I */
	 /* .line 529 */
	 /* .local v12, "uid":I */
	 /* iget-boolean v6, v1, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->isExternal:Z */
	 if ( v6 != null) { // if-eqz v6, :cond_3
		 /* .line 530 */
		 /* if-nez v4, :cond_2 */
		 int v6 = -1; // const/4 v6, -0x1
	 } // :cond_2
	 /* iget v6, v4, Landroid/system/StructStat;->st_gid:I */
	 /* .line 531 */
	 /* .local v6, "gid":I */
} // :goto_0
final String v7 = ""; // const-string v7, ""
/* move-object/from16 v17, v7 */
/* .local v7, "seInfo":Ljava/lang/String; */
/* .line 533 */
} // .end local v6 # "gid":I
} // .end local v7 # "seInfo":Ljava/lang/String;
} // :cond_3
/* iget v6, v5, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 534 */
/* .restart local v6 # "gid":I */
v7 = this.seInfo;
/* move-object/from16 v17, v7 */
/* .line 537 */
/* .local v17, "seInfo":Ljava/lang/String; */
} // :goto_1
v7 = this.this$0;
com.android.server.MiuiRestoreManagerService .-$$Nest$fgetmInstaller ( v7 );
v8 = this.src;
v9 = this.targetBase;
v10 = this.targetRelative;
/* iget-boolean v11, v1, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->copy:Z */
/* iget v14, v1, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->targetMode:I */
/* iget-boolean v15, v1, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->isExternal:Z */
/* move v13, v6 */
/* move/from16 v16, v15 */
/* move-object/from16 v15, v17 */
v7 = /* invoke-virtual/range {v7 ..v16}, Lcom/android/server/pm/Installer;->transferData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIIILjava/lang/String;Z)I */
/* move v3, v7 */
/* .line 539 */
int v7 = 0; // const/4 v7, 0x0
/* .line 540 */
/* .local v7, "curStat":Landroid/system/StructStat; */
/* .line 541 */
/* if-nez v3, :cond_7 */
/* iget-boolean v8, v1, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->copy:Z */
if ( v8 != null) { // if-eqz v8, :cond_7
if ( v4 != null) { // if-eqz v4, :cond_7
v8 = this.this$0;
com.android.server.MiuiRestoreManagerService .-$$Nest$fgetmInstaller ( v8 );
v9 = this.src;
/* .line 544 */
v8 = (( com.android.server.pm.Installer ) v8 ).getDataFileStat ( v9, v0 ); // invoke-virtual {v8, v9, v0}, Lcom/android/server/pm/Installer;->getDataFileStat(Ljava/lang/String;Ljava/util/List;)I
if ( v8 != null) { // if-eqz v8, :cond_7
/* .line 545 */
/* invoke-direct {v1, v0}, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;->parseStructStatFromList(Ljava/util/List;)Landroid/system/StructStat; */
/* move-object v7, v8 */
if ( v8 != null) { // if-eqz v8, :cond_7
/* iget-wide v8, v4, Landroid/system/StructStat;->st_mtime:J */
/* iget-wide v10, v7, Landroid/system/StructStat;->st_mtime:J */
/* cmp-long v8, v8, v10 */
/* if-nez v8, :cond_4 */
/* iget-wide v8, v4, Landroid/system/StructStat;->st_size:J */
/* iget-wide v10, v7, Landroid/system/StructStat;->st_size:J */
/* cmp-long v8, v8, v10 */
/* if-nez v8, :cond_4 */
/* .line 547 */
} // :cond_4
/* new-instance v8, Lcom/android/server/MiuiRestoreManagerService$FileChangedException; */
final String v9 = "file has been changed"; // const-string v9, "file has been changed"
/* invoke-direct {v8, v9}, Lcom/android/server/MiuiRestoreManagerService$FileChangedException;-><init>(Ljava/lang/String;)V */
} // .end local p0 # "this":Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;
/* throw v8 */
/* .line 523 */
} // .end local v6 # "gid":I
} // .end local v7 # "curStat":Landroid/system/StructStat;
} // .end local v12 # "uid":I
} // .end local v17 # "seInfo":Ljava/lang/String;
/* .restart local p0 # "this":Lcom/android/server/MiuiRestoreManagerService$TransferDataTask; */
} // :cond_5
/* new-instance v6, Lcom/android/server/MiuiRestoreManagerService$BadSeInfoException; */
/* const-string/jumbo v7, "se info is null" */
/* invoke-direct {v6, v7}, Lcom/android/server/MiuiRestoreManagerService$BadSeInfoException;-><init>(Ljava/lang/String;)V */
} // .end local p0 # "this":Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;
/* throw v6 */
/* .line 520 */
/* .restart local p0 # "this":Lcom/android/server/MiuiRestoreManagerService$TransferDataTask; */
} // :cond_6
/* new-instance v6, Lcom/android/server/MiuiRestoreManagerService$QueryAppInfoErrorException; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "no app info with pkg: "; // const-string v8, "no app info with pkg: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v8 = this.targetPkgName;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v6, v7}, Lcom/android/server/MiuiRestoreManagerService$QueryAppInfoErrorException;-><init>(Ljava/lang/String;)V */
} // .end local p0 # "this":Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;
/* throw v6 */
/* :try_end_0 */
/* .catch Lcom/android/server/MiuiRestoreManagerService$QueryAppInfoErrorException; {:try_start_0 ..:try_end_0} :catch_3 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_3 */
/* .catch Lcom/android/server/MiuiRestoreManagerService$BadSeInfoException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catch Lcom/android/server/pm/Installer$InstallerException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Lcom/android/server/MiuiRestoreManagerService$FileChangedException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 559 */
} // .end local v0 # "tmpStatList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v3 # "errorCode":I
} // .end local v4 # "preStat":Landroid/system/StructStat;
} // .end local v5 # "info":Landroid/content/pm/ApplicationInfo;
/* .restart local p0 # "this":Lcom/android/server/MiuiRestoreManagerService$TransferDataTask; */
/* :catch_0 */
/* move-exception v0 */
/* .line 560 */
/* .local v0, "e":Lcom/android/server/MiuiRestoreManagerService$FileChangedException; */
int v3 = -7; // const/4 v3, -0x7
/* .line 561 */
/* .restart local v3 # "errorCode":I */
final String v4 = "file changed "; // const-string v4, "file changed "
android.util.Slog .w ( v2,v4,v0 );
/* .line 556 */
} // .end local v0 # "e":Lcom/android/server/MiuiRestoreManagerService$FileChangedException;
} // .end local v3 # "errorCode":I
/* :catch_1 */
/* move-exception v0 */
/* .line 557 */
/* .local v0, "e":Lcom/android/server/pm/Installer$InstallerException; */
int v3 = -1; // const/4 v3, -0x1
/* .line 558 */
/* .restart local v3 # "errorCode":I */
/* const-string/jumbo v4, "transfer data dir error " */
android.util.Slog .w ( v2,v4,v0 );
} // .end local v0 # "e":Lcom/android/server/pm/Installer$InstallerException;
/* .line 553 */
} // .end local v3 # "errorCode":I
/* :catch_2 */
/* move-exception v0 */
/* .line 554 */
/* .local v0, "e":Lcom/android/server/MiuiRestoreManagerService$BadSeInfoException; */
int v3 = -6; // const/4 v3, -0x6
/* .line 555 */
/* .restart local v3 # "errorCode":I */
/* const-string/jumbo v4, "se info is null " */
android.util.Slog .w ( v2,v4,v0 );
} // .end local v0 # "e":Lcom/android/server/MiuiRestoreManagerService$BadSeInfoException;
/* .line 550 */
} // .end local v3 # "errorCode":I
/* :catch_3 */
/* move-exception v0 */
/* .line 551 */
/* .local v0, "e":Ljava/lang/Exception; */
int v3 = -2; // const/4 v3, -0x2
/* .line 552 */
/* .restart local v3 # "errorCode":I */
final String v4 = "query app info failed "; // const-string v4, "query app info failed "
android.util.Slog .w ( v2,v4,v0 );
/* .line 562 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_7
} // :goto_2
/* nop */
/* .line 565 */
} // :goto_3
try { // :try_start_1
v0 = this.callback;
v4 = this.src;
int v5 = 2; // const/4 v5, 0x2
/* new-array v5, v5, [Ljava/lang/CharSequence; */
v6 = this.targetBase;
int v7 = 0; // const/4 v7, 0x0
/* aput-object v6, v5, v7 */
v6 = this.targetRelative;
int v7 = 1; // const/4 v7, 0x1
/* aput-object v6, v5, v7 */
android.text.TextUtils .concat ( v5 );
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_4 */
/* .line 568 */
/* .line 566 */
/* :catch_4 */
/* move-exception v0 */
/* .line 567 */
/* .local v0, "e":Landroid/os/RemoteException; */
final String v4 = "error when notify onTransferDataEnd "; // const-string v4, "error when notify onTransferDataEnd "
android.util.Slog .e ( v2,v4,v0 );
/* .line 569 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_4
return;
} // .end method
