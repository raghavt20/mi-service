public class com.android.server.RescuePartyImpl extends com.android.server.RescuePartyStub {
	 /* .source "RescuePartyImpl.java" */
	 /* # direct methods */
	 public com.android.server.RescuePartyImpl ( ) {
		 /* .locals 0 */
		 /* .line 20 */
		 /* invoke-direct {p0}, Lcom/android/server/RescuePartyStub;-><init>()V */
		 return;
	 } // .end method
	 static void lambda$maybeDoResetConfig$0 ( android.content.Context p0 ) { //synthethic
		 /* .locals 3 */
		 /* .param p0, "context" # Landroid/content/Context; */
		 /* .line 81 */
		 try { // :try_start_0
			 /* const-class v0, Landroid/os/PowerManager; */
			 (( android.content.Context ) p0 ).getSystemService ( v0 ); // invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
			 /* check-cast v0, Landroid/os/PowerManager; */
			 /* .line 82 */
			 /* .local v0, "pm":Landroid/os/PowerManager; */
			 if ( v0 != null) { // if-eqz v0, :cond_0
				 /* .line 83 */
				 final String v1 = "RescueParty"; // const-string v1, "RescueParty"
				 (( android.os.PowerManager ) v0 ).reboot ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V
				 /* :try_end_0 */
				 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
				 /* .line 87 */
			 } // .end local v0 # "pm":Landroid/os/PowerManager;
		 } // :cond_0
		 /* .line 85 */
		 /* :catchall_0 */
		 /* move-exception v0 */
		 /* .line 86 */
		 /* .local v0, "t":Ljava/lang/Throwable; */
		 final String v1 = "RescuePartyPlus"; // const-string v1, "RescuePartyPlus"
		 final String v2 = "do config reset failed!"; // const-string v2, "do config reset failed!"
		 android.util.Slog .e ( v1,v2,v0 );
		 /* .line 88 */
	 } // .end local v0 # "t":Ljava/lang/Throwable;
} // :goto_0
return;
} // .end method
/* # virtual methods */
public Boolean isLauncher ( android.content.Context p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 24 */
v0 = com.android.server.RescuePartyPlusHelper .checkDisableRescuePartyPlus ( );
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 25 */
} // :cond_0
if ( p2 != null) { // if-eqz p2, :cond_1
	 com.android.server.RescuePartyPlusHelper .getLauncherPackageName ( p1 );
	 v0 = 	 (( java.lang.String ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 26 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* .line 28 */
	 } // :cond_1
} // .end method
public Boolean maybeDoResetConfig ( android.content.Context p0, java.lang.String p1 ) {
	 /* .locals 9 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .param p2, "failedPackage" # Ljava/lang/String; */
	 /* .line 33 */
	 v0 = 	 android.text.TextUtils .isEmpty ( p2 );
	 /* if-nez v0, :cond_0 */
	 /* .line 34 */
	 /* const-string/jumbo v0, "sys.rescue_party_failed_package" */
	 android.os.SystemProperties .set ( v0,p2 );
	 /* .line 37 */
} // :cond_0
v0 = com.android.server.RescuePartyPlusHelper .checkDisableRescuePartyPlus ( );
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 40 */
} // :cond_1
v0 = com.android.server.RescuePartyPlusHelper .getMitigationTempCount ( );
/* .line 43 */
/* .local v0, "mitigationCount":I */
v2 = com.android.server.RescuePartyPlusHelper .getConfigResetProcessStatus ( );
int v3 = 1; // const/4 v3, 0x1
final String v4 = "RescuePartyPlus"; // const-string v4, "RescuePartyPlus"
if ( v2 != null) { // if-eqz v2, :cond_2
	 /* .line 44 */
	 final String v1 = "Config Reset in progress!"; // const-string v1, "Config Reset in progress!"
	 android.util.Slog .w ( v4,v1 );
	 /* .line 45 */
	 /* const-string/jumbo v1, "sys.powerctl" */
	 final String v2 = "reboot,RescueParty"; // const-string v2, "reboot,RescueParty"
	 android.os.SystemProperties .set ( v1,v2 );
	 /* .line 46 */
	 /* .line 50 */
} // :cond_2
int v2 = 5; // const/4 v2, 0x5
/* if-eq v0, v2, :cond_3 */
/* .line 51 */
/* .line 55 */
} // :cond_3
com.android.server.RescuePartyPlusHelper .setLastResetConfigStatus ( v3 );
/* .line 56 */
com.android.server.RescuePartyPlusHelper .setConfigResetProcessStatus ( v3 );
/* .line 57 */
final String v1 = "Start Config Reset!"; // const-string v1, "Start Config Reset!"
android.util.Slog .w ( v4,v1 );
/* .line 58 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Finished rescue level CONFIG_RESET for package "; // const-string v2, "Finished rescue level CONFIG_RESET for package "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v2 = 3; // const/4 v2, 0x3
com.android.server.pm.PackageManagerServiceUtils .logCriticalInfo ( v2,v1 );
/* .line 61 */
com.android.server.RescuePartyPlusHelper .tryGetCloudControlOrDefaultData ( );
/* .line 64 */
/* .local v1, "deleteFileSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
v6 = } // :goto_0
if ( v6 != null) { // if-eqz v6, :cond_5
/* check-cast v6, Ljava/lang/String; */
/* .line 65 */
/* .local v6, "filename":Ljava/lang/String; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Preparing to delete files: "; // const-string v8, "Preparing to delete files: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v4,v7 );
/* .line 66 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.pm.PackageManagerServiceUtils .logCriticalInfo ( v2,v7 );
/* .line 67 */
/* new-instance v7, Ljava/io/File; */
/* invoke-direct {v7, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 68 */
/* .local v7, "file":Ljava/io/File; */
v8 = (( java.io.File ) v7 ).exists ( ); // invoke-virtual {v7}, Ljava/io/File;->exists()Z
if ( v8 != null) { // if-eqz v8, :cond_4
/* .line 69 */
(( java.io.File ) v7 ).delete ( ); // invoke-virtual {v7}, Ljava/io/File;->delete()Z
/* .line 71 */
} // .end local v6 # "filename":Ljava/lang/String;
} // .end local v7 # "file":Ljava/io/File;
} // :cond_4
/* .line 74 */
} // :cond_5
v2 = com.android.server.RescuePartyPlusHelper .resetTheme ( p2 );
/* if-nez v2, :cond_6 */
/* .line 75 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Reset theme failed: "; // const-string v5, "Reset theme failed: "
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v4,v2 );
/* .line 79 */
} // :cond_6
/* new-instance v2, Lcom/android/server/RescuePartyImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v2, p1}, Lcom/android/server/RescuePartyImpl$$ExternalSyntheticLambda0;-><init>(Landroid/content/Context;)V */
/* .line 89 */
/* .local v2, "runnable":Ljava/lang/Runnable; */
/* new-instance v4, Ljava/lang/Thread; */
/* invoke-direct {v4, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V */
/* .line 90 */
/* .local v4, "thread":Ljava/lang/Thread; */
(( java.lang.Thread ) v4 ).start ( ); // invoke-virtual {v4}, Ljava/lang/Thread;->start()V
/* .line 91 */
} // .end method
