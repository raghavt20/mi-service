.class Lcom/android/server/audio/GameAudioEnhancer$SettingsObserver;
.super Landroid/database/ContentObserver;
.source "GameAudioEnhancer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/GameAudioEnhancer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/audio/GameAudioEnhancer;


# direct methods
.method constructor <init>(Lcom/android/server/audio/GameAudioEnhancer;)V
    .locals 3

    .line 104
    iput-object p1, p0, Lcom/android/server/audio/GameAudioEnhancer$SettingsObserver;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    .line 105
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 106
    invoke-static {p1}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$fgetmContentResolver(Lcom/android/server/audio/GameAudioEnhancer;)Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "game_mode_enable"

    invoke-static {v1}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 108
    invoke-static {p1}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$fgetmContentResolver(Lcom/android/server/audio/GameAudioEnhancer;)Landroid/content/ContentResolver;

    move-result-object p1

    const-string v0, "game_mode_packages"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 110
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;I)V
    .locals 3
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "userId"    # I

    .line 114
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 115
    const-string v0, "game_mode_enable"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 116
    .local v0, "audioEnhancerUri":Landroid/net/Uri;
    if-ne p2, v0, :cond_0

    .line 118
    iget-object v1, p0, Lcom/android/server/audio/GameAudioEnhancer$SettingsObserver;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    invoke-static {v1}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$mupdateGameModeSettingstatus(Lcom/android/server/audio/GameAudioEnhancer;)V

    goto :goto_0

    .line 120
    :cond_0
    iget-object v1, p0, Lcom/android/server/audio/GameAudioEnhancer$SettingsObserver;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    invoke-static {v1}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$fgetmLock(Lcom/android/server/audio/GameAudioEnhancer;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 122
    :try_start_0
    iget-object v2, p0, Lcom/android/server/audio/GameAudioEnhancer$SettingsObserver;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    invoke-static {v2}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$mupdateWhiteList(Lcom/android/server/audio/GameAudioEnhancer;)V

    .line 123
    monitor-exit v1

    .line 125
    :goto_0
    return-void

    .line 123
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
