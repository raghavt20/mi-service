.class public Lcom/android/server/audio/GameAudioEnhancer;
.super Ljava/lang/Object;
.source "GameAudioEnhancer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/audio/GameAudioEnhancer$SettingsObserver;,
        Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;
    }
.end annotation


# static fields
.field private static final ADUIO_GAME_SOUND_EFFECT_SWITCH:Ljava/lang/String; = "audio_game_sound_effect_switch"

.field private static final ADUIO_GAME_SOUND_NEW_EFFECT_SWITCH:Ljava/lang/String; = "audio_game_sound_mode_switch"

.field private static final AUDIO_GAME_PACKAGE_NAME:Ljava/lang/String; = "audio_game_package_name"

.field public static final GAME_MODE_ENABLE:Ljava/lang/String; = "game_mode_enable"

.field public static final GAME_MODE_PKGS:Ljava/lang/String; = "game_mode_packages"

.field private static final MSG_SET_PROCESS_LISTENER:I = 0x1

.field private static final MSG_START_GAME_EFFECT:I = 0x2

.field private static final MSG_STOP_GAME_EFFECT:I = 0x3

.field private static final PM_SERVICE_NAME:Ljava/lang/String; = "ProcessManager"

.field private static final QUERY_PM_SERVICE_DELAY_MS:I = 0x3e8

.field private static final QUERY_PM_SERVICE_MAX_TIMES:I = 0x14

.field private static final SEND_PARAMETER_DELAY_MS:I = 0x1f4

.field private static final TAG:Ljava/lang/String; = "GameAudioEnhancer"


# instance fields
.field private final LOCAL_ENABLED_PACKAGES:[Ljava/lang/String;

.field private mAudioManager:Landroid/media/AudioManager;

.field private final mContentResolver:Landroid/content/ContentResolver;

.field private mContext:Landroid/content/Context;

.field private mCurForegroundPkg:Ljava/lang/String;

.field private mCurrentEnablePkg:Ljava/lang/String;

.field private mEnabledPackages:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mForegroundInfoListener:Lmiui/process/IForegroundInfoListener$Stub;

.field private mFreeFormWindowPkg:Ljava/lang/String;

.field private mFreeWinVersion:I

.field private final mFreeformCallBack:Lmiui/app/IFreeformCallback;

.field private mGameModeSwitchStatus:I

.field private mHandler:Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;

.field private final mLock:Ljava/lang/Object;

.field private volatile mParamSet:Z

.field private mQueryPMServiceTime:I

.field private mSettingsObserver:Lcom/android/server/audio/GameAudioEnhancer$SettingsObserver;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContentResolver(Lcom/android/server/audio/GameAudioEnhancer;)Landroid/content/ContentResolver;
    .locals 0

    iget-object p0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mContentResolver:Landroid/content/ContentResolver;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/audio/GameAudioEnhancer;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmCurForegroundPkg(Lcom/android/server/audio/GameAudioEnhancer;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mCurForegroundPkg:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmForegroundInfoListener(Lcom/android/server/audio/GameAudioEnhancer;)Lmiui/process/IForegroundInfoListener$Stub;
    .locals 0

    iget-object p0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mForegroundInfoListener:Lmiui/process/IForegroundInfoListener$Stub;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmFreeWinVersion(Lcom/android/server/audio/GameAudioEnhancer;)I
    .locals 0

    iget p0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mFreeWinVersion:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmGameModeSwitchStatus(Lcom/android/server/audio/GameAudioEnhancer;)I
    .locals 0

    iget p0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mGameModeSwitchStatus:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/audio/GameAudioEnhancer;)Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mHandler:Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmLock(Lcom/android/server/audio/GameAudioEnhancer;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mLock:Ljava/lang/Object;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmParamSet(Lcom/android/server/audio/GameAudioEnhancer;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mParamSet:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmQueryPMServiceTime(Lcom/android/server/audio/GameAudioEnhancer;)I
    .locals 0

    iget p0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mQueryPMServiceTime:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmCurForegroundPkg(Lcom/android/server/audio/GameAudioEnhancer;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/audio/GameAudioEnhancer;->mCurForegroundPkg:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmFreeFormWindowPkg(Lcom/android/server/audio/GameAudioEnhancer;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/audio/GameAudioEnhancer;->mFreeFormWindowPkg:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmParamSet(Lcom/android/server/audio/GameAudioEnhancer;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/audio/GameAudioEnhancer;->mParamSet:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmQueryPMServiceTime(Lcom/android/server/audio/GameAudioEnhancer;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/audio/GameAudioEnhancer;->mQueryPMServiceTime:I

    return-void
.end method

.method static bridge synthetic -$$Nest$minitProcessListenerAsync(Lcom/android/server/audio/GameAudioEnhancer;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/audio/GameAudioEnhancer;->initProcessListenerAsync()V

    return-void
.end method

.method static bridge synthetic -$$Nest$misPackageEnabled(Lcom/android/server/audio/GameAudioEnhancer;Ljava/lang/String;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/audio/GameAudioEnhancer;->isPackageEnabled(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$msetGameMode(Lcom/android/server/audio/GameAudioEnhancer;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/audio/GameAudioEnhancer;->setGameMode(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetParamSend(Lcom/android/server/audio/GameAudioEnhancer;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/audio/GameAudioEnhancer;->setParamSend(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateGameModeSettingstatus(Lcom/android/server/audio/GameAudioEnhancer;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/audio/GameAudioEnhancer;->updateGameModeSettingstatus()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateWhiteList(Lcom/android/server/audio/GameAudioEnhancer;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/audio/GameAudioEnhancer;->updateWhiteList()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const-string v0, "com.tencent.tmgp.cf"

    const-string v1, "com.tencent.mf.uam"

    const-string v2, "com.tencent.toaa"

    const-string v3, "com.tencent.tmgp.cod"

    const-string v4, "com.miHoYo.ys.mi"

    const-string v5, "com.tencent.tmgp.sgame"

    filled-new-array/range {v0 .. v5}, [Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->LOCAL_ENABLED_PACKAGES:[Ljava/lang/String;

    .line 59
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mEnabledPackages:Ljava/util/Set;

    .line 64
    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mCurrentEnablePkg:Ljava/lang/String;

    .line 65
    iput-object v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mCurForegroundPkg:Ljava/lang/String;

    .line 66
    iput-object v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mFreeFormWindowPkg:Ljava/lang/String;

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mParamSet:Z

    .line 68
    const/4 v1, 0x1

    iput v1, p0, Lcom/android/server/audio/GameAudioEnhancer;->mGameModeSwitchStatus:I

    .line 69
    iput v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mQueryPMServiceTime:I

    .line 70
    invoke-static {}, Lmiui/app/MiuiFreeFormManager;->getMiuiFreeformVersion()I

    move-result v1

    iput v1, p0, Lcom/android/server/audio/GameAudioEnhancer;->mFreeWinVersion:I

    .line 72
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/android/server/audio/GameAudioEnhancer;->mLock:Ljava/lang/Object;

    .line 146
    new-instance v1, Lcom/android/server/audio/GameAudioEnhancer$1;

    invoke-direct {v1, p0}, Lcom/android/server/audio/GameAudioEnhancer$1;-><init>(Lcom/android/server/audio/GameAudioEnhancer;)V

    iput-object v1, p0, Lcom/android/server/audio/GameAudioEnhancer;->mFreeformCallBack:Lmiui/app/IFreeformCallback;

    .line 209
    new-instance v1, Lcom/android/server/audio/GameAudioEnhancer$2;

    invoke-direct {v1, p0}, Lcom/android/server/audio/GameAudioEnhancer$2;-><init>(Lcom/android/server/audio/GameAudioEnhancer;)V

    iput-object v1, p0, Lcom/android/server/audio/GameAudioEnhancer;->mForegroundInfoListener:Lmiui/process/IForegroundInfoListener$Stub;

    .line 81
    iput-object p1, p0, Lcom/android/server/audio/GameAudioEnhancer;->mContext:Landroid/content/Context;

    .line 82
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/audio/GameAudioEnhancer;->mContentResolver:Landroid/content/ContentResolver;

    .line 83
    new-instance v1, Lcom/android/server/audio/GameAudioEnhancer$SettingsObserver;

    invoke-direct {v1, p0}, Lcom/android/server/audio/GameAudioEnhancer$SettingsObserver;-><init>(Lcom/android/server/audio/GameAudioEnhancer;)V

    iput-object v1, p0, Lcom/android/server/audio/GameAudioEnhancer;->mSettingsObserver:Lcom/android/server/audio/GameAudioEnhancer$SettingsObserver;

    .line 84
    iget-object v1, p0, Lcom/android/server/audio/GameAudioEnhancer;->mContext:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/android/server/audio/GameAudioEnhancer;->mAudioManager:Landroid/media/AudioManager;

    .line 85
    new-instance v1, Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;

    invoke-direct {v1, p0, p2}, Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;-><init>(Lcom/android/server/audio/GameAudioEnhancer;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/server/audio/GameAudioEnhancer;->mHandler:Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;

    .line 86
    iput v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mQueryPMServiceTime:I

    .line 87
    invoke-direct {p0}, Lcom/android/server/audio/GameAudioEnhancer;->initSettingStatus()V

    .line 88
    invoke-direct {p0}, Lcom/android/server/audio/GameAudioEnhancer;->initEnabledPackages()V

    .line 89
    invoke-direct {p0}, Lcom/android/server/audio/GameAudioEnhancer;->initProcessListenerAsync()V

    .line 90
    invoke-direct {p0}, Lcom/android/server/audio/GameAudioEnhancer;->registerForegroundObserver()V

    .line 91
    return-void
.end method

.method private beClosed(Ljava/lang/String;)V
    .locals 4
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 292
    iget-boolean v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mParamSet:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/server/audio/GameAudioEnhancer;->isPackageEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 293
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mParamSet:Z

    .line 294
    iget-object v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mHandler:Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 297
    :cond_0
    return-void
.end method

.method private beOpened(Ljava/lang/String;)V
    .locals 4
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 277
    iget-boolean v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mParamSet:Z

    const-wide/16 v1, 0x1f4

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/server/audio/GameAudioEnhancer;->isPackageEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 278
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mParamSet:Z

    .line 279
    iput-object p1, p0, Lcom/android/server/audio/GameAudioEnhancer;->mCurrentEnablePkg:Ljava/lang/String;

    .line 280
    iget-object v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mHandler:Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v0, v3, v1, v2}, Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 282
    return-void

    .line 284
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mParamSet:Z

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/android/server/audio/GameAudioEnhancer;->isPackageEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 285
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mParamSet:Z

    .line 286
    iget-object v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mHandler:Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;

    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v0, v3, v1, v2}, Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 289
    :cond_1
    return-void
.end method

.method private initEnabledPackages()V
    .locals 1

    .line 348
    invoke-direct {p0}, Lcom/android/server/audio/GameAudioEnhancer;->initEnabledPackagesFromCloudSettings()Z

    move-result v0

    if-nez v0, :cond_0

    .line 349
    invoke-direct {p0}, Lcom/android/server/audio/GameAudioEnhancer;->initLocalPackages()V

    .line 351
    :cond_0
    return-void
.end method

.method private initEnabledPackagesFromCloudSettings()Z
    .locals 9

    .line 354
    iget-object v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mContentResolver:Landroid/content/ContentResolver;

    const-string v1, "game_mode_packages"

    invoke-static {v0, v1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 355
    .local v0, "pkgs":Ljava/lang/String;
    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    .line 358
    :cond_0
    iget-object v2, p0, Lcom/android/server/audio/GameAudioEnhancer;->mEnabledPackages:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 359
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    sub-int/2addr v2, v3

    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 360
    .local v2, "strPkgs":Ljava/lang/String;
    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 361
    .local v4, "jsonStrs":[Ljava/lang/String;
    array-length v5, v4

    :goto_0
    if-ge v1, v5, :cond_1

    aget-object v6, v4, v1

    .line 362
    .local v6, "jsonStr":Ljava/lang/String;
    iget-object v7, p0, Lcom/android/server/audio/GameAudioEnhancer;->mEnabledPackages:Ljava/util/Set;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    sub-int/2addr v8, v3

    invoke-virtual {v6, v3, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 361
    .end local v6    # "jsonStr":Ljava/lang/String;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 364
    :cond_1
    return v3

    .line 356
    .end local v2    # "strPkgs":Ljava/lang/String;
    .end local v4    # "jsonStrs":[Ljava/lang/String;
    :cond_2
    :goto_1
    return v1
.end method

.method private initLocalPackages()V
    .locals 2

    .line 369
    iget-object v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mEnabledPackages:Ljava/util/Set;

    iget-object v1, p0, Lcom/android/server/audio/GameAudioEnhancer;->LOCAL_ENABLED_PACKAGES:[Ljava/lang/String;

    invoke-static {v0, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 370
    return-void
.end method

.method private initProcessListenerAsync()V
    .locals 4

    .line 330
    iget-object v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mHandler:Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 332
    return-void
.end method

.method private initSettingStatus()V
    .locals 5

    .line 335
    const-string v0, "game_mode_enable"

    const/4 v1, 0x0

    .line 337
    .local v1, "enable":I
    :try_start_0
    iget-object v2, p0, Lcom/android/server/audio/GameAudioEnhancer;->mContentResolver:Landroid/content/ContentResolver;

    invoke-static {v2, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v0

    .line 341
    goto :goto_0

    .line 338
    :catch_0
    move-exception v2

    .line 340
    .local v2, "e":Ljava/lang/Exception;
    iget-object v3, p0, Lcom/android/server/audio/GameAudioEnhancer;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v4, 0x1

    invoke-static {v3, v0, v4}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 342
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_0
    nop

    .line 343
    return-void
.end method

.method private isPackageEnabled(Ljava/lang/String;)Z
    .locals 2
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 259
    iget-object v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 260
    :try_start_0
    iget-object v1, p0, Lcom/android/server/audio/GameAudioEnhancer;->mEnabledPackages:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 261
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 263
    :cond_0
    monitor-exit v0

    const/4 v0, 0x0

    return v0

    .line 264
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private registerForegroundObserver()V
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mFreeformCallBack:Lmiui/app/IFreeformCallback;

    invoke-static {v0}, Lmiui/app/MiuiFreeFormManager;->registerFreeformCallback(Lmiui/app/IFreeformCallback;)V

    .line 95
    iget-object v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mForegroundInfoListener:Lmiui/process/IForegroundInfoListener$Stub;

    invoke-static {v0}, Lmiui/process/ProcessManager;->registerForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V

    .line 96
    return-void
.end method

.method private setGameMode(Z)V
    .locals 7
    .param p1, "on"    # Z

    .line 314
    const-string v0, "ro.vendor.audio.game.mode"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    const-string v1, "GameAudioEnhancer"

    const-string v2, "="

    const-string v3, "audio_game_package_name"

    const-string v4, "on;"

    const-string v5, "off;"

    if-eqz v0, :cond_2

    .line 315
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "sendparam=AudioGameEnhance;audio_game_sound_mode_switch="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 316
    if-eqz p1, :cond_0

    move-object v6, v4

    goto :goto_0

    :cond_0
    move-object v6, v5

    :goto_0
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v6, p0, Lcom/android/server/audio/GameAudioEnhancer;->mCurrentEnablePkg:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 315
    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 318
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "audio_game_sound_mode_switch="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    move-object v4, v5

    :goto_1
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/audio/GameAudioEnhancer;->mCurrentEnablePkg:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 321
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "sendparam=AudioGameEnhance;audio_game_sound_effect_switch="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 322
    if-eqz p1, :cond_3

    move-object v6, v4

    goto :goto_2

    :cond_3
    move-object v6, v5

    :goto_2
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v6, p0, Lcom/android/server/audio/GameAudioEnhancer;->mCurrentEnablePkg:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 321
    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 324
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "audio_game_sound_effect_switch="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    if-eqz p1, :cond_4

    goto :goto_3

    :cond_4
    move-object v4, v5

    :goto_3
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/audio/GameAudioEnhancer;->mCurrentEnablePkg:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    :goto_4
    return-void
.end method

.method private setGameModeEnabled(I)V
    .locals 1
    .param p1, "enable"    # I

    .line 305
    iput p1, p0, Lcom/android/server/audio/GameAudioEnhancer;->mGameModeSwitchStatus:I

    .line 306
    iget-object v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mFreeFormWindowPkg:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 307
    return-void

    .line 309
    :cond_0
    iget-object v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mFreeFormWindowPkg:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/server/audio/GameAudioEnhancer;->setParamSend(Ljava/lang/String;)V

    .line 311
    return-void
.end method

.method private setParamSend(Ljava/lang/String;)V
    .locals 2
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 268
    iget v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mGameModeSwitchStatus:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 269
    invoke-direct {p0, p1}, Lcom/android/server/audio/GameAudioEnhancer;->beOpened(Ljava/lang/String;)V

    goto :goto_0

    .line 271
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/server/audio/GameAudioEnhancer;->beClosed(Ljava/lang/String;)V

    .line 272
    invoke-direct {p0}, Lcom/android/server/audio/GameAudioEnhancer;->unregisterForegroundObserver()V

    .line 274
    :goto_0
    return-void
.end method

.method private setWhiteList(Ljava/lang/String;)V
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;

    .line 300
    iget-object v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mEnabledPackages:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 301
    invoke-direct {p0}, Lcom/android/server/audio/GameAudioEnhancer;->initEnabledPackagesFromCloudSettings()Z

    .line 302
    return-void
.end method

.method private unregisterForegroundObserver()V
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mFreeformCallBack:Lmiui/app/IFreeformCallback;

    invoke-static {v0}, Lmiui/app/MiuiFreeFormManager;->unregisterFreeformCallback(Lmiui/app/IFreeformCallback;)V

    .line 100
    iget-object v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mForegroundInfoListener:Lmiui/process/IForegroundInfoListener$Stub;

    invoke-static {v0}, Lmiui/process/ProcessManager;->unregisterForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V

    .line 101
    return-void
.end method

.method private updateGameModeSettingstatus()V
    .locals 3

    .line 130
    :try_start_0
    iget-object v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mContentResolver:Landroid/content/ContentResolver;

    const-string v1, "game_mode_enable"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 131
    .local v0, "gameModeEnable":I
    invoke-direct {p0, v0}, Lcom/android/server/audio/GameAudioEnhancer;->setGameModeEnabled(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    .end local v0    # "gameModeEnable":I
    goto :goto_0

    .line 132
    :catch_0
    move-exception v0

    .line 133
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateGameModeSettingstatus: Exception "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GameAudioEnhancer"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private updateWhiteList()V
    .locals 3

    .line 139
    :try_start_0
    iget-object v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mContentResolver:Landroid/content/ContentResolver;

    const-string v1, "game_mode_packages"

    invoke-static {v0, v1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 140
    .local v0, "gameModeEnablePkgs":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/android/server/audio/GameAudioEnhancer;->setWhiteList(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 143
    .end local v0    # "gameModeEnablePkgs":Ljava/lang/String;
    goto :goto_0

    .line 141
    :catch_0
    move-exception v0

    .line 142
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateGameModeSettingstatus: Exception "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GameAudioEnhancer"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method
