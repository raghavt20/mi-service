public class com.android.server.audio.MQSUtils {
	 /* .source "MQSUtils.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/audio/MQSUtils$AudioStateTrackData;, */
	 /* Lcom/android/server/audio/MQSUtils$WaveformInfo; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String AUDIO_EVENT_ID;
private static final java.lang.String AUDIO_EVENT_NAME;
private static final Integer AUDIO_VISUAL_ENABLED;
private static final Integer FLAG_NON_ANONYMOUS;
private static final Integer FLAG_NOT_LIMITED_BY_USER_EXPERIENCE_PLAN;
private static final java.lang.String HAPTIC_FEEDBACK_INFINITE_INTENSITY;
private static final java.lang.String IS_HEARING_ASSIST_SUPPORT;
private static final java.lang.String IS_TRANSMIT_SUPPORT;
private static final java.lang.String KEY_PARAM_RINGTONE_DEVICE;
private static final java.lang.String ONE_TRACK_ACTION;
private static final java.lang.String PACKAGE_NAME;
private static final java.lang.String PARAM_RINGTONE_DEVICE_OFF;
private static final java.lang.String PARAM_RINGTONE_DEVICE_ON;
private static final Integer SOUND_ASSIST_ENABLED;
private static final java.lang.String SUPPORT_HEARING_ASSIST;
private static final java.lang.String SUPPORT_TRANSMIT;
private static final java.lang.String TAG;
private static Float VIBRATION_DEFAULT_INFINITE_INTENSITY;
private static final java.lang.String VIBRATOR_EVENT_ID;
private static final java.lang.String VIBRATOR_EVENT_NAME;
private static Integer day;
private static java.lang.String mMqsModuleId;
private static final java.util.Set mMusicWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static Integer month;
private static final java.lang.String none;
private static Integer year;
/* # instance fields */
private android.content.Context mContext;
private android.media.MiuiXlog mMiuiXlog;
/* # direct methods */
static com.android.server.audio.MQSUtils ( ) {
/* .locals 50 */
/* .line 39 */
final String v0 = "mqs_audio_data_21031000"; // const-string v0, "mqs_audio_data_21031000"
/* .line 41 */
/* const/high16 v0, 0x3f800000 # 1.0f */
/* .line 60 */
/* new-instance v0, Ljava/util/HashSet; */
final String v1 = "com.netease.cloudmusic"; // const-string v1, "com.netease.cloudmusic"
final String v2 = "com.tencent.qqmusic"; // const-string v2, "com.tencent.qqmusic"
final String v3 = "com.iloen.melon"; // const-string v3, "com.iloen.melon"
final String v4 = "mp3.player.freemusic"; // const-string v4, "mp3.player.freemusic"
final String v5 = "com.kugou.android"; // const-string v5, "com.kugou.android"
final String v6 = "cn.kuwo.player"; // const-string v6, "cn.kuwo.player"
final String v7 = "com.google.android.apps.youtube.music"; // const-string v7, "com.google.android.apps.youtube.music"
final String v8 = "com.tencent.blackkey"; // const-string v8, "com.tencent.blackkey"
final String v9 = "cmccwm.mobilemusic"; // const-string v9, "cmccwm.mobilemusic"
final String v10 = "com.migu.music.mini"; // const-string v10, "com.migu.music.mini"
final String v11 = "com.ting.mp3.android"; // const-string v11, "com.ting.mp3.android"
final String v12 = "com.blueocean.musicplayer"; // const-string v12, "com.blueocean.musicplayer"
final String v13 = "com.tencent.ibg.joox"; // const-string v13, "com.tencent.ibg.joox"
final String v14 = "com.kugou.android.ringtone"; // const-string v14, "com.kugou.android.ringtone"
final String v15 = "com.shoujiduoduo.dj"; // const-string v15, "com.shoujiduoduo.dj"
final String v16 = "com.spotify.music"; // const-string v16, "com.spotify.music"
final String v17 = "com.shoujiduoduo.ringtone"; // const-string v17, "com.shoujiduoduo.ringtone"
final String v18 = "com.hiby.music"; // const-string v18, "com.hiby.music"
final String v19 = "com.miui.player"; // const-string v19, "com.miui.player"
final String v20 = "com.google.android.music"; // const-string v20, "com.google.android.music"
final String v21 = "com.tencent.ibg.joox"; // const-string v21, "com.tencent.ibg.joox"
final String v22 = "com.skysoft.kkbox.android"; // const-string v22, "com.skysoft.kkbox.android"
final String v23 = "com.sofeh.android.musicstudio3"; // const-string v23, "com.sofeh.android.musicstudio3"
final String v24 = "com.gamestar.perfectpiano"; // const-string v24, "com.gamestar.perfectpiano"
final String v25 = "com.opalastudios.pads"; // const-string v25, "com.opalastudios.pads"
final String v26 = "com.magix.android.mmjam"; // const-string v26, "com.magix.android.mmjam"
final String v27 = "com.musicplayer.playermusic"; // const-string v27, "com.musicplayer.playermusic"
final String v28 = "com.gaana"; // const-string v28, "com.gaana"
final String v29 = "com.maxmpz.audioplayer"; // const-string v29, "com.maxmpz.audioplayer"
final String v30 = "com.melodis.midomiMusicIdentifier.freemium"; // const-string v30, "com.melodis.midomiMusicIdentifier.freemium"
final String v31 = "com.mixvibes.remixlive"; // const-string v31, "com.mixvibes.remixlive"
final String v32 = "com.starmakerinteractive.starmaker"; // const-string v32, "com.starmakerinteractive.starmaker"
final String v33 = "com.smule.singandroid"; // const-string v33, "com.smule.singandroid"
final String v34 = "com.djit.apps.stream"; // const-string v34, "com.djit.apps.stream"
/* const-string/jumbo v35, "tunein.service" */
final String v36 = "com.shazam.android"; // const-string v36, "com.shazam.android"
final String v37 = "com.jangomobile.android"; // const-string v37, "com.jangomobile.android"
final String v38 = "com.pandoralite"; // const-string v38, "com.pandoralite"
final String v39 = "com.tube.hqmusic"; // const-string v39, "com.tube.hqmusic"
final String v40 = "com.amazon.avod.thirdpartyclient"; // const-string v40, "com.amazon.avod.thirdpartyclient"
final String v41 = "com.atmusic.app"; // const-string v41, "com.atmusic.app"
final String v42 = "com.rubycell.pianisthd"; // const-string v42, "com.rubycell.pianisthd"
final String v43 = "com.agminstruments.drumpadmachine"; // const-string v43, "com.agminstruments.drumpadmachine"
final String v44 = "com.playermusic.musicplayerapp"; // const-string v44, "com.playermusic.musicplayerapp"
final String v45 = "com.famousbluemedia.piano"; // const-string v45, "com.famousbluemedia.piano"
final String v46 = "com.apple.android.music"; // const-string v46, "com.apple.android.music"
final String v47 = "mb32r.musica.gratis.music.player.free.download"; // const-string v47, "mb32r.musica.gratis.music.player.free.download"
final String v48 = "com.famousbluemedia.yokee"; // const-string v48, "com.famousbluemedia.yokee"
final String v49 = "com.ss.android.ugc.trill"; // const-string v49, "com.ss.android.ugc.trill"
/* filled-new-array/range {v1 ..v49}, [Ljava/lang/String; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V */
return;
} // .end method
public com.android.server.audio.MQSUtils ( ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 89 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 47 */
/* new-instance v0, Landroid/media/MiuiXlog; */
/* invoke-direct {v0}, Landroid/media/MiuiXlog;-><init>()V */
this.mMiuiXlog = v0;
/* .line 90 */
this.mContext = p1;
/* .line 91 */
return;
} // .end method
private java.lang.String check3DAudioSwitchStatus ( ) {
/* .locals 11 */
/* .line 296 */
v0 = this.mContext;
final String v1 = "audio"; // const-string v1, "audio"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/media/AudioManager; */
/* .line 297 */
/* .local v0, "mAudioManager":Landroid/media/AudioManager; */
(( android.media.AudioManager ) v0 ).getSpatializer ( ); // invoke-virtual {v0}, Landroid/media/AudioManager;->getSpatializer()Landroid/media/Spatializer;
/* .line 298 */
/* .local v1, "mSpatializer":Landroid/media/Spatializer; */
v2 = (( android.media.Spatializer ) v1 ).getImmersiveAudioLevel ( ); // invoke-virtual {v1}, Landroid/media/Spatializer;->getImmersiveAudioLevel()I
final String v3 = "open"; // const-string v3, "open"
final String v4 = "close"; // const-string v4, "close"
final String v5 = "3D Audio Switch status is :"; // const-string v5, "3D Audio Switch status is :"
final String v6 = "none"; // const-string v6, "none"
final String v7 = "device not support 3D audio"; // const-string v7, "device not support 3D audio"
int v8 = 1; // const/4 v8, 0x1
final String v9 = "MiSound.MQSUtils"; // const-string v9, "MiSound.MQSUtils"
int v10 = 0; // const/4 v10, 0x0
/* if-lez v2, :cond_3 */
/* .line 299 */
final String v2 = "ro.audio.spatializer_enabled"; // const-string v2, "ro.audio.spatializer_enabled"
v2 = android.os.SystemProperties .getBoolean ( v2,v10 );
/* .line 300 */
/* .local v2, "spatializer_enabled":Z */
/* if-nez v2, :cond_0 */
/* .line 302 */
android.util.Slog .d ( v9,v7 );
/* .line 303 */
/* .line 305 */
} // :cond_0
v6 = this.mContext;
(( android.content.Context ) v6 ).getContentResolver ( ); // invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v7, "spatial_audio_feature_enable" */
v6 = android.provider.Settings$Global .getInt ( v6,v7,v10 );
/* if-ne v6, v8, :cond_1 */
} // :cond_1
/* move v8, v10 */
} // :goto_0
/* move v6, v8 */
/* .line 306 */
/* .local v6, "status":Z */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v9,v5 );
/* .line 307 */
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 308 */
/* .line 310 */
} // :cond_2
/* .line 313 */
} // .end local v2 # "spatializer_enabled":Z
} // .end local v6 # "status":Z
} // :cond_3
final String v2 = "ro.vendor.audio.feature.spatial"; // const-string v2, "ro.vendor.audio.feature.spatial"
v2 = android.os.SystemProperties .getInt ( v2,v10 );
/* .line 314 */
/* .local v2, "flag":I */
if ( v2 != null) { // if-eqz v2, :cond_6
/* if-ne v8, v2, :cond_4 */
/* .line 319 */
} // :cond_4
final String v6 = "persist.vendor.audio.3dsurround.enable"; // const-string v6, "persist.vendor.audio.3dsurround.enable"
v6 = android.os.SystemProperties .getBoolean ( v6,v10 );
/* .line 320 */
/* .restart local v6 # "status":Z */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v9,v5 );
/* .line 321 */
if ( v6 != null) { // if-eqz v6, :cond_5
/* .line 322 */
/* .line 324 */
} // :cond_5
/* .line 316 */
} // .end local v6 # "status":Z
} // :cond_6
} // :goto_1
android.util.Slog .d ( v9,v7 );
/* .line 317 */
} // .end method
private java.lang.String checkAllowSpeakerToRing ( ) {
/* .locals 3 */
/* .line 249 */
final String v0 = "ro.vendor.audio.ring.filter"; // const-string v0, "ro.vendor.audio.ring.filter"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* if-nez v0, :cond_0 */
/* .line 250 */
final String v0 = "MiSound.MQSUtils"; // const-string v0, "MiSound.MQSUtils"
final String v1 = "device not support AllowSpeakerToRing"; // const-string v1, "device not support AllowSpeakerToRing"
android.util.Slog .d ( v0,v1 );
/* .line 251 */
final String v0 = "none"; // const-string v0, "none"
/* .line 254 */
} // :cond_0
v0 = this.mContext;
final String v1 = "audio"; // const-string v1, "audio"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/media/AudioManager; */
/* .line 255 */
/* .local v0, "mAudioManager":Landroid/media/AudioManager; */
/* const-string/jumbo v1, "set_spk_ring_filter_mask" */
(( android.media.AudioManager ) v0 ).getParameters ( v1 ); // invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;
/* .line 257 */
/* .local v1, "paramStr":Ljava/lang/String; */
/* const-string/jumbo v2, "set_spk_ring_filter_mask=0" */
v2 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 258 */
final String v2 = "open"; // const-string v2, "open"
/* .line 259 */
} // :cond_1
/* const-string/jumbo v2, "set_spk_ring_filter_mask=3" */
v2 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 260 */
final String v2 = "close"; // const-string v2, "close"
/* .line 262 */
} // :cond_2
final String v2 = "Undefined"; // const-string v2, "Undefined"
} // .end method
private java.lang.String checkAudioVisualStatus ( ) {
/* .locals 3 */
/* .line 192 */
final String v0 = "ro.vendor.audio.sfx.audiovisual"; // const-string v0, "ro.vendor.audio.sfx.audiovisual"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* if-nez v0, :cond_0 */
/* .line 193 */
final String v0 = "MiSound.MQSUtils"; // const-string v0, "MiSound.MQSUtils"
final String v1 = "device not support AudioVisual"; // const-string v1, "device not support AudioVisual"
android.util.Slog .d ( v0,v1 );
/* .line 194 */
final String v0 = "none"; // const-string v0, "none"
/* .line 195 */
} // :cond_0
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "audio_visual_screen_lock_on"; // const-string v2, "audio_visual_screen_lock_on"
v0 = android.provider.Settings$Global .getInt ( v0,v2,v1 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_1 */
/* .line 197 */
final String v0 = "open"; // const-string v0, "open"
/* .line 199 */
} // :cond_1
final String v0 = "close"; // const-string v0, "close"
} // .end method
private java.lang.String checkDolbySwitchStatus ( ) {
/* .locals 4 */
/* .line 282 */
final String v0 = "ro.vendor.audio.dolby.dax.support"; // const-string v0, "ro.vendor.audio.dolby.dax.support"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
final String v2 = "MiSound.MQSUtils"; // const-string v2, "MiSound.MQSUtils"
/* if-nez v0, :cond_0 */
/* .line 283 */
final String v0 = "device not support dolby audio"; // const-string v0, "device not support dolby audio"
android.util.Slog .d ( v2,v0 );
/* .line 284 */
final String v0 = "none"; // const-string v0, "none"
/* .line 286 */
} // :cond_0
final String v0 = "persist.vendor.audio.misound.disable"; // const-string v0, "persist.vendor.audio.misound.disable"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* .line 287 */
/* .local v0, "status":Z */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Dolby Switch status is :"; // const-string v3, "Dolby Switch status is :"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v1 );
/* .line 288 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 289 */
final String v1 = "open"; // const-string v1, "open"
/* .line 291 */
} // :cond_1
final String v1 = "close"; // const-string v1, "close"
} // .end method
private java.lang.String checkEarsCompensationStatus ( ) {
/* .locals 4 */
/* .line 268 */
final String v0 = "ro.vendor.audio.sfx.earadj"; // const-string v0, "ro.vendor.audio.sfx.earadj"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
final String v1 = "MiSound.MQSUtils"; // const-string v1, "MiSound.MQSUtils"
/* if-nez v0, :cond_0 */
/* .line 269 */
final String v0 = "device not support EarsCompensation"; // const-string v0, "device not support EarsCompensation"
android.util.Slog .d ( v1,v0 );
/* .line 270 */
final String v0 = "none"; // const-string v0, "none"
/* .line 272 */
} // :cond_0
final String v0 = "persist.vendor.audio.ears.compensation.state"; // const-string v0, "persist.vendor.audio.ears.compensation.state"
final String v2 = ""; // const-string v2, ""
android.os.SystemProperties .get ( v0,v2 );
/* .line 273 */
/* .local v0, "status":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "ears compensation status is :"; // const-string v3, "ears compensation status is :"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* .line 274 */
final String v1 = "1"; // const-string v1, "1"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 275 */
final String v1 = "open"; // const-string v1, "open"
/* .line 277 */
} // :cond_1
final String v1 = "close"; // const-string v1, "close"
} // .end method
private java.lang.String checkHIFIStatus ( ) {
/* .locals 4 */
/* .line 226 */
final String v0 = "ro.vendor.audio.hifi"; // const-string v0, "ro.vendor.audio.hifi"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
final String v2 = "MiSound.MQSUtils"; // const-string v2, "MiSound.MQSUtils"
/* if-nez v0, :cond_0 */
/* .line 227 */
final String v0 = "device not support HIFI"; // const-string v0, "device not support HIFI"
android.util.Slog .d ( v2,v0 );
/* .line 228 */
final String v0 = "none"; // const-string v0, "none"
/* .line 230 */
} // :cond_0
final String v0 = "persist.vendor.audio.hifi"; // const-string v0, "persist.vendor.audio.hifi"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* .line 231 */
/* .local v0, "status":Z */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "HiFi Switch status is :"; // const-string v3, "HiFi Switch status is :"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v1 );
/* .line 232 */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_1 */
/* .line 233 */
final String v1 = "open"; // const-string v1, "open"
/* .line 235 */
} // :cond_1
final String v1 = "close"; // const-string v1, "close"
} // .end method
private java.lang.String checkHapticStatus ( ) {
/* .locals 4 */
/* .line 562 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const/16 v1, 0x3e8 */
int v2 = -2; // const/4 v2, -0x2
final String v3 = "haptic_feedback_enabled"; // const-string v3, "haptic_feedback_enabled"
v0 = android.provider.Settings$System .getIntForUser ( v0,v3,v1,v2 );
/* .line 563 */
/* .local v0, "status":I */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* .line 564 */
final String v1 = "open"; // const-string v1, "open"
/* .line 565 */
} // :cond_0
/* if-nez v0, :cond_1 */
/* .line 566 */
final String v1 = "close"; // const-string v1, "close"
/* .line 568 */
} // :cond_1
final String v1 = "none"; // const-string v1, "none"
} // .end method
private java.lang.String checkHarmankardonStatus ( ) {
/* .locals 3 */
/* .line 204 */
final String v0 = "ro.vendor.audio.sfx.harmankardon"; // const-string v0, "ro.vendor.audio.sfx.harmankardon"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* if-nez v0, :cond_0 */
/* .line 205 */
final String v0 = "MiSound.MQSUtils"; // const-string v0, "MiSound.MQSUtils"
final String v1 = "device not support harmankardon"; // const-string v1, "device not support harmankardon"
android.util.Slog .d ( v0,v1 );
/* .line 206 */
final String v0 = "none"; // const-string v0, "none"
/* .line 208 */
} // :cond_0
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v2, "settings_system_harman_kardon_enable" */
v0 = android.provider.Settings$Global .getInt ( v0,v2,v1 );
/* .line 209 */
/* .local v0, "flag":I */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v1, v0, :cond_1 */
/* .line 210 */
final String v1 = "open"; // const-string v1, "open"
/* .line 212 */
} // :cond_1
final String v1 = "close"; // const-string v1, "close"
} // .end method
private Boolean checkHearingAssistSupport ( ) {
/* .locals 4 */
/* .line 175 */
v0 = this.mContext;
final String v1 = "audio"; // const-string v1, "audio"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/media/AudioManager; */
/* .line 176 */
/* .local v0, "audioManager":Landroid/media/AudioManager; */
/* const-string/jumbo v1, "sound_transmit_ha_support" */
(( android.media.AudioManager ) v0 ).getParameters ( v1 ); // invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;
/* .line 177 */
/* .local v1, "hearingAssistSupport":Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_1
v2 = (( java.lang.String ) v1 ).length ( ); // invoke-virtual {v1}, Ljava/lang/String;->length()I
int v3 = 1; // const/4 v3, 0x1
/* if-lt v2, v3, :cond_1 */
/* const-string/jumbo v2, "sound_transmit_ha_support=true" */
v2 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_0 */
/* .line 180 */
} // :cond_0
/* .line 178 */
} // :cond_1
} // :goto_0
int v2 = 0; // const/4 v2, 0x0
} // .end method
private java.lang.String checkMisoundEqStatus ( ) {
/* .locals 5 */
/* .line 377 */
final String v0 = "persist.vendor.audio.sfx.hd.eq"; // const-string v0, "persist.vendor.audio.sfx.hd.eq"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
/* .line 378 */
/* .local v0, "eq":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "misound eq is :"; // const-string v3, "misound eq is :"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiSound.MQSUtils"; // const-string v3, "MiSound.MQSUtils"
android.util.Slog .i ( v3,v2 );
/* .line 380 */
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 381 */
final String v1 = "null"; // const-string v1, "null"
/* .local v1, "status":Ljava/lang/String; */
/* .line 382 */
} // .end local v1 # "status":Ljava/lang/String;
} // :cond_0
final String v1 = "0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000"; // const-string v1, "0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 383 */
final String v1 = "close"; // const-string v1, "close"
/* .restart local v1 # "status":Ljava/lang/String; */
/* .line 385 */
} // .end local v1 # "status":Ljava/lang/String;
} // :cond_1
final String v1 = "open"; // const-string v1, "open"
/* .line 388 */
/* .restart local v1 # "status":Ljava/lang/String; */
} // :goto_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "misound eq status is :"; // const-string v4, "misound eq status is :"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v2 );
/* .line 389 */
} // .end method
private java.lang.String checkMisoundHeadphoneType ( ) {
/* .locals 3 */
/* .line 371 */
final String v0 = "persist.vendor.audio.sfx.hd.type"; // const-string v0, "persist.vendor.audio.sfx.hd.type"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
/* .line 372 */
/* .local v0, "type":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "misound headphone type is :"; // const-string v2, "misound headphone type is :"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiSound.MQSUtils"; // const-string v2, "MiSound.MQSUtils"
android.util.Slog .i ( v2,v1 );
/* .line 373 */
} // .end method
private java.lang.String checkMisoundStatus ( ) {
/* .locals 3 */
/* .line 359 */
final String v0 = "persist.vendor.audio.sfx.hd.music.state"; // const-string v0, "persist.vendor.audio.sfx.hd.music.state"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
/* .line 360 */
/* .local v0, "status":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "misound status is :"; // const-string v2, "misound status is :"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiSound.MQSUtils"; // const-string v2, "MiSound.MQSUtils"
android.util.Slog .i ( v2,v1 );
/* .line 361 */
final String v1 = "1"; // const-string v1, "1"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 362 */
final String v1 = "open"; // const-string v1, "open"
/* .line 363 */
} // :cond_0
final String v1 = "0"; // const-string v1, "0"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 364 */
final String v1 = "close"; // const-string v1, "close"
/* .line 366 */
} // :cond_1
final String v1 = "none"; // const-string v1, "none"
} // .end method
private java.lang.String checkMultiAppVolumeStatus ( ) {
/* .locals 3 */
/* .line 217 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "sound_assist_key" */
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$Global .getInt ( v0,v1,v2 );
/* .line 218 */
/* .local v0, "status":I */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* .line 219 */
final String v1 = "open"; // const-string v1, "open"
/* .line 221 */
} // :cond_0
final String v1 = "close"; // const-string v1, "close"
} // .end method
private java.lang.String checkMultiSoundStatus ( ) {
/* .locals 3 */
/* .line 240 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "key_ignore_music_focus_req"; // const-string v1, "key_ignore_music_focus_req"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$Global .getInt ( v0,v1,v2 );
/* .line 241 */
/* .local v0, "status":I */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* .line 242 */
final String v1 = "open"; // const-string v1, "open"
/* .line 244 */
} // :cond_0
final String v1 = "close"; // const-string v1, "close"
} // .end method
private java.lang.String checkSpatialAudioSwitchStatus ( ) {
/* .locals 9 */
/* .line 330 */
v0 = this.mContext;
final String v1 = "audio"; // const-string v1, "audio"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/media/AudioManager; */
/* .line 331 */
/* .local v0, "mAudioManager":Landroid/media/AudioManager; */
(( android.media.AudioManager ) v0 ).getSpatializer ( ); // invoke-virtual {v0}, Landroid/media/AudioManager;->getSpatializer()Landroid/media/Spatializer;
/* .line 332 */
/* .local v1, "mSpatializer":Landroid/media/Spatializer; */
v2 = (( android.media.Spatializer ) v1 ).getImmersiveAudioLevel ( ); // invoke-virtual {v1}, Landroid/media/Spatializer;->getImmersiveAudioLevel()I
final String v3 = "open"; // const-string v3, "open"
final String v4 = "close"; // const-string v4, "close"
final String v5 = "Spatial Audio Switch status is :"; // const-string v5, "Spatial Audio Switch status is :"
final String v6 = "MiSound.MQSUtils"; // const-string v6, "MiSound.MQSUtils"
int v7 = 0; // const/4 v7, 0x0
/* if-lez v2, :cond_2 */
/* .line 333 */
v2 = (( android.media.Spatializer ) v1 ).getDesiredHeadTrackingMode ( ); // invoke-virtual {v1}, Landroid/media/Spatializer;->getDesiredHeadTrackingMode()I
/* .line 334 */
/* .local v2, "mode":I */
int v8 = 1; // const/4 v8, 0x1
/* if-ne v2, v8, :cond_0 */
/* move v7, v8 */
/* .line 335 */
/* .local v7, "status":Z */
} // :cond_0
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v5 ); // invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v6,v5 );
/* .line 336 */
if ( v7 != null) { // if-eqz v7, :cond_1
/* .line 337 */
/* .line 339 */
} // :cond_1
/* .line 342 */
} // .end local v2 # "mode":I
} // .end local v7 # "status":Z
} // :cond_2
final String v2 = "ro.vendor.audio.feature.spatial"; // const-string v2, "ro.vendor.audio.feature.spatial"
v2 = android.os.SystemProperties .getInt ( v2,v7 );
/* .line 343 */
/* .local v2, "flag":I */
if ( v2 != null) { // if-eqz v2, :cond_5
int v8 = 2; // const/4 v8, 0x2
/* if-ne v8, v2, :cond_3 */
/* .line 348 */
} // :cond_3
final String v8 = "persist.vendor.audio.spatial.enable"; // const-string v8, "persist.vendor.audio.spatial.enable"
v7 = android.os.SystemProperties .getBoolean ( v8,v7 );
/* .line 349 */
/* .restart local v7 # "status":Z */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v5 ); // invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v6,v5 );
/* .line 350 */
if ( v7 != null) { // if-eqz v7, :cond_4
/* .line 351 */
/* .line 353 */
} // :cond_4
/* .line 345 */
} // .end local v7 # "status":Z
} // :cond_5
} // :goto_0
final String v3 = "device not support spatial audio"; // const-string v3, "device not support spatial audio"
android.util.Slog .d ( v6,v3 );
/* .line 346 */
final String v3 = "none"; // const-string v3, "none"
} // .end method
private Boolean checkVoiceprintNoiseReductionSupport ( ) {
/* .locals 2 */
/* .line 184 */
final String v0 = "ro.vendor.audio.voip.assistant"; // const-string v0, "ro.vendor.audio.voip.assistant"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 185 */
int v0 = 1; // const/4 v0, 0x1
/* .line 187 */
} // :cond_0
} // .end method
private Boolean checkWirelessTransmissionSupport ( ) {
/* .locals 4 */
/* .line 166 */
v0 = this.mContext;
final String v1 = "audio"; // const-string v1, "audio"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/media/AudioManager; */
/* .line 167 */
/* .local v0, "audioManager":Landroid/media/AudioManager; */
/* const-string/jumbo v1, "sound_transmit_support" */
(( android.media.AudioManager ) v0 ).getParameters ( v1 ); // invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;
/* .line 168 */
/* .local v1, "transmitSupport":Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_1
v2 = (( java.lang.String ) v1 ).length ( ); // invoke-virtual {v1}, Ljava/lang/String;->length()I
int v3 = 1; // const/4 v3, 0x1
/* if-lt v2, v3, :cond_1 */
/* const-string/jumbo v2, "sound_transmit_support=true" */
v2 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_0 */
/* .line 171 */
} // :cond_0
/* .line 169 */
} // :cond_1
} // :goto_0
int v2 = 0; // const/4 v2, 0x0
} // .end method
private static android.content.Intent configureIntent ( java.lang.String p0, java.lang.String p1, android.os.Bundle p2 ) {
/* .locals 3 */
/* .param p0, "appId" # Ljava/lang/String; */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "params" # Landroid/os/Bundle; */
/* .line 143 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "onetrack.action.TRACK_EVENT"; // const-string v1, "onetrack.action.TRACK_EVENT"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 144 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.miui.analytics"; // const-string v1, "com.miui.analytics"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 145 */
final String v1 = "PACKAGE"; // const-string v1, "PACKAGE"
final String v2 = "android"; // const-string v2, "android"
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 146 */
int v1 = 3; // const/4 v1, 0x3
(( android.content.Intent ) v0 ).setFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 147 */
final String v1 = "APP_ID"; // const-string v1, "APP_ID"
(( android.content.Intent ) v0 ).putExtra ( v1, p0 ); // invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 148 */
final String v1 = "EVENT_NAME"; // const-string v1, "EVENT_NAME"
(( android.content.Intent ) v0 ).putExtra ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 149 */
(( android.content.Intent ) v0 ).putExtras ( p2 ); // invoke-virtual {v0, p2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
/* .line 151 */
} // .end method
private Float getHapticFeedbackFloatLevel ( ) {
/* .locals 4 */
/* .line 573 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v2 = -2; // const/4 v2, -0x2
final String v3 = "haptic_feedback_infinite_intensity"; // const-string v3, "haptic_feedback_infinite_intensity"
v0 = android.provider.Settings$System .getFloatForUser ( v0,v3,v1,v2 );
} // .end method
private Boolean isGlobalBuild ( ) {
/* .locals 1 */
/* .line 658 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
} // .end method
private Boolean isNotAllowedRegion ( ) {
/* .locals 4 */
/* .line 662 */
final String v0 = "ro.miui.region"; // const-string v0, "ro.miui.region"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
/* .line 663 */
/* .local v0, "region":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "the region is :" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiSound.MQSUtils"; // const-string v3, "MiSound.MQSUtils"
android.util.Slog .i ( v3,v2 );
/* .line 664 */
final String v2 = "IN"; // const-string v2, "IN"
v2 = (( java.lang.String ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_1 */
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :cond_1
} // :goto_0
int v1 = 1; // const/4 v1, 0x1
} // :goto_1
} // .end method
private Boolean isReportXiaomiServer ( ) {
/* .locals 3 */
/* .line 668 */
final String v0 = "ro.miui.region"; // const-string v0, "ro.miui.region"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
/* .line 669 */
/* .local v0, "region":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "the region is :" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiSound.MQSUtils"; // const-string v2, "MiSound.MQSUtils"
android.util.Slog .i ( v2,v1 );
/* .line 670 */
final String v1 = "CN"; // const-string v1, "CN"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_1 */
final String v1 = "RU"; // const-string v1, "RU"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :cond_1
} // :goto_0
int v1 = 1; // const/4 v1, 0x1
} // :goto_1
} // .end method
public static Boolean trackConcurrentVoipInfo ( android.content.Context p0, java.util.List p1 ) {
/* .locals 5 */
/* .param p0, "context" # Landroid/content/Context; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/content/Context;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 94 */
/* .local p1, "clients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
int v0 = 0; // const/4 v0, 0x0
if ( p0 != null) { // if-eqz p0, :cond_2
v1 = if ( p1 != null) { // if-eqz p1, :cond_2
/* if-nez v1, :cond_0 */
/* .line 99 */
} // :cond_0
try { // :try_start_0
/* new-instance v1, Lcom/android/server/audio/MQSUtils; */
/* invoke-direct {v1, p0}, Lcom/android/server/audio/MQSUtils;-><init>(Landroid/content/Context;)V */
/* .line 100 */
/* .local v1, "util":Lcom/android/server/audio/MQSUtils; */
v2 = /* invoke-direct {v1}, Lcom/android/server/audio/MQSUtils;->isReportXiaomiServer()Z */
/* if-nez v2, :cond_1 */
/* .line 101 */
/* .line 104 */
} // :cond_1
/* new-instance v2, Landroid/os/Bundle; */
/* invoke-direct {v2}, Landroid/os/Bundle;-><init>()V */
/* .line 105 */
/* .local v2, "params":Landroid/os/Bundle; */
/* const-string/jumbo v3, "voip_packages" */
java.util.Arrays .toString ( v4 );
(( android.os.Bundle ) v2 ).putString ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 106 */
final String v3 = "31000000086"; // const-string v3, "31000000086"
final String v4 = "concurrent_voip"; // const-string v4, "concurrent_voip"
com.android.server.audio.MQSUtils .configureIntent ( v3,v4,v2 );
(( android.content.Context ) p0 ).startService ( v3 ); // invoke-virtual {p0, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 107 */
int v0 = 1; // const/4 v0, 0x1
/* .line 108 */
} // .end local v1 # "util":Lcom/android/server/audio/MQSUtils;
} // .end local v2 # "params":Landroid/os/Bundle;
/* :catch_0 */
/* move-exception v1 */
/* .line 109 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "trackConcurrentVoipInfo exception : " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiSound.MQSUtils"; // const-string v3, "MiSound.MQSUtils"
android.util.Log .d ( v3,v2 );
/* .line 111 */
} // .end local v1 # "e":Ljava/lang/Exception;
/* .line 95 */
} // :cond_2
} // :goto_0
} // .end method
/* # virtual methods */
public Boolean needToReport ( ) {
/* .locals 6 */
/* .line 155 */
java.util.Calendar .getInstance ( );
/* .line 156 */
/* .local v0, "calendar":Ljava/util/Calendar; */
int v2 = 1; // const/4 v2, 0x1
v3 = (( java.util.Calendar ) v0 ).get ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I
int v4 = 5; // const/4 v4, 0x5
int v5 = 2; // const/4 v5, 0x2
/* if-ne v1, v3, :cond_0 */
v3 = (( java.util.Calendar ) v0 ).get ( v5 ); // invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I
/* add-int/2addr v3, v2 */
/* if-ne v1, v3, :cond_0 */
v3 = (( java.util.Calendar ) v0 ).get ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I
/* if-ne v1, v3, :cond_0 */
/* .line 157 */
int v1 = 0; // const/4 v1, 0x0
/* .line 158 */
} // :cond_0
v1 = (( java.util.Calendar ) v0 ).get ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I
/* .line 159 */
v1 = (( java.util.Calendar ) v0 ).get ( v5 ); // invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I
/* add-int/2addr v1, v2 */
/* .line 160 */
v1 = (( java.util.Calendar ) v0 ).get ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I
/* .line 161 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "needToReport year: "; // const-string v3, "needToReport year: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = "month: "; // const-string v3, "month: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = "day: "; // const-string v3, "day: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiSound.MQSUtils"; // const-string v3, "MiSound.MQSUtils"
android.util.Slog .d ( v3,v1 );
/* .line 162 */
} // .end method
public void reportAbnormalAudioStatus ( com.android.server.audio.MQSUtils$AudioStateTrackData p0 ) {
/* .locals 9 */
/* .param p1, "audioStateTrackData" # Lcom/android/server/audio/MQSUtils$AudioStateTrackData; */
/* .line 115 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "[TF-BT] reportAbnormalAudioStatus: "; // const-string v1, "[TF-BT] reportAbnormalAudioStatus: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiSound.MQSUtils"; // const-string v1, "MiSound.MQSUtils"
android.util.Log .d ( v1,v0 );
/* .line 116 */
v0 = this.mContext;
if ( v0 != null) { // if-eqz v0, :cond_2
/* if-nez p1, :cond_0 */
/* .line 119 */
} // :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/audio/MQSUtils;->isReportXiaomiServer()Z */
/* if-nez v0, :cond_1 */
/* .line 120 */
return;
/* .line 122 */
} // :cond_1
v2 = this.mAbnormalReason;
v3 = this.mEventSource;
v4 = this.mPackageName;
v5 = this.mBtName;
/* iget v0, p1, Lcom/android/server/audio/MQSUtils$AudioStateTrackData;->mA2dpConnectState:I */
/* .line 133 */
java.lang.Integer .valueOf ( v0 );
/* iget v0, p1, Lcom/android/server/audio/MQSUtils$AudioStateTrackData;->mScoConnectState:I */
java.lang.Integer .valueOf ( v0 );
v8 = this.mAudioStateName;
/* filled-new-array/range {v2 ..v8}, [Ljava/lang/Object; */
/* .line 122 */
/* const-string/jumbo v2, "{\"name\":\"check_audio_route_for_bluetooth\",\"audio_event\":{\"audio_route_abnormal_reason\":\"%s\",\"audio_exception_details\":\"%s\",\"package_name\":\"%s\",\"btName\":\"%s\",\"a2dp_connect_state\":\"%d\",\"sco_connect_state\":\"%d\",\"audio_status_name\":\"%s\"},\"dgt\":\"null\",\"audio_ext\":\"null\" }" */
java.lang.String .format ( v2,v0 );
/* .line 136 */
/* .local v0, "result":Ljava/lang/String; */
try { // :try_start_0
v2 = this.mMiuiXlog;
(( android.media.MiuiXlog ) v2 ).miuiXlogSend ( v0 ); // invoke-virtual {v2, v0}, Landroid/media/MiuiXlog;->miuiXlogSend(Ljava/lang/String;)I
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 139 */
/* .line 137 */
/* :catch_0 */
/* move-exception v2 */
/* .line 138 */
/* .local v2, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "[TF-BT] reportAbnormalAudioStatus exception : "; // const-string v4, "[TF-BT] reportAbnormalAudioStatus exception : "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v3 );
/* .line 140 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_0
return;
/* .line 117 */
} // .end local v0 # "result":Ljava/lang/String;
} // :cond_2
} // :goto_1
return;
} // .end method
public void reportAudioButtonStatus ( ) {
/* .locals 21 */
/* .line 410 */
final String v0 = "open"; // const-string v0, "open"
final String v1 = "reportAudioButtonStatus start."; // const-string v1, "reportAudioButtonStatus start."
final String v2 = "MiSound.MQSUtils"; // const-string v2, "MiSound.MQSUtils"
android.util.Slog .i ( v2,v1 );
/* .line 411 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/audio/MQSUtils;->checkAudioVisualStatus()Ljava/lang/String; */
/* .line 412 */
/* .local v1, "audio_visual_status":Ljava/lang/String; */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/audio/MQSUtils;->checkMultiAppVolumeStatus()Ljava/lang/String; */
/* .line 413 */
/* .local v3, "multi_app_volume_button":Ljava/lang/String; */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/audio/MQSUtils;->checkHIFIStatus()Ljava/lang/String; */
/* .line 414 */
/* .local v4, "HIFI_button":Ljava/lang/String; */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/audio/MQSUtils;->checkMultiSoundStatus()Ljava/lang/String; */
/* .line 415 */
/* .local v5, "multi_sound_button":Ljava/lang/String; */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/audio/MQSUtils;->checkAllowSpeakerToRing()Ljava/lang/String; */
/* .line 416 */
/* .local v6, "allow_speaker_to_ring_button":Ljava/lang/String; */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/audio/MQSUtils;->checkEarsCompensationStatus()Ljava/lang/String; */
/* .line 417 */
/* .local v7, "ears_compensation_button":Ljava/lang/String; */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/audio/MQSUtils;->checkDolbySwitchStatus()Ljava/lang/String; */
/* .line 418 */
/* .local v8, "dolby_button":Ljava/lang/String; */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/audio/MQSUtils;->check3DAudioSwitchStatus()Ljava/lang/String; */
/* .line 419 */
/* .local v9, "_3D_audio_button":Ljava/lang/String; */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/audio/MQSUtils;->checkSpatialAudioSwitchStatus()Ljava/lang/String; */
/* .line 420 */
/* .local v10, "spatial_audio_button":Ljava/lang/String; */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/audio/MQSUtils;->checkMisoundStatus()Ljava/lang/String; */
/* .line 421 */
/* .local v11, "misound_status_button":Ljava/lang/String; */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/audio/MQSUtils;->checkMisoundEqStatus()Ljava/lang/String; */
/* .line 422 */
/* .local v12, "misound_eq_button":Ljava/lang/String; */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/audio/MQSUtils;->checkHarmankardonStatus()Ljava/lang/String; */
/* .line 423 */
/* .local v13, "harmankardon_status":Ljava/lang/String; */
/* new-instance v14, Ljava/lang/StringBuilder; */
/* invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V */
final String v15 = "ears compensation button is :"; // const-string v15, "ears compensation button is :"
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).append ( v7 ); // invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).toString ( ); // invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v14 );
/* .line 426 */
try { // :try_start_0
/* new-instance v14, Landroid/content/Intent; */
final String v15 = "onetrack.action.TRACK_EVENT"; // const-string v15, "onetrack.action.TRACK_EVENT"
/* invoke-direct {v14, v15}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 427 */
/* .local v14, "intent":Landroid/content/Intent; */
v15 = /* invoke-direct/range {p0 ..p0}, Lcom/android/server/audio/MQSUtils;->isGlobalBuild()Z */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_a */
/* move-object/from16 v16, v2 */
int v2 = 2; // const/4 v2, 0x2
/* if-nez v15, :cond_0 */
/* .line 428 */
try { // :try_start_1
(( android.content.Intent ) v14 ).setFlags ( v2 ); // invoke-virtual {v14, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 548 */
} // .end local v14 # "intent":Landroid/content/Intent;
/* :catch_0 */
/* move-exception v0 */
/* move-object v2, v10 */
/* move-object v10, v11 */
/* move-object v11, v12 */
/* move-object/from16 v12, p0 */
/* goto/16 :goto_11 */
/* .line 430 */
/* .restart local v14 # "intent":Landroid/content/Intent; */
} // :cond_0
} // :goto_0
try { // :try_start_2
final String v15 = "com.miui.analytics"; // const-string v15, "com.miui.analytics"
(( android.content.Intent ) v14 ).setPackage ( v15 ); // invoke-virtual {v14, v15}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 431 */
final String v15 = "APP_ID"; // const-string v15, "APP_ID"
final String v2 = "31000000086"; // const-string v2, "31000000086"
(( android.content.Intent ) v14 ).putExtra ( v15, v2 ); // invoke-virtual {v14, v15, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 432 */
final String v2 = "EVENT_NAME"; // const-string v2, "EVENT_NAME"
final String v15 = "audio_button"; // const-string v15, "audio_button"
(( android.content.Intent ) v14 ).putExtra ( v2, v15 ); // invoke-virtual {v14, v2, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 433 */
final String v2 = "PACKAGE"; // const-string v2, "PACKAGE"
final String v15 = "android"; // const-string v15, "android"
(( android.content.Intent ) v14 ).putExtra ( v2, v15 ); // invoke-virtual {v14, v2, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 434 */
/* new-instance v2, Landroid/os/Bundle; */
/* invoke-direct {v2}, Landroid/os/Bundle;-><init>()V */
/* .line 435 */
/* .local v2, "params":Landroid/os/Bundle; */
v15 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_9 */
/* move-object/from16 v17, v2 */
} // .end local v2 # "params":Landroid/os/Bundle;
/* .local v17, "params":Landroid/os/Bundle; */
final String v2 = "audio_visual_status"; // const-string v2, "audio_visual_status"
/* move-object/from16 v18, v12 */
} // .end local v12 # "misound_eq_button":Ljava/lang/String;
/* .local v18, "misound_eq_button":Ljava/lang/String; */
final String v12 = "none"; // const-string v12, "none"
/* move-object/from16 v19, v11 */
} // .end local v11 # "misound_status_button":Ljava/lang/String;
/* .local v19, "misound_status_button":Ljava/lang/String; */
final String v11 = "close"; // const-string v11, "close"
/* move-object/from16 v20, v10 */
} // .end local v10 # "spatial_audio_button":Ljava/lang/String;
/* .local v20, "spatial_audio_button":Ljava/lang/String; */
int v10 = 1; // const/4 v10, 0x1
if ( v15 != null) { // if-eqz v15, :cond_1
/* .line 436 */
try { // :try_start_3
(( android.content.Intent ) v14 ).putExtra ( v2, v10 ); // invoke-virtual {v14, v2, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_1 */
/* .line 548 */
} // .end local v14 # "intent":Landroid/content/Intent;
} // .end local v17 # "params":Landroid/os/Bundle;
/* :catch_1 */
/* move-exception v0 */
/* move-object/from16 v12, p0 */
/* move-object/from16 v11, v18 */
/* move-object/from16 v10, v19 */
/* move-object/from16 v2, v20 */
/* goto/16 :goto_11 */
/* .line 437 */
/* .restart local v14 # "intent":Landroid/content/Intent; */
/* .restart local v17 # "params":Landroid/os/Bundle; */
} // :cond_1
try { // :try_start_4
v15 = (( java.lang.String ) v1 ).equals ( v11 ); // invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_8 */
if ( v15 != null) { // if-eqz v15, :cond_2
/* .line 438 */
int v15 = 0; // const/4 v15, 0x0
try { // :try_start_5
(( android.content.Intent ) v14 ).putExtra ( v2, v15 ); // invoke-virtual {v14, v2, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* :try_end_5 */
/* .catch Ljava/lang/Exception; {:try_start_5 ..:try_end_5} :catch_1 */
/* .line 440 */
} // :cond_2
try { // :try_start_6
(( android.content.Intent ) v14 ).putExtra ( v2, v12 ); // invoke-virtual {v14, v2, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 443 */
} // :goto_1
v2 = (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_6 */
/* .catch Ljava/lang/Exception; {:try_start_6 ..:try_end_6} :catch_8 */
final String v15 = "multi_app_volume_button"; // const-string v15, "multi_app_volume_button"
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 444 */
try { // :try_start_7
(( android.content.Intent ) v14 ).putExtra ( v15, v10 ); // invoke-virtual {v14, v15, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* :try_end_7 */
/* .catch Ljava/lang/Exception; {:try_start_7 ..:try_end_7} :catch_1 */
/* .line 446 */
} // :cond_3
int v2 = 0; // const/4 v2, 0x0
try { // :try_start_8
(( android.content.Intent ) v14 ).putExtra ( v15, v2 ); // invoke-virtual {v14, v15, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 449 */
} // :goto_2
v2 = (( java.lang.String ) v4 ).equals ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_8 */
/* .catch Ljava/lang/Exception; {:try_start_8 ..:try_end_8} :catch_8 */
final String v15 = "HIFI_button"; // const-string v15, "HIFI_button"
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 450 */
try { // :try_start_9
(( android.content.Intent ) v14 ).putExtra ( v15, v10 ); // invoke-virtual {v14, v15, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* :try_end_9 */
/* .catch Ljava/lang/Exception; {:try_start_9 ..:try_end_9} :catch_1 */
/* .line 451 */
} // :cond_4
try { // :try_start_a
v2 = (( java.lang.String ) v4 ).equals ( v11 ); // invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_a */
/* .catch Ljava/lang/Exception; {:try_start_a ..:try_end_a} :catch_8 */
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 452 */
int v2 = 0; // const/4 v2, 0x0
try { // :try_start_b
(( android.content.Intent ) v14 ).putExtra ( v15, v2 ); // invoke-virtual {v14, v15, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* :try_end_b */
/* .catch Ljava/lang/Exception; {:try_start_b ..:try_end_b} :catch_1 */
/* .line 454 */
} // :cond_5
try { // :try_start_c
(( android.content.Intent ) v14 ).putExtra ( v15, v12 ); // invoke-virtual {v14, v15, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 457 */
} // :goto_3
v2 = (( java.lang.String ) v5 ).equals ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_c */
/* .catch Ljava/lang/Exception; {:try_start_c ..:try_end_c} :catch_8 */
final String v15 = "multi_sound_button"; // const-string v15, "multi_sound_button"
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 458 */
try { // :try_start_d
(( android.content.Intent ) v14 ).putExtra ( v15, v10 ); // invoke-virtual {v14, v15, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* :try_end_d */
/* .catch Ljava/lang/Exception; {:try_start_d ..:try_end_d} :catch_1 */
/* .line 460 */
} // :cond_6
int v2 = 0; // const/4 v2, 0x0
try { // :try_start_e
(( android.content.Intent ) v14 ).putExtra ( v15, v2 ); // invoke-virtual {v14, v15, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 463 */
} // :goto_4
v2 = (( java.lang.String ) v6 ).equals ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_e */
/* .catch Ljava/lang/Exception; {:try_start_e ..:try_end_e} :catch_8 */
final String v15 = "allow_speaker_to_ring_button"; // const-string v15, "allow_speaker_to_ring_button"
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 464 */
try { // :try_start_f
(( android.content.Intent ) v14 ).putExtra ( v15, v10 ); // invoke-virtual {v14, v15, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* :try_end_f */
/* .catch Ljava/lang/Exception; {:try_start_f ..:try_end_f} :catch_1 */
/* .line 465 */
} // :cond_7
try { // :try_start_10
v2 = (( java.lang.String ) v6 ).equals ( v11 ); // invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_10 */
/* .catch Ljava/lang/Exception; {:try_start_10 ..:try_end_10} :catch_8 */
if ( v2 != null) { // if-eqz v2, :cond_8
/* .line 466 */
int v2 = 0; // const/4 v2, 0x0
try { // :try_start_11
(( android.content.Intent ) v14 ).putExtra ( v15, v2 ); // invoke-virtual {v14, v15, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* :try_end_11 */
/* .catch Ljava/lang/Exception; {:try_start_11 ..:try_end_11} :catch_1 */
/* .line 468 */
} // :cond_8
try { // :try_start_12
(( android.content.Intent ) v14 ).putExtra ( v15, v12 ); // invoke-virtual {v14, v15, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 471 */
} // :goto_5
v2 = (( java.lang.String ) v7 ).equals ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_12 */
/* .catch Ljava/lang/Exception; {:try_start_12 ..:try_end_12} :catch_8 */
final String v15 = "hear_compensation_state"; // const-string v15, "hear_compensation_state"
if ( v2 != null) { // if-eqz v2, :cond_9
/* .line 472 */
try { // :try_start_13
(( android.content.Intent ) v14 ).putExtra ( v15, v10 ); // invoke-virtual {v14, v15, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* :try_end_13 */
/* .catch Ljava/lang/Exception; {:try_start_13 ..:try_end_13} :catch_1 */
/* .line 473 */
} // :cond_9
try { // :try_start_14
v2 = (( java.lang.String ) v7 ).equals ( v11 ); // invoke-virtual {v7, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_14 */
/* .catch Ljava/lang/Exception; {:try_start_14 ..:try_end_14} :catch_8 */
if ( v2 != null) { // if-eqz v2, :cond_a
/* .line 474 */
int v2 = 0; // const/4 v2, 0x0
try { // :try_start_15
(( android.content.Intent ) v14 ).putExtra ( v15, v2 ); // invoke-virtual {v14, v15, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* :try_end_15 */
/* .catch Ljava/lang/Exception; {:try_start_15 ..:try_end_15} :catch_1 */
/* .line 476 */
} // :cond_a
try { // :try_start_16
(( android.content.Intent ) v14 ).putExtra ( v15, v12 ); // invoke-virtual {v14, v15, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 479 */
} // :goto_6
v2 = (( java.lang.String ) v13 ).equals ( v0 ); // invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_16 */
/* .catch Ljava/lang/Exception; {:try_start_16 ..:try_end_16} :catch_8 */
final String v15 = "harmankardon_status"; // const-string v15, "harmankardon_status"
if ( v2 != null) { // if-eqz v2, :cond_b
/* .line 480 */
try { // :try_start_17
(( android.content.Intent ) v14 ).putExtra ( v15, v10 ); // invoke-virtual {v14, v15, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* :try_end_17 */
/* .catch Ljava/lang/Exception; {:try_start_17 ..:try_end_17} :catch_1 */
/* .line 481 */
} // :cond_b
try { // :try_start_18
v2 = (( java.lang.String ) v13 ).equals ( v11 ); // invoke-virtual {v13, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_18 */
/* .catch Ljava/lang/Exception; {:try_start_18 ..:try_end_18} :catch_8 */
if ( v2 != null) { // if-eqz v2, :cond_c
/* .line 482 */
int v2 = 0; // const/4 v2, 0x0
try { // :try_start_19
(( android.content.Intent ) v14 ).putExtra ( v15, v2 ); // invoke-virtual {v14, v15, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* :try_end_19 */
/* .catch Ljava/lang/Exception; {:try_start_19 ..:try_end_19} :catch_1 */
/* .line 484 */
} // :cond_c
try { // :try_start_1a
(( android.content.Intent ) v14 ).putExtra ( v15, v12 ); // invoke-virtual {v14, v15, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 487 */
} // :goto_7
v2 = (( java.lang.String ) v8 ).equals ( v0 ); // invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_1a */
/* .catch Ljava/lang/Exception; {:try_start_1a ..:try_end_1a} :catch_8 */
final String v15 = "dolby_state"; // const-string v15, "dolby_state"
if ( v2 != null) { // if-eqz v2, :cond_d
/* .line 488 */
try { // :try_start_1b
(( android.content.Intent ) v14 ).putExtra ( v15, v10 ); // invoke-virtual {v14, v15, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* :try_end_1b */
/* .catch Ljava/lang/Exception; {:try_start_1b ..:try_end_1b} :catch_1 */
/* .line 489 */
} // :cond_d
try { // :try_start_1c
v2 = (( java.lang.String ) v8 ).equals ( v11 ); // invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_1c */
/* .catch Ljava/lang/Exception; {:try_start_1c ..:try_end_1c} :catch_8 */
if ( v2 != null) { // if-eqz v2, :cond_e
/* .line 490 */
int v2 = 0; // const/4 v2, 0x0
try { // :try_start_1d
(( android.content.Intent ) v14 ).putExtra ( v15, v2 ); // invoke-virtual {v14, v15, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* :try_end_1d */
/* .catch Ljava/lang/Exception; {:try_start_1d ..:try_end_1d} :catch_1 */
/* .line 492 */
} // :cond_e
try { // :try_start_1e
(( android.content.Intent ) v14 ).putExtra ( v15, v12 ); // invoke-virtual {v14, v15, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 495 */
} // :goto_8
v2 = (( java.lang.String ) v9 ).equals ( v0 ); // invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_1e */
/* .catch Ljava/lang/Exception; {:try_start_1e ..:try_end_1e} :catch_8 */
final String v15 = "audio_3D_state"; // const-string v15, "audio_3D_state"
if ( v2 != null) { // if-eqz v2, :cond_f
/* .line 496 */
try { // :try_start_1f
(( android.content.Intent ) v14 ).putExtra ( v15, v10 ); // invoke-virtual {v14, v15, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* :try_end_1f */
/* .catch Ljava/lang/Exception; {:try_start_1f ..:try_end_1f} :catch_1 */
/* .line 497 */
} // :cond_f
try { // :try_start_20
v2 = (( java.lang.String ) v9 ).equals ( v11 ); // invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_20 */
/* .catch Ljava/lang/Exception; {:try_start_20 ..:try_end_20} :catch_8 */
if ( v2 != null) { // if-eqz v2, :cond_10
/* .line 498 */
int v2 = 0; // const/4 v2, 0x0
try { // :try_start_21
(( android.content.Intent ) v14 ).putExtra ( v15, v2 ); // invoke-virtual {v14, v15, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* :try_end_21 */
/* .catch Ljava/lang/Exception; {:try_start_21 ..:try_end_21} :catch_1 */
/* .line 500 */
} // :cond_10
try { // :try_start_22
(( android.content.Intent ) v14 ).putExtra ( v15, v12 ); // invoke-virtual {v14, v15, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* :try_end_22 */
/* .catch Ljava/lang/Exception; {:try_start_22 ..:try_end_22} :catch_8 */
/* .line 503 */
} // :goto_9
/* move-object/from16 v2, v20 */
} // .end local v20 # "spatial_audio_button":Ljava/lang/String;
/* .local v2, "spatial_audio_button":Ljava/lang/String; */
try { // :try_start_23
v15 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_23 */
/* .catch Ljava/lang/Exception; {:try_start_23 ..:try_end_23} :catch_7 */
/* const-string/jumbo v10, "spatial_audio_state" */
if ( v15 != null) { // if-eqz v15, :cond_11
/* .line 504 */
int v11 = 1; // const/4 v11, 0x1
try { // :try_start_24
(( android.content.Intent ) v14 ).putExtra ( v10, v11 ); // invoke-virtual {v14, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* :try_end_24 */
/* .catch Ljava/lang/Exception; {:try_start_24 ..:try_end_24} :catch_2 */
/* .line 548 */
} // .end local v14 # "intent":Landroid/content/Intent;
} // .end local v17 # "params":Landroid/os/Bundle;
/* :catch_2 */
/* move-exception v0 */
/* move-object/from16 v12, p0 */
/* move-object/from16 v11, v18 */
/* move-object/from16 v10, v19 */
/* goto/16 :goto_11 */
/* .line 505 */
/* .restart local v14 # "intent":Landroid/content/Intent; */
/* .restart local v17 # "params":Landroid/os/Bundle; */
} // :cond_11
try { // :try_start_25
v11 = (( java.lang.String ) v2 ).equals ( v11 ); // invoke-virtual {v2, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_25 */
/* .catch Ljava/lang/Exception; {:try_start_25 ..:try_end_25} :catch_7 */
if ( v11 != null) { // if-eqz v11, :cond_12
/* .line 506 */
int v11 = 0; // const/4 v11, 0x0
try { // :try_start_26
(( android.content.Intent ) v14 ).putExtra ( v10, v11 ); // invoke-virtual {v14, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* :try_end_26 */
/* .catch Ljava/lang/Exception; {:try_start_26 ..:try_end_26} :catch_2 */
/* .line 508 */
} // :cond_12
try { // :try_start_27
(( android.content.Intent ) v14 ).putExtra ( v10, v12 ); // invoke-virtual {v14, v10, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* :try_end_27 */
/* .catch Ljava/lang/Exception; {:try_start_27 ..:try_end_27} :catch_7 */
/* .line 511 */
} // :goto_a
/* move-object/from16 v10, v19 */
} // .end local v19 # "misound_status_button":Ljava/lang/String;
/* .local v10, "misound_status_button":Ljava/lang/String; */
try { // :try_start_28
v11 = (( java.lang.String ) v10 ).equals ( v0 ); // invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_28 */
/* .catch Ljava/lang/Exception; {:try_start_28 ..:try_end_28} :catch_6 */
final String v12 = "misound_status_button"; // const-string v12, "misound_status_button"
if ( v11 != null) { // if-eqz v11, :cond_13
/* .line 512 */
int v11 = 1; // const/4 v11, 0x1
try { // :try_start_29
(( android.content.Intent ) v14 ).putExtra ( v12, v11 ); // invoke-virtual {v14, v12, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 513 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/audio/MQSUtils;->checkMisoundHeadphoneType()Ljava/lang/String; */
/* .line 514 */
/* .local v11, "type":Ljava/lang/String; */
final String v12 = "misound_headphone_type"; // const-string v12, "misound_headphone_type"
(( android.content.Intent ) v14 ).putExtra ( v12, v11 ); // invoke-virtual {v14, v12, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* :try_end_29 */
/* .catch Ljava/lang/Exception; {:try_start_29 ..:try_end_29} :catch_3 */
/* .line 515 */
/* nop */
} // .end local v11 # "type":Ljava/lang/String;
/* .line 548 */
} // .end local v14 # "intent":Landroid/content/Intent;
} // .end local v17 # "params":Landroid/os/Bundle;
/* :catch_3 */
/* move-exception v0 */
/* move-object/from16 v12, p0 */
/* move-object/from16 v11, v18 */
/* goto/16 :goto_11 */
/* .line 516 */
/* .restart local v14 # "intent":Landroid/content/Intent; */
/* .restart local v17 # "params":Landroid/os/Bundle; */
} // :cond_13
int v11 = 0; // const/4 v11, 0x0
try { // :try_start_2a
(( android.content.Intent ) v14 ).putExtra ( v12, v11 ); // invoke-virtual {v14, v12, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* :try_end_2a */
/* .catch Ljava/lang/Exception; {:try_start_2a ..:try_end_2a} :catch_6 */
/* .line 519 */
} // :goto_b
/* move-object/from16 v11, v18 */
} // .end local v18 # "misound_eq_button":Ljava/lang/String;
/* .local v11, "misound_eq_button":Ljava/lang/String; */
try { // :try_start_2b
v0 = (( java.lang.String ) v11 ).equals ( v0 ); // invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_2b */
/* .catch Ljava/lang/Exception; {:try_start_2b ..:try_end_2b} :catch_5 */
final String v12 = "misound_eq_button"; // const-string v12, "misound_eq_button"
if ( v0 != null) { // if-eqz v0, :cond_14
/* .line 520 */
int v0 = 1; // const/4 v0, 0x1
try { // :try_start_2c
(( android.content.Intent ) v14 ).putExtra ( v12, v0 ); // invoke-virtual {v14, v12, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 522 */
} // :cond_14
int v0 = 0; // const/4 v0, 0x0
(( android.content.Intent ) v14 ).putExtra ( v12, v0 ); // invoke-virtual {v14, v12, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 525 */
} // :goto_c
v0 = /* invoke-direct/range {p0 ..p0}, Lcom/android/server/audio/MQSUtils;->checkWirelessTransmissionSupport()Z */
/* :try_end_2c */
/* .catch Ljava/lang/Exception; {:try_start_2c ..:try_end_2c} :catch_5 */
/* const-string/jumbo v12, "support_wireless_transmission" */
if ( v0 != null) { // if-eqz v0, :cond_15
/* .line 526 */
int v0 = 1; // const/4 v0, 0x1
try { // :try_start_2d
(( android.content.Intent ) v14 ).putExtra ( v12, v0 ); // invoke-virtual {v14, v12, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 528 */
} // :cond_15
int v0 = 0; // const/4 v0, 0x0
(( android.content.Intent ) v14 ).putExtra ( v12, v0 ); // invoke-virtual {v14, v12, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 531 */
} // :goto_d
v0 = /* invoke-direct/range {p0 ..p0}, Lcom/android/server/audio/MQSUtils;->checkHearingAssistSupport()Z */
/* :try_end_2d */
/* .catch Ljava/lang/Exception; {:try_start_2d ..:try_end_2d} :catch_5 */
/* const-string/jumbo v12, "support_hearing_assist" */
if ( v0 != null) { // if-eqz v0, :cond_16
/* .line 532 */
int v0 = 1; // const/4 v0, 0x1
try { // :try_start_2e
(( android.content.Intent ) v14 ).putExtra ( v12, v0 ); // invoke-virtual {v14, v12, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 534 */
} // :cond_16
int v0 = 0; // const/4 v0, 0x0
(( android.content.Intent ) v14 ).putExtra ( v12, v0 ); // invoke-virtual {v14, v12, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 537 */
} // :goto_e
v0 = /* invoke-direct/range {p0 ..p0}, Lcom/android/server/audio/MQSUtils;->checkVoiceprintNoiseReductionSupport()Z */
/* :try_end_2e */
/* .catch Ljava/lang/Exception; {:try_start_2e ..:try_end_2e} :catch_5 */
/* const-string/jumbo v12, "support_voiceprint_noise_reduction" */
if ( v0 != null) { // if-eqz v0, :cond_17
/* .line 538 */
int v0 = 1; // const/4 v0, 0x1
try { // :try_start_2f
(( android.content.Intent ) v14 ).putExtra ( v12, v0 ); // invoke-virtual {v14, v12, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 540 */
} // :cond_17
int v0 = 0; // const/4 v0, 0x0
(( android.content.Intent ) v14 ).putExtra ( v12, v0 ); // invoke-virtual {v14, v12, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 543 */
} // :goto_f
/* move-object/from16 v0, v17 */
} // .end local v17 # "params":Landroid/os/Bundle;
/* .local v0, "params":Landroid/os/Bundle; */
(( android.content.Intent ) v14 ).putExtras ( v0 ); // invoke-virtual {v14, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
/* .line 544 */
/* sget-boolean v12, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v12, :cond_18 */
/* .line 545 */
int v12 = 2; // const/4 v12, 0x2
(( android.content.Intent ) v14 ).setFlags ( v12 ); // invoke-virtual {v14, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* :try_end_2f */
/* .catch Ljava/lang/Exception; {:try_start_2f ..:try_end_2f} :catch_5 */
/* .line 547 */
} // :cond_18
/* move-object/from16 v12, p0 */
try { // :try_start_30
v15 = this.mContext;
(( android.content.Context ) v15 ).startService ( v14 ); // invoke-virtual {v15, v14}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
/* :try_end_30 */
/* .catch Ljava/lang/Exception; {:try_start_30 ..:try_end_30} :catch_4 */
/* .line 551 */
/* nop */
} // .end local v0 # "params":Landroid/os/Bundle;
} // .end local v14 # "intent":Landroid/content/Intent;
/* .line 548 */
/* :catch_4 */
/* move-exception v0 */
/* :catch_5 */
/* move-exception v0 */
/* move-object/from16 v12, p0 */
} // .end local v11 # "misound_eq_button":Ljava/lang/String;
/* .restart local v18 # "misound_eq_button":Ljava/lang/String; */
/* :catch_6 */
/* move-exception v0 */
/* move-object/from16 v12, p0 */
/* move-object/from16 v11, v18 */
} // .end local v18 # "misound_eq_button":Ljava/lang/String;
/* .restart local v11 # "misound_eq_button":Ljava/lang/String; */
} // .end local v10 # "misound_status_button":Ljava/lang/String;
} // .end local v11 # "misound_eq_button":Ljava/lang/String;
/* .restart local v18 # "misound_eq_button":Ljava/lang/String; */
/* .restart local v19 # "misound_status_button":Ljava/lang/String; */
/* :catch_7 */
/* move-exception v0 */
/* move-object/from16 v12, p0 */
/* move-object/from16 v11, v18 */
/* move-object/from16 v10, v19 */
} // .end local v18 # "misound_eq_button":Ljava/lang/String;
} // .end local v19 # "misound_status_button":Ljava/lang/String;
/* .restart local v10 # "misound_status_button":Ljava/lang/String; */
/* .restart local v11 # "misound_eq_button":Ljava/lang/String; */
} // .end local v2 # "spatial_audio_button":Ljava/lang/String;
} // .end local v10 # "misound_status_button":Ljava/lang/String;
} // .end local v11 # "misound_eq_button":Ljava/lang/String;
/* .restart local v18 # "misound_eq_button":Ljava/lang/String; */
/* .restart local v19 # "misound_status_button":Ljava/lang/String; */
/* .restart local v20 # "spatial_audio_button":Ljava/lang/String; */
/* :catch_8 */
/* move-exception v0 */
/* move-object/from16 v12, p0 */
/* move-object/from16 v11, v18 */
/* move-object/from16 v10, v19 */
/* move-object/from16 v2, v20 */
} // .end local v18 # "misound_eq_button":Ljava/lang/String;
} // .end local v19 # "misound_status_button":Ljava/lang/String;
} // .end local v20 # "spatial_audio_button":Ljava/lang/String;
/* .restart local v2 # "spatial_audio_button":Ljava/lang/String; */
/* .restart local v10 # "misound_status_button":Ljava/lang/String; */
/* .restart local v11 # "misound_eq_button":Ljava/lang/String; */
} // .end local v2 # "spatial_audio_button":Ljava/lang/String;
/* .local v10, "spatial_audio_button":Ljava/lang/String; */
/* .local v11, "misound_status_button":Ljava/lang/String; */
/* .restart local v12 # "misound_eq_button":Ljava/lang/String; */
/* :catch_9 */
/* move-exception v0 */
/* :catch_a */
/* move-exception v0 */
/* move-object/from16 v16, v2 */
} // :goto_10
/* move-object v2, v10 */
/* move-object v10, v11 */
/* move-object v11, v12 */
/* move-object/from16 v12, p0 */
/* .line 549 */
} // .end local v12 # "misound_eq_button":Ljava/lang/String;
/* .local v0, "e":Ljava/lang/Exception; */
/* .restart local v2 # "spatial_audio_button":Ljava/lang/String; */
/* .local v10, "misound_status_button":Ljava/lang/String; */
/* .local v11, "misound_eq_button":Ljava/lang/String; */
} // :goto_11
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 550 */
final String v14 = "erroe for reportAudioButton"; // const-string v14, "erroe for reportAudioButton"
/* move-object/from16 v15, v16 */
android.util.Slog .d ( v15,v14 );
/* .line 553 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_12
return;
} // .end method
public void reportAudioSilentObserverToOnetrack ( Integer p0, java.lang.String p1, java.lang.String p2, Integer p3 ) {
/* .locals 17 */
/* .param p1, "level" # I */
/* .param p2, "location" # Ljava/lang/String; */
/* .param p3, "reason" # Ljava/lang/String; */
/* .param p4, "type" # I */
/* .line 395 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "reportAudioSilentObserverToOnetrack, level: "; // const-string v1, "reportAudioSilentObserverToOnetrack, level: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move/from16 v1, p1 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", location: "; // const-string v2, ", location: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v10, p2 */
(( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", reason: "; // const-string v2, ", reason: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v11, p3 */
(( java.lang.StringBuilder ) v0 ).append ( v11 ); // invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", type: "; // const-string v2, ", type: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move/from16 v12, p4 */
(( java.lang.StringBuilder ) v0 ).append ( v12 ); // invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v13 = "MiSound.MQSUtils"; // const-string v13, "MiSound.MQSUtils"
android.util.Log .d ( v13,v0 );
/* .line 397 */
java.time.LocalDateTime .now ( );
/* .line 398 */
/* .local v14, "date_time":Ljava/time/LocalDateTime; */
/* const-string/jumbo v0, "yyyy-MM-dd HH:mm:ss.SSS" */
java.time.format.DateTimeFormatter .ofPattern ( v0 );
/* .line 399 */
/* .local v15, "formatter":Ljava/time/format/DateTimeFormatter; */
(( java.time.LocalDateTime ) v14 ).format ( v15 ); // invoke-virtual {v14, v15}, Ljava/time/LocalDateTime;->format(Ljava/time/format/DateTimeFormatter;)Ljava/lang/String;
/* .line 400 */
/* .local v16, "current_time":Ljava/lang/String; */
final String v2 = ""; // const-string v2, ""
/* invoke-static/range {p1 ..p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer; */
/* invoke-static/range {p4 ..p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer; */
final String v7 = ""; // const-string v7, ""
final String v8 = ""; // const-string v8, ""
/* move-object/from16 v3, p2 */
/* move-object/from16 v4, p3 */
/* move-object/from16 v9, v16 */
/* filled-new-array/range {v2 ..v9}, [Ljava/lang/Object; */
/* const-string/jumbo v2, "{\"name\":\"audio_silent_observer\",\"audio_event\":{\"scenario\":\"%s\", \"location\":\"%s\", \"silent_reason\":\"%s\", \"level\":\"%d\", \"silent_type\":\"%d\", \"source_sink\":\"%s\", \"audio_device\":\"%s\", \"extra_info\":\"%s\"}, \"dgt\":\"null\",\"audio_ext\":\"null\" }" */
java.lang.String .format ( v2,v0 );
/* .line 402 */
/* .local v2, "result":Ljava/lang/String; */
/* move-object/from16 v3, p0 */
try { // :try_start_0
v0 = this.mMiuiXlog;
(( android.media.MiuiXlog ) v0 ).miuiXlogSend ( v2 ); // invoke-virtual {v0, v2}, Landroid/media/MiuiXlog;->miuiXlogSend(Ljava/lang/String;)I
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 405 */
/* .line 403 */
/* :catch_0 */
/* move-exception v0 */
/* .line 404 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "reportAudioSilentObserverToOnetrack exception : "; // const-string v5, "reportAudioSilentObserverToOnetrack exception : "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v13,v4 );
/* .line 406 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public void reportAudioVisualDailyUse ( java.util.List p0 ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/media/AudioPlaybackConfiguration;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 556 */
/* .local p1, "currentPlayback":Ljava/util/List;, "Ljava/util/List<Landroid/media/AudioPlaybackConfiguration;>;" */
/* invoke-direct {p0}, Lcom/android/server/audio/MQSUtils;->checkAudioVisualStatus()Ljava/lang/String; */
/* .line 557 */
/* .local v0, "status":Ljava/lang/String; */
final String v1 = "open"; // const-string v1, "open"
(( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 558 */
return;
} // .end method
public void reportBtMultiVoipDailyUse ( android.content.Context p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "scoState" # Ljava/lang/String; */
/* .param p3, "multiVoipPackages" # Ljava/lang/String; */
/* .line 580 */
/* if-nez p1, :cond_0 */
/* .line 581 */
return;
/* .line 583 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "reportBtMultiVoipDailyUse start, scoState="; // const-string v1, "reportBtMultiVoipDailyUse start, scoState="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", multiVoipPackages="; // const-string v1, ", multiVoipPackages="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiSound.MQSUtils"; // const-string v1, "MiSound.MQSUtils"
android.util.Slog .i ( v1,v0 );
/* .line 586 */
try { // :try_start_0
v0 = /* invoke-direct {p0}, Lcom/android/server/audio/MQSUtils;->isReportXiaomiServer()Z */
/* if-nez v0, :cond_1 */
/* .line 587 */
return;
/* .line 589 */
} // :cond_1
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 590 */
/* .local v0, "params":Landroid/os/Bundle; */
final String v2 = "sco_state_internal"; // const-string v2, "sco_state_internal"
(( android.os.Bundle ) v0 ).putString ( v2, p2 ); // invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 591 */
/* const-string/jumbo v2, "voip_packages" */
(( android.os.Bundle ) v0 ).putString ( v2, p3 ); // invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 592 */
final String v2 = "31000000086"; // const-string v2, "31000000086"
final String v3 = "multi_voip_communication_for_bluetooth_device"; // const-string v3, "multi_voip_communication_for_bluetooth_device"
com.android.server.audio.MQSUtils .configureIntent ( v2,v3,v0 );
(( android.content.Context ) p1 ).startService ( v2 ); // invoke-virtual {p1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 597 */
/* nop */
} // .end local v0 # "params":Landroid/os/Bundle;
/* .line 594 */
/* :catch_0 */
/* move-exception v0 */
/* .line 595 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 596 */
final String v2 = "error for reportBtMultiVoipDailyUse"; // const-string v2, "error for reportBtMultiVoipDailyUse"
android.util.Slog .d ( v1,v2 );
/* .line 598 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public void reportVibrateStatus ( ) {
/* .locals 11 */
/* .line 612 */
v0 = /* invoke-direct {p0}, Lcom/android/server/audio/MQSUtils;->isReportXiaomiServer()Z */
/* if-nez v0, :cond_0 */
/* .line 613 */
return;
/* .line 615 */
} // :cond_0
final String v0 = "reportVibrateStatus start."; // const-string v0, "reportVibrateStatus start."
final String v1 = "MiSound.MQSUtils"; // const-string v1, "MiSound.MQSUtils"
android.util.Slog .i ( v1,v0 );
/* .line 616 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v2, "vibrate_when_ringing" */
int v3 = 1; // const/4 v3, 0x1
v0 = android.provider.Settings$System .getInt ( v0,v2,v3 );
/* .line 617 */
/* .local v0, "vibrate_ring":I */
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v4, "vibrate_in_silent" */
v2 = android.provider.Settings$System .getInt ( v2,v4,v3 );
/* .line 618 */
/* .local v2, "vibrate_silent":I */
/* invoke-direct {p0}, Lcom/android/server/audio/MQSUtils;->checkHapticStatus()Ljava/lang/String; */
/* .line 619 */
/* .local v4, "haptic_status":Ljava/lang/String; */
v5 = /* invoke-direct {p0}, Lcom/android/server/audio/MQSUtils;->getHapticFeedbackFloatLevel()F */
/* .line 622 */
/* .local v5, "haptic_level":F */
try { // :try_start_0
/* new-instance v6, Landroid/content/Intent; */
final String v7 = "onetrack.action.TRACK_EVENT"; // const-string v7, "onetrack.action.TRACK_EVENT"
/* invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 623 */
/* .local v6, "intent":Landroid/content/Intent; */
v7 = /* invoke-direct {p0}, Lcom/android/server/audio/MQSUtils;->isGlobalBuild()Z */
int v8 = 2; // const/4 v8, 0x2
/* if-nez v7, :cond_1 */
/* .line 624 */
(( android.content.Intent ) v6 ).setFlags ( v8 ); // invoke-virtual {v6, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 626 */
} // :cond_1
final String v7 = "com.miui.analytics"; // const-string v7, "com.miui.analytics"
(( android.content.Intent ) v6 ).setPackage ( v7 ); // invoke-virtual {v6, v7}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 627 */
final String v7 = "APP_ID"; // const-string v7, "APP_ID"
final String v9 = "31000000089"; // const-string v9, "31000000089"
(( android.content.Intent ) v6 ).putExtra ( v7, v9 ); // invoke-virtual {v6, v7, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 628 */
final String v7 = "EVENT_NAME"; // const-string v7, "EVENT_NAME"
final String v9 = "haptic_status"; // const-string v9, "haptic_status"
(( android.content.Intent ) v6 ).putExtra ( v7, v9 ); // invoke-virtual {v6, v7, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 629 */
final String v7 = "PACKAGE"; // const-string v7, "PACKAGE"
final String v9 = "android"; // const-string v9, "android"
(( android.content.Intent ) v6 ).putExtra ( v7, v9 ); // invoke-virtual {v6, v7, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 630 */
/* new-instance v7, Landroid/os/Bundle; */
/* invoke-direct {v7}, Landroid/os/Bundle;-><init>()V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 631 */
/* .local v7, "params":Landroid/os/Bundle; */
int v9 = 0; // const/4 v9, 0x0
/* const-string/jumbo v10, "status_ring" */
/* if-ne v0, v3, :cond_2 */
/* .line 632 */
try { // :try_start_1
(( android.content.Intent ) v6 ).putExtra ( v10, v3 ); // invoke-virtual {v6, v10, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 634 */
} // :cond_2
(( android.content.Intent ) v6 ).putExtra ( v10, v9 ); // invoke-virtual {v6, v10, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 636 */
} // :goto_0
/* const-string/jumbo v10, "status_silent" */
/* if-ne v2, v3, :cond_3 */
/* .line 637 */
try { // :try_start_2
(( android.content.Intent ) v6 ).putExtra ( v10, v3 ); // invoke-virtual {v6, v10, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 639 */
} // :cond_3
(( android.content.Intent ) v6 ).putExtra ( v10, v9 ); // invoke-virtual {v6, v10, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 641 */
} // :goto_1
final String v9 = "open"; // const-string v9, "open"
v9 = (( java.lang.String ) v4 ).equals ( v9 ); // invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v9 != null) { // if-eqz v9, :cond_4
/* .line 642 */
final String v9 = "haptic_intensity_status"; // const-string v9, "haptic_intensity_status"
(( android.content.Intent ) v6 ).putExtra ( v9, v3 ); // invoke-virtual {v6, v9, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 643 */
final String v3 = "haptic_intensity_position"; // const-string v3, "haptic_intensity_position"
(( android.content.Intent ) v6 ).putExtra ( v3, v5 ); // invoke-virtual {v6, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;
/* .line 645 */
} // :cond_4
(( android.content.Intent ) v6 ).putExtras ( v7 ); // invoke-virtual {v6, v7}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
/* .line 646 */
/* sget-boolean v3, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v3, :cond_5 */
/* .line 647 */
(( android.content.Intent ) v6 ).setFlags ( v8 ); // invoke-virtual {v6, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 649 */
} // :cond_5
v3 = this.mContext;
(( android.content.Context ) v3 ).startService ( v6 ); // invoke-virtual {v3, v6}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 653 */
/* nop */
} // .end local v6 # "intent":Landroid/content/Intent;
} // .end local v7 # "params":Landroid/os/Bundle;
/* .line 650 */
/* :catch_0 */
/* move-exception v3 */
/* .line 651 */
/* .local v3, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v3 ).printStackTrace ( ); // invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
/* .line 652 */
final String v6 = "erroe for reportVibrate"; // const-string v6, "erroe for reportVibrate"
android.util.Slog .d ( v1,v6 );
/* .line 655 */
} // .end local v3 # "e":Ljava/lang/Exception;
} // :goto_2
return;
} // .end method
public void reportWaveformInfo ( com.android.server.audio.MQSUtils$WaveformInfo p0 ) {
/* .locals 4 */
/* .param p1, "vibratorInfo" # Lcom/android/server/audio/MQSUtils$WaveformInfo; */
/* .line 601 */
final String v0 = "report WaveformInfo start."; // const-string v0, "report WaveformInfo start."
final String v1 = "MiSound.MQSUtils"; // const-string v1, "MiSound.MQSUtils"
android.util.Slog .i ( v1,v0 );
/* .line 602 */
v0 = this.opPkg;
v2 = this.attrs;
v3 = this.effect;
/* filled-new-array {v0, v2, v3}, [Ljava/lang/Object; */
/* const-string/jumbo v2, "{\"name\":\"waveform_info\",\"audio_event\":{\"waveform_package_name\":\"%s\",\"waveform_usage\":\"%s\",\"waveform_effect\":\"%s\"},\"dgt\":\"null\",\"audio_ext\":\"null\" }" */
java.lang.String .format ( v2,v0 );
/* .line 603 */
/* .local v0, "result":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "reportWaveformInfo:"; // const-string v3, "reportWaveformInfo:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v2 );
/* .line 605 */
try { // :try_start_0
v2 = this.mMiuiXlog;
(( android.media.MiuiXlog ) v2 ).miuiXlogSend ( v0 ); // invoke-virtual {v2, v0}, Landroid/media/MiuiXlog;->miuiXlogSend(Ljava/lang/String;)I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 608 */
/* .line 606 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 607 */
/* .local v2, "e":Ljava/lang/Throwable; */
final String v3 = "can not use miuiXlogSend!!!"; // const-string v3, "can not use miuiXlogSend!!!"
android.util.Log .e ( v1,v3 );
/* .line 609 */
} // .end local v2 # "e":Ljava/lang/Throwable;
} // :goto_0
return;
} // .end method
