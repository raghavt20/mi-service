public class com.android.server.audio.CloudServiceSettings$Setting {
	 /* .source "CloudServiceSettings.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/audio/CloudServiceSettings; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x1 */
/* name = "Setting" */
} // .end annotation
/* # static fields */
private static final java.lang.String NAME;
private static final java.lang.String SECRET;
/* # instance fields */
private java.lang.String mLastSetsVersionCode;
protected Boolean mSecreted;
protected java.util.HashSet mSecretedItem;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/StringBuilder;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public java.lang.String mSettingName;
protected Boolean mSuccess;
private java.lang.String mVersionCode;
final com.android.server.audio.CloudServiceSettings this$0; //synthetic
/* # direct methods */
public com.android.server.audio.CloudServiceSettings$Setting ( ) {
/* .locals 3 */
/* .param p1, "this$0" # Lcom/android/server/audio/CloudServiceSettings; */
/* .param p2, "json" # Lorg/json/JSONObject; */
/* .line 372 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 368 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/audio/CloudServiceSettings$Setting;->mSuccess:Z */
/* .line 369 */
/* iput-boolean v0, p0, Lcom/android/server/audio/CloudServiceSettings$Setting;->mSecreted:Z */
/* .line 370 */
/* new-instance v1, Ljava/util/HashSet; */
/* invoke-direct {v1}, Ljava/util/HashSet;-><init>()V */
this.mSecretedItem = v1;
/* .line 374 */
try { // :try_start_0
final String v1 = "name"; // const-string v1, "name"
(( org.json.JSONObject ) p2 ).getString ( v1 ); // invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
this.mSettingName = v1;
/* .line 375 */
/* const-string/jumbo v1, "versionCode" */
final String v2 = ""; // const-string v2, ""
(( org.json.JSONObject ) p2 ).optString ( v1, v2 ); // invoke-virtual {p2, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
this.mVersionCode = v1;
/* .line 376 */
/* const-string/jumbo v1, "secret" */
v0 = (( org.json.JSONObject ) p2 ).optBoolean ( v1, v0 ); // invoke-virtual {p2, v1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z
/* iput-boolean v0, p0, Lcom/android/server/audio/CloudServiceSettings$Setting;->mSecreted:Z */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 379 */
/* .line 377 */
/* :catch_0 */
/* move-exception v0 */
/* .line 378 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "CloudServiceSettings"; // const-string v1, "CloudServiceSettings"
final String v2 = "fail to parse Setting"; // const-string v2, "fail to parse Setting"
android.util.Log .e ( v1,v2 );
/* .line 380 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
/* # virtual methods */
protected void decrypt ( ) {
/* .locals 5 */
/* .line 408 */
v0 = this.mSecretedItem;
(( java.util.HashSet ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;
/* .line 409 */
/* .local v0, "it":Ljava/util/Iterator; */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 410 */
/* check-cast v1, Ljava/lang/StringBuilder; */
/* .line 411 */
/* .local v1, "sb":Ljava/lang/StringBuilder; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "decryptPassword :"; // const-string v3, "decryptPassword :"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "CloudServiceSettings"; // const-string v3, "CloudServiceSettings"
android.util.Log .d ( v3,v2 );
/* .line 412 */
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.audio.CloudServiceSettings .-$$Nest$smdecryptPassword ( v2 );
/* .line 413 */
/* .local v2, "dec":Ljava/lang/String; */
int v3 = 0; // const/4 v3, 0x0
v4 = (( java.lang.StringBuilder ) v1 ).length ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I
(( java.lang.StringBuilder ) v1 ).delete ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;
/* .line 414 */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 415 */
/* .line 416 */
} // .end local v1 # "sb":Ljava/lang/StringBuilder;
} // .end local v2 # "dec":Ljava/lang/String;
/* .line 417 */
} // :cond_0
return;
} // .end method
public Boolean set ( ) {
/* .locals 2 */
/* .line 383 */
(( com.android.server.audio.CloudServiceSettings$Setting ) p0 ).decrypt ( ); // invoke-virtual {p0}, Lcom/android/server/audio/CloudServiceSettings$Setting;->decrypt()V
/* .line 384 */
/* iget-boolean v0, p0, Lcom/android/server/audio/CloudServiceSettings$Setting;->mSuccess:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 385 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
v1 = this.mSettingName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " already sets"; // const-string v1, " already sets"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "CloudServiceSettings"; // const-string v1, "CloudServiceSettings"
android.util.Log .d ( v1,v0 );
/* .line 387 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/audio/CloudServiceSettings$Setting;->mSuccess:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
final String v0 = ""; // const-string v0, ""
v1 = this.mVersionCode;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_2 */
v0 = this.mVersionCode;
v1 = this.mLastSetsVersionCode;
v0 = (( java.lang.String ) v0 ).compareTo ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I
/* if-lez v0, :cond_1 */
/* .line 391 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 388 */
} // :cond_2
} // :goto_0
v0 = this.mVersionCode;
this.mLastSetsVersionCode = v0;
/* .line 389 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean updateTo ( com.android.server.audio.CloudServiceSettings$Setting p0 ) {
/* .locals 2 */
/* .param p1, "s" # Lcom/android/server/audio/CloudServiceSettings$Setting; */
/* .line 396 */
v0 = this.mSettingName;
v1 = this.mSettingName;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 397 */
v0 = this.mVersionCode;
this.mVersionCode = v0;
/* .line 398 */
/* iget-boolean v0, p1, Lcom/android/server/audio/CloudServiceSettings$Setting;->mSecreted:Z */
/* iput-boolean v0, p0, Lcom/android/server/audio/CloudServiceSettings$Setting;->mSecreted:Z */
/* .line 399 */
v0 = this.mSecretedItem;
this.mSecretedItem = v0;
/* .line 400 */
int v0 = 1; // const/4 v0, 0x1
/* .line 402 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
v1 = this.mSettingName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " update to "; // const-string v1, " update to "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mSettingName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " fail, item not match!"; // const-string v1, " fail, item not match!"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "CloudServiceSettings"; // const-string v1, "CloudServiceSettings"
android.util.Log .e ( v1,v0 );
/* .line 403 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
