class com.android.server.audio.AudioGameEffect$2 extends android.content.BroadcastReceiver {
	 /* .source "AudioGameEffect.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/audio/AudioGameEffect; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.audio.AudioGameEffect this$0; //synthetic
/* # direct methods */
 com.android.server.audio.AudioGameEffect$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/audio/AudioGameEffect; */
/* .line 262 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 9 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 266 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 267 */
/* .local v0, "action":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "onReceive, action="; // const-string v2, "onReceive, action="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "AudioGameEffect"; // const-string v2, "AudioGameEffect"
android.util.Log .i ( v2,v1 );
/* .line 268 */
v1 = this.this$0;
com.android.server.audio.AudioGameEffect .-$$Nest$fgetmCurrentEnablePkg ( v1 );
final String v2 = ""; // const-string v2, ""
v1 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_6 */
/* .line 269 */
final String v1 = "android.intent.action.HEADSET_PLUG"; // const-string v1, "android.intent.action.HEADSET_PLUG"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v2 = 3; // const/4 v2, 0x3
/* const-string/jumbo v3, "speaker" */
final String v4 = "headsets"; // const-string v4, "headsets"
int v5 = -1; // const/4 v5, -0x1
int v6 = 2; // const/4 v6, 0x2
/* const-wide/16 v7, 0x1f4 */
if ( v1 != null) { // if-eqz v1, :cond_2
	 /* .line 270 */
	 /* const-string/jumbo v1, "state" */
	 v1 = 	 (( android.content.Intent ) p2 ).getIntExtra ( v1, v5 ); // invoke-virtual {p2, v1, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
	 /* .line 271 */
	 /* .local v1, "state":I */
	 int v5 = 1; // const/4 v5, 0x1
	 /* if-ne v1, v5, :cond_0 */
	 /* .line 272 */
	 v2 = this.this$0;
	 com.android.server.audio.AudioGameEffect .-$$Nest$fputmCurrentDevice ( v2,v4 );
	 /* .line 273 */
	 v2 = this.this$0;
	 com.android.server.audio.AudioGameEffect .-$$Nest$msendMsgDelay ( v2,v6,v7,v8 );
	 /* .line 274 */
} // :cond_0
/* if-nez v1, :cond_5 */
/* .line 275 */
v4 = this.this$0;
com.android.server.audio.AudioGameEffect .-$$Nest$fputmCurrentDevice ( v4,v3 );
/* .line 276 */
v3 = this.this$0;
v3 = com.android.server.audio.AudioGameEffect .-$$Nest$misCurrentSceneSupported ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_1
	 /* .line 277 */
	 v2 = this.this$0;
	 com.android.server.audio.AudioGameEffect .-$$Nest$msendMsgDelay ( v2,v6,v7,v8 );
	 /* .line 279 */
} // :cond_1
v3 = this.this$0;
com.android.server.audio.AudioGameEffect .-$$Nest$msendMsgDelay ( v3,v2,v7,v8 );
/* .line 282 */
} // .end local v1 # "state":I
} // :cond_2
final String v1 = "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"; // const-string v1, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 283 */
final String v1 = "android.bluetooth.profile.extra.STATE"; // const-string v1, "android.bluetooth.profile.extra.STATE"
v1 = (( android.content.Intent ) p2 ).getIntExtra ( v1, v5 ); // invoke-virtual {p2, v1, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 284 */
/* .restart local v1 # "state":I */
/* if-ne v1, v6, :cond_3 */
/* .line 285 */
v2 = this.this$0;
com.android.server.audio.AudioGameEffect .-$$Nest$fputmCurrentDevice ( v2,v4 );
/* .line 286 */
v2 = this.this$0;
com.android.server.audio.AudioGameEffect .-$$Nest$msendMsgDelay ( v2,v6,v7,v8 );
/* .line 287 */
} // :cond_3
/* if-nez v1, :cond_6 */
/* .line 288 */
v4 = this.this$0;
com.android.server.audio.AudioGameEffect .-$$Nest$fputmCurrentDevice ( v4,v3 );
/* .line 289 */
v3 = this.this$0;
v3 = com.android.server.audio.AudioGameEffect .-$$Nest$misCurrentSceneSupported ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 290 */
v2 = this.this$0;
com.android.server.audio.AudioGameEffect .-$$Nest$msendMsgDelay ( v2,v6,v7,v8 );
/* .line 292 */
} // :cond_4
v3 = this.this$0;
com.android.server.audio.AudioGameEffect .-$$Nest$msendMsgDelay ( v3,v2,v7,v8 );
/* .line 282 */
} // .end local v1 # "state":I
} // :cond_5
} // :goto_0
/* nop */
/* .line 297 */
} // :cond_6
} // :goto_1
return;
} // .end method
