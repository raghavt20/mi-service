.class public Lcom/android/server/audio/AudioQueryWeatherService;
.super Ljava/lang/Object;
.source "AudioQueryWeatherService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;,
        Lcom/android/server/audio/AudioQueryWeatherService$LocationObserver;
    }
.end annotation


# static fields
.field private static final CITY_CONTENT_URI:Ljava/lang/String; = "content://weather/selected_city"

.field private static final CITY_PROJECTION:[Ljava/lang/String;

.field private static final CITY_QUERY_TOKEN:I = 0x3e9

.field private static final DEBUG:Z = true

.field private static final FLAG_LOCATION_TRUE:I = 0x1

.field private static final MAX_SUNRISE_TIME:I = 0xa

.field private static final MAX_SUNSET_TIME:I = 0x16

.field private static final MIN_SUNRISE_TIME:I = 0x3

.field private static final MIN_SUNSET_TIME:I = 0xf

.field private static final NO_FIND_OUT_RESULT:I = -0x1

.field private static final TAG:Ljava/lang/String; = "AudioQueryWeatherService"

.field private static final WEATHER_CONTENT_URI:Ljava/lang/String; = "content://weather/actualWeatherData"

.field private static final WEATHER_PROJECTION:[Ljava/lang/String;

.field private static final WEATHER_QUERY_TOKEN:I = 0x3e8


# instance fields
.field private mBootCompleteReceiver:Landroid/content/BroadcastReceiver;

.field private final mContentResolver:Landroid/content/ContentResolver;

.field private mContext:Landroid/content/Context;

.field private mInternationalLocation:Z

.field private mLocationObserver:Lcom/android/server/audio/AudioQueryWeatherService$LocationObserver;

.field private mNextSunriseSunsetTime:Z

.field private mQueryHandler:Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;

.field private mSunriseTimeHours:I

.field private mSunriseTimeMins:I

.field private mSunsetTimeHours:I

.field private mSunsetTimeMins:I

.field private mUpdateTimeReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContentResolver(Lcom/android/server/audio/AudioQueryWeatherService;)Landroid/content/ContentResolver;
    .locals 0

    iget-object p0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mContentResolver:Landroid/content/ContentResolver;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmNextSunriseSunsetTime(Lcom/android/server/audio/AudioQueryWeatherService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mNextSunriseSunsetTime:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mstartCityQuery(Lcom/android/server/audio/AudioQueryWeatherService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/audio/AudioQueryWeatherService;->startCityQuery()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateCityInfo(Lcom/android/server/audio/AudioQueryWeatherService;Landroid/database/Cursor;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/audio/AudioQueryWeatherService;->updateCityInfo(Landroid/database/Cursor;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateWeatherInfo(Lcom/android/server/audio/AudioQueryWeatherService;Landroid/database/Cursor;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/audio/AudioQueryWeatherService;->updateWeatherInfo(Landroid/database/Cursor;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 4

    .line 36
    const-string/jumbo v0, "sunrise"

    const-string/jumbo v1, "sunset"

    const-string v2, "city_id"

    filled-new-array {v2, v0, v1}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/audio/AudioQueryWeatherService;->WEATHER_PROJECTION:[Ljava/lang/String;

    .line 41
    const-string v0, "flag"

    const-string v1, "belongings"

    const-string v2, "posID"

    const-string v3, "position"

    filled-new-array {v2, v3, v0, v1}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/audio/AudioQueryWeatherService;->CITY_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mNextSunriseSunsetTime:Z

    .line 78
    const-string v1, "AudioQueryWeatherService"

    const-string v2, "construct!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    iput-object p1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mContext:Landroid/content/Context;

    .line 81
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mContentResolver:Landroid/content/ContentResolver;

    .line 82
    iget-object v1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mQueryHandler:Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;

    if-nez v1, :cond_0

    .line 83
    new-instance v1, Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;

    iget-object v2, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mContext:Landroid/content/Context;

    invoke-direct {v1, p0, v2}, Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;-><init>(Lcom/android/server/audio/AudioQueryWeatherService;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mQueryHandler:Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;

    .line 85
    :cond_0
    iput v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunriseTimeHours:I

    .line 86
    iput v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunsetTimeHours:I

    .line 87
    iput v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunriseTimeMins:I

    .line 88
    iput v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunsetTimeMins:I

    .line 89
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mInternationalLocation:Z

    .line 90
    return-void
.end method

.method private CalculateLocationAndQuery(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "flag"    # I
    .param p2, "cityId"    # Ljava/lang/String;
    .param p3, "belongings"    # Ljava/lang/String;

    .line 172
    iget-object v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x110f01a3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 173
    .local v0, "LocationJudgment":Ljava/lang/String;
    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    .line 174
    if-eqz p3, :cond_0

    invoke-virtual {p3, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 175
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mInternationalLocation:Z

    goto :goto_0

    .line 177
    :cond_0
    iput-boolean v1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mInternationalLocation:Z

    .line 179
    :goto_0
    iget-boolean v1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mInternationalLocation:Z

    if-nez v1, :cond_2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 180
    invoke-direct {p0, p2}, Lcom/android/server/audio/AudioQueryWeatherService;->startWeatherQuery(Ljava/lang/String;)V

    goto :goto_1

    .line 183
    :cond_1
    iput-boolean v1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mInternationalLocation:Z

    .line 185
    :cond_2
    :goto_1
    return-void
.end method

.method private CalculateSunriseAndSunsetTime(JJ)V
    .locals 6
    .param p1, "sunriseTime"    # J
    .param p3, "sunsetTime"    # J

    .line 236
    invoke-static {}, Landroid/icu/util/Calendar;->getInstance()Landroid/icu/util/Calendar;

    move-result-object v0

    .line 237
    .local v0, "SunCalendar":Landroid/icu/util/Calendar;
    const-wide/16 v1, 0x0

    cmp-long v3, p1, v1

    if-eqz v3, :cond_4

    cmp-long v1, p3, v1

    if-eqz v1, :cond_4

    .line 238
    invoke-virtual {v0, p1, p2}, Landroid/icu/util/Calendar;->setTimeInMillis(J)V

    .line 239
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/icu/util/Calendar;->get(I)I

    move-result v2

    iput v2, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunriseTimeHours:I

    .line 240
    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Landroid/icu/util/Calendar;->get(I)I

    move-result v3

    iput v3, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunriseTimeMins:I

    .line 242
    iget v3, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunriseTimeHours:I

    const/4 v4, 0x3

    const/4 v5, 0x0

    if-ge v3, v4, :cond_0

    .line 243
    iput v4, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunriseTimeHours:I

    .line 244
    iput v5, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunriseTimeMins:I

    .line 246
    :cond_0
    iget v3, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunriseTimeHours:I

    const/16 v4, 0xa

    if-le v3, v4, :cond_1

    .line 247
    iput v1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunriseTimeHours:I

    .line 248
    iput v5, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunriseTimeMins:I

    .line 250
    :cond_1
    invoke-virtual {v0, p3, p4}, Landroid/icu/util/Calendar;->setTimeInMillis(J)V

    .line 251
    invoke-virtual {v0, v1}, Landroid/icu/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunsetTimeHours:I

    .line 252
    invoke-virtual {v0, v2}, Landroid/icu/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunsetTimeMins:I

    .line 254
    iget v1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunsetTimeHours:I

    const/16 v2, 0xf

    if-ge v1, v2, :cond_2

    .line 255
    iput v2, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunsetTimeHours:I

    .line 256
    iput v5, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunsetTimeMins:I

    .line 258
    :cond_2
    iget v1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunsetTimeHours:I

    const/16 v2, 0x16

    if-le v1, v2, :cond_3

    .line 259
    const/16 v1, 0x17

    iput v1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunsetTimeHours:I

    .line 260
    iput v5, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunsetTimeMins:I

    .line 262
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateWeatherInfo sunriseHour="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunriseTimeHours:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sunriseMin="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunriseTimeMins:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mNextSunriseSunsetTime="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mNextSunriseSunsetTime:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AudioQueryWeatherService"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateWeatherInfo sunsetHour="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunsetTimeHours:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " sunsetMin="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunsetTimeMins:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 266
    :cond_4
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mInternationalLocation:Z

    .line 268
    :goto_0
    return-void
.end method

.method private startCityQuery()V
    .locals 10

    .line 157
    const-class v0, Lcom/android/server/audio/AudioQueryWeatherService;

    monitor-enter v0

    .line 158
    :try_start_0
    iget-object v1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mQueryHandler:Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;

    if-eqz v1, :cond_0

    .line 159
    const/16 v2, 0x3e9

    invoke-virtual {v1, v2}, Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;->cancelOperation(I)V

    .line 160
    iget-object v1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mQueryHandler:Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;

    const/16 v2, 0x3e8

    invoke-virtual {v1, v2}, Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;->cancelOperation(I)V

    .line 161
    const-string v1, "content://weather/selected_city"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    .line 163
    .local v5, "contentUri":Landroid/net/Uri;
    iget-object v2, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mQueryHandler:Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;

    const/16 v3, 0x3e9

    const/4 v4, 0x0

    sget-object v6, Lcom/android/server/audio/AudioQueryWeatherService;->CITY_PROJECTION:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    .end local v5    # "contentUri":Landroid/net/Uri;
    :cond_0
    monitor-exit v0

    .line 168
    return-void

    .line 167
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private startWeatherQuery(Ljava/lang/String;)V
    .locals 10
    .param p1, "cityId"    # Ljava/lang/String;

    .line 221
    const-string v0, "AudioQueryWeatherService"

    const-string/jumbo v1, "startWeatherQuery start!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    const-class v0, Lcom/android/server/audio/AudioQueryWeatherService;

    monitor-enter v0

    .line 224
    :try_start_0
    iget-object v1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mQueryHandler:Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;

    if-eqz v1, :cond_0

    .line 225
    const/16 v2, 0x3e8

    invoke-virtual {v1, v2}, Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;->cancelOperation(I)V

    .line 226
    const-string v1, "content://weather/actualWeatherData"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "2"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "0"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    .line 227
    .local v5, "contentUri":Landroid/net/Uri;
    filled-new-array {p1}, [Ljava/lang/String;

    move-result-object v8

    .line 228
    .local v8, "selectionArgs":[Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mQueryHandler:Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;

    const/16 v3, 0x3e8

    const/4 v4, 0x0

    sget-object v6, Lcom/android/server/audio/AudioQueryWeatherService;->WEATHER_PROJECTION:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    .end local v5    # "contentUri":Landroid/net/Uri;
    .end local v8    # "selectionArgs":[Ljava/lang/String;
    :cond_0
    monitor-exit v0

    .line 233
    return-void

    .line 232
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private updateCityInfo(Landroid/database/Cursor;)V
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;

    .line 188
    const/4 v0, 0x0

    .line 189
    .local v0, "cityId":Ljava/lang/String;
    const/4 v1, 0x0

    .line 190
    .local v1, "belongings":Ljava/lang/String;
    const/4 v2, -0x1

    .line 192
    .local v2, "flag":I
    const-string v3, "AudioQueryWeatherService"

    if-eqz p1, :cond_1

    .line 194
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 195
    const-string v4, "posID"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v0, v4

    .line 196
    const-string v4, "belongings"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v1, v4

    .line 197
    const-string v4, "flag"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move v2, v4

    .line 198
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "updateCityInfo flag:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 204
    :cond_0
    :try_start_1
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 207
    :goto_0
    goto :goto_1

    .line 205
    :catch_0
    move-exception v3

    .line 206
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 208
    .end local v3    # "e":Ljava/lang/Exception;
    goto :goto_1

    .line 203
    :catchall_0
    move-exception v3

    goto :goto_2

    .line 200
    :catch_1
    move-exception v3

    .line 201
    .restart local v3    # "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 204
    .end local v3    # "e":Ljava/lang/Exception;
    :try_start_3
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 208
    :goto_1
    nop

    .line 216
    invoke-direct {p0, v2, v0, v1}, Lcom/android/server/audio/AudioQueryWeatherService;->CalculateLocationAndQuery(ILjava/lang/String;Ljava/lang/String;)V

    .line 217
    return-void

    .line 204
    :goto_2
    :try_start_4
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 207
    goto :goto_3

    .line 205
    :catch_2
    move-exception v4

    .line 206
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 208
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_3
    throw v3

    .line 211
    :cond_1
    const-string v4, "location: weather maybe uninstalled"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mInternationalLocation:Z

    .line 214
    return-void
.end method

.method private updateWeatherInfo(Landroid/database/Cursor;)V
    .locals 7
    .param p1, "cursor"    # Landroid/database/Cursor;

    .line 271
    const-string/jumbo v0, "updateWeatherInfo start!"

    const-string v1, "AudioQueryWeatherService"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    const/4 v0, 0x1

    if-eqz p1, :cond_2

    .line 274
    const-wide/16 v1, 0x0

    .line 275
    .local v1, "sunriseTime":J
    const-wide/16 v3, 0x0

    .line 276
    .local v3, "sunsetTime":J
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 277
    iget-boolean v5, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mNextSunriseSunsetTime:Z

    if-ne v5, v0, :cond_0

    .line 278
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 280
    :cond_0
    const-string/jumbo v5, "sunrise"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    move-wide v1, v5

    .line 281
    const-string/jumbo v5, "sunset"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    move-wide v3, v5

    goto :goto_0

    .line 283
    :cond_1
    iput-boolean v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mInternationalLocation:Z

    .line 285
    :goto_0
    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/audio/AudioQueryWeatherService;->CalculateSunriseAndSunsetTime(JJ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 291
    .end local v1    # "sunriseTime":J
    .end local v3    # "sunsetTime":J
    :try_start_1
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 290
    :catchall_0
    move-exception v0

    goto :goto_3

    .line 286
    :catch_0
    move-exception v1

    .line 287
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    iput-boolean v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mInternationalLocation:Z

    .line 288
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 291
    .end local v1    # "e":Ljava/lang/Exception;
    :try_start_3
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 294
    :goto_1
    goto :goto_2

    .line 292
    :catch_1
    move-exception v0

    .line 293
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 295
    .end local v0    # "e":Ljava/lang/Exception;
    nop

    :goto_2
    goto :goto_5

    .line 291
    :goto_3
    :try_start_4
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 294
    goto :goto_4

    .line 292
    :catch_2
    move-exception v1

    .line 293
    .restart local v1    # "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 295
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_4
    throw v0

    .line 297
    :cond_2
    const-string/jumbo v2, "weather maybe uninstalled"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    iput-boolean v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mInternationalLocation:Z

    .line 300
    :goto_5
    return-void
.end method


# virtual methods
.method public getDefaultTimeZoneStatus()Z
    .locals 1

    .line 303
    iget-boolean v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mInternationalLocation:Z

    return v0
.end method

.method public getSunriseTimeHours()I
    .locals 1

    .line 307
    iget v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunriseTimeHours:I

    return v0
.end method

.method public getSunriseTimeMins()I
    .locals 1

    .line 315
    iget v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunriseTimeMins:I

    return v0
.end method

.method public getSunsetTimeHours()I
    .locals 1

    .line 311
    iget v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunsetTimeHours:I

    return v0
.end method

.method public getSunsetTimeMins()I
    .locals 1

    .line 319
    iget v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunsetTimeMins:I

    return v0
.end method

.method public onCreate()V
    .locals 4

    .line 94
    const-string v0, "AudioQueryWeatherService"

    const-string v1, "onCreate!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    iget-object v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mBootCompleteReceiver:Landroid/content/BroadcastReceiver;

    const/16 v1, 0x3e8

    if-nez v0, :cond_0

    .line 98
    new-instance v0, Lcom/android/server/audio/AudioQueryWeatherService$1;

    invoke-direct {v0, p0}, Lcom/android/server/audio/AudioQueryWeatherService$1;-><init>(Lcom/android/server/audio/AudioQueryWeatherService;)V

    iput-object v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mBootCompleteReceiver:Landroid/content/BroadcastReceiver;

    .line 109
    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.BOOT_COMPLETED"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 110
    .local v0, "intentFilterBootComplete":Landroid/content/IntentFilter;
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->setPriority(I)V

    .line 111
    iget-object v2, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mBootCompleteReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 115
    .end local v0    # "intentFilterBootComplete":Landroid/content/IntentFilter;
    :cond_0
    iget-object v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mUpdateTimeReceiver:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_1

    .line 116
    new-instance v0, Lcom/android/server/audio/AudioQueryWeatherService$2;

    invoke-direct {v0, p0}, Lcom/android/server/audio/AudioQueryWeatherService$2;-><init>(Lcom/android/server/audio/AudioQueryWeatherService;)V

    iput-object v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mUpdateTimeReceiver:Landroid/content/BroadcastReceiver;

    .line 128
    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "com.android.media.update.sunrise.sunset.time"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 129
    .local v0, "intentFilterUpdateTime":Landroid/content/IntentFilter;
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->setPriority(I)V

    .line 130
    iget-object v1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mUpdateTimeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 132
    .end local v0    # "intentFilterUpdateTime":Landroid/content/IntentFilter;
    :cond_1
    new-instance v0, Lcom/android/server/audio/AudioQueryWeatherService$LocationObserver;

    invoke-direct {v0, p0}, Lcom/android/server/audio/AudioQueryWeatherService$LocationObserver;-><init>(Lcom/android/server/audio/AudioQueryWeatherService;)V

    iput-object v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mLocationObserver:Lcom/android/server/audio/AudioQueryWeatherService$LocationObserver;

    .line 133
    invoke-virtual {v0}, Lcom/android/server/audio/AudioQueryWeatherService$LocationObserver;->onCreate()V

    .line 134
    return-void
.end method
