class com.android.server.audio.AudioGameEffect$GameEffectHandler extends android.os.Handler {
	 /* .source "AudioGameEffect.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/audio/AudioGameEffect; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "GameEffectHandler" */
} // .end annotation
/* # instance fields */
final com.android.server.audio.AudioGameEffect this$0; //synthetic
/* # direct methods */
 com.android.server.audio.AudioGameEffect$GameEffectHandler ( ) {
/* .locals 0 */
/* .line 181 */
this.this$0 = p1;
/* .line 182 */
/* invoke-direct {p0}, Landroid/os/Handler;-><init>()V */
/* .line 183 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 3 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 187 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 204 */
/* :pswitch_0 */
v0 = this.this$0;
com.android.server.audio.AudioGameEffect .-$$Nest$mstopGameEffect ( v0 );
/* .line 205 */
/* .line 201 */
/* :pswitch_1 */
v0 = this.this$0;
com.android.server.audio.AudioGameEffect .-$$Nest$mstartGameEffect ( v0 );
/* .line 202 */
/* .line 189 */
/* :pswitch_2 */
final String v0 = "ProcessManager"; // const-string v0, "ProcessManager"
android.os.ServiceManager .getService ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 190 */
	 v0 = this.this$0;
	 com.android.server.audio.AudioGameEffect .-$$Nest$fgetmForegroundInfoListener ( v0 );
	 miui.process.ProcessManager .registerForegroundInfoListener ( v0 );
	 /* .line 192 */
} // :cond_0
v0 = this.this$0;
v1 = com.android.server.audio.AudioGameEffect .-$$Nest$fgetmQueryPMServiceTime ( v0 );
/* add-int/lit8 v2, v1, 0x1 */
com.android.server.audio.AudioGameEffect .-$$Nest$fputmQueryPMServiceTime ( v0,v2 );
/* const/16 v0, 0x14 */
final String v2 = "AudioGameEffect"; // const-string v2, "AudioGameEffect"
/* if-ge v1, v0, :cond_1 */
/* .line 193 */
final String v0 = "process manager service not published, wait 1 second"; // const-string v0, "process manager service not published, wait 1 second"
android.util.Log .w ( v2,v0 );
/* .line 194 */
v0 = this.this$0;
com.android.server.audio.AudioGameEffect .-$$Nest$minitProcessListenerAsync ( v0 );
/* .line 196 */
} // :cond_1
final String v0 = "failed to get ProcessManager service"; // const-string v0, "failed to get ProcessManager service"
android.util.Log .e ( v2,v0 );
/* .line 199 */
/* nop */
/* .line 209 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
