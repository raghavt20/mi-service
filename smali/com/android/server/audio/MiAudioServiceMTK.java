public class com.android.server.audio.MiAudioServiceMTK implements android.media.Spatializer$OnSpatializerStateChangedListener {
	 /* .source "MiAudioServiceMTK.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/audio/MiAudioServiceMTK$SpatializerType;, */
	 /* Lcom/android/server/audio/MiAudioServiceMTK$DeviceChangeBroadcastReceiver; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
/* # instance fields */
private final Integer SPATIALIZER_PARAM_SPATIALIZER_TYPE;
private final Integer SPATIALIZER_PARAM_SPEAKER_ROTATION;
private android.media.AudioManager mAudioManager;
private com.android.server.audio.AudioService mAudioService;
final android.content.Context mContext;
private com.android.server.audio.MiAudioServiceMTK$DeviceChangeBroadcastReceiver mDeviceConnectStateListener;
com.android.server.audio.dolbyeffect.DolbyEffectController mDolbyEffectController;
private android.database.ContentObserver mEffecImplementerObserver;
private android.net.Uri mEffecImplementerUri;
private final Boolean mIsSupportedDolbyEffectControl;
private final Boolean mIsSupportedSelectedDolbyTunningByVolume;
private android.media.Spatializer mSpatializer;
private Boolean mSpatializerEnabled;
private Integer mSpatilizerType;
private final Boolean mUseXiaoMiSpatilizer;
/* # direct methods */
static com.android.server.audio.AudioService -$$Nest$fgetmAudioService ( com.android.server.audio.MiAudioServiceMTK p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mAudioService;
} // .end method
static Integer -$$Nest$fgetmSpatilizerType ( com.android.server.audio.MiAudioServiceMTK p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mSpatilizerType:I */
} // .end method
static Boolean -$$Nest$fgetmUseXiaoMiSpatilizer ( com.android.server.audio.MiAudioServiceMTK p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mUseXiaoMiSpatilizer:Z */
} // .end method
static void -$$Nest$fputmSpatilizerType ( com.android.server.audio.MiAudioServiceMTK p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput p1, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mSpatilizerType:I */
	 return;
} // .end method
static -$$Nest$mintToBytes ( com.android.server.audio.MiAudioServiceMTK p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/audio/MiAudioServiceMTK;->intToBytes(I)[B */
} // .end method
public com.android.server.audio.MiAudioServiceMTK ( ) {
	 /* .locals 2 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .param p2, "audioservice" # Lcom/android/server/audio/AudioService; */
	 /* .line 59 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 41 */
	 /* const/16 v0, 0x130 */
	 /* iput v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->SPATIALIZER_PARAM_SPEAKER_ROTATION:I */
	 /* .line 42 */
	 /* const/16 v0, 0x120 */
	 /* iput v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->SPATIALIZER_PARAM_SPATIALIZER_TYPE:I */
	 /* .line 43 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput-boolean v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mSpatializerEnabled:Z */
	 /* .line 44 */
	 v0 = com.android.server.audio.MiAudioServiceMTK$SpatializerType.DOLBY;
	 v0 = 	 (( com.android.server.audio.MiAudioServiceMTK$SpatializerType ) v0 ).ordinal ( ); // invoke-virtual {v0}, Lcom/android/server/audio/MiAudioServiceMTK$SpatializerType;->ordinal()I
	 /* iput v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mSpatilizerType:I */
	 /* .line 50 */
	 /* nop */
	 /* .line 51 */
	 /* const-string/jumbo v0, "vendor.audio.dolby.control.support" */
	 android.os.SystemProperties .get ( v0 );
	 /* const-string/jumbo v1, "true" */
	 v0 = 	 (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 /* iput-boolean v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mIsSupportedDolbyEffectControl:Z */
	 /* .line 52 */
	 /* nop */
	 /* .line 53 */
	 /* const-string/jumbo v0, "vendor.audio.dolby.control.tunning.by.volume.support" */
	 android.os.SystemProperties .get ( v0 );
	 v0 = 	 (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 /* iput-boolean v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mIsSupportedSelectedDolbyTunningByVolume:Z */
	 /* .line 54 */
	 /* nop */
	 /* .line 55 */
	 /* const-string/jumbo v0, "vendor.audio.useXiaomiSpatializer" */
	 android.os.SystemProperties .get ( v0 );
	 v0 = 	 (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 /* iput-boolean v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mUseXiaoMiSpatilizer:Z */
	 /* .line 60 */
	 this.mContext = p1;
	 /* .line 61 */
	 this.mAudioService = p2;
	 /* .line 62 */
	 final String v0 = "MiAudioServiceMTK"; // const-string v0, "MiAudioServiceMTK"
	 final String v1 = "MiAudioServiceMTK()"; // const-string v1, "MiAudioServiceMTK()"
	 android.util.Log .d ( v0,v1 );
	 /* .line 63 */
	 return;
} // .end method
private intToBytes ( Integer p0 ) {
	 /* .locals 3 */
	 /* .param p1, "src" # I */
	 /* .line 215 */
	 int v0 = 4; // const/4 v0, 0x4
	 /* new-array v0, v0, [B */
	 /* .line 216 */
	 /* .local v0, "result":[B */
	 /* shr-int/lit8 v1, p1, 0x18 */
	 /* and-int/lit16 v1, v1, 0xff */
	 /* int-to-byte v1, v1 */
	 int v2 = 3; // const/4 v2, 0x3
	 /* aput-byte v1, v0, v2 */
	 /* .line 217 */
	 /* shr-int/lit8 v1, p1, 0x10 */
	 /* and-int/lit16 v1, v1, 0xff */
	 /* int-to-byte v1, v1 */
	 int v2 = 2; // const/4 v2, 0x2
	 /* aput-byte v1, v0, v2 */
	 /* .line 218 */
	 /* shr-int/lit8 v1, p1, 0x8 */
	 /* and-int/lit16 v1, v1, 0xff */
	 /* int-to-byte v1, v1 */
	 int v2 = 1; // const/4 v2, 0x1
	 /* aput-byte v1, v0, v2 */
	 /* .line 219 */
	 /* and-int/lit16 v1, p1, 0xff */
	 /* int-to-byte v1, v1 */
	 int v2 = 0; // const/4 v2, 0x0
	 /* aput-byte v1, v0, v2 */
	 /* .line 220 */
} // .end method
/* # virtual methods */
public void DeviceConectedStateListenerInit ( ) {
	 /* .locals 3 */
	 /* .line 105 */
	 /* new-instance v0, Lcom/android/server/audio/MiAudioServiceMTK$DeviceChangeBroadcastReceiver; */
	 int v1 = 0; // const/4 v1, 0x0
	 /* invoke-direct {v0, p0, v1}, Lcom/android/server/audio/MiAudioServiceMTK$DeviceChangeBroadcastReceiver;-><init>(Lcom/android/server/audio/MiAudioServiceMTK;Lcom/android/server/audio/MiAudioServiceMTK$DeviceChangeBroadcastReceiver-IA;)V */
	 this.mDeviceConnectStateListener = v0;
	 /* .line 106 */
	 /* new-instance v0, Landroid/content/IntentFilter; */
	 /* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
	 /* .line 107 */
	 /* .local v0, "filter":Landroid/content/IntentFilter; */
	 final String v1 = "android.intent.action.HEADSET_PLUG"; // const-string v1, "android.intent.action.HEADSET_PLUG"
	 (( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
	 /* .line 109 */
	 final String v1 = "android.bluetooth.headset.action.VENDOR_SPECIFIC_HEADSET_EVENT"; // const-string v1, "android.bluetooth.headset.action.VENDOR_SPECIFIC_HEADSET_EVENT"
	 (( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
	 /* .line 110 */
	 final String v1 = "android.bluetooth.device.action.BOND_STATE_CHANGED"; // const-string v1, "android.bluetooth.device.action.BOND_STATE_CHANGED"
	 (( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
	 /* .line 111 */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "android.bluetooth.headset.intent.category.companyid."; // const-string v2, "android.bluetooth.headset.intent.category.companyid."
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* .line 112 */
	 /* const/16 v2, 0x38f */
	 java.lang.Integer .toString ( v2 );
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 /* .line 111 */
	 (( android.content.IntentFilter ) v0 ).addCategory ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V
	 /* .line 113 */
	 v1 = this.mContext;
	 v2 = this.mDeviceConnectStateListener;
	 (( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
	 /* .line 114 */
	 return;
} // .end method
public void DolbyEffectControllerInit ( ) {
	 /* .locals 1 */
	 /* .line 79 */
	 v0 = this.mContext;
	 com.android.server.audio.dolbyeffect.DolbyEffectController .getInstance ( v0 );
	 this.mDolbyEffectController = v0;
	 /* .line 80 */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 81 */
		 (( com.android.server.audio.dolbyeffect.DolbyEffectController ) v0 ).init ( ); // invoke-virtual {v0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->init()V
		 /* .line 83 */
	 } // :cond_0
	 return;
} // .end method
public void SettingsObserver ( ) {
	 /* .locals 4 */
	 /* .line 127 */
	 final String v0 = "effect_implementer"; // const-string v0, "effect_implementer"
	 android.provider.Settings$Global .getUriFor ( v0 );
	 this.mEffecImplementerUri = v0;
	 /* .line 128 */
	 /* new-instance v0, Lcom/android/server/audio/MiAudioServiceMTK$1; */
	 int v1 = 0; // const/4 v1, 0x0
	 /* invoke-direct {v0, p0, v1}, Lcom/android/server/audio/MiAudioServiceMTK$1;-><init>(Lcom/android/server/audio/MiAudioServiceMTK;Landroid/os/Handler;)V */
	 this.mEffecImplementerObserver = v0;
	 /* .line 151 */
	 v0 = this.mContext;
	 (( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 v1 = this.mEffecImplementerUri;
	 int v2 = 1; // const/4 v2, 0x1
	 v3 = this.mEffecImplementerObserver;
	 (( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
	 /* .line 152 */
	 return;
} // .end method
public void SpatialStateInit ( ) {
	 /* .locals 3 */
	 /* .line 86 */
	 v0 = this.mContext;
	 /* const-class v1, Landroid/media/AudioManager; */
	 (( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
	 /* check-cast v0, Landroid/media/AudioManager; */
	 this.mAudioManager = v0;
	 /* .line 87 */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 88 */
		 (( android.media.AudioManager ) v0 ).getSpatializer ( ); // invoke-virtual {v0}, Landroid/media/AudioManager;->getSpatializer()Landroid/media/Spatializer;
		 this.mSpatializer = v0;
		 /* .line 90 */
	 } // :cond_0
	 v0 = this.mSpatializer;
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 91 */
		 java.util.concurrent.Executors .newSingleThreadExecutor ( );
		 (( android.media.Spatializer ) v0 ).addOnSpatializerStateChangedListener ( v1, p0 ); // invoke-virtual {v0, v1, p0}, Landroid/media/Spatializer;->addOnSpatializerStateChangedListener(Ljava/util/concurrent/Executor;Landroid/media/Spatializer$OnSpatializerStateChangedListener;)V
		 /* .line 93 */
	 } // :cond_1
	 v0 = this.mContext;
	 (( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 final String v1 = "effect_implementer"; // const-string v1, "effect_implementer"
	 android.provider.Settings$Global .getString ( v0,v1 );
	 /* .line 94 */
	 /* .local v0, "effecImplementer":Ljava/lang/String; */
	 final String v1 = "dolby"; // const-string v1, "dolby"
	 v1 = 	 (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_2
		 /* .line 95 */
		 v1 = com.android.server.audio.MiAudioServiceMTK$SpatializerType.DOLBY;
		 v1 = 		 (( com.android.server.audio.MiAudioServiceMTK$SpatializerType ) v1 ).ordinal ( ); // invoke-virtual {v1}, Lcom/android/server/audio/MiAudioServiceMTK$SpatializerType;->ordinal()I
		 /* iput v1, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mSpatilizerType:I */
		 /* .line 96 */
	 } // :cond_2
	 final String v1 = "misound"; // const-string v1, "misound"
	 v1 = 	 (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_3
		 /* .line 97 */
		 v1 = com.android.server.audio.MiAudioServiceMTK$SpatializerType.MISOUND;
		 v1 = 		 (( com.android.server.audio.MiAudioServiceMTK$SpatializerType ) v1 ).ordinal ( ); // invoke-virtual {v1}, Lcom/android/server/audio/MiAudioServiceMTK$SpatializerType;->ordinal()I
		 /* iput v1, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mSpatilizerType:I */
		 /* .line 98 */
	 } // :cond_3
	 final String v1 = "none"; // const-string v1, "none"
	 v1 = 	 (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_4
		 /* .line 99 */
		 v1 = com.android.server.audio.MiAudioServiceMTK$SpatializerType.NONE;
		 v1 = 		 (( com.android.server.audio.MiAudioServiceMTK$SpatializerType ) v1 ).ordinal ( ); // invoke-virtual {v1}, Lcom/android/server/audio/MiAudioServiceMTK$SpatializerType;->ordinal()I
		 /* iput v1, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mSpatilizerType:I */
		 /* .line 101 */
	 } // :cond_4
} // :goto_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Init EffectImplementer is "; // const-string v2, "Init EffectImplementer is "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiAudioServiceMTK"; // const-string v2, "MiAudioServiceMTK"
android.util.Log .d ( v2,v1 );
/* .line 102 */
return;
} // .end method
public void adjustStreamVolume ( Integer p0, Integer p1, Integer p2, java.lang.String p3, java.lang.String p4, Integer p5, Integer p6, java.lang.String p7, Boolean p8, Integer p9 ) {
/* .locals 3 */
/* .param p1, "streamType" # I */
/* .param p2, "direction" # I */
/* .param p3, "flags" # I */
/* .param p4, "callingPackage" # Ljava/lang/String; */
/* .param p5, "caller" # Ljava/lang/String; */
/* .param p6, "uid" # I */
/* .param p7, "pid" # I */
/* .param p8, "attributionTag" # Ljava/lang/String; */
/* .param p9, "hasModifyAudioSettings" # Z */
/* .param p10, "keyEventMode" # I */
/* .line 201 */
v0 = this.mDolbyEffectController;
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 202 */
	 /* iget-boolean v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mIsSupportedSelectedDolbyTunningByVolume:Z */
	 /* if-nez v0, :cond_0 */
	 /* .line 203 */
	 return;
	 /* .line 204 */
} // :cond_0
int v0 = 3; // const/4 v0, 0x3
/* if-ne p1, v0, :cond_1 */
/* .line 205 */
v1 = this.mAudioService;
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 206 */
	 int v2 = 2; // const/4 v2, 0x2
	 v0 = 	 (( com.android.server.audio.AudioService ) v1 ).getDeviceStreamVolume ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Lcom/android/server/audio/AudioService;->getDeviceStreamVolume(II)I
	 /* .line 207 */
	 /* .local v0, "newVolume":I */
	 final String v1 = "MiAudioServiceMTK"; // const-string v1, "MiAudioServiceMTK"
	 final String v2 = "adjustStreamVolume"; // const-string v2, "adjustStreamVolume"
	 android.util.Log .d ( v1,v2 );
	 /* .line 208 */
	 v1 = this.mDolbyEffectController;
	 (( com.android.server.audio.dolbyeffect.DolbyEffectController ) v1 ).receiveVolumeChanged ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->receiveVolumeChanged(I)V
	 /* .line 212 */
} // .end local v0 # "newVolume":I
} // :cond_1
return;
} // .end method
public void functionsRoadedInit ( ) {
/* .locals 1 */
/* .line 70 */
/* iget-boolean v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mIsSupportedDolbyEffectControl:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 71 */
(( com.android.server.audio.MiAudioServiceMTK ) p0 ).DolbyEffectControllerInit ( ); // invoke-virtual {p0}, Lcom/android/server/audio/MiAudioServiceMTK;->DolbyEffectControllerInit()V
/* .line 72 */
(( com.android.server.audio.MiAudioServiceMTK ) p0 ).DeviceConectedStateListenerInit ( ); // invoke-virtual {p0}, Lcom/android/server/audio/MiAudioServiceMTK;->DeviceConectedStateListenerInit()V
/* .line 74 */
} // :cond_0
(( com.android.server.audio.MiAudioServiceMTK ) p0 ).SettingsObserver ( ); // invoke-virtual {p0}, Lcom/android/server/audio/MiAudioServiceMTK;->SettingsObserver()V
/* .line 75 */
(( com.android.server.audio.MiAudioServiceMTK ) p0 ).SpatialStateInit ( ); // invoke-virtual {p0}, Lcom/android/server/audio/MiAudioServiceMTK;->SpatialStateInit()V
/* .line 76 */
return;
} // .end method
public void onRotationUpdate ( java.lang.Integer p0 ) {
/* .locals 5 */
/* .param p1, "rotation" # Ljava/lang/Integer; */
/* .line 182 */
v0 = this.mDolbyEffectController;
final String v1 = "MiAudioServiceMTK"; // const-string v1, "MiAudioServiceMTK"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 183 */
final String v0 = "onRotationUpdate, receiveRotationChanged"; // const-string v0, "onRotationUpdate, receiveRotationChanged"
android.util.Log .d ( v1,v0 );
/* .line 184 */
v0 = this.mDolbyEffectController;
v2 = (( java.lang.Integer ) p1 ).intValue ( ); // invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I
(( com.android.server.audio.dolbyeffect.DolbyEffectController ) v0 ).receiveRotationChanged ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->receiveRotationChanged(I)V
/* .line 186 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mUseXiaoMiSpatilizer:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mAudioService;
if ( v0 != null) { // if-eqz v0, :cond_1
	 v0 = 	 (( com.android.server.audio.AudioService ) v0 ).isSpatializerEnabled ( ); // invoke-virtual {v0}, Lcom/android/server/audio/AudioService;->isSpatializerEnabled()Z
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 v0 = this.mAudioService;
		 v0 = 		 (( com.android.server.audio.AudioService ) v0 ).isSpatializerAvailable ( ); // invoke-virtual {v0}, Lcom/android/server/audio/AudioService;->isSpatializerAvailable()Z
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 /* .line 187 */
			 v0 = 			 (( java.lang.Integer ) p1 ).intValue ( ); // invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I
			 /* invoke-direct {p0, v0}, Lcom/android/server/audio/MiAudioServiceMTK;->intToBytes(I)[B */
			 /* .line 189 */
			 /* .local v0, "Rotation":[B */
			 try { // :try_start_0
				 /* new-instance v2, Ljava/lang/StringBuilder; */
				 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
				 /* const-string/jumbo v3, "setSpatializerParameter SPATIALIZER_PARAM_SPEAKER_ROTATION = " */
				 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
				 android.util.Log .d ( v1,v2 );
				 /* .line 190 */
				 v2 = this.mAudioService;
				 /* const/16 v3, 0x130 */
				 (( com.android.server.audio.AudioService ) v2 ).setSpatializerParameter ( v3, v0 ); // invoke-virtual {v2, v3, v0}, Lcom/android/server/audio/AudioService;->setSpatializerParameter(I[B)V
				 /* :try_end_0 */
				 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
				 /* .line 193 */
				 /* .line 191 */
				 /* :catch_0 */
				 /* move-exception v2 */
				 /* .line 192 */
				 /* .local v2, "e":Ljava/lang/Exception; */
				 /* new-instance v3, Ljava/lang/StringBuilder; */
				 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
				 /* const-string/jumbo v4, "setSpatializerParameter SPATIALIZER_PARAM_SPEAKER_ROTATION Exception " */
				 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
				 android.util.Log .e ( v1,v3 );
				 /* .line 195 */
			 } // .end local v0 # "Rotation":[B
		 } // .end local v2 # "e":Ljava/lang/Exception;
	 } // :cond_1
} // :goto_0
return;
} // .end method
public void onSpatializerAvailableChanged ( android.media.Spatializer p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "spat" # Landroid/media/Spatializer; */
/* .param p2, "available" # Z */
/* .line 175 */
v0 = this.mDolbyEffectController;
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 176 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "onSpatializerAvailableChanged: available = "; // const-string v1, "onSpatializerAvailableChanged: available = "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v1 = "MiAudioServiceMTK"; // const-string v1, "MiAudioServiceMTK"
	 android.util.Log .d ( v1,v0 );
	 /* .line 177 */
	 v0 = this.mDolbyEffectController;
	 (( com.android.server.audio.dolbyeffect.DolbyEffectController ) v0 ).receiveSpatializerAvailableChanged ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->receiveSpatializerAvailableChanged(Z)V
	 /* .line 179 */
} // :cond_0
return;
} // .end method
public void onSpatializerEnabledChanged ( android.media.Spatializer p0, Boolean p1 ) {
/* .locals 4 */
/* .param p1, "spat" # Landroid/media/Spatializer; */
/* .param p2, "enabled" # Z */
/* .line 156 */
/* iput-boolean p2, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mSpatializerEnabled:Z */
/* .line 157 */
v0 = this.mDolbyEffectController;
final String v1 = "MiAudioServiceMTK"; // const-string v1, "MiAudioServiceMTK"
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 158 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "onSpatializerEnabledChanged: enabled = "; // const-string v2, "onSpatializerEnabledChanged: enabled = "
	 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Log .d ( v1,v0 );
	 /* .line 159 */
	 v0 = this.mDolbyEffectController;
	 (( com.android.server.audio.dolbyeffect.DolbyEffectController ) v0 ).receiveSpatializerEnabledChanged ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->receiveSpatializerEnabledChanged(Z)V
	 /* .line 161 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mUseXiaoMiSpatilizer:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
	 /* iget-boolean v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mSpatializerEnabled:Z */
	 if ( v0 != null) { // if-eqz v0, :cond_2
		 /* .line 163 */
		 try { // :try_start_0
			 /* new-instance v0, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v2 = "onSpatializerEnabledChanged: setSpatializerParameter mSpatilizerType = "; // const-string v2, "onSpatializerEnabledChanged: setSpatializerParameter mSpatilizerType = "
			 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 /* iget v2, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mSpatilizerType:I */
			 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 android.util.Log .d ( v1,v0 );
			 /* .line 164 */
			 v0 = this.mAudioService;
			 if ( v0 != null) { // if-eqz v0, :cond_1
				 /* .line 165 */
				 /* iget v2, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mSpatilizerType:I */
				 /* invoke-direct {p0, v2}, Lcom/android/server/audio/MiAudioServiceMTK;->intToBytes(I)[B */
				 /* const/16 v3, 0x120 */
				 (( com.android.server.audio.AudioService ) v0 ).setSpatializerParameter ( v3, v2 ); // invoke-virtual {v0, v3, v2}, Lcom/android/server/audio/AudioService;->setSpatializerParameter(I[B)V
				 /* :try_end_0 */
				 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
				 /* .line 169 */
			 } // :cond_1
			 /* .line 167 */
			 /* :catch_0 */
			 /* move-exception v0 */
			 /* .line 168 */
			 /* .local v0, "e":Ljava/lang/Exception; */
			 /* new-instance v2, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v3 = "onSpatializerEnabledChanged Exception:"; // const-string v3, "onSpatializerEnabledChanged Exception:"
			 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 android.util.Log .e ( v1,v2 );
			 /* .line 171 */
		 } // .end local v0 # "e":Ljava/lang/Exception;
	 } // :cond_2
} // :goto_0
return;
} // .end method
public void onSystemReady ( ) {
/* .locals 0 */
/* .line 66 */
(( com.android.server.audio.MiAudioServiceMTK ) p0 ).functionsRoadedInit ( ); // invoke-virtual {p0}, Lcom/android/server/audio/MiAudioServiceMTK;->functionsRoadedInit()V
/* .line 67 */
return;
} // .end method
