.class Lcom/android/server/audio/CloudServiceSettings$ParametersSetting;
.super Lcom/android/server/audio/CloudServiceSettings$Setting;
.source "CloudServiceSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/CloudServiceSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ParametersSetting"
.end annotation


# static fields
.field private static final KVPAIRS:Ljava/lang/String; = "kvpairs"


# instance fields
.field private mKvpairs:Ljava/lang/StringBuilder;

.field final synthetic this$0:Lcom/android/server/audio/CloudServiceSettings;


# direct methods
.method constructor <init>(Lcom/android/server/audio/CloudServiceSettings;Lorg/json/JSONObject;)V
    .locals 3
    .param p1, "this$0"    # Lcom/android/server/audio/CloudServiceSettings;
    .param p2, "json"    # Lorg/json/JSONObject;

    .line 542
    iput-object p1, p0, Lcom/android/server/audio/CloudServiceSettings$ParametersSetting;->this$0:Lcom/android/server/audio/CloudServiceSettings;

    .line 543
    invoke-direct {p0, p1, p2}, Lcom/android/server/audio/CloudServiceSettings$Setting;-><init>(Lcom/android/server/audio/CloudServiceSettings;Lorg/json/JSONObject;)V

    .line 545
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "kvpairs"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/audio/CloudServiceSettings$ParametersSetting;->mKvpairs:Ljava/lang/StringBuilder;

    .line 546
    iget-boolean v0, p0, Lcom/android/server/audio/CloudServiceSettings$ParametersSetting;->mSecreted:Z

    if-eqz v0, :cond_0

    .line 547
    iget-object v0, p0, Lcom/android/server/audio/CloudServiceSettings$ParametersSetting;->mSecretedItem:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$ParametersSetting;->mKvpairs:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 551
    :cond_0
    goto :goto_0

    .line 549
    :catch_0
    move-exception v0

    .line 550
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "CloudServiceSettings"

    const-string v2, "fail to parse ParametersSetting"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method


# virtual methods
.method public set()Z
    .locals 2

    .line 556
    invoke-super {p0}, Lcom/android/server/audio/CloudServiceSettings$Setting;->set()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 557
    return v1

    .line 559
    :cond_0
    iget-object v0, p0, Lcom/android/server/audio/CloudServiceSettings$ParametersSetting;->mKvpairs:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    iput-boolean v1, p0, Lcom/android/server/audio/CloudServiceSettings$ParametersSetting;->mSuccess:Z

    .line 560
    iget-boolean v0, p0, Lcom/android/server/audio/CloudServiceSettings$ParametersSetting;->mSuccess:Z

    return v0
.end method

.method public updateTo(Lcom/android/server/audio/CloudServiceSettings$Setting;)Z
    .locals 2
    .param p1, "s"    # Lcom/android/server/audio/CloudServiceSettings$Setting;

    .line 565
    invoke-super {p0, p1}, Lcom/android/server/audio/CloudServiceSettings$Setting;->updateTo(Lcom/android/server/audio/CloudServiceSettings$Setting;)Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/android/server/audio/CloudServiceSettings$ParametersSetting;

    if-eqz v0, :cond_0

    .line 566
    move-object v0, p1

    check-cast v0, Lcom/android/server/audio/CloudServiceSettings$ParametersSetting;

    .line 567
    .local v0, "ps":Lcom/android/server/audio/CloudServiceSettings$ParametersSetting;
    iget-object v1, v0, Lcom/android/server/audio/CloudServiceSettings$ParametersSetting;->mKvpairs:Ljava/lang/StringBuilder;

    iput-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$ParametersSetting;->mKvpairs:Ljava/lang/StringBuilder;

    .line 568
    const/4 v1, 0x1

    return v1

    .line 570
    .end local v0    # "ps":Lcom/android/server/audio/CloudServiceSettings$ParametersSetting;
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "fail to update ParametersSetting: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$ParametersSetting;->mSettingName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CloudServiceSettings"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 571
    const/4 v0, 0x0

    return v0
.end method
