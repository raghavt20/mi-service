.class public Lcom/android/server/audio/AudioServiceStubImpl;
.super Lcom/android/server/audio/AudioServiceStub;
.source "AudioServiceStubImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.audio.AudioServiceStub$$"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/audio/AudioServiceStubImpl$H;,
        Lcom/android/server/audio/AudioServiceStubImpl$AudioPowerSaveEnable;,
        Lcom/android/server/audio/AudioServiceStubImpl$EffectChangeBroadcastReceiver;
    }
.end annotation


# static fields
.field private static final ABNORMAL_AUDIO_INFO_EVENT_NAME:Ljava/lang/String; = "check_audio_route_for_bluetooth"

.field public static final ACTION_VOLUME_BOOST:Ljava/lang/String; = "miui.intent.action.VOLUME_BOOST"

.field private static final AUDIO_ADSP_SPATIALIZER_AVAILABLE:Ljava/lang/String; = "ro.vendor.audio.adsp.spatial"

.field private static final AUDIO_ADSP_SPATIALIZER_ENABLE:Ljava/lang/String; = "persist.vendor.audio.dolbysurround.enable"

.field private static final AUDIO_BT_CONFIG_PROP:I

.field private static final AUDIO_CAMERA_BT_RECORD_SUPPORT:Ljava/lang/String; = "ro.vendor.audio.camera.bt.record.support"

.field private static final AUDIO_CINEMA_MODE_SUPPORT:Ljava/lang/String; = "persist.vendor.audio.cinema.support"

.field private static final AUDIO_DOLBY_CONTROL_SUPPORT:Ljava/lang/String; = "vendor.audio.dolby.control.support"

.field private static final AUDIO_ONETRACK_SILENT_OBSERVER:Ljava/lang/String; = "audio_silent_observer"

.field private static final AUDIO_PARAMETER_DEFAULT:Ljava/lang/String; = "sound_transmit_enable=0"

.field private static final AUDIO_PARAMETER_HA:Ljava/lang/String; = "sound_transmit_enable=6"

.field private static final AUDIO_PARAMETER_KARAOKE_OFF:Ljava/lang/String; = "audio_karaoke_enable=0"

.field private static final AUDIO_PARAMETER_KARAOKE_ON:Ljava/lang/String; = "audio_karaoke_enable=1"

.field private static final AUDIO_PARAMETER_KTV_OFF:Ljava/lang/String; = "audio_karaoke_ktvmode=disable"

.field private static final AUDIO_PARAMETER_KTV_ON:Ljava/lang/String; = "audio_karaoke_ktvmode=enable"

.field private static final AUDIO_PARAMETER_WT:Ljava/lang/String; = "sound_transmit_enable=1"

.field private static final AUDIO_POWER_SAVE_SETTING:Ljava/lang/String; = "persist.vendor.audio.power.save.setting"

.field private static final AUDIO_SILENT_LEVEL:Ljava/lang/String; = "audio_silent_level"

.field private static final AUDIO_SILENT_LOCATION:Ljava/lang/String; = "audio_silent_location"

.field private static final AUDIO_SILENT_REASON:Ljava/lang/String; = "audio_silent_reason"

.field private static final AUDIO_SILENT_TYPE:Ljava/lang/String; = "audio_silent_type"

.field private static final CAMERA_AUDIO_HEADSET_STATE:Ljava/lang/String; = "audio_headset_state"

.field private static final CAMERA_PACKAGE_NAME:Ljava/lang/String; = "com.android.camera"

.field private static final DEFAULT_AUDIO_PARAMETERS:[Ljava/lang/String;

.field private static final DELAY_NOTIFY_BT_MAX_NUM:I = 0x3

.field private static final DELAY_NOTIFY_BT_STOPBLUETOOTHSCO_MS:I = 0xc8

.field public static final EXTRA_BOOST_STATE:Ljava/lang/String; = "volume_boost_state"

.field private static final FLAG_REALLY_TIME_MODE_ENABLED:I = 0x1

.field private static final FLAG_SOUND_LEAK_PROTECTION_ENABLED:I = 0x2

.field private static final HEADPHONES_EVENT_NAME:Ljava/lang/String; = "headphones"

.field static HIFI_NOT_SUPPORT_DEVICE_LIST:Ljava/util/List; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final KEY_PERSIST_CUMULATIVE_PLAYBACK_MS:Ljava/lang/String; = "key_persist_cumulative_playback_ms"

.field private static final KEY_PERSIST_NOTIFICATION_DATE:Ljava/lang/String; = "key_persist_notification_date"

.field private static final KEY_PERSIST_PLAYBACK_CONTINUOUS_MS:Ljava/lang/String; = "key_persist_playback_continuous_ms"

.field private static final KEY_PERSIST_PLAYBACK_HIGH_VOICE:Ljava/lang/String; = "key_persist_playback_high_voice"

.field private static final KEY_PERSIST_PLAYBACK_START_MS:Ljava/lang/String; = "key_persist_playback_start_ms"

.field public static final LONG_TIME_PROMPT:I = 0x2

.field public static final MAX_VOLUME_LONG_TIME:I = 0x3

.field private static final MAX_VOLUME_VALID_TIME_INTERVAL:I = 0x240c8400

.field private static final MIUI_MAX_MUSIC_VOLUME_STEP:I = 0xf

.field private static final MQSSERVER_REPORT_RATE_MS:I

.field private static final MSG_DATA_TRACK:I = 0x1

.field private static final MSG_MQSSERVER_REPORT:I = 0x6

.field private static final MSG_REPORT_ABNORMAL_AUDIO_STATE:I = 0x5

.field private static final MSG_REPORT_AUDIO_SILENT_OBSERVER:I = 0x4

.field private static final MSG_REPORT_MULTI_VOIP_DAILY_USE_FOR_BT_CONNECT:I = 0x2

.field private static final MSG_REPORT_WAVEFORM_INFO_TRACK:I = 0x3

.field private static final MSG_UPDATE_AUDIO_MODE:I = 0x7

.field private static final MSG_UPDATE_MEDIA_STATE:I = 0x9

.field private static final MSG_UPDATE_MEETING_MODE:I = 0x8

.field private static final MUSIC_ACTIVE_CONTINUOUS_MS_MAX:I = 0x36ee80

.field public static final MUSIC_ACTIVE_CONTINUOUS_POLL_PERIOD_MS:I = 0xea60

.field private static final MUSIC_ACTIVE_DATA_REPORT_INTERVAL:I = 0xea60

.field private static final MUSIC_ACTIVE_RETRY_POLL_PERIOD_MS:I = 0x7530

.field private static final NOTE_USB_HEADSET_PLUG:I = 0x53466666

.field public static final READ_MUSIC_PLAY_BACK_DELAY:I = 0x2710

.field private static final REPORT_AUDIO_EXCEPTION_INFO_INTERVAL_TIME:I = 0x1770

.field private static final SCO_DEVICES_MAX_NUM:I = 0xa

.field private static final STATE_AUDIO_SCO_CONNECTED:Ljava/lang/String; = "connect"

.field private static final STATE_AUDIO_SCO_DISCONNECTED:Ljava/lang/String; = "disconnect"

.field private static final TAG:Ljava/lang/String; = "AudioServiceStubImpl"

.field private static final TRANSMIT_AUDIO_PARAMETERS:[Ljava/lang/String;

.field private static final WAVEFORM_EVENT_NAME:Ljava/lang/String; = "waveform_info"

.field public static mIsAudioPlaybackTriggerSupported:Z


# instance fields
.field private final REALLY_TIME_MODE_ENABLED:Z

.field private final SOUND_LEAK_PROTECTION_ENABLED:Z

.field private cameraToastServiceRiid:I

.field private isSupportPollAudioMicStatus:Z

.field private mA2dpDeviceConnectedState:I

.field private mAudioGameEffect:Lcom/android/server/audio/AudioGameEffect;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mAudioMode:I

.field private final mAudioParameterClientList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/server/audio/AudioParameterClient;",
            ">;"
        }
    .end annotation
.end field

.field private mAudioPowerSaveModeObserver:Lcom/android/server/audio/AudioPowerSaveModeObserver;

.field private mAudioQueryWeatherService:Lcom/android/server/audio/AudioQueryWeatherService;

.field private mAudioService:Lcom/android/server/audio/AudioService;

.field private mAudioThermalObserver:Lcom/android/server/audio/AudioThermalObserver;

.field private mBtHelper:Lcom/android/server/audio/BtHelper;

.field private mBtName:Ljava/lang/String;

.field private mCloudService:Lcom/android/server/audio/CloudServiceThread;

.field private mCommunicationRouteClients:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mContinuousMs:J

.field public mCumulativePlaybackEndTime:J

.field public mCumulativePlaybackStartTime:J

.field private mDelayNotifyBtStopScoCount:I

.field private mDeviceBroker:Lcom/android/server/audio/AudioDeviceBroker;

.field private mEffectChangeBroadcastReceiver:Lcom/android/server/audio/AudioServiceStubImpl$EffectChangeBroadcastReceiver;

.field private mFeatureAdapter:Lcom/android/server/audio/feature/FeatureAdapter;

.field private mFoldableAdapter:Lcom/android/server/audio/foldable/FoldableAdapter;

.field private mGameAudioEnhancer:Lcom/android/server/audio/GameAudioEnhancer;

.field private mHandler:Landroid/os/Handler;

.field private mHeadsetDeviceConnectedState:I

.field private mHighLevel:I

.field private mInDelayNotifyBtStopSco:Z

.field private final mIsAdspSpatializerAvailable:Z

.field private final mIsSupportCinemaMode:Z

.field private final mIsSupportFWAudioEffectCenter:Z

.field private final mIsSupportedCameraRecord:Z

.field private final mIsSupportedDolbyEffectControl:Z

.field private mIsSupportedReportAudioRouteState:Z

.field private mMiAudioServiceMTK:Lcom/android/server/audio/MiAudioServiceMTK;

.field private mMiuiXlog:Landroid/media/MiuiXlog;

.field private mModeOwnerPid:I

.field private mMqsUtils:Lcom/android/server/audio/MQSUtils;

.field private mMultiVoipPackages:Ljava/lang/String;

.field private mMusicPlaybackContinuousMs:J

.field private mMusicPlaybackContinuousMsTotal:J

.field private mMusicVolumeDb:F

.field private mMusicVolumeIndex:I

.field private mNm:Landroid/app/NotificationManager;

.field private mNotificationDate:Ljava/time/LocalDate;

.field private mNotificationTimes:I

.field private mPadAdapter:Lcom/android/server/audio/pad/PadAdapter;

.field private mParameterClientDeathListener:Lcom/android/server/audio/AudioParameterClient$ClientDeathListener;

.field public mPlaybackEndTime:J

.field public mPlaybackStartTime:J

.field private mPreferredCommunicationDevice:Landroid/media/AudioDeviceAttributes;

.field private mPrescaleAbsoluteVolume:[F

.field mReceiveNotificationDevice:I

.field private mRegisterContentName:[Ljava/lang/String;

.field private mRequesterPackageForAudioMode:Ljava/lang/String;

.field private mScoBtState:I

.field private final mSetScoCommunicationDevice:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Lcom/android/server/audio/AudioDeviceBroker$CommunicationDeviceInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mSettingAudioPowerSave:I

.field private mStartMs:J

.field private mSuperVoiceVolumeOn:Z

.field private mSuperVoiceVolumeSupported:Z

.field private mSuperVolumeOn:Z

.field private final mVolumeAttenuation:I

.field private mVolumeBoostEnabled:Z

.field private mWorkerThread:Landroid/os/HandlerThread;

.field public markBluetoothheadsetstub:Z

.field private sLastAudioStateExpTime:J

.field private sLastReportAudioErrorType:I


# direct methods
.method public static synthetic $r8$lambda$5sdfGQc0TAbQHmbJYBYrXtxEdDE(Lcom/android/server/audio/AudioServiceStubImpl;Landroid/os/IBinder;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/audio/AudioServiceStubImpl;->lambda$addAudioParameterClient$2(Landroid/os/IBinder;Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmAudioService(Lcom/android/server/audio/AudioServiceStubImpl;)Lcom/android/server/audio/AudioService;
    .locals 0

    iget-object p0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioService:Lcom/android/server/audio/AudioService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/audio/AudioServiceStubImpl;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/audio/AudioServiceStubImpl;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMqsUtils(Lcom/android/server/audio/AudioServiceStubImpl;)Lcom/android/server/audio/MQSUtils;
    .locals 0

    iget-object p0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mMqsUtils:Lcom/android/server/audio/MQSUtils;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMultiVoipPackages(Lcom/android/server/audio/AudioServiceStubImpl;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mMultiVoipPackages:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmNm(Lcom/android/server/audio/AudioServiceStubImpl;)Landroid/app/NotificationManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mNm:Landroid/app/NotificationManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmMultiVoipPackages(Lcom/android/server/audio/AudioServiceStubImpl;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mMultiVoipPackages:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$mcreateNotification(Lcom/android/server/audio/AudioServiceStubImpl;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/audio/AudioServiceStubImpl;->createNotification(Landroid/content/Context;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetDeviceToStringForStreamMusic(Lcom/android/server/audio/AudioServiceStubImpl;I)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/audio/AudioServiceStubImpl;->getDeviceToStringForStreamMusic(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mhandleAudioModeUpdate(Lcom/android/server/audio/AudioServiceStubImpl;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/audio/AudioServiceStubImpl;->handleAudioModeUpdate(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleMediaStateUpdate(Lcom/android/server/audio/AudioServiceStubImpl;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/audio/AudioServiceStubImpl;->handleMediaStateUpdate(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleMeetingModeUpdate(Lcom/android/server/audio/AudioServiceStubImpl;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/audio/AudioServiceStubImpl;->handleMeetingModeUpdate(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msendEffectRefresh(Lcom/android/server/audio/AudioServiceStubImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/audio/AudioServiceStubImpl;->sendEffectRefresh()V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetMQSSERVER_REPORT_RATE_MS()I
    .locals 1

    sget v0, Lcom/android/server/audio/AudioServiceStubImpl;->MQSSERVER_REPORT_RATE_MS:I

    return v0
.end method

.method static constructor <clinit>()V
    .locals 4

    .line 128
    nop

    .line 129
    const-string v0, "ro.vendor.audio.bt.adapter"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/server/audio/AudioServiceStubImpl;->AUDIO_BT_CONFIG_PROP:I

    .line 227
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/audio/AudioServiceStubImpl;->HIFI_NOT_SUPPORT_DEVICE_LIST:Ljava/util/List;

    .line 230
    const-string v2, "scorpio"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 231
    sget-object v0, Lcom/android/server/audio/AudioServiceStubImpl;->HIFI_NOT_SUPPORT_DEVICE_LIST:Ljava/util/List;

    const-string v2, "lithium"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 263
    nop

    .line 264
    const-string v0, "ro.vendor.audio.onetrack.upload.interval"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/server/audio/AudioServiceStubImpl;->MQSSERVER_REPORT_RATE_MS:I

    .line 328
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsAudioPlaybackTriggerSupported:Z

    .line 369
    const-string v0, "audio_karaoke_enable=0"

    const-string v1, "audio_karaoke_ktvmode=disable"

    const-string/jumbo v2, "sound_transmit_enable=0"

    filled-new-array {v2, v2, v0, v1}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/audio/AudioServiceStubImpl;->DEFAULT_AUDIO_PARAMETERS:[Ljava/lang/String;

    .line 372
    const-string v0, "audio_karaoke_enable=1"

    const-string v1, "audio_karaoke_ktvmode=enable"

    const-string/jumbo v2, "sound_transmit_enable=1"

    const-string/jumbo v3, "sound_transmit_enable=6"

    filled-new-array {v2, v3, v0, v1}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/audio/AudioServiceStubImpl;->TRANSMIT_AUDIO_PARAMETERS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 18

    .line 286
    move-object/from16 v0, p0

    invoke-direct/range {p0 .. p0}, Lcom/android/server/audio/AudioServiceStub;-><init>()V

    .line 102
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->isSupportPollAudioMicStatus:Z

    .line 114
    nop

    .line 115
    const-string v2, "ro.vendor.audio.adsp.spatial"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsAdspSpatializerAvailable:Z

    .line 117
    nop

    .line 118
    const-string v2, "ro.vendor.audio.camera.bt.record.support"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v4, "true"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    iput-boolean v2, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportedCameraRecord:Z

    .line 120
    nop

    .line 121
    const-string/jumbo v2, "vendor.audio.dolby.control.support"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    iput-boolean v2, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportedDolbyEffectControl:Z

    .line 123
    nop

    .line 124
    const-string v2, "persist.vendor.audio.power.save.setting"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mSettingAudioPowerSave:I

    .line 130
    sget v2, Lcom/android/server/audio/AudioServiceStubImpl;->AUDIO_BT_CONFIG_PROP:I

    and-int/lit8 v5, v2, 0x1

    if-eqz v5, :cond_0

    move v5, v1

    goto :goto_0

    :cond_0
    move v5, v3

    :goto_0
    iput-boolean v5, v0, Lcom/android/server/audio/AudioServiceStubImpl;->REALLY_TIME_MODE_ENABLED:Z

    .line 132
    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    move v1, v3

    :goto_1
    iput-boolean v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->SOUND_LEAK_PROTECTION_ENABLED:Z

    .line 135
    nop

    .line 136
    const-string v1, "persist.vendor.audio.cinema.support"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportCinemaMode:Z

    .line 138
    nop

    .line 139
    const-string v1, "ro.vendor.audio.fweffect"

    invoke-static {v1, v3}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportFWAudioEffectCenter:Z

    .line 144
    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->sLastAudioStateExpTime:J

    .line 145
    const/4 v5, -0x1

    iput v5, v0, Lcom/android/server/audio/AudioServiceStubImpl;->sLastReportAudioErrorType:I

    .line 146
    iput v3, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mHeadsetDeviceConnectedState:I

    .line 147
    const/16 v6, 0xa

    iput v6, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mScoBtState:I

    .line 148
    iput v3, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mA2dpDeviceConnectedState:I

    .line 151
    new-instance v6, Ljava/util/LinkedList;

    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    iput-object v6, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mCommunicationRouteClients:Ljava/util/List;

    .line 152
    new-instance v6, Ljava/util/LinkedList;

    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    iput-object v6, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mSetScoCommunicationDevice:Ljava/util/LinkedList;

    .line 163
    nop

    .line 164
    const-string v6, "ro.vendor.audio.playbackcapture.attenuation"

    const/16 v7, 0x14

    invoke-static {v6, v7}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v6

    iput v6, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mVolumeAttenuation:I

    .line 170
    iput v3, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mDelayNotifyBtStopScoCount:I

    .line 174
    iput-boolean v3, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mInDelayNotifyBtStopSco:Z

    .line 178
    const-string v6, ""

    iput-object v6, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mMultiVoipPackages:Ljava/lang/String;

    .line 181
    iput v3, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mModeOwnerPid:I

    .line 193
    new-instance v6, Landroid/media/MiuiXlog;

    invoke-direct {v6}, Landroid/media/MiuiXlog;-><init>()V

    iput-object v6, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mMiuiXlog:Landroid/media/MiuiXlog;

    .line 198
    const/4 v6, 0x5

    new-array v6, v6, [F

    fill-array-data v6, :array_0

    iput-object v6, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mPrescaleAbsoluteVolume:[F

    .line 206
    const-string/jumbo v7, "zen_mode"

    const-string v8, "notification_sound"

    const-string v9, "calendar_alert"

    const-string v10, "notes_alert"

    const-string/jumbo v11, "sms_received_sound"

    const-string/jumbo v12, "sms_received_sound_slot_1"

    const-string/jumbo v13, "sms_received_sound_slot_2"

    const-string v14, "random_note_mode_random_sound_number"

    const-string v15, "random_note_mode_sequence_sound_number"

    const-string v16, "random_note_mode_sequence_time_interval_ms"

    const-string v17, "random_note_mode_mute_time_interval_ms"

    filled-new-array/range {v7 .. v17}, [Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mRegisterContentName:[Ljava/lang/String;

    .line 221
    iput-boolean v3, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mVolumeBoostEnabled:Z

    .line 225
    const-string v6, "ro.vendor.audio.voice.super_volume"

    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    iput-boolean v4, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mSuperVoiceVolumeSupported:Z

    .line 235
    const/high16 v4, 0x4000000

    iput v4, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mReceiveNotificationDevice:I

    .line 236
    iput v3, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mNotificationTimes:I

    .line 237
    iput v5, v0, Lcom/android/server/audio/AudioServiceStubImpl;->cameraToastServiceRiid:I

    .line 271
    iput-boolean v3, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mSuperVolumeOn:Z

    .line 318
    const/4 v4, 0x0

    iput-object v4, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mNotificationDate:Ljava/time/LocalDate;

    .line 342
    iput-wide v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMs:J

    .line 343
    iput-wide v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMsTotal:J

    .line 344
    iput-wide v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mPlaybackStartTime:J

    .line 345
    iput-wide v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mPlaybackEndTime:J

    .line 348
    iput-wide v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mStartMs:J

    .line 349
    iput-wide v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mContinuousMs:J

    .line 350
    iput v3, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mHighLevel:I

    .line 353
    iput-wide v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mCumulativePlaybackStartTime:J

    .line 354
    iput-wide v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mCumulativePlaybackEndTime:J

    .line 357
    iput-boolean v3, v0, Lcom/android/server/audio/AudioServiceStubImpl;->markBluetoothheadsetstub:Z

    .line 360
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioParameterClientList:Ljava/util/List;

    .line 375
    iput-object v4, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mParameterClientDeathListener:Lcom/android/server/audio/AudioParameterClient$ClientDeathListener;

    .line 377
    iput-boolean v3, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mSuperVoiceVolumeOn:Z

    .line 287
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "AudioServiceStubImpl"

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mWorkerThread:Landroid/os/HandlerThread;

    .line 288
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 289
    new-instance v1, Lcom/android/server/audio/AudioServiceStubImpl$H;

    iget-object v2, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mWorkerThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/android/server/audio/AudioServiceStubImpl$H;-><init>(Lcom/android/server/audio/AudioServiceStubImpl;Landroid/os/Looper;)V

    iput-object v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mHandler:Landroid/os/Handler;

    .line 290
    return-void

    nop

    :array_0
    .array-data 4
        0x3f000000    # 0.5f
        0x3f333333    # 0.7f
        0x3f59999a    # 0.85f
        0x3f666666    # 0.9f
        0x3f733333    # 0.95f
    .end array-data
.end method

.method private addAudioParameterClient(Landroid/os/IBinder;Ljava/lang/String;)Lcom/android/server/audio/AudioParameterClient;
    .locals 4
    .param p1, "binder"    # Landroid/os/IBinder;
    .param p2, "targetParameter"    # Ljava/lang/String;

    .line 1292
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioParameterClientList:Ljava/util/List;

    monitor-enter v0

    .line 1293
    const/4 v1, 0x0

    :try_start_0
    invoke-direct {p0, p1, p2, v1}, Lcom/android/server/audio/AudioServiceStubImpl;->removeAudioParameterClient(Landroid/os/IBinder;Ljava/lang/String;Z)Lcom/android/server/audio/AudioParameterClient;

    move-result-object v2

    .line 1295
    .local v2, "client":Lcom/android/server/audio/AudioParameterClient;
    iget-object v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mParameterClientDeathListener:Lcom/android/server/audio/AudioParameterClient$ClientDeathListener;

    if-nez v3, :cond_0

    .line 1296
    new-instance v3, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda7;

    invoke-direct {v3, p0}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda7;-><init>(Lcom/android/server/audio/AudioServiceStubImpl;)V

    iput-object v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mParameterClientDeathListener:Lcom/android/server/audio/AudioParameterClient$ClientDeathListener;

    .line 1310
    :cond_0
    if-nez v2, :cond_1

    .line 1311
    new-instance v3, Lcom/android/server/audio/AudioParameterClient;

    invoke-direct {v3, p1, p2}, Lcom/android/server/audio/AudioParameterClient;-><init>(Landroid/os/IBinder;Ljava/lang/String;)V

    move-object v2, v3

    .line 1312
    iget-object v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mParameterClientDeathListener:Lcom/android/server/audio/AudioParameterClient$ClientDeathListener;

    invoke-virtual {v2, v3}, Lcom/android/server/audio/AudioParameterClient;->setClientDiedListener(Lcom/android/server/audio/AudioParameterClient$ClientDeathListener;)V

    .line 1313
    invoke-virtual {v2}, Lcom/android/server/audio/AudioParameterClient;->registerDeathRecipient()Z

    .line 1315
    :cond_1
    iget-object v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioParameterClientList:Ljava/util/List;

    invoke-interface {v3, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1316
    monitor-exit v0

    return-object v2

    .line 1317
    .end local v2    # "client":Lcom/android/server/audio/AudioParameterClient;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private adjustHiFiVolume(ILandroid/content/Context;)V
    .locals 2
    .param p1, "direction"    # I
    .param p2, "context"    # Landroid/content/Context;

    .line 808
    invoke-static {p2}, Lmiui/util/AudioManagerHelper;->getHiFiVolume(Landroid/content/Context;)I

    move-result v0

    .line 810
    .local v0, "currentHiFiVolume":I
    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    .line 811
    add-int/lit8 v1, v0, -0xa

    invoke-static {p2, v1}, Lmiui/util/AudioManagerHelper;->setHiFiVolume(Landroid/content/Context;I)V

    goto :goto_0

    .line 813
    :cond_0
    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    const/16 v1, 0x64

    if-ge v0, v1, :cond_1

    .line 814
    add-int/lit8 v1, v0, 0xa

    invoke-static {p2, v1}, Lmiui/util/AudioManagerHelper;->setHiFiVolume(Landroid/content/Context;I)V

    .line 817
    :cond_1
    :goto_0
    return-void
.end method

.method private checkAudioRouteForBtConnected()Z
    .locals 4

    .line 1983
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioService:Lcom/android/server/audio/AudioService;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/audio/AudioService;->getDeviceForStream(I)I

    move-result v0

    .line 1984
    .local v0, "voiceDevice":I
    sget-object v2, Landroid/media/AudioSystem;->DEVICE_OUT_ALL_SCO_SET:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1985
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "checkAudioRouteForBtConnected: route is error voiceDevice=0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1986
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1985
    const-string v3, "AudioServiceStubImpl"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1987
    return v1

    .line 1989
    :cond_0
    const/4 v1, 0x1

    return v1
.end method

.method private clearSetScoCommunicationDevice(I)V
    .locals 4
    .param p1, "skipPid"    # I

    .line 2075
    iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportedReportAudioRouteState:Z

    if-eqz v0, :cond_0

    .line 2076
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mSetScoCommunicationDevice:Ljava/util/LinkedList;

    monitor-enter v0

    .line 2077
    :try_start_0
    const-string v1, "AudioServiceStubImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "clearSetScoCommunicationDevice: skipPid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2078
    iget-object v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mSetScoCommunicationDevice:Ljava/util/LinkedList;

    new-instance v2, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda6;

    invoke-direct {v2, p1}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda6;-><init>(I)V

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->removeIf(Ljava/util/function/Predicate;)Z

    .line 2079
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 2081
    :cond_0
    :goto_0
    return-void
.end method

.method private createNotification(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .line 773
    iget v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mNotificationTimes:I

    const/4 v1, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mNotificationTimes:I

    .line 774
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 775
    .local v0, "it":Landroid/content/Intent;
    const-string v2, "com.miui.misound"

    const-string v3, "com.miui.misound.HeadsetSettingsActivity"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 777
    const/high16 v2, 0x4000000

    const/4 v3, 0x0

    invoke-static {p1, v3, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 780
    .local v2, "pit":Landroid/app/PendingIntent;
    sget-object v4, Lcom/android/internal/notification/SystemNotificationChannels;->USB:Ljava/lang/String;

    .line 781
    .local v4, "channel":Ljava/lang/String;
    const v5, 0x110f0229

    .line 782
    .local v5, "title_usb_handset":I
    const v6, 0x110f0227

    .line 783
    .local v6, "text_usb_handset":I
    new-instance v7, Landroid/app/Notification$Builder;

    invoke-direct {v7, p1, v4}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 784
    const v8, 0x10808b9

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v7

    .line 785
    const-wide/16 v8, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v7

    .line 786
    invoke-virtual {v7, v1}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v7

    .line 787
    invoke-virtual {v7, v3}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    move-result-object v3

    .line 788
    const v7, 0x106001c

    invoke-virtual {p1, v7}, Landroid/content/Context;->getColor(I)I

    move-result v7

    invoke-virtual {v3, v7}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;

    move-result-object v3

    .line 791
    const-string/jumbo v7, "sys"

    invoke-virtual {v3, v7}, Landroid/app/Notification$Builder;->setCategory(Ljava/lang/String;)Landroid/app/Notification$Builder;

    move-result-object v3

    .line 792
    invoke-virtual {v3, v1}, Landroid/app/Notification$Builder;->setVisibility(I)Landroid/app/Notification$Builder;

    move-result-object v1

    .line 793
    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v1

    .line 794
    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    .line 795
    invoke-virtual {p1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    .line 796
    .local v1, "builder":Landroid/app/Notification$Builder;
    invoke-virtual {v1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v3

    .line 797
    .local v3, "notify":Landroid/app/Notification;
    nop

    .line 804
    iget-object v7, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mNm:Landroid/app/NotificationManager;

    const v8, 0x53466666

    invoke-virtual {v7, v8, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 805
    return-void
.end method

.method private getAudioParameterClient(Landroid/os/IBinder;Ljava/lang/String;)Lcom/android/server/audio/AudioParameterClient;
    .locals 4
    .param p1, "binder"    # Landroid/os/IBinder;
    .param p2, "targetParameter"    # Ljava/lang/String;

    .line 1263
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioParameterClientList:Ljava/util/List;

    monitor-enter v0

    .line 1264
    :try_start_0
    iget-object v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioParameterClientList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v1

    .line 1265
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/audio/AudioParameterClient;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1266
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/audio/AudioParameterClient;

    .line 1267
    .local v2, "client":Lcom/android/server/audio/AudioParameterClient;
    invoke-virtual {v2}, Lcom/android/server/audio/AudioParameterClient;->getBinder()Landroid/os/IBinder;

    move-result-object v3

    if-ne v3, p1, :cond_0

    .line 1268
    invoke-virtual {v2}, Lcom/android/server/audio/AudioParameterClient;->getTargetParameter()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1269
    monitor-exit v0

    return-object v2

    .line 1271
    .end local v2    # "client":Lcom/android/server/audio/AudioParameterClient;
    :cond_0
    goto :goto_0

    .line 1272
    :cond_1
    monitor-exit v0

    const/4 v0, 0x0

    return-object v0

    .line 1273
    .end local v1    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/audio/AudioParameterClient;>;"
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getDeviceToStringForStreamMusic(I)Ljava/lang/String;
    .locals 2
    .param p1, "device"    # I

    .line 742
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getDeviceToString : device="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AudioServiceStubImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 744
    sparse-switch p1, :sswitch_data_0

    .line 756
    const-string v0, "getDeviceToString : other devices"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 759
    const-string v0, "other devices"

    return-object v0

    .line 748
    :sswitch_0
    const-string/jumbo v0, "usb.headset"

    return-object v0

    .line 754
    :sswitch_1
    const-string v0, "bluetooth"

    return-object v0

    .line 752
    :sswitch_2
    const-string/jumbo v0, "wired.headphone"

    return-object v0

    .line 750
    :sswitch_3
    const-string/jumbo v0, "wired.headset"

    return-object v0

    .line 746
    :sswitch_4
    const-string/jumbo v0, "speaker"

    return-object v0

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_4
        0x4 -> :sswitch_3
        0x8 -> :sswitch_2
        0x80 -> :sswitch_1
        0x4000000 -> :sswitch_0
    .end sparse-switch
.end method

.method private getIntPredicates(ILandroid/media/audiopolicy/AudioMix;Ljava/util/function/ToIntFunction;)[I
    .locals 2
    .param p1, "rule"    # I
    .param p2, "mix"    # Landroid/media/audiopolicy/AudioMix;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/media/audiopolicy/AudioMix;",
            "Ljava/util/function/ToIntFunction<",
            "Landroid/media/audiopolicy/AudioMixingRule$AudioMixMatchCriterion;",
            ">;)[I"
        }
    .end annotation

    .line 1194
    .local p3, "getPredicate":Ljava/util/function/ToIntFunction;, "Ljava/util/function/ToIntFunction<Landroid/media/audiopolicy/AudioMixingRule$AudioMixMatchCriterion;>;"
    invoke-virtual {p2}, Landroid/media/audiopolicy/AudioMix;->getRule()Landroid/media/audiopolicy/AudioMixingRule;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/audiopolicy/AudioMixingRule;->getCriteria()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->stream()Ljava/util/stream/Stream;

    move-result-object v0

    new-instance v1, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda5;

    invoke-direct {v1, p1}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda5;-><init>(I)V

    .line 1195
    invoke-interface {v0, v1}, Ljava/util/stream/Stream;->filter(Ljava/util/function/Predicate;)Ljava/util/stream/Stream;

    move-result-object v0

    .line 1196
    invoke-interface {v0, p3}, Ljava/util/stream/Stream;->mapToInt(Ljava/util/function/ToIntFunction;)Ljava/util/stream/IntStream;

    move-result-object v0

    .line 1197
    invoke-interface {v0}, Ljava/util/stream/IntStream;->toArray()[I

    move-result-object v0

    .line 1194
    return-object v0
.end method

.method private handleAudioModeUpdate(I)V
    .locals 1
    .param p1, "audioMode"    # I

    .line 2101
    sget-boolean v0, Lcom/android/server/audio/pad/PadAdapter;->ENABLE:Z

    if-eqz v0, :cond_0

    .line 2102
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mPadAdapter:Lcom/android/server/audio/pad/PadAdapter;

    invoke-virtual {v0, p1}, Lcom/android/server/audio/pad/PadAdapter;->handleAudioModeUpdate(I)V

    .line 2105
    :cond_0
    sget-boolean v0, Lcom/android/server/audio/foldable/FoldableAdapter;->ENABLE:Z

    if-eqz v0, :cond_1

    .line 2106
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mFoldableAdapter:Lcom/android/server/audio/foldable/FoldableAdapter;

    invoke-virtual {v0, p1}, Lcom/android/server/audio/foldable/FoldableAdapter;->onUpdateAudioMode(I)V

    .line 2108
    :cond_1
    return-void
.end method

.method private handleMediaStateUpdate(Z)V
    .locals 1
    .param p1, "mediaActive"    # Z

    .line 2111
    sget-boolean v0, Lcom/android/server/audio/foldable/FoldableAdapter;->ENABLE:Z

    if-eqz v0, :cond_0

    .line 2112
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mFoldableAdapter:Lcom/android/server/audio/foldable/FoldableAdapter;

    invoke-virtual {v0, p1}, Lcom/android/server/audio/foldable/FoldableAdapter;->onUpdateMediaState(Z)V

    .line 2114
    :cond_0
    return-void
.end method

.method private handleMeetingModeUpdate(Ljava/lang/String;)V
    .locals 1
    .param p1, "parameter"    # Ljava/lang/String;

    .line 2117
    sget-boolean v0, Lcom/android/server/audio/pad/PadAdapter;->ENABLE:Z

    if-eqz v0, :cond_0

    .line 2118
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mPadAdapter:Lcom/android/server/audio/pad/PadAdapter;

    invoke-virtual {v0, p1}, Lcom/android/server/audio/pad/PadAdapter;->handleMeetingModeUpdate(Ljava/lang/String;)V

    .line 2120
    :cond_0
    return-void
.end method

.method private handleParameters(Ljava/lang/String;)V
    .locals 11
    .param p1, "keyValuePairs"    # Ljava/lang/String;

    .line 1394
    sget-boolean v0, Lcom/android/server/audio/pad/PadAdapter;->ENABLE:Z

    if-eqz v0, :cond_0

    .line 1395
    if-eqz p1, :cond_0

    const-string v0, "remote_record_mode"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1396
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x8

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1397
    return-void

    .line 1401
    :cond_0
    const-string v0, ";"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1402
    .local v0, "kvpairs":[Ljava/lang/String;
    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_4

    aget-object v4, v0, v3

    .line 1403
    .local v4, "pair":Ljava/lang/String;
    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 1404
    .local v5, "kv":[Ljava/lang/String;
    const-string v6, "audio_sys_with_mic"

    aget-object v7, v5, v2

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    if-eqz v6, :cond_2

    .line 1405
    const-string v6, "1"

    aget-object v7, v5, v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    .line 1406
    .local v6, "enable":Z
    if-eqz v6, :cond_1

    .line 1407
    iget v7, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicVolumeDb:F

    iget v10, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mVolumeAttenuation:I

    neg-int v10, v10

    int-to-float v10, v10

    cmpl-float v7, v7, v10

    if-lez v7, :cond_3

    .line 1408
    iget-object v7, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioService:Lcom/android/server/audio/AudioService;

    .line 1409
    invoke-virtual {v7, v9}, Lcom/android/server/audio/AudioService;->getStreamMaxVolume(I)I

    move-result v7

    .line 1410
    .local v7, "musicMaxIndex":I
    iget-object v10, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioService:Lcom/android/server/audio/AudioService;

    invoke-virtual {v10, v9, v7, v8}, Lcom/android/server/audio/AudioService;->setStreamVolumeInt(III)V

    .line 1412
    .end local v7    # "musicMaxIndex":I
    goto :goto_1

    .line 1414
    :cond_1
    iget-object v7, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioService:Lcom/android/server/audio/AudioService;

    iget v10, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicVolumeIndex:I

    invoke-virtual {v7, v9, v10, v8}, Lcom/android/server/audio/AudioService;->setStreamVolumeInt(III)V

    goto :goto_1

    .line 1417
    .end local v6    # "enable":Z
    :cond_2
    const-string v6, "audio_playback_capture_for_screen"

    aget-object v10, v5, v2

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    const-string/jumbo v6, "true"

    aget-object v7, v5, v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1418
    iget-object v6, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioService:Lcom/android/server/audio/AudioService;

    invoke-virtual {v6, v9, v8}, Lcom/android/server/audio/AudioService;->getDeviceStreamVolume(II)I

    move-result v6

    iput v6, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicVolumeIndex:I

    .line 1420
    invoke-static {v9, v6, v8}, Landroid/media/AudioSystem;->getStreamVolumeDB(III)F

    move-result v6

    iput v6, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicVolumeDb:F

    goto :goto_2

    .line 1417
    :cond_3
    :goto_1
    nop

    .line 1402
    .end local v4    # "pair":Ljava/lang/String;
    .end local v5    # "kv":[Ljava/lang/String;
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1425
    :cond_4
    return-void
.end method

.method private isApplyMiuiCustom()Z
    .locals 2

    .line 924
    const-string v0, "ro.vendor.audio.skip_miui_volume_custom"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 925
    return v1

    .line 927
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public static isBluetoothHeadsetDevice(Landroid/bluetooth/BluetoothClass;)Z
    .locals 3
    .param p0, "bluetoothClass"    # Landroid/bluetooth/BluetoothClass;

    .line 684
    const/4 v0, 0x0

    if-nez p0, :cond_0

    .line 685
    return v0

    .line 687
    :cond_0
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothClass;->getDeviceClass()I

    move-result v1

    .line 688
    .local v1, "deviceClass":I
    const/16 v2, 0x418

    if-eq v1, v2, :cond_1

    const/16 v2, 0x404

    if-ne v1, v2, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method private isMusicPlayerActive(Landroid/media/AudioPlaybackConfiguration;)Z
    .locals 4
    .param p1, "apc"    # Landroid/media/AudioPlaybackConfiguration;

    .line 1154
    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 1155
    :cond_0
    invoke-virtual {p1}, Landroid/media/AudioPlaybackConfiguration;->getAudioAttributes()Landroid/media/AudioAttributes;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/AudioAttributes;->getUsage()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 1156
    invoke-virtual {p1}, Landroid/media/AudioPlaybackConfiguration;->getAudioAttributes()Landroid/media/AudioAttributes;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/AudioAttributes;->getVolumeControlStream()I

    move-result v1

    const/4 v3, 0x3

    if-ne v1, v3, :cond_2

    .line 1157
    :cond_1
    invoke-virtual {p1}, Landroid/media/AudioPlaybackConfiguration;->getPlayerState()I

    move-result v1

    const/4 v3, 0x2

    if-ne v1, v3, :cond_2

    .line 1158
    return v2

    .line 1160
    :cond_2
    return v0
.end method

.method private isMuteMusicFromMIUI(Landroid/content/ContentResolver;)Z
    .locals 3
    .param p1, "cr"    # Landroid/content/ContentResolver;

    .line 912
    const/4 v0, -0x3

    const-string v1, "mute_music_at_silent"

    const/4 v2, 0x0

    invoke-static {p1, v1, v2, v0}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    .line 914
    .local v0, "muteMusic":I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v2, v1

    :cond_0
    return v2
.end method

.method private isSetScoCommunicationDevice()Z
    .locals 6

    .line 1968
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mSetScoCommunicationDevice:Ljava/util/LinkedList;

    monitor-enter v0

    .line 1969
    const/4 v1, 0x0

    .line 1971
    .local v1, "isAppSetScoRequest":Z
    :try_start_0
    iget-object v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mSetScoCommunicationDevice:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/audio/AudioDeviceBroker$CommunicationDeviceInfo;

    .line 1972
    .local v3, "deviceInfo":Lcom/android/server/audio/AudioDeviceBroker$CommunicationDeviceInfo;
    iget v4, v3, Lcom/android/server/audio/AudioDeviceBroker$CommunicationDeviceInfo;->mPid:I

    iget v5, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mModeOwnerPid:I

    if-ne v4, v5, :cond_0

    iget-object v4, v3, Lcom/android/server/audio/AudioDeviceBroker$CommunicationDeviceInfo;->mDevice:Landroid/media/AudioDeviceAttributes;

    if-eqz v4, :cond_0

    .line 1973
    const/4 v1, 0x1

    .line 1974
    goto :goto_1

    .line 1976
    .end local v3    # "deviceInfo":Lcom/android/server/audio/AudioDeviceBroker$CommunicationDeviceInfo;
    :cond_0
    goto :goto_0

    .line 1977
    :cond_1
    :goto_1
    const-string v2, "AudioServiceStubImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isSetScoCommunicationDevice="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1978
    monitor-exit v0

    return v1

    .line 1979
    .end local v1    # "isAppSetScoRequest":Z
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private synthetic lambda$addAudioParameterClient$2(Landroid/os/IBinder;Ljava/lang/String;)V
    .locals 3
    .param p1, "diedBinder"    # Landroid/os/IBinder;
    .param p2, "diedTargetParameter"    # Ljava/lang/String;

    .line 1297
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/android/server/audio/AudioServiceStubImpl;->TRANSMIT_AUDIO_PARAMETERS:[Ljava/lang/String;

    array-length v2, v1

    if-ge v0, v2, :cond_2

    .line 1298
    aget-object v1, v1, v0

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1299
    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v1}, Lcom/android/server/audio/AudioServiceStubImpl;->removeAudioParameterClient(Landroid/os/IBinder;Ljava/lang/String;Z)Lcom/android/server/audio/AudioParameterClient;

    .line 1300
    iget-object v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioParameterClientList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1301
    iget-object v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioParameterClientList:Ljava/util/List;

    .line 1302
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/audio/AudioParameterClient;

    invoke-virtual {v1}, Lcom/android/server/audio/AudioParameterClient;->getTargetParameter()Ljava/lang/String;

    move-result-object v1

    .line 1301
    invoke-static {v1}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    goto :goto_1

    .line 1304
    :cond_0
    sget-object v1, Lcom/android/server/audio/AudioServiceStubImpl;->DEFAULT_AUDIO_PARAMETERS:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-static {v1}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 1297
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1308
    .end local v0    # "i":I
    :cond_2
    return-void
.end method

.method static synthetic lambda$clearSetScoCommunicationDevice$5(ILcom/android/server/audio/AudioDeviceBroker$CommunicationDeviceInfo;)Z
    .locals 1
    .param p0, "skipPid"    # I
    .param p1, "deviceInfo"    # Lcom/android/server/audio/AudioDeviceBroker$CommunicationDeviceInfo;

    .line 2078
    iget v0, p1, Lcom/android/server/audio/AudioDeviceBroker$CommunicationDeviceInfo;->mPid:I

    if-eq v0, p0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method static synthetic lambda$getAudioPolicyMatchUids$0(Landroid/media/audiopolicy/AudioMixingRule$AudioMixMatchCriterion;)I
    .locals 1
    .param p0, "criterion"    # Landroid/media/audiopolicy/AudioMixingRule$AudioMixMatchCriterion;

    .line 1181
    invoke-virtual {p0}, Landroid/media/audiopolicy/AudioMixingRule$AudioMixMatchCriterion;->getIntProp()I

    move-result v0

    return v0
.end method

.method static synthetic lambda$getIntPredicates$1(ILandroid/media/audiopolicy/AudioMixingRule$AudioMixMatchCriterion;)Z
    .locals 1
    .param p0, "rule"    # I
    .param p1, "criterion"    # Landroid/media/audiopolicy/AudioMixingRule$AudioMixMatchCriterion;

    .line 1195
    invoke-virtual {p1}, Landroid/media/audiopolicy/AudioMixingRule$AudioMixMatchCriterion;->getRule()I

    move-result v0

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method static synthetic lambda$handleWaveformInfoTracked$3(Landroid/os/vibrator/VibrationEffectSegment;)Ljava/lang/String;
    .locals 1
    .param p0, "seg"    # Landroid/os/vibrator/VibrationEffectSegment;

    .line 1545
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$handleWaveformInfoTracked$4(Landroid/os/vibrator/VibrationEffectSegment;)Ljava/lang/String;
    .locals 1
    .param p0, "seg"    # Landroid/os/vibrator/VibrationEffectSegment;

    .line 1553
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private needEnableVoiceVolumeBoost(IZIII)Z
    .locals 4
    .param p1, "dir"    # I
    .param p2, "isMax"    # Z
    .param p3, "device"    # I
    .param p4, "alias"    # I
    .param p5, "mode"    # I

    .line 841
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "needEnableVoiceVolumeBoost"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ismax="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " device="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " alias="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " dir="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AudioServiceStubImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 843
    const/4 v0, 0x2

    const/4 v1, 0x0

    if-eq p5, v0, :cond_0

    .line 844
    return v1

    .line 846
    :cond_0
    if-nez p4, :cond_4

    const/4 v0, 0x1

    if-ne p3, v0, :cond_4

    .line 848
    const-string v2, "ro.vendor.audio.voice.volume.boost"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "manual"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    goto :goto_0

    .line 851
    :cond_1
    if-ne p1, v0, :cond_2

    if-eqz p2, :cond_2

    .line 853
    return v0

    .line 855
    :cond_2
    const/4 v2, -0x1

    if-ne p1, v2, :cond_3

    if-eqz p2, :cond_3

    .line 857
    return v0

    .line 859
    :cond_3
    return v1

    .line 849
    :cond_4
    :goto_0
    return v1
.end method

.method private notifyBtStopBluetoothSco(Ljava/lang/String;)V
    .locals 2
    .param p1, "eventSource"    # Ljava/lang/String;

    .line 1825
    iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mInDelayNotifyBtStopSco:Z

    if-nez v0, :cond_0

    .line 1826
    return-void

    .line 1828
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/server/audio/AudioServiceStubImpl;->resetNotifyBtStopScoStatus(Ljava/lang/String;)V

    .line 1829
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mDeviceBroker:Lcom/android/server/audio/AudioDeviceBroker;

    invoke-virtual {v0}, Lcom/android/server/audio/AudioDeviceBroker;->isBluetoothScoRequested()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1830
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mBtHelper:Lcom/android/server/audio/BtHelper;

    invoke-virtual {v0, p1}, Lcom/android/server/audio/BtHelper;->stopBluetoothSco(Ljava/lang/String;)Z

    .line 1831
    const-string v0, "disconnect"

    invoke-direct {p0, v0}, Lcom/android/server/audio/AudioServiceStubImpl;->reportMultiVoipDailyUseForBtConnect(Ljava/lang/String;)V

    goto :goto_0

    .line 1833
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "don\'t need notify bt stopBluetoothSco modeOwnerPid: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mModeOwnerPid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", eventSource: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AudioServiceStubImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1835
    const-string v0, "connect"

    invoke-direct {p0, v0}, Lcom/android/server/audio/AudioServiceStubImpl;->reportMultiVoipDailyUseForBtConnect(Ljava/lang/String;)V

    .line 1837
    :goto_0
    return-void
.end method

.method private persistCumulativePlaybackStartMsToSettings()V
    .locals 4

    .line 1033
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "persistCumulativePlaybackStartMsToSettings: mCumulativePlaybackStartTime: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mCumulativePlaybackStartTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AudioServiceStubImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1035
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "key_persist_cumulative_playback_ms"

    iget-wide v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mCumulativePlaybackStartTime:J

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$Global;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    .line 1037
    return-void
.end method

.method private persistNotificationDateToSettings(Landroid/content/Context;Ljava/time/LocalDate;)V
    .locals 3
    .param p1, "cx"    # Landroid/content/Context;
    .param p2, "localDate"    # Ljava/time/LocalDate;

    .line 1040
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "persistNotificationDateToSettings: localDate: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AudioServiceStubImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1041
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1042
    invoke-virtual {p2}, Ljava/time/LocalDate;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1041
    const-string v2, "key_persist_notification_date"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1043
    return-void
.end method

.method private removeAudioParameterClient(Landroid/os/IBinder;Ljava/lang/String;Z)Lcom/android/server/audio/AudioParameterClient;
    .locals 3
    .param p1, "binder"    # Landroid/os/IBinder;
    .param p2, "targetParameter"    # Ljava/lang/String;
    .param p3, "unregister"    # Z

    .line 1278
    invoke-direct {p0, p1, p2}, Lcom/android/server/audio/AudioServiceStubImpl;->getAudioParameterClient(Landroid/os/IBinder;Ljava/lang/String;)Lcom/android/server/audio/AudioParameterClient;

    move-result-object v0

    .line 1279
    .local v0, "client":Lcom/android/server/audio/AudioParameterClient;
    iget-object v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioParameterClientList:Ljava/util/List;

    monitor-enter v1

    .line 1280
    if-eqz v0, :cond_1

    .line 1281
    :try_start_0
    iget-object v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioParameterClientList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1282
    if-eqz p3, :cond_0

    .line 1283
    invoke-virtual {v0}, Lcom/android/server/audio/AudioParameterClient;->unregisterDeathRecipient()V

    .line 1285
    :cond_0
    monitor-exit v1

    return-object v0

    .line 1287
    :cond_1
    monitor-exit v1

    const/4 v1, 0x0

    return-object v1

    .line 1288
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private reportAudioStatus(Landroid/content/Context;Ljava/util/List;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Landroid/media/AudioPlaybackConfiguration;",
            ">;)V"
        }
    .end annotation

    .line 763
    .local p2, "apcList":Ljava/util/List;, "Ljava/util/List<Landroid/media/AudioPlaybackConfiguration;>;"
    new-instance v0, Lcom/android/server/audio/MQSUtils;

    invoke-direct {v0, p1}, Lcom/android/server/audio/MQSUtils;-><init>(Landroid/content/Context;)V

    .line 764
    .local v0, "mqs":Lcom/android/server/audio/MQSUtils;
    invoke-virtual {v0, p2}, Lcom/android/server/audio/MQSUtils;->reportAudioVisualDailyUse(Ljava/util/List;)V

    .line 765
    invoke-virtual {v0}, Lcom/android/server/audio/MQSUtils;->needToReport()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 766
    invoke-virtual {v0}, Lcom/android/server/audio/MQSUtils;->reportAudioButtonStatus()V

    .line 767
    invoke-virtual {v0}, Lcom/android/server/audio/MQSUtils;->reportVibrateStatus()V

    .line 768
    invoke-virtual {p0, p1}, Lcom/android/server/audio/AudioServiceStubImpl;->reportAudioHwState(Landroid/content/Context;)V

    .line 770
    :cond_0
    return-void
.end method

.method private reportMultiVoipDailyUseForBtConnect(Ljava/lang/String;)V
    .locals 2
    .param p1, "scoState"    # Ljava/lang/String;

    .line 1848
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1849
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1850
    return-void
.end method

.method private resetNotifyBtStopScoStatus(Ljava/lang/String;)V
    .locals 2
    .param p1, "eventSource"    # Ljava/lang/String;

    .line 1840
    iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mInDelayNotifyBtStopSco:Z

    if-eqz v0, :cond_0

    .line 1841
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mDelayNotifyBtStopScoCount:I

    .line 1842
    iput-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mInDelayNotifyBtStopSco:Z

    .line 1843
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "resetNotifyBtStopScoStatus, eventSource: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AudioServiceStubImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1845
    :cond_0
    return-void
.end method

.method private sendEffectRefresh()V
    .locals 10

    .line 1497
    const-string/jumbo v0, "spatial"

    const-string/jumbo v1, "surround"

    const-string v2, "misound"

    const-string v3, "dolby"

    const-string/jumbo v4, "sendEffectRefresh"

    const-string v5, "AudioServiceStubImpl"

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1499
    :try_start_0
    iget-object v4, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/media/audiofx/AudioEffectCenter;->getInstance(Landroid/content/Context;)Landroid/media/audiofx/AudioEffectCenter;

    move-result-object v4

    .line 1500
    .local v4, "audioEffectCenter":Landroid/media/audiofx/AudioEffectCenter;
    new-instance v6, Landroid/content/Intent;

    const-string v7, "miui.intent.action.ACTION_AUDIO_EFFECT_REFRESH"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1501
    .local v6, "intent":Landroid/content/Intent;
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 1502
    .local v7, "bundle":Landroid/os/Bundle;
    const-string v8, "dolby_available"

    .line 1503
    invoke-virtual {v4, v3}, Landroid/media/audiofx/AudioEffectCenter;->isEffectAvailable(Ljava/lang/String;)Z

    move-result v9

    .line 1502
    invoke-virtual {v7, v8, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1504
    const-string v8, "dolby_active"

    .line 1505
    invoke-virtual {v4, v3}, Landroid/media/audiofx/AudioEffectCenter;->isEffectActive(Ljava/lang/String;)Z

    move-result v3

    .line 1504
    invoke-virtual {v7, v8, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1506
    const-string v3, "misound_available"

    .line 1507
    invoke-virtual {v4, v2}, Landroid/media/audiofx/AudioEffectCenter;->isEffectAvailable(Ljava/lang/String;)Z

    move-result v8

    .line 1506
    invoke-virtual {v7, v3, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1508
    const-string v3, "misound_active"

    .line 1509
    invoke-virtual {v4, v2}, Landroid/media/audiofx/AudioEffectCenter;->isEffectActive(Ljava/lang/String;)Z

    move-result v2

    .line 1508
    invoke-virtual {v7, v3, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1510
    const-string v2, "none_available"

    const-string v3, "none"

    .line 1511
    invoke-virtual {v4, v3}, Landroid/media/audiofx/AudioEffectCenter;->isEffectAvailable(Ljava/lang/String;)Z

    move-result v3

    .line 1510
    invoke-virtual {v7, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1512
    const-string/jumbo v2, "surround_available"

    .line 1513
    invoke-virtual {v4, v1}, Landroid/media/audiofx/AudioEffectCenter;->isEffectAvailable(Ljava/lang/String;)Z

    move-result v3

    .line 1512
    invoke-virtual {v7, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1514
    const-string/jumbo v2, "surround_active"

    .line 1515
    invoke-virtual {v4, v1}, Landroid/media/audiofx/AudioEffectCenter;->isEffectActive(Ljava/lang/String;)Z

    move-result v1

    .line 1514
    invoke-virtual {v7, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1516
    const-string/jumbo v1, "spatial_available"

    .line 1517
    invoke-virtual {v4, v0}, Landroid/media/audiofx/AudioEffectCenter;->isEffectAvailable(Ljava/lang/String;)Z

    move-result v2

    .line 1516
    invoke-virtual {v7, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1518
    const-string/jumbo v1, "spatial_active"

    .line 1519
    invoke-virtual {v4, v0}, Landroid/media/audiofx/AudioEffectCenter;->isEffectActive(Ljava/lang/String;)Z

    move-result v0

    .line 1518
    invoke-virtual {v7, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1520
    const-string v0, "bundle"

    invoke-virtual {v6, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1521
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1525
    .end local v4    # "audioEffectCenter":Landroid/media/audiofx/AudioEffectCenter;
    .end local v6    # "intent":Landroid/content/Intent;
    .end local v7    # "bundle":Landroid/os/Bundle;
    goto :goto_0

    .line 1522
    :catch_0
    move-exception v0

    .line 1523
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string/jumbo v1, "sendEffectRefresh error"

    invoke-static {v5, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1524
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    .line 1526
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :goto_0
    return-void
.end method

.method private sendVolumeBoostBroadcast(ZLandroid/content/Context;)V
    .locals 4
    .param p1, "boostEnabled"    # Z
    .param p2, "context"    # Landroid/content/Context;

    .line 872
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 874
    .local v0, "ident":J
    :try_start_0
    new-instance v2, Landroid/content/Intent;

    const-string v3, "miui.intent.action.VOLUME_BOOST"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 875
    .local v2, "intent":Landroid/content/Intent;
    const-string/jumbo v3, "volume_boost_state"

    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 876
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {p2, v2, v3}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 878
    .end local v2    # "intent":Landroid/content/Intent;
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 879
    nop

    .line 880
    return-void

    .line 878
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 879
    throw v2
.end method

.method private setSuperVoiceVolume(ZLandroid/content/Context;)V
    .locals 4
    .param p1, "boostEnabled"    # Z
    .param p2, "context"    # Landroid/content/Context;

    .line 882
    const-string v0, "audio"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 883
    .local v0, "am":Landroid/media/AudioManager;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SUPER_VOICE_VOLUME="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p1, :cond_0

    const-string v2, "on"

    goto :goto_0

    :cond_0
    const-string v2, "off"

    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 884
    .local v1, "params":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "params:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "AudioServiceStubImpl"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 885
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 886
    iput-boolean p1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mSuperVoiceVolumeOn:Z

    .line 887
    return-void
.end method

.method private setVolumeBoost(ZLandroid/content/Context;)V
    .locals 4
    .param p1, "boostEnabled"    # Z
    .param p2, "context"    # Landroid/content/Context;

    .line 863
    const-string v0, "audio"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 864
    .local v0, "am":Landroid/media/AudioManager;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "voice_volume_boost="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p1, :cond_0

    const-string/jumbo v2, "true"

    goto :goto_0

    :cond_0
    const-string v2, "false"

    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 865
    .local v1, "params":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "params:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "AudioServiceStubImpl"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 866
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 867
    iput-boolean p1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mVolumeBoostEnabled:Z

    .line 868
    invoke-direct {p0, p1, p2}, Lcom/android/server/audio/AudioServiceStubImpl;->sendVolumeBoostBroadcast(ZLandroid/content/Context;)V

    .line 869
    return-void
.end method

.method private shouldAdjustHiFiVolume(IIIILandroid/content/Context;)Z
    .locals 6
    .param p1, "streamType"    # I
    .param p2, "direction"    # I
    .param p3, "streamIndex"    # I
    .param p4, "maxIndex"    # I
    .param p5, "context"    # Landroid/content/Context;

    .line 821
    sget-object v0, Lcom/android/server/audio/AudioServiceStubImpl;->HIFI_NOT_SUPPORT_DEVICE_LIST:Ljava/util/List;

    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 822
    return v1

    .line 824
    :cond_0
    const/4 v0, 0x3

    if-ne p1, v0, :cond_6

    .line 825
    invoke-static {p5}, Lmiui/util/AudioManagerHelper;->isHiFiMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_2

    .line 829
    :cond_1
    invoke-static {p5}, Lmiui/util/AudioManagerHelper;->getHiFiVolume(Landroid/content/Context;)I

    move-result v0

    .line 830
    .local v0, "currentHiFiVolume":I
    move v2, p4

    .line 831
    .local v2, "maxStreamIndex":I
    const/4 v3, -0x1

    const/4 v4, 0x1

    if-ne p2, v3, :cond_2

    if-lez v0, :cond_2

    move v3, v4

    goto :goto_0

    :cond_2
    move v3, v1

    .line 833
    .local v3, "adjustDownHiFiVolume":Z
    :goto_0
    if-ne p2, v4, :cond_3

    if-ne p3, v2, :cond_3

    move v5, v4

    goto :goto_1

    :cond_3
    move v5, v1

    .line 835
    .local v5, "adjustUpHiFiVolume":Z
    :goto_1
    if-nez v3, :cond_4

    if-eqz v5, :cond_5

    :cond_4
    move v1, v4

    :cond_5
    return v1

    .line 826
    .end local v0    # "currentHiFiVolume":I
    .end local v2    # "maxStreamIndex":I
    .end local v3    # "adjustDownHiFiVolume":Z
    .end local v5    # "adjustUpHiFiVolume":Z
    :cond_6
    :goto_2
    return v1
.end method

.method private startHearingProtectionService(Landroid/content/Context;I)V
    .locals 4
    .param p1, "cx"    # Landroid/content/Context;
    .param p2, "msgId"    # I

    .line 949
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 950
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.miui.misound.hearingprotection.notification"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 951
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.miui.misound"

    const-string v3, "com.miui.misound.hearingprotection.HearingProtectionService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 953
    const-string v1, "notificationId"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 954
    invoke-virtual {p1, v0}, Landroid/content/Context;->startForegroundService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 957
    nop

    .end local v0    # "intent":Landroid/content/Intent;
    goto :goto_0

    .line 955
    :catch_0
    move-exception v0

    .line 956
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "AudioServiceStubImpl"

    const-string v2, "fail to start HearingProtectionService"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 958
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private transportDataToService(Landroid/content/Context;JJZ)Landroid/content/ComponentName;
    .locals 4
    .param p1, "cx"    # Landroid/content/Context;
    .param p2, "beginTime"    # J
    .param p4, "endTime"    # J
    .param p6, "isHigh"    # Z

    .line 963
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 964
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.miui.misound.write.data"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 965
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.miui.misound"

    const-string v3, "com.miui.misound.hearingprotection.HearingProtectionService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 967
    const-string v1, "beginMillis"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 968
    const-string v1, "endMillis"

    invoke-virtual {v0, v1, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 969
    const-string v1, "isHighPitch"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 970
    invoke-virtual {p1, v0}, Landroid/content/Context;->startForegroundService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 971
    .end local v0    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 972
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "AudioServiceStubImpl"

    const-string v2, "fail to transport data to service"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 974
    .end local v0    # "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method public adjustDefaultStreamVolumeForMiui([I)V
    .locals 1
    .param p1, "defaultStreamVolume"    # [I

    .line 918
    invoke-direct {p0}, Lcom/android/server/audio/AudioServiceStubImpl;->isApplyMiuiCustom()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 919
    invoke-static {p1}, Landroid/media/AudioServiceInjector;->adjustDefaultStreamVolume([I)V

    .line 921
    :cond_0
    return-void
.end method

.method public adjustStreamVolumeMiAudioServiceMTK(IIILjava/lang/String;Ljava/lang/String;IILjava/lang/String;ZI)V
    .locals 12
    .param p1, "streamType"    # I
    .param p2, "direction"    # I
    .param p3, "flags"    # I
    .param p4, "callingPackage"    # Ljava/lang/String;
    .param p5, "caller"    # Ljava/lang/String;
    .param p6, "uid"    # I
    .param p7, "pid"    # I
    .param p8, "attributionTag"    # Ljava/lang/String;
    .param p9, "hasModifyAudioSettings"    # Z
    .param p10, "keyEventMode"    # I

    .line 1348
    move-object v0, p0

    iget-object v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mMiAudioServiceMTK:Lcom/android/server/audio/MiAudioServiceMTK;

    move v2, p1

    move v3, p2

    move v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move-object/from16 v9, p8

    move/from16 v10, p9

    move/from16 v11, p10

    invoke-virtual/range {v1 .. v11}, Lcom/android/server/audio/MiAudioServiceMTK;->adjustStreamVolume(IIILjava/lang/String;Ljava/lang/String;IILjava/lang/String;ZI)V

    .line 1349
    return-void
.end method

.method public adjustVolume(Landroid/media/AudioPlaybackConfiguration;F)V
    .locals 3
    .param p1, "apc"    # Landroid/media/AudioPlaybackConfiguration;
    .param p2, "volume"    # F

    .line 619
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p2, v0

    if-lez v0, :cond_0

    goto :goto_0

    .line 623
    :cond_0
    const/4 v0, 0x0

    cmpg-float v0, p2, v0

    if-gez v0, :cond_1

    .line 624
    const/4 p2, 0x0

    .line 627
    :cond_1
    invoke-static {}, Lcom/android/server/audio/PlaybackActivityMonitorStub;->get()Lcom/android/server/audio/PlaybackActivityMonitorStub;

    move-result-object v0

    const-string v1, "AudioServiceStubImpl"

    iget-object v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-interface {v0, p1, p2, v1, v2}, Lcom/android/server/audio/PlaybackActivityMonitorStub;->setPlayerVolume(Landroid/media/AudioPlaybackConfiguration;FLjava/lang/String;Landroid/content/Context;)V

    .line 629
    :goto_0
    return-void
.end method

.method public createAudioRecordForLoopbackWithClient(Landroid/os/ParcelFileDescriptor;JLandroid/os/IBinder;)Landroid/os/IBinder;
    .locals 2
    .param p1, "sharedMem"    # Landroid/os/ParcelFileDescriptor;
    .param p2, "size"    # J
    .param p4, "token"    # Landroid/os/IBinder;

    .line 647
    new-instance v0, Landroid/media/MiuiAudioRecord;

    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-direct {v0, v1, p2, p3, p4}, Landroid/media/MiuiAudioRecord;-><init>(Ljava/io/FileDescriptor;JLandroid/os/IBinder;)V

    return-object v0
.end method

.method public createKeyErrorNotification(Landroid/content/Context;I)V
    .locals 16
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "notificationID"    # I

    .line 1719
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "notification"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    iput-object v2, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mNm:Landroid/app/NotificationManager;

    .line 1721
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 1722
    .local v2, "keyErrorIntent":Landroid/content/Intent;
    const-string v3, "android.server.Volume_Key_Error"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1723
    const/4 v3, 0x0

    const/high16 v4, 0x4000000

    invoke-static {v1, v3, v2, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    .line 1725
    .local v5, "keyErrorPit":Landroid/app/PendingIntent;
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 1726
    .local v6, "intent":Landroid/content/Intent;
    const-string v7, "android.server.Volume_Key_NoBlock"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1727
    invoke-static {v1, v3, v6, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 1729
    .local v4, "pit":Landroid/app/PendingIntent;
    sget-object v7, Lcom/android/internal/notification/SystemNotificationChannels;->USB_HEADSET:Ljava/lang/String;

    .line 1730
    .local v7, "channel":Ljava/lang/String;
    const v8, 0x110f0431

    .line 1732
    .local v8, "title_optimization":I
    const v9, 0x110f0430

    .line 1734
    .local v9, "text_optimization":I
    const v10, 0x110f02a7

    .line 1735
    .local v10, "button_ok":I
    const v11, 0x110f02a6

    .line 1737
    .local v11, "button_cancel":I
    new-instance v12, Landroid/app/Notification$Builder;

    invoke-direct {v12, v1, v7}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 1738
    const v13, 0x10808b9

    invoke-virtual {v12, v13}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v12

    .line 1739
    const-wide/16 v14, 0x0

    invoke-virtual {v12, v14, v15}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v12

    .line 1740
    invoke-virtual {v12, v3}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v12

    .line 1741
    const/4 v14, 0x1

    invoke-virtual {v12, v14}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v12

    .line 1742
    invoke-virtual {v12, v3}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    move-result-object v3

    .line 1743
    const v12, 0x106001c

    invoke-virtual {v1, v12}, Landroid/content/Context;->getColor(I)I

    move-result v12

    invoke-virtual {v3, v12}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;

    move-result-object v3

    .line 1745
    const-string v12, "recommendation"

    invoke-virtual {v3, v12}, Landroid/app/Notification$Builder;->setCategory(Ljava/lang/String;)Landroid/app/Notification$Builder;

    move-result-object v3

    .line 1746
    invoke-virtual {v3, v14}, Landroid/app/Notification$Builder;->setVisibility(I)Landroid/app/Notification$Builder;

    move-result-object v3

    .line 1747
    invoke-virtual {v1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v12}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    .line 1748
    invoke-virtual {v1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v12}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    .line 1750
    invoke-virtual {v1, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1749
    invoke-virtual {v3, v13, v12, v5}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v3

    .line 1752
    invoke-virtual {v1, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1751
    invoke-virtual {v3, v13, v12, v4}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v3

    .line 1753
    .local v3, "builder":Landroid/app/Notification$Builder;
    invoke-virtual {v3}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v12

    .line 1754
    .local v12, "notify":Landroid/app/Notification;
    iget-object v13, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mNm:Landroid/app/NotificationManager;

    move/from16 v14, p2

    invoke-virtual {v13, v14, v12}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 1755
    return-void
.end method

.method public createMiuiAudioRecord(Landroid/os/ParcelFileDescriptor;J)Landroid/os/IBinder;
    .locals 2
    .param p1, "sharedMem"    # Landroid/os/ParcelFileDescriptor;
    .param p2, "size"    # J

    .line 641
    new-instance v0, Landroid/media/MiuiAudioRecord;

    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-direct {v0, v1, p2, p3}, Landroid/media/MiuiAudioRecord;-><init>(Ljava/io/FileDescriptor;J)V

    return-object v0
.end method

.method public customMinStreamVolume([I)V
    .locals 0
    .param p1, "minStreamVolume"    # [I

    .line 1165
    invoke-static {p1}, Landroid/media/AudioServiceInjector;->customMinStreamVolume([I)V

    .line 1166
    return-void
.end method

.method public delayNotifyBtStopBluetoothSco(Landroid/os/Handler;Ljava/lang/String;)V
    .locals 3
    .param p1, "brokerHandler"    # Landroid/os/Handler;
    .param p2, "eventSource"    # Ljava/lang/String;

    .line 1790
    if-eqz p1, :cond_0

    .line 1791
    nop

    .line 1792
    const/16 v0, 0x73

    invoke-virtual {p1, v0, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1791
    const-wide/16 v1, 0xc8

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1795
    :cond_0
    return-void
.end method

.method public delayNotifyBtStopBluetoothScoIfNeed(ILjava/lang/String;)Z
    .locals 6
    .param p1, "pid"    # I
    .param p2, "eventSource"    # Ljava/lang/String;

    .line 1760
    const/4 v0, 0x0

    if-eqz p2, :cond_0

    const-string/jumbo v1, "setSpeakerphoneOn"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1761
    invoke-direct {p0, p2}, Lcom/android/server/audio/AudioServiceStubImpl;->resetNotifyBtStopScoStatus(Ljava/lang/String;)V

    .line 1762
    return v0

    .line 1765
    :cond_0
    iget-object v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioService:Lcom/android/server/audio/AudioService;

    invoke-virtual {v1}, Lcom/android/server/audio/AudioService;->getModeOwnerPid()I

    move-result v1

    .line 1766
    .local v1, "modeOwnerPidFromAudioService":I
    const-string v2, ", mModeOwnerPid\uff1a "

    const-string v3, "modeOwnerPidFromAudioService\uff1a"

    const-string v4, "AudioServiceStubImpl"

    if-eqz v1, :cond_6

    iget v5, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mModeOwnerPid:I

    if-nez v5, :cond_1

    goto :goto_1

    .line 1772
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mModeOwnerPid:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1774
    const/4 v2, 0x1

    if-eq v1, p1, :cond_2

    iget v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mModeOwnerPid:I

    if-ne p1, v3, :cond_2

    iget-object v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mDeviceBroker:Lcom/android/server/audio/AudioDeviceBroker;

    .line 1776
    invoke-virtual {v3, v1}, Lcom/android/server/audio/AudioDeviceBroker;->isBluetoothScoRequestForPid(I)Z

    move-result v3

    if-nez v3, :cond_3

    :cond_2
    if-ne v1, p1, :cond_4

    iget v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mModeOwnerPid:I

    if-ne v3, p1, :cond_4

    iget-object v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mDeviceBroker:Lcom/android/server/audio/AudioDeviceBroker;

    iget-object v4, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioService:Lcom/android/server/audio/AudioService;

    .line 1779
    invoke-virtual {v4, p1}, Lcom/android/server/audio/AudioService;->getNextModeOwnerPid(I)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/android/server/audio/AudioDeviceBroker;->isBluetoothScoRequestForPid(I)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    move v3, v2

    goto :goto_0

    :cond_4
    move v3, v0

    .line 1780
    .local v3, "isScoRequestExisting":Z
    :goto_0
    if-eqz v3, :cond_5

    .line 1781
    iput-boolean v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mInDelayNotifyBtStopSco:Z

    .line 1782
    return v2

    .line 1784
    :cond_5
    invoke-direct {p0, p2}, Lcom/android/server/audio/AudioServiceStubImpl;->resetNotifyBtStopScoStatus(Ljava/lang/String;)V

    .line 1785
    return v0

    .line 1767
    .end local v3    # "isScoRequestExisting":Z
    :cond_6
    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mModeOwnerPid:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1769
    invoke-direct {p0, p2}, Lcom/android/server/audio/AudioServiceStubImpl;->resetNotifyBtStopScoStatus(Ljava/lang/String;)V

    .line 1770
    return v0
.end method

.method public dumpMediaSound(Ljava/io/PrintWriter;)V
    .locals 1
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 633
    const-string v0, "  mOpenSoundAssist="

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 634
    invoke-static {}, Lcom/android/server/audio/PlaybackActivityMonitorStub;->get()Lcom/android/server/audio/PlaybackActivityMonitorStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/audio/PlaybackActivityMonitorStub;->isSoundAssistOpen()Z

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 635
    const-string v0, "  mIgnrMusicFocusReq="

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 636
    invoke-static {}, Lcom/android/server/audio/PlaybackActivityMonitorStub;->get()Lcom/android/server/audio/PlaybackActivityMonitorStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/audio/PlaybackActivityMonitorStub;->isForceIgnoreGranted()Z

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 637
    return-void
.end method

.method public enableHifiVolume(IIIILandroid/content/Context;)V
    .locals 1
    .param p1, "stream"    # I
    .param p2, "direction"    # I
    .param p3, "oldIndex"    # I
    .param p4, "indexMax"    # I
    .param p5, "context"    # Landroid/content/Context;

    .line 521
    invoke-direct/range {p0 .. p5}, Lcom/android/server/audio/AudioServiceStubImpl;->shouldAdjustHiFiVolume(IIIILandroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 522
    invoke-direct {p0, p2, p5}, Lcom/android/server/audio/AudioServiceStubImpl;->adjustHiFiVolume(ILandroid/content/Context;)V

    .line 525
    :cond_0
    return-void
.end method

.method public enableSuperIndex(III)I
    .locals 3
    .param p1, "mStreamType"    # I
    .param p2, "mIndexSuper"    # I
    .param p3, "mIndexMax"    # I

    .line 1658
    const/4 v0, -0x1

    .line 1659
    .local v0, "superIndexAdd":I
    const-string v1, "ro.vendor.audio.volume_super_index_add"

    const/4 v2, -0x1

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1661
    if-eq v0, v2, :cond_1

    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    const/4 v1, 0x5

    if-ne p1, v1, :cond_1

    .line 1665
    :cond_0
    add-int p2, p3, v0

    .line 1666
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SuperVolume: mIndexSuper is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mStreamType is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AudioServiceStubImpl"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1668
    :cond_1
    return p2
.end method

.method public enableVoiceVolumeBoost(IZIILjava/lang/String;ILandroid/content/Context;)Z
    .locals 7
    .param p1, "dir"    # I
    .param p2, "isMax"    # Z
    .param p3, "device"    # I
    .param p4, "alias"    # I
    .param p5, "pkg"    # Ljava/lang/String;
    .param p6, "mode"    # I
    .param p7, "context"    # Landroid/content/Context;

    .line 440
    const/4 v0, 0x0

    .line 441
    .local v0, "isChanged":Z
    invoke-virtual {p0, p5}, Lcom/android/server/audio/AudioServiceStubImpl;->isCtsVerifier(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 442
    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p6

    invoke-direct/range {v1 .. v6}, Lcom/android/server/audio/AudioServiceStubImpl;->needEnableVoiceVolumeBoost(IZIII)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 447
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    iget-boolean v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mVolumeBoostEnabled:Z

    if-nez v2, :cond_0

    .line 448
    invoke-direct {p0, v1, p7}, Lcom/android/server/audio/AudioServiceStubImpl;->setVolumeBoost(ZLandroid/content/Context;)V

    .line 449
    const/4 v0, 0x1

    goto :goto_0

    .line 450
    :cond_0
    const/4 v1, -0x1

    if-ne p1, v1, :cond_1

    iget-boolean v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mVolumeBoostEnabled:Z

    if-eqz v1, :cond_1

    .line 451
    const/4 v1, 0x0

    invoke-direct {p0, v1, p7}, Lcom/android/server/audio/AudioServiceStubImpl;->setVolumeBoost(ZLandroid/content/Context;)V

    .line 452
    const/4 v0, 0x1

    .line 456
    :cond_1
    :goto_0
    return v0
.end method

.method public foldDisable()V
    .locals 0

    .line 674
    invoke-static {}, Lcom/android/server/audio/FoldHelper;->disable()V

    .line 675
    return-void
.end method

.method public foldEnable()V
    .locals 0

    .line 669
    invoke-static {}, Lcom/android/server/audio/FoldHelper;->enable()V

    .line 670
    return-void
.end method

.method public foldInit()V
    .locals 0

    .line 664
    invoke-static {}, Lcom/android/server/audio/FoldHelper;->init()V

    .line 665
    return-void
.end method

.method public getAbsoluteVolumeIndex(II)I
    .locals 5
    .param p1, "index"    # I
    .param p2, "indexMax"    # I

    .line 403
    div-int/lit16 v0, p2, 0x96

    .line 404
    .local v0, "step":I
    if-nez p1, :cond_0

    .line 406
    const/4 p1, 0x0

    goto :goto_0

    .line 407
    :cond_0
    if-lez p1, :cond_1

    mul-int/lit8 v1, v0, 0x5

    if-gt p1, v1, :cond_1

    .line 409
    new-instance v1, Ljava/math/BigDecimal;

    sub-int v2, p1, v0

    div-int/2addr v2, v0

    int-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/math/BigDecimal;-><init>(D)V

    .line 410
    .local v1, "bd":Ljava/math/BigDecimal;
    invoke-virtual {v1}, Ljava/math/BigDecimal;->intValue()I

    move-result v2

    .line 411
    .local v2, "pos":I
    int-to-float v3, p2

    iget-object v4, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mPrescaleAbsoluteVolume:[F

    aget v4, v4, v2

    mul-float/2addr v3, v4

    float-to-int v3, v3

    div-int/lit8 p1, v3, 0xa

    .line 412
    .end local v1    # "bd":Ljava/math/BigDecimal;
    .end local v2    # "pos":I
    goto :goto_0

    .line 414
    :cond_1
    add-int/lit8 v1, p2, 0x5

    div-int/lit8 p1, v1, 0xa

    .line 416
    :goto_0
    return p1
.end method

.method public getAudioPolicyMatchUids(Ljava/util/HashMap;)[I
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Landroid/os/IBinder;",
            "Lcom/android/server/audio/AudioService$AudioPolicyProxy;",
            ">;)[I"
        }
    .end annotation

    .line 1171
    .local p1, "policys":Ljava/util/HashMap;, "Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/audio/AudioService$AudioPolicyProxy;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1175
    .local v0, "uidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-virtual {p1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/audio/AudioService$AudioPolicyProxy;

    .line 1176
    .local v2, "policy":Lcom/android/server/audio/AudioService$AudioPolicyProxy;
    iget-object v3, v2, Lcom/android/server/audio/AudioService$AudioPolicyProxy;->mProjection:Landroid/media/projection/IMediaProjection;

    if-nez v3, :cond_0

    .line 1177
    goto :goto_0

    .line 1179
    :cond_0
    invoke-virtual {v2}, Lcom/android/server/audio/AudioService$AudioPolicyProxy;->getMixes()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/media/audiopolicy/AudioMix;

    .line 1180
    .local v4, "mix":Landroid/media/audiopolicy/AudioMix;
    new-instance v5, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda0;

    invoke-direct {v5}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda0;-><init>()V

    const/4 v6, 0x4

    invoke-direct {p0, v6, v4, v5}, Lcom/android/server/audio/AudioServiceStubImpl;->getIntPredicates(ILandroid/media/audiopolicy/AudioMix;Ljava/util/function/ToIntFunction;)[I

    move-result-object v5

    .line 1182
    .local v5, "matchUidArray":[I
    nop

    .line 1183
    invoke-static {v5}, Ljava/util/Arrays;->stream([I)Ljava/util/stream/IntStream;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/stream/IntStream;->boxed()Ljava/util/stream/Stream;

    move-result-object v6

    .line 1184
    invoke-static {}, Ljava/util/stream/Collectors;->toList()Ljava/util/stream/Collector;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    .line 1185
    .local v6, "listCollect":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v0, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1186
    .end local v4    # "mix":Landroid/media/audiopolicy/AudioMix;
    .end local v5    # "matchUidArray":[I
    .end local v6    # "listCollect":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    goto :goto_1

    .line 1187
    .end local v2    # "policy":Lcom/android/server/audio/AudioService$AudioPolicyProxy;
    :cond_1
    goto :goto_0

    .line 1188
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->stream()Ljava/util/stream/Stream;

    move-result-object v1

    new-instance v2, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda1;

    invoke-direct {v2}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda1;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/stream/Stream;->mapToInt(Ljava/util/function/ToIntFunction;)Ljava/util/stream/IntStream;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/stream/IntStream;->toArray()[I

    move-result-object v1

    .line 1189
    .local v1, "arrays":[I
    return-object v1
.end method

.method public getBluetoothHeadset()Z
    .locals 1

    .line 709
    iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->markBluetoothheadsetstub:Z

    return v0
.end method

.method public getMiAudioService(Landroid/content/Context;)Lcom/android/server/audio/AudioService;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 1333
    new-instance v0, Lcom/android/server/audio/MiAudioService;

    invoke-direct {v0, p1}, Lcom/android/server/audio/MiAudioService;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public getModeDirectly(Lcom/android/server/audio/AudioService$SetModeDeathHandler;)I
    .locals 2
    .param p1, "currentModeHandler"    # Lcom/android/server/audio/AudioService$SetModeDeathHandler;

    .line 1641
    const-string v0, "AudioServiceStubImpl"

    const-string v1, "When ble is connected, getMode directly to avoid long waiting times."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1642
    if-eqz p1, :cond_0

    .line 1643
    invoke-virtual {p1}, Lcom/android/server/audio/AudioService$SetModeDeathHandler;->getMode()I

    move-result v0

    return v0

    .line 1645
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getMusicVolumeStep(ILjava/lang/String;I)I
    .locals 2
    .param p1, "stream"    # I
    .param p2, "callingPackage"    # Ljava/lang/String;
    .param p3, "maxVolume"    # I

    .line 393
    invoke-virtual {p0, p1, p2}, Lcom/android/server/audio/AudioServiceStubImpl;->isSupportSteplessVolume(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 394
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "adjustStreamVolume(): SupportSteplessVolume, maxVolume = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AudioServiceStubImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    div-int/lit8 v0, p3, 0xf

    return v0

    .line 397
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public getNotificationUri(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "type"    # Ljava/lang/String;

    .line 652
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioQueryWeatherService:Lcom/android/server/audio/AudioQueryWeatherService;

    invoke-virtual {v0}, Lcom/android/server/audio/AudioQueryWeatherService;->getSunriseTimeHours()I

    move-result v0

    .line 653
    .local v0, "SunriseTimeHours":I
    iget-object v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioQueryWeatherService:Lcom/android/server/audio/AudioQueryWeatherService;

    invoke-virtual {v1}, Lcom/android/server/audio/AudioQueryWeatherService;->getSunriseTimeMins()I

    move-result v1

    .line 654
    .local v1, "SunriseTimeMins":I
    iget-object v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioQueryWeatherService:Lcom/android/server/audio/AudioQueryWeatherService;

    invoke-virtual {v2}, Lcom/android/server/audio/AudioQueryWeatherService;->getSunsetTimeHours()I

    move-result v2

    .line 655
    .local v2, "SunsetTimeHours":I
    iget-object v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioQueryWeatherService:Lcom/android/server/audio/AudioQueryWeatherService;

    invoke-virtual {v3}, Lcom/android/server/audio/AudioQueryWeatherService;->getSunsetTimeMins()I

    move-result v3

    .line 656
    .local v3, "SunsetTimeMins":I
    iget-object v4, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioQueryWeatherService:Lcom/android/server/audio/AudioQueryWeatherService;

    invoke-virtual {v4}, Lcom/android/server/audio/AudioQueryWeatherService;->getDefaultTimeZoneStatus()Z

    move-result v4

    invoke-static {v4}, Landroid/media/AudioServiceInjector;->setDefaultTimeZoneStatus(Z)V

    .line 657
    invoke-static {v0, v1, v2, v3}, Landroid/media/AudioServiceInjector;->setSunriseAndSunsetTime(IIII)V

    .line 658
    iget-object v4, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/media/AudioServiceInjector;->checkSunriseAndSunsetTimeUpdate(Landroid/content/Context;)V

    .line 659
    invoke-static {p1}, Landroid/media/AudioServiceInjector;->getNotificationUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public getRingerMode(Landroid/content/Context;I)I
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mode"    # I

    .line 421
    invoke-static {p1, p2}, Lmiui/util/AudioManagerHelper;->getValidatedRingerMode(Landroid/content/Context;I)I

    move-result v0

    .line 423
    .local v0, "miuiMode":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getRingerMode originMode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " destMode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AudioServiceStubImpl"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    return v0
.end method

.method public getSuperIndex(IIILjava/util/Set;Landroid/content/Context;)I
    .locals 3
    .param p1, "mStreamType"    # I
    .param p2, "mIndexSuper"    # I
    .param p3, "mIndexMax"    # I
    .param p5, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;",
            "Landroid/content/Context;",
            ")I"
        }
    .end annotation

    .line 1699
    .local p4, "deviceSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-virtual {p5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;

    move-result-object v0

    .line 1701
    .local v0, "pkg":Ljava/lang/String;
    const/4 v1, -0x1

    if-eq p2, v1, :cond_0

    invoke-interface {p4}, Ljava/util/Set;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p4, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 1702
    invoke-virtual {p0, v0}, Lcom/android/server/audio/AudioServiceStubImpl;->isCtsVerifier(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1703
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SuperVolume: getMxIndex: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";mStreamType is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";pkg is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AudioServiceStubImpl"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1704
    return p2

    .line 1706
    :cond_0
    return p3
.end method

.method public handleLowBattery(II)V
    .locals 1
    .param p1, "batteryPct"    # I
    .param p2, "audioControlStatus"    # I

    .line 2087
    sget-boolean v0, Lcom/android/server/audio/feature/FeatureAdapter;->ENABLE:Z

    if-eqz v0, :cond_0

    .line 2088
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mFeatureAdapter:Lcom/android/server/audio/feature/FeatureAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/audio/feature/FeatureAdapter;->handleLowBattery(II)V

    .line 2090
    :cond_0
    return-void
.end method

.method public handleMicrophoneMuteChanged(Z)V
    .locals 1
    .param p1, "muted"    # Z

    .line 1615
    sget-boolean v0, Lcom/android/server/audio/pad/PadAdapter;->ENABLE:Z

    if-eqz v0, :cond_0

    .line 1616
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mPadAdapter:Lcom/android/server/audio/pad/PadAdapter;

    invoke-virtual {v0, p1}, Lcom/android/server/audio/pad/PadAdapter;->handleMicrophoneMuteChanged(Z)V

    .line 1618
    :cond_0
    return-void
.end method

.method public handleReceiveBtEventChanged(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .line 1863
    iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportedReportAudioRouteState:Z

    if-nez v0, :cond_0

    .line 1864
    return-void

    .line 1866
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1867
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.bluetooth.headset.profile.action.ACTIVE_DEVICE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1868
    const-string v1, "android.bluetooth.device.extra.DEVICE"

    const-class v2, Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    .line 1870
    .local v1, "btDevice":Landroid/bluetooth/BluetoothDevice;
    if-eqz v1, :cond_1

    const/4 v2, 0x2

    goto :goto_0

    .line 1871
    :cond_1
    const/4 v2, 0x0

    :goto_0
    iput v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mHeadsetDeviceConnectedState:I

    .line 1872
    if-eqz v1, :cond_2

    .line 1873
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mBtName:Ljava/lang/String;

    goto :goto_1

    .line 1874
    :cond_2
    iget v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mA2dpDeviceConnectedState:I

    if-nez v2, :cond_4

    .line 1875
    const-string v2, ""

    iput-object v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mBtName:Ljava/lang/String;

    goto :goto_1

    .line 1877
    .end local v1    # "btDevice":Landroid/bluetooth/BluetoothDevice;
    :cond_3
    const-string v1, "android.bluetooth.headset.profile.action.AUDIO_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1878
    const-string v1, "android.bluetooth.profile.extra.STATE"

    const/4 v2, -0x1

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mScoBtState:I

    goto :goto_2

    .line 1877
    :cond_4
    :goto_1
    nop

    .line 1880
    :goto_2
    return-void
.end method

.method public handleRecordEventUpdate(I)V
    .locals 1
    .param p1, "audioSource"    # I

    .line 1622
    sget-boolean v0, Lcom/android/server/audio/pad/PadAdapter;->ENABLE:Z

    if-eqz v0, :cond_0

    .line 1623
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mPadAdapter:Lcom/android/server/audio/pad/PadAdapter;

    invoke-virtual {v0, p1}, Lcom/android/server/audio/pad/PadAdapter;->handleRecordEventUpdate(I)V

    .line 1625
    :cond_0
    return-void
.end method

.method public handleSpeakerChanged(Landroid/content/Context;IZ)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pid"    # I
    .param p3, "speakerOn"    # Z

    .line 1247
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "handleSpeakerChanged audiomode "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AudioServiceStubImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1248
    iget v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioMode:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 1249
    :cond_0
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-static {p2, p3, v0}, Landroid/media/AudioServiceInjector;->handleSpeakerChanged(IZI)V

    .line 1251
    :cond_1
    return-void
.end method

.method public handleWaveformInfoTracked(Landroid/os/CombinedVibration;Ljava/lang/String;Landroid/os/VibrationAttributes;)V
    .locals 10
    .param p1, "effect"    # Landroid/os/CombinedVibration;
    .param p2, "opPkg"    # Ljava/lang/String;
    .param p3, "attrs"    # Landroid/os/VibrationAttributes;

    .line 1531
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mMiuiXlog:Landroid/media/MiuiXlog;

    const-string/jumbo v1, "waveform_info"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/media/MiuiXlog;->checkXlogPermission(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1532
    instance-of v0, p1, Landroid/os/CombinedVibration$Mono;

    if-eqz v0, :cond_1

    .line 1533
    move-object v0, p1

    check-cast v0, Landroid/os/CombinedVibration$Mono;

    invoke-virtual {v0}, Landroid/os/CombinedVibration$Mono;->getEffect()Landroid/os/VibrationEffect;

    move-result-object v0

    .line 1534
    .local v0, "vibEffect":Landroid/os/VibrationEffect;
    move-object v1, v0

    check-cast v1, Landroid/os/VibrationEffect$Composed;

    .line 1535
    .local v1, "composed":Landroid/os/VibrationEffect$Composed;
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v1}, Landroid/os/VibrationEffect$Composed;->getSegments()Ljava/util/List;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1536
    .local v3, "segments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/vibrator/VibrationEffectSegment;>;"
    invoke-virtual {v1}, Landroid/os/VibrationEffect$Composed;->getSegments()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/vibrator/VibrationEffectSegment;

    .line 1537
    .local v4, "segment":Landroid/os/vibrator/VibrationEffectSegment;
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 1538
    .local v5, "segmentCount":I
    instance-of v6, v4, Landroid/os/vibrator/StepSegment;

    if-eqz v6, :cond_1

    .line 1539
    new-instance v6, Lcom/android/server/audio/MQSUtils$WaveformInfo;

    invoke-direct {v6}, Lcom/android/server/audio/MQSUtils$WaveformInfo;-><init>()V

    .line 1540
    .local v6, "vibratorInfo":Lcom/android/server/audio/MQSUtils$WaveformInfo;
    iput-object p2, v6, Lcom/android/server/audio/MQSUtils$WaveformInfo;->opPkg:Ljava/lang/String;

    .line 1541
    invoke-virtual {p3}, Landroid/os/VibrationAttributes;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/android/server/audio/MQSUtils$WaveformInfo;->attrs:Ljava/lang/String;

    .line 1542
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "WaveformInfo segmentCount: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "AudioServiceStubImpl"

    invoke-static {v8, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1543
    const-string v7, ", "

    const/4 v8, 0x4

    if-gt v5, v8, :cond_0

    .line 1544
    invoke-virtual {v3}, Ljava/util/ArrayList;->stream()Ljava/util/stream/Stream;

    move-result-object v2

    new-instance v8, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda2;

    invoke-direct {v8}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda2;-><init>()V

    .line 1545
    invoke-interface {v2, v8}, Ljava/util/stream/Stream;->map(Ljava/util/function/Function;)Ljava/util/stream/Stream;

    move-result-object v2

    new-instance v8, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda3;

    invoke-direct {v8}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda3;-><init>()V

    .line 1546
    invoke-static {v8}, Ljava/util/stream/Collectors;->toCollection(Ljava/util/function/Supplier;)Ljava/util/stream/Collector;

    move-result-object v8

    invoke-interface {v2, v8}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    .line 1547
    .local v2, "segmentsToString":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {v7, v2}, Ljava/lang/String;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v7

    .line 1548
    .local v7, "result":Ljava/lang/String;
    iput-object v7, v6, Lcom/android/server/audio/MQSUtils$WaveformInfo;->effect:Ljava/lang/String;

    .line 1549
    .end local v2    # "segmentsToString":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v7    # "result":Ljava/lang/String;
    goto :goto_0

    .line 1550
    :cond_0
    new-instance v9, Ljava/util/ArrayList;

    .line 1551
    invoke-virtual {v3, v2, v8}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v2

    invoke-direct {v9, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object v2, v9

    .line 1552
    .local v2, "newSegments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/vibrator/VibrationEffectSegment;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->stream()Ljava/util/stream/Stream;

    move-result-object v8

    new-instance v9, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda4;

    invoke-direct {v9}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda4;-><init>()V

    .line 1553
    invoke-interface {v8, v9}, Ljava/util/stream/Stream;->map(Ljava/util/function/Function;)Ljava/util/stream/Stream;

    move-result-object v8

    new-instance v9, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda3;

    invoke-direct {v9}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda3;-><init>()V

    .line 1554
    invoke-static {v9}, Ljava/util/stream/Collectors;->toCollection(Ljava/util/function/Supplier;)Ljava/util/stream/Collector;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/ArrayList;

    .line 1555
    .local v8, "newSegmentsToString":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {v7, v8}, Ljava/lang/String;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v7

    .line 1556
    .local v7, "newResult":Ljava/lang/String;
    iput-object v7, v6, Lcom/android/server/audio/MQSUtils$WaveformInfo;->effect:Ljava/lang/String;

    .line 1558
    .end local v2    # "newSegments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/vibrator/VibrationEffectSegment;>;"
    .end local v7    # "newResult":Ljava/lang/String;
    .end local v8    # "newSegmentsToString":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_0
    iget-object v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mHandler:Landroid/os/Handler;

    const/4 v7, 0x3

    invoke-virtual {v2, v7, v6}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 1559
    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    .line 1563
    .end local v0    # "vibEffect":Landroid/os/VibrationEffect;
    .end local v1    # "composed":Landroid/os/VibrationEffect$Composed;
    .end local v3    # "segments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/vibrator/VibrationEffectSegment;>;"
    .end local v4    # "segment":Landroid/os/vibrator/VibrationEffectSegment;
    .end local v5    # "segmentCount":I
    .end local v6    # "vibratorInfo":Lcom/android/server/audio/MQSUtils$WaveformInfo;
    :cond_1
    return-void
.end method

.method public init(Landroid/content/Context;Lcom/android/server/audio/AudioService;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "service"    # Lcom/android/server/audio/AudioService;

    .line 1437
    const-string v0, "AudioServiceStubImpl"

    const-string v1, "initContext ..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1438
    iput-object p1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mContext:Landroid/content/Context;

    .line 1439
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioManager:Landroid/media/AudioManager;

    .line 1440
    iput-object p2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioService:Lcom/android/server/audio/AudioService;

    .line 1441
    new-instance v0, Lcom/android/server/audio/MQSUtils;

    invoke-direct {v0, p1}, Lcom/android/server/audio/MQSUtils;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mMqsUtils:Lcom/android/server/audio/MQSUtils;

    .line 1443
    new-instance v0, Lcom/android/server/audio/MiAudioServiceMTK;

    invoke-direct {v0, p1, p2}, Lcom/android/server/audio/MiAudioServiceMTK;-><init>(Landroid/content/Context;Lcom/android/server/audio/AudioService;)V

    iput-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mMiAudioServiceMTK:Lcom/android/server/audio/MiAudioServiceMTK;

    .line 1445
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mMiuiXlog:Landroid/media/MiuiXlog;

    const-string v1, "check_audio_route_for_bluetooth"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/media/MiuiXlog;->checkXlogPermission(Ljava/lang/String;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportedReportAudioRouteState:Z

    .line 1447
    sget v0, Lcom/android/server/audio/AudioServiceStubImpl;->MQSSERVER_REPORT_RATE_MS:I

    if-lez v0, :cond_0

    .line 1448
    iget-object v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    int-to-long v3, v0

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1453
    :cond_0
    sget-boolean v0, Lcom/android/server/audio/pad/PadAdapter;->ENABLE:Z

    if-eqz v0, :cond_1

    .line 1454
    new-instance v0, Lcom/android/server/audio/pad/PadAdapter;

    invoke-direct {v0, p1}, Lcom/android/server/audio/pad/PadAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mPadAdapter:Lcom/android/server/audio/pad/PadAdapter;

    .line 1457
    :cond_1
    sget-boolean v0, Lcom/android/server/audio/foldable/FoldableAdapter;->ENABLE:Z

    if-eqz v0, :cond_2

    .line 1458
    new-instance v0, Lcom/android/server/audio/foldable/FoldableAdapter;

    iget-object v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mWorkerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/android/server/audio/foldable/FoldableAdapter;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mFoldableAdapter:Lcom/android/server/audio/foldable/FoldableAdapter;

    .line 1461
    :cond_2
    sget-boolean v0, Lcom/android/server/audio/feature/FeatureAdapter;->ENABLE:Z

    if-eqz v0, :cond_3

    .line 1462
    new-instance v0, Lcom/android/server/audio/feature/FeatureAdapter;

    invoke-direct {v0, p1}, Lcom/android/server/audio/feature/FeatureAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mFeatureAdapter:Lcom/android/server/audio/feature/FeatureAdapter;

    .line 1465
    :cond_3
    iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportFWAudioEffectCenter:Z

    if-eqz v0, :cond_4

    .line 1466
    invoke-virtual {p0}, Lcom/android/server/audio/AudioServiceStubImpl;->initEffectChangeBroadcastReceiver()V

    .line 1468
    :cond_4
    return-void
.end method

.method public initAudioImplStatus(Lcom/android/server/audio/AudioService;Lcom/android/server/audio/AudioDeviceBroker;Lcom/android/server/audio/BtHelper;)V
    .locals 0
    .param p1, "service"    # Lcom/android/server/audio/AudioService;
    .param p2, "deviceBroker"    # Lcom/android/server/audio/AudioDeviceBroker;
    .param p3, "btHelper"    # Lcom/android/server/audio/BtHelper;

    .line 1430
    iput-object p1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioService:Lcom/android/server/audio/AudioService;

    .line 1431
    iput-object p2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mDeviceBroker:Lcom/android/server/audio/AudioDeviceBroker;

    .line 1432
    iput-object p3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mBtHelper:Lcom/android/server/audio/BtHelper;

    .line 1433
    return-void
.end method

.method public initEffectChangeBroadcastReceiver()V
    .locals 3

    .line 1490
    new-instance v0, Lcom/android/server/audio/AudioServiceStubImpl$EffectChangeBroadcastReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/audio/AudioServiceStubImpl$EffectChangeBroadcastReceiver;-><init>(Lcom/android/server/audio/AudioServiceStubImpl;Lcom/android/server/audio/AudioServiceStubImpl$EffectChangeBroadcastReceiver-IA;)V

    iput-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mEffectChangeBroadcastReceiver:Lcom/android/server/audio/AudioServiceStubImpl$EffectChangeBroadcastReceiver;

    .line 1491
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1492
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "miui.intent.action.ACTION_AUDIO_EFFECT_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1493
    iget-object v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mEffectChangeBroadcastReceiver:Lcom/android/server/audio/AudioServiceStubImpl$EffectChangeBroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1494
    return-void
.end method

.method public insertPlaybackMsToHealth(Landroid/content/Context;)Z
    .locals 11
    .param p1, "cx"    # Landroid/content/Context;

    .line 1003
    iget-wide v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mStartMs:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    const/4 v1, 0x1

    if-eqz v0, :cond_2

    iget-wide v6, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mContinuousMs:J

    cmp-long v0, v6, v2

    if-eqz v0, :cond_2

    .line 1004
    add-long v8, v6, v6

    .line 1005
    iget v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mHighLevel:I

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    move v10, v1

    goto :goto_0

    :cond_0
    move v10, v2

    .line 1004
    :goto_0
    move-object v4, p0

    move-object v5, p1

    invoke-direct/range {v4 .. v10}, Lcom/android/server/audio/AudioServiceStubImpl;->transportDataToService(Landroid/content/Context;JJZ)Landroid/content/ComponentName;

    move-result-object v0

    .line 1006
    .local v0, "componentName":Landroid/content/ComponentName;
    if-eqz v0, :cond_1

    .line 1007
    const-string v3, "readPlaybackMsSettings"

    invoke-virtual {p0, p1, v1, v2, v3}, Lcom/android/server/audio/AudioServiceStubImpl;->persistPlaybackMsToSettings(Landroid/content/Context;ZILjava/lang/String;)V

    .line 1009
    return v1

    .line 1011
    :cond_1
    return v2

    .line 1014
    .end local v0    # "componentName":Landroid/content/ComponentName;
    :cond_2
    return v1
.end method

.method public isAdspSpatializerAvailable()Z
    .locals 2

    .line 2124
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 2125
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;

    move-result-object v0

    .line 2126
    .local v0, "packageName":Ljava/lang/String;
    const-string v1, "com.tencent.qqmusic"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2127
    iget-boolean v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsAdspSpatializerAvailable:Z

    return v1

    .line 2130
    .end local v0    # "packageName":Ljava/lang/String;
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isAdspSpatializerEnable()Z
    .locals 2

    .line 2135
    invoke-virtual {p0}, Lcom/android/server/audio/AudioServiceStubImpl;->isAdspSpatializerAvailable()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 2136
    const-string v0, "persist.vendor.audio.dolbysurround.enable"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2137
    const/4 v0, 0x1

    return v0

    .line 2139
    :cond_0
    return v1

    .line 2142
    :cond_1
    return v1
.end method

.method public isAudioPlaybackTriggerSupported()Z
    .locals 1

    .line 933
    sget-boolean v0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsAudioPlaybackTriggerSupported:Z

    return v0
.end method

.method public isCtsVerifier(Ljava/lang/String;)Z
    .locals 2
    .param p1, "callingPackage"    # Ljava/lang/String;

    .line 892
    const/4 v0, 0x1

    if-eqz p1, :cond_1

    const-string v1, "com.android.cts"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 893
    const-string v1, "android.media.cts"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 894
    :cond_0
    return v0

    .line 897
    :cond_1
    if-eqz p1, :cond_2

    const-string v1, "com.google.android.gts"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 898
    return v0

    .line 900
    :cond_2
    if-eqz p1, :cond_3

    const-string v1, "android.media.audio.cts"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 901
    return v0

    .line 903
    :cond_3
    const/4 v0, 0x0

    return v0
.end method

.method isSoundAssistantFunction(Landroid/net/Uri;)Z
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .line 611
    const-string v0, "key_ignore_music_focus_req"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 613
    const-string/jumbo v0, "sound_assist_key"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 611
    :goto_1
    return v0
.end method

.method public isStreamVolumeInt(IZ)Z
    .locals 1
    .param p1, "mode"    # I
    .param p2, "isLeConnectedForDeviceBroker"    # Z

    .line 1636
    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    :cond_0
    if-eqz p2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSupportSteplessVolume(ILjava/lang/String;)Z
    .locals 1
    .param p1, "stream"    # I
    .param p2, "callingPackage"    # Ljava/lang/String;

    .line 389
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    invoke-virtual {p0, p2}, Lcom/android/server/audio/AudioServiceStubImpl;->isCtsVerifier(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public musicVolumeAdjustmentAllowed(IILandroid/content/ContentResolver;)Z
    .locals 1
    .param p1, "zenMode"    # I
    .param p2, "streamAlias"    # I
    .param p3, "cr"    # Landroid/content/ContentResolver;

    .line 429
    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 431
    invoke-direct {p0, p3}, Lcom/android/server/audio/AudioServiceStubImpl;->isMuteMusicFromMIUI(Landroid/content/ContentResolver;)Z

    move-result v0

    return v0

    .line 433
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public notifyBtStateToDolbyEffectController(Landroid/content/Context;Lcom/android/server/audio/AudioDeviceBroker$BtDeviceInfo;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "btInfo"    # Lcom/android/server/audio/AudioDeviceBroker$BtDeviceInfo;

    .line 1322
    iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportedDolbyEffectControl:Z

    if-eqz v0, :cond_0

    .line 1323
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1324
    .local v0, "bundle":Landroid/os/Bundle;
    iget-object v1, p2, Lcom/android/server/audio/AudioDeviceBroker$BtDeviceInfo;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    const-string v2, "device"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1325
    iget v1, p2, Lcom/android/server/audio/AudioDeviceBroker$BtDeviceInfo;->mProfile:I

    invoke-static {v1}, Landroid/bluetooth/BluetoothProfile;->getProfileName(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "profile"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1326
    iget v1, p2, Lcom/android/server/audio/AudioDeviceBroker$BtDeviceInfo;->mState:I

    invoke-static {v1}, Landroid/bluetooth/BluetoothProfile;->getConnectionStateName(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "state"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1327
    invoke-static {p1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->getInstance(Landroid/content/Context;)Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->btStateChangedFromDeviceBroker(Landroid/os/Bundle;)V

    .line 1329
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_0
    return-void
.end method

.method public notifyVolumeChangedToDolbyEffectController(Landroid/content/Context;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "stream"    # I
    .param p3, "newVolumeIndex"    # I

    .line 1712
    iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportedDolbyEffectControl:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    .line 1713
    invoke-static {p1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->getInstance(Landroid/content/Context;)Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->receiveVolumeChanged(I)V

    .line 1715
    :cond_0
    return-void
.end method

.method public onAudioServerDied()V
    .locals 2

    .line 1472
    const-string v0, "AudioServiceStubImpl"

    const-string v1, "onAudioServerDied ..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1473
    sget-boolean v0, Lcom/android/server/audio/foldable/FoldableAdapter;->ENABLE:Z

    if-eqz v0, :cond_0

    .line 1474
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mFoldableAdapter:Lcom/android/server/audio/foldable/FoldableAdapter;

    invoke-virtual {v0}, Lcom/android/server/audio/foldable/FoldableAdapter;->onAudioServerDied()V

    .line 1476
    :cond_0
    return-void
.end method

.method public onCheckAudioRoute(Ljava/lang/String;)V
    .locals 6
    .param p1, "eventSource"    # Ljava/lang/String;

    .line 1919
    iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportedReportAudioRouteState:Z

    if-nez v0, :cond_0

    .line 1920
    return-void

    .line 1922
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, "AudioServiceStubImpl"

    if-eqz v0, :cond_1

    .line 1923
    const-string v0, "onCheckAudioRoute: invalid eventSource"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1924
    return-void

    .line 1927
    :cond_1
    iget v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mA2dpDeviceConnectedState:I

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    iget v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mHeadsetDeviceConnectedState:I

    if-eq v0, v2, :cond_2

    .line 1929
    const-string v0, "onCheckAudioRoute: no active bt device"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1930
    return-void

    .line 1933
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCheckAudioRoute: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1934
    const/4 v0, 0x0

    .line 1935
    .local v0, "isReport":Z
    const/4 v2, -0x1

    .line 1937
    .local v2, "errorType":I
    iget v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mA2dpDeviceConnectedState:I

    if-nez v3, :cond_3

    .line 1938
    const/4 v2, 0x4

    .line 1939
    const/4 v0, 0x1

    .line 1941
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1942
    .local v3, "builderSource":Ljava/lang/StringBuilder;
    if-nez v0, :cond_8

    iget v4, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioMode:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_8

    .line 1943
    invoke-direct {p0}, Lcom/android/server/audio/AudioServiceStubImpl;->checkAudioRouteForBtConnected()Z

    move-result v4

    if-nez v4, :cond_8

    .line 1944
    iget-object v4, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mPreferredCommunicationDevice:Landroid/media/AudioDeviceAttributes;

    if-eqz v4, :cond_4

    .line 1945
    invoke-virtual {v4}, Landroid/media/AudioDeviceAttributes;->getType()I

    move-result v4

    const/4 v5, 0x7

    if-ne v4, v5, :cond_4

    const/4 v4, 0x1

    goto :goto_0

    :cond_4
    const/4 v4, 0x0

    .line 1947
    .local v4, "audioBaseScoState":Z
    :goto_0
    if-eqz v4, :cond_5

    .line 1948
    const/4 v2, 0x7

    goto :goto_1

    .line 1949
    :cond_5
    invoke-direct {p0}, Lcom/android/server/audio/AudioServiceStubImpl;->isSetScoCommunicationDevice()Z

    move-result v5

    if-nez v5, :cond_6

    .line 1950
    const/4 v2, 0x3

    .line 1951
    const-string v5, " app don\'t request sco"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1952
    :cond_6
    iget-object v5, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mDeviceBroker:Lcom/android/server/audio/AudioDeviceBroker;

    invoke-virtual {v5}, Lcom/android/server/audio/AudioDeviceBroker;->isBluetoothScoRequested()Z

    move-result v5

    if-nez v5, :cond_7

    .line 1953
    const/4 v2, 0x3

    .line 1954
    const-string v5, " app request sco, but stop sco"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1956
    :cond_7
    const/4 v2, 0x0

    .line 1958
    :goto_1
    const/4 v0, 0x1

    .line 1960
    .end local v4    # "audioBaseScoState":Z
    :cond_8
    if-eqz v0, :cond_9

    .line 1961
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[TF-BT] onCheckAudioRoute: errorType="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1962
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, ""

    const-string v5, "maybeError"

    invoke-virtual {p0, v2, v1, v4, v5}, Lcom/android/server/audio/AudioServiceStubImpl;->reportAbnormalAudioStatus(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1965
    :cond_9
    return-void
.end method

.method public onCheckMusicPlaybackContinuous(Landroid/content/Context;IZLjava/util/Set;)Z
    .locals 21
    .param p1, "cx"    # Landroid/content/Context;
    .param p2, "device"    # I
    .param p3, "isHigh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "IZ",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    .line 1049
    .local p4, "safeMediaVolumeDevices":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move/from16 v9, p2

    move/from16 v10, p3

    invoke-virtual/range {p0 .. p0}, Lcom/android/server/audio/AudioServiceStubImpl;->getBluetoothHeadset()Z

    move-result v11

    .line 1050
    .local v11, "isBluetoothHeadset":Z
    const/4 v12, 0x0

    .line 1051
    .local v12, "needQueue":Z
    const/16 v0, 0x80

    const/4 v14, 0x0

    if-eq v9, v0, :cond_0

    const/16 v0, 0x100

    if-ne v9, v0, :cond_1

    :cond_0
    if-nez v11, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v14

    :goto_0
    move v15, v0

    .line 1052
    .local v15, "isBluetoothspeaker":Z
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    move-object/from16 v6, p4

    invoke-interface {v6, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    const-wide/16 v4, 0x0

    const-string v1, "AudioServiceStubImpl"

    if-eqz v0, :cond_7

    if-eqz v15, :cond_2

    move-wide v13, v4

    goto/16 :goto_2

    .line 1064
    :cond_2
    const/4 v0, 0x3

    invoke-static {v0, v14}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    move-result v2

    const-wide/32 v16, 0xea60

    if-eqz v2, :cond_5

    .line 1065
    iget-wide v2, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMs:J

    add-long v2, v2, v16

    iput-wide v2, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMs:J

    .line 1066
    iget-wide v2, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMsTotal:J

    add-long v2, v2, v16

    iput-wide v2, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMsTotal:J

    .line 1067
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "music isActive ,start loop ="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMs:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "\uff0cMUSIC_ACTIVE_CONTINUOUS_MS_MAX = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v2, 0x36ee80

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1069
    iget-wide v2, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMs:J

    const-wide/32 v18, 0x36ee80

    cmp-long v0, v2, v18

    if-ltz v0, :cond_3

    .line 1070
    const-string v0, "music isActive max ,post warning dialog"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1071
    iput-wide v4, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMs:J

    .line 1072
    invoke-static {}, Ljava/time/LocalDate;->now()Ljava/time/LocalDate;

    move-result-object v0

    invoke-direct {v7, v8, v0}, Lcom/android/server/audio/AudioServiceStubImpl;->persistNotificationDateToSettings(Landroid/content/Context;Ljava/time/LocalDate;)V

    .line 1076
    :cond_3
    iget-wide v2, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMsTotal:J

    cmp-long v0, v2, v16

    if-ltz v0, :cond_4

    .line 1077
    const-string v0, "need to report hearing data"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1078
    iget-wide v2, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mPlaybackStartTime:J

    iget-wide v0, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMsTotal:J

    add-long v16, v2, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide v13, v4

    move-wide/from16 v4, v16

    move/from16 v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/android/server/audio/AudioServiceStubImpl;->transportDataToService(Landroid/content/Context;JJZ)Landroid/content/ComponentName;

    .line 1080
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mPlaybackStartTime:J

    .line 1081
    iput-wide v13, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMsTotal:J

    .line 1083
    :cond_4
    nop

    .line 1084
    nop

    .line 1083
    const-string v0, "music isActive ,start loop"

    const/4 v1, 0x0

    invoke-virtual {v7, v8, v1, v10, v0}, Lcom/android/server/audio/AudioServiceStubImpl;->persistPlaybackMsToSettings(Landroid/content/Context;ZILjava/lang/String;)V

    .line 1085
    const/4 v0, 0x1

    return v0

    .line 1087
    :cond_5
    move-wide v13, v4

    const/16 v2, 0x7530

    invoke-static {v0, v2}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    move-result v20

    .line 1089
    .local v20, "isRencentActive":Z
    if-eqz v20, :cond_6

    .line 1090
    iget-wide v2, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMs:J

    add-long v2, v2, v16

    iput-wide v2, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMs:J

    .line 1091
    iget-wide v2, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMsTotal:J

    add-long v2, v2, v16

    iput-wide v2, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMsTotal:J

    .line 1092
    const-string v0, "isRencentActive true,need retry again"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1093
    const-string v0, "isRencentActive true"

    const/4 v1, 0x0

    invoke-virtual {v7, v8, v1, v10, v0}, Lcom/android/server/audio/AudioServiceStubImpl;->persistPlaybackMsToSettings(Landroid/content/Context;ZILjava/lang/String;)V

    .line 1095
    const/4 v0, 0x1

    .end local v12    # "needQueue":Z
    .local v0, "needQueue":Z
    goto :goto_1

    .line 1097
    .end local v0    # "needQueue":Z
    .restart local v12    # "needQueue":Z
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isRencentActive false,reset time "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMs:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1099
    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Lcom/android/server/audio/AudioServiceStubImpl;->updatePlaybackTime(Z)V

    .line 1100
    iget-wide v2, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mPlaybackStartTime:J

    iget-wide v0, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMsTotal:J

    add-long v4, v2, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/android/server/audio/AudioServiceStubImpl;->transportDataToService(Landroid/content/Context;JJZ)Landroid/content/ComponentName;

    .line 1102
    const-string v0, "isRencentActive false"

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v7, v8, v1, v2, v0}, Lcom/android/server/audio/AudioServiceStubImpl;->persistPlaybackMsToSettings(Landroid/content/Context;ZILjava/lang/String;)V

    .line 1104
    iput-wide v13, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMs:J

    .line 1105
    iput-wide v13, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMsTotal:J

    .line 1106
    const/4 v0, 0x0

    .line 1108
    .end local v12    # "needQueue":Z
    .restart local v0    # "needQueue":Z
    :goto_1
    return v0

    .line 1052
    .end local v0    # "needQueue":Z
    .end local v20    # "isRencentActive":Z
    .restart local v12    # "needQueue":Z
    :cond_7
    move-wide v13, v4

    .line 1054
    :goto_2
    const-string v0, "device is not support,reset time calculation "

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1055
    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Lcom/android/server/audio/AudioServiceStubImpl;->updatePlaybackTime(Z)V

    .line 1056
    iget-wide v2, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mPlaybackStartTime:J

    iget-wide v0, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMsTotal:J

    add-long v4, v2, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/android/server/audio/AudioServiceStubImpl;->transportDataToService(Landroid/content/Context;JJZ)Landroid/content/ComponentName;

    .line 1058
    const-string v0, "device is not support"

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v7, v8, v1, v2, v0}, Lcom/android/server/audio/AudioServiceStubImpl;->persistPlaybackMsToSettings(Landroid/content/Context;ZILjava/lang/String;)V

    .line 1060
    iput-wide v13, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMs:J

    .line 1061
    iput-wide v13, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMsTotal:J

    .line 1062
    return v12
.end method

.method public onNotifyBtStopBluetoothSco(Landroid/os/Handler;Ljava/lang/String;)V
    .locals 5
    .param p1, "brokerHandler"    # Landroid/os/Handler;
    .param p2, "eventSource"    # Ljava/lang/String;

    .line 1807
    iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mInDelayNotifyBtStopSco:Z

    const-string v1, "AudioServiceStubImpl"

    if-nez v0, :cond_0

    .line 1808
    const-string v0, "onNotifyBtStopBluetoothSco: ignore current request"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1810
    :cond_0
    iget v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mDelayNotifyBtStopScoCount:I

    const/4 v2, 0x1

    add-int/2addr v0, v2

    iput v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mDelayNotifyBtStopScoCount:I

    .line 1811
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioService:Lcom/android/server/audio/AudioService;

    iget v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mModeOwnerPid:I

    invoke-virtual {v0, v3}, Lcom/android/server/audio/AudioService;->getNextModeOwnerPid(I)I

    move-result v0

    .line 1812
    .local v0, "nextPidFromAudioService":I
    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mDeviceBroker:Lcom/android/server/audio/AudioDeviceBroker;

    .line 1813
    invoke-virtual {v3, v0}, Lcom/android/server/audio/AudioDeviceBroker;->isBluetoothScoRequestForPid(I)Z

    move-result v3

    if-eqz v3, :cond_1

    iget v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mModeOwnerPid:I

    if-eq v0, v3, :cond_1

    iget v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mDelayNotifyBtStopScoCount:I

    const/4 v4, 0x3

    if-gt v3, v4, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    .line 1816
    .local v2, "isContinueDelayNotify":Z
    :goto_0
    if-eqz v2, :cond_2

    .line 1817
    invoke-virtual {p0, p1, p2}, Lcom/android/server/audio/AudioServiceStubImpl;->delayNotifyBtStopBluetoothSco(Landroid/os/Handler;Ljava/lang/String;)V

    .line 1818
    const-string v3, "onNotifyBtStopBluetoothSco: continue delay notify bt stopBluetoothSco"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1820
    :cond_2
    invoke-direct {p0, p2}, Lcom/android/server/audio/AudioServiceStubImpl;->notifyBtStopBluetoothSco(Ljava/lang/String;)V

    .line 1822
    :goto_1
    return-void
.end method

.method public onPlayerTracked(Landroid/media/AudioPlaybackConfiguration;)V
    .locals 1
    .param p1, "apc"    # Landroid/media/AudioPlaybackConfiguration;

    .line 2094
    sget-boolean v0, Lcom/android/server/audio/feature/FeatureAdapter;->ENABLE:Z

    if-eqz v0, :cond_0

    .line 2095
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mFeatureAdapter:Lcom/android/server/audio/feature/FeatureAdapter;

    invoke-virtual {v0, p1}, Lcom/android/server/audio/feature/FeatureAdapter;->onPlayerTracked(Landroid/media/AudioPlaybackConfiguration;)V

    .line 2097
    :cond_0
    return-void
.end method

.method public onRotationUpdateMiAudioServiceMTK(Ljava/lang/Integer;)V
    .locals 1
    .param p1, "rotation"    # Ljava/lang/Integer;

    .line 1342
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mMiAudioServiceMTK:Lcom/android/server/audio/MiAudioServiceMTK;

    invoke-virtual {v0, p1}, Lcom/android/server/audio/MiAudioServiceMTK;->onRotationUpdate(Ljava/lang/Integer;)V

    .line 1343
    return-void
.end method

.method public onSetCommunicationDeviceForClient(Lcom/android/server/audio/AudioDeviceBroker$CommunicationDeviceInfo;)V
    .locals 5
    .param p1, "deviceInfo"    # Lcom/android/server/audio/AudioDeviceBroker$CommunicationDeviceInfo;

    .line 2052
    iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportedReportAudioRouteState:Z

    if-eqz v0, :cond_4

    .line 2053
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mSetScoCommunicationDevice:Ljava/util/LinkedList;

    monitor-enter v0

    .line 2054
    if-eqz p1, :cond_3

    :try_start_0
    iget-object v1, p1, Lcom/android/server/audio/AudioDeviceBroker$CommunicationDeviceInfo;->mDevice:Landroid/media/AudioDeviceAttributes;

    if-nez v1, :cond_0

    goto :goto_0

    .line 2057
    :cond_0
    iget-object v1, p1, Lcom/android/server/audio/AudioDeviceBroker$CommunicationDeviceInfo;->mDevice:Landroid/media/AudioDeviceAttributes;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/android/server/audio/AudioDeviceBroker$CommunicationDeviceInfo;->mDevice:Landroid/media/AudioDeviceAttributes;

    .line 2058
    invoke-virtual {v1}, Landroid/media/AudioDeviceAttributes;->getType()I

    move-result v1

    const/4 v2, 0x7

    if-ne v1, v2, :cond_2

    .line 2059
    iget-object v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mSetScoCommunicationDevice:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    .line 2060
    .local v1, "scoDeviceNum":I
    const/16 v2, 0xa

    if-le v1, v2, :cond_1

    .line 2061
    iget-object v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mSetScoCommunicationDevice:Ljava/util/LinkedList;

    div-int/lit8 v3, v1, 0x2

    const/4 v4, 0x0

    invoke-virtual {v2, v4, v3}, Ljava/util/LinkedList;->subList(II)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 2063
    :cond_1
    const-string v2, "AudioServiceStubImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onSetCommunicationDeviceForClient: add deviceInfo="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2064
    iget-object v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mSetScoCommunicationDevice:Ljava/util/LinkedList;

    invoke-virtual {v2, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 2066
    .end local v1    # "scoDeviceNum":I
    :cond_2
    monitor-exit v0

    goto :goto_1

    .line 2055
    :cond_3
    :goto_0
    monitor-exit v0

    return-void

    .line 2066
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 2068
    :cond_4
    :goto_1
    return-void
.end method

.method public onShowHearingProtectionNotification(Landroid/content/Context;I)V
    .locals 1
    .param p1, "cx"    # Landroid/content/Context;
    .param p2, "msgId"    # I

    .line 939
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 941
    return-void

    .line 944
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/android/server/audio/AudioServiceStubImpl;->startHearingProtectionService(Landroid/content/Context;I)V

    .line 945
    return-void
.end method

.method public onSystemReadyMiAudioServiceMTK()V
    .locals 1

    .line 1338
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mMiAudioServiceMTK:Lcom/android/server/audio/MiAudioServiceMTK;

    invoke-virtual {v0}, Lcom/android/server/audio/MiAudioServiceMTK;->onSystemReady()V

    .line 1339
    return-void
.end method

.method public onTrigger(Ljava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/media/AudioPlaybackConfiguration;",
            ">;)Z"
        }
    .end annotation

    .line 1140
    .local p1, "configs":Ljava/util/List;, "Ljava/util/List<Landroid/media/AudioPlaybackConfiguration;>;"
    if-nez p1, :cond_0

    .line 1141
    const/4 v0, 0x0

    return v0

    .line 1143
    :cond_0
    const/4 v0, 0x0

    .line 1144
    .local v0, "isMusicActive":Z
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/AudioPlaybackConfiguration;

    .line 1145
    .local v2, "config":Landroid/media/AudioPlaybackConfiguration;
    invoke-direct {p0, v2}, Lcom/android/server/audio/AudioServiceStubImpl;->isMusicPlayerActive(Landroid/media/AudioPlaybackConfiguration;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1146
    const/4 v0, 0x1

    .line 1147
    goto :goto_1

    .line 1149
    .end local v2    # "config":Landroid/media/AudioPlaybackConfiguration;
    :cond_1
    goto :goto_0

    .line 1150
    :cond_2
    :goto_1
    return v0
.end method

.method public onUpdateAudioMode(ILjava/lang/String;Landroid/content/Context;)V
    .locals 3
    .param p1, "audioMode"    # I
    .param p2, "requesterPackage"    # Ljava/lang/String;
    .param p3, "context"    # Landroid/content/Context;

    .line 498
    iput p1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioMode:I

    .line 499
    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    goto :goto_0

    .line 502
    :cond_0
    if-nez p1, :cond_2

    .line 504
    invoke-static {}, Lmiui/tipclose/TipHelperProxy;->getInstance()Lmiui/tipclose/TipHelperProxy;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/tipclose/TipHelperProxy;->hideTipForPhone()V

    goto :goto_1

    .line 501
    :cond_1
    :goto_0
    invoke-static {}, Lmiui/tipclose/TipHelperProxy;->getInstance()Lmiui/tipclose/TipHelperProxy;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p2}, Lmiui/tipclose/TipHelperProxy;->showTipForPhone(ZLjava/lang/String;)V

    .line 506
    :cond_2
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onUpdateAudioMode audiomode "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " package:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AudioServiceStubImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    iput-object p2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mRequesterPackageForAudioMode:Ljava/lang/String;

    .line 509
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 510
    return-void
.end method

.method public onUpdateMediaState(Z)V
    .locals 3
    .param p1, "mediaActive"    # Z

    .line 514
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onUpdateMediaState mediaActive="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AudioServiceStubImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 515
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x9

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 516
    return-void
.end method

.method public persistPlaybackMsToSettings(Landroid/content/Context;ZILjava/lang/String;)V
    .locals 6
    .param p1, "cx"    # Landroid/content/Context;
    .param p2, "isNeedClearData"    # Z
    .param p3, "highLevel"    # I
    .param p4, "from"    # Ljava/lang/String;

    .line 1019
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "persistPlaybackMsToSettings: isNeedClearData: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mPlaybackStartTime: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mPlaybackStartTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mMusicPlaybackContinuousMsTotal: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMsTotal:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " highLevel: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " from: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AudioServiceStubImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1023
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1024
    const-wide/16 v1, 0x0

    if-eqz p2, :cond_0

    move-wide v3, v1

    goto :goto_0

    :cond_0
    iget-wide v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mPlaybackStartTime:J

    .line 1023
    :goto_0
    const-string v5, "key_persist_playback_start_ms"

    invoke-static {v0, v5, v3, v4}, Landroid/provider/Settings$Global;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    .line 1025
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1027
    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    iget-wide v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMsTotal:J

    .line 1025
    :goto_1
    const-string v3, "key_persist_playback_continuous_ms"

    invoke-static {v0, v3, v1, v2}, Landroid/provider/Settings$Global;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    .line 1028
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "key_persist_playback_high_voice"

    invoke-static {v0, v1, p3}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1030
    return-void
.end method

.method public reSetAudioCinemaModeThermal()V
    .locals 1

    .line 1387
    iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportCinemaMode:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioThermalObserver:Lcom/android/server/audio/AudioThermalObserver;

    if-eqz v0, :cond_0

    .line 1388
    invoke-virtual {v0}, Lcom/android/server/audio/AudioThermalObserver;->reSetAudioCinemaModeThermal()V

    .line 1390
    :cond_0
    return-void
.end method

.method public reSetAudioParam()V
    .locals 1

    .line 1373
    iget v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mSettingAudioPowerSave:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioPowerSaveModeObserver:Lcom/android/server/audio/AudioPowerSaveModeObserver;

    if-eqz v0, :cond_0

    .line 1374
    invoke-virtual {v0}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->reSetAudioPowerParam()V

    .line 1376
    :cond_0
    return-void
.end method

.method public readPlaybackMsSettings(Landroid/content/Context;)V
    .locals 5
    .param p1, "cx"    # Landroid/content/Context;

    .line 978
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "key_persist_playback_start_ms"

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$Global;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mStartMs:J

    .line 980
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "key_persist_playback_continuous_ms"

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$Global;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mContinuousMs:J

    .line 982
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "key_persist_playback_high_voice"

    const/4 v4, 0x0

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mHighLevel:I

    .line 985
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "key_persist_cumulative_playback_ms"

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$Global;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mCumulativePlaybackStartTime:J

    .line 988
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "key_persist_notification_date"

    invoke-static {v0, v1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 990
    .local v0, "dateTime":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 991
    nop

    .line 992
    const-string/jumbo v1, "yyyy-MM-dd"

    invoke-static {v1}, Ljava/time/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Ljava/time/format/DateTimeFormatter;

    move-result-object v1

    .line 991
    invoke-static {v0, v1}, Ljava/time/LocalDate;->parse(Ljava/lang/CharSequence;Ljava/time/format/DateTimeFormatter;)Ljava/time/LocalDate;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mNotificationDate:Ljava/time/LocalDate;

    .line 994
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "readPlaybackMsSettings: startMs: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mStartMs:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " continuousMs: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mContinuousMs:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " highLevel: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mHighLevel:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mNotificationDate="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mNotificationDate:Ljava/time/LocalDate;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mCumulativePlaybackStartTime: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mCumulativePlaybackStartTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AudioServiceStubImpl"

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 998
    return-void
.end method

.method public realTimeModeEnabled()Z
    .locals 2

    .line 294
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 295
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;

    move-result-object v0

    .line 296
    .local v0, "packageName":Ljava/lang/String;
    const-string v1, "android.media.audio.cts"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 297
    iget-boolean v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->REALLY_TIME_MODE_ENABLED:Z

    return v1

    .line 300
    .end local v0    # "packageName":Ljava/lang/String;
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public registerContentObserverForMiui(Landroid/content/ContentResolver;ZLandroid/database/ContentObserver;I)V
    .locals 3
    .param p1, "contentResolver"    # Landroid/content/ContentResolver;
    .param p2, "notifyForDescendents"    # Z
    .param p3, "observer"    # Landroid/database/ContentObserver;
    .param p4, "userAll"    # I

    .line 576
    nop

    .line 577
    nop

    .line 578
    const-string v0, "mute_music_at_silent"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 577
    invoke-virtual {p1, v0, p2, p3, p4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 585
    const-string/jumbo v0, "sound_assist_key"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0, p2, p3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 587
    const-string v0, "key_ignore_music_focus_req"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0, p2, p3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 590
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mRegisterContentName:[Ljava/lang/String;

    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 591
    aget-object v1, v1, v0

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1, v1, p2, p3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 590
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 594
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method public reportAbnormalAudioStatus(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 17
    .param p1, "errorType"    # I
    .param p2, "eventSource"    # Ljava/lang/String;
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "audioState"    # Ljava/lang/String;

    .line 1995
    move-object/from16 v0, p0

    move/from16 v9, p1

    move-object/from16 v10, p2

    iget-boolean v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportedReportAudioRouteState:Z

    const-string v2, "AudioServiceStubImpl"

    if-eqz v1, :cond_8

    .line 1996
    const/4 v1, -0x1

    if-ne v9, v1, :cond_0

    .line 1997
    const-string v1, "reportAudioStatus\uff1a audio route is normal"

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1998
    return-void

    .line 2000
    :cond_0
    const-string v1, "ok"

    const/4 v3, 0x1

    move-object/from16 v11, p4

    if-ne v11, v1, :cond_1

    iget-object v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mMiuiXlog:Landroid/media/MiuiXlog;

    const-string v4, "check_audio_route_for_bluetooth"

    invoke-virtual {v1, v4, v3}, Landroid/media/MiuiXlog;->checkXlogPermission(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2002
    const-string v1, "reportAbnormalAudioStatus: TR6_XLOG_DISABLE"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2003
    return-void

    .line 2005
    :cond_1
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2006
    const-string v1, "reportAudioStatus\uff1a eventSource is empty"

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2007
    return-void

    .line 2010
    :cond_2
    const/4 v1, 0x2

    if-eq v9, v3, :cond_3

    if-eq v9, v1, :cond_3

    iget v3, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mA2dpDeviceConnectedState:I

    if-nez v3, :cond_3

    iget v3, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mHeadsetDeviceConnectedState:I

    if-nez v3, :cond_3

    .line 2014
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "reportAudioStatus\uff1a no active bt device\uff0cerrorType="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2015
    return-void

    .line 2018
    :cond_3
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget v3, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioMode:I

    if-eq v3, v1, :cond_4

    const/4 v1, 0x3

    if-ne v3, v1, :cond_5

    .line 2021
    :cond_4
    iget-object v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mRequesterPackageForAudioMode:Ljava/lang/String;

    move-object v12, v1

    .end local p3    # "packageName":Ljava/lang/String;
    .local v1, "packageName":Ljava/lang/String;
    goto :goto_0

    .line 2022
    .end local v1    # "packageName":Ljava/lang/String;
    .restart local p3    # "packageName":Ljava/lang/String;
    :cond_5
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_6

    .line 2023
    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    move-object v12, v1

    .end local p3    # "packageName":Ljava/lang/String;
    .restart local v1    # "packageName":Ljava/lang/String;
    goto :goto_0

    .line 2025
    .end local v1    # "packageName":Ljava/lang/String;
    .restart local p3    # "packageName":Ljava/lang/String;
    :cond_6
    move-object/from16 v12, p3

    .end local p3    # "packageName":Ljava/lang/String;
    .local v12, "packageName":Ljava/lang/String;
    :goto_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v13

    .line 2026
    .local v13, "currentTime":J
    iget-wide v3, v0, Lcom/android/server/audio/AudioServiceStubImpl;->sLastAudioStateExpTime:J

    sub-long v3, v13, v3

    const-wide/16 v5, 0x1770

    cmp-long v1, v3, v5

    if-gez v1, :cond_7

    iget v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->sLastReportAudioErrorType:I

    if-ne v9, v1, :cond_7

    .line 2028
    iput-wide v13, v0, Lcom/android/server/audio/AudioServiceStubImpl;->sLastAudioStateExpTime:J

    .line 2029
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "reportAudioStatus: the upload interval has not exceeded 6000 s, errorType="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " eventSource="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " packageName="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2032
    return-void

    .line 2034
    :cond_7
    iput v9, v0, Lcom/android/server/audio/AudioServiceStubImpl;->sLastReportAudioErrorType:I

    .line 2035
    iput-wide v13, v0, Lcom/android/server/audio/AudioServiceStubImpl;->sLastAudioStateExpTime:J

    .line 2036
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " modePid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mModeOwnerPid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " routeClients="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mCommunicationRouteClients:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mScoBtState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mScoBtState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 2040
    .local v15, "audioEventSource":Ljava/lang/String;
    new-instance v16, Lcom/android/server/audio/MQSUtils$AudioStateTrackData;

    iget-object v5, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mBtName:Ljava/lang/String;

    iget v6, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mA2dpDeviceConnectedState:I

    iget v7, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mHeadsetDeviceConnectedState:I

    move-object/from16 v1, v16

    move/from16 v2, p1

    move-object v3, v15

    move-object v4, v12

    move-object/from16 v8, p4

    invoke-direct/range {v1 .. v8}, Lcom/android/server/audio/MQSUtils$AudioStateTrackData;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    .line 2043
    .local v1, "audioTrackData":Lcom/android/server/audio/MQSUtils$AudioStateTrackData;
    iget-object v2, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x5

    invoke-virtual {v2, v3, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    .line 2044
    .end local v1    # "audioTrackData":Lcom/android/server/audio/MQSUtils$AudioStateTrackData;
    .end local v13    # "currentTime":J
    .end local v15    # "audioEventSource":Ljava/lang/String;
    goto :goto_1

    .line 2045
    .end local v12    # "packageName":Ljava/lang/String;
    .restart local p3    # "packageName":Ljava/lang/String;
    :cond_8
    move-object/from16 v11, p4

    const-string v1, "reportAudioStatus\uff1a no report permission"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v12, p3

    .line 2047
    .end local p3    # "packageName":Ljava/lang/String;
    .restart local v12    # "packageName":Ljava/lang/String;
    :goto_1
    return-void
.end method

.method public reportAudioHwState(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 567
    invoke-static {p1}, Lcom/android/server/audio/MQSAudioHardware;->getInstance(Landroid/content/Context;)Lcom/android/server/audio/MQSAudioHardware;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/audio/MQSAudioHardware;->onetrack()V

    .line 568
    return-void
.end method

.method public reportAudioSilentObserverToOnetrack(ILjava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p1, "level"    # I
    .param p2, "location"    # Ljava/lang/String;
    .param p3, "silent_reason"    # Ljava/lang/String;
    .param p4, "silent_type"    # I

    .line 1568
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mMiuiXlog:Landroid/media/MiuiXlog;

    const-string v1, "audio_silent_observer"

    invoke-virtual {v0, v1, p1}, Landroid/media/MiuiXlog;->checkXlogPermission(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1569
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1570
    .local v0, "info":Landroid/os/Bundle;
    const-string v1, "audio_silent_level"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1571
    const-string v1, "audio_silent_location"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1572
    const-string v1, "audio_silent_reason"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1573
    const-string v1, "audio_silent_type"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1574
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 1575
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x4

    iput v2, v1, Landroid/os/Message;->what:I

    .line 1576
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1577
    iget-object v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1579
    .end local v0    # "info":Landroid/os/Bundle;
    .end local v1    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method public reportNotificationEventToOnetrack(Ljava/lang/String;)V
    .locals 8
    .param p1, "isTurnOn"    # Ljava/lang/String;

    .line 1585
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "reportNotificationEventToOnetrack, isTurnOn: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AudioServiceStubImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1586
    invoke-static {}, Ljava/time/LocalDateTime;->now()Ljava/time/LocalDateTime;

    move-result-object v0

    .line 1587
    .local v0, "date_time":Ljava/time/LocalDateTime;
    const-string/jumbo v2, "yyyy-MM-dd HH:mm:ss.SSS"

    invoke-static {v2}, Ljava/time/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Ljava/time/format/DateTimeFormatter;

    move-result-object v2

    .line 1588
    .local v2, "formatter":Ljava/time/format/DateTimeFormatter;
    invoke-virtual {v0, v2}, Ljava/time/LocalDateTime;->format(Ljava/time/format/DateTimeFormatter;)Ljava/lang/String;

    move-result-object v3

    .line 1589
    .local v3, "current_time":Ljava/lang/String;
    const-string/jumbo v4, "{\"name\":\"audio_notification_alias\",\"audio_event\":{\"notification_isalias_ring\":\"%s\" , \"current_time\":\"%s\"}, \"dgt\":\"null\",\"audio_ext\":\"null\" }"

    filled-new-array {p1, v3}, [Ljava/lang/Object;

    move-result-object v5

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1590
    .local v4, "result":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "result: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1592
    :try_start_0
    iget-object v5, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mMiuiXlog:Landroid/media/MiuiXlog;

    invoke-virtual {v5, v4}, Landroid/media/MiuiXlog;->miuiXlogSend(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1595
    goto :goto_0

    .line 1593
    :catch_0
    move-exception v5

    .line 1594
    .local v5, "e":Ljava/lang/Exception;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "reportNotificationEventToOnetrack exception : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1597
    .end local v5    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public setA2dpDeviceClassForOneTrack(Ljava/lang/String;)V
    .locals 3
    .param p1, "deviceClassName"    # Ljava/lang/String;

    .line 1855
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mMiuiXlog:Landroid/media/MiuiXlog;

    const-string v1, "headphones"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/media/MiuiXlog;->checkXlogPermission(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1856
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "a2dp_device_class_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 1858
    :cond_0
    return-void
.end method

.method public setBluetoothHeadset(Landroid/bluetooth/BluetoothDevice;)V
    .locals 6
    .param p1, "btDevice"    # Landroid/bluetooth/BluetoothDevice;

    .line 694
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    move-result-object v0

    .line 695
    .local v0, "bluetoothClass":Landroid/bluetooth/BluetoothClass;
    const-string v1, "AudioServiceStubImpl"

    if-eqz v0, :cond_0

    .line 696
    invoke-static {v0}, Lcom/android/server/audio/AudioServiceStubImpl;->isBluetoothHeadsetDevice(Landroid/bluetooth/BluetoothClass;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->markBluetoothheadsetstub:Z

    .line 697
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothClass;->getDeviceClass()I

    move-result v2

    .line 698
    .local v2, "deviceClass":I
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothClass;->getMajorDeviceClass()I

    move-result v3

    .line 699
    .local v3, "majorClass":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "majorClass:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", deviceClass:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 700
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", markBluetoothhead:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/android/server/audio/AudioServiceStubImpl;->markBluetoothheadsetstub:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 699
    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 702
    .end local v2    # "deviceClass":I
    .end local v3    # "majorClass":I
    goto :goto_0

    .line 703
    :cond_0
    const-string v2, "bluetoothClass is null"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 705
    :goto_0
    return-void
.end method

.method public setHifiVolume(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "volume"    # I

    .line 529
    invoke-static {p1, p2}, Lmiui/util/AudioManagerHelper;->setHiFiVolume(Landroid/content/Context;I)V

    .line 530
    return-void
.end method

.method public setPreferredCommunicationDevice(Landroid/media/AudioDeviceAttributes;Ljava/util/LinkedList;)V
    .locals 5
    .param p1, "device"    # Landroid/media/AudioDeviceAttributes;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/media/AudioDeviceAttributes;",
            "Ljava/util/LinkedList<",
            "Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;",
            ">;)V"
        }
    .end annotation

    .line 1900
    .local p2, "routeClients":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;>;"
    iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportedReportAudioRouteState:Z

    if-eqz v0, :cond_2

    .line 1901
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1902
    .local v0, "scoClients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;

    .line 1903
    .local v2, "client":Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;
    invoke-virtual {v2}, Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;->getDevice()Landroid/media/AudioDeviceAttributes;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1904
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;->getPid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " deviceType="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1905
    invoke-virtual {v2}, Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;->getDevice()Landroid/media/AudioDeviceAttributes;

    move-result-object v4

    invoke-virtual {v4}, Landroid/media/AudioDeviceAttributes;->getType()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " cb="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1906
    invoke-virtual {v2}, Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;->getBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1907
    .local v3, "clientInfo":Ljava/lang/String;
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1909
    .end local v2    # "client":Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;
    .end local v3    # "clientInfo":Ljava/lang/String;
    :cond_0
    goto :goto_0

    .line 1910
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setPreferredCommunicationDevice\uff1a clients="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1911
    invoke-interface {v0}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1910
    const-string v2, "AudioServiceStubImpl"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1912
    iput-object p1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mPreferredCommunicationDevice:Landroid/media/AudioDeviceAttributes;

    .line 1913
    iput-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mCommunicationRouteClients:Ljava/util/List;

    .line 1915
    .end local v0    # "scoClients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    return-void
.end method

.method public setPutGlobalInt(Ljava/lang/Object;Lcom/android/server/audio/SettingsAdapter;Landroid/content/ContentResolver;Z)V
    .locals 4
    .param p1, "settingslock"    # Ljava/lang/Object;
    .param p2, "settings"    # Lcom/android/server/audio/SettingsAdapter;
    .param p3, "contentresolver"    # Landroid/content/ContentResolver;
    .param p4, "enabled"    # Z

    .line 1602
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 1604
    .local v0, "token":J
    :try_start_0
    monitor-enter p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1605
    :try_start_1
    const-string/jumbo v2, "spatial_audio_feature_enable"

    .line 1606
    if-eqz p4, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    .line 1605
    :goto_0
    invoke-virtual {p2, p3, v2, v3}, Lcom/android/server/audio/SettingsAdapter;->putGlobalInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1607
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1609
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1610
    nop

    .line 1611
    return-void

    .line 1607
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .end local v0    # "token":J
    .end local p0    # "this":Lcom/android/server/audio/AudioServiceStubImpl;
    .end local p1    # "settingslock":Ljava/lang/Object;
    .end local p2    # "settings":Lcom/android/server/audio/SettingsAdapter;
    .end local p3    # "contentresolver":Landroid/content/ContentResolver;
    .end local p4    # "enabled":Z
    :try_start_3
    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1609
    .restart local v0    # "token":J
    .restart local p0    # "this":Lcom/android/server/audio/AudioServiceStubImpl;
    .restart local p1    # "settingslock":Ljava/lang/Object;
    .restart local p2    # "settings":Lcom/android/server/audio/SettingsAdapter;
    .restart local p3    # "contentresolver":Landroid/content/ContentResolver;
    .restart local p4    # "enabled":Z
    :catchall_1
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1610
    throw v2
.end method

.method public setStreamMusicOrBluetoothScoIndex(Landroid/content/Context;III)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "index"    # I
    .param p3, "stream"    # I
    .param p4, "device"    # I

    .line 713
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/server/audio/AudioServiceStubImpl$2;

    invoke-direct {v1, p0, p3, p2, p4}, Lcom/android/server/audio/AudioServiceStubImpl$2;-><init>(Lcom/android/server/audio/AudioServiceStubImpl;III)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 738
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 739
    return-void
.end method

.method public setSuperIndex(IIII)I
    .locals 6
    .param p1, "index"    # I
    .param p2, "device"    # I
    .param p3, "mStreamType"    # I
    .param p4, "mIndexMax"    # I

    .line 1673
    div-int/lit8 v0, p4, 0xa

    .line 1674
    .local v0, "maxIndex":I
    const/4 v1, 0x3

    const/4 v2, 0x2

    if-eq p3, v1, :cond_0

    if-eq p3, v2, :cond_0

    const/4 v1, 0x5

    if-ne p3, v1, :cond_2

    :cond_0
    if-ne p2, v2, :cond_2

    .line 1677
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SuperVolume: setSuperIndex index="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " device="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mStreamType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AudioServiceStubImpl"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1678
    new-instance v1, Ljava/lang/String;

    const-string v3, ""

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 1679
    .local v1, "params":Ljava/lang/String;
    const-string v3, ";SuperStream="

    const-string v4, "SpkVolIdx="

    if-le p1, v0, :cond_1

    .line 1680
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1681
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1682
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mSuperVolumeOn:Z

    .line 1683
    invoke-static {v1}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 1684
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SuperVolume: mSuperVolumeOn = true   setParameters: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1685
    :cond_1
    iget-boolean v5, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mSuperVolumeOn:Z

    if-eqz v5, :cond_2

    .line 1686
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1687
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1688
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mSuperVolumeOn:Z

    .line 1689
    invoke-static {v1}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 1690
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SuperVolume: mSuperVolumeOn = false  setParameters: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1694
    .end local v1    # "params":Ljava/lang/String;
    :cond_2
    :goto_0
    if-le p1, v0, :cond_3

    move v1, v0

    goto :goto_1

    :cond_3
    move v1, p1

    :goto_1
    return v1
.end method

.method public showDeviceConnectNotification(Landroid/content/Context;IZ)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "device"    # I
    .param p3, "isShow"    # Z

    .line 534
    iget v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mReceiveNotificationDevice:I

    and-int/2addr v0, p2

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 535
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mNm:Landroid/app/NotificationManager;

    if-nez v0, :cond_0

    .line 536
    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mNm:Landroid/app/NotificationManager;

    .line 538
    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/android/server/audio/AudioServiceStubImpl$1;

    invoke-direct {v1, p0, p3, p1}, Lcom/android/server/audio/AudioServiceStubImpl$1;-><init>(Lcom/android/server/audio/AudioServiceStubImpl;ZLandroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 549
    :cond_1
    return-void
.end method

.method public showVisualEffect(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Landroid/os/Handler;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "action"    # Ljava/lang/String;
    .param p4, "handler"    # Landroid/os/Handler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Landroid/media/AudioPlaybackConfiguration;",
            ">;",
            "Landroid/os/Handler;",
            ")V"
        }
    .end annotation

    .line 559
    .local p3, "apcList":Ljava/util/List;, "Ljava/util/List<Landroid/media/AudioPlaybackConfiguration;>;"
    invoke-static {p2, p3, p4}, Landroid/media/AudioServiceInjector;->startAudioVisualIfsatisfiedWith(Ljava/lang/String;Ljava/util/List;Landroid/os/Handler;)V

    .line 562
    invoke-direct {p0, p1, p3}, Lcom/android/server/audio/AudioServiceStubImpl;->reportAudioStatus(Landroid/content/Context;Ljava/util/List;)V

    .line 563
    return-void
.end method

.method public showVisualEffectNotification(Landroid/content/Context;II)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uid"    # I
    .param p3, "event"    # I

    .line 553
    invoke-static {p2, p3, p1}, Landroid/media/AudioServiceInjector;->showNotification(IILandroid/content/Context;)V

    .line 554
    return-void
.end method

.method public soundLeakProtectionEnabled()Z
    .locals 1

    .line 305
    iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->SOUND_LEAK_PROTECTION_ENABLED:Z

    return v0
.end method

.method public spatializerSetFeature(Lcom/android/server/audio/SettingsAdapter;Lcom/android/server/audio/SpatializerHelper;Landroid/content/ContentResolver;I)V
    .locals 2
    .param p1, "settings"    # Lcom/android/server/audio/SettingsAdapter;
    .param p2, "spatializerhelper"    # Lcom/android/server/audio/SpatializerHelper;
    .param p3, "contentresolver"    # Landroid/content/ContentResolver;
    .param p4, "num"    # I

    .line 1629
    const-string/jumbo v0, "spatial_audio_feature_enable"

    invoke-virtual {p1, p3, v0, p4}, Lcom/android/server/audio/SettingsAdapter;->getGlobalInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    move v0, v1

    .line 1632
    .local v0, "featureEnabled":Z
    invoke-virtual {p2, v0}, Lcom/android/server/audio/SpatializerHelper;->setFeatureEnabled(Z)V

    .line 1633
    return-void
.end method

.method public startAudioGameEffect(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 1355
    new-instance v0, Lcom/android/server/audio/AudioGameEffect;

    invoke-direct {v0, p1}, Lcom/android/server/audio/AudioGameEffect;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioGameEffect:Lcom/android/server/audio/AudioGameEffect;

    .line 1356
    return-void
.end method

.method public startAudioPowerSaveModeObserver(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 1367
    iget v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mSettingAudioPowerSave:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1368
    new-instance v0, Lcom/android/server/audio/AudioPowerSaveModeObserver;

    iget-object v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/server/audio/AudioPowerSaveModeObserver;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioPowerSaveModeObserver:Lcom/android/server/audio/AudioPowerSaveModeObserver;

    .line 1370
    :cond_0
    return-void
.end method

.method public startAudioQueryWeatherService(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 310
    iput-object p1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mContext:Landroid/content/Context;

    .line 311
    new-instance v0, Lcom/android/server/audio/AudioQueryWeatherService;

    iget-object v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/server/audio/AudioQueryWeatherService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioQueryWeatherService:Lcom/android/server/audio/AudioQueryWeatherService;

    .line 312
    invoke-virtual {v0}, Lcom/android/server/audio/AudioQueryWeatherService;->onCreate()V

    .line 314
    new-instance v0, Lcom/android/server/audio/CloudServiceThread;

    iget-object v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/server/audio/CloudServiceThread;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mCloudService:Lcom/android/server/audio/CloudServiceThread;

    .line 315
    invoke-virtual {v0}, Lcom/android/server/audio/CloudServiceThread;->start()V

    .line 316
    return-void
.end method

.method public startAudioThermalObserver(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 1381
    iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportCinemaMode:Z

    if-eqz v0, :cond_0

    .line 1382
    new-instance v0, Lcom/android/server/audio/AudioThermalObserver;

    iget-object v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/server/audio/AudioThermalObserver;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioThermalObserver:Lcom/android/server/audio/AudioThermalObserver;

    .line 1384
    :cond_0
    return-void
.end method

.method public startCameraRecordService(Landroid/content/Context;Landroid/media/AudioRecordingConfiguration;II)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "audioConfig"    # Landroid/media/AudioRecordingConfiguration;
    .param p3, "eventType"    # I
    .param p4, "riidNow"    # I

    .line 1204
    iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportedCameraRecord:Z

    if-eqz v0, :cond_2

    if-eqz p2, :cond_2

    .line 1205
    iget v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->cameraToastServiceRiid:I

    const-string v1, "AudioServiceStubImpl"

    if-ne p4, v0, :cond_0

    .line 1206
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "the riid is exist, do not startCameraRecordService again  "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1207
    return-void

    .line 1209
    :cond_0
    iput p4, p0, Lcom/android/server/audio/AudioServiceStubImpl;->cameraToastServiceRiid:I

    .line 1210
    nop

    .line 1211
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1210
    const-string v2, "audio_headset_state"

    const/4 v3, -0x1

    invoke-static {v0, v2, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 1212
    .local v0, "cameraAudioHeadsetState":I
    invoke-virtual {p2}, Landroid/media/AudioRecordingConfiguration;->getClientPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.android.camera"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    .line 1214
    invoke-virtual {p2}, Landroid/media/AudioRecordingConfiguration;->getClientAudioSource()I

    move-result v4

    const/4 v5, 0x5

    if-ne v4, v5, :cond_2

    .line 1216
    :try_start_0
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 1217
    .local v4, "intent":Landroid/content/Intent;
    new-instance v5, Landroid/content/ComponentName;

    const-string v6, "com.miui.audiomonitor"

    const-string v7, "com.miui.audiomonitor.MiuiCameraBTRecordService"

    invoke-direct {v5, v6, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1219
    const-string v5, "packageName"

    invoke-virtual {v4, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1220
    invoke-virtual {p2}, Landroid/media/AudioRecordingConfiguration;->getAudioDevice()Landroid/media/AudioDeviceInfo;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 1221
    invoke-virtual {p2}, Landroid/media/AudioRecordingConfiguration;->getAudioDevice()Landroid/media/AudioDeviceInfo;

    move-result-object v5

    invoke-virtual {v5}, Landroid/media/AudioDeviceInfo;->getType()I

    move-result v5

    .line 1222
    .local v5, "deviceType":I
    const-string v6, "deviceType"

    invoke-virtual {v4, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1223
    const-string v6, "packageName %s deviceType %d eventType %d"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v3, v7, v8

    .line 1224
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v7, v2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v7, v3

    .line 1223
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1226
    .end local v5    # "deviceType":I
    :cond_1
    const-string v2, "eventType"

    invoke-virtual {v4, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1227
    invoke-virtual {p1, v4}, Landroid/content/Context;->startForegroundService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1230
    nop

    .end local v4    # "intent":Landroid/content/Intent;
    goto :goto_0

    .line 1228
    :catch_0
    move-exception v2

    .line 1229
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "fail to startCameraRecordService "

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1233
    .end local v0    # "cameraAudioHeadsetState":I
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_2
    :goto_0
    return-void
.end method

.method public startDolbyEffectController(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 1650
    iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportedDolbyEffectControl:Z

    if-eqz v0, :cond_0

    .line 1651
    const-string v0, "AudioServiceStubImpl"

    const-string/jumbo v1, "startDolbyEffectControl"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1652
    invoke-static {p1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->getInstance(Landroid/content/Context;)Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->init()V

    .line 1654
    :cond_0
    return-void
.end method

.method public startGameAudioEnhancer()V
    .locals 3

    .line 1360
    const-string v0, "ro.vendor.audio.game.mode"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1361
    const-string v0, "ro.vendor.audio.game.effect"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1362
    :cond_0
    new-instance v0, Lcom/android/server/audio/GameAudioEnhancer;

    iget-object v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mWorkerThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/android/server/audio/GameAudioEnhancer;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mGameAudioEnhancer:Lcom/android/server/audio/GameAudioEnhancer;

    .line 1364
    :cond_1
    return-void
.end method

.method public startMqsServer(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .line 679
    invoke-static {p1}, Lcom/android/server/audio/MQSserver;->getInstance(Landroid/content/Context;)Lcom/android/server/audio/MQSserver;

    .line 680
    return-void
.end method

.method public startPollAudioMicStatus(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 381
    const-string v0, "AudioServiceStubImpl"

    const-string/jumbo v1, "startPollAudioMicStatus"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->isSupportPollAudioMicStatus:Z

    if-eqz v0, :cond_0

    .line 383
    invoke-static {p1}, Lcom/android/server/audio/AudioDeviceMoniter;->getInstance(Landroid/content/Context;)Lcom/android/server/audio/AudioDeviceMoniter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/audio/AudioDeviceMoniter;->startPollAudioMicStatus()V

    .line 385
    :cond_0
    return-void
.end method

.method public stopCameraRecordService(Landroid/content/Context;Landroid/media/AudioRecordingConfiguration;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "audioConfig"    # Landroid/media/AudioRecordingConfiguration;
    .param p3, "riid"    # I

    .line 1237
    iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportedCameraRecord:Z

    if-eqz v0, :cond_0

    .line 1238
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "stopCameraRecordService riidNow "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AudioServiceStubImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1239
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/media/AudioRecordingConfiguration;->getClientPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.android.camera"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1240
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->cameraToastServiceRiid:I

    .line 1243
    :cond_0
    return-void
.end method

.method public superVoiceVolumeChanged(IIILjava/lang/String;IIILandroid/content/Context;)Z
    .locals 3
    .param p1, "device"    # I
    .param p2, "alias"    # I
    .param p3, "mode"    # I
    .param p4, "pkg"    # Ljava/lang/String;
    .param p5, "dir"    # I
    .param p6, "curIdx"    # I
    .param p7, "maxIdx"    # I
    .param p8, "context"    # Landroid/content/Context;

    .line 477
    const/4 v0, 0x0

    .line 478
    .local v0, "isChanged":Z
    const/4 v1, 0x2

    if-ne p1, v1, :cond_2

    if-nez p2, :cond_2

    if-eq p3, v1, :cond_0

    const/4 v1, 0x3

    if-ne p3, v1, :cond_2

    :cond_0
    iget-boolean v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mSuperVoiceVolumeSupported:Z

    if-eqz v1, :cond_2

    .line 481
    invoke-virtual {p0, p4}, Lcom/android/server/audio/AudioServiceStubImpl;->isCtsVerifier(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 482
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SuperVoiceVolumeChanged device="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " stream="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " dir="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cur="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " max="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AudioServiceStubImpl"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 484
    const/4 v1, 0x1

    if-ne p5, v1, :cond_1

    add-int/lit8 v2, p6, 0xa

    if-ne v2, p7, :cond_1

    iget-boolean v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mSuperVoiceVolumeOn:Z

    if-nez v2, :cond_1

    .line 485
    invoke-direct {p0, v1, p8}, Lcom/android/server/audio/AudioServiceStubImpl;->setSuperVoiceVolume(ZLandroid/content/Context;)V

    .line 486
    const/4 v0, 0x1

    goto :goto_0

    .line 487
    :cond_1
    const/4 v1, -0x1

    if-ne p5, v1, :cond_2

    if-ne p6, p7, :cond_2

    iget-boolean v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mSuperVoiceVolumeOn:Z

    if-eqz v1, :cond_2

    .line 488
    const/4 v1, 0x0

    invoke-direct {p0, v1, p8}, Lcom/android/server/audio/AudioServiceStubImpl;->setSuperVoiceVolume(ZLandroid/content/Context;)V

    .line 489
    const/4 v0, 0x1

    .line 492
    :cond_2
    :goto_0
    return v0
.end method

.method public updateAudioParameterClients(Landroid/os/IBinder;Ljava/lang/String;)V
    .locals 1
    .param p1, "binder"    # Landroid/os/IBinder;
    .param p2, "targetParameter"    # Ljava/lang/String;

    .line 1254
    invoke-direct {p0, p2}, Lcom/android/server/audio/AudioServiceStubImpl;->handleParameters(Ljava/lang/String;)V

    .line 1255
    sget-object v0, Lcom/android/server/audio/AudioServiceStubImpl;->TRANSMIT_AUDIO_PARAMETERS:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1256
    invoke-direct {p0, p1, p2}, Lcom/android/server/audio/AudioServiceStubImpl;->addAudioParameterClient(Landroid/os/IBinder;Ljava/lang/String;)Lcom/android/server/audio/AudioParameterClient;

    goto :goto_0

    .line 1257
    :cond_0
    sget-object v0, Lcom/android/server/audio/AudioServiceStubImpl;->DEFAULT_AUDIO_PARAMETERS:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1258
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/android/server/audio/AudioServiceStubImpl;->removeAudioParameterClient(Landroid/os/IBinder;Ljava/lang/String;Z)Lcom/android/server/audio/AudioParameterClient;

    .line 1260
    :cond_1
    :goto_0
    return-void
.end method

.method public updateBluetoothActiveDevice(IILjava/lang/String;)V
    .locals 1
    .param p1, "state"    # I
    .param p2, "profile"    # I
    .param p3, "name"    # Ljava/lang/String;

    .line 1884
    iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportedReportAudioRouteState:Z

    if-eqz v0, :cond_1

    .line 1885
    const/4 v0, 0x2

    if-ne p2, v0, :cond_1

    .line 1886
    iput p1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mA2dpDeviceConnectedState:I

    .line 1887
    if-ne p1, v0, :cond_0

    .line 1888
    iput-object p3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mBtName:Ljava/lang/String;

    goto :goto_0

    .line 1889
    :cond_0
    iget v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mHeadsetDeviceConnectedState:I

    if-nez v0, :cond_1

    .line 1890
    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mBtName:Ljava/lang/String;

    .line 1894
    :cond_1
    :goto_0
    return-void
.end method

.method public updateConcurrentVoipInfo(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/server/audio/AudioService$SetModeDeathHandler;",
            ">;)V"
        }
    .end annotation

    .line 1480
    .local p1, "setModeDeathHandlers":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/audio/AudioService$SetModeDeathHandler;>;"
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    .line 1481
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1482
    .local v0, "clients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/audio/AudioService$SetModeDeathHandler;

    .line 1483
    .local v3, "handler":Lcom/android/server/audio/AudioService$SetModeDeathHandler;
    invoke-virtual {v3}, Lcom/android/server/audio/AudioService$SetModeDeathHandler;->getPackage()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1484
    .end local v3    # "handler":Lcom/android/server/audio/AudioService$SetModeDeathHandler;
    goto :goto_0

    .line 1485
    :cond_0
    iget-object v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 1487
    .end local v0    # "clients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    return-void
.end method

.method public updateCumulativePlaybackTime(Z)V
    .locals 4
    .param p1, "start"    # Z

    .line 1115
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1116
    .local v0, "systemTime":J
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " updateCumulativePlaybackTime start="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " systemTime="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "AudioServiceStubImpl"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1117
    if-eqz p1, :cond_0

    .line 1118
    iput-wide v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mCumulativePlaybackStartTime:J

    .line 1119
    invoke-direct {p0}, Lcom/android/server/audio/AudioServiceStubImpl;->persistCumulativePlaybackStartMsToSettings()V

    goto :goto_0

    .line 1121
    :cond_0
    iput-wide v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mCumulativePlaybackEndTime:J

    .line 1123
    :goto_0
    return-void
.end method

.method public updateModeOwnerPid(ILjava/lang/String;)V
    .locals 2
    .param p1, "pid"    # I
    .param p2, "eventSource"    # Ljava/lang/String;

    .line 1799
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "updateModeOwnerPid, pid: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", eventSource: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AudioServiceStubImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1800
    iput p1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mModeOwnerPid:I

    .line 1801
    invoke-direct {p0, p1}, Lcom/android/server/audio/AudioServiceStubImpl;->clearSetScoCommunicationDevice(I)V

    .line 1802
    invoke-direct {p0, p2}, Lcom/android/server/audio/AudioServiceStubImpl;->notifyBtStopBluetoothSco(Ljava/lang/String;)V

    .line 1803
    return-void
.end method

.method public updateNotificationMode(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .line 908
    invoke-static {p1}, Landroid/media/AudioServiceInjector;->updateNotificationMode(Landroid/content/Context;)V

    .line 909
    return-void
.end method

.method public updatePlaybackTime(Z)V
    .locals 7
    .param p1, "start"    # Z

    .line 1127
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1128
    .local v0, "systemTime":J
    if-eqz p1, :cond_0

    .line 1129
    iput-wide v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mPlaybackStartTime:J

    goto :goto_0

    .line 1131
    :cond_0
    iput-wide v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mPlaybackEndTime:J

    .line 1133
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " updatePlaybackTime start="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " systemTime="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " throd="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mPlaybackEndTime:J

    iget-wide v5, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mPlaybackStartTime:J

    sub-long/2addr v3, v5

    .line 1134
    invoke-static {v3, v4}, Ljava/lang/Math;->abs(J)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1133
    const-string v3, "AudioServiceStubImpl"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1135
    return-void
.end method

.method public updateVolumeBoostState(IILandroid/content/Context;)V
    .locals 2
    .param p1, "audioMode"    # I
    .param p2, "modeOwnerPid"    # I
    .param p3, "context"    # Landroid/content/Context;

    .line 463
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mVolumeBoostEnabled:Z

    if-eqz v0, :cond_0

    .line 464
    const/4 v0, 0x0

    invoke-direct {p0, v0, p3}, Lcom/android/server/audio/AudioServiceStubImpl;->setVolumeBoost(ZLandroid/content/Context;)V

    goto :goto_0

    .line 465
    :cond_0
    if-nez p1, :cond_1

    .line 467
    invoke-static {}, Lmiui/tipclose/TipHelperProxy;->getInstance()Lmiui/tipclose/TipHelperProxy;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/tipclose/TipHelperProxy;->hideTipForPhone()V

    .line 469
    :cond_1
    :goto_0
    iput p1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioMode:I

    .line 471
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "updateVolumeBoostState audiomode "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AudioServiceStubImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    return-void
.end method

.method public uriState(Landroid/net/Uri;Landroid/content/Context;Landroid/content/ContentResolver;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "contentResolver"    # Landroid/content/ContentResolver;

    .line 599
    const-string/jumbo v0, "sound_assist_key"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 600
    invoke-static {}, Lcom/android/server/audio/PlaybackActivityMonitorStub;->get()Lcom/android/server/audio/PlaybackActivityMonitorStub;

    move-result-object v0

    invoke-interface {v0, p3}, Lcom/android/server/audio/PlaybackActivityMonitorStub;->loadSoundAssistSettings(Landroid/content/ContentResolver;)V

    .line 601
    invoke-static {}, Lcom/android/server/audio/PlaybackActivityMonitorStub;->get()Lcom/android/server/audio/PlaybackActivityMonitorStub;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/android/server/audio/PlaybackActivityMonitorStub;->resetPlayerVolume(Landroid/content/Context;)V

    .line 602
    return-void

    .line 605
    :cond_0
    const-string v0, "key_ignore_music_focus_req"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 606
    invoke-static {}, Lcom/android/server/audio/PlaybackActivityMonitorStub;->get()Lcom/android/server/audio/PlaybackActivityMonitorStub;

    move-result-object v0

    invoke-interface {v0, p3}, Lcom/android/server/audio/PlaybackActivityMonitorStub;->loadSoundAssistSettings(Landroid/content/ContentResolver;)V

    .line 608
    :cond_1
    return-void
.end method
