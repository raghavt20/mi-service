.class public Lcom/android/server/audio/AudioPowerSaveModeObserver;
.super Ljava/lang/Object;
.source "AudioPowerSaveModeObserver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener;,
        Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;
    }
.end annotation


# static fields
.field private static final ACTION_POWER_SAVE_MODE_CHANGED:Ljava/lang/String; = "miui.intent.action.POWER_SAVE_MODE_CHANGED"

.field private static final AUDIO_POWER_SAVE_LEVEL_PATH:Ljava/lang/String; = "/sys/class/thermal/power_save/power_level"

.field private static final AUDIO_POWER_SAVE_SETTING:Ljava/lang/String; = "persist.vendor.audio.power.save.setting"

.field private static final AUDIO_POWER_SAVE_STATE_PATH:Ljava/lang/String; = "/sys/class/thermal/power_save/powersave_mode"

.field private static final KEY_GLOBAL_VOICE_TRIGGER_ENABLED:Ljava/lang/String; = "global_voice_trigger_enabled"

.field private static final METHOD_START_RECOGNITION:Ljava/lang/String; = "method_start_recognition"

.field private static final METHOD_STOP_RECOGNITION:Ljava/lang/String; = "method_stop_recognition"

.field public static final MSG_AUDIO_POWER_SAVE_POLICY:I = 0x9c4

.field public static final MSG_CHECK_AUDIO_POWER_SAVE_POLICY:I = 0x9c8

.field public static final MSG_CLEAR_AUDIO_POWER_SAVE_POLICY:I = 0x9c7

.field public static final MSG_SET_AUDIO_POWER_SAVE_LEVEL_POLICY:I = 0x9c6

.field public static final MSG_SET_AUDIO_POWER_SAVE_MODE_POLICY:I = 0x9c5

.field private static final READ_WAIT_TIME_SECONDS:I = 0x3

.field private static final TAG:Ljava/lang/String; = "AudioPowerSaveModeObserver"

.field private static final VALUE_GLOBAL_VOICE_TRIGGER_DISABLE:I = 0x0

.field private static final VALUE_GLOBAL_VOICE_TRIGGER_ENABLED:I = 0x1

.field private static mIsVoiceTriggerEnableForAudioPowerSave:I

.field private static mIsVoiceTriggerEnableForGlobal:I


# instance fields
.field private mAudioPowerLevelListener:Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener;

.field private mAudioPowerSaveBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mAudioPowerSaveListener:Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener;

.field private mContext:Landroid/content/Context;

.field private mHandler:Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;

.field private mLevelRecord:I

.field private mModeRecord:I

.field private mUri:Landroid/net/Uri;

.field private mVoiceTriggerRecord:I


# direct methods
.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/audio/AudioPowerSaveModeObserver;)Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mHandler:Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mcheckAudioPowerSaveLevel(Lcom/android/server/audio/AudioPowerSaveModeObserver;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->checkAudioPowerSaveLevel(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mcheckAudioPowerSaveState(Lcom/android/server/audio/AudioPowerSaveModeObserver;Ljava/lang/String;)I
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->checkAudioPowerSaveState(Ljava/lang/String;)I

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$msetAudioPowerSaveLevel(Lcom/android/server/audio/AudioPowerSaveModeObserver;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->setAudioPowerSaveLevel(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetAudioPowerSaveMode(Lcom/android/server/audio/AudioPowerSaveModeObserver;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->setAudioPowerSaveMode(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mtoSleep(Lcom/android/server/audio/AudioPowerSaveModeObserver;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->toSleep()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 70
    const/4 v0, -0x1

    sput v0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mIsVoiceTriggerEnableForAudioPowerSave:I

    .line 72
    sput v0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mIsVoiceTriggerEnableForGlobal:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mAudioPowerSaveListener:Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener;

    .line 51
    iput-object v0, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mAudioPowerLevelListener:Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener;

    .line 57
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mModeRecord:I

    .line 58
    iput v0, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mLevelRecord:I

    .line 59
    iput v0, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mVoiceTriggerRecord:I

    .line 63
    const-string v1, "content://com.miui.voicetrigger.SmModelProvider/mode/voicetrigger"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mUri:Landroid/net/Uri;

    .line 395
    new-instance v1, Lcom/android/server/audio/AudioPowerSaveModeObserver$1;

    invoke-direct {v1, p0}, Lcom/android/server/audio/AudioPowerSaveModeObserver$1;-><init>(Lcom/android/server/audio/AudioPowerSaveModeObserver;)V

    iput-object v1, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mAudioPowerSaveBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 77
    iput v0, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mModeRecord:I

    .line 78
    iput v0, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mLevelRecord:I

    .line 79
    iput-object p1, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mContext:Landroid/content/Context;

    .line 80
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "AudioPowerSaveModeObserver"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 81
    .local v0, "thread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 82
    new-instance v1, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;-><init>(Lcom/android/server/audio/AudioPowerSaveModeObserver;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mHandler:Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;

    .line 83
    new-instance v1, Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener;

    const-string v2, "/sys/class/thermal/power_save/powersave_mode"

    invoke-direct {v1, p0, v2}, Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener;-><init>(Lcom/android/server/audio/AudioPowerSaveModeObserver;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mAudioPowerSaveListener:Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener;

    .line 84
    new-instance v1, Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener;

    const-string v2, "/sys/class/thermal/power_save/power_level"

    invoke-direct {v1, p0, v2}, Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener;-><init>(Lcom/android/server/audio/AudioPowerSaveModeObserver;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mAudioPowerLevelListener:Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener;

    .line 85
    invoke-direct {p0}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->watchAudioPowerStateListener()V

    .line 86
    return-void
.end method

.method private checkAudioPowerSaveLevel(Ljava/lang/String;)V
    .locals 8
    .param p1, "path"    # Ljava/lang/String;

    .line 225
    const-string v0, "AudioPowerSaveModeObserver"

    const/4 v1, 0x0

    .line 226
    .local v1, "mLevel":Ljava/lang/String;
    const/4 v2, 0x0

    .line 228
    .local v2, "mPowerLevel":I
    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->getContentFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v1, v3

    if-eqz v3, :cond_2

    .line 229
    const-string v3, "0"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const/4 v4, 0x0

    if-nez v3, :cond_1

    const-string v3, "-1"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    .line 232
    :cond_0
    const/4 v3, 0x7

    const/4 v5, 0x6

    invoke-virtual {v1, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    move v2, v6

    .line 233
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "checkAudioPowerSaveLevel mPowerLevel is: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " in "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "{"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 234
    invoke-virtual {v1, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "}"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x9

    invoke-virtual {v1, v3, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 233
    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 230
    :cond_1
    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    move v2, v3

    .line 236
    :goto_1
    iget-object v3, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mHandler:Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;

    const/16 v5, 0x9c6

    invoke-virtual {v3, v5}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->removeMessages(I)V

    .line 237
    iget-object v3, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mHandler:Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;

    invoke-virtual {v3, v5, v2, v4}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v4

    const-wide/16 v5, 0x3e8

    invoke-virtual {v3, v4, v5, v6}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_2

    .line 240
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "path:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 244
    :goto_2
    goto :goto_3

    .line 242
    :catch_0
    move-exception v3

    .line 243
    .local v3, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkAudioPowerSaveLevel "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_3
    return-void
.end method

.method private checkAudioPowerSaveState(Ljava/lang/String;)I
    .locals 8
    .param p1, "path"    # Ljava/lang/String;

    .line 205
    const-string v0, "AudioPowerSaveModeObserver"

    const/4 v1, 0x0

    .line 206
    .local v1, "mState":Ljava/lang/String;
    const/4 v2, 0x0

    .line 208
    .local v2, "mEnable":I
    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->getContentFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v1, v3

    if-eqz v3, :cond_0

    .line 209
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 210
    .local v3, "mPowerSave":I
    move v2, v3

    .line 211
    invoke-direct {p0}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->checkAudioPowerSaveVoiceTrigger()V

    .line 212
    iget-object v4, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mHandler:Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;

    const/16 v5, 0x9c5

    invoke-virtual {v4, v5}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->removeMessages(I)V

    .line 213
    iget-object v4, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mHandler:Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v3, v6}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    const-wide/16 v6, 0x3e8

    invoke-virtual {v4, v5, v6, v7}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 215
    nop

    .end local v3    # "mPowerSave":I
    goto :goto_0

    .line 216
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "path:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    :goto_0
    goto :goto_1

    .line 218
    :catch_0
    move-exception v3

    .line 219
    .local v3, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkAudioPowerSaveState "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_1
    return v2
.end method

.method private checkAudioPowerSaveVoiceTrigger()V
    .locals 3

    .line 248
    const-string v0, "AudioPowerSaveModeObserver"

    const-string v1, "enter checkAudioPowerSaveVoiceTrigger"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    iget-object v0, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "global_voice_trigger_enabled"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 250
    .local v0, "isVoicetriggerenablel":I
    sput v0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mIsVoiceTriggerEnableForGlobal:I

    .line 251
    return-void
.end method

.method private getContentFromFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "filePath"    # Ljava/lang/String;

    .line 283
    const-string v0, "can not get temp state "

    const-string v1, "AudioPowerSaveModeObserver"

    const/4 v2, 0x0

    .line 284
    .local v2, "is":Ljava/io/FileInputStream;
    const-string v3, ""

    .line 286
    .local v3, "content":Ljava/lang/String;
    :try_start_0
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 287
    .local v4, "file":Ljava/io/File;
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    move-object v2, v5

    .line 288
    invoke-static {v2}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->readInputStream(Ljava/io/FileInputStream;)[B

    move-result-object v5

    .line 289
    .local v5, "data":[B
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v5}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v3, v6

    .line 297
    .end local v4    # "file":Ljava/io/File;
    .end local v5    # "data":[B
    nop

    .line 299
    :try_start_1
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 302
    :goto_0
    goto/16 :goto_2

    .line 300
    :catch_0
    move-exception v4

    .line 301
    .local v4, "e":Ljava/io/IOException;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    :goto_1
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .end local v4    # "e":Ljava/io/IOException;
    goto :goto_0

    .line 297
    :catchall_0
    move-exception v4

    goto/16 :goto_3

    .line 294
    :catch_1
    move-exception v4

    .line 295
    .local v4, "e":Ljava/lang/IndexOutOfBoundsException;
    :try_start_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "index exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 297
    nop

    .end local v4    # "e":Ljava/lang/IndexOutOfBoundsException;
    if-eqz v2, :cond_0

    .line 299
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 300
    :catch_2
    move-exception v4

    .line 301
    .local v4, "e":Ljava/io/IOException;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_1

    .line 292
    .end local v4    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v4

    .line 293
    .restart local v4    # "e":Ljava/io/IOException;
    :try_start_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IO exception when read file "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 297
    nop

    .end local v4    # "e":Ljava/io/IOException;
    if-eqz v2, :cond_0

    .line 299
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_0

    .line 300
    :catch_4
    move-exception v4

    .line 301
    .restart local v4    # "e":Ljava/io/IOException;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_1

    .line 290
    .end local v4    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v4

    .line 291
    .local v4, "e":Ljava/io/FileNotFoundException;
    :try_start_6
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "can\'t find file "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 297
    nop

    .end local v4    # "e":Ljava/io/FileNotFoundException;
    if-eqz v2, :cond_0

    .line 299
    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    goto/16 :goto_0

    .line 300
    :catch_6
    move-exception v4

    .line 301
    .local v4, "e":Ljava/io/IOException;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto/16 :goto_1

    .line 305
    .end local v4    # "e":Ljava/io/IOException;
    :cond_0
    :goto_2
    return-object v3

    .line 297
    :goto_3
    if-eqz v2, :cond_1

    .line 299
    :try_start_8
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_7

    .line 302
    goto :goto_4

    .line 300
    :catch_7
    move-exception v5

    .line 301
    .local v5, "e":Ljava/io/IOException;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    .end local v5    # "e":Ljava/io/IOException;
    :cond_1
    :goto_4
    throw v4
.end method

.method private isSupportSetVoiceTrigger(Ljava/lang/String;)Z
    .locals 8
    .param p1, "powersavemode"    # Ljava/lang/String;

    .line 254
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 255
    .local v2, "voicetriggermode":Ljava/lang/String;
    const/4 v3, 0x2

    invoke-virtual {p1, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 257
    .local v3, "audiopowersavemode":Ljava/lang/String;
    const-string v4, "1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 258
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mVoiceTriggerRecord:I

    .line 261
    :cond_0
    iget v5, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mVoiceTriggerRecord:I

    const/4 v6, -0x1

    const-string v7, "AudioPowerSaveModeObserver"

    if-nez v5, :cond_1

    .line 262
    const-string v1, "VoiceTriggerMode is disable now, do not setVoiceTrigger"

    invoke-static {v7, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    sput v6, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mIsVoiceTriggerEnableForAudioPowerSave:I

    .line 264
    return v0

    .line 267
    :cond_1
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    sget v4, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mIsVoiceTriggerEnableForGlobal:I

    if-nez v4, :cond_2

    .line 268
    const-string v1, "open audiopowersavemode, VoiceTrigger is disable now, do not setVoiceTrigger"

    invoke-static {v7, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    sput v6, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mIsVoiceTriggerEnableForAudioPowerSave:I

    .line 270
    return v0

    .line 273
    :cond_2
    const-string v4, "0"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    sget v4, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mIsVoiceTriggerEnableForGlobal:I

    sget v5, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mIsVoiceTriggerEnableForAudioPowerSave:I

    if-eq v4, v5, :cond_3

    .line 274
    const-string v1, "close powersavemode, VoiceTrigger is diff now, do not setVoiceTrigger"

    invoke-static {v7, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    sput v6, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mIsVoiceTriggerEnableForAudioPowerSave:I

    .line 276
    return v0

    .line 279
    :cond_3
    return v1
.end method

.method private static readInputStream(Ljava/io/FileInputStream;)[B
    .locals 9
    .param p0, "is"    # Ljava/io/FileInputStream;

    .line 354
    const-string v0, "readInputStream "

    const-string v1, "AudioPowerSaveModeObserver"

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 355
    .local v2, "byteStream":Ljava/io/ByteArrayOutputStream;
    const/16 v3, 0x200

    .line 356
    .local v3, "blockSize":I
    const/16 v4, 0x200

    new-array v5, v4, [B

    .line 357
    .local v5, "buffer":[B
    const/4 v6, 0x0

    .line 359
    .local v6, "count":I
    :goto_0
    const/4 v7, 0x0

    :try_start_0
    invoke-virtual {p0, v5, v7, v4}, Ljava/io/FileInputStream;->read([BII)I

    move-result v8

    move v6, v8

    if-lez v8, :cond_0

    .line 360
    invoke-virtual {v2, v5, v7, v6}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 362
    :cond_0
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 367
    :try_start_1
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 370
    goto :goto_1

    .line 368
    :catch_0
    move-exception v7

    .line 369
    .local v7, "e":Ljava/io/IOException;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    .end local v7    # "e":Ljava/io/IOException;
    :goto_1
    return-object v4

    .line 366
    :catchall_0
    move-exception v4

    goto :goto_3

    .line 363
    :catch_1
    move-exception v4

    .line 364
    .local v4, "e":Ljava/lang/Exception;
    :try_start_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 367
    .end local v4    # "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 370
    goto :goto_2

    .line 368
    :catch_2
    move-exception v4

    .line 369
    .local v4, "e":Ljava/io/IOException;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    .end local v4    # "e":Ljava/io/IOException;
    nop

    .line 372
    :goto_2
    const/4 v0, 0x0

    return-object v0

    .line 367
    :goto_3
    :try_start_4
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 370
    goto :goto_4

    .line 368
    :catch_3
    move-exception v7

    .line 369
    .restart local v7    # "e":Ljava/io/IOException;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    .end local v7    # "e":Ljava/io/IOException;
    :goto_4
    throw v4
.end method

.method private registerBroadCastReceiver()V
    .locals 4

    .line 377
    const-string v0, "AudioPowerSaveModeObserver"

    :try_start_0
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 378
    .local v1, "mIntentFilter":Landroid/content/IntentFilter;
    const-string v2, "miui.intent.action.POWER_SAVE_MODE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 379
    iget-object v2, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mAudioPowerSaveBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 380
    const-string v2, "registerBroadCastReceiver"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 383
    nop

    .end local v1    # "mIntentFilter":Landroid/content/IntentFilter;
    goto :goto_0

    .line 381
    :catch_0
    move-exception v1

    .line 382
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "registerBroadCastReceiver exception "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private setAudioPowerSaveLevel(I)V
    .locals 4
    .param p1, "mPowerSaveLevel"    # I

    .line 160
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    .line 162
    :cond_0
    iget v0, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mModeRecord:I

    const-string v1, "AudioPowerSaveModeObserver"

    if-nez v0, :cond_1

    .line 163
    const-string v0, "PowerMode is close, setPowerLevel 0"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    const/4 p1, 0x0

    .line 166
    :cond_1
    iget v0, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mLevelRecord:I

    if-eq v0, p1, :cond_2

    .line 168
    :try_start_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 169
    .local v0, "level":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Audio_Power_Save_Level="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 170
    .local v2, "mParameter":Ljava/lang/String;
    invoke-static {v2}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 171
    iput p1, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mLevelRecord:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 174
    .end local v0    # "level":Ljava/lang/String;
    .end local v2    # "mParameter":Ljava/lang/String;
    goto :goto_0

    .line 172
    :catch_0
    move-exception v0

    .line 173
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setAudioPowerSaveLevel exception "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    :goto_0
    return-void
.end method

.method private setAudioPowerSaveMode(I)V
    .locals 5
    .param p1, "mPowerSaveMode"    # I

    .line 137
    const-string v0, "AudioPowerSaveModeObserver"

    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    const/4 p1, 0x0

    .line 140
    :cond_0
    :try_start_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 141
    .local v1, "mode":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "0"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 142
    :cond_1
    iget v2, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mModeRecord:I

    if-eq v2, p1, :cond_2

    .line 143
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "natural mode =  "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Audio_Power_Save_Mode="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 145
    .local v2, "mParameter":Ljava/lang/String;
    invoke-static {v2}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 146
    iput p1, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mModeRecord:I

    .line 147
    .end local v2    # "mParameter":Ljava/lang/String;
    goto :goto_0

    .line 148
    :cond_2
    const-string v2, "AudioPowerSaveMode is no need to change"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    :goto_0
    const-string v2, "persist.vendor.audio.power.save.setting"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 151
    .local v2, "currentAudioPowerSave":I
    and-int/lit8 v3, v2, 0x8

    if-eqz v3, :cond_3

    .line 152
    invoke-direct {p0, v1}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->setAudioPowerSaveVoiceTrigger(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    .end local v1    # "mode":Ljava/lang/String;
    .end local v2    # "currentAudioPowerSave":I
    :cond_3
    goto :goto_1

    .line 154
    :catch_0
    move-exception v1

    .line 155
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setAudioPowerSaveMode exception "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method

.method private setAudioPowerSaveVoiceTrigger(Ljava/lang/String;)V
    .locals 7
    .param p1, "powersavemode"    # Ljava/lang/String;

    .line 179
    invoke-direct {p0, p1}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->isSupportSetVoiceTrigger(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "AudioPowerSaveModeObserver"

    if-nez v0, :cond_0

    .line 180
    const-string v0, "no need to change VoiceTrigger"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    return-void

    .line 185
    :cond_0
    :try_start_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 186
    .local v0, "bundle":Landroid/os/Bundle;
    nop

    .line 187
    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v4, "1"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v4, 0x0

    if-eqz v2, :cond_1

    .line 188
    const-string/jumbo v2, "setVoiceTrigger stop"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    iget-object v2, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mUri:Landroid/net/Uri;

    const-string v5, "method_stop_recognition"

    invoke-virtual {v2, v3, v5, v4, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 190
    const/4 v2, 0x0

    sput v2, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mIsVoiceTriggerEnableForAudioPowerSave:I

    goto :goto_0

    .line 192
    :cond_1
    const-string/jumbo v2, "setVoiceTrigger start"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    iget-object v2, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v5, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mUri:Landroid/net/Uri;

    const-string v6, "method_start_recognition"

    invoke-virtual {v2, v5, v6, v4, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 194
    sput v3, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mIsVoiceTriggerEnableForAudioPowerSave:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 201
    .end local v0    # "bundle":Landroid/os/Bundle;
    :goto_0
    goto :goto_1

    .line 199
    :catch_0
    move-exception v0

    .line 200
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setVoiceTrigger exception "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method

.method private toSleep()V
    .locals 3

    .line 310
    :try_start_0
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x3

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 313
    goto :goto_0

    .line 311
    :catch_0
    move-exception v0

    .line 312
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "toSleep exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AudioPowerSaveModeObserver"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private unregisterBroadCastReceiver()V
    .locals 4

    .line 388
    const-string v0, "AudioPowerSaveModeObserver"

    :try_start_0
    iget-object v1, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mAudioPowerSaveBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 389
    const-string/jumbo v1, "unregisterBroadCastReceiver"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 392
    goto :goto_0

    .line 390
    :catch_0
    move-exception v1

    .line 391
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unregisterBroadCastReceiver exception "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private unwatchAudioPowerStateListener()V
    .locals 3

    .line 327
    :try_start_0
    iget-object v0, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mAudioPowerSaveListener:Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener;

    invoke-virtual {v0}, Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener;->stopWatching()V

    .line 328
    iget-object v0, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mAudioPowerLevelListener:Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener;

    invoke-virtual {v0}, Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener;->stopWatching()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 331
    goto :goto_0

    .line 329
    :catch_0
    move-exception v0

    .line 330
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unwatchAudioPowerStateListener exception "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AudioPowerSaveModeObserver"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private watchAudioPowerStateListener()V
    .locals 3

    .line 318
    :try_start_0
    iget-object v0, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mAudioPowerSaveListener:Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener;

    invoke-virtual {v0}, Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener;->startWatching()V

    .line 319
    iget-object v0, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mAudioPowerLevelListener:Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener;

    invoke-virtual {v0}, Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener;->startWatching()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 322
    goto :goto_0

    .line 320
    :catch_0
    move-exception v0

    .line 321
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "watchAudioPowerStateListener exception "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AudioPowerSaveModeObserver"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method


# virtual methods
.method public reSetAudioPowerParam()V
    .locals 3

    .line 414
    :try_start_0
    iget-object v0, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mHandler:Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;

    const/16 v1, 0x9c8

    invoke-virtual {v0, v1}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 417
    goto :goto_0

    .line 415
    :catch_0
    move-exception v0

    .line 416
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "reSetAudioPowerParam exception "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AudioPowerSaveModeObserver"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 418
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method
