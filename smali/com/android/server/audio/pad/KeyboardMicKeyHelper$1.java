class com.android.server.audio.pad.KeyboardMicKeyHelper$1 extends android.widget.Toast$Callback {
	 /* .source "KeyboardMicKeyHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->initToast()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.audio.pad.KeyboardMicKeyHelper this$0; //synthetic
/* # direct methods */
 com.android.server.audio.pad.KeyboardMicKeyHelper$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/audio/pad/KeyboardMicKeyHelper; */
/* .line 58 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/widget/Toast$Callback;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onToastHidden ( ) {
/* .locals 6 */
/* .line 69 */
v0 = this.this$0;
com.android.server.audio.pad.KeyboardMicKeyHelper .-$$Nest$fgetmToastStateLock ( v0 );
/* monitor-enter v0 */
/* .line 70 */
try { // :try_start_0
	 final String v1 = "PadAdapter.KeyboardMicKeyHelper"; // const-string v1, "PadAdapter.KeyboardMicKeyHelper"
	 final String v2 = "onToastHidden() ..."; // const-string v2, "onToastHidden() ..."
	 android.util.Log .d ( v1,v2 );
	 /* .line 71 */
	 v1 = this.this$0;
	 int v2 = 0; // const/4 v2, 0x0
	 com.android.server.audio.pad.KeyboardMicKeyHelper .-$$Nest$fputmToastShowing ( v1,v2 );
	 /* .line 72 */
	 v1 = this.this$0;
	 v1 = 	 com.android.server.audio.pad.KeyboardMicKeyHelper .-$$Nest$fgetmStatePending ( v1 );
	 if ( v1 != null) { // if-eqz v1, :cond_1
		 /* .line 73 */
		 final String v1 = "PadAdapter.KeyboardMicKeyHelper"; // const-string v1, "PadAdapter.KeyboardMicKeyHelper"
		 /* const-string/jumbo v3, "show toast pending ..." */
		 android.util.Log .d ( v1,v3 );
		 /* .line 74 */
		 v1 = this.this$0;
		 com.android.server.audio.pad.KeyboardMicKeyHelper .-$$Nest$fgetmToast ( v1 );
		 v3 = this.this$0;
		 v4 = 		 com.android.server.audio.pad.KeyboardMicKeyHelper .-$$Nest$fgetmStatePending ( v3 );
		 int v5 = 1; // const/4 v5, 0x1
		 /* if-ne v4, v5, :cond_0 */
	 } // :cond_0
	 /* move v5, v2 */
} // :goto_0
com.android.server.audio.pad.KeyboardMicKeyHelper .-$$Nest$mgetToastString ( v3,v5 );
(( android.widget.Toast ) v1 ).setText ( v3 ); // invoke-virtual {v1, v3}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V
/* .line 75 */
v1 = this.this$0;
com.android.server.audio.pad.KeyboardMicKeyHelper .-$$Nest$fputmStatePending ( v1,v2 );
/* .line 76 */
v1 = this.this$0;
com.android.server.audio.pad.KeyboardMicKeyHelper .-$$Nest$fgetmToast ( v1 );
(( android.widget.Toast ) v1 ).show ( ); // invoke-virtual {v1}, Landroid/widget/Toast;->show()V
/* .line 78 */
} // :cond_1
/* monitor-exit v0 */
/* .line 79 */
return;
/* .line 78 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void onToastShown ( ) {
/* .locals 3 */
/* .line 61 */
v0 = this.this$0;
com.android.server.audio.pad.KeyboardMicKeyHelper .-$$Nest$fgetmToastStateLock ( v0 );
/* monitor-enter v0 */
/* .line 62 */
try { // :try_start_0
final String v1 = "PadAdapter.KeyboardMicKeyHelper"; // const-string v1, "PadAdapter.KeyboardMicKeyHelper"
final String v2 = "onToastShown() ..."; // const-string v2, "onToastShown() ..."
android.util.Log .d ( v1,v2 );
/* .line 63 */
v1 = this.this$0;
int v2 = 1; // const/4 v2, 0x1
com.android.server.audio.pad.KeyboardMicKeyHelper .-$$Nest$fputmToastShowing ( v1,v2 );
/* .line 64 */
/* monitor-exit v0 */
/* .line 65 */
return;
/* .line 64 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
