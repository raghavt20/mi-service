class com.android.server.audio.pad.VoipFocusHelper$2 extends android.hardware.camera2.CameraManager$AvailabilityCallback {
	 /* .source "VoipFocusHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/audio/pad/VoipFocusHelper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.audio.pad.VoipFocusHelper this$0; //synthetic
/* # direct methods */
 com.android.server.audio.pad.VoipFocusHelper$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/audio/pad/VoipFocusHelper; */
/* .line 385 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/hardware/camera2/CameraManager$AvailabilityCallback;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onCameraClosed ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "cameraId" # Ljava/lang/String; */
/* .line 397 */
final String v0 = "PadAdapter.VoipFocusHelper"; // const-string v0, "PadAdapter.VoipFocusHelper"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "onCameraClosed() cameraId : "; // const-string v2, "onCameraClosed() cameraId : "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v1 );
/* .line 398 */
v0 = this.this$0;
com.android.server.audio.pad.VoipFocusHelper .-$$Nest$fgetmCameraStateLock ( v0 );
/* monitor-enter v0 */
/* .line 399 */
try { // :try_start_0
	 v1 = this.this$0;
	 com.android.server.audio.pad.VoipFocusHelper .-$$Nest$fgetmOpenedCameras ( v1 );
	 (( java.util.HashSet ) v1 ).remove ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z
	 /* .line 400 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* .line 401 */
	 v0 = this.this$0;
	 com.android.server.audio.pad.VoipFocusHelper .-$$Nest$mupdateFaceInfoSrc ( v0 );
	 /* .line 402 */
	 return;
	 /* .line 400 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 try { // :try_start_1
		 /* monitor-exit v0 */
		 /* :try_end_1 */
		 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
		 /* throw v1 */
	 } // .end method
	 public void onCameraOpened ( java.lang.String p0, java.lang.String p1 ) {
		 /* .locals 3 */
		 /* .param p1, "cameraId" # Ljava/lang/String; */
		 /* .param p2, "packageId" # Ljava/lang/String; */
		 /* .line 388 */
		 final String v0 = "PadAdapter.VoipFocusHelper"; // const-string v0, "PadAdapter.VoipFocusHelper"
		 /* new-instance v1, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v2 = "onCameraOpened() cameraId : "; // const-string v2, "onCameraOpened() cameraId : "
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 final String v2 = ", packageId : "; // const-string v2, ", packageId : "
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Log .d ( v0,v1 );
		 /* .line 389 */
		 v0 = this.this$0;
		 com.android.server.audio.pad.VoipFocusHelper .-$$Nest$fgetmCameraStateLock ( v0 );
		 /* monitor-enter v0 */
		 /* .line 390 */
		 try { // :try_start_0
			 v1 = this.this$0;
			 com.android.server.audio.pad.VoipFocusHelper .-$$Nest$fgetmOpenedCameras ( v1 );
			 (( java.util.HashSet ) v1 ).add ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
			 /* .line 391 */
			 /* monitor-exit v0 */
			 /* :try_end_0 */
			 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
			 /* .line 392 */
			 v0 = this.this$0;
			 com.android.server.audio.pad.VoipFocusHelper .-$$Nest$mupdateFaceInfoSrc ( v0 );
			 /* .line 393 */
			 return;
			 /* .line 391 */
			 /* :catchall_0 */
			 /* move-exception v1 */
			 try { // :try_start_1
				 /* monitor-exit v0 */
				 /* :try_end_1 */
				 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
				 /* throw v1 */
			 } // .end method
