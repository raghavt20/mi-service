.class Lcom/android/server/audio/pad/KeyboardMicKeyHelper$H;
.super Landroid/os/Handler;
.source "KeyboardMicKeyHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/pad/KeyboardMicKeyHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "H"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/audio/pad/KeyboardMicKeyHelper;


# direct methods
.method public constructor <init>(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 194
    iput-object p1, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$H;->this$0:Lcom/android/server/audio/pad/KeyboardMicKeyHelper;

    .line 195
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 196
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .line 200
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 205
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$H;->this$0:Lcom/android/server/audio/pad/KeyboardMicKeyHelper;

    invoke-static {v0}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->-$$Nest$fgetmAudioManager(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;)Landroid/media/AudioManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMicrophoneMute()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$H;->this$0:Lcom/android/server/audio/pad/KeyboardMicKeyHelper;

    invoke-virtual {v0}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->showEnableMicDialog()V

    goto :goto_0

    .line 202
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$H;->this$0:Lcom/android/server/audio/pad/KeyboardMicKeyHelper;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->showMicMuteChanged(Z)V

    .line 203
    nop

    .line 210
    :cond_0
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
