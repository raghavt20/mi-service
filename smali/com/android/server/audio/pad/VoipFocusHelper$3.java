class com.android.server.audio.pad.VoipFocusHelper$3 implements android.content.ServiceConnection {
	 /* .source "VoipFocusHelper.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/audio/pad/VoipFocusHelper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.audio.pad.VoipFocusHelper this$0; //synthetic
/* # direct methods */
 com.android.server.audio.pad.VoipFocusHelper$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/audio/pad/VoipFocusHelper; */
/* .line 410 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onServiceConnected ( android.content.ComponentName p0, android.os.IBinder p1 ) {
/* .locals 3 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .param p2, "service" # Landroid/os/IBinder; */
/* .line 413 */
final String v0 = "PadAdapter.VoipFocusHelper"; // const-string v0, "PadAdapter.VoipFocusHelper"
final String v1 = "AonService connected !"; // const-string v1, "AonService connected !"
android.util.Log .d ( v0,v1 );
/* .line 414 */
v0 = this.this$0;
com.android.server.audio.pad.VoipFocusHelper .-$$Nest$fgetmAonServiceLock ( v0 );
/* monitor-enter v0 */
/* .line 415 */
try { // :try_start_0
	 v1 = this.this$0;
	 com.xiaomi.aon.IMiAON$Stub .asInterface ( p2 );
	 com.android.server.audio.pad.VoipFocusHelper .-$$Nest$fputmAonService ( v1,v2 );
	 /* .line 416 */
	 v1 = this.this$0;
	 com.android.server.audio.pad.VoipFocusHelper .-$$Nest$fgetmAonServiceLock ( v1 );
	 (( java.lang.Object ) v1 ).notifyAll ( ); // invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V
	 /* .line 417 */
	 v1 = this.this$0;
	 com.android.server.audio.pad.VoipFocusHelper .-$$Nest$mupdateFaceInfoSrc ( v1 );
	 /* .line 418 */
	 /* monitor-exit v0 */
	 /* .line 419 */
	 return;
	 /* .line 418 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
} // .end method
public void onServiceDisconnected ( android.content.ComponentName p0 ) {
	 /* .locals 3 */
	 /* .param p1, "name" # Landroid/content/ComponentName; */
	 /* .line 422 */
	 final String v0 = "PadAdapter.VoipFocusHelper"; // const-string v0, "PadAdapter.VoipFocusHelper"
	 final String v1 = "AonService disconnected !"; // const-string v1, "AonService disconnected !"
	 android.util.Log .d ( v0,v1 );
	 /* .line 423 */
	 v0 = this.this$0;
	 com.android.server.audio.pad.VoipFocusHelper .-$$Nest$fgetmAonServiceLock ( v0 );
	 /* monitor-enter v0 */
	 /* .line 424 */
	 try { // :try_start_0
		 v1 = this.this$0;
		 int v2 = 0; // const/4 v2, 0x0
		 com.android.server.audio.pad.VoipFocusHelper .-$$Nest$fputmAonService ( v1,v2 );
		 /* .line 425 */
		 /* monitor-exit v0 */
		 /* .line 426 */
		 return;
		 /* .line 425 */
		 /* :catchall_0 */
		 /* move-exception v1 */
		 /* monitor-exit v0 */
		 /* :try_end_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
		 /* throw v1 */
	 } // .end method
