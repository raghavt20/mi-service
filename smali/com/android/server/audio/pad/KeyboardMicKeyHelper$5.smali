.class Lcom/android/server/audio/pad/KeyboardMicKeyHelper$5;
.super Ljava/lang/Object;
.source "KeyboardMicKeyHelper.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->createDialog()Lmiuix/appcompat/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/audio/pad/KeyboardMicKeyHelper;


# direct methods
.method constructor <init>(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/audio/pad/KeyboardMicKeyHelper;

    .line 111
    iput-object p1, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$5;->this$0:Lcom/android/server/audio/pad/KeyboardMicKeyHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .line 114
    const-string v0, "PadAdapter.KeyboardMicKeyHelper"

    const-string v1, "dialog onDismiss()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    iget-object v0, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$5;->this$0:Lcom/android/server/audio/pad/KeyboardMicKeyHelper;

    invoke-static {v0}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->-$$Nest$fgetmDialogStateLock(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 116
    :try_start_0
    iget-object v1, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$5;->this$0:Lcom/android/server/audio/pad/KeyboardMicKeyHelper;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->-$$Nest$fputmDialog(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;Lmiuix/appcompat/app/AlertDialog;)V

    .line 117
    monitor-exit v0

    .line 118
    return-void

    .line 117
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
