.class public Lcom/android/server/audio/pad/VoipFocusHelper;
.super Ljava/lang/Object;
.source "VoipFocusHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/audio/pad/VoipFocusHelper$H;
    }
.end annotation


# static fields
.field private static final AON_SERVICE_COMPONENT:Landroid/content/ComponentName;

.field private static final AON_VIEW_REGION:[I

.field private static final CAMERA_SERVICE_BINDER_NAME:Ljava/lang/String; = "media.camera"

.field private static final FACE_DETECT_IME_OUT_MS:I = 0xbb8

.field private static final FACE_MONITOR_AON:I = 0x1

.field private static final FACE_MONITOR_CAMERA:I = 0x2

.field private static final FACE_MONITOR_NONE:I = 0x0

.field private static final MSG_FACE_DETECT_TIME_OUT:I = 0x3

.field private static final MSG_FACE_INFO_UPDATE:I = 0x2

.field private static final MSG_FACE_SRC_UPDATE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "PadAdapter.VoipFocusHelper"

.field private static final VOIP_FOCUS_MODE_OFF:Ljava/lang/String; = "voip_focus_mode=off"

.field private static final VOIP_FOCUS_MODE_ON:Ljava/lang/String; = "voip_focus_mode=on"


# instance fields
.field private mAonConnection:Landroid/content/ServiceConnection;

.field private mAonListener:Lcom/xiaomi/aon/IMiAONListener;

.field private mAonService:Lcom/xiaomi/aon/IMiAON;

.field private mAonServiceLock:Ljava/lang/Object;

.field private mAudioMode:I

.field private mAvailabilityCallback:Landroid/hardware/camera2/CameraManager$AvailabilityCallback;

.field private mCameraFaceListener:Landroid/hardware/ICameraFaceListener;

.field private mCameraManager:Landroid/hardware/camera2/CameraManager;

.field private mCameraService:Landroid/hardware/ICameraService;

.field private mCameraStateLock:Ljava/lang/Object;

.field private mContext:Landroid/content/Context;

.field private mFaceDetected:Z

.field private mFaceMonitorState:I

.field private mHandler:Lcom/android/server/audio/pad/VoipFocusHelper$H;

.field private mHandlerLock:Ljava/lang/Object;

.field private mOpenedCameras:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mOrientation:I

.field private mOrientationListener:Landroid/hardware/SensorEventListener;

.field private mOrientationListening:Z

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mSettingOn:Z

.field private volatile mVoipFocusMode:Z

.field private mWorkerThread:Landroid/os/HandlerThread;


# direct methods
.method static bridge synthetic -$$Nest$fgetmAonServiceLock(Lcom/android/server/audio/pad/VoipFocusHelper;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mAonServiceLock:Ljava/lang/Object;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmCameraStateLock(Lcom/android/server/audio/pad/VoipFocusHelper;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mCameraStateLock:Ljava/lang/Object;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmOpenedCameras(Lcom/android/server/audio/pad/VoipFocusHelper;)Ljava/util/HashSet;
    .locals 0

    iget-object p0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mOpenedCameras:Ljava/util/HashSet;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmOrientation(Lcom/android/server/audio/pad/VoipFocusHelper;)I
    .locals 0

    iget p0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mOrientation:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmAonService(Lcom/android/server/audio/pad/VoipFocusHelper;Lcom/xiaomi/aon/IMiAON;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mAonService:Lcom/xiaomi/aon/IMiAON;

    return-void
.end method

.method static bridge synthetic -$$Nest$monFaceInfoUpdate(Lcom/android/server/audio/pad/VoipFocusHelper;Lcom/android/server/audio/pad/FaceInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/audio/pad/VoipFocusHelper;->onFaceInfoUpdate(Lcom/android/server/audio/pad/FaceInfo;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monFaceSrcUpdate(Lcom/android/server/audio/pad/VoipFocusHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->onFaceSrcUpdate()V

    return-void
.end method

.method static bridge synthetic -$$Nest$monOrientationUpdate(Lcom/android/server/audio/pad/VoipFocusHelper;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/audio/pad/VoipFocusHelper;->onOrientationUpdate(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mpostUpdateFaceInfo(Lcom/android/server/audio/pad/VoipFocusHelper;Lcom/android/server/audio/pad/FaceInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/audio/pad/VoipFocusHelper;->postUpdateFaceInfo(Lcom/android/server/audio/pad/FaceInfo;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateFaceInfoSrc(Lcom/android/server/audio/pad/VoipFocusHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->updateFaceInfoSrc()V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetAON_VIEW_REGION()[I
    .locals 1

    sget-object v0, Lcom/android/server/audio/pad/VoipFocusHelper;->AON_VIEW_REGION:[I

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 406
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.xiaomi.aon"

    const-string v2, "com.xiaomi.aon.AONService"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/android/server/audio/pad/VoipFocusHelper;->AON_SERVICE_COMPONENT:Landroid/content/ComponentName;

    .line 408
    const/16 v0, 0x140

    const/16 v1, 0xf0

    const/4 v2, 0x0

    filled-new-array {v2, v2, v0, v1}, [I

    move-result-object v0

    sput-object v0, Lcom/android/server/audio/pad/VoipFocusHelper;->AON_VIEW_REGION:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mAudioMode:I

    .line 55
    const/4 v1, -0x1

    iput v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mOrientation:I

    .line 58
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mOpenedCameras:Ljava/util/HashSet;

    .line 59
    iput v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mFaceMonitorState:I

    .line 63
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mHandlerLock:Ljava/lang/Object;

    .line 64
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mCameraStateLock:Ljava/lang/Object;

    .line 65
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mAonServiceLock:Ljava/lang/Object;

    .line 372
    new-instance v0, Lcom/android/server/audio/pad/VoipFocusHelper$1;

    invoke-direct {v0, p0}, Lcom/android/server/audio/pad/VoipFocusHelper$1;-><init>(Lcom/android/server/audio/pad/VoipFocusHelper;)V

    iput-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mOrientationListener:Landroid/hardware/SensorEventListener;

    .line 385
    new-instance v0, Lcom/android/server/audio/pad/VoipFocusHelper$2;

    invoke-direct {v0, p0}, Lcom/android/server/audio/pad/VoipFocusHelper$2;-><init>(Lcom/android/server/audio/pad/VoipFocusHelper;)V

    iput-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mAvailabilityCallback:Landroid/hardware/camera2/CameraManager$AvailabilityCallback;

    .line 410
    new-instance v0, Lcom/android/server/audio/pad/VoipFocusHelper$3;

    invoke-direct {v0, p0}, Lcom/android/server/audio/pad/VoipFocusHelper$3;-><init>(Lcom/android/server/audio/pad/VoipFocusHelper;)V

    iput-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mAonConnection:Landroid/content/ServiceConnection;

    .line 428
    new-instance v0, Lcom/android/server/audio/pad/VoipFocusHelper$4;

    invoke-direct {v0, p0}, Lcom/android/server/audio/pad/VoipFocusHelper$4;-><init>(Lcom/android/server/audio/pad/VoipFocusHelper;)V

    iput-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mAonListener:Lcom/xiaomi/aon/IMiAONListener;

    .line 447
    new-instance v0, Lcom/android/server/audio/pad/VoipFocusHelper$5;

    invoke-direct {v0, p0}, Lcom/android/server/audio/pad/VoipFocusHelper$5;-><init>(Lcom/android/server/audio/pad/VoipFocusHelper;)V

    iput-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mCameraFaceListener:Landroid/hardware/ICameraFaceListener;

    .line 68
    const-string v0, "PadAdapter.VoipFocusHelper"

    const-string v1, "VoipFocusHelper Construct ..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    iput-object p1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mContext:Landroid/content/Context;

    .line 70
    const-string/jumbo v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mSensorManager:Landroid/hardware/SensorManager;

    .line 71
    iget-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mContext:Landroid/content/Context;

    const-string v1, "camera"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/camera2/CameraManager;

    iput-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mCameraManager:Landroid/hardware/camera2/CameraManager;

    .line 72
    return-void
.end method

.method private bindAonService()V
    .locals 6

    .line 175
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 176
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/android/server/audio/pad/VoipFocusHelper;->AON_SERVICE_COMPONENT:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 178
    :try_start_0
    iget-object v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mAonConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 179
    const-string v1, "PadAdapter.VoipFocusHelper"

    const-string v2, "Failed to bind to AonService !"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    return-void

    .line 184
    :cond_0
    goto :goto_0

    .line 182
    :catch_0
    move-exception v1

    .line 183
    .local v1, "exception":Ljava/lang/SecurityException;
    const-string v2, "PadAdapter.VoipFocusHelper"

    const-string v3, "Forbidden to bind to AonService !"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    .end local v1    # "exception":Ljava/lang/SecurityException;
    :goto_0
    iget-object v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mAonServiceLock:Ljava/lang/Object;

    monitor-enter v1

    .line 188
    :try_start_1
    iget-object v2, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mAonServiceLock:Ljava/lang/Object;

    const-wide/16 v3, 0x2710

    invoke-virtual {v2, v3, v4}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 191
    goto :goto_1

    .line 192
    :catchall_0
    move-exception v2

    goto :goto_2

    .line 189
    :catch_1
    move-exception v2

    .line 190
    .local v2, "exception":Ljava/lang/InterruptedException;
    :try_start_2
    const-string v3, "PadAdapter.VoipFocusHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "interrupted exception : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    .end local v2    # "exception":Ljava/lang/InterruptedException;
    :goto_1
    monitor-exit v1

    .line 193
    return-void

    .line 192
    :goto_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method

.method private disableCameraStateMonitor()V
    .locals 2

    .line 170
    iget-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mCameraManager:Landroid/hardware/camera2/CameraManager;

    iget-object v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mAvailabilityCallback:Landroid/hardware/camera2/CameraManager$AvailabilityCallback;

    invoke-virtual {v0, v1}, Landroid/hardware/camera2/CameraManager;->unregisterAvailabilityCallback(Landroid/hardware/camera2/CameraManager$AvailabilityCallback;)V

    .line 171
    iget-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mOpenedCameras:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 172
    return-void
.end method

.method private disableOrientationListener()V
    .locals 2

    .line 157
    iget-boolean v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mOrientationListening:Z

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mOrientationListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 159
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mOrientationListening:Z

    .line 160
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mOrientation:I

    .line 162
    :cond_0
    return-void
.end method

.method private enableAonFaceMonitor()V
    .locals 2

    .line 236
    const-string v0, "PadAdapter.VoipFocusHelper"

    const-string v1, "enableAonFaceMonitor() ..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->unregisterCameraFaceListener()V

    .line 238
    invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->registerMiAonListener()V

    .line 239
    return-void
.end method

.method private enableCameraFaceMonitor()V
    .locals 2

    .line 242
    const-string v0, "PadAdapter.VoipFocusHelper"

    const-string v1, "enableCameraFaceMonitor() ..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->unregisterMiAonListener()V

    .line 244
    invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->registerCameraFaceListener()V

    .line 245
    return-void
.end method

.method private enableCameraStateMonitor()V
    .locals 3

    .line 166
    iget-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mCameraManager:Landroid/hardware/camera2/CameraManager;

    iget-object v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mAvailabilityCallback:Landroid/hardware/camera2/CameraManager$AvailabilityCallback;

    iget-object v2, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mHandler:Lcom/android/server/audio/pad/VoipFocusHelper$H;

    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CameraManager;->registerAvailabilityCallback(Landroid/hardware/camera2/CameraManager$AvailabilityCallback;Landroid/os/Handler;)V

    .line 167
    return-void
.end method

.method private enableOrientationListener()V
    .locals 5

    .line 144
    iget-boolean v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mOrientationListening:Z

    if-nez v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mSensorManager:Landroid/hardware/SensorManager;

    .line 146
    const/16 v1, 0x1b

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;

    move-result-object v0

    .line 147
    .local v0, "orientationSensor":Landroid/hardware/Sensor;
    if-eqz v0, :cond_0

    .line 148
    const-string v1, "PadAdapter.VoipFocusHelper"

    const-string v3, "register device_orientation sensor ..."

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    iget-object v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mOrientationListener:Landroid/hardware/SensorEventListener;

    const/4 v4, 0x2

    invoke-virtual {v1, v3, v0, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 151
    iput-boolean v2, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mOrientationListening:Z

    .line 154
    .end local v0    # "orientationSensor":Landroid/hardware/Sensor;
    :cond_0
    return-void
.end method

.method private faceDetectedStateChanged(Z)V
    .locals 2
    .param p1, "detected"    # Z

    .line 357
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "face detected : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PadAdapter.VoipFocusHelper"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    iput-boolean p1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mFaceDetected:Z

    .line 360
    if-eqz p1, :cond_0

    const-string/jumbo v0, "voip_focus_mode=on"

    goto :goto_0

    :cond_0
    const-string/jumbo v0, "voip_focus_mode=off"

    :goto_0
    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 361
    return-void
.end method

.method private onFaceInfoUpdate(Lcom/android/server/audio/pad/FaceInfo;)V
    .locals 5
    .param p1, "info"    # Lcom/android/server/audio/pad/FaceInfo;

    .line 334
    const-string v0, "PadAdapter.VoipFocusHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onFaceInfoUpdate() faceInfo : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    if-eqz p1, :cond_2

    .line 336
    iget-boolean v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mFaceDetected:Z

    if-nez v0, :cond_0

    .line 337
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/server/audio/pad/VoipFocusHelper;->faceDetectedStateChanged(Z)V

    .line 339
    :cond_0
    iget-object v0, p1, Lcom/android/server/audio/pad/FaceInfo;->mParamViewRegion:Ljava/lang/String;

    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 340
    iget-object v0, p1, Lcom/android/server/audio/pad/FaceInfo;->mParamFocusRegion:Ljava/lang/String;

    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 342
    iget-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mHandlerLock:Ljava/lang/Object;

    monitor-enter v0

    .line 343
    :try_start_0
    iget-object v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mHandler:Lcom/android/server/audio/pad/VoipFocusHelper$H;

    if-eqz v1, :cond_1

    .line 345
    const/4 v2, 0x3

    const-wide/16 v3, 0xbb8

    invoke-virtual {v1, v2, v3, v4}, Lcom/android/server/audio/pad/VoipFocusHelper$H;->sendEmptyMessageDelayed(IJ)Z

    .line 348
    :cond_1
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 350
    :cond_2
    iget-boolean v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mFaceDetected:Z

    if-eqz v0, :cond_3

    .line 351
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/server/audio/pad/VoipFocusHelper;->faceDetectedStateChanged(Z)V

    .line 354
    :cond_3
    :goto_0
    return-void
.end method

.method private onFaceSrcUpdate()V
    .locals 7

    .line 303
    iget-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mCameraStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 304
    :try_start_0
    iget-object v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mOpenedCameras:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 305
    const/4 v1, 0x1

    iput v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mFaceMonitorState:I

    .line 306
    invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->enableAonFaceMonitor()V

    .line 307
    monitor-exit v0

    return-void

    .line 310
    :cond_0
    iget-object v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mOpenedCameras:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 311
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 312
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 314
    .local v2, "id":Ljava/lang/String;
    :try_start_1
    iget-object v3, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mCameraManager:Landroid/hardware/camera2/CameraManager;

    .line 315
    invoke-virtual {v3, v2}, Landroid/hardware/camera2/CameraManager;->getCameraCharacteristics(Ljava/lang/String;)Landroid/hardware/camera2/CameraCharacteristics;

    move-result-object v3

    .line 316
    .local v3, "characteristics":Landroid/hardware/camera2/CameraCharacteristics;
    sget-object v4, Landroid/hardware/camera2/CameraCharacteristics;->LENS_FACING:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {v3, v4}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-nez v4, :cond_1

    .line 318
    const-string v4, "PadAdapter.VoipFocusHelper"

    const-string v5, "camera facing front ..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    const/4 v4, 0x2

    iput v4, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mFaceMonitorState:I

    .line 320
    invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->enableCameraFaceMonitor()V
    :try_end_1
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 321
    :try_start_2
    monitor-exit v0

    return-void

    .line 325
    .end local v3    # "characteristics":Landroid/hardware/camera2/CameraCharacteristics;
    :cond_1
    goto :goto_1

    .line 323
    :catch_0
    move-exception v3

    .line 324
    .local v3, "exception":Landroid/hardware/camera2/CameraAccessException;
    const-string v4, "PadAdapter.VoipFocusHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "camera access exception : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    .end local v2    # "id":Ljava/lang/String;
    .end local v3    # "exception":Landroid/hardware/camera2/CameraAccessException;
    :goto_1
    goto :goto_0

    .line 328
    :cond_2
    const/4 v2, 0x0

    iput v2, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mFaceMonitorState:I

    .line 329
    invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->stopFaceMonitor()V

    .line 330
    .end local v1    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    monitor-exit v0

    .line 331
    return-void

    .line 330
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method private onOrientationUpdate(I)V
    .locals 2
    .param p1, "orientation"    # I

    .line 364
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onOrientationUpdate : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PadAdapter.VoipFocusHelper"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    iput p1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mOrientation:I

    .line 367
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "voip_tx_rotation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mOrientation:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 368
    return-void
.end method

.method private postUpdateFaceInfo(Lcom/android/server/audio/pad/FaceInfo;)V
    .locals 4
    .param p1, "info"    # Lcom/android/server/audio/pad/FaceInfo;

    .line 214
    iget-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mHandlerLock:Ljava/lang/Object;

    monitor-enter v0

    .line 215
    :try_start_0
    iget-object v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mHandler:Lcom/android/server/audio/pad/VoipFocusHelper$H;

    if-eqz v1, :cond_0

    .line 217
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/android/server/audio/pad/VoipFocusHelper$H;->removeMessages(I)V

    .line 219
    iget-object v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mHandler:Lcom/android/server/audio/pad/VoipFocusHelper$H;

    const/4 v3, 0x3

    invoke-virtual {v1, v3}, Lcom/android/server/audio/pad/VoipFocusHelper$H;->removeMessages(I)V

    .line 221
    iget-object v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mHandler:Lcom/android/server/audio/pad/VoipFocusHelper$H;

    invoke-virtual {v1, v2, p1}, Lcom/android/server/audio/pad/VoipFocusHelper$H;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 223
    :cond_0
    monitor-exit v0

    .line 225
    return-void

    .line 223
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private registerCameraFaceListener()V
    .locals 5

    .line 276
    iget-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mCameraService:Landroid/hardware/ICameraService;

    if-nez v0, :cond_1

    .line 277
    const-string v0, "media.camera"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 278
    .local v0, "binder":Landroid/os/IBinder;
    const-string v1, "PadAdapter.VoipFocusHelper"

    if-nez v0, :cond_0

    .line 279
    const-string v2, "CAMERA_SERVICE not supported !"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    return-void

    .line 282
    :cond_0
    invoke-static {v0}, Landroid/hardware/ICameraService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/hardware/ICameraService;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mCameraService:Landroid/hardware/ICameraService;

    .line 284
    :try_start_0
    iget-object v3, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mCameraFaceListener:Landroid/hardware/ICameraFaceListener;

    invoke-interface {v2, v3}, Landroid/hardware/ICameraService;->addFaceListener(Landroid/hardware/ICameraFaceListener;)[Landroid/hardware/CameraStatus;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 287
    goto :goto_0

    .line 285
    :catch_0
    move-exception v2

    .line 286
    .local v2, "exception":Landroid/os/RemoteException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "registerCameraFaceListener exception : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    .end local v0    # "binder":Landroid/os/IBinder;
    .end local v2    # "exception":Landroid/os/RemoteException;
    :cond_1
    :goto_0
    return-void
.end method

.method private registerMiAonListener()V
    .locals 6

    .line 248
    iget-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mAonServiceLock:Ljava/lang/Object;

    monitor-enter v0

    .line 249
    :try_start_0
    iget-object v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mAonService:Lcom/xiaomi/aon/IMiAON;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 251
    :try_start_1
    iget-object v2, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mAonListener:Lcom/xiaomi/aon/IMiAONListener;

    const/4 v3, 0x2

    const/high16 v4, 0x41200000    # 10.0f

    const v5, 0x927c0

    invoke-interface {v1, v3, v4, v5, v2}, Lcom/xiaomi/aon/IMiAON;->registerListener(IFILcom/xiaomi/aon/IMiAONListener;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 256
    goto :goto_0

    .line 254
    :catch_0
    move-exception v1

    .line 255
    .local v1, "exception":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "PadAdapter.VoipFocusHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "register aon listener exception : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 252
    .end local v1    # "exception":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    .line 253
    .local v1, "exception":Landroid/os/RemoteException;
    const-string v2, "PadAdapter.VoipFocusHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "register aon listener remote exception : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    nop

    .line 258
    .end local v1    # "exception":Landroid/os/RemoteException;
    :cond_0
    :goto_0
    monitor-exit v0

    .line 259
    return-void

    .line 258
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method private startFaceMonitor()V
    .locals 2

    .line 203
    const-string v0, "PadAdapter.VoipFocusHelper"

    const-string/jumbo v1, "start face monitor ..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->updateFaceInfoSrc()V

    .line 205
    return-void
.end method

.method private startVoipFocusModeIfNeed()V
    .locals 3

    .line 109
    iget-boolean v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mVoipFocusMode:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mSettingOn:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mAudioMode:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 110
    const-string v0, "PadAdapter.VoipFocusHelper"

    const-string/jumbo v1, "start voip focus mode ..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mVoipFocusMode:Z

    .line 112
    iget-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mHandlerLock:Ljava/lang/Object;

    monitor-enter v0

    .line 114
    :try_start_0
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "PadAdapter.VoipFocusHelper"

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mWorkerThread:Landroid/os/HandlerThread;

    .line 115
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 116
    new-instance v1, Lcom/android/server/audio/pad/VoipFocusHelper$H;

    iget-object v2, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mWorkerThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/server/audio/pad/VoipFocusHelper$H;-><init>(Lcom/android/server/audio/pad/VoipFocusHelper;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mHandler:Lcom/android/server/audio/pad/VoipFocusHelper$H;

    .line 117
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119
    invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->bindAonService()V

    .line 120
    invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->enableCameraStateMonitor()V

    .line 121
    invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->startFaceMonitor()V

    goto :goto_0

    .line 117
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 123
    :cond_0
    :goto_0
    return-void
.end method

.method private stopFaceMonitor()V
    .locals 2

    .line 208
    const-string v0, "PadAdapter.VoipFocusHelper"

    const-string/jumbo v1, "stop face monitor ..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->unregisterMiAonListener()V

    .line 210
    invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->unregisterCameraFaceListener()V

    .line 211
    return-void
.end method

.method private stopVoipFocusModeIfNeed(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .line 126
    iget-boolean v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mVoipFocusMode:Z

    if-eqz v0, :cond_0

    .line 127
    const-string v0, "PadAdapter.VoipFocusHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "stop voip focus by reason : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mVoipFocusMode:Z

    .line 129
    iput-boolean v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mFaceDetected:Z

    .line 130
    invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->disableCameraStateMonitor()V

    .line 131
    invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->stopFaceMonitor()V

    .line 132
    invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->unbindAonService()V

    .line 134
    iget-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mHandlerLock:Ljava/lang/Object;

    monitor-enter v0

    .line 136
    const/4 v1, 0x0

    :try_start_0
    iput-object v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mHandler:Lcom/android/server/audio/pad/VoipFocusHelper$H;

    .line 137
    iget-object v2, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mWorkerThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->quitSafely()Z

    .line 138
    iput-object v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mWorkerThread:Landroid/os/HandlerThread;

    .line 139
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 141
    :cond_0
    :goto_0
    return-void
.end method

.method private unbindAonService()V
    .locals 3

    .line 196
    iget-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mAonServiceLock:Ljava/lang/Object;

    monitor-enter v0

    .line 197
    const/4 v1, 0x0

    :try_start_0
    iput-object v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mAonService:Lcom/xiaomi/aon/IMiAON;

    .line 198
    iget-object v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mAonConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 199
    monitor-exit v0

    .line 200
    return-void

    .line 199
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private unregisterCameraFaceListener()V
    .locals 3

    .line 292
    iget-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mCameraService:Landroid/hardware/ICameraService;

    if-eqz v0, :cond_0

    .line 294
    :try_start_0
    iget-object v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mCameraFaceListener:Landroid/hardware/ICameraFaceListener;

    invoke-interface {v0, v1}, Landroid/hardware/ICameraService;->removeFaceListener(Landroid/hardware/ICameraFaceListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 297
    goto :goto_0

    .line 295
    :catch_0
    move-exception v0

    .line 296
    .local v0, "exception":Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unregisterCameraFaceListener exception : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PadAdapter.VoipFocusHelper"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    .end local v0    # "exception":Landroid/os/RemoteException;
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mCameraService:Landroid/hardware/ICameraService;

    .line 300
    :cond_0
    return-void
.end method

.method private unregisterMiAonListener()V
    .locals 5

    .line 262
    iget-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mAonServiceLock:Ljava/lang/Object;

    monitor-enter v0

    .line 263
    :try_start_0
    iget-object v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mAonService:Lcom/xiaomi/aon/IMiAON;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 265
    :try_start_1
    iget-object v2, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mAonListener:Lcom/xiaomi/aon/IMiAONListener;

    const/4 v3, 0x2

    invoke-interface {v1, v3, v2}, Lcom/xiaomi/aon/IMiAON;->unregisterListener(ILcom/xiaomi/aon/IMiAONListener;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 270
    goto :goto_0

    .line 268
    :catch_0
    move-exception v1

    .line 269
    .local v1, "exception":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "PadAdapter.VoipFocusHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "unregister aon listener exception : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 266
    .end local v1    # "exception":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    .line 267
    .local v1, "exception":Landroid/os/RemoteException;
    const-string v2, "PadAdapter.VoipFocusHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "unregister aon listener remote exception : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    nop

    .line 272
    .end local v1    # "exception":Landroid/os/RemoteException;
    :cond_0
    :goto_0
    monitor-exit v0

    .line 273
    return-void

    .line 272
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method private updateFaceInfoSrc()V
    .locals 3

    .line 228
    iget-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mHandlerLock:Ljava/lang/Object;

    monitor-enter v0

    .line 229
    :try_start_0
    iget-object v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mHandler:Lcom/android/server/audio/pad/VoipFocusHelper$H;

    if-eqz v1, :cond_0

    .line 230
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/server/audio/pad/VoipFocusHelper$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 232
    :cond_0
    monitor-exit v0

    .line 233
    return-void

    .line 232
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method public handleAudioModeUpdate(I)V
    .locals 2
    .param p1, "mode"    # I

    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "audio mode update : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PadAdapter.VoipFocusHelper"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    iput p1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mAudioMode:I

    .line 77
    if-nez p1, :cond_0

    .line 80
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mSettingOn:Z

    .line 81
    invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->disableOrientationListener()V

    .line 82
    const-string v0, "exit call mode"

    invoke-direct {p0, v0}, Lcom/android/server/audio/pad/VoipFocusHelper;->stopVoipFocusModeIfNeed(Ljava/lang/String;)V

    .line 85
    :cond_0
    iget v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mAudioMode:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 86
    invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->enableOrientationListener()V

    .line 87
    invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->startVoipFocusModeIfNeed()V

    .line 89
    :cond_1
    return-void
.end method

.method public handleMeetingModeUpdate(Ljava/lang/String;)V
    .locals 2
    .param p1, "parameter"    # Ljava/lang/String;

    .line 92
    if-eqz p1, :cond_4

    const-string v0, "remote_record_mode"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 97
    :cond_0
    const-string v0, "remote_record_mode=single_on"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 98
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mSettingOn:Z

    .line 99
    invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->startVoipFocusModeIfNeed()V

    goto :goto_0

    .line 100
    :cond_1
    const-string v0, "remote_record_mode=multi_on"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 101
    const-string v0, "remote_record_mode=surround_on"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 102
    const-string v0, "remote_record_mode=off"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 103
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mSettingOn:Z

    .line 104
    const-string v0, "exit meeting mode"

    invoke-direct {p0, v0}, Lcom/android/server/audio/pad/VoipFocusHelper;->stopVoipFocusModeIfNeed(Ljava/lang/String;)V

    .line 106
    :cond_3
    :goto_0
    return-void

    .line 93
    :cond_4
    :goto_1
    const-string v0, "PadAdapter.VoipFocusHelper"

    const-string v1, "invalid parameter ..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    return-void
.end method
