public class com.android.server.audio.pad.PadAdapter {
	 /* .source "PadAdapter.java" */
	 /* # static fields */
	 private static final Integer CONFIG_PROP;
	 public static final Boolean ENABLE;
	 private static final Integer FLAG_KEYBOARD_MIC_KEY_ENABLE;
	 private static final Integer FLAG_PAD_ADAPTER_ENABLE;
	 private static final Integer FLAG_VOIP_FOCUS_ENABLE;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private final Boolean MIC_KEY_ENABLE;
	 private final Boolean VOIP_FOCUS_ENABLE;
	 private com.android.server.audio.pad.KeyboardMicKeyHelper mMicKeyHelper;
	 private com.android.server.audio.pad.VoipFocusHelper mVoipFocusHelper;
	 /* # direct methods */
	 static com.android.server.audio.pad.PadAdapter ( ) {
		 /* .locals 3 */
		 /* .line 13 */
		 /* nop */
		 /* .line 14 */
		 final String v0 = "ro.vendor.audio.pad.adapter"; // const-string v0, "ro.vendor.audio.pad.adapter"
		 int v1 = 0; // const/4 v1, 0x0
		 v0 = 		 android.os.SystemProperties .getInt ( v0,v1 );
		 /* .line 17 */
		 int v2 = 1; // const/4 v2, 0x1
		 /* and-int/2addr v0, v2 */
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* move v1, v2 */
		 } // :cond_0
		 com.android.server.audio.pad.PadAdapter.ENABLE = (v1!= 0);
		 return;
	 } // .end method
	 public com.android.server.audio.pad.PadAdapter ( ) {
		 /* .locals 4 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 24 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 18 */
		 /* and-int/lit8 v1, v0, 0x2 */
		 int v2 = 1; // const/4 v2, 0x1
		 int v3 = 0; // const/4 v3, 0x0
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 /* move v1, v2 */
		 } // :cond_0
		 /* move v1, v3 */
	 } // :goto_0
	 /* iput-boolean v1, p0, Lcom/android/server/audio/pad/PadAdapter;->VOIP_FOCUS_ENABLE:Z */
	 /* .line 19 */
	 /* and-int/lit8 v0, v0, 0x4 */
	 if ( v0 != null) { // if-eqz v0, :cond_1
	 } // :cond_1
	 /* move v2, v3 */
} // :goto_1
/* iput-boolean v2, p0, Lcom/android/server/audio/pad/PadAdapter;->MIC_KEY_ENABLE:Z */
/* .line 25 */
final String v0 = "AudioService.PadAdapter"; // const-string v0, "AudioService.PadAdapter"
final String v3 = "PadAdapter Construct ..."; // const-string v3, "PadAdapter Construct ..."
android.util.Log .d ( v0,v3 );
/* .line 26 */
if ( v1 != null) { // if-eqz v1, :cond_2
	 /* .line 27 */
	 /* new-instance v0, Lcom/android/server/audio/pad/VoipFocusHelper; */
	 /* invoke-direct {v0, p1}, Lcom/android/server/audio/pad/VoipFocusHelper;-><init>(Landroid/content/Context;)V */
	 this.mVoipFocusHelper = v0;
	 /* .line 30 */
} // :cond_2
if ( v2 != null) { // if-eqz v2, :cond_3
	 /* .line 31 */
	 /* new-instance v0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper; */
	 /* invoke-direct {v0, p1}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;-><init>(Landroid/content/Context;)V */
	 this.mMicKeyHelper = v0;
	 /* .line 33 */
} // :cond_3
return;
} // .end method
/* # virtual methods */
public void handleAudioModeUpdate ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "mode" # I */
/* .line 36 */
/* iget-boolean v0, p0, Lcom/android/server/audio/pad/PadAdapter;->VOIP_FOCUS_ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 37 */
	 v0 = this.mVoipFocusHelper;
	 (( com.android.server.audio.pad.VoipFocusHelper ) v0 ).handleAudioModeUpdate ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/audio/pad/VoipFocusHelper;->handleAudioModeUpdate(I)V
	 /* .line 39 */
} // :cond_0
return;
} // .end method
public void handleMeetingModeUpdate ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "parameter" # Ljava/lang/String; */
/* .line 54 */
/* iget-boolean v0, p0, Lcom/android/server/audio/pad/PadAdapter;->VOIP_FOCUS_ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 55 */
	 v0 = this.mVoipFocusHelper;
	 (( com.android.server.audio.pad.VoipFocusHelper ) v0 ).handleMeetingModeUpdate ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/audio/pad/VoipFocusHelper;->handleMeetingModeUpdate(Ljava/lang/String;)V
	 /* .line 57 */
} // :cond_0
return;
} // .end method
public void handleMicrophoneMuteChanged ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "muted" # Z */
/* .line 42 */
/* iget-boolean v0, p0, Lcom/android/server/audio/pad/PadAdapter;->MIC_KEY_ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 43 */
	 v0 = this.mMicKeyHelper;
	 (( com.android.server.audio.pad.KeyboardMicKeyHelper ) v0 ).handleMicrophoneMuteChanged ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->handleMicrophoneMuteChanged(Z)V
	 /* .line 45 */
} // :cond_0
return;
} // .end method
public void handleRecordEventUpdate ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "audioSource" # I */
/* .line 48 */
/* iget-boolean v0, p0, Lcom/android/server/audio/pad/PadAdapter;->MIC_KEY_ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 49 */
	 v0 = this.mMicKeyHelper;
	 (( com.android.server.audio.pad.KeyboardMicKeyHelper ) v0 ).handleRecordEventUpdate ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->handleRecordEventUpdate(I)V
	 /* .line 51 */
} // :cond_0
return;
} // .end method
