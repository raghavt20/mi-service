public class com.android.server.audio.pad.VoipFocusHelper {
	 /* .source "VoipFocusHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/audio/pad/VoipFocusHelper$H; */
	 /* } */
} // .end annotation
/* # static fields */
private static final android.content.ComponentName AON_SERVICE_COMPONENT;
private static final AON_VIEW_REGION;
private static final java.lang.String CAMERA_SERVICE_BINDER_NAME;
private static final Integer FACE_DETECT_IME_OUT_MS;
private static final Integer FACE_MONITOR_AON;
private static final Integer FACE_MONITOR_CAMERA;
private static final Integer FACE_MONITOR_NONE;
private static final Integer MSG_FACE_DETECT_TIME_OUT;
private static final Integer MSG_FACE_INFO_UPDATE;
private static final Integer MSG_FACE_SRC_UPDATE;
private static final java.lang.String TAG;
private static final java.lang.String VOIP_FOCUS_MODE_OFF;
private static final java.lang.String VOIP_FOCUS_MODE_ON;
/* # instance fields */
private android.content.ServiceConnection mAonConnection;
private com.xiaomi.aon.IMiAONListener mAonListener;
private com.xiaomi.aon.IMiAON mAonService;
private java.lang.Object mAonServiceLock;
private Integer mAudioMode;
private android.hardware.camera2.CameraManager$AvailabilityCallback mAvailabilityCallback;
private android.hardware.ICameraFaceListener mCameraFaceListener;
private android.hardware.camera2.CameraManager mCameraManager;
private android.hardware.ICameraService mCameraService;
private java.lang.Object mCameraStateLock;
private android.content.Context mContext;
private Boolean mFaceDetected;
private Integer mFaceMonitorState;
private com.android.server.audio.pad.VoipFocusHelper$H mHandler;
private java.lang.Object mHandlerLock;
private java.util.HashSet mOpenedCameras;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private volatile Integer mOrientation;
private android.hardware.SensorEventListener mOrientationListener;
private Boolean mOrientationListening;
private android.hardware.SensorManager mSensorManager;
private Boolean mSettingOn;
private volatile Boolean mVoipFocusMode;
private android.os.HandlerThread mWorkerThread;
/* # direct methods */
static java.lang.Object -$$Nest$fgetmAonServiceLock ( com.android.server.audio.pad.VoipFocusHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mAonServiceLock;
} // .end method
static java.lang.Object -$$Nest$fgetmCameraStateLock ( com.android.server.audio.pad.VoipFocusHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCameraStateLock;
} // .end method
static java.util.HashSet -$$Nest$fgetmOpenedCameras ( com.android.server.audio.pad.VoipFocusHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mOpenedCameras;
} // .end method
static Integer -$$Nest$fgetmOrientation ( com.android.server.audio.pad.VoipFocusHelper p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mOrientation:I */
} // .end method
static void -$$Nest$fputmAonService ( com.android.server.audio.pad.VoipFocusHelper p0, com.xiaomi.aon.IMiAON p1 ) { //bridge//synthethic
/* .locals 0 */
this.mAonService = p1;
return;
} // .end method
static void -$$Nest$monFaceInfoUpdate ( com.android.server.audio.pad.VoipFocusHelper p0, com.android.server.audio.pad.FaceInfo p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/audio/pad/VoipFocusHelper;->onFaceInfoUpdate(Lcom/android/server/audio/pad/FaceInfo;)V */
return;
} // .end method
static void -$$Nest$monFaceSrcUpdate ( com.android.server.audio.pad.VoipFocusHelper p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->onFaceSrcUpdate()V */
return;
} // .end method
static void -$$Nest$monOrientationUpdate ( com.android.server.audio.pad.VoipFocusHelper p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/audio/pad/VoipFocusHelper;->onOrientationUpdate(I)V */
return;
} // .end method
static void -$$Nest$mpostUpdateFaceInfo ( com.android.server.audio.pad.VoipFocusHelper p0, com.android.server.audio.pad.FaceInfo p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/audio/pad/VoipFocusHelper;->postUpdateFaceInfo(Lcom/android/server/audio/pad/FaceInfo;)V */
return;
} // .end method
static void -$$Nest$mupdateFaceInfoSrc ( com.android.server.audio.pad.VoipFocusHelper p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->updateFaceInfoSrc()V */
return;
} // .end method
static -$$Nest$sfgetAON_VIEW_REGION ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.audio.pad.VoipFocusHelper.AON_VIEW_REGION;
} // .end method
static com.android.server.audio.pad.VoipFocusHelper ( ) {
/* .locals 3 */
/* .line 406 */
/* new-instance v0, Landroid/content/ComponentName; */
final String v1 = "com.xiaomi.aon"; // const-string v1, "com.xiaomi.aon"
final String v2 = "com.xiaomi.aon.AONService"; // const-string v2, "com.xiaomi.aon.AONService"
/* invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 408 */
/* const/16 v0, 0x140 */
/* const/16 v1, 0xf0 */
int v2 = 0; // const/4 v2, 0x0
/* filled-new-array {v2, v2, v0, v1}, [I */
return;
} // .end method
public com.android.server.audio.pad.VoipFocusHelper ( ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 67 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 52 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mAudioMode:I */
/* .line 55 */
int v1 = -1; // const/4 v1, -0x1
/* iput v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mOrientation:I */
/* .line 58 */
/* new-instance v1, Ljava/util/HashSet; */
/* invoke-direct {v1}, Ljava/util/HashSet;-><init>()V */
this.mOpenedCameras = v1;
/* .line 59 */
/* iput v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mFaceMonitorState:I */
/* .line 63 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mHandlerLock = v0;
/* .line 64 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mCameraStateLock = v0;
/* .line 65 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mAonServiceLock = v0;
/* .line 372 */
/* new-instance v0, Lcom/android/server/audio/pad/VoipFocusHelper$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/audio/pad/VoipFocusHelper$1;-><init>(Lcom/android/server/audio/pad/VoipFocusHelper;)V */
this.mOrientationListener = v0;
/* .line 385 */
/* new-instance v0, Lcom/android/server/audio/pad/VoipFocusHelper$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/audio/pad/VoipFocusHelper$2;-><init>(Lcom/android/server/audio/pad/VoipFocusHelper;)V */
this.mAvailabilityCallback = v0;
/* .line 410 */
/* new-instance v0, Lcom/android/server/audio/pad/VoipFocusHelper$3; */
/* invoke-direct {v0, p0}, Lcom/android/server/audio/pad/VoipFocusHelper$3;-><init>(Lcom/android/server/audio/pad/VoipFocusHelper;)V */
this.mAonConnection = v0;
/* .line 428 */
/* new-instance v0, Lcom/android/server/audio/pad/VoipFocusHelper$4; */
/* invoke-direct {v0, p0}, Lcom/android/server/audio/pad/VoipFocusHelper$4;-><init>(Lcom/android/server/audio/pad/VoipFocusHelper;)V */
this.mAonListener = v0;
/* .line 447 */
/* new-instance v0, Lcom/android/server/audio/pad/VoipFocusHelper$5; */
/* invoke-direct {v0, p0}, Lcom/android/server/audio/pad/VoipFocusHelper$5;-><init>(Lcom/android/server/audio/pad/VoipFocusHelper;)V */
this.mCameraFaceListener = v0;
/* .line 68 */
final String v0 = "PadAdapter.VoipFocusHelper"; // const-string v0, "PadAdapter.VoipFocusHelper"
final String v1 = "VoipFocusHelper Construct ..."; // const-string v1, "VoipFocusHelper Construct ..."
android.util.Log .d ( v0,v1 );
/* .line 69 */
this.mContext = p1;
/* .line 70 */
/* const-string/jumbo v0, "sensor" */
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/SensorManager; */
this.mSensorManager = v0;
/* .line 71 */
v0 = this.mContext;
final String v1 = "camera"; // const-string v1, "camera"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/camera2/CameraManager; */
this.mCameraManager = v0;
/* .line 72 */
return;
} // .end method
private void bindAonService ( ) {
/* .locals 6 */
/* .line 175 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 176 */
/* .local v0, "intent":Landroid/content/Intent; */
v1 = com.android.server.audio.pad.VoipFocusHelper.AON_SERVICE_COMPONENT;
(( android.content.Intent ) v0 ).setComponent ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 178 */
try { // :try_start_0
v1 = this.mContext;
v2 = this.mAonConnection;
int v3 = 1; // const/4 v3, 0x1
v1 = (( android.content.Context ) v1 ).bindService ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
/* if-nez v1, :cond_0 */
/* .line 179 */
final String v1 = "PadAdapter.VoipFocusHelper"; // const-string v1, "PadAdapter.VoipFocusHelper"
final String v2 = "Failed to bind to AonService !"; // const-string v2, "Failed to bind to AonService !"
android.util.Log .d ( v1,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/SecurityException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 180 */
return;
/* .line 184 */
} // :cond_0
/* .line 182 */
/* :catch_0 */
/* move-exception v1 */
/* .line 183 */
/* .local v1, "exception":Ljava/lang/SecurityException; */
final String v2 = "PadAdapter.VoipFocusHelper"; // const-string v2, "PadAdapter.VoipFocusHelper"
final String v3 = "Forbidden to bind to AonService !"; // const-string v3, "Forbidden to bind to AonService !"
android.util.Log .d ( v2,v3 );
/* .line 185 */
} // .end local v1 # "exception":Ljava/lang/SecurityException;
} // :goto_0
v1 = this.mAonServiceLock;
/* monitor-enter v1 */
/* .line 188 */
try { // :try_start_1
v2 = this.mAonServiceLock;
/* const-wide/16 v3, 0x2710 */
(( java.lang.Object ) v2 ).wait ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/lang/Object;->wait(J)V
/* :try_end_1 */
/* .catch Ljava/lang/InterruptedException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 191 */
/* .line 192 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 189 */
/* :catch_1 */
/* move-exception v2 */
/* .line 190 */
/* .local v2, "exception":Ljava/lang/InterruptedException; */
try { // :try_start_2
final String v3 = "PadAdapter.VoipFocusHelper"; // const-string v3, "PadAdapter.VoipFocusHelper"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "interrupted exception : "; // const-string v5, "interrupted exception : "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v3,v4 );
/* .line 192 */
} // .end local v2 # "exception":Ljava/lang/InterruptedException;
} // :goto_1
/* monitor-exit v1 */
/* .line 193 */
return;
/* .line 192 */
} // :goto_2
/* monitor-exit v1 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v2 */
} // .end method
private void disableCameraStateMonitor ( ) {
/* .locals 2 */
/* .line 170 */
v0 = this.mCameraManager;
v1 = this.mAvailabilityCallback;
(( android.hardware.camera2.CameraManager ) v0 ).unregisterAvailabilityCallback ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/camera2/CameraManager;->unregisterAvailabilityCallback(Landroid/hardware/camera2/CameraManager$AvailabilityCallback;)V
/* .line 171 */
v0 = this.mOpenedCameras;
(( java.util.HashSet ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->clear()V
/* .line 172 */
return;
} // .end method
private void disableOrientationListener ( ) {
/* .locals 2 */
/* .line 157 */
/* iget-boolean v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mOrientationListening:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 158 */
v0 = this.mSensorManager;
v1 = this.mOrientationListener;
(( android.hardware.SensorManager ) v0 ).unregisterListener ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V
/* .line 159 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mOrientationListening:Z */
/* .line 160 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mOrientation:I */
/* .line 162 */
} // :cond_0
return;
} // .end method
private void enableAonFaceMonitor ( ) {
/* .locals 2 */
/* .line 236 */
final String v0 = "PadAdapter.VoipFocusHelper"; // const-string v0, "PadAdapter.VoipFocusHelper"
final String v1 = "enableAonFaceMonitor() ..."; // const-string v1, "enableAonFaceMonitor() ..."
android.util.Log .d ( v0,v1 );
/* .line 237 */
/* invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->unregisterCameraFaceListener()V */
/* .line 238 */
/* invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->registerMiAonListener()V */
/* .line 239 */
return;
} // .end method
private void enableCameraFaceMonitor ( ) {
/* .locals 2 */
/* .line 242 */
final String v0 = "PadAdapter.VoipFocusHelper"; // const-string v0, "PadAdapter.VoipFocusHelper"
final String v1 = "enableCameraFaceMonitor() ..."; // const-string v1, "enableCameraFaceMonitor() ..."
android.util.Log .d ( v0,v1 );
/* .line 243 */
/* invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->unregisterMiAonListener()V */
/* .line 244 */
/* invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->registerCameraFaceListener()V */
/* .line 245 */
return;
} // .end method
private void enableCameraStateMonitor ( ) {
/* .locals 3 */
/* .line 166 */
v0 = this.mCameraManager;
v1 = this.mAvailabilityCallback;
v2 = this.mHandler;
(( android.hardware.camera2.CameraManager ) v0 ).registerAvailabilityCallback ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CameraManager;->registerAvailabilityCallback(Landroid/hardware/camera2/CameraManager$AvailabilityCallback;Landroid/os/Handler;)V
/* .line 167 */
return;
} // .end method
private void enableOrientationListener ( ) {
/* .locals 5 */
/* .line 144 */
/* iget-boolean v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mOrientationListening:Z */
/* if-nez v0, :cond_0 */
/* .line 145 */
v0 = this.mSensorManager;
/* .line 146 */
/* const/16 v1, 0x1b */
int v2 = 1; // const/4 v2, 0x1
(( android.hardware.SensorManager ) v0 ).getDefaultSensor ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;
/* .line 147 */
/* .local v0, "orientationSensor":Landroid/hardware/Sensor; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 148 */
final String v1 = "PadAdapter.VoipFocusHelper"; // const-string v1, "PadAdapter.VoipFocusHelper"
final String v3 = "register device_orientation sensor ..."; // const-string v3, "register device_orientation sensor ..."
android.util.Log .d ( v1,v3 );
/* .line 149 */
v1 = this.mSensorManager;
v3 = this.mOrientationListener;
int v4 = 2; // const/4 v4, 0x2
(( android.hardware.SensorManager ) v1 ).registerListener ( v3, v0, v4 ); // invoke-virtual {v1, v3, v0, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z
/* .line 151 */
/* iput-boolean v2, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mOrientationListening:Z */
/* .line 154 */
} // .end local v0 # "orientationSensor":Landroid/hardware/Sensor;
} // :cond_0
return;
} // .end method
private void faceDetectedStateChanged ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "detected" # Z */
/* .line 357 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "face detected : "; // const-string v1, "face detected : "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "PadAdapter.VoipFocusHelper"; // const-string v1, "PadAdapter.VoipFocusHelper"
android.util.Log .d ( v1,v0 );
/* .line 358 */
/* iput-boolean p1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mFaceDetected:Z */
/* .line 360 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* const-string/jumbo v0, "voip_focus_mode=on" */
} // :cond_0
/* const-string/jumbo v0, "voip_focus_mode=off" */
} // :goto_0
android.media.AudioSystem .setParameters ( v0 );
/* .line 361 */
return;
} // .end method
private void onFaceInfoUpdate ( com.android.server.audio.pad.FaceInfo p0 ) {
/* .locals 5 */
/* .param p1, "info" # Lcom/android/server/audio/pad/FaceInfo; */
/* .line 334 */
final String v0 = "PadAdapter.VoipFocusHelper"; // const-string v0, "PadAdapter.VoipFocusHelper"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "onFaceInfoUpdate() faceInfo : "; // const-string v2, "onFaceInfoUpdate() faceInfo : "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v1 );
/* .line 335 */
if ( p1 != null) { // if-eqz p1, :cond_2
/* .line 336 */
/* iget-boolean v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mFaceDetected:Z */
/* if-nez v0, :cond_0 */
/* .line 337 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, v0}, Lcom/android/server/audio/pad/VoipFocusHelper;->faceDetectedStateChanged(Z)V */
/* .line 339 */
} // :cond_0
v0 = this.mParamViewRegion;
android.media.AudioSystem .setParameters ( v0 );
/* .line 340 */
v0 = this.mParamFocusRegion;
android.media.AudioSystem .setParameters ( v0 );
/* .line 342 */
v0 = this.mHandlerLock;
/* monitor-enter v0 */
/* .line 343 */
try { // :try_start_0
v1 = this.mHandler;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 345 */
int v2 = 3; // const/4 v2, 0x3
/* const-wide/16 v3, 0xbb8 */
(( com.android.server.audio.pad.VoipFocusHelper$H ) v1 ).sendEmptyMessageDelayed ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Lcom/android/server/audio/pad/VoipFocusHelper$H;->sendEmptyMessageDelayed(IJ)Z
/* .line 348 */
} // :cond_1
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 350 */
} // :cond_2
/* iget-boolean v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mFaceDetected:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 351 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/android/server/audio/pad/VoipFocusHelper;->faceDetectedStateChanged(Z)V */
/* .line 354 */
} // :cond_3
} // :goto_0
return;
} // .end method
private void onFaceSrcUpdate ( ) {
/* .locals 7 */
/* .line 303 */
v0 = this.mCameraStateLock;
/* monitor-enter v0 */
/* .line 304 */
try { // :try_start_0
v1 = this.mOpenedCameras;
v1 = (( java.util.HashSet ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/HashSet;->size()I
/* if-nez v1, :cond_0 */
/* .line 305 */
int v1 = 1; // const/4 v1, 0x1
/* iput v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mFaceMonitorState:I */
/* .line 306 */
/* invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->enableAonFaceMonitor()V */
/* .line 307 */
/* monitor-exit v0 */
return;
/* .line 310 */
} // :cond_0
v1 = this.mOpenedCameras;
(( java.util.HashSet ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;
/* .line 311 */
/* .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;" */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 312 */
/* check-cast v2, Ljava/lang/String; */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 314 */
/* .local v2, "id":Ljava/lang/String; */
try { // :try_start_1
v3 = this.mCameraManager;
/* .line 315 */
(( android.hardware.camera2.CameraManager ) v3 ).getCameraCharacteristics ( v2 ); // invoke-virtual {v3, v2}, Landroid/hardware/camera2/CameraManager;->getCameraCharacteristics(Ljava/lang/String;)Landroid/hardware/camera2/CameraCharacteristics;
/* .line 316 */
/* .local v3, "characteristics":Landroid/hardware/camera2/CameraCharacteristics; */
v4 = android.hardware.camera2.CameraCharacteristics.LENS_FACING;
(( android.hardware.camera2.CameraCharacteristics ) v3 ).get ( v4 ); // invoke-virtual {v3, v4}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;
/* check-cast v4, Ljava/lang/Integer; */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* if-nez v4, :cond_1 */
/* .line 318 */
final String v4 = "PadAdapter.VoipFocusHelper"; // const-string v4, "PadAdapter.VoipFocusHelper"
final String v5 = "camera facing front ..."; // const-string v5, "camera facing front ..."
android.util.Log .d ( v4,v5 );
/* .line 319 */
int v4 = 2; // const/4 v4, 0x2
/* iput v4, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mFaceMonitorState:I */
/* .line 320 */
/* invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->enableCameraFaceMonitor()V */
/* :try_end_1 */
/* .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 321 */
try { // :try_start_2
/* monitor-exit v0 */
return;
/* .line 325 */
} // .end local v3 # "characteristics":Landroid/hardware/camera2/CameraCharacteristics;
} // :cond_1
/* .line 323 */
/* :catch_0 */
/* move-exception v3 */
/* .line 324 */
/* .local v3, "exception":Landroid/hardware/camera2/CameraAccessException; */
final String v4 = "PadAdapter.VoipFocusHelper"; // const-string v4, "PadAdapter.VoipFocusHelper"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "camera access exception : "; // const-string v6, "camera access exception : "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v4,v5 );
/* .line 326 */
} // .end local v2 # "id":Ljava/lang/String;
} // .end local v3 # "exception":Landroid/hardware/camera2/CameraAccessException;
} // :goto_1
/* .line 328 */
} // :cond_2
int v2 = 0; // const/4 v2, 0x0
/* iput v2, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mFaceMonitorState:I */
/* .line 329 */
/* invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->stopFaceMonitor()V */
/* .line 330 */
} // .end local v1 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
/* monitor-exit v0 */
/* .line 331 */
return;
/* .line 330 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
private void onOrientationUpdate ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "orientation" # I */
/* .line 364 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "onOrientationUpdate : "; // const-string v1, "onOrientationUpdate : "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "PadAdapter.VoipFocusHelper"; // const-string v1, "PadAdapter.VoipFocusHelper"
android.util.Log .d ( v1,v0 );
/* .line 365 */
/* iput p1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mOrientation:I */
/* .line 367 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "voip_tx_rotation=" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mOrientation:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.media.AudioSystem .setParameters ( v0 );
/* .line 368 */
return;
} // .end method
private void postUpdateFaceInfo ( com.android.server.audio.pad.FaceInfo p0 ) {
/* .locals 4 */
/* .param p1, "info" # Lcom/android/server/audio/pad/FaceInfo; */
/* .line 214 */
v0 = this.mHandlerLock;
/* monitor-enter v0 */
/* .line 215 */
try { // :try_start_0
v1 = this.mHandler;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 217 */
int v2 = 2; // const/4 v2, 0x2
(( com.android.server.audio.pad.VoipFocusHelper$H ) v1 ).removeMessages ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/audio/pad/VoipFocusHelper$H;->removeMessages(I)V
/* .line 219 */
v1 = this.mHandler;
int v3 = 3; // const/4 v3, 0x3
(( com.android.server.audio.pad.VoipFocusHelper$H ) v1 ).removeMessages ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/audio/pad/VoipFocusHelper$H;->removeMessages(I)V
/* .line 221 */
v1 = this.mHandler;
(( com.android.server.audio.pad.VoipFocusHelper$H ) v1 ).obtainMessage ( v2, p1 ); // invoke-virtual {v1, v2, p1}, Lcom/android/server/audio/pad/VoipFocusHelper$H;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( android.os.Message ) v1 ).sendToTarget ( ); // invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V
/* .line 223 */
} // :cond_0
/* monitor-exit v0 */
/* .line 225 */
return;
/* .line 223 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void registerCameraFaceListener ( ) {
/* .locals 5 */
/* .line 276 */
v0 = this.mCameraService;
/* if-nez v0, :cond_1 */
/* .line 277 */
final String v0 = "media.camera"; // const-string v0, "media.camera"
android.os.ServiceManager .getService ( v0 );
/* .line 278 */
/* .local v0, "binder":Landroid/os/IBinder; */
final String v1 = "PadAdapter.VoipFocusHelper"; // const-string v1, "PadAdapter.VoipFocusHelper"
/* if-nez v0, :cond_0 */
/* .line 279 */
final String v2 = "CAMERA_SERVICE not supported !"; // const-string v2, "CAMERA_SERVICE not supported !"
android.util.Log .d ( v1,v2 );
/* .line 280 */
return;
/* .line 282 */
} // :cond_0
android.hardware.ICameraService$Stub .asInterface ( v0 );
this.mCameraService = v2;
/* .line 284 */
try { // :try_start_0
v3 = this.mCameraFaceListener;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 287 */
/* .line 285 */
/* :catch_0 */
/* move-exception v2 */
/* .line 286 */
/* .local v2, "exception":Landroid/os/RemoteException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "registerCameraFaceListener exception : "; // const-string v4, "registerCameraFaceListener exception : "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v3 );
/* .line 289 */
} // .end local v0 # "binder":Landroid/os/IBinder;
} // .end local v2 # "exception":Landroid/os/RemoteException;
} // :cond_1
} // :goto_0
return;
} // .end method
private void registerMiAonListener ( ) {
/* .locals 6 */
/* .line 248 */
v0 = this.mAonServiceLock;
/* monitor-enter v0 */
/* .line 249 */
try { // :try_start_0
v1 = this.mAonService;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 251 */
try { // :try_start_1
v2 = this.mAonListener;
int v3 = 2; // const/4 v3, 0x2
/* const/high16 v4, 0x41200000 # 10.0f */
/* const v5, 0x927c0 */
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 256 */
/* .line 254 */
/* :catch_0 */
/* move-exception v1 */
/* .line 255 */
/* .local v1, "exception":Ljava/lang/Exception; */
try { // :try_start_2
final String v2 = "PadAdapter.VoipFocusHelper"; // const-string v2, "PadAdapter.VoipFocusHelper"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "register aon listener exception : "; // const-string v4, "register aon listener exception : "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v3 );
/* .line 252 */
} // .end local v1 # "exception":Ljava/lang/Exception;
/* :catch_1 */
/* move-exception v1 */
/* .line 253 */
/* .local v1, "exception":Landroid/os/RemoteException; */
final String v2 = "PadAdapter.VoipFocusHelper"; // const-string v2, "PadAdapter.VoipFocusHelper"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "register aon listener remote exception : "; // const-string v4, "register aon listener remote exception : "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v3 );
/* .line 256 */
/* nop */
/* .line 258 */
} // .end local v1 # "exception":Landroid/os/RemoteException;
} // :cond_0
} // :goto_0
/* monitor-exit v0 */
/* .line 259 */
return;
/* .line 258 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
private void startFaceMonitor ( ) {
/* .locals 2 */
/* .line 203 */
final String v0 = "PadAdapter.VoipFocusHelper"; // const-string v0, "PadAdapter.VoipFocusHelper"
/* const-string/jumbo v1, "start face monitor ..." */
android.util.Log .d ( v0,v1 );
/* .line 204 */
/* invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->updateFaceInfoSrc()V */
/* .line 205 */
return;
} // .end method
private void startVoipFocusModeIfNeed ( ) {
/* .locals 3 */
/* .line 109 */
/* iget-boolean v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mVoipFocusMode:Z */
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mSettingOn:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mAudioMode:I */
int v1 = 3; // const/4 v1, 0x3
/* if-ne v0, v1, :cond_0 */
/* .line 110 */
final String v0 = "PadAdapter.VoipFocusHelper"; // const-string v0, "PadAdapter.VoipFocusHelper"
/* const-string/jumbo v1, "start voip focus mode ..." */
android.util.Log .d ( v0,v1 );
/* .line 111 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mVoipFocusMode:Z */
/* .line 112 */
v0 = this.mHandlerLock;
/* monitor-enter v0 */
/* .line 114 */
try { // :try_start_0
/* new-instance v1, Landroid/os/HandlerThread; */
final String v2 = "PadAdapter.VoipFocusHelper"; // const-string v2, "PadAdapter.VoipFocusHelper"
/* invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mWorkerThread = v1;
/* .line 115 */
(( android.os.HandlerThread ) v1 ).start ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V
/* .line 116 */
/* new-instance v1, Lcom/android/server/audio/pad/VoipFocusHelper$H; */
v2 = this.mWorkerThread;
(( android.os.HandlerThread ) v2 ).getLooper ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v1, p0, v2}, Lcom/android/server/audio/pad/VoipFocusHelper$H;-><init>(Lcom/android/server/audio/pad/VoipFocusHelper;Landroid/os/Looper;)V */
this.mHandler = v1;
/* .line 117 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 119 */
/* invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->bindAonService()V */
/* .line 120 */
/* invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->enableCameraStateMonitor()V */
/* .line 121 */
/* invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->startFaceMonitor()V */
/* .line 117 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
/* .line 123 */
} // :cond_0
} // :goto_0
return;
} // .end method
private void stopFaceMonitor ( ) {
/* .locals 2 */
/* .line 208 */
final String v0 = "PadAdapter.VoipFocusHelper"; // const-string v0, "PadAdapter.VoipFocusHelper"
/* const-string/jumbo v1, "stop face monitor ..." */
android.util.Log .d ( v0,v1 );
/* .line 209 */
/* invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->unregisterMiAonListener()V */
/* .line 210 */
/* invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->unregisterCameraFaceListener()V */
/* .line 211 */
return;
} // .end method
private void stopVoipFocusModeIfNeed ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .line 126 */
/* iget-boolean v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mVoipFocusMode:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 127 */
final String v0 = "PadAdapter.VoipFocusHelper"; // const-string v0, "PadAdapter.VoipFocusHelper"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "stop voip focus by reason : " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v1 );
/* .line 128 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mVoipFocusMode:Z */
/* .line 129 */
/* iput-boolean v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mFaceDetected:Z */
/* .line 130 */
/* invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->disableCameraStateMonitor()V */
/* .line 131 */
/* invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->stopFaceMonitor()V */
/* .line 132 */
/* invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->unbindAonService()V */
/* .line 134 */
v0 = this.mHandlerLock;
/* monitor-enter v0 */
/* .line 136 */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
this.mHandler = v1;
/* .line 137 */
v2 = this.mWorkerThread;
(( android.os.HandlerThread ) v2 ).quitSafely ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->quitSafely()Z
/* .line 138 */
this.mWorkerThread = v1;
/* .line 139 */
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 141 */
} // :cond_0
} // :goto_0
return;
} // .end method
private void unbindAonService ( ) {
/* .locals 3 */
/* .line 196 */
v0 = this.mAonServiceLock;
/* monitor-enter v0 */
/* .line 197 */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
this.mAonService = v1;
/* .line 198 */
v1 = this.mContext;
v2 = this.mAonConnection;
(( android.content.Context ) v1 ).unbindService ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
/* .line 199 */
/* monitor-exit v0 */
/* .line 200 */
return;
/* .line 199 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void unregisterCameraFaceListener ( ) {
/* .locals 3 */
/* .line 292 */
v0 = this.mCameraService;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 294 */
try { // :try_start_0
v1 = this.mCameraFaceListener;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 297 */
/* .line 295 */
/* :catch_0 */
/* move-exception v0 */
/* .line 296 */
/* .local v0, "exception":Landroid/os/RemoteException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "unregisterCameraFaceListener exception : " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "PadAdapter.VoipFocusHelper"; // const-string v2, "PadAdapter.VoipFocusHelper"
android.util.Log .e ( v2,v1 );
/* .line 298 */
} // .end local v0 # "exception":Landroid/os/RemoteException;
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
this.mCameraService = v0;
/* .line 300 */
} // :cond_0
return;
} // .end method
private void unregisterMiAonListener ( ) {
/* .locals 5 */
/* .line 262 */
v0 = this.mAonServiceLock;
/* monitor-enter v0 */
/* .line 263 */
try { // :try_start_0
v1 = this.mAonService;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 265 */
try { // :try_start_1
v2 = this.mAonListener;
int v3 = 2; // const/4 v3, 0x2
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 270 */
/* .line 268 */
/* :catch_0 */
/* move-exception v1 */
/* .line 269 */
/* .local v1, "exception":Ljava/lang/Exception; */
try { // :try_start_2
final String v2 = "PadAdapter.VoipFocusHelper"; // const-string v2, "PadAdapter.VoipFocusHelper"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "unregister aon listener exception : " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v3 );
/* .line 266 */
} // .end local v1 # "exception":Ljava/lang/Exception;
/* :catch_1 */
/* move-exception v1 */
/* .line 267 */
/* .local v1, "exception":Landroid/os/RemoteException; */
final String v2 = "PadAdapter.VoipFocusHelper"; // const-string v2, "PadAdapter.VoipFocusHelper"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "unregister aon listener remote exception : " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v3 );
/* .line 270 */
/* nop */
/* .line 272 */
} // .end local v1 # "exception":Landroid/os/RemoteException;
} // :cond_0
} // :goto_0
/* monitor-exit v0 */
/* .line 273 */
return;
/* .line 272 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
private void updateFaceInfoSrc ( ) {
/* .locals 3 */
/* .line 228 */
v0 = this.mHandlerLock;
/* monitor-enter v0 */
/* .line 229 */
try { // :try_start_0
v1 = this.mHandler;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 230 */
int v2 = 1; // const/4 v2, 0x1
(( com.android.server.audio.pad.VoipFocusHelper$H ) v1 ).obtainMessage ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/audio/pad/VoipFocusHelper$H;->obtainMessage(I)Landroid/os/Message;
(( android.os.Message ) v1 ).sendToTarget ( ); // invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V
/* .line 232 */
} // :cond_0
/* monitor-exit v0 */
/* .line 233 */
return;
/* .line 232 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
/* # virtual methods */
public void handleAudioModeUpdate ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "mode" # I */
/* .line 75 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "audio mode update : "; // const-string v1, "audio mode update : "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "PadAdapter.VoipFocusHelper"; // const-string v1, "PadAdapter.VoipFocusHelper"
android.util.Log .d ( v1,v0 );
/* .line 76 */
/* iput p1, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mAudioMode:I */
/* .line 77 */
/* if-nez p1, :cond_0 */
/* .line 80 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mSettingOn:Z */
/* .line 81 */
/* invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->disableOrientationListener()V */
/* .line 82 */
final String v0 = "exit call mode"; // const-string v0, "exit call mode"
/* invoke-direct {p0, v0}, Lcom/android/server/audio/pad/VoipFocusHelper;->stopVoipFocusModeIfNeed(Ljava/lang/String;)V */
/* .line 85 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mAudioMode:I */
int v1 = 3; // const/4 v1, 0x3
/* if-ne v0, v1, :cond_1 */
/* .line 86 */
/* invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->enableOrientationListener()V */
/* .line 87 */
/* invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->startVoipFocusModeIfNeed()V */
/* .line 89 */
} // :cond_1
return;
} // .end method
public void handleMeetingModeUpdate ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "parameter" # Ljava/lang/String; */
/* .line 92 */
if ( p1 != null) { // if-eqz p1, :cond_4
final String v0 = "remote_record_mode"; // const-string v0, "remote_record_mode"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_0 */
/* .line 97 */
} // :cond_0
final String v0 = "remote_record_mode=single_on"; // const-string v0, "remote_record_mode=single_on"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 98 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mSettingOn:Z */
/* .line 99 */
/* invoke-direct {p0}, Lcom/android/server/audio/pad/VoipFocusHelper;->startVoipFocusModeIfNeed()V */
/* .line 100 */
} // :cond_1
final String v0 = "remote_record_mode=multi_on"; // const-string v0, "remote_record_mode=multi_on"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_2 */
/* .line 101 */
final String v0 = "remote_record_mode=surround_on"; // const-string v0, "remote_record_mode=surround_on"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_2 */
/* .line 102 */
final String v0 = "remote_record_mode=off"; // const-string v0, "remote_record_mode=off"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 103 */
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper;->mSettingOn:Z */
/* .line 104 */
final String v0 = "exit meeting mode"; // const-string v0, "exit meeting mode"
/* invoke-direct {p0, v0}, Lcom/android/server/audio/pad/VoipFocusHelper;->stopVoipFocusModeIfNeed(Ljava/lang/String;)V */
/* .line 106 */
} // :cond_3
} // :goto_0
return;
/* .line 93 */
} // :cond_4
} // :goto_1
final String v0 = "PadAdapter.VoipFocusHelper"; // const-string v0, "PadAdapter.VoipFocusHelper"
final String v1 = "invalid parameter ..."; // const-string v1, "invalid parameter ..."
android.util.Log .d ( v0,v1 );
/* .line 94 */
return;
} // .end method
