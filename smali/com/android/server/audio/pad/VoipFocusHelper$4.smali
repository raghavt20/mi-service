.class Lcom/android/server/audio/pad/VoipFocusHelper$4;
.super Lcom/xiaomi/aon/IMiAONListener$Stub;
.source "VoipFocusHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/pad/VoipFocusHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/audio/pad/VoipFocusHelper;


# direct methods
.method constructor <init>(Lcom/android/server/audio/pad/VoipFocusHelper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/audio/pad/VoipFocusHelper;

    .line 428
    iput-object p1, p0, Lcom/android/server/audio/pad/VoipFocusHelper$4;->this$0:Lcom/android/server/audio/pad/VoipFocusHelper;

    invoke-direct {p0}, Lcom/xiaomi/aon/IMiAONListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallbackListener(I[I)V
    .locals 3
    .param p1, "Type"    # I
    .param p2, "data"    # [I

    .line 431
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onCallbackListener() data : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PadAdapter.VoipFocusHelper"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    iget-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper$4;->this$0:Lcom/android/server/audio/pad/VoipFocusHelper;

    invoke-static {v0}, Lcom/android/server/audio/pad/VoipFocusHelper;->-$$Nest$fgetmOrientation(Lcom/android/server/audio/pad/VoipFocusHelper;)I

    move-result v0

    invoke-static {}, Lcom/android/server/audio/pad/VoipFocusHelper;->-$$Nest$sfgetAON_VIEW_REGION()[I

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2, v0, v1, p2}, Lcom/android/server/audio/pad/FaceInfo;->build(II[I[I)Lcom/android/server/audio/pad/FaceInfo;

    move-result-object v0

    .line 434
    .local v0, "faceInfo":Lcom/android/server/audio/pad/FaceInfo;
    if-eqz v0, :cond_0

    .line 435
    iget-object v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper$4;->this$0:Lcom/android/server/audio/pad/VoipFocusHelper;

    invoke-static {v1, v0}, Lcom/android/server/audio/pad/VoipFocusHelper;->-$$Nest$mpostUpdateFaceInfo(Lcom/android/server/audio/pad/VoipFocusHelper;Lcom/android/server/audio/pad/FaceInfo;)V

    .line 437
    :cond_0
    return-void
.end method

.method public onImageAvailiable(I)V
    .locals 0
    .param p1, "frame_id"    # I

    .line 441
    return-void
.end method
