.class Lcom/android/server/audio/pad/KeyboardMicKeyHelper$1;
.super Landroid/widget/Toast$Callback;
.source "KeyboardMicKeyHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->initToast()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/audio/pad/KeyboardMicKeyHelper;


# direct methods
.method constructor <init>(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/audio/pad/KeyboardMicKeyHelper;

    .line 58
    iput-object p1, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$1;->this$0:Lcom/android/server/audio/pad/KeyboardMicKeyHelper;

    invoke-direct {p0}, Landroid/widget/Toast$Callback;-><init>()V

    return-void
.end method


# virtual methods
.method public onToastHidden()V
    .locals 6

    .line 69
    iget-object v0, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$1;->this$0:Lcom/android/server/audio/pad/KeyboardMicKeyHelper;

    invoke-static {v0}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->-$$Nest$fgetmToastStateLock(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 70
    :try_start_0
    const-string v1, "PadAdapter.KeyboardMicKeyHelper"

    const-string v2, "onToastHidden() ..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    iget-object v1, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$1;->this$0:Lcom/android/server/audio/pad/KeyboardMicKeyHelper;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->-$$Nest$fputmToastShowing(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;Z)V

    .line 72
    iget-object v1, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$1;->this$0:Lcom/android/server/audio/pad/KeyboardMicKeyHelper;

    invoke-static {v1}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->-$$Nest$fgetmStatePending(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;)I

    move-result v1

    if-eqz v1, :cond_1

    .line 73
    const-string v1, "PadAdapter.KeyboardMicKeyHelper"

    const-string/jumbo v3, "show toast pending ..."

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    iget-object v1, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$1;->this$0:Lcom/android/server/audio/pad/KeyboardMicKeyHelper;

    invoke-static {v1}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->-$$Nest$fgetmToast(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;)Landroid/widget/Toast;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$1;->this$0:Lcom/android/server/audio/pad/KeyboardMicKeyHelper;

    invoke-static {v3}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->-$$Nest$fgetmStatePending(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    goto :goto_0

    :cond_0
    move v5, v2

    :goto_0
    invoke-static {v3, v5}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->-$$Nest$mgetToastString(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 75
    iget-object v1, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$1;->this$0:Lcom/android/server/audio/pad/KeyboardMicKeyHelper;

    invoke-static {v1, v2}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->-$$Nest$fputmStatePending(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;I)V

    .line 76
    iget-object v1, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$1;->this$0:Lcom/android/server/audio/pad/KeyboardMicKeyHelper;

    invoke-static {v1}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->-$$Nest$fgetmToast(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 78
    :cond_1
    monitor-exit v0

    .line 79
    return-void

    .line 78
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onToastShown()V
    .locals 3

    .line 61
    iget-object v0, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$1;->this$0:Lcom/android/server/audio/pad/KeyboardMicKeyHelper;

    invoke-static {v0}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->-$$Nest$fgetmToastStateLock(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 62
    :try_start_0
    const-string v1, "PadAdapter.KeyboardMicKeyHelper"

    const-string v2, "onToastShown() ..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    iget-object v1, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$1;->this$0:Lcom/android/server/audio/pad/KeyboardMicKeyHelper;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->-$$Nest$fputmToastShowing(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;Z)V

    .line 64
    monitor-exit v0

    .line 65
    return-void

    .line 64
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
