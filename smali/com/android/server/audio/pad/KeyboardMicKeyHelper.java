public class com.android.server.audio.pad.KeyboardMicKeyHelper {
	 /* .source "KeyboardMicKeyHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/audio/pad/KeyboardMicKeyHelper$H; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer MSG_MIC_MUTE_CHANGED;
private static final Integer MSG_MIC_SRC_RECORD_UPDATE;
private static final Integer STATE_PENDING_MUTED;
private static final Integer STATE_PENDING_NONE;
private static final Integer STATE_PENDING_UNMUTED;
private static final java.lang.String TAG;
/* # instance fields */
private android.media.AudioManager mAudioManager;
private android.content.Context mContext;
private miuix.appcompat.app.AlertDialog mDialog;
private java.lang.Object mDialogStateLock;
private android.os.Handler mHandler;
private Integer mStatePending;
private android.widget.Toast mToast;
private volatile Boolean mToastShowing;
private java.lang.Object mToastStateLock;
/* # direct methods */
static android.media.AudioManager -$$Nest$fgetmAudioManager ( com.android.server.audio.pad.KeyboardMicKeyHelper p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mAudioManager;
} // .end method
static java.lang.Object -$$Nest$fgetmDialogStateLock ( com.android.server.audio.pad.KeyboardMicKeyHelper p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mDialogStateLock;
} // .end method
static Integer -$$Nest$fgetmStatePending ( com.android.server.audio.pad.KeyboardMicKeyHelper p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mStatePending:I */
} // .end method
static android.widget.Toast -$$Nest$fgetmToast ( com.android.server.audio.pad.KeyboardMicKeyHelper p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mToast;
} // .end method
static java.lang.Object -$$Nest$fgetmToastStateLock ( com.android.server.audio.pad.KeyboardMicKeyHelper p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mToastStateLock;
} // .end method
static void -$$Nest$fputmDialog ( com.android.server.audio.pad.KeyboardMicKeyHelper p0, miuix.appcompat.app.AlertDialog p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mDialog = p1;
	 return;
} // .end method
static void -$$Nest$fputmStatePending ( com.android.server.audio.pad.KeyboardMicKeyHelper p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput p1, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mStatePending:I */
	 return;
} // .end method
static void -$$Nest$fputmToastShowing ( com.android.server.audio.pad.KeyboardMicKeyHelper p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mToastShowing:Z */
	 return;
} // .end method
static java.lang.String -$$Nest$mgetToastString ( com.android.server.audio.pad.KeyboardMicKeyHelper p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->getToastString(Z)Ljava/lang/String; */
} // .end method
public com.android.server.audio.pad.KeyboardMicKeyHelper ( ) {
	 /* .locals 2 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 35 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 27 */
	 /* new-instance v0, Ljava/lang/Object; */
	 /* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
	 this.mToastStateLock = v0;
	 /* .line 28 */
	 /* new-instance v0, Ljava/lang/Object; */
	 /* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
	 this.mDialogStateLock = v0;
	 /* .line 36 */
	 final String v0 = "PadAdapter.KeyboardMicKeyHelper"; // const-string v0, "PadAdapter.KeyboardMicKeyHelper"
	 final String v1 = "KeyboardMicKeyHelper Construct ..."; // const-string v1, "KeyboardMicKeyHelper Construct ..."
	 android.util.Log .d ( v0,v1 );
	 /* .line 37 */
	 this.mContext = p1;
	 /* .line 38 */
	 /* new-instance v0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$H; */
	 (( android.content.Context ) p1 ).getMainLooper ( ); // invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;
	 /* invoke-direct {v0, p0, v1}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$H;-><init>(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;Landroid/os/Looper;)V */
	 this.mHandler = v0;
	 /* .line 39 */
	 v0 = this.mContext;
	 final String v1 = "audio"; // const-string v1, "audio"
	 (( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
	 /* check-cast v0, Landroid/media/AudioManager; */
	 this.mAudioManager = v0;
	 /* .line 40 */
	 return;
} // .end method
private miuix.appcompat.app.AlertDialog createDialog ( ) {
	 /* .locals 5 */
	 /* .line 84 */
	 android.app.ActivityThread .currentActivityThread ( );
	 (( android.app.ActivityThread ) v0 ).getSystemUiContext ( ); // invoke-virtual {v0}, Landroid/app/ActivityThread;->getSystemUiContext()Landroid/app/ContextImpl;
	 /* .line 85 */
	 /* .local v0, "uiContext":Landroid/content/Context; */
	 (( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
	 /* const-string/jumbo v2, "style" */
	 final String v3 = "miuix.stub"; // const-string v3, "miuix.stub"
	 final String v4 = "AlertDialog.Theme.DayNight"; // const-string v4, "AlertDialog.Theme.DayNight"
	 v1 = 	 (( android.content.res.Resources ) v1 ).getIdentifier ( v4, v2, v3 ); // invoke-virtual {v1, v4, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
	 /* .line 87 */
	 /* .local v1, "themeId":I */
	 /* new-instance v2, Lmiuix/appcompat/app/AlertDialog$Builder; */
	 /* invoke-direct {v2, v0, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V */
	 /* .line 88 */
	 /* const v3, 0x110f01cd */
	 /* invoke-direct {p0, v3}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->getString(I)Ljava/lang/String; */
	 (( miuix.appcompat.app.AlertDialog$Builder ) v2 ).setTitle ( v3 ); // invoke-virtual {v2, v3}, Lmiuix/appcompat/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;
	 /* .line 89 */
	 /* const v3, 0x110f01cc */
	 /* invoke-direct {p0, v3}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->getString(I)Ljava/lang/String; */
	 (( miuix.appcompat.app.AlertDialog$Builder ) v2 ).setMessage ( v3 ); // invoke-virtual {v2, v3}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;
	 /* .line 90 */
	 /* const v3, 0x110f01cb */
	 /* invoke-direct {p0, v3}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->getString(I)Ljava/lang/String; */
	 /* new-instance v4, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$3; */
	 /* invoke-direct {v4, p0}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$3;-><init>(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;)V */
	 (( miuix.appcompat.app.AlertDialog$Builder ) v2 ).setPositiveButton ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;
	 /* .line 97 */
	 /* const v3, 0x110f01ca */
	 /* invoke-direct {p0, v3}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->getString(I)Ljava/lang/String; */
	 /* new-instance v4, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$2; */
	 /* invoke-direct {v4, p0}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$2;-><init>(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;)V */
	 (( miuix.appcompat.app.AlertDialog$Builder ) v2 ).setNegativeButton ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lmiuix/appcompat/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;
	 /* .line 104 */
	 (( miuix.appcompat.app.AlertDialog$Builder ) v2 ).create ( ); // invoke-virtual {v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->create()Lmiuix/appcompat/app/AlertDialog;
	 /* .line 105 */
	 /* .local v2, "dialog":Lmiuix/appcompat/app/AlertDialog; */
	 /* new-instance v3, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$4; */
	 /* invoke-direct {v3, p0}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$4;-><init>(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;)V */
	 (( miuix.appcompat.app.AlertDialog ) v2 ).setOnShowListener ( v3 ); // invoke-virtual {v2, v3}, Lmiuix/appcompat/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V
	 /* .line 111 */
	 /* new-instance v3, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$5; */
	 /* invoke-direct {v3, p0}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$5;-><init>(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;)V */
	 (( miuix.appcompat.app.AlertDialog ) v2 ).setOnDismissListener ( v3 ); // invoke-virtual {v2, v3}, Lmiuix/appcompat/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V
	 /* .line 120 */
	 int v3 = 0; // const/4 v3, 0x0
	 (( miuix.appcompat.app.AlertDialog ) v2 ).setCanceledOnTouchOutside ( v3 ); // invoke-virtual {v2, v3}, Lmiuix/appcompat/app/AlertDialog;->setCanceledOnTouchOutside(Z)V
	 /* .line 121 */
	 (( miuix.appcompat.app.AlertDialog ) v2 ).getWindow ( ); // invoke-virtual {v2}, Lmiuix/appcompat/app/AlertDialog;->getWindow()Landroid/view/Window;
	 /* const/16 v4, 0x7d3 */
	 (( android.view.Window ) v3 ).setType ( v4 ); // invoke-virtual {v3, v4}, Landroid/view/Window;->setType(I)V
	 /* .line 122 */
} // .end method
private void dismissEnableMicDialogIfNeed ( ) {
	 /* .locals 2 */
	 /* .line 159 */
	 final String v0 = "PadAdapter.KeyboardMicKeyHelper"; // const-string v0, "PadAdapter.KeyboardMicKeyHelper"
	 final String v1 = "dismissEnableMicDialogIfNeed() ..."; // const-string v1, "dismissEnableMicDialogIfNeed() ..."
	 android.util.Log .d ( v0,v1 );
	 /* .line 160 */
	 v0 = this.mDialogStateLock;
	 /* monitor-enter v0 */
	 /* .line 161 */
	 try { // :try_start_0
		 v1 = this.mDialog;
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 v1 = 			 (( miuix.appcompat.app.AlertDialog ) v1 ).isShowing ( ); // invoke-virtual {v1}, Lmiuix/appcompat/app/AlertDialog;->isShowing()Z
			 if ( v1 != null) { // if-eqz v1, :cond_0
				 /* .line 162 */
				 v1 = this.mDialog;
				 (( miuix.appcompat.app.AlertDialog ) v1 ).dismiss ( ); // invoke-virtual {v1}, Lmiuix/appcompat/app/AlertDialog;->dismiss()V
				 /* .line 164 */
			 } // :cond_0
			 /* monitor-exit v0 */
			 /* .line 165 */
			 return;
			 /* .line 164 */
			 /* :catchall_0 */
			 /* move-exception v1 */
			 /* monitor-exit v0 */
			 /* :try_end_0 */
			 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
			 /* throw v1 */
		 } // .end method
		 private java.lang.String getString ( Integer p0 ) {
			 /* .locals 1 */
			 /* .param p1, "id" # I */
			 /* .line 188 */
			 v0 = this.mContext;
			 (( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
			 (( android.content.res.Resources ) v0 ).getString ( p1 ); // invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
		 } // .end method
		 private java.lang.String getToastString ( Boolean p0 ) {
			 /* .locals 1 */
			 /* .param p1, "muted" # Z */
			 /* .line 183 */
			 if ( p1 != null) { // if-eqz p1, :cond_0
				 /* const v0, 0x110f03c5 */
				 /* invoke-direct {p0, v0}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->getString(I)Ljava/lang/String; */
				 /* .line 184 */
			 } // :cond_0
			 /* const v0, 0x110f03c6 */
			 /* invoke-direct {p0, v0}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->getString(I)Ljava/lang/String; */
			 /* .line 183 */
		 } // :goto_0
	 } // .end method
	 private void initToast ( ) {
		 /* .locals 2 */
		 /* .line 56 */
		 /* new-instance v0, Landroid/widget/Toast; */
		 v1 = this.mContext;
		 /* invoke-direct {v0, v1}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V */
		 this.mToast = v0;
		 /* .line 57 */
		 int v1 = 1; // const/4 v1, 0x1
		 (( android.widget.Toast ) v0 ).setDuration ( v1 ); // invoke-virtual {v0, v1}, Landroid/widget/Toast;->setDuration(I)V
		 /* .line 58 */
		 v0 = this.mToast;
		 /* new-instance v1, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$1; */
		 /* invoke-direct {v1, p0}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$1;-><init>(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;)V */
		 (( android.widget.Toast ) v0 ).addCallback ( v1 ); // invoke-virtual {v0, v1}, Landroid/widget/Toast;->addCallback(Landroid/widget/Toast$Callback;)V
		 /* .line 81 */
		 return;
	 } // .end method
	 private Boolean isMicrophoneAudioSource ( Integer p0 ) {
		 /* .locals 1 */
		 /* .param p1, "source" # I */
		 /* .line 168 */
		 /* packed-switch p1, :pswitch_data_0 */
		 /* .line 178 */
		 /* :pswitch_0 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* .line 176 */
		 /* :pswitch_1 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* nop */
		 /* :pswitch_data_0 */
		 /* .packed-switch 0x0 */
		 /* :pswitch_1 */
		 /* :pswitch_1 */
		 /* :pswitch_0 */
		 /* :pswitch_0 */
		 /* :pswitch_0 */
		 /* :pswitch_1 */
		 /* :pswitch_1 */
		 /* :pswitch_1 */
		 /* :pswitch_0 */
		 /* :pswitch_1 */
		 /* :pswitch_1 */
	 } // .end packed-switch
} // .end method
/* # virtual methods */
public void handleMicrophoneMuteChanged ( Boolean p0 ) {
	 /* .locals 3 */
	 /* .param p1, "muted" # Z */
	 /* .line 43 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "handleMicrophoneMuteChanged(), muted : "; // const-string v1, "handleMicrophoneMuteChanged(), muted : "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v1 = "PadAdapter.KeyboardMicKeyHelper"; // const-string v1, "PadAdapter.KeyboardMicKeyHelper"
	 android.util.Log .d ( v1,v0 );
	 /* .line 44 */
	 v0 = this.mHandler;
	 int v1 = 1; // const/4 v1, 0x1
	 java.lang.Boolean .valueOf ( p1 );
	 (( android.os.Handler ) v0 ).obtainMessage ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
	 (( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
	 /* .line 45 */
	 return;
} // .end method
public void handleRecordEventUpdate ( Integer p0 ) {
	 /* .locals 2 */
	 /* .param p1, "audioSource" # I */
	 /* .line 48 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "handleRecordEventUpdate(), audio source : "; // const-string v1, "handleRecordEventUpdate(), audio source : "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v1 = "PadAdapter.KeyboardMicKeyHelper"; // const-string v1, "PadAdapter.KeyboardMicKeyHelper"
	 android.util.Log .d ( v1,v0 );
	 /* .line 49 */
	 v0 = 	 /* invoke-direct {p0, p1}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->isMicrophoneAudioSource(I)Z */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 50 */
		 v0 = this.mHandler;
		 int v1 = 2; // const/4 v1, 0x2
		 (( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
		 (( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
		 /* .line 52 */
	 } // :cond_0
	 return;
} // .end method
void showEnableMicDialog ( ) {
	 /* .locals 2 */
	 /* .line 149 */
	 final String v0 = "PadAdapter.KeyboardMicKeyHelper"; // const-string v0, "PadAdapter.KeyboardMicKeyHelper"
	 /* const-string/jumbo v1, "showEnableMicDialog() ..." */
	 android.util.Log .d ( v0,v1 );
	 /* .line 150 */
	 v0 = this.mDialogStateLock;
	 /* monitor-enter v0 */
	 /* .line 151 */
	 try { // :try_start_0
		 v1 = this.mDialog;
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 v1 = 			 (( miuix.appcompat.app.AlertDialog ) v1 ).isShowing ( ); // invoke-virtual {v1}, Lmiuix/appcompat/app/AlertDialog;->isShowing()Z
			 /* if-nez v1, :cond_1 */
			 /* .line 152 */
		 } // :cond_0
		 /* invoke-direct {p0}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->createDialog()Lmiuix/appcompat/app/AlertDialog; */
		 this.mDialog = v1;
		 /* .line 153 */
		 (( miuix.appcompat.app.AlertDialog ) v1 ).show ( ); // invoke-virtual {v1}, Lmiuix/appcompat/app/AlertDialog;->show()V
		 /* .line 155 */
	 } // :cond_1
	 /* monitor-exit v0 */
	 /* .line 156 */
	 return;
	 /* .line 155 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
} // .end method
void showMicMuteChanged ( Boolean p0 ) {
	 /* .locals 3 */
	 /* .param p1, "muted" # Z */
	 /* .line 126 */
	 final String v0 = "PadAdapter.KeyboardMicKeyHelper"; // const-string v0, "PadAdapter.KeyboardMicKeyHelper"
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 /* const-string/jumbo v2, "showMicMuteChanged muted : " */
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Log .d ( v0,v1 );
	 /* .line 127 */
	 v0 = this.mToast;
	 /* if-nez v0, :cond_0 */
	 /* .line 128 */
	 /* invoke-direct {p0}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->initToast()V */
	 /* .line 131 */
} // :cond_0
v0 = this.mToastStateLock;
/* monitor-enter v0 */
/* .line 132 */
try { // :try_start_0
	 /* iget-boolean v1, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mToastShowing:Z */
	 if ( v1 != null) { // if-eqz v1, :cond_2
		 /* .line 133 */
		 if ( p1 != null) { // if-eqz p1, :cond_1
			 int v1 = 1; // const/4 v1, 0x1
		 } // :cond_1
		 int v1 = 2; // const/4 v1, 0x2
	 } // :goto_0
	 /* iput v1, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mStatePending:I */
	 /* .line 134 */
	 v1 = this.mToast;
	 (( android.widget.Toast ) v1 ).cancel ( ); // invoke-virtual {v1}, Landroid/widget/Toast;->cancel()V
	 /* .line 136 */
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
/* iput v1, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mStatePending:I */
/* .line 137 */
v1 = this.mToast;
/* invoke-direct {p0, p1}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->getToastString(Z)Ljava/lang/String; */
(( android.widget.Toast ) v1 ).setText ( v2 ); // invoke-virtual {v1, v2}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V
/* .line 138 */
v1 = this.mToast;
(( android.widget.Toast ) v1 ).show ( ); // invoke-virtual {v1}, Landroid/widget/Toast;->show()V
/* .line 140 */
} // :goto_1
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 143 */
/* if-nez p1, :cond_3 */
/* .line 144 */
/* invoke-direct {p0}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->dismissEnableMicDialogIfNeed()V */
/* .line 146 */
} // :cond_3
return;
/* .line 140 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
