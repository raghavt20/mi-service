public class com.android.server.audio.pad.FaceInfo {
	 /* .source "FaceInfo.java" */
	 /* # static fields */
	 private static final Integer AON_SCALE_X;
	 private static final Integer AON_SCALE_Y;
	 private static final java.lang.String PREFIX_FACE_RECTS;
	 private static final java.lang.String PREFIX_VIEW_REGION;
	 private static final Integer RESCALED_VIEW_HEIGHT;
	 private static final Integer RESCALED_VIEW_WIDTH;
	 public static final Integer SRC_AON;
	 public static final Integer SRC_CAMERA;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 public java.lang.String mParamFocusRegion;
	 public java.lang.String mParamViewRegion;
	 private Integer mSource;
	 /* # direct methods */
	 static com.android.server.audio.pad.FaceInfo ( ) {
		 /* .locals 2 */
		 /* .line 19 */
		 /* nop */
		 /* .line 20 */
		 final String v0 = "ro.vendor.audio.aon.xscale"; // const-string v0, "ro.vendor.audio.aon.xscale"
		 int v1 = 0; // const/4 v1, 0x0
		 v0 = 		 android.os.SystemProperties .getInt ( v0,v1 );
		 /* .line 21 */
		 /* nop */
		 /* .line 22 */
		 final String v0 = "ro.vendor.audio.aon.yscale"; // const-string v0, "ro.vendor.audio.aon.yscale"
		 v0 = 		 android.os.SystemProperties .getInt ( v0,v1 );
		 /* .line 21 */
		 return;
	 } // .end method
	 private com.android.server.audio.pad.FaceInfo ( ) {
		 /* .locals 0 */
		 /* .param p1, "source" # I */
		 /* .param p2, "paramViewRegion" # Ljava/lang/String; */
		 /* .param p3, "paramFocusRegion" # Ljava/lang/String; */
		 /* .line 28 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 29 */
		 /* iput p1, p0, Lcom/android/server/audio/pad/FaceInfo;->mSource:I */
		 /* .line 30 */
		 this.mParamViewRegion = p2;
		 /* .line 31 */
		 this.mParamFocusRegion = p3;
		 /* .line 32 */
		 return;
	 } // .end method
	 private static java.lang.String arrayToString ( Integer[] p0 ) {
		 /* .locals 4 */
		 /* .param p0, "arr" # [I */
		 /* .line 165 */
		 if ( p0 != null) { // if-eqz p0, :cond_3
			 /* array-length v0, p0 */
			 /* if-nez v0, :cond_0 */
			 /* .line 168 */
		 } // :cond_0
		 /* array-length v0, p0 */
		 /* .line 169 */
		 /* .local v0, "len":I */
		 /* new-instance v1, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
		 /* .line 170 */
		 /* .local v1, "builder":Ljava/lang/StringBuilder; */
		 int v2 = 0; // const/4 v2, 0x0
		 /* .local v2, "i":I */
	 } // :goto_0
	 /* if-ge v2, v0, :cond_2 */
	 /* .line 171 */
	 /* aget v3, p0, v2 */
	 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 /* .line 172 */
	 /* add-int/lit8 v3, v0, -0x1 */
	 /* if-ge v2, v3, :cond_1 */
	 /* .line 173 */
	 final String v3 = ","; // const-string v3, ","
	 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* .line 170 */
} // :cond_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 176 */
} // .end local v2 # "i":I
} // :cond_2
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 166 */
} // .end local v0 # "len":I
} // .end local v1 # "builder":Ljava/lang/StringBuilder;
} // :cond_3
} // :goto_1
final String v0 = ""; // const-string v0, ""
} // .end method
public static com.android.server.audio.pad.FaceInfo build ( Integer p0, Integer p1, Integer[] p2, Integer[] p3 ) {
/* .locals 23 */
/* .param p0, "src" # I */
/* .param p1, "orientation" # I */
/* .param p2, "viewRegion" # [I */
/* .param p3, "faceRects" # [I */
/* .line 36 */
/* move/from16 v0, p0 */
/* move-object/from16 v1, p2 */
/* move-object/from16 v2, p3 */
int v3 = 0; // const/4 v3, 0x0
if ( v1 != null) { // if-eqz v1, :cond_18
/* array-length v4, v1 */
int v5 = 4; // const/4 v5, 0x4
/* if-ne v4, v5, :cond_18 */
/* if-nez v2, :cond_0 */
/* goto/16 :goto_f */
/* .line 40 */
} // :cond_0
int v4 = 1; // const/4 v4, 0x1
/* if-ne v0, v4, :cond_1 */
int v6 = 6; // const/4 v6, 0x6
} // :cond_1
/* move v6, v5 */
/* .line 41 */
/* .local v6, "dataNumPerFace":I */
} // :goto_0
/* array-length v7, v2 */
/* div-int/2addr v7, v6 */
/* .line 42 */
/* .local v7, "faceNum":I */
/* if-lt v7, v4, :cond_17 */
/* array-length v8, v2 */
/* rem-int/2addr v8, v6 */
if ( v8 != null) { // if-eqz v8, :cond_2
/* goto/16 :goto_e */
/* .line 46 */
} // :cond_2
/* if-ne v0, v4, :cond_3 */
/* .line 47 */
int v7 = 1; // const/4 v7, 0x1
/* .line 51 */
} // :cond_3
v3 = /* invoke-static/range {p1 ..p1}, Lcom/android/server/audio/pad/FaceInfo;->orientationToScene(I)I */
/* .line 52 */
/* .local v3, "scene":I */
int v8 = 0; // const/4 v8, 0x0
/* .line 53 */
/* .local v8, "curX1":I */
int v9 = 0; // const/4 v9, 0x0
/* .line 54 */
/* .local v9, "curY1":I */
int v10 = 0; // const/4 v10, 0x0
/* .line 55 */
/* .local v10, "curX2":I */
int v11 = 0; // const/4 v11, 0x0
/* .line 56 */
/* .local v11, "curY2":I */
int v12 = 2; // const/4 v12, 0x2
/* aget v13, v1, v12 */
/* .line 57 */
/* .local v13, "width":I */
int v14 = 3; // const/4 v14, 0x3
/* aget v15, v1, v14 */
/* .line 58 */
/* .local v15, "height":I */
/* mul-int/lit8 v5, v7, 0x4 */
/* new-array v5, v5, [I */
/* .line 60 */
/* .local v5, "rectsExtracted":[I */
/* const/16 v16, 0x0 */
/* move/from16 v14, v16 */
/* .local v14, "faceId":I */
} // :goto_1
/* const/16 v18, 0x0 */
/* if-ge v14, v7, :cond_15 */
/* .line 61 */
/* if-ne v0, v4, :cond_4 */
/* .line 62 */
/* mul-int v19, v14, v6 */
/* const/16 v17, 0x2 */
/* add-int/lit8 v19, v19, 0x2 */
/* aget v8, v2, v19 */
/* .line 63 */
/* mul-int v19, v14, v6 */
/* const/16 v16, 0x3 */
/* add-int/lit8 v19, v19, 0x3 */
/* aget v9, v2, v19 */
/* .line 64 */
/* mul-int v19, v14, v6 */
/* add-int/lit8 v19, v19, 0x2 */
/* aget v19, v2, v19 */
/* mul-int v20, v14, v6 */
/* add-int/lit8 v20, v20, 0x0 */
/* aget v20, v2, v20 */
/* add-int v19, v19, v20 */
/* .line 66 */
} // .end local v10 # "curX2":I
/* .local v19, "curX2":I */
/* mul-int v10, v14, v6 */
/* const/16 v16, 0x3 */
/* add-int/lit8 v10, v10, 0x3 */
/* aget v10, v2, v10 */
/* mul-int v20, v14, v6 */
/* add-int/lit8 v20, v20, 0x1 */
/* aget v20, v2, v20 */
/* add-int v10, v10, v20 */
/* move v11, v10 */
/* move/from16 v10, v19 */
} // .end local v11 # "curY2":I
/* .local v10, "curY2":I */
/* .line 68 */
} // .end local v19 # "curX2":I
/* .local v10, "curX2":I */
/* .restart local v11 # "curY2":I */
} // :cond_4
int v12 = 2; // const/4 v12, 0x2
/* if-ne v0, v12, :cond_5 */
/* .line 69 */
/* mul-int v17, v14, v6 */
/* add-int/lit8 v17, v17, 0x0 */
/* aget v8, v2, v17 */
/* .line 70 */
/* mul-int v17, v14, v6 */
/* add-int/lit8 v17, v17, 0x1 */
/* aget v9, v2, v17 */
/* .line 71 */
/* mul-int v17, v14, v6 */
/* add-int/lit8 v20, v17, 0x2 */
/* aget v10, v2, v20 */
/* .line 72 */
/* mul-int v12, v14, v6 */
/* const/16 v16, 0x3 */
/* add-int/lit8 v12, v12, 0x3 */
/* aget v11, v2, v12 */
/* .line 75 */
} // :cond_5
} // :goto_2
/* if-eq v3, v4, :cond_7 */
int v12 = 2; // const/4 v12, 0x2
/* if-ne v3, v12, :cond_6 */
/* .line 81 */
} // :cond_6
/* mul-int/lit8 v12, v14, 0x4 */
/* add-int/lit8 v12, v12, 0x0 */
/* aput v9, v5, v12 */
/* .line 82 */
/* mul-int/lit8 v12, v14, 0x4 */
/* add-int/2addr v12, v4 */
/* aput v8, v5, v12 */
/* .line 83 */
/* mul-int/lit8 v12, v14, 0x4 */
/* const/16 v17, 0x2 */
/* add-int/lit8 v12, v12, 0x2 */
/* aput v11, v5, v12 */
/* .line 84 */
/* mul-int/lit8 v12, v14, 0x4 */
/* const/16 v16, 0x3 */
/* add-int/lit8 v12, v12, 0x3 */
/* aput v10, v5, v12 */
int v4 = 3; // const/4 v4, 0x3
/* .line 76 */
} // :cond_7
} // :goto_3
/* mul-int/lit8 v12, v14, 0x4 */
/* add-int/lit8 v12, v12, 0x0 */
/* sub-int v20, v15, v11 */
/* aput v20, v5, v12 */
/* .line 77 */
/* mul-int/lit8 v12, v14, 0x4 */
/* add-int/2addr v12, v4 */
/* sub-int v20, v13, v10 */
/* aput v20, v5, v12 */
/* .line 78 */
/* mul-int/lit8 v12, v14, 0x4 */
/* const/16 v17, 0x2 */
/* add-int/lit8 v12, v12, 0x2 */
/* sub-int v20, v15, v9 */
/* aput v20, v5, v12 */
/* .line 79 */
/* mul-int/lit8 v12, v14, 0x4 */
int v4 = 3; // const/4 v4, 0x3
/* add-int/2addr v12, v4 */
/* sub-int v16, v13, v8 */
/* aput v16, v5, v12 */
/* .line 86 */
} // :goto_4
int v12 = 1; // const/4 v12, 0x1
/* if-ne v0, v12, :cond_14 */
/* .line 87 */
/* if-eq v3, v12, :cond_f */
/* if-ne v3, v4, :cond_8 */
/* goto/16 :goto_9 */
/* .line 99 */
} // :cond_8
int v4 = 2; // const/4 v4, 0x2
/* if-eq v3, v4, :cond_a */
int v4 = 4; // const/4 v4, 0x4
/* if-ne v3, v4, :cond_9 */
} // :cond_9
} // :goto_5
/* goto/16 :goto_c */
/* .line 100 */
} // :cond_a
} // :goto_6
/* mul-int/lit8 v4, v14, 0x4 */
/* add-int/lit8 v4, v4, 0x0 */
/* aget v4, v5, v4 */
/* mul-int/lit8 v12, v14, 0x4 */
/* const/16 v17, 0x2 */
/* add-int/lit8 v12, v12, 0x2 */
/* aget v12, v5, v12 */
/* add-int/2addr v4, v12 */
/* div-int/lit8 v4, v4, 0x2 */
/* .line 102 */
/* .local v4, "center":I */
/* div-int/lit8 v12, v15, 0x2 */
/* sub-int/2addr v12, v4 */
/* mul-int v12, v12, v21 */
/* div-int/lit8 v21, v15, 0x2 */
/* div-int v12, v12, v21 */
/* .line 103 */
/* .local v12, "delta":I */
/* mul-int/lit8 v21, v14, 0x4 */
/* add-int/lit8 v21, v21, 0x0 */
/* mul-int/lit8 v22, v14, 0x4 */
/* add-int/lit8 v22, v22, 0x0 */
/* aget v22, v5, v22 */
/* add-int v22, v22, v12 */
/* aput v22, v5, v21 */
/* .line 104 */
/* mul-int/lit8 v21, v14, 0x4 */
/* const/16 v17, 0x2 */
/* add-int/lit8 v21, v21, 0x2 */
/* mul-int/lit8 v22, v14, 0x4 */
/* add-int/lit8 v22, v22, 0x2 */
/* aget v22, v5, v22 */
/* add-int v22, v22, v12 */
/* aput v22, v5, v21 */
/* .line 105 */
/* mul-int/lit8 v21, v14, 0x4 */
/* add-int/lit8 v21, v21, 0x0 */
/* mul-int/lit8 v22, v14, 0x4 */
/* add-int/lit8 v22, v22, 0x0 */
/* aget v22, v5, v22 */
/* if-gez v22, :cond_b */
/* move/from16 v2, v18 */
/* .line 106 */
} // :cond_b
/* mul-int/lit8 v22, v14, 0x4 */
/* add-int/lit8 v22, v22, 0x0 */
/* aget v2, v5, v22 */
/* if-le v2, v15, :cond_c */
/* move v2, v15 */
/* .line 107 */
} // :cond_c
/* mul-int/lit8 v2, v14, 0x4 */
/* add-int/lit8 v2, v2, 0x0 */
/* aget v2, v5, v2 */
} // :goto_7
/* aput v2, v5, v21 */
/* .line 108 */
/* mul-int/lit8 v2, v14, 0x4 */
/* const/16 v17, 0x2 */
/* add-int/lit8 v2, v2, 0x2 */
/* mul-int/lit8 v21, v14, 0x4 */
/* add-int/lit8 v21, v21, 0x2 */
/* aget v21, v5, v21 */
/* if-gez v21, :cond_d */
/* move/from16 v22, v4 */
/* move/from16 v4, v18 */
/* .line 109 */
} // :cond_d
/* mul-int/lit8 v21, v14, 0x4 */
/* add-int/lit8 v21, v21, 0x2 */
/* move/from16 v22, v4 */
} // .end local v4 # "center":I
/* .local v22, "center":I */
/* aget v4, v5, v21 */
/* if-le v4, v15, :cond_e */
/* move v4, v15 */
/* .line 110 */
} // :cond_e
/* mul-int/lit8 v4, v14, 0x4 */
/* add-int/lit8 v4, v4, 0x2 */
/* aget v4, v5, v4 */
} // :goto_8
/* aput v4, v5, v2 */
/* goto/16 :goto_c */
/* .line 88 */
} // .end local v12 # "delta":I
} // .end local v22 # "center":I
} // :cond_f
} // :goto_9
/* mul-int/lit8 v2, v14, 0x4 */
int v4 = 1; // const/4 v4, 0x1
/* add-int/2addr v2, v4 */
/* aget v2, v5, v2 */
/* mul-int/lit8 v4, v14, 0x4 */
int v12 = 3; // const/4 v12, 0x3
/* add-int/2addr v4, v12 */
/* aget v4, v5, v4 */
/* add-int/2addr v2, v4 */
int v4 = 2; // const/4 v4, 0x2
/* div-int/2addr v2, v4 */
/* .line 90 */
/* .local v2, "center":I */
/* div-int/lit8 v4, v13, 0x2 */
/* sub-int/2addr v4, v2 */
/* mul-int/2addr v4, v12 */
/* div-int/lit8 v12, v13, 0x2 */
/* div-int/2addr v4, v12 */
/* .line 91 */
/* .local v4, "delta":I */
/* mul-int/lit8 v12, v14, 0x4 */
/* const/16 v20, 0x1 */
/* add-int/lit8 v12, v12, 0x1 */
/* mul-int/lit8 v21, v14, 0x4 */
/* add-int/lit8 v21, v21, 0x1 */
/* aget v21, v5, v21 */
/* add-int v21, v21, v4 */
/* aput v21, v5, v12 */
/* .line 92 */
/* mul-int/lit8 v12, v14, 0x4 */
/* const/16 v16, 0x3 */
/* add-int/lit8 v12, v12, 0x3 */
/* mul-int/lit8 v21, v14, 0x4 */
/* add-int/lit8 v21, v21, 0x3 */
/* aget v21, v5, v21 */
/* add-int v21, v21, v4 */
/* aput v21, v5, v12 */
/* .line 93 */
/* mul-int/lit8 v12, v14, 0x4 */
/* const/16 v20, 0x1 */
/* add-int/lit8 v12, v12, 0x1 */
/* mul-int/lit8 v21, v14, 0x4 */
/* add-int/lit8 v21, v21, 0x1 */
/* aget v21, v5, v21 */
/* if-gez v21, :cond_10 */
/* move/from16 v22, v2 */
/* move/from16 v2, v18 */
/* .line 94 */
} // :cond_10
/* mul-int/lit8 v21, v14, 0x4 */
/* add-int/lit8 v21, v21, 0x1 */
/* move/from16 v22, v2 */
} // .end local v2 # "center":I
/* .restart local v22 # "center":I */
/* aget v2, v5, v21 */
/* if-le v2, v13, :cond_11 */
/* move v2, v13 */
/* .line 95 */
} // :cond_11
/* mul-int/lit8 v2, v14, 0x4 */
/* add-int/lit8 v2, v2, 0x1 */
/* aget v2, v5, v2 */
} // :goto_a
/* aput v2, v5, v12 */
/* .line 96 */
/* mul-int/lit8 v2, v14, 0x4 */
int v12 = 3; // const/4 v12, 0x3
/* add-int/2addr v2, v12 */
/* mul-int/lit8 v16, v14, 0x4 */
/* add-int/lit8 v16, v16, 0x3 */
/* aget v16, v5, v16 */
/* if-gez v16, :cond_12 */
/* move/from16 v12, v18 */
/* .line 97 */
} // :cond_12
/* mul-int/lit8 v16, v14, 0x4 */
/* add-int/lit8 v16, v16, 0x3 */
/* aget v12, v5, v16 */
/* if-le v12, v13, :cond_13 */
/* move v12, v13 */
/* .line 98 */
} // :cond_13
/* mul-int/lit8 v12, v14, 0x4 */
/* const/16 v16, 0x3 */
/* add-int/lit8 v12, v12, 0x3 */
/* aget v12, v5, v12 */
} // :goto_b
/* aput v12, v5, v2 */
} // .end local v4 # "delta":I
} // .end local v22 # "center":I
/* goto/16 :goto_5 */
/* .line 112 */
} // :goto_c
/* mul-int/lit8 v2, v14, 0x4 */
/* add-int/lit8 v2, v2, 0x0 */
/* mul-int/lit8 v4, v14, 0x4 */
/* add-int/lit8 v4, v4, 0x0 */
/* aget v4, v5, v4 */
/* const/16 v12, 0x990 */
/* mul-int/2addr v4, v12 */
/* div-int/2addr v4, v15 */
/* aput v4, v5, v2 */
/* .line 114 */
/* mul-int/lit8 v2, v14, 0x4 */
int v4 = 1; // const/4 v4, 0x1
/* add-int/2addr v2, v4 */
/* mul-int/lit8 v12, v14, 0x4 */
/* add-int/2addr v12, v4 */
/* aget v4, v5, v12 */
/* const/16 v12, 0xcc0 */
/* mul-int/2addr v4, v12 */
/* div-int/2addr v4, v13 */
/* aput v4, v5, v2 */
/* .line 116 */
/* mul-int/lit8 v2, v14, 0x4 */
int v4 = 2; // const/4 v4, 0x2
/* add-int/2addr v2, v4 */
/* mul-int/lit8 v12, v14, 0x4 */
/* add-int/2addr v12, v4 */
/* aget v4, v5, v12 */
/* const/16 v12, 0x990 */
/* mul-int/2addr v4, v12 */
/* div-int/2addr v4, v15 */
/* aput v4, v5, v2 */
/* .line 118 */
/* mul-int/lit8 v2, v14, 0x4 */
int v4 = 3; // const/4 v4, 0x3
/* add-int/2addr v2, v4 */
/* mul-int/lit8 v12, v14, 0x4 */
/* add-int/2addr v12, v4 */
/* aget v4, v5, v12 */
/* const/16 v12, 0xcc0 */
/* mul-int/2addr v4, v12 */
/* div-int/2addr v4, v13 */
/* aput v4, v5, v2 */
/* .line 60 */
} // :cond_14
/* add-int/lit8 v14, v14, 0x1 */
/* move-object/from16 v2, p3 */
int v4 = 1; // const/4 v4, 0x1
int v12 = 2; // const/4 v12, 0x2
/* goto/16 :goto_1 */
/* .line 123 */
} // .end local v14 # "faceId":I
} // :cond_15
int v2 = 4; // const/4 v2, 0x4
/* new-array v2, v2, [I */
/* .line 124 */
/* .local v2, "regionExtracted":[I */
/* aget v4, v1, v18 */
/* aput v4, v2, v18 */
/* .line 125 */
int v4 = 1; // const/4 v4, 0x1
/* aget v12, v1, v4 */
/* aput v12, v2, v4 */
/* .line 126 */
/* if-ne v0, v4, :cond_16 */
/* .line 127 */
/* const/16 v4, 0x990 */
int v12 = 2; // const/4 v12, 0x2
/* aput v4, v2, v12 */
/* .line 128 */
/* const/16 v4, 0xcc0 */
int v14 = 3; // const/4 v14, 0x3
/* aput v4, v2, v14 */
/* .line 130 */
} // :cond_16
int v12 = 2; // const/4 v12, 0x2
int v14 = 3; // const/4 v14, 0x3
/* aget v4, v1, v14 */
/* aput v4, v2, v12 */
/* .line 131 */
/* aget v4, v1, v12 */
/* aput v4, v2, v14 */
/* .line 135 */
} // :goto_d
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v12 = "fvsam_voip_tx=ViewRegion@"; // const-string v12, "fvsam_voip_tx=ViewRegion@"
(( java.lang.StringBuilder ) v4 ).append ( v12 ); // invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 136 */
final String v12 = ","; // const-string v12, ","
(( java.lang.StringBuilder ) v4 ).append ( v12 ); // invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.audio.pad.FaceInfo .arrayToString ( v2 );
(( java.lang.StringBuilder ) v4 ).append ( v14 ); // invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 137 */
/* .local v4, "paramViewRegion":Ljava/lang/String; */
/* new-instance v14, Ljava/lang/StringBuilder; */
/* invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "fvsam_voip_tx=FocusRegion@"; // const-string v1, "fvsam_voip_tx=FocusRegion@"
(( java.lang.StringBuilder ) v14 ).append ( v1 ); // invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 138 */
(( java.lang.StringBuilder ) v1 ).append ( v12 ); // invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.audio.pad.FaceInfo .arrayToString ( v5 );
(( java.lang.StringBuilder ) v1 ).append ( v12 ); // invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 140 */
/* .local v1, "paramFocusRegion":Ljava/lang/String; */
/* new-instance v12, Lcom/android/server/audio/pad/FaceInfo; */
/* invoke-direct {v12, v0, v4, v1}, Lcom/android/server/audio/pad/FaceInfo;-><init>(ILjava/lang/String;Ljava/lang/String;)V */
/* .line 43 */
} // .end local v1 # "paramFocusRegion":Ljava/lang/String;
} // .end local v2 # "regionExtracted":[I
} // .end local v3 # "scene":I
} // .end local v4 # "paramViewRegion":Ljava/lang/String;
} // .end local v5 # "rectsExtracted":[I
} // .end local v8 # "curX1":I
} // .end local v9 # "curY1":I
} // .end local v10 # "curX2":I
} // .end local v11 # "curY2":I
} // .end local v13 # "width":I
} // .end local v15 # "height":I
} // :cond_17
} // :goto_e
/* .line 37 */
} // .end local v6 # "dataNumPerFace":I
} // .end local v7 # "faceNum":I
} // :cond_18
} // :goto_f
} // .end method
private static Integer orientationToScene ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "orientation" # I */
/* .line 144 */
int v0 = -1; // const/4 v0, -0x1
/* .line 145 */
/* .local v0, "scene":I */
/* packed-switch p0, :pswitch_data_0 */
/* .line 156 */
/* :pswitch_0 */
int v0 = 3; // const/4 v0, 0x3
/* .line 157 */
/* .line 153 */
/* :pswitch_1 */
int v0 = 4; // const/4 v0, 0x4
/* .line 154 */
/* .line 150 */
/* :pswitch_2 */
int v0 = 1; // const/4 v0, 0x1
/* .line 151 */
/* .line 147 */
/* :pswitch_3 */
int v0 = 2; // const/4 v0, 0x2
/* .line 148 */
/* nop */
/* .line 161 */
} // :goto_0
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private java.lang.String sourceToString ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "source" # I */
/* .line 180 */
/* const-string/jumbo v0, "unknown" */
/* .line 181 */
/* .local v0, "srcStr":Ljava/lang/String; */
/* packed-switch p1, :pswitch_data_0 */
/* .line 186 */
/* :pswitch_0 */
final String v0 = "camera"; // const-string v0, "camera"
/* .line 187 */
/* .line 183 */
/* :pswitch_1 */
final String v0 = "aon"; // const-string v0, "aon"
/* .line 184 */
/* nop */
/* .line 191 */
} // :goto_0
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
/* # virtual methods */
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 196 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "FaceInfo : mSource : "; // const-string v1, "FaceInfo : mSource : "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/audio/pad/FaceInfo;->mSource:I */
/* invoke-direct {p0, v1}, Lcom/android/server/audio/pad/FaceInfo;->sourceToString(I)Ljava/lang/String; */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", mParamViewRegion : "; // const-string v1, ", mParamViewRegion : "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mParamViewRegion;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", mParamFocusRegion : "; // const-string v1, ", mParamFocusRegion : "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mParamFocusRegion;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
