.class public Lcom/android/server/audio/pad/PadAdapter;
.super Ljava/lang/Object;
.source "PadAdapter.java"


# static fields
.field private static final CONFIG_PROP:I

.field public static final ENABLE:Z

.field private static final FLAG_KEYBOARD_MIC_KEY_ENABLE:I = 0x4

.field private static final FLAG_PAD_ADAPTER_ENABLE:I = 0x1

.field private static final FLAG_VOIP_FOCUS_ENABLE:I = 0x2

.field private static final TAG:Ljava/lang/String; = "AudioService.PadAdapter"


# instance fields
.field private final MIC_KEY_ENABLE:Z

.field private final VOIP_FOCUS_ENABLE:Z

.field private mMicKeyHelper:Lcom/android/server/audio/pad/KeyboardMicKeyHelper;

.field private mVoipFocusHelper:Lcom/android/server/audio/pad/VoipFocusHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 13
    nop

    .line 14
    const-string v0, "ro.vendor.audio.pad.adapter"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/server/audio/pad/PadAdapter;->CONFIG_PROP:I

    .line 17
    const/4 v2, 0x1

    and-int/2addr v0, v2

    if-eqz v0, :cond_0

    move v1, v2

    :cond_0
    sput-boolean v1, Lcom/android/server/audio/pad/PadAdapter;->ENABLE:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    sget v0, Lcom/android/server/audio/pad/PadAdapter;->CONFIG_PROP:I

    and-int/lit8 v1, v0, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    move v1, v3

    :goto_0
    iput-boolean v1, p0, Lcom/android/server/audio/pad/PadAdapter;->VOIP_FOCUS_ENABLE:Z

    .line 19
    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move v2, v3

    :goto_1
    iput-boolean v2, p0, Lcom/android/server/audio/pad/PadAdapter;->MIC_KEY_ENABLE:Z

    .line 25
    const-string v0, "AudioService.PadAdapter"

    const-string v3, "PadAdapter Construct ..."

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 26
    if-eqz v1, :cond_2

    .line 27
    new-instance v0, Lcom/android/server/audio/pad/VoipFocusHelper;

    invoke-direct {v0, p1}, Lcom/android/server/audio/pad/VoipFocusHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/audio/pad/PadAdapter;->mVoipFocusHelper:Lcom/android/server/audio/pad/VoipFocusHelper;

    .line 30
    :cond_2
    if-eqz v2, :cond_3

    .line 31
    new-instance v0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;

    invoke-direct {v0, p1}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/audio/pad/PadAdapter;->mMicKeyHelper:Lcom/android/server/audio/pad/KeyboardMicKeyHelper;

    .line 33
    :cond_3
    return-void
.end method


# virtual methods
.method public handleAudioModeUpdate(I)V
    .locals 1
    .param p1, "mode"    # I

    .line 36
    iget-boolean v0, p0, Lcom/android/server/audio/pad/PadAdapter;->VOIP_FOCUS_ENABLE:Z

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/android/server/audio/pad/PadAdapter;->mVoipFocusHelper:Lcom/android/server/audio/pad/VoipFocusHelper;

    invoke-virtual {v0, p1}, Lcom/android/server/audio/pad/VoipFocusHelper;->handleAudioModeUpdate(I)V

    .line 39
    :cond_0
    return-void
.end method

.method public handleMeetingModeUpdate(Ljava/lang/String;)V
    .locals 1
    .param p1, "parameter"    # Ljava/lang/String;

    .line 54
    iget-boolean v0, p0, Lcom/android/server/audio/pad/PadAdapter;->VOIP_FOCUS_ENABLE:Z

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/android/server/audio/pad/PadAdapter;->mVoipFocusHelper:Lcom/android/server/audio/pad/VoipFocusHelper;

    invoke-virtual {v0, p1}, Lcom/android/server/audio/pad/VoipFocusHelper;->handleMeetingModeUpdate(Ljava/lang/String;)V

    .line 57
    :cond_0
    return-void
.end method

.method public handleMicrophoneMuteChanged(Z)V
    .locals 1
    .param p1, "muted"    # Z

    .line 42
    iget-boolean v0, p0, Lcom/android/server/audio/pad/PadAdapter;->MIC_KEY_ENABLE:Z

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/android/server/audio/pad/PadAdapter;->mMicKeyHelper:Lcom/android/server/audio/pad/KeyboardMicKeyHelper;

    invoke-virtual {v0, p1}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->handleMicrophoneMuteChanged(Z)V

    .line 45
    :cond_0
    return-void
.end method

.method public handleRecordEventUpdate(I)V
    .locals 1
    .param p1, "audioSource"    # I

    .line 48
    iget-boolean v0, p0, Lcom/android/server/audio/pad/PadAdapter;->MIC_KEY_ENABLE:Z

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/android/server/audio/pad/PadAdapter;->mMicKeyHelper:Lcom/android/server/audio/pad/KeyboardMicKeyHelper;

    invoke-virtual {v0, p1}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->handleRecordEventUpdate(I)V

    .line 51
    :cond_0
    return-void
.end method
