class com.android.server.audio.pad.VoipFocusHelper$H extends android.os.Handler {
	 /* .source "VoipFocusHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/audio/pad/VoipFocusHelper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "H" */
} // .end annotation
/* # instance fields */
final com.android.server.audio.pad.VoipFocusHelper this$0; //synthetic
/* # direct methods */
public com.android.server.audio.pad.VoipFocusHelper$H ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 468 */
this.this$0 = p1;
/* .line 469 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 470 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 2 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 474 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 482 */
/* :pswitch_0 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.android.server.audio.pad.VoipFocusHelper .-$$Nest$monFaceInfoUpdate ( v0,v1 );
/* .line 479 */
/* :pswitch_1 */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Lcom/android/server/audio/pad/FaceInfo; */
com.android.server.audio.pad.VoipFocusHelper .-$$Nest$monFaceInfoUpdate ( v0,v1 );
/* .line 480 */
/* .line 476 */
/* :pswitch_2 */
v0 = this.this$0;
com.android.server.audio.pad.VoipFocusHelper .-$$Nest$monFaceSrcUpdate ( v0 );
/* .line 477 */
/* nop */
/* .line 485 */
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
