.class public Lcom/android/server/audio/pad/KeyboardMicKeyHelper;
.super Ljava/lang/Object;
.source "KeyboardMicKeyHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/audio/pad/KeyboardMicKeyHelper$H;
    }
.end annotation


# static fields
.field private static final MSG_MIC_MUTE_CHANGED:I = 0x1

.field private static final MSG_MIC_SRC_RECORD_UPDATE:I = 0x2

.field private static final STATE_PENDING_MUTED:I = 0x1

.field private static final STATE_PENDING_NONE:I = 0x0

.field private static final STATE_PENDING_UNMUTED:I = 0x2

.field private static final TAG:Ljava/lang/String; = "PadAdapter.KeyboardMicKeyHelper"


# instance fields
.field private mAudioManager:Landroid/media/AudioManager;

.field private mContext:Landroid/content/Context;

.field private mDialog:Lmiuix/appcompat/app/AlertDialog;

.field private mDialogStateLock:Ljava/lang/Object;

.field private mHandler:Landroid/os/Handler;

.field private mStatePending:I

.field private mToast:Landroid/widget/Toast;

.field private volatile mToastShowing:Z

.field private mToastStateLock:Ljava/lang/Object;


# direct methods
.method static bridge synthetic -$$Nest$fgetmAudioManager(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;)Landroid/media/AudioManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mAudioManager:Landroid/media/AudioManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDialogStateLock(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mDialogStateLock:Ljava/lang/Object;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmStatePending(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;)I
    .locals 0

    iget p0, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mStatePending:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmToast(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;)Landroid/widget/Toast;
    .locals 0

    iget-object p0, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mToast:Landroid/widget/Toast;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmToastStateLock(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mToastStateLock:Ljava/lang/Object;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmDialog(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;Lmiuix/appcompat/app/AlertDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mDialog:Lmiuix/appcompat/app/AlertDialog;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmStatePending(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mStatePending:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmToastShowing(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mToastShowing:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetToastString(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;Z)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->getToastString(Z)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mToastStateLock:Ljava/lang/Object;

    .line 28
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mDialogStateLock:Ljava/lang/Object;

    .line 36
    const-string v0, "PadAdapter.KeyboardMicKeyHelper"

    const-string v1, "KeyboardMicKeyHelper Construct ..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    iput-object p1, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mContext:Landroid/content/Context;

    .line 38
    new-instance v0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$H;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$H;-><init>(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mHandler:Landroid/os/Handler;

    .line 39
    iget-object v0, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mAudioManager:Landroid/media/AudioManager;

    .line 40
    return-void
.end method

.method private createDialog()Lmiuix/appcompat/app/AlertDialog;
    .locals 5

    .line 84
    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActivityThread;->getSystemUiContext()Landroid/app/ContextImpl;

    move-result-object v0

    .line 85
    .local v0, "uiContext":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string/jumbo v2, "style"

    const-string v3, "miuix.stub"

    const-string v4, "AlertDialog.Theme.DayNight"

    invoke-virtual {v1, v4, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 87
    .local v1, "themeId":I
    new-instance v2, Lmiuix/appcompat/app/AlertDialog$Builder;

    invoke-direct {v2, v0, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 88
    const v3, 0x110f01cd

    invoke-direct {p0, v3}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lmiuix/appcompat/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v2

    .line 89
    const v3, 0x110f01cc

    invoke-direct {p0, v3}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v2

    .line 90
    const v3, 0x110f01cb

    invoke-direct {p0, v3}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$3;

    invoke-direct {v4, p0}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$3;-><init>(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;)V

    invoke-virtual {v2, v3, v4}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v2

    .line 97
    const v3, 0x110f01ca

    invoke-direct {p0, v3}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$2;

    invoke-direct {v4, p0}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$2;-><init>(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;)V

    invoke-virtual {v2, v3, v4}, Lmiuix/appcompat/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v2

    .line 104
    invoke-virtual {v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->create()Lmiuix/appcompat/app/AlertDialog;

    move-result-object v2

    .line 105
    .local v2, "dialog":Lmiuix/appcompat/app/AlertDialog;
    new-instance v3, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$4;

    invoke-direct {v3, p0}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$4;-><init>(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;)V

    invoke-virtual {v2, v3}, Lmiuix/appcompat/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 111
    new-instance v3, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$5;

    invoke-direct {v3, p0}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$5;-><init>(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;)V

    invoke-virtual {v2, v3}, Lmiuix/appcompat/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 120
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lmiuix/appcompat/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 121
    invoke-virtual {v2}, Lmiuix/appcompat/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/16 v4, 0x7d3

    invoke-virtual {v3, v4}, Landroid/view/Window;->setType(I)V

    .line 122
    return-object v2
.end method

.method private dismissEnableMicDialogIfNeed()V
    .locals 2

    .line 159
    const-string v0, "PadAdapter.KeyboardMicKeyHelper"

    const-string v1, "dismissEnableMicDialogIfNeed() ..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    iget-object v0, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mDialogStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 161
    :try_start_0
    iget-object v1, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mDialog:Lmiuix/appcompat/app/AlertDialog;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lmiuix/appcompat/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 162
    iget-object v1, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mDialog:Lmiuix/appcompat/app/AlertDialog;

    invoke-virtual {v1}, Lmiuix/appcompat/app/AlertDialog;->dismiss()V

    .line 164
    :cond_0
    monitor-exit v0

    .line 165
    return-void

    .line 164
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getString(I)Ljava/lang/String;
    .locals 1
    .param p1, "id"    # I

    .line 188
    iget-object v0, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getToastString(Z)Ljava/lang/String;
    .locals 1
    .param p1, "muted"    # Z

    .line 183
    if-eqz p1, :cond_0

    const v0, 0x110f03c5

    invoke-direct {p0, v0}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 184
    :cond_0
    const v0, 0x110f03c6

    invoke-direct {p0, v0}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 183
    :goto_0
    return-object v0
.end method

.method private initToast()V
    .locals 2

    .line 56
    new-instance v0, Landroid/widget/Toast;

    iget-object v1, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mToast:Landroid/widget/Toast;

    .line 57
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setDuration(I)V

    .line 58
    iget-object v0, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mToast:Landroid/widget/Toast;

    new-instance v1, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$1;

    invoke-direct {v1, p0}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper$1;-><init>(Lcom/android/server/audio/pad/KeyboardMicKeyHelper;)V

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->addCallback(Landroid/widget/Toast$Callback;)V

    .line 81
    return-void
.end method

.method private isMicrophoneAudioSource(I)Z
    .locals 1
    .param p1, "source"    # I

    .line 168
    packed-switch p1, :pswitch_data_0

    .line 178
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 176
    :pswitch_1
    const/4 v0, 0x1

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public handleMicrophoneMuteChanged(Z)V
    .locals 3
    .param p1, "muted"    # Z

    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "handleMicrophoneMuteChanged(), muted : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PadAdapter.KeyboardMicKeyHelper"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    iget-object v0, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 45
    return-void
.end method

.method public handleRecordEventUpdate(I)V
    .locals 2
    .param p1, "audioSource"    # I

    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "handleRecordEventUpdate(), audio source : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PadAdapter.KeyboardMicKeyHelper"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    invoke-direct {p0, p1}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->isMicrophoneAudioSource(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 52
    :cond_0
    return-void
.end method

.method showEnableMicDialog()V
    .locals 2

    .line 149
    const-string v0, "PadAdapter.KeyboardMicKeyHelper"

    const-string/jumbo v1, "showEnableMicDialog() ..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    iget-object v0, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mDialogStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 151
    :try_start_0
    iget-object v1, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mDialog:Lmiuix/appcompat/app/AlertDialog;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lmiuix/appcompat/app/AlertDialog;->isShowing()Z

    move-result v1

    if-nez v1, :cond_1

    .line 152
    :cond_0
    invoke-direct {p0}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->createDialog()Lmiuix/appcompat/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mDialog:Lmiuix/appcompat/app/AlertDialog;

    .line 153
    invoke-virtual {v1}, Lmiuix/appcompat/app/AlertDialog;->show()V

    .line 155
    :cond_1
    monitor-exit v0

    .line 156
    return-void

    .line 155
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method showMicMuteChanged(Z)V
    .locals 3
    .param p1, "muted"    # Z

    .line 126
    const-string v0, "PadAdapter.KeyboardMicKeyHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "showMicMuteChanged muted : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    iget-object v0, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mToast:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 128
    invoke-direct {p0}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->initToast()V

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mToastStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 132
    :try_start_0
    iget-boolean v1, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mToastShowing:Z

    if-eqz v1, :cond_2

    .line 133
    if-eqz p1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x2

    :goto_0
    iput v1, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mStatePending:I

    .line 134
    iget-object v1, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->cancel()V

    goto :goto_1

    .line 136
    :cond_2
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mStatePending:I

    .line 137
    iget-object v1, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mToast:Landroid/widget/Toast;

    invoke-direct {p0, p1}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->getToastString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 138
    iget-object v1, p0, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->mToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 140
    :goto_1
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143
    if-nez p1, :cond_3

    .line 144
    invoke-direct {p0}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->dismissEnableMicDialogIfNeed()V

    .line 146
    :cond_3
    return-void

    .line 140
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
