class com.android.server.audio.pad.KeyboardMicKeyHelper$5 implements android.content.DialogInterface$OnDismissListener {
	 /* .source "KeyboardMicKeyHelper.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->createDialog()Lmiuix/appcompat/app/AlertDialog; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.audio.pad.KeyboardMicKeyHelper this$0; //synthetic
/* # direct methods */
 com.android.server.audio.pad.KeyboardMicKeyHelper$5 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/audio/pad/KeyboardMicKeyHelper; */
/* .line 111 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onDismiss ( android.content.DialogInterface p0 ) {
/* .locals 3 */
/* .param p1, "dialog" # Landroid/content/DialogInterface; */
/* .line 114 */
final String v0 = "PadAdapter.KeyboardMicKeyHelper"; // const-string v0, "PadAdapter.KeyboardMicKeyHelper"
final String v1 = "dialog onDismiss()"; // const-string v1, "dialog onDismiss()"
android.util.Log .d ( v0,v1 );
/* .line 115 */
v0 = this.this$0;
com.android.server.audio.pad.KeyboardMicKeyHelper .-$$Nest$fgetmDialogStateLock ( v0 );
/* monitor-enter v0 */
/* .line 116 */
try { // :try_start_0
	 v1 = this.this$0;
	 int v2 = 0; // const/4 v2, 0x0
	 com.android.server.audio.pad.KeyboardMicKeyHelper .-$$Nest$fputmDialog ( v1,v2 );
	 /* .line 117 */
	 /* monitor-exit v0 */
	 /* .line 118 */
	 return;
	 /* .line 117 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
} // .end method
