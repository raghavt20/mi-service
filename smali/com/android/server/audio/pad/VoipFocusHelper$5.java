class com.android.server.audio.pad.VoipFocusHelper$5 extends android.hardware.ICameraFaceListener$Stub {
	 /* .source "VoipFocusHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/audio/pad/VoipFocusHelper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.audio.pad.VoipFocusHelper this$0; //synthetic
/* # direct methods */
 com.android.server.audio.pad.VoipFocusHelper$5 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/audio/pad/VoipFocusHelper; */
/* .line 447 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/hardware/ICameraFaceListener$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onFaceUpdate ( Integer[] p0, Integer[] p1 ) {
/* .locals 2 */
/* .param p1, "viewRegion" # [I */
/* .param p2, "faceRects" # [I */
/* .line 450 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "[camera] face detected, viewRegion : "; // const-string v1, "[camera] face detected, viewRegion : "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .toString ( p1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", faceRects : "; // const-string v1, ", faceRects : "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 451 */
java.util.Arrays .toString ( p2 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 450 */
final String v1 = "PadAdapter.VoipFocusHelper"; // const-string v1, "PadAdapter.VoipFocusHelper"
android.util.Log .d ( v1,v0 );
/* .line 452 */
v0 = this.this$0;
v0 = com.android.server.audio.pad.VoipFocusHelper .-$$Nest$fgetmOrientation ( v0 );
int v1 = 2; // const/4 v1, 0x2
com.android.server.audio.pad.FaceInfo .build ( v1,v0,p1,p2 );
/* .line 454 */
/* .local v0, "faceInfo":Lcom/android/server/audio/pad/FaceInfo; */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 455 */
	 v1 = this.this$0;
	 com.android.server.audio.pad.VoipFocusHelper .-$$Nest$mpostUpdateFaceInfo ( v1,v0 );
	 /* .line 457 */
} // :cond_0
return;
} // .end method
