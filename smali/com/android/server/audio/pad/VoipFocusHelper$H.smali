.class Lcom/android/server/audio/pad/VoipFocusHelper$H;
.super Landroid/os/Handler;
.source "VoipFocusHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/pad/VoipFocusHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "H"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/audio/pad/VoipFocusHelper;


# direct methods
.method public constructor <init>(Lcom/android/server/audio/pad/VoipFocusHelper;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 468
    iput-object p1, p0, Lcom/android/server/audio/pad/VoipFocusHelper$H;->this$0:Lcom/android/server/audio/pad/VoipFocusHelper;

    .line 469
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 470
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .line 474
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 482
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper$H;->this$0:Lcom/android/server/audio/pad/VoipFocusHelper;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/audio/pad/VoipFocusHelper;->-$$Nest$monFaceInfoUpdate(Lcom/android/server/audio/pad/VoipFocusHelper;Lcom/android/server/audio/pad/FaceInfo;)V

    goto :goto_0

    .line 479
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper$H;->this$0:Lcom/android/server/audio/pad/VoipFocusHelper;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/android/server/audio/pad/FaceInfo;

    invoke-static {v0, v1}, Lcom/android/server/audio/pad/VoipFocusHelper;->-$$Nest$monFaceInfoUpdate(Lcom/android/server/audio/pad/VoipFocusHelper;Lcom/android/server/audio/pad/FaceInfo;)V

    .line 480
    goto :goto_0

    .line 476
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper$H;->this$0:Lcom/android/server/audio/pad/VoipFocusHelper;

    invoke-static {v0}, Lcom/android/server/audio/pad/VoipFocusHelper;->-$$Nest$monFaceSrcUpdate(Lcom/android/server/audio/pad/VoipFocusHelper;)V

    .line 477
    nop

    .line 485
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
