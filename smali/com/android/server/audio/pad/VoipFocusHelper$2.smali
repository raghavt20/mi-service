.class Lcom/android/server/audio/pad/VoipFocusHelper$2;
.super Landroid/hardware/camera2/CameraManager$AvailabilityCallback;
.source "VoipFocusHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/pad/VoipFocusHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/audio/pad/VoipFocusHelper;


# direct methods
.method constructor <init>(Lcom/android/server/audio/pad/VoipFocusHelper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/audio/pad/VoipFocusHelper;

    .line 385
    iput-object p1, p0, Lcom/android/server/audio/pad/VoipFocusHelper$2;->this$0:Lcom/android/server/audio/pad/VoipFocusHelper;

    invoke-direct {p0}, Landroid/hardware/camera2/CameraManager$AvailabilityCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onCameraClosed(Ljava/lang/String;)V
    .locals 3
    .param p1, "cameraId"    # Ljava/lang/String;

    .line 397
    const-string v0, "PadAdapter.VoipFocusHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCameraClosed() cameraId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    iget-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper$2;->this$0:Lcom/android/server/audio/pad/VoipFocusHelper;

    invoke-static {v0}, Lcom/android/server/audio/pad/VoipFocusHelper;->-$$Nest$fgetmCameraStateLock(Lcom/android/server/audio/pad/VoipFocusHelper;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 399
    :try_start_0
    iget-object v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper$2;->this$0:Lcom/android/server/audio/pad/VoipFocusHelper;

    invoke-static {v1}, Lcom/android/server/audio/pad/VoipFocusHelper;->-$$Nest$fgetmOpenedCameras(Lcom/android/server/audio/pad/VoipFocusHelper;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 400
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 401
    iget-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper$2;->this$0:Lcom/android/server/audio/pad/VoipFocusHelper;

    invoke-static {v0}, Lcom/android/server/audio/pad/VoipFocusHelper;->-$$Nest$mupdateFaceInfoSrc(Lcom/android/server/audio/pad/VoipFocusHelper;)V

    .line 402
    return-void

    .line 400
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public onCameraOpened(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "cameraId"    # Ljava/lang/String;
    .param p2, "packageId"    # Ljava/lang/String;

    .line 388
    const-string v0, "PadAdapter.VoipFocusHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCameraOpened() cameraId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", packageId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    iget-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper$2;->this$0:Lcom/android/server/audio/pad/VoipFocusHelper;

    invoke-static {v0}, Lcom/android/server/audio/pad/VoipFocusHelper;->-$$Nest$fgetmCameraStateLock(Lcom/android/server/audio/pad/VoipFocusHelper;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 390
    :try_start_0
    iget-object v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper$2;->this$0:Lcom/android/server/audio/pad/VoipFocusHelper;

    invoke-static {v1}, Lcom/android/server/audio/pad/VoipFocusHelper;->-$$Nest$fgetmOpenedCameras(Lcom/android/server/audio/pad/VoipFocusHelper;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 391
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 392
    iget-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper$2;->this$0:Lcom/android/server/audio/pad/VoipFocusHelper;

    invoke-static {v0}, Lcom/android/server/audio/pad/VoipFocusHelper;->-$$Nest$mupdateFaceInfoSrc(Lcom/android/server/audio/pad/VoipFocusHelper;)V

    .line 393
    return-void

    .line 391
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
