.class Lcom/android/server/audio/pad/VoipFocusHelper$3;
.super Ljava/lang/Object;
.source "VoipFocusHelper.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/pad/VoipFocusHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/audio/pad/VoipFocusHelper;


# direct methods
.method constructor <init>(Lcom/android/server/audio/pad/VoipFocusHelper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/audio/pad/VoipFocusHelper;

    .line 410
    iput-object p1, p0, Lcom/android/server/audio/pad/VoipFocusHelper$3;->this$0:Lcom/android/server/audio/pad/VoipFocusHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .line 413
    const-string v0, "PadAdapter.VoipFocusHelper"

    const-string v1, "AonService connected !"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    iget-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper$3;->this$0:Lcom/android/server/audio/pad/VoipFocusHelper;

    invoke-static {v0}, Lcom/android/server/audio/pad/VoipFocusHelper;->-$$Nest$fgetmAonServiceLock(Lcom/android/server/audio/pad/VoipFocusHelper;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 415
    :try_start_0
    iget-object v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper$3;->this$0:Lcom/android/server/audio/pad/VoipFocusHelper;

    invoke-static {p2}, Lcom/xiaomi/aon/IMiAON$Stub;->asInterface(Landroid/os/IBinder;)Lcom/xiaomi/aon/IMiAON;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/server/audio/pad/VoipFocusHelper;->-$$Nest$fputmAonService(Lcom/android/server/audio/pad/VoipFocusHelper;Lcom/xiaomi/aon/IMiAON;)V

    .line 416
    iget-object v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper$3;->this$0:Lcom/android/server/audio/pad/VoipFocusHelper;

    invoke-static {v1}, Lcom/android/server/audio/pad/VoipFocusHelper;->-$$Nest$fgetmAonServiceLock(Lcom/android/server/audio/pad/VoipFocusHelper;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 417
    iget-object v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper$3;->this$0:Lcom/android/server/audio/pad/VoipFocusHelper;

    invoke-static {v1}, Lcom/android/server/audio/pad/VoipFocusHelper;->-$$Nest$mupdateFaceInfoSrc(Lcom/android/server/audio/pad/VoipFocusHelper;)V

    .line 418
    monitor-exit v0

    .line 419
    return-void

    .line 418
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;

    .line 422
    const-string v0, "PadAdapter.VoipFocusHelper"

    const-string v1, "AonService disconnected !"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    iget-object v0, p0, Lcom/android/server/audio/pad/VoipFocusHelper$3;->this$0:Lcom/android/server/audio/pad/VoipFocusHelper;

    invoke-static {v0}, Lcom/android/server/audio/pad/VoipFocusHelper;->-$$Nest$fgetmAonServiceLock(Lcom/android/server/audio/pad/VoipFocusHelper;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 424
    :try_start_0
    iget-object v1, p0, Lcom/android/server/audio/pad/VoipFocusHelper$3;->this$0:Lcom/android/server/audio/pad/VoipFocusHelper;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/server/audio/pad/VoipFocusHelper;->-$$Nest$fputmAonService(Lcom/android/server/audio/pad/VoipFocusHelper;Lcom/xiaomi/aon/IMiAON;)V

    .line 425
    monitor-exit v0

    .line 426
    return-void

    .line 425
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
