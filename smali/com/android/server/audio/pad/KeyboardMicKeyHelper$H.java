class com.android.server.audio.pad.KeyboardMicKeyHelper$H extends android.os.Handler {
	 /* .source "KeyboardMicKeyHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/audio/pad/KeyboardMicKeyHelper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "H" */
} // .end annotation
/* # instance fields */
final com.android.server.audio.pad.KeyboardMicKeyHelper this$0; //synthetic
/* # direct methods */
public com.android.server.audio.pad.KeyboardMicKeyHelper$H ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 194 */
this.this$0 = p1;
/* .line 195 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 196 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 2 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 200 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 205 */
/* :pswitch_0 */
v0 = this.this$0;
com.android.server.audio.pad.KeyboardMicKeyHelper .-$$Nest$fgetmAudioManager ( v0 );
v0 = (( android.media.AudioManager ) v0 ).isMicrophoneMute ( ); // invoke-virtual {v0}, Landroid/media/AudioManager;->isMicrophoneMute()Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 206 */
	 v0 = this.this$0;
	 (( com.android.server.audio.pad.KeyboardMicKeyHelper ) v0 ).showEnableMicDialog ( ); // invoke-virtual {v0}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->showEnableMicDialog()V
	 /* .line 202 */
	 /* :pswitch_1 */
	 v0 = this.this$0;
	 v1 = this.obj;
	 /* check-cast v1, Ljava/lang/Boolean; */
	 v1 = 	 (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
	 (( com.android.server.audio.pad.KeyboardMicKeyHelper ) v0 ).showMicMuteChanged ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/audio/pad/KeyboardMicKeyHelper;->showMicMuteChanged(Z)V
	 /* .line 203 */
	 /* nop */
	 /* .line 210 */
} // :cond_0
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
