class com.android.server.audio.CloudServiceSettings {
	 /* .source "CloudServiceSettings.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;, */
	 /* Lcom/android/server/audio/CloudServiceSettings$Setting;, */
	 /* Lcom/android/server/audio/CloudServiceSettings$ParametersSetting;, */
	 /* Lcom/android/server/audio/CloudServiceSettings$PropertySetting;, */
	 /* Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String CHARSET_NAME;
public static final java.lang.String FILE_DOWNLOAD;
public static final java.lang.String PARAMETERS_SET;
public static final java.lang.String PROPERTY_SET;
private static final java.lang.String SECRET;
public static final java.lang.String SETTINGSPRIVIDER_SET;
private static final java.lang.String TRANSFORMATION_AES;
private static final java.lang.String TRANSFORMATION_AES_E_PAD;
private static final java.lang.String VERSION_CODE;
public static vendor.xiaomi.hardware.misys.V2_0.IMiSys mHidlmisysV2;
private static java.util.HashMap mSettingsKV;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private java.lang.String DEBUG_SETTINGS;
private final java.lang.String TAG;
private java.lang.Class iMisys_V1;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Class<", */
/* "*>;" */
/* } */
} // .end annotation
} // .end field
private java.lang.Class iMisys_V2;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Class<", */
/* "*>;" */
/* } */
} // .end annotation
} // .end field
private java.lang.Object iQConfig;
private android.content.Context mContext;
private final java.lang.String mModuleName;
public java.util.HashMap mSettings;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/audio/CloudServiceSettings$Setting;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public java.lang.String mVersionCode;
/* # direct methods */
static java.lang.Class -$$Nest$fgetiMisys_V1 ( com.android.server.audio.CloudServiceSettings p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.iMisys_V1;
} // .end method
static java.lang.Class -$$Nest$fgetiMisys_V2 ( com.android.server.audio.CloudServiceSettings p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.iMisys_V2;
} // .end method
static java.lang.Object -$$Nest$fgetiQConfig ( com.android.server.audio.CloudServiceSettings p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.iQConfig;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.audio.CloudServiceSettings p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static void -$$Nest$minvokeEraseFileOrDirectory ( com.android.server.audio.CloudServiceSettings p0, java.lang.Class p1, java.lang.String p2, java.lang.String p3 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/audio/CloudServiceSettings;->invokeEraseFileOrDirectory(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)V */
return;
} // .end method
static Boolean -$$Nest$minvokeIsExists ( com.android.server.audio.CloudServiceSettings p0, java.lang.Class p1, java.lang.String p2, java.lang.String p3 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/audio/CloudServiceSettings;->invokeIsExists(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Z */
} // .end method
static java.lang.String -$$Nest$smdecryptPassword ( java.lang.String p0 ) { //bridge//synthethic
/* .locals 0 */
com.android.server.audio.CloudServiceSettings .decryptPassword ( p0 );
} // .end method
static com.android.server.audio.CloudServiceSettings ( ) {
/* .locals 1 */
/* .line 80 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
return;
} // .end method
 com.android.server.audio.CloudServiceSettings ( ) {
/* .locals 4 */
/* .param p1, "ctx" # Landroid/content/Context; */
/* .line 135 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 56 */
final String v0 = "CloudServiceSettings"; // const-string v0, "CloudServiceSettings"
this.TAG = v0;
/* .line 62 */
final String v1 = "app_misound_feature_support"; // const-string v1, "app_misound_feature_support"
this.mModuleName = v1;
/* .line 66 */
final String v1 = ""; // const-string v1, ""
this.mVersionCode = v1;
/* .line 72 */
this.DEBUG_SETTINGS = v1;
/* .line 87 */
/* new-instance v1, Ljava/util/HashMap; */
/* invoke-direct {v1}, Ljava/util/HashMap;-><init>()V */
this.mSettings = v1;
/* .line 130 */
int v1 = 0; // const/4 v1, 0x0
this.iMisys_V1 = v1;
/* .line 131 */
this.iMisys_V2 = v1;
/* .line 132 */
this.iQConfig = v1;
/* .line 136 */
this.mContext = p1;
/* .line 137 */
(( com.android.server.audio.CloudServiceSettings ) p0 ).fetchDataAll ( ); // invoke-virtual {p0}, Lcom/android/server/audio/CloudServiceSettings;->fetchDataAll()V
/* .line 139 */
try { // :try_start_0
/* const-string/jumbo v1, "vendor.xiaomi.hardware.misys.V1_0.IMiSys" */
/* invoke-direct {p0, v1}, Lcom/android/server/audio/CloudServiceSettings;->invokeIMiSys(Ljava/lang/String;)Ljava/lang/Class; */
this.iMisys_V1 = v1;
/* .line 140 */
/* const-string/jumbo v1, "vendor.xiaomi.hardware.misys.V2_0.IMiSys" */
/* invoke-direct {p0, v1}, Lcom/android/server/audio/CloudServiceSettings;->invokeIMiSys(Ljava/lang/String;)Ljava/lang/Class; */
this.iMisys_V2 = v1;
/* .line 141 */
/* const-string/jumbo v1, "vendor.xiaomi.hardware.misys.V3_0.MiSys" */
v1 = /* invoke-direct {p0, v1}, Lcom/android/server/audio/CloudServiceSettings;->invokeinit(Ljava/lang/String;)I */
/* .line 142 */
/* .local v1, "misysInit":I */
/* if-nez v1, :cond_0 */
/* .line 143 */
final String v2 = "MiSys V3_0 init failed ==> return"; // const-string v2, "MiSys V3_0 init failed ==> return"
android.util.Log .e ( v0,v2 );
/* .line 145 */
} // :cond_0
final String v2 = "debug.media.video.chipset"; // const-string v2, "debug.media.video.chipset"
final String v3 = "7"; // const-string v3, "7"
android.os.SystemProperties .get ( v2,v3 );
final String v3 = "6"; // const-string v3, "6"
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 146 */
int v2 = 1; // const/4 v2, 0x1
vendor.xiaomi.hardware.misys.V2_0.IMiSys .getService ( v2 );
/* .line 148 */
} // :cond_1
/* invoke-direct {p0}, Lcom/android/server/audio/CloudServiceSettings;->invokeQConfig()Ljava/lang/Object; */
this.iQConfig = v2;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 152 */
} // .end local v1 # "misysInit":I
} // :goto_0
/* .line 150 */
/* :catch_0 */
/* move-exception v1 */
/* .line 151 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "fail to get Misys"; // const-string v3, "fail to get Misys"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v2 );
/* .line 153 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
private static java.lang.String decryptPassword ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p0, "content" # Ljava/lang/String; */
/* .line 694 */
final String v0 = ""; // const-string v0, ""
/* .line 696 */
/* .local v0, "res":Ljava/lang/String; */
try { // :try_start_0
/* new-instance v1, Ljavax/crypto/spec/SecretKeySpec; */
/* const-string/jumbo v2, "sys_audio_secret" */
(( java.lang.String ) v2 ).getBytes ( ); // invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B
final String v3 = "AES"; // const-string v3, "AES"
/* invoke-direct {v1, v2, v3}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V */
/* .line 697 */
/* .local v1, "key":Ljavax/crypto/SecretKey; */
final String v2 = "AES/ECB/ZeroBytePadding"; // const-string v2, "AES/ECB/ZeroBytePadding"
javax.crypto.Cipher .getInstance ( v2 );
/* .line 698 */
/* .local v2, "cipher":Ljavax/crypto/Cipher; */
int v3 = 2; // const/4 v3, 0x2
(( javax.crypto.Cipher ) v2 ).init ( v3, v1 ); // invoke-virtual {v2, v3, v1}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V
/* .line 699 */
(( java.lang.String ) p0 ).getBytes ( ); // invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B
int v4 = 0; // const/4 v4, 0x0
android.util.Base64 .decode ( v3,v4 );
(( javax.crypto.Cipher ) v2 ).doFinal ( v3 ); // invoke-virtual {v2, v3}, Ljavax/crypto/Cipher;->doFinal([B)[B
/* .line 700 */
/* .local v3, "bytes":[B */
/* new-instance v4, Ljava/lang/String; */
final String v5 = "UTF-8"; // const-string v5, "UTF-8"
/* invoke-direct {v4, v3, v5}, Ljava/lang/String;-><init>([BLjava/lang/String;)V */
/* :try_end_0 */
/* .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/security/InvalidKeyException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljavax/crypto/BadPaddingException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v0, v4 */
/* .line 706 */
} // .end local v1 # "key":Ljavax/crypto/SecretKey;
} // .end local v2 # "cipher":Ljavax/crypto/Cipher;
} // .end local v3 # "bytes":[B
/* .line 701 */
/* :catch_0 */
/* move-exception v1 */
/* .line 705 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 707 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
} // .end method
private void invokeEraseFileOrDirectory ( java.lang.Class p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 4 */
/* .param p2, "path" # Ljava/lang/String; */
/* .param p3, "fileName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Class<", */
/* "*>;", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 213 */
/* .local p1, "aClass1":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 214 */
try { // :try_start_0
final String v0 = "EraseFileOrDirectory"; // const-string v0, "EraseFileOrDirectory"
int v1 = 2; // const/4 v1, 0x2
/* new-array v1, v1, [Ljava/lang/Class; */
/* const-class v2, Ljava/lang/String; */
int v3 = 0; // const/4 v3, 0x0
/* aput-object v2, v1, v3 */
/* const-class v2, Ljava/lang/String; */
int v3 = 1; // const/4 v3, 0x1
/* aput-object v2, v1, v3 */
(( java.lang.Class ) p1 ).getMethod ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 215 */
/* .local v0, "methodErase":Ljava/lang/reflect/Method; */
(( java.lang.reflect.Method ) v0 ).setAccessible ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 216 */
/* filled-new-array {p2, p3}, [Ljava/lang/Object; */
int v2 = 0; // const/4 v2, 0x0
(( java.lang.reflect.Method ) v0 ).invoke ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catch Ljava/lang/NoSuchMethodException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/IllegalAccessException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 222 */
} // .end local v0 # "methodErase":Ljava/lang/reflect/Method;
/* :catch_0 */
/* move-exception v0 */
/* .line 223 */
/* .local v0, "e":Ljava/lang/IllegalAccessException; */
(( java.lang.IllegalAccessException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V
/* .line 220 */
} // .end local v0 # "e":Ljava/lang/IllegalAccessException;
/* :catch_1 */
/* move-exception v0 */
/* .line 221 */
/* .local v0, "e":Ljava/lang/NoSuchMethodException; */
(( java.lang.NoSuchMethodException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V
} // .end local v0 # "e":Ljava/lang/NoSuchMethodException;
/* .line 218 */
/* :catch_2 */
/* move-exception v0 */
/* .line 219 */
/* .local v0, "e":Ljava/lang/reflect/InvocationTargetException; */
(( java.lang.reflect.InvocationTargetException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V
/* .line 224 */
} // .end local v0 # "e":Ljava/lang/reflect/InvocationTargetException;
} // :cond_0
} // :goto_0
/* nop */
/* .line 225 */
} // :goto_1
return;
} // .end method
private java.lang.Class invokeIMiSys ( java.lang.String p0 ) {
/* .locals 7 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/lang/Class<", */
/* "*>;" */
/* } */
} // .end annotation
/* .line 174 */
int v0 = 0; // const/4 v0, 0x0
/* .line 176 */
/* .local v0, "aClass1":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
java.lang.Class .forName ( p1 );
/* move-object v0, v2 */
/* .line 177 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 178 */
final String v2 = "getService"; // const-string v2, "getService"
int v3 = 1; // const/4 v3, 0x1
/* new-array v4, v3, [Ljava/lang/Class; */
/* const-class v5, Ljava/lang/Boolean; */
int v6 = 0; // const/4 v6, 0x0
/* aput-object v5, v4, v6 */
(( java.lang.Class ) v0 ).getMethod ( v2, v4 ); // invoke-virtual {v0, v2, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 179 */
/* .local v2, "methodGetService":Ljava/lang/reflect/Method; */
(( java.lang.reflect.Method ) v2 ).setAccessible ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 180 */
/* new-array v4, v3, [Ljava/lang/Object; */
java.lang.Boolean .valueOf ( v3 );
/* aput-object v3, v4, v6 */
(( java.lang.reflect.Method ) v2 ).invoke ( v1, v4 ); // invoke-virtual {v2, v1, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/Class; */
/* :try_end_0 */
/* .catch Ljava/lang/ClassNotFoundException; {:try_start_0 ..:try_end_0} :catch_3 */
/* .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catch Ljava/lang/NoSuchMethodException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/IllegalAccessException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 188 */
} // .end local v2 # "methodGetService":Ljava/lang/reflect/Method;
/* :catch_0 */
/* move-exception v2 */
/* .line 189 */
/* .local v2, "e":Ljava/lang/IllegalAccessException; */
(( java.lang.IllegalAccessException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V
/* .line 186 */
} // .end local v2 # "e":Ljava/lang/IllegalAccessException;
/* :catch_1 */
/* move-exception v2 */
/* .line 187 */
/* .local v2, "e":Ljava/lang/NoSuchMethodException; */
(( java.lang.NoSuchMethodException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/NoSuchMethodException;->printStackTrace()V
} // .end local v2 # "e":Ljava/lang/NoSuchMethodException;
/* .line 184 */
/* :catch_2 */
/* move-exception v2 */
/* .line 185 */
/* .local v2, "e":Ljava/lang/reflect/InvocationTargetException; */
(( java.lang.reflect.InvocationTargetException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V
} // .end local v2 # "e":Ljava/lang/reflect/InvocationTargetException;
/* .line 182 */
/* :catch_3 */
/* move-exception v2 */
/* .line 183 */
/* .local v2, "e":Ljava/lang/ClassNotFoundException; */
(( java.lang.ClassNotFoundException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/ClassNotFoundException;->printStackTrace()V
/* .line 190 */
} // .end local v2 # "e":Ljava/lang/ClassNotFoundException;
} // :cond_0
} // :goto_0
/* nop */
/* .line 191 */
} // :goto_1
} // .end method
private Boolean invokeIsExists ( java.lang.Class p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 5 */
/* .param p2, "path" # Ljava/lang/String; */
/* .param p3, "fileName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Class<", */
/* "*>;", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ")Z" */
/* } */
} // .end annotation
/* .line 196 */
/* .local p1, "aClass1":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 197 */
try { // :try_start_0
final String v1 = "init"; // const-string v1, "init"
int v2 = 2; // const/4 v2, 0x2
/* new-array v2, v2, [Ljava/lang/Class; */
/* const-class v3, Ljava/lang/String; */
/* aput-object v3, v2, v0 */
/* const-class v3, Ljava/lang/String; */
int v4 = 1; // const/4 v4, 0x1
/* aput-object v3, v2, v4 */
(( java.lang.Class ) p1 ).getMethod ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 198 */
/* .local v1, "methodinit":Ljava/lang/reflect/Method; */
(( java.lang.reflect.Method ) v1 ).setAccessible ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 199 */
/* filled-new-array {p2, p3}, [Ljava/lang/Object; */
int v3 = 0; // const/4 v3, 0x0
(( java.lang.reflect.Method ) v1 ).invoke ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/Boolean; */
v0 = (( java.lang.Boolean ) v2 ).booleanValue ( ); // invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
/* :try_end_0 */
/* .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catch Ljava/lang/NoSuchMethodException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/IllegalAccessException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 205 */
} // .end local v1 # "methodinit":Ljava/lang/reflect/Method;
/* :catch_0 */
/* move-exception v1 */
/* .line 206 */
/* .local v1, "e":Ljava/lang/IllegalAccessException; */
(( java.lang.IllegalAccessException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V
/* .line 203 */
} // .end local v1 # "e":Ljava/lang/IllegalAccessException;
/* :catch_1 */
/* move-exception v1 */
/* .line 204 */
/* .local v1, "e":Ljava/lang/NoSuchMethodException; */
(( java.lang.NoSuchMethodException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/NoSuchMethodException;->printStackTrace()V
} // .end local v1 # "e":Ljava/lang/NoSuchMethodException;
/* .line 201 */
/* :catch_2 */
/* move-exception v1 */
/* .line 202 */
/* .local v1, "e":Ljava/lang/reflect/InvocationTargetException; */
(( java.lang.reflect.InvocationTargetException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V
/* .line 207 */
} // .end local v1 # "e":Ljava/lang/reflect/InvocationTargetException;
} // :cond_0
} // :goto_0
/* nop */
/* .line 208 */
} // :goto_1
} // .end method
private java.lang.Object invokeQConfig ( ) {
/* .locals 7 */
/* .line 157 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
/* const-string/jumbo v1, "vendor.qti.hardware.qconfig.IQConfig/default" */
android.os.ServiceManager .getService ( v1 );
/* .line 158 */
/* .local v1, "binder":Landroid/os/IBinder; */
/* if-nez v1, :cond_0 */
/* .line 159 */
/* .line 161 */
} // :cond_0
/* const-string/jumbo v2, "vendor.qti.hardware.qconfig.IQConfig$Stub" */
java.lang.Class .forName ( v2 );
/* .line 162 */
/* .local v2, "qClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 163 */
final String v3 = "asInterface"; // const-string v3, "asInterface"
int v4 = 1; // const/4 v4, 0x1
/* new-array v4, v4, [Ljava/lang/Class; */
/* const-class v5, Landroid/os/IBinder; */
int v6 = 0; // const/4 v6, 0x0
/* aput-object v5, v4, v6 */
(( java.lang.Class ) v2 ).getDeclaredMethod ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 164 */
/* .local v3, "asInterfaceMethod":Ljava/lang/reflect/Method; */
/* filled-new-array {v1}, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v3 ).invoke ( v0, v4 ); // invoke-virtual {v3, v0, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 165 */
/* .local v0, "aidlInterface":Ljava/lang/Object; */
/* .line 169 */
} // .end local v0 # "aidlInterface":Ljava/lang/Object;
} // .end local v1 # "binder":Landroid/os/IBinder;
} // .end local v2 # "qClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
} // .end local v3 # "asInterfaceMethod":Ljava/lang/reflect/Method;
} // :cond_1
/* .line 167 */
/* :catch_0 */
/* move-exception v1 */
/* .line 168 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 170 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
} // .end method
private Integer invokeWriteToFile ( java.lang.String p0, Object[] p1, java.lang.String p2, java.lang.String p3, Long p4 ) {
/* .locals 9 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "jArray" # [B */
/* .param p3, "path" # Ljava/lang/String; */
/* .param p4, "name" # Ljava/lang/String; */
/* .param p5, "fileSize" # J */
/* .line 249 */
int v0 = 0; // const/4 v0, 0x0
/* .line 251 */
/* .local v0, "aClass1":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
java.lang.Class .forName ( p1 );
/* move-object v0, v2 */
/* .line 252 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 253 */
/* const-string/jumbo v2, "writeToFile" */
int v3 = 4; // const/4 v3, 0x4
/* new-array v4, v3, [Ljava/lang/Class; */
/* const-class v5, [B */
/* aput-object v5, v4, v1 */
/* const-class v5, Ljava/lang/String; */
int v6 = 1; // const/4 v6, 0x1
/* aput-object v5, v4, v6 */
/* const-class v5, Ljava/lang/String; */
int v7 = 2; // const/4 v7, 0x2
/* aput-object v5, v4, v7 */
v5 = java.lang.Long.TYPE;
int v8 = 3; // const/4 v8, 0x3
/* aput-object v5, v4, v8 */
(( java.lang.Class ) v0 ).getMethod ( v2, v4 ); // invoke-virtual {v0, v2, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 254 */
/* .local v2, "methodWriteToFile":Ljava/lang/reflect/Method; */
(( java.lang.reflect.Method ) v2 ).setAccessible ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 255 */
/* new-array v3, v3, [Ljava/lang/Object; */
/* aput-object p2, v3, v1 */
/* aput-object p3, v3, v6 */
/* aput-object p4, v3, v7 */
java.lang.Long .valueOf ( p5,p6 );
/* aput-object v4, v3, v8 */
int v4 = 0; // const/4 v4, 0x0
(( java.lang.reflect.Method ) v2 ).invoke ( v4, v3 ); // invoke-virtual {v2, v4, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* :try_end_0 */
/* .catch Ljava/lang/ClassNotFoundException; {:try_start_0 ..:try_end_0} :catch_3 */
/* .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catch Ljava/lang/NoSuchMethodException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/IllegalAccessException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 263 */
} // .end local v2 # "methodWriteToFile":Ljava/lang/reflect/Method;
/* :catch_0 */
/* move-exception v2 */
/* .line 264 */
/* .local v2, "e":Ljava/lang/IllegalAccessException; */
(( java.lang.IllegalAccessException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V
/* .line 261 */
} // .end local v2 # "e":Ljava/lang/IllegalAccessException;
/* :catch_1 */
/* move-exception v2 */
/* .line 262 */
/* .local v2, "e":Ljava/lang/NoSuchMethodException; */
(( java.lang.NoSuchMethodException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/NoSuchMethodException;->printStackTrace()V
} // .end local v2 # "e":Ljava/lang/NoSuchMethodException;
/* .line 259 */
/* :catch_2 */
/* move-exception v2 */
/* .line 260 */
/* .local v2, "e":Ljava/lang/reflect/InvocationTargetException; */
(( java.lang.reflect.InvocationTargetException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V
} // .end local v2 # "e":Ljava/lang/reflect/InvocationTargetException;
/* .line 257 */
/* :catch_3 */
/* move-exception v2 */
/* .line 258 */
/* .local v2, "e":Ljava/lang/ClassNotFoundException; */
(( java.lang.ClassNotFoundException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/ClassNotFoundException;->printStackTrace()V
/* .line 265 */
} // .end local v2 # "e":Ljava/lang/ClassNotFoundException;
} // :cond_0
} // :goto_0
/* nop */
/* .line 266 */
} // :goto_1
} // .end method
private Integer invokeinit ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 228 */
int v0 = 0; // const/4 v0, 0x0
/* .line 230 */
/* .local v0, "aClass1":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
java.lang.Class .forName ( p1 );
/* move-object v0, v2 */
/* .line 231 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 232 */
final String v2 = "init"; // const-string v2, "init"
/* new-array v3, v1, [Ljava/lang/Class; */
(( java.lang.Class ) v0 ).getMethod ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 233 */
/* .local v2, "methodinit":Ljava/lang/reflect/Method; */
int v3 = 1; // const/4 v3, 0x1
(( java.lang.reflect.Method ) v2 ).setAccessible ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 234 */
/* new-array v3, v1, [Ljava/lang/Object; */
int v4 = 0; // const/4 v4, 0x0
(( java.lang.reflect.Method ) v2 ).invoke ( v4, v3 ); // invoke-virtual {v2, v4, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* :try_end_0 */
/* .catch Ljava/lang/ClassNotFoundException; {:try_start_0 ..:try_end_0} :catch_3 */
/* .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catch Ljava/lang/NoSuchMethodException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/IllegalAccessException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 242 */
} // .end local v2 # "methodinit":Ljava/lang/reflect/Method;
/* :catch_0 */
/* move-exception v2 */
/* .line 243 */
/* .local v2, "e":Ljava/lang/IllegalAccessException; */
(( java.lang.IllegalAccessException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V
/* .line 240 */
} // .end local v2 # "e":Ljava/lang/IllegalAccessException;
/* :catch_1 */
/* move-exception v2 */
/* .line 241 */
/* .local v2, "e":Ljava/lang/NoSuchMethodException; */
(( java.lang.NoSuchMethodException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/NoSuchMethodException;->printStackTrace()V
} // .end local v2 # "e":Ljava/lang/NoSuchMethodException;
/* .line 238 */
/* :catch_2 */
/* move-exception v2 */
/* .line 239 */
/* .local v2, "e":Ljava/lang/reflect/InvocationTargetException; */
(( java.lang.reflect.InvocationTargetException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V
} // .end local v2 # "e":Ljava/lang/reflect/InvocationTargetException;
/* .line 236 */
/* :catch_3 */
/* move-exception v2 */
/* .line 237 */
/* .local v2, "e":Ljava/lang/ClassNotFoundException; */
(( java.lang.ClassNotFoundException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/ClassNotFoundException;->printStackTrace()V
/* .line 244 */
} // .end local v2 # "e":Ljava/lang/ClassNotFoundException;
} // :cond_0
} // :goto_0
/* nop */
/* .line 245 */
} // :goto_1
} // .end method
private void parseSettings ( ) {
/* .locals 8 */
/* .line 304 */
final String v0 = "CloudServiceSettings"; // const-string v0, "CloudServiceSettings"
final String v1 = ""; // const-string v1, ""
this.mVersionCode = v1;
/* .line 305 */
v1 = com.android.server.audio.CloudServiceSettings.mSettingsKV;
/* const-string/jumbo v2, "versionCode" */
v1 = (( java.util.HashMap ) v1 ).containsKey ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 306 */
v1 = com.android.server.audio.CloudServiceSettings.mSettingsKV;
(( java.util.HashMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/util/List; */
int v2 = 0; // const/4 v2, 0x0
/* check-cast v1, Ljava/lang/String; */
this.mVersionCode = v1;
/* .line 309 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 310 */
/* .local v1, "s":Lcom/android/server/audio/CloudServiceSettings$Setting; */
try { // :try_start_0
v2 = com.android.server.audio.CloudServiceSettings.mSettingsKV;
final String v3 = "fileDownload"; // const-string v3, "fileDownload"
java.util.Collections .emptyList ( );
(( java.util.HashMap ) v2 ).getOrDefault ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Ljava/util/List; */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_2
/* check-cast v3, Ljava/lang/String; */
/* .line 311 */
/* .local v3, "str":Ljava/lang/String; */
/* new-instance v4, Lorg/json/JSONArray; */
/* invoke-direct {v4, v3}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V */
/* .line 312 */
/* .local v4, "array":Lorg/json/JSONArray; */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "i":I */
} // :goto_1
v6 = (( org.json.JSONArray ) v4 ).length ( ); // invoke-virtual {v4}, Lorg/json/JSONArray;->length()I
/* if-ge v5, v6, :cond_1 */
/* .line 313 */
/* new-instance v6, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting; */
(( org.json.JSONArray ) v4 ).getJSONObject ( v5 ); // invoke-virtual {v4, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
/* invoke-direct {v6, p0, v7}, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;-><init>(Lcom/android/server/audio/CloudServiceSettings;Lorg/json/JSONObject;)V */
/* move-object v1, v6 */
/* .line 314 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "parsed FileDownloadSettings "; // const-string v7, "parsed FileDownloadSettings "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( org.json.JSONArray ) v4 ).getJSONObject ( v5 ); // invoke-virtual {v4, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v6 );
/* .line 315 */
/* invoke-direct {p0, v1}, Lcom/android/server/audio/CloudServiceSettings;->updateOrPut(Lcom/android/server/audio/CloudServiceSettings$Setting;)V */
/* .line 312 */
/* add-int/lit8 v5, v5, 0x1 */
/* .line 317 */
} // .end local v3 # "str":Ljava/lang/String;
} // .end local v4 # "array":Lorg/json/JSONArray;
} // .end local v5 # "i":I
} // :cond_1
/* .line 319 */
} // :cond_2
v2 = com.android.server.audio.CloudServiceSettings.mSettingsKV;
final String v3 = "parameterSet"; // const-string v3, "parameterSet"
java.util.Collections .emptyList ( );
(( java.util.HashMap ) v2 ).getOrDefault ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Ljava/util/List; */
v3 = } // :goto_2
if ( v3 != null) { // if-eqz v3, :cond_4
/* check-cast v3, Ljava/lang/String; */
/* .line 320 */
/* .restart local v3 # "str":Ljava/lang/String; */
/* new-instance v4, Lorg/json/JSONArray; */
/* invoke-direct {v4, v3}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V */
/* .line 321 */
/* .restart local v4 # "array":Lorg/json/JSONArray; */
int v5 = 0; // const/4 v5, 0x0
/* .restart local v5 # "i":I */
} // :goto_3
v6 = (( org.json.JSONArray ) v4 ).length ( ); // invoke-virtual {v4}, Lorg/json/JSONArray;->length()I
/* if-ge v5, v6, :cond_3 */
/* .line 322 */
/* new-instance v6, Lcom/android/server/audio/CloudServiceSettings$ParametersSetting; */
(( org.json.JSONArray ) v4 ).getJSONObject ( v5 ); // invoke-virtual {v4, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
/* invoke-direct {v6, p0, v7}, Lcom/android/server/audio/CloudServiceSettings$ParametersSetting;-><init>(Lcom/android/server/audio/CloudServiceSettings;Lorg/json/JSONObject;)V */
/* move-object v1, v6 */
/* .line 323 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "parsed ParametersSettings "; // const-string v7, "parsed ParametersSettings "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( org.json.JSONArray ) v4 ).getJSONObject ( v5 ); // invoke-virtual {v4, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v6 );
/* .line 324 */
/* invoke-direct {p0, v1}, Lcom/android/server/audio/CloudServiceSettings;->updateOrPut(Lcom/android/server/audio/CloudServiceSettings$Setting;)V */
/* .line 321 */
/* add-int/lit8 v5, v5, 0x1 */
/* .line 326 */
} // .end local v3 # "str":Ljava/lang/String;
} // .end local v4 # "array":Lorg/json/JSONArray;
} // .end local v5 # "i":I
} // :cond_3
/* .line 328 */
} // :cond_4
v2 = com.android.server.audio.CloudServiceSettings.mSettingsKV;
final String v3 = "propertySet"; // const-string v3, "propertySet"
java.util.Collections .emptyList ( );
(( java.util.HashMap ) v2 ).getOrDefault ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Ljava/util/List; */
v3 = } // :goto_4
if ( v3 != null) { // if-eqz v3, :cond_6
/* check-cast v3, Ljava/lang/String; */
/* .line 329 */
/* .restart local v3 # "str":Ljava/lang/String; */
/* new-instance v4, Lorg/json/JSONArray; */
/* invoke-direct {v4, v3}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V */
/* .line 330 */
/* .restart local v4 # "array":Lorg/json/JSONArray; */
int v5 = 0; // const/4 v5, 0x0
/* .restart local v5 # "i":I */
} // :goto_5
v6 = (( org.json.JSONArray ) v4 ).length ( ); // invoke-virtual {v4}, Lorg/json/JSONArray;->length()I
/* if-ge v5, v6, :cond_5 */
/* .line 331 */
/* new-instance v6, Lcom/android/server/audio/CloudServiceSettings$PropertySetting; */
(( org.json.JSONArray ) v4 ).getJSONObject ( v5 ); // invoke-virtual {v4, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
/* invoke-direct {v6, p0, v7}, Lcom/android/server/audio/CloudServiceSettings$PropertySetting;-><init>(Lcom/android/server/audio/CloudServiceSettings;Lorg/json/JSONObject;)V */
/* move-object v1, v6 */
/* .line 332 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "parsed PropertySetting "; // const-string v7, "parsed PropertySetting "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( org.json.JSONArray ) v4 ).getJSONObject ( v5 ); // invoke-virtual {v4, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v6 );
/* .line 333 */
/* invoke-direct {p0, v1}, Lcom/android/server/audio/CloudServiceSettings;->updateOrPut(Lcom/android/server/audio/CloudServiceSettings$Setting;)V */
/* .line 330 */
/* add-int/lit8 v5, v5, 0x1 */
/* .line 335 */
} // .end local v3 # "str":Ljava/lang/String;
} // .end local v4 # "array":Lorg/json/JSONArray;
} // .end local v5 # "i":I
} // :cond_5
/* .line 337 */
} // :cond_6
v2 = com.android.server.audio.CloudServiceSettings.mSettingsKV;
/* const-string/jumbo v3, "settingsProviderSet" */
java.util.Collections .emptyList ( );
(( java.util.HashMap ) v2 ).getOrDefault ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Ljava/util/List; */
v3 = } // :goto_6
if ( v3 != null) { // if-eqz v3, :cond_8
/* check-cast v3, Ljava/lang/String; */
/* .line 338 */
/* .restart local v3 # "str":Ljava/lang/String; */
/* new-instance v4, Lorg/json/JSONArray; */
/* invoke-direct {v4, v3}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V */
/* .line 339 */
/* .restart local v4 # "array":Lorg/json/JSONArray; */
int v5 = 0; // const/4 v5, 0x0
/* .restart local v5 # "i":I */
} // :goto_7
v6 = (( org.json.JSONArray ) v4 ).length ( ); // invoke-virtual {v4}, Lorg/json/JSONArray;->length()I
/* if-ge v5, v6, :cond_7 */
/* .line 340 */
/* new-instance v6, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings; */
(( org.json.JSONArray ) v4 ).getJSONObject ( v5 ); // invoke-virtual {v4, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
/* invoke-direct {v6, p0, v7}, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;-><init>(Lcom/android/server/audio/CloudServiceSettings;Lorg/json/JSONObject;)V */
/* move-object v1, v6 */
/* .line 341 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "parsed SettingsProviderSettings "; // const-string v7, "parsed SettingsProviderSettings "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( org.json.JSONArray ) v4 ).getJSONObject ( v5 ); // invoke-virtual {v4, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v6 );
/* .line 342 */
/* invoke-direct {p0, v1}, Lcom/android/server/audio/CloudServiceSettings;->updateOrPut(Lcom/android/server/audio/CloudServiceSettings$Setting;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 339 */
/* add-int/lit8 v5, v5, 0x1 */
/* .line 344 */
} // .end local v3 # "str":Ljava/lang/String;
} // .end local v4 # "array":Lorg/json/JSONArray;
} // .end local v5 # "i":I
} // :cond_7
/* .line 347 */
} // .end local v1 # "s":Lcom/android/server/audio/CloudServiceSettings$Setting;
} // :cond_8
/* .line 345 */
/* :catch_0 */
/* move-exception v1 */
/* .line 346 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "fail to parse setting "; // const-string v3, "fail to parse setting "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v2 );
/* .line 348 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_8
return;
} // .end method
private void updateOrPut ( com.android.server.audio.CloudServiceSettings$Setting p0 ) {
/* .locals 3 */
/* .param p1, "s" # Lcom/android/server/audio/CloudServiceSettings$Setting; */
/* .line 351 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 352 */
v0 = this.mSettings;
v1 = this.mSettingName;
v0 = (( java.util.HashMap ) v0 ).containsKey ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
final String v1 = "CloudServiceSettings"; // const-string v1, "CloudServiceSettings"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 353 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "update " */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mSettingName;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v0 );
/* .line 354 */
v0 = this.mSettings;
v1 = this.mSettingName;
(( java.util.HashMap ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/audio/CloudServiceSettings$Setting; */
(( com.android.server.audio.CloudServiceSettings$Setting ) v0 ).updateTo ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/audio/CloudServiceSettings$Setting;->updateTo(Lcom/android/server/audio/CloudServiceSettings$Setting;)Z
/* .line 356 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "record "; // const-string v2, "record "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mSettingName;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v0 );
/* .line 357 */
v0 = this.mSettings;
v1 = this.mSettingName;
(( java.util.HashMap ) v0 ).put ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 360 */
} // :cond_1
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void fetchDataAll ( ) {
/* .locals 9 */
/* .line 269 */
v0 = com.android.server.audio.CloudServiceSettings.mSettingsKV;
(( java.util.HashMap ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->clear()V
/* .line 270 */
int v0 = 0; // const/4 v0, 0x0
/* .line 271 */
/* .local v0, "dataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;" */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "debug_audio_cloud"; // const-string v2, "debug_audio_cloud"
android.provider.Settings$Global .getString ( v1,v2 );
this.DEBUG_SETTINGS = v1;
/* .line 273 */
final String v2 = "CloudServiceSettings"; // const-string v2, "CloudServiceSettings"
if ( v1 != null) { // if-eqz v1, :cond_0
try { // :try_start_0
final String v3 = ""; // const-string v3, ""
v1 = (( java.lang.String ) v3 ).equals ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_0 */
/* .line 274 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "debug settings "; // const-string v3, "debug settings "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.DEBUG_SETTINGS;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v1 );
/* .line 275 */
v1 = this.mSettings;
(( java.util.HashMap ) v1 ).clear ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->clear()V
/* .line 276 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* move-object v0, v1 */
/* .line 277 */
/* new-instance v1, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
v3 = this.DEBUG_SETTINGS;
/* invoke-direct {v1, v3}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;-><init>(Ljava/lang/String;)V */
/* .line 279 */
} // :cond_0
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v3 = "app_misound_feature_support"; // const-string v3, "app_misound_feature_support"
android.provider.MiuiSettings$SettingsCloudData .getCloudDataList ( v1,v3 );
/* move-object v0, v1 */
/* .line 281 */
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 282 */
v3 = } // :goto_1
if ( v3 != null) { // if-eqz v3, :cond_3
/* check-cast v3, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
/* .line 283 */
/* .local v3, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v3 ).json ( ); // invoke-virtual {v3}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
(( org.json.JSONObject ) v4 ).keySet ( ); // invoke-virtual {v4}, Lorg/json/JSONObject;->keySet()Ljava/util/Set;
v5 = } // :goto_2
if ( v5 != null) { // if-eqz v5, :cond_2
/* check-cast v5, Ljava/lang/String; */
/* .line 284 */
/* .local v5, "key":Ljava/lang/String; */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v3 ).json ( ); // invoke-virtual {v3}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
(( org.json.JSONObject ) v6 ).getString ( v5 ); // invoke-virtual {v6, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 285 */
/* .local v6, "value":Ljava/lang/String; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "key :"; // const-string v8, "key :"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = ", value:"; // const-string v8, ", value:"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v7 );
/* .line 286 */
v7 = com.android.server.audio.CloudServiceSettings.mSettingsKV;
v7 = (( java.util.HashMap ) v7 ).containsKey ( v5 ); // invoke-virtual {v7, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v7 != null) { // if-eqz v7, :cond_1
/* .line 287 */
v7 = com.android.server.audio.CloudServiceSettings.mSettingsKV;
(( java.util.HashMap ) v7 ).get ( v5 ); // invoke-virtual {v7, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v7, Ljava/util/List; */
/* .line 289 */
} // :cond_1
/* new-instance v7, Ljava/util/ArrayList; */
/* filled-new-array {v6}, [Ljava/lang/String; */
java.util.Arrays .asList ( v8 );
/* invoke-direct {v7, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 290 */
/* .local v7, "myList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v8 = com.android.server.audio.CloudServiceSettings.mSettingsKV;
(( java.util.HashMap ) v8 ).put ( v5, v7 ); // invoke-virtual {v8, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 292 */
} // .end local v5 # "key":Ljava/lang/String;
} // .end local v6 # "value":Ljava/lang/String;
} // .end local v7 # "myList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :goto_3
/* .line 293 */
} // .end local v3 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
} // :cond_2
/* .line 294 */
} // :cond_3
/* invoke-direct {p0}, Lcom/android/server/audio/CloudServiceSettings;->parseSettings()V */
/* .line 296 */
} // :cond_4
final String v1 = "null data"; // const-string v1, "null data"
android.util.Log .d ( v2,v1 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 300 */
} // :goto_4
/* .line 298 */
/* :catch_0 */
/* move-exception v1 */
/* .line 299 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "fail to fetch data "; // const-string v4, "fail to fetch data "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v3 );
/* .line 301 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_5
return;
} // .end method
