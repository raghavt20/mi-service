public class com.android.server.audio.FoldHelper {
	 /* .source "FoldHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/audio/FoldHelper$AudioFoldListener; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
private static com.android.server.audio.FoldHelper$AudioFoldListener sFoldListener;
/* # direct methods */
private com.android.server.audio.FoldHelper ( ) {
	 /* .locals 0 */
	 /* .line 33 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 35 */
	 return;
} // .end method
static void disable ( ) {
	 /* .locals 3 */
	 /* .line 52 */
	 v0 = com.android.server.audio.FoldHelper.sFoldListener;
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 54 */
		 try { // :try_start_0
			 android.view.WindowManagerGlobal .getWindowManagerService ( );
			 v1 = com.android.server.audio.FoldHelper.sFoldListener;
			 /* .line 55 */
			 /* :try_end_0 */
			 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 58 */
			 /* .line 56 */
			 /* :catch_0 */
			 /* move-exception v0 */
			 /* .line 57 */
			 /* .local v0, "e":Ljava/lang/Exception; */
			 /* new-instance v1, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
			 /* const-string/jumbo v2, "unregisterDisplayFoldListener error " */
			 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 final String v2 = "AudioService.FoldHelper"; // const-string v2, "AudioService.FoldHelper"
			 android.util.Log .e ( v2,v1 );
			 /* .line 60 */
		 } // .end local v0 # "e":Ljava/lang/Exception;
	 } // :cond_0
} // :goto_0
return;
} // .end method
static void enable ( ) {
/* .locals 3 */
/* .line 44 */
try { // :try_start_0
	 android.view.WindowManagerGlobal .getWindowManagerService ( );
	 v1 = com.android.server.audio.FoldHelper.sFoldListener;
	 /* .line 45 */
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 48 */
	 /* .line 46 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 47 */
	 /* .local v0, "e":Ljava/lang/Exception; */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "registerDisplayFoldListener error "; // const-string v2, "registerDisplayFoldListener error "
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v2 = "AudioService.FoldHelper"; // const-string v2, "AudioService.FoldHelper"
	 android.util.Log .e ( v2,v1 );
	 /* .line 49 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
static void init ( ) {
/* .locals 1 */
/* .line 38 */
/* new-instance v0, Lcom/android/server/audio/FoldHelper$AudioFoldListener; */
/* invoke-direct {v0}, Lcom/android/server/audio/FoldHelper$AudioFoldListener;-><init>()V */
/* .line 39 */
com.android.server.audio.FoldHelper .enable ( );
/* .line 40 */
return;
} // .end method
