class com.android.server.audio.CloudServiceThread$1 extends android.database.ContentObserver {
	 /* .source "CloudServiceThread.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/audio/CloudServiceThread;-><init>(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.audio.CloudServiceThread this$0; //synthetic
/* # direct methods */
 com.android.server.audio.CloudServiceThread$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/audio/CloudServiceThread; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 45 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 3 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 48 */
/* invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V */
/* .line 49 */
final String v0 = "CloudServiceThread"; // const-string v0, "CloudServiceThread"
final String v1 = "cloud data changed"; // const-string v1, "cloud data changed"
android.util.Log .d ( v0,v1 );
/* .line 50 */
v0 = this.this$0;
com.android.server.audio.CloudServiceThread .-$$Nest$fgetmLock ( v0 );
/* monitor-enter v0 */
/* .line 51 */
try { // :try_start_0
	 final String v1 = "CloudServiceThread"; // const-string v1, "CloudServiceThread"
	 final String v2 = "enter before notify"; // const-string v2, "enter before notify"
	 android.util.Log .d ( v1,v2 );
	 /* .line 52 */
	 v1 = this.this$0;
	 com.android.server.audio.CloudServiceThread .-$$Nest$fgetmLock ( v1 );
	 (( java.lang.Object ) v1 ).notifyAll ( ); // invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V
	 /* .line 53 */
	 final String v1 = "CloudServiceThread"; // const-string v1, "CloudServiceThread"
	 final String v2 = "enter after notify"; // const-string v2, "enter after notify"
	 android.util.Log .d ( v1,v2 );
	 /* .line 54 */
	 /* monitor-exit v0 */
	 /* .line 55 */
	 return;
	 /* .line 54 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
} // .end method
