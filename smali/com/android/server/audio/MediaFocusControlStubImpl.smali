.class public Lcom/android/server/audio/MediaFocusControlStubImpl;
.super Ljava/lang/Object;
.source "MediaFocusControlStubImpl.java"

# interfaces
.implements Lcom/android/server/audio/MediaFocusControlStub;


# static fields
.field private static final CHECK_DELAY_MS:J = 0xc8L

.field private static final DELAY_COUNT_MAX:I = 0x8

.field private static final DELAY_DISPATCH_FOCUS_CHANGE_MS:J = 0x190L

.field private static final MSG_L_DELAYED_FOCUS_CHANGE:I = 0x3

.field private static final TAG:Ljava/lang/String; = "MediaFocusControlStubImpl"


# instance fields
.field private mAudioMode:I

.field private mBtAudioSuspended:Z

.field private mDelayCount:I

.field private mInDelayDispatch:Z

.field private mLastReleasedUid:I

.field private mPendingAudioMode:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mInDelayDispatch:Z

    .line 20
    iput-boolean v0, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mBtAudioSuspended:Z

    .line 21
    iput v0, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mAudioMode:I

    .line 22
    iput v0, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mPendingAudioMode:I

    .line 26
    iput v0, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mDelayCount:I

    return-void
.end method

.method private inModeChanging()Z
    .locals 6

    .line 64
    iget v0, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mPendingAudioMode:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_0

    iget v4, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mAudioMode:I

    if-nez v4, :cond_0

    move v4, v1

    goto :goto_0

    :cond_0
    move v4, v2

    .line 66
    .local v4, "fromCommunicationToNormal":Z
    :goto_0
    if-nez v0, :cond_1

    iget v0, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mAudioMode:I

    if-ne v0, v3, :cond_1

    move v0, v1

    goto :goto_1

    :cond_1
    move v0, v2

    .line 68
    .local v0, "fromNormalToCommunication":Z
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "inModeChanging: fromCommunicationToNormal="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", fromNormalToCommunication="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, "MediaFocusControlStubImpl"

    invoke-static {v5, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    if-nez v4, :cond_3

    if-eqz v0, :cond_2

    goto :goto_2

    :cond_2
    move v1, v2

    :cond_3
    :goto_2
    return v1
.end method


# virtual methods
.method public delayFocusChangeDispatch(ILcom/android/server/audio/FocusRequester;Ljava/lang/String;Landroid/os/Handler;)V
    .locals 8
    .param p1, "focusGain"    # I
    .param p2, "fr"    # Lcom/android/server/audio/FocusRequester;
    .param p3, "clientId"    # Ljava/lang/String;
    .param p4, "focusHandler"    # Landroid/os/Handler;

    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "delay dispatching "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-wide/16 v1, 0x190

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MediaFocusControlStubImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    const-wide/16 v5, 0x190

    move-object v2, p0

    move v3, p1

    move-object v4, p2

    move-object v7, p4

    invoke-virtual/range {v2 .. v7}, Lcom/android/server/audio/MediaFocusControlStubImpl;->postDelayedFocusChange(ILcom/android/server/audio/FocusRequester;JLandroid/os/Handler;)V

    .line 99
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/server/audio/MediaFocusControlStubImpl;->setAudioFocusDelayDispatchState(Z)V

    .line 100
    return-void
.end method

.method public handleDelayedMessage(Landroid/os/Message;Landroid/os/Handler;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;
    .param p2, "focusHandler"    # Landroid/os/Handler;

    .line 127
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/audio/FocusRequester;

    .line 128
    .local v0, "fr":Lcom/android/server/audio/FocusRequester;
    iget v7, p1, Landroid/os/Message;->arg1:I

    .line 129
    .local v7, "focusChange":I
    iget-boolean v1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mBtAudioSuspended:Z

    const-string v2, "MediaFocusControlStubImpl"

    if-nez v1, :cond_0

    iget v1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mAudioMode:I

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/android/server/audio/MediaFocusControlStubImpl;->inModeChanging()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget v1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mDelayCount:I

    const/16 v3, 0x8

    if-ge v1, v3, :cond_1

    .line 131
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bt/audioMode switch not ready, delay dispatching focus change 200ms, count "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mDelayCount:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    iget v1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mDelayCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mDelayCount:I

    .line 134
    const-wide/16 v4, 0xc8

    move-object v1, p0

    move v2, v7

    move-object v3, v0

    move-object v6, p2

    invoke-virtual/range {v1 .. v6}, Lcom/android/server/audio/MediaFocusControlStubImpl;->postDelayedFocusChange(ILcom/android/server/audio/FocusRequester;JLandroid/os/Handler;)V

    goto :goto_0

    .line 136
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handling MSG_L_DELAYED_FOCUS_CHANGE focusChange="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " packageName="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 137
    invoke-virtual {v0}, Lcom/android/server/audio/FocusRequester;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " clientId="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/server/audio/FocusRequester;->getClientId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 136
    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    invoke-virtual {v0, v7}, Lcom/android/server/audio/FocusRequester;->dispatchFocusChange(I)I

    .line 139
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/android/server/audio/MediaFocusControlStubImpl;->setAudioFocusDelayDispatchState(Z)V

    .line 140
    iput v1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mDelayCount:I

    .line 142
    :goto_0
    return-void
.end method

.method public handleDelayedMessageWithdraw(Lcom/android/server/audio/FocusRequester;Landroid/os/Handler;)V
    .locals 2
    .param p1, "fr"    # Lcom/android/server/audio/FocusRequester;
    .param p2, "focusHandler"    # Landroid/os/Handler;

    .line 117
    iget-boolean v0, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mInDelayDispatch:Z

    if-eqz v0, :cond_0

    .line 118
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "handleDelayedMessageWithdraw(): withdraw message to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 119
    invoke-virtual {p1}, Lcom/android/server/audio/FocusRequester;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 118
    const-string v1, "MediaFocusControlStubImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    const/4 v0, 0x3

    invoke-virtual {p2, v0, p1}, Landroid/os/Handler;->removeEqualMessages(ILjava/lang/Object;)V

    .line 121
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/server/audio/MediaFocusControlStubImpl;->setAudioFocusDelayDispatchState(Z)V

    .line 123
    :cond_0
    return-void
.end method

.method public isDelayedDispatchFocusChangeNeeded(II)Z
    .locals 3
    .param p1, "topUid"    # I
    .param p2, "focusGain"    # I

    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "isDelayedDispatchFocusChangeNeeded: topUid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mLastReleasedUid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mLastReleasedUid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mPendingAudioMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mPendingAudioMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mBtAudioSuspended="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mBtAudioSuspended:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", focusGain="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mAudioMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mAudioMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MediaFocusControlStubImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    iget v0, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mAudioMode:I

    const/4 v1, 0x3

    const/4 v2, 0x1

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mBtAudioSuspended:Z

    if-nez v0, :cond_1

    .line 48
    :cond_0
    invoke-direct {p0}, Lcom/android/server/audio/MediaFocusControlStubImpl;->inModeChanging()Z

    move-result v0

    if-eqz v0, :cond_2

    if-ne p2, v2, :cond_2

    iget v0, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mLastReleasedUid:I

    if-eq p1, v0, :cond_2

    :cond_1
    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    .line 47
    :goto_0
    return v2
.end method

.method public postDelayedFocusChange(ILcom/android/server/audio/FocusRequester;JLandroid/os/Handler;)V
    .locals 2
    .param p1, "focusChange"    # I
    .param p2, "fr"    # Lcom/android/server/audio/FocusRequester;
    .param p3, "delayMs"    # J
    .param p5, "focusHandler"    # Landroid/os/Handler;

    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "postDelayedFocusChange(): send focusChange="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 108
    invoke-virtual {p2}, Lcom/android/server/audio/FocusRequester;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " delayMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 107
    const-string v1, "MediaFocusControlStubImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    const/4 v0, 0x3

    invoke-virtual {p5, v0, p2}, Landroid/os/Handler;->removeEqualMessages(ILjava/lang/Object;)V

    .line 110
    nop

    .line 111
    const/4 v1, 0x0

    invoke-virtual {p5, v0, p1, v1, p2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 110
    invoke-virtual {p5, v0, p3, p4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 113
    return-void
.end method

.method public setAudioFocusDelayDispatchState(Z)V
    .locals 2
    .param p1, "state"    # Z

    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setAudioFocusDelayDispatchState(): prior state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mInDelayDispatch:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", current state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MediaFocusControlStubImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    iput-boolean p1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mInDelayDispatch:Z

    .line 84
    return-void
.end method

.method public setAudioMode(I)V
    .locals 2
    .param p1, "mode"    # I

    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setAudioMode() "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MediaFocusControlStubImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    iput p1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mAudioMode:I

    .line 56
    return-void
.end method

.method public setBtAudioSuspended(Z)V
    .locals 2
    .param p1, "btAudioSuspended"    # Z

    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setBtAudioSuspended() "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MediaFocusControlStubImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    iput-boolean p1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mBtAudioSuspended:Z

    .line 77
    return-void
.end method

.method public setLastReleasedUid(I)V
    .locals 0
    .param p1, "uid"    # I

    .line 88
    iput p1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mLastReleasedUid:I

    .line 89
    return-void
.end method

.method public setPendingAudioMode(I)V
    .locals 0
    .param p1, "mode"    # I

    .line 60
    iput p1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mPendingAudioMode:I

    .line 61
    return-void
.end method
