class com.android.server.audio.CloudServiceSettings$PropertySetting extends com.android.server.audio.CloudServiceSettings$Setting {
	 /* .source "CloudServiceSettings.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/audio/CloudServiceSettings; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "PropertySetting" */
} // .end annotation
/* # static fields */
private static final java.lang.String KEY;
private static final java.lang.String VALUE;
/* # instance fields */
private java.lang.StringBuilder mKey;
private java.lang.StringBuilder mValue;
final com.android.server.audio.CloudServiceSettings this$0; //synthetic
/* # direct methods */
 com.android.server.audio.CloudServiceSettings$PropertySetting ( ) {
/* .locals 3 */
/* .param p1, "this$0" # Lcom/android/server/audio/CloudServiceSettings; */
/* .param p2, "json" # Lorg/json/JSONObject; */
/* .line 582 */
this.this$0 = p1;
/* .line 583 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/audio/CloudServiceSettings$Setting;-><init>(Lcom/android/server/audio/CloudServiceSettings;Lorg/json/JSONObject;)V */
/* .line 585 */
try { // :try_start_0
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 final String v1 = "key"; // const-string v1, "key"
	 (( org.json.JSONObject ) p2 ).getString ( v1 ); // invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
	 /* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
	 this.mKey = v0;
	 /* .line 586 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* const-string/jumbo v1, "value" */
	 (( org.json.JSONObject ) p2 ).getString ( v1 ); // invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
	 /* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
	 this.mValue = v0;
	 /* .line 587 */
	 /* iget-boolean v0, p0, Lcom/android/server/audio/CloudServiceSettings$PropertySetting;->mSecreted:Z */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 588 */
		 v0 = this.mSecretedItem;
		 v1 = this.mKey;
		 (( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
		 /* .line 589 */
		 v0 = this.mSecretedItem;
		 v1 = this.mValue;
		 (( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 593 */
	 } // :cond_0
	 /* .line 591 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 592 */
	 /* .local v0, "e":Ljava/lang/Exception; */
	 final String v1 = "CloudServiceSettings"; // const-string v1, "CloudServiceSettings"
	 final String v2 = "fail to parse FileDownloadSetting"; // const-string v2, "fail to parse FileDownloadSetting"
	 android.util.Log .e ( v1,v2 );
	 /* .line 594 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
/* # virtual methods */
public Boolean set ( ) {
/* .locals 3 */
/* .line 598 */
v0 = /* invoke-super {p0}, Lcom/android/server/audio/CloudServiceSettings$Setting;->set()Z */
/* if-nez v0, :cond_0 */
/* .line 599 */
int v0 = 0; // const/4 v0, 0x0
/* .line 603 */
} // :cond_0
try { // :try_start_0
v0 = this.mKey;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AudioCloudCtrlProp_"; // const-string v1, "AudioCloudCtrlProp_"
v0 = (( java.lang.String ) v0 ).startsWith ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 604 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
v1 = this.mKey;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "="; // const-string v1, "="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mValue;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.media.AudioSystem .setParameters ( v0 );
/* .line 606 */
} // :cond_1
v0 = this.mKey;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v1 = this.mValue;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.os.SystemProperties .set ( v0,v1 );
/* .line 609 */
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/audio/CloudServiceSettings$PropertySetting;->mSuccess:Z */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 613 */
/* .line 610 */
/* :catch_0 */
/* move-exception v0 */
/* .line 611 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "fail to set property, key: "; // const-string v2, "fail to set property, key: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mKey;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " value: "; // const-string v2, " value: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mValue;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", Exception: "; // const-string v2, ", Exception: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "CloudServiceSettings"; // const-string v2, "CloudServiceSettings"
android.util.Log .d ( v2,v1 );
/* .line 614 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
/* iget-boolean v0, p0, Lcom/android/server/audio/CloudServiceSettings$PropertySetting;->mSuccess:Z */
} // .end method
public Boolean updateTo ( com.android.server.audio.CloudServiceSettings$Setting p0 ) {
/* .locals 2 */
/* .param p1, "s" # Lcom/android/server/audio/CloudServiceSettings$Setting; */
/* .line 619 */
v0 = /* invoke-super {p0, p1}, Lcom/android/server/audio/CloudServiceSettings$Setting;->updateTo(Lcom/android/server/audio/CloudServiceSettings$Setting;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* instance-of v0, p1, Lcom/android/server/audio/CloudServiceSettings$PropertySetting; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 620 */
/* move-object v0, p1 */
/* check-cast v0, Lcom/android/server/audio/CloudServiceSettings$PropertySetting; */
/* .line 621 */
/* .local v0, "ps":Lcom/android/server/audio/CloudServiceSettings$PropertySetting; */
v1 = this.mKey;
this.mKey = v1;
/* .line 622 */
v1 = this.mValue;
this.mValue = v1;
/* .line 623 */
int v1 = 1; // const/4 v1, 0x1
/* .line 625 */
} // .end local v0 # "ps":Lcom/android/server/audio/CloudServiceSettings$PropertySetting;
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "fail to update PropertySetting: "; // const-string v1, "fail to update PropertySetting: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mSettingName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "CloudServiceSettings"; // const-string v1, "CloudServiceSettings"
android.util.Log .e ( v1,v0 );
/* .line 626 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
