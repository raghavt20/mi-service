class com.android.server.audio.MiAudioServiceMTK$DeviceChangeBroadcastReceiver extends android.content.BroadcastReceiver {
	 /* .source "MiAudioServiceMTK.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/audio/MiAudioServiceMTK; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "DeviceChangeBroadcastReceiver" */
} // .end annotation
/* # instance fields */
final com.android.server.audio.MiAudioServiceMTK this$0; //synthetic
/* # direct methods */
private com.android.server.audio.MiAudioServiceMTK$DeviceChangeBroadcastReceiver ( ) {
/* .locals 0 */
/* .line 116 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
 com.android.server.audio.MiAudioServiceMTK$DeviceChangeBroadcastReceiver ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/audio/MiAudioServiceMTK$DeviceChangeBroadcastReceiver;-><init>(Lcom/android/server/audio/MiAudioServiceMTK;)V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 119 */
v0 = this.this$0;
v0 = this.mDolbyEffectController;
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 120 */
	 final String v0 = "MiAudioServiceMTK"; // const-string v0, "MiAudioServiceMTK"
	 final String v1 = "DeviceChangeBroadcastReceiver onReceive"; // const-string v1, "DeviceChangeBroadcastReceiver onReceive"
	 android.util.Log .d ( v0,v1 );
	 /* .line 121 */
	 v0 = this.this$0;
	 v0 = this.mDolbyEffectController;
	 (( com.android.server.audio.dolbyeffect.DolbyEffectController ) v0 ).receiveDeviceConnectStateChanged ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->receiveDeviceConnectStateChanged(Landroid/content/Context;Landroid/content/Intent;)V
	 /* .line 123 */
} // :cond_0
return;
} // .end method
