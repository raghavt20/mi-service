.class Lcom/android/server/audio/CloudServiceThread$1;
.super Landroid/database/ContentObserver;
.source "CloudServiceThread.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/audio/CloudServiceThread;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/audio/CloudServiceThread;


# direct methods
.method constructor <init>(Lcom/android/server/audio/CloudServiceThread;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/audio/CloudServiceThread;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 45
    iput-object p1, p0, Lcom/android/server/audio/CloudServiceThread$1;->this$0:Lcom/android/server/audio/CloudServiceThread;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 3
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 48
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 49
    const-string v0, "CloudServiceThread"

    const-string v1, "cloud data changed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    iget-object v0, p0, Lcom/android/server/audio/CloudServiceThread$1;->this$0:Lcom/android/server/audio/CloudServiceThread;

    invoke-static {v0}, Lcom/android/server/audio/CloudServiceThread;->-$$Nest$fgetmLock(Lcom/android/server/audio/CloudServiceThread;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 51
    :try_start_0
    const-string v1, "CloudServiceThread"

    const-string v2, "enter before notify"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    iget-object v1, p0, Lcom/android/server/audio/CloudServiceThread$1;->this$0:Lcom/android/server/audio/CloudServiceThread;

    invoke-static {v1}, Lcom/android/server/audio/CloudServiceThread;->-$$Nest$fgetmLock(Lcom/android/server/audio/CloudServiceThread;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 53
    const-string v1, "CloudServiceThread"

    const-string v2, "enter after notify"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    monitor-exit v0

    .line 55
    return-void

    .line 54
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
