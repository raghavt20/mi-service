.class Lcom/android/server/audio/AudioGameEffect$2;
.super Landroid/content/BroadcastReceiver;
.source "AudioGameEffect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/AudioGameEffect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/audio/AudioGameEffect;


# direct methods
.method constructor <init>(Lcom/android/server/audio/AudioGameEffect;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/audio/AudioGameEffect;

    .line 262
    iput-object p1, p0, Lcom/android/server/audio/AudioGameEffect$2;->this$0:Lcom/android/server/audio/AudioGameEffect;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 266
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 267
    .local v0, "action":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onReceive, action="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AudioGameEffect"

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    iget-object v1, p0, Lcom/android/server/audio/AudioGameEffect$2;->this$0:Lcom/android/server/audio/AudioGameEffect;

    invoke-static {v1}, Lcom/android/server/audio/AudioGameEffect;->-$$Nest$fgetmCurrentEnablePkg(Lcom/android/server/audio/AudioGameEffect;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 269
    const-string v1, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x3

    const-string/jumbo v3, "speaker"

    const-string v4, "headsets"

    const/4 v5, -0x1

    const/4 v6, 0x2

    const-wide/16 v7, 0x1f4

    if-eqz v1, :cond_2

    .line 270
    const-string/jumbo v1, "state"

    invoke-virtual {p2, v1, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 271
    .local v1, "state":I
    const/4 v5, 0x1

    if-ne v1, v5, :cond_0

    .line 272
    iget-object v2, p0, Lcom/android/server/audio/AudioGameEffect$2;->this$0:Lcom/android/server/audio/AudioGameEffect;

    invoke-static {v2, v4}, Lcom/android/server/audio/AudioGameEffect;->-$$Nest$fputmCurrentDevice(Lcom/android/server/audio/AudioGameEffect;Ljava/lang/String;)V

    .line 273
    iget-object v2, p0, Lcom/android/server/audio/AudioGameEffect$2;->this$0:Lcom/android/server/audio/AudioGameEffect;

    invoke-static {v2, v6, v7, v8}, Lcom/android/server/audio/AudioGameEffect;->-$$Nest$msendMsgDelay(Lcom/android/server/audio/AudioGameEffect;IJ)V

    goto :goto_0

    .line 274
    :cond_0
    if-nez v1, :cond_5

    .line 275
    iget-object v4, p0, Lcom/android/server/audio/AudioGameEffect$2;->this$0:Lcom/android/server/audio/AudioGameEffect;

    invoke-static {v4, v3}, Lcom/android/server/audio/AudioGameEffect;->-$$Nest$fputmCurrentDevice(Lcom/android/server/audio/AudioGameEffect;Ljava/lang/String;)V

    .line 276
    iget-object v3, p0, Lcom/android/server/audio/AudioGameEffect$2;->this$0:Lcom/android/server/audio/AudioGameEffect;

    invoke-static {v3}, Lcom/android/server/audio/AudioGameEffect;->-$$Nest$misCurrentSceneSupported(Lcom/android/server/audio/AudioGameEffect;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 277
    iget-object v2, p0, Lcom/android/server/audio/AudioGameEffect$2;->this$0:Lcom/android/server/audio/AudioGameEffect;

    invoke-static {v2, v6, v7, v8}, Lcom/android/server/audio/AudioGameEffect;->-$$Nest$msendMsgDelay(Lcom/android/server/audio/AudioGameEffect;IJ)V

    goto :goto_0

    .line 279
    :cond_1
    iget-object v3, p0, Lcom/android/server/audio/AudioGameEffect$2;->this$0:Lcom/android/server/audio/AudioGameEffect;

    invoke-static {v3, v2, v7, v8}, Lcom/android/server/audio/AudioGameEffect;->-$$Nest$msendMsgDelay(Lcom/android/server/audio/AudioGameEffect;IJ)V

    goto :goto_0

    .line 282
    .end local v1    # "state":I
    :cond_2
    const-string v1, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 283
    const-string v1, "android.bluetooth.profile.extra.STATE"

    invoke-virtual {p2, v1, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 284
    .restart local v1    # "state":I
    if-ne v1, v6, :cond_3

    .line 285
    iget-object v2, p0, Lcom/android/server/audio/AudioGameEffect$2;->this$0:Lcom/android/server/audio/AudioGameEffect;

    invoke-static {v2, v4}, Lcom/android/server/audio/AudioGameEffect;->-$$Nest$fputmCurrentDevice(Lcom/android/server/audio/AudioGameEffect;Ljava/lang/String;)V

    .line 286
    iget-object v2, p0, Lcom/android/server/audio/AudioGameEffect$2;->this$0:Lcom/android/server/audio/AudioGameEffect;

    invoke-static {v2, v6, v7, v8}, Lcom/android/server/audio/AudioGameEffect;->-$$Nest$msendMsgDelay(Lcom/android/server/audio/AudioGameEffect;IJ)V

    goto :goto_1

    .line 287
    :cond_3
    if-nez v1, :cond_6

    .line 288
    iget-object v4, p0, Lcom/android/server/audio/AudioGameEffect$2;->this$0:Lcom/android/server/audio/AudioGameEffect;

    invoke-static {v4, v3}, Lcom/android/server/audio/AudioGameEffect;->-$$Nest$fputmCurrentDevice(Lcom/android/server/audio/AudioGameEffect;Ljava/lang/String;)V

    .line 289
    iget-object v3, p0, Lcom/android/server/audio/AudioGameEffect$2;->this$0:Lcom/android/server/audio/AudioGameEffect;

    invoke-static {v3}, Lcom/android/server/audio/AudioGameEffect;->-$$Nest$misCurrentSceneSupported(Lcom/android/server/audio/AudioGameEffect;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 290
    iget-object v2, p0, Lcom/android/server/audio/AudioGameEffect$2;->this$0:Lcom/android/server/audio/AudioGameEffect;

    invoke-static {v2, v6, v7, v8}, Lcom/android/server/audio/AudioGameEffect;->-$$Nest$msendMsgDelay(Lcom/android/server/audio/AudioGameEffect;IJ)V

    goto :goto_1

    .line 292
    :cond_4
    iget-object v3, p0, Lcom/android/server/audio/AudioGameEffect$2;->this$0:Lcom/android/server/audio/AudioGameEffect;

    invoke-static {v3, v2, v7, v8}, Lcom/android/server/audio/AudioGameEffect;->-$$Nest$msendMsgDelay(Lcom/android/server/audio/AudioGameEffect;IJ)V

    goto :goto_1

    .line 282
    .end local v1    # "state":I
    :cond_5
    :goto_0
    nop

    .line 297
    :cond_6
    :goto_1
    return-void
.end method
