public class com.android.server.audio.BtHelperImpl implements com.android.server.audio.BtHelperStub {
	 /* .source "BtHelperImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final Integer AUDIO_FORMAT_FORCE_AOSP;
	 private static final Integer SPATIAL_AUDIO_TYPE_SUPPORT_GYRO_AND_ALGO;
	 private static final Integer SPATIAL_AUDIO_TYPE_SUPPORT_GYRO_ONLY;
	 private static final java.lang.String TAG;
	 /* # direct methods */
	 public com.android.server.audio.BtHelperImpl ( ) {
		 /* .locals 0 */
		 /* .line 20 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Boolean checkEncoderFormat ( android.bluetooth.BluetoothCodecConfig p0 ) {
		 /* .locals 2 */
		 /* .param p1, "btCodecConfig" # Landroid/bluetooth/BluetoothCodecConfig; */
		 /* .line 30 */
		 v0 = 		 (( com.android.server.audio.BtHelperImpl ) p0 ).getFormat ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/audio/BtHelperImpl;->getFormat(Landroid/bluetooth/BluetoothCodecConfig;)I
		 /* const/high16 v1, 0x7f000000 */
		 /* if-ne v0, v1, :cond_0 */
		 /* .line 31 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* .line 33 */
	 } // :cond_0
	 int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean connectBtScoHelper ( android.bluetooth.BluetoothHeadset p0, com.android.server.audio.BtHelper p1 ) {
	 /* .locals 2 */
	 /* .param p1, "bluetoothheadset" # Landroid/bluetooth/BluetoothHeadset; */
	 /* .param p2, "bthelper" # Lcom/android/server/audio/BtHelper; */
	 /* .line 77 */
	 final String v0 = "bthelper try to connect the sco"; // const-string v0, "bthelper try to connect the sco"
	 final String v1 = "AS.BtHelperImpl"; // const-string v1, "AS.BtHelperImpl"
	 android.util.Log .i ( v1,v0 );
	 /* .line 78 */
	 int v0 = 1; // const/4 v0, 0x1
	 (( com.android.server.audio.BtHelper ) p2 ).broadcastScoConnectionState ( v0 ); // invoke-virtual {p2, v0}, Lcom/android/server/audio/BtHelper;->broadcastScoConnectionState(I)V
	 /* .line 79 */
	 /* if-nez p1, :cond_0 */
	 /* .line 80 */
	 final String v0 = "mBluetoothHeadset == null"; // const-string v0, "mBluetoothHeadset == null"
	 android.util.Log .i ( v1,v0 );
	 /* .line 81 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 83 */
} // :cond_0
v0 = (( android.bluetooth.BluetoothHeadset ) p1 ).startScoUsingVirtualVoiceCall ( ); // invoke-virtual {p1}, Landroid/bluetooth/BluetoothHeadset;->startScoUsingVirtualVoiceCall()Z
} // .end method
public Boolean disconnectBtScoHelper ( android.bluetooth.BluetoothHeadset p0, com.android.server.audio.BtHelper p1 ) {
/* .locals 3 */
/* .param p1, "bluetoothheadset" # Landroid/bluetooth/BluetoothHeadset; */
/* .param p2, "bthelper" # Lcom/android/server/audio/BtHelper; */
/* .line 67 */
final String v0 = "bthelper try to disconnect the sco"; // const-string v0, "bthelper try to disconnect the sco"
final String v1 = "AS.BtHelperImpl"; // const-string v1, "AS.BtHelperImpl"
android.util.Log .i ( v1,v0 );
/* .line 68 */
int v0 = 0; // const/4 v0, 0x0
(( com.android.server.audio.BtHelper ) p2 ).broadcastScoConnectionState ( v0 ); // invoke-virtual {p2, v0}, Lcom/android/server/audio/BtHelper;->broadcastScoConnectionState(I)V
/* .line 69 */
/* if-nez p1, :cond_0 */
/* .line 70 */
final String v2 = "mBluetoothHeadset == null"; // const-string v2, "mBluetoothHeadset == null"
android.util.Log .i ( v1,v2 );
/* .line 71 */
/* .line 73 */
} // :cond_0
v0 = (( android.bluetooth.BluetoothHeadset ) p1 ).stopScoUsingVirtualVoiceCall ( ); // invoke-virtual {p1}, Landroid/bluetooth/BluetoothHeadset;->stopScoUsingVirtualVoiceCall()Z
} // .end method
public Integer getFormat ( android.bluetooth.BluetoothCodecConfig p0 ) {
/* .locals 4 */
/* .param p1, "config" # Landroid/bluetooth/BluetoothCodecConfig; */
/* .line 38 */
try { // :try_start_0
/* const-class v0, Landroid/bluetooth/BluetoothCodecConfig; */
final String v1 = "getEncoderFormat"; // const-string v1, "getEncoderFormat"
int v2 = 0; // const/4 v2, 0x0
/* move-object v3, v2 */
/* check-cast v3, [Ljava/lang/Class; */
(( java.lang.Class ) v0 ).getDeclaredMethod ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 39 */
/* .local v0, "isGetFormatMethod":Ljava/lang/reflect/Method; */
int v1 = 1; // const/4 v1, 0x1
(( java.lang.reflect.Method ) v0 ).setAccessible ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 40 */
/* move-object v1, v2 */
/* check-cast v1, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v0 ).invoke ( p1, v2 ); // invoke-virtual {v0, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 41 */
} // .end local v0 # "isGetFormatMethod":Ljava/lang/reflect/Method;
/* :catch_0 */
/* move-exception v0 */
/* .line 42 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "getFormat error "; // const-string v2, "getFormat error "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "AS.BtHelperImpl"; // const-string v2, "AS.BtHelperImpl"
android.util.Log .e ( v2,v1 );
/* .line 44 */
} // .end local v0 # "e":Ljava/lang/Exception;
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean handleSpatialAudioDeviceConnect ( android.bluetooth.BluetoothDevice p0, android.bluetooth.BluetoothDevice p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "spatialDevice" # Landroid/bluetooth/BluetoothDevice; */
/* .param p2, "currActiveDevice" # Landroid/bluetooth/BluetoothDevice; */
/* .param p3, "spatialAudioType" # I */
/* .line 48 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "spatialAudioType: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AS.BtHelperImpl"; // const-string v1, "AS.BtHelperImpl"
android.util.Log .i ( v1,v0 );
/* .line 49 */
v0 = java.util.Objects .equals ( p1,p2 );
if ( v0 != null) { // if-eqz v0, :cond_1
int v0 = 2; // const/4 v0, 0x2
/* if-eq p3, v0, :cond_0 */
int v0 = 3; // const/4 v0, 0x3
/* if-ne p3, v0, :cond_1 */
/* .line 51 */
} // :cond_0
/* const-string/jumbo v0, "spatial audio device connect" */
android.util.Log .d ( v1,v0 );
/* .line 52 */
/* const-string/jumbo v0, "spatial_audio_headphone_connect=true" */
android.media.AudioSystem .setParameters ( v0 );
/* .line 53 */
int v0 = 1; // const/4 v0, 0x1
/* .line 55 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void handleSpatialAudioDeviceDisConnect ( android.bluetooth.BluetoothDevice p0, android.bluetooth.BluetoothDevice p1 ) {
/* .locals 2 */
/* .param p1, "spatialDevice" # Landroid/bluetooth/BluetoothDevice; */
/* .param p2, "previousActiveDevice" # Landroid/bluetooth/BluetoothDevice; */
/* .line 59 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 60 */
v0 = java.util.Objects .equals ( p1,p2 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 61 */
final String v0 = "AS.BtHelperImpl"; // const-string v0, "AS.BtHelperImpl"
final String v1 = "change to not spatial audio device"; // const-string v1, "change to not spatial audio device"
android.util.Log .d ( v0,v1 );
/* .line 62 */
/* const-string/jumbo v0, "spatial_audio_headphone_connect=false" */
android.media.AudioSystem .setParameters ( v0 );
/* .line 64 */
} // :cond_0
return;
} // .end method
public Boolean isFakeHfp ( android.content.Intent p0 ) {
/* .locals 2 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 87 */
final String v0 = "android.bluetooth.device.extra.NAME"; // const-string v0, "android.bluetooth.device.extra.NAME"
(( android.content.Intent ) p1 ).getStringExtra ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
/* .line 88 */
/* .local v0, "broadcastType":Ljava/lang/String; */
final String v1 = "fake_hfp_broadcast"; // const-string v1, "fake_hfp_broadcast"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
} // .end method
