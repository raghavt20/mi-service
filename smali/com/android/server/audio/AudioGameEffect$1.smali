.class Lcom/android/server/audio/AudioGameEffect$1;
.super Lmiui/process/IForegroundInfoListener$Stub;
.source "AudioGameEffect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/AudioGameEffect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/audio/AudioGameEffect;


# direct methods
.method constructor <init>(Lcom/android/server/audio/AudioGameEffect;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/audio/AudioGameEffect;

    .line 94
    iput-object p1, p0, Lcom/android/server/audio/AudioGameEffect$1;->this$0:Lcom/android/server/audio/AudioGameEffect;

    invoke-direct {p0}, Lmiui/process/IForegroundInfoListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onForegroundInfoChanged(Lmiui/process/ForegroundInfo;)V
    .locals 5
    .param p1, "foregroundInfo"    # Lmiui/process/ForegroundInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "foreground change to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", last foreground is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lmiui/process/ForegroundInfo;->mLastForegroundPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AudioGameEffect"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    iget-object v0, p0, Lcom/android/server/audio/AudioGameEffect$1;->this$0:Lcom/android/server/audio/AudioGameEffect;

    iget-object v2, p1, Lmiui/process/ForegroundInfo;->mLastForegroundPackageName:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/android/server/audio/AudioGameEffect;->-$$Nest$misPackageEnabled(Lcom/android/server/audio/AudioGameEffect;Ljava/lang/String;)Z

    move-result v0

    const-wide/16 v2, 0x1f4

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/android/server/audio/AudioGameEffect$1;->this$0:Lcom/android/server/audio/AudioGameEffect;

    const-string v4, ""

    invoke-static {v0, v4}, Lcom/android/server/audio/AudioGameEffect;->-$$Nest$fputmCurrentEnablePkg(Lcom/android/server/audio/AudioGameEffect;Ljava/lang/String;)V

    .line 100
    iget-object v0, p0, Lcom/android/server/audio/AudioGameEffect$1;->this$0:Lcom/android/server/audio/AudioGameEffect;

    invoke-static {v0}, Lcom/android/server/audio/AudioGameEffect;->-$$Nest$fgetisEffectOn(Lcom/android/server/audio/AudioGameEffect;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/android/server/audio/AudioGameEffect$1;->this$0:Lcom/android/server/audio/AudioGameEffect;

    const/4 v4, 0x3

    invoke-static {v0, v4, v2, v3}, Lcom/android/server/audio/AudioGameEffect;->-$$Nest$msendMsgDelay(Lcom/android/server/audio/AudioGameEffect;IJ)V

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/android/server/audio/AudioGameEffect$1;->this$0:Lcom/android/server/audio/AudioGameEffect;

    iget-object v4, p1, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/android/server/audio/AudioGameEffect;->-$$Nest$misPackageEnabled(Lcom/android/server/audio/AudioGameEffect;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 105
    iget-object v0, p0, Lcom/android/server/audio/AudioGameEffect$1;->this$0:Lcom/android/server/audio/AudioGameEffect;

    iget-object v4, p1, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/android/server/audio/AudioGameEffect;->-$$Nest$fputmCurrentEnablePkg(Lcom/android/server/audio/AudioGameEffect;Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/android/server/audio/AudioGameEffect$1;->this$0:Lcom/android/server/audio/AudioGameEffect;

    invoke-static {v0}, Lcom/android/server/audio/AudioGameEffect;->-$$Nest$misCurrentDeviceSupported(Lcom/android/server/audio/AudioGameEffect;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 107
    const-string v0, "current device not support"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    return-void

    .line 110
    :cond_1
    iget-object v0, p0, Lcom/android/server/audio/AudioGameEffect$1;->this$0:Lcom/android/server/audio/AudioGameEffect;

    invoke-static {v0}, Lcom/android/server/audio/AudioGameEffect;->-$$Nest$misSpatialAudioEnabled(Lcom/android/server/audio/AudioGameEffect;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 111
    const-string/jumbo v0, "spatial audio enabled, return"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    return-void

    .line 114
    :cond_2
    iget-object v0, p0, Lcom/android/server/audio/AudioGameEffect$1;->this$0:Lcom/android/server/audio/AudioGameEffect;

    const/4 v1, 0x2

    invoke-static {v0, v1, v2, v3}, Lcom/android/server/audio/AudioGameEffect;->-$$Nest$msendMsgDelay(Lcom/android/server/audio/AudioGameEffect;IJ)V

    .line 116
    :cond_3
    return-void
.end method
