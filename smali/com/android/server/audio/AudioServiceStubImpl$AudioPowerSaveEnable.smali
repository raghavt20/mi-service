.class public interface abstract Lcom/android/server/audio/AudioServiceStubImpl$AudioPowerSaveEnable;
.super Ljava/lang/Object;
.source "AudioServiceStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/AudioServiceStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AudioPowerSaveEnable"
.end annotation


# static fields
.field public static final AUDIOPOWERSAVE_AUDIOBIN_ENABLE:I = 0x4

.field public static final AUDIOPOWERSAVE_MAIN_ENABLE:I = 0x1

.field public static final AUDIOPOWERSAVE_OUTPUT_ENABLE:I = 0x2

.field public static final AUDIOPOWERSAVE_VOICETRIGGER_ENABLE:I = 0x8

.field public static final AUDIOPOWERSAVE_VOLUME_ENABLE:I = 0x10
