class com.android.server.audio.GameAudioEnhancer$WorkHandler extends android.os.Handler {
	 /* .source "GameAudioEnhancer.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/audio/GameAudioEnhancer; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "WorkHandler" */
} // .end annotation
/* # instance fields */
final com.android.server.audio.GameAudioEnhancer this$0; //synthetic
/* # direct methods */
public com.android.server.audio.GameAudioEnhancer$WorkHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 227 */
this.this$0 = p1;
/* .line 228 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 229 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 3 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 233 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 250 */
/* :pswitch_0 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.android.server.audio.GameAudioEnhancer .-$$Nest$msetGameMode ( v0,v1 );
/* .line 251 */
/* .line 247 */
/* :pswitch_1 */
v0 = this.this$0;
int v1 = 1; // const/4 v1, 0x1
com.android.server.audio.GameAudioEnhancer .-$$Nest$msetGameMode ( v0,v1 );
/* .line 248 */
/* .line 235 */
/* :pswitch_2 */
final String v0 = "ProcessManager"; // const-string v0, "ProcessManager"
android.os.ServiceManager .getService ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 236 */
	 v0 = this.this$0;
	 com.android.server.audio.GameAudioEnhancer .-$$Nest$fgetmForegroundInfoListener ( v0 );
	 miui.process.ProcessManager .registerForegroundInfoListener ( v0 );
	 /* .line 238 */
} // :cond_0
v0 = this.this$0;
v1 = com.android.server.audio.GameAudioEnhancer .-$$Nest$fgetmQueryPMServiceTime ( v0 );
/* add-int/lit8 v2, v1, 0x1 */
com.android.server.audio.GameAudioEnhancer .-$$Nest$fputmQueryPMServiceTime ( v0,v2 );
/* const/16 v0, 0x14 */
final String v2 = "GameAudioEnhancer"; // const-string v2, "GameAudioEnhancer"
/* if-ge v1, v0, :cond_1 */
/* .line 239 */
final String v0 = "process manager service not published, wait 1 second"; // const-string v0, "process manager service not published, wait 1 second"
android.util.Log .w ( v2,v0 );
/* .line 240 */
v0 = this.this$0;
com.android.server.audio.GameAudioEnhancer .-$$Nest$minitProcessListenerAsync ( v0 );
/* .line 242 */
} // :cond_1
final String v0 = "failed to get ProcessManager service"; // const-string v0, "failed to get ProcessManager service"
android.util.Log .e ( v2,v0 );
/* .line 245 */
/* nop */
/* .line 255 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
