.class Lcom/android/server/audio/AudioThermalObserver$WorkHandler;
.super Landroid/os/Handler;
.source "AudioThermalObserver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/AudioThermalObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WorkHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/audio/AudioThermalObserver;


# direct methods
.method public constructor <init>(Lcom/android/server/audio/AudioThermalObserver;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 65
    iput-object p1, p0, Lcom/android/server/audio/AudioThermalObserver$WorkHandler;->this$0:Lcom/android/server/audio/AudioThermalObserver;

    .line 66
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 67
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .line 71
    :try_start_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 77
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/audio/AudioThermalObserver$WorkHandler;->this$0:Lcom/android/server/audio/AudioThermalObserver;

    invoke-static {v0}, Lcom/android/server/audio/AudioThermalObserver;->-$$Nest$fgetmHandler(Lcom/android/server/audio/AudioThermalObserver;)Lcom/android/server/audio/AudioThermalObserver$WorkHandler;

    move-result-object v0

    const/16 v1, 0x9c5

    invoke-virtual {v0, v1}, Lcom/android/server/audio/AudioThermalObserver$WorkHandler;->removeMessages(I)V

    .line 78
    iget-object v0, p0, Lcom/android/server/audio/AudioThermalObserver$WorkHandler;->this$0:Lcom/android/server/audio/AudioThermalObserver;

    const-string v1, "/sys/class/thermal/thermal_message/video_mode"

    invoke-static {v0, v1}, Lcom/android/server/audio/AudioThermalObserver;->-$$Nest$mcheckAudioHighTemperatureMode(Lcom/android/server/audio/AudioThermalObserver;Ljava/lang/String;)V

    .line 79
    goto :goto_0

    .line 73
    :pswitch_1
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 74
    .local v0, "mAudioHighTemperatureMode":I
    iget-object v1, p0, Lcom/android/server/audio/AudioThermalObserver$WorkHandler;->this$0:Lcom/android/server/audio/AudioThermalObserver;

    invoke-static {v1, v0}, Lcom/android/server/audio/AudioThermalObserver;->-$$Nest$msetAudioHighTemperatureMode(Lcom/android/server/audio/AudioThermalObserver;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    nop

    .line 85
    .end local v0    # "mAudioHighTemperatureMode":I
    :goto_0
    goto :goto_1

    .line 83
    :catch_0
    move-exception v0

    .line 84
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMessage exception "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AudioThermalObserver"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void

    :pswitch_data_0
    .packed-switch 0x9c5
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
