.class public Lcom/android/server/audio/AudioDeviceBrokerStubImpl;
.super Ljava/lang/Object;
.source "AudioDeviceBrokerStubImpl.java"

# interfaces
.implements Lcom/android/server/audio/AudioDeviceBrokerStub;


# static fields
.field private static final TAG:Ljava/lang/String; = "AudioDeviceBrokerStubImpl"


# instance fields
.field private isappbleneed:Z

.field private mMiSound:Landroid/media/audiofx/MiSound;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/audio/AudioDeviceBrokerStubImpl;->isappbleneed:Z

    return-void
.end method


# virtual methods
.method public getDeviceAttrAddr(Ljava/util/LinkedList;ILcom/android/server/audio/AudioDeviceInventory;)Ljava/lang/String;
    .locals 5
    .param p2, "audiodeviceBLEHeadset"    # I
    .param p3, "deviceinventory"    # Lcom/android/server/audio/AudioDeviceInventory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList<",
            "Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;",
            ">;I",
            "Lcom/android/server/audio/AudioDeviceInventory;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 61
    .local p1, "mCommunicationRouteClients":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;>;"
    invoke-virtual {p1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 62
    invoke-virtual {p1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 63
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 64
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;

    .line 65
    .local v1, "cl":Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;
    invoke-virtual {v1}, Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;->getDevice()Landroid/media/AudioDeviceAttributes;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;->getDevice()Landroid/media/AudioDeviceAttributes;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/AudioDeviceAttributes;->getType()I

    move-result v2

    if-ne v2, p2, :cond_0

    .line 66
    invoke-virtual {p3}, Lcom/android/server/audio/AudioDeviceInventory;->getLeAudioAddress()Ljava/lang/String;

    move-result-object v2

    .line 67
    .local v2, "address":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 68
    const-string v3, "AudioDeviceBrokerStubImpl"

    const-string v4, "preferredCommunicationDevice, return ble device when ble is selected by other app"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    return-object v2

    .line 72
    .end local v1    # "cl":Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;
    .end local v2    # "address":Ljava/lang/String;
    :cond_0
    goto :goto_0

    .line 74
    .end local v0    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;>;"
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public isSetSpeakerphoneOn(ZZIZ)Z
    .locals 4
    .param p1, "isLeDeviceConnected"    # Z
    .param p2, "speakerOn"    # Z
    .param p3, "pid"    # I
    .param p4, "isBuiltinSpkType"    # Z

    .line 33
    if-eqz p1, :cond_0

    if-nez p2, :cond_0

    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setSpeakerphoneOn, on: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " pid: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, "when ble is connect. getCommunicationRouteClientForPid directly to avoid waiting too long"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "AudioDeviceBrokerStubImpl"

    invoke-static {v3, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    if-eqz p4, :cond_0

    .line 37
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "when ble is connect return."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    const/4 v0, 0x1

    return v0

    .line 42
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public setBTBLEParameters(Landroid/media/AudioDeviceAttributes;I)V
    .locals 1
    .param p1, "preferredCommunicationDevice"    # Landroid/media/AudioDeviceAttributes;
    .param p2, "audiodeviceBLEHeadset"    # I

    .line 79
    if-eqz p1, :cond_0

    .line 80
    invoke-virtual {p1}, Landroid/media/AudioDeviceAttributes;->getType()I

    move-result v0

    if-eq v0, p2, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/android/server/audio/AudioDeviceBrokerStubImpl;->isappbleneed:Z

    if-eqz v0, :cond_1

    .line 81
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/audio/AudioDeviceBrokerStubImpl;->isappbleneed:Z

    .line 82
    const-string v0, "BT_BLE=off"

    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    goto :goto_0

    .line 83
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/media/AudioDeviceAttributes;->getType()I

    move-result v0

    if-ne v0, p2, :cond_2

    iget-boolean v0, p0, Lcom/android/server/audio/AudioDeviceBrokerStubImpl;->isappbleneed:Z

    if-nez v0, :cond_2

    .line 84
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/audio/AudioDeviceBrokerStubImpl;->isappbleneed:Z

    .line 85
    const-string v0, "BT_BLE=on"

    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 87
    :cond_2
    :goto_0
    return-void
.end method

.method public setMiSoundEnable(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .line 92
    const-string v0, "AudioDeviceBrokerStubImpl"

    :try_start_0
    new-instance v1, Landroid/media/audiofx/MiSound;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v2}, Landroid/media/audiofx/MiSound;-><init>(II)V

    iput-object v1, p0, Lcom/android/server/audio/AudioDeviceBrokerStubImpl;->mMiSound:Landroid/media/audiofx/MiSound;

    .line 93
    invoke-virtual {v1, p1}, Landroid/media/audiofx/MiSound;->setEnabled(Z)I
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 101
    iget-object v0, p0, Lcom/android/server/audio/AudioDeviceBrokerStubImpl;->mMiSound:Landroid/media/audiofx/MiSound;

    if-eqz v0, :cond_0

    .line 102
    :goto_0
    invoke-virtual {v0}, Landroid/media/audiofx/MiSound;->release()V

    goto :goto_1

    .line 101
    :catchall_0
    move-exception v0

    goto :goto_2

    .line 98
    :catch_0
    move-exception v1

    .line 99
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateMisoundTunning: RuntimeException"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 101
    nop

    .end local v1    # "e":Ljava/lang/RuntimeException;
    iget-object v0, p0, Lcom/android/server/audio/AudioDeviceBrokerStubImpl;->mMiSound:Landroid/media/audiofx/MiSound;

    if-eqz v0, :cond_0

    .line 102
    goto :goto_0

    .line 96
    :catch_1
    move-exception v1

    .line 97
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    :try_start_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateMisoundTunning: IllegalArgumentException"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 101
    nop

    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    iget-object v0, p0, Lcom/android/server/audio/AudioDeviceBrokerStubImpl;->mMiSound:Landroid/media/audiofx/MiSound;

    if-eqz v0, :cond_0

    .line 102
    goto :goto_0

    .line 94
    :catch_2
    move-exception v1

    .line 95
    .local v1, "e":Ljava/lang/UnsupportedOperationException;
    :try_start_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateMisoundTunning: UnsupportedOperationException"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 101
    nop

    .end local v1    # "e":Ljava/lang/UnsupportedOperationException;
    iget-object v0, p0, Lcom/android/server/audio/AudioDeviceBrokerStubImpl;->mMiSound:Landroid/media/audiofx/MiSound;

    if-eqz v0, :cond_0

    .line 102
    goto :goto_0

    .line 105
    :cond_0
    :goto_1
    return-void

    .line 101
    :goto_2
    iget-object v1, p0, Lcom/android/server/audio/AudioDeviceBrokerStubImpl;->mMiSound:Landroid/media/audiofx/MiSound;

    if-eqz v1, :cond_1

    .line 102
    invoke-virtual {v1}, Landroid/media/audiofx/MiSound;->release()V

    .line 104
    :cond_1
    throw v0
.end method

.method public updateBtScoAudioHelper(ZZLcom/android/server/audio/BtHelper;Z)V
    .locals 2
    .param p1, "wasblescorequested"    # Z
    .param p2, "isblescorequested"    # Z
    .param p3, "bthelper"    # Lcom/android/server/audio/BtHelper;
    .param p4, "iseventsourcestartswith"    # Z

    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setCommunicationRouteForClient for LEA "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AudioDeviceBrokerStubImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    if-eqz p1, :cond_0

    if-nez p2, :cond_0

    if-nez p4, :cond_0

    .line 52
    invoke-virtual {p3}, Lcom/android/server/audio/BtHelper;->disconnectBluetoothScoAudioHelper()Z

    goto :goto_0

    .line 53
    :cond_0
    if-nez p1, :cond_1

    if-eqz p2, :cond_1

    if-nez p4, :cond_1

    .line 54
    invoke-virtual {p3}, Lcom/android/server/audio/BtHelper;->connectBluetoothScoAudioHelper()Z

    .line 56
    :cond_1
    :goto_0
    return-void
.end method
