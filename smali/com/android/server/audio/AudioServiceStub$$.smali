.class public final Lcom/android/server/audio/AudioServiceStub$$;
.super Ljava/lang/Object;
.source "AudioServiceStub$$.java"

# interfaces
.implements Lcom/miui/base/MiuiStubRegistry$ImplProviderManifest;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final collectImplProviders(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/base/MiuiStubRegistry$ImplProvider<",
            "*>;>;)V"
        }
    .end annotation

    .line 19
    .local p1, "outProviders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/base/MiuiStubRegistry$ImplProvider<*>;>;"
    new-instance v0, Lcom/android/server/WiredAccessoryManagerStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/WiredAccessoryManagerStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.WiredAccessoryManagerStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    new-instance v0, Lcom/android/server/audio/SpatializerHelperStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/audio/SpatializerHelperStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.audio.SpatializerHelperStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    new-instance v0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.audio.PlaybackActivityMonitorStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    new-instance v0, Lcom/android/server/audio/AudioDeviceInventoryStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/audio/AudioDeviceInventoryStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.audio.AudioDeviceInventoryStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    new-instance v0, Lcom/android/server/audio/BtHelperImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/audio/BtHelperImpl$Provider;-><init>()V

    const-string v1, "com.android.server.audio.BtHelperStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    new-instance v0, Lcom/android/server/audio/FocusRequesterStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/audio/FocusRequesterStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.audio.FocusRequesterStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    new-instance v0, Lcom/android/server/audio/SoundDoseHelperStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/audio/SoundDoseHelperStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.audio.SoundDoseHelperStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    new-instance v0, Lcom/android/server/audio/MediaFocusControlStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/audio/MediaFocusControlStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.audio.MediaFocusControlStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    new-instance v0, Lcom/android/server/audio/AudioServiceStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/audio/AudioServiceStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.audio.AudioServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    new-instance v0, Lcom/android/server/audio/AudioDeviceBrokerStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/audio/AudioDeviceBrokerStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.audio.AudioDeviceBrokerStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    return-void
.end method
