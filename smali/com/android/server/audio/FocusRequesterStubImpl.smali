.class public Lcom/android/server/audio/FocusRequesterStubImpl;
.super Ljava/lang/Object;
.source "FocusRequesterStubImpl.java"

# interfaces
.implements Lcom/android/server/audio/FocusRequesterStub;


# static fields
.field private static final TAG:Ljava/lang/String; = "FocusRequesterStubImpl"


# instance fields
.field private mIGreezeManager:Lmiui/greeze/IGreezeManager;


# direct methods
.method static bridge synthetic -$$Nest$mgetGreeze(Lcom/android/server/audio/FocusRequesterStubImpl;)Lmiui/greeze/IGreezeManager;
    .locals 0

    invoke-direct {p0}, Lcom/android/server/audio/FocusRequesterStubImpl;->getGreeze()Lmiui/greeze/IGreezeManager;

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>()V
    .locals 1

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/audio/FocusRequesterStubImpl;->mIGreezeManager:Lmiui/greeze/IGreezeManager;

    return-void
.end method

.method private getGreeze()Lmiui/greeze/IGreezeManager;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/android/server/audio/FocusRequesterStubImpl;->mIGreezeManager:Lmiui/greeze/IGreezeManager;

    if-nez v0, :cond_0

    .line 23
    nop

    .line 24
    const-string v0, "greezer"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 23
    invoke-static {v0}, Lmiui/greeze/IGreezeManager$Stub;->asInterface(Landroid/os/IBinder;)Lmiui/greeze/IGreezeManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/audio/FocusRequesterStubImpl;->mIGreezeManager:Lmiui/greeze/IGreezeManager;

    .line 25
    :cond_0
    iget-object v0, p0, Lcom/android/server/audio/FocusRequesterStubImpl;->mIGreezeManager:Lmiui/greeze/IGreezeManager;

    return-object v0
.end method


# virtual methods
.method public notifyFocusChange(ILandroid/os/Handler;Ljava/lang/String;)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "h"    # Landroid/os/Handler;
    .param p3, "reason"    # Ljava/lang/String;

    .line 29
    invoke-direct {p0}, Lcom/android/server/audio/FocusRequesterStubImpl;->getGreeze()Lmiui/greeze/IGreezeManager;

    move-result-object v0

    if-nez v0, :cond_0

    .line 30
    return-void

    .line 32
    :cond_0
    invoke-static {p1}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    .line 33
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "notifyFocusChange uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FocusRequesterStubImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    new-instance v0, Lcom/android/server/audio/FocusRequesterStubImpl$1;

    invoke-direct {v0, p0, p1, p3}, Lcom/android/server/audio/FocusRequesterStubImpl$1;-><init>(Lcom/android/server/audio/FocusRequesterStubImpl;ILjava/lang/String;)V

    invoke-virtual {p2, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 48
    :cond_1
    return-void
.end method
