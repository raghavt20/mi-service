.class public Lcom/android/server/audio/dolbyeffect/DolbyEffectController;
.super Ljava/lang/Object;
.source "DolbyEffectController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/audio/dolbyeffect/DolbyEffectController$DolbyTunning;,
        Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;
    }
.end annotation


# static fields
.field private static final EXTRA_VOLUME_STREAM_TYPE:Ljava/lang/String; = "android.media.EXTRA_VOLUME_STREAM_TYPE"

.field private static final HEADSET_STATE:Ljava/lang/String; = "state"

.field static final MANU_ID_XIAO_MI:I = 0x38f

.field private static final MSG_BT_STATE_CHANGED:I = 0x1

.field private static final MSG_BT_STATE_CHANGED_DEVICEBROKER:I = 0x3

.field private static final MSG_HEADSET_PLUG:I = 0x2

.field private static final MSG_ROTATION_CHANGED:I = 0x4

.field private static final MSG_SPATIALIZER_AVALIABLE_STATE:I = 0x6

.field private static final MSG_SPATIALIZER_ENABLED_STATE:I = 0x5

.field private static final MSG_VOLUME_CHANGED:I = 0x0

.field private static final PORT_BLUETOOTH:I = 0x4

.field private static final PORT_HDMI:I = 0x1

.field private static final PORT_HEADPHONE:I = 0x3

.field private static final PORT_INTERNAL_SPEAKER:I = 0x0

.field private static final PORT_MIRACAST:I = 0x2

.field private static final PORT_USB:I = 0x5

.field private static final TAG:Ljava/lang/String; = "DolbyEffectController"

.field private static mCurrentDevices:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile sInstance:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

.field private static sService:Landroid/media/IAudioService;


# instance fields
.field private final mAdspSpatializerAvailable:Z

.field private mAdspSpatializerEnable:Z

.field private mContext:Landroid/content/Context;

.field private mCurrentDolbyTunning:Lcom/android/server/audio/dolbyeffect/DolbyEffectController$DolbyTunning;

.field private mCurrentPort:I

.field private mCurrentRotation:Ljava/lang/String;

.field private mCurrentVolumeLevel:Ljava/lang/String;

.field private mDolbyAudioEffect:Lcom/dolby/dax/DolbyAudioEffect;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private final mIsSupportFWAudioEffectCenter:Z

.field private mSpatializerEnabledAvaliable:[Z

.field private mVolumeThreshold:I

.field private mWorkHandler:Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;


# direct methods
.method static bridge synthetic -$$Nest$monBTStateChanged(Lcom/android/server/audio/dolbyeffect/DolbyEffectController;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->onBTStateChanged(Landroid/content/Intent;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monBTStateChangedFromDeviceBroker(Lcom/android/server/audio/dolbyeffect/DolbyEffectController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->onBTStateChangedFromDeviceBroker(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monHeadsetPlug(Lcom/android/server/audio/dolbyeffect/DolbyEffectController;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->onHeadsetPlug(Landroid/content/Intent;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monRotationChanged(Lcom/android/server/audio/dolbyeffect/DolbyEffectController;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->onRotationChanged(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monSpatializerStateChanged(Lcom/android/server/audio/dolbyeffect/DolbyEffectController;IZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->onSpatializerStateChanged(IZ)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monVolumeChanged(Lcom/android/server/audio/dolbyeffect/DolbyEffectController;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->onVolumeChanged(I)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 66
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDevices:Ljava/util/LinkedList;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    nop

    .line 62
    const-string v0, "ro.vendor.audio.fweffect"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mIsSupportFWAudioEffectCenter:Z

    .line 68
    const/16 v0, 0x64

    iput v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mVolumeThreshold:I

    .line 69
    const-string v0, "disabled"

    iput-object v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentVolumeLevel:Ljava/lang/String;

    .line 70
    iput-object v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentRotation:Ljava/lang/String;

    .line 71
    iput v1, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I

    .line 72
    const/4 v0, 0x2

    new-array v0, v0, [Z

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mSpatializerEnabledAvaliable:[Z

    .line 73
    nop

    .line 74
    const-string v0, "ro.vendor.audio.adsp.spatial"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mAdspSpatializerAvailable:Z

    .line 75
    iput-boolean v1, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mAdspSpatializerEnable:Z

    .line 81
    new-instance v0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$DolbyTunning;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$DolbyTunning;-><init>(Lcom/android/server/audio/dolbyeffect/DolbyEffectController;Lcom/android/server/audio/dolbyeffect/DolbyEffectController$DolbyTunning-IA;)V

    iput-object v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDolbyTunning:Lcom/android/server/audio/dolbyeffect/DolbyEffectController$DolbyTunning;

    .line 85
    iput-object p1, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mContext:Landroid/content/Context;

    .line 86
    return-void

    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
    .end array-data
.end method

.method private containsDevice(Ljava/lang/String;)I
    .locals 3
    .param p1, "deviceName"    # Ljava/lang/String;

    .line 536
    const/4 v0, 0x0

    .line 537
    .local v0, "product":Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDevices:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 538
    sget-object v2, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDevices:Ljava/util/LinkedList;

    invoke-virtual {v2, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;

    .line 539
    invoke-virtual {v0}, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->getDevice()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 540
    return v1

    .line 537
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 543
    .end local v1    # "i":I
    :cond_1
    const/4 v1, -0x1

    return v1
.end method

.method private getDeviceidForDevice(III)Ljava/lang/String;
    .locals 2
    .param p1, "type"    # I
    .param p2, "id1"    # I
    .param p3, "id2"    # I

    .line 715
    const-string v0, ""

    .line 716
    .local v0, "device_id":Ljava/lang/String;
    sget v1, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->TYPE_USB:I

    if-ne p1, v1, :cond_0

    .line 717
    invoke-static {p2, p3}, Lcom/android/server/audio/dolbyeffect/DeviceId;->getUsbDeviceId(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 718
    :cond_0
    sget v1, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->TYPE_BT:I

    if-ne p1, v1, :cond_1

    .line 719
    invoke-static {p2, p3}, Lcom/android/server/audio/dolbyeffect/DeviceId;->getBtDeviceId(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 720
    :cond_1
    sget v1, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->TYPE_WIRED:I

    if-ne p1, v1, :cond_2

    .line 721
    invoke-static {p2, p3}, Lcom/android/server/audio/dolbyeffect/DeviceId;->getWiredDeviceId(II)Ljava/lang/String;

    move-result-object v0

    .line 723
    :cond_2
    :goto_0
    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/server/audio/dolbyeffect/DolbyEffectController;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .line 92
    sget-object v0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->sInstance:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    if-nez v0, :cond_0

    .line 93
    new-instance v0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    invoke-direct {v0, p0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->sInstance:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    .line 95
    :cond_0
    sget-object v0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->sInstance:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    return-object v0
.end method

.method private static getService()Landroid/media/IAudioService;
    .locals 2

    .line 513
    sget-object v0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->sService:Landroid/media/IAudioService;

    if-eqz v0, :cond_0

    .line 514
    return-object v0

    .line 516
    :cond_0
    const-string v0, "audio"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 517
    .local v0, "b":Landroid/os/IBinder;
    invoke-static {v0}, Landroid/media/IAudioService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/media/IAudioService;

    move-result-object v1

    sput-object v1, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->sService:Landroid/media/IAudioService;

    .line 518
    return-object v1
.end method

.method private notifyEffectChanged()V
    .locals 2

    .line 506
    const-string v0, "DolbyEffectController"

    const-string v1, "notifyEffectChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.ACTION_AUDIO_EFFECT_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 508
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 509
    return-void
.end method

.method private onBTStateChanged(Landroid/content/Intent;)V
    .locals 19
    .param p1, "intent"    # Landroid/content/Intent;

    .line 308
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 309
    .local v2, "action":Ljava/lang/String;
    if-eqz v2, :cond_5

    .line 310
    const/4 v3, 0x0

    .line 311
    .local v3, "changed":Z
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v4

    const/4 v5, -0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    sparse-switch v4, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string v4, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v4, v6

    goto :goto_1

    :sswitch_1
    const-string v4, "android.bluetooth.headset.action.VENDOR_SPECIFIC_HEADSET_EVENT"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v4, v7

    goto :goto_1

    :goto_0
    move v4, v5

    :goto_1
    const-string v8, "DolbyEffectController"

    packed-switch v4, :pswitch_data_0

    goto/16 :goto_4

    .line 334
    :pswitch_0
    const-string v4, "android.bluetooth.device.extra.BOND_STATE"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 335
    .local v4, "state":I
    const-string v5, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/bluetooth/BluetoothDevice;

    .line 336
    .local v5, "extraDevice":Landroid/bluetooth/BluetoothDevice;
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    :cond_1
    const-string v6, "NULL"

    .line 337
    .local v6, "deviceName2":Ljava/lang/String;
    :goto_2
    const/16 v7, 0xa

    if-ne v4, v7, :cond_4

    .line 338
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ACTION_BOND_STATE_CHANGED: device "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, " bond disconnect"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    invoke-direct {v0, v6}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->removeDeviceForName(Ljava/lang/String;)Z

    move-result v3

    goto/16 :goto_4

    .line 313
    .end local v4    # "state":I
    .end local v5    # "extraDevice":Landroid/bluetooth/BluetoothDevice;
    .end local v6    # "deviceName2":Ljava/lang/String;
    :pswitch_1
    const/4 v4, 0x2

    new-array v4, v4, [I

    .line 314
    .local v4, "ids":[I
    const-string v5, ""

    filled-new-array {v5}, [Ljava/lang/String;

    move-result-object v5

    .line 315
    .local v5, "deviceName":[Ljava/lang/String;
    invoke-static {v1, v4, v5}, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->tryGetIdsFromIntent(Landroid/content/Intent;[I[Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    aget v9, v4, v7

    if-eqz v9, :cond_4

    aget v9, v4, v6

    if-eqz v9, :cond_4

    .line 316
    aget-object v9, v5, v7

    invoke-direct {v0, v9}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->containsDevice(Ljava/lang/String;)I

    move-result v9

    .line 317
    .local v9, "idx":I
    const-string v10, " minorid: "

    const-string v11, " majorid: "

    if-ltz v9, :cond_3

    .line 318
    sget-object v12, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDevices:Ljava/util/LinkedList;

    invoke-virtual {v12, v9}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;

    .line 319
    .local v12, "device2":Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;
    invoke-virtual {v12}, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->getMajorID()I

    move-result v13

    aget v14, v4, v7

    if-eq v13, v14, :cond_2

    invoke-virtual {v12}, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->getMinorID()I

    move-result v13

    aget v14, v4, v6

    if-eq v13, v14, :cond_2

    .line 320
    aget v13, v4, v7

    invoke-virtual {v12, v13}, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->setMajorID(I)V

    .line 321
    aget v13, v4, v6

    invoke-virtual {v12, v13}, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->setMinorID(I)V

    .line 322
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "onBTStateChanged: device updated "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    aget-object v14, v5, v7

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    aget v7, v4, v7

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget v6, v4, v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v8, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    const/4 v3, 0x1

    .line 325
    .end local v12    # "device2":Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;
    :cond_2
    goto :goto_3

    .line 326
    :cond_3
    new-instance v18, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;

    sget v13, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->TYPE_BT:I

    aget v14, v4, v7

    aget v15, v4, v6

    aget-object v16, v5, v7

    const/16 v17, 0x0

    move-object/from16 v12, v18

    invoke-direct/range {v12 .. v17}, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;-><init>(IIILjava/lang/String;Z)V

    .line 327
    .local v12, "newDevice":Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;
    sget-object v13, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDevices:Ljava/util/LinkedList;

    invoke-virtual {v13, v12}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 328
    const/4 v3, 0x1

    .line 329
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "onBTStateChanged: device connected "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    aget-object v14, v5, v7

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    aget v7, v4, v7

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget v6, v4, v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v8, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    .end local v9    # "idx":I
    .end local v12    # "newDevice":Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;
    :goto_3
    nop

    .line 345
    .end local v4    # "ids":[I
    .end local v5    # "deviceName":[Ljava/lang/String;
    :cond_4
    :goto_4
    if-eqz v3, :cond_5

    .line 346
    sget-object v4, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDevices:Ljava/util/LinkedList;

    invoke-direct {v0, v4}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->onDeviceChanged(Ljava/util/LinkedList;)V

    .line 349
    .end local v3    # "changed":Z
    :cond_5
    return-void

    :sswitch_data_0
    .sparse-switch
        0x69ab72ba -> :sswitch_1
        0x7e2cc189 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private onBTStateChangedFromDeviceBroker(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "device"    # Ljava/lang/String;
    .param p2, "profile"    # Ljava/lang/String;
    .param p3, "state"    # Ljava/lang/String;

    .line 352
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onBTStateChangedFromDeviceBroker btDevice="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " profile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DolbyEffectController"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    const-string v0, "A2DP"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 357
    const-string v0, "STATE_CONNECTED"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 358
    invoke-direct {p0, p1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->containsDevice(Ljava/lang/String;)I

    move-result v0

    .line 359
    .local v0, "idx":I
    if-ltz v0, :cond_0

    .line 360
    sget-object v2, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDevices:Ljava/util/LinkedList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;

    .line 361
    .local v2, "product":Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;
    sget-object v3, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDevices:Ljava/util/LinkedList;

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    .line 362
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->setState(Z)V

    .line 363
    sget-object v3, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDevices:Ljava/util/LinkedList;

    invoke-virtual {v3, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 364
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onBTStateChangedFromDeviceBroker: device active: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " majorid: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->getMajorID()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " minorid: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->getMinorID()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    .end local v2    # "product":Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;
    goto :goto_0

    .line 366
    :cond_0
    sget-object v2, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDevices:Ljava/util/LinkedList;

    new-instance v9, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;

    sget v4, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->TYPE_BT:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x1

    move-object v3, v9

    move-object v7, p1

    invoke-direct/range {v3 .. v8}, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;-><init>(IIILjava/lang/String;Z)V

    invoke-virtual {v2, v9}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 367
    const-string v2, "onBTStateChangedFromDeviceBroker: Bt headset connected"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    :goto_0
    sget-object v1, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDevices:Ljava/util/LinkedList;

    invoke-direct {p0, v1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->onDeviceChanged(Ljava/util/LinkedList;)V

    .end local v0    # "idx":I
    goto :goto_1

    .line 370
    :cond_1
    const-string v0, "STATE_DISCONNECTED"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 371
    invoke-direct {p0, p1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->containsDevice(Ljava/lang/String;)I

    move-result v0

    .line 372
    .restart local v0    # "idx":I
    if-ltz v0, :cond_3

    .line 373
    sget-object v2, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDevices:Ljava/util/LinkedList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;

    .line 374
    .restart local v2    # "product":Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->setState(Z)V

    .line 375
    sget-object v3, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDevices:Ljava/util/LinkedList;

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    .line 376
    const-string v3, "onBTStateChangedFromDeviceBroker: Bt headset disconnected"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    sget-object v1, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDevices:Ljava/util/LinkedList;

    invoke-direct {p0, v1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->onDeviceChanged(Ljava/util/LinkedList;)V

    goto :goto_2

    .line 370
    .end local v0    # "idx":I
    .end local v2    # "product":Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;
    :cond_2
    :goto_1
    nop

    .line 381
    :cond_3
    :goto_2
    return-void
.end method

.method private onDeviceChanged(Ljava/util/LinkedList;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList<",
            "Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;",
            ">;)V"
        }
    .end annotation

    .line 442
    .local p1, "currentDevices":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;>;"
    const-string v0, "onDeviceChanged"

    const-string v1, "DolbyEffectController"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    :try_start_0
    iget v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I

    .line 445
    .local v0, "cacheCurrentPort":I
    invoke-virtual {p1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    const/4 v3, 0x3

    const/4 v4, 0x0

    if-eqz v2, :cond_2

    .line 446
    const-string v2, "no devices connected, CurrentPort is set to INTERNAL_SPEAKER"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 447
    iput v4, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I

    .line 448
    const-string/jumbo v2, "true"

    const-string/jumbo v4, "vendor.audio.dolby.control.tunning.by.volume.support"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 449
    invoke-static {}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->getService()Landroid/media/IAudioService;

    move-result-object v2

    const/4 v4, 0x2

    invoke-interface {v2, v3, v4}, Landroid/media/IAudioService;->getDeviceStreamVolume(II)I

    move-result v2

    .line 450
    .local v2, "index":I
    iget v3, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mVolumeThreshold:I

    if-le v2, v3, :cond_0

    .line 451
    const-string v3, "HIGH"

    iput-object v3, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentVolumeLevel:Ljava/lang/String;

    .line 452
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "no devices connected and volume >= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mVolumeThreshold:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 454
    :cond_0
    const-string v3, "LOW"

    iput-object v3, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentVolumeLevel:Ljava/lang/String;

    .line 455
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "no devices connected and volume < "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mVolumeThreshold:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 458
    .end local v2    # "index":I
    :cond_1
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateDolbyTunning from onDeviceChanged(),mCurrentPort is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " newPort is"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    invoke-virtual {p0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->updateDolbyTunning()V

    goto :goto_2

    .line 461
    :cond_2
    invoke-virtual {p1}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;

    .line 462
    .local v2, "device":Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;
    invoke-virtual {v2}, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->getDeviceType()I

    move-result v5

    sget v6, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->TYPE_USB:I

    if-ne v5, v6, :cond_3

    .line 463
    const/4 v3, 0x5

    iput v3, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I

    .line 464
    const-string/jumbo v3, "setSelectedTuningDevice for USB devices"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 465
    :cond_3
    invoke-virtual {v2}, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->getDeviceType()I

    move-result v5

    sget v6, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->TYPE_WIRED:I

    if-ne v5, v6, :cond_4

    .line 466
    iput v3, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I

    .line 467
    const-string/jumbo v3, "setSelectedTuningDevice for wired devices"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 468
    :cond_4
    invoke-virtual {v2}, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->getDeviceType()I

    move-result v3

    sget v5, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->TYPE_BT:I

    if-ne v3, v5, :cond_6

    .line 469
    move-object v3, v2

    check-cast v3, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;

    .line 470
    .local v3, "device2":Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;
    invoke-virtual {v3}, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->getMajorID()I

    move-result v5

    .line 471
    .local v5, "id1":I
    invoke-virtual {v3}, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->getMinorID()I

    move-result v6

    .line 472
    .local v6, "id2":I
    invoke-virtual {v3}, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->getState()Z

    move-result v7

    .line 473
    .local v7, "state":Z
    if-eqz v7, :cond_5

    .line 474
    const/4 v4, 0x4

    iput v4, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I

    goto :goto_1

    .line 476
    :cond_5
    iput v4, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I

    .line 487
    .end local v3    # "device2":Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;
    .end local v5    # "id1":I
    .end local v6    # "id2":I
    .end local v7    # "state":Z
    :cond_6
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "updateDolbyTunning from onDeviceChanged(), mCurrentPort is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " newPort is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 488
    invoke-virtual {p0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->updateDolbyTunning()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 492
    .end local v0    # "cacheCurrentPort":I
    .end local v2    # "device":Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;
    :goto_2
    goto :goto_3

    .line 490
    :catch_0
    move-exception v0

    .line 491
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onDeviceChanged: Exception "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_3
    iget-boolean v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mIsSupportFWAudioEffectCenter:Z

    if-eqz v0, :cond_9

    .line 495
    iget-object v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/media/audiofx/AudioEffectCenter;->getInstance(Landroid/content/Context;)Landroid/media/audiofx/AudioEffectCenter;

    move-result-object v0

    .line 496
    .local v0, "audioEffectCenter":Landroid/media/audiofx/AudioEffectCenter;
    const-string v1, "dolby"

    invoke-virtual {v0, v1}, Landroid/media/audiofx/AudioEffectCenter;->isEffectActive(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 497
    const-string v1, "misound"

    invoke-virtual {v0, v1}, Landroid/media/audiofx/AudioEffectCenter;->isEffectActive(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    goto :goto_4

    .line 500
    :cond_7
    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/media/audiofx/AudioEffectCenter;->setEffectActive(Ljava/lang/String;Z)V

    goto :goto_5

    .line 498
    :cond_8
    :goto_4
    invoke-direct {p0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->notifyEffectChanged()V

    .line 503
    .end local v0    # "audioEffectCenter":Landroid/media/audiofx/AudioEffectCenter;
    :cond_9
    :goto_5
    return-void
.end method

.method private onHeadsetPlug(Landroid/content/Intent;)V
    .locals 10
    .param p1, "intent"    # Landroid/content/Intent;

    .line 252
    const-string v0, "DolbyEffectController"

    const/4 v1, 0x0

    .line 253
    .local v1, "changed":Z
    :try_start_0
    invoke-static {}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->getService()Landroid/media/IAudioService;

    move-result-object v2

    const/4 v3, 0x3

    invoke-interface {v2, v3}, Landroid/media/IAudioService;->getDeviceMaskForStream(I)I

    move-result v2

    .line 254
    .local v2, "newDevices":I
    const-string/jumbo v3, "state"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 255
    .local v3, "state":I
    const/high16 v5, 0x4000000

    const-string/jumbo v6, "wired headset"

    const-string v7, "USB headset"

    if-nez v3, :cond_1

    .line 256
    :try_start_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "detected device disconnected, new devices: "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    and-int v4, v2, v5

    if-nez v4, :cond_0

    invoke-direct {p0, v7}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->containsDevice(Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_0

    .line 259
    invoke-direct {p0, v7}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->removeDeviceForName(Ljava/lang/String;)Z

    move-result v4

    move v1, v4

    .line 262
    :cond_0
    and-int/lit8 v4, v2, 0xc

    if-nez v4, :cond_4

    invoke-direct {p0, v6}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->containsDevice(Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_4

    .line 263
    invoke-direct {p0, v6}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->removeDeviceForName(Ljava/lang/String;)Z

    move-result v4

    move v1, v4

    goto :goto_0

    .line 265
    :cond_1
    const/4 v8, 0x1

    if-ne v3, v8, :cond_4

    .line 266
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "detected device connected, new devices: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v0, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    and-int/2addr v5, v2

    if-eqz v5, :cond_3

    .line 269
    new-instance v5, Lcom/android/server/audio/dolbyeffect/deviceinfo/UsbDeviceInfo;

    sget v6, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->TYPE_USB:I

    invoke-direct {v5, v6, v4, v4}, Lcom/android/server/audio/dolbyeffect/deviceinfo/UsbDeviceInfo;-><init>(III)V

    move-object v4, v5

    .line 270
    .local v4, "newDevice":Lcom/android/server/audio/dolbyeffect/deviceinfo/UsbDeviceInfo;
    invoke-direct {p0, v7}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->containsDevice(Ljava/lang/String;)I

    move-result v5

    if-gez v5, :cond_2

    .line 271
    sget-object v5, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDevices:Ljava/util/LinkedList;

    invoke-virtual {v5, v4}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 272
    const/4 v1, 0x1

    .line 273
    const-string v5, "onReceive: USB headset connected"

    invoke-static {v0, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    .end local v4    # "newDevice":Lcom/android/server/audio/dolbyeffect/deviceinfo/UsbDeviceInfo;
    :cond_2
    goto :goto_0

    :cond_3
    and-int/lit8 v5, v2, 0xc

    if-eqz v5, :cond_2

    .line 276
    new-instance v5, Lcom/android/server/audio/dolbyeffect/deviceinfo/WiredDeviceInfo;

    sget v7, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->TYPE_WIRED:I

    invoke-direct {v5, v7, v4, v4}, Lcom/android/server/audio/dolbyeffect/deviceinfo/WiredDeviceInfo;-><init>(III)V

    move-object v4, v5

    .line 277
    .local v4, "newDevice":Lcom/android/server/audio/dolbyeffect/deviceinfo/WiredDeviceInfo;
    invoke-direct {p0, v6}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->containsDevice(Ljava/lang/String;)I

    move-result v5

    if-gez v5, :cond_4

    .line 278
    sget-object v5, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDevices:Ljava/util/LinkedList;

    invoke-virtual {v5, v4}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 279
    const/4 v1, 0x1

    .line 280
    const-string v5, "onReceive: wired headset connected"

    invoke-static {v0, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    .end local v4    # "newDevice":Lcom/android/server/audio/dolbyeffect/deviceinfo/WiredDeviceInfo;
    :cond_4
    :goto_0
    if-eqz v1, :cond_5

    .line 285
    sget-object v4, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDevices:Ljava/util/LinkedList;

    invoke-direct {p0, v4}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->onDeviceChanged(Ljava/util/LinkedList;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 289
    .end local v1    # "changed":Z
    .end local v2    # "newDevices":I
    .end local v3    # "state":I
    :cond_5
    goto :goto_1

    .line 287
    :catch_0
    move-exception v1

    .line 288
    .local v1, "e":Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onHeadsetPlug: RemoteException "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_1
    return-void
.end method

.method private onRotationChanged(I)V
    .locals 6
    .param p1, "rotation"    # I

    .line 408
    const-string v0, "DolbyEffectController"

    :try_start_0
    iget-object v1, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentRotation:Ljava/lang/String;

    .line 409
    .local v1, "cachCurrentRotation":Ljava/lang/String;
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 410
    .local v2, "newRotation":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "newRotation is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 412
    const-string v3, "0"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 413
    const-string v3, "90"

    move-object v2, v3

    .line 416
    :cond_0
    iput-object v2, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentRotation:Ljava/lang/String;

    .line 418
    iget-object v3, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mSpatializerEnabledAvaliable:[Z

    const/4 v4, 0x0

    aget-boolean v4, v3, v4

    if-eqz v4, :cond_4

    const/4 v4, 0x1

    aget-boolean v3, v3, v4

    if-nez v3, :cond_1

    goto :goto_0

    .line 423
    :cond_1
    iget v3, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I

    if-eqz v3, :cond_2

    .line 424
    const-string v3, "Current Spatilizer routing device is not speaker, bypass onRotationChanged"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    return-void

    .line 428
    :cond_2
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v4, "newRotation is"

    if-eqz v3, :cond_3

    .line 429
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mCurrentRotation is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", bypass onRotationChanged"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 430
    return-void

    .line 433
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "updateDolbyTunning from onRotationChanged(), mCurrentRotation is "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    invoke-virtual {p0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->updateDolbyTunning()V

    .line 438
    .end local v1    # "cachCurrentRotation":Ljava/lang/String;
    .end local v2    # "newRotation":Ljava/lang/String;
    goto :goto_1

    .line 419
    .restart local v1    # "cachCurrentRotation":Ljava/lang/String;
    .restart local v2    # "newRotation":Ljava/lang/String;
    :cond_4
    :goto_0
    const-string v3, "SpatializerEnabledAvaliable is false, bypass onRotationChanged"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    .line 420
    return-void

    .line 436
    .end local v1    # "cachCurrentRotation":Ljava/lang/String;
    .end local v2    # "newRotation":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 437
    .local v1, "e":Ljava/lang/RuntimeException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onRotationChanged: RuntimeException "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :goto_1
    return-void
.end method

.method private onSpatializerStateChanged(IZ)V
    .locals 6
    .param p1, "type"    # I
    .param p2, "value"    # Z

    .line 293
    iget-object v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mSpatializerEnabledAvaliable:[Z

    const/4 v1, 0x0

    aget-boolean v2, v0, v1

    const/4 v3, 0x1

    aget-boolean v4, v0, v3

    and-int/2addr v2, v4

    .line 294
    .local v2, "currentState":Z
    move v4, v2

    .line 295
    .local v4, "cachecurrentState":Z
    const/4 v5, 0x5

    if-ne p1, v5, :cond_0

    .line 296
    aput-boolean p2, v0, v1

    goto :goto_0

    .line 297
    :cond_0
    const/4 v5, 0x6

    if-ne p1, v5, :cond_1

    .line 298
    aput-boolean p2, v0, v3

    .line 300
    :cond_1
    :goto_0
    aget-boolean v1, v0, v1

    aget-boolean v0, v0, v3

    and-int/2addr v0, v1

    .line 301
    .local v0, "newState":Z
    if-eq v2, v0, :cond_2

    .line 302
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateDolbyTunning() from onSpatializerStateChanged(), currentState is"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " newState is"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "DolbyEffectController"

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    invoke-virtual {p0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->updateDolbyTunning()V

    .line 305
    :cond_2
    return-void
.end method

.method private onVolumeChanged(I)V
    .locals 5
    .param p1, "newVolume"    # I

    .line 386
    const-string v0, "DolbyEffectController"

    :try_start_0
    iget-object v1, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentVolumeLevel:Ljava/lang/String;

    move-object v2, v1

    .line 387
    .local v2, "cachCurrentVolumeLevel":Ljava/lang/String;
    iget v3, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mVolumeThreshold:I

    if-le p1, v3, :cond_0

    .line 388
    const-string v3, "HIGH"

    .local v3, "newVolumeLevel":Ljava/lang/String;
    goto :goto_0

    .line 390
    .end local v3    # "newVolumeLevel":Ljava/lang/String;
    :cond_0
    const-string v3, "LOW"

    .line 392
    .restart local v3    # "newVolumeLevel":Ljava/lang/String;
    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 393
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "newVolumeLevel = "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " mCurrentVolumeLevel = "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentVolumeLevel:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " bypass onVolumeChanged"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    return-void

    .line 396
    :cond_1
    iput-object v3, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentVolumeLevel:Ljava/lang/String;

    .line 398
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "updateDolbyTunning from onVolumeChanged(), mCurrentVolumeLevel is "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " newVolumeLevel is "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    invoke-virtual {p0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->updateDolbyTunning()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 403
    .end local v2    # "cachCurrentVolumeLevel":Ljava/lang/String;
    .end local v3    # "newVolumeLevel":Ljava/lang/String;
    goto :goto_1

    .line 401
    :catch_0
    move-exception v1

    .line 402
    .local v1, "e":Ljava/lang/RuntimeException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onVolumeChanged: RuntimeException "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :goto_1
    return-void
.end method

.method private removeDeviceForName(Ljava/lang/String;)Z
    .locals 5
    .param p1, "deviceName"    # Ljava/lang/String;

    .line 547
    const/4 v0, 0x0

    .line 548
    .local v0, "product":Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;
    sget-object v1, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDevices:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_1

    .line 549
    sget-object v3, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDevices:Ljava/util/LinkedList;

    invoke-virtual {v3, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;

    .line 550
    invoke-virtual {v0}, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->getDevice()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 551
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "remove device name: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "DolbyEffectController"

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    sget-object v3, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDevices:Ljava/util/LinkedList;

    invoke-virtual {v3, v1}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    .line 553
    return v2

    .line 548
    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 556
    .end local v1    # "i":I
    :cond_1
    const/4 v1, 0x0

    return v1
.end method

.method private removeDeviceForType(I)Z
    .locals 5
    .param p1, "type"    # I

    .line 522
    const/4 v0, 0x0

    .line 523
    .local v0, "product":Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;
    const/4 v1, 0x0

    .line 524
    .local v1, "removed":Z
    sget-object v2, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDevices:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_1

    .line 525
    sget-object v3, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDevices:Ljava/util/LinkedList;

    invoke-virtual {v3, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;

    .line 526
    invoke-virtual {v0}, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->getDeviceType()I

    move-result v3

    if-ne v3, p1, :cond_0

    .line 527
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "remove device type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "DolbyEffectController"

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 528
    sget-object v3, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDevices:Ljava/util/LinkedList;

    invoke-virtual {v3, v2}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    .line 529
    const/4 v1, 0x1

    .line 524
    :cond_0
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 532
    .end local v2    # "i":I
    :cond_1
    return v1
.end method


# virtual methods
.method public acquireDolbyDeviceIdForAdspSpatializer(I)Ljava/lang/String;
    .locals 1
    .param p1, "port"    # I

    .line 650
    const-string v0, "default_internal_speaker"

    .line 651
    .local v0, "device_id":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 659
    :pswitch_1
    const-string v0, "headphone_spatializer"

    .line 660
    goto :goto_0

    .line 662
    :pswitch_2
    const-string v0, "bluetooth_spatializer"

    goto :goto_0

    .line 656
    :pswitch_3
    const-string v0, "headphone_spatializer"

    .line 657
    goto :goto_0

    .line 653
    :pswitch_4
    const-string/jumbo v0, "speaker_spatializer_90"

    .line 654
    nop

    .line 665
    :goto_0
    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public acquireDolbyDeviceIdForNonSpatializer(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "Port"    # I
    .param p2, "VolumeLevel"    # Ljava/lang/String;
    .param p3, "Rotation"    # Ljava/lang/String;

    .line 560
    const-string v0, "default_internal_speaker"

    .line 561
    .local v0, "device_id":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_3

    .line 579
    :pswitch_1
    const-string v0, "default_usb"

    .line 580
    goto :goto_3

    .line 582
    :pswitch_2
    const-string v0, "default_bluetooth"

    goto :goto_3

    .line 576
    :pswitch_3
    const-string v0, "default_headphone"

    .line 577
    goto :goto_3

    .line 563
    :pswitch_4
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string v1, "disabled"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_1

    :sswitch_1
    const-string v1, "HIGH"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_1

    :sswitch_2
    const-string v1, "LOW"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    goto :goto_1

    :goto_0
    const/4 v1, -0x1

    :goto_1
    packed-switch v1, :pswitch_data_1

    goto :goto_2

    .line 571
    :pswitch_5
    const-string/jumbo v0, "speaker_volume_low"

    goto :goto_2

    .line 568
    :pswitch_6
    const-string/jumbo v0, "speaker_volume_high"

    .line 569
    goto :goto_2

    .line 565
    :pswitch_7
    const-string v0, "default_internal_speaker"

    .line 566
    nop

    .line 574
    :goto_2
    nop

    .line 585
    :goto_3
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x12734 -> :sswitch_2
        0x21d5a2 -> :sswitch_1
        0x10263a7c -> :sswitch_0
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method public acquireDolbyDeviceIdForSpatializer(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "Port"    # I
    .param p2, "VolumeLevel"    # Ljava/lang/String;
    .param p3, "Rotation"    # Ljava/lang/String;

    .line 589
    const-string v0, "default_internal_speaker"

    .line 590
    .local v0, "device_id":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_b

    .line 640
    :pswitch_1
    const-string v0, "headphone_spatializer"

    .line 641
    goto/16 :goto_b

    .line 643
    :pswitch_2
    const-string v0, "bluetooth_spatializer"

    goto/16 :goto_b

    .line 636
    :pswitch_3
    const-string v0, "headphone_spatializer"

    .line 637
    goto/16 :goto_b

    .line 592
    :pswitch_4
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const-string v5, "disabled"

    const/4 v6, -0x1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    invoke-virtual {p2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    goto :goto_1

    :sswitch_1
    const-string v1, "HIGH"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v3

    goto :goto_1

    :sswitch_2
    const-string v1, "LOW"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v4

    goto :goto_1

    :goto_0
    move v1, v6

    :goto_1
    const-string v7, "270"

    const-string v8, "90"

    packed-switch v1, :pswitch_data_1

    goto/16 :goto_a

    .line 620
    :pswitch_5
    invoke-virtual {p3}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    :cond_1
    goto :goto_2

    :sswitch_3
    invoke-virtual {p3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_3

    :sswitch_4
    invoke-virtual {p3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v2, v4

    goto :goto_3

    :sswitch_5
    invoke-virtual {p3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v2, v3

    goto :goto_3

    :goto_2
    move v2, v6

    :goto_3
    packed-switch v2, :pswitch_data_2

    goto/16 :goto_a

    .line 628
    :pswitch_6
    const-string/jumbo v0, "speaker_spatilizer_270_volume_low"

    goto/16 :goto_a

    .line 625
    :pswitch_7
    const-string/jumbo v0, "speaker_spatilizer_90_volume_low"

    .line 626
    goto/16 :goto_a

    .line 622
    :pswitch_8
    const-string/jumbo v0, "speaker_spatilizer_90_volume_low"

    .line 623
    goto/16 :goto_a

    .line 607
    :pswitch_9
    invoke-virtual {p3}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_2

    :cond_2
    goto :goto_4

    :sswitch_6
    invoke-virtual {p3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_5

    :sswitch_7
    invoke-virtual {p3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    move v2, v4

    goto :goto_5

    :sswitch_8
    invoke-virtual {p3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    move v2, v3

    goto :goto_5

    :goto_4
    move v2, v6

    :goto_5
    packed-switch v2, :pswitch_data_3

    goto :goto_6

    .line 615
    :pswitch_a
    const-string/jumbo v0, "speaker_spatilizer_270_volume_high"

    goto :goto_6

    .line 612
    :pswitch_b
    const-string/jumbo v0, "speaker_spatilizer_90_volume_high"

    .line 613
    goto :goto_6

    .line 609
    :pswitch_c
    const-string/jumbo v0, "speaker_spatilizer_90_volume_high"

    .line 610
    nop

    .line 618
    :goto_6
    goto :goto_a

    .line 594
    :pswitch_d
    invoke-virtual {p3}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_3

    :cond_3
    goto :goto_7

    :sswitch_9
    invoke-virtual {p3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_8

    :sswitch_a
    invoke-virtual {p3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v2, v4

    goto :goto_8

    :sswitch_b
    invoke-virtual {p3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v2, v3

    goto :goto_8

    :goto_7
    move v2, v6

    :goto_8
    packed-switch v2, :pswitch_data_4

    goto :goto_9

    .line 602
    :pswitch_e
    const-string/jumbo v0, "speaker_spatilizer_270"

    goto :goto_9

    .line 599
    :pswitch_f
    const-string/jumbo v0, "speaker_spatilizer_90"

    .line 600
    goto :goto_9

    .line 596
    :pswitch_10
    const-string/jumbo v0, "speaker_spatilizer_90"

    .line 597
    nop

    .line 605
    :goto_9
    nop

    .line 633
    :goto_a
    nop

    .line 646
    :goto_b
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x12734 -> :sswitch_2
        0x21d5a2 -> :sswitch_1
        0x10263a7c -> :sswitch_0
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_d
        :pswitch_9
        :pswitch_5
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        0x717 -> :sswitch_5
        0xc28b -> :sswitch_4
        0x10263a7c -> :sswitch_3
    .end sparse-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
    .end packed-switch

    :sswitch_data_2
    .sparse-switch
        0x717 -> :sswitch_8
        0xc28b -> :sswitch_7
        0x10263a7c -> :sswitch_6
    .end sparse-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_c
        :pswitch_b
        :pswitch_a
    .end packed-switch

    :sswitch_data_3
    .sparse-switch
        0x717 -> :sswitch_b
        0xc28b -> :sswitch_a
        0x10263a7c -> :sswitch_9
    .end sparse-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_10
        :pswitch_f
        :pswitch_e
    .end packed-switch
.end method

.method public btStateChangedFromDeviceBroker(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "btinfo"    # Landroid/os/Bundle;

    .line 240
    iget-object v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mWorkHandler:Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;

    if-eqz v0, :cond_0

    .line 241
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 242
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x3

    iput v1, v0, Landroid/os/Message;->what:I

    .line 243
    invoke-virtual {v0, p1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 244
    iget-object v1, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mWorkHandler:Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;

    invoke-virtual {v1, v0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;->sendMessage(Landroid/os/Message;)Z

    .line 245
    .end local v0    # "msg":Landroid/os/Message;
    goto :goto_0

    .line 246
    :cond_0
    const-string v0, "DolbyEffectController"

    const-string v1, "mWorkHandler doesn\'t init"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    :goto_0
    return-void
.end method

.method public init()V
    .locals 5

    .line 100
    const-string/jumbo v0, "true"

    const-string v1, "DolbyEffectController"

    :try_start_0
    const-string v2, "DolbyEffectController init..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    const-string v2, "ro.audio.spatializer_enabled"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 103
    iget-object v2, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mSpatializerEnabledAvaliable:[Z

    invoke-static {}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->getService()Landroid/media/IAudioService;

    move-result-object v3

    invoke-interface {v3}, Landroid/media/IAudioService;->isSpatializerEnabled()Z

    move-result v3

    const/4 v4, 0x0

    aput-boolean v3, v2, v4

    .line 104
    iget-object v2, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mSpatializerEnabledAvaliable:[Z

    invoke-static {}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->getService()Landroid/media/IAudioService;

    move-result-object v3

    invoke-interface {v3}, Landroid/media/IAudioService;->isSpatializerAvailable()Z

    move-result v3

    const/4 v4, 0x1

    aput-boolean v3, v2, v4

    .line 107
    :cond_0
    const-string/jumbo v2, "vendor.audio.dolby.control.tunning.by.volume.support"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 108
    invoke-static {}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->getService()Landroid/media/IAudioService;

    move-result-object v0

    const/4 v2, 0x3

    invoke-interface {v0, v2}, Landroid/media/IAudioService;->getStreamMaxVolume(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0xa

    div-int/lit8 v0, v0, 0xf

    iput v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mVolumeThreshold:I

    .line 109
    invoke-static {}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->getService()Landroid/media/IAudioService;

    move-result-object v0

    const/4 v3, 0x2

    invoke-interface {v0, v2, v3}, Landroid/media/IAudioService;->getDeviceStreamVolume(II)I

    move-result v0

    .line 110
    .local v0, "index":I
    iget v2, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mVolumeThreshold:I

    if-le v0, v2, :cond_1

    .line 111
    const-string v2, "HIGH"

    iput-object v2, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentVolumeLevel:Ljava/lang/String;

    goto :goto_0

    .line 113
    :cond_1
    const-string v2, "LOW"

    iput-object v2, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentVolumeLevel:Ljava/lang/String;

    .line 117
    .end local v0    # "index":I
    :cond_2
    :goto_0
    new-instance v0, Landroid/os/HandlerThread;

    const-string v2, "DolbyEffect"

    invoke-direct {v0, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mHandlerThread:Landroid/os/HandlerThread;

    .line 118
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 119
    new-instance v0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;

    iget-object v2, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;-><init>(Lcom/android/server/audio/dolbyeffect/DolbyEffectController;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mWorkHandler:Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;

    .line 121
    const-string/jumbo v0, "updateDolbyTunning from init()"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    invoke-virtual {p0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->updateDolbyTunning()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 125
    goto :goto_1

    .line 123
    :catch_0
    move-exception v0

    .line 124
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "init: Exception "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method

.method public receiveDeviceConnectStateChanged(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 172
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 173
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 174
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onReceive: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "DolbyEffectController"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x2

    sparse-switch v1, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string v1, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v3

    goto :goto_1

    :sswitch_1
    const-string v1, "android.bluetooth.headset.action.VENDOR_SPECIFIC_HEADSET_EVENT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    goto :goto_1

    :sswitch_2
    const-string v1, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_1

    :goto_0
    const/4 v1, -0x1

    :goto_1
    packed-switch v1, :pswitch_data_0

    goto :goto_2

    .line 185
    :pswitch_0
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 186
    .local v1, "msg":Landroid/os/Message;
    iput v2, v1, Landroid/os/Message;->what:I

    .line 187
    iput-object p2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 188
    iget-object v2, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mWorkHandler:Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;

    invoke-virtual {v2, v1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;->sendMessage(Landroid/os/Message;)Z

    .line 189
    goto :goto_2

    .line 178
    .end local v1    # "msg":Landroid/os/Message;
    :pswitch_1
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 179
    .restart local v1    # "msg":Landroid/os/Message;
    iput v3, v1, Landroid/os/Message;->what:I

    .line 180
    iput-object p2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 181
    iget-object v2, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mWorkHandler:Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;

    invoke-virtual {v2, v1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;->sendMessage(Landroid/os/Message;)Z

    .line 182
    nop

    .line 194
    .end local v1    # "msg":Landroid/os/Message;
    :cond_1
    :goto_2
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x63ecb970 -> :sswitch_2
        0x69ab72ba -> :sswitch_1
        0x7e2cc189 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public receiveRotationChanged(I)V
    .locals 2
    .param p1, "rotationChanged"    # I

    .line 139
    iget-object v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mWorkHandler:Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;

    if-eqz v0, :cond_0

    .line 140
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 141
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x4

    iput v1, v0, Landroid/os/Message;->what:I

    .line 142
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 143
    iget-object v1, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mWorkHandler:Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;

    invoke-virtual {v1, v0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;->sendMessage(Landroid/os/Message;)Z

    .line 144
    .end local v0    # "msg":Landroid/os/Message;
    goto :goto_0

    .line 145
    :cond_0
    const-string v0, "DolbyEffectController"

    const-string v1, "mWorkHandler doesn\'t init"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    :goto_0
    return-void
.end method

.method public receiveSpatializerAvailableChanged(Z)V
    .locals 2
    .param p1, "available"    # Z

    .line 161
    iget-object v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mWorkHandler:Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;

    if-eqz v0, :cond_0

    .line 162
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 163
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x6

    iput v1, v0, Landroid/os/Message;->what:I

    .line 164
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 165
    iget-object v1, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mWorkHandler:Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;

    invoke-virtual {v1, v0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;->sendMessage(Landroid/os/Message;)Z

    .line 166
    .end local v0    # "msg":Landroid/os/Message;
    goto :goto_0

    .line 167
    :cond_0
    const-string v0, "DolbyEffectController"

    const-string v1, "mWorkHandler doesn\'t init"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    :goto_0
    return-void
.end method

.method public receiveSpatializerEnabledChanged(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .line 150
    iget-object v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mWorkHandler:Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;

    if-eqz v0, :cond_0

    .line 151
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 152
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x5

    iput v1, v0, Landroid/os/Message;->what:I

    .line 153
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 154
    iget-object v1, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mWorkHandler:Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;

    invoke-virtual {v1, v0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;->sendMessage(Landroid/os/Message;)Z

    .line 155
    .end local v0    # "msg":Landroid/os/Message;
    goto :goto_0

    .line 156
    :cond_0
    const-string v0, "DolbyEffectController"

    const-string v1, "mWorkHandler doesn\'t init"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    :goto_0
    return-void
.end method

.method public receiveVolumeChanged(I)V
    .locals 2
    .param p1, "newVolume"    # I

    .line 129
    iget-object v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mWorkHandler:Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;

    if-eqz v0, :cond_0

    .line 130
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 131
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->what:I

    .line 132
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 133
    iget-object v1, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mWorkHandler:Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;

    invoke-virtual {v1, v0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;->sendMessage(Landroid/os/Message;)Z

    .line 134
    .end local v0    # "msg":Landroid/os/Message;
    goto :goto_0

    .line 135
    :cond_0
    const-string v0, "DolbyEffectController"

    const-string v1, "mWorkHandler doesn\'t init"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    :goto_0
    return-void
.end method

.method public updateDolbyTunning()V
    .locals 5

    .line 670
    const-string v0, "DolbyEffectController"

    :try_start_0
    new-instance v1, Lcom/dolby/dax/DolbyAudioEffect;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v2}, Lcom/dolby/dax/DolbyAudioEffect;-><init>(II)V

    iput-object v1, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mDolbyAudioEffect:Lcom/dolby/dax/DolbyAudioEffect;

    .line 671
    const-string v1, "persist.vendor.audio.dolbysurround.enable"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mAdspSpatializerEnable:Z

    .line 673
    iget-object v1, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mDolbyAudioEffect:Lcom/dolby/dax/DolbyAudioEffect;

    invoke-virtual {v1}, Lcom/dolby/dax/DolbyAudioEffect;->hasControl()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 675
    iget-object v1, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mSpatializerEnabledAvaliable:[Z

    aget-boolean v2, v1, v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    aget-boolean v1, v1, v2

    if-eqz v1, :cond_0

    .line 676
    const-string v1, "The dolby tuning selected for Spatializer, begin..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 677
    iget v1, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I

    iget-object v2, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentVolumeLevel:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentRotation:Ljava/lang/String;

    invoke-virtual {p0, v1, v2, v3}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->acquireDolbyDeviceIdForSpatializer(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .local v1, "newDeviceId":Ljava/lang/String;
    goto :goto_0

    .line 678
    .end local v1    # "newDeviceId":Ljava/lang/String;
    :cond_0
    iget-boolean v1, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mAdspSpatializerAvailable:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mAdspSpatializerEnable:Z

    if-eqz v1, :cond_1

    .line 679
    const-string v1, "[adsp spatial] The dolby tuning selected for adsp Spatializer, begin..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 681
    iget v1, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I

    invoke-virtual {p0, v1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->acquireDolbyDeviceIdForAdspSpatializer(I)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "newDeviceId":Ljava/lang/String;
    goto :goto_0

    .line 683
    .end local v1    # "newDeviceId":Ljava/lang/String;
    :cond_1
    const-string v1, "The dolby tuning selected for nonSpatializer, begin..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 684
    iget v1, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I

    iget-object v2, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentVolumeLevel:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentRotation:Ljava/lang/String;

    invoke-virtual {p0, v1, v2, v3}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->acquireDolbyDeviceIdForNonSpatializer(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 687
    .restart local v1    # "newDeviceId":Ljava/lang/String;
    :goto_0
    iget v2, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I

    iget-object v3, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDolbyTunning:Lcom/android/server/audio/dolbyeffect/DolbyEffectController$DolbyTunning;

    iget v3, v3, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$DolbyTunning;->mPort:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDolbyTunning:Lcom/android/server/audio/dolbyeffect/DolbyEffectController$DolbyTunning;

    iget-object v2, v2, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$DolbyTunning;->mDeviceId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 688
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "The selected dolby tuning is same as current [mCurrentPort ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "newDeviceId = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "],bypass updateDolbyTunning"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 707
    iget-object v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mDolbyAudioEffect:Lcom/dolby/dax/DolbyAudioEffect;

    if-eqz v0, :cond_2

    .line 708
    invoke-virtual {v0}, Lcom/dolby/dax/DolbyAudioEffect;->release()V

    .line 689
    :cond_2
    return-void

    .line 692
    :cond_3
    :try_start_1
    iget-object v2, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDolbyTunning:Lcom/android/server/audio/dolbyeffect/DolbyEffectController$DolbyTunning;

    iget v3, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I

    iput v3, v2, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$DolbyTunning;->mPort:I

    .line 693
    iget-object v2, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDolbyTunning:Lcom/android/server/audio/dolbyeffect/DolbyEffectController$DolbyTunning;

    iput-object v1, v2, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$DolbyTunning;->mDeviceId:Ljava/lang/String;

    .line 695
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateDolbyTunning, Port = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDolbyTunning:Lcom/android/server/audio/dolbyeffect/DolbyEffectController$DolbyTunning;

    iget v3, v3, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$DolbyTunning;->mPort:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", DeviceId = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDolbyTunning:Lcom/android/server/audio/dolbyeffect/DolbyEffectController$DolbyTunning;

    iget-object v3, v3, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$DolbyTunning;->mDeviceId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 696
    iget-object v2, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mDolbyAudioEffect:Lcom/dolby/dax/DolbyAudioEffect;

    iget-object v3, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDolbyTunning:Lcom/android/server/audio/dolbyeffect/DolbyEffectController$DolbyTunning;

    iget v3, v3, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$DolbyTunning;->mPort:I

    iget-object v4, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentDolbyTunning:Lcom/android/server/audio/dolbyeffect/DolbyEffectController$DolbyTunning;

    iget-object v4, v4, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$DolbyTunning;->mDeviceId:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/dolby/dax/DolbyAudioEffect;->setSelectedTuningDevice(ILjava/lang/String;)V

    .line 697
    .end local v1    # "newDeviceId":Ljava/lang/String;
    goto :goto_1

    .line 698
    :cond_4
    const-string/jumbo v1, "setSelectedTuningDevice do not hasControl"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 707
    :goto_1
    iget-object v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mDolbyAudioEffect:Lcom/dolby/dax/DolbyAudioEffect;

    if-eqz v0, :cond_5

    .line 708
    goto :goto_2

    .line 707
    :catchall_0
    move-exception v0

    goto :goto_4

    .line 704
    :catch_0
    move-exception v1

    .line 705
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateDolbyTunning: RuntimeException"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 707
    nop

    .end local v1    # "e":Ljava/lang/RuntimeException;
    iget-object v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mDolbyAudioEffect:Lcom/dolby/dax/DolbyAudioEffect;

    if-eqz v0, :cond_5

    .line 708
    :goto_2
    goto :goto_3

    .line 702
    :catch_1
    move-exception v1

    .line 703
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    :try_start_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateDolbyTunning: IllegalArgumentException"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 707
    nop

    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    iget-object v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mDolbyAudioEffect:Lcom/dolby/dax/DolbyAudioEffect;

    if-eqz v0, :cond_5

    .line 708
    goto :goto_3

    .line 700
    :catch_2
    move-exception v1

    .line 701
    .local v1, "e":Ljava/lang/UnsupportedOperationException;
    :try_start_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateDolbyTunning: UnsupportedOperationException"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 707
    nop

    .end local v1    # "e":Ljava/lang/UnsupportedOperationException;
    iget-object v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mDolbyAudioEffect:Lcom/dolby/dax/DolbyAudioEffect;

    if-eqz v0, :cond_5

    .line 708
    :goto_3
    invoke-virtual {v0}, Lcom/dolby/dax/DolbyAudioEffect;->release()V

    .line 712
    :cond_5
    return-void

    .line 707
    :goto_4
    iget-object v1, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mDolbyAudioEffect:Lcom/dolby/dax/DolbyAudioEffect;

    if-eqz v1, :cond_6

    .line 708
    invoke-virtual {v1}, Lcom/dolby/dax/DolbyAudioEffect;->release()V

    .line 710
    :cond_6
    throw v0
.end method
