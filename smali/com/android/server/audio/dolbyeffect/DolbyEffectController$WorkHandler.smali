.class Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;
.super Landroid/os/Handler;
.source "DolbyEffectController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/dolbyeffect/DolbyEffectController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WorkHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;


# direct methods
.method public constructor <init>(Lcom/android/server/audio/dolbyeffect/DolbyEffectController;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 198
    iput-object p1, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;->this$0:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    .line 199
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 200
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .line 204
    iget v0, p1, Landroid/os/Message;->what:I

    const-string v1, "DolbyEffectController"

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 226
    :pswitch_0
    const-string v0, "receive MSG_SPATIALIZER_AVALIABLE_STATE"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    iget-object v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;->this$0:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    const/4 v2, 0x6

    invoke-static {v0, v2, v1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->-$$Nest$monSpatializerStateChanged(Lcom/android/server/audio/dolbyeffect/DolbyEffectController;IZ)V

    .line 228
    goto/16 :goto_0

    .line 230
    :pswitch_1
    const-string v0, "receive MSG_SPATIALIZER_ENABLED_STATE"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    iget-object v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;->this$0:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    const/4 v2, 0x5

    invoke-static {v0, v2, v1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->-$$Nest$monSpatializerStateChanged(Lcom/android/server/audio/dolbyeffect/DolbyEffectController;IZ)V

    .line 232
    goto :goto_0

    .line 222
    :pswitch_2
    const-string v0, "receive MSG_ROTATION_CHANGED"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    iget-object v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;->this$0:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->-$$Nest$monRotationChanged(Lcom/android/server/audio/dolbyeffect/DolbyEffectController;I)V

    .line 224
    goto :goto_0

    .line 214
    :pswitch_3
    const-string v0, "receive MSG_BT_STATE_CHANGED_DEVICEBROKER"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    iget-object v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;->this$0:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "device"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "profile"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const-string/jumbo v4, "state"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->-$$Nest$monBTStateChangedFromDeviceBroker(Lcom/android/server/audio/dolbyeffect/DolbyEffectController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    goto :goto_0

    .line 218
    :pswitch_4
    const-string v0, "receive MSG_HEADSET_PLUG"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    iget-object v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;->this$0:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/content/Intent;

    invoke-static {v0, v1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->-$$Nest$monHeadsetPlug(Lcom/android/server/audio/dolbyeffect/DolbyEffectController;Landroid/content/Intent;)V

    .line 220
    goto :goto_0

    .line 210
    :pswitch_5
    const-string v0, "receive MSG_BT_STATE_CHANGED"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    iget-object v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;->this$0:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/content/Intent;

    invoke-static {v0, v1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->-$$Nest$monBTStateChanged(Lcom/android/server/audio/dolbyeffect/DolbyEffectController;Landroid/content/Intent;)V

    .line 212
    goto :goto_0

    .line 206
    :pswitch_6
    const-string v0, "receive MSG_VOLUME_CHANGED"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    iget-object v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;->this$0:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->-$$Nest$monVolumeChanged(Lcom/android/server/audio/dolbyeffect/DolbyEffectController;I)V

    .line 208
    nop

    .line 236
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
