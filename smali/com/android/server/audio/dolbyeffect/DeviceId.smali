.class public Lcom/android/server/audio/dolbyeffect/DeviceId;
.super Ljava/lang/Object;
.source "DeviceId.java"


# static fields
.field public static final BLUETOOTH_AIRDOTS_3_PRO:Ljava/lang/String; = "bluetooth_AirDots_3_Pro"

.field public static final BLUETOOTH_AIR_2_SE:Ljava/lang/String; = "bluetooth_Air_2_SE"

.field public static final BLUETOOTH_DEFAULT:Ljava/lang/String; = "default_bluetooth"

.field public static final BLUETOOTH_REDMI_BUDS_4_PRO:Ljava/lang/String; = "bluetooth_Redmi_Buds_4_Pro"

.field public static final BLUETOOTH_SPATIALIZER:Ljava/lang/String; = "bluetooth_spatializer"

.field public static final SPK_DEFAULT:Ljava/lang/String; = "default_internal_speaker"

.field public static final SPK_SPATIALIZER:Ljava/lang/String; = "speaker_spatializer_90"

.field public static final SPK_SPATIALIZER_270:Ljava/lang/String; = "speaker_spatilizer_270"

.field public static final SPK_SPATIALIZER_270_VOLUME_HIGH:Ljava/lang/String; = "speaker_spatilizer_270_volume_high"

.field public static final SPK_SPATIALIZER_270_VOLUME_LOW:Ljava/lang/String; = "speaker_spatilizer_270_volume_low"

.field public static final SPK_SPATIALIZER_90:Ljava/lang/String; = "speaker_spatilizer_90"

.field public static final SPK_SPATIALIZER_90_VOLUME_HIGH:Ljava/lang/String; = "speaker_spatilizer_90_volume_high"

.field public static final SPK_SPATIALIZER_90_VOLUME_LOW:Ljava/lang/String; = "speaker_spatilizer_90_volume_low"

.field public static final SPK_VOLUME_HIGH:Ljava/lang/String; = "speaker_volume_high"

.field public static final SPK_VOLUME_LOW:Ljava/lang/String; = "speaker_volume_low"

.field public static final USB_DEFAULT:Ljava/lang/String; = "default_usb"

.field public static final USB_SPATIALIZER:Ljava/lang/String; = "headphone_spatializer"

.field public static final WIRED_DEFAULT:Ljava/lang/String; = "default_headphone"

.field public static final WIRED_SPATIALIZER:Ljava/lang/String; = "headphone_spatializer"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getBtDeviceId(II)Ljava/lang/String;
    .locals 1
    .param p0, "majorId"    # I
    .param p1, "minorId"    # I

    .line 32
    const v0, 0x10110

    if-ne p0, v0, :cond_1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const/4 v0, 0x7

    if-ne p1, v0, :cond_1

    .line 33
    :cond_0
    const-string v0, "bluetooth_Redmi_Buds_4_Pro"

    return-object v0

    .line 34
    :cond_1
    const v0, 0x10109

    if-ne p0, v0, :cond_2

    const/4 v0, 0x6

    if-ne p1, v0, :cond_2

    .line 35
    const-string v0, "bluetooth_AirDots_3_Pro"

    return-object v0

    .line 36
    :cond_2
    const v0, 0x10101

    if-ne p0, v0, :cond_3

    if-nez p1, :cond_3

    .line 37
    const-string v0, "bluetooth_Air_2_SE"

    return-object v0

    .line 39
    :cond_3
    const-string v0, "default_bluetooth"

    return-object v0
.end method

.method public static getUsbDeviceId(II)Ljava/lang/String;
    .locals 1
    .param p0, "vendorId"    # I
    .param p1, "productId"    # I

    .line 44
    const-string v0, "default_usb"

    return-object v0
.end method

.method public static getWiredDeviceId(II)Ljava/lang/String;
    .locals 1
    .param p0, "vendorId"    # I
    .param p1, "productId"    # I

    .line 48
    const-string v0, "default_headphone"

    return-object v0
.end method
