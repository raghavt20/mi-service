public class com.android.server.audio.dolbyeffect.DolbyEffectController {
	 /* .source "DolbyEffectController.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/audio/dolbyeffect/DolbyEffectController$DolbyTunning;, */
	 /* Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String EXTRA_VOLUME_STREAM_TYPE;
private static final java.lang.String HEADSET_STATE;
static final Integer MANU_ID_XIAO_MI;
private static final Integer MSG_BT_STATE_CHANGED;
private static final Integer MSG_BT_STATE_CHANGED_DEVICEBROKER;
private static final Integer MSG_HEADSET_PLUG;
private static final Integer MSG_ROTATION_CHANGED;
private static final Integer MSG_SPATIALIZER_AVALIABLE_STATE;
private static final Integer MSG_SPATIALIZER_ENABLED_STATE;
private static final Integer MSG_VOLUME_CHANGED;
private static final Integer PORT_BLUETOOTH;
private static final Integer PORT_HDMI;
private static final Integer PORT_HEADPHONE;
private static final Integer PORT_INTERNAL_SPEAKER;
private static final Integer PORT_MIRACAST;
private static final Integer PORT_USB;
private static final java.lang.String TAG;
private static java.util.LinkedList mCurrentDevices;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/LinkedList<", */
/* "Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static volatile com.android.server.audio.dolbyeffect.DolbyEffectController sInstance;
private static android.media.IAudioService sService;
/* # instance fields */
private final Boolean mAdspSpatializerAvailable;
private Boolean mAdspSpatializerEnable;
private android.content.Context mContext;
private com.android.server.audio.dolbyeffect.DolbyEffectController$DolbyTunning mCurrentDolbyTunning;
private Integer mCurrentPort;
private java.lang.String mCurrentRotation;
private java.lang.String mCurrentVolumeLevel;
private com.dolby.dax.DolbyAudioEffect mDolbyAudioEffect;
private android.os.HandlerThread mHandlerThread;
private final Boolean mIsSupportFWAudioEffectCenter;
private mSpatializerEnabledAvaliable;
private Integer mVolumeThreshold;
private com.android.server.audio.dolbyeffect.DolbyEffectController$WorkHandler mWorkHandler;
/* # direct methods */
static void -$$Nest$monBTStateChanged ( com.android.server.audio.dolbyeffect.DolbyEffectController p0, android.content.Intent p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->onBTStateChanged(Landroid/content/Intent;)V */
return;
} // .end method
static void -$$Nest$monBTStateChangedFromDeviceBroker ( com.android.server.audio.dolbyeffect.DolbyEffectController p0, java.lang.String p1, java.lang.String p2, java.lang.String p3 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->onBTStateChangedFromDeviceBroker(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$monHeadsetPlug ( com.android.server.audio.dolbyeffect.DolbyEffectController p0, android.content.Intent p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->onHeadsetPlug(Landroid/content/Intent;)V */
return;
} // .end method
static void -$$Nest$monRotationChanged ( com.android.server.audio.dolbyeffect.DolbyEffectController p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->onRotationChanged(I)V */
return;
} // .end method
static void -$$Nest$monSpatializerStateChanged ( com.android.server.audio.dolbyeffect.DolbyEffectController p0, Integer p1, Boolean p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->onSpatializerStateChanged(IZ)V */
return;
} // .end method
static void -$$Nest$monVolumeChanged ( com.android.server.audio.dolbyeffect.DolbyEffectController p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->onVolumeChanged(I)V */
return;
} // .end method
static com.android.server.audio.dolbyeffect.DolbyEffectController ( ) {
/* .locals 1 */
/* .line 66 */
/* new-instance v0, Ljava/util/LinkedList; */
/* invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V */
return;
} // .end method
private com.android.server.audio.dolbyeffect.DolbyEffectController ( ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 84 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 61 */
/* nop */
/* .line 62 */
final String v0 = "ro.vendor.audio.fweffect"; // const-string v0, "ro.vendor.audio.fweffect"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mIsSupportFWAudioEffectCenter:Z */
/* .line 68 */
/* const/16 v0, 0x64 */
/* iput v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mVolumeThreshold:I */
/* .line 69 */
final String v0 = "disabled"; // const-string v0, "disabled"
this.mCurrentVolumeLevel = v0;
/* .line 70 */
this.mCurrentRotation = v0;
/* .line 71 */
/* iput v1, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I */
/* .line 72 */
int v0 = 2; // const/4 v0, 0x2
/* new-array v0, v0, [Z */
/* fill-array-data v0, :array_0 */
this.mSpatializerEnabledAvaliable = v0;
/* .line 73 */
/* nop */
/* .line 74 */
final String v0 = "ro.vendor.audio.adsp.spatial"; // const-string v0, "ro.vendor.audio.adsp.spatial"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mAdspSpatializerAvailable:Z */
/* .line 75 */
/* iput-boolean v1, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mAdspSpatializerEnable:Z */
/* .line 81 */
/* new-instance v0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$DolbyTunning; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$DolbyTunning;-><init>(Lcom/android/server/audio/dolbyeffect/DolbyEffectController;Lcom/android/server/audio/dolbyeffect/DolbyEffectController$DolbyTunning-IA;)V */
this.mCurrentDolbyTunning = v0;
/* .line 85 */
this.mContext = p1;
/* .line 86 */
return;
/* nop */
/* :array_0 */
/* .array-data 1 */
/* 0x0t */
/* 0x0t */
} // .end array-data
} // .end method
private Integer containsDevice ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "deviceName" # Ljava/lang/String; */
/* .line 536 */
int v0 = 0; // const/4 v0, 0x0
/* .line 537 */
/* .local v0, "product":Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = com.android.server.audio.dolbyeffect.DolbyEffectController.mCurrentDevices;
v2 = (( java.util.LinkedList ) v2 ).size ( ); // invoke-virtual {v2}, Ljava/util/LinkedList;->size()I
/* if-ge v1, v2, :cond_1 */
/* .line 538 */
v2 = com.android.server.audio.dolbyeffect.DolbyEffectController.mCurrentDevices;
(( java.util.LinkedList ) v2 ).get ( v1 ); // invoke-virtual {v2, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;
/* move-object v0, v2 */
/* check-cast v0, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase; */
/* .line 539 */
(( com.android.server.audio.dolbyeffect.deviceinfo.DeviceInfoBase ) v0 ).getDevice ( ); // invoke-virtual {v0}, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->getDevice()Ljava/lang/String;
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 540 */
/* .line 537 */
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 543 */
} // .end local v1 # "i":I
} // :cond_1
int v1 = -1; // const/4 v1, -0x1
} // .end method
private java.lang.String getDeviceidForDevice ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "type" # I */
/* .param p2, "id1" # I */
/* .param p3, "id2" # I */
/* .line 715 */
final String v0 = ""; // const-string v0, ""
/* .line 716 */
/* .local v0, "device_id":Ljava/lang/String; */
/* if-ne p1, v1, :cond_0 */
/* .line 717 */
com.android.server.audio.dolbyeffect.DeviceId .getUsbDeviceId ( p2,p3 );
/* .line 718 */
} // :cond_0
/* if-ne p1, v1, :cond_1 */
/* .line 719 */
com.android.server.audio.dolbyeffect.DeviceId .getBtDeviceId ( p2,p3 );
/* .line 720 */
} // :cond_1
/* if-ne p1, v1, :cond_2 */
/* .line 721 */
com.android.server.audio.dolbyeffect.DeviceId .getWiredDeviceId ( p2,p3 );
/* .line 723 */
} // :cond_2
} // :goto_0
} // .end method
public static com.android.server.audio.dolbyeffect.DolbyEffectController getInstance ( android.content.Context p0 ) {
/* .locals 1 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 92 */
v0 = com.android.server.audio.dolbyeffect.DolbyEffectController.sInstance;
/* if-nez v0, :cond_0 */
/* .line 93 */
/* new-instance v0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController; */
/* invoke-direct {v0, p0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;-><init>(Landroid/content/Context;)V */
/* .line 95 */
} // :cond_0
v0 = com.android.server.audio.dolbyeffect.DolbyEffectController.sInstance;
} // .end method
private static android.media.IAudioService getService ( ) {
/* .locals 2 */
/* .line 513 */
v0 = com.android.server.audio.dolbyeffect.DolbyEffectController.sService;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 514 */
/* .line 516 */
} // :cond_0
final String v0 = "audio"; // const-string v0, "audio"
android.os.ServiceManager .getService ( v0 );
/* .line 517 */
/* .local v0, "b":Landroid/os/IBinder; */
android.media.IAudioService$Stub .asInterface ( v0 );
/* .line 518 */
} // .end method
private void notifyEffectChanged ( ) {
/* .locals 2 */
/* .line 506 */
final String v0 = "DolbyEffectController"; // const-string v0, "DolbyEffectController"
final String v1 = "notifyEffectChanged"; // const-string v1, "notifyEffectChanged"
android.util.Log .d ( v0,v1 );
/* .line 507 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "miui.intent.action.ACTION_AUDIO_EFFECT_CHANGED"; // const-string v1, "miui.intent.action.ACTION_AUDIO_EFFECT_CHANGED"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 508 */
/* .local v0, "intent":Landroid/content/Intent; */
v1 = this.mContext;
(( android.content.Context ) v1 ).sendBroadcast ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
/* .line 509 */
return;
} // .end method
private void onBTStateChanged ( android.content.Intent p0 ) {
/* .locals 19 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 308 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
/* invoke-virtual/range {p1 ..p1}, Landroid/content/Intent;->getAction()Ljava/lang/String; */
/* .line 309 */
/* .local v2, "action":Ljava/lang/String; */
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 310 */
int v3 = 0; // const/4 v3, 0x0
/* .line 311 */
/* .local v3, "changed":Z */
v4 = (( java.lang.String ) v2 ).hashCode ( ); // invoke-virtual {v2}, Ljava/lang/String;->hashCode()I
int v5 = -1; // const/4 v5, -0x1
int v6 = 1; // const/4 v6, 0x1
int v7 = 0; // const/4 v7, 0x0
/* sparse-switch v4, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
final String v4 = "android.bluetooth.device.action.BOND_STATE_CHANGED"; // const-string v4, "android.bluetooth.device.action.BOND_STATE_CHANGED"
v4 = (( java.lang.String ) v2 ).equals ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_0
/* move v4, v6 */
/* :sswitch_1 */
final String v4 = "android.bluetooth.headset.action.VENDOR_SPECIFIC_HEADSET_EVENT"; // const-string v4, "android.bluetooth.headset.action.VENDOR_SPECIFIC_HEADSET_EVENT"
v4 = (( java.lang.String ) v2 ).equals ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_0
/* move v4, v7 */
} // :goto_0
/* move v4, v5 */
} // :goto_1
final String v8 = "DolbyEffectController"; // const-string v8, "DolbyEffectController"
/* packed-switch v4, :pswitch_data_0 */
/* goto/16 :goto_4 */
/* .line 334 */
/* :pswitch_0 */
final String v4 = "android.bluetooth.device.extra.BOND_STATE"; // const-string v4, "android.bluetooth.device.extra.BOND_STATE"
v4 = (( android.content.Intent ) v1 ).getIntExtra ( v4, v5 ); // invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 335 */
/* .local v4, "state":I */
final String v5 = "android.bluetooth.device.extra.DEVICE"; // const-string v5, "android.bluetooth.device.extra.DEVICE"
(( android.content.Intent ) v1 ).getParcelableExtra ( v5 ); // invoke-virtual {v1, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;
/* check-cast v5, Landroid/bluetooth/BluetoothDevice; */
/* .line 336 */
/* .local v5, "extraDevice":Landroid/bluetooth/BluetoothDevice; */
if ( v5 != null) { // if-eqz v5, :cond_1
(( android.bluetooth.BluetoothDevice ) v5 ).getAddress ( ); // invoke-virtual {v5}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;
} // :cond_1
final String v6 = "NULL"; // const-string v6, "NULL"
/* .line 337 */
/* .local v6, "deviceName2":Ljava/lang/String; */
} // :goto_2
/* const/16 v7, 0xa */
/* if-ne v4, v7, :cond_4 */
/* .line 338 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "ACTION_BOND_STATE_CHANGED: device "; // const-string v9, "ACTION_BOND_STATE_CHANGED: device "
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = " bond disconnect"; // const-string v9, " bond disconnect"
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v8,v7 );
/* .line 339 */
v3 = /* invoke-direct {v0, v6}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->removeDeviceForName(Ljava/lang/String;)Z */
/* goto/16 :goto_4 */
/* .line 313 */
} // .end local v4 # "state":I
} // .end local v5 # "extraDevice":Landroid/bluetooth/BluetoothDevice;
} // .end local v6 # "deviceName2":Ljava/lang/String;
/* :pswitch_1 */
int v4 = 2; // const/4 v4, 0x2
/* new-array v4, v4, [I */
/* .line 314 */
/* .local v4, "ids":[I */
final String v5 = ""; // const-string v5, ""
/* filled-new-array {v5}, [Ljava/lang/String; */
/* .line 315 */
/* .local v5, "deviceName":[Ljava/lang/String; */
v9 = com.android.server.audio.dolbyeffect.deviceinfo.BtDeviceInfo .tryGetIdsFromIntent ( v1,v4,v5 );
if ( v9 != null) { // if-eqz v9, :cond_4
/* aget v9, v4, v7 */
if ( v9 != null) { // if-eqz v9, :cond_4
/* aget v9, v4, v6 */
if ( v9 != null) { // if-eqz v9, :cond_4
/* .line 316 */
/* aget-object v9, v5, v7 */
v9 = /* invoke-direct {v0, v9}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->containsDevice(Ljava/lang/String;)I */
/* .line 317 */
/* .local v9, "idx":I */
final String v10 = " minorid: "; // const-string v10, " minorid: "
final String v11 = " majorid: "; // const-string v11, " majorid: "
/* if-ltz v9, :cond_3 */
/* .line 318 */
v12 = com.android.server.audio.dolbyeffect.DolbyEffectController.mCurrentDevices;
(( java.util.LinkedList ) v12 ).get ( v9 ); // invoke-virtual {v12, v9}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;
/* check-cast v12, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo; */
/* .line 319 */
/* .local v12, "device2":Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo; */
v13 = (( com.android.server.audio.dolbyeffect.deviceinfo.BtDeviceInfo ) v12 ).getMajorID ( ); // invoke-virtual {v12}, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->getMajorID()I
/* aget v14, v4, v7 */
/* if-eq v13, v14, :cond_2 */
v13 = (( com.android.server.audio.dolbyeffect.deviceinfo.BtDeviceInfo ) v12 ).getMinorID ( ); // invoke-virtual {v12}, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->getMinorID()I
/* aget v14, v4, v6 */
/* if-eq v13, v14, :cond_2 */
/* .line 320 */
/* aget v13, v4, v7 */
(( com.android.server.audio.dolbyeffect.deviceinfo.BtDeviceInfo ) v12 ).setMajorID ( v13 ); // invoke-virtual {v12, v13}, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->setMajorID(I)V
/* .line 321 */
/* aget v13, v4, v6 */
(( com.android.server.audio.dolbyeffect.deviceinfo.BtDeviceInfo ) v12 ).setMinorID ( v13 ); // invoke-virtual {v12, v13}, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->setMinorID(I)V
/* .line 322 */
/* new-instance v13, Ljava/lang/StringBuilder; */
/* invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V */
final String v14 = "onBTStateChanged: device updated "; // const-string v14, "onBTStateChanged: device updated "
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-object v14, v5, v7 */
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v11 ); // invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget v7, v4, v7 */
(( java.lang.StringBuilder ) v11 ).append ( v7 ); // invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v10 ); // invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget v6, v4, v6 */
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v8,v6 );
/* .line 323 */
int v3 = 1; // const/4 v3, 0x1
/* .line 325 */
} // .end local v12 # "device2":Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;
} // :cond_2
/* .line 326 */
} // :cond_3
/* new-instance v18, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo; */
/* aget v14, v4, v7 */
/* aget v15, v4, v6 */
/* aget-object v16, v5, v7 */
/* const/16 v17, 0x0 */
/* move-object/from16 v12, v18 */
/* invoke-direct/range {v12 ..v17}, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;-><init>(IIILjava/lang/String;Z)V */
/* .line 327 */
/* .local v12, "newDevice":Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo; */
v13 = com.android.server.audio.dolbyeffect.DolbyEffectController.mCurrentDevices;
(( java.util.LinkedList ) v13 ).add ( v12 ); // invoke-virtual {v13, v12}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
/* .line 328 */
int v3 = 1; // const/4 v3, 0x1
/* .line 329 */
/* new-instance v13, Ljava/lang/StringBuilder; */
/* invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V */
final String v14 = "onBTStateChanged: device connected "; // const-string v14, "onBTStateChanged: device connected "
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-object v14, v5, v7 */
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v11 ); // invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget v7, v4, v7 */
(( java.lang.StringBuilder ) v11 ).append ( v7 ); // invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v10 ); // invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget v6, v4, v6 */
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v8,v6 );
/* .line 331 */
} // .end local v9 # "idx":I
} // .end local v12 # "newDevice":Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;
} // :goto_3
/* nop */
/* .line 345 */
} // .end local v4 # "ids":[I
} // .end local v5 # "deviceName":[Ljava/lang/String;
} // :cond_4
} // :goto_4
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 346 */
v4 = com.android.server.audio.dolbyeffect.DolbyEffectController.mCurrentDevices;
/* invoke-direct {v0, v4}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->onDeviceChanged(Ljava/util/LinkedList;)V */
/* .line 349 */
} // .end local v3 # "changed":Z
} // :cond_5
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x69ab72ba -> :sswitch_1 */
/* 0x7e2cc189 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void onBTStateChangedFromDeviceBroker ( java.lang.String p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 10 */
/* .param p1, "device" # Ljava/lang/String; */
/* .param p2, "profile" # Ljava/lang/String; */
/* .param p3, "state" # Ljava/lang/String; */
/* .line 352 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "onBTStateChangedFromDeviceBroker btDevice="; // const-string v1, "onBTStateChangedFromDeviceBroker btDevice="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " profile="; // const-string v1, " profile="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " state="; // const-string v1, " state="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "DolbyEffectController"; // const-string v1, "DolbyEffectController"
android.util.Log .d ( v1,v0 );
/* .line 356 */
final String v0 = "A2DP"; // const-string v0, "A2DP"
v0 = (( java.lang.String ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 357 */
final String v0 = "STATE_CONNECTED"; // const-string v0, "STATE_CONNECTED"
v0 = (( java.lang.String ) v0 ).equals ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 358 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->containsDevice(Ljava/lang/String;)I */
/* .line 359 */
/* .local v0, "idx":I */
/* if-ltz v0, :cond_0 */
/* .line 360 */
v2 = com.android.server.audio.dolbyeffect.DolbyEffectController.mCurrentDevices;
(( java.util.LinkedList ) v2 ).get ( v0 ); // invoke-virtual {v2, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo; */
/* .line 361 */
/* .local v2, "product":Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo; */
v3 = com.android.server.audio.dolbyeffect.DolbyEffectController.mCurrentDevices;
(( java.util.LinkedList ) v3 ).remove ( v0 ); // invoke-virtual {v3, v0}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;
/* .line 362 */
int v3 = 1; // const/4 v3, 0x1
(( com.android.server.audio.dolbyeffect.deviceinfo.BtDeviceInfo ) v2 ).setState ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->setState(Z)V
/* .line 363 */
v3 = com.android.server.audio.dolbyeffect.DolbyEffectController.mCurrentDevices;
(( java.util.LinkedList ) v3 ).add ( v2 ); // invoke-virtual {v3, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
/* .line 364 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "onBTStateChangedFromDeviceBroker: device active: "; // const-string v4, "onBTStateChangedFromDeviceBroker: device active: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " majorid: "; // const-string v4, " majorid: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = (( com.android.server.audio.dolbyeffect.deviceinfo.BtDeviceInfo ) v2 ).getMajorID ( ); // invoke-virtual {v2}, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->getMajorID()I
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " minorid: "; // const-string v4, " minorid: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = (( com.android.server.audio.dolbyeffect.deviceinfo.BtDeviceInfo ) v2 ).getMinorID ( ); // invoke-virtual {v2}, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->getMinorID()I
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v3 );
/* .line 365 */
} // .end local v2 # "product":Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;
/* .line 366 */
} // :cond_0
v2 = com.android.server.audio.dolbyeffect.DolbyEffectController.mCurrentDevices;
/* new-instance v9, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo; */
int v5 = 0; // const/4 v5, 0x0
int v6 = 0; // const/4 v6, 0x0
int v8 = 1; // const/4 v8, 0x1
/* move-object v3, v9 */
/* move-object v7, p1 */
/* invoke-direct/range {v3 ..v8}, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;-><init>(IIILjava/lang/String;Z)V */
(( java.util.LinkedList ) v2 ).add ( v9 ); // invoke-virtual {v2, v9}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
/* .line 367 */
final String v2 = "onBTStateChangedFromDeviceBroker: Bt headset connected"; // const-string v2, "onBTStateChangedFromDeviceBroker: Bt headset connected"
android.util.Log .d ( v1,v2 );
/* .line 369 */
} // :goto_0
v1 = com.android.server.audio.dolbyeffect.DolbyEffectController.mCurrentDevices;
/* invoke-direct {p0, v1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->onDeviceChanged(Ljava/util/LinkedList;)V */
} // .end local v0 # "idx":I
/* .line 370 */
} // :cond_1
final String v0 = "STATE_DISCONNECTED"; // const-string v0, "STATE_DISCONNECTED"
v0 = (( java.lang.String ) v0 ).equals ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 371 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->containsDevice(Ljava/lang/String;)I */
/* .line 372 */
/* .restart local v0 # "idx":I */
/* if-ltz v0, :cond_3 */
/* .line 373 */
v2 = com.android.server.audio.dolbyeffect.DolbyEffectController.mCurrentDevices;
(( java.util.LinkedList ) v2 ).get ( v0 ); // invoke-virtual {v2, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo; */
/* .line 374 */
/* .restart local v2 # "product":Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo; */
int v3 = 0; // const/4 v3, 0x0
(( com.android.server.audio.dolbyeffect.deviceinfo.BtDeviceInfo ) v2 ).setState ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->setState(Z)V
/* .line 375 */
v3 = com.android.server.audio.dolbyeffect.DolbyEffectController.mCurrentDevices;
(( java.util.LinkedList ) v3 ).remove ( v0 ); // invoke-virtual {v3, v0}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;
/* .line 376 */
final String v3 = "onBTStateChangedFromDeviceBroker: Bt headset disconnected"; // const-string v3, "onBTStateChangedFromDeviceBroker: Bt headset disconnected"
android.util.Log .d ( v1,v3 );
/* .line 377 */
v1 = com.android.server.audio.dolbyeffect.DolbyEffectController.mCurrentDevices;
/* invoke-direct {p0, v1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->onDeviceChanged(Ljava/util/LinkedList;)V */
/* .line 370 */
} // .end local v0 # "idx":I
} // .end local v2 # "product":Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;
} // :cond_2
} // :goto_1
/* nop */
/* .line 381 */
} // :cond_3
} // :goto_2
return;
} // .end method
private void onDeviceChanged ( java.util.LinkedList p0 ) {
/* .locals 8 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/LinkedList<", */
/* "Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 442 */
/* .local p1, "currentDevices":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;>;" */
final String v0 = "onDeviceChanged"; // const-string v0, "onDeviceChanged"
final String v1 = "DolbyEffectController"; // const-string v1, "DolbyEffectController"
android.util.Log .d ( v1,v0 );
/* .line 444 */
try { // :try_start_0
/* iget v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I */
/* .line 445 */
/* .local v0, "cacheCurrentPort":I */
v2 = (( java.util.LinkedList ) p1 ).isEmpty ( ); // invoke-virtual {p1}, Ljava/util/LinkedList;->isEmpty()Z
int v3 = 3; // const/4 v3, 0x3
int v4 = 0; // const/4 v4, 0x0
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 446 */
final String v2 = "no devices connected, CurrentPort is set to INTERNAL_SPEAKER"; // const-string v2, "no devices connected, CurrentPort is set to INTERNAL_SPEAKER"
android.util.Log .d ( v1,v2 );
/* .line 447 */
/* iput v4, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I */
/* .line 448 */
/* const-string/jumbo v2, "true" */
/* const-string/jumbo v4, "vendor.audio.dolby.control.tunning.by.volume.support" */
android.os.SystemProperties .get ( v4 );
v2 = (( java.lang.String ) v2 ).equals ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 449 */
com.android.server.audio.dolbyeffect.DolbyEffectController .getService ( );
v2 = int v4 = 2; // const/4 v4, 0x2
/* .line 450 */
/* .local v2, "index":I */
/* iget v3, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mVolumeThreshold:I */
/* if-le v2, v3, :cond_0 */
/* .line 451 */
final String v3 = "HIGH"; // const-string v3, "HIGH"
this.mCurrentVolumeLevel = v3;
/* .line 452 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "no devices connected and volume >= "; // const-string v4, "no devices connected and volume >= "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mVolumeThreshold:I */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v3 );
/* .line 454 */
} // :cond_0
final String v3 = "LOW"; // const-string v3, "LOW"
this.mCurrentVolumeLevel = v3;
/* .line 455 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "no devices connected and volume < "; // const-string v4, "no devices connected and volume < "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mVolumeThreshold:I */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v3 );
/* .line 458 */
} // .end local v2 # "index":I
} // :cond_1
} // :goto_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "updateDolbyTunning from onDeviceChanged(),mCurrentPort is " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " newPort is"; // const-string v3, " newPort is"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v2 );
/* .line 459 */
(( com.android.server.audio.dolbyeffect.DolbyEffectController ) p0 ).updateDolbyTunning ( ); // invoke-virtual {p0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->updateDolbyTunning()V
/* .line 461 */
} // :cond_2
(( java.util.LinkedList ) p1 ).getLast ( ); // invoke-virtual {p1}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase; */
/* .line 462 */
/* .local v2, "device":Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase; */
v5 = (( com.android.server.audio.dolbyeffect.deviceinfo.DeviceInfoBase ) v2 ).getDeviceType ( ); // invoke-virtual {v2}, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->getDeviceType()I
/* if-ne v5, v6, :cond_3 */
/* .line 463 */
int v3 = 5; // const/4 v3, 0x5
/* iput v3, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I */
/* .line 464 */
/* const-string/jumbo v3, "setSelectedTuningDevice for USB devices" */
android.util.Log .d ( v1,v3 );
/* .line 465 */
} // :cond_3
v5 = (( com.android.server.audio.dolbyeffect.deviceinfo.DeviceInfoBase ) v2 ).getDeviceType ( ); // invoke-virtual {v2}, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->getDeviceType()I
/* if-ne v5, v6, :cond_4 */
/* .line 466 */
/* iput v3, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I */
/* .line 467 */
/* const-string/jumbo v3, "setSelectedTuningDevice for wired devices" */
android.util.Log .d ( v1,v3 );
/* .line 468 */
} // :cond_4
v3 = (( com.android.server.audio.dolbyeffect.deviceinfo.DeviceInfoBase ) v2 ).getDeviceType ( ); // invoke-virtual {v2}, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->getDeviceType()I
/* if-ne v3, v5, :cond_6 */
/* .line 469 */
/* move-object v3, v2 */
/* check-cast v3, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo; */
/* .line 470 */
/* .local v3, "device2":Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo; */
v5 = (( com.android.server.audio.dolbyeffect.deviceinfo.BtDeviceInfo ) v3 ).getMajorID ( ); // invoke-virtual {v3}, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->getMajorID()I
/* .line 471 */
/* .local v5, "id1":I */
v6 = (( com.android.server.audio.dolbyeffect.deviceinfo.BtDeviceInfo ) v3 ).getMinorID ( ); // invoke-virtual {v3}, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->getMinorID()I
/* .line 472 */
/* .local v6, "id2":I */
v7 = (( com.android.server.audio.dolbyeffect.deviceinfo.BtDeviceInfo ) v3 ).getState ( ); // invoke-virtual {v3}, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->getState()Z
/* .line 473 */
/* .local v7, "state":Z */
if ( v7 != null) { // if-eqz v7, :cond_5
/* .line 474 */
int v4 = 4; // const/4 v4, 0x4
/* iput v4, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I */
/* .line 476 */
} // :cond_5
/* iput v4, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I */
/* .line 487 */
} // .end local v3 # "device2":Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;
} // .end local v5 # "id1":I
} // .end local v6 # "id2":I
} // .end local v7 # "state":Z
} // :cond_6
} // :goto_1
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "updateDolbyTunning from onDeviceChanged(), mCurrentPort is " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " newPort is "; // const-string v4, " newPort is "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v3 );
/* .line 488 */
(( com.android.server.audio.dolbyeffect.DolbyEffectController ) p0 ).updateDolbyTunning ( ); // invoke-virtual {p0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->updateDolbyTunning()V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 492 */
} // .end local v0 # "cacheCurrentPort":I
} // .end local v2 # "device":Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;
} // :goto_2
/* .line 490 */
/* :catch_0 */
/* move-exception v0 */
/* .line 491 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "onDeviceChanged: Exception "; // const-string v3, "onDeviceChanged: Exception "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 494 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_3
/* iget-boolean v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mIsSupportFWAudioEffectCenter:Z */
if ( v0 != null) { // if-eqz v0, :cond_9
/* .line 495 */
v0 = this.mContext;
android.media.audiofx.AudioEffectCenter .getInstance ( v0 );
/* .line 496 */
/* .local v0, "audioEffectCenter":Landroid/media/audiofx/AudioEffectCenter; */
final String v1 = "dolby"; // const-string v1, "dolby"
v1 = (( android.media.audiofx.AudioEffectCenter ) v0 ).isEffectActive ( v1 ); // invoke-virtual {v0, v1}, Landroid/media/audiofx/AudioEffectCenter;->isEffectActive(Ljava/lang/String;)Z
/* if-nez v1, :cond_8 */
/* .line 497 */
final String v1 = "misound"; // const-string v1, "misound"
v2 = (( android.media.audiofx.AudioEffectCenter ) v0 ).isEffectActive ( v1 ); // invoke-virtual {v0, v1}, Landroid/media/audiofx/AudioEffectCenter;->isEffectActive(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 500 */
} // :cond_7
int v2 = 1; // const/4 v2, 0x1
(( android.media.audiofx.AudioEffectCenter ) v0 ).setEffectActive ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/media/audiofx/AudioEffectCenter;->setEffectActive(Ljava/lang/String;Z)V
/* .line 498 */
} // :cond_8
} // :goto_4
/* invoke-direct {p0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->notifyEffectChanged()V */
/* .line 503 */
} // .end local v0 # "audioEffectCenter":Landroid/media/audiofx/AudioEffectCenter;
} // :cond_9
} // :goto_5
return;
} // .end method
private void onHeadsetPlug ( android.content.Intent p0 ) {
/* .locals 10 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 252 */
final String v0 = "DolbyEffectController"; // const-string v0, "DolbyEffectController"
int v1 = 0; // const/4 v1, 0x0
/* .line 253 */
/* .local v1, "changed":Z */
try { // :try_start_0
com.android.server.audio.dolbyeffect.DolbyEffectController .getService ( );
v2 = int v3 = 3; // const/4 v3, 0x3
/* .line 254 */
/* .local v2, "newDevices":I */
/* const-string/jumbo v3, "state" */
int v4 = 0; // const/4 v4, 0x0
v3 = (( android.content.Intent ) p1 ).getIntExtra ( v3, v4 ); // invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 255 */
/* .local v3, "state":I */
/* const/high16 v5, 0x4000000 */
/* const-string/jumbo v6, "wired headset" */
final String v7 = "USB headset"; // const-string v7, "USB headset"
/* if-nez v3, :cond_1 */
/* .line 256 */
try { // :try_start_1
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "detected device disconnected, new devices: "; // const-string v8, "detected device disconnected, new devices: "
(( java.lang.StringBuilder ) v4 ).append ( v8 ); // invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v4 );
/* .line 258 */
/* and-int v4, v2, v5 */
/* if-nez v4, :cond_0 */
v4 = /* invoke-direct {p0, v7}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->containsDevice(Ljava/lang/String;)I */
/* if-ltz v4, :cond_0 */
/* .line 259 */
v4 = /* invoke-direct {p0, v7}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->removeDeviceForName(Ljava/lang/String;)Z */
/* move v1, v4 */
/* .line 262 */
} // :cond_0
/* and-int/lit8 v4, v2, 0xc */
/* if-nez v4, :cond_4 */
v4 = /* invoke-direct {p0, v6}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->containsDevice(Ljava/lang/String;)I */
/* if-ltz v4, :cond_4 */
/* .line 263 */
v4 = /* invoke-direct {p0, v6}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->removeDeviceForName(Ljava/lang/String;)Z */
/* move v1, v4 */
/* .line 265 */
} // :cond_1
int v8 = 1; // const/4 v8, 0x1
/* if-ne v3, v8, :cond_4 */
/* .line 266 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "detected device connected, new devices: "; // const-string v9, "detected device connected, new devices: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v2 ); // invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v8 );
/* .line 268 */
/* and-int/2addr v5, v2 */
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 269 */
/* new-instance v5, Lcom/android/server/audio/dolbyeffect/deviceinfo/UsbDeviceInfo; */
/* invoke-direct {v5, v6, v4, v4}, Lcom/android/server/audio/dolbyeffect/deviceinfo/UsbDeviceInfo;-><init>(III)V */
/* move-object v4, v5 */
/* .line 270 */
/* .local v4, "newDevice":Lcom/android/server/audio/dolbyeffect/deviceinfo/UsbDeviceInfo; */
v5 = /* invoke-direct {p0, v7}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->containsDevice(Ljava/lang/String;)I */
/* if-gez v5, :cond_2 */
/* .line 271 */
v5 = com.android.server.audio.dolbyeffect.DolbyEffectController.mCurrentDevices;
(( java.util.LinkedList ) v5 ).add ( v4 ); // invoke-virtual {v5, v4}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
/* .line 272 */
int v1 = 1; // const/4 v1, 0x1
/* .line 273 */
final String v5 = "onReceive: USB headset connected"; // const-string v5, "onReceive: USB headset connected"
android.util.Log .d ( v0,v5 );
/* .line 275 */
} // .end local v4 # "newDevice":Lcom/android/server/audio/dolbyeffect/deviceinfo/UsbDeviceInfo;
} // :cond_2
} // :cond_3
/* and-int/lit8 v5, v2, 0xc */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 276 */
/* new-instance v5, Lcom/android/server/audio/dolbyeffect/deviceinfo/WiredDeviceInfo; */
/* invoke-direct {v5, v7, v4, v4}, Lcom/android/server/audio/dolbyeffect/deviceinfo/WiredDeviceInfo;-><init>(III)V */
/* move-object v4, v5 */
/* .line 277 */
/* .local v4, "newDevice":Lcom/android/server/audio/dolbyeffect/deviceinfo/WiredDeviceInfo; */
v5 = /* invoke-direct {p0, v6}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->containsDevice(Ljava/lang/String;)I */
/* if-gez v5, :cond_4 */
/* .line 278 */
v5 = com.android.server.audio.dolbyeffect.DolbyEffectController.mCurrentDevices;
(( java.util.LinkedList ) v5 ).add ( v4 ); // invoke-virtual {v5, v4}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
/* .line 279 */
int v1 = 1; // const/4 v1, 0x1
/* .line 280 */
final String v5 = "onReceive: wired headset connected"; // const-string v5, "onReceive: wired headset connected"
android.util.Log .d ( v0,v5 );
/* .line 284 */
} // .end local v4 # "newDevice":Lcom/android/server/audio/dolbyeffect/deviceinfo/WiredDeviceInfo;
} // :cond_4
} // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 285 */
v4 = com.android.server.audio.dolbyeffect.DolbyEffectController.mCurrentDevices;
/* invoke-direct {p0, v4}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->onDeviceChanged(Ljava/util/LinkedList;)V */
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 289 */
} // .end local v1 # "changed":Z
} // .end local v2 # "newDevices":I
} // .end local v3 # "state":I
} // :cond_5
/* .line 287 */
/* :catch_0 */
/* move-exception v1 */
/* .line 288 */
/* .local v1, "e":Landroid/os/RemoteException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "onHeadsetPlug: RemoteException "; // const-string v3, "onHeadsetPlug: RemoteException "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v2 );
/* .line 290 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_1
return;
} // .end method
private void onRotationChanged ( Integer p0 ) {
/* .locals 6 */
/* .param p1, "rotation" # I */
/* .line 408 */
final String v0 = "DolbyEffectController"; // const-string v0, "DolbyEffectController"
try { // :try_start_0
v1 = this.mCurrentRotation;
/* .line 409 */
/* .local v1, "cachCurrentRotation":Ljava/lang/String; */
java.lang.String .valueOf ( p1 );
/* .line 410 */
/* .local v2, "newRotation":Ljava/lang/String; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "newRotation is "; // const-string v4, "newRotation is "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v3 );
/* .line 412 */
final String v3 = "0"; // const-string v3, "0"
v3 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 413 */
final String v3 = "90"; // const-string v3, "90"
/* move-object v2, v3 */
/* .line 416 */
} // :cond_0
this.mCurrentRotation = v2;
/* .line 418 */
v3 = this.mSpatializerEnabledAvaliable;
int v4 = 0; // const/4 v4, 0x0
/* aget-boolean v4, v3, v4 */
if ( v4 != null) { // if-eqz v4, :cond_4
int v4 = 1; // const/4 v4, 0x1
/* aget-boolean v3, v3, v4 */
/* if-nez v3, :cond_1 */
/* .line 423 */
} // :cond_1
/* iget v3, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 424 */
final String v3 = "Current Spatilizer routing device is not speaker, bypass onRotationChanged"; // const-string v3, "Current Spatilizer routing device is not speaker, bypass onRotationChanged"
android.util.Log .d ( v0,v3 );
/* .line 425 */
return;
/* .line 428 */
} // :cond_2
v3 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_0 */
/* .catch Ljava/lang/RuntimeException; {:try_start_0 ..:try_end_0} :catch_0 */
final String v4 = "newRotation is"; // const-string v4, "newRotation is"
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 429 */
try { // :try_start_1
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "mCurrentRotation is "; // const-string v5, "mCurrentRotation is "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", bypass onRotationChanged"; // const-string v4, ", bypass onRotationChanged"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v3 );
/* .line 430 */
return;
/* .line 433 */
} // :cond_3
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "updateDolbyTunning from onRotationChanged(), mCurrentRotation is " */
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v3 );
/* .line 434 */
(( com.android.server.audio.dolbyeffect.DolbyEffectController ) p0 ).updateDolbyTunning ( ); // invoke-virtual {p0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->updateDolbyTunning()V
/* .line 438 */
} // .end local v1 # "cachCurrentRotation":Ljava/lang/String;
} // .end local v2 # "newRotation":Ljava/lang/String;
/* .line 419 */
/* .restart local v1 # "cachCurrentRotation":Ljava/lang/String; */
/* .restart local v2 # "newRotation":Ljava/lang/String; */
} // :cond_4
} // :goto_0
final String v3 = "SpatializerEnabledAvaliable is false, bypass onRotationChanged"; // const-string v3, "SpatializerEnabledAvaliable is false, bypass onRotationChanged"
android.util.Log .d ( v0,v3 );
/* :try_end_1 */
/* .catch Ljava/lang/RuntimeException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 420 */
return;
/* .line 436 */
} // .end local v1 # "cachCurrentRotation":Ljava/lang/String;
} // .end local v2 # "newRotation":Ljava/lang/String;
/* :catch_0 */
/* move-exception v1 */
/* .line 437 */
/* .local v1, "e":Ljava/lang/RuntimeException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "onRotationChanged: RuntimeException "; // const-string v3, "onRotationChanged: RuntimeException "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v2 );
/* .line 439 */
} // .end local v1 # "e":Ljava/lang/RuntimeException;
} // :goto_1
return;
} // .end method
private void onSpatializerStateChanged ( Integer p0, Boolean p1 ) {
/* .locals 6 */
/* .param p1, "type" # I */
/* .param p2, "value" # Z */
/* .line 293 */
v0 = this.mSpatializerEnabledAvaliable;
int v1 = 0; // const/4 v1, 0x0
/* aget-boolean v2, v0, v1 */
int v3 = 1; // const/4 v3, 0x1
/* aget-boolean v4, v0, v3 */
/* and-int/2addr v2, v4 */
/* .line 294 */
/* .local v2, "currentState":Z */
/* move v4, v2 */
/* .line 295 */
/* .local v4, "cachecurrentState":Z */
int v5 = 5; // const/4 v5, 0x5
/* if-ne p1, v5, :cond_0 */
/* .line 296 */
/* aput-boolean p2, v0, v1 */
/* .line 297 */
} // :cond_0
int v5 = 6; // const/4 v5, 0x6
/* if-ne p1, v5, :cond_1 */
/* .line 298 */
/* aput-boolean p2, v0, v3 */
/* .line 300 */
} // :cond_1
} // :goto_0
/* aget-boolean v1, v0, v1 */
/* aget-boolean v0, v0, v3 */
/* and-int/2addr v0, v1 */
/* .line 301 */
/* .local v0, "newState":Z */
/* if-eq v2, v0, :cond_2 */
/* .line 302 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "updateDolbyTunning() from onSpatializerStateChanged(), currentState is" */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = " newState is"; // const-string v3, " newState is"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "DolbyEffectController"; // const-string v3, "DolbyEffectController"
android.util.Log .d ( v3,v1 );
/* .line 303 */
(( com.android.server.audio.dolbyeffect.DolbyEffectController ) p0 ).updateDolbyTunning ( ); // invoke-virtual {p0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->updateDolbyTunning()V
/* .line 305 */
} // :cond_2
return;
} // .end method
private void onVolumeChanged ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "newVolume" # I */
/* .line 386 */
final String v0 = "DolbyEffectController"; // const-string v0, "DolbyEffectController"
try { // :try_start_0
v1 = this.mCurrentVolumeLevel;
/* move-object v2, v1 */
/* .line 387 */
/* .local v2, "cachCurrentVolumeLevel":Ljava/lang/String; */
/* iget v3, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mVolumeThreshold:I */
/* if-le p1, v3, :cond_0 */
/* .line 388 */
final String v3 = "HIGH"; // const-string v3, "HIGH"
/* .local v3, "newVolumeLevel":Ljava/lang/String; */
/* .line 390 */
} // .end local v3 # "newVolumeLevel":Ljava/lang/String;
} // :cond_0
final String v3 = "LOW"; // const-string v3, "LOW"
/* .line 392 */
/* .restart local v3 # "newVolumeLevel":Ljava/lang/String; */
} // :goto_0
v1 = (( java.lang.String ) v3 ).equals ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 393 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "newVolumeLevel = "; // const-string v4, "newVolumeLevel = "
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " mCurrentVolumeLevel = "; // const-string v4, " mCurrentVolumeLevel = "
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mCurrentVolumeLevel;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " bypass onVolumeChanged"; // const-string v4, " bypass onVolumeChanged"
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v1 );
/* .line 394 */
return;
/* .line 396 */
} // :cond_1
this.mCurrentVolumeLevel = v3;
/* .line 398 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "updateDolbyTunning from onVolumeChanged(), mCurrentVolumeLevel is " */
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " newVolumeLevel is "; // const-string v4, " newVolumeLevel is "
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v1 );
/* .line 399 */
(( com.android.server.audio.dolbyeffect.DolbyEffectController ) p0 ).updateDolbyTunning ( ); // invoke-virtual {p0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->updateDolbyTunning()V
/* :try_end_0 */
/* .catch Ljava/lang/RuntimeException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 403 */
} // .end local v2 # "cachCurrentVolumeLevel":Ljava/lang/String;
} // .end local v3 # "newVolumeLevel":Ljava/lang/String;
/* .line 401 */
/* :catch_0 */
/* move-exception v1 */
/* .line 402 */
/* .local v1, "e":Ljava/lang/RuntimeException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "onVolumeChanged: RuntimeException "; // const-string v3, "onVolumeChanged: RuntimeException "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v2 );
/* .line 404 */
} // .end local v1 # "e":Ljava/lang/RuntimeException;
} // :goto_1
return;
} // .end method
private Boolean removeDeviceForName ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "deviceName" # Ljava/lang/String; */
/* .line 547 */
int v0 = 0; // const/4 v0, 0x0
/* .line 548 */
/* .local v0, "product":Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase; */
v1 = com.android.server.audio.dolbyeffect.DolbyEffectController.mCurrentDevices;
v1 = (( java.util.LinkedList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/LinkedList;->size()I
int v2 = 1; // const/4 v2, 0x1
/* sub-int/2addr v1, v2 */
/* .local v1, "i":I */
} // :goto_0
/* if-ltz v1, :cond_1 */
/* .line 549 */
v3 = com.android.server.audio.dolbyeffect.DolbyEffectController.mCurrentDevices;
(( java.util.LinkedList ) v3 ).get ( v1 ); // invoke-virtual {v3, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;
/* move-object v0, v3 */
/* check-cast v0, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase; */
/* .line 550 */
(( com.android.server.audio.dolbyeffect.deviceinfo.DeviceInfoBase ) v0 ).getDevice ( ); // invoke-virtual {v0}, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->getDevice()Ljava/lang/String;
v3 = (( java.lang.String ) v3 ).equals ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 551 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "remove device name: "; // const-string v4, "remove device name: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "DolbyEffectController"; // const-string v4, "DolbyEffectController"
android.util.Log .d ( v4,v3 );
/* .line 552 */
v3 = com.android.server.audio.dolbyeffect.DolbyEffectController.mCurrentDevices;
(( java.util.LinkedList ) v3 ).remove ( v1 ); // invoke-virtual {v3, v1}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;
/* .line 553 */
/* .line 548 */
} // :cond_0
/* add-int/lit8 v1, v1, -0x1 */
/* .line 556 */
} // .end local v1 # "i":I
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // .end method
private Boolean removeDeviceForType ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "type" # I */
/* .line 522 */
int v0 = 0; // const/4 v0, 0x0
/* .line 523 */
/* .local v0, "product":Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase; */
int v1 = 0; // const/4 v1, 0x0
/* .line 524 */
/* .local v1, "removed":Z */
v2 = com.android.server.audio.dolbyeffect.DolbyEffectController.mCurrentDevices;
v2 = (( java.util.LinkedList ) v2 ).size ( ); // invoke-virtual {v2}, Ljava/util/LinkedList;->size()I
/* add-int/lit8 v2, v2, -0x1 */
/* .local v2, "i":I */
} // :goto_0
/* if-ltz v2, :cond_1 */
/* .line 525 */
v3 = com.android.server.audio.dolbyeffect.DolbyEffectController.mCurrentDevices;
(( java.util.LinkedList ) v3 ).get ( v2 ); // invoke-virtual {v3, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;
/* move-object v0, v3 */
/* check-cast v0, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase; */
/* .line 526 */
v3 = (( com.android.server.audio.dolbyeffect.deviceinfo.DeviceInfoBase ) v0 ).getDeviceType ( ); // invoke-virtual {v0}, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->getDeviceType()I
/* if-ne v3, p1, :cond_0 */
/* .line 527 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "remove device type: "; // const-string v4, "remove device type: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "DolbyEffectController"; // const-string v4, "DolbyEffectController"
android.util.Log .d ( v4,v3 );
/* .line 528 */
v3 = com.android.server.audio.dolbyeffect.DolbyEffectController.mCurrentDevices;
(( java.util.LinkedList ) v3 ).remove ( v2 ); // invoke-virtual {v3, v2}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;
/* .line 529 */
int v1 = 1; // const/4 v1, 0x1
/* .line 524 */
} // :cond_0
/* add-int/lit8 v2, v2, -0x1 */
/* .line 532 */
} // .end local v2 # "i":I
} // :cond_1
} // .end method
/* # virtual methods */
public java.lang.String acquireDolbyDeviceIdForAdspSpatializer ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "port" # I */
/* .line 650 */
final String v0 = "default_internal_speaker"; // const-string v0, "default_internal_speaker"
/* .line 651 */
/* .local v0, "device_id":Ljava/lang/String; */
/* packed-switch p1, :pswitch_data_0 */
/* :pswitch_0 */
/* .line 659 */
/* :pswitch_1 */
final String v0 = "headphone_spatializer"; // const-string v0, "headphone_spatializer"
/* .line 660 */
/* .line 662 */
/* :pswitch_2 */
final String v0 = "bluetooth_spatializer"; // const-string v0, "bluetooth_spatializer"
/* .line 656 */
/* :pswitch_3 */
final String v0 = "headphone_spatializer"; // const-string v0, "headphone_spatializer"
/* .line 657 */
/* .line 653 */
/* :pswitch_4 */
/* const-string/jumbo v0, "speaker_spatializer_90" */
/* .line 654 */
/* nop */
/* .line 665 */
} // :goto_0
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_4 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
public java.lang.String acquireDolbyDeviceIdForNonSpatializer ( Integer p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 2 */
/* .param p1, "Port" # I */
/* .param p2, "VolumeLevel" # Ljava/lang/String; */
/* .param p3, "Rotation" # Ljava/lang/String; */
/* .line 560 */
final String v0 = "default_internal_speaker"; // const-string v0, "default_internal_speaker"
/* .line 561 */
/* .local v0, "device_id":Ljava/lang/String; */
/* packed-switch p1, :pswitch_data_0 */
/* :pswitch_0 */
/* .line 579 */
/* :pswitch_1 */
final String v0 = "default_usb"; // const-string v0, "default_usb"
/* .line 580 */
/* .line 582 */
/* :pswitch_2 */
final String v0 = "default_bluetooth"; // const-string v0, "default_bluetooth"
/* .line 576 */
/* :pswitch_3 */
final String v0 = "default_headphone"; // const-string v0, "default_headphone"
/* .line 577 */
/* .line 563 */
/* :pswitch_4 */
v1 = (( java.lang.String ) p2 ).hashCode ( ); // invoke-virtual {p2}, Ljava/lang/String;->hashCode()I
/* sparse-switch v1, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
final String v1 = "disabled"; // const-string v1, "disabled"
v1 = (( java.lang.String ) p2 ).equals ( v1 ); // invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 0; // const/4 v1, 0x0
/* :sswitch_1 */
final String v1 = "HIGH"; // const-string v1, "HIGH"
v1 = (( java.lang.String ) p2 ).equals ( v1 ); // invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 1; // const/4 v1, 0x1
/* :sswitch_2 */
final String v1 = "LOW"; // const-string v1, "LOW"
v1 = (( java.lang.String ) p2 ).equals ( v1 ); // invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 2; // const/4 v1, 0x2
} // :goto_0
int v1 = -1; // const/4 v1, -0x1
} // :goto_1
/* packed-switch v1, :pswitch_data_1 */
/* .line 571 */
/* :pswitch_5 */
/* const-string/jumbo v0, "speaker_volume_low" */
/* .line 568 */
/* :pswitch_6 */
/* const-string/jumbo v0, "speaker_volume_high" */
/* .line 569 */
/* .line 565 */
/* :pswitch_7 */
final String v0 = "default_internal_speaker"; // const-string v0, "default_internal_speaker"
/* .line 566 */
/* nop */
/* .line 574 */
} // :goto_2
/* nop */
/* .line 585 */
} // :goto_3
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_4 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x12734 -> :sswitch_2 */
/* 0x21d5a2 -> :sswitch_1 */
/* 0x10263a7c -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_1 */
/* .packed-switch 0x0 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
} // .end packed-switch
} // .end method
public java.lang.String acquireDolbyDeviceIdForSpatializer ( Integer p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 9 */
/* .param p1, "Port" # I */
/* .param p2, "VolumeLevel" # Ljava/lang/String; */
/* .param p3, "Rotation" # Ljava/lang/String; */
/* .line 589 */
final String v0 = "default_internal_speaker"; // const-string v0, "default_internal_speaker"
/* .line 590 */
/* .local v0, "device_id":Ljava/lang/String; */
/* packed-switch p1, :pswitch_data_0 */
/* :pswitch_0 */
/* goto/16 :goto_b */
/* .line 640 */
/* :pswitch_1 */
final String v0 = "headphone_spatializer"; // const-string v0, "headphone_spatializer"
/* .line 641 */
/* goto/16 :goto_b */
/* .line 643 */
/* :pswitch_2 */
final String v0 = "bluetooth_spatializer"; // const-string v0, "bluetooth_spatializer"
/* goto/16 :goto_b */
/* .line 636 */
/* :pswitch_3 */
final String v0 = "headphone_spatializer"; // const-string v0, "headphone_spatializer"
/* .line 637 */
/* goto/16 :goto_b */
/* .line 592 */
/* :pswitch_4 */
v1 = (( java.lang.String ) p2 ).hashCode ( ); // invoke-virtual {p2}, Ljava/lang/String;->hashCode()I
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
int v4 = 2; // const/4 v4, 0x2
final String v5 = "disabled"; // const-string v5, "disabled"
int v6 = -1; // const/4 v6, -0x1
/* sparse-switch v1, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
v1 = (( java.lang.String ) p2 ).equals ( v5 ); // invoke-virtual {p2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* move v1, v2 */
/* :sswitch_1 */
final String v1 = "HIGH"; // const-string v1, "HIGH"
v1 = (( java.lang.String ) p2 ).equals ( v1 ); // invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* move v1, v3 */
/* :sswitch_2 */
final String v1 = "LOW"; // const-string v1, "LOW"
v1 = (( java.lang.String ) p2 ).equals ( v1 ); // invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* move v1, v4 */
} // :goto_0
/* move v1, v6 */
} // :goto_1
final String v7 = "270"; // const-string v7, "270"
final String v8 = "90"; // const-string v8, "90"
/* packed-switch v1, :pswitch_data_1 */
/* goto/16 :goto_a */
/* .line 620 */
/* :pswitch_5 */
v1 = (( java.lang.String ) p3 ).hashCode ( ); // invoke-virtual {p3}, Ljava/lang/String;->hashCode()I
/* sparse-switch v1, :sswitch_data_1 */
} // :cond_1
/* :sswitch_3 */
v1 = (( java.lang.String ) p3 ).equals ( v5 ); // invoke-virtual {p3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* :sswitch_4 */
v1 = (( java.lang.String ) p3 ).equals ( v7 ); // invoke-virtual {p3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* move v2, v4 */
/* :sswitch_5 */
v1 = (( java.lang.String ) p3 ).equals ( v8 ); // invoke-virtual {p3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* move v2, v3 */
} // :goto_2
/* move v2, v6 */
} // :goto_3
/* packed-switch v2, :pswitch_data_2 */
/* goto/16 :goto_a */
/* .line 628 */
/* :pswitch_6 */
/* const-string/jumbo v0, "speaker_spatilizer_270_volume_low" */
/* goto/16 :goto_a */
/* .line 625 */
/* :pswitch_7 */
/* const-string/jumbo v0, "speaker_spatilizer_90_volume_low" */
/* .line 626 */
/* goto/16 :goto_a */
/* .line 622 */
/* :pswitch_8 */
/* const-string/jumbo v0, "speaker_spatilizer_90_volume_low" */
/* .line 623 */
/* goto/16 :goto_a */
/* .line 607 */
/* :pswitch_9 */
v1 = (( java.lang.String ) p3 ).hashCode ( ); // invoke-virtual {p3}, Ljava/lang/String;->hashCode()I
/* sparse-switch v1, :sswitch_data_2 */
} // :cond_2
/* :sswitch_6 */
v1 = (( java.lang.String ) p3 ).equals ( v5 ); // invoke-virtual {p3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* :sswitch_7 */
v1 = (( java.lang.String ) p3 ).equals ( v7 ); // invoke-virtual {p3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* move v2, v4 */
/* :sswitch_8 */
v1 = (( java.lang.String ) p3 ).equals ( v8 ); // invoke-virtual {p3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* move v2, v3 */
} // :goto_4
/* move v2, v6 */
} // :goto_5
/* packed-switch v2, :pswitch_data_3 */
/* .line 615 */
/* :pswitch_a */
/* const-string/jumbo v0, "speaker_spatilizer_270_volume_high" */
/* .line 612 */
/* :pswitch_b */
/* const-string/jumbo v0, "speaker_spatilizer_90_volume_high" */
/* .line 613 */
/* .line 609 */
/* :pswitch_c */
/* const-string/jumbo v0, "speaker_spatilizer_90_volume_high" */
/* .line 610 */
/* nop */
/* .line 618 */
} // :goto_6
/* .line 594 */
/* :pswitch_d */
v1 = (( java.lang.String ) p3 ).hashCode ( ); // invoke-virtual {p3}, Ljava/lang/String;->hashCode()I
/* sparse-switch v1, :sswitch_data_3 */
} // :cond_3
/* :sswitch_9 */
v1 = (( java.lang.String ) p3 ).equals ( v5 ); // invoke-virtual {p3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* :sswitch_a */
v1 = (( java.lang.String ) p3 ).equals ( v7 ); // invoke-virtual {p3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* move v2, v4 */
/* :sswitch_b */
v1 = (( java.lang.String ) p3 ).equals ( v8 ); // invoke-virtual {p3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* move v2, v3 */
} // :goto_7
/* move v2, v6 */
} // :goto_8
/* packed-switch v2, :pswitch_data_4 */
/* .line 602 */
/* :pswitch_e */
/* const-string/jumbo v0, "speaker_spatilizer_270" */
/* .line 599 */
/* :pswitch_f */
/* const-string/jumbo v0, "speaker_spatilizer_90" */
/* .line 600 */
/* .line 596 */
/* :pswitch_10 */
/* const-string/jumbo v0, "speaker_spatilizer_90" */
/* .line 597 */
/* nop */
/* .line 605 */
} // :goto_9
/* nop */
/* .line 633 */
} // :goto_a
/* nop */
/* .line 646 */
} // :goto_b
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_4 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x12734 -> :sswitch_2 */
/* 0x21d5a2 -> :sswitch_1 */
/* 0x10263a7c -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_1 */
/* .packed-switch 0x0 */
/* :pswitch_d */
/* :pswitch_9 */
/* :pswitch_5 */
} // .end packed-switch
/* :sswitch_data_1 */
/* .sparse-switch */
/* 0x717 -> :sswitch_5 */
/* 0xc28b -> :sswitch_4 */
/* 0x10263a7c -> :sswitch_3 */
} // .end sparse-switch
/* :pswitch_data_2 */
/* .packed-switch 0x0 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
} // .end packed-switch
/* :sswitch_data_2 */
/* .sparse-switch */
/* 0x717 -> :sswitch_8 */
/* 0xc28b -> :sswitch_7 */
/* 0x10263a7c -> :sswitch_6 */
} // .end sparse-switch
/* :pswitch_data_3 */
/* .packed-switch 0x0 */
/* :pswitch_c */
/* :pswitch_b */
/* :pswitch_a */
} // .end packed-switch
/* :sswitch_data_3 */
/* .sparse-switch */
/* 0x717 -> :sswitch_b */
/* 0xc28b -> :sswitch_a */
/* 0x10263a7c -> :sswitch_9 */
} // .end sparse-switch
/* :pswitch_data_4 */
/* .packed-switch 0x0 */
/* :pswitch_10 */
/* :pswitch_f */
/* :pswitch_e */
} // .end packed-switch
} // .end method
public void btStateChangedFromDeviceBroker ( android.os.Bundle p0 ) {
/* .locals 2 */
/* .param p1, "btinfo" # Landroid/os/Bundle; */
/* .line 240 */
v0 = this.mWorkHandler;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 241 */
android.os.Message .obtain ( );
/* .line 242 */
/* .local v0, "msg":Landroid/os/Message; */
int v1 = 3; // const/4 v1, 0x3
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 243 */
(( android.os.Message ) v0 ).setData ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
/* .line 244 */
v1 = this.mWorkHandler;
(( com.android.server.audio.dolbyeffect.DolbyEffectController$WorkHandler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 245 */
} // .end local v0 # "msg":Landroid/os/Message;
/* .line 246 */
} // :cond_0
final String v0 = "DolbyEffectController"; // const-string v0, "DolbyEffectController"
final String v1 = "mWorkHandler doesn\'t init"; // const-string v1, "mWorkHandler doesn\'t init"
android.util.Log .e ( v0,v1 );
/* .line 248 */
} // :goto_0
return;
} // .end method
public void init ( ) {
/* .locals 5 */
/* .line 100 */
/* const-string/jumbo v0, "true" */
final String v1 = "DolbyEffectController"; // const-string v1, "DolbyEffectController"
try { // :try_start_0
final String v2 = "DolbyEffectController init..."; // const-string v2, "DolbyEffectController init..."
android.util.Log .d ( v1,v2 );
/* .line 102 */
final String v2 = "ro.audio.spatializer_enabled"; // const-string v2, "ro.audio.spatializer_enabled"
android.os.SystemProperties .get ( v2 );
v2 = (( java.lang.String ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 103 */
v2 = this.mSpatializerEnabledAvaliable;
v3 = com.android.server.audio.dolbyeffect.DolbyEffectController .getService ( );
int v4 = 0; // const/4 v4, 0x0
/* aput-boolean v3, v2, v4 */
/* .line 104 */
v2 = this.mSpatializerEnabledAvaliable;
v3 = com.android.server.audio.dolbyeffect.DolbyEffectController .getService ( );
int v4 = 1; // const/4 v4, 0x1
/* aput-boolean v3, v2, v4 */
/* .line 107 */
} // :cond_0
/* const-string/jumbo v2, "vendor.audio.dolby.control.tunning.by.volume.support" */
android.os.SystemProperties .get ( v2 );
v0 = (( java.lang.String ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 108 */
com.android.server.audio.dolbyeffect.DolbyEffectController .getService ( );
v0 = int v2 = 3; // const/4 v2, 0x3
/* mul-int/lit8 v0, v0, 0xa */
/* div-int/lit8 v0, v0, 0xf */
/* iput v0, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mVolumeThreshold:I */
/* .line 109 */
com.android.server.audio.dolbyeffect.DolbyEffectController .getService ( );
v0 = int v3 = 2; // const/4 v3, 0x2
/* .line 110 */
/* .local v0, "index":I */
/* iget v2, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mVolumeThreshold:I */
/* if-le v0, v2, :cond_1 */
/* .line 111 */
final String v2 = "HIGH"; // const-string v2, "HIGH"
this.mCurrentVolumeLevel = v2;
/* .line 113 */
} // :cond_1
final String v2 = "LOW"; // const-string v2, "LOW"
this.mCurrentVolumeLevel = v2;
/* .line 117 */
} // .end local v0 # "index":I
} // :cond_2
} // :goto_0
/* new-instance v0, Landroid/os/HandlerThread; */
final String v2 = "DolbyEffect"; // const-string v2, "DolbyEffect"
/* invoke-direct {v0, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mHandlerThread = v0;
/* .line 118 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 119 */
/* new-instance v0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler; */
v2 = this.mHandlerThread;
(( android.os.HandlerThread ) v2 ).getLooper ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v2}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;-><init>(Lcom/android/server/audio/dolbyeffect/DolbyEffectController;Landroid/os/Looper;)V */
this.mWorkHandler = v0;
/* .line 121 */
/* const-string/jumbo v0, "updateDolbyTunning from init()" */
android.util.Log .d ( v1,v0 );
/* .line 122 */
(( com.android.server.audio.dolbyeffect.DolbyEffectController ) p0 ).updateDolbyTunning ( ); // invoke-virtual {p0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->updateDolbyTunning()V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 125 */
/* .line 123 */
/* :catch_0 */
/* move-exception v0 */
/* .line 124 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "init: Exception "; // const-string v3, "init: Exception "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 126 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
public void receiveDeviceConnectStateChanged ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 172 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 173 */
/* .local v0, "action":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 174 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "onReceive: "; // const-string v2, "onReceive: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "DolbyEffectController"; // const-string v2, "DolbyEffectController"
android.util.Log .d ( v2,v1 );
/* .line 176 */
v1 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
int v2 = 1; // const/4 v2, 0x1
int v3 = 2; // const/4 v3, 0x2
/* sparse-switch v1, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
final String v1 = "android.bluetooth.device.action.BOND_STATE_CHANGED"; // const-string v1, "android.bluetooth.device.action.BOND_STATE_CHANGED"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* move v1, v3 */
/* :sswitch_1 */
final String v1 = "android.bluetooth.headset.action.VENDOR_SPECIFIC_HEADSET_EVENT"; // const-string v1, "android.bluetooth.headset.action.VENDOR_SPECIFIC_HEADSET_EVENT"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* move v1, v2 */
/* :sswitch_2 */
final String v1 = "android.intent.action.HEADSET_PLUG"; // const-string v1, "android.intent.action.HEADSET_PLUG"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
int v1 = -1; // const/4 v1, -0x1
} // :goto_1
/* packed-switch v1, :pswitch_data_0 */
/* .line 185 */
/* :pswitch_0 */
android.os.Message .obtain ( );
/* .line 186 */
/* .local v1, "msg":Landroid/os/Message; */
/* iput v2, v1, Landroid/os/Message;->what:I */
/* .line 187 */
this.obj = p2;
/* .line 188 */
v2 = this.mWorkHandler;
(( com.android.server.audio.dolbyeffect.DolbyEffectController$WorkHandler ) v2 ).sendMessage ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 189 */
/* .line 178 */
} // .end local v1 # "msg":Landroid/os/Message;
/* :pswitch_1 */
android.os.Message .obtain ( );
/* .line 179 */
/* .restart local v1 # "msg":Landroid/os/Message; */
/* iput v3, v1, Landroid/os/Message;->what:I */
/* .line 180 */
this.obj = p2;
/* .line 181 */
v2 = this.mWorkHandler;
(( com.android.server.audio.dolbyeffect.DolbyEffectController$WorkHandler ) v2 ).sendMessage ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 182 */
/* nop */
/* .line 194 */
} // .end local v1 # "msg":Landroid/os/Message;
} // :cond_1
} // :goto_2
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x63ecb970 -> :sswitch_2 */
/* 0x69ab72ba -> :sswitch_1 */
/* 0x7e2cc189 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void receiveRotationChanged ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "rotationChanged" # I */
/* .line 139 */
v0 = this.mWorkHandler;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 140 */
android.os.Message .obtain ( );
/* .line 141 */
/* .local v0, "msg":Landroid/os/Message; */
int v1 = 4; // const/4 v1, 0x4
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 142 */
java.lang.Integer .valueOf ( p1 );
this.obj = v1;
/* .line 143 */
v1 = this.mWorkHandler;
(( com.android.server.audio.dolbyeffect.DolbyEffectController$WorkHandler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 144 */
} // .end local v0 # "msg":Landroid/os/Message;
/* .line 145 */
} // :cond_0
final String v0 = "DolbyEffectController"; // const-string v0, "DolbyEffectController"
final String v1 = "mWorkHandler doesn\'t init"; // const-string v1, "mWorkHandler doesn\'t init"
android.util.Log .e ( v0,v1 );
/* .line 147 */
} // :goto_0
return;
} // .end method
public void receiveSpatializerAvailableChanged ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "available" # Z */
/* .line 161 */
v0 = this.mWorkHandler;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 162 */
android.os.Message .obtain ( );
/* .line 163 */
/* .local v0, "msg":Landroid/os/Message; */
int v1 = 6; // const/4 v1, 0x6
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 164 */
java.lang.Boolean .valueOf ( p1 );
this.obj = v1;
/* .line 165 */
v1 = this.mWorkHandler;
(( com.android.server.audio.dolbyeffect.DolbyEffectController$WorkHandler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 166 */
} // .end local v0 # "msg":Landroid/os/Message;
/* .line 167 */
} // :cond_0
final String v0 = "DolbyEffectController"; // const-string v0, "DolbyEffectController"
final String v1 = "mWorkHandler doesn\'t init"; // const-string v1, "mWorkHandler doesn\'t init"
android.util.Log .e ( v0,v1 );
/* .line 169 */
} // :goto_0
return;
} // .end method
public void receiveSpatializerEnabledChanged ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enabled" # Z */
/* .line 150 */
v0 = this.mWorkHandler;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 151 */
android.os.Message .obtain ( );
/* .line 152 */
/* .local v0, "msg":Landroid/os/Message; */
int v1 = 5; // const/4 v1, 0x5
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 153 */
java.lang.Boolean .valueOf ( p1 );
this.obj = v1;
/* .line 154 */
v1 = this.mWorkHandler;
(( com.android.server.audio.dolbyeffect.DolbyEffectController$WorkHandler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 155 */
} // .end local v0 # "msg":Landroid/os/Message;
/* .line 156 */
} // :cond_0
final String v0 = "DolbyEffectController"; // const-string v0, "DolbyEffectController"
final String v1 = "mWorkHandler doesn\'t init"; // const-string v1, "mWorkHandler doesn\'t init"
android.util.Log .e ( v0,v1 );
/* .line 158 */
} // :goto_0
return;
} // .end method
public void receiveVolumeChanged ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "newVolume" # I */
/* .line 129 */
v0 = this.mWorkHandler;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 130 */
android.os.Message .obtain ( );
/* .line 131 */
/* .local v0, "msg":Landroid/os/Message; */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 132 */
java.lang.Integer .valueOf ( p1 );
this.obj = v1;
/* .line 133 */
v1 = this.mWorkHandler;
(( com.android.server.audio.dolbyeffect.DolbyEffectController$WorkHandler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$WorkHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 134 */
} // .end local v0 # "msg":Landroid/os/Message;
/* .line 135 */
} // :cond_0
final String v0 = "DolbyEffectController"; // const-string v0, "DolbyEffectController"
final String v1 = "mWorkHandler doesn\'t init"; // const-string v1, "mWorkHandler doesn\'t init"
android.util.Log .e ( v0,v1 );
/* .line 137 */
} // :goto_0
return;
} // .end method
public void updateDolbyTunning ( ) {
/* .locals 5 */
/* .line 670 */
final String v0 = "DolbyEffectController"; // const-string v0, "DolbyEffectController"
try { // :try_start_0
/* new-instance v1, Lcom/dolby/dax/DolbyAudioEffect; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v1, v2, v2}, Lcom/dolby/dax/DolbyAudioEffect;-><init>(II)V */
this.mDolbyAudioEffect = v1;
/* .line 671 */
final String v1 = "persist.vendor.audio.dolbysurround.enable"; // const-string v1, "persist.vendor.audio.dolbysurround.enable"
v1 = android.os.SystemProperties .getBoolean ( v1,v2 );
/* iput-boolean v1, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mAdspSpatializerEnable:Z */
/* .line 673 */
v1 = this.mDolbyAudioEffect;
v1 = (( com.dolby.dax.DolbyAudioEffect ) v1 ).hasControl ( ); // invoke-virtual {v1}, Lcom/dolby/dax/DolbyAudioEffect;->hasControl()Z
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 675 */
v1 = this.mSpatializerEnabledAvaliable;
/* aget-boolean v2, v1, v2 */
if ( v2 != null) { // if-eqz v2, :cond_0
int v2 = 1; // const/4 v2, 0x1
/* aget-boolean v1, v1, v2 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 676 */
final String v1 = "The dolby tuning selected for Spatializer, begin..."; // const-string v1, "The dolby tuning selected for Spatializer, begin..."
android.util.Log .d ( v0,v1 );
/* .line 677 */
/* iget v1, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I */
v2 = this.mCurrentVolumeLevel;
v3 = this.mCurrentRotation;
(( com.android.server.audio.dolbyeffect.DolbyEffectController ) p0 ).acquireDolbyDeviceIdForSpatializer ( v1, v2, v3 ); // invoke-virtual {p0, v1, v2, v3}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->acquireDolbyDeviceIdForSpatializer(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
/* .local v1, "newDeviceId":Ljava/lang/String; */
/* .line 678 */
} // .end local v1 # "newDeviceId":Ljava/lang/String;
} // :cond_0
/* iget-boolean v1, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mAdspSpatializerAvailable:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* iget-boolean v1, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mAdspSpatializerEnable:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 679 */
final String v1 = "[adsp spatial] The dolby tuning selected for adsp Spatializer, begin..."; // const-string v1, "[adsp spatial] The dolby tuning selected for adsp Spatializer, begin..."
android.util.Log .d ( v0,v1 );
/* .line 681 */
/* iget v1, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I */
(( com.android.server.audio.dolbyeffect.DolbyEffectController ) p0 ).acquireDolbyDeviceIdForAdspSpatializer ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->acquireDolbyDeviceIdForAdspSpatializer(I)Ljava/lang/String;
/* .restart local v1 # "newDeviceId":Ljava/lang/String; */
/* .line 683 */
} // .end local v1 # "newDeviceId":Ljava/lang/String;
} // :cond_1
final String v1 = "The dolby tuning selected for nonSpatializer, begin..."; // const-string v1, "The dolby tuning selected for nonSpatializer, begin..."
android.util.Log .d ( v0,v1 );
/* .line 684 */
/* iget v1, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I */
v2 = this.mCurrentVolumeLevel;
v3 = this.mCurrentRotation;
(( com.android.server.audio.dolbyeffect.DolbyEffectController ) p0 ).acquireDolbyDeviceIdForNonSpatializer ( v1, v2, v3 ); // invoke-virtual {p0, v1, v2, v3}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->acquireDolbyDeviceIdForNonSpatializer(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
/* .line 687 */
/* .restart local v1 # "newDeviceId":Ljava/lang/String; */
} // :goto_0
/* iget v2, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I */
v3 = this.mCurrentDolbyTunning;
/* iget v3, v3, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$DolbyTunning;->mPort:I */
/* if-ne v2, v3, :cond_3 */
v2 = this.mCurrentDolbyTunning;
v2 = this.mDeviceId;
v2 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 688 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "The selected dolby tuning is same as current [mCurrentPort ="; // const-string v3, "The selected dolby tuning is same as current [mCurrentPort ="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = "newDeviceId = "; // const-string v3, "newDeviceId = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "],bypass updateDolbyTunning"; // const-string v3, "],bypass updateDolbyTunning"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/RuntimeException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 707 */
v0 = this.mDolbyAudioEffect;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 708 */
(( com.dolby.dax.DolbyAudioEffect ) v0 ).release ( ); // invoke-virtual {v0}, Lcom/dolby/dax/DolbyAudioEffect;->release()V
/* .line 689 */
} // :cond_2
return;
/* .line 692 */
} // :cond_3
try { // :try_start_1
v2 = this.mCurrentDolbyTunning;
/* iget v3, p0, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->mCurrentPort:I */
/* iput v3, v2, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$DolbyTunning;->mPort:I */
/* .line 693 */
v2 = this.mCurrentDolbyTunning;
this.mDeviceId = v1;
/* .line 695 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "updateDolbyTunning, Port = " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mCurrentDolbyTunning;
/* iget v3, v3, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$DolbyTunning;->mPort:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", DeviceId = "; // const-string v3, ", DeviceId = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mCurrentDolbyTunning;
v3 = this.mDeviceId;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v2 );
/* .line 696 */
v2 = this.mDolbyAudioEffect;
v3 = this.mCurrentDolbyTunning;
/* iget v3, v3, Lcom/android/server/audio/dolbyeffect/DolbyEffectController$DolbyTunning;->mPort:I */
v4 = this.mCurrentDolbyTunning;
v4 = this.mDeviceId;
(( com.dolby.dax.DolbyAudioEffect ) v2 ).setSelectedTuningDevice ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lcom/dolby/dax/DolbyAudioEffect;->setSelectedTuningDevice(ILjava/lang/String;)V
/* .line 697 */
} // .end local v1 # "newDeviceId":Ljava/lang/String;
/* .line 698 */
} // :cond_4
/* const-string/jumbo v1, "setSelectedTuningDevice do not hasControl" */
android.util.Log .d ( v0,v1 );
/* :try_end_1 */
/* .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 ..:try_end_1} :catch_2 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catch Ljava/lang/RuntimeException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 707 */
} // :goto_1
v0 = this.mDolbyAudioEffect;
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 708 */
/* .line 707 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 704 */
/* :catch_0 */
/* move-exception v1 */
/* .line 705 */
/* .local v1, "e":Ljava/lang/RuntimeException; */
try { // :try_start_2
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "updateDolbyTunning: RuntimeException" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v2 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 707 */
/* nop */
} // .end local v1 # "e":Ljava/lang/RuntimeException;
v0 = this.mDolbyAudioEffect;
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 708 */
} // :goto_2
/* .line 702 */
/* :catch_1 */
/* move-exception v1 */
/* .line 703 */
/* .local v1, "e":Ljava/lang/IllegalArgumentException; */
try { // :try_start_3
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "updateDolbyTunning: IllegalArgumentException" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v2 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 707 */
/* nop */
} // .end local v1 # "e":Ljava/lang/IllegalArgumentException;
v0 = this.mDolbyAudioEffect;
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 708 */
/* .line 700 */
/* :catch_2 */
/* move-exception v1 */
/* .line 701 */
/* .local v1, "e":Ljava/lang/UnsupportedOperationException; */
try { // :try_start_4
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "updateDolbyTunning: UnsupportedOperationException" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v2 );
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 707 */
/* nop */
} // .end local v1 # "e":Ljava/lang/UnsupportedOperationException;
v0 = this.mDolbyAudioEffect;
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 708 */
} // :goto_3
(( com.dolby.dax.DolbyAudioEffect ) v0 ).release ( ); // invoke-virtual {v0}, Lcom/dolby/dax/DolbyAudioEffect;->release()V
/* .line 712 */
} // :cond_5
return;
/* .line 707 */
} // :goto_4
v1 = this.mDolbyAudioEffect;
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 708 */
(( com.dolby.dax.DolbyAudioEffect ) v1 ).release ( ); // invoke-virtual {v1}, Lcom/dolby/dax/DolbyAudioEffect;->release()V
/* .line 710 */
} // :cond_6
/* throw v0 */
} // .end method
