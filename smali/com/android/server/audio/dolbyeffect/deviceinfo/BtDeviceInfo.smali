.class public Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;
.super Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;
.source "BtDeviceInfo.java"


# static fields
.field private static final ATSETTING_VERSION:I = 0x0

.field private static final AT_SETTING_PRE_SUFFIX:Ljava/lang/String; = "FF"

.field private static final HEADSET_AT_COMMAND_TAG:Ljava/lang/String; = "01020101"


# instance fields
.field mMajorID:I

.field mMinorID:I

.field mState:Z


# direct methods
.method public constructor <init>(IIILjava/lang/String;Z)V
    .locals 0
    .param p1, "deviceType"    # I
    .param p2, "majorID"    # I
    .param p3, "minorID"    # I
    .param p4, "device"    # Ljava/lang/String;
    .param p5, "state"    # Z

    .line 43
    invoke-direct {p0}, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;-><init>()V

    .line 44
    iput p2, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->mMajorID:I

    .line 45
    iput p3, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->mMinorID:I

    .line 46
    iput-object p4, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->mDevice:Ljava/lang/String;

    .line 47
    iput p1, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->mDeviceType:I

    .line 48
    iput-boolean p5, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->mState:Z

    .line 49
    return-void
.end method

.method public static hexToByteArray(Ljava/lang/String;)[B
    .locals 6
    .param p0, "digits"    # Ljava/lang/String;

    .line 109
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 114
    .local v0, "bytes":I
    new-array v1, v0, [B

    .line 116
    .local v1, "result":[B
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 117
    div-int/lit8 v3, v2, 0x2

    add-int/lit8 v4, v2, 0x2

    invoke-virtual {p0, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x10

    invoke-static {v4, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    int-to-byte v4, v4

    aput-byte v4, v1, v3
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    add-int/lit8 v2, v2, 0x2

    goto :goto_0

    .line 122
    .end local v2    # "i":I
    :cond_0
    goto :goto_1

    .line 120
    :catch_0
    move-exception v2

    .line 121
    .local v2, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v2}, Ljava/lang/NumberFormatException;->printStackTrace()V

    .line 123
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :goto_1
    return-object v1
.end method

.method public static tryGetIdsFromIntent(Landroid/content/Intent;[I[Ljava/lang/String;)Z
    .locals 16
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "ids"    # [I
    .param p2, "deviceName"    # [Ljava/lang/String;

    .line 53
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    array-length v3, v1

    const/4 v4, 0x0

    const/4 v5, 0x2

    if-ne v3, v5, :cond_d

    array-length v3, v2

    const/4 v6, 0x1

    if-eq v3, v6, :cond_0

    goto/16 :goto_6

    .line 57
    :cond_0
    const-string v3, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/bluetooth/BluetoothDevice;

    .line 58
    .local v3, "extraDevice":Landroid/bluetooth/BluetoothDevice;
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    :cond_1
    const-string v7, "NULL"

    :goto_0
    aput-object v7, v2, v4

    .line 59
    const-string v7, "android.bluetooth.headset.extra.VENDOR_SPECIFIC_HEADSET_EVENT_ARGS"

    invoke-virtual {v0, v7}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/Object;

    .line 60
    .local v7, "args":[Ljava/lang/Object;
    if-eqz v7, :cond_c

    array-length v8, v7

    if-eq v8, v6, :cond_2

    goto/16 :goto_5

    .line 63
    :cond_2
    const/4 v8, 0x0

    .line 64
    .local v8, "majorId":I
    const/4 v9, 0x0

    .line 65
    .local v9, "minorId":I
    aget-object v10, v7, v4

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    .line 66
    .local v10, "local":Ljava/lang/String;
    const-string v11, "FF"

    invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_b

    invoke-virtual {v10, v11}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_b

    .line 67
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v11

    const/4 v12, 0x4

    if-ge v11, v12, :cond_3

    goto/16 :goto_4

    .line 71
    :cond_3
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v11

    sub-int/2addr v11, v5

    invoke-virtual {v10, v5, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    .line 72
    const-string v11, "01020101"

    invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_4

    .line 73
    return v4

    .line 75
    :cond_4
    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 76
    invoke-static {v10}, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->hexToByteArray(Ljava/lang/String;)[B

    move-result-object v11

    .line 77
    .local v11, "argbyte":[B
    aget-byte v13, v11, v6

    if-nez v13, :cond_a

    aget-byte v13, v11, v4

    add-int/2addr v13, v6

    mul-int/2addr v13, v5

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v14

    if-eq v13, v14, :cond_5

    goto :goto_3

    .line 81
    :cond_5
    invoke-virtual {v10, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 82
    invoke-static {v10}, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->hexToByteArray(Ljava/lang/String;)[B

    move-result-object v11

    .line 83
    const/4 v13, 0x2

    .line 84
    .local v13, "position":I
    :goto_1
    array-length v14, v11

    sub-int/2addr v14, v6

    if-gt v13, v14, :cond_8

    .line 85
    add-int/lit8 v14, v13, -0x2

    aget-byte v14, v11, v14

    .line 86
    .local v14, "lengthFlag":I
    add-int v15, v13, v14

    sub-int/2addr v15, v5

    array-length v12, v11

    sub-int/2addr v12, v6

    if-le v15, v12, :cond_6

    .line 88
    return v4

    .line 90
    :cond_6
    add-int/lit8 v12, v13, -0x1

    aget-byte v12, v11, v12

    .line 91
    .local v12, "tag":I
    new-array v15, v14, [B

    .line 92
    .local v15, "arr":[B
    add-int/lit8 v6, v14, -0x1

    invoke-static {v11, v13, v15, v4, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 93
    const/16 v6, 0x20

    if-ne v12, v6, :cond_7

    .line 94
    const/4 v6, 0x3

    aget-byte v6, v15, v6

    aget-byte v4, v15, v5

    mul-int/lit16 v4, v4, 0x100

    add-int/2addr v6, v4

    const/4 v4, 0x1

    aget-byte v5, v15, v4

    mul-int/lit16 v5, v5, 0x100

    mul-int/lit16 v5, v5, 0x100

    add-int/2addr v6, v5

    .line 95
    .end local v8    # "majorId":I
    .local v6, "majorId":I
    const/4 v4, 0x5

    aget-byte v4, v15, v4

    mul-int/lit16 v4, v4, 0x100

    const/4 v5, 0x4

    aget-byte v8, v15, v5

    add-int/2addr v4, v8

    move v9, v4

    move v8, v6

    .end local v9    # "minorId":I
    .local v4, "minorId":I
    goto :goto_2

    .line 93
    .end local v4    # "minorId":I
    .end local v6    # "majorId":I
    .restart local v8    # "majorId":I
    .restart local v9    # "minorId":I
    :cond_7
    const/4 v5, 0x4

    .line 97
    :goto_2
    add-int/lit8 v4, v14, 0x1

    add-int/2addr v13, v4

    .line 98
    .end local v12    # "tag":I
    .end local v14    # "lengthFlag":I
    .end local v15    # "arr":[B
    move v12, v5

    const/4 v4, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x1

    goto :goto_1

    .line 99
    :cond_8
    if-nez v8, :cond_9

    if-nez v9, :cond_9

    .line 100
    const/4 v4, 0x0

    return v4

    .line 99
    :cond_9
    const/4 v4, 0x0

    .line 102
    aput v8, v1, v4

    .line 103
    const/4 v4, 0x1

    aput v9, v1, v4

    .line 105
    return v4

    .line 79
    .end local v13    # "position":I
    :cond_a
    :goto_3
    return v4

    .line 69
    .end local v11    # "argbyte":[B
    :cond_b
    :goto_4
    return v4

    .line 61
    .end local v8    # "majorId":I
    .end local v9    # "minorId":I
    .end local v10    # "local":Ljava/lang/String;
    :cond_c
    :goto_5
    return v4

    .line 54
    .end local v3    # "extraDevice":Landroid/bluetooth/BluetoothDevice;
    .end local v7    # "args":[Ljava/lang/Object;
    :cond_d
    :goto_6
    return v4
.end method


# virtual methods
.method public getMajorID()I
    .locals 1

    .line 16
    iget v0, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->mMajorID:I

    return v0
.end method

.method public getMinorID()I
    .locals 1

    .line 24
    iget v0, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->mMinorID:I

    return v0
.end method

.method public getState()Z
    .locals 1

    .line 32
    iget-boolean v0, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->mState:Z

    return v0
.end method

.method public setMajorID(I)V
    .locals 0
    .param p1, "MajorID"    # I

    .line 20
    iput p1, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->mMajorID:I

    .line 21
    return-void
.end method

.method public setMinorID(I)V
    .locals 0
    .param p1, "MinorID"    # I

    .line 28
    iput p1, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->mMinorID:I

    .line 29
    return-void
.end method

.method public setState(Z)V
    .locals 0
    .param p1, "State"    # Z

    .line 36
    iput-boolean p1, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->mState:Z

    .line 37
    return-void
.end method
