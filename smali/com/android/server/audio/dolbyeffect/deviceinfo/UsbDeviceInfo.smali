.class public Lcom/android/server/audio/dolbyeffect/deviceinfo/UsbDeviceInfo;
.super Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;
.source "UsbDeviceInfo.java"


# instance fields
.field mProductID:I

.field mVendorID:I


# direct methods
.method public constructor <init>(III)V
    .locals 1
    .param p1, "mDeviceType"    # I
    .param p2, "mVendorID"    # I
    .param p3, "mProductID"    # I

    .line 23
    invoke-direct {p0}, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;-><init>()V

    .line 24
    iput p2, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/UsbDeviceInfo;->mVendorID:I

    .line 25
    iput p3, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/UsbDeviceInfo;->mProductID:I

    .line 26
    iput p1, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/UsbDeviceInfo;->mDeviceType:I

    .line 27
    const-string v0, "USB headset"

    iput-object v0, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/UsbDeviceInfo;->mDevice:Ljava/lang/String;

    .line 28
    return-void
.end method


# virtual methods
.method public getProductID()I
    .locals 1

    .line 13
    iget v0, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/UsbDeviceInfo;->mProductID:I

    return v0
.end method

.method public getVendorID()I
    .locals 1

    .line 5
    iget v0, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/UsbDeviceInfo;->mVendorID:I

    return v0
.end method

.method public setProductID(I)V
    .locals 0
    .param p1, "ProductID"    # I

    .line 17
    iput p1, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/UsbDeviceInfo;->mProductID:I

    .line 18
    return-void
.end method

.method public setVendorID(I)V
    .locals 0
    .param p1, "VendorID"    # I

    .line 9
    iput p1, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/UsbDeviceInfo;->mVendorID:I

    .line 10
    return-void
.end method
