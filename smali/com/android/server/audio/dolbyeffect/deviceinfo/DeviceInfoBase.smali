.class public Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;
.super Ljava/lang/Object;
.source "DeviceInfoBase.java"


# static fields
.field public static TYPE_BT:I

.field public static TYPE_USB:I

.field public static TYPE_WIRED:I


# instance fields
.field mDevice:Ljava/lang/String;

.field mDeviceType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 4
    const/4 v0, 0x0

    sput v0, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->TYPE_USB:I

    .line 5
    const/4 v0, 0x1

    sput v0, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->TYPE_WIRED:I

    .line 6
    const/4 v0, 0x2

    sput v0, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->TYPE_BT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDevice()Ljava/lang/String;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->mDevice:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceType()I
    .locals 1

    .line 11
    iget v0, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->mDeviceType:I

    return v0
.end method

.method public setDevice(Ljava/lang/String;)V
    .locals 0
    .param p1, "Device"    # Ljava/lang/String;

    .line 23
    iput-object p1, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->mDevice:Ljava/lang/String;

    .line 24
    return-void
.end method

.method public setDeviceType(I)V
    .locals 0
    .param p1, "DeviceType"    # I

    .line 15
    iput p1, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->mDeviceType:I

    .line 16
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BaseDeviceProduct{mDeviceType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->mDeviceType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mDevice=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->mDevice:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
