public class com.android.server.audio.dolbyeffect.deviceinfo.DeviceInfoBase {
	 /* .source "DeviceInfoBase.java" */
	 /* # static fields */
	 public static Integer TYPE_BT;
	 public static Integer TYPE_USB;
	 public static Integer TYPE_WIRED;
	 /* # instance fields */
	 java.lang.String mDevice;
	 Integer mDeviceType;
	 /* # direct methods */
	 static com.android.server.audio.dolbyeffect.deviceinfo.DeviceInfoBase ( ) {
		 /* .locals 1 */
		 /* .line 4 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* .line 5 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* .line 6 */
		 int v0 = 2; // const/4 v0, 0x2
		 return;
	 } // .end method
	 public com.android.server.audio.dolbyeffect.deviceinfo.DeviceInfoBase ( ) {
		 /* .locals 0 */
		 /* .line 3 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public java.lang.String getDevice ( ) {
		 /* .locals 1 */
		 /* .line 19 */
		 v0 = this.mDevice;
	 } // .end method
	 public Integer getDeviceType ( ) {
		 /* .locals 1 */
		 /* .line 11 */
		 /* iget v0, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->mDeviceType:I */
	 } // .end method
	 public void setDevice ( java.lang.String p0 ) {
		 /* .locals 0 */
		 /* .param p1, "Device" # Ljava/lang/String; */
		 /* .line 23 */
		 this.mDevice = p1;
		 /* .line 24 */
		 return;
	 } // .end method
	 public void setDeviceType ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "DeviceType" # I */
		 /* .line 15 */
		 /* iput p1, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->mDeviceType:I */
		 /* .line 16 */
		 return;
	 } // .end method
	 public java.lang.String toString ( ) {
		 /* .locals 2 */
		 /* .line 28 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v1 = "BaseDeviceProduct{mDeviceType="; // const-string v1, "BaseDeviceProduct{mDeviceType="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v1, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;->mDeviceType:I */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v1 = ", mDevice=\'"; // const-string v1, ", mDevice=\'"
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v1 = this.mDevice;
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* const/16 v1, 0x27 */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
		 /* const/16 v1, 0x7d */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 } // .end method
