public class com.android.server.audio.dolbyeffect.deviceinfo.BtDeviceInfo extends com.android.server.audio.dolbyeffect.deviceinfo.DeviceInfoBase {
	 /* .source "BtDeviceInfo.java" */
	 /* # static fields */
	 private static final Integer ATSETTING_VERSION;
	 private static final java.lang.String AT_SETTING_PRE_SUFFIX;
	 private static final java.lang.String HEADSET_AT_COMMAND_TAG;
	 /* # instance fields */
	 Integer mMajorID;
	 Integer mMinorID;
	 Boolean mState;
	 /* # direct methods */
	 public com.android.server.audio.dolbyeffect.deviceinfo.BtDeviceInfo ( ) {
		 /* .locals 0 */
		 /* .param p1, "deviceType" # I */
		 /* .param p2, "majorID" # I */
		 /* .param p3, "minorID" # I */
		 /* .param p4, "device" # Ljava/lang/String; */
		 /* .param p5, "state" # Z */
		 /* .line 43 */
		 /* invoke-direct {p0}, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;-><init>()V */
		 /* .line 44 */
		 /* iput p2, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->mMajorID:I */
		 /* .line 45 */
		 /* iput p3, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->mMinorID:I */
		 /* .line 46 */
		 this.mDevice = p4;
		 /* .line 47 */
		 /* iput p1, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->mDeviceType:I */
		 /* .line 48 */
		 /* iput-boolean p5, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->mState:Z */
		 /* .line 49 */
		 return;
	 } // .end method
	 public static hexToByteArray ( java.lang.String p0 ) {
		 /* .locals 6 */
		 /* .param p0, "digits" # Ljava/lang/String; */
		 /* .line 109 */
		 v0 = 		 (( java.lang.String ) p0 ).length ( ); // invoke-virtual {p0}, Ljava/lang/String;->length()I
		 /* div-int/lit8 v0, v0, 0x2 */
		 /* .line 114 */
		 /* .local v0, "bytes":I */
		 /* new-array v1, v0, [B */
		 /* .line 116 */
		 /* .local v1, "result":[B */
		 int v2 = 0; // const/4 v2, 0x0
		 /* .local v2, "i":I */
	 } // :goto_0
	 try { // :try_start_0
		 v3 = 		 (( java.lang.String ) p0 ).length ( ); // invoke-virtual {p0}, Ljava/lang/String;->length()I
		 /* if-ge v2, v3, :cond_0 */
		 /* .line 117 */
		 /* div-int/lit8 v3, v2, 0x2 */
		 /* add-int/lit8 v4, v2, 0x2 */
		 (( java.lang.String ) p0 ).substring ( v2, v4 ); // invoke-virtual {p0, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;
		 /* const/16 v5, 0x10 */
		 v4 = 		 java.lang.Integer .parseInt ( v4,v5 );
		 /* int-to-byte v4, v4 */
		 /* aput-byte v4, v1, v3 */
		 /* :try_end_0 */
		 /* .catch Ljava/lang/NumberFormatException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 116 */
		 /* add-int/lit8 v2, v2, 0x2 */
		 /* .line 122 */
	 } // .end local v2 # "i":I
} // :cond_0
/* .line 120 */
/* :catch_0 */
/* move-exception v2 */
/* .line 121 */
/* .local v2, "e":Ljava/lang/NumberFormatException; */
(( java.lang.NumberFormatException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/NumberFormatException;->printStackTrace()V
/* .line 123 */
} // .end local v2 # "e":Ljava/lang/NumberFormatException;
} // :goto_1
} // .end method
public static Boolean tryGetIdsFromIntent ( android.content.Intent p0, Integer[] p1, java.lang.String[] p2 ) {
/* .locals 16 */
/* .param p0, "intent" # Landroid/content/Intent; */
/* .param p1, "ids" # [I */
/* .param p2, "deviceName" # [Ljava/lang/String; */
/* .line 53 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
/* move-object/from16 v2, p2 */
/* array-length v3, v1 */
int v4 = 0; // const/4 v4, 0x0
int v5 = 2; // const/4 v5, 0x2
/* if-ne v3, v5, :cond_d */
/* array-length v3, v2 */
int v6 = 1; // const/4 v6, 0x1
/* if-eq v3, v6, :cond_0 */
/* goto/16 :goto_6 */
/* .line 57 */
} // :cond_0
final String v3 = "android.bluetooth.device.extra.DEVICE"; // const-string v3, "android.bluetooth.device.extra.DEVICE"
(( android.content.Intent ) v0 ).getParcelableExtra ( v3 ); // invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;
/* check-cast v3, Landroid/bluetooth/BluetoothDevice; */
/* .line 58 */
/* .local v3, "extraDevice":Landroid/bluetooth/BluetoothDevice; */
if ( v3 != null) { // if-eqz v3, :cond_1
(( android.bluetooth.BluetoothDevice ) v3 ).getAddress ( ); // invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;
} // :cond_1
final String v7 = "NULL"; // const-string v7, "NULL"
} // :goto_0
/* aput-object v7, v2, v4 */
/* .line 59 */
final String v7 = "android.bluetooth.headset.extra.VENDOR_SPECIFIC_HEADSET_EVENT_ARGS"; // const-string v7, "android.bluetooth.headset.extra.VENDOR_SPECIFIC_HEADSET_EVENT_ARGS"
(( android.content.Intent ) v0 ).getExtra ( v7 ); // invoke-virtual {v0, v7}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v7, [Ljava/lang/Object; */
/* .line 60 */
/* .local v7, "args":[Ljava/lang/Object; */
if ( v7 != null) { // if-eqz v7, :cond_c
/* array-length v8, v7 */
/* if-eq v8, v6, :cond_2 */
/* goto/16 :goto_5 */
/* .line 63 */
} // :cond_2
int v8 = 0; // const/4 v8, 0x0
/* .line 64 */
/* .local v8, "majorId":I */
int v9 = 0; // const/4 v9, 0x0
/* .line 65 */
/* .local v9, "minorId":I */
/* aget-object v10, v7, v4 */
/* check-cast v10, Ljava/lang/String; */
(( java.lang.String ) v10 ).toUpperCase ( ); // invoke-virtual {v10}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
/* .line 66 */
/* .local v10, "local":Ljava/lang/String; */
final String v11 = "FF"; // const-string v11, "FF"
v12 = (( java.lang.String ) v10 ).startsWith ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v12 != null) { // if-eqz v12, :cond_b
v11 = (( java.lang.String ) v10 ).endsWith ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z
if ( v11 != null) { // if-eqz v11, :cond_b
/* .line 67 */
v11 = (( java.lang.String ) v10 ).length ( ); // invoke-virtual {v10}, Ljava/lang/String;->length()I
int v12 = 4; // const/4 v12, 0x4
/* if-ge v11, v12, :cond_3 */
/* goto/16 :goto_4 */
/* .line 71 */
} // :cond_3
v11 = (( java.lang.String ) v10 ).length ( ); // invoke-virtual {v10}, Ljava/lang/String;->length()I
/* sub-int/2addr v11, v5 */
(( java.lang.String ) v10 ).substring ( v5, v11 ); // invoke-virtual {v10, v5, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* .line 72 */
final String v11 = "01020101"; // const-string v11, "01020101"
v11 = (( java.lang.String ) v10 ).startsWith ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* if-nez v11, :cond_4 */
/* .line 73 */
/* .line 75 */
} // :cond_4
/* const/16 v11, 0x8 */
(( java.lang.String ) v10 ).substring ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;
/* .line 76 */
com.android.server.audio.dolbyeffect.deviceinfo.BtDeviceInfo .hexToByteArray ( v10 );
/* .line 77 */
/* .local v11, "argbyte":[B */
/* aget-byte v13, v11, v6 */
/* if-nez v13, :cond_a */
/* aget-byte v13, v11, v4 */
/* add-int/2addr v13, v6 */
/* mul-int/2addr v13, v5 */
v14 = (( java.lang.String ) v10 ).length ( ); // invoke-virtual {v10}, Ljava/lang/String;->length()I
/* if-eq v13, v14, :cond_5 */
/* .line 81 */
} // :cond_5
(( java.lang.String ) v10 ).substring ( v12 ); // invoke-virtual {v10, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;
/* .line 82 */
com.android.server.audio.dolbyeffect.deviceinfo.BtDeviceInfo .hexToByteArray ( v10 );
/* .line 83 */
int v13 = 2; // const/4 v13, 0x2
/* .line 84 */
/* .local v13, "position":I */
} // :goto_1
/* array-length v14, v11 */
/* sub-int/2addr v14, v6 */
/* if-gt v13, v14, :cond_8 */
/* .line 85 */
/* add-int/lit8 v14, v13, -0x2 */
/* aget-byte v14, v11, v14 */
/* .line 86 */
/* .local v14, "lengthFlag":I */
/* add-int v15, v13, v14 */
/* sub-int/2addr v15, v5 */
/* array-length v12, v11 */
/* sub-int/2addr v12, v6 */
/* if-le v15, v12, :cond_6 */
/* .line 88 */
/* .line 90 */
} // :cond_6
/* add-int/lit8 v12, v13, -0x1 */
/* aget-byte v12, v11, v12 */
/* .line 91 */
/* .local v12, "tag":I */
/* new-array v15, v14, [B */
/* .line 92 */
/* .local v15, "arr":[B */
/* add-int/lit8 v6, v14, -0x1 */
java.lang.System .arraycopy ( v11,v13,v15,v4,v6 );
/* .line 93 */
/* const/16 v6, 0x20 */
/* if-ne v12, v6, :cond_7 */
/* .line 94 */
int v6 = 3; // const/4 v6, 0x3
/* aget-byte v6, v15, v6 */
/* aget-byte v4, v15, v5 */
/* mul-int/lit16 v4, v4, 0x100 */
/* add-int/2addr v6, v4 */
int v4 = 1; // const/4 v4, 0x1
/* aget-byte v5, v15, v4 */
/* mul-int/lit16 v5, v5, 0x100 */
/* mul-int/lit16 v5, v5, 0x100 */
/* add-int/2addr v6, v5 */
/* .line 95 */
} // .end local v8 # "majorId":I
/* .local v6, "majorId":I */
int v4 = 5; // const/4 v4, 0x5
/* aget-byte v4, v15, v4 */
/* mul-int/lit16 v4, v4, 0x100 */
int v5 = 4; // const/4 v5, 0x4
/* aget-byte v8, v15, v5 */
/* add-int/2addr v4, v8 */
/* move v9, v4 */
/* move v8, v6 */
} // .end local v9 # "minorId":I
/* .local v4, "minorId":I */
/* .line 93 */
} // .end local v4 # "minorId":I
} // .end local v6 # "majorId":I
/* .restart local v8 # "majorId":I */
/* .restart local v9 # "minorId":I */
} // :cond_7
int v5 = 4; // const/4 v5, 0x4
/* .line 97 */
} // :goto_2
/* add-int/lit8 v4, v14, 0x1 */
/* add-int/2addr v13, v4 */
/* .line 98 */
} // .end local v12 # "tag":I
} // .end local v14 # "lengthFlag":I
} // .end local v15 # "arr":[B
/* move v12, v5 */
int v4 = 0; // const/4 v4, 0x0
int v5 = 2; // const/4 v5, 0x2
int v6 = 1; // const/4 v6, 0x1
/* .line 99 */
} // :cond_8
/* if-nez v8, :cond_9 */
/* if-nez v9, :cond_9 */
/* .line 100 */
int v4 = 0; // const/4 v4, 0x0
/* .line 99 */
} // :cond_9
int v4 = 0; // const/4 v4, 0x0
/* .line 102 */
/* aput v8, v1, v4 */
/* .line 103 */
int v4 = 1; // const/4 v4, 0x1
/* aput v9, v1, v4 */
/* .line 105 */
/* .line 79 */
} // .end local v13 # "position":I
} // :cond_a
} // :goto_3
/* .line 69 */
} // .end local v11 # "argbyte":[B
} // :cond_b
} // :goto_4
/* .line 61 */
} // .end local v8 # "majorId":I
} // .end local v9 # "minorId":I
} // .end local v10 # "local":Ljava/lang/String;
} // :cond_c
} // :goto_5
/* .line 54 */
} // .end local v3 # "extraDevice":Landroid/bluetooth/BluetoothDevice;
} // .end local v7 # "args":[Ljava/lang/Object;
} // :cond_d
} // :goto_6
} // .end method
/* # virtual methods */
public Integer getMajorID ( ) {
/* .locals 1 */
/* .line 16 */
/* iget v0, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->mMajorID:I */
} // .end method
public Integer getMinorID ( ) {
/* .locals 1 */
/* .line 24 */
/* iget v0, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->mMinorID:I */
} // .end method
public Boolean getState ( ) {
/* .locals 1 */
/* .line 32 */
/* iget-boolean v0, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->mState:Z */
} // .end method
public void setMajorID ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "MajorID" # I */
/* .line 20 */
/* iput p1, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->mMajorID:I */
/* .line 21 */
return;
} // .end method
public void setMinorID ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "MinorID" # I */
/* .line 28 */
/* iput p1, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->mMinorID:I */
/* .line 29 */
return;
} // .end method
public void setState ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "State" # Z */
/* .line 36 */
/* iput-boolean p1, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/BtDeviceInfo;->mState:Z */
/* .line 37 */
return;
} // .end method
