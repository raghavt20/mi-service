.class public Lcom/android/server/audio/dolbyeffect/deviceinfo/WiredDeviceInfo;
.super Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;
.source "WiredDeviceInfo.java"


# instance fields
.field mProductID:I

.field mVendorID:I


# direct methods
.method public constructor <init>(III)V
    .locals 1
    .param p1, "mDeviceType"    # I
    .param p2, "mVendorID"    # I
    .param p3, "mProductID"    # I

    .line 23
    invoke-direct {p0}, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;-><init>()V

    .line 24
    iput p2, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/WiredDeviceInfo;->mVendorID:I

    .line 25
    iput p3, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/WiredDeviceInfo;->mProductID:I

    .line 26
    iput p1, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/WiredDeviceInfo;->mDeviceType:I

    .line 27
    const-string/jumbo v0, "wired headset"

    iput-object v0, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/WiredDeviceInfo;->mDevice:Ljava/lang/String;

    .line 28
    return-void
.end method


# virtual methods
.method public getProductID()I
    .locals 1

    .line 13
    iget v0, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/WiredDeviceInfo;->mProductID:I

    return v0
.end method

.method public getVendorID()I
    .locals 1

    .line 5
    iget v0, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/WiredDeviceInfo;->mVendorID:I

    return v0
.end method

.method public setProductID(I)V
    .locals 0
    .param p1, "ProductID"    # I

    .line 17
    iput p1, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/WiredDeviceInfo;->mProductID:I

    .line 18
    return-void
.end method

.method public setVendorID(I)V
    .locals 0
    .param p1, "VendorID"    # I

    .line 9
    iput p1, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/WiredDeviceInfo;->mVendorID:I

    .line 10
    return-void
.end method
