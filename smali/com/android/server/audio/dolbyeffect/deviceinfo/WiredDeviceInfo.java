public class com.android.server.audio.dolbyeffect.deviceinfo.WiredDeviceInfo extends com.android.server.audio.dolbyeffect.deviceinfo.DeviceInfoBase {
	 /* .source "WiredDeviceInfo.java" */
	 /* # instance fields */
	 Integer mProductID;
	 Integer mVendorID;
	 /* # direct methods */
	 public com.android.server.audio.dolbyeffect.deviceinfo.WiredDeviceInfo ( ) {
		 /* .locals 1 */
		 /* .param p1, "mDeviceType" # I */
		 /* .param p2, "mVendorID" # I */
		 /* .param p3, "mProductID" # I */
		 /* .line 23 */
		 /* invoke-direct {p0}, Lcom/android/server/audio/dolbyeffect/deviceinfo/DeviceInfoBase;-><init>()V */
		 /* .line 24 */
		 /* iput p2, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/WiredDeviceInfo;->mVendorID:I */
		 /* .line 25 */
		 /* iput p3, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/WiredDeviceInfo;->mProductID:I */
		 /* .line 26 */
		 /* iput p1, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/WiredDeviceInfo;->mDeviceType:I */
		 /* .line 27 */
		 /* const-string/jumbo v0, "wired headset" */
		 this.mDevice = v0;
		 /* .line 28 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Integer getProductID ( ) {
		 /* .locals 1 */
		 /* .line 13 */
		 /* iget v0, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/WiredDeviceInfo;->mProductID:I */
	 } // .end method
	 public Integer getVendorID ( ) {
		 /* .locals 1 */
		 /* .line 5 */
		 /* iget v0, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/WiredDeviceInfo;->mVendorID:I */
	 } // .end method
	 public void setProductID ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "ProductID" # I */
		 /* .line 17 */
		 /* iput p1, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/WiredDeviceInfo;->mProductID:I */
		 /* .line 18 */
		 return;
	 } // .end method
	 public void setVendorID ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "VendorID" # I */
		 /* .line 9 */
		 /* iput p1, p0, Lcom/android/server/audio/dolbyeffect/deviceinfo/WiredDeviceInfo;->mVendorID:I */
		 /* .line 10 */
		 return;
	 } // .end method
