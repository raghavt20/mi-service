public class com.android.server.audio.dolbyeffect.DeviceId {
	 /* .source "DeviceId.java" */
	 /* # static fields */
	 public static final java.lang.String BLUETOOTH_AIRDOTS_3_PRO;
	 public static final java.lang.String BLUETOOTH_AIR_2_SE;
	 public static final java.lang.String BLUETOOTH_DEFAULT;
	 public static final java.lang.String BLUETOOTH_REDMI_BUDS_4_PRO;
	 public static final java.lang.String BLUETOOTH_SPATIALIZER;
	 public static final java.lang.String SPK_DEFAULT;
	 public static final java.lang.String SPK_SPATIALIZER;
	 public static final java.lang.String SPK_SPATIALIZER_270;
	 public static final java.lang.String SPK_SPATIALIZER_270_VOLUME_HIGH;
	 public static final java.lang.String SPK_SPATIALIZER_270_VOLUME_LOW;
	 public static final java.lang.String SPK_SPATIALIZER_90;
	 public static final java.lang.String SPK_SPATIALIZER_90_VOLUME_HIGH;
	 public static final java.lang.String SPK_SPATIALIZER_90_VOLUME_LOW;
	 public static final java.lang.String SPK_VOLUME_HIGH;
	 public static final java.lang.String SPK_VOLUME_LOW;
	 public static final java.lang.String USB_DEFAULT;
	 public static final java.lang.String USB_SPATIALIZER;
	 public static final java.lang.String WIRED_DEFAULT;
	 public static final java.lang.String WIRED_SPATIALIZER;
	 /* # direct methods */
	 public com.android.server.audio.dolbyeffect.DeviceId ( ) {
		 /* .locals 0 */
		 /* .line 3 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static java.lang.String getBtDeviceId ( Integer p0, Integer p1 ) {
		 /* .locals 1 */
		 /* .param p0, "majorId" # I */
		 /* .param p1, "minorId" # I */
		 /* .line 32 */
		 /* const v0, 0x10110 */
		 /* if-ne p0, v0, :cond_1 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* if-eq p1, v0, :cond_0 */
		 int v0 = 2; // const/4 v0, 0x2
		 /* if-eq p1, v0, :cond_0 */
		 int v0 = 3; // const/4 v0, 0x3
		 /* if-eq p1, v0, :cond_0 */
		 int v0 = 7; // const/4 v0, 0x7
		 /* if-ne p1, v0, :cond_1 */
		 /* .line 33 */
	 } // :cond_0
	 final String v0 = "bluetooth_Redmi_Buds_4_Pro"; // const-string v0, "bluetooth_Redmi_Buds_4_Pro"
	 /* .line 34 */
} // :cond_1
/* const v0, 0x10109 */
/* if-ne p0, v0, :cond_2 */
int v0 = 6; // const/4 v0, 0x6
/* if-ne p1, v0, :cond_2 */
/* .line 35 */
final String v0 = "bluetooth_AirDots_3_Pro"; // const-string v0, "bluetooth_AirDots_3_Pro"
/* .line 36 */
} // :cond_2
/* const v0, 0x10101 */
/* if-ne p0, v0, :cond_3 */
/* if-nez p1, :cond_3 */
/* .line 37 */
final String v0 = "bluetooth_Air_2_SE"; // const-string v0, "bluetooth_Air_2_SE"
/* .line 39 */
} // :cond_3
final String v0 = "default_bluetooth"; // const-string v0, "default_bluetooth"
} // .end method
public static java.lang.String getUsbDeviceId ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p0, "vendorId" # I */
/* .param p1, "productId" # I */
/* .line 44 */
final String v0 = "default_usb"; // const-string v0, "default_usb"
} // .end method
public static java.lang.String getWiredDeviceId ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p0, "vendorId" # I */
/* .param p1, "productId" # I */
/* .line 48 */
final String v0 = "default_headphone"; // const-string v0, "default_headphone"
} // .end method
