class com.android.server.audio.dolbyeffect.DolbyEffectController$WorkHandler extends android.os.Handler {
	 /* .source "DolbyEffectController.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/audio/dolbyeffect/DolbyEffectController; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "WorkHandler" */
} // .end annotation
/* # instance fields */
final com.android.server.audio.dolbyeffect.DolbyEffectController this$0; //synthetic
/* # direct methods */
public com.android.server.audio.dolbyeffect.DolbyEffectController$WorkHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 198 */
this.this$0 = p1;
/* .line 199 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 200 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 5 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 204 */
/* iget v0, p1, Landroid/os/Message;->what:I */
final String v1 = "DolbyEffectController"; // const-string v1, "DolbyEffectController"
/* packed-switch v0, :pswitch_data_0 */
/* goto/16 :goto_0 */
/* .line 226 */
/* :pswitch_0 */
final String v0 = "receive MSG_SPATIALIZER_AVALIABLE_STATE"; // const-string v0, "receive MSG_SPATIALIZER_AVALIABLE_STATE"
android.util.Log .d ( v1,v0 );
/* .line 227 */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Ljava/lang/Boolean; */
v1 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
int v2 = 6; // const/4 v2, 0x6
com.android.server.audio.dolbyeffect.DolbyEffectController .-$$Nest$monSpatializerStateChanged ( v0,v2,v1 );
/* .line 228 */
/* goto/16 :goto_0 */
/* .line 230 */
/* :pswitch_1 */
final String v0 = "receive MSG_SPATIALIZER_ENABLED_STATE"; // const-string v0, "receive MSG_SPATIALIZER_ENABLED_STATE"
android.util.Log .d ( v1,v0 );
/* .line 231 */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Ljava/lang/Boolean; */
v1 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
int v2 = 5; // const/4 v2, 0x5
com.android.server.audio.dolbyeffect.DolbyEffectController .-$$Nest$monSpatializerStateChanged ( v0,v2,v1 );
/* .line 232 */
/* .line 222 */
/* :pswitch_2 */
final String v0 = "receive MSG_ROTATION_CHANGED"; // const-string v0, "receive MSG_ROTATION_CHANGED"
android.util.Log .d ( v1,v0 );
/* .line 223 */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
com.android.server.audio.dolbyeffect.DolbyEffectController .-$$Nest$monRotationChanged ( v0,v1 );
/* .line 224 */
/* .line 214 */
/* :pswitch_3 */
final String v0 = "receive MSG_BT_STATE_CHANGED_DEVICEBROKER"; // const-string v0, "receive MSG_BT_STATE_CHANGED_DEVICEBROKER"
android.util.Log .d ( v1,v0 );
/* .line 215 */
v0 = this.this$0;
(( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
final String v2 = "device"; // const-string v2, "device"
(( android.os.Bundle ) v1 ).getString ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
(( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
final String v3 = "profile"; // const-string v3, "profile"
(( android.os.Bundle ) v2 ).getString ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
(( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
/* const-string/jumbo v4, "state" */
(( android.os.Bundle ) v3 ).getString ( v4 ); // invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
com.android.server.audio.dolbyeffect.DolbyEffectController .-$$Nest$monBTStateChangedFromDeviceBroker ( v0,v1,v2,v3 );
/* .line 216 */
/* .line 218 */
/* :pswitch_4 */
final String v0 = "receive MSG_HEADSET_PLUG"; // const-string v0, "receive MSG_HEADSET_PLUG"
android.util.Log .d ( v1,v0 );
/* .line 219 */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Landroid/content/Intent; */
com.android.server.audio.dolbyeffect.DolbyEffectController .-$$Nest$monHeadsetPlug ( v0,v1 );
/* .line 220 */
/* .line 210 */
/* :pswitch_5 */
final String v0 = "receive MSG_BT_STATE_CHANGED"; // const-string v0, "receive MSG_BT_STATE_CHANGED"
android.util.Log .d ( v1,v0 );
/* .line 211 */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Landroid/content/Intent; */
com.android.server.audio.dolbyeffect.DolbyEffectController .-$$Nest$monBTStateChanged ( v0,v1 );
/* .line 212 */
/* .line 206 */
/* :pswitch_6 */
final String v0 = "receive MSG_VOLUME_CHANGED"; // const-string v0, "receive MSG_VOLUME_CHANGED"
android.util.Log .d ( v1,v0 );
/* .line 207 */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
com.android.server.audio.dolbyeffect.DolbyEffectController .-$$Nest$monVolumeChanged ( v0,v1 );
/* .line 208 */
/* nop */
/* .line 236 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
