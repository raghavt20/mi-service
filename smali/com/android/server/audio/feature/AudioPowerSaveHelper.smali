.class public Lcom/android/server/audio/feature/AudioPowerSaveHelper;
.super Ljava/lang/Object;
.source "AudioPowerSaveHelper.java"


# static fields
.field private static final AUDIO_CONTROL_STATUS_VOLUME_CONTROL:I = 0x1

.field private static final AUDIO_CONTROL_STATUS_VOLUME_RESUME:I = 0x0

.field private static final BATTERY_PERCENTAGE_LOW:I = 0xa

.field private static final BATTERY_PERCENTAGE_LOWER:I = 0x5

.field private static final TAG:Ljava/lang/String; = "FeatureAdapter.AudioPowerSaveHelper"

.field private static final VOLUME_HIGH_SCALE_FOR_BATTERY_LOW:F = 0.8f

.field private static final VOLUME_HIGH_SCALE_FOR_BATTERY_LOWER:F = 0.7f

.field private static final VOLUME_LOW_SCALE_FOR_BATTERY_LOW:F = 0.9f

.field private static final VOLUME_LOW_SCALE_FOR_BATTERY_LOWER:F = 0.8f

.field private static final VOLUME_STATUS_FOR_BATTERY_ADJUSTED_LOW:I = 0x1

.field private static final VOLUME_STATUS_FOR_BATTERY_ADJUSTED_LOWER:I = 0x2

.field private static final VOLUME_STATUS_FOR_BATTERY_NORMAL:I


# instance fields
.field private mAudioManager:Landroid/media/AudioManager;

.field private mContext:Landroid/content/Context;

.field private mCurVolumeStatusForBattery:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->mCurVolumeStatusForBattery:I

    .line 43
    const-string v0, "FeatureAdapter.AudioPowerSaveHelper"

    const-string v1, "AudioPowerSaveHelper Construct ..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    iput-object p1, p0, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->mContext:Landroid/content/Context;

    .line 45
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->mAudioManager:Landroid/media/AudioManager;

    .line 46
    return-void
.end method

.method private adjustPlayerVolumeForBattery(Landroid/media/AudioPlaybackConfiguration;)V
    .locals 7
    .param p1, "apc"    # Landroid/media/AudioPlaybackConfiguration;

    .line 102
    const-string v0, "FeatureAdapter.AudioPowerSaveHelper"

    invoke-virtual {p1}, Landroid/media/AudioPlaybackConfiguration;->getAudioAttributes()Landroid/media/AudioAttributes;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/AudioAttributes;->getVolumeControlStream()I

    move-result v1

    .line 103
    .local v1, "streamType":I
    invoke-virtual {p1}, Landroid/media/AudioPlaybackConfiguration;->getAudioAttributes()Landroid/media/AudioAttributes;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/AudioAttributes;->getUsage()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    .line 105
    :cond_0
    iget-object v2, p0, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2, v1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v2

    .line 106
    .local v2, "maxVolume":I
    iget-object v3, p0, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v3, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v3

    .line 107
    .local v3, "curStreamVolume":I
    invoke-direct {p0, v3, v2}, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->calculatePlayerVolumeForBattery(II)F

    move-result v4

    .line 109
    .local v4, "finalPlayerVolume":F
    :try_start_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "adjustPlayerVolumeForBattery. finalPlayerVolume:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    invoke-virtual {p1}, Landroid/media/AudioPlaybackConfiguration;->getPlayerProxy()Landroid/media/PlayerProxy;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/media/PlayerProxy;->setVolume(F)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    goto :goto_0

    .line 111
    :catch_0
    move-exception v5

    .line 112
    .local v5, "e":Ljava/lang/Exception;
    const-string v6, "adjustPlayerVolumeForBattery "

    invoke-static {v0, v6, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 115
    .end local v2    # "maxVolume":I
    .end local v3    # "curStreamVolume":I
    .end local v4    # "finalPlayerVolume":F
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_0
    return-void
.end method

.method private calculatePlayerVolumeForBattery(II)F
    .locals 7
    .param p1, "curStreamVolume"    # I
    .param p2, "maxVolume"    # I

    .line 118
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "calculatePlayerVolume curStreamVolume:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxVolume:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mCurVolumeStatusForBattery:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->mCurVolumeStatusForBattery:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FeatureAdapter.AudioPowerSaveHelper"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    const/high16 v0, 0x3f800000    # 1.0f

    .line 121
    .local v0, "finalVolume":F
    int-to-double v1, p1

    int-to-double v3, p2

    const-wide v5, 0x3fc999999999999aL    # 0.2

    mul-double/2addr v3, v5

    cmpg-double v1, v1, v3

    if-gtz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 122
    .local v1, "isVolumeLow":Z
    :goto_0
    iget v2, p0, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->mCurVolumeStatusForBattery:I

    packed-switch v2, :pswitch_data_0

    goto :goto_1

    .line 124
    :pswitch_0
    if-eqz v1, :cond_1

    .line 125
    const v0, 0x3f4ccccd    # 0.8f

    goto :goto_1

    .line 127
    :cond_1
    const v0, 0x3f333333    # 0.7f

    .line 129
    goto :goto_1

    .line 132
    :pswitch_1
    if-eqz v1, :cond_2

    .line 133
    const v0, 0x3f666666    # 0.9f

    goto :goto_1

    .line 135
    :cond_2
    const v0, 0x3f4ccccd    # 0.8f

    .line 137
    nop

    .line 143
    :goto_1
    return v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private controlVolumeForBattery(I)V
    .locals 2
    .param p1, "batteryPct"    # I

    .line 67
    if-gez p1, :cond_0

    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "invalid batteryPct:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FeatureAdapter.AudioPowerSaveHelper"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    return-void

    .line 70
    :cond_0
    const/16 v0, 0xa

    if-le p1, v0, :cond_1

    .line 71
    invoke-direct {p0}, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->resumeVolumeForBattery()V

    goto :goto_2

    .line 73
    :cond_1
    const/4 v0, 0x5

    if-gt p1, v0, :cond_3

    .line 74
    iget v0, p0, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->mCurVolumeStatusForBattery:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 75
    return-void

    .line 77
    :cond_2
    iput v1, p0, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->mCurVolumeStatusForBattery:I

    goto :goto_0

    .line 79
    :cond_3
    iget v0, p0, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->mCurVolumeStatusForBattery:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 80
    return-void

    .line 82
    :cond_4
    iput v1, p0, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->mCurVolumeStatusForBattery:I

    .line 84
    :goto_0
    iget-object v0, p0, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getActivePlaybackConfigurations()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioPlaybackConfiguration;

    .line 85
    .local v1, "apc":Landroid/media/AudioPlaybackConfiguration;
    invoke-direct {p0, v1}, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->adjustPlayerVolumeForBattery(Landroid/media/AudioPlaybackConfiguration;)V

    .line 86
    .end local v1    # "apc":Landroid/media/AudioPlaybackConfiguration;
    goto :goto_1

    .line 88
    :cond_5
    :goto_2
    return-void
.end method

.method private resumeVolumeForBattery()V
    .locals 2

    .line 91
    iget v0, p0, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->mCurVolumeStatusForBattery:I

    if-nez v0, :cond_0

    .line 92
    return-void

    .line 94
    :cond_0
    const-string v0, "FeatureAdapter.AudioPowerSaveHelper"

    const-string v1, "resume player volume for battery"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->mCurVolumeStatusForBattery:I

    .line 96
    iget-object v0, p0, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getActivePlaybackConfigurations()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioPlaybackConfiguration;

    .line 97
    .local v1, "apc":Landroid/media/AudioPlaybackConfiguration;
    invoke-direct {p0, v1}, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->adjustPlayerVolumeForBattery(Landroid/media/AudioPlaybackConfiguration;)V

    .line 98
    .end local v1    # "apc":Landroid/media/AudioPlaybackConfiguration;
    goto :goto_0

    .line 99
    :cond_1
    return-void
.end method


# virtual methods
.method public handleLowBattery(II)V
    .locals 3
    .param p1, "batteryPct"    # I
    .param p2, "audioControlStatus"    # I

    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "handleLowBattery batteryPct: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", audioControlStatus:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FeatureAdapter.AudioPowerSaveHelper"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    packed-switch p2, :pswitch_data_0

    .line 61
    const-string/jumbo v0, "unknown audioControlStatus"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    return-void

    .line 57
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AUDIO_CONTROL_STATUS_VOLUME_CONTROL batteryPct:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    invoke-direct {p0, p1}, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->controlVolumeForBattery(I)V

    .line 59
    goto :goto_0

    .line 53
    :pswitch_1
    const-string v0, "AUDIO_CONTROL_STATUS_VOLUME_RESUME"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    invoke-direct {p0}, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->resumeVolumeForBattery()V

    .line 55
    nop

    .line 64
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public initPlayerForBattery(Landroid/media/AudioPlaybackConfiguration;)V
    .locals 2
    .param p1, "apc"    # Landroid/media/AudioPlaybackConfiguration;

    .line 147
    const-string v0, "FeatureAdapter.AudioPowerSaveHelper"

    const-string v1, "initPlayerForBattery"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    iget v0, p0, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->mCurVolumeStatusForBattery:I

    if-eqz v0, :cond_0

    .line 149
    invoke-direct {p0, p1}, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->adjustPlayerVolumeForBattery(Landroid/media/AudioPlaybackConfiguration;)V

    .line 151
    :cond_0
    return-void
.end method
