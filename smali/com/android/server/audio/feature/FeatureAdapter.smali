.class public Lcom/android/server/audio/feature/FeatureAdapter;
.super Ljava/lang/Object;
.source "FeatureAdapter.java"


# static fields
.field private static final CONFIG_PROP:I

.field public static final ENABLE:Z

.field private static final FLAG_FEATURE_ADAPTER_ENABLE:I = 0x1

.field private static final FLAG_LOW_BATTERY_ENABLE:I = 0x2

.field private static final FLAG_POWER_SAVE_ENABLE:I = 0x4

.field private static final TAG:Ljava/lang/String; = "AudioService.FeatureAdapter"


# instance fields
.field private final LOW_BATTERY_ENABLE:Z

.field private final POWER_SAVE_ENABLE:Z

.field private mAudioPowerSaveHelper:Lcom/android/server/audio/feature/AudioPowerSaveHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 14
    nop

    .line 15
    const-string v0, "ro.vendor.audio.feature.adapter"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/server/audio/feature/FeatureAdapter;->CONFIG_PROP:I

    .line 16
    const/4 v2, 0x1

    and-int/2addr v0, v2

    if-eqz v0, :cond_0

    move v1, v2

    :cond_0
    sput-boolean v1, Lcom/android/server/audio/feature/FeatureAdapter;->ENABLE:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    sget v0, Lcom/android/server/audio/feature/FeatureAdapter;->CONFIG_PROP:I

    and-int/lit8 v1, v0, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    move v1, v3

    :goto_0
    iput-boolean v1, p0, Lcom/android/server/audio/feature/FeatureAdapter;->LOW_BATTERY_ENABLE:Z

    .line 18
    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move v2, v3

    :goto_1
    iput-boolean v2, p0, Lcom/android/server/audio/feature/FeatureAdapter;->POWER_SAVE_ENABLE:Z

    .line 23
    const-string v0, "AudioService.FeatureAdapter"

    const-string v3, "FeatureAdapter Construct ..."

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 25
    if-nez v1, :cond_2

    if-eqz v2, :cond_3

    .line 26
    :cond_2
    new-instance v0, Lcom/android/server/audio/feature/AudioPowerSaveHelper;

    invoke-direct {v0, p1}, Lcom/android/server/audio/feature/AudioPowerSaveHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/audio/feature/FeatureAdapter;->mAudioPowerSaveHelper:Lcom/android/server/audio/feature/AudioPowerSaveHelper;

    .line 28
    :cond_3
    return-void
.end method


# virtual methods
.method public handleLowBattery(II)V
    .locals 1
    .param p1, "batteryPct"    # I
    .param p2, "audioControlStatus"    # I

    .line 31
    iget-boolean v0, p0, Lcom/android/server/audio/feature/FeatureAdapter;->LOW_BATTERY_ENABLE:Z

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/android/server/audio/feature/FeatureAdapter;->mAudioPowerSaveHelper:Lcom/android/server/audio/feature/AudioPowerSaveHelper;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->handleLowBattery(II)V

    .line 34
    :cond_0
    return-void
.end method

.method public onPlayerTracked(Landroid/media/AudioPlaybackConfiguration;)V
    .locals 1
    .param p1, "apc"    # Landroid/media/AudioPlaybackConfiguration;

    .line 37
    iget-boolean v0, p0, Lcom/android/server/audio/feature/FeatureAdapter;->LOW_BATTERY_ENABLE:Z

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/android/server/audio/feature/FeatureAdapter;->mAudioPowerSaveHelper:Lcom/android/server/audio/feature/AudioPowerSaveHelper;

    invoke-virtual {v0, p1}, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->initPlayerForBattery(Landroid/media/AudioPlaybackConfiguration;)V

    .line 40
    :cond_0
    return-void
.end method
