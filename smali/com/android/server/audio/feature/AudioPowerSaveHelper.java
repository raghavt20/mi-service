public class com.android.server.audio.feature.AudioPowerSaveHelper {
	 /* .source "AudioPowerSaveHelper.java" */
	 /* # static fields */
	 private static final Integer AUDIO_CONTROL_STATUS_VOLUME_CONTROL;
	 private static final Integer AUDIO_CONTROL_STATUS_VOLUME_RESUME;
	 private static final Integer BATTERY_PERCENTAGE_LOW;
	 private static final Integer BATTERY_PERCENTAGE_LOWER;
	 private static final java.lang.String TAG;
	 private static final Float VOLUME_HIGH_SCALE_FOR_BATTERY_LOW;
	 private static final Float VOLUME_HIGH_SCALE_FOR_BATTERY_LOWER;
	 private static final Float VOLUME_LOW_SCALE_FOR_BATTERY_LOW;
	 private static final Float VOLUME_LOW_SCALE_FOR_BATTERY_LOWER;
	 private static final Integer VOLUME_STATUS_FOR_BATTERY_ADJUSTED_LOW;
	 private static final Integer VOLUME_STATUS_FOR_BATTERY_ADJUSTED_LOWER;
	 private static final Integer VOLUME_STATUS_FOR_BATTERY_NORMAL;
	 /* # instance fields */
	 private android.media.AudioManager mAudioManager;
	 private android.content.Context mContext;
	 private Integer mCurVolumeStatusForBattery;
	 /* # direct methods */
	 public com.android.server.audio.feature.AudioPowerSaveHelper ( ) {
		 /* .locals 2 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 42 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 29 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput v0, p0, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->mCurVolumeStatusForBattery:I */
		 /* .line 43 */
		 final String v0 = "FeatureAdapter.AudioPowerSaveHelper"; // const-string v0, "FeatureAdapter.AudioPowerSaveHelper"
		 final String v1 = "AudioPowerSaveHelper Construct ..."; // const-string v1, "AudioPowerSaveHelper Construct ..."
		 android.util.Log .d ( v0,v1 );
		 /* .line 44 */
		 this.mContext = p1;
		 /* .line 45 */
		 final String v0 = "audio"; // const-string v0, "audio"
		 (( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
		 /* check-cast v0, Landroid/media/AudioManager; */
		 this.mAudioManager = v0;
		 /* .line 46 */
		 return;
	 } // .end method
	 private void adjustPlayerVolumeForBattery ( android.media.AudioPlaybackConfiguration p0 ) {
		 /* .locals 7 */
		 /* .param p1, "apc" # Landroid/media/AudioPlaybackConfiguration; */
		 /* .line 102 */
		 final String v0 = "FeatureAdapter.AudioPowerSaveHelper"; // const-string v0, "FeatureAdapter.AudioPowerSaveHelper"
		 (( android.media.AudioPlaybackConfiguration ) p1 ).getAudioAttributes ( ); // invoke-virtual {p1}, Landroid/media/AudioPlaybackConfiguration;->getAudioAttributes()Landroid/media/AudioAttributes;
		 v1 = 		 (( android.media.AudioAttributes ) v1 ).getVolumeControlStream ( ); // invoke-virtual {v1}, Landroid/media/AudioAttributes;->getVolumeControlStream()I
		 /* .line 103 */
		 /* .local v1, "streamType":I */
		 (( android.media.AudioPlaybackConfiguration ) p1 ).getAudioAttributes ( ); // invoke-virtual {p1}, Landroid/media/AudioPlaybackConfiguration;->getAudioAttributes()Landroid/media/AudioAttributes;
		 v2 = 		 (( android.media.AudioAttributes ) v2 ).getUsage ( ); // invoke-virtual {v2}, Landroid/media/AudioAttributes;->getUsage()I
		 int v3 = 1; // const/4 v3, 0x1
		 /* if-eq v2, v3, :cond_0 */
		 int v2 = 3; // const/4 v2, 0x3
		 /* if-ne v1, v2, :cond_1 */
		 /* .line 105 */
	 } // :cond_0
	 v2 = this.mAudioManager;
	 v2 = 	 (( android.media.AudioManager ) v2 ).getStreamMaxVolume ( v1 ); // invoke-virtual {v2, v1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I
	 /* .line 106 */
	 /* .local v2, "maxVolume":I */
	 v3 = this.mAudioManager;
	 v3 = 	 (( android.media.AudioManager ) v3 ).getStreamVolume ( v1 ); // invoke-virtual {v3, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I
	 /* .line 107 */
	 /* .local v3, "curStreamVolume":I */
	 v4 = 	 /* invoke-direct {p0, v3, v2}, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->calculatePlayerVolumeForBattery(II)F */
	 /* .line 109 */
	 /* .local v4, "finalPlayerVolume":F */
	 try { // :try_start_0
		 /* new-instance v5, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v6 = "adjustPlayerVolumeForBattery.finalPlayerVolume:"; // const-string v6, "adjustPlayerVolumeForBattery.finalPlayerVolume:"
		 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Log .d ( v0,v5 );
		 /* .line 110 */
		 (( android.media.AudioPlaybackConfiguration ) p1 ).getPlayerProxy ( ); // invoke-virtual {p1}, Landroid/media/AudioPlaybackConfiguration;->getPlayerProxy()Landroid/media/PlayerProxy;
		 (( android.media.PlayerProxy ) v5 ).setVolume ( v4 ); // invoke-virtual {v5, v4}, Landroid/media/PlayerProxy;->setVolume(F)V
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 113 */
		 /* .line 111 */
		 /* :catch_0 */
		 /* move-exception v5 */
		 /* .line 112 */
		 /* .local v5, "e":Ljava/lang/Exception; */
		 final String v6 = "adjustPlayerVolumeForBattery "; // const-string v6, "adjustPlayerVolumeForBattery "
		 android.util.Log .e ( v0,v6,v5 );
		 /* .line 115 */
	 } // .end local v2 # "maxVolume":I
} // .end local v3 # "curStreamVolume":I
} // .end local v4 # "finalPlayerVolume":F
} // .end local v5 # "e":Ljava/lang/Exception;
} // :cond_1
} // :goto_0
return;
} // .end method
private Float calculatePlayerVolumeForBattery ( Integer p0, Integer p1 ) {
/* .locals 7 */
/* .param p1, "curStreamVolume" # I */
/* .param p2, "maxVolume" # I */
/* .line 118 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "calculatePlayerVolume curStreamVolume:"; // const-string v1, "calculatePlayerVolume curStreamVolume:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", maxVolume:"; // const-string v1, ", maxVolume:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mCurVolumeStatusForBattery:"; // const-string v1, ", mCurVolumeStatusForBattery:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->mCurVolumeStatusForBattery:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "FeatureAdapter.AudioPowerSaveHelper"; // const-string v1, "FeatureAdapter.AudioPowerSaveHelper"
android.util.Log .d ( v1,v0 );
/* .line 120 */
/* const/high16 v0, 0x3f800000 # 1.0f */
/* .line 121 */
/* .local v0, "finalVolume":F */
/* int-to-double v1, p1 */
/* int-to-double v3, p2 */
/* const-wide v5, 0x3fc999999999999aL # 0.2 */
/* mul-double/2addr v3, v5 */
/* cmpg-double v1, v1, v3 */
/* if-gtz v1, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 122 */
/* .local v1, "isVolumeLow":Z */
} // :goto_0
/* iget v2, p0, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->mCurVolumeStatusForBattery:I */
/* packed-switch v2, :pswitch_data_0 */
/* .line 124 */
/* :pswitch_0 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 125 */
/* const v0, 0x3f4ccccd # 0.8f */
/* .line 127 */
} // :cond_1
/* const v0, 0x3f333333 # 0.7f */
/* .line 129 */
/* .line 132 */
/* :pswitch_1 */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 133 */
/* const v0, 0x3f666666 # 0.9f */
/* .line 135 */
} // :cond_2
/* const v0, 0x3f4ccccd # 0.8f */
/* .line 137 */
/* nop */
/* .line 143 */
} // :goto_1
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void controlVolumeForBattery ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "batteryPct" # I */
/* .line 67 */
/* if-gez p1, :cond_0 */
/* .line 68 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "invalid batteryPct:"; // const-string v1, "invalid batteryPct:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "FeatureAdapter.AudioPowerSaveHelper"; // const-string v1, "FeatureAdapter.AudioPowerSaveHelper"
android.util.Log .d ( v1,v0 );
/* .line 69 */
return;
/* .line 70 */
} // :cond_0
/* const/16 v0, 0xa */
/* if-le p1, v0, :cond_1 */
/* .line 71 */
/* invoke-direct {p0}, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->resumeVolumeForBattery()V */
/* .line 73 */
} // :cond_1
int v0 = 5; // const/4 v0, 0x5
/* if-gt p1, v0, :cond_3 */
/* .line 74 */
/* iget v0, p0, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->mCurVolumeStatusForBattery:I */
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_2 */
/* .line 75 */
return;
/* .line 77 */
} // :cond_2
/* iput v1, p0, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->mCurVolumeStatusForBattery:I */
/* .line 79 */
} // :cond_3
/* iget v0, p0, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->mCurVolumeStatusForBattery:I */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_4 */
/* .line 80 */
return;
/* .line 82 */
} // :cond_4
/* iput v1, p0, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->mCurVolumeStatusForBattery:I */
/* .line 84 */
} // :goto_0
v0 = this.mAudioManager;
(( android.media.AudioManager ) v0 ).getActivePlaybackConfigurations ( ); // invoke-virtual {v0}, Landroid/media/AudioManager;->getActivePlaybackConfigurations()Ljava/util/List;
v1 = } // :goto_1
if ( v1 != null) { // if-eqz v1, :cond_5
/* check-cast v1, Landroid/media/AudioPlaybackConfiguration; */
/* .line 85 */
/* .local v1, "apc":Landroid/media/AudioPlaybackConfiguration; */
/* invoke-direct {p0, v1}, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->adjustPlayerVolumeForBattery(Landroid/media/AudioPlaybackConfiguration;)V */
/* .line 86 */
} // .end local v1 # "apc":Landroid/media/AudioPlaybackConfiguration;
/* .line 88 */
} // :cond_5
} // :goto_2
return;
} // .end method
private void resumeVolumeForBattery ( ) {
/* .locals 2 */
/* .line 91 */
/* iget v0, p0, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->mCurVolumeStatusForBattery:I */
/* if-nez v0, :cond_0 */
/* .line 92 */
return;
/* .line 94 */
} // :cond_0
final String v0 = "FeatureAdapter.AudioPowerSaveHelper"; // const-string v0, "FeatureAdapter.AudioPowerSaveHelper"
final String v1 = "resume player volume for battery"; // const-string v1, "resume player volume for battery"
android.util.Log .d ( v0,v1 );
/* .line 95 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->mCurVolumeStatusForBattery:I */
/* .line 96 */
v0 = this.mAudioManager;
(( android.media.AudioManager ) v0 ).getActivePlaybackConfigurations ( ); // invoke-virtual {v0}, Landroid/media/AudioManager;->getActivePlaybackConfigurations()Ljava/util/List;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Landroid/media/AudioPlaybackConfiguration; */
/* .line 97 */
/* .local v1, "apc":Landroid/media/AudioPlaybackConfiguration; */
/* invoke-direct {p0, v1}, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->adjustPlayerVolumeForBattery(Landroid/media/AudioPlaybackConfiguration;)V */
/* .line 98 */
} // .end local v1 # "apc":Landroid/media/AudioPlaybackConfiguration;
/* .line 99 */
} // :cond_1
return;
} // .end method
/* # virtual methods */
public void handleLowBattery ( Integer p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "batteryPct" # I */
/* .param p2, "audioControlStatus" # I */
/* .line 49 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "handleLowBattery batteryPct: "; // const-string v1, "handleLowBattery batteryPct: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", audioControlStatus:"; // const-string v1, ", audioControlStatus:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "FeatureAdapter.AudioPowerSaveHelper"; // const-string v1, "FeatureAdapter.AudioPowerSaveHelper"
android.util.Log .d ( v1,v0 );
/* .line 51 */
/* packed-switch p2, :pswitch_data_0 */
/* .line 61 */
/* const-string/jumbo v0, "unknown audioControlStatus" */
android.util.Log .d ( v1,v0 );
/* .line 62 */
return;
/* .line 57 */
/* :pswitch_0 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "AUDIO_CONTROL_STATUS_VOLUME_CONTROL batteryPct:"; // const-string v2, "AUDIO_CONTROL_STATUS_VOLUME_CONTROL batteryPct:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v0 );
/* .line 58 */
/* invoke-direct {p0, p1}, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->controlVolumeForBattery(I)V */
/* .line 59 */
/* .line 53 */
/* :pswitch_1 */
final String v0 = "AUDIO_CONTROL_STATUS_VOLUME_RESUME"; // const-string v0, "AUDIO_CONTROL_STATUS_VOLUME_RESUME"
android.util.Log .d ( v1,v0 );
/* .line 54 */
/* invoke-direct {p0}, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->resumeVolumeForBattery()V */
/* .line 55 */
/* nop */
/* .line 64 */
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void initPlayerForBattery ( android.media.AudioPlaybackConfiguration p0 ) {
/* .locals 2 */
/* .param p1, "apc" # Landroid/media/AudioPlaybackConfiguration; */
/* .line 147 */
final String v0 = "FeatureAdapter.AudioPowerSaveHelper"; // const-string v0, "FeatureAdapter.AudioPowerSaveHelper"
final String v1 = "initPlayerForBattery"; // const-string v1, "initPlayerForBattery"
android.util.Log .d ( v0,v1 );
/* .line 148 */
/* iget v0, p0, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->mCurVolumeStatusForBattery:I */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 149 */
/* invoke-direct {p0, p1}, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->adjustPlayerVolumeForBattery(Landroid/media/AudioPlaybackConfiguration;)V */
/* .line 151 */
} // :cond_0
return;
} // .end method
