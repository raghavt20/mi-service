public class com.android.server.audio.feature.FeatureAdapter {
	 /* .source "FeatureAdapter.java" */
	 /* # static fields */
	 private static final Integer CONFIG_PROP;
	 public static final Boolean ENABLE;
	 private static final Integer FLAG_FEATURE_ADAPTER_ENABLE;
	 private static final Integer FLAG_LOW_BATTERY_ENABLE;
	 private static final Integer FLAG_POWER_SAVE_ENABLE;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private final Boolean LOW_BATTERY_ENABLE;
	 private final Boolean POWER_SAVE_ENABLE;
	 private com.android.server.audio.feature.AudioPowerSaveHelper mAudioPowerSaveHelper;
	 /* # direct methods */
	 static com.android.server.audio.feature.FeatureAdapter ( ) {
		 /* .locals 3 */
		 /* .line 14 */
		 /* nop */
		 /* .line 15 */
		 final String v0 = "ro.vendor.audio.feature.adapter"; // const-string v0, "ro.vendor.audio.feature.adapter"
		 int v1 = 0; // const/4 v1, 0x0
		 v0 = 		 android.os.SystemProperties .getInt ( v0,v1 );
		 /* .line 16 */
		 int v2 = 1; // const/4 v2, 0x1
		 /* and-int/2addr v0, v2 */
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* move v1, v2 */
		 } // :cond_0
		 com.android.server.audio.feature.FeatureAdapter.ENABLE = (v1!= 0);
		 return;
	 } // .end method
	 public com.android.server.audio.feature.FeatureAdapter ( ) {
		 /* .locals 4 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 22 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 17 */
		 /* and-int/lit8 v1, v0, 0x2 */
		 int v2 = 1; // const/4 v2, 0x1
		 int v3 = 0; // const/4 v3, 0x0
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 /* move v1, v2 */
		 } // :cond_0
		 /* move v1, v3 */
	 } // :goto_0
	 /* iput-boolean v1, p0, Lcom/android/server/audio/feature/FeatureAdapter;->LOW_BATTERY_ENABLE:Z */
	 /* .line 18 */
	 /* and-int/lit8 v0, v0, 0x2 */
	 if ( v0 != null) { // if-eqz v0, :cond_1
	 } // :cond_1
	 /* move v2, v3 */
} // :goto_1
/* iput-boolean v2, p0, Lcom/android/server/audio/feature/FeatureAdapter;->POWER_SAVE_ENABLE:Z */
/* .line 23 */
final String v0 = "AudioService.FeatureAdapter"; // const-string v0, "AudioService.FeatureAdapter"
final String v3 = "FeatureAdapter Construct ..."; // const-string v3, "FeatureAdapter Construct ..."
android.util.Log .d ( v0,v3 );
/* .line 25 */
/* if-nez v1, :cond_2 */
if ( v2 != null) { // if-eqz v2, :cond_3
	 /* .line 26 */
} // :cond_2
/* new-instance v0, Lcom/android/server/audio/feature/AudioPowerSaveHelper; */
/* invoke-direct {v0, p1}, Lcom/android/server/audio/feature/AudioPowerSaveHelper;-><init>(Landroid/content/Context;)V */
this.mAudioPowerSaveHelper = v0;
/* .line 28 */
} // :cond_3
return;
} // .end method
/* # virtual methods */
public void handleLowBattery ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "batteryPct" # I */
/* .param p2, "audioControlStatus" # I */
/* .line 31 */
/* iget-boolean v0, p0, Lcom/android/server/audio/feature/FeatureAdapter;->LOW_BATTERY_ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 32 */
v0 = this.mAudioPowerSaveHelper;
(( com.android.server.audio.feature.AudioPowerSaveHelper ) v0 ).handleLowBattery ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->handleLowBattery(II)V
/* .line 34 */
} // :cond_0
return;
} // .end method
public void onPlayerTracked ( android.media.AudioPlaybackConfiguration p0 ) {
/* .locals 1 */
/* .param p1, "apc" # Landroid/media/AudioPlaybackConfiguration; */
/* .line 37 */
/* iget-boolean v0, p0, Lcom/android/server/audio/feature/FeatureAdapter;->LOW_BATTERY_ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 38 */
v0 = this.mAudioPowerSaveHelper;
(( com.android.server.audio.feature.AudioPowerSaveHelper ) v0 ).initPlayerForBattery ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/audio/feature/AudioPowerSaveHelper;->initPlayerForBattery(Landroid/media/AudioPlaybackConfiguration;)V
/* .line 40 */
} // :cond_0
return;
} // .end method
