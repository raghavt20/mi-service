class com.android.server.audio.MQSUtils$AudioStateTrackData {
	 /* .source "MQSUtils.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/audio/MQSUtils; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = "AudioStateTrackData" */
} // .end annotation
/* # instance fields */
final Integer mA2dpConnectState;
final java.lang.String mAbnormalReason;
final java.lang.String mAudioStateName;
final java.lang.String mBtName;
final java.lang.String mEventSource;
final java.lang.String mPackageName;
final Integer mScoConnectState;
/* # direct methods */
public com.android.server.audio.MQSUtils$AudioStateTrackData ( ) {
/* .locals 1 */
/* .param p1, "errorType" # I */
/* .param p2, "eventSource" # Ljava/lang/String; */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .param p4, "btName" # Ljava/lang/String; */
/* .param p5, "a2dpConnectState" # I */
/* .param p6, "scoConnectState" # I */
/* .param p7, "audioStateName" # Ljava/lang/String; */
/* .line 684 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 685 */
com.android.server.audio.MQSUtils$AudioStateTrackData .audioErrorTypeToString ( p1 );
this.mAbnormalReason = v0;
/* .line 686 */
this.mEventSource = p2;
/* .line 687 */
this.mPackageName = p3;
/* .line 688 */
this.mBtName = p4;
/* .line 689 */
/* iput p5, p0, Lcom/android/server/audio/MQSUtils$AudioStateTrackData;->mA2dpConnectState:I */
/* .line 690 */
/* iput p6, p0, Lcom/android/server/audio/MQSUtils$AudioStateTrackData;->mScoConnectState:I */
/* .line 691 */
this.mAudioStateName = p7;
/* .line 692 */
return;
} // .end method
public static java.lang.String audioErrorTypeToString ( Integer p0 ) {
/* .locals 2 */
/* .param p0, "errorType" # I */
/* .line 695 */
/* packed-switch p0, :pswitch_data_0 */
/* .line 715 */
/* :pswitch_0 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "unknown error:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 713 */
/* :pswitch_1 */
final String v0 = "audioLeaky"; // const-string v0, "audioLeaky"
/* .line 711 */
/* :pswitch_2 */
final String v0 = "audioPolicyManagerException"; // const-string v0, "audioPolicyManagerException"
/* .line 709 */
/* :pswitch_3 */
final String v0 = "bluetoothScoException"; // const-string v0, "bluetoothScoException"
/* .line 707 */
/* :pswitch_4 */
final String v0 = "audioScoException"; // const-string v0, "audioScoException"
/* .line 705 */
/* :pswitch_5 */
final String v0 = "btConnectionException"; // const-string v0, "btConnectionException"
/* .line 703 */
/* :pswitch_6 */
final String v0 = "externalAppScoException"; // const-string v0, "externalAppScoException"
/* .line 701 */
/* :pswitch_7 */
final String v0 = "ScoTimeOutException"; // const-string v0, "ScoTimeOutException"
/* .line 699 */
/* :pswitch_8 */
final String v0 = "modeTimeOutException"; // const-string v0, "modeTimeOutException"
/* .line 697 */
/* :pswitch_9 */
final String v0 = "ok"; // const-string v0, "ok"
/* nop */
/* :pswitch_data_0 */
/* .packed-switch -0x1 */
/* :pswitch_9 */
/* :pswitch_0 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
/* # virtual methods */
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 721 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "AudioStateTrackData: mAbnormalReason="; // const-string v1, "AudioStateTrackData: mAbnormalReason="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mAbnormalReason;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", mEventSource="; // const-string v1, ", mEventSource="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mEventSource;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", mPackageName="; // const-string v1, ", mPackageName="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mPackageName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", mBtName="; // const-string v1, ", mBtName="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mBtName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", mScoConnectState="; // const-string v1, ", mScoConnectState="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/audio/MQSUtils$AudioStateTrackData;->mScoConnectState:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mA2dpConnectState="; // const-string v1, ", mA2dpConnectState="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/audio/MQSUtils$AudioStateTrackData;->mA2dpConnectState:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
