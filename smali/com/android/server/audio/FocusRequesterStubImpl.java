public class com.android.server.audio.FocusRequesterStubImpl implements com.android.server.audio.FocusRequesterStub {
	 /* .source "FocusRequesterStubImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private miui.greeze.IGreezeManager mIGreezeManager;
	 /* # direct methods */
	 static miui.greeze.IGreezeManager -$$Nest$mgetGreeze ( com.android.server.audio.FocusRequesterStubImpl p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0}, Lcom/android/server/audio/FocusRequesterStubImpl;->getGreeze()Lmiui/greeze/IGreezeManager; */
	 } // .end method
	 public com.android.server.audio.FocusRequesterStubImpl ( ) {
		 /* .locals 1 */
		 /* .line 16 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 19 */
		 int v0 = 0; // const/4 v0, 0x0
		 this.mIGreezeManager = v0;
		 return;
	 } // .end method
	 private miui.greeze.IGreezeManager getGreeze ( ) {
		 /* .locals 1 */
		 /* .line 22 */
		 v0 = this.mIGreezeManager;
		 /* if-nez v0, :cond_0 */
		 /* .line 23 */
		 /* nop */
		 /* .line 24 */
		 final String v0 = "greezer"; // const-string v0, "greezer"
		 android.os.ServiceManager .getService ( v0 );
		 /* .line 23 */
		 miui.greeze.IGreezeManager$Stub .asInterface ( v0 );
		 this.mIGreezeManager = v0;
		 /* .line 25 */
	 } // :cond_0
	 v0 = this.mIGreezeManager;
} // .end method
/* # virtual methods */
public void notifyFocusChange ( Integer p0, android.os.Handler p1, java.lang.String p2 ) {
	 /* .locals 2 */
	 /* .param p1, "uid" # I */
	 /* .param p2, "h" # Landroid/os/Handler; */
	 /* .param p3, "reason" # Ljava/lang/String; */
	 /* .line 29 */
	 /* invoke-direct {p0}, Lcom/android/server/audio/FocusRequesterStubImpl;->getGreeze()Lmiui/greeze/IGreezeManager; */
	 /* if-nez v0, :cond_0 */
	 /* .line 30 */
	 return;
	 /* .line 32 */
} // :cond_0
v0 = android.os.UserHandle .isApp ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_1
	 if ( p2 != null) { // if-eqz p2, :cond_1
		 /* .line 33 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v1 = "notifyFocusChange uid="; // const-string v1, "notifyFocusChange uid="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 final String v1 = "FocusRequesterStubImpl"; // const-string v1, "FocusRequesterStubImpl"
		 android.util.Log .d ( v1,v0 );
		 /* .line 34 */
		 /* new-instance v0, Lcom/android/server/audio/FocusRequesterStubImpl$1; */
		 /* invoke-direct {v0, p0, p1, p3}, Lcom/android/server/audio/FocusRequesterStubImpl$1;-><init>(Lcom/android/server/audio/FocusRequesterStubImpl;ILjava/lang/String;)V */
		 (( android.os.Handler ) p2 ).post ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
		 /* .line 48 */
	 } // :cond_1
	 return;
} // .end method
