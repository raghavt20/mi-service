.class public Lcom/android/server/audio/MiAudioService;
.super Lcom/android/server/audio/AudioService;
.source "MiAudioService.java"

# interfaces
.implements Landroid/media/Spatializer$OnSpatializerStateChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/audio/MiAudioService$SpatializerType;,
        Lcom/android/server/audio/MiAudioService$DeviceChangeBroadcastReceiver;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MiAudioService"


# instance fields
.field private final SPATIALIZER_PARAM_SPATIALIZER_TYPE:I

.field private final SPATIALIZER_PARAM_SPEAKER_ROTATION:I

.field private mAudioManager:Landroid/media/AudioManager;

.field private mDeviceConnectStateListener:Lcom/android/server/audio/MiAudioService$DeviceChangeBroadcastReceiver;

.field mDolbyEffectController:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

.field private mEffecImplementerObserver:Landroid/database/ContentObserver;

.field private mEffecImplementerUri:Landroid/net/Uri;

.field private final mIsSupportedDolbyEffectControl:Z

.field private final mIsSupportedSelectedDolbyTunningByVolume:Z

.field private mSpatializer:Landroid/media/Spatializer;

.field private mSpatializerEnabled:Z

.field private mSpatilizerType:I

.field private mSpeakerOn:Z

.field private final mUseXiaoMiSpatilizer:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetmSpatilizerType(Lcom/android/server/audio/MiAudioService;)I
    .locals 0

    iget p0, p0, Lcom/android/server/audio/MiAudioService;->mSpatilizerType:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmUseXiaoMiSpatilizer(Lcom/android/server/audio/MiAudioService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/audio/MiAudioService;->mUseXiaoMiSpatilizer:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmSpatilizerType(Lcom/android/server/audio/MiAudioService;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/audio/MiAudioService;->mSpatilizerType:I

    return-void
.end method

.method static bridge synthetic -$$Nest$mintToBytes(Lcom/android/server/audio/MiAudioService;I)[B
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/audio/MiAudioService;->intToBytes(I)[B

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .line 63
    invoke-static {}, Lcom/android/server/audio/AudioSystemAdapter;->getDefaultAdapter()Lcom/android/server/audio/AudioSystemAdapter;

    move-result-object v2

    .line 64
    invoke-static {p1}, Lcom/android/server/audio/SystemServerAdapter;->getDefaultAdapter(Landroid/content/Context;)Lcom/android/server/audio/SystemServerAdapter;

    move-result-object v3

    .line 65
    invoke-static {}, Lcom/android/server/audio/SettingsAdapter;->getDefaultAdapter()Lcom/android/server/audio/SettingsAdapter;

    move-result-object v4

    new-instance v5, Lcom/android/server/audio/DefaultAudioPolicyFacade;

    invoke-direct {v5}, Lcom/android/server/audio/DefaultAudioPolicyFacade;-><init>()V

    const/4 v6, 0x0

    .line 63
    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/android/server/audio/AudioService;-><init>(Landroid/content/Context;Lcom/android/server/audio/AudioSystemAdapter;Lcom/android/server/audio/SystemServerAdapter;Lcom/android/server/audio/SettingsAdapter;Lcom/android/server/audio/AudioPolicyFacade;Landroid/os/Looper;)V

    .line 43
    const/16 v0, 0x130

    iput v0, p0, Lcom/android/server/audio/MiAudioService;->SPATIALIZER_PARAM_SPEAKER_ROTATION:I

    .line 44
    const/16 v0, 0x120

    iput v0, p0, Lcom/android/server/audio/MiAudioService;->SPATIALIZER_PARAM_SPATIALIZER_TYPE:I

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/audio/MiAudioService;->mSpatializerEnabled:Z

    .line 46
    sget-object v1, Lcom/android/server/audio/MiAudioService$SpatializerType;->DOLBY:Lcom/android/server/audio/MiAudioService$SpatializerType;

    invoke-virtual {v1}, Lcom/android/server/audio/MiAudioService$SpatializerType;->ordinal()I

    move-result v1

    iput v1, p0, Lcom/android/server/audio/MiAudioService;->mSpatilizerType:I

    .line 47
    iput-boolean v0, p0, Lcom/android/server/audio/MiAudioService;->mSpeakerOn:Z

    .line 53
    nop

    .line 54
    const-string/jumbo v0, "vendor.audio.dolby.control.support"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/audio/MiAudioService;->mIsSupportedDolbyEffectControl:Z

    .line 55
    nop

    .line 56
    const-string/jumbo v0, "vendor.audio.dolby.control.tunning.by.volume.support"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/audio/MiAudioService;->mIsSupportedSelectedDolbyTunningByVolume:Z

    .line 57
    nop

    .line 58
    const-string/jumbo v0, "vendor.audio.useXiaomiSpatializer"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/audio/MiAudioService;->mUseXiaoMiSpatilizer:Z

    .line 68
    const-string v0, "MiAudioService"

    const-string v1, "MiAudioService()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    return-void
.end method

.method private intToBytes(I)[B
    .locals 3
    .param p1, "src"    # I

    .line 227
    const/4 v0, 0x4

    new-array v0, v0, [B

    .line 228
    .local v0, "result":[B
    shr-int/lit8 v1, p1, 0x18

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    const/4 v2, 0x3

    aput-byte v1, v0, v2

    .line 229
    shr-int/lit8 v1, p1, 0x10

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    const/4 v2, 0x2

    aput-byte v1, v0, v2

    .line 230
    shr-int/lit8 v1, p1, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    const/4 v2, 0x1

    aput-byte v1, v0, v2

    .line 231
    and-int/lit16 v1, p1, 0xff

    int-to-byte v1, v1

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 232
    return-object v0
.end method


# virtual methods
.method public DeviceConectedStateListenerInit()V
    .locals 3

    .line 113
    new-instance v0, Lcom/android/server/audio/MiAudioService$DeviceChangeBroadcastReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/audio/MiAudioService$DeviceChangeBroadcastReceiver;-><init>(Lcom/android/server/audio/MiAudioService;Lcom/android/server/audio/MiAudioService$DeviceChangeBroadcastReceiver-IA;)V

    iput-object v0, p0, Lcom/android/server/audio/MiAudioService;->mDeviceConnectStateListener:Lcom/android/server/audio/MiAudioService$DeviceChangeBroadcastReceiver;

    .line 114
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 115
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 117
    const-string v1, "android.bluetooth.headset.action.VENDOR_SPECIFIC_HEADSET_EVENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 118
    const-string v1, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 119
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "android.bluetooth.headset.intent.category.companyid."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 120
    const/16 v2, 0x38f

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 119
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 121
    iget-object v1, p0, Lcom/android/server/audio/MiAudioService;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/audio/MiAudioService;->mDeviceConnectStateListener:Lcom/android/server/audio/MiAudioService$DeviceChangeBroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 122
    return-void
.end method

.method public DolbyEffectControllerInit()V
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/android/server/audio/MiAudioService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->getInstance(Landroid/content/Context;)Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/audio/MiAudioService;->mDolbyEffectController:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    .line 88
    if-eqz v0, :cond_0

    .line 89
    invoke-virtual {v0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->init()V

    .line 91
    :cond_0
    return-void
.end method

.method public SettingsObserver()V
    .locals 4

    .line 135
    const-string v0, "effect_implementer"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/audio/MiAudioService;->mEffecImplementerUri:Landroid/net/Uri;

    .line 136
    new-instance v0, Lcom/android/server/audio/MiAudioService$1;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/audio/MiAudioService$1;-><init>(Lcom/android/server/audio/MiAudioService;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/audio/MiAudioService;->mEffecImplementerObserver:Landroid/database/ContentObserver;

    .line 159
    iget-object v0, p0, Lcom/android/server/audio/MiAudioService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/audio/MiAudioService;->mEffecImplementerUri:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/server/audio/MiAudioService;->mEffecImplementerObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 160
    return-void
.end method

.method public SpatialStateInit()V
    .locals 3

    .line 94
    iget-object v0, p0, Lcom/android/server/audio/MiAudioService;->mContext:Landroid/content/Context;

    const-class v1, Landroid/media/AudioManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/android/server/audio/MiAudioService;->mAudioManager:Landroid/media/AudioManager;

    .line 95
    if-eqz v0, :cond_0

    .line 96
    invoke-virtual {v0}, Landroid/media/AudioManager;->getSpatializer()Landroid/media/Spatializer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/audio/MiAudioService;->mSpatializer:Landroid/media/Spatializer;

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/android/server/audio/MiAudioService;->mSpatializer:Landroid/media/Spatializer;

    if-eqz v0, :cond_1

    .line 99
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Landroid/media/Spatializer;->addOnSpatializerStateChangedListener(Ljava/util/concurrent/Executor;Landroid/media/Spatializer$OnSpatializerStateChangedListener;)V

    .line 101
    :cond_1
    iget-object v0, p0, Lcom/android/server/audio/MiAudioService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "effect_implementer"

    invoke-static {v0, v1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 102
    .local v0, "effecImplementer":Ljava/lang/String;
    const-string v1, "dolby"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 103
    sget-object v1, Lcom/android/server/audio/MiAudioService$SpatializerType;->DOLBY:Lcom/android/server/audio/MiAudioService$SpatializerType;

    invoke-virtual {v1}, Lcom/android/server/audio/MiAudioService$SpatializerType;->ordinal()I

    move-result v1

    iput v1, p0, Lcom/android/server/audio/MiAudioService;->mSpatilizerType:I

    goto :goto_0

    .line 104
    :cond_2
    const-string v1, "misound"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 105
    sget-object v1, Lcom/android/server/audio/MiAudioService$SpatializerType;->MISOUND:Lcom/android/server/audio/MiAudioService$SpatializerType;

    invoke-virtual {v1}, Lcom/android/server/audio/MiAudioService$SpatializerType;->ordinal()I

    move-result v1

    iput v1, p0, Lcom/android/server/audio/MiAudioService;->mSpatilizerType:I

    goto :goto_0

    .line 106
    :cond_3
    const-string v1, "none"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 107
    sget-object v1, Lcom/android/server/audio/MiAudioService$SpatializerType;->NONE:Lcom/android/server/audio/MiAudioService$SpatializerType;

    invoke-virtual {v1}, Lcom/android/server/audio/MiAudioService$SpatializerType;->ordinal()I

    move-result v1

    iput v1, p0, Lcom/android/server/audio/MiAudioService;->mSpatilizerType:I

    .line 109
    :cond_4
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Init EffectImplementer is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiAudioService"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    return-void
.end method

.method protected adjustStreamVolume(IIILjava/lang/String;Ljava/lang/String;IILjava/lang/String;ZI)V
    .locals 3
    .param p1, "streamType"    # I
    .param p2, "direction"    # I
    .param p3, "flags"    # I
    .param p4, "callingPackage"    # Ljava/lang/String;
    .param p5, "caller"    # Ljava/lang/String;
    .param p6, "uid"    # I
    .param p7, "pid"    # I
    .param p8, "attributionTag"    # Ljava/lang/String;
    .param p9, "hasModifyAudioSettings"    # Z
    .param p10, "keyEventMode"    # I

    .line 214
    invoke-super/range {p0 .. p10}, Lcom/android/server/audio/AudioService;->adjustStreamVolume(IIILjava/lang/String;Ljava/lang/String;IILjava/lang/String;ZI)V

    .line 215
    iget-object v0, p0, Lcom/android/server/audio/MiAudioService;->mDolbyEffectController:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    if-eqz v0, :cond_1

    .line 216
    iget-boolean v0, p0, Lcom/android/server/audio/MiAudioService;->mIsSupportedSelectedDolbyTunningByVolume:Z

    if-nez v0, :cond_0

    .line 217
    return-void

    .line 218
    :cond_0
    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 219
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/android/server/audio/MiAudioService;->getDeviceStreamVolume(II)I

    move-result v0

    .line 220
    .local v0, "newVolume":I
    const-string v1, "MiAudioService"

    const-string v2, "adjustStreamVolume"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    iget-object v1, p0, Lcom/android/server/audio/MiAudioService;->mDolbyEffectController:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    invoke-virtual {v1, v0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->receiveVolumeChanged(I)V

    .line 224
    .end local v0    # "newVolume":I
    :cond_1
    return-void
.end method

.method public functionsRoadedInit()V
    .locals 1

    .line 78
    iget-boolean v0, p0, Lcom/android/server/audio/MiAudioService;->mIsSupportedDolbyEffectControl:Z

    if-eqz v0, :cond_0

    .line 79
    invoke-virtual {p0}, Lcom/android/server/audio/MiAudioService;->DolbyEffectControllerInit()V

    .line 80
    invoke-virtual {p0}, Lcom/android/server/audio/MiAudioService;->DeviceConectedStateListenerInit()V

    .line 82
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/audio/MiAudioService;->SettingsObserver()V

    .line 83
    invoke-virtual {p0}, Lcom/android/server/audio/MiAudioService;->SpatialStateInit()V

    .line 84
    return-void
.end method

.method public onRotationUpdate(Ljava/lang/Integer;)V
    .locals 5
    .param p1, "rotation"    # Ljava/lang/Integer;

    .line 193
    invoke-super {p0, p1}, Lcom/android/server/audio/AudioService;->onRotationUpdate(Ljava/lang/Integer;)V

    .line 194
    iget-object v0, p0, Lcom/android/server/audio/MiAudioService;->mDolbyEffectController:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    const-string v1, "MiAudioService"

    if-eqz v0, :cond_0

    .line 195
    const-string v0, "onRotationUpdate, receiveRotationChanged"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    iget-object v0, p0, Lcom/android/server/audio/MiAudioService;->mDolbyEffectController:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->receiveRotationChanged(I)V

    .line 198
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/audio/MiAudioService;->mUseXiaoMiSpatilizer:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/server/audio/MiAudioService;->isSpatializerEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/server/audio/MiAudioService;->isSpatializerAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 199
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/server/audio/MiAudioService;->intToBytes(I)[B

    move-result-object v0

    .line 201
    .local v0, "Rotation":[B
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setSpatializerParameter SPATIALIZER_PARAM_SPEAKER_ROTATION = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    const/16 v2, 0x130

    invoke-virtual {p0, v2, v0}, Lcom/android/server/audio/MiAudioService;->setSpatializerParameter(I[B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 205
    goto :goto_0

    .line 203
    :catch_0
    move-exception v2

    .line 204
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setSpatializerParameter SPATIALIZER_PARAM_SPEAKER_ROTATION Exception "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    .end local v0    # "Rotation":[B
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_0
    return-void
.end method

.method public onSpatializerAvailableChanged(Landroid/media/Spatializer;Z)V
    .locals 2
    .param p1, "spat"    # Landroid/media/Spatializer;
    .param p2, "available"    # Z

    .line 185
    iget-object v0, p0, Lcom/android/server/audio/MiAudioService;->mDolbyEffectController:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    if-eqz v0, :cond_0

    .line 186
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onSpatializerAvailableChanged: available = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiAudioService"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    iget-object v0, p0, Lcom/android/server/audio/MiAudioService;->mDolbyEffectController:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    invoke-virtual {v0, p2}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->receiveSpatializerAvailableChanged(Z)V

    .line 189
    :cond_0
    return-void
.end method

.method public onSpatializerEnabledChanged(Landroid/media/Spatializer;Z)V
    .locals 4
    .param p1, "spat"    # Landroid/media/Spatializer;
    .param p2, "enabled"    # Z

    .line 164
    iput-boolean p2, p0, Lcom/android/server/audio/MiAudioService;->mSpatializerEnabled:Z

    .line 165
    iget-object v0, p0, Lcom/android/server/audio/MiAudioService;->mDolbyEffectController:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    const-string v1, "MiAudioService"

    if-eqz v0, :cond_0

    .line 166
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSpatializerEnabledChanged: enabled = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    iget-object v0, p0, Lcom/android/server/audio/MiAudioService;->mDolbyEffectController:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    invoke-virtual {v0, p2}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->receiveSpatializerEnabledChanged(Z)V

    .line 169
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/audio/MiAudioService;->mUseXiaoMiSpatilizer:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/audio/MiAudioService;->mSpatializerEnabled:Z

    if-eqz v0, :cond_1

    .line 171
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSpatializerEnabledChanged: setSpatializerParameter mSpatilizerType = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/server/audio/MiAudioService;->mSpatilizerType:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    iget v0, p0, Lcom/android/server/audio/MiAudioService;->mSpatilizerType:I

    invoke-direct {p0, v0}, Lcom/android/server/audio/MiAudioService;->intToBytes(I)[B

    move-result-object v0

    const/16 v2, 0x120

    invoke-virtual {p0, v2, v0}, Lcom/android/server/audio/MiAudioService;->setSpatializerParameter(I[B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 175
    goto :goto_0

    .line 173
    :catch_0
    move-exception v0

    .line 174
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onSpatializerEnabledChanged Exception:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_0
    const-string v0, "onSpatializerEnabledChanged notifyEffectChanged"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.ACTION_AUDIO_EFFECT_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 180
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/android/server/audio/MiAudioService;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 181
    return-void
.end method

.method public onSystemReady()V
    .locals 0

    .line 73
    invoke-super {p0}, Lcom/android/server/audio/AudioService;->onSystemReady()V

    .line 74
    invoke-virtual {p0}, Lcom/android/server/audio/MiAudioService;->functionsRoadedInit()V

    .line 75
    return-void
.end method
