.class Lcom/android/server/audio/MiAudioService$1;
.super Landroid/database/ContentObserver;
.source "MiAudioService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/audio/MiAudioService;->SettingsObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/audio/MiAudioService;


# direct methods
.method constructor <init>(Lcom/android/server/audio/MiAudioService;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/audio/MiAudioService;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 136
    iput-object p1, p0, Lcom/android/server/audio/MiAudioService$1;->this$0:Lcom/android/server/audio/MiAudioService;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 5
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 139
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 140
    iget-object v0, p0, Lcom/android/server/audio/MiAudioService$1;->this$0:Lcom/android/server/audio/MiAudioService;

    iget-object v0, v0, Lcom/android/server/audio/MiAudioService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "effect_implementer"

    invoke-static {v0, v1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 141
    .local v0, "effecImplementer":Ljava/lang/String;
    const-string v1, "dolby"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 142
    iget-object v1, p0, Lcom/android/server/audio/MiAudioService$1;->this$0:Lcom/android/server/audio/MiAudioService;

    sget-object v2, Lcom/android/server/audio/MiAudioService$SpatializerType;->DOLBY:Lcom/android/server/audio/MiAudioService$SpatializerType;

    invoke-virtual {v2}, Lcom/android/server/audio/MiAudioService$SpatializerType;->ordinal()I

    move-result v2

    invoke-static {v1, v2}, Lcom/android/server/audio/MiAudioService;->-$$Nest$fputmSpatilizerType(Lcom/android/server/audio/MiAudioService;I)V

    goto :goto_0

    .line 143
    :cond_0
    const-string v1, "misound"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 144
    iget-object v1, p0, Lcom/android/server/audio/MiAudioService$1;->this$0:Lcom/android/server/audio/MiAudioService;

    sget-object v2, Lcom/android/server/audio/MiAudioService$SpatializerType;->MISOUND:Lcom/android/server/audio/MiAudioService$SpatializerType;

    invoke-virtual {v2}, Lcom/android/server/audio/MiAudioService$SpatializerType;->ordinal()I

    move-result v2

    invoke-static {v1, v2}, Lcom/android/server/audio/MiAudioService;->-$$Nest$fputmSpatilizerType(Lcom/android/server/audio/MiAudioService;I)V

    goto :goto_0

    .line 145
    :cond_1
    const-string v1, "none"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 146
    iget-object v1, p0, Lcom/android/server/audio/MiAudioService$1;->this$0:Lcom/android/server/audio/MiAudioService;

    sget-object v2, Lcom/android/server/audio/MiAudioService$SpatializerType;->NONE:Lcom/android/server/audio/MiAudioService$SpatializerType;

    invoke-virtual {v2}, Lcom/android/server/audio/MiAudioService$SpatializerType;->ordinal()I

    move-result v2

    invoke-static {v1, v2}, Lcom/android/server/audio/MiAudioService;->-$$Nest$fputmSpatilizerType(Lcom/android/server/audio/MiAudioService;I)V

    .line 148
    :cond_2
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Current EffectImplementer is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiAudioService"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    iget-object v1, p0, Lcom/android/server/audio/MiAudioService$1;->this$0:Lcom/android/server/audio/MiAudioService;

    invoke-static {v1}, Lcom/android/server/audio/MiAudioService;->-$$Nest$fgetmUseXiaoMiSpatilizer(Lcom/android/server/audio/MiAudioService;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/server/audio/MiAudioService$1;->this$0:Lcom/android/server/audio/MiAudioService;

    invoke-virtual {v1}, Lcom/android/server/audio/MiAudioService;->isSpatializerEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/server/audio/MiAudioService$1;->this$0:Lcom/android/server/audio/MiAudioService;

    invoke-virtual {v1}, Lcom/android/server/audio/MiAudioService;->isSpatializerAvailable()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 150
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SettingsObserver setSpatializerParameter spatilizerType = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/audio/MiAudioService$1;->this$0:Lcom/android/server/audio/MiAudioService;

    invoke-static {v3}, Lcom/android/server/audio/MiAudioService;->-$$Nest$fgetmSpatilizerType(Lcom/android/server/audio/MiAudioService;)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    :try_start_0
    iget-object v1, p0, Lcom/android/server/audio/MiAudioService$1;->this$0:Lcom/android/server/audio/MiAudioService;

    invoke-static {v1}, Lcom/android/server/audio/MiAudioService;->-$$Nest$fgetmSpatilizerType(Lcom/android/server/audio/MiAudioService;)I

    move-result v3

    invoke-static {v1, v3}, Lcom/android/server/audio/MiAudioService;->-$$Nest$mintToBytes(Lcom/android/server/audio/MiAudioService;I)[B

    move-result-object v3

    const/16 v4, 0x120

    invoke-virtual {v1, v4, v3}, Lcom/android/server/audio/MiAudioService;->setSpatializerParameter(I[B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 155
    goto :goto_1

    .line 153
    :catch_0
    move-exception v1

    .line 154
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SettingsObserver Exception "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_3
    :goto_1
    return-void
.end method
