.class public Lcom/android/server/audio/SpatializerHelperStubImpl;
.super Ljava/lang/Object;
.source "SpatializerHelperStubImpl.java"

# interfaces
.implements Lcom/android/server/audio/SpatializerHelperStub;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public disablePropertyByMode(I)V
    .locals 1
    .param p1, "mode"    # I

    .line 32
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 33
    const-string/jumbo v0, "vendor.spatial.headtrack=0"

    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 36
    :cond_0
    return-void
.end method

.method public setPropertyByMode(ZI)V
    .locals 1
    .param p1, "isHeadTrackerAvailable"    # Z
    .param p2, "mode"    # I

    .line 17
    if-eqz p1, :cond_1

    .line 18
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 19
    const-string/jumbo v0, "vendor.spatial.headtrack=1"

    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    goto :goto_0

    .line 21
    :cond_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    .line 22
    const-string/jumbo v0, "vendor.spatial.headtrack=0"

    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    goto :goto_0

    .line 26
    :cond_1
    const-string/jumbo v0, "vendor.spatial.headtrack=-1"

    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 29
    :cond_2
    :goto_0
    return-void
.end method
