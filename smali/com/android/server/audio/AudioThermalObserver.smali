.class public Lcom/android/server/audio/AudioThermalObserver;
.super Ljava/lang/Object;
.source "AudioThermalObserver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/audio/AudioThermalObserver$AudioHighTemperatureListener;,
        Lcom/android/server/audio/AudioThermalObserver$WorkHandler;
    }
.end annotation


# static fields
.field private static final AUDIO_HIGH_TEMPERATURE_STATE_PATH:Ljava/lang/String; = "/sys/class/thermal/thermal_message/video_mode"

.field public static final MSG_AUDIO_THERMAL_POLICY:I = 0x9c4

.field public static final MSG_CHECK_AUDIO_HIGH_TEMPERATURE_POLICY:I = 0x9c6

.field public static final MSG_SET_AUDIO_HIGH_TEMPERATURE_POLICY:I = 0x9c5

.field private static final READ_WAIT_TIME_SECONDS:I = 0x3

.field private static final TAG:Ljava/lang/String; = "AudioThermalObserver"


# instance fields
.field private mAudioHighTemperatureListener:Lcom/android/server/audio/AudioThermalObserver$AudioHighTemperatureListener;

.field private mContext:Landroid/content/Context;

.field private mHandler:Lcom/android/server/audio/AudioThermalObserver$WorkHandler;

.field private mTempRecord:I


# direct methods
.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/audio/AudioThermalObserver;)Lcom/android/server/audio/AudioThermalObserver$WorkHandler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/audio/AudioThermalObserver;->mHandler:Lcom/android/server/audio/AudioThermalObserver$WorkHandler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mcheckAudioHighTemperatureMode(Lcom/android/server/audio/AudioThermalObserver;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/audio/AudioThermalObserver;->checkAudioHighTemperatureMode(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetAudioHighTemperatureMode(Lcom/android/server/audio/AudioThermalObserver;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/audio/AudioThermalObserver;->setAudioHighTemperatureMode(I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/audio/AudioThermalObserver;->mAudioHighTemperatureListener:Lcom/android/server/audio/AudioThermalObserver$AudioHighTemperatureListener;

    .line 51
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/audio/AudioThermalObserver;->mTempRecord:I

    .line 55
    iput v0, p0, Lcom/android/server/audio/AudioThermalObserver;->mTempRecord:I

    .line 56
    iput-object p1, p0, Lcom/android/server/audio/AudioThermalObserver;->mContext:Landroid/content/Context;

    .line 57
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "AudioThermalObserver"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 58
    .local v0, "thread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 59
    new-instance v1, Lcom/android/server/audio/AudioThermalObserver$WorkHandler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/server/audio/AudioThermalObserver$WorkHandler;-><init>(Lcom/android/server/audio/AudioThermalObserver;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/server/audio/AudioThermalObserver;->mHandler:Lcom/android/server/audio/AudioThermalObserver$WorkHandler;

    .line 60
    new-instance v1, Lcom/android/server/audio/AudioThermalObserver$AudioHighTemperatureListener;

    const-string v2, "/sys/class/thermal/thermal_message/video_mode"

    invoke-direct {v1, p0, v2}, Lcom/android/server/audio/AudioThermalObserver$AudioHighTemperatureListener;-><init>(Lcom/android/server/audio/AudioThermalObserver;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/server/audio/AudioThermalObserver;->mAudioHighTemperatureListener:Lcom/android/server/audio/AudioThermalObserver$AudioHighTemperatureListener;

    .line 61
    invoke-direct {p0}, Lcom/android/server/audio/AudioThermalObserver;->watchAudioHighTemperatureListener()V

    .line 62
    return-void
.end method

.method private checkAudioHighTemperatureMode(Ljava/lang/String;)V
    .locals 6
    .param p1, "path"    # Ljava/lang/String;

    .line 108
    const-string v0, "AudioThermalObserver"

    const/4 v1, 0x0

    .line 109
    .local v1, "mMode":Ljava/lang/String;
    const/4 v2, 0x0

    .line 111
    .local v2, "mTempMode":I
    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/server/audio/AudioThermalObserver;->getContentFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v1, v3

    if-eqz v3, :cond_0

    .line 112
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    move v2, v3

    .line 113
    iget-object v3, p0, Lcom/android/server/audio/AudioThermalObserver;->mHandler:Lcom/android/server/audio/AudioThermalObserver$WorkHandler;

    const/16 v4, 0x9c5

    invoke-virtual {v3, v4}, Lcom/android/server/audio/AudioThermalObserver$WorkHandler;->removeMessages(I)V

    .line 114
    iget-object v3, p0, Lcom/android/server/audio/AudioThermalObserver;->mHandler:Lcom/android/server/audio/AudioThermalObserver$WorkHandler;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v2, v5}, Lcom/android/server/audio/AudioThermalObserver$WorkHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/server/audio/AudioThermalObserver$WorkHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 117
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HighTemperature path:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "is null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :goto_0
    goto :goto_1

    .line 119
    :catch_0
    move-exception v3

    .line 120
    .local v3, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkAudioHighTemperatureMode "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method

.method private getContentFromFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "filePath"    # Ljava/lang/String;

    .line 125
    const-string v0, "can not get temp state "

    const-string v1, "AudioThermalObserver"

    const/4 v2, 0x0

    .line 126
    .local v2, "is":Ljava/io/FileInputStream;
    const-string v3, ""

    .line 128
    .local v3, "content":Ljava/lang/String;
    :try_start_0
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 129
    .local v4, "file":Ljava/io/File;
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    move-object v2, v5

    .line 130
    invoke-static {v2}, Lcom/android/server/audio/AudioThermalObserver;->readInputStream(Ljava/io/FileInputStream;)[B

    move-result-object v5

    .line 131
    .local v5, "data":[B
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v5}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v3, v6

    .line 139
    .end local v4    # "file":Ljava/io/File;
    .end local v5    # "data":[B
    nop

    .line 141
    :try_start_1
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 144
    :goto_0
    goto/16 :goto_2

    .line 142
    :catch_0
    move-exception v4

    .line 143
    .local v4, "e":Ljava/io/IOException;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    :goto_1
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .end local v4    # "e":Ljava/io/IOException;
    goto :goto_0

    .line 139
    :catchall_0
    move-exception v4

    goto/16 :goto_3

    .line 136
    :catch_1
    move-exception v4

    .line 137
    .local v4, "e":Ljava/lang/IndexOutOfBoundsException;
    :try_start_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "index exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 139
    nop

    .end local v4    # "e":Ljava/lang/IndexOutOfBoundsException;
    if-eqz v2, :cond_0

    .line 141
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 142
    :catch_2
    move-exception v4

    .line 143
    .local v4, "e":Ljava/io/IOException;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_1

    .line 134
    .end local v4    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v4

    .line 135
    .restart local v4    # "e":Ljava/io/IOException;
    :try_start_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IO exception when read file "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 139
    nop

    .end local v4    # "e":Ljava/io/IOException;
    if-eqz v2, :cond_0

    .line 141
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_0

    .line 142
    :catch_4
    move-exception v4

    .line 143
    .restart local v4    # "e":Ljava/io/IOException;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_1

    .line 132
    .end local v4    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v4

    .line 133
    .local v4, "e":Ljava/io/FileNotFoundException;
    :try_start_6
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "can\'t find file "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 139
    nop

    .end local v4    # "e":Ljava/io/FileNotFoundException;
    if-eqz v2, :cond_0

    .line 141
    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    goto/16 :goto_0

    .line 142
    :catch_6
    move-exception v4

    .line 143
    .local v4, "e":Ljava/io/IOException;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto/16 :goto_1

    .line 147
    .end local v4    # "e":Ljava/io/IOException;
    :cond_0
    :goto_2
    return-object v3

    .line 139
    :goto_3
    if-eqz v2, :cond_1

    .line 141
    :try_start_8
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_7

    .line 144
    goto :goto_4

    .line 142
    :catch_7
    move-exception v5

    .line 143
    .local v5, "e":Ljava/io/IOException;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    .end local v5    # "e":Ljava/io/IOException;
    :cond_1
    :goto_4
    throw v4
.end method

.method private static readInputStream(Ljava/io/FileInputStream;)[B
    .locals 9
    .param p0, "is"    # Ljava/io/FileInputStream;

    .line 194
    const-string v0, "readInputStream "

    const-string v1, "AudioThermalObserver"

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 195
    .local v2, "byteStream":Ljava/io/ByteArrayOutputStream;
    const/16 v3, 0x200

    .line 196
    .local v3, "blockSize":I
    const/16 v4, 0x200

    new-array v5, v4, [B

    .line 197
    .local v5, "buffer":[B
    const/4 v6, 0x0

    .line 199
    .local v6, "count":I
    :goto_0
    const/4 v7, 0x0

    :try_start_0
    invoke-virtual {p0, v5, v7, v4}, Ljava/io/FileInputStream;->read([BII)I

    move-result v8

    move v6, v8

    if-lez v8, :cond_0

    .line 200
    invoke-virtual {v2, v5, v7, v6}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 202
    :cond_0
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 207
    :try_start_1
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 210
    goto :goto_1

    .line 208
    :catch_0
    move-exception v7

    .line 209
    .local v7, "e":Ljava/io/IOException;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    .end local v7    # "e":Ljava/io/IOException;
    :goto_1
    return-object v4

    .line 206
    :catchall_0
    move-exception v4

    goto :goto_3

    .line 203
    :catch_1
    move-exception v4

    .line 204
    .local v4, "e":Ljava/lang/Exception;
    :try_start_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 207
    .end local v4    # "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 210
    goto :goto_2

    .line 208
    :catch_2
    move-exception v4

    .line 209
    .local v4, "e":Ljava/io/IOException;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    .end local v4    # "e":Ljava/io/IOException;
    nop

    .line 212
    :goto_2
    const/4 v0, 0x0

    return-object v0

    .line 207
    :goto_3
    :try_start_4
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 210
    goto :goto_4

    .line 208
    :catch_3
    move-exception v7

    .line 209
    .restart local v7    # "e":Ljava/io/IOException;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    .end local v7    # "e":Ljava/io/IOException;
    :goto_4
    throw v4
.end method

.method private setAudioHighTemperatureMode(I)V
    .locals 5
    .param p1, "mHighTemperatureMode"    # I

    .line 90
    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 92
    :cond_0
    iget v0, p0, Lcom/android/server/audio/AudioThermalObserver;->mTempRecord:I

    const-string v1, "AudioThermalObserver"

    if-eq v0, p1, :cond_1

    .line 94
    :try_start_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 95
    .local v0, "mode":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Audio_High_Temperature_Mode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 96
    .local v2, "mParameter":Ljava/lang/String;
    invoke-static {v2}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 97
    iput p1, p0, Lcom/android/server/audio/AudioThermalObserver;->mTempRecord:I

    .line 98
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setAudioHighTemperatureMode: mHighTemperatureMode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; mTempRecord = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/server/audio/AudioThermalObserver;->mTempRecord:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    nop

    .end local v0    # "mode":Ljava/lang/String;
    .end local v2    # "mParameter":Ljava/lang/String;
    goto :goto_0

    .line 99
    :catch_0
    move-exception v0

    .line 100
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setAudioHighTemperatureMode exception "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    .end local v0    # "e":Ljava/lang/Exception;
    goto :goto_0

    .line 103
    :cond_1
    const-string v0, "AudioHighTemperatureMode is no need to change"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    :goto_0
    return-void
.end method

.method private toSleep()V
    .locals 3

    .line 152
    :try_start_0
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x3

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 155
    goto :goto_0

    .line 153
    :catch_0
    move-exception v0

    .line 154
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "toSleep exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AudioThermalObserver"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private unwatchAudioHighTemperatureListener()V
    .locals 3

    .line 160
    :try_start_0
    iget-object v0, p0, Lcom/android/server/audio/AudioThermalObserver;->mAudioHighTemperatureListener:Lcom/android/server/audio/AudioThermalObserver$AudioHighTemperatureListener;

    invoke-virtual {v0}, Lcom/android/server/audio/AudioThermalObserver$AudioHighTemperatureListener;->stopWatching()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    goto :goto_0

    .line 161
    :catch_0
    move-exception v0

    .line 162
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unwatchAudioHighTemperatureListener exception "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AudioThermalObserver"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private watchAudioHighTemperatureListener()V
    .locals 3

    .line 168
    :try_start_0
    iget-object v0, p0, Lcom/android/server/audio/AudioThermalObserver;->mAudioHighTemperatureListener:Lcom/android/server/audio/AudioThermalObserver$AudioHighTemperatureListener;

    invoke-virtual {v0}, Lcom/android/server/audio/AudioThermalObserver$AudioHighTemperatureListener;->startWatching()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 171
    goto :goto_0

    .line 169
    :catch_0
    move-exception v0

    .line 170
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "watchAudioHighTemperatureListener exception "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AudioThermalObserver"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method


# virtual methods
.method public reSetAudioCinemaModeThermal()V
    .locals 3

    .line 217
    :try_start_0
    iget-object v0, p0, Lcom/android/server/audio/AudioThermalObserver;->mHandler:Lcom/android/server/audio/AudioThermalObserver$WorkHandler;

    const/16 v1, 0x9c6

    invoke-virtual {v0, v1}, Lcom/android/server/audio/AudioThermalObserver$WorkHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/audio/AudioThermalObserver$WorkHandler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    goto :goto_0

    .line 218
    :catch_0
    move-exception v0

    .line 219
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "reSetAudioCinemaModeThermal exception "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AudioThermalObserver"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method
