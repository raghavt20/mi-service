class com.android.server.audio.CloudServiceSettings$FileDownloadSetting extends com.android.server.audio.CloudServiceSettings$Setting {
	 /* .source "CloudServiceSettings.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/audio/CloudServiceSettings; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "FileDownloadSetting" */
} // .end annotation
/* # static fields */
private static final java.lang.String DATA;
private static final java.lang.String ENCODING;
private static final java.lang.String PATH;
/* # instance fields */
private java.lang.StringBuilder mAbsolutePath;
private java.lang.StringBuilder mData;
private java.lang.String mEncoding;
final com.android.server.audio.CloudServiceSettings this$0; //synthetic
/* # direct methods */
 com.android.server.audio.CloudServiceSettings$FileDownloadSetting ( ) {
/* .locals 3 */
/* .param p1, "this$0" # Lcom/android/server/audio/CloudServiceSettings; */
/* .param p2, "json" # Lorg/json/JSONObject; */
/* .line 429 */
this.this$0 = p1;
/* .line 430 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/audio/CloudServiceSettings$Setting;-><init>(Lcom/android/server/audio/CloudServiceSettings;Lorg/json/JSONObject;)V */
/* .line 432 */
try { // :try_start_0
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 final String v1 = "absolutePath"; // const-string v1, "absolutePath"
	 (( org.json.JSONObject ) p2 ).getString ( v1 ); // invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
	 /* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
	 this.mAbsolutePath = v0;
	 /* .line 433 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 final String v1 = "data"; // const-string v1, "data"
	 (( org.json.JSONObject ) p2 ).getString ( v1 ); // invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
	 /* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
	 this.mData = v0;
	 /* .line 434 */
	 /* iget-boolean v0, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mSecreted:Z */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 435 */
		 v0 = this.mSecretedItem;
		 v1 = this.mAbsolutePath;
		 (( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
		 /* .line 436 */
		 v0 = this.mSecretedItem;
		 v1 = this.mData;
		 (( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
		 /* .line 438 */
	 } // :cond_0
	 final String v0 = "encoding"; // const-string v0, "encoding"
	 final String v1 = "B64"; // const-string v1, "B64"
	 (( org.json.JSONObject ) p2 ).optString ( v0, v1 ); // invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
	 this.mEncoding = v0;
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 441 */
	 /* .line 439 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 440 */
	 /* .local v0, "e":Ljava/lang/Exception; */
	 final String v1 = "CloudServiceSettings"; // const-string v1, "CloudServiceSettings"
	 final String v2 = "fail to parse FileDownloadSetting"; // const-string v2, "fail to parse FileDownloadSetting"
	 android.util.Log .e ( v1,v2 );
	 /* .line 442 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
/* # virtual methods */
public Boolean set ( ) {
/* .locals 15 */
/* .line 446 */
v0 = /* invoke-super {p0}, Lcom/android/server/audio/CloudServiceSettings$Setting;->set()Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 447 */
/* .line 448 */
} // :cond_0
v0 = this.this$0;
com.android.server.audio.CloudServiceSettings .-$$Nest$fgetiQConfig ( v0 );
final String v2 = "CloudServiceSettings"; // const-string v2, "CloudServiceSettings"
/* if-nez v0, :cond_1 */
v0 = com.android.server.audio.CloudServiceSettings.mHidlmisysV2;
/* if-nez v0, :cond_1 */
/* .line 449 */
final String v0 = "fail to write file, misys is null"; // const-string v0, "fail to write file, misys is null"
android.util.Log .d ( v2,v0 );
/* .line 450 */
/* .line 452 */
} // :cond_1
/* iget-boolean v0, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mSecreted:Z */
/* if-nez v0, :cond_2 */
/* .line 453 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "start downloading: " */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mSettingName;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ", mAbsolutePath: "; // const-string v3, ", mAbsolutePath: "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mAbsolutePath;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v3 = ", mData: "; // const-string v3, ", mData: "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mData;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v3 = ", mEncoding: "; // const-string v3, ", mEncoding: "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mEncoding;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v0 );
/* .line 455 */
} // :cond_2
v0 = this.mAbsolutePath;
v0 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-nez v0, :cond_3 */
/* .line 456 */
final String v0 = "error path, return !"; // const-string v0, "error path, return !"
android.util.Log .d ( v2,v0 );
/* .line 457 */
/* .line 459 */
} // :cond_3
v0 = this.mAbsolutePath;
final String v3 = "/"; // const-string v3, "/"
v4 = (( java.lang.StringBuilder ) v0 ).lastIndexOf ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I
(( java.lang.StringBuilder ) v0 ).substring ( v1, v4 ); // invoke-virtual {v0, v1, v4}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;
/* .line 460 */
/* .local v0, "path":Ljava/lang/String; */
v4 = this.mAbsolutePath;
v3 = (( java.lang.StringBuilder ) v4 ).lastIndexOf ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I
int v11 = 1; // const/4 v11, 0x1
/* add-int/2addr v3, v11 */
(( java.lang.StringBuilder ) v4 ).substring ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;
/* .line 461 */
/* .local v3, "fileName":Ljava/lang/String; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "path :"; // const-string v5, "path :"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " fileName: "; // const-string v5, " fileName: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v4 );
/* .line 464 */
try { // :try_start_0
v4 = this.this$0;
com.android.server.audio.CloudServiceSettings .-$$Nest$fgetiMisys_V2 ( v4 );
v4 = com.android.server.audio.CloudServiceSettings .-$$Nest$minvokeIsExists ( v4,v5,v0,v3 );
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 465 */
final String v4 = "file already exits, override!"; // const-string v4, "file already exits, override!"
android.util.Log .d ( v2,v4 );
/* .line 466 */
v4 = this.this$0;
com.android.server.audio.CloudServiceSettings .-$$Nest$fgetiMisys_V1 ( v4 );
com.android.server.audio.CloudServiceSettings .-$$Nest$minvokeEraseFileOrDirectory ( v4,v5,v0,v3 );
/* .line 468 */
} // :cond_4
/* new-instance v4, Ljava/io/ByteArrayOutputStream; */
/* invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V */
/* .line 469 */
/* .local v4, "byteData":Ljava/io/ByteArrayOutputStream; */
int v5 = 0; // const/4 v5, 0x0
/* .line 470 */
/* .local v5, "dataTowrite":[B */
v6 = this.mEncoding;
v7 = (( java.lang.String ) v6 ).hashCode ( ); // invoke-virtual {v6}, Ljava/lang/String;->hashCode()I
int v8 = -1; // const/4 v8, -0x1
/* sparse-switch v7, :sswitch_data_0 */
} // :cond_5
/* :sswitch_0 */
final String v7 = "FDS"; // const-string v7, "FDS"
v6 = (( java.lang.String ) v6 ).equals ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_5
/* move v6, v11 */
/* :sswitch_1 */
final String v7 = "B64"; // const-string v7, "B64"
v6 = (( java.lang.String ) v6 ).equals ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_5
/* move v6, v1 */
} // :goto_0
/* move v6, v8 */
} // :goto_1
/* packed-switch v6, :pswitch_data_0 */
/* .line 493 */
/* .line 479 */
/* :pswitch_0 */
/* new-instance v6, Ljava/net/URL; */
v7 = this.mData;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v6, v7}, Ljava/net/URL;-><init>(Ljava/lang/String;)V */
/* .line 480 */
/* .local v6, "url":Ljava/net/URL; */
(( java.net.URL ) v6 ).openConnection ( ); // invoke-virtual {v6}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;
/* check-cast v7, Ljava/net/HttpURLConnection; */
/* .line 481 */
/* .local v7, "urlConnection":Ljava/net/HttpURLConnection; */
/* const v9, 0x1e8480 */
(( java.net.HttpURLConnection ) v7 ).setConnectTimeout ( v9 ); // invoke-virtual {v7, v9}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V
/* .line 482 */
(( java.net.HttpURLConnection ) v7 ).setReadTimeout ( v9 ); // invoke-virtual {v7, v9}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V
/* .line 483 */
/* const/16 v9, 0x400 */
/* new-array v9, v9, [B */
/* move-object v5, v9 */
/* .line 484 */
/* new-instance v9, Ljava/io/BufferedInputStream; */
(( java.net.HttpURLConnection ) v7 ).getInputStream ( ); // invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;
/* invoke-direct {v9, v10}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V */
/* .line 485 */
/* .local v9, "in":Ljava/io/InputStream; */
int v10 = 0; // const/4 v10, 0x0
/* .line 486 */
/* .local v10, "len":I */
} // :goto_2
v12 = (( java.io.InputStream ) v9 ).read ( v5 ); // invoke-virtual {v9, v5}, Ljava/io/InputStream;->read([B)I
/* move v10, v12 */
/* if-eq v12, v8, :cond_6 */
/* .line 487 */
(( java.io.ByteArrayOutputStream ) v4 ).write ( v5, v1, v10 ); // invoke-virtual {v4, v5, v1, v10}, Ljava/io/ByteArrayOutputStream;->write([BII)V
/* .line 489 */
} // :cond_6
(( java.io.InputStream ) v9 ).close ( ); // invoke-virtual {v9}, Ljava/io/InputStream;->close()V
/* .line 490 */
/* move-object v12, v5 */
/* .line 472 */
} // .end local v6 # "url":Ljava/net/URL;
} // .end local v7 # "urlConnection":Ljava/net/HttpURLConnection;
} // .end local v9 # "in":Ljava/io/InputStream;
} // .end local v10 # "len":I
/* :pswitch_1 */
v6 = this.mData;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Base64 .decode ( v6,v1 );
/* move-object v5, v6 */
/* .line 473 */
if ( v5 != null) { // if-eqz v5, :cond_7
/* .line 474 */
/* array-length v6, v5 */
(( java.io.ByteArrayOutputStream ) v4 ).write ( v5, v1, v6 ); // invoke-virtual {v4, v5, v1, v6}, Ljava/io/ByteArrayOutputStream;->write([BII)V
/* .line 495 */
} // :cond_7
/* move-object v12, v5 */
/* .line 493 */
} // :goto_3
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "fail to download "; // const-string v7, "fail to download "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.mSettingName;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " encoding type not recognized"; // const-string v7, " encoding type not recognized"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v6 );
/* move-object v12, v5 */
/* .line 495 */
} // .end local v5 # "dataTowrite":[B
/* .local v12, "dataTowrite":[B */
} // :goto_4
v5 = this.mAbsolutePath;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v6 = "/data/vendor/video/videobox.json"; // const-string v6, "/data/vendor/video/videobox.json"
v5 = (( java.lang.String ) v5 ).equals ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_c
/* .line 496 */
v5 = com.android.server.audio.CloudServiceSettings.mHidlmisysV2;
if ( v5 != null) { // if-eqz v5, :cond_9
/* .line 497 */
/* new-instance v5, Ljava/util/ArrayList; */
/* invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V */
/* move-object v13, v5 */
/* .line 498 */
/* .local v13, "writeData":Ljava/util/ArrayList; */
(( java.io.ByteArrayOutputStream ) v4 ).toByteArray ( ); // invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
/* move-object v14, v5 */
/* .line 499 */
/* .local v14, "byteDataArray":[B */
/* array-length v5, v14 */
} // :goto_5
/* if-ge v1, v5, :cond_8 */
/* aget-byte v6, v14, v1 */
/* .line 500 */
/* .local v6, "bt":B */
java.lang.Byte .valueOf ( v6 );
(( java.util.ArrayList ) v13 ).add ( v7 ); // invoke-virtual {v13, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 499 */
/* nop */
} // .end local v6 # "bt":B
/* add-int/lit8 v1, v1, 0x1 */
/* .line 502 */
} // :cond_8
v5 = com.android.server.audio.CloudServiceSettings.mHidlmisysV2;
v1 = (( java.util.ArrayList ) v13 ).size ( ); // invoke-virtual {v13}, Ljava/util/ArrayList;->size()I
/* int-to-long v9, v1 */
/* move-object v6, v0 */
/* move-object v7, v3 */
/* move-object v8, v13 */
/* invoke-interface/range {v5 ..v10}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys;->MiSysWriteBuffer(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;J)I */
/* .line 503 */
/* iput-boolean v11, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mSuccess:Z */
} // .end local v13 # "writeData":Ljava/util/ArrayList;
} // .end local v14 # "byteDataArray":[B
/* .line 504 */
} // :cond_9
v5 = this.this$0;
com.android.server.audio.CloudServiceSettings .-$$Nest$fgetiQConfig ( v5 );
if ( v5 != null) { // if-eqz v5, :cond_b
/* .line 505 */
v5 = this.this$0;
com.android.server.audio.CloudServiceSettings .-$$Nest$fgetiQConfig ( v5 );
(( java.lang.Object ) v5 ).getClass ( ); // invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
final String v6 = "WriteToFile"; // const-string v6, "WriteToFile"
/* new-array v7, v11, [Ljava/lang/Class; */
/* const-class v8, [C */
/* aput-object v8, v7, v1 */
(( java.lang.Class ) v5 ).getDeclaredMethod ( v6, v7 ); // invoke-virtual {v5, v6, v7}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 506 */
/* .local v1, "writeMethod":Ljava/lang/reflect/Method; */
if ( v1 != null) { // if-eqz v1, :cond_c
/* .line 507 */
v5 = (( java.io.ByteArrayOutputStream ) v4 ).size ( ); // invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->size()I
/* new-array v5, v5, [C */
/* .line 508 */
/* .local v5, "charData":[C */
(( java.io.ByteArrayOutputStream ) v4 ).toByteArray ( ); // invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
/* .line 509 */
/* .local v6, "byteDataArray":[B */
int v7 = 0; // const/4 v7, 0x0
/* .local v7, "i":I */
} // :goto_6
/* array-length v8, v6 */
/* sub-int/2addr v8, v11 */
/* if-ge v7, v8, :cond_a */
/* .line 510 */
/* div-int/lit8 v8, v7, 0x2 */
/* add-int/lit8 v9, v7, 0x1 */
/* aget-byte v9, v6, v9 */
/* and-int/lit16 v9, v9, 0xff */
/* shl-int/lit8 v9, v9, 0x8 */
/* aget-byte v10, v6, v7 */
/* and-int/lit16 v10, v10, 0xff */
/* or-int/2addr v9, v10 */
/* int-to-char v9, v9 */
/* aput-char v9, v5, v8 */
/* .line 509 */
/* add-int/lit8 v7, v7, 0x2 */
/* .line 512 */
} // .end local v7 # "i":I
} // :cond_a
v7 = this.this$0;
com.android.server.audio.CloudServiceSettings .-$$Nest$fgetiQConfig ( v7 );
/* filled-new-array {v5}, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v1 ).invoke ( v7, v8 ); // invoke-virtual {v1, v7, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* .line 513 */
/* iput-boolean v11, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mSuccess:Z */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 504 */
} // .end local v1 # "writeMethod":Ljava/lang/reflect/Method;
} // .end local v5 # "charData":[C
} // .end local v6 # "byteDataArray":[B
} // :cond_b
} // :goto_7
/* nop */
/* .line 519 */
} // .end local v4 # "byteData":Ljava/io/ByteArrayOutputStream;
} // .end local v12 # "dataTowrite":[B
} // :cond_c
} // :goto_8
/* .line 517 */
/* :catch_0 */
/* move-exception v1 */
/* .line 518 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "fail to write data: "; // const-string v5, "fail to write data: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v4 );
/* .line 520 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_9
/* iget-boolean v1, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mSuccess:Z */
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0xfe80 -> :sswitch_1 */
/* 0x10f55 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public Boolean updateTo ( com.android.server.audio.CloudServiceSettings$Setting p0 ) {
/* .locals 2 */
/* .param p1, "s" # Lcom/android/server/audio/CloudServiceSettings$Setting; */
/* .line 525 */
v0 = /* invoke-super {p0, p1}, Lcom/android/server/audio/CloudServiceSettings$Setting;->updateTo(Lcom/android/server/audio/CloudServiceSettings$Setting;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* instance-of v0, p1, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 526 */
/* move-object v0, p1 */
/* check-cast v0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting; */
/* .line 527 */
/* .local v0, "fs":Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting; */
v1 = this.mAbsolutePath;
this.mAbsolutePath = v1;
/* .line 528 */
v1 = this.mData;
this.mData = v1;
/* .line 529 */
v1 = this.mEncoding;
this.mEncoding = v1;
/* .line 530 */
int v1 = 1; // const/4 v1, 0x1
/* .line 532 */
} // .end local v0 # "fs":Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "fail to update FileDownloadSetting: "; // const-string v1, "fail to update FileDownloadSetting: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mSettingName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "CloudServiceSettings"; // const-string v1, "CloudServiceSettings"
android.util.Log .e ( v1,v0 );
/* .line 533 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
