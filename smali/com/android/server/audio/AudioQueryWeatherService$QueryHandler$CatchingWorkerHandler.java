public class com.android.server.audio.AudioQueryWeatherService$QueryHandler$CatchingWorkerHandler extends android.content.AsyncQueryHandler$WorkerHandler {
	 /* .source "AudioQueryWeatherService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x4 */
/* name = "CatchingWorkerHandler" */
} // .end annotation
/* # instance fields */
final com.android.server.audio.AudioQueryWeatherService$QueryHandler this$1; //synthetic
/* # direct methods */
public com.android.server.audio.AudioQueryWeatherService$QueryHandler$CatchingWorkerHandler ( ) {
/* .locals 0 */
/* .param p1, "this$1" # Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 324 */
this.this$1 = p1;
/* .line 325 */
/* invoke-direct {p0, p1, p2}, Landroid/content/AsyncQueryHandler$WorkerHandler;-><init>(Landroid/content/AsyncQueryHandler;Landroid/os/Looper;)V */
/* .line 326 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 3 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 332 */
final String v0 = "AudioQueryWeatherService"; // const-string v0, "AudioQueryWeatherService"
try { // :try_start_0
	 /* invoke-super {p0, p1}, Landroid/content/AsyncQueryHandler$WorkerHandler;->handleMessage(Landroid/os/Message;)V */
	 /* :try_end_0 */
	 /* .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_0 ..:try_end_0} :catch_2 */
	 /* .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_0 ..:try_end_0} :catch_1 */
	 /* .catch Landroid/database/sqlite/SQLiteDatabaseCorruptException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 339 */
} // :goto_0
/* .line 337 */
/* :catch_0 */
/* move-exception v1 */
/* .line 338 */
/* .local v1, "e":Landroid/database/sqlite/SQLiteDatabaseCorruptException; */
final String v2 = "Exception on background"; // const-string v2, "Exception on background"
android.util.Log .d ( v0,v2,v1 );
/* .line 335 */
} // .end local v1 # "e":Landroid/database/sqlite/SQLiteDatabaseCorruptException;
/* :catch_1 */
/* move-exception v1 */
/* .line 336 */
/* .local v1, "e":Landroid/database/sqlite/SQLiteFullException; */
final String v2 = "Exception worker thread"; // const-string v2, "Exception worker thread"
android.util.Log .d ( v0,v2,v1 );
} // .end local v1 # "e":Landroid/database/sqlite/SQLiteFullException;
/* .line 333 */
/* :catch_2 */
/* move-exception v1 */
/* .line 334 */
/* .local v1, "e":Landroid/database/sqlite/SQLiteDiskIOException; */
final String v2 = "Exception background worker thread"; // const-string v2, "Exception background worker thread"
android.util.Log .d ( v0,v2,v1 );
} // .end local v1 # "e":Landroid/database/sqlite/SQLiteDiskIOException;
/* .line 340 */
} // :goto_1
return;
} // .end method
