.class Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;
.super Landroid/os/Handler;
.source "AudioPowerSaveModeObserver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/AudioPowerSaveModeObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WorkHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/audio/AudioPowerSaveModeObserver;


# direct methods
.method public constructor <init>(Lcom/android/server/audio/AudioPowerSaveModeObserver;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 89
    iput-object p1, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->this$0:Lcom/android/server/audio/AudioPowerSaveModeObserver;

    .line 90
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 91
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .line 95
    const-string v0, "AudioPowerSaveModeObserver"

    :try_start_0
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x9c6

    const/16 v3, 0x9c5

    packed-switch v1, :pswitch_data_0

    goto/16 :goto_0

    .line 116
    :pswitch_0
    const-string v1, "MSG_CHECK_AUDIO_POWER_SAVE_POLICY"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    iget-object v1, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->this$0:Lcom/android/server/audio/AudioPowerSaveModeObserver;

    invoke-static {v1}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->-$$Nest$fgetmHandler(Lcom/android/server/audio/AudioPowerSaveModeObserver;)Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->removeMessages(I)V

    .line 118
    iget-object v1, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->this$0:Lcom/android/server/audio/AudioPowerSaveModeObserver;

    invoke-static {v1}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->-$$Nest$fgetmHandler(Lcom/android/server/audio/AudioPowerSaveModeObserver;)Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->removeMessages(I)V

    .line 119
    iget-object v1, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->this$0:Lcom/android/server/audio/AudioPowerSaveModeObserver;

    const-string v2, "/sys/class/thermal/power_save/powersave_mode"

    invoke-static {v1, v2}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->-$$Nest$mcheckAudioPowerSaveState(Lcom/android/server/audio/AudioPowerSaveModeObserver;Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    .line 120
    iget-object v1, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->this$0:Lcom/android/server/audio/AudioPowerSaveModeObserver;

    invoke-static {v1}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->-$$Nest$mtoSleep(Lcom/android/server/audio/AudioPowerSaveModeObserver;)V

    .line 121
    iget-object v1, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->this$0:Lcom/android/server/audio/AudioPowerSaveModeObserver;

    const-string v2, "/sys/class/thermal/power_save/power_level"

    invoke-static {v1, v2}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->-$$Nest$mcheckAudioPowerSaveLevel(Lcom/android/server/audio/AudioPowerSaveModeObserver;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 123
    :cond_0
    iget-object v1, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->this$0:Lcom/android/server/audio/AudioPowerSaveModeObserver;

    invoke-static {v1}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->-$$Nest$fgetmHandler(Lcom/android/server/audio/AudioPowerSaveModeObserver;)Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->this$0:Lcom/android/server/audio/AudioPowerSaveModeObserver;

    invoke-static {v2}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->-$$Nest$fgetmHandler(Lcom/android/server/audio/AudioPowerSaveModeObserver;)Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;

    move-result-object v2

    const/16 v3, 0x9c7

    invoke-virtual {v2, v3}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->sendMessage(Landroid/os/Message;)Z

    .line 124
    const-string v1, "PowerSaveMode is close now"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    goto :goto_0

    .line 109
    :pswitch_1
    const-string v1, "MSG_CLEAR_AUDIO_POWER_SAVE_POLICY"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    iget-object v1, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->this$0:Lcom/android/server/audio/AudioPowerSaveModeObserver;

    invoke-static {v1}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->-$$Nest$fgetmHandler(Lcom/android/server/audio/AudioPowerSaveModeObserver;)Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->removeMessages(I)V

    .line 111
    iget-object v1, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->this$0:Lcom/android/server/audio/AudioPowerSaveModeObserver;

    invoke-static {v1}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->-$$Nest$fgetmHandler(Lcom/android/server/audio/AudioPowerSaveModeObserver;)Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->removeMessages(I)V

    .line 112
    iget-object v1, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->this$0:Lcom/android/server/audio/AudioPowerSaveModeObserver;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->-$$Nest$msetAudioPowerSaveMode(Lcom/android/server/audio/AudioPowerSaveModeObserver;I)V

    .line 113
    iget-object v1, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->this$0:Lcom/android/server/audio/AudioPowerSaveModeObserver;

    invoke-static {v1, v2}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->-$$Nest$msetAudioPowerSaveLevel(Lcom/android/server/audio/AudioPowerSaveModeObserver;I)V

    .line 114
    goto :goto_0

    .line 103
    :pswitch_2
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 104
    .local v1, "mAudioPowerSaveLevel":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MSG_SET_AUDIO_POWER_SAVE_LEVEL_POLICY, AudioPowerSaveLevel is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    iget-object v2, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->this$0:Lcom/android/server/audio/AudioPowerSaveModeObserver;

    invoke-static {v2, v1}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->-$$Nest$msetAudioPowerSaveLevel(Lcom/android/server/audio/AudioPowerSaveModeObserver;I)V

    .line 107
    goto :goto_0

    .line 97
    .end local v1    # "mAudioPowerSaveLevel":I
    :pswitch_3
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 98
    .local v1, "mAudioPowerSaveMode":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MSG_SET_AUDIO_POWER_SAVE_MODE_POLICY, AudioPowerSaveMode is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    iget-object v2, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->this$0:Lcom/android/server/audio/AudioPowerSaveModeObserver;

    invoke-static {v2, v1}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->-$$Nest$msetAudioPowerSaveMode(Lcom/android/server/audio/AudioPowerSaveModeObserver;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    nop

    .line 132
    .end local v1    # "mAudioPowerSaveMode":I
    :goto_0
    goto :goto_1

    .line 130
    :catch_0
    move-exception v1

    .line 131
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleMessage exception "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x9c5
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
