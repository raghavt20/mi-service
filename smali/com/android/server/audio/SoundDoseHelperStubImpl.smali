.class public Lcom/android/server/audio/SoundDoseHelperStubImpl;
.super Ljava/lang/Object;
.source "SoundDoseHelperStubImpl.java"

# interfaces
.implements Lcom/android/server/audio/SoundDoseHelperStub;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public updateSafeMediaVolumeIndex(I)I
    .locals 2
    .param p1, "safeMediaVolumeIndex"    # I

    .line 13
    const-string v0, "ro.config.safe_media_volume_index"

    const/4 v1, -0x1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 15
    .local v0, "propSafeMediaVolumeIndex":I
    if-eq v0, v1, :cond_0

    .line 16
    mul-int/lit8 v1, v0, 0xa

    return v1

    .line 18
    :cond_0
    return p1
.end method
