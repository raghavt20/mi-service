.class Lcom/android/server/audio/AudioQueryWeatherService$LocationObserver;
.super Landroid/database/ContentObserver;
.source "AudioQueryWeatherService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/AudioQueryWeatherService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LocationObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/audio/AudioQueryWeatherService;


# direct methods
.method constructor <init>(Lcom/android/server/audio/AudioQueryWeatherService;)V
    .locals 2

    .line 137
    iput-object p1, p0, Lcom/android/server/audio/AudioQueryWeatherService$LocationObserver;->this$0:Lcom/android/server/audio/AudioQueryWeatherService;

    .line 138
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 139
    invoke-static {p1}, Lcom/android/server/audio/AudioQueryWeatherService;->-$$Nest$fgetmContentResolver(Lcom/android/server/audio/AudioQueryWeatherService;)Landroid/content/ContentResolver;

    move-result-object p1

    const-string v0, "content://weather/selected_city"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 140
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2
    .param p1, "selfChange"    # Z

    .line 149
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "location change:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AudioQueryWeatherService"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    iget-object v0, p0, Lcom/android/server/audio/AudioQueryWeatherService$LocationObserver;->this$0:Lcom/android/server/audio/AudioQueryWeatherService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/audio/AudioQueryWeatherService;->-$$Nest$fputmNextSunriseSunsetTime(Lcom/android/server/audio/AudioQueryWeatherService;Z)V

    .line 152
    iget-object v0, p0, Lcom/android/server/audio/AudioQueryWeatherService$LocationObserver;->this$0:Lcom/android/server/audio/AudioQueryWeatherService;

    invoke-static {v0}, Lcom/android/server/audio/AudioQueryWeatherService;->-$$Nest$mstartCityQuery(Lcom/android/server/audio/AudioQueryWeatherService;)V

    .line 153
    return-void
.end method

.method public onCreate()V
    .locals 2

    .line 143
    const-string v0, "AudioQueryWeatherService"

    const-string v1, "LocationObserver:onCreate!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    return-void
.end method
