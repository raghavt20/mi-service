public class com.android.server.audio.SpatializerHelperStubImpl implements com.android.server.audio.SpatializerHelperStub {
	 /* .source "SpatializerHelperStubImpl.java" */
	 /* # interfaces */
	 /* # direct methods */
	 public com.android.server.audio.SpatializerHelperStubImpl ( ) {
		 /* .locals 0 */
		 /* .line 14 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void disablePropertyByMode ( Integer p0 ) {
		 /* .locals 1 */
		 /* .param p1, "mode" # I */
		 /* .line 32 */
		 int v0 = -1; // const/4 v0, -0x1
		 /* if-ne p1, v0, :cond_0 */
		 /* .line 33 */
		 /* const-string/jumbo v0, "vendor.spatial.headtrack=0" */
		 android.media.AudioSystem .setParameters ( v0 );
		 /* .line 36 */
	 } // :cond_0
	 return;
} // .end method
public void setPropertyByMode ( Boolean p0, Integer p1 ) {
	 /* .locals 1 */
	 /* .param p1, "isHeadTrackerAvailable" # Z */
	 /* .param p2, "mode" # I */
	 /* .line 17 */
	 if ( p1 != null) { // if-eqz p1, :cond_1
		 /* .line 18 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* if-ne p2, v0, :cond_0 */
		 /* .line 19 */
		 /* const-string/jumbo v0, "vendor.spatial.headtrack=1" */
		 android.media.AudioSystem .setParameters ( v0 );
		 /* .line 21 */
	 } // :cond_0
	 int v0 = -1; // const/4 v0, -0x1
	 /* if-ne p2, v0, :cond_2 */
	 /* .line 22 */
	 /* const-string/jumbo v0, "vendor.spatial.headtrack=0" */
	 android.media.AudioSystem .setParameters ( v0 );
	 /* .line 26 */
} // :cond_1
/* const-string/jumbo v0, "vendor.spatial.headtrack=-1" */
android.media.AudioSystem .setParameters ( v0 );
/* .line 29 */
} // :cond_2
} // :goto_0
return;
} // .end method
