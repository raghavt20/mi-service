class com.android.server.audio.AudioThermalObserver$AudioHighTemperatureListener extends com.android.server.FixedFileObserver {
	 /* .source "AudioThermalObserver.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/audio/AudioThermalObserver; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "AudioHighTemperatureListener" */
} // .end annotation
/* # instance fields */
final com.android.server.audio.AudioThermalObserver this$0; //synthetic
/* # direct methods */
public com.android.server.audio.AudioThermalObserver$AudioHighTemperatureListener ( ) {
/* .locals 2 */
/* .param p2, "path" # Ljava/lang/String; */
/* .line 175 */
this.this$0 = p1;
/* .line 176 */
/* invoke-direct {p0, p2}, Lcom/android/server/FixedFileObserver;-><init>(Ljava/lang/String;)V */
/* .line 177 */
com.android.server.audio.AudioThermalObserver .-$$Nest$fgetmHandler ( p1 );
com.android.server.audio.AudioThermalObserver .-$$Nest$fgetmHandler ( p1 );
/* const/16 v1, 0x9c6 */
(( com.android.server.audio.AudioThermalObserver$WorkHandler ) p1 ).obtainMessage ( v1 ); // invoke-virtual {p1, v1}, Lcom/android/server/audio/AudioThermalObserver$WorkHandler;->obtainMessage(I)Landroid/os/Message;
(( com.android.server.audio.AudioThermalObserver$WorkHandler ) v0 ).sendMessage ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/audio/AudioThermalObserver$WorkHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 178 */
return;
} // .end method
/* # virtual methods */
public void onEvent ( Integer p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "event" # I */
/* .param p2, "path" # Ljava/lang/String; */
/* .line 181 */
/* packed-switch p1, :pswitch_data_0 */
/* .line 183 */
/* :pswitch_0 */
final String v0 = "AudioThermalObserver"; // const-string v0, "AudioThermalObserver"
final String v1 = "HighTemperature modify event"; // const-string v1, "HighTemperature modify event"
android.util.Log .d ( v0,v1 );
/* .line 184 */
v0 = this.this$0;
com.android.server.audio.AudioThermalObserver .-$$Nest$fgetmHandler ( v0 );
v1 = this.this$0;
com.android.server.audio.AudioThermalObserver .-$$Nest$fgetmHandler ( v1 );
/* const/16 v2, 0x9c6 */
(( com.android.server.audio.AudioThermalObserver$WorkHandler ) v1 ).obtainMessage ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/audio/AudioThermalObserver$WorkHandler;->obtainMessage(I)Landroid/os/Message;
(( com.android.server.audio.AudioThermalObserver$WorkHandler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/audio/AudioThermalObserver$WorkHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 185 */
/* nop */
/* .line 190 */
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x2 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
