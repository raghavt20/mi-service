.class Lcom/android/server/audio/AudioServiceStubImpl$H;
.super Landroid/os/Handler;
.source "AudioServiceStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/AudioServiceStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "H"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/audio/AudioServiceStubImpl;


# direct methods
.method public constructor <init>(Lcom/android/server/audio/AudioServiceStubImpl;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 2159
    iput-object p1, p0, Lcom/android/server/audio/AudioServiceStubImpl$H;->this$0:Lcom/android/server/audio/AudioServiceStubImpl;

    .line 2160
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 2161
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .line 2164
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 2207
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl$H;->this$0:Lcom/android/server/audio/AudioServiceStubImpl;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/audio/AudioServiceStubImpl;->-$$Nest$mhandleMediaStateUpdate(Lcom/android/server/audio/AudioServiceStubImpl;Z)V

    .line 2208
    goto/16 :goto_0

    .line 2204
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl$H;->this$0:Lcom/android/server/audio/AudioServiceStubImpl;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/server/audio/AudioServiceStubImpl;->-$$Nest$mhandleMeetingModeUpdate(Lcom/android/server/audio/AudioServiceStubImpl;Ljava/lang/String;)V

    .line 2205
    goto/16 :goto_0

    .line 2201
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl$H;->this$0:Lcom/android/server/audio/AudioServiceStubImpl;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/audio/AudioServiceStubImpl;->-$$Nest$mhandleAudioModeUpdate(Lcom/android/server/audio/AudioServiceStubImpl;I)V

    .line 2202
    goto/16 :goto_0

    .line 2193
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl$H;->this$0:Lcom/android/server/audio/AudioServiceStubImpl;

    invoke-static {v0}, Lcom/android/server/audio/AudioServiceStubImpl;->-$$Nest$fgetmContext(Lcom/android/server/audio/AudioServiceStubImpl;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/audio/MQSserver;->getInstance(Landroid/content/Context;)Lcom/android/server/audio/MQSserver;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/audio/MQSserver;->asynReportData()V

    .line 2194
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl$H;->this$0:Lcom/android/server/audio/AudioServiceStubImpl;

    invoke-static {v0}, Lcom/android/server/audio/AudioServiceStubImpl;->-$$Nest$fgetmHandler(Lcom/android/server/audio/AudioServiceStubImpl;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/audio/AudioServiceStubImpl$H;->this$0:Lcom/android/server/audio/AudioServiceStubImpl;

    invoke-static {v1}, Lcom/android/server/audio/AudioServiceStubImpl;->-$$Nest$fgetmHandler(Lcom/android/server/audio/AudioServiceStubImpl;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-static {}, Lcom/android/server/audio/AudioServiceStubImpl;->-$$Nest$sfgetMQSSERVER_REPORT_RATE_MS()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 2196
    goto/16 :goto_0

    .line 2198
    :pswitch_4
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl$H;->this$0:Lcom/android/server/audio/AudioServiceStubImpl;

    invoke-static {v0}, Lcom/android/server/audio/AudioServiceStubImpl;->-$$Nest$fgetmMqsUtils(Lcom/android/server/audio/AudioServiceStubImpl;)Lcom/android/server/audio/MQSUtils;

    move-result-object v0

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/android/server/audio/MQSUtils$AudioStateTrackData;

    invoke-virtual {v0, v1}, Lcom/android/server/audio/MQSUtils;->reportAbnormalAudioStatus(Lcom/android/server/audio/MQSUtils$AudioStateTrackData;)V

    .line 2199
    goto/16 :goto_0

    .line 2186
    :pswitch_5
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl$H;->this$0:Lcom/android/server/audio/AudioServiceStubImpl;

    invoke-static {v0}, Lcom/android/server/audio/AudioServiceStubImpl;->-$$Nest$fgetmMqsUtils(Lcom/android/server/audio/AudioServiceStubImpl;)Lcom/android/server/audio/MQSUtils;

    move-result-object v0

    .line 2187
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "audio_silent_level"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 2188
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "audio_silent_location"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2189
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "audio_silent_reason"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2190
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "audio_silent_type"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 2186
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/server/audio/MQSUtils;->reportAudioSilentObserverToOnetrack(ILjava/lang/String;Ljava/lang/String;I)V

    .line 2191
    goto/16 :goto_0

    .line 2181
    :pswitch_6
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl$H;->this$0:Lcom/android/server/audio/AudioServiceStubImpl;

    invoke-static {v0}, Lcom/android/server/audio/AudioServiceStubImpl;->-$$Nest$fgetmMqsUtils(Lcom/android/server/audio/AudioServiceStubImpl;)Lcom/android/server/audio/MQSUtils;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2182
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl$H;->this$0:Lcom/android/server/audio/AudioServiceStubImpl;

    invoke-static {v0}, Lcom/android/server/audio/AudioServiceStubImpl;->-$$Nest$fgetmMqsUtils(Lcom/android/server/audio/AudioServiceStubImpl;)Lcom/android/server/audio/MQSUtils;

    move-result-object v0

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/android/server/audio/MQSUtils$WaveformInfo;

    invoke-virtual {v0, v1}, Lcom/android/server/audio/MQSUtils;->reportWaveformInfo(Lcom/android/server/audio/MQSUtils$WaveformInfo;)V

    goto :goto_0

    .line 2175
    :pswitch_7
    iget-object v0, p0, Lcom/android/server/audio/AudioServiceStubImpl$H;->this$0:Lcom/android/server/audio/AudioServiceStubImpl;

    invoke-static {v0}, Lcom/android/server/audio/AudioServiceStubImpl;->-$$Nest$fgetmMqsUtils(Lcom/android/server/audio/AudioServiceStubImpl;)Lcom/android/server/audio/MQSUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/audio/AudioServiceStubImpl$H;->this$0:Lcom/android/server/audio/AudioServiceStubImpl;

    invoke-static {v1}, Lcom/android/server/audio/AudioServiceStubImpl;->-$$Nest$fgetmContext(Lcom/android/server/audio/AudioServiceStubImpl;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/android/server/audio/AudioServiceStubImpl$H;->this$0:Lcom/android/server/audio/AudioServiceStubImpl;

    invoke-static {v3}, Lcom/android/server/audio/AudioServiceStubImpl;->-$$Nest$fgetmMultiVoipPackages(Lcom/android/server/audio/AudioServiceStubImpl;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/audio/MQSUtils;->reportBtMultiVoipDailyUse(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 2177
    goto :goto_0

    .line 2166
    :pswitch_8
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    .line 2167
    .local v0, "clients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateConcurrentVoipInfo voip apps : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2168
    invoke-interface {v0}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2167
    const-string v2, "AudioServiceStubImpl"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2169
    iget-object v1, p0, Lcom/android/server/audio/AudioServiceStubImpl$H;->this$0:Lcom/android/server/audio/AudioServiceStubImpl;

    invoke-static {v1}, Lcom/android/server/audio/AudioServiceStubImpl;->-$$Nest$fgetmAudioService(Lcom/android/server/audio/AudioServiceStubImpl;)Lcom/android/server/audio/AudioService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/audio/AudioService;->isBluetoothA2dpOn()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2170
    iget-object v1, p0, Lcom/android/server/audio/AudioServiceStubImpl$H;->this$0:Lcom/android/server/audio/AudioServiceStubImpl;

    invoke-interface {v0}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/server/audio/AudioServiceStubImpl;->-$$Nest$fputmMultiVoipPackages(Lcom/android/server/audio/AudioServiceStubImpl;Ljava/lang/String;)V

    .line 2172
    :cond_0
    iget-object v1, p0, Lcom/android/server/audio/AudioServiceStubImpl$H;->this$0:Lcom/android/server/audio/AudioServiceStubImpl;

    invoke-static {v1}, Lcom/android/server/audio/AudioServiceStubImpl;->-$$Nest$fgetmContext(Lcom/android/server/audio/AudioServiceStubImpl;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/android/server/audio/MQSUtils;->trackConcurrentVoipInfo(Landroid/content/Context;Ljava/util/List;)Z

    .line 2173
    nop

    .line 2212
    .end local v0    # "clients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
