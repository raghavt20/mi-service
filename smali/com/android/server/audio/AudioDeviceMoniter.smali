.class public Lcom/android/server/audio/AudioDeviceMoniter;
.super Ljava/lang/Object;
.source "AudioDeviceMoniter.java"


# static fields
.field private static final ACTION_DISABLE_NFC_POLLING:Ljava/lang/String; = "com.android.nfc.action.DISABLE_POLLING"

.field private static final ACTION_ENABLE_NFC_POLLING:Ljava/lang/String; = "com.android.nfc.action.ENABLE_POLLING"

.field public static final AUDIO_STATE_OFF:I = 0x0

.field public static final AUDIO_STATE_ON:I = 0x1

.field private static final PROPERTY_MIC_STATUS:Ljava/lang/String; = "vendor.audio.mic.status"

.field private static final PROPERTY_RCV_STATUS:Ljava/lang/String; = "vendor.audio.receiver.status"

.field private static final TAG:Ljava/lang/String; = "AudioDeviceMoniter"

.field private static volatile sInstance:Lcom/android/server/audio/AudioDeviceMoniter;


# instance fields
.field private mAction:Landroid/content/Intent;

.field private mContext:Landroid/content/Context;

.field private mCurrentNfcPollState:I


# direct methods
.method static bridge synthetic -$$Nest$mpollMicAndRcv(Lcom/android/server/audio/AudioDeviceMoniter;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/audio/AudioDeviceMoniter;->pollMicAndRcv()V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const-string v0, "AudioDeviceMoniter init..."

    const-string v1, "AudioDeviceMoniter"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    iput-object p1, p0, Lcom/android/server/audio/AudioDeviceMoniter;->mContext:Landroid/content/Context;

    .line 48
    const-string v0, "AudioDeviceMoniter init done"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/server/audio/AudioDeviceMoniter;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .line 55
    sget-object v0, Lcom/android/server/audio/AudioDeviceMoniter;->sInstance:Lcom/android/server/audio/AudioDeviceMoniter;

    if-nez v0, :cond_0

    .line 56
    new-instance v0, Lcom/android/server/audio/AudioDeviceMoniter;

    invoke-direct {v0, p0}, Lcom/android/server/audio/AudioDeviceMoniter;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/server/audio/AudioDeviceMoniter;->sInstance:Lcom/android/server/audio/AudioDeviceMoniter;

    .line 58
    :cond_0
    sget-object v0, Lcom/android/server/audio/AudioDeviceMoniter;->sInstance:Lcom/android/server/audio/AudioDeviceMoniter;

    return-object v0
.end method

.method private pollMicAndRcv()V
    .locals 10

    .line 76
    nop

    :goto_0
    const-string/jumbo v0, "vendor.audio.mic.status"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 77
    .local v0, "AudioMicState":Ljava/lang/String;
    const-string/jumbo v1, "vendor.audio.receiver.status"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 78
    .local v1, "AudioRcvState":Ljava/lang/String;
    const/4 v2, 0x0

    .line 79
    .local v2, "micState":I
    const/4 v3, 0x0

    .line 80
    .local v3, "rcvState":I
    const/4 v4, 0x0

    .line 81
    .local v4, "isMicStatusInvalid":Z
    const/4 v5, 0x0

    .line 83
    .local v5, "isRcvStatusInvalid":Z
    const-string v6, "on"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    const-string v8, "off"

    if-eqz v7, :cond_0

    .line 84
    const/4 v2, 0x1

    goto :goto_1

    .line 85
    :cond_0
    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 86
    const/4 v2, 0x0

    goto :goto_1

    .line 88
    :cond_1
    const/4 v4, 0x1

    .line 91
    :goto_1
    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 92
    const/4 v3, 0x1

    goto :goto_2

    .line 93
    :cond_2
    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 94
    const/4 v3, 0x0

    goto :goto_2

    .line 96
    :cond_3
    const/4 v5, 0x1

    .line 99
    :goto_2
    const-string v6, "AudioDeviceMoniter"

    if-eqz v4, :cond_4

    if-eqz v5, :cond_4

    .line 100
    const-string/jumbo v7, "unexpected value for AudioRcvState and AudioMicState."

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    nop

    .line 118
    .end local v0    # "AudioMicState":Ljava/lang/String;
    .end local v1    # "AudioRcvState":Ljava/lang/String;
    .end local v2    # "micState":I
    .end local v3    # "rcvState":I
    .end local v4    # "isMicStatusInvalid":Z
    .end local v5    # "isRcvStatusInvalid":Z
    return-void

    .line 104
    .restart local v0    # "AudioMicState":Ljava/lang/String;
    .restart local v1    # "AudioRcvState":Ljava/lang/String;
    .restart local v2    # "micState":I
    .restart local v3    # "rcvState":I
    .restart local v4    # "isMicStatusInvalid":Z
    .restart local v5    # "isRcvStatusInvalid":Z
    :cond_4
    or-int v7, v3, v2

    .line 105
    .local v7, "mPollState":I
    iget v8, p0, Lcom/android/server/audio/AudioDeviceMoniter;->mCurrentNfcPollState:I

    if-eq v7, v8, :cond_6

    .line 106
    if-eqz v7, :cond_5

    .line 107
    new-instance v8, Landroid/content/Intent;

    const-string v9, "com.android.nfc.action.DISABLE_POLLING"

    invoke-direct {v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v8, p0, Lcom/android/server/audio/AudioDeviceMoniter;->mAction:Landroid/content/Intent;

    goto :goto_3

    .line 109
    :cond_5
    new-instance v8, Landroid/content/Intent;

    const-string v9, "com.android.nfc.action.ENABLE_POLLING"

    invoke-direct {v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v8, p0, Lcom/android/server/audio/AudioDeviceMoniter;->mAction:Landroid/content/Intent;

    .line 111
    :goto_3
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "nfc status be changed to "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", sent broadcast to nfc..."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    iget-object v6, p0, Lcom/android/server/audio/AudioDeviceMoniter;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/android/server/audio/AudioDeviceMoniter;->mAction:Landroid/content/Intent;

    sget-object v9, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v6, v8, v9}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 113
    iput v7, p0, Lcom/android/server/audio/AudioDeviceMoniter;->mCurrentNfcPollState:I

    .line 116
    :cond_6
    const-wide/16 v8, 0x1f4

    invoke-static {v8, v9}, Landroid/os/SystemClock;->sleep(J)V

    .line 117
    .end local v0    # "AudioMicState":Ljava/lang/String;
    .end local v1    # "AudioRcvState":Ljava/lang/String;
    .end local v2    # "micState":I
    .end local v3    # "rcvState":I
    .end local v4    # "isMicStatusInvalid":Z
    .end local v5    # "isRcvStatusInvalid":Z
    .end local v7    # "mPollState":I
    goto/16 :goto_0
.end method


# virtual methods
.method public final startPollAudioMicStatus()V
    .locals 1

    .line 65
    new-instance v0, Lcom/android/server/audio/AudioDeviceMoniter$1;

    invoke-direct {v0, p0}, Lcom/android/server/audio/AudioDeviceMoniter$1;-><init>(Lcom/android/server/audio/AudioDeviceMoniter;)V

    .line 71
    .local v0, "MicAndRcvPollThread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 72
    return-void
.end method
