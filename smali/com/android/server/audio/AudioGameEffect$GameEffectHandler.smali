.class Lcom/android/server/audio/AudioGameEffect$GameEffectHandler;
.super Landroid/os/Handler;
.source "AudioGameEffect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/AudioGameEffect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GameEffectHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/audio/AudioGameEffect;


# direct methods
.method constructor <init>(Lcom/android/server/audio/AudioGameEffect;)V
    .locals 0

    .line 181
    iput-object p1, p0, Lcom/android/server/audio/AudioGameEffect$GameEffectHandler;->this$0:Lcom/android/server/audio/AudioGameEffect;

    .line 182
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 183
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .line 187
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 204
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/audio/AudioGameEffect$GameEffectHandler;->this$0:Lcom/android/server/audio/AudioGameEffect;

    invoke-static {v0}, Lcom/android/server/audio/AudioGameEffect;->-$$Nest$mstopGameEffect(Lcom/android/server/audio/AudioGameEffect;)V

    .line 205
    goto :goto_0

    .line 201
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/audio/AudioGameEffect$GameEffectHandler;->this$0:Lcom/android/server/audio/AudioGameEffect;

    invoke-static {v0}, Lcom/android/server/audio/AudioGameEffect;->-$$Nest$mstartGameEffect(Lcom/android/server/audio/AudioGameEffect;)V

    .line 202
    goto :goto_0

    .line 189
    :pswitch_2
    const-string v0, "ProcessManager"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/android/server/audio/AudioGameEffect$GameEffectHandler;->this$0:Lcom/android/server/audio/AudioGameEffect;

    invoke-static {v0}, Lcom/android/server/audio/AudioGameEffect;->-$$Nest$fgetmForegroundInfoListener(Lcom/android/server/audio/AudioGameEffect;)Lmiui/process/IForegroundInfoListener$Stub;

    move-result-object v0

    invoke-static {v0}, Lmiui/process/ProcessManager;->registerForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V

    goto :goto_0

    .line 192
    :cond_0
    iget-object v0, p0, Lcom/android/server/audio/AudioGameEffect$GameEffectHandler;->this$0:Lcom/android/server/audio/AudioGameEffect;

    invoke-static {v0}, Lcom/android/server/audio/AudioGameEffect;->-$$Nest$fgetmQueryPMServiceTime(Lcom/android/server/audio/AudioGameEffect;)I

    move-result v1

    add-int/lit8 v2, v1, 0x1

    invoke-static {v0, v2}, Lcom/android/server/audio/AudioGameEffect;->-$$Nest$fputmQueryPMServiceTime(Lcom/android/server/audio/AudioGameEffect;I)V

    const/16 v0, 0x14

    const-string v2, "AudioGameEffect"

    if-ge v1, v0, :cond_1

    .line 193
    const-string v0, "process manager service not published, wait 1 second"

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    iget-object v0, p0, Lcom/android/server/audio/AudioGameEffect$GameEffectHandler;->this$0:Lcom/android/server/audio/AudioGameEffect;

    invoke-static {v0}, Lcom/android/server/audio/AudioGameEffect;->-$$Nest$minitProcessListenerAsync(Lcom/android/server/audio/AudioGameEffect;)V

    goto :goto_0

    .line 196
    :cond_1
    const-string v0, "failed to get ProcessManager service"

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    nop

    .line 209
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
