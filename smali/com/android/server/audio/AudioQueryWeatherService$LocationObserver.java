class com.android.server.audio.AudioQueryWeatherService$LocationObserver extends android.database.ContentObserver {
	 /* .source "AudioQueryWeatherService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/audio/AudioQueryWeatherService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "LocationObserver" */
} // .end annotation
/* # instance fields */
final com.android.server.audio.AudioQueryWeatherService this$0; //synthetic
/* # direct methods */
 com.android.server.audio.AudioQueryWeatherService$LocationObserver ( ) {
/* .locals 2 */
/* .line 137 */
this.this$0 = p1;
/* .line 138 */
/* new-instance v0, Landroid/os/Handler; */
/* invoke-direct {v0}, Landroid/os/Handler;-><init>()V */
/* invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 139 */
com.android.server.audio.AudioQueryWeatherService .-$$Nest$fgetmContentResolver ( p1 );
final String v0 = "content://weather/selected_city"; // const-string v0, "content://weather/selected_city"
android.net.Uri .parse ( v0 );
int v1 = 0; // const/4 v1, 0x0
(( android.content.ContentResolver ) p1 ).registerContentObserver ( v0, v1, p0 ); // invoke-virtual {p1, v0, v1, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 140 */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "selfChange" # Z */
/* .line 149 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "location change:"; // const-string v1, "location change:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AudioQueryWeatherService"; // const-string v1, "AudioQueryWeatherService"
android.util.Log .d ( v1,v0 );
/* .line 151 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.android.server.audio.AudioQueryWeatherService .-$$Nest$fputmNextSunriseSunsetTime ( v0,v1 );
/* .line 152 */
v0 = this.this$0;
com.android.server.audio.AudioQueryWeatherService .-$$Nest$mstartCityQuery ( v0 );
/* .line 153 */
return;
} // .end method
public void onCreate ( ) {
/* .locals 2 */
/* .line 143 */
final String v0 = "AudioQueryWeatherService"; // const-string v0, "AudioQueryWeatherService"
final String v1 = "LocationObserver:onCreate!"; // const-string v1, "LocationObserver:onCreate!"
android.util.Log .d ( v0,v1 );
/* .line 145 */
return;
} // .end method
