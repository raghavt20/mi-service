public class com.android.server.audio.MiAudioService extends com.android.server.audio.AudioService implements android.media.Spatializer$OnSpatializerStateChangedListener {
	 /* .source "MiAudioService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/audio/MiAudioService$SpatializerType;, */
	 /* Lcom/android/server/audio/MiAudioService$DeviceChangeBroadcastReceiver; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
/* # instance fields */
private final Integer SPATIALIZER_PARAM_SPATIALIZER_TYPE;
private final Integer SPATIALIZER_PARAM_SPEAKER_ROTATION;
private android.media.AudioManager mAudioManager;
private com.android.server.audio.MiAudioService$DeviceChangeBroadcastReceiver mDeviceConnectStateListener;
com.android.server.audio.dolbyeffect.DolbyEffectController mDolbyEffectController;
private android.database.ContentObserver mEffecImplementerObserver;
private android.net.Uri mEffecImplementerUri;
private final Boolean mIsSupportedDolbyEffectControl;
private final Boolean mIsSupportedSelectedDolbyTunningByVolume;
private android.media.Spatializer mSpatializer;
private Boolean mSpatializerEnabled;
private Integer mSpatilizerType;
private Boolean mSpeakerOn;
private final Boolean mUseXiaoMiSpatilizer;
/* # direct methods */
static Integer -$$Nest$fgetmSpatilizerType ( com.android.server.audio.MiAudioService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/audio/MiAudioService;->mSpatilizerType:I */
} // .end method
static Boolean -$$Nest$fgetmUseXiaoMiSpatilizer ( com.android.server.audio.MiAudioService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/audio/MiAudioService;->mUseXiaoMiSpatilizer:Z */
} // .end method
static void -$$Nest$fputmSpatilizerType ( com.android.server.audio.MiAudioService p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput p1, p0, Lcom/android/server/audio/MiAudioService;->mSpatilizerType:I */
	 return;
} // .end method
static -$$Nest$mintToBytes ( com.android.server.audio.MiAudioService p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/audio/MiAudioService;->intToBytes(I)[B */
} // .end method
public com.android.server.audio.MiAudioService ( ) {
	 /* .locals 7 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 63 */
	 com.android.server.audio.AudioSystemAdapter .getDefaultAdapter ( );
	 /* .line 64 */
	 com.android.server.audio.SystemServerAdapter .getDefaultAdapter ( p1 );
	 /* .line 65 */
	 com.android.server.audio.SettingsAdapter .getDefaultAdapter ( );
	 /* new-instance v5, Lcom/android/server/audio/DefaultAudioPolicyFacade; */
	 /* invoke-direct {v5}, Lcom/android/server/audio/DefaultAudioPolicyFacade;-><init>()V */
	 int v6 = 0; // const/4 v6, 0x0
	 /* .line 63 */
	 /* move-object v0, p0 */
	 /* move-object v1, p1 */
	 /* invoke-direct/range {v0 ..v6}, Lcom/android/server/audio/AudioService;-><init>(Landroid/content/Context;Lcom/android/server/audio/AudioSystemAdapter;Lcom/android/server/audio/SystemServerAdapter;Lcom/android/server/audio/SettingsAdapter;Lcom/android/server/audio/AudioPolicyFacade;Landroid/os/Looper;)V */
	 /* .line 43 */
	 /* const/16 v0, 0x130 */
	 /* iput v0, p0, Lcom/android/server/audio/MiAudioService;->SPATIALIZER_PARAM_SPEAKER_ROTATION:I */
	 /* .line 44 */
	 /* const/16 v0, 0x120 */
	 /* iput v0, p0, Lcom/android/server/audio/MiAudioService;->SPATIALIZER_PARAM_SPATIALIZER_TYPE:I */
	 /* .line 45 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput-boolean v0, p0, Lcom/android/server/audio/MiAudioService;->mSpatializerEnabled:Z */
	 /* .line 46 */
	 v1 = com.android.server.audio.MiAudioService$SpatializerType.DOLBY;
	 v1 = 	 (( com.android.server.audio.MiAudioService$SpatializerType ) v1 ).ordinal ( ); // invoke-virtual {v1}, Lcom/android/server/audio/MiAudioService$SpatializerType;->ordinal()I
	 /* iput v1, p0, Lcom/android/server/audio/MiAudioService;->mSpatilizerType:I */
	 /* .line 47 */
	 /* iput-boolean v0, p0, Lcom/android/server/audio/MiAudioService;->mSpeakerOn:Z */
	 /* .line 53 */
	 /* nop */
	 /* .line 54 */
	 /* const-string/jumbo v0, "vendor.audio.dolby.control.support" */
	 android.os.SystemProperties .get ( v0 );
	 /* const-string/jumbo v1, "true" */
	 v0 = 	 (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 /* iput-boolean v0, p0, Lcom/android/server/audio/MiAudioService;->mIsSupportedDolbyEffectControl:Z */
	 /* .line 55 */
	 /* nop */
	 /* .line 56 */
	 /* const-string/jumbo v0, "vendor.audio.dolby.control.tunning.by.volume.support" */
	 android.os.SystemProperties .get ( v0 );
	 v0 = 	 (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 /* iput-boolean v0, p0, Lcom/android/server/audio/MiAudioService;->mIsSupportedSelectedDolbyTunningByVolume:Z */
	 /* .line 57 */
	 /* nop */
	 /* .line 58 */
	 /* const-string/jumbo v0, "vendor.audio.useXiaomiSpatializer" */
	 android.os.SystemProperties .get ( v0 );
	 v0 = 	 (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 /* iput-boolean v0, p0, Lcom/android/server/audio/MiAudioService;->mUseXiaoMiSpatilizer:Z */
	 /* .line 68 */
	 final String v0 = "MiAudioService"; // const-string v0, "MiAudioService"
	 final String v1 = "MiAudioService()"; // const-string v1, "MiAudioService()"
	 android.util.Log .d ( v0,v1 );
	 /* .line 69 */
	 return;
} // .end method
private intToBytes ( Integer p0 ) {
	 /* .locals 3 */
	 /* .param p1, "src" # I */
	 /* .line 227 */
	 int v0 = 4; // const/4 v0, 0x4
	 /* new-array v0, v0, [B */
	 /* .line 228 */
	 /* .local v0, "result":[B */
	 /* shr-int/lit8 v1, p1, 0x18 */
	 /* and-int/lit16 v1, v1, 0xff */
	 /* int-to-byte v1, v1 */
	 int v2 = 3; // const/4 v2, 0x3
	 /* aput-byte v1, v0, v2 */
	 /* .line 229 */
	 /* shr-int/lit8 v1, p1, 0x10 */
	 /* and-int/lit16 v1, v1, 0xff */
	 /* int-to-byte v1, v1 */
	 int v2 = 2; // const/4 v2, 0x2
	 /* aput-byte v1, v0, v2 */
	 /* .line 230 */
	 /* shr-int/lit8 v1, p1, 0x8 */
	 /* and-int/lit16 v1, v1, 0xff */
	 /* int-to-byte v1, v1 */
	 int v2 = 1; // const/4 v2, 0x1
	 /* aput-byte v1, v0, v2 */
	 /* .line 231 */
	 /* and-int/lit16 v1, p1, 0xff */
	 /* int-to-byte v1, v1 */
	 int v2 = 0; // const/4 v2, 0x0
	 /* aput-byte v1, v0, v2 */
	 /* .line 232 */
} // .end method
/* # virtual methods */
public void DeviceConectedStateListenerInit ( ) {
	 /* .locals 3 */
	 /* .line 113 */
	 /* new-instance v0, Lcom/android/server/audio/MiAudioService$DeviceChangeBroadcastReceiver; */
	 int v1 = 0; // const/4 v1, 0x0
	 /* invoke-direct {v0, p0, v1}, Lcom/android/server/audio/MiAudioService$DeviceChangeBroadcastReceiver;-><init>(Lcom/android/server/audio/MiAudioService;Lcom/android/server/audio/MiAudioService$DeviceChangeBroadcastReceiver-IA;)V */
	 this.mDeviceConnectStateListener = v0;
	 /* .line 114 */
	 /* new-instance v0, Landroid/content/IntentFilter; */
	 /* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
	 /* .line 115 */
	 /* .local v0, "filter":Landroid/content/IntentFilter; */
	 final String v1 = "android.intent.action.HEADSET_PLUG"; // const-string v1, "android.intent.action.HEADSET_PLUG"
	 (( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
	 /* .line 117 */
	 final String v1 = "android.bluetooth.headset.action.VENDOR_SPECIFIC_HEADSET_EVENT"; // const-string v1, "android.bluetooth.headset.action.VENDOR_SPECIFIC_HEADSET_EVENT"
	 (( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
	 /* .line 118 */
	 final String v1 = "android.bluetooth.device.action.BOND_STATE_CHANGED"; // const-string v1, "android.bluetooth.device.action.BOND_STATE_CHANGED"
	 (( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
	 /* .line 119 */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "android.bluetooth.headset.intent.category.companyid."; // const-string v2, "android.bluetooth.headset.intent.category.companyid."
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* .line 120 */
	 /* const/16 v2, 0x38f */
	 java.lang.Integer .toString ( v2 );
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 /* .line 119 */
	 (( android.content.IntentFilter ) v0 ).addCategory ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V
	 /* .line 121 */
	 v1 = this.mContext;
	 v2 = this.mDeviceConnectStateListener;
	 (( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
	 /* .line 122 */
	 return;
} // .end method
public void DolbyEffectControllerInit ( ) {
	 /* .locals 1 */
	 /* .line 87 */
	 v0 = this.mContext;
	 com.android.server.audio.dolbyeffect.DolbyEffectController .getInstance ( v0 );
	 this.mDolbyEffectController = v0;
	 /* .line 88 */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 89 */
		 (( com.android.server.audio.dolbyeffect.DolbyEffectController ) v0 ).init ( ); // invoke-virtual {v0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->init()V
		 /* .line 91 */
	 } // :cond_0
	 return;
} // .end method
public void SettingsObserver ( ) {
	 /* .locals 4 */
	 /* .line 135 */
	 final String v0 = "effect_implementer"; // const-string v0, "effect_implementer"
	 android.provider.Settings$Global .getUriFor ( v0 );
	 this.mEffecImplementerUri = v0;
	 /* .line 136 */
	 /* new-instance v0, Lcom/android/server/audio/MiAudioService$1; */
	 int v1 = 0; // const/4 v1, 0x0
	 /* invoke-direct {v0, p0, v1}, Lcom/android/server/audio/MiAudioService$1;-><init>(Lcom/android/server/audio/MiAudioService;Landroid/os/Handler;)V */
	 this.mEffecImplementerObserver = v0;
	 /* .line 159 */
	 v0 = this.mContext;
	 (( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 v1 = this.mEffecImplementerUri;
	 int v2 = 1; // const/4 v2, 0x1
	 v3 = this.mEffecImplementerObserver;
	 (( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
	 /* .line 160 */
	 return;
} // .end method
public void SpatialStateInit ( ) {
	 /* .locals 3 */
	 /* .line 94 */
	 v0 = this.mContext;
	 /* const-class v1, Landroid/media/AudioManager; */
	 (( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
	 /* check-cast v0, Landroid/media/AudioManager; */
	 this.mAudioManager = v0;
	 /* .line 95 */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 96 */
		 (( android.media.AudioManager ) v0 ).getSpatializer ( ); // invoke-virtual {v0}, Landroid/media/AudioManager;->getSpatializer()Landroid/media/Spatializer;
		 this.mSpatializer = v0;
		 /* .line 98 */
	 } // :cond_0
	 v0 = this.mSpatializer;
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 99 */
		 java.util.concurrent.Executors .newSingleThreadExecutor ( );
		 (( android.media.Spatializer ) v0 ).addOnSpatializerStateChangedListener ( v1, p0 ); // invoke-virtual {v0, v1, p0}, Landroid/media/Spatializer;->addOnSpatializerStateChangedListener(Ljava/util/concurrent/Executor;Landroid/media/Spatializer$OnSpatializerStateChangedListener;)V
		 /* .line 101 */
	 } // :cond_1
	 v0 = this.mContext;
	 (( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 final String v1 = "effect_implementer"; // const-string v1, "effect_implementer"
	 android.provider.Settings$Global .getString ( v0,v1 );
	 /* .line 102 */
	 /* .local v0, "effecImplementer":Ljava/lang/String; */
	 final String v1 = "dolby"; // const-string v1, "dolby"
	 v1 = 	 (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_2
		 /* .line 103 */
		 v1 = com.android.server.audio.MiAudioService$SpatializerType.DOLBY;
		 v1 = 		 (( com.android.server.audio.MiAudioService$SpatializerType ) v1 ).ordinal ( ); // invoke-virtual {v1}, Lcom/android/server/audio/MiAudioService$SpatializerType;->ordinal()I
		 /* iput v1, p0, Lcom/android/server/audio/MiAudioService;->mSpatilizerType:I */
		 /* .line 104 */
	 } // :cond_2
	 final String v1 = "misound"; // const-string v1, "misound"
	 v1 = 	 (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_3
		 /* .line 105 */
		 v1 = com.android.server.audio.MiAudioService$SpatializerType.MISOUND;
		 v1 = 		 (( com.android.server.audio.MiAudioService$SpatializerType ) v1 ).ordinal ( ); // invoke-virtual {v1}, Lcom/android/server/audio/MiAudioService$SpatializerType;->ordinal()I
		 /* iput v1, p0, Lcom/android/server/audio/MiAudioService;->mSpatilizerType:I */
		 /* .line 106 */
	 } // :cond_3
	 final String v1 = "none"; // const-string v1, "none"
	 v1 = 	 (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_4
		 /* .line 107 */
		 v1 = com.android.server.audio.MiAudioService$SpatializerType.NONE;
		 v1 = 		 (( com.android.server.audio.MiAudioService$SpatializerType ) v1 ).ordinal ( ); // invoke-virtual {v1}, Lcom/android/server/audio/MiAudioService$SpatializerType;->ordinal()I
		 /* iput v1, p0, Lcom/android/server/audio/MiAudioService;->mSpatilizerType:I */
		 /* .line 109 */
	 } // :cond_4
} // :goto_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Init EffectImplementer is "; // const-string v2, "Init EffectImplementer is "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiAudioService"; // const-string v2, "MiAudioService"
android.util.Log .d ( v2,v1 );
/* .line 110 */
return;
} // .end method
protected void adjustStreamVolume ( Integer p0, Integer p1, Integer p2, java.lang.String p3, java.lang.String p4, Integer p5, Integer p6, java.lang.String p7, Boolean p8, Integer p9 ) {
/* .locals 3 */
/* .param p1, "streamType" # I */
/* .param p2, "direction" # I */
/* .param p3, "flags" # I */
/* .param p4, "callingPackage" # Ljava/lang/String; */
/* .param p5, "caller" # Ljava/lang/String; */
/* .param p6, "uid" # I */
/* .param p7, "pid" # I */
/* .param p8, "attributionTag" # Ljava/lang/String; */
/* .param p9, "hasModifyAudioSettings" # Z */
/* .param p10, "keyEventMode" # I */
/* .line 214 */
/* invoke-super/range {p0 ..p10}, Lcom/android/server/audio/AudioService;->adjustStreamVolume(IIILjava/lang/String;Ljava/lang/String;IILjava/lang/String;ZI)V */
/* .line 215 */
v0 = this.mDolbyEffectController;
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 216 */
	 /* iget-boolean v0, p0, Lcom/android/server/audio/MiAudioService;->mIsSupportedSelectedDolbyTunningByVolume:Z */
	 /* if-nez v0, :cond_0 */
	 /* .line 217 */
	 return;
	 /* .line 218 */
} // :cond_0
int v0 = 3; // const/4 v0, 0x3
/* if-ne p1, v0, :cond_1 */
/* .line 219 */
int v1 = 2; // const/4 v1, 0x2
v0 = (( com.android.server.audio.MiAudioService ) p0 ).getDeviceStreamVolume ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/audio/MiAudioService;->getDeviceStreamVolume(II)I
/* .line 220 */
/* .local v0, "newVolume":I */
final String v1 = "MiAudioService"; // const-string v1, "MiAudioService"
final String v2 = "adjustStreamVolume"; // const-string v2, "adjustStreamVolume"
android.util.Log .d ( v1,v2 );
/* .line 221 */
v1 = this.mDolbyEffectController;
(( com.android.server.audio.dolbyeffect.DolbyEffectController ) v1 ).receiveVolumeChanged ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->receiveVolumeChanged(I)V
/* .line 224 */
} // .end local v0 # "newVolume":I
} // :cond_1
return;
} // .end method
public void functionsRoadedInit ( ) {
/* .locals 1 */
/* .line 78 */
/* iget-boolean v0, p0, Lcom/android/server/audio/MiAudioService;->mIsSupportedDolbyEffectControl:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 79 */
(( com.android.server.audio.MiAudioService ) p0 ).DolbyEffectControllerInit ( ); // invoke-virtual {p0}, Lcom/android/server/audio/MiAudioService;->DolbyEffectControllerInit()V
/* .line 80 */
(( com.android.server.audio.MiAudioService ) p0 ).DeviceConectedStateListenerInit ( ); // invoke-virtual {p0}, Lcom/android/server/audio/MiAudioService;->DeviceConectedStateListenerInit()V
/* .line 82 */
} // :cond_0
(( com.android.server.audio.MiAudioService ) p0 ).SettingsObserver ( ); // invoke-virtual {p0}, Lcom/android/server/audio/MiAudioService;->SettingsObserver()V
/* .line 83 */
(( com.android.server.audio.MiAudioService ) p0 ).SpatialStateInit ( ); // invoke-virtual {p0}, Lcom/android/server/audio/MiAudioService;->SpatialStateInit()V
/* .line 84 */
return;
} // .end method
public void onRotationUpdate ( java.lang.Integer p0 ) {
/* .locals 5 */
/* .param p1, "rotation" # Ljava/lang/Integer; */
/* .line 193 */
/* invoke-super {p0, p1}, Lcom/android/server/audio/AudioService;->onRotationUpdate(Ljava/lang/Integer;)V */
/* .line 194 */
v0 = this.mDolbyEffectController;
final String v1 = "MiAudioService"; // const-string v1, "MiAudioService"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 195 */
final String v0 = "onRotationUpdate, receiveRotationChanged"; // const-string v0, "onRotationUpdate, receiveRotationChanged"
android.util.Log .d ( v1,v0 );
/* .line 196 */
v0 = this.mDolbyEffectController;
v2 = (( java.lang.Integer ) p1 ).intValue ( ); // invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I
(( com.android.server.audio.dolbyeffect.DolbyEffectController ) v0 ).receiveRotationChanged ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->receiveRotationChanged(I)V
/* .line 198 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/audio/MiAudioService;->mUseXiaoMiSpatilizer:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = (( com.android.server.audio.MiAudioService ) p0 ).isSpatializerEnabled ( ); // invoke-virtual {p0}, Lcom/android/server/audio/MiAudioService;->isSpatializerEnabled()Z
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = (( com.android.server.audio.MiAudioService ) p0 ).isSpatializerAvailable ( ); // invoke-virtual {p0}, Lcom/android/server/audio/MiAudioService;->isSpatializerAvailable()Z
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 199 */
	 v0 = 	 (( java.lang.Integer ) p1 ).intValue ( ); // invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I
	 /* invoke-direct {p0, v0}, Lcom/android/server/audio/MiAudioService;->intToBytes(I)[B */
	 /* .line 201 */
	 /* .local v0, "Rotation":[B */
	 try { // :try_start_0
		 /* new-instance v2, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
		 /* const-string/jumbo v3, "setSpatializerParameter SPATIALIZER_PARAM_SPEAKER_ROTATION = " */
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Log .d ( v1,v2 );
		 /* .line 202 */
		 /* const/16 v2, 0x130 */
		 (( com.android.server.audio.MiAudioService ) p0 ).setSpatializerParameter ( v2, v0 ); // invoke-virtual {p0, v2, v0}, Lcom/android/server/audio/MiAudioService;->setSpatializerParameter(I[B)V
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 205 */
		 /* .line 203 */
		 /* :catch_0 */
		 /* move-exception v2 */
		 /* .line 204 */
		 /* .local v2, "e":Ljava/lang/Exception; */
		 /* new-instance v3, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
		 /* const-string/jumbo v4, "setSpatializerParameter SPATIALIZER_PARAM_SPEAKER_ROTATION Exception " */
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Log .e ( v1,v3 );
		 /* .line 207 */
	 } // .end local v0 # "Rotation":[B
} // .end local v2 # "e":Ljava/lang/Exception;
} // :cond_1
} // :goto_0
return;
} // .end method
public void onSpatializerAvailableChanged ( android.media.Spatializer p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "spat" # Landroid/media/Spatializer; */
/* .param p2, "available" # Z */
/* .line 185 */
v0 = this.mDolbyEffectController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 186 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "onSpatializerAvailableChanged: available = "; // const-string v1, "onSpatializerAvailableChanged: available = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiAudioService"; // const-string v1, "MiAudioService"
android.util.Log .d ( v1,v0 );
/* .line 187 */
v0 = this.mDolbyEffectController;
(( com.android.server.audio.dolbyeffect.DolbyEffectController ) v0 ).receiveSpatializerAvailableChanged ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->receiveSpatializerAvailableChanged(Z)V
/* .line 189 */
} // :cond_0
return;
} // .end method
public void onSpatializerEnabledChanged ( android.media.Spatializer p0, Boolean p1 ) {
/* .locals 4 */
/* .param p1, "spat" # Landroid/media/Spatializer; */
/* .param p2, "enabled" # Z */
/* .line 164 */
/* iput-boolean p2, p0, Lcom/android/server/audio/MiAudioService;->mSpatializerEnabled:Z */
/* .line 165 */
v0 = this.mDolbyEffectController;
final String v1 = "MiAudioService"; // const-string v1, "MiAudioService"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 166 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "onSpatializerEnabledChanged: enabled = "; // const-string v2, "onSpatializerEnabledChanged: enabled = "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v0 );
/* .line 167 */
v0 = this.mDolbyEffectController;
(( com.android.server.audio.dolbyeffect.DolbyEffectController ) v0 ).receiveSpatializerEnabledChanged ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->receiveSpatializerEnabledChanged(Z)V
/* .line 169 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/audio/MiAudioService;->mUseXiaoMiSpatilizer:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v0, p0, Lcom/android/server/audio/MiAudioService;->mSpatializerEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 171 */
try { // :try_start_0
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "onSpatializerEnabledChanged: setSpatializerParameter mSpatilizerType = "; // const-string v2, "onSpatializerEnabledChanged: setSpatializerParameter mSpatilizerType = "
	 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v2, p0, Lcom/android/server/audio/MiAudioService;->mSpatilizerType:I */
	 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Log .d ( v1,v0 );
	 /* .line 172 */
	 /* iget v0, p0, Lcom/android/server/audio/MiAudioService;->mSpatilizerType:I */
	 /* invoke-direct {p0, v0}, Lcom/android/server/audio/MiAudioService;->intToBytes(I)[B */
	 /* const/16 v2, 0x120 */
	 (( com.android.server.audio.MiAudioService ) p0 ).setSpatializerParameter ( v2, v0 ); // invoke-virtual {p0, v2, v0}, Lcom/android/server/audio/MiAudioService;->setSpatializerParameter(I[B)V
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 175 */
	 /* .line 173 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 174 */
	 /* .local v0, "e":Ljava/lang/Exception; */
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v3 = "onSpatializerEnabledChanged Exception:"; // const-string v3, "onSpatializerEnabledChanged Exception:"
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Log .e ( v1,v2 );
	 /* .line 177 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_1
} // :goto_0
final String v0 = "onSpatializerEnabledChanged notifyEffectChanged"; // const-string v0, "onSpatializerEnabledChanged notifyEffectChanged"
android.util.Log .d ( v1,v0 );
/* .line 178 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "miui.intent.action.ACTION_AUDIO_EFFECT_CHANGED"; // const-string v1, "miui.intent.action.ACTION_AUDIO_EFFECT_CHANGED"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 180 */
/* .local v0, "intent":Landroid/content/Intent; */
v1 = this.mContext;
(( android.content.Context ) v1 ).sendBroadcast ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
/* .line 181 */
return;
} // .end method
public void onSystemReady ( ) {
/* .locals 0 */
/* .line 73 */
/* invoke-super {p0}, Lcom/android/server/audio/AudioService;->onSystemReady()V */
/* .line 74 */
(( com.android.server.audio.MiAudioService ) p0 ).functionsRoadedInit ( ); // invoke-virtual {p0}, Lcom/android/server/audio/MiAudioService;->functionsRoadedInit()V
/* .line 75 */
return;
} // .end method
