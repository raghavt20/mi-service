.class public Lcom/android/server/audio/AudioDeviceInventoryStubImpl;
.super Ljava/lang/Object;
.source "AudioDeviceInventoryStubImpl.java"

# interfaces
.implements Lcom/android/server/audio/AudioDeviceInventoryStub;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getLeAddr(Ljava/util/LinkedHashMap;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/android/server/audio/AudioDeviceInventory$DeviceInfo;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 30
    .local p1, "connecteddevices":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/String;Lcom/android/server/audio/AudioDeviceInventory$DeviceInfo;>;"
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_2

    .line 33
    :cond_0
    const/4 v0, 0x0

    .line 34
    .local v0, "addr":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/audio/AudioDeviceInventory$DeviceInfo;

    .line 35
    .local v2, "di":Lcom/android/server/audio/AudioDeviceInventory$DeviceInfo;
    iget v3, v2, Lcom/android/server/audio/AudioDeviceInventory$DeviceInfo;->mDeviceType:I

    const/high16 v4, 0x20000000

    if-ne v3, v4, :cond_1

    .line 36
    iget-object v0, v2, Lcom/android/server/audio/AudioDeviceInventory$DeviceInfo;->mDeviceAddress:Ljava/lang/String;

    .line 37
    goto :goto_1

    .line 39
    .end local v2    # "di":Lcom/android/server/audio/AudioDeviceInventory$DeviceInfo;
    :cond_1
    goto :goto_0

    .line 40
    :cond_2
    :goto_1
    return-object v0

    .line 31
    .end local v0    # "addr":Ljava/lang/String;
    :cond_3
    :goto_2
    const/4 v0, 0x0

    return-object v0
.end method

.method public postSetVolumeIndex(Lcom/android/server/audio/AudioDeviceBroker$BtDeviceInfo;Lcom/android/server/audio/AudioDeviceBroker;I)V
    .locals 3
    .param p1, "btInfo"    # Lcom/android/server/audio/AudioDeviceBroker$BtDeviceInfo;
    .param p2, "audiodevicebroker"    # Lcom/android/server/audio/AudioDeviceBroker;
    .param p3, "streammusic"    # I

    .line 19
    if-nez p2, :cond_0

    .line 20
    return-void

    .line 22
    :cond_0
    iget v0, p1, Lcom/android/server/audio/AudioDeviceBroker$BtDeviceInfo;->mVolume:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 23
    iget v0, p1, Lcom/android/server/audio/AudioDeviceBroker$BtDeviceInfo;->mVolume:I

    mul-int/lit8 v0, v0, 0xa

    iget v1, p1, Lcom/android/server/audio/AudioDeviceBroker$BtDeviceInfo;->mAudioSystemDevice:I

    const-string v2, "onSetBtActiveDevice"

    invoke-virtual {p2, p3, v0, v1, v2}, Lcom/android/server/audio/AudioDeviceBroker;->postSetVolumeIndexOnDevice(IIILjava/lang/String;)V

    .line 27
    :cond_1
    return-void
.end method
