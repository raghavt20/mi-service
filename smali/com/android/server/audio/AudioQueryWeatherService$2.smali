.class Lcom/android/server/audio/AudioQueryWeatherService$2;
.super Landroid/content/BroadcastReceiver;
.source "AudioQueryWeatherService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/audio/AudioQueryWeatherService;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/audio/AudioQueryWeatherService;


# direct methods
.method constructor <init>(Lcom/android/server/audio/AudioQueryWeatherService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/audio/AudioQueryWeatherService;

    .line 116
    iput-object p1, p0, Lcom/android/server/audio/AudioQueryWeatherService$2;->this$0:Lcom/android/server/audio/AudioQueryWeatherService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 120
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "receive sunrise.sunset.time broadcast : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AudioQueryWeatherService"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    iget-object v0, p0, Lcom/android/server/audio/AudioQueryWeatherService$2;->this$0:Lcom/android/server/audio/AudioQueryWeatherService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/server/audio/AudioQueryWeatherService;->-$$Nest$fputmNextSunriseSunsetTime(Lcom/android/server/audio/AudioQueryWeatherService;Z)V

    .line 124
    iget-object v0, p0, Lcom/android/server/audio/AudioQueryWeatherService$2;->this$0:Lcom/android/server/audio/AudioQueryWeatherService;

    invoke-static {v0}, Lcom/android/server/audio/AudioQueryWeatherService;->-$$Nest$mstartCityQuery(Lcom/android/server/audio/AudioQueryWeatherService;)V

    .line 125
    return-void
.end method
