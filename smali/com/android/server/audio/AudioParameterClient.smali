.class public Lcom/android/server/audio/AudioParameterClient;
.super Ljava/lang/Object;
.source "AudioParameterClient.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/audio/AudioParameterClient$ClientDeathListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "AudioParameterClient"


# instance fields
.field private callback:Lcom/android/server/audio/AudioParameterClient$ClientDeathListener;

.field private final clientBinder:Landroid/os/IBinder;

.field private targetParameter:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/os/IBinder;Ljava/lang/String;)V
    .locals 0
    .param p1, "binder"    # Landroid/os/IBinder;
    .param p2, "targetParameter"    # Ljava/lang/String;

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/android/server/audio/AudioParameterClient;->clientBinder:Landroid/os/IBinder;

    .line 36
    iput-object p2, p0, Lcom/android/server/audio/AudioParameterClient;->targetParameter:Ljava/lang/String;

    .line 37
    return-void
.end method


# virtual methods
.method public binderDied()V
    .locals 3

    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AudioParameterClient binderDied "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/audio/AudioParameterClient;->targetParameter:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AudioParameterClient"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    iget-object v0, p0, Lcom/android/server/audio/AudioParameterClient;->callback:Lcom/android/server/audio/AudioParameterClient$ClientDeathListener;

    if-eqz v0, :cond_0

    .line 68
    iget-object v1, p0, Lcom/android/server/audio/AudioParameterClient;->clientBinder:Landroid/os/IBinder;

    iget-object v2, p0, Lcom/android/server/audio/AudioParameterClient;->targetParameter:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/android/server/audio/AudioParameterClient$ClientDeathListener;->onBinderDied(Landroid/os/IBinder;Ljava/lang/String;)V

    .line 70
    :cond_0
    return-void
.end method

.method public getBinder()Landroid/os/IBinder;
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/android/server/audio/AudioParameterClient;->clientBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public getTargetParameter()Ljava/lang/String;
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/android/server/audio/AudioParameterClient;->targetParameter:Ljava/lang/String;

    return-object v0
.end method

.method public registerDeathRecipient()Z
    .locals 4

    .line 44
    const/4 v0, 0x0

    .line 46
    .local v0, "status":Z
    :try_start_0
    iget-object v1, p0, Lcom/android/server/audio/AudioParameterClient;->clientBinder:Landroid/os/IBinder;

    const/4 v2, 0x0

    invoke-interface {v1, p0, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    const/4 v0, 0x1

    .line 52
    goto :goto_0

    .line 48
    :catch_0
    move-exception v1

    .line 49
    .local v1, "e":Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AudioParameterClient could not link to binder death "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/audio/AudioParameterClient;->targetParameter:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "AudioParameterClient"

    invoke-static {v3, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 53
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_0
    return v0
.end method

.method public setClientDiedListener(Lcom/android/server/audio/AudioParameterClient$ClientDeathListener;)V
    .locals 0
    .param p1, "callback"    # Lcom/android/server/audio/AudioParameterClient$ClientDeathListener;

    .line 40
    iput-object p1, p0, Lcom/android/server/audio/AudioParameterClient;->callback:Lcom/android/server/audio/AudioParameterClient$ClientDeathListener;

    .line 41
    return-void
.end method

.method public unregisterDeathRecipient()V
    .locals 3

    .line 58
    :try_start_0
    iget-object v0, p0, Lcom/android/server/audio/AudioParameterClient;->clientBinder:Landroid/os/IBinder;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    goto :goto_0

    .line 59
    :catch_0
    move-exception v0

    .line 60
    .local v0, "e":Ljava/util/NoSuchElementException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AudioParameterClient could not not unregistered to binder "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/audio/AudioParameterClient;->targetParameter:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AudioParameterClient"

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    .end local v0    # "e":Ljava/util/NoSuchElementException;
    :goto_0
    return-void
.end method
