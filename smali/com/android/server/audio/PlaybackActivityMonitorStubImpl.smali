.class public Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;
.super Ljava/lang/Object;
.source "PlaybackActivityMonitorStubImpl.java"

# interfaces
.implements Lcom/android/server/audio/PlaybackActivityMonitorStub;


# static fields
.field private static final APPS_VOLUME_AUTHORITY:Ljava/lang/String; = "com.miui.misound.AppVolumeProvider"

.field private static final APPS_VOLUME_CONTENT_URI:Landroid/net/Uri;

.field private static final APPS_VOLUME_PATH:Ljava/lang/String; = "assistant"

.field private static final APPS_VOLUME_URI:Landroid/net/Uri;

.field private static final LOAD_APPS_VOLUME_DELAY:I = 0x1388

.field private static final TAG:Ljava/lang/String; = "PlaybackActivityMonitorStubImpl"


# instance fields
.field private mIsForceIgnoreGranted:Z

.field private final mMusicVolumeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mOpenSoundAssist:Z

.field private mRouteCastUID:I

.field private objKeyLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 35
    nop

    .line 36
    const-string v0, "content://com.miui.misound.AppVolumeProvider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->APPS_VOLUME_CONTENT_URI:Landroid/net/Uri;

    .line 38
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 39
    const-string v1, "assistant"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->APPS_VOLUME_URI:Landroid/net/Uri;

    .line 38
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mOpenSoundAssist:Z

    .line 32
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->objKeyLock:Ljava/lang/Object;

    .line 33
    iput-boolean v0, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mIsForceIgnoreGranted:Z

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mMusicVolumeMap:Ljava/util/HashMap;

    .line 42
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mRouteCastUID:I

    return-void
.end method

.method private getPkgName(Landroid/media/AudioPlaybackConfiguration;Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1, "apc"    # Landroid/media/AudioPlaybackConfiguration;
    .param p2, "context"    # Landroid/content/Context;

    .line 127
    if-eqz p1, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    .line 130
    :cond_0
    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 131
    .local v0, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {p1}, Landroid/media/AudioPlaybackConfiguration;->getClientUid()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 128
    .end local v0    # "pm":Landroid/content/pm/PackageManager;
    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private initPlayerVolume(Ljava/lang/String;F)V
    .locals 3
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "volume"    # F

    .line 110
    if-eqz p1, :cond_0

    .line 111
    iget-object v0, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mMusicVolumeMap:Ljava/util/HashMap;

    monitor-enter v0

    .line 112
    :try_start_0
    iget-object v1, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mMusicVolumeMap:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 115
    :cond_0
    :goto_0
    return-void
.end method

.method private isMusicPlayerActive(Landroid/media/AudioPlaybackConfiguration;)Z
    .locals 4
    .param p1, "apc"    # Landroid/media/AudioPlaybackConfiguration;

    .line 118
    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 119
    :cond_0
    invoke-virtual {p1}, Landroid/media/AudioPlaybackConfiguration;->getAudioAttributes()Landroid/media/AudioAttributes;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/AudioAttributes;->getUsage()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    .line 120
    invoke-virtual {p1}, Landroid/media/AudioPlaybackConfiguration;->getAudioAttributes()Landroid/media/AudioAttributes;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/AudioAttributes;->getVolumeControlStream()I

    move-result v1

    const/4 v3, 0x3

    if-ne v1, v3, :cond_1

    goto :goto_0

    .line 123
    :cond_1
    return v0

    .line 121
    :cond_2
    :goto_0
    return v2
.end method


# virtual methods
.method public dumpPlayersVolume(Ljava/io/PrintWriter;)V
    .locals 7
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 261
    const-string v0, "\n  volume players:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 262
    invoke-virtual {p0}, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->getPkgVolumes()Ljava/util/HashMap;

    move-result-object v0

    .line 263
    .local v0, "volumeMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Float;>;"
    if-eqz v0, :cond_1

    .line 264
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 265
    .local v1, "pkgList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 266
    .local v3, "pkg":Ljava/lang/String;
    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    .line 267
    .local v4, "playerVolume":F
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "\n  pkg="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " volume="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 268
    .end local v3    # "pkg":Ljava/lang/String;
    .end local v4    # "playerVolume":F
    goto :goto_0

    .line 269
    :cond_0
    const-string v2, "\n"

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 271
    .end local v1    # "pkgList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    return-void
.end method

.method public getPkgVolumes()Ljava/util/HashMap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .line 104
    iget-object v0, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mMusicVolumeMap:Ljava/util/HashMap;

    monitor-enter v0

    .line 105
    :try_start_0
    iget-object v1, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mMusicVolumeMap:Ljava/util/HashMap;

    monitor-exit v0

    return-object v1

    .line 106
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getPlayerVolume(Landroid/content/Context;Landroid/media/AudioPlaybackConfiguration;)F
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "apc"    # Landroid/media/AudioPlaybackConfiguration;

    .line 92
    iget-boolean v0, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mOpenSoundAssist:Z

    const/high16 v1, 0x3f800000    # 1.0f

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    .line 95
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 96
    .local v0, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {p2}, Landroid/media/AudioPlaybackConfiguration;->getClientUid()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;

    move-result-object v2

    .line 97
    .local v2, "pkg":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mMusicVolumeMap:Ljava/util/HashMap;

    monitor-enter v3

    .line 98
    :try_start_0
    iget-object v4, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mMusicVolumeMap:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v4, v2, v1}, Ljava/util/HashMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    monitor-exit v3

    return v1

    .line 99
    :catchall_0
    move-exception v1

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 93
    .end local v0    # "pm":Landroid/content/pm/PackageManager;
    .end local v2    # "pkg":Ljava/lang/String;
    :cond_1
    :goto_0
    return v1
.end method

.method public getPlayerVolume(Ljava/lang/String;)F
    .locals 3
    .param p1, "pkg"    # Ljava/lang/String;

    .line 79
    iget-boolean v0, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mOpenSoundAssist:Z

    const/high16 v1, 0x3f800000    # 1.0f

    if-nez v0, :cond_0

    .line 80
    return v1

    .line 82
    :cond_0
    if-nez p1, :cond_1

    .line 83
    return v1

    .line 85
    :cond_1
    iget-object v0, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mMusicVolumeMap:Ljava/util/HashMap;

    monitor-enter v0

    .line 86
    :try_start_0
    iget-object v2, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mMusicVolumeMap:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v2, p1, v1}, Ljava/util/HashMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    monitor-exit v0

    return v1

    .line 87
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getRouteCastUID()I
    .locals 1

    .line 319
    iget v0, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mRouteCastUID:I

    return v0
.end method

.method public ignoreFocusRequest(Lcom/android/server/audio/FocusRequester;I)Z
    .locals 7
    .param p1, "frWinner"    # Lcom/android/server/audio/FocusRequester;
    .param p2, "uid"    # I

    .line 324
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 325
    return v0

    .line 328
    :cond_0
    invoke-virtual {p0, p2}, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->interruptMusicPlayback(I)Z

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_1

    .line 329
    return v2

    .line 332
    :cond_1
    invoke-virtual {p1}, Lcom/android/server/audio/FocusRequester;->getAudioAttributes()Landroid/media/AudioAttributes;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/AudioAttributes;->getUsage()I

    move-result v1

    .line 333
    .local v1, "usage":I
    if-eq v1, v2, :cond_3

    const/4 v3, 0x5

    if-ne v1, v3, :cond_2

    goto :goto_0

    :cond_2
    move v3, v0

    goto :goto_1

    :cond_3
    :goto_0
    move v3, v2

    .line 337
    .local v3, "usageMatched":Z
    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " mRouteCast: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mRouteCastUID:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " uid: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "PlaybackActivityMonitorStubImpl"

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    const/4 v4, 0x0

    .line 339
    .local v4, "openSmallWindowMediaProjection":Z
    invoke-virtual {p1}, Lcom/android/server/audio/FocusRequester;->getClientUid()I

    move-result v5

    iget v6, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mRouteCastUID:I

    if-eq v5, v6, :cond_4

    if-ne v6, p2, :cond_7

    .line 340
    :cond_4
    if-nez v3, :cond_6

    const/4 v5, 0x4

    if-ne v1, v5, :cond_5

    goto :goto_2

    :cond_5
    move v5, v0

    goto :goto_3

    :cond_6
    :goto_2
    move v5, v2

    :goto_3
    move v3, v5

    .line 341
    const/4 v4, 0x1

    .line 344
    :cond_7
    if-eqz v3, :cond_9

    iget-boolean v5, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mIsForceIgnoreGranted:Z

    if-nez v5, :cond_8

    if-eqz v4, :cond_9

    :cond_8
    move v0, v2

    :cond_9
    return v0
.end method

.method public interruptMusicPlayback(I)Z
    .locals 8
    .param p1, "uid"    # I

    .line 349
    const-string v0, "audio"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 350
    .local v0, "b":Landroid/os/IBinder;
    invoke-static {v0}, Landroid/media/IAudioService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/media/IAudioService;

    move-result-object v1

    .line 351
    .local v1, "audioService":Landroid/media/IAudioService;
    const/4 v2, 0x0

    .line 353
    .local v2, "uidArray":[I
    :try_start_0
    invoke-interface {v1}, Landroid/media/IAudioService;->getAudioPolicyMatchUids()[I

    move-result-object v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v3

    .line 356
    nop

    .line 359
    const/4 v3, 0x0

    .line 360
    .local v3, "hasUid":Z
    const-string v4, "PlaybackActivityMonitorStubImpl"

    if-eqz v2, :cond_1

    array-length v5, v2

    if-lez v5, :cond_1

    .line 361
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    array-length v6, v2

    if-ge v5, v6, :cond_1

    .line 362
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getAudioPolicyMatchUids="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget v7, v2, v5

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    aget v6, v2, v5

    if-ne v6, p1, :cond_0

    .line 364
    const/4 v3, 0x1

    .line 365
    goto :goto_1

    .line 361
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 369
    .end local v5    # "i":I
    :cond_1
    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "interruptMusicPlayback uid="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " hasUid="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    xor-int/lit8 v4, v3, 0x1

    return v4

    .line 354
    .end local v3    # "hasUid":Z
    :catch_0
    move-exception v3

    .line 355
    .local v3, "e":Landroid/os/RemoteException;
    const/4 v4, 0x1

    return v4
.end method

.method public isForceIgnoreGranted()Z
    .locals 1

    .line 69
    iget-boolean v0, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mIsForceIgnoreGranted:Z

    return v0
.end method

.method public isSoundAssistOpen()Z
    .locals 2

    .line 46
    iget-object v0, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->objKeyLock:Ljava/lang/Object;

    monitor-enter v0

    .line 47
    :try_start_0
    iget-boolean v1, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mOpenSoundAssist:Z

    monitor-exit v0

    return v1

    .line 48
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public loadAppsVolume(Landroid/content/ContentResolver;)Z
    .locals 9
    .param p1, "cr"    # Landroid/content/ContentResolver;

    .line 158
    const-string v0, "PlaybackActivityMonitorStubImpl"

    const/4 v1, 0x0

    .line 159
    .local v1, "cs":Landroid/database/Cursor;
    const/4 v2, 0x0

    if-nez p1, :cond_0

    .line 160
    return v2

    .line 163
    :cond_0
    :try_start_0
    sget-object v4, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->APPS_VOLUME_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v3

    .line 167
    goto :goto_0

    .line 165
    :catch_0
    move-exception v3

    .line 166
    .local v3, "e":Ljava/lang/Exception;
    const-string v4, "loadAppsVolume: query exception"

    invoke-static {v0, v4, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 169
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_0
    if-nez v1, :cond_1

    .line 170
    return v2

    .line 173
    :cond_1
    :goto_1
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 174
    const-string v3, "_package_name"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 175
    .local v3, "pkgName":Ljava/lang/String;
    const-string v4, "_volume"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x42c80000    # 100.0f

    div-float/2addr v4, v5

    .line 176
    .local v4, "volume":F
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "loadAppsVolume pkgName "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " volume="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    invoke-direct {p0, v3, v4}, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->initPlayerVolume(Ljava/lang/String;F)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 178
    .end local v3    # "pkgName":Ljava/lang/String;
    .end local v4    # "volume":F
    goto :goto_1

    .line 179
    :cond_2
    nop

    .line 183
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 179
    const/4 v0, 0x1

    return v0

    .line 183
    :catchall_0
    move-exception v0

    goto :goto_2

    .line 180
    :catch_1
    move-exception v3

    .line 181
    .local v3, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v4, "loadAppsVolume: init player Volume error"

    invoke-static {v0, v4, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 183
    nop

    .end local v3    # "e":Ljava/lang/Exception;
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 184
    nop

    .line 186
    return v2

    .line 183
    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 184
    throw v0
.end method

.method public loadSoundAssistSettings(Landroid/content/ContentResolver;)V
    .locals 3
    .param p1, "cr"    # Landroid/content/ContentResolver;

    .line 288
    if-nez p1, :cond_0

    .line 289
    return-void

    .line 291
    :cond_0
    const-string/jumbo v0, "sound_assist_key"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mOpenSoundAssist:Z

    .line 293
    const-string v0, "key_ignore_music_focus_req"

    invoke-static {p1, v0, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v2, :cond_2

    move v1, v2

    :cond_2
    iput-boolean v1, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mIsForceIgnoreGranted:Z

    .line 295
    return-void
.end method

.method public resetPlayerVolume(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .line 136
    if-nez p1, :cond_0

    .line 137
    return-void

    .line 139
    :cond_0
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 140
    .local v0, "am":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->getActivePlaybackConfigurations()Ljava/util/List;

    move-result-object v1

    .line 141
    .local v1, "apcList":Ljava/util/List;, "Ljava/util/List<Landroid/media/AudioPlaybackConfiguration;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "resetPlayerVolume size="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "PlaybackActivityMonitorStubImpl"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/media/AudioPlaybackConfiguration;

    .line 143
    .local v4, "apc":Landroid/media/AudioPlaybackConfiguration;
    invoke-virtual {v4}, Landroid/media/AudioPlaybackConfiguration;->getClientUid()I

    move-result v5

    const/16 v6, 0x2710

    if-le v5, v6, :cond_1

    .line 144
    invoke-direct {p0, v4}, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->isMusicPlayerActive(Landroid/media/AudioPlaybackConfiguration;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 146
    :try_start_0
    invoke-direct {p0, v4, p1}, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->getPkgName(Landroid/media/AudioPlaybackConfiguration;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 147
    .local v5, "pkgName":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "resetPlayerVolume pkgName="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    invoke-virtual {v4}, Landroid/media/AudioPlaybackConfiguration;->getPlayerProxy()Landroid/media/PlayerProxy;

    move-result-object v6

    invoke-virtual {p0, v5}, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->getPlayerVolume(Ljava/lang/String;)F

    move-result v7

    invoke-virtual {v6, v7}, Landroid/media/PlayerProxy;->setVolume(F)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 151
    .end local v5    # "pkgName":Ljava/lang/String;
    goto :goto_1

    .line 149
    :catch_0
    move-exception v5

    .line 150
    .local v5, "e":Ljava/lang/Exception;
    const-string/jumbo v6, "setPlayerVolume error in resetPlayerVolume"

    invoke-static {v3, v6, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 153
    .end local v4    # "apc":Landroid/media/AudioPlaybackConfiguration;
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_1
    goto :goto_0

    .line 154
    :cond_2
    return-void
.end method

.method public setForceIgnoreGrantedStatus(Z)V
    .locals 0
    .param p1, "open"    # Z

    .line 74
    iput-boolean p1, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mIsForceIgnoreGranted:Z

    .line 75
    return-void
.end method

.method public setPlayerVolume(Landroid/media/AudioPlaybackConfiguration;FLjava/lang/String;Landroid/content/Context;)V
    .locals 8
    .param p1, "apc"    # Landroid/media/AudioPlaybackConfiguration;
    .param p2, "volume"    # F
    .param p3, "from"    # Ljava/lang/String;
    .param p4, "context"    # Landroid/content/Context;

    .line 210
    iget-boolean v0, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mOpenSoundAssist:Z

    if-eqz v0, :cond_8

    if-nez p4, :cond_0

    goto/16 :goto_5

    .line 213
    :cond_0
    iget-object v0, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mMusicVolumeMap:Ljava/util/HashMap;

    monitor-enter v0

    .line 214
    :try_start_0
    invoke-virtual {p1}, Landroid/media/AudioPlaybackConfiguration;->getClientUid()I

    move-result v1

    const/16 v2, 0x2710

    if-ge v1, v2, :cond_1

    .line 215
    const-string v1, "PlaybackActivityMonitorStubImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setPlayerVolume skip system uid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/media/AudioPlaybackConfiguration;->getClientUid()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    monitor-exit v0

    return-void

    .line 218
    :cond_1
    invoke-direct {p0, p1, p4}, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->getPkgName(Landroid/media/AudioPlaybackConfiguration;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 219
    .local v1, "pkg":Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 220
    iget-object v2, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mMusicVolumeMap:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    .end local v1    # "pkg":Ljava/lang/String;
    :cond_2
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225
    const/high16 v0, 0x3f800000    # 1.0f

    :try_start_1
    invoke-direct {p0, p1}, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->isMusicPlayerActive(Landroid/media/AudioPlaybackConfiguration;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 226
    invoke-virtual {p1}, Landroid/media/AudioPlaybackConfiguration;->getPlayerProxy()Landroid/media/PlayerProxy;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/media/PlayerProxy;->setVolume(F)V

    goto :goto_0

    .line 228
    :cond_3
    invoke-virtual {p1}, Landroid/media/AudioPlaybackConfiguration;->getPlayerProxy()Landroid/media/PlayerProxy;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/media/PlayerProxy;->setVolume(F)V

    .line 229
    const-string v1, "PlaybackActivityMonitorStubImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "set volume is only for active music player"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 233
    :goto_0
    goto :goto_1

    .line 231
    :catch_0
    move-exception v1

    .line 232
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "PlaybackActivityMonitorStubImpl"

    const-string/jumbo v3, "setPlayerVolume "

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 234
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    const-string v1, "audio"

    invoke-virtual {p4, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    .line 235
    .local v1, "am":Landroid/media/AudioManager;
    invoke-virtual {v1}, Landroid/media/AudioManager;->getActivePlaybackConfigurations()Ljava/util/List;

    move-result-object v2

    .line 236
    .local v2, "apcList":Ljava/util/List;, "Ljava/util/List<Landroid/media/AudioPlaybackConfiguration;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/media/AudioPlaybackConfiguration;

    .line 237
    .local v4, "apcNew":Landroid/media/AudioPlaybackConfiguration;
    invoke-virtual {p1, v4}, Landroid/media/AudioPlaybackConfiguration;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-virtual {p1}, Landroid/media/AudioPlaybackConfiguration;->getClientUid()I

    move-result v5

    invoke-virtual {v4}, Landroid/media/AudioPlaybackConfiguration;->getClientUid()I

    move-result v6

    if-eq v5, v6, :cond_5

    .line 238
    goto :goto_2

    .line 241
    :cond_5
    :try_start_2
    invoke-direct {p0, v4}, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->isMusicPlayerActive(Landroid/media/AudioPlaybackConfiguration;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 242
    invoke-virtual {v4}, Landroid/media/AudioPlaybackConfiguration;->getPlayerProxy()Landroid/media/PlayerProxy;

    move-result-object v5

    invoke-virtual {v5, p2}, Landroid/media/PlayerProxy;->setVolume(F)V

    goto :goto_3

    .line 244
    :cond_6
    invoke-virtual {v4}, Landroid/media/AudioPlaybackConfiguration;->getPlayerProxy()Landroid/media/PlayerProxy;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/media/PlayerProxy;->setVolume(F)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 248
    :goto_3
    goto :goto_4

    .line 246
    :catch_1
    move-exception v5

    .line 247
    .local v5, "e":Ljava/lang/Exception;
    const-string v6, "PlaybackActivityMonitorStubImpl"

    const-string/jumbo v7, "setPlayerVolume in same process error"

    invoke-static {v6, v7, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 249
    .end local v4    # "apcNew":Landroid/media/AudioPlaybackConfiguration;
    .end local v5    # "e":Ljava/lang/Exception;
    :goto_4
    goto :goto_2

    .line 250
    :cond_7
    return-void

    .line 222
    .end local v1    # "am":Landroid/media/AudioManager;
    .end local v2    # "apcList":Ljava/util/List;, "Ljava/util/List<Landroid/media/AudioPlaybackConfiguration;>;"
    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1

    .line 211
    :cond_8
    :goto_5
    return-void
.end method

.method public setRouteCastUID(Ljava/lang/String;)V
    .locals 6
    .param p1, "keyValuePairs"    # Ljava/lang/String;

    .line 299
    const-string v0, "routecast=off"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, -0x1

    if-eqz v0, :cond_0

    .line 300
    iput v1, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mRouteCastUID:I

    .line 301
    return-void

    .line 303
    :cond_0
    const-string v0, "routecast=on"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 305
    :try_start_0
    const-string v0, ";"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 306
    .local v0, "params":[Ljava/lang/String;
    array-length v2, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, v0, v3

    .line 307
    .local v4, "str":Ljava/lang/String;
    const-string/jumbo v5, "uid="

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 308
    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mRouteCastUID:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 306
    .end local v4    # "str":Ljava/lang/String;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 314
    .end local v0    # "params":[Ljava/lang/String;
    :cond_2
    goto :goto_1

    .line 311
    :catch_0
    move-exception v0

    .line 312
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 313
    iput v1, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mRouteCastUID:I

    .line 316
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    :goto_1
    return-void
.end method

.method public setSoundAssistStatus(Z)V
    .locals 2
    .param p1, "open"    # Z

    .line 62
    iget-object v0, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->objKeyLock:Ljava/lang/Object;

    monitor-enter v0

    .line 63
    :try_start_0
    iput-boolean p1, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mOpenSoundAssist:Z

    .line 64
    monitor-exit v0

    .line 65
    return-void

    .line 64
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public soundAssistPlayState(Landroid/media/AudioPlaybackConfiguration;IILandroid/content/Context;)V
    .locals 1
    .param p1, "apc"    # Landroid/media/AudioPlaybackConfiguration;
    .param p2, "event"    # I
    .param p3, "eventValue"    # I
    .param p4, "context"    # Landroid/content/Context;

    .line 53
    invoke-virtual {p0}, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->isSoundAssistOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    invoke-virtual {p1, p2, p3}, Landroid/media/AudioPlaybackConfiguration;->handleStateEvent(II)Z

    .line 56
    const-string v0, "playerEvent"

    invoke-virtual {p0, p1, v0, p4}, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->updatePlayerVolumeByApc(Landroid/media/AudioPlaybackConfiguration;Ljava/lang/String;Landroid/content/Context;)V

    .line 58
    :cond_0
    return-void
.end method

.method public startPlayerVolumeService(Landroid/content/Context;II)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "streamType"    # I
    .param p3, "flags"    # I

    .line 192
    const-string v0, "com.miui.misound.playervolume.VolumeUIService"

    iget-boolean v1, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mOpenSoundAssist:Z

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 194
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 195
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 196
    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.miui.misound"

    invoke-direct {v2, v3, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 198
    const-string/jumbo v0, "streamType"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 199
    const-string v0, "flags"

    invoke-virtual {v1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 200
    invoke-virtual {p1, v1}, Landroid/content/Context;->startForegroundService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 203
    nop

    .end local v1    # "intent":Landroid/content/Intent;
    goto :goto_0

    .line 201
    :catch_0
    move-exception v0

    .line 202
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "PlaybackActivityMonitorStubImpl"

    const-string v2, "fail to start VolumeUIService"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return-void
.end method

.method public updatePlayerVolume(Landroid/media/AudioPlaybackConfiguration;ILandroid/content/Context;)V
    .locals 1
    .param p1, "apc"    # Landroid/media/AudioPlaybackConfiguration;
    .param p2, "event"    # I
    .param p3, "context"    # Landroid/content/Context;

    .line 254
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 255
    const-string v0, "playerEvent"

    invoke-virtual {p0, p1, v0, p3}, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->updatePlayerVolumeByApc(Landroid/media/AudioPlaybackConfiguration;Ljava/lang/String;Landroid/content/Context;)V

    .line 257
    :cond_0
    return-void
.end method

.method public updatePlayerVolumeByApc(Landroid/media/AudioPlaybackConfiguration;Ljava/lang/String;Landroid/content/Context;)V
    .locals 2
    .param p1, "apc"    # Landroid/media/AudioPlaybackConfiguration;
    .param p2, "from"    # Ljava/lang/String;
    .param p3, "context"    # Landroid/content/Context;

    .line 276
    iget-boolean v0, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mOpenSoundAssist:Z

    if-nez v0, :cond_0

    .line 277
    return-void

    .line 279
    :cond_0
    invoke-direct {p0, p1, p3}, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->getPkgName(Landroid/media/AudioPlaybackConfiguration;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 280
    .local v0, "pkgName":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 281
    invoke-virtual {p0, v0}, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->getPlayerVolume(Ljava/lang/String;)F

    move-result v1

    .line 282
    .local v1, "vol":F
    invoke-virtual {p0, p1, v1, p2, p3}, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->setPlayerVolume(Landroid/media/AudioPlaybackConfiguration;FLjava/lang/String;Landroid/content/Context;)V

    .line 284
    .end local v1    # "vol":F
    :cond_1
    return-void
.end method
