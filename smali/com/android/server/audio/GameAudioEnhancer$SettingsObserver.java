class com.android.server.audio.GameAudioEnhancer$SettingsObserver extends android.database.ContentObserver {
	 /* .source "GameAudioEnhancer.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/audio/GameAudioEnhancer; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "SettingsObserver" */
} // .end annotation
/* # instance fields */
final com.android.server.audio.GameAudioEnhancer this$0; //synthetic
/* # direct methods */
 com.android.server.audio.GameAudioEnhancer$SettingsObserver ( ) {
/* .locals 3 */
/* .line 104 */
this.this$0 = p1;
/* .line 105 */
/* new-instance v0, Landroid/os/Handler; */
/* invoke-direct {v0}, Landroid/os/Handler;-><init>()V */
/* invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 106 */
com.android.server.audio.GameAudioEnhancer .-$$Nest$fgetmContentResolver ( p1 );
final String v1 = "game_mode_enable"; // const-string v1, "game_mode_enable"
android.provider.Settings$Global .getUriFor ( v1 );
int v2 = 0; // const/4 v2, 0x0
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0 ); // invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 108 */
com.android.server.audio.GameAudioEnhancer .-$$Nest$fgetmContentResolver ( p1 );
final String v0 = "game_mode_packages"; // const-string v0, "game_mode_packages"
android.provider.Settings$Global .getUriFor ( v0 );
(( android.content.ContentResolver ) p1 ).registerContentObserver ( v0, v2, p0 ); // invoke-virtual {p1, v0, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 110 */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .param p3, "userId" # I */
/* .line 114 */
/* invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V */
/* .line 115 */
final String v0 = "game_mode_enable"; // const-string v0, "game_mode_enable"
android.provider.Settings$Global .getUriFor ( v0 );
/* .line 116 */
/* .local v0, "audioEnhancerUri":Landroid/net/Uri; */
/* if-ne p2, v0, :cond_0 */
/* .line 118 */
v1 = this.this$0;
com.android.server.audio.GameAudioEnhancer .-$$Nest$mupdateGameModeSettingstatus ( v1 );
/* .line 120 */
} // :cond_0
v1 = this.this$0;
com.android.server.audio.GameAudioEnhancer .-$$Nest$fgetmLock ( v1 );
/* monitor-enter v1 */
/* .line 122 */
try { // :try_start_0
v2 = this.this$0;
com.android.server.audio.GameAudioEnhancer .-$$Nest$mupdateWhiteList ( v2 );
/* .line 123 */
/* monitor-exit v1 */
/* .line 125 */
} // :goto_0
return;
/* .line 123 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
