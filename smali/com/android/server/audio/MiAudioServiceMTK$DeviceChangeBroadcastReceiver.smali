.class Lcom/android/server/audio/MiAudioServiceMTK$DeviceChangeBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MiAudioServiceMTK.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/MiAudioServiceMTK;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeviceChangeBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/audio/MiAudioServiceMTK;


# direct methods
.method private constructor <init>(Lcom/android/server/audio/MiAudioServiceMTK;)V
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/android/server/audio/MiAudioServiceMTK$DeviceChangeBroadcastReceiver;->this$0:Lcom/android/server/audio/MiAudioServiceMTK;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/audio/MiAudioServiceMTK;Lcom/android/server/audio/MiAudioServiceMTK$DeviceChangeBroadcastReceiver-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/audio/MiAudioServiceMTK$DeviceChangeBroadcastReceiver;-><init>(Lcom/android/server/audio/MiAudioServiceMTK;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 119
    iget-object v0, p0, Lcom/android/server/audio/MiAudioServiceMTK$DeviceChangeBroadcastReceiver;->this$0:Lcom/android/server/audio/MiAudioServiceMTK;

    iget-object v0, v0, Lcom/android/server/audio/MiAudioServiceMTK;->mDolbyEffectController:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    if-eqz v0, :cond_0

    .line 120
    const-string v0, "MiAudioServiceMTK"

    const-string v1, "DeviceChangeBroadcastReceiver onReceive"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    iget-object v0, p0, Lcom/android/server/audio/MiAudioServiceMTK$DeviceChangeBroadcastReceiver;->this$0:Lcom/android/server/audio/MiAudioServiceMTK;

    iget-object v0, v0, Lcom/android/server/audio/MiAudioServiceMTK;->mDolbyEffectController:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->receiveDeviceConnectStateChanged(Landroid/content/Context;Landroid/content/Intent;)V

    .line 123
    :cond_0
    return-void
.end method
