.class public Lcom/android/server/audio/MiAudioServiceMTK;
.super Ljava/lang/Object;
.source "MiAudioServiceMTK.java"

# interfaces
.implements Landroid/media/Spatializer$OnSpatializerStateChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/audio/MiAudioServiceMTK$SpatializerType;,
        Lcom/android/server/audio/MiAudioServiceMTK$DeviceChangeBroadcastReceiver;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MiAudioServiceMTK"


# instance fields
.field private final SPATIALIZER_PARAM_SPATIALIZER_TYPE:I

.field private final SPATIALIZER_PARAM_SPEAKER_ROTATION:I

.field private mAudioManager:Landroid/media/AudioManager;

.field private mAudioService:Lcom/android/server/audio/AudioService;

.field final mContext:Landroid/content/Context;

.field private mDeviceConnectStateListener:Lcom/android/server/audio/MiAudioServiceMTK$DeviceChangeBroadcastReceiver;

.field mDolbyEffectController:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

.field private mEffecImplementerObserver:Landroid/database/ContentObserver;

.field private mEffecImplementerUri:Landroid/net/Uri;

.field private final mIsSupportedDolbyEffectControl:Z

.field private final mIsSupportedSelectedDolbyTunningByVolume:Z

.field private mSpatializer:Landroid/media/Spatializer;

.field private mSpatializerEnabled:Z

.field private mSpatilizerType:I

.field private final mUseXiaoMiSpatilizer:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetmAudioService(Lcom/android/server/audio/MiAudioServiceMTK;)Lcom/android/server/audio/AudioService;
    .locals 0

    iget-object p0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mAudioService:Lcom/android/server/audio/AudioService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSpatilizerType(Lcom/android/server/audio/MiAudioServiceMTK;)I
    .locals 0

    iget p0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mSpatilizerType:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmUseXiaoMiSpatilizer(Lcom/android/server/audio/MiAudioServiceMTK;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mUseXiaoMiSpatilizer:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmSpatilizerType(Lcom/android/server/audio/MiAudioServiceMTK;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mSpatilizerType:I

    return-void
.end method

.method static bridge synthetic -$$Nest$mintToBytes(Lcom/android/server/audio/MiAudioServiceMTK;I)[B
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/audio/MiAudioServiceMTK;->intToBytes(I)[B

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/server/audio/AudioService;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "audioservice"    # Lcom/android/server/audio/AudioService;

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/16 v0, 0x130

    iput v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->SPATIALIZER_PARAM_SPEAKER_ROTATION:I

    .line 42
    const/16 v0, 0x120

    iput v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->SPATIALIZER_PARAM_SPATIALIZER_TYPE:I

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mSpatializerEnabled:Z

    .line 44
    sget-object v0, Lcom/android/server/audio/MiAudioServiceMTK$SpatializerType;->DOLBY:Lcom/android/server/audio/MiAudioServiceMTK$SpatializerType;

    invoke-virtual {v0}, Lcom/android/server/audio/MiAudioServiceMTK$SpatializerType;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mSpatilizerType:I

    .line 50
    nop

    .line 51
    const-string/jumbo v0, "vendor.audio.dolby.control.support"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mIsSupportedDolbyEffectControl:Z

    .line 52
    nop

    .line 53
    const-string/jumbo v0, "vendor.audio.dolby.control.tunning.by.volume.support"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mIsSupportedSelectedDolbyTunningByVolume:Z

    .line 54
    nop

    .line 55
    const-string/jumbo v0, "vendor.audio.useXiaomiSpatializer"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mUseXiaoMiSpatilizer:Z

    .line 60
    iput-object p1, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mContext:Landroid/content/Context;

    .line 61
    iput-object p2, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mAudioService:Lcom/android/server/audio/AudioService;

    .line 62
    const-string v0, "MiAudioServiceMTK"

    const-string v1, "MiAudioServiceMTK()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    return-void
.end method

.method private intToBytes(I)[B
    .locals 3
    .param p1, "src"    # I

    .line 215
    const/4 v0, 0x4

    new-array v0, v0, [B

    .line 216
    .local v0, "result":[B
    shr-int/lit8 v1, p1, 0x18

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    const/4 v2, 0x3

    aput-byte v1, v0, v2

    .line 217
    shr-int/lit8 v1, p1, 0x10

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    const/4 v2, 0x2

    aput-byte v1, v0, v2

    .line 218
    shr-int/lit8 v1, p1, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    const/4 v2, 0x1

    aput-byte v1, v0, v2

    .line 219
    and-int/lit16 v1, p1, 0xff

    int-to-byte v1, v1

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 220
    return-object v0
.end method


# virtual methods
.method public DeviceConectedStateListenerInit()V
    .locals 3

    .line 105
    new-instance v0, Lcom/android/server/audio/MiAudioServiceMTK$DeviceChangeBroadcastReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/audio/MiAudioServiceMTK$DeviceChangeBroadcastReceiver;-><init>(Lcom/android/server/audio/MiAudioServiceMTK;Lcom/android/server/audio/MiAudioServiceMTK$DeviceChangeBroadcastReceiver-IA;)V

    iput-object v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mDeviceConnectStateListener:Lcom/android/server/audio/MiAudioServiceMTK$DeviceChangeBroadcastReceiver;

    .line 106
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 107
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 109
    const-string v1, "android.bluetooth.headset.action.VENDOR_SPECIFIC_HEADSET_EVENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 110
    const-string v1, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 111
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "android.bluetooth.headset.intent.category.companyid."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 112
    const/16 v2, 0x38f

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 111
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 113
    iget-object v1, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mDeviceConnectStateListener:Lcom/android/server/audio/MiAudioServiceMTK$DeviceChangeBroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 114
    return-void
.end method

.method public DolbyEffectControllerInit()V
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->getInstance(Landroid/content/Context;)Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mDolbyEffectController:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    .line 80
    if-eqz v0, :cond_0

    .line 81
    invoke-virtual {v0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->init()V

    .line 83
    :cond_0
    return-void
.end method

.method public SettingsObserver()V
    .locals 4

    .line 127
    const-string v0, "effect_implementer"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mEffecImplementerUri:Landroid/net/Uri;

    .line 128
    new-instance v0, Lcom/android/server/audio/MiAudioServiceMTK$1;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/audio/MiAudioServiceMTK$1;-><init>(Lcom/android/server/audio/MiAudioServiceMTK;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mEffecImplementerObserver:Landroid/database/ContentObserver;

    .line 151
    iget-object v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mEffecImplementerUri:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mEffecImplementerObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 152
    return-void
.end method

.method public SpatialStateInit()V
    .locals 3

    .line 86
    iget-object v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mContext:Landroid/content/Context;

    const-class v1, Landroid/media/AudioManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mAudioManager:Landroid/media/AudioManager;

    .line 87
    if-eqz v0, :cond_0

    .line 88
    invoke-virtual {v0}, Landroid/media/AudioManager;->getSpatializer()Landroid/media/Spatializer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mSpatializer:Landroid/media/Spatializer;

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mSpatializer:Landroid/media/Spatializer;

    if-eqz v0, :cond_1

    .line 91
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Landroid/media/Spatializer;->addOnSpatializerStateChangedListener(Ljava/util/concurrent/Executor;Landroid/media/Spatializer$OnSpatializerStateChangedListener;)V

    .line 93
    :cond_1
    iget-object v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "effect_implementer"

    invoke-static {v0, v1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 94
    .local v0, "effecImplementer":Ljava/lang/String;
    const-string v1, "dolby"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 95
    sget-object v1, Lcom/android/server/audio/MiAudioServiceMTK$SpatializerType;->DOLBY:Lcom/android/server/audio/MiAudioServiceMTK$SpatializerType;

    invoke-virtual {v1}, Lcom/android/server/audio/MiAudioServiceMTK$SpatializerType;->ordinal()I

    move-result v1

    iput v1, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mSpatilizerType:I

    goto :goto_0

    .line 96
    :cond_2
    const-string v1, "misound"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 97
    sget-object v1, Lcom/android/server/audio/MiAudioServiceMTK$SpatializerType;->MISOUND:Lcom/android/server/audio/MiAudioServiceMTK$SpatializerType;

    invoke-virtual {v1}, Lcom/android/server/audio/MiAudioServiceMTK$SpatializerType;->ordinal()I

    move-result v1

    iput v1, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mSpatilizerType:I

    goto :goto_0

    .line 98
    :cond_3
    const-string v1, "none"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 99
    sget-object v1, Lcom/android/server/audio/MiAudioServiceMTK$SpatializerType;->NONE:Lcom/android/server/audio/MiAudioServiceMTK$SpatializerType;

    invoke-virtual {v1}, Lcom/android/server/audio/MiAudioServiceMTK$SpatializerType;->ordinal()I

    move-result v1

    iput v1, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mSpatilizerType:I

    .line 101
    :cond_4
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Init EffectImplementer is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiAudioServiceMTK"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    return-void
.end method

.method public adjustStreamVolume(IIILjava/lang/String;Ljava/lang/String;IILjava/lang/String;ZI)V
    .locals 3
    .param p1, "streamType"    # I
    .param p2, "direction"    # I
    .param p3, "flags"    # I
    .param p4, "callingPackage"    # Ljava/lang/String;
    .param p5, "caller"    # Ljava/lang/String;
    .param p6, "uid"    # I
    .param p7, "pid"    # I
    .param p8, "attributionTag"    # Ljava/lang/String;
    .param p9, "hasModifyAudioSettings"    # Z
    .param p10, "keyEventMode"    # I

    .line 201
    iget-object v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mDolbyEffectController:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    if-eqz v0, :cond_1

    .line 202
    iget-boolean v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mIsSupportedSelectedDolbyTunningByVolume:Z

    if-nez v0, :cond_0

    .line 203
    return-void

    .line 204
    :cond_0
    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 205
    iget-object v1, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mAudioService:Lcom/android/server/audio/AudioService;

    if-eqz v1, :cond_1

    .line 206
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, Lcom/android/server/audio/AudioService;->getDeviceStreamVolume(II)I

    move-result v0

    .line 207
    .local v0, "newVolume":I
    const-string v1, "MiAudioServiceMTK"

    const-string v2, "adjustStreamVolume"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    iget-object v1, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mDolbyEffectController:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    invoke-virtual {v1, v0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->receiveVolumeChanged(I)V

    .line 212
    .end local v0    # "newVolume":I
    :cond_1
    return-void
.end method

.method public functionsRoadedInit()V
    .locals 1

    .line 70
    iget-boolean v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mIsSupportedDolbyEffectControl:Z

    if-eqz v0, :cond_0

    .line 71
    invoke-virtual {p0}, Lcom/android/server/audio/MiAudioServiceMTK;->DolbyEffectControllerInit()V

    .line 72
    invoke-virtual {p0}, Lcom/android/server/audio/MiAudioServiceMTK;->DeviceConectedStateListenerInit()V

    .line 74
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/audio/MiAudioServiceMTK;->SettingsObserver()V

    .line 75
    invoke-virtual {p0}, Lcom/android/server/audio/MiAudioServiceMTK;->SpatialStateInit()V

    .line 76
    return-void
.end method

.method public onRotationUpdate(Ljava/lang/Integer;)V
    .locals 5
    .param p1, "rotation"    # Ljava/lang/Integer;

    .line 182
    iget-object v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mDolbyEffectController:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    const-string v1, "MiAudioServiceMTK"

    if-eqz v0, :cond_0

    .line 183
    const-string v0, "onRotationUpdate, receiveRotationChanged"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    iget-object v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mDolbyEffectController:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->receiveRotationChanged(I)V

    .line 186
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mUseXiaoMiSpatilizer:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mAudioService:Lcom/android/server/audio/AudioService;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/server/audio/AudioService;->isSpatializerEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mAudioService:Lcom/android/server/audio/AudioService;

    invoke-virtual {v0}, Lcom/android/server/audio/AudioService;->isSpatializerAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 187
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/server/audio/MiAudioServiceMTK;->intToBytes(I)[B

    move-result-object v0

    .line 189
    .local v0, "Rotation":[B
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setSpatializerParameter SPATIALIZER_PARAM_SPEAKER_ROTATION = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    iget-object v2, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mAudioService:Lcom/android/server/audio/AudioService;

    const/16 v3, 0x130

    invoke-virtual {v2, v3, v0}, Lcom/android/server/audio/AudioService;->setSpatializerParameter(I[B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    goto :goto_0

    .line 191
    :catch_0
    move-exception v2

    .line 192
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setSpatializerParameter SPATIALIZER_PARAM_SPEAKER_ROTATION Exception "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    .end local v0    # "Rotation":[B
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_0
    return-void
.end method

.method public onSpatializerAvailableChanged(Landroid/media/Spatializer;Z)V
    .locals 2
    .param p1, "spat"    # Landroid/media/Spatializer;
    .param p2, "available"    # Z

    .line 175
    iget-object v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mDolbyEffectController:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    if-eqz v0, :cond_0

    .line 176
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onSpatializerAvailableChanged: available = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiAudioServiceMTK"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    iget-object v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mDolbyEffectController:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    invoke-virtual {v0, p2}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->receiveSpatializerAvailableChanged(Z)V

    .line 179
    :cond_0
    return-void
.end method

.method public onSpatializerEnabledChanged(Landroid/media/Spatializer;Z)V
    .locals 4
    .param p1, "spat"    # Landroid/media/Spatializer;
    .param p2, "enabled"    # Z

    .line 156
    iput-boolean p2, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mSpatializerEnabled:Z

    .line 157
    iget-object v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mDolbyEffectController:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    const-string v1, "MiAudioServiceMTK"

    if-eqz v0, :cond_0

    .line 158
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSpatializerEnabledChanged: enabled = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    iget-object v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mDolbyEffectController:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    invoke-virtual {v0, p2}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->receiveSpatializerEnabledChanged(Z)V

    .line 161
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mUseXiaoMiSpatilizer:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mSpatializerEnabled:Z

    if-eqz v0, :cond_2

    .line 163
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSpatializerEnabledChanged: setSpatializerParameter mSpatilizerType = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mSpatilizerType:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    iget-object v0, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mAudioService:Lcom/android/server/audio/AudioService;

    if-eqz v0, :cond_1

    .line 165
    iget v2, p0, Lcom/android/server/audio/MiAudioServiceMTK;->mSpatilizerType:I

    invoke-direct {p0, v2}, Lcom/android/server/audio/MiAudioServiceMTK;->intToBytes(I)[B

    move-result-object v2

    const/16 v3, 0x120

    invoke-virtual {v0, v3, v2}, Lcom/android/server/audio/AudioService;->setSpatializerParameter(I[B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 169
    :cond_1
    goto :goto_0

    .line 167
    :catch_0
    move-exception v0

    .line 168
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onSpatializerEnabledChanged Exception:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    :goto_0
    return-void
.end method

.method public onSystemReady()V
    .locals 0

    .line 66
    invoke-virtual {p0}, Lcom/android/server/audio/MiAudioServiceMTK;->functionsRoadedInit()V

    .line 67
    return-void
.end method
