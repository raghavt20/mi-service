class com.android.server.audio.GameAudioEnhancer$2 extends miui.process.IForegroundInfoListener$Stub {
	 /* .source "GameAudioEnhancer.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/audio/GameAudioEnhancer; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.audio.GameAudioEnhancer this$0; //synthetic
/* # direct methods */
 com.android.server.audio.GameAudioEnhancer$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/audio/GameAudioEnhancer; */
/* .line 210 */
this.this$0 = p1;
/* invoke-direct {p0}, Lmiui/process/IForegroundInfoListener$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onForegroundInfoChanged ( miui.process.ForegroundInfo p0 ) {
/* .locals 5 */
/* .param p1, "foregroundInfo" # Lmiui/process/ForegroundInfo; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 213 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "foreground change to "; // const-string v1, "foreground change to "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mForegroundPackageName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", last foreground is "; // const-string v1, ", last foreground is "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mLastForegroundPackageName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "GameAudioEnhancer"; // const-string v1, "GameAudioEnhancer"
android.util.Log .i ( v1,v0 );
/* .line 215 */
v0 = this.this$0;
v1 = this.mForegroundPackageName;
com.android.server.audio.GameAudioEnhancer .-$$Nest$fputmCurForegroundPkg ( v0,v1 );
/* .line 216 */
v0 = this.mLastForegroundPackageName;
/* .line 217 */
/* .local v0, "LastForegroundPkg":Ljava/lang/String; */
v1 = this.this$0;
v1 = com.android.server.audio.GameAudioEnhancer .-$$Nest$fgetmParamSet ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = this.this$0;
v1 = com.android.server.audio.GameAudioEnhancer .-$$Nest$misPackageEnabled ( v1,v0 );
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 218 */
	 v1 = this.this$0;
	 int v2 = 0; // const/4 v2, 0x0
	 com.android.server.audio.GameAudioEnhancer .-$$Nest$fputmParamSet ( v1,v2 );
	 /* .line 219 */
	 v1 = this.this$0;
	 com.android.server.audio.GameAudioEnhancer .-$$Nest$fgetmHandler ( v1 );
	 v2 = this.this$0;
	 com.android.server.audio.GameAudioEnhancer .-$$Nest$fgetmHandler ( v2 );
	 int v3 = 3; // const/4 v3, 0x3
	 (( com.android.server.audio.GameAudioEnhancer$WorkHandler ) v2 ).obtainMessage ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;->obtainMessage(I)Landroid/os/Message;
	 /* const-wide/16 v3, 0x1f4 */
	 (( com.android.server.audio.GameAudioEnhancer$WorkHandler ) v1 ).sendMessageDelayed ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
	 /* .line 222 */
} // :cond_0
v1 = this.this$0;
com.android.server.audio.GameAudioEnhancer .-$$Nest$fgetmCurForegroundPkg ( v1 );
com.android.server.audio.GameAudioEnhancer .-$$Nest$msetParamSend ( v1,v2 );
/* .line 223 */
return;
} // .end method
