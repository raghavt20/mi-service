.class final Lcom/android/server/audio/CloudServiceThread;
.super Ljava/lang/Thread;
.source "CloudServiceThread.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mCloudObserver:Landroid/database/ContentObserver;

.field private mCloudServiceSettings:Lcom/android/server/audio/CloudServiceSettings;

.field private mContext:Landroid/content/Context;

.field private mCurrentVersion:Ljava/lang/String;

.field private mDebugTriggerUri:Landroid/net/Uri;

.field private mLock:Ljava/lang/Object;

.field private mStop:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetmLock(Lcom/android/server/audio/CloudServiceThread;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/android/server/audio/CloudServiceThread;->mLock:Ljava/lang/Object;

    return-object p0
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "ctx"    # Landroid/content/Context;

    .line 41
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 23
    const-string v0, "CloudServiceThread"

    iput-object v0, p0, Lcom/android/server/audio/CloudServiceThread;->TAG:Ljava/lang/String;

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/audio/CloudServiceThread;->mStop:Z

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/audio/CloudServiceThread;->mCurrentVersion:Ljava/lang/String;

    .line 33
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/audio/CloudServiceThread;->mLock:Ljava/lang/Object;

    .line 42
    iput-object p1, p0, Lcom/android/server/audio/CloudServiceThread;->mContext:Landroid/content/Context;

    .line 43
    invoke-direct {p0}, Lcom/android/server/audio/CloudServiceThread;->loadSettings()V

    .line 44
    const-string/jumbo v0, "test_audio_cloud"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/audio/CloudServiceThread;->mDebugTriggerUri:Landroid/net/Uri;

    .line 45
    new-instance v0, Lcom/android/server/audio/CloudServiceThread$1;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/audio/CloudServiceThread$1;-><init>(Lcom/android/server/audio/CloudServiceThread;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/audio/CloudServiceThread;->mCloudObserver:Landroid/database/ContentObserver;

    .line 57
    iget-object v0, p0, Lcom/android/server/audio/CloudServiceThread;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/audio/CloudServiceThread;->mCloudObserver:Landroid/database/ContentObserver;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 59
    iget-object v0, p0, Lcom/android/server/audio/CloudServiceThread;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/audio/CloudServiceThread;->mDebugTriggerUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/server/audio/CloudServiceThread;->mCloudObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 61
    return-void
.end method

.method private loadSettings()V
    .locals 2

    .line 85
    new-instance v0, Lcom/android/server/audio/CloudServiceSettings;

    iget-object v1, p0, Lcom/android/server/audio/CloudServiceThread;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/server/audio/CloudServiceSettings;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/audio/CloudServiceThread;->mCloudServiceSettings:Lcom/android/server/audio/CloudServiceSettings;

    .line 86
    return-void
.end method

.method private triggerSettings()V
    .locals 5

    .line 89
    iget-object v0, p0, Lcom/android/server/audio/CloudServiceThread;->mCloudServiceSettings:Lcom/android/server/audio/CloudServiceSettings;

    iget-object v0, v0, Lcom/android/server/audio/CloudServiceSettings;->mSettings:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/audio/CloudServiceSettings$Setting;

    .line 90
    .local v1, "s":Lcom/android/server/audio/CloudServiceSettings$Setting;
    invoke-virtual {v1}, Lcom/android/server/audio/CloudServiceSettings$Setting;->set()Z

    move-result v2

    const-string v3, "CloudServiceThread"

    if-eqz v2, :cond_0

    .line 91
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v1, Lcom/android/server/audio/CloudServiceSettings$Setting;->mSettingName:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " set successfully!"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 93
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v1, Lcom/android/server/audio/CloudServiceSettings$Setting;->mSettingName:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " didn\'t set !"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    .end local v1    # "s":Lcom/android/server/audio/CloudServiceSettings$Setting;
    :goto_1
    goto :goto_0

    .line 96
    :cond_1
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 65
    nop

    :goto_0
    iget-boolean v0, p0, Lcom/android/server/audio/CloudServiceThread;->mStop:Z

    if-nez v0, :cond_2

    .line 66
    const-string v0, "CloudServiceThread"

    const-string v1, "enter run"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    const-string v0, ""

    iget-object v1, p0, Lcom/android/server/audio/CloudServiceThread;->mCloudServiceSettings:Lcom/android/server/audio/CloudServiceSettings;

    iget-object v1, v1, Lcom/android/server/audio/CloudServiceSettings;->mVersionCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/audio/CloudServiceThread;->mCloudServiceSettings:Lcom/android/server/audio/CloudServiceSettings;

    iget-object v0, v0, Lcom/android/server/audio/CloudServiceSettings;->mVersionCode:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/server/audio/CloudServiceThread;->mCurrentVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_1

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/android/server/audio/CloudServiceThread;->mCloudServiceSettings:Lcom/android/server/audio/CloudServiceSettings;

    iget-object v0, v0, Lcom/android/server/audio/CloudServiceSettings;->mVersionCode:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/server/audio/CloudServiceThread;->mCurrentVersion:Ljava/lang/String;

    .line 69
    const-string v0, "CloudServiceThread"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "update current version "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/audio/CloudServiceThread;->mCurrentVersion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    invoke-direct {p0}, Lcom/android/server/audio/CloudServiceThread;->triggerSettings()V

    .line 72
    :cond_1
    iget-object v0, p0, Lcom/android/server/audio/CloudServiceThread;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 73
    :try_start_0
    const-string v1, "CloudServiceThread"

    const-string v2, "enter wait"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75
    :try_start_1
    iget-object v1, p0, Lcom/android/server/audio/CloudServiceThread;->mLock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 78
    goto :goto_1

    .line 76
    :catch_0
    move-exception v1

    .line 77
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "CloudServiceThread"

    const-string v3, "enter wait exception"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 80
    iget-object v0, p0, Lcom/android/server/audio/CloudServiceThread;->mCloudServiceSettings:Lcom/android/server/audio/CloudServiceSettings;

    invoke-virtual {v0}, Lcom/android/server/audio/CloudServiceSettings;->fetchDataAll()V

    goto :goto_0

    .line 79
    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1

    .line 82
    :cond_2
    return-void
.end method
