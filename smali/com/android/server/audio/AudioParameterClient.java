public class com.android.server.audio.AudioParameterClient implements android.os.IBinder$DeathRecipient {
	 /* .source "AudioParameterClient.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/audio/AudioParameterClient$ClientDeathListener; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
/* # instance fields */
private com.android.server.audio.AudioParameterClient$ClientDeathListener callback;
private final android.os.IBinder clientBinder;
private java.lang.String targetParameter;
/* # direct methods */
 com.android.server.audio.AudioParameterClient ( ) {
	 /* .locals 0 */
	 /* .param p1, "binder" # Landroid/os/IBinder; */
	 /* .param p2, "targetParameter" # Ljava/lang/String; */
	 /* .line 34 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 35 */
	 this.clientBinder = p1;
	 /* .line 36 */
	 this.targetParameter = p2;
	 /* .line 37 */
	 return;
} // .end method
/* # virtual methods */
public void binderDied ( ) {
	 /* .locals 3 */
	 /* .line 66 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "AudioParameterClient binderDied "; // const-string v1, "AudioParameterClient binderDied "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v1 = this.targetParameter;
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v1 = "AudioParameterClient"; // const-string v1, "AudioParameterClient"
	 android.util.Log .d ( v1,v0 );
	 /* .line 67 */
	 v0 = this.callback;
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 68 */
		 v1 = this.clientBinder;
		 v2 = this.targetParameter;
		 /* .line 70 */
	 } // :cond_0
	 return;
} // .end method
public android.os.IBinder getBinder ( ) {
	 /* .locals 1 */
	 /* .line 73 */
	 v0 = this.clientBinder;
} // .end method
public java.lang.String getTargetParameter ( ) {
	 /* .locals 1 */
	 /* .line 77 */
	 v0 = this.targetParameter;
} // .end method
public Boolean registerDeathRecipient ( ) {
	 /* .locals 4 */
	 /* .line 44 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 46 */
	 /* .local v0, "status":Z */
	 try { // :try_start_0
		 v1 = this.clientBinder;
		 int v2 = 0; // const/4 v2, 0x0
		 /* :try_end_0 */
		 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 47 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* .line 52 */
		 /* .line 48 */
		 /* :catch_0 */
		 /* move-exception v1 */
		 /* .line 49 */
		 /* .local v1, "e":Landroid/os/RemoteException; */
		 /* new-instance v2, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v3 = "AudioParameterClient could not link to binder death "; // const-string v3, "AudioParameterClient could not link to binder death "
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v3 = this.targetParameter;
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 final String v3 = "AudioParameterClient"; // const-string v3, "AudioParameterClient"
		 android.util.Log .w ( v3,v2 );
		 /* .line 51 */
		 (( android.os.RemoteException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V
		 /* .line 53 */
	 } // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_0
} // .end method
public void setClientDiedListener ( com.android.server.audio.AudioParameterClient$ClientDeathListener p0 ) {
/* .locals 0 */
/* .param p1, "callback" # Lcom/android/server/audio/AudioParameterClient$ClientDeathListener; */
/* .line 40 */
this.callback = p1;
/* .line 41 */
return;
} // .end method
public void unregisterDeathRecipient ( ) {
/* .locals 3 */
/* .line 58 */
try { // :try_start_0
	 v0 = this.clientBinder;
	 int v1 = 0; // const/4 v1, 0x0
	 /* :try_end_0 */
	 /* .catch Ljava/util/NoSuchElementException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 62 */
	 /* .line 59 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 60 */
	 /* .local v0, "e":Ljava/util/NoSuchElementException; */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "AudioParameterClient could not not unregistered to binder "; // const-string v2, "AudioParameterClient could not not unregistered to binder "
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v2 = this.targetParameter;
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v2 = "AudioParameterClient"; // const-string v2, "AudioParameterClient"
	 android.util.Log .w ( v2,v1 );
	 /* .line 63 */
} // .end local v0 # "e":Ljava/util/NoSuchElementException;
} // :goto_0
return;
} // .end method
