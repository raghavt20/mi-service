class com.android.server.audio.AudioPowerSaveModeObserver$AudioPowerSaveStateListener extends com.android.server.FixedFileObserver {
	 /* .source "AudioPowerSaveModeObserver.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/audio/AudioPowerSaveModeObserver; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "AudioPowerSaveStateListener" */
} // .end annotation
/* # instance fields */
final com.android.server.audio.AudioPowerSaveModeObserver this$0; //synthetic
/* # direct methods */
public com.android.server.audio.AudioPowerSaveModeObserver$AudioPowerSaveStateListener ( ) {
/* .locals 2 */
/* .param p2, "path" # Ljava/lang/String; */
/* .line 335 */
this.this$0 = p1;
/* .line 336 */
/* invoke-direct {p0, p2}, Lcom/android/server/FixedFileObserver;-><init>(Ljava/lang/String;)V */
/* .line 337 */
com.android.server.audio.AudioPowerSaveModeObserver .-$$Nest$fgetmHandler ( p1 );
com.android.server.audio.AudioPowerSaveModeObserver .-$$Nest$fgetmHandler ( p1 );
/* const/16 v1, 0x9c8 */
(( com.android.server.audio.AudioPowerSaveModeObserver$WorkHandler ) p1 ).obtainMessage ( v1 ); // invoke-virtual {p1, v1}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->obtainMessage(I)Landroid/os/Message;
(( com.android.server.audio.AudioPowerSaveModeObserver$WorkHandler ) v0 ).sendMessage ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 338 */
return;
} // .end method
/* # virtual methods */
public void onEvent ( Integer p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "event" # I */
/* .param p2, "path" # Ljava/lang/String; */
/* .line 341 */
/* packed-switch p1, :pswitch_data_0 */
/* .line 343 */
/* :pswitch_0 */
final String v0 = "AudioPowerSaveModeObserver"; // const-string v0, "AudioPowerSaveModeObserver"
final String v1 = "MODIFY event"; // const-string v1, "MODIFY event"
android.util.Log .d ( v0,v1 );
/* .line 344 */
v0 = this.this$0;
com.android.server.audio.AudioPowerSaveModeObserver .-$$Nest$fgetmHandler ( v0 );
v1 = this.this$0;
com.android.server.audio.AudioPowerSaveModeObserver .-$$Nest$fgetmHandler ( v1 );
/* const/16 v2, 0x9c8 */
(( com.android.server.audio.AudioPowerSaveModeObserver$WorkHandler ) v1 ).obtainMessage ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->obtainMessage(I)Landroid/os/Message;
(( com.android.server.audio.AudioPowerSaveModeObserver$WorkHandler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 345 */
/* nop */
/* .line 350 */
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x2 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
