public class com.android.server.audio.AudioThermalObserver {
	 /* .source "AudioThermalObserver.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/audio/AudioThermalObserver$AudioHighTemperatureListener;, */
	 /* Lcom/android/server/audio/AudioThermalObserver$WorkHandler; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String AUDIO_HIGH_TEMPERATURE_STATE_PATH;
public static final Integer MSG_AUDIO_THERMAL_POLICY;
public static final Integer MSG_CHECK_AUDIO_HIGH_TEMPERATURE_POLICY;
public static final Integer MSG_SET_AUDIO_HIGH_TEMPERATURE_POLICY;
private static final Integer READ_WAIT_TIME_SECONDS;
private static final java.lang.String TAG;
/* # instance fields */
private com.android.server.audio.AudioThermalObserver$AudioHighTemperatureListener mAudioHighTemperatureListener;
private android.content.Context mContext;
private com.android.server.audio.AudioThermalObserver$WorkHandler mHandler;
private Integer mTempRecord;
/* # direct methods */
static com.android.server.audio.AudioThermalObserver$WorkHandler -$$Nest$fgetmHandler ( com.android.server.audio.AudioThermalObserver p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mHandler;
} // .end method
static void -$$Nest$mcheckAudioHighTemperatureMode ( com.android.server.audio.AudioThermalObserver p0, java.lang.String p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/audio/AudioThermalObserver;->checkAudioHighTemperatureMode(Ljava/lang/String;)V */
	 return;
} // .end method
static void -$$Nest$msetAudioHighTemperatureMode ( com.android.server.audio.AudioThermalObserver p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/audio/AudioThermalObserver;->setAudioHighTemperatureMode(I)V */
	 return;
} // .end method
public com.android.server.audio.AudioThermalObserver ( ) {
	 /* .locals 3 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 54 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 47 */
	 int v0 = 0; // const/4 v0, 0x0
	 this.mAudioHighTemperatureListener = v0;
	 /* .line 51 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput v0, p0, Lcom/android/server/audio/AudioThermalObserver;->mTempRecord:I */
	 /* .line 55 */
	 /* iput v0, p0, Lcom/android/server/audio/AudioThermalObserver;->mTempRecord:I */
	 /* .line 56 */
	 this.mContext = p1;
	 /* .line 57 */
	 /* new-instance v0, Landroid/os/HandlerThread; */
	 final String v1 = "AudioThermalObserver"; // const-string v1, "AudioThermalObserver"
	 /* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
	 /* .line 58 */
	 /* .local v0, "thread":Landroid/os/HandlerThread; */
	 (( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
	 /* .line 59 */
	 /* new-instance v1, Lcom/android/server/audio/AudioThermalObserver$WorkHandler; */
	 (( android.os.HandlerThread ) v0 ).getLooper ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
	 /* invoke-direct {v1, p0, v2}, Lcom/android/server/audio/AudioThermalObserver$WorkHandler;-><init>(Lcom/android/server/audio/AudioThermalObserver;Landroid/os/Looper;)V */
	 this.mHandler = v1;
	 /* .line 60 */
	 /* new-instance v1, Lcom/android/server/audio/AudioThermalObserver$AudioHighTemperatureListener; */
	 final String v2 = "/sys/class/thermal/thermal_message/video_mode"; // const-string v2, "/sys/class/thermal/thermal_message/video_mode"
	 /* invoke-direct {v1, p0, v2}, Lcom/android/server/audio/AudioThermalObserver$AudioHighTemperatureListener;-><init>(Lcom/android/server/audio/AudioThermalObserver;Ljava/lang/String;)V */
	 this.mAudioHighTemperatureListener = v1;
	 /* .line 61 */
	 /* invoke-direct {p0}, Lcom/android/server/audio/AudioThermalObserver;->watchAudioHighTemperatureListener()V */
	 /* .line 62 */
	 return;
} // .end method
private void checkAudioHighTemperatureMode ( java.lang.String p0 ) {
	 /* .locals 6 */
	 /* .param p1, "path" # Ljava/lang/String; */
	 /* .line 108 */
	 final String v0 = "AudioThermalObserver"; // const-string v0, "AudioThermalObserver"
	 int v1 = 0; // const/4 v1, 0x0
	 /* .line 109 */
	 /* .local v1, "mMode":Ljava/lang/String; */
	 int v2 = 0; // const/4 v2, 0x0
	 /* .line 111 */
	 /* .local v2, "mTempMode":I */
	 try { // :try_start_0
		 /* invoke-direct {p0, p1}, Lcom/android/server/audio/AudioThermalObserver;->getContentFromFile(Ljava/lang/String;)Ljava/lang/String; */
		 /* move-object v1, v3 */
		 if ( v3 != null) { // if-eqz v3, :cond_0
			 /* .line 112 */
			 v3 = 			 java.lang.Integer .parseInt ( v1 );
			 /* move v2, v3 */
			 /* .line 113 */
			 v3 = this.mHandler;
			 /* const/16 v4, 0x9c5 */
			 (( com.android.server.audio.AudioThermalObserver$WorkHandler ) v3 ).removeMessages ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/audio/AudioThermalObserver$WorkHandler;->removeMessages(I)V
			 /* .line 114 */
			 v3 = this.mHandler;
			 int v5 = 0; // const/4 v5, 0x0
			 (( com.android.server.audio.AudioThermalObserver$WorkHandler ) v3 ).obtainMessage ( v4, v2, v5 ); // invoke-virtual {v3, v4, v2, v5}, Lcom/android/server/audio/AudioThermalObserver$WorkHandler;->obtainMessage(III)Landroid/os/Message;
			 (( com.android.server.audio.AudioThermalObserver$WorkHandler ) v3 ).sendMessage ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/audio/AudioThermalObserver$WorkHandler;->sendMessage(Landroid/os/Message;)Z
			 /* .line 117 */
		 } // :cond_0
		 /* new-instance v3, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v4 = "HighTemperature path:"; // const-string v4, "HighTemperature path:"
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 final String v4 = "is null"; // const-string v4, "is null"
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Log .d ( v0,v3 );
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 121 */
	 } // :goto_0
	 /* .line 119 */
	 /* :catch_0 */
	 /* move-exception v3 */
	 /* .line 120 */
	 /* .local v3, "e":Ljava/lang/Exception; */
	 /* new-instance v4, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v5 = "checkAudioHighTemperatureMode "; // const-string v5, "checkAudioHighTemperatureMode "
	 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Log .e ( v0,v4 );
	 /* .line 122 */
} // .end local v3 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
private java.lang.String getContentFromFile ( java.lang.String p0 ) {
/* .locals 7 */
/* .param p1, "filePath" # Ljava/lang/String; */
/* .line 125 */
final String v0 = "can not get temp state "; // const-string v0, "can not get temp state "
final String v1 = "AudioThermalObserver"; // const-string v1, "AudioThermalObserver"
int v2 = 0; // const/4 v2, 0x0
/* .line 126 */
/* .local v2, "is":Ljava/io/FileInputStream; */
final String v3 = ""; // const-string v3, ""
/* .line 128 */
/* .local v3, "content":Ljava/lang/String; */
try { // :try_start_0
/* new-instance v4, Ljava/io/File; */
/* invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 129 */
/* .local v4, "file":Ljava/io/File; */
/* new-instance v5, Ljava/io/FileInputStream; */
/* invoke-direct {v5, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* move-object v2, v5 */
/* .line 130 */
com.android.server.audio.AudioThermalObserver .readInputStream ( v2 );
/* .line 131 */
/* .local v5, "data":[B */
/* new-instance v6, Ljava/lang/String; */
/* invoke-direct {v6, v5}, Ljava/lang/String;-><init>([B)V */
(( java.lang.String ) v6 ).trim ( ); // invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;
/* :try_end_0 */
/* .catch Ljava/io/FileNotFoundException; {:try_start_0 ..:try_end_0} :catch_5 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_3 */
/* .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move-object v3, v6 */
/* .line 139 */
} // .end local v4 # "file":Ljava/io/File;
} // .end local v5 # "data":[B
/* nop */
/* .line 141 */
try { // :try_start_1
(( java.io.FileInputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 144 */
} // :goto_0
/* goto/16 :goto_2 */
/* .line 142 */
/* :catch_0 */
/* move-exception v4 */
/* .line 143 */
/* .local v4, "e":Ljava/io/IOException; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
} // :goto_1
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .w ( v1,v0 );
} // .end local v4 # "e":Ljava/io/IOException;
/* .line 139 */
/* :catchall_0 */
/* move-exception v4 */
/* goto/16 :goto_3 */
/* .line 136 */
/* :catch_1 */
/* move-exception v4 */
/* .line 137 */
/* .local v4, "e":Ljava/lang/IndexOutOfBoundsException; */
try { // :try_start_2
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "index exception: "; // const-string v6, "index exception: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .w ( v1,v5 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 139 */
/* nop */
} // .end local v4 # "e":Ljava/lang/IndexOutOfBoundsException;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 141 */
try { // :try_start_3
(( java.io.FileInputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_2 */
/* .line 142 */
/* :catch_2 */
/* move-exception v4 */
/* .line 143 */
/* .local v4, "e":Ljava/io/IOException; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 134 */
} // .end local v4 # "e":Ljava/io/IOException;
/* :catch_3 */
/* move-exception v4 */
/* .line 135 */
/* .restart local v4 # "e":Ljava/io/IOException; */
try { // :try_start_4
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "IO exception when read file "; // const-string v6, "IO exception when read file "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .w ( v1,v5 );
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 139 */
/* nop */
} // .end local v4 # "e":Ljava/io/IOException;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 141 */
try { // :try_start_5
(( java.io.FileInputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
/* :try_end_5 */
/* .catch Ljava/io/IOException; {:try_start_5 ..:try_end_5} :catch_4 */
/* .line 142 */
/* :catch_4 */
/* move-exception v4 */
/* .line 143 */
/* .restart local v4 # "e":Ljava/io/IOException; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 132 */
} // .end local v4 # "e":Ljava/io/IOException;
/* :catch_5 */
/* move-exception v4 */
/* .line 133 */
/* .local v4, "e":Ljava/io/FileNotFoundException; */
try { // :try_start_6
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "can\'t find file "; // const-string v6, "can\'t find file "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .w ( v1,v5 );
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_0 */
/* .line 139 */
/* nop */
} // .end local v4 # "e":Ljava/io/FileNotFoundException;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 141 */
try { // :try_start_7
(( java.io.FileInputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
/* :try_end_7 */
/* .catch Ljava/io/IOException; {:try_start_7 ..:try_end_7} :catch_6 */
/* goto/16 :goto_0 */
/* .line 142 */
/* :catch_6 */
/* move-exception v4 */
/* .line 143 */
/* .local v4, "e":Ljava/io/IOException; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* goto/16 :goto_1 */
/* .line 147 */
} // .end local v4 # "e":Ljava/io/IOException;
} // :cond_0
} // :goto_2
/* .line 139 */
} // :goto_3
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 141 */
try { // :try_start_8
(( java.io.FileInputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
/* :try_end_8 */
/* .catch Ljava/io/IOException; {:try_start_8 ..:try_end_8} :catch_7 */
/* .line 144 */
/* .line 142 */
/* :catch_7 */
/* move-exception v5 */
/* .line 143 */
/* .local v5, "e":Ljava/io/IOException; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .w ( v1,v0 );
/* .line 146 */
} // .end local v5 # "e":Ljava/io/IOException;
} // :cond_1
} // :goto_4
/* throw v4 */
} // .end method
private static readInputStream ( java.io.FileInputStream p0 ) {
/* .locals 9 */
/* .param p0, "is" # Ljava/io/FileInputStream; */
/* .line 194 */
final String v0 = "readInputStream "; // const-string v0, "readInputStream "
final String v1 = "AudioThermalObserver"; // const-string v1, "AudioThermalObserver"
/* new-instance v2, Ljava/io/ByteArrayOutputStream; */
/* invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V */
/* .line 195 */
/* .local v2, "byteStream":Ljava/io/ByteArrayOutputStream; */
/* const/16 v3, 0x200 */
/* .line 196 */
/* .local v3, "blockSize":I */
/* const/16 v4, 0x200 */
/* new-array v5, v4, [B */
/* .line 197 */
/* .local v5, "buffer":[B */
int v6 = 0; // const/4 v6, 0x0
/* .line 199 */
/* .local v6, "count":I */
} // :goto_0
int v7 = 0; // const/4 v7, 0x0
try { // :try_start_0
v8 = (( java.io.FileInputStream ) p0 ).read ( v5, v7, v4 ); // invoke-virtual {p0, v5, v7, v4}, Ljava/io/FileInputStream;->read([BII)I
/* move v6, v8 */
/* if-lez v8, :cond_0 */
/* .line 200 */
(( java.io.ByteArrayOutputStream ) v2 ).write ( v5, v7, v6 ); // invoke-virtual {v2, v5, v7, v6}, Ljava/io/ByteArrayOutputStream;->write([BII)V
/* .line 202 */
} // :cond_0
(( java.io.ByteArrayOutputStream ) v2 ).toByteArray ( ); // invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 207 */
try { // :try_start_1
(( java.io.ByteArrayOutputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 210 */
/* .line 208 */
/* :catch_0 */
/* move-exception v7 */
/* .line 209 */
/* .local v7, "e":Ljava/io/IOException; */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v0 ); // invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .w ( v1,v0 );
/* .line 202 */
} // .end local v7 # "e":Ljava/io/IOException;
} // :goto_1
/* .line 206 */
/* :catchall_0 */
/* move-exception v4 */
/* .line 203 */
/* :catch_1 */
/* move-exception v4 */
/* .line 204 */
/* .local v4, "e":Ljava/lang/Exception; */
try { // :try_start_2
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .w ( v1,v7 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 207 */
} // .end local v4 # "e":Ljava/lang/Exception;
try { // :try_start_3
(( java.io.ByteArrayOutputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_2 */
/* .line 210 */
/* .line 208 */
/* :catch_2 */
/* move-exception v4 */
/* .line 209 */
/* .local v4, "e":Ljava/io/IOException; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .w ( v1,v0 );
/* .line 211 */
} // .end local v4 # "e":Ljava/io/IOException;
/* nop */
/* .line 212 */
} // :goto_2
int v0 = 0; // const/4 v0, 0x0
/* .line 207 */
} // :goto_3
try { // :try_start_4
(( java.io.ByteArrayOutputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_3 */
/* .line 210 */
/* .line 208 */
/* :catch_3 */
/* move-exception v7 */
/* .line 209 */
/* .restart local v7 # "e":Ljava/io/IOException; */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v0 ); // invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .w ( v1,v0 );
/* .line 211 */
} // .end local v7 # "e":Ljava/io/IOException;
} // :goto_4
/* throw v4 */
} // .end method
private void setAudioHighTemperatureMode ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "mHighTemperatureMode" # I */
/* .line 90 */
int v0 = 1; // const/4 v0, 0x1
/* if-eq p1, v0, :cond_0 */
if ( p1 != null) { // if-eqz p1, :cond_0
int p1 = 0; // const/4 p1, 0x0
/* .line 92 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/audio/AudioThermalObserver;->mTempRecord:I */
final String v1 = "AudioThermalObserver"; // const-string v1, "AudioThermalObserver"
/* if-eq v0, p1, :cond_1 */
/* .line 94 */
try { // :try_start_0
java.lang.String .valueOf ( p1 );
/* .line 95 */
/* .local v0, "mode":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Audio_High_Temperature_Mode="; // const-string v3, "Audio_High_Temperature_Mode="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 96 */
/* .local v2, "mParameter":Ljava/lang/String; */
android.media.AudioSystem .setParameters ( v2 );
/* .line 97 */
/* iput p1, p0, Lcom/android/server/audio/AudioThermalObserver;->mTempRecord:I */
/* .line 98 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "setAudioHighTemperatureMode: mHighTemperatureMode = " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = "; mTempRecord = "; // const-string v4, "; mTempRecord = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/android/server/audio/AudioThermalObserver;->mTempRecord:I */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 101 */
/* nop */
} // .end local v0 # "mode":Ljava/lang/String;
} // .end local v2 # "mParameter":Ljava/lang/String;
/* .line 99 */
/* :catch_0 */
/* move-exception v0 */
/* .line 100 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "setAudioHighTemperatureMode exception " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 101 */
} // .end local v0 # "e":Ljava/lang/Exception;
/* .line 103 */
} // :cond_1
final String v0 = "AudioHighTemperatureMode is no need to change"; // const-string v0, "AudioHighTemperatureMode is no need to change"
android.util.Log .d ( v1,v0 );
/* .line 105 */
} // :goto_0
return;
} // .end method
private void toSleep ( ) {
/* .locals 3 */
/* .line 152 */
try { // :try_start_0
v0 = java.util.concurrent.TimeUnit.SECONDS;
/* const-wide/16 v1, 0x3 */
(( java.util.concurrent.TimeUnit ) v0 ).sleep ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->sleep(J)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 155 */
/* .line 153 */
/* :catch_0 */
/* move-exception v0 */
/* .line 154 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "toSleep exception: " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "AudioThermalObserver"; // const-string v2, "AudioThermalObserver"
android.util.Log .e ( v2,v1 );
/* .line 156 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void unwatchAudioHighTemperatureListener ( ) {
/* .locals 3 */
/* .line 160 */
try { // :try_start_0
v0 = this.mAudioHighTemperatureListener;
(( com.android.server.audio.AudioThermalObserver$AudioHighTemperatureListener ) v0 ).stopWatching ( ); // invoke-virtual {v0}, Lcom/android/server/audio/AudioThermalObserver$AudioHighTemperatureListener;->stopWatching()V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 163 */
/* .line 161 */
/* :catch_0 */
/* move-exception v0 */
/* .line 162 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "unwatchAudioHighTemperatureListener exception " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "AudioThermalObserver"; // const-string v2, "AudioThermalObserver"
android.util.Log .e ( v2,v1 );
/* .line 164 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void watchAudioHighTemperatureListener ( ) {
/* .locals 3 */
/* .line 168 */
try { // :try_start_0
v0 = this.mAudioHighTemperatureListener;
(( com.android.server.audio.AudioThermalObserver$AudioHighTemperatureListener ) v0 ).startWatching ( ); // invoke-virtual {v0}, Lcom/android/server/audio/AudioThermalObserver$AudioHighTemperatureListener;->startWatching()V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 171 */
/* .line 169 */
/* :catch_0 */
/* move-exception v0 */
/* .line 170 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "watchAudioHighTemperatureListener exception " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "AudioThermalObserver"; // const-string v2, "AudioThermalObserver"
android.util.Log .e ( v2,v1 );
/* .line 172 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void reSetAudioCinemaModeThermal ( ) {
/* .locals 3 */
/* .line 217 */
try { // :try_start_0
v0 = this.mHandler;
/* const/16 v1, 0x9c6 */
(( com.android.server.audio.AudioThermalObserver$WorkHandler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/audio/AudioThermalObserver$WorkHandler;->obtainMessage(I)Landroid/os/Message;
(( com.android.server.audio.AudioThermalObserver$WorkHandler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/audio/AudioThermalObserver$WorkHandler;->sendMessage(Landroid/os/Message;)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 220 */
/* .line 218 */
/* :catch_0 */
/* move-exception v0 */
/* .line 219 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "reSetAudioCinemaModeThermal exception "; // const-string v2, "reSetAudioCinemaModeThermal exception "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "AudioThermalObserver"; // const-string v2, "AudioThermalObserver"
android.util.Log .e ( v2,v1 );
/* .line 221 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
