public class com.android.server.audio.MQSserver extends java.lang.Thread {
	 /* .source "MQSserver.java" */
	 /* # static fields */
	 private static final java.lang.String ANDROID_VERSION;
	 private static final Integer FLAG_NON_ANONYMOUS;
	 private static final Integer MAX_LEN;
	 private static final java.lang.String MQS_COUNT;
	 private static final java.lang.String MQS_MODULE_ID;
	 private static final java.lang.String TAG;
	 private static final java.lang.String XLOG_DEV;
	 private static volatile com.android.server.audio.MQSserver mMQSserver;
	 /* # instance fields */
	 private Integer day;
	 private java.io.FileInputStream fis;
	 private java.util.ArrayList list;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/ArrayList<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private android.content.Context mContext;
private final java.lang.Object mListLock;
private volatile Boolean mStopRequst;
private Integer month;
private Integer year;
/* # direct methods */
public static void $r8$lambda$eAnnhZVYY3cpH7-vV23vllWGpfA ( com.android.server.audio.MQSserver p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/audio/MQSserver;->lambda$asynReportData$0()V */
return;
} // .end method
static com.android.server.audio.MQSserver ( ) {
/* .locals 2 */
/* .line 30 */
final String v0 = "ro.build.version.release"; // const-string v0, "ro.build.version.release"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
return;
} // .end method
private com.android.server.audio.MQSserver ( ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 37 */
/* invoke-direct {p0}, Ljava/lang/Thread;-><init>()V */
/* .line 34 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mListLock = v0;
/* .line 35 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.list = v0;
/* .line 38 */
this.mContext = p1;
/* .line 39 */
return;
} // .end method
public static com.android.server.audio.MQSserver getInstance ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 42 */
v0 = com.android.server.audio.MQSserver.mMQSserver;
/* if-nez v0, :cond_1 */
/* .line 43 */
/* const-class v0, Lcom/android/server/audio/MQSserver; */
/* monitor-enter v0 */
/* .line 44 */
try { // :try_start_0
	 v1 = com.android.server.audio.MQSserver.mMQSserver;
	 /* if-nez v1, :cond_0 */
	 /* .line 45 */
	 /* new-instance v1, Lcom/android/server/audio/MQSserver; */
	 /* invoke-direct {v1, p0}, Lcom/android/server/audio/MQSserver;-><init>(Landroid/content/Context;)V */
	 /* .line 46 */
	 v1 = com.android.server.audio.MQSserver.mMQSserver;
	 v1 = 	 (( com.android.server.audio.MQSserver ) v1 ).init ( ); // invoke-virtual {v1}, Lcom/android/server/audio/MQSserver;->init()Z
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 47 */
		 v1 = com.android.server.audio.MQSserver.mMQSserver;
		 (( com.android.server.audio.MQSserver ) v1 ).start ( ); // invoke-virtual {v1}, Lcom/android/server/audio/MQSserver;->start()V
		 /* .line 50 */
	 } // :cond_0
	 /* monitor-exit v0 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
	 /* .line 52 */
} // :cond_1
} // :goto_0
v0 = com.android.server.audio.MQSserver.mMQSserver;
} // .end method
private Boolean isAlowedEventReportInternationalRegion ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "event_name" # Ljava/lang/String; */
/* .line 300 */
final String v0 = "headphones"; // const-string v0, "headphones"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* const-string/jumbo v0, "voicecall_and_voip" */
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
private Boolean isReportXiaomiServer ( ) {
/* .locals 3 */
/* .line 294 */
final String v0 = "ro.miui.region"; // const-string v0, "ro.miui.region"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
/* .line 295 */
/* .local v0, "region":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "the region is :" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MQSserver"; // const-string v2, "MQSserver"
android.util.Slog .i ( v2,v1 );
/* .line 296 */
final String v1 = "CN"; // const-string v1, "CN"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_1 */
final String v1 = "RU"; // const-string v1, "RU"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :cond_1
} // :goto_0
int v1 = 1; // const/4 v1, 0x1
} // :goto_1
} // .end method
private void lambda$asynReportData$0 ( ) { //synthethic
/* .locals 0 */
/* .line 232 */
/* invoke-direct {p0}, Lcom/android/server/audio/MQSserver;->reportData()V */
/* .line 233 */
return;
} // .end method
private void reportData ( ) {
/* .locals 3 */
/* .line 221 */
v0 = this.mListLock;
/* monitor-enter v0 */
/* .line 222 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
try { // :try_start_0
v2 = this.list;
v2 = (( java.util.ArrayList ) v2 ).size ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->size()I
/* if-ge v1, v2, :cond_0 */
/* .line 223 */
v2 = this.list;
(( java.util.ArrayList ) v2 ).get ( v1 ); // invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/String; */
/* .line 224 */
/* .local v2, "str":Ljava/lang/String; */
(( com.android.server.audio.MQSserver ) p0 ).onetrack_report ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/audio/MQSserver;->onetrack_report(Ljava/lang/String;)Z
/* .line 222 */
/* nop */
} // .end local v2 # "str":Ljava/lang/String;
/* add-int/lit8 v1, v1, 0x1 */
/* .line 226 */
} // .end local v1 # "i":I
} // :cond_0
v1 = this.list;
(( java.util.ArrayList ) v1 ).clear ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V
/* .line 227 */
/* monitor-exit v0 */
/* .line 228 */
return;
/* .line 227 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
/* # virtual methods */
public Boolean addList ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "str" # Ljava/lang/String; */
/* .line 92 */
try { // :try_start_0
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* .line 93 */
/* .local v0, "data_json":Lorg/json/JSONObject; */
final String v1 = "1"; // const-string v1, "1"
/* .line 94 */
/* .local v1, "cn":Ljava/lang/String; */
final String v2 = "onetrack_count"; // const-string v2, "onetrack_count"
(( org.json.JSONObject ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 95 */
(( org.json.JSONObject ) v0 ).toString ( ); // invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
/* .line 96 */
/* .local v2, "newstr":Ljava/lang/String; */
final String v3 = "MQSserver"; // const-string v3, "MQSserver"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "add count to List : "; // const-string v5, "add count to List : "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v3,v4 );
/* .line 97 */
v3 = this.mListLock;
/* monitor-enter v3 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 98 */
try { // :try_start_1
v4 = this.list;
(( java.util.ArrayList ) v4 ).add ( v2 ); // invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 99 */
/* monitor-exit v3 */
/* .line 100 */
int v3 = 1; // const/4 v3, 0x1
/* .line 99 */
/* :catchall_0 */
/* move-exception v4 */
/* monitor-exit v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local p0 # "this":Lcom/android/server/audio/MQSserver;
} // .end local p1 # "str":Ljava/lang/String;
try { // :try_start_2
/* throw v4 */
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 101 */
} // .end local v0 # "data_json":Lorg/json/JSONObject;
} // .end local v1 # "cn":Ljava/lang/String;
} // .end local v2 # "newstr":Ljava/lang/String;
/* .restart local p0 # "this":Lcom/android/server/audio/MQSserver; */
/* .restart local p1 # "str":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v0 */
/* .line 102 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 103 */
final String v1 = "MQSserver"; // const-string v1, "MQSserver"
final String v2 = "erroe for addList"; // const-string v2, "erroe for addList"
android.util.Log .i ( v1,v2 );
/* .line 105 */
} // .end local v0 # "e":Ljava/lang/Exception;
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void asynReportData ( ) {
/* .locals 2 */
/* .line 231 */
/* new-instance v0, Ljava/lang/Thread; */
/* new-instance v1, Lcom/android/server/audio/MQSserver$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/android/server/audio/MQSserver$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/audio/MQSserver;)V */
/* invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V */
/* .line 233 */
(( java.lang.Thread ) v0 ).start ( ); // invoke-virtual {v0}, Ljava/lang/Thread;->start()V
/* .line 234 */
return;
} // .end method
public Boolean checkList ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 8 */
/* .param p1, "Name" # Ljava/lang/String; */
/* .param p2, "Audio_Event" # Ljava/lang/String; */
/* .line 70 */
v0 = this.mListLock;
/* monitor-enter v0 */
/* .line 72 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
int v2 = 0; // const/4 v2, 0x0
try { // :try_start_0
v3 = this.list;
v3 = (( java.util.ArrayList ) v3 ).size ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
/* if-ge v1, v3, :cond_1 */
/* .line 73 */
v3 = this.list;
(( java.util.ArrayList ) v3 ).get ( v1 ); // invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/String; */
/* .line 74 */
/* .local v3, "str":Ljava/lang/String; */
/* new-instance v4, Lorg/json/JSONObject; */
/* invoke-direct {v4, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* .line 75 */
/* .local v4, "data_json":Lorg/json/JSONObject; */
final String v5 = "name"; // const-string v5, "name"
(( org.json.JSONObject ) v4 ).optString ( v5 ); // invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 76 */
/* .local v5, "name":Ljava/lang/String; */
final String v6 = "audio_event"; // const-string v6, "audio_event"
(( org.json.JSONObject ) v4 ).optString ( v6 ); // invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 77 */
/* .local v6, "audio_event":Ljava/lang/String; */
v7 = (( java.lang.String ) v5 ).equals ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v7 != null) { // if-eqz v7, :cond_0
v2 = (( java.lang.String ) v6 ).equals ( p2 ); // invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 78 */
try { // :try_start_1
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 72 */
} // .end local v3 # "str":Ljava/lang/String;
} // .end local v4 # "data_json":Lorg/json/JSONObject;
} // .end local v5 # "name":Ljava/lang/String;
} // .end local v6 # "audio_event":Ljava/lang/String;
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 85 */
} // .end local v1 # "i":I
} // :cond_1
/* nop */
/* .line 86 */
/* monitor-exit v0 */
/* .line 87 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 81 */
/* :catch_0 */
/* move-exception v1 */
/* .line 82 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 83 */
final String v3 = "MQSserver"; // const-string v3, "MQSserver"
final String v4 = "erroe for checkList"; // const-string v4, "erroe for checkList"
android.util.Log .i ( v3,v4 );
/* .line 84 */
/* monitor-exit v0 */
/* .line 87 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean init ( ) {
/* .locals 7 */
/* .line 148 */
final String v0 = " "; // const-string v0, " "
final String v1 = "MQSserver"; // const-string v1, "MQSserver"
/* new-instance v2, Ljava/io/File; */
final String v3 = "/dev/xlog"; // const-string v3, "/dev/xlog"
/* invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 150 */
/* .local v2, "file":Ljava/io/File; */
int v4 = 0; // const/4 v4, 0x0
try { // :try_start_0
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "file exists "; // const-string v6, "file exists "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v5 );
/* .line 151 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "file can read "; // const-string v6, "file can read "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = (( java.io.File ) v2 ).canRead ( ); // invoke-virtual {v2}, Ljava/io/File;->canRead()Z
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v5 );
/* .line 152 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "file is file "; // const-string v6, "file is file "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = (( java.io.File ) v2 ).isFile ( ); // invoke-virtual {v2}, Ljava/io/File;->isFile()Z
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v5 );
/* .line 153 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "file is isDirectory "; // const-string v6, "file is isDirectory "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = (( java.io.File ) v2 ).isDirectory ( ); // invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v0 );
/* .line 154 */
v0 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( java.io.File ) v2 ).canRead ( ); // invoke-virtual {v2}, Ljava/io/File;->canRead()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 155 */
/* new-instance v0, Ljava/io/FileInputStream; */
/* invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
this.fis = v0;
/* .line 156 */
(( java.io.File ) v2 ).delete ( ); // invoke-virtual {v2}, Ljava/io/File;->delete()Z
/* .line 157 */
/* iput-boolean v4, p0, Lcom/android/server/audio/MQSserver;->mStopRequst:Z */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 158 */
int v0 = 1; // const/4 v0, 0x1
/* .line 163 */
} // :cond_0
/* .line 160 */
/* :catch_0 */
/* move-exception v0 */
/* .line 161 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 162 */
final String v3 = "erroe for opening /dev/xlog"; // const-string v3, "erroe for opening /dev/xlog"
android.util.Log .i ( v1,v3 );
/* .line 164 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
(( java.io.File ) v2 ).delete ( ); // invoke-virtual {v2}, Ljava/io/File;->delete()Z
/* .line 165 */
} // .end method
public Boolean needToReport ( ) {
/* .locals 7 */
/* .line 56 */
java.util.Calendar .getInstance ( );
/* .line 57 */
/* .local v0, "calendar":Ljava/util/Calendar; */
/* iget v1, p0, Lcom/android/server/audio/MQSserver;->year:I */
int v2 = 1; // const/4 v2, 0x1
v3 = (( java.util.Calendar ) v0 ).get ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I
int v4 = 0; // const/4 v4, 0x0
int v5 = 5; // const/4 v5, 0x5
int v6 = 2; // const/4 v6, 0x2
/* if-ne v1, v3, :cond_0 */
/* iget v1, p0, Lcom/android/server/audio/MQSserver;->month:I */
v3 = (( java.util.Calendar ) v0 ).get ( v6 ); // invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I
/* add-int/2addr v3, v2 */
/* if-ne v1, v3, :cond_0 */
/* iget v1, p0, Lcom/android/server/audio/MQSserver;->day:I */
v3 = (( java.util.Calendar ) v0 ).get ( v5 ); // invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I
/* if-ne v1, v3, :cond_0 */
/* .line 58 */
/* .line 59 */
} // :cond_0
v1 = (( java.util.Calendar ) v0 ).get ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I
/* iput v1, p0, Lcom/android/server/audio/MQSserver;->year:I */
/* .line 60 */
v1 = (( java.util.Calendar ) v0 ).get ( v6 ); // invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I
/* add-int/2addr v1, v2 */
/* iput v1, p0, Lcom/android/server/audio/MQSserver;->month:I */
/* .line 61 */
v1 = (( java.util.Calendar ) v0 ).get ( v5 ); // invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I
/* iput v1, p0, Lcom/android/server/audio/MQSserver;->day:I */
/* .line 62 */
/* const-string/jumbo v1, "sys.boot_completed" */
android.os.SystemProperties .get ( v1 );
final String v3 = "1"; // const-string v3, "1"
v1 = (( java.lang.String ) v3 ).equals ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 63 */
final String v1 = "MQSserver"; // const-string v1, "MQSserver"
final String v3 = "boot completed"; // const-string v3, "boot completed"
android.util.Log .d ( v1,v3 );
/* .line 64 */
/* .line 66 */
} // :cond_1
} // .end method
public Boolean onetrack_report ( java.lang.String p0 ) {
/* .locals 13 */
/* .param p1, "data" # Ljava/lang/String; */
/* .line 237 */
final String v0 = "onetrack_count"; // const-string v0, "onetrack_count"
final String v1 = "haptic_event"; // const-string v1, "haptic_event"
final String v2 = "name"; // const-string v2, "name"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "onetrack_report: "; // const-string v4, "onetrack_report: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "MQSserver"; // const-string v4, "MQSserver"
android.util.Log .d ( v4,v3 );
/* .line 239 */
int v3 = 0; // const/4 v3, 0x0
try { // :try_start_0
/* new-instance v5, Lorg/json/JSONObject; */
/* invoke-direct {v5, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* .line 240 */
/* .local v5, "data_json":Lorg/json/JSONObject; */
(( org.json.JSONObject ) v5 ).getString ( v2 ); // invoke-virtual {v5, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 241 */
/* .local v6, "event_name":Ljava/lang/String; */
v7 = /* invoke-direct {p0}, Lcom/android/server/audio/MQSserver;->isReportXiaomiServer()Z */
/* if-nez v7, :cond_0 */
v2 = (( org.json.JSONObject ) v5 ).has ( v2 ); // invoke-virtual {v5, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = /* invoke-direct {p0, v6}, Lcom/android/server/audio/MQSserver;->isAlowedEventReportInternationalRegion(Ljava/lang/String;)Z */
/* if-nez v2, :cond_0 */
/* .line 242 */
final String v0 = "This event is not reported in the international version"; // const-string v0, "This event is not reported in the international version"
android.util.Log .d ( v4,v0 );
/* .line 243 */
/* .line 245 */
} // :cond_0
/* new-instance v2, Landroid/content/Intent; */
final String v7 = "onetrack.action.TRACK_EVENT"; // const-string v7, "onetrack.action.TRACK_EVENT"
/* invoke-direct {v2, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 246 */
/* .local v2, "intent":Landroid/content/Intent; */
int v7 = 0; // const/4 v7, 0x0
/* .line 247 */
/* .local v7, "event_context":Ljava/lang/String; */
v8 = /* invoke-direct {p0}, Lcom/android/server/audio/MQSserver;->isReportXiaomiServer()Z */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
final String v9 = "PACKAGE"; // const-string v9, "PACKAGE"
final String v10 = "APP_ID"; // const-string v10, "APP_ID"
final String v11 = "audio_event"; // const-string v11, "audio_event"
if ( v8 != null) { // if-eqz v8, :cond_3
/* .line 248 */
try { // :try_start_1
v8 = (( org.json.JSONObject ) v5 ).has ( v11 ); // invoke-virtual {v5, v11}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v8 != null) { // if-eqz v8, :cond_1
/* .line 249 */
(( org.json.JSONObject ) v5 ).getString ( v11 ); // invoke-virtual {v5, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 250 */
} // .end local v7 # "event_context":Ljava/lang/String;
/* .local v1, "event_context":Ljava/lang/String; */
final String v7 = "31000000086"; // const-string v7, "31000000086"
(( android.content.Intent ) v2 ).putExtra ( v10, v7 ); // invoke-virtual {v2, v10, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 251 */
} // .end local v1 # "event_context":Ljava/lang/String;
/* .restart local v7 # "event_context":Ljava/lang/String; */
} // :cond_1
v8 = (( org.json.JSONObject ) v5 ).has ( v1 ); // invoke-virtual {v5, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v8 != null) { // if-eqz v8, :cond_2
/* .line 252 */
(( org.json.JSONObject ) v5 ).getString ( v1 ); // invoke-virtual {v5, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 253 */
} // .end local v7 # "event_context":Ljava/lang/String;
/* .restart local v1 # "event_context":Ljava/lang/String; */
final String v7 = "31000000089"; // const-string v7, "31000000089"
(( android.content.Intent ) v2 ).putExtra ( v10, v7 ); // invoke-virtual {v2, v10, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 255 */
} // .end local v1 # "event_context":Ljava/lang/String;
/* .restart local v7 # "event_context":Ljava/lang/String; */
} // :cond_2
/* .line 258 */
} // :cond_3
v1 = (( org.json.JSONObject ) v5 ).has ( v11 ); // invoke-virtual {v5, v11}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 259 */
(( org.json.JSONObject ) v5 ).getString ( v11 ); // invoke-virtual {v5, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 260 */
} // .end local v7 # "event_context":Ljava/lang/String;
/* .restart local v1 # "event_context":Ljava/lang/String; */
final String v7 = "31000000962"; // const-string v7, "31000000962"
(( android.content.Intent ) v2 ).putExtra ( v10, v7 ); // invoke-virtual {v2, v10, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 261 */
final String v7 = "com.mi.global.multimedia"; // const-string v7, "com.mi.global.multimedia"
(( android.content.Intent ) v2 ).putExtra ( v9, v7 ); // invoke-virtual {v2, v9, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 262 */
final String v7 = "PROJECT_ID"; // const-string v7, "PROJECT_ID"
final String v8 = "mi-multimedia-global"; // const-string v8, "mi-multimedia-global"
(( android.content.Intent ) v2 ).putExtra ( v7, v8 ); // invoke-virtual {v2, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 263 */
final String v7 = "TOPIC"; // const-string v7, "TOPIC"
final String v8 = "mqs_multimedia"; // const-string v8, "mqs_multimedia"
(( android.content.Intent ) v2 ).putExtra ( v7, v8 ); // invoke-virtual {v2, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 264 */
final String v7 = "PRIVATE_KEY_ID"; // const-string v7, "PRIVATE_KEY_ID"
final String v8 = "c5b2b941d0b2f19780459b7e48ffce62303edf28"; // const-string v8, "c5b2b941d0b2f19780459b7e48ffce62303edf28"
(( android.content.Intent ) v2 ).putExtra ( v7, v8 ); // invoke-virtual {v2, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 265 */
final String v7 = "This event is reported in the international version"; // const-string v7, "This event is reported in the international version"
android.util.Log .d ( v4,v7 );
/* .line 270 */
} // :goto_0
(( org.json.JSONObject ) v5 ).getString ( v0 ); // invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 271 */
/* .local v7, "count":Ljava/lang/String; */
final String v8 = "com.miui.analytics"; // const-string v8, "com.miui.analytics"
(( android.content.Intent ) v2 ).setPackage ( v8 ); // invoke-virtual {v2, v8}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 272 */
final String v8 = "EVENT_NAME"; // const-string v8, "EVENT_NAME"
(( android.content.Intent ) v2 ).putExtra ( v8, v6 ); // invoke-virtual {v2, v8, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 273 */
final String v8 = "android"; // const-string v8, "android"
(( android.content.Intent ) v2 ).putExtra ( v9, v8 ); // invoke-virtual {v2, v9, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 274 */
(( android.content.Intent ) v2 ).putExtra ( v0, v7 ); // invoke-virtual {v2, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 275 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* .line 276 */
/* .local v0, "context_json":Lorg/json/JSONObject; */
int v8 = 0; // const/4 v8, 0x0
/* .local v8, "i":I */
} // :goto_1
(( org.json.JSONObject ) v0 ).names ( ); // invoke-virtual {v0}, Lorg/json/JSONObject;->names()Lorg/json/JSONArray;
v9 = (( org.json.JSONArray ) v9 ).length ( ); // invoke-virtual {v9}, Lorg/json/JSONArray;->length()I
/* if-ge v8, v9, :cond_4 */
/* .line 277 */
(( org.json.JSONObject ) v0 ).names ( ); // invoke-virtual {v0}, Lorg/json/JSONObject;->names()Lorg/json/JSONArray;
(( org.json.JSONArray ) v9 ).getString ( v8 ); // invoke-virtual {v9, v8}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
/* .line 278 */
/* .local v9, "key":Ljava/lang/String; */
(( org.json.JSONObject ) v0 ).getString ( v9 ); // invoke-virtual {v0, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 279 */
/* .local v10, "value":Ljava/lang/String; */
(( android.content.Intent ) v2 ).putExtra ( v9, v10 ); // invoke-virtual {v2, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 280 */
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
final String v12 = "key: "; // const-string v12, "key: "
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v9 ); // invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v12 = " ,value: "; // const-string v12, " ,value: "
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v10 ); // invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v4,v11 );
/* .line 276 */
/* nop */
} // .end local v9 # "key":Ljava/lang/String;
} // .end local v10 # "value":Ljava/lang/String;
/* add-int/lit8 v8, v8, 0x1 */
/* .line 282 */
} // .end local v8 # "i":I
} // :cond_4
int v8 = 2; // const/4 v8, 0x2
(( android.content.Intent ) v2 ).setFlags ( v8 ); // invoke-virtual {v2, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 283 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v9, "startService: " */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.content.Intent ) v2 ).toString ( ); // invoke-virtual {v2}, Landroid/content/Intent;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v4,v8 );
/* .line 284 */
v8 = this.mContext;
(( android.content.Context ) v8 ).startService ( v2 ); // invoke-virtual {v8, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 285 */
int v3 = 1; // const/4 v3, 0x1
/* .line 267 */
} // .end local v0 # "context_json":Lorg/json/JSONObject;
} // .end local v1 # "event_context":Ljava/lang/String;
/* .local v7, "event_context":Ljava/lang/String; */
} // :cond_5
/* .line 286 */
} // .end local v2 # "intent":Landroid/content/Intent;
} // .end local v5 # "data_json":Lorg/json/JSONObject;
} // .end local v6 # "event_name":Ljava/lang/String;
} // .end local v7 # "event_context":Ljava/lang/String;
/* :catch_0 */
/* move-exception v0 */
/* .line 287 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 288 */
final String v1 = "erroe for reportData"; // const-string v1, "erroe for reportData"
android.util.Log .i ( v4,v1 );
/* .line 290 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // .end method
public void run ( ) {
/* .locals 10 */
/* .line 170 */
final String v0 = "MQSserver"; // const-string v0, "MQSserver"
/* const/16 v1, -0x13 */
android.os.Process .setThreadPriority ( v1 );
/* .line 172 */
/* const/16 v1, 0x200 */
try { // :try_start_0
/* new-array v2, v1, [B */
/* .line 173 */
/* .local v2, "buf":[B */
int v3 = 0; // const/4 v3, 0x0
/* .line 174 */
/* .local v3, "str":Ljava/lang/String; */
} // :goto_0
/* iget-boolean v4, p0, Lcom/android/server/audio/MQSserver;->mStopRequst:Z */
/* if-nez v4, :cond_3 */
/* .line 175 */
v4 = this.fis;
v4 = (( java.io.FileInputStream ) v4 ).read ( v2 ); // invoke-virtual {v4, v2}, Ljava/io/FileInputStream;->read([B)I
/* .line 176 */
/* .local v4, "len":I */
/* new-instance v5, Ljava/lang/String; */
int v6 = 0; // const/4 v6, 0x0
/* invoke-direct {v5, v2, v6, v4}, Ljava/lang/String;-><init>([BII)V */
/* move-object v3, v5 */
/* .line 177 */
/* nop */
/* .line 181 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "getData len: "; // const-string v6, "getData len: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = " content: "; // const-string v6, " content: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v0,v5 );
/* .line 182 */
/* new-instance v5, Lorg/json/JSONObject; */
/* invoke-direct {v5, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* .line 183 */
/* .local v5, "data_json":Lorg/json/JSONObject; */
final String v6 = "name"; // const-string v6, "name"
(( org.json.JSONObject ) v5 ).optString ( v6 ); // invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 184 */
/* .local v6, "name":Ljava/lang/String; */
final String v7 = "audio_event"; // const-string v7, "audio_event"
(( org.json.JSONObject ) v5 ).optString ( v7 ); // invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 185 */
/* .local v7, "audio_event":Ljava/lang/String; */
/* if-le v4, v1, :cond_0 */
/* .line 186 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v9, "the length is out of range: " */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v9 = (( java.lang.String ) v3 ).length ( ); // invoke-virtual {v3}, Ljava/lang/String;->length()I
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v8 );
/* .line 188 */
} // :cond_0
v8 = (( com.android.server.audio.MQSserver ) p0 ).checkList ( v6, v7 ); // invoke-virtual {p0, v6, v7}, Lcom/android/server/audio/MQSserver;->checkList(Ljava/lang/String;Ljava/lang/String;)Z
if ( v8 != null) { // if-eqz v8, :cond_1
/* .line 189 */
(( com.android.server.audio.MQSserver ) p0 ).updateCount ( v6, v7 ); // invoke-virtual {p0, v6, v7}, Lcom/android/server/audio/MQSserver;->updateCount(Ljava/lang/String;Ljava/lang/String;)Z
/* .line 190 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v9, "updateCount: name: " */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = "/audio_event : "; // const-string v9, "/audio_event : "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v8 );
/* .line 192 */
} // :cond_1
(( com.android.server.audio.MQSserver ) p0 ).addList ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/audio/MQSserver;->addList(Ljava/lang/String;)Z
/* .line 195 */
} // :goto_1
v8 = (( com.android.server.audio.MQSserver ) p0 ).needToReport ( ); // invoke-virtual {p0}, Lcom/android/server/audio/MQSserver;->needToReport()Z
if ( v8 != null) { // if-eqz v8, :cond_2
/* .line 196 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "needToReport year: "; // const-string v9, "needToReport year: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v9, p0, Lcom/android/server/audio/MQSserver;->year:I */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = "month: "; // const-string v9, "month: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v9, p0, Lcom/android/server/audio/MQSserver;->month:I */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = "day: "; // const-string v9, "day: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v9, p0, Lcom/android/server/audio/MQSserver;->day:I */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v8 );
/* .line 197 */
/* invoke-direct {p0}, Lcom/android/server/audio/MQSserver;->reportData()V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 199 */
} // .end local v4 # "len":I
} // .end local v5 # "data_json":Lorg/json/JSONObject;
} // .end local v6 # "name":Ljava/lang/String;
} // .end local v7 # "audio_event":Ljava/lang/String;
} // :cond_2
/* goto/16 :goto_0 */
/* .line 203 */
} // .end local v2 # "buf":[B
} // .end local v3 # "str":Ljava/lang/String;
} // :cond_3
/* .line 200 */
/* :catch_0 */
/* move-exception v1 */
/* .line 201 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 202 */
final String v2 = "erroe for reading /dev/xlog"; // const-string v2, "erroe for reading /dev/xlog"
android.util.Log .e ( v0,v2 );
/* .line 205 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_2
try { // :try_start_1
v1 = this.fis;
(( java.io.FileInputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 209 */
/* .line 206 */
/* :catch_1 */
/* move-exception v1 */
/* .line 207 */
/* .restart local v1 # "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 208 */
final String v2 = "erroe for close /dev/xlog"; // const-string v2, "erroe for close /dev/xlog"
android.util.Log .e ( v0,v2 );
/* .line 210 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_3
return;
} // .end method
public void setStop ( ) {
/* .locals 2 */
/* .line 213 */
final String v0 = "MQSserver"; // const-string v0, "MQSserver"
/* const-string/jumbo v1, "setStop request" */
android.util.Log .d ( v0,v1 );
/* .line 214 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/audio/MQSserver;->mStopRequst:Z */
/* .line 215 */
v0 = this.list;
v0 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* if-lez v0, :cond_0 */
/* .line 216 */
/* invoke-direct {p0}, Lcom/android/server/audio/MQSserver;->reportData()V */
/* .line 218 */
} // :cond_0
return;
} // .end method
public Boolean updateCount ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 12 */
/* .param p1, "Name" # Ljava/lang/String; */
/* .param p2, "Audio_Event" # Ljava/lang/String; */
/* .line 109 */
int v0 = 0; // const/4 v0, 0x0
/* .line 110 */
/* .local v0, "str":Ljava/lang/String; */
v1 = this.mListLock;
/* monitor-enter v1 */
/* .line 111 */
try { // :try_start_0
final String v2 = "MQSserver"; // const-string v2, "MQSserver"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "enter updateCount"; // const-string v4, "enter updateCount"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v3 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 113 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
try { // :try_start_1
v3 = this.list;
v3 = (( java.util.ArrayList ) v3 ).size ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
/* if-ge v2, v3, :cond_2 */
/* .line 114 */
v3 = this.list;
(( java.util.ArrayList ) v3 ).get ( v2 ); // invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/String; */
/* move-object v0, v3 */
/* .line 115 */
/* new-instance v3, Lorg/json/JSONObject; */
/* invoke-direct {v3, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* .line 116 */
/* .local v3, "data_json":Lorg/json/JSONObject; */
final String v4 = "name"; // const-string v4, "name"
(( org.json.JSONObject ) v3 ).optString ( v4 ); // invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 117 */
/* .local v4, "name":Ljava/lang/String; */
final String v5 = "audio_event"; // const-string v5, "audio_event"
(( org.json.JSONObject ) v3 ).optString ( v5 ); // invoke-virtual {v3, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 118 */
/* .local v5, "audio_event":Ljava/lang/String; */
v6 = (( java.lang.String ) v4 ).equals ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_1
v6 = (( java.lang.String ) v5 ).equals ( p2 ); // invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 119 */
v6 = this.list;
v6 = (( java.util.ArrayList ) v6 ).indexOf ( v0 ); // invoke-virtual {v6, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I
/* .line 120 */
/* .local v6, "index":I */
v7 = this.list;
(( java.util.ArrayList ) v7 ).remove ( v0 ); // invoke-virtual {v7, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
/* .line 121 */
final String v7 = "onetrack_count"; // const-string v7, "onetrack_count"
final String v8 = "empty"; // const-string v8, "empty"
(( org.json.JSONObject ) v3 ).optString ( v7, v8 ); // invoke-virtual {v3, v7, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
/* .line 122 */
/* .local v7, "cn":Ljava/lang/String; */
final String v8 = "MQSserver"; // const-string v8, "MQSserver"
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "read count frome file: "; // const-string v10, "read count frome file: "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v7 ); // invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v8,v9 );
/* .line 123 */
final String v8 = "empty"; // const-string v8, "empty"
v8 = (( java.lang.String ) v7 ).equals ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v9 = 1; // const/4 v9, 0x1
if ( v8 != null) { // if-eqz v8, :cond_0
/* .line 124 */
final String v8 = "1"; // const-string v8, "1"
/* move-object v7, v8 */
/* .line 126 */
} // :cond_0
v8 = java.lang.Integer .parseInt ( v7 );
/* .line 127 */
/* .local v8, "count":I */
/* add-int/2addr v8, v9 */
/* .line 128 */
java.lang.String .valueOf ( v8 );
/* move-object v7, v10 */
/* .line 130 */
} // .end local v8 # "count":I
} // :goto_1
final String v8 = "onetrack_count"; // const-string v8, "onetrack_count"
(( org.json.JSONObject ) v3 ).put ( v8, v7 ); // invoke-virtual {v3, v8, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 131 */
(( org.json.JSONObject ) v3 ).toString ( ); // invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
/* move-object v0, v8 */
/* .line 132 */
v8 = this.list;
(( java.util.ArrayList ) v8 ).add ( v6, v0 ); // invoke-virtual {v8, v6, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
/* .line 133 */
final String v8 = "MQSserver"; // const-string v8, "MQSserver"
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v11, "update RBI json: " */
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v0 ); // invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v8,v10 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 135 */
try { // :try_start_2
/* monitor-exit v1 */
/* .line 113 */
} // .end local v3 # "data_json":Lorg/json/JSONObject;
} // .end local v4 # "name":Ljava/lang/String;
} // .end local v5 # "audio_event":Ljava/lang/String;
} // .end local v6 # "index":I
} // .end local v7 # "cn":Ljava/lang/String;
} // :cond_1
/* add-int/lit8 v2, v2, 0x1 */
/* goto/16 :goto_0 */
/* .line 141 */
} // .end local v2 # "i":I
} // :cond_2
/* .line 138 */
/* :catch_0 */
/* move-exception v2 */
/* .line 139 */
/* .local v2, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* .line 140 */
final String v3 = "MQSserver"; // const-string v3, "MQSserver"
final String v4 = "erroe for updateCount"; // const-string v4, "erroe for updateCount"
android.util.Log .i ( v3,v4 );
/* .line 143 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_2
/* monitor-exit v1 */
int v1 = 0; // const/4 v1, 0x0
/* .line 144 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v2 */
} // .end method
