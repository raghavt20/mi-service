class com.android.server.audio.CloudServiceSettings$SettingsProviderSettings extends com.android.server.audio.CloudServiceSettings$Setting {
	 /* .source "CloudServiceSettings.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/audio/CloudServiceSettings; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "SettingsProviderSettings" */
} // .end annotation
/* # static fields */
private static final java.lang.String KEY;
private static final java.lang.String SCOPE;
private static final java.lang.String VALUE;
/* # instance fields */
private java.lang.StringBuilder mKey;
private java.lang.StringBuilder mScope;
private java.lang.StringBuilder mValue;
final com.android.server.audio.CloudServiceSettings this$0; //synthetic
/* # direct methods */
 com.android.server.audio.CloudServiceSettings$SettingsProviderSettings ( ) {
/* .locals 3 */
/* .param p1, "this$0" # Lcom/android/server/audio/CloudServiceSettings; */
/* .param p2, "json" # Lorg/json/JSONObject; */
/* .line 640 */
this.this$0 = p1;
/* .line 641 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/audio/CloudServiceSettings$Setting;-><init>(Lcom/android/server/audio/CloudServiceSettings;Lorg/json/JSONObject;)V */
/* .line 643 */
try { // :try_start_0
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 final String v1 = "key"; // const-string v1, "key"
	 (( org.json.JSONObject ) p2 ).getString ( v1 ); // invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
	 /* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
	 this.mKey = v0;
	 /* .line 644 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* const-string/jumbo v1, "value" */
	 (( org.json.JSONObject ) p2 ).getString ( v1 ); // invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
	 /* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
	 this.mValue = v0;
	 /* .line 645 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 final String v1 = "scope"; // const-string v1, "scope"
	 (( org.json.JSONObject ) p2 ).getString ( v1 ); // invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
	 /* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
	 this.mScope = v0;
	 /* .line 646 */
	 /* iget-boolean v0, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mSecreted:Z */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 647 */
		 v0 = this.mSecretedItem;
		 v1 = this.mKey;
		 (( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
		 /* .line 648 */
		 v0 = this.mSecretedItem;
		 v1 = this.mValue;
		 (( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
		 /* .line 649 */
		 v0 = this.mSecretedItem;
		 v1 = this.mScope;
		 (( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 653 */
	 } // :cond_0
	 /* .line 651 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 652 */
	 /* .local v0, "e":Ljava/lang/Exception; */
	 final String v1 = "CloudServiceSettings"; // const-string v1, "CloudServiceSettings"
	 final String v2 = "fail to parse FileDownloadSetting"; // const-string v2, "fail to parse FileDownloadSetting"
	 android.util.Log .e ( v1,v2 );
	 /* .line 654 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
/* # virtual methods */
public Boolean set ( ) {
/* .locals 4 */
/* .line 658 */
v0 = /* invoke-super {p0}, Lcom/android/server/audio/CloudServiceSettings$Setting;->set()Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 659 */
/* .line 661 */
} // :cond_0
v0 = this.this$0;
com.android.server.audio.CloudServiceSettings .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 662 */
/* .local v0, "cr":Landroid/content/ContentResolver; */
v2 = this.mScope;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v3 = (( java.lang.String ) v2 ).hashCode ( ); // invoke-virtual {v2}, Ljava/lang/String;->hashCode()I
/* sparse-switch v3, :sswitch_data_0 */
} // :cond_1
/* :sswitch_0 */
/* const-string/jumbo v1, "system" */
v1 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
int v1 = 1; // const/4 v1, 0x1
/* :sswitch_1 */
/* const-string/jumbo v1, "secure" */
v1 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
int v1 = 2; // const/4 v1, 0x2
/* :sswitch_2 */
final String v3 = "global"; // const-string v3, "global"
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
} // :goto_0
int v1 = -1; // const/4 v1, -0x1
} // :goto_1
/* packed-switch v1, :pswitch_data_0 */
/* .line 673 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "fail to set: "; // const-string v2, "fail to set: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mSettingName;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " error scope!"; // const-string v2, " error scope!"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "CloudServiceSettings"; // const-string v2, "CloudServiceSettings"
android.util.Log .e ( v2,v1 );
/* .line 670 */
/* :pswitch_0 */
v1 = this.mKey;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v2 = this.mValue;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v1 = android.provider.Settings$Secure .putString ( v0,v1,v2 );
/* iput-boolean v1, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mSuccess:Z */
/* .line 671 */
/* .line 667 */
/* :pswitch_1 */
v1 = this.mKey;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v2 = this.mValue;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v1 = android.provider.Settings$System .putString ( v0,v1,v2 );
/* iput-boolean v1, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mSuccess:Z */
/* .line 668 */
/* .line 664 */
/* :pswitch_2 */
v1 = this.mKey;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v2 = this.mValue;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v1 = android.provider.Settings$Global .putString ( v0,v1,v2 );
/* iput-boolean v1, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mSuccess:Z */
/* .line 665 */
/* nop */
/* .line 675 */
} // :goto_2
/* iget-boolean v1, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mSuccess:Z */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x4a16fc5d -> :sswitch_2 */
/* -0x3604a489 -> :sswitch_1 */
/* -0x34e38dd1 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public Boolean updateTo ( com.android.server.audio.CloudServiceSettings$Setting p0 ) {
/* .locals 2 */
/* .param p1, "s" # Lcom/android/server/audio/CloudServiceSettings$Setting; */
/* .line 680 */
v0 = /* invoke-super {p0, p1}, Lcom/android/server/audio/CloudServiceSettings$Setting;->updateTo(Lcom/android/server/audio/CloudServiceSettings$Setting;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* instance-of v0, p1, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 681 */
/* move-object v0, p1 */
/* check-cast v0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings; */
/* .line 682 */
/* .local v0, "ss":Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings; */
v1 = this.mKey;
this.mKey = v1;
/* .line 683 */
v1 = this.mValue;
this.mValue = v1;
/* .line 684 */
v1 = this.mScope;
this.mScope = v1;
/* .line 685 */
int v1 = 1; // const/4 v1, 0x1
/* .line 687 */
} // .end local v0 # "ss":Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "fail to update SettingsProviderSettings: "; // const-string v1, "fail to update SettingsProviderSettings: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mSettingName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "CloudServiceSettings"; // const-string v1, "CloudServiceSettings"
android.util.Log .e ( v1,v0 );
/* .line 688 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
