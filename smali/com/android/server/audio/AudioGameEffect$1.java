class com.android.server.audio.AudioGameEffect$1 extends miui.process.IForegroundInfoListener$Stub {
	 /* .source "AudioGameEffect.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/audio/AudioGameEffect; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.audio.AudioGameEffect this$0; //synthetic
/* # direct methods */
 com.android.server.audio.AudioGameEffect$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/audio/AudioGameEffect; */
/* .line 94 */
this.this$0 = p1;
/* invoke-direct {p0}, Lmiui/process/IForegroundInfoListener$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onForegroundInfoChanged ( miui.process.ForegroundInfo p0 ) {
/* .locals 5 */
/* .param p1, "foregroundInfo" # Lmiui/process/ForegroundInfo; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 97 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "foreground change to "; // const-string v1, "foreground change to "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mForegroundPackageName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", last foreground is "; // const-string v1, ", last foreground is "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mLastForegroundPackageName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AudioGameEffect"; // const-string v1, "AudioGameEffect"
android.util.Log .i ( v1,v0 );
/* .line 98 */
v0 = this.this$0;
v2 = this.mLastForegroundPackageName;
v0 = com.android.server.audio.AudioGameEffect .-$$Nest$misPackageEnabled ( v0,v2 );
/* const-wide/16 v2, 0x1f4 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 99 */
v0 = this.this$0;
final String v4 = ""; // const-string v4, ""
com.android.server.audio.AudioGameEffect .-$$Nest$fputmCurrentEnablePkg ( v0,v4 );
/* .line 100 */
v0 = this.this$0;
v0 = com.android.server.audio.AudioGameEffect .-$$Nest$fgetisEffectOn ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 101 */
	 v0 = this.this$0;
	 int v4 = 3; // const/4 v4, 0x3
	 com.android.server.audio.AudioGameEffect .-$$Nest$msendMsgDelay ( v0,v4,v2,v3 );
	 /* .line 104 */
} // :cond_0
v0 = this.this$0;
v4 = this.mForegroundPackageName;
v0 = com.android.server.audio.AudioGameEffect .-$$Nest$misPackageEnabled ( v0,v4 );
if ( v0 != null) { // if-eqz v0, :cond_3
	 /* .line 105 */
	 v0 = this.this$0;
	 v4 = this.mForegroundPackageName;
	 com.android.server.audio.AudioGameEffect .-$$Nest$fputmCurrentEnablePkg ( v0,v4 );
	 /* .line 106 */
	 v0 = this.this$0;
	 v0 = 	 com.android.server.audio.AudioGameEffect .-$$Nest$misCurrentDeviceSupported ( v0 );
	 /* if-nez v0, :cond_1 */
	 /* .line 107 */
	 final String v0 = "current device not support"; // const-string v0, "current device not support"
	 android.util.Log .i ( v1,v0 );
	 /* .line 108 */
	 return;
	 /* .line 110 */
} // :cond_1
v0 = this.this$0;
v0 = com.android.server.audio.AudioGameEffect .-$$Nest$misSpatialAudioEnabled ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
	 /* .line 111 */
	 /* const-string/jumbo v0, "spatial audio enabled, return" */
	 android.util.Log .i ( v1,v0 );
	 /* .line 112 */
	 return;
	 /* .line 114 */
} // :cond_2
v0 = this.this$0;
int v1 = 2; // const/4 v1, 0x2
com.android.server.audio.AudioGameEffect .-$$Nest$msendMsgDelay ( v0,v1,v2,v3 );
/* .line 116 */
} // :cond_3
return;
} // .end method
