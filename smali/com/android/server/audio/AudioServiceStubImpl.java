public class com.android.server.audio.AudioServiceStubImpl extends com.android.server.audio.AudioServiceStub {
	 /* .source "AudioServiceStubImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.audio.AudioServiceStub$$" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/android/server/audio/AudioServiceStubImpl$H;, */
/* Lcom/android/server/audio/AudioServiceStubImpl$AudioPowerSaveEnable;, */
/* Lcom/android/server/audio/AudioServiceStubImpl$EffectChangeBroadcastReceiver; */
/* } */
} // .end annotation
/* # static fields */
private static final java.lang.String ABNORMAL_AUDIO_INFO_EVENT_NAME;
public static final java.lang.String ACTION_VOLUME_BOOST;
private static final java.lang.String AUDIO_ADSP_SPATIALIZER_AVAILABLE;
private static final java.lang.String AUDIO_ADSP_SPATIALIZER_ENABLE;
private static final Integer AUDIO_BT_CONFIG_PROP;
private static final java.lang.String AUDIO_CAMERA_BT_RECORD_SUPPORT;
private static final java.lang.String AUDIO_CINEMA_MODE_SUPPORT;
private static final java.lang.String AUDIO_DOLBY_CONTROL_SUPPORT;
private static final java.lang.String AUDIO_ONETRACK_SILENT_OBSERVER;
private static final java.lang.String AUDIO_PARAMETER_DEFAULT;
private static final java.lang.String AUDIO_PARAMETER_HA;
private static final java.lang.String AUDIO_PARAMETER_KARAOKE_OFF;
private static final java.lang.String AUDIO_PARAMETER_KARAOKE_ON;
private static final java.lang.String AUDIO_PARAMETER_KTV_OFF;
private static final java.lang.String AUDIO_PARAMETER_KTV_ON;
private static final java.lang.String AUDIO_PARAMETER_WT;
private static final java.lang.String AUDIO_POWER_SAVE_SETTING;
private static final java.lang.String AUDIO_SILENT_LEVEL;
private static final java.lang.String AUDIO_SILENT_LOCATION;
private static final java.lang.String AUDIO_SILENT_REASON;
private static final java.lang.String AUDIO_SILENT_TYPE;
private static final java.lang.String CAMERA_AUDIO_HEADSET_STATE;
private static final java.lang.String CAMERA_PACKAGE_NAME;
private static final java.lang.String DEFAULT_AUDIO_PARAMETERS;
private static final Integer DELAY_NOTIFY_BT_MAX_NUM;
private static final Integer DELAY_NOTIFY_BT_STOPBLUETOOTHSCO_MS;
public static final java.lang.String EXTRA_BOOST_STATE;
private static final Integer FLAG_REALLY_TIME_MODE_ENABLED;
private static final Integer FLAG_SOUND_LEAK_PROTECTION_ENABLED;
private static final java.lang.String HEADPHONES_EVENT_NAME;
static java.util.List HIFI_NOT_SUPPORT_DEVICE_LIST;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.String KEY_PERSIST_CUMULATIVE_PLAYBACK_MS;
private static final java.lang.String KEY_PERSIST_NOTIFICATION_DATE;
private static final java.lang.String KEY_PERSIST_PLAYBACK_CONTINUOUS_MS;
private static final java.lang.String KEY_PERSIST_PLAYBACK_HIGH_VOICE;
private static final java.lang.String KEY_PERSIST_PLAYBACK_START_MS;
public static final Integer LONG_TIME_PROMPT;
public static final Integer MAX_VOLUME_LONG_TIME;
private static final Integer MAX_VOLUME_VALID_TIME_INTERVAL;
private static final Integer MIUI_MAX_MUSIC_VOLUME_STEP;
private static final Integer MQSSERVER_REPORT_RATE_MS;
private static final Integer MSG_DATA_TRACK;
private static final Integer MSG_MQSSERVER_REPORT;
private static final Integer MSG_REPORT_ABNORMAL_AUDIO_STATE;
private static final Integer MSG_REPORT_AUDIO_SILENT_OBSERVER;
private static final Integer MSG_REPORT_MULTI_VOIP_DAILY_USE_FOR_BT_CONNECT;
private static final Integer MSG_REPORT_WAVEFORM_INFO_TRACK;
private static final Integer MSG_UPDATE_AUDIO_MODE;
private static final Integer MSG_UPDATE_MEDIA_STATE;
private static final Integer MSG_UPDATE_MEETING_MODE;
private static final Integer MUSIC_ACTIVE_CONTINUOUS_MS_MAX;
public static final Integer MUSIC_ACTIVE_CONTINUOUS_POLL_PERIOD_MS;
private static final Integer MUSIC_ACTIVE_DATA_REPORT_INTERVAL;
private static final Integer MUSIC_ACTIVE_RETRY_POLL_PERIOD_MS;
private static final Integer NOTE_USB_HEADSET_PLUG;
public static final Integer READ_MUSIC_PLAY_BACK_DELAY;
private static final Integer REPORT_AUDIO_EXCEPTION_INFO_INTERVAL_TIME;
private static final Integer SCO_DEVICES_MAX_NUM;
private static final java.lang.String STATE_AUDIO_SCO_CONNECTED;
private static final java.lang.String STATE_AUDIO_SCO_DISCONNECTED;
private static final java.lang.String TAG;
private static final java.lang.String TRANSMIT_AUDIO_PARAMETERS;
private static final java.lang.String WAVEFORM_EVENT_NAME;
public static Boolean mIsAudioPlaybackTriggerSupported;
/* # instance fields */
private final Boolean REALLY_TIME_MODE_ENABLED;
private final Boolean SOUND_LEAK_PROTECTION_ENABLED;
private Integer cameraToastServiceRiid;
private Boolean isSupportPollAudioMicStatus;
private Integer mA2dpDeviceConnectedState;
private com.android.server.audio.AudioGameEffect mAudioGameEffect;
private android.media.AudioManager mAudioManager;
private Integer mAudioMode;
private final java.util.List mAudioParameterClientList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/android/server/audio/AudioParameterClient;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.audio.AudioPowerSaveModeObserver mAudioPowerSaveModeObserver;
private com.android.server.audio.AudioQueryWeatherService mAudioQueryWeatherService;
private com.android.server.audio.AudioService mAudioService;
private com.android.server.audio.AudioThermalObserver mAudioThermalObserver;
private com.android.server.audio.BtHelper mBtHelper;
private java.lang.String mBtName;
private com.android.server.audio.CloudServiceThread mCloudService;
private java.util.List mCommunicationRouteClients;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.content.Context mContext;
private Long mContinuousMs;
public Long mCumulativePlaybackEndTime;
public Long mCumulativePlaybackStartTime;
private Integer mDelayNotifyBtStopScoCount;
private com.android.server.audio.AudioDeviceBroker mDeviceBroker;
private com.android.server.audio.AudioServiceStubImpl$EffectChangeBroadcastReceiver mEffectChangeBroadcastReceiver;
private com.android.server.audio.feature.FeatureAdapter mFeatureAdapter;
private com.android.server.audio.foldable.FoldableAdapter mFoldableAdapter;
private com.android.server.audio.GameAudioEnhancer mGameAudioEnhancer;
private android.os.Handler mHandler;
private Integer mHeadsetDeviceConnectedState;
private Integer mHighLevel;
private Boolean mInDelayNotifyBtStopSco;
private final Boolean mIsAdspSpatializerAvailable;
private final Boolean mIsSupportCinemaMode;
private final Boolean mIsSupportFWAudioEffectCenter;
private final Boolean mIsSupportedCameraRecord;
private final Boolean mIsSupportedDolbyEffectControl;
private Boolean mIsSupportedReportAudioRouteState;
private com.android.server.audio.MiAudioServiceMTK mMiAudioServiceMTK;
private android.media.MiuiXlog mMiuiXlog;
private Integer mModeOwnerPid;
private com.android.server.audio.MQSUtils mMqsUtils;
private java.lang.String mMultiVoipPackages;
private Long mMusicPlaybackContinuousMs;
private Long mMusicPlaybackContinuousMsTotal;
private Float mMusicVolumeDb;
private Integer mMusicVolumeIndex;
private android.app.NotificationManager mNm;
private java.time.LocalDate mNotificationDate;
private Integer mNotificationTimes;
private com.android.server.audio.pad.PadAdapter mPadAdapter;
private com.android.server.audio.AudioParameterClient$ClientDeathListener mParameterClientDeathListener;
public Long mPlaybackEndTime;
public Long mPlaybackStartTime;
private android.media.AudioDeviceAttributes mPreferredCommunicationDevice;
private mPrescaleAbsoluteVolume;
Integer mReceiveNotificationDevice;
private java.lang.String mRegisterContentName;
private java.lang.String mRequesterPackageForAudioMode;
private Integer mScoBtState;
private final java.util.LinkedList mSetScoCommunicationDevice;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/LinkedList<", */
/* "Lcom/android/server/audio/AudioDeviceBroker$CommunicationDeviceInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final Integer mSettingAudioPowerSave;
private Long mStartMs;
private Boolean mSuperVoiceVolumeOn;
private Boolean mSuperVoiceVolumeSupported;
private Boolean mSuperVolumeOn;
private final Integer mVolumeAttenuation;
private Boolean mVolumeBoostEnabled;
private android.os.HandlerThread mWorkerThread;
public Boolean markBluetoothheadsetstub;
private Long sLastAudioStateExpTime;
private Integer sLastReportAudioErrorType;
/* # direct methods */
public static void $r8$lambda$5sdfGQc0TAbQHmbJYBYrXtxEdDE ( com.android.server.audio.AudioServiceStubImpl p0, android.os.IBinder p1, java.lang.String p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/audio/AudioServiceStubImpl;->lambda$addAudioParameterClient$2(Landroid/os/IBinder;Ljava/lang/String;)V */
return;
} // .end method
static com.android.server.audio.AudioService -$$Nest$fgetmAudioService ( com.android.server.audio.AudioServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mAudioService;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.audio.AudioServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.android.server.audio.AudioServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static com.android.server.audio.MQSUtils -$$Nest$fgetmMqsUtils ( com.android.server.audio.AudioServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMqsUtils;
} // .end method
static java.lang.String -$$Nest$fgetmMultiVoipPackages ( com.android.server.audio.AudioServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMultiVoipPackages;
} // .end method
static android.app.NotificationManager -$$Nest$fgetmNm ( com.android.server.audio.AudioServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mNm;
} // .end method
static void -$$Nest$fputmMultiVoipPackages ( com.android.server.audio.AudioServiceStubImpl p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mMultiVoipPackages = p1;
return;
} // .end method
static void -$$Nest$mcreateNotification ( com.android.server.audio.AudioServiceStubImpl p0, android.content.Context p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/audio/AudioServiceStubImpl;->createNotification(Landroid/content/Context;)V */
return;
} // .end method
static java.lang.String -$$Nest$mgetDeviceToStringForStreamMusic ( com.android.server.audio.AudioServiceStubImpl p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/audio/AudioServiceStubImpl;->getDeviceToStringForStreamMusic(I)Ljava/lang/String; */
} // .end method
static void -$$Nest$mhandleAudioModeUpdate ( com.android.server.audio.AudioServiceStubImpl p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/audio/AudioServiceStubImpl;->handleAudioModeUpdate(I)V */
return;
} // .end method
static void -$$Nest$mhandleMediaStateUpdate ( com.android.server.audio.AudioServiceStubImpl p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/audio/AudioServiceStubImpl;->handleMediaStateUpdate(Z)V */
return;
} // .end method
static void -$$Nest$mhandleMeetingModeUpdate ( com.android.server.audio.AudioServiceStubImpl p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/audio/AudioServiceStubImpl;->handleMeetingModeUpdate(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$msendEffectRefresh ( com.android.server.audio.AudioServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/audio/AudioServiceStubImpl;->sendEffectRefresh()V */
return;
} // .end method
static Integer -$$Nest$sfgetMQSSERVER_REPORT_RATE_MS ( ) { //bridge//synthethic
/* .locals 1 */
} // .end method
static com.android.server.audio.AudioServiceStubImpl ( ) {
/* .locals 4 */
/* .line 128 */
/* nop */
/* .line 129 */
final String v0 = "ro.vendor.audio.bt.adapter"; // const-string v0, "ro.vendor.audio.bt.adapter"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* .line 227 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 230 */
final String v2 = "scorpio"; // const-string v2, "scorpio"
/* .line 231 */
v0 = com.android.server.audio.AudioServiceStubImpl.HIFI_NOT_SUPPORT_DEVICE_LIST;
final String v2 = "lithium"; // const-string v2, "lithium"
/* .line 263 */
/* nop */
/* .line 264 */
final String v0 = "ro.vendor.audio.onetrack.upload.interval"; // const-string v0, "ro.vendor.audio.onetrack.upload.interval"
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* .line 328 */
int v0 = 1; // const/4 v0, 0x1
com.android.server.audio.AudioServiceStubImpl.mIsAudioPlaybackTriggerSupported = (v0!= 0);
/* .line 369 */
final String v0 = "audio_karaoke_enable=0"; // const-string v0, "audio_karaoke_enable=0"
final String v1 = "audio_karaoke_ktvmode=disable"; // const-string v1, "audio_karaoke_ktvmode=disable"
/* const-string/jumbo v2, "sound_transmit_enable=0" */
/* filled-new-array {v2, v2, v0, v1}, [Ljava/lang/String; */
/* .line 372 */
final String v0 = "audio_karaoke_enable=1"; // const-string v0, "audio_karaoke_enable=1"
final String v1 = "audio_karaoke_ktvmode=enable"; // const-string v1, "audio_karaoke_ktvmode=enable"
/* const-string/jumbo v2, "sound_transmit_enable=1" */
/* const-string/jumbo v3, "sound_transmit_enable=6" */
/* filled-new-array {v2, v3, v0, v1}, [Ljava/lang/String; */
return;
} // .end method
public com.android.server.audio.AudioServiceStubImpl ( ) {
/* .locals 18 */
/* .line 286 */
/* move-object/from16 v0, p0 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/audio/AudioServiceStub;-><init>()V */
/* .line 102 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->isSupportPollAudioMicStatus:Z */
/* .line 114 */
/* nop */
/* .line 115 */
final String v2 = "ro.vendor.audio.adsp.spatial"; // const-string v2, "ro.vendor.audio.adsp.spatial"
int v3 = 0; // const/4 v3, 0x0
v2 = android.os.SystemProperties .getBoolean ( v2,v3 );
/* iput-boolean v2, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsAdspSpatializerAvailable:Z */
/* .line 117 */
/* nop */
/* .line 118 */
final String v2 = "ro.vendor.audio.camera.bt.record.support"; // const-string v2, "ro.vendor.audio.camera.bt.record.support"
android.os.SystemProperties .get ( v2 );
/* const-string/jumbo v4, "true" */
v2 = (( java.lang.String ) v4 ).equals ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* iput-boolean v2, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportedCameraRecord:Z */
/* .line 120 */
/* nop */
/* .line 121 */
/* const-string/jumbo v2, "vendor.audio.dolby.control.support" */
android.os.SystemProperties .get ( v2 );
v2 = (( java.lang.String ) v4 ).equals ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* iput-boolean v2, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportedDolbyEffectControl:Z */
/* .line 123 */
/* nop */
/* .line 124 */
final String v2 = "persist.vendor.audio.power.save.setting"; // const-string v2, "persist.vendor.audio.power.save.setting"
v2 = android.os.SystemProperties .getInt ( v2,v3 );
/* iput v2, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mSettingAudioPowerSave:I */
/* .line 130 */
/* and-int/lit8 v5, v2, 0x1 */
if ( v5 != null) { // if-eqz v5, :cond_0
/* move v5, v1 */
} // :cond_0
/* move v5, v3 */
} // :goto_0
/* iput-boolean v5, v0, Lcom/android/server/audio/AudioServiceStubImpl;->REALLY_TIME_MODE_ENABLED:Z */
/* .line 132 */
/* and-int/lit8 v2, v2, 0x2 */
if ( v2 != null) { // if-eqz v2, :cond_1
} // :cond_1
/* move v1, v3 */
} // :goto_1
/* iput-boolean v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->SOUND_LEAK_PROTECTION_ENABLED:Z */
/* .line 135 */
/* nop */
/* .line 136 */
final String v1 = "persist.vendor.audio.cinema.support"; // const-string v1, "persist.vendor.audio.cinema.support"
android.os.SystemProperties .get ( v1 );
v1 = (( java.lang.String ) v4 ).equals ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* iput-boolean v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportCinemaMode:Z */
/* .line 138 */
/* nop */
/* .line 139 */
final String v1 = "ro.vendor.audio.fweffect"; // const-string v1, "ro.vendor.audio.fweffect"
v1 = android.os.SystemProperties .getBoolean ( v1,v3 );
/* iput-boolean v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportFWAudioEffectCenter:Z */
/* .line 144 */
/* const-wide/16 v1, 0x0 */
/* iput-wide v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->sLastAudioStateExpTime:J */
/* .line 145 */
int v5 = -1; // const/4 v5, -0x1
/* iput v5, v0, Lcom/android/server/audio/AudioServiceStubImpl;->sLastReportAudioErrorType:I */
/* .line 146 */
/* iput v3, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mHeadsetDeviceConnectedState:I */
/* .line 147 */
/* const/16 v6, 0xa */
/* iput v6, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mScoBtState:I */
/* .line 148 */
/* iput v3, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mA2dpDeviceConnectedState:I */
/* .line 151 */
/* new-instance v6, Ljava/util/LinkedList; */
/* invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V */
this.mCommunicationRouteClients = v6;
/* .line 152 */
/* new-instance v6, Ljava/util/LinkedList; */
/* invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V */
this.mSetScoCommunicationDevice = v6;
/* .line 163 */
/* nop */
/* .line 164 */
final String v6 = "ro.vendor.audio.playbackcapture.attenuation"; // const-string v6, "ro.vendor.audio.playbackcapture.attenuation"
/* const/16 v7, 0x14 */
v6 = android.os.SystemProperties .getInt ( v6,v7 );
/* iput v6, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mVolumeAttenuation:I */
/* .line 170 */
/* iput v3, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mDelayNotifyBtStopScoCount:I */
/* .line 174 */
/* iput-boolean v3, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mInDelayNotifyBtStopSco:Z */
/* .line 178 */
final String v6 = ""; // const-string v6, ""
this.mMultiVoipPackages = v6;
/* .line 181 */
/* iput v3, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mModeOwnerPid:I */
/* .line 193 */
/* new-instance v6, Landroid/media/MiuiXlog; */
/* invoke-direct {v6}, Landroid/media/MiuiXlog;-><init>()V */
this.mMiuiXlog = v6;
/* .line 198 */
int v6 = 5; // const/4 v6, 0x5
/* new-array v6, v6, [F */
/* fill-array-data v6, :array_0 */
this.mPrescaleAbsoluteVolume = v6;
/* .line 206 */
/* const-string/jumbo v7, "zen_mode" */
final String v8 = "notification_sound"; // const-string v8, "notification_sound"
final String v9 = "calendar_alert"; // const-string v9, "calendar_alert"
final String v10 = "notes_alert"; // const-string v10, "notes_alert"
/* const-string/jumbo v11, "sms_received_sound" */
/* const-string/jumbo v12, "sms_received_sound_slot_1" */
/* const-string/jumbo v13, "sms_received_sound_slot_2" */
final String v14 = "random_note_mode_random_sound_number"; // const-string v14, "random_note_mode_random_sound_number"
final String v15 = "random_note_mode_sequence_sound_number"; // const-string v15, "random_note_mode_sequence_sound_number"
final String v16 = "random_note_mode_sequence_time_interval_ms"; // const-string v16, "random_note_mode_sequence_time_interval_ms"
final String v17 = "random_note_mode_mute_time_interval_ms"; // const-string v17, "random_note_mode_mute_time_interval_ms"
/* filled-new-array/range {v7 ..v17}, [Ljava/lang/String; */
this.mRegisterContentName = v6;
/* .line 221 */
/* iput-boolean v3, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mVolumeBoostEnabled:Z */
/* .line 225 */
final String v6 = "ro.vendor.audio.voice.super_volume"; // const-string v6, "ro.vendor.audio.voice.super_volume"
android.os.SystemProperties .get ( v6 );
v4 = (( java.lang.String ) v6 ).equals ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* iput-boolean v4, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mSuperVoiceVolumeSupported:Z */
/* .line 235 */
/* const/high16 v4, 0x4000000 */
/* iput v4, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mReceiveNotificationDevice:I */
/* .line 236 */
/* iput v3, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mNotificationTimes:I */
/* .line 237 */
/* iput v5, v0, Lcom/android/server/audio/AudioServiceStubImpl;->cameraToastServiceRiid:I */
/* .line 271 */
/* iput-boolean v3, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mSuperVolumeOn:Z */
/* .line 318 */
int v4 = 0; // const/4 v4, 0x0
this.mNotificationDate = v4;
/* .line 342 */
/* iput-wide v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMs:J */
/* .line 343 */
/* iput-wide v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMsTotal:J */
/* .line 344 */
/* iput-wide v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mPlaybackStartTime:J */
/* .line 345 */
/* iput-wide v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mPlaybackEndTime:J */
/* .line 348 */
/* iput-wide v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mStartMs:J */
/* .line 349 */
/* iput-wide v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mContinuousMs:J */
/* .line 350 */
/* iput v3, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mHighLevel:I */
/* .line 353 */
/* iput-wide v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mCumulativePlaybackStartTime:J */
/* .line 354 */
/* iput-wide v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mCumulativePlaybackEndTime:J */
/* .line 357 */
/* iput-boolean v3, v0, Lcom/android/server/audio/AudioServiceStubImpl;->markBluetoothheadsetstub:Z */
/* .line 360 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
this.mAudioParameterClientList = v1;
/* .line 375 */
this.mParameterClientDeathListener = v4;
/* .line 377 */
/* iput-boolean v3, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mSuperVoiceVolumeOn:Z */
/* .line 287 */
/* new-instance v1, Landroid/os/HandlerThread; */
final String v2 = "AudioServiceStubImpl"; // const-string v2, "AudioServiceStubImpl"
/* invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mWorkerThread = v1;
/* .line 288 */
(( android.os.HandlerThread ) v1 ).start ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V
/* .line 289 */
/* new-instance v1, Lcom/android/server/audio/AudioServiceStubImpl$H; */
v2 = this.mWorkerThread;
(( android.os.HandlerThread ) v2 ).getLooper ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v1, v0, v2}, Lcom/android/server/audio/AudioServiceStubImpl$H;-><init>(Lcom/android/server/audio/AudioServiceStubImpl;Landroid/os/Looper;)V */
this.mHandler = v1;
/* .line 290 */
return;
/* nop */
/* :array_0 */
/* .array-data 4 */
/* 0x3f000000 # 0.5f */
/* 0x3f333333 # 0.7f */
/* 0x3f59999a # 0.85f */
/* 0x3f666666 # 0.9f */
/* 0x3f733333 # 0.95f */
} // .end array-data
} // .end method
private com.android.server.audio.AudioParameterClient addAudioParameterClient ( android.os.IBinder p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "binder" # Landroid/os/IBinder; */
/* .param p2, "targetParameter" # Ljava/lang/String; */
/* .line 1292 */
v0 = this.mAudioParameterClientList;
/* monitor-enter v0 */
/* .line 1293 */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
/* invoke-direct {p0, p1, p2, v1}, Lcom/android/server/audio/AudioServiceStubImpl;->removeAudioParameterClient(Landroid/os/IBinder;Ljava/lang/String;Z)Lcom/android/server/audio/AudioParameterClient; */
/* .line 1295 */
/* .local v2, "client":Lcom/android/server/audio/AudioParameterClient; */
v3 = this.mParameterClientDeathListener;
/* if-nez v3, :cond_0 */
/* .line 1296 */
/* new-instance v3, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda7; */
/* invoke-direct {v3, p0}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda7;-><init>(Lcom/android/server/audio/AudioServiceStubImpl;)V */
this.mParameterClientDeathListener = v3;
/* .line 1310 */
} // :cond_0
/* if-nez v2, :cond_1 */
/* .line 1311 */
/* new-instance v3, Lcom/android/server/audio/AudioParameterClient; */
/* invoke-direct {v3, p1, p2}, Lcom/android/server/audio/AudioParameterClient;-><init>(Landroid/os/IBinder;Ljava/lang/String;)V */
/* move-object v2, v3 */
/* .line 1312 */
v3 = this.mParameterClientDeathListener;
(( com.android.server.audio.AudioParameterClient ) v2 ).setClientDiedListener ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/audio/AudioParameterClient;->setClientDiedListener(Lcom/android/server/audio/AudioParameterClient$ClientDeathListener;)V
/* .line 1313 */
(( com.android.server.audio.AudioParameterClient ) v2 ).registerDeathRecipient ( ); // invoke-virtual {v2}, Lcom/android/server/audio/AudioParameterClient;->registerDeathRecipient()Z
/* .line 1315 */
} // :cond_1
v3 = this.mAudioParameterClientList;
/* .line 1316 */
/* monitor-exit v0 */
/* .line 1317 */
} // .end local v2 # "client":Lcom/android/server/audio/AudioParameterClient;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void adjustHiFiVolume ( Integer p0, android.content.Context p1 ) {
/* .locals 2 */
/* .param p1, "direction" # I */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 808 */
v0 = miui.util.AudioManagerHelper .getHiFiVolume ( p2 );
/* .line 810 */
/* .local v0, "currentHiFiVolume":I */
int v1 = -1; // const/4 v1, -0x1
/* if-ne p1, v1, :cond_0 */
/* .line 811 */
/* add-int/lit8 v1, v0, -0xa */
miui.util.AudioManagerHelper .setHiFiVolume ( p2,v1 );
/* .line 813 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
/* if-ne p1, v1, :cond_1 */
/* const/16 v1, 0x64 */
/* if-ge v0, v1, :cond_1 */
/* .line 814 */
/* add-int/lit8 v1, v0, 0xa */
miui.util.AudioManagerHelper .setHiFiVolume ( p2,v1 );
/* .line 817 */
} // :cond_1
} // :goto_0
return;
} // .end method
private Boolean checkAudioRouteForBtConnected ( ) {
/* .locals 4 */
/* .line 1983 */
v0 = this.mAudioService;
int v1 = 0; // const/4 v1, 0x0
v0 = (( com.android.server.audio.AudioService ) v0 ).getDeviceForStream ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/audio/AudioService;->getDeviceForStream(I)I
/* .line 1984 */
/* .local v0, "voiceDevice":I */
v2 = android.media.AudioSystem.DEVICE_OUT_ALL_SCO_SET;
v2 = java.lang.Integer .valueOf ( v0 );
/* if-nez v2, :cond_0 */
/* .line 1985 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "checkAudioRouteForBtConnected: route is error voiceDevice=0x"; // const-string v3, "checkAudioRouteForBtConnected: route is error voiceDevice=0x"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1986 */
java.lang.Integer .toHexString ( v0 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1985 */
final String v3 = "AudioServiceStubImpl"; // const-string v3, "AudioServiceStubImpl"
android.util.Log .d ( v3,v2 );
/* .line 1987 */
/* .line 1989 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
} // .end method
private void clearSetScoCommunicationDevice ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "skipPid" # I */
/* .line 2075 */
/* iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportedReportAudioRouteState:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2076 */
v0 = this.mSetScoCommunicationDevice;
/* monitor-enter v0 */
/* .line 2077 */
try { // :try_start_0
final String v1 = "AudioServiceStubImpl"; // const-string v1, "AudioServiceStubImpl"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "clearSetScoCommunicationDevice: skipPid="; // const-string v3, "clearSetScoCommunicationDevice: skipPid="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v2 );
/* .line 2078 */
v1 = this.mSetScoCommunicationDevice;
/* new-instance v2, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda6; */
/* invoke-direct {v2, p1}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda6;-><init>(I)V */
(( java.util.LinkedList ) v1 ).removeIf ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/LinkedList;->removeIf(Ljava/util/function/Predicate;)Z
/* .line 2079 */
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 2081 */
} // :cond_0
} // :goto_0
return;
} // .end method
private void createNotification ( android.content.Context p0 ) {
/* .locals 10 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 773 */
/* iget v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mNotificationTimes:I */
int v1 = 1; // const/4 v1, 0x1
/* add-int/2addr v0, v1 */
/* iput v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mNotificationTimes:I */
/* .line 774 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 775 */
/* .local v0, "it":Landroid/content/Intent; */
final String v2 = "com.miui.misound"; // const-string v2, "com.miui.misound"
final String v3 = "com.miui.misound.HeadsetSettingsActivity"; // const-string v3, "com.miui.misound.HeadsetSettingsActivity"
(( android.content.Intent ) v0 ).setClassName ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 777 */
/* const/high16 v2, 0x4000000 */
int v3 = 0; // const/4 v3, 0x0
android.app.PendingIntent .getActivity ( p1,v3,v0,v2 );
/* .line 780 */
/* .local v2, "pit":Landroid/app/PendingIntent; */
v4 = com.android.internal.notification.SystemNotificationChannels.USB;
/* .line 781 */
/* .local v4, "channel":Ljava/lang/String; */
/* const v5, 0x110f0229 */
/* .line 782 */
/* .local v5, "title_usb_handset":I */
/* const v6, 0x110f0227 */
/* .line 783 */
/* .local v6, "text_usb_handset":I */
/* new-instance v7, Landroid/app/Notification$Builder; */
/* invoke-direct {v7, p1, v4}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V */
/* .line 784 */
/* const v8, 0x10808b9 */
(( android.app.Notification$Builder ) v7 ).setSmallIcon ( v8 ); // invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;
/* .line 785 */
/* const-wide/16 v8, 0x0 */
(( android.app.Notification$Builder ) v7 ).setWhen ( v8, v9 ); // invoke-virtual {v7, v8, v9}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;
/* .line 786 */
(( android.app.Notification$Builder ) v7 ).setOngoing ( v1 ); // invoke-virtual {v7, v1}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;
/* .line 787 */
(( android.app.Notification$Builder ) v7 ).setDefaults ( v3 ); // invoke-virtual {v7, v3}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;
/* .line 788 */
/* const v7, 0x106001c */
v7 = (( android.content.Context ) p1 ).getColor ( v7 ); // invoke-virtual {p1, v7}, Landroid/content/Context;->getColor(I)I
(( android.app.Notification$Builder ) v3 ).setColor ( v7 ); // invoke-virtual {v3, v7}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;
/* .line 791 */
/* const-string/jumbo v7, "sys" */
(( android.app.Notification$Builder ) v3 ).setCategory ( v7 ); // invoke-virtual {v3, v7}, Landroid/app/Notification$Builder;->setCategory(Ljava/lang/String;)Landroid/app/Notification$Builder;
/* .line 792 */
(( android.app.Notification$Builder ) v3 ).setVisibility ( v1 ); // invoke-virtual {v3, v1}, Landroid/app/Notification$Builder;->setVisibility(I)Landroid/app/Notification$Builder;
/* .line 793 */
(( android.app.Notification$Builder ) v1 ).setContentIntent ( v2 ); // invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;
/* .line 794 */
(( android.content.Context ) p1 ).getString ( v5 ); // invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;
(( android.app.Notification$Builder ) v1 ).setContentTitle ( v3 ); // invoke-virtual {v1, v3}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
/* .line 795 */
(( android.content.Context ) p1 ).getString ( v6 ); // invoke-virtual {p1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;
(( android.app.Notification$Builder ) v1 ).setContentText ( v3 ); // invoke-virtual {v1, v3}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
/* .line 796 */
/* .local v1, "builder":Landroid/app/Notification$Builder; */
(( android.app.Notification$Builder ) v1 ).build ( ); // invoke-virtual {v1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;
/* .line 797 */
/* .local v3, "notify":Landroid/app/Notification; */
/* nop */
/* .line 804 */
v7 = this.mNm;
/* const v8, 0x53466666 */
(( android.app.NotificationManager ) v7 ).notify ( v8, v3 ); // invoke-virtual {v7, v8, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V
/* .line 805 */
return;
} // .end method
private com.android.server.audio.AudioParameterClient getAudioParameterClient ( android.os.IBinder p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "binder" # Landroid/os/IBinder; */
/* .param p2, "targetParameter" # Ljava/lang/String; */
/* .line 1263 */
v0 = this.mAudioParameterClientList;
/* monitor-enter v0 */
/* .line 1264 */
try { // :try_start_0
v1 = this.mAudioParameterClientList;
/* .line 1265 */
/* .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/audio/AudioParameterClient;>;" */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1266 */
/* check-cast v2, Lcom/android/server/audio/AudioParameterClient; */
/* .line 1267 */
/* .local v2, "client":Lcom/android/server/audio/AudioParameterClient; */
(( com.android.server.audio.AudioParameterClient ) v2 ).getBinder ( ); // invoke-virtual {v2}, Lcom/android/server/audio/AudioParameterClient;->getBinder()Landroid/os/IBinder;
/* if-ne v3, p1, :cond_0 */
/* .line 1268 */
(( com.android.server.audio.AudioParameterClient ) v2 ).getTargetParameter ( ); // invoke-virtual {v2}, Lcom/android/server/audio/AudioParameterClient;->getTargetParameter()Ljava/lang/String;
v3 = (( java.lang.String ) v3 ).equals ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1269 */
/* monitor-exit v0 */
/* .line 1271 */
} // .end local v2 # "client":Lcom/android/server/audio/AudioParameterClient;
} // :cond_0
/* .line 1272 */
} // :cond_1
/* monitor-exit v0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1273 */
} // .end local v1 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/audio/AudioParameterClient;>;"
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private java.lang.String getDeviceToStringForStreamMusic ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "device" # I */
/* .line 742 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "getDeviceToString : device="; // const-string v1, "getDeviceToString : device="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AudioServiceStubImpl"; // const-string v1, "AudioServiceStubImpl"
android.util.Log .d ( v1,v0 );
/* .line 744 */
/* sparse-switch p1, :sswitch_data_0 */
/* .line 756 */
final String v0 = "getDeviceToString : other devices"; // const-string v0, "getDeviceToString : other devices"
android.util.Log .d ( v1,v0 );
/* .line 759 */
final String v0 = "other devices"; // const-string v0, "other devices"
/* .line 748 */
/* :sswitch_0 */
/* const-string/jumbo v0, "usb.headset" */
/* .line 754 */
/* :sswitch_1 */
final String v0 = "bluetooth"; // const-string v0, "bluetooth"
/* .line 752 */
/* :sswitch_2 */
/* const-string/jumbo v0, "wired.headphone" */
/* .line 750 */
/* :sswitch_3 */
/* const-string/jumbo v0, "wired.headset" */
/* .line 746 */
/* :sswitch_4 */
/* const-string/jumbo v0, "speaker" */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x2 -> :sswitch_4 */
/* 0x4 -> :sswitch_3 */
/* 0x8 -> :sswitch_2 */
/* 0x80 -> :sswitch_1 */
/* 0x4000000 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
private getIntPredicates ( Integer p0, android.media.audiopolicy.AudioMix p1, java.util.function.ToIntFunction p2 ) {
/* .locals 2 */
/* .param p1, "rule" # I */
/* .param p2, "mix" # Landroid/media/audiopolicy/AudioMix; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I", */
/* "Landroid/media/audiopolicy/AudioMix;", */
/* "Ljava/util/function/ToIntFunction<", */
/* "Landroid/media/audiopolicy/AudioMixingRule$AudioMixMatchCriterion;", */
/* ">;)[I" */
/* } */
} // .end annotation
/* .line 1194 */
/* .local p3, "getPredicate":Ljava/util/function/ToIntFunction;, "Ljava/util/function/ToIntFunction<Landroid/media/audiopolicy/AudioMixingRule$AudioMixMatchCriterion;>;" */
(( android.media.audiopolicy.AudioMix ) p2 ).getRule ( ); // invoke-virtual {p2}, Landroid/media/audiopolicy/AudioMix;->getRule()Landroid/media/audiopolicy/AudioMixingRule;
(( android.media.audiopolicy.AudioMixingRule ) v0 ).getCriteria ( ); // invoke-virtual {v0}, Landroid/media/audiopolicy/AudioMixingRule;->getCriteria()Ljava/util/ArrayList;
(( java.util.ArrayList ) v0 ).stream ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->stream()Ljava/util/stream/Stream;
/* new-instance v1, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda5; */
/* invoke-direct {v1, p1}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda5;-><init>(I)V */
/* .line 1195 */
/* .line 1196 */
/* .line 1197 */
/* .line 1194 */
} // .end method
private void handleAudioModeUpdate ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "audioMode" # I */
/* .line 2101 */
/* sget-boolean v0, Lcom/android/server/audio/pad/PadAdapter;->ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2102 */
v0 = this.mPadAdapter;
(( com.android.server.audio.pad.PadAdapter ) v0 ).handleAudioModeUpdate ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/audio/pad/PadAdapter;->handleAudioModeUpdate(I)V
/* .line 2105 */
} // :cond_0
/* sget-boolean v0, Lcom/android/server/audio/foldable/FoldableAdapter;->ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2106 */
v0 = this.mFoldableAdapter;
(( com.android.server.audio.foldable.FoldableAdapter ) v0 ).onUpdateAudioMode ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/audio/foldable/FoldableAdapter;->onUpdateAudioMode(I)V
/* .line 2108 */
} // :cond_1
return;
} // .end method
private void handleMediaStateUpdate ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "mediaActive" # Z */
/* .line 2111 */
/* sget-boolean v0, Lcom/android/server/audio/foldable/FoldableAdapter;->ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2112 */
v0 = this.mFoldableAdapter;
(( com.android.server.audio.foldable.FoldableAdapter ) v0 ).onUpdateMediaState ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/audio/foldable/FoldableAdapter;->onUpdateMediaState(Z)V
/* .line 2114 */
} // :cond_0
return;
} // .end method
private void handleMeetingModeUpdate ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "parameter" # Ljava/lang/String; */
/* .line 2117 */
/* sget-boolean v0, Lcom/android/server/audio/pad/PadAdapter;->ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2118 */
v0 = this.mPadAdapter;
(( com.android.server.audio.pad.PadAdapter ) v0 ).handleMeetingModeUpdate ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/audio/pad/PadAdapter;->handleMeetingModeUpdate(Ljava/lang/String;)V
/* .line 2120 */
} // :cond_0
return;
} // .end method
private void handleParameters ( java.lang.String p0 ) {
/* .locals 11 */
/* .param p1, "keyValuePairs" # Ljava/lang/String; */
/* .line 1394 */
/* sget-boolean v0, Lcom/android/server/audio/pad/PadAdapter;->ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1395 */
if ( p1 != null) { // if-eqz p1, :cond_0
final String v0 = "remote_record_mode"; // const-string v0, "remote_record_mode"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1396 */
v0 = this.mHandler;
/* const/16 v1, 0x8 */
(( android.os.Handler ) v0 ).obtainMessage ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 1397 */
return;
/* .line 1401 */
} // :cond_0
final String v0 = ";"; // const-string v0, ";"
(( java.lang.String ) p1 ).split ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1402 */
/* .local v0, "kvpairs":[Ljava/lang/String; */
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
/* move v3, v2 */
} // :goto_0
/* if-ge v3, v1, :cond_4 */
/* aget-object v4, v0, v3 */
/* .line 1403 */
/* .local v4, "pair":Ljava/lang/String; */
final String v5 = "="; // const-string v5, "="
(( java.lang.String ) v4 ).split ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1404 */
/* .local v5, "kv":[Ljava/lang/String; */
final String v6 = "audio_sys_with_mic"; // const-string v6, "audio_sys_with_mic"
/* aget-object v7, v5, v2 */
v6 = (( java.lang.String ) v6 ).equals ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v7 = 1; // const/4 v7, 0x1
int v8 = 2; // const/4 v8, 0x2
int v9 = 3; // const/4 v9, 0x3
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 1405 */
final String v6 = "1"; // const-string v6, "1"
/* aget-object v7, v5, v7 */
v6 = (( java.lang.String ) v6 ).equals ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 1406 */
/* .local v6, "enable":Z */
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 1407 */
/* iget v7, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicVolumeDb:F */
/* iget v10, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mVolumeAttenuation:I */
/* neg-int v10, v10 */
/* int-to-float v10, v10 */
/* cmpl-float v7, v7, v10 */
/* if-lez v7, :cond_3 */
/* .line 1408 */
v7 = this.mAudioService;
/* .line 1409 */
v7 = (( com.android.server.audio.AudioService ) v7 ).getStreamMaxVolume ( v9 ); // invoke-virtual {v7, v9}, Lcom/android/server/audio/AudioService;->getStreamMaxVolume(I)I
/* .line 1410 */
/* .local v7, "musicMaxIndex":I */
v10 = this.mAudioService;
(( com.android.server.audio.AudioService ) v10 ).setStreamVolumeInt ( v9, v7, v8 ); // invoke-virtual {v10, v9, v7, v8}, Lcom/android/server/audio/AudioService;->setStreamVolumeInt(III)V
/* .line 1412 */
} // .end local v7 # "musicMaxIndex":I
/* .line 1414 */
} // :cond_1
v7 = this.mAudioService;
/* iget v10, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicVolumeIndex:I */
(( com.android.server.audio.AudioService ) v7 ).setStreamVolumeInt ( v9, v10, v8 ); // invoke-virtual {v7, v9, v10, v8}, Lcom/android/server/audio/AudioService;->setStreamVolumeInt(III)V
/* .line 1417 */
} // .end local v6 # "enable":Z
} // :cond_2
final String v6 = "audio_playback_capture_for_screen"; // const-string v6, "audio_playback_capture_for_screen"
/* aget-object v10, v5, v2 */
v6 = (( java.lang.String ) v6 ).equals ( v10 ); // invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_3
/* const-string/jumbo v6, "true" */
/* aget-object v7, v5, v7 */
v6 = (( java.lang.String ) v6 ).equals ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 1418 */
v6 = this.mAudioService;
v6 = (( com.android.server.audio.AudioService ) v6 ).getDeviceStreamVolume ( v9, v8 ); // invoke-virtual {v6, v9, v8}, Lcom/android/server/audio/AudioService;->getDeviceStreamVolume(II)I
/* iput v6, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicVolumeIndex:I */
/* .line 1420 */
v6 = android.media.AudioSystem .getStreamVolumeDB ( v9,v6,v8 );
/* iput v6, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicVolumeDb:F */
/* .line 1417 */
} // :cond_3
} // :goto_1
/* nop */
/* .line 1402 */
} // .end local v4 # "pair":Ljava/lang/String;
} // .end local v5 # "kv":[Ljava/lang/String;
} // :goto_2
/* add-int/lit8 v3, v3, 0x1 */
/* .line 1425 */
} // :cond_4
return;
} // .end method
private Boolean isApplyMiuiCustom ( ) {
/* .locals 2 */
/* .line 924 */
final String v0 = "ro.vendor.audio.skip_miui_volume_custom"; // const-string v0, "ro.vendor.audio.skip_miui_volume_custom"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 925 */
/* .line 927 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
public static Boolean isBluetoothHeadsetDevice ( android.bluetooth.BluetoothClass p0 ) {
/* .locals 3 */
/* .param p0, "bluetoothClass" # Landroid/bluetooth/BluetoothClass; */
/* .line 684 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p0, :cond_0 */
/* .line 685 */
/* .line 687 */
} // :cond_0
v1 = (( android.bluetooth.BluetoothClass ) p0 ).getDeviceClass ( ); // invoke-virtual {p0}, Landroid/bluetooth/BluetoothClass;->getDeviceClass()I
/* .line 688 */
/* .local v1, "deviceClass":I */
/* const/16 v2, 0x418 */
/* if-eq v1, v2, :cond_1 */
/* const/16 v2, 0x404 */
/* if-ne v1, v2, :cond_2 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // :cond_2
} // .end method
private Boolean isMusicPlayerActive ( android.media.AudioPlaybackConfiguration p0 ) {
/* .locals 4 */
/* .param p1, "apc" # Landroid/media/AudioPlaybackConfiguration; */
/* .line 1154 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 1155 */
} // :cond_0
(( android.media.AudioPlaybackConfiguration ) p1 ).getAudioAttributes ( ); // invoke-virtual {p1}, Landroid/media/AudioPlaybackConfiguration;->getAudioAttributes()Landroid/media/AudioAttributes;
v1 = (( android.media.AudioAttributes ) v1 ).getUsage ( ); // invoke-virtual {v1}, Landroid/media/AudioAttributes;->getUsage()I
int v2 = 1; // const/4 v2, 0x1
/* if-eq v1, v2, :cond_1 */
/* .line 1156 */
(( android.media.AudioPlaybackConfiguration ) p1 ).getAudioAttributes ( ); // invoke-virtual {p1}, Landroid/media/AudioPlaybackConfiguration;->getAudioAttributes()Landroid/media/AudioAttributes;
v1 = (( android.media.AudioAttributes ) v1 ).getVolumeControlStream ( ); // invoke-virtual {v1}, Landroid/media/AudioAttributes;->getVolumeControlStream()I
int v3 = 3; // const/4 v3, 0x3
/* if-ne v1, v3, :cond_2 */
/* .line 1157 */
} // :cond_1
v1 = (( android.media.AudioPlaybackConfiguration ) p1 ).getPlayerState ( ); // invoke-virtual {p1}, Landroid/media/AudioPlaybackConfiguration;->getPlayerState()I
int v3 = 2; // const/4 v3, 0x2
/* if-ne v1, v3, :cond_2 */
/* .line 1158 */
/* .line 1160 */
} // :cond_2
} // .end method
private Boolean isMuteMusicFromMIUI ( android.content.ContentResolver p0 ) {
/* .locals 3 */
/* .param p1, "cr" # Landroid/content/ContentResolver; */
/* .line 912 */
int v0 = -3; // const/4 v0, -0x3
final String v1 = "mute_music_at_silent"; // const-string v1, "mute_music_at_silent"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$System .getIntForUser ( p1,v1,v2,v0 );
/* .line 914 */
/* .local v0, "muteMusic":I */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v2, v1 */
} // :cond_0
} // .end method
private Boolean isSetScoCommunicationDevice ( ) {
/* .locals 6 */
/* .line 1968 */
v0 = this.mSetScoCommunicationDevice;
/* monitor-enter v0 */
/* .line 1969 */
int v1 = 0; // const/4 v1, 0x0
/* .line 1971 */
/* .local v1, "isAppSetScoRequest":Z */
try { // :try_start_0
v2 = this.mSetScoCommunicationDevice;
(( java.util.LinkedList ) v2 ).iterator ( ); // invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Lcom/android/server/audio/AudioDeviceBroker$CommunicationDeviceInfo; */
/* .line 1972 */
/* .local v3, "deviceInfo":Lcom/android/server/audio/AudioDeviceBroker$CommunicationDeviceInfo; */
/* iget v4, v3, Lcom/android/server/audio/AudioDeviceBroker$CommunicationDeviceInfo;->mPid:I */
/* iget v5, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mModeOwnerPid:I */
/* if-ne v4, v5, :cond_0 */
v4 = this.mDevice;
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 1973 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1974 */
/* .line 1976 */
} // .end local v3 # "deviceInfo":Lcom/android/server/audio/AudioDeviceBroker$CommunicationDeviceInfo;
} // :cond_0
/* .line 1977 */
} // :cond_1
} // :goto_1
final String v2 = "AudioServiceStubImpl"; // const-string v2, "AudioServiceStubImpl"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "isSetScoCommunicationDevice="; // const-string v4, "isSetScoCommunicationDevice="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v3 );
/* .line 1978 */
/* monitor-exit v0 */
/* .line 1979 */
} // .end local v1 # "isAppSetScoRequest":Z
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void lambda$addAudioParameterClient$2 ( android.os.IBinder p0, java.lang.String p1 ) { //synthethic
/* .locals 3 */
/* .param p1, "diedBinder" # Landroid/os/IBinder; */
/* .param p2, "diedTargetParameter" # Ljava/lang/String; */
/* .line 1297 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = com.android.server.audio.AudioServiceStubImpl.TRANSMIT_AUDIO_PARAMETERS;
/* array-length v2, v1 */
/* if-ge v0, v2, :cond_2 */
/* .line 1298 */
/* aget-object v1, v1, v0 */
v1 = (( java.lang.String ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1299 */
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {p0, p1, p2, v1}, Lcom/android/server/audio/AudioServiceStubImpl;->removeAudioParameterClient(Landroid/os/IBinder;Ljava/lang/String;Z)Lcom/android/server/audio/AudioParameterClient; */
/* .line 1300 */
v1 = v1 = this.mAudioParameterClientList;
/* if-lez v1, :cond_0 */
/* .line 1301 */
v1 = this.mAudioParameterClientList;
/* .line 1302 */
int v2 = 0; // const/4 v2, 0x0
/* check-cast v1, Lcom/android/server/audio/AudioParameterClient; */
(( com.android.server.audio.AudioParameterClient ) v1 ).getTargetParameter ( ); // invoke-virtual {v1}, Lcom/android/server/audio/AudioParameterClient;->getTargetParameter()Ljava/lang/String;
/* .line 1301 */
android.media.AudioSystem .setParameters ( v1 );
/* .line 1304 */
} // :cond_0
v1 = com.android.server.audio.AudioServiceStubImpl.DEFAULT_AUDIO_PARAMETERS;
/* aget-object v1, v1, v0 */
android.media.AudioSystem .setParameters ( v1 );
/* .line 1297 */
} // :cond_1
} // :goto_1
/* add-int/lit8 v0, v0, 0x1 */
/* .line 1308 */
} // .end local v0 # "i":I
} // :cond_2
return;
} // .end method
static Boolean lambda$clearSetScoCommunicationDevice$5 ( Integer p0, com.android.server.audio.AudioDeviceBroker$CommunicationDeviceInfo p1 ) { //synthethic
/* .locals 1 */
/* .param p0, "skipPid" # I */
/* .param p1, "deviceInfo" # Lcom/android/server/audio/AudioDeviceBroker$CommunicationDeviceInfo; */
/* .line 2078 */
/* iget v0, p1, Lcom/android/server/audio/AudioDeviceBroker$CommunicationDeviceInfo;->mPid:I */
/* if-eq v0, p0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
static Integer lambda$getAudioPolicyMatchUids$0 ( android.media.audiopolicy.AudioMixingRule$AudioMixMatchCriterion p0 ) { //synthethic
/* .locals 1 */
/* .param p0, "criterion" # Landroid/media/audiopolicy/AudioMixingRule$AudioMixMatchCriterion; */
/* .line 1181 */
v0 = (( android.media.audiopolicy.AudioMixingRule$AudioMixMatchCriterion ) p0 ).getIntProp ( ); // invoke-virtual {p0}, Landroid/media/audiopolicy/AudioMixingRule$AudioMixMatchCriterion;->getIntProp()I
} // .end method
static Boolean lambda$getIntPredicates$1 ( Integer p0, android.media.audiopolicy.AudioMixingRule$AudioMixMatchCriterion p1 ) { //synthethic
/* .locals 1 */
/* .param p0, "rule" # I */
/* .param p1, "criterion" # Landroid/media/audiopolicy/AudioMixingRule$AudioMixMatchCriterion; */
/* .line 1195 */
v0 = (( android.media.audiopolicy.AudioMixingRule$AudioMixMatchCriterion ) p1 ).getRule ( ); // invoke-virtual {p1}, Landroid/media/audiopolicy/AudioMixingRule$AudioMixMatchCriterion;->getRule()I
/* if-ne v0, p0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
static java.lang.String lambda$handleWaveformInfoTracked$3 ( android.os.vibrator.VibrationEffectSegment p0 ) { //synthethic
/* .locals 1 */
/* .param p0, "seg" # Landroid/os/vibrator/VibrationEffectSegment; */
/* .line 1545 */
(( java.lang.Object ) p0 ).toString ( ); // invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // .end method
static java.lang.String lambda$handleWaveformInfoTracked$4 ( android.os.vibrator.VibrationEffectSegment p0 ) { //synthethic
/* .locals 1 */
/* .param p0, "seg" # Landroid/os/vibrator/VibrationEffectSegment; */
/* .line 1553 */
(( java.lang.Object ) p0 ).toString ( ); // invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // .end method
private Boolean needEnableVoiceVolumeBoost ( Integer p0, Boolean p1, Integer p2, Integer p3, Integer p4 ) {
/* .locals 4 */
/* .param p1, "dir" # I */
/* .param p2, "isMax" # Z */
/* .param p3, "device" # I */
/* .param p4, "alias" # I */
/* .param p5, "mode" # I */
/* .line 841 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "needEnableVoiceVolumeBoost"; // const-string v1, "needEnableVoiceVolumeBoost"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p5 ); // invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " ismax="; // const-string v1, " ismax="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " device="; // const-string v1, " device="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " alias="; // const-string v1, " alias="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " dir="; // const-string v1, " dir="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AudioServiceStubImpl"; // const-string v1, "AudioServiceStubImpl"
android.util.Log .d ( v1,v0 );
/* .line 843 */
int v0 = 2; // const/4 v0, 0x2
int v1 = 0; // const/4 v1, 0x0
/* if-eq p5, v0, :cond_0 */
/* .line 844 */
/* .line 846 */
} // :cond_0
/* if-nez p4, :cond_4 */
int v0 = 1; // const/4 v0, 0x1
/* if-ne p3, v0, :cond_4 */
/* .line 848 */
final String v2 = "ro.vendor.audio.voice.volume.boost"; // const-string v2, "ro.vendor.audio.voice.volume.boost"
android.os.SystemProperties .get ( v2 );
final String v3 = "manual"; // const-string v3, "manual"
v2 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_1 */
/* .line 851 */
} // :cond_1
/* if-ne p1, v0, :cond_2 */
if ( p2 != null) { // if-eqz p2, :cond_2
/* .line 853 */
/* .line 855 */
} // :cond_2
int v2 = -1; // const/4 v2, -0x1
/* if-ne p1, v2, :cond_3 */
if ( p2 != null) { // if-eqz p2, :cond_3
/* .line 857 */
/* .line 859 */
} // :cond_3
/* .line 849 */
} // :cond_4
} // :goto_0
} // .end method
private void notifyBtStopBluetoothSco ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "eventSource" # Ljava/lang/String; */
/* .line 1825 */
/* iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mInDelayNotifyBtStopSco:Z */
/* if-nez v0, :cond_0 */
/* .line 1826 */
return;
/* .line 1828 */
} // :cond_0
/* invoke-direct {p0, p1}, Lcom/android/server/audio/AudioServiceStubImpl;->resetNotifyBtStopScoStatus(Ljava/lang/String;)V */
/* .line 1829 */
v0 = this.mDeviceBroker;
v0 = (( com.android.server.audio.AudioDeviceBroker ) v0 ).isBluetoothScoRequested ( ); // invoke-virtual {v0}, Lcom/android/server/audio/AudioDeviceBroker;->isBluetoothScoRequested()Z
/* if-nez v0, :cond_1 */
/* .line 1830 */
v0 = this.mBtHelper;
(( com.android.server.audio.BtHelper ) v0 ).stopBluetoothSco ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/audio/BtHelper;->stopBluetoothSco(Ljava/lang/String;)Z
/* .line 1831 */
final String v0 = "disconnect"; // const-string v0, "disconnect"
/* invoke-direct {p0, v0}, Lcom/android/server/audio/AudioServiceStubImpl;->reportMultiVoipDailyUseForBtConnect(Ljava/lang/String;)V */
/* .line 1833 */
} // :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "don\'t need notify bt stopBluetoothSco modeOwnerPid: "; // const-string v1, "don\'t need notify bt stopBluetoothSco modeOwnerPid: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mModeOwnerPid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", eventSource: "; // const-string v1, ", eventSource: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AudioServiceStubImpl"; // const-string v1, "AudioServiceStubImpl"
android.util.Log .d ( v1,v0 );
/* .line 1835 */
final String v0 = "connect"; // const-string v0, "connect"
/* invoke-direct {p0, v0}, Lcom/android/server/audio/AudioServiceStubImpl;->reportMultiVoipDailyUseForBtConnect(Ljava/lang/String;)V */
/* .line 1837 */
} // :goto_0
return;
} // .end method
private void persistCumulativePlaybackStartMsToSettings ( ) {
/* .locals 4 */
/* .line 1033 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "persistCumulativePlaybackStartMsToSettings: mCumulativePlaybackStartTime: "; // const-string v1, "persistCumulativePlaybackStartMsToSettings: mCumulativePlaybackStartTime: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mCumulativePlaybackStartTime:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AudioServiceStubImpl"; // const-string v1, "AudioServiceStubImpl"
android.util.Log .d ( v1,v0 );
/* .line 1035 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "key_persist_cumulative_playback_ms"; // const-string v1, "key_persist_cumulative_playback_ms"
/* iget-wide v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mCumulativePlaybackStartTime:J */
android.provider.Settings$Global .putLong ( v0,v1,v2,v3 );
/* .line 1037 */
return;
} // .end method
private void persistNotificationDateToSettings ( android.content.Context p0, java.time.LocalDate p1 ) {
/* .locals 3 */
/* .param p1, "cx" # Landroid/content/Context; */
/* .param p2, "localDate" # Ljava/time/LocalDate; */
/* .line 1040 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "persistNotificationDateToSettings: localDate: "; // const-string v1, "persistNotificationDateToSettings: localDate: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AudioServiceStubImpl"; // const-string v1, "AudioServiceStubImpl"
android.util.Log .d ( v1,v0 );
/* .line 1041 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1042 */
(( java.time.LocalDate ) p2 ).toString ( ); // invoke-virtual {p2}, Ljava/time/LocalDate;->toString()Ljava/lang/String;
/* .line 1041 */
final String v2 = "key_persist_notification_date"; // const-string v2, "key_persist_notification_date"
android.provider.Settings$Global .putString ( v0,v2,v1 );
/* .line 1043 */
return;
} // .end method
private com.android.server.audio.AudioParameterClient removeAudioParameterClient ( android.os.IBinder p0, java.lang.String p1, Boolean p2 ) {
/* .locals 3 */
/* .param p1, "binder" # Landroid/os/IBinder; */
/* .param p2, "targetParameter" # Ljava/lang/String; */
/* .param p3, "unregister" # Z */
/* .line 1278 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/audio/AudioServiceStubImpl;->getAudioParameterClient(Landroid/os/IBinder;Ljava/lang/String;)Lcom/android/server/audio/AudioParameterClient; */
/* .line 1279 */
/* .local v0, "client":Lcom/android/server/audio/AudioParameterClient; */
v1 = this.mAudioParameterClientList;
/* monitor-enter v1 */
/* .line 1280 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1281 */
try { // :try_start_0
v2 = this.mAudioParameterClientList;
/* .line 1282 */
if ( p3 != null) { // if-eqz p3, :cond_0
/* .line 1283 */
(( com.android.server.audio.AudioParameterClient ) v0 ).unregisterDeathRecipient ( ); // invoke-virtual {v0}, Lcom/android/server/audio/AudioParameterClient;->unregisterDeathRecipient()V
/* .line 1285 */
} // :cond_0
/* monitor-exit v1 */
/* .line 1287 */
} // :cond_1
/* monitor-exit v1 */
int v1 = 0; // const/4 v1, 0x0
/* .line 1288 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
private void reportAudioStatus ( android.content.Context p0, java.util.List p1 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/content/Context;", */
/* "Ljava/util/List<", */
/* "Landroid/media/AudioPlaybackConfiguration;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 763 */
/* .local p2, "apcList":Ljava/util/List;, "Ljava/util/List<Landroid/media/AudioPlaybackConfiguration;>;" */
/* new-instance v0, Lcom/android/server/audio/MQSUtils; */
/* invoke-direct {v0, p1}, Lcom/android/server/audio/MQSUtils;-><init>(Landroid/content/Context;)V */
/* .line 764 */
/* .local v0, "mqs":Lcom/android/server/audio/MQSUtils; */
(( com.android.server.audio.MQSUtils ) v0 ).reportAudioVisualDailyUse ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/audio/MQSUtils;->reportAudioVisualDailyUse(Ljava/util/List;)V
/* .line 765 */
v1 = (( com.android.server.audio.MQSUtils ) v0 ).needToReport ( ); // invoke-virtual {v0}, Lcom/android/server/audio/MQSUtils;->needToReport()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 766 */
(( com.android.server.audio.MQSUtils ) v0 ).reportAudioButtonStatus ( ); // invoke-virtual {v0}, Lcom/android/server/audio/MQSUtils;->reportAudioButtonStatus()V
/* .line 767 */
(( com.android.server.audio.MQSUtils ) v0 ).reportVibrateStatus ( ); // invoke-virtual {v0}, Lcom/android/server/audio/MQSUtils;->reportVibrateStatus()V
/* .line 768 */
(( com.android.server.audio.AudioServiceStubImpl ) p0 ).reportAudioHwState ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/audio/AudioServiceStubImpl;->reportAudioHwState(Landroid/content/Context;)V
/* .line 770 */
} // :cond_0
return;
} // .end method
private void reportMultiVoipDailyUseForBtConnect ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "scoState" # Ljava/lang/String; */
/* .line 1848 */
v0 = this.mHandler;
int v1 = 2; // const/4 v1, 0x2
(( android.os.Handler ) v0 ).obtainMessage ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 1849 */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 1850 */
return;
} // .end method
private void resetNotifyBtStopScoStatus ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "eventSource" # Ljava/lang/String; */
/* .line 1840 */
/* iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mInDelayNotifyBtStopSco:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1841 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mDelayNotifyBtStopScoCount:I */
/* .line 1842 */
/* iput-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mInDelayNotifyBtStopSco:Z */
/* .line 1843 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "resetNotifyBtStopScoStatus, eventSource: "; // const-string v1, "resetNotifyBtStopScoStatus, eventSource: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AudioServiceStubImpl"; // const-string v1, "AudioServiceStubImpl"
android.util.Log .d ( v1,v0 );
/* .line 1845 */
} // :cond_0
return;
} // .end method
private void sendEffectRefresh ( ) {
/* .locals 10 */
/* .line 1497 */
/* const-string/jumbo v0, "spatial" */
/* const-string/jumbo v1, "surround" */
final String v2 = "misound"; // const-string v2, "misound"
final String v3 = "dolby"; // const-string v3, "dolby"
/* const-string/jumbo v4, "sendEffectRefresh" */
final String v5 = "AudioServiceStubImpl"; // const-string v5, "AudioServiceStubImpl"
android.util.Log .d ( v5,v4 );
/* .line 1499 */
try { // :try_start_0
v4 = this.mContext;
android.media.audiofx.AudioEffectCenter .getInstance ( v4 );
/* .line 1500 */
/* .local v4, "audioEffectCenter":Landroid/media/audiofx/AudioEffectCenter; */
/* new-instance v6, Landroid/content/Intent; */
final String v7 = "miui.intent.action.ACTION_AUDIO_EFFECT_REFRESH"; // const-string v7, "miui.intent.action.ACTION_AUDIO_EFFECT_REFRESH"
/* invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 1501 */
/* .local v6, "intent":Landroid/content/Intent; */
/* new-instance v7, Landroid/os/Bundle; */
/* invoke-direct {v7}, Landroid/os/Bundle;-><init>()V */
/* .line 1502 */
/* .local v7, "bundle":Landroid/os/Bundle; */
final String v8 = "dolby_available"; // const-string v8, "dolby_available"
/* .line 1503 */
v9 = (( android.media.audiofx.AudioEffectCenter ) v4 ).isEffectAvailable ( v3 ); // invoke-virtual {v4, v3}, Landroid/media/audiofx/AudioEffectCenter;->isEffectAvailable(Ljava/lang/String;)Z
/* .line 1502 */
(( android.os.Bundle ) v7 ).putBoolean ( v8, v9 ); // invoke-virtual {v7, v8, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
/* .line 1504 */
final String v8 = "dolby_active"; // const-string v8, "dolby_active"
/* .line 1505 */
v3 = (( android.media.audiofx.AudioEffectCenter ) v4 ).isEffectActive ( v3 ); // invoke-virtual {v4, v3}, Landroid/media/audiofx/AudioEffectCenter;->isEffectActive(Ljava/lang/String;)Z
/* .line 1504 */
(( android.os.Bundle ) v7 ).putBoolean ( v8, v3 ); // invoke-virtual {v7, v8, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
/* .line 1506 */
final String v3 = "misound_available"; // const-string v3, "misound_available"
/* .line 1507 */
v8 = (( android.media.audiofx.AudioEffectCenter ) v4 ).isEffectAvailable ( v2 ); // invoke-virtual {v4, v2}, Landroid/media/audiofx/AudioEffectCenter;->isEffectAvailable(Ljava/lang/String;)Z
/* .line 1506 */
(( android.os.Bundle ) v7 ).putBoolean ( v3, v8 ); // invoke-virtual {v7, v3, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
/* .line 1508 */
final String v3 = "misound_active"; // const-string v3, "misound_active"
/* .line 1509 */
v2 = (( android.media.audiofx.AudioEffectCenter ) v4 ).isEffectActive ( v2 ); // invoke-virtual {v4, v2}, Landroid/media/audiofx/AudioEffectCenter;->isEffectActive(Ljava/lang/String;)Z
/* .line 1508 */
(( android.os.Bundle ) v7 ).putBoolean ( v3, v2 ); // invoke-virtual {v7, v3, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
/* .line 1510 */
final String v2 = "none_available"; // const-string v2, "none_available"
final String v3 = "none"; // const-string v3, "none"
/* .line 1511 */
v3 = (( android.media.audiofx.AudioEffectCenter ) v4 ).isEffectAvailable ( v3 ); // invoke-virtual {v4, v3}, Landroid/media/audiofx/AudioEffectCenter;->isEffectAvailable(Ljava/lang/String;)Z
/* .line 1510 */
(( android.os.Bundle ) v7 ).putBoolean ( v2, v3 ); // invoke-virtual {v7, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
/* .line 1512 */
/* const-string/jumbo v2, "surround_available" */
/* .line 1513 */
v3 = (( android.media.audiofx.AudioEffectCenter ) v4 ).isEffectAvailable ( v1 ); // invoke-virtual {v4, v1}, Landroid/media/audiofx/AudioEffectCenter;->isEffectAvailable(Ljava/lang/String;)Z
/* .line 1512 */
(( android.os.Bundle ) v7 ).putBoolean ( v2, v3 ); // invoke-virtual {v7, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
/* .line 1514 */
/* const-string/jumbo v2, "surround_active" */
/* .line 1515 */
v1 = (( android.media.audiofx.AudioEffectCenter ) v4 ).isEffectActive ( v1 ); // invoke-virtual {v4, v1}, Landroid/media/audiofx/AudioEffectCenter;->isEffectActive(Ljava/lang/String;)Z
/* .line 1514 */
(( android.os.Bundle ) v7 ).putBoolean ( v2, v1 ); // invoke-virtual {v7, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
/* .line 1516 */
/* const-string/jumbo v1, "spatial_available" */
/* .line 1517 */
v2 = (( android.media.audiofx.AudioEffectCenter ) v4 ).isEffectAvailable ( v0 ); // invoke-virtual {v4, v0}, Landroid/media/audiofx/AudioEffectCenter;->isEffectAvailable(Ljava/lang/String;)Z
/* .line 1516 */
(( android.os.Bundle ) v7 ).putBoolean ( v1, v2 ); // invoke-virtual {v7, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
/* .line 1518 */
/* const-string/jumbo v1, "spatial_active" */
/* .line 1519 */
v0 = (( android.media.audiofx.AudioEffectCenter ) v4 ).isEffectActive ( v0 ); // invoke-virtual {v4, v0}, Landroid/media/audiofx/AudioEffectCenter;->isEffectActive(Ljava/lang/String;)Z
/* .line 1518 */
(( android.os.Bundle ) v7 ).putBoolean ( v1, v0 ); // invoke-virtual {v7, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
/* .line 1520 */
final String v0 = "bundle"; // const-string v0, "bundle"
(( android.content.Intent ) v6 ).putExtra ( v0, v7 ); // invoke-virtual {v6, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;
/* .line 1521 */
v0 = this.mContext;
(( android.content.Context ) v0 ).sendBroadcast ( v6 ); // invoke-virtual {v0, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
/* :try_end_0 */
/* .catch Ljava/lang/RuntimeException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1525 */
} // .end local v4 # "audioEffectCenter":Landroid/media/audiofx/AudioEffectCenter;
} // .end local v6 # "intent":Landroid/content/Intent;
} // .end local v7 # "bundle":Landroid/os/Bundle;
/* .line 1522 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1523 */
/* .local v0, "e":Ljava/lang/RuntimeException; */
/* const-string/jumbo v1, "sendEffectRefresh error" */
android.util.Log .d ( v5,v1 );
/* .line 1524 */
(( java.lang.RuntimeException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V
/* .line 1526 */
} // .end local v0 # "e":Ljava/lang/RuntimeException;
} // :goto_0
return;
} // .end method
private void sendVolumeBoostBroadcast ( Boolean p0, android.content.Context p1 ) {
/* .locals 4 */
/* .param p1, "boostEnabled" # Z */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 872 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 874 */
/* .local v0, "ident":J */
try { // :try_start_0
/* new-instance v2, Landroid/content/Intent; */
final String v3 = "miui.intent.action.VOLUME_BOOST"; // const-string v3, "miui.intent.action.VOLUME_BOOST"
/* invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 875 */
/* .local v2, "intent":Landroid/content/Intent; */
/* const-string/jumbo v3, "volume_boost_state" */
(( android.content.Intent ) v2 ).putExtra ( v3, p1 ); // invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 876 */
v3 = android.os.UserHandle.ALL;
(( android.content.Context ) p2 ).sendStickyBroadcastAsUser ( v2, v3 ); // invoke-virtual {p2, v2, v3}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 878 */
} // .end local v2 # "intent":Landroid/content/Intent;
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 879 */
/* nop */
/* .line 880 */
return;
/* .line 878 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 879 */
/* throw v2 */
} // .end method
private void setSuperVoiceVolume ( Boolean p0, android.content.Context p1 ) {
/* .locals 4 */
/* .param p1, "boostEnabled" # Z */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 882 */
final String v0 = "audio"; // const-string v0, "audio"
(( android.content.Context ) p2 ).getSystemService ( v0 ); // invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/media/AudioManager; */
/* .line 883 */
/* .local v0, "am":Landroid/media/AudioManager; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "SUPER_VOICE_VOLUME="; // const-string v2, "SUPER_VOICE_VOLUME="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
if ( p1 != null) { // if-eqz p1, :cond_0
final String v2 = "on"; // const-string v2, "on"
} // :cond_0
final String v2 = "off"; // const-string v2, "off"
} // :goto_0
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 884 */
/* .local v1, "params":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "params:"; // const-string v3, "params:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "AudioServiceStubImpl"; // const-string v3, "AudioServiceStubImpl"
android.util.Log .d ( v3,v2 );
/* .line 885 */
(( android.media.AudioManager ) v0 ).setParameters ( v1 ); // invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V
/* .line 886 */
/* iput-boolean p1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mSuperVoiceVolumeOn:Z */
/* .line 887 */
return;
} // .end method
private void setVolumeBoost ( Boolean p0, android.content.Context p1 ) {
/* .locals 4 */
/* .param p1, "boostEnabled" # Z */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 863 */
final String v0 = "audio"; // const-string v0, "audio"
(( android.content.Context ) p2 ).getSystemService ( v0 ); // invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/media/AudioManager; */
/* .line 864 */
/* .local v0, "am":Landroid/media/AudioManager; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "voice_volume_boost=" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
if ( p1 != null) { // if-eqz p1, :cond_0
/* const-string/jumbo v2, "true" */
} // :cond_0
final String v2 = "false"; // const-string v2, "false"
} // :goto_0
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 865 */
/* .local v1, "params":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "params:"; // const-string v3, "params:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "AudioServiceStubImpl"; // const-string v3, "AudioServiceStubImpl"
android.util.Log .d ( v3,v2 );
/* .line 866 */
(( android.media.AudioManager ) v0 ).setParameters ( v1 ); // invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V
/* .line 867 */
/* iput-boolean p1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mVolumeBoostEnabled:Z */
/* .line 868 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/audio/AudioServiceStubImpl;->sendVolumeBoostBroadcast(ZLandroid/content/Context;)V */
/* .line 869 */
return;
} // .end method
private Boolean shouldAdjustHiFiVolume ( Integer p0, Integer p1, Integer p2, Integer p3, android.content.Context p4 ) {
/* .locals 6 */
/* .param p1, "streamType" # I */
/* .param p2, "direction" # I */
/* .param p3, "streamIndex" # I */
/* .param p4, "maxIndex" # I */
/* .param p5, "context" # Landroid/content/Context; */
/* .line 821 */
v0 = com.android.server.audio.AudioServiceStubImpl.HIFI_NOT_SUPPORT_DEVICE_LIST;
v0 = v1 = android.os.Build.DEVICE;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 822 */
/* .line 824 */
} // :cond_0
int v0 = 3; // const/4 v0, 0x3
/* if-ne p1, v0, :cond_6 */
/* .line 825 */
v0 = miui.util.AudioManagerHelper .isHiFiMode ( p5 );
/* if-nez v0, :cond_1 */
/* .line 829 */
} // :cond_1
v0 = miui.util.AudioManagerHelper .getHiFiVolume ( p5 );
/* .line 830 */
/* .local v0, "currentHiFiVolume":I */
/* move v2, p4 */
/* .line 831 */
/* .local v2, "maxStreamIndex":I */
int v3 = -1; // const/4 v3, -0x1
int v4 = 1; // const/4 v4, 0x1
/* if-ne p2, v3, :cond_2 */
/* if-lez v0, :cond_2 */
/* move v3, v4 */
} // :cond_2
/* move v3, v1 */
/* .line 833 */
/* .local v3, "adjustDownHiFiVolume":Z */
} // :goto_0
/* if-ne p2, v4, :cond_3 */
/* if-ne p3, v2, :cond_3 */
/* move v5, v4 */
} // :cond_3
/* move v5, v1 */
/* .line 835 */
/* .local v5, "adjustUpHiFiVolume":Z */
} // :goto_1
/* if-nez v3, :cond_4 */
if ( v5 != null) { // if-eqz v5, :cond_5
} // :cond_4
/* move v1, v4 */
} // :cond_5
/* .line 826 */
} // .end local v0 # "currentHiFiVolume":I
} // .end local v2 # "maxStreamIndex":I
} // .end local v3 # "adjustDownHiFiVolume":Z
} // .end local v5 # "adjustUpHiFiVolume":Z
} // :cond_6
} // :goto_2
} // .end method
private void startHearingProtectionService ( android.content.Context p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "cx" # Landroid/content/Context; */
/* .param p2, "msgId" # I */
/* .line 949 */
try { // :try_start_0
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 950 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.miui.misound.hearingprotection.notification"; // const-string v1, "com.miui.misound.hearingprotection.notification"
(( android.content.Intent ) v0 ).setAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 951 */
/* new-instance v1, Landroid/content/ComponentName; */
final String v2 = "com.miui.misound"; // const-string v2, "com.miui.misound"
final String v3 = "com.miui.misound.hearingprotection.HearingProtectionService"; // const-string v3, "com.miui.misound.hearingprotection.HearingProtectionService"
/* invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( android.content.Intent ) v0 ).setComponent ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 953 */
final String v1 = "notificationId"; // const-string v1, "notificationId"
(( android.content.Intent ) v0 ).putExtra ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 954 */
(( android.content.Context ) p1 ).startForegroundService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->startForegroundService(Landroid/content/Intent;)Landroid/content/ComponentName;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 957 */
/* nop */
} // .end local v0 # "intent":Landroid/content/Intent;
/* .line 955 */
/* :catch_0 */
/* move-exception v0 */
/* .line 956 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "AudioServiceStubImpl"; // const-string v1, "AudioServiceStubImpl"
final String v2 = "fail to start HearingProtectionService"; // const-string v2, "fail to start HearingProtectionService"
android.util.Log .e ( v1,v2 );
/* .line 958 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private android.content.ComponentName transportDataToService ( android.content.Context p0, Long p1, Long p2, Boolean p3 ) {
/* .locals 4 */
/* .param p1, "cx" # Landroid/content/Context; */
/* .param p2, "beginTime" # J */
/* .param p4, "endTime" # J */
/* .param p6, "isHigh" # Z */
/* .line 963 */
try { // :try_start_0
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 964 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.miui.misound.write.data"; // const-string v1, "com.miui.misound.write.data"
(( android.content.Intent ) v0 ).setAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 965 */
/* new-instance v1, Landroid/content/ComponentName; */
final String v2 = "com.miui.misound"; // const-string v2, "com.miui.misound"
final String v3 = "com.miui.misound.hearingprotection.HearingProtectionService"; // const-string v3, "com.miui.misound.hearingprotection.HearingProtectionService"
/* invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( android.content.Intent ) v0 ).setComponent ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 967 */
final String v1 = "beginMillis"; // const-string v1, "beginMillis"
(( android.content.Intent ) v0 ).putExtra ( v1, p2, p3 ); // invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;
/* .line 968 */
final String v1 = "endMillis"; // const-string v1, "endMillis"
(( android.content.Intent ) v0 ).putExtra ( v1, p4, p5 ); // invoke-virtual {v0, v1, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;
/* .line 969 */
final String v1 = "isHighPitch"; // const-string v1, "isHighPitch"
(( android.content.Intent ) v0 ).putExtra ( v1, p6 ); // invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 970 */
(( android.content.Context ) p1 ).startForegroundService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->startForegroundService(Landroid/content/Intent;)Landroid/content/ComponentName;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 971 */
} // .end local v0 # "intent":Landroid/content/Intent;
/* :catch_0 */
/* move-exception v0 */
/* .line 972 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "AudioServiceStubImpl"; // const-string v1, "AudioServiceStubImpl"
final String v2 = "fail to transport data to service"; // const-string v2, "fail to transport data to service"
android.util.Log .e ( v1,v2 );
/* .line 974 */
} // .end local v0 # "e":Ljava/lang/Exception;
int v0 = 0; // const/4 v0, 0x0
} // .end method
/* # virtual methods */
public void adjustDefaultStreamVolumeForMiui ( Integer[] p0 ) {
/* .locals 1 */
/* .param p1, "defaultStreamVolume" # [I */
/* .line 918 */
v0 = /* invoke-direct {p0}, Lcom/android/server/audio/AudioServiceStubImpl;->isApplyMiuiCustom()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 919 */
android.media.AudioServiceInjector .adjustDefaultStreamVolume ( p1 );
/* .line 921 */
} // :cond_0
return;
} // .end method
public void adjustStreamVolumeMiAudioServiceMTK ( Integer p0, Integer p1, Integer p2, java.lang.String p3, java.lang.String p4, Integer p5, Integer p6, java.lang.String p7, Boolean p8, Integer p9 ) {
/* .locals 12 */
/* .param p1, "streamType" # I */
/* .param p2, "direction" # I */
/* .param p3, "flags" # I */
/* .param p4, "callingPackage" # Ljava/lang/String; */
/* .param p5, "caller" # Ljava/lang/String; */
/* .param p6, "uid" # I */
/* .param p7, "pid" # I */
/* .param p8, "attributionTag" # Ljava/lang/String; */
/* .param p9, "hasModifyAudioSettings" # Z */
/* .param p10, "keyEventMode" # I */
/* .line 1348 */
/* move-object v0, p0 */
v1 = this.mMiAudioServiceMTK;
/* move v2, p1 */
/* move v3, p2 */
/* move v4, p3 */
/* move-object/from16 v5, p4 */
/* move-object/from16 v6, p5 */
/* move/from16 v7, p6 */
/* move/from16 v8, p7 */
/* move-object/from16 v9, p8 */
/* move/from16 v10, p9 */
/* move/from16 v11, p10 */
/* invoke-virtual/range {v1 ..v11}, Lcom/android/server/audio/MiAudioServiceMTK;->adjustStreamVolume(IIILjava/lang/String;Ljava/lang/String;IILjava/lang/String;ZI)V */
/* .line 1349 */
return;
} // .end method
public void adjustVolume ( android.media.AudioPlaybackConfiguration p0, Float p1 ) {
/* .locals 3 */
/* .param p1, "apc" # Landroid/media/AudioPlaybackConfiguration; */
/* .param p2, "volume" # F */
/* .line 619 */
/* const/high16 v0, 0x3f800000 # 1.0f */
/* cmpl-float v0, p2, v0 */
/* if-lez v0, :cond_0 */
/* .line 623 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* cmpg-float v0, p2, v0 */
/* if-gez v0, :cond_1 */
/* .line 624 */
int p2 = 0; // const/4 p2, 0x0
/* .line 627 */
} // :cond_1
com.android.server.audio.PlaybackActivityMonitorStub .get ( );
final String v1 = "AudioServiceStubImpl"; // const-string v1, "AudioServiceStubImpl"
v2 = this.mContext;
/* .line 629 */
} // :goto_0
return;
} // .end method
public android.os.IBinder createAudioRecordForLoopbackWithClient ( android.os.ParcelFileDescriptor p0, Long p1, android.os.IBinder p2 ) {
/* .locals 2 */
/* .param p1, "sharedMem" # Landroid/os/ParcelFileDescriptor; */
/* .param p2, "size" # J */
/* .param p4, "token" # Landroid/os/IBinder; */
/* .line 647 */
/* new-instance v0, Landroid/media/MiuiAudioRecord; */
(( android.os.ParcelFileDescriptor ) p1 ).getFileDescriptor ( ); // invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;
/* invoke-direct {v0, v1, p2, p3, p4}, Landroid/media/MiuiAudioRecord;-><init>(Ljava/io/FileDescriptor;JLandroid/os/IBinder;)V */
} // .end method
public void createKeyErrorNotification ( android.content.Context p0, Integer p1 ) {
/* .locals 16 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "notificationID" # I */
/* .line 1719 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
final String v2 = "notification"; // const-string v2, "notification"
(( android.content.Context ) v1 ).getSystemService ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v2, Landroid/app/NotificationManager; */
this.mNm = v2;
/* .line 1721 */
/* new-instance v2, Landroid/content/Intent; */
/* invoke-direct {v2}, Landroid/content/Intent;-><init>()V */
/* .line 1722 */
/* .local v2, "keyErrorIntent":Landroid/content/Intent; */
final String v3 = "android.server.Volume_Key_Error"; // const-string v3, "android.server.Volume_Key_Error"
(( android.content.Intent ) v2 ).setAction ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 1723 */
int v3 = 0; // const/4 v3, 0x0
/* const/high16 v4, 0x4000000 */
android.app.PendingIntent .getBroadcast ( v1,v3,v2,v4 );
/* .line 1725 */
/* .local v5, "keyErrorPit":Landroid/app/PendingIntent; */
/* new-instance v6, Landroid/content/Intent; */
/* invoke-direct {v6}, Landroid/content/Intent;-><init>()V */
/* .line 1726 */
/* .local v6, "intent":Landroid/content/Intent; */
final String v7 = "android.server.Volume_Key_NoBlock"; // const-string v7, "android.server.Volume_Key_NoBlock"
(( android.content.Intent ) v6 ).setAction ( v7 ); // invoke-virtual {v6, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 1727 */
android.app.PendingIntent .getBroadcast ( v1,v3,v6,v4 );
/* .line 1729 */
/* .local v4, "pit":Landroid/app/PendingIntent; */
v7 = com.android.internal.notification.SystemNotificationChannels.USB_HEADSET;
/* .line 1730 */
/* .local v7, "channel":Ljava/lang/String; */
/* const v8, 0x110f0431 */
/* .line 1732 */
/* .local v8, "title_optimization":I */
/* const v9, 0x110f0430 */
/* .line 1734 */
/* .local v9, "text_optimization":I */
/* const v10, 0x110f02a7 */
/* .line 1735 */
/* .local v10, "button_ok":I */
/* const v11, 0x110f02a6 */
/* .line 1737 */
/* .local v11, "button_cancel":I */
/* new-instance v12, Landroid/app/Notification$Builder; */
/* invoke-direct {v12, v1, v7}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V */
/* .line 1738 */
/* const v13, 0x10808b9 */
(( android.app.Notification$Builder ) v12 ).setSmallIcon ( v13 ); // invoke-virtual {v12, v13}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;
/* .line 1739 */
/* const-wide/16 v14, 0x0 */
(( android.app.Notification$Builder ) v12 ).setWhen ( v14, v15 ); // invoke-virtual {v12, v14, v15}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;
/* .line 1740 */
(( android.app.Notification$Builder ) v12 ).setOngoing ( v3 ); // invoke-virtual {v12, v3}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;
/* .line 1741 */
int v14 = 1; // const/4 v14, 0x1
(( android.app.Notification$Builder ) v12 ).setAutoCancel ( v14 ); // invoke-virtual {v12, v14}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;
/* .line 1742 */
(( android.app.Notification$Builder ) v12 ).setDefaults ( v3 ); // invoke-virtual {v12, v3}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;
/* .line 1743 */
/* const v12, 0x106001c */
v12 = (( android.content.Context ) v1 ).getColor ( v12 ); // invoke-virtual {v1, v12}, Landroid/content/Context;->getColor(I)I
(( android.app.Notification$Builder ) v3 ).setColor ( v12 ); // invoke-virtual {v3, v12}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;
/* .line 1745 */
final String v12 = "recommendation"; // const-string v12, "recommendation"
(( android.app.Notification$Builder ) v3 ).setCategory ( v12 ); // invoke-virtual {v3, v12}, Landroid/app/Notification$Builder;->setCategory(Ljava/lang/String;)Landroid/app/Notification$Builder;
/* .line 1746 */
(( android.app.Notification$Builder ) v3 ).setVisibility ( v14 ); // invoke-virtual {v3, v14}, Landroid/app/Notification$Builder;->setVisibility(I)Landroid/app/Notification$Builder;
/* .line 1747 */
(( android.content.Context ) v1 ).getString ( v8 ); // invoke-virtual {v1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;
(( android.app.Notification$Builder ) v3 ).setContentTitle ( v12 ); // invoke-virtual {v3, v12}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
/* .line 1748 */
(( android.content.Context ) v1 ).getString ( v9 ); // invoke-virtual {v1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;
(( android.app.Notification$Builder ) v3 ).setContentText ( v12 ); // invoke-virtual {v3, v12}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
/* .line 1750 */
(( android.content.Context ) v1 ).getString ( v10 ); // invoke-virtual {v1, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;
/* .line 1749 */
(( android.app.Notification$Builder ) v3 ).addAction ( v13, v12, v5 ); // invoke-virtual {v3, v13, v12, v5}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;
/* .line 1752 */
(( android.content.Context ) v1 ).getString ( v11 ); // invoke-virtual {v1, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;
/* .line 1751 */
(( android.app.Notification$Builder ) v3 ).addAction ( v13, v12, v4 ); // invoke-virtual {v3, v13, v12, v4}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;
/* .line 1753 */
/* .local v3, "builder":Landroid/app/Notification$Builder; */
(( android.app.Notification$Builder ) v3 ).build ( ); // invoke-virtual {v3}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;
/* .line 1754 */
/* .local v12, "notify":Landroid/app/Notification; */
v13 = this.mNm;
/* move/from16 v14, p2 */
(( android.app.NotificationManager ) v13 ).notify ( v14, v12 ); // invoke-virtual {v13, v14, v12}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V
/* .line 1755 */
return;
} // .end method
public android.os.IBinder createMiuiAudioRecord ( android.os.ParcelFileDescriptor p0, Long p1 ) {
/* .locals 2 */
/* .param p1, "sharedMem" # Landroid/os/ParcelFileDescriptor; */
/* .param p2, "size" # J */
/* .line 641 */
/* new-instance v0, Landroid/media/MiuiAudioRecord; */
(( android.os.ParcelFileDescriptor ) p1 ).getFileDescriptor ( ); // invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;
/* invoke-direct {v0, v1, p2, p3}, Landroid/media/MiuiAudioRecord;-><init>(Ljava/io/FileDescriptor;J)V */
} // .end method
public void customMinStreamVolume ( Integer[] p0 ) {
/* .locals 0 */
/* .param p1, "minStreamVolume" # [I */
/* .line 1165 */
android.media.AudioServiceInjector .customMinStreamVolume ( p1 );
/* .line 1166 */
return;
} // .end method
public void delayNotifyBtStopBluetoothSco ( android.os.Handler p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "brokerHandler" # Landroid/os/Handler; */
/* .param p2, "eventSource" # Ljava/lang/String; */
/* .line 1790 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 1791 */
/* nop */
/* .line 1792 */
/* const/16 v0, 0x73 */
(( android.os.Handler ) p1 ).obtainMessage ( v0, p2 ); // invoke-virtual {p1, v0, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 1791 */
/* const-wide/16 v1, 0xc8 */
(( android.os.Handler ) p1 ).sendMessageDelayed ( v0, v1, v2 ); // invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 1795 */
} // :cond_0
return;
} // .end method
public Boolean delayNotifyBtStopBluetoothScoIfNeed ( Integer p0, java.lang.String p1 ) {
/* .locals 6 */
/* .param p1, "pid" # I */
/* .param p2, "eventSource" # Ljava/lang/String; */
/* .line 1760 */
int v0 = 0; // const/4 v0, 0x0
if ( p2 != null) { // if-eqz p2, :cond_0
/* const-string/jumbo v1, "setSpeakerphoneOn" */
v1 = (( java.lang.String ) p2 ).startsWith ( v1 ); // invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1761 */
/* invoke-direct {p0, p2}, Lcom/android/server/audio/AudioServiceStubImpl;->resetNotifyBtStopScoStatus(Ljava/lang/String;)V */
/* .line 1762 */
/* .line 1765 */
} // :cond_0
v1 = this.mAudioService;
v1 = (( com.android.server.audio.AudioService ) v1 ).getModeOwnerPid ( ); // invoke-virtual {v1}, Lcom/android/server/audio/AudioService;->getModeOwnerPid()I
/* .line 1766 */
/* .local v1, "modeOwnerPidFromAudioService":I */
final String v2 = ", mModeOwnerPid\uff1a "; // const-string v2, ", mModeOwnerPid\uff1a "
final String v3 = "modeOwnerPidFromAudioService\uff1a"; // const-string v3, "modeOwnerPidFromAudioService\uff1a"
final String v4 = "AudioServiceStubImpl"; // const-string v4, "AudioServiceStubImpl"
if ( v1 != null) { // if-eqz v1, :cond_6
/* iget v5, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mModeOwnerPid:I */
/* if-nez v5, :cond_1 */
/* .line 1772 */
} // :cond_1
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mModeOwnerPid:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v4,v2 );
/* .line 1774 */
int v2 = 1; // const/4 v2, 0x1
/* if-eq v1, p1, :cond_2 */
/* iget v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mModeOwnerPid:I */
/* if-ne p1, v3, :cond_2 */
v3 = this.mDeviceBroker;
/* .line 1776 */
v3 = (( com.android.server.audio.AudioDeviceBroker ) v3 ).isBluetoothScoRequestForPid ( v1 ); // invoke-virtual {v3, v1}, Lcom/android/server/audio/AudioDeviceBroker;->isBluetoothScoRequestForPid(I)Z
/* if-nez v3, :cond_3 */
} // :cond_2
/* if-ne v1, p1, :cond_4 */
/* iget v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mModeOwnerPid:I */
/* if-ne v3, p1, :cond_4 */
v3 = this.mDeviceBroker;
v4 = this.mAudioService;
/* .line 1779 */
v4 = (( com.android.server.audio.AudioService ) v4 ).getNextModeOwnerPid ( p1 ); // invoke-virtual {v4, p1}, Lcom/android/server/audio/AudioService;->getNextModeOwnerPid(I)I
v3 = (( com.android.server.audio.AudioDeviceBroker ) v3 ).isBluetoothScoRequestForPid ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/audio/AudioDeviceBroker;->isBluetoothScoRequestForPid(I)Z
if ( v3 != null) { // if-eqz v3, :cond_4
} // :cond_3
/* move v3, v2 */
} // :cond_4
/* move v3, v0 */
/* .line 1780 */
/* .local v3, "isScoRequestExisting":Z */
} // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 1781 */
/* iput-boolean v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mInDelayNotifyBtStopSco:Z */
/* .line 1782 */
/* .line 1784 */
} // :cond_5
/* invoke-direct {p0, p2}, Lcom/android/server/audio/AudioServiceStubImpl;->resetNotifyBtStopScoStatus(Ljava/lang/String;)V */
/* .line 1785 */
/* .line 1767 */
} // .end local v3 # "isScoRequestExisting":Z
} // :cond_6
} // :goto_1
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mModeOwnerPid:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v4,v2 );
/* .line 1769 */
/* invoke-direct {p0, p2}, Lcom/android/server/audio/AudioServiceStubImpl;->resetNotifyBtStopScoStatus(Ljava/lang/String;)V */
/* .line 1770 */
} // .end method
public void dumpMediaSound ( java.io.PrintWriter p0 ) {
/* .locals 1 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 633 */
final String v0 = " mOpenSoundAssist="; // const-string v0, " mOpenSoundAssist="
(( java.io.PrintWriter ) p1 ).print ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 634 */
v0 = com.android.server.audio.PlaybackActivityMonitorStub .get ( );
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 635 */
final String v0 = " mIgnrMusicFocusReq="; // const-string v0, " mIgnrMusicFocusReq="
(( java.io.PrintWriter ) p1 ).print ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 636 */
v0 = com.android.server.audio.PlaybackActivityMonitorStub .get ( );
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 637 */
return;
} // .end method
public void enableHifiVolume ( Integer p0, Integer p1, Integer p2, Integer p3, android.content.Context p4 ) {
/* .locals 1 */
/* .param p1, "stream" # I */
/* .param p2, "direction" # I */
/* .param p3, "oldIndex" # I */
/* .param p4, "indexMax" # I */
/* .param p5, "context" # Landroid/content/Context; */
/* .line 521 */
v0 = /* invoke-direct/range {p0 ..p5}, Lcom/android/server/audio/AudioServiceStubImpl;->shouldAdjustHiFiVolume(IIIILandroid/content/Context;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 522 */
/* invoke-direct {p0, p2, p5}, Lcom/android/server/audio/AudioServiceStubImpl;->adjustHiFiVolume(ILandroid/content/Context;)V */
/* .line 525 */
} // :cond_0
return;
} // .end method
public Integer enableSuperIndex ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "mStreamType" # I */
/* .param p2, "mIndexSuper" # I */
/* .param p3, "mIndexMax" # I */
/* .line 1658 */
int v0 = -1; // const/4 v0, -0x1
/* .line 1659 */
/* .local v0, "superIndexAdd":I */
final String v1 = "ro.vendor.audio.volume_super_index_add"; // const-string v1, "ro.vendor.audio.volume_super_index_add"
int v2 = -1; // const/4 v2, -0x1
v0 = android.os.SystemProperties .getInt ( v1,v2 );
/* .line 1661 */
/* if-eq v0, v2, :cond_1 */
int v1 = 3; // const/4 v1, 0x3
/* if-eq p1, v1, :cond_0 */
int v1 = 2; // const/4 v1, 0x2
/* if-eq p1, v1, :cond_0 */
int v1 = 5; // const/4 v1, 0x5
/* if-ne p1, v1, :cond_1 */
/* .line 1665 */
} // :cond_0
/* add-int p2, p3, v0 */
/* .line 1666 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "SuperVolume: mIndexSuper is "; // const-string v2, "SuperVolume: mIndexSuper is "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " mStreamType is "; // const-string v2, " mStreamType is "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "AudioServiceStubImpl"; // const-string v2, "AudioServiceStubImpl"
android.util.Log .d ( v2,v1 );
/* .line 1668 */
} // :cond_1
} // .end method
public Boolean enableVoiceVolumeBoost ( Integer p0, Boolean p1, Integer p2, Integer p3, java.lang.String p4, Integer p5, android.content.Context p6 ) {
/* .locals 7 */
/* .param p1, "dir" # I */
/* .param p2, "isMax" # Z */
/* .param p3, "device" # I */
/* .param p4, "alias" # I */
/* .param p5, "pkg" # Ljava/lang/String; */
/* .param p6, "mode" # I */
/* .param p7, "context" # Landroid/content/Context; */
/* .line 440 */
int v0 = 0; // const/4 v0, 0x0
/* .line 441 */
/* .local v0, "isChanged":Z */
v1 = (( com.android.server.audio.AudioServiceStubImpl ) p0 ).isCtsVerifier ( p5 ); // invoke-virtual {p0, p5}, Lcom/android/server/audio/AudioServiceStubImpl;->isCtsVerifier(Ljava/lang/String;)Z
/* if-nez v1, :cond_1 */
/* .line 442 */
/* move-object v1, p0 */
/* move v2, p1 */
/* move v3, p2 */
/* move v4, p3 */
/* move v5, p4 */
/* move v6, p6 */
v1 = /* invoke-direct/range {v1 ..v6}, Lcom/android/server/audio/AudioServiceStubImpl;->needEnableVoiceVolumeBoost(IZIII)Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 447 */
int v1 = 1; // const/4 v1, 0x1
/* if-ne p1, v1, :cond_0 */
/* iget-boolean v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mVolumeBoostEnabled:Z */
/* if-nez v2, :cond_0 */
/* .line 448 */
/* invoke-direct {p0, v1, p7}, Lcom/android/server/audio/AudioServiceStubImpl;->setVolumeBoost(ZLandroid/content/Context;)V */
/* .line 449 */
int v0 = 1; // const/4 v0, 0x1
/* .line 450 */
} // :cond_0
int v1 = -1; // const/4 v1, -0x1
/* if-ne p1, v1, :cond_1 */
/* iget-boolean v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mVolumeBoostEnabled:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 451 */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {p0, v1, p7}, Lcom/android/server/audio/AudioServiceStubImpl;->setVolumeBoost(ZLandroid/content/Context;)V */
/* .line 452 */
int v0 = 1; // const/4 v0, 0x1
/* .line 456 */
} // :cond_1
} // :goto_0
} // .end method
public void foldDisable ( ) {
/* .locals 0 */
/* .line 674 */
com.android.server.audio.FoldHelper .disable ( );
/* .line 675 */
return;
} // .end method
public void foldEnable ( ) {
/* .locals 0 */
/* .line 669 */
com.android.server.audio.FoldHelper .enable ( );
/* .line 670 */
return;
} // .end method
public void foldInit ( ) {
/* .locals 0 */
/* .line 664 */
com.android.server.audio.FoldHelper .init ( );
/* .line 665 */
return;
} // .end method
public Integer getAbsoluteVolumeIndex ( Integer p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "index" # I */
/* .param p2, "indexMax" # I */
/* .line 403 */
/* div-int/lit16 v0, p2, 0x96 */
/* .line 404 */
/* .local v0, "step":I */
/* if-nez p1, :cond_0 */
/* .line 406 */
int p1 = 0; // const/4 p1, 0x0
/* .line 407 */
} // :cond_0
/* if-lez p1, :cond_1 */
/* mul-int/lit8 v1, v0, 0x5 */
/* if-gt p1, v1, :cond_1 */
/* .line 409 */
/* new-instance v1, Ljava/math/BigDecimal; */
/* sub-int v2, p1, v0 */
/* div-int/2addr v2, v0 */
/* int-to-double v2, v2 */
java.lang.Math .ceil ( v2,v3 );
/* move-result-wide v2 */
/* invoke-direct {v1, v2, v3}, Ljava/math/BigDecimal;-><init>(D)V */
/* .line 410 */
/* .local v1, "bd":Ljava/math/BigDecimal; */
v2 = (( java.math.BigDecimal ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/math/BigDecimal;->intValue()I
/* .line 411 */
/* .local v2, "pos":I */
/* int-to-float v3, p2 */
v4 = this.mPrescaleAbsoluteVolume;
/* aget v4, v4, v2 */
/* mul-float/2addr v3, v4 */
/* float-to-int v3, v3 */
/* div-int/lit8 p1, v3, 0xa */
/* .line 412 */
} // .end local v1 # "bd":Ljava/math/BigDecimal;
} // .end local v2 # "pos":I
/* .line 414 */
} // :cond_1
/* add-int/lit8 v1, p2, 0x5 */
/* div-int/lit8 p1, v1, 0xa */
/* .line 416 */
} // :goto_0
} // .end method
public getAudioPolicyMatchUids ( java.util.HashMap p0 ) {
/* .locals 8 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/HashMap<", */
/* "Landroid/os/IBinder;", */
/* "Lcom/android/server/audio/AudioService$AudioPolicyProxy;", */
/* ">;)[I" */
/* } */
} // .end annotation
/* .line 1171 */
/* .local p1, "policys":Ljava/util/HashMap;, "Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/audio/AudioService$AudioPolicyProxy;>;" */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 1175 */
/* .local v0, "uidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
(( java.util.HashMap ) p1 ).values ( ); // invoke-virtual {p1}, Ljava/util/HashMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Lcom/android/server/audio/AudioService$AudioPolicyProxy; */
/* .line 1176 */
/* .local v2, "policy":Lcom/android/server/audio/AudioService$AudioPolicyProxy; */
v3 = this.mProjection;
/* if-nez v3, :cond_0 */
/* .line 1177 */
/* .line 1179 */
} // :cond_0
(( com.android.server.audio.AudioService$AudioPolicyProxy ) v2 ).getMixes ( ); // invoke-virtual {v2}, Lcom/android/server/audio/AudioService$AudioPolicyProxy;->getMixes()Ljava/util/ArrayList;
(( java.util.ArrayList ) v3 ).iterator ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v4 = } // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_1
/* check-cast v4, Landroid/media/audiopolicy/AudioMix; */
/* .line 1180 */
/* .local v4, "mix":Landroid/media/audiopolicy/AudioMix; */
/* new-instance v5, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v5}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda0;-><init>()V */
int v6 = 4; // const/4 v6, 0x4
/* invoke-direct {p0, v6, v4, v5}, Lcom/android/server/audio/AudioServiceStubImpl;->getIntPredicates(ILandroid/media/audiopolicy/AudioMix;Ljava/util/function/ToIntFunction;)[I */
/* .line 1182 */
/* .local v5, "matchUidArray":[I */
/* nop */
/* .line 1183 */
java.util.Arrays .stream ( v5 );
/* .line 1184 */
java.util.stream.Collectors .toList ( );
/* check-cast v6, Ljava/util/List; */
/* .line 1185 */
/* .local v6, "listCollect":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .line 1186 */
} // .end local v4 # "mix":Landroid/media/audiopolicy/AudioMix;
} // .end local v5 # "matchUidArray":[I
} // .end local v6 # "listCollect":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
/* .line 1187 */
} // .end local v2 # "policy":Lcom/android/server/audio/AudioService$AudioPolicyProxy;
} // :cond_1
/* .line 1188 */
} // :cond_2
/* new-instance v2, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda1; */
/* invoke-direct {v2}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda1;-><init>()V */
/* .line 1189 */
/* .local v1, "arrays":[I */
} // .end method
public Boolean getBluetoothHeadset ( ) {
/* .locals 1 */
/* .line 709 */
/* iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->markBluetoothheadsetstub:Z */
} // .end method
public com.android.server.audio.AudioService getMiAudioService ( android.content.Context p0 ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 1333 */
/* new-instance v0, Lcom/android/server/audio/MiAudioService; */
/* invoke-direct {v0, p1}, Lcom/android/server/audio/MiAudioService;-><init>(Landroid/content/Context;)V */
} // .end method
public Integer getModeDirectly ( com.android.server.audio.AudioService$SetModeDeathHandler p0 ) {
/* .locals 2 */
/* .param p1, "currentModeHandler" # Lcom/android/server/audio/AudioService$SetModeDeathHandler; */
/* .line 1641 */
final String v0 = "AudioServiceStubImpl"; // const-string v0, "AudioServiceStubImpl"
final String v1 = "When ble is connected, getMode directly to avoid long waiting times."; // const-string v1, "When ble is connected, getMode directly to avoid long waiting times."
android.util.Log .v ( v0,v1 );
/* .line 1642 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 1643 */
v0 = (( com.android.server.audio.AudioService$SetModeDeathHandler ) p1 ).getMode ( ); // invoke-virtual {p1}, Lcom/android/server/audio/AudioService$SetModeDeathHandler;->getMode()I
/* .line 1645 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer getMusicVolumeStep ( Integer p0, java.lang.String p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "stream" # I */
/* .param p2, "callingPackage" # Ljava/lang/String; */
/* .param p3, "maxVolume" # I */
/* .line 393 */
v0 = (( com.android.server.audio.AudioServiceStubImpl ) p0 ).isSupportSteplessVolume ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/audio/AudioServiceStubImpl;->isSupportSteplessVolume(ILjava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 394 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "adjustStreamVolume(): SupportSteplessVolume, maxVolume = "; // const-string v1, "adjustStreamVolume(): SupportSteplessVolume, maxVolume = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AudioServiceStubImpl"; // const-string v1, "AudioServiceStubImpl"
android.util.Log .d ( v1,v0 );
/* .line 395 */
/* div-int/lit8 v0, p3, 0xf */
/* .line 397 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
public java.lang.String getNotificationUri ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "type" # Ljava/lang/String; */
/* .line 652 */
v0 = this.mAudioQueryWeatherService;
v0 = (( com.android.server.audio.AudioQueryWeatherService ) v0 ).getSunriseTimeHours ( ); // invoke-virtual {v0}, Lcom/android/server/audio/AudioQueryWeatherService;->getSunriseTimeHours()I
/* .line 653 */
/* .local v0, "SunriseTimeHours":I */
v1 = this.mAudioQueryWeatherService;
v1 = (( com.android.server.audio.AudioQueryWeatherService ) v1 ).getSunriseTimeMins ( ); // invoke-virtual {v1}, Lcom/android/server/audio/AudioQueryWeatherService;->getSunriseTimeMins()I
/* .line 654 */
/* .local v1, "SunriseTimeMins":I */
v2 = this.mAudioQueryWeatherService;
v2 = (( com.android.server.audio.AudioQueryWeatherService ) v2 ).getSunsetTimeHours ( ); // invoke-virtual {v2}, Lcom/android/server/audio/AudioQueryWeatherService;->getSunsetTimeHours()I
/* .line 655 */
/* .local v2, "SunsetTimeHours":I */
v3 = this.mAudioQueryWeatherService;
v3 = (( com.android.server.audio.AudioQueryWeatherService ) v3 ).getSunsetTimeMins ( ); // invoke-virtual {v3}, Lcom/android/server/audio/AudioQueryWeatherService;->getSunsetTimeMins()I
/* .line 656 */
/* .local v3, "SunsetTimeMins":I */
v4 = this.mAudioQueryWeatherService;
v4 = (( com.android.server.audio.AudioQueryWeatherService ) v4 ).getDefaultTimeZoneStatus ( ); // invoke-virtual {v4}, Lcom/android/server/audio/AudioQueryWeatherService;->getDefaultTimeZoneStatus()Z
android.media.AudioServiceInjector .setDefaultTimeZoneStatus ( v4 );
/* .line 657 */
android.media.AudioServiceInjector .setSunriseAndSunsetTime ( v0,v1,v2,v3 );
/* .line 658 */
v4 = this.mContext;
android.media.AudioServiceInjector .checkSunriseAndSunsetTimeUpdate ( v4 );
/* .line 659 */
android.media.AudioServiceInjector .getNotificationUri ( p1 );
} // .end method
public Integer getRingerMode ( android.content.Context p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "mode" # I */
/* .line 421 */
v0 = miui.util.AudioManagerHelper .getValidatedRingerMode ( p1,p2 );
/* .line 423 */
/* .local v0, "miuiMode":I */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "getRingerMode originMode"; // const-string v2, "getRingerMode originMode"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " destMode="; // const-string v2, " destMode="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "AudioServiceStubImpl"; // const-string v2, "AudioServiceStubImpl"
android.util.Log .d ( v2,v1 );
/* .line 424 */
} // .end method
public Integer getSuperIndex ( Integer p0, Integer p1, Integer p2, java.util.Set p3, android.content.Context p4 ) {
/* .locals 3 */
/* .param p1, "mStreamType" # I */
/* .param p2, "mIndexSuper" # I */
/* .param p3, "mIndexMax" # I */
/* .param p5, "context" # Landroid/content/Context; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(III", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;", */
/* "Landroid/content/Context;", */
/* ")I" */
/* } */
} // .end annotation
/* .line 1699 */
/* .local p4, "deviceSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
(( android.content.Context ) p5 ).getPackageManager ( ); // invoke-virtual {p5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
v1 = android.os.Binder .getCallingUid ( );
(( android.content.pm.PackageManager ) v0 ).getNameForUid ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;
/* .line 1701 */
/* .local v0, "pkg":Ljava/lang/String; */
int v1 = -1; // const/4 v1, -0x1
v1 = /* if-eq p2, v1, :cond_0 */
int v2 = 1; // const/4 v2, 0x1
/* if-ne v1, v2, :cond_0 */
int v1 = 2; // const/4 v1, 0x2
v1 = java.lang.Integer .valueOf ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1702 */
v1 = (( com.android.server.audio.AudioServiceStubImpl ) p0 ).isCtsVerifier ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/audio/AudioServiceStubImpl;->isCtsVerifier(Ljava/lang/String;)Z
/* if-nez v1, :cond_0 */
/* .line 1703 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "SuperVolume: getMxIndex: "; // const-string v2, "SuperVolume: getMxIndex: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ";mStreamType is "; // const-string v2, ";mStreamType is "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ";pkg is "; // const-string v2, ";pkg is "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "AudioServiceStubImpl"; // const-string v2, "AudioServiceStubImpl"
android.util.Log .d ( v2,v1 );
/* .line 1704 */
/* .line 1706 */
} // :cond_0
} // .end method
public void handleLowBattery ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "batteryPct" # I */
/* .param p2, "audioControlStatus" # I */
/* .line 2087 */
/* sget-boolean v0, Lcom/android/server/audio/feature/FeatureAdapter;->ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2088 */
v0 = this.mFeatureAdapter;
(( com.android.server.audio.feature.FeatureAdapter ) v0 ).handleLowBattery ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/audio/feature/FeatureAdapter;->handleLowBattery(II)V
/* .line 2090 */
} // :cond_0
return;
} // .end method
public void handleMicrophoneMuteChanged ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "muted" # Z */
/* .line 1615 */
/* sget-boolean v0, Lcom/android/server/audio/pad/PadAdapter;->ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1616 */
v0 = this.mPadAdapter;
(( com.android.server.audio.pad.PadAdapter ) v0 ).handleMicrophoneMuteChanged ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/audio/pad/PadAdapter;->handleMicrophoneMuteChanged(Z)V
/* .line 1618 */
} // :cond_0
return;
} // .end method
public void handleReceiveBtEventChanged ( android.content.Intent p0 ) {
/* .locals 3 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 1863 */
/* iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportedReportAudioRouteState:Z */
/* if-nez v0, :cond_0 */
/* .line 1864 */
return;
/* .line 1866 */
} // :cond_0
(( android.content.Intent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 1867 */
/* .local v0, "action":Ljava/lang/String; */
final String v1 = "android.bluetooth.headset.profile.action.ACTIVE_DEVICE_CHANGED"; // const-string v1, "android.bluetooth.headset.profile.action.ACTIVE_DEVICE_CHANGED"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 1868 */
final String v1 = "android.bluetooth.device.extra.DEVICE"; // const-string v1, "android.bluetooth.device.extra.DEVICE"
/* const-class v2, Landroid/bluetooth/BluetoothDevice; */
(( android.content.Intent ) p1 ).getParcelableExtra ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v1, Landroid/bluetooth/BluetoothDevice; */
/* .line 1870 */
/* .local v1, "btDevice":Landroid/bluetooth/BluetoothDevice; */
if ( v1 != null) { // if-eqz v1, :cond_1
int v2 = 2; // const/4 v2, 0x2
/* .line 1871 */
} // :cond_1
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* iput v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mHeadsetDeviceConnectedState:I */
/* .line 1872 */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1873 */
(( android.bluetooth.BluetoothDevice ) v1 ).getName ( ); // invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;
this.mBtName = v2;
/* .line 1874 */
} // :cond_2
/* iget v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mA2dpDeviceConnectedState:I */
/* if-nez v2, :cond_4 */
/* .line 1875 */
final String v2 = ""; // const-string v2, ""
this.mBtName = v2;
/* .line 1877 */
} // .end local v1 # "btDevice":Landroid/bluetooth/BluetoothDevice;
} // :cond_3
final String v1 = "android.bluetooth.headset.profile.action.AUDIO_STATE_CHANGED"; // const-string v1, "android.bluetooth.headset.profile.action.AUDIO_STATE_CHANGED"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 1878 */
final String v1 = "android.bluetooth.profile.extra.STATE"; // const-string v1, "android.bluetooth.profile.extra.STATE"
int v2 = -1; // const/4 v2, -0x1
v1 = (( android.content.Intent ) p1 ).getIntExtra ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* iput v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mScoBtState:I */
/* .line 1877 */
} // :cond_4
} // :goto_1
/* nop */
/* .line 1880 */
} // :goto_2
return;
} // .end method
public void handleRecordEventUpdate ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "audioSource" # I */
/* .line 1622 */
/* sget-boolean v0, Lcom/android/server/audio/pad/PadAdapter;->ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1623 */
v0 = this.mPadAdapter;
(( com.android.server.audio.pad.PadAdapter ) v0 ).handleRecordEventUpdate ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/audio/pad/PadAdapter;->handleRecordEventUpdate(I)V
/* .line 1625 */
} // :cond_0
return;
} // .end method
public void handleSpeakerChanged ( android.content.Context p0, Integer p1, Boolean p2 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "pid" # I */
/* .param p3, "speakerOn" # Z */
/* .line 1247 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "handleSpeakerChanged audiomode "; // const-string v1, "handleSpeakerChanged audiomode "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioMode:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AudioServiceStubImpl"; // const-string v1, "AudioServiceStubImpl"
android.util.Log .d ( v1,v0 );
/* .line 1248 */
/* iget v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioMode:I */
int v1 = 2; // const/4 v1, 0x2
/* if-eq v0, v1, :cond_0 */
int v1 = 3; // const/4 v1, 0x3
/* if-ne v0, v1, :cond_1 */
/* .line 1249 */
} // :cond_0
v0 = android.os.Binder .getCallingUid ( );
android.media.AudioServiceInjector .handleSpeakerChanged ( p2,p3,v0 );
/* .line 1251 */
} // :cond_1
return;
} // .end method
public void handleWaveformInfoTracked ( android.os.CombinedVibration p0, java.lang.String p1, android.os.VibrationAttributes p2 ) {
/* .locals 10 */
/* .param p1, "effect" # Landroid/os/CombinedVibration; */
/* .param p2, "opPkg" # Ljava/lang/String; */
/* .param p3, "attrs" # Landroid/os/VibrationAttributes; */
/* .line 1531 */
v0 = this.mMiuiXlog;
/* const-string/jumbo v1, "waveform_info" */
int v2 = 0; // const/4 v2, 0x0
v0 = (( android.media.MiuiXlog ) v0 ).checkXlogPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/media/MiuiXlog;->checkXlogPermission(Ljava/lang/String;I)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1532 */
/* instance-of v0, p1, Landroid/os/CombinedVibration$Mono; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1533 */
/* move-object v0, p1 */
/* check-cast v0, Landroid/os/CombinedVibration$Mono; */
(( android.os.CombinedVibration$Mono ) v0 ).getEffect ( ); // invoke-virtual {v0}, Landroid/os/CombinedVibration$Mono;->getEffect()Landroid/os/VibrationEffect;
/* .line 1534 */
/* .local v0, "vibEffect":Landroid/os/VibrationEffect; */
/* move-object v1, v0 */
/* check-cast v1, Landroid/os/VibrationEffect$Composed; */
/* .line 1535 */
/* .local v1, "composed":Landroid/os/VibrationEffect$Composed; */
/* new-instance v3, Ljava/util/ArrayList; */
(( android.os.VibrationEffect$Composed ) v1 ).getSegments ( ); // invoke-virtual {v1}, Landroid/os/VibrationEffect$Composed;->getSegments()Ljava/util/List;
/* invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 1536 */
/* .local v3, "segments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/vibrator/VibrationEffectSegment;>;" */
(( android.os.VibrationEffect$Composed ) v1 ).getSegments ( ); // invoke-virtual {v1}, Landroid/os/VibrationEffect$Composed;->getSegments()Ljava/util/List;
/* check-cast v4, Landroid/os/vibrator/VibrationEffectSegment; */
/* .line 1537 */
/* .local v4, "segment":Landroid/os/vibrator/VibrationEffectSegment; */
v5 = (( java.util.ArrayList ) v3 ).size ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
/* .line 1538 */
/* .local v5, "segmentCount":I */
/* instance-of v6, v4, Landroid/os/vibrator/StepSegment; */
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 1539 */
/* new-instance v6, Lcom/android/server/audio/MQSUtils$WaveformInfo; */
/* invoke-direct {v6}, Lcom/android/server/audio/MQSUtils$WaveformInfo;-><init>()V */
/* .line 1540 */
/* .local v6, "vibratorInfo":Lcom/android/server/audio/MQSUtils$WaveformInfo; */
this.opPkg = p2;
/* .line 1541 */
(( android.os.VibrationAttributes ) p3 ).toString ( ); // invoke-virtual {p3}, Landroid/os/VibrationAttributes;->toString()Ljava/lang/String;
this.attrs = v7;
/* .line 1542 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "WaveformInfo segmentCount: "; // const-string v8, "WaveformInfo segmentCount: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v8 = "AudioServiceStubImpl"; // const-string v8, "AudioServiceStubImpl"
android.util.Log .d ( v8,v7 );
/* .line 1543 */
final String v7 = ", "; // const-string v7, ", "
int v8 = 4; // const/4 v8, 0x4
/* if-gt v5, v8, :cond_0 */
/* .line 1544 */
(( java.util.ArrayList ) v3 ).stream ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->stream()Ljava/util/stream/Stream;
/* new-instance v8, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda2; */
/* invoke-direct {v8}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda2;-><init>()V */
/* .line 1545 */
/* new-instance v8, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda3; */
/* invoke-direct {v8}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda3;-><init>()V */
/* .line 1546 */
java.util.stream.Collectors .toCollection ( v8 );
/* check-cast v2, Ljava/util/ArrayList; */
/* .line 1547 */
/* .local v2, "segmentsToString":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
java.lang.String .join ( v7,v2 );
/* .line 1548 */
/* .local v7, "result":Ljava/lang/String; */
this.effect = v7;
/* .line 1549 */
} // .end local v2 # "segmentsToString":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
} // .end local v7 # "result":Ljava/lang/String;
/* .line 1550 */
} // :cond_0
/* new-instance v9, Ljava/util/ArrayList; */
/* .line 1551 */
(( java.util.ArrayList ) v3 ).subList ( v2, v8 ); // invoke-virtual {v3, v2, v8}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;
/* invoke-direct {v9, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* move-object v2, v9 */
/* .line 1552 */
/* .local v2, "newSegments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/vibrator/VibrationEffectSegment;>;" */
(( java.util.ArrayList ) v2 ).stream ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->stream()Ljava/util/stream/Stream;
/* new-instance v9, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda4; */
/* invoke-direct {v9}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda4;-><init>()V */
/* .line 1553 */
/* new-instance v9, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda3; */
/* invoke-direct {v9}, Lcom/android/server/audio/AudioServiceStubImpl$$ExternalSyntheticLambda3;-><init>()V */
/* .line 1554 */
java.util.stream.Collectors .toCollection ( v9 );
/* check-cast v8, Ljava/util/ArrayList; */
/* .line 1555 */
/* .local v8, "newSegmentsToString":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
java.lang.String .join ( v7,v8 );
/* .line 1556 */
/* .local v7, "newResult":Ljava/lang/String; */
this.effect = v7;
/* .line 1558 */
} // .end local v2 # "newSegments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/vibrator/VibrationEffectSegment;>;"
} // .end local v7 # "newResult":Ljava/lang/String;
} // .end local v8 # "newSegmentsToString":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
} // :goto_0
v2 = this.mHandler;
int v7 = 3; // const/4 v7, 0x3
(( android.os.Handler ) v2 ).obtainMessage ( v7, v6 ); // invoke-virtual {v2, v7, v6}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 1559 */
(( android.os.Message ) v2 ).sendToTarget ( ); // invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V
/* .line 1563 */
} // .end local v0 # "vibEffect":Landroid/os/VibrationEffect;
} // .end local v1 # "composed":Landroid/os/VibrationEffect$Composed;
} // .end local v3 # "segments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/vibrator/VibrationEffectSegment;>;"
} // .end local v4 # "segment":Landroid/os/vibrator/VibrationEffectSegment;
} // .end local v5 # "segmentCount":I
} // .end local v6 # "vibratorInfo":Lcom/android/server/audio/MQSUtils$WaveformInfo;
} // :cond_1
return;
} // .end method
public void init ( android.content.Context p0, com.android.server.audio.AudioService p1 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "service" # Lcom/android/server/audio/AudioService; */
/* .line 1437 */
final String v0 = "AudioServiceStubImpl"; // const-string v0, "AudioServiceStubImpl"
final String v1 = "initContext ..."; // const-string v1, "initContext ..."
android.util.Log .d ( v0,v1 );
/* .line 1438 */
this.mContext = p1;
/* .line 1439 */
final String v0 = "audio"; // const-string v0, "audio"
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/media/AudioManager; */
this.mAudioManager = v0;
/* .line 1440 */
this.mAudioService = p2;
/* .line 1441 */
/* new-instance v0, Lcom/android/server/audio/MQSUtils; */
/* invoke-direct {v0, p1}, Lcom/android/server/audio/MQSUtils;-><init>(Landroid/content/Context;)V */
this.mMqsUtils = v0;
/* .line 1443 */
/* new-instance v0, Lcom/android/server/audio/MiAudioServiceMTK; */
/* invoke-direct {v0, p1, p2}, Lcom/android/server/audio/MiAudioServiceMTK;-><init>(Landroid/content/Context;Lcom/android/server/audio/AudioService;)V */
this.mMiAudioServiceMTK = v0;
/* .line 1445 */
v0 = this.mMiuiXlog;
final String v1 = "check_audio_route_for_bluetooth"; // const-string v1, "check_audio_route_for_bluetooth"
int v2 = 0; // const/4 v2, 0x0
v0 = (( android.media.MiuiXlog ) v0 ).checkXlogPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/media/MiuiXlog;->checkXlogPermission(Ljava/lang/String;I)Z
/* iput-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportedReportAudioRouteState:Z */
/* .line 1447 */
/* if-lez v0, :cond_0 */
/* .line 1448 */
v1 = this.mHandler;
int v2 = 6; // const/4 v2, 0x6
(( android.os.Handler ) v1 ).obtainMessage ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* int-to-long v3, v0 */
(( android.os.Handler ) v1 ).sendMessageDelayed ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 1453 */
} // :cond_0
/* sget-boolean v0, Lcom/android/server/audio/pad/PadAdapter;->ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1454 */
/* new-instance v0, Lcom/android/server/audio/pad/PadAdapter; */
/* invoke-direct {v0, p1}, Lcom/android/server/audio/pad/PadAdapter;-><init>(Landroid/content/Context;)V */
this.mPadAdapter = v0;
/* .line 1457 */
} // :cond_1
/* sget-boolean v0, Lcom/android/server/audio/foldable/FoldableAdapter;->ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1458 */
/* new-instance v0, Lcom/android/server/audio/foldable/FoldableAdapter; */
v1 = this.mWorkerThread;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p1, v1}, Lcom/android/server/audio/foldable/FoldableAdapter;-><init>(Landroid/content/Context;Landroid/os/Looper;)V */
this.mFoldableAdapter = v0;
/* .line 1461 */
} // :cond_2
/* sget-boolean v0, Lcom/android/server/audio/feature/FeatureAdapter;->ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 1462 */
/* new-instance v0, Lcom/android/server/audio/feature/FeatureAdapter; */
/* invoke-direct {v0, p1}, Lcom/android/server/audio/feature/FeatureAdapter;-><init>(Landroid/content/Context;)V */
this.mFeatureAdapter = v0;
/* .line 1465 */
} // :cond_3
/* iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportFWAudioEffectCenter:Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 1466 */
(( com.android.server.audio.AudioServiceStubImpl ) p0 ).initEffectChangeBroadcastReceiver ( ); // invoke-virtual {p0}, Lcom/android/server/audio/AudioServiceStubImpl;->initEffectChangeBroadcastReceiver()V
/* .line 1468 */
} // :cond_4
return;
} // .end method
public void initAudioImplStatus ( com.android.server.audio.AudioService p0, com.android.server.audio.AudioDeviceBroker p1, com.android.server.audio.BtHelper p2 ) {
/* .locals 0 */
/* .param p1, "service" # Lcom/android/server/audio/AudioService; */
/* .param p2, "deviceBroker" # Lcom/android/server/audio/AudioDeviceBroker; */
/* .param p3, "btHelper" # Lcom/android/server/audio/BtHelper; */
/* .line 1430 */
this.mAudioService = p1;
/* .line 1431 */
this.mDeviceBroker = p2;
/* .line 1432 */
this.mBtHelper = p3;
/* .line 1433 */
return;
} // .end method
public void initEffectChangeBroadcastReceiver ( ) {
/* .locals 3 */
/* .line 1490 */
/* new-instance v0, Lcom/android/server/audio/AudioServiceStubImpl$EffectChangeBroadcastReceiver; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/android/server/audio/AudioServiceStubImpl$EffectChangeBroadcastReceiver;-><init>(Lcom/android/server/audio/AudioServiceStubImpl;Lcom/android/server/audio/AudioServiceStubImpl$EffectChangeBroadcastReceiver-IA;)V */
this.mEffectChangeBroadcastReceiver = v0;
/* .line 1491 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 1492 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v1 = "miui.intent.action.ACTION_AUDIO_EFFECT_CHANGED"; // const-string v1, "miui.intent.action.ACTION_AUDIO_EFFECT_CHANGED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 1493 */
v1 = this.mContext;
v2 = this.mEffectChangeBroadcastReceiver;
(( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 1494 */
return;
} // .end method
public Boolean insertPlaybackMsToHealth ( android.content.Context p0 ) {
/* .locals 11 */
/* .param p1, "cx" # Landroid/content/Context; */
/* .line 1003 */
/* iget-wide v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mStartMs:J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget-wide v6, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mContinuousMs:J */
/* cmp-long v0, v6, v2 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1004 */
/* add-long v8, v6, v6 */
/* .line 1005 */
/* iget v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mHighLevel:I */
int v2 = 0; // const/4 v2, 0x0
/* if-ne v0, v1, :cond_0 */
/* move v10, v1 */
} // :cond_0
/* move v10, v2 */
/* .line 1004 */
} // :goto_0
/* move-object v4, p0 */
/* move-object v5, p1 */
/* invoke-direct/range {v4 ..v10}, Lcom/android/server/audio/AudioServiceStubImpl;->transportDataToService(Landroid/content/Context;JJZ)Landroid/content/ComponentName; */
/* .line 1006 */
/* .local v0, "componentName":Landroid/content/ComponentName; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1007 */
final String v3 = "readPlaybackMsSettings"; // const-string v3, "readPlaybackMsSettings"
(( com.android.server.audio.AudioServiceStubImpl ) p0 ).persistPlaybackMsToSettings ( p1, v1, v2, v3 ); // invoke-virtual {p0, p1, v1, v2, v3}, Lcom/android/server/audio/AudioServiceStubImpl;->persistPlaybackMsToSettings(Landroid/content/Context;ZILjava/lang/String;)V
/* .line 1009 */
/* .line 1011 */
} // :cond_1
/* .line 1014 */
} // .end local v0 # "componentName":Landroid/content/ComponentName;
} // :cond_2
} // .end method
public Boolean isAdspSpatializerAvailable ( ) {
/* .locals 2 */
/* .line 2124 */
v0 = this.mContext;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2125 */
(( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
v1 = android.os.Binder .getCallingUid ( );
(( android.content.pm.PackageManager ) v0 ).getNameForUid ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;
/* .line 2126 */
/* .local v0, "packageName":Ljava/lang/String; */
final String v1 = "com.tencent.qqmusic"; // const-string v1, "com.tencent.qqmusic"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 2127 */
/* iget-boolean v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsAdspSpatializerAvailable:Z */
/* .line 2130 */
} // .end local v0 # "packageName":Ljava/lang/String;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isAdspSpatializerEnable ( ) {
/* .locals 2 */
/* .line 2135 */
v0 = (( com.android.server.audio.AudioServiceStubImpl ) p0 ).isAdspSpatializerAvailable ( ); // invoke-virtual {p0}, Lcom/android/server/audio/AudioServiceStubImpl;->isAdspSpatializerAvailable()Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2136 */
final String v0 = "persist.vendor.audio.dolbysurround.enable"; // const-string v0, "persist.vendor.audio.dolbysurround.enable"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2137 */
int v0 = 1; // const/4 v0, 0x1
/* .line 2139 */
} // :cond_0
/* .line 2142 */
} // :cond_1
} // .end method
public Boolean isAudioPlaybackTriggerSupported ( ) {
/* .locals 1 */
/* .line 933 */
/* sget-boolean v0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsAudioPlaybackTriggerSupported:Z */
} // .end method
public Boolean isCtsVerifier ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "callingPackage" # Ljava/lang/String; */
/* .line 892 */
int v0 = 1; // const/4 v0, 0x1
if ( p1 != null) { // if-eqz p1, :cond_1
final String v1 = "com.android.cts"; // const-string v1, "com.android.cts"
v1 = (( java.lang.String ) p1 ).startsWith ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* if-nez v1, :cond_0 */
/* .line 893 */
final String v1 = "android.media.cts"; // const-string v1, "android.media.cts"
v1 = (( java.lang.String ) v1 ).equals ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 894 */
} // :cond_0
/* .line 897 */
} // :cond_1
if ( p1 != null) { // if-eqz p1, :cond_2
final String v1 = "com.google.android.gts"; // const-string v1, "com.google.android.gts"
v1 = (( java.lang.String ) p1 ).startsWith ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 898 */
/* .line 900 */
} // :cond_2
if ( p1 != null) { // if-eqz p1, :cond_3
final String v1 = "android.media.audio.cts"; // const-string v1, "android.media.audio.cts"
v1 = (( java.lang.String ) p1 ).startsWith ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 901 */
/* .line 903 */
} // :cond_3
int v0 = 0; // const/4 v0, 0x0
} // .end method
Boolean isSoundAssistantFunction ( android.net.Uri p0 ) {
/* .locals 1 */
/* .param p1, "uri" # Landroid/net/Uri; */
/* .line 611 */
final String v0 = "key_ignore_music_focus_req"; // const-string v0, "key_ignore_music_focus_req"
android.provider.Settings$Global .getUriFor ( v0 );
v0 = (( android.net.Uri ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 613 */
/* const-string/jumbo v0, "sound_assist_key" */
android.provider.Settings$Global .getUriFor ( v0 );
v0 = (( android.net.Uri ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 611 */
} // :goto_1
} // .end method
public Boolean isStreamVolumeInt ( Integer p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "mode" # I */
/* .param p2, "isLeConnectedForDeviceBroker" # Z */
/* .line 1636 */
int v0 = 2; // const/4 v0, 0x2
/* if-eq p1, v0, :cond_0 */
int v0 = 3; // const/4 v0, 0x3
/* if-ne p1, v0, :cond_1 */
} // :cond_0
if ( p2 != null) { // if-eqz p2, :cond_1
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isSupportSteplessVolume ( Integer p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "stream" # I */
/* .param p2, "callingPackage" # Ljava/lang/String; */
/* .line 389 */
int v0 = 3; // const/4 v0, 0x3
/* if-ne p1, v0, :cond_0 */
v0 = (( com.android.server.audio.AudioServiceStubImpl ) p0 ).isCtsVerifier ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/audio/AudioServiceStubImpl;->isCtsVerifier(Ljava/lang/String;)Z
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean musicVolumeAdjustmentAllowed ( Integer p0, Integer p1, android.content.ContentResolver p2 ) {
/* .locals 1 */
/* .param p1, "zenMode" # I */
/* .param p2, "streamAlias" # I */
/* .param p3, "cr" # Landroid/content/ContentResolver; */
/* .line 429 */
int v0 = 3; // const/4 v0, 0x3
/* if-ne p2, v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
/* if-ne p1, v0, :cond_0 */
/* .line 431 */
v0 = /* invoke-direct {p0, p3}, Lcom/android/server/audio/AudioServiceStubImpl;->isMuteMusicFromMIUI(Landroid/content/ContentResolver;)Z */
/* .line 433 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void notifyBtStateToDolbyEffectController ( android.content.Context p0, com.android.server.audio.AudioDeviceBroker$BtDeviceInfo p1 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "btInfo" # Lcom/android/server/audio/AudioDeviceBroker$BtDeviceInfo; */
/* .line 1322 */
/* iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportedDolbyEffectControl:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1323 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 1324 */
/* .local v0, "bundle":Landroid/os/Bundle; */
v1 = this.mDevice;
(( android.bluetooth.BluetoothDevice ) v1 ).getAddress ( ); // invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;
final String v2 = "device"; // const-string v2, "device"
(( android.os.Bundle ) v0 ).putString ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1325 */
/* iget v1, p2, Lcom/android/server/audio/AudioDeviceBroker$BtDeviceInfo;->mProfile:I */
android.bluetooth.BluetoothProfile .getProfileName ( v1 );
final String v2 = "profile"; // const-string v2, "profile"
(( android.os.Bundle ) v0 ).putString ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1326 */
/* iget v1, p2, Lcom/android/server/audio/AudioDeviceBroker$BtDeviceInfo;->mState:I */
android.bluetooth.BluetoothProfile .getConnectionStateName ( v1 );
/* const-string/jumbo v2, "state" */
(( android.os.Bundle ) v0 ).putString ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1327 */
com.android.server.audio.dolbyeffect.DolbyEffectController .getInstance ( p1 );
(( com.android.server.audio.dolbyeffect.DolbyEffectController ) v1 ).btStateChangedFromDeviceBroker ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->btStateChangedFromDeviceBroker(Landroid/os/Bundle;)V
/* .line 1329 */
} // .end local v0 # "bundle":Landroid/os/Bundle;
} // :cond_0
return;
} // .end method
public void notifyVolumeChangedToDolbyEffectController ( android.content.Context p0, Integer p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "stream" # I */
/* .param p3, "newVolumeIndex" # I */
/* .line 1712 */
/* iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportedDolbyEffectControl:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 3; // const/4 v0, 0x3
/* if-ne p2, v0, :cond_0 */
/* .line 1713 */
com.android.server.audio.dolbyeffect.DolbyEffectController .getInstance ( p1 );
(( com.android.server.audio.dolbyeffect.DolbyEffectController ) v0 ).receiveVolumeChanged ( p3 ); // invoke-virtual {v0, p3}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->receiveVolumeChanged(I)V
/* .line 1715 */
} // :cond_0
return;
} // .end method
public void onAudioServerDied ( ) {
/* .locals 2 */
/* .line 1472 */
final String v0 = "AudioServiceStubImpl"; // const-string v0, "AudioServiceStubImpl"
final String v1 = "onAudioServerDied ..."; // const-string v1, "onAudioServerDied ..."
android.util.Log .d ( v0,v1 );
/* .line 1473 */
/* sget-boolean v0, Lcom/android/server/audio/foldable/FoldableAdapter;->ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1474 */
v0 = this.mFoldableAdapter;
(( com.android.server.audio.foldable.FoldableAdapter ) v0 ).onAudioServerDied ( ); // invoke-virtual {v0}, Lcom/android/server/audio/foldable/FoldableAdapter;->onAudioServerDied()V
/* .line 1476 */
} // :cond_0
return;
} // .end method
public void onCheckAudioRoute ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "eventSource" # Ljava/lang/String; */
/* .line 1919 */
/* iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportedReportAudioRouteState:Z */
/* if-nez v0, :cond_0 */
/* .line 1920 */
return;
/* .line 1922 */
} // :cond_0
v0 = android.text.TextUtils .isEmpty ( p1 );
final String v1 = "AudioServiceStubImpl"; // const-string v1, "AudioServiceStubImpl"
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1923 */
final String v0 = "onCheckAudioRoute: invalid eventSource"; // const-string v0, "onCheckAudioRoute: invalid eventSource"
android.util.Log .d ( v1,v0 );
/* .line 1924 */
return;
/* .line 1927 */
} // :cond_1
/* iget v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mA2dpDeviceConnectedState:I */
int v2 = 2; // const/4 v2, 0x2
/* if-eq v0, v2, :cond_2 */
/* iget v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mHeadsetDeviceConnectedState:I */
/* if-eq v0, v2, :cond_2 */
/* .line 1929 */
final String v0 = "onCheckAudioRoute: no active bt device"; // const-string v0, "onCheckAudioRoute: no active bt device"
android.util.Log .d ( v1,v0 );
/* .line 1930 */
return;
/* .line 1933 */
} // :cond_2
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "onCheckAudioRoute: "; // const-string v2, "onCheckAudioRoute: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v0 );
/* .line 1934 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1935 */
/* .local v0, "isReport":Z */
int v2 = -1; // const/4 v2, -0x1
/* .line 1937 */
/* .local v2, "errorType":I */
/* iget v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mA2dpDeviceConnectedState:I */
/* if-nez v3, :cond_3 */
/* .line 1938 */
int v2 = 4; // const/4 v2, 0x4
/* .line 1939 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1941 */
} // :cond_3
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
/* .line 1942 */
/* .local v3, "builderSource":Ljava/lang/StringBuilder; */
/* if-nez v0, :cond_8 */
/* iget v4, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioMode:I */
int v5 = 3; // const/4 v5, 0x3
/* if-ne v4, v5, :cond_8 */
/* .line 1943 */
v4 = /* invoke-direct {p0}, Lcom/android/server/audio/AudioServiceStubImpl;->checkAudioRouteForBtConnected()Z */
/* if-nez v4, :cond_8 */
/* .line 1944 */
v4 = this.mPreferredCommunicationDevice;
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 1945 */
v4 = (( android.media.AudioDeviceAttributes ) v4 ).getType ( ); // invoke-virtual {v4}, Landroid/media/AudioDeviceAttributes;->getType()I
int v5 = 7; // const/4 v5, 0x7
/* if-ne v4, v5, :cond_4 */
int v4 = 1; // const/4 v4, 0x1
} // :cond_4
int v4 = 0; // const/4 v4, 0x0
/* .line 1947 */
/* .local v4, "audioBaseScoState":Z */
} // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 1948 */
int v2 = 7; // const/4 v2, 0x7
/* .line 1949 */
} // :cond_5
v5 = /* invoke-direct {p0}, Lcom/android/server/audio/AudioServiceStubImpl;->isSetScoCommunicationDevice()Z */
/* if-nez v5, :cond_6 */
/* .line 1950 */
int v2 = 3; // const/4 v2, 0x3
/* .line 1951 */
final String v5 = " app don\'t request sco"; // const-string v5, " app don\'t request sco"
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1952 */
} // :cond_6
v5 = this.mDeviceBroker;
v5 = (( com.android.server.audio.AudioDeviceBroker ) v5 ).isBluetoothScoRequested ( ); // invoke-virtual {v5}, Lcom/android/server/audio/AudioDeviceBroker;->isBluetoothScoRequested()Z
/* if-nez v5, :cond_7 */
/* .line 1953 */
int v2 = 3; // const/4 v2, 0x3
/* .line 1954 */
final String v5 = " app request sco, but stop sco"; // const-string v5, " app request sco, but stop sco"
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1956 */
} // :cond_7
int v2 = 0; // const/4 v2, 0x0
/* .line 1958 */
} // :goto_1
int v0 = 1; // const/4 v0, 0x1
/* .line 1960 */
} // .end local v4 # "audioBaseScoState":Z
} // :cond_8
if ( v0 != null) { // if-eqz v0, :cond_9
/* .line 1961 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "[TF-BT] onCheckAudioRoute: errorType="; // const-string v5, "[TF-BT] onCheckAudioRoute: errorType="
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v4 );
/* .line 1962 */
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = ""; // const-string v4, ""
final String v5 = "maybeError"; // const-string v5, "maybeError"
(( com.android.server.audio.AudioServiceStubImpl ) p0 ).reportAbnormalAudioStatus ( v2, v1, v4, v5 ); // invoke-virtual {p0, v2, v1, v4, v5}, Lcom/android/server/audio/AudioServiceStubImpl;->reportAbnormalAudioStatus(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
/* .line 1965 */
} // :cond_9
return;
} // .end method
public Boolean onCheckMusicPlaybackContinuous ( android.content.Context p0, Integer p1, Boolean p2, java.util.Set p3 ) {
/* .locals 21 */
/* .param p1, "cx" # Landroid/content/Context; */
/* .param p2, "device" # I */
/* .param p3, "isHigh" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/content/Context;", */
/* "IZ", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 1049 */
/* .local p4, "safeMediaVolumeDevices":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
/* move-object/from16 v7, p0 */
/* move-object/from16 v8, p1 */
/* move/from16 v9, p2 */
/* move/from16 v10, p3 */
v11 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/audio/AudioServiceStubImpl;->getBluetoothHeadset()Z */
/* .line 1050 */
/* .local v11, "isBluetoothHeadset":Z */
int v12 = 0; // const/4 v12, 0x0
/* .line 1051 */
/* .local v12, "needQueue":Z */
/* const/16 v0, 0x80 */
int v14 = 0; // const/4 v14, 0x0
/* if-eq v9, v0, :cond_0 */
/* const/16 v0, 0x100 */
/* if-ne v9, v0, :cond_1 */
} // :cond_0
/* if-nez v11, :cond_1 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
/* move v0, v14 */
} // :goto_0
/* move v15, v0 */
/* .line 1052 */
/* .local v15, "isBluetoothspeaker":Z */
/* invoke-static/range {p2 ..p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer; */
v0 = /* move-object/from16 v6, p4 */
/* const-wide/16 v4, 0x0 */
final String v1 = "AudioServiceStubImpl"; // const-string v1, "AudioServiceStubImpl"
if ( v0 != null) { // if-eqz v0, :cond_7
if ( v15 != null) { // if-eqz v15, :cond_2
/* move-wide v13, v4 */
/* goto/16 :goto_2 */
/* .line 1064 */
} // :cond_2
int v0 = 3; // const/4 v0, 0x3
v2 = android.media.AudioSystem .isStreamActive ( v0,v14 );
/* const-wide/32 v16, 0xea60 */
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 1065 */
/* iget-wide v2, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMs:J */
/* add-long v2, v2, v16 */
/* iput-wide v2, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMs:J */
/* .line 1066 */
/* iget-wide v2, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMsTotal:J */
/* add-long v2, v2, v16 */
/* iput-wide v2, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMsTotal:J */
/* .line 1067 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "music isActive ,start loop ="; // const-string v2, "music isActive ,start loop ="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMs:J */
(( java.lang.StringBuilder ) v0 ).append ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
/* const-string/jumbo v2, "\uff0cMUSIC_ACTIVE_CONTINUOUS_MS_MAX = " */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const v2, 0x36ee80 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v0 );
/* .line 1069 */
/* iget-wide v2, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMs:J */
/* const-wide/32 v18, 0x36ee80 */
/* cmp-long v0, v2, v18 */
/* if-ltz v0, :cond_3 */
/* .line 1070 */
final String v0 = "music isActive max ,post warning dialog"; // const-string v0, "music isActive max ,post warning dialog"
android.util.Log .d ( v1,v0 );
/* .line 1071 */
/* iput-wide v4, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMs:J */
/* .line 1072 */
java.time.LocalDate .now ( );
/* invoke-direct {v7, v8, v0}, Lcom/android/server/audio/AudioServiceStubImpl;->persistNotificationDateToSettings(Landroid/content/Context;Ljava/time/LocalDate;)V */
/* .line 1076 */
} // :cond_3
/* iget-wide v2, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMsTotal:J */
/* cmp-long v0, v2, v16 */
/* if-ltz v0, :cond_4 */
/* .line 1077 */
final String v0 = "need to report hearing data"; // const-string v0, "need to report hearing data"
android.util.Log .d ( v1,v0 );
/* .line 1078 */
/* iget-wide v2, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mPlaybackStartTime:J */
/* iget-wide v0, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMsTotal:J */
/* add-long v16, v2, v0 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
/* move-wide v13, v4 */
/* move-wide/from16 v4, v16 */
/* move/from16 v6, p3 */
/* invoke-direct/range {v0 ..v6}, Lcom/android/server/audio/AudioServiceStubImpl;->transportDataToService(Landroid/content/Context;JJZ)Landroid/content/ComponentName; */
/* .line 1080 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mPlaybackStartTime:J */
/* .line 1081 */
/* iput-wide v13, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMsTotal:J */
/* .line 1083 */
} // :cond_4
/* nop */
/* .line 1084 */
/* nop */
/* .line 1083 */
final String v0 = "music isActive ,start loop"; // const-string v0, "music isActive ,start loop"
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.audio.AudioServiceStubImpl ) v7 ).persistPlaybackMsToSettings ( v8, v1, v10, v0 ); // invoke-virtual {v7, v8, v1, v10, v0}, Lcom/android/server/audio/AudioServiceStubImpl;->persistPlaybackMsToSettings(Landroid/content/Context;ZILjava/lang/String;)V
/* .line 1085 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1087 */
} // :cond_5
/* move-wide v13, v4 */
/* const/16 v2, 0x7530 */
v20 = android.media.AudioSystem .isStreamActive ( v0,v2 );
/* .line 1089 */
/* .local v20, "isRencentActive":Z */
if ( v20 != null) { // if-eqz v20, :cond_6
/* .line 1090 */
/* iget-wide v2, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMs:J */
/* add-long v2, v2, v16 */
/* iput-wide v2, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMs:J */
/* .line 1091 */
/* iget-wide v2, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMsTotal:J */
/* add-long v2, v2, v16 */
/* iput-wide v2, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMsTotal:J */
/* .line 1092 */
final String v0 = "isRencentActive true,need retry again"; // const-string v0, "isRencentActive true,need retry again"
android.util.Log .d ( v1,v0 );
/* .line 1093 */
final String v0 = "isRencentActive true"; // const-string v0, "isRencentActive true"
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.audio.AudioServiceStubImpl ) v7 ).persistPlaybackMsToSettings ( v8, v1, v10, v0 ); // invoke-virtual {v7, v8, v1, v10, v0}, Lcom/android/server/audio/AudioServiceStubImpl;->persistPlaybackMsToSettings(Landroid/content/Context;ZILjava/lang/String;)V
/* .line 1095 */
int v0 = 1; // const/4 v0, 0x1
} // .end local v12 # "needQueue":Z
/* .local v0, "needQueue":Z */
/* .line 1097 */
} // .end local v0 # "needQueue":Z
/* .restart local v12 # "needQueue":Z */
} // :cond_6
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "isRencentActive false,reset time "; // const-string v2, "isRencentActive false,reset time "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMs:J */
(( java.lang.StringBuilder ) v0 ).append ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v0 );
/* .line 1099 */
int v0 = 0; // const/4 v0, 0x0
(( com.android.server.audio.AudioServiceStubImpl ) v7 ).updatePlaybackTime ( v0 ); // invoke-virtual {v7, v0}, Lcom/android/server/audio/AudioServiceStubImpl;->updatePlaybackTime(Z)V
/* .line 1100 */
/* iget-wide v2, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mPlaybackStartTime:J */
/* iget-wide v0, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMsTotal:J */
/* add-long v4, v2, v0 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
/* move/from16 v6, p3 */
/* invoke-direct/range {v0 ..v6}, Lcom/android/server/audio/AudioServiceStubImpl;->transportDataToService(Landroid/content/Context;JJZ)Landroid/content/ComponentName; */
/* .line 1102 */
final String v0 = "isRencentActive false"; // const-string v0, "isRencentActive false"
int v1 = 1; // const/4 v1, 0x1
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.audio.AudioServiceStubImpl ) v7 ).persistPlaybackMsToSettings ( v8, v1, v2, v0 ); // invoke-virtual {v7, v8, v1, v2, v0}, Lcom/android/server/audio/AudioServiceStubImpl;->persistPlaybackMsToSettings(Landroid/content/Context;ZILjava/lang/String;)V
/* .line 1104 */
/* iput-wide v13, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMs:J */
/* .line 1105 */
/* iput-wide v13, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMsTotal:J */
/* .line 1106 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1108 */
} // .end local v12 # "needQueue":Z
/* .restart local v0 # "needQueue":Z */
} // :goto_1
/* .line 1052 */
} // .end local v0 # "needQueue":Z
} // .end local v20 # "isRencentActive":Z
/* .restart local v12 # "needQueue":Z */
} // :cond_7
/* move-wide v13, v4 */
/* .line 1054 */
} // :goto_2
final String v0 = "device is not support,reset time calculation "; // const-string v0, "device is not support,reset time calculation "
android.util.Log .d ( v1,v0 );
/* .line 1055 */
int v0 = 0; // const/4 v0, 0x0
(( com.android.server.audio.AudioServiceStubImpl ) v7 ).updatePlaybackTime ( v0 ); // invoke-virtual {v7, v0}, Lcom/android/server/audio/AudioServiceStubImpl;->updatePlaybackTime(Z)V
/* .line 1056 */
/* iget-wide v2, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mPlaybackStartTime:J */
/* iget-wide v0, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMsTotal:J */
/* add-long v4, v2, v0 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
/* move/from16 v6, p3 */
/* invoke-direct/range {v0 ..v6}, Lcom/android/server/audio/AudioServiceStubImpl;->transportDataToService(Landroid/content/Context;JJZ)Landroid/content/ComponentName; */
/* .line 1058 */
final String v0 = "device is not support"; // const-string v0, "device is not support"
int v1 = 1; // const/4 v1, 0x1
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.audio.AudioServiceStubImpl ) v7 ).persistPlaybackMsToSettings ( v8, v1, v2, v0 ); // invoke-virtual {v7, v8, v1, v2, v0}, Lcom/android/server/audio/AudioServiceStubImpl;->persistPlaybackMsToSettings(Landroid/content/Context;ZILjava/lang/String;)V
/* .line 1060 */
/* iput-wide v13, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMs:J */
/* .line 1061 */
/* iput-wide v13, v7, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMsTotal:J */
/* .line 1062 */
} // .end method
public void onNotifyBtStopBluetoothSco ( android.os.Handler p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "brokerHandler" # Landroid/os/Handler; */
/* .param p2, "eventSource" # Ljava/lang/String; */
/* .line 1807 */
/* iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mInDelayNotifyBtStopSco:Z */
final String v1 = "AudioServiceStubImpl"; // const-string v1, "AudioServiceStubImpl"
/* if-nez v0, :cond_0 */
/* .line 1808 */
final String v0 = "onNotifyBtStopBluetoothSco: ignore current request"; // const-string v0, "onNotifyBtStopBluetoothSco: ignore current request"
android.util.Log .d ( v1,v0 );
/* .line 1810 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mDelayNotifyBtStopScoCount:I */
int v2 = 1; // const/4 v2, 0x1
/* add-int/2addr v0, v2 */
/* iput v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mDelayNotifyBtStopScoCount:I */
/* .line 1811 */
v0 = this.mAudioService;
/* iget v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mModeOwnerPid:I */
v0 = (( com.android.server.audio.AudioService ) v0 ).getNextModeOwnerPid ( v3 ); // invoke-virtual {v0, v3}, Lcom/android/server/audio/AudioService;->getNextModeOwnerPid(I)I
/* .line 1812 */
/* .local v0, "nextPidFromAudioService":I */
if ( v0 != null) { // if-eqz v0, :cond_1
v3 = this.mDeviceBroker;
/* .line 1813 */
v3 = (( com.android.server.audio.AudioDeviceBroker ) v3 ).isBluetoothScoRequestForPid ( v0 ); // invoke-virtual {v3, v0}, Lcom/android/server/audio/AudioDeviceBroker;->isBluetoothScoRequestForPid(I)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* iget v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mModeOwnerPid:I */
/* if-eq v0, v3, :cond_1 */
/* iget v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mDelayNotifyBtStopScoCount:I */
int v4 = 3; // const/4 v4, 0x3
/* if-gt v3, v4, :cond_1 */
} // :cond_1
int v2 = 0; // const/4 v2, 0x0
/* .line 1816 */
/* .local v2, "isContinueDelayNotify":Z */
} // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 1817 */
(( com.android.server.audio.AudioServiceStubImpl ) p0 ).delayNotifyBtStopBluetoothSco ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/audio/AudioServiceStubImpl;->delayNotifyBtStopBluetoothSco(Landroid/os/Handler;Ljava/lang/String;)V
/* .line 1818 */
final String v3 = "onNotifyBtStopBluetoothSco: continue delay notify bt stopBluetoothSco"; // const-string v3, "onNotifyBtStopBluetoothSco: continue delay notify bt stopBluetoothSco"
android.util.Log .d ( v1,v3 );
/* .line 1820 */
} // :cond_2
/* invoke-direct {p0, p2}, Lcom/android/server/audio/AudioServiceStubImpl;->notifyBtStopBluetoothSco(Ljava/lang/String;)V */
/* .line 1822 */
} // :goto_1
return;
} // .end method
public void onPlayerTracked ( android.media.AudioPlaybackConfiguration p0 ) {
/* .locals 1 */
/* .param p1, "apc" # Landroid/media/AudioPlaybackConfiguration; */
/* .line 2094 */
/* sget-boolean v0, Lcom/android/server/audio/feature/FeatureAdapter;->ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2095 */
v0 = this.mFeatureAdapter;
(( com.android.server.audio.feature.FeatureAdapter ) v0 ).onPlayerTracked ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/audio/feature/FeatureAdapter;->onPlayerTracked(Landroid/media/AudioPlaybackConfiguration;)V
/* .line 2097 */
} // :cond_0
return;
} // .end method
public void onRotationUpdateMiAudioServiceMTK ( java.lang.Integer p0 ) {
/* .locals 1 */
/* .param p1, "rotation" # Ljava/lang/Integer; */
/* .line 1342 */
v0 = this.mMiAudioServiceMTK;
(( com.android.server.audio.MiAudioServiceMTK ) v0 ).onRotationUpdate ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/audio/MiAudioServiceMTK;->onRotationUpdate(Ljava/lang/Integer;)V
/* .line 1343 */
return;
} // .end method
public void onSetCommunicationDeviceForClient ( com.android.server.audio.AudioDeviceBroker$CommunicationDeviceInfo p0 ) {
/* .locals 5 */
/* .param p1, "deviceInfo" # Lcom/android/server/audio/AudioDeviceBroker$CommunicationDeviceInfo; */
/* .line 2052 */
/* iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportedReportAudioRouteState:Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 2053 */
v0 = this.mSetScoCommunicationDevice;
/* monitor-enter v0 */
/* .line 2054 */
if ( p1 != null) { // if-eqz p1, :cond_3
try { // :try_start_0
v1 = this.mDevice;
/* if-nez v1, :cond_0 */
/* .line 2057 */
} // :cond_0
v1 = this.mDevice;
if ( v1 != null) { // if-eqz v1, :cond_2
v1 = this.mDevice;
/* .line 2058 */
v1 = (( android.media.AudioDeviceAttributes ) v1 ).getType ( ); // invoke-virtual {v1}, Landroid/media/AudioDeviceAttributes;->getType()I
int v2 = 7; // const/4 v2, 0x7
/* if-ne v1, v2, :cond_2 */
/* .line 2059 */
v1 = this.mSetScoCommunicationDevice;
v1 = (( java.util.LinkedList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/LinkedList;->size()I
/* .line 2060 */
/* .local v1, "scoDeviceNum":I */
/* const/16 v2, 0xa */
/* if-le v1, v2, :cond_1 */
/* .line 2061 */
v2 = this.mSetScoCommunicationDevice;
/* div-int/lit8 v3, v1, 0x2 */
int v4 = 0; // const/4 v4, 0x0
(( java.util.LinkedList ) v2 ).subList ( v4, v3 ); // invoke-virtual {v2, v4, v3}, Ljava/util/LinkedList;->subList(II)Ljava/util/List;
/* .line 2063 */
} // :cond_1
final String v2 = "AudioServiceStubImpl"; // const-string v2, "AudioServiceStubImpl"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "onSetCommunicationDeviceForClient: add deviceInfo="; // const-string v4, "onSetCommunicationDeviceForClient: add deviceInfo="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v3 );
/* .line 2064 */
v2 = this.mSetScoCommunicationDevice;
(( java.util.LinkedList ) v2 ).add ( p1 ); // invoke-virtual {v2, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
/* .line 2066 */
} // .end local v1 # "scoDeviceNum":I
} // :cond_2
/* monitor-exit v0 */
/* .line 2055 */
} // :cond_3
} // :goto_0
/* monitor-exit v0 */
return;
/* .line 2066 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 2068 */
} // :cond_4
} // :goto_1
return;
} // .end method
public void onShowHearingProtectionNotification ( android.content.Context p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "cx" # Landroid/content/Context; */
/* .param p2, "msgId" # I */
/* .line 939 */
int v0 = 2; // const/4 v0, 0x2
/* if-ne p2, v0, :cond_0 */
/* .line 941 */
return;
/* .line 944 */
} // :cond_0
/* invoke-direct {p0, p1, p2}, Lcom/android/server/audio/AudioServiceStubImpl;->startHearingProtectionService(Landroid/content/Context;I)V */
/* .line 945 */
return;
} // .end method
public void onSystemReadyMiAudioServiceMTK ( ) {
/* .locals 1 */
/* .line 1338 */
v0 = this.mMiAudioServiceMTK;
(( com.android.server.audio.MiAudioServiceMTK ) v0 ).onSystemReady ( ); // invoke-virtual {v0}, Lcom/android/server/audio/MiAudioServiceMTK;->onSystemReady()V
/* .line 1339 */
return;
} // .end method
public Boolean onTrigger ( java.util.List p0 ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/media/AudioPlaybackConfiguration;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 1140 */
/* .local p1, "configs":Ljava/util/List;, "Ljava/util/List<Landroid/media/AudioPlaybackConfiguration;>;" */
/* if-nez p1, :cond_0 */
/* .line 1141 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1143 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 1144 */
/* .local v0, "isMusicActive":Z */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Landroid/media/AudioPlaybackConfiguration; */
/* .line 1145 */
/* .local v2, "config":Landroid/media/AudioPlaybackConfiguration; */
v3 = /* invoke-direct {p0, v2}, Lcom/android/server/audio/AudioServiceStubImpl;->isMusicPlayerActive(Landroid/media/AudioPlaybackConfiguration;)Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1146 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1147 */
/* .line 1149 */
} // .end local v2 # "config":Landroid/media/AudioPlaybackConfiguration;
} // :cond_1
/* .line 1150 */
} // :cond_2
} // :goto_1
} // .end method
public void onUpdateAudioMode ( Integer p0, java.lang.String p1, android.content.Context p2 ) {
/* .locals 3 */
/* .param p1, "audioMode" # I */
/* .param p2, "requesterPackage" # Ljava/lang/String; */
/* .param p3, "context" # Landroid/content/Context; */
/* .line 498 */
/* iput p1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioMode:I */
/* .line 499 */
int v0 = 2; // const/4 v0, 0x2
/* if-eq p1, v0, :cond_1 */
int v0 = 3; // const/4 v0, 0x3
/* if-ne p1, v0, :cond_0 */
/* .line 502 */
} // :cond_0
/* if-nez p1, :cond_2 */
/* .line 504 */
miui.tipclose.TipHelperProxy .getInstance ( );
(( miui.tipclose.TipHelperProxy ) v0 ).hideTipForPhone ( ); // invoke-virtual {v0}, Lmiui/tipclose/TipHelperProxy;->hideTipForPhone()V
/* .line 501 */
} // :cond_1
} // :goto_0
miui.tipclose.TipHelperProxy .getInstance ( );
int v1 = 0; // const/4 v1, 0x0
(( miui.tipclose.TipHelperProxy ) v0 ).showTipForPhone ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Lmiui/tipclose/TipHelperProxy;->showTipForPhone(ZLjava/lang/String;)V
/* .line 506 */
} // :cond_2
} // :goto_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "onUpdateAudioMode audiomode "; // const-string v1, "onUpdateAudioMode audiomode "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioMode:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " package:"; // const-string v1, " package:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AudioServiceStubImpl"; // const-string v1, "AudioServiceStubImpl"
android.util.Log .d ( v1,v0 );
/* .line 507 */
this.mRequesterPackageForAudioMode = p2;
/* .line 509 */
v0 = this.mHandler;
int v1 = 7; // const/4 v1, 0x7
java.lang.Integer .valueOf ( p1 );
(( android.os.Handler ) v0 ).obtainMessage ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 510 */
return;
} // .end method
public void onUpdateMediaState ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "mediaActive" # Z */
/* .line 514 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "onUpdateMediaState mediaActive="; // const-string v1, "onUpdateMediaState mediaActive="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AudioServiceStubImpl"; // const-string v1, "AudioServiceStubImpl"
android.util.Log .d ( v1,v0 );
/* .line 515 */
v0 = this.mHandler;
/* const/16 v1, 0x9 */
java.lang.Boolean .valueOf ( p1 );
(( android.os.Handler ) v0 ).obtainMessage ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 516 */
return;
} // .end method
public void persistPlaybackMsToSettings ( android.content.Context p0, Boolean p1, Integer p2, java.lang.String p3 ) {
/* .locals 6 */
/* .param p1, "cx" # Landroid/content/Context; */
/* .param p2, "isNeedClearData" # Z */
/* .param p3, "highLevel" # I */
/* .param p4, "from" # Ljava/lang/String; */
/* .line 1019 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "persistPlaybackMsToSettings: isNeedClearData: "; // const-string v1, "persistPlaybackMsToSettings: isNeedClearData: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " mPlaybackStartTime: "; // const-string v1, " mPlaybackStartTime: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mPlaybackStartTime:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = " mMusicPlaybackContinuousMsTotal: "; // const-string v1, " mMusicPlaybackContinuousMsTotal: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMsTotal:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = " highLevel: "; // const-string v1, " highLevel: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " from: "; // const-string v1, " from: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AudioServiceStubImpl"; // const-string v1, "AudioServiceStubImpl"
android.util.Log .i ( v1,v0 );
/* .line 1023 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1024 */
/* const-wide/16 v1, 0x0 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* move-wide v3, v1 */
} // :cond_0
/* iget-wide v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mPlaybackStartTime:J */
/* .line 1023 */
} // :goto_0
final String v5 = "key_persist_playback_start_ms"; // const-string v5, "key_persist_playback_start_ms"
android.provider.Settings$Global .putLong ( v0,v5,v3,v4 );
/* .line 1025 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1027 */
if ( p2 != null) { // if-eqz p2, :cond_1
} // :cond_1
/* iget-wide v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mMusicPlaybackContinuousMsTotal:J */
/* .line 1025 */
} // :goto_1
final String v3 = "key_persist_playback_continuous_ms"; // const-string v3, "key_persist_playback_continuous_ms"
android.provider.Settings$Global .putLong ( v0,v3,v1,v2 );
/* .line 1028 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "key_persist_playback_high_voice"; // const-string v1, "key_persist_playback_high_voice"
android.provider.Settings$Global .putInt ( v0,v1,p3 );
/* .line 1030 */
return;
} // .end method
public void reSetAudioCinemaModeThermal ( ) {
/* .locals 1 */
/* .line 1387 */
/* iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportCinemaMode:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mAudioThermalObserver;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1388 */
(( com.android.server.audio.AudioThermalObserver ) v0 ).reSetAudioCinemaModeThermal ( ); // invoke-virtual {v0}, Lcom/android/server/audio/AudioThermalObserver;->reSetAudioCinemaModeThermal()V
/* .line 1390 */
} // :cond_0
return;
} // .end method
public void reSetAudioParam ( ) {
/* .locals 1 */
/* .line 1373 */
/* iget v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mSettingAudioPowerSave:I */
/* and-int/lit8 v0, v0, 0x1 */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mAudioPowerSaveModeObserver;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1374 */
(( com.android.server.audio.AudioPowerSaveModeObserver ) v0 ).reSetAudioPowerParam ( ); // invoke-virtual {v0}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->reSetAudioPowerParam()V
/* .line 1376 */
} // :cond_0
return;
} // .end method
public void readPlaybackMsSettings ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "cx" # Landroid/content/Context; */
/* .line 978 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "key_persist_playback_start_ms"; // const-string v1, "key_persist_playback_start_ms"
/* const-wide/16 v2, 0x0 */
android.provider.Settings$Global .getLong ( v0,v1,v2,v3 );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mStartMs:J */
/* .line 980 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "key_persist_playback_continuous_ms"; // const-string v1, "key_persist_playback_continuous_ms"
android.provider.Settings$Global .getLong ( v0,v1,v2,v3 );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mContinuousMs:J */
/* .line 982 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "key_persist_playback_high_voice"; // const-string v1, "key_persist_playback_high_voice"
int v4 = 0; // const/4 v4, 0x0
v0 = android.provider.Settings$Global .getInt ( v0,v1,v4 );
/* iput v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mHighLevel:I */
/* .line 985 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "key_persist_cumulative_playback_ms"; // const-string v1, "key_persist_cumulative_playback_ms"
android.provider.Settings$Global .getLong ( v0,v1,v2,v3 );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mCumulativePlaybackStartTime:J */
/* .line 988 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "key_persist_notification_date"; // const-string v1, "key_persist_notification_date"
android.provider.Settings$Global .getString ( v0,v1 );
/* .line 990 */
/* .local v0, "dateTime":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 991 */
/* nop */
/* .line 992 */
/* const-string/jumbo v1, "yyyy-MM-dd" */
java.time.format.DateTimeFormatter .ofPattern ( v1 );
/* .line 991 */
java.time.LocalDate .parse ( v0,v1 );
this.mNotificationDate = v1;
/* .line 994 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "readPlaybackMsSettings: startMs: "; // const-string v2, "readPlaybackMsSettings: startMs: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mStartMs:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = " continuousMs: "; // const-string v2, " continuousMs: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mContinuousMs:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = " highLevel: "; // const-string v2, " highLevel: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mHighLevel:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " mNotificationDate="; // const-string v2, " mNotificationDate="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mNotificationDate;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = " mCumulativePlaybackStartTime: "; // const-string v2, " mCumulativePlaybackStartTime: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mCumulativePlaybackStartTime:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "AudioServiceStubImpl"; // const-string v2, "AudioServiceStubImpl"
android.util.Log .i ( v2,v1 );
/* .line 998 */
return;
} // .end method
public Boolean realTimeModeEnabled ( ) {
/* .locals 2 */
/* .line 294 */
v0 = this.mContext;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 295 */
(( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
v1 = android.os.Binder .getCallingUid ( );
(( android.content.pm.PackageManager ) v0 ).getNameForUid ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;
/* .line 296 */
/* .local v0, "packageName":Ljava/lang/String; */
final String v1 = "android.media.audio.cts"; // const-string v1, "android.media.audio.cts"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_0 */
/* .line 297 */
/* iget-boolean v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->REALLY_TIME_MODE_ENABLED:Z */
/* .line 300 */
} // .end local v0 # "packageName":Ljava/lang/String;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void registerContentObserverForMiui ( android.content.ContentResolver p0, Boolean p1, android.database.ContentObserver p2, Integer p3 ) {
/* .locals 3 */
/* .param p1, "contentResolver" # Landroid/content/ContentResolver; */
/* .param p2, "notifyForDescendents" # Z */
/* .param p3, "observer" # Landroid/database/ContentObserver; */
/* .param p4, "userAll" # I */
/* .line 576 */
/* nop */
/* .line 577 */
/* nop */
/* .line 578 */
final String v0 = "mute_music_at_silent"; // const-string v0, "mute_music_at_silent"
android.provider.Settings$System .getUriFor ( v0 );
/* .line 577 */
(( android.content.ContentResolver ) p1 ).registerContentObserver ( v0, p2, p3, p4 ); // invoke-virtual {p1, v0, p2, p3, p4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 585 */
/* const-string/jumbo v0, "sound_assist_key" */
android.provider.Settings$Global .getUriFor ( v0 );
(( android.content.ContentResolver ) p1 ).registerContentObserver ( v0, p2, p3 ); // invoke-virtual {p1, v0, p2, p3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 587 */
final String v0 = "key_ignore_music_focus_req"; // const-string v0, "key_ignore_music_focus_req"
android.provider.Settings$Global .getUriFor ( v0 );
(( android.content.ContentResolver ) p1 ).registerContentObserver ( v0, p2, p3 ); // invoke-virtual {p1, v0, p2, p3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 590 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = this.mRegisterContentName;
/* array-length v2, v1 */
/* if-ge v0, v2, :cond_0 */
/* .line 591 */
/* aget-object v1, v1, v0 */
android.provider.Settings$System .getUriFor ( v1 );
(( android.content.ContentResolver ) p1 ).registerContentObserver ( v1, p2, p3 ); // invoke-virtual {p1, v1, p2, p3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 590 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 594 */
} // .end local v0 # "i":I
} // :cond_0
return;
} // .end method
public void reportAbnormalAudioStatus ( Integer p0, java.lang.String p1, java.lang.String p2, java.lang.String p3 ) {
/* .locals 17 */
/* .param p1, "errorType" # I */
/* .param p2, "eventSource" # Ljava/lang/String; */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .param p4, "audioState" # Ljava/lang/String; */
/* .line 1995 */
/* move-object/from16 v0, p0 */
/* move/from16 v9, p1 */
/* move-object/from16 v10, p2 */
/* iget-boolean v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportedReportAudioRouteState:Z */
final String v2 = "AudioServiceStubImpl"; // const-string v2, "AudioServiceStubImpl"
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 1996 */
int v1 = -1; // const/4 v1, -0x1
/* if-ne v9, v1, :cond_0 */
/* .line 1997 */
final String v1 = "reportAudioStatus\uff1a audio route is normal"; // const-string v1, "reportAudioStatus\uff1a audio route is normal"
android.util.Log .w ( v2,v1 );
/* .line 1998 */
return;
/* .line 2000 */
} // :cond_0
final String v1 = "ok"; // const-string v1, "ok"
int v3 = 1; // const/4 v3, 0x1
/* move-object/from16 v11, p4 */
/* if-ne v11, v1, :cond_1 */
v1 = this.mMiuiXlog;
final String v4 = "check_audio_route_for_bluetooth"; // const-string v4, "check_audio_route_for_bluetooth"
v1 = (( android.media.MiuiXlog ) v1 ).checkXlogPermission ( v4, v3 ); // invoke-virtual {v1, v4, v3}, Landroid/media/MiuiXlog;->checkXlogPermission(Ljava/lang/String;I)Z
/* if-nez v1, :cond_1 */
/* .line 2002 */
final String v1 = "reportAbnormalAudioStatus: TR6_XLOG_DISABLE"; // const-string v1, "reportAbnormalAudioStatus: TR6_XLOG_DISABLE"
android.util.Log .d ( v2,v1 );
/* .line 2003 */
return;
/* .line 2005 */
} // :cond_1
v1 = /* invoke-static/range {p2 ..p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 2006 */
final String v1 = "reportAudioStatus\uff1a eventSource is empty"; // const-string v1, "reportAudioStatus\uff1a eventSource is empty"
android.util.Log .w ( v2,v1 );
/* .line 2007 */
return;
/* .line 2010 */
} // :cond_2
int v1 = 2; // const/4 v1, 0x2
/* if-eq v9, v3, :cond_3 */
/* if-eq v9, v1, :cond_3 */
/* iget v3, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mA2dpDeviceConnectedState:I */
/* if-nez v3, :cond_3 */
/* iget v3, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mHeadsetDeviceConnectedState:I */
/* if-nez v3, :cond_3 */
/* .line 2014 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "reportAudioStatus\uff1a no active bt device\uff0cerrorType="; // const-string v3, "reportAudioStatus\uff1a no active bt device\uff0cerrorType="
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v1 );
/* .line 2015 */
return;
/* .line 2018 */
} // :cond_3
v3 = /* invoke-static/range {p3 ..p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z */
if ( v3 != null) { // if-eqz v3, :cond_5
/* iget v3, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioMode:I */
/* if-eq v3, v1, :cond_4 */
int v1 = 3; // const/4 v1, 0x3
/* if-ne v3, v1, :cond_5 */
/* .line 2021 */
} // :cond_4
v1 = this.mRequesterPackageForAudioMode;
/* move-object v12, v1 */
} // .end local p3 # "packageName":Ljava/lang/String;
/* .local v1, "packageName":Ljava/lang/String; */
/* .line 2022 */
} // .end local v1 # "packageName":Ljava/lang/String;
/* .restart local p3 # "packageName":Ljava/lang/String; */
} // :cond_5
v1 = /* invoke-static/range {p3 ..p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z */
if ( v1 != null) { // if-eqz v1, :cond_6
v1 = this.mContext;
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 2023 */
(( android.content.Context ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
/* move-object v12, v1 */
} // .end local p3 # "packageName":Ljava/lang/String;
/* .restart local v1 # "packageName":Ljava/lang/String; */
/* .line 2025 */
} // .end local v1 # "packageName":Ljava/lang/String;
/* .restart local p3 # "packageName":Ljava/lang/String; */
} // :cond_6
/* move-object/from16 v12, p3 */
} // .end local p3 # "packageName":Ljava/lang/String;
/* .local v12, "packageName":Ljava/lang/String; */
} // :goto_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v13 */
/* .line 2026 */
/* .local v13, "currentTime":J */
/* iget-wide v3, v0, Lcom/android/server/audio/AudioServiceStubImpl;->sLastAudioStateExpTime:J */
/* sub-long v3, v13, v3 */
/* const-wide/16 v5, 0x1770 */
/* cmp-long v1, v3, v5 */
/* if-gez v1, :cond_7 */
/* iget v1, v0, Lcom/android/server/audio/AudioServiceStubImpl;->sLastReportAudioErrorType:I */
/* if-ne v9, v1, :cond_7 */
/* .line 2028 */
/* iput-wide v13, v0, Lcom/android/server/audio/AudioServiceStubImpl;->sLastAudioStateExpTime:J */
/* .line 2029 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "reportAudioStatus: the upload interval has not exceeded 6000 s, errorType="; // const-string v3, "reportAudioStatus: the upload interval has not exceeded 6000 s, errorType="
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " eventSource="; // const-string v3, " eventSource="
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v10 ); // invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " packageName="; // const-string v3, " packageName="
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v12 ); // invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v1 );
/* .line 2032 */
return;
/* .line 2034 */
} // :cond_7
/* iput v9, v0, Lcom/android/server/audio/AudioServiceStubImpl;->sLastReportAudioErrorType:I */
/* .line 2035 */
/* iput-wide v13, v0, Lcom/android/server/audio/AudioServiceStubImpl;->sLastAudioStateExpTime:J */
/* .line 2036 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v10 ); // invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " modePid="; // const-string v2, " modePid="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mModeOwnerPid:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " routeClients="; // const-string v2, " routeClients="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mCommunicationRouteClients;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = " mScoBtState="; // const-string v2, " mScoBtState="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mScoBtState:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2040 */
/* .local v15, "audioEventSource":Ljava/lang/String; */
/* new-instance v16, Lcom/android/server/audio/MQSUtils$AudioStateTrackData; */
v5 = this.mBtName;
/* iget v6, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mA2dpDeviceConnectedState:I */
/* iget v7, v0, Lcom/android/server/audio/AudioServiceStubImpl;->mHeadsetDeviceConnectedState:I */
/* move-object/from16 v1, v16 */
/* move/from16 v2, p1 */
/* move-object v3, v15 */
/* move-object v4, v12 */
/* move-object/from16 v8, p4 */
/* invoke-direct/range {v1 ..v8}, Lcom/android/server/audio/MQSUtils$AudioStateTrackData;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V */
/* .line 2043 */
/* .local v1, "audioTrackData":Lcom/android/server/audio/MQSUtils$AudioStateTrackData; */
v2 = this.mHandler;
int v3 = 5; // const/4 v3, 0x5
(( android.os.Handler ) v2 ).obtainMessage ( v3, v1 ); // invoke-virtual {v2, v3, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( android.os.Message ) v2 ).sendToTarget ( ); // invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V
/* .line 2044 */
} // .end local v1 # "audioTrackData":Lcom/android/server/audio/MQSUtils$AudioStateTrackData;
} // .end local v13 # "currentTime":J
} // .end local v15 # "audioEventSource":Ljava/lang/String;
/* .line 2045 */
} // .end local v12 # "packageName":Ljava/lang/String;
/* .restart local p3 # "packageName":Ljava/lang/String; */
} // :cond_8
/* move-object/from16 v11, p4 */
final String v1 = "reportAudioStatus\uff1a no report permission"; // const-string v1, "reportAudioStatus\uff1a no report permission"
android.util.Log .d ( v2,v1 );
/* move-object/from16 v12, p3 */
/* .line 2047 */
} // .end local p3 # "packageName":Ljava/lang/String;
/* .restart local v12 # "packageName":Ljava/lang/String; */
} // :goto_1
return;
} // .end method
public void reportAudioHwState ( android.content.Context p0 ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 567 */
com.android.server.audio.MQSAudioHardware .getInstance ( p1 );
(( com.android.server.audio.MQSAudioHardware ) v0 ).onetrack ( ); // invoke-virtual {v0}, Lcom/android/server/audio/MQSAudioHardware;->onetrack()V
/* .line 568 */
return;
} // .end method
public void reportAudioSilentObserverToOnetrack ( Integer p0, java.lang.String p1, java.lang.String p2, Integer p3 ) {
/* .locals 3 */
/* .param p1, "level" # I */
/* .param p2, "location" # Ljava/lang/String; */
/* .param p3, "silent_reason" # Ljava/lang/String; */
/* .param p4, "silent_type" # I */
/* .line 1568 */
v0 = this.mMiuiXlog;
final String v1 = "audio_silent_observer"; // const-string v1, "audio_silent_observer"
v0 = (( android.media.MiuiXlog ) v0 ).checkXlogPermission ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/media/MiuiXlog;->checkXlogPermission(Ljava/lang/String;I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1569 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 1570 */
/* .local v0, "info":Landroid/os/Bundle; */
final String v1 = "audio_silent_level"; // const-string v1, "audio_silent_level"
(( android.os.Bundle ) v0 ).putInt ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 1571 */
final String v1 = "audio_silent_location"; // const-string v1, "audio_silent_location"
(( android.os.Bundle ) v0 ).putString ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1572 */
final String v1 = "audio_silent_reason"; // const-string v1, "audio_silent_reason"
(( android.os.Bundle ) v0 ).putString ( v1, p3 ); // invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1573 */
final String v1 = "audio_silent_type"; // const-string v1, "audio_silent_type"
(( android.os.Bundle ) v0 ).putInt ( v1, p4 ); // invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 1574 */
android.os.Message .obtain ( );
/* .line 1575 */
/* .local v1, "msg":Landroid/os/Message; */
int v2 = 4; // const/4 v2, 0x4
/* iput v2, v1, Landroid/os/Message;->what:I */
/* .line 1576 */
(( android.os.Message ) v1 ).setData ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
/* .line 1577 */
v2 = this.mHandler;
(( android.os.Handler ) v2 ).sendMessage ( v1 ); // invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 1579 */
} // .end local v0 # "info":Landroid/os/Bundle;
} // .end local v1 # "msg":Landroid/os/Message;
} // :cond_0
return;
} // .end method
public void reportNotificationEventToOnetrack ( java.lang.String p0 ) {
/* .locals 8 */
/* .param p1, "isTurnOn" # Ljava/lang/String; */
/* .line 1585 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "reportNotificationEventToOnetrack, isTurnOn: "; // const-string v1, "reportNotificationEventToOnetrack, isTurnOn: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AudioServiceStubImpl"; // const-string v1, "AudioServiceStubImpl"
android.util.Log .d ( v1,v0 );
/* .line 1586 */
java.time.LocalDateTime .now ( );
/* .line 1587 */
/* .local v0, "date_time":Ljava/time/LocalDateTime; */
/* const-string/jumbo v2, "yyyy-MM-dd HH:mm:ss.SSS" */
java.time.format.DateTimeFormatter .ofPattern ( v2 );
/* .line 1588 */
/* .local v2, "formatter":Ljava/time/format/DateTimeFormatter; */
(( java.time.LocalDateTime ) v0 ).format ( v2 ); // invoke-virtual {v0, v2}, Ljava/time/LocalDateTime;->format(Ljava/time/format/DateTimeFormatter;)Ljava/lang/String;
/* .line 1589 */
/* .local v3, "current_time":Ljava/lang/String; */
/* const-string/jumbo v4, "{\"name\":\"audio_notification_alias\",\"audio_event\":{\"notification_isalias_ring\":\"%s\" , \"current_time\":\"%s\"}, \"dgt\":\"null\",\"audio_ext\":\"null\" }" */
/* filled-new-array {p1, v3}, [Ljava/lang/Object; */
java.lang.String .format ( v4,v5 );
/* .line 1590 */
/* .local v4, "result":Ljava/lang/String; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "result: "; // const-string v6, "result: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v5 );
/* .line 1592 */
try { // :try_start_0
v5 = this.mMiuiXlog;
(( android.media.MiuiXlog ) v5 ).miuiXlogSend ( v4 ); // invoke-virtual {v5, v4}, Landroid/media/MiuiXlog;->miuiXlogSend(Ljava/lang/String;)I
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1595 */
/* .line 1593 */
/* :catch_0 */
/* move-exception v5 */
/* .line 1594 */
/* .local v5, "e":Ljava/lang/Exception; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "reportNotificationEventToOnetrack exception : "; // const-string v7, "reportNotificationEventToOnetrack exception : "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v6 );
/* .line 1597 */
} // .end local v5 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public void setA2dpDeviceClassForOneTrack ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "deviceClassName" # Ljava/lang/String; */
/* .line 1855 */
v0 = this.mMiuiXlog;
final String v1 = "headphones"; // const-string v1, "headphones"
int v2 = 0; // const/4 v2, 0x0
v0 = (( android.media.MiuiXlog ) v0 ).checkXlogPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/media/MiuiXlog;->checkXlogPermission(Ljava/lang/String;I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1856 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "a2dp_device_class_name="; // const-string v1, "a2dp_device_class_name="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.media.AudioSystem .setParameters ( v0 );
/* .line 1858 */
} // :cond_0
return;
} // .end method
public void setBluetoothHeadset ( android.bluetooth.BluetoothDevice p0 ) {
/* .locals 6 */
/* .param p1, "btDevice" # Landroid/bluetooth/BluetoothDevice; */
/* .line 694 */
(( android.bluetooth.BluetoothDevice ) p1 ).getBluetoothClass ( ); // invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;
/* .line 695 */
/* .local v0, "bluetoothClass":Landroid/bluetooth/BluetoothClass; */
final String v1 = "AudioServiceStubImpl"; // const-string v1, "AudioServiceStubImpl"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 696 */
v2 = com.android.server.audio.AudioServiceStubImpl .isBluetoothHeadsetDevice ( v0 );
/* iput-boolean v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->markBluetoothheadsetstub:Z */
/* .line 697 */
v2 = (( android.bluetooth.BluetoothClass ) v0 ).getDeviceClass ( ); // invoke-virtual {v0}, Landroid/bluetooth/BluetoothClass;->getDeviceClass()I
/* .line 698 */
/* .local v2, "deviceClass":I */
v3 = (( android.bluetooth.BluetoothClass ) v0 ).getMajorDeviceClass ( ); // invoke-virtual {v0}, Landroid/bluetooth/BluetoothClass;->getMajorDeviceClass()I
/* .line 699 */
/* .local v3, "majorClass":I */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "majorClass:"; // const-string v5, "majorClass:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.Integer .toHexString ( v3 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ", deviceClass:"; // const-string v5, ", deviceClass:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 700 */
java.lang.Integer .toHexString ( v2 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ", markBluetoothhead:"; // const-string v5, ", markBluetoothhead:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v5, p0, Lcom/android/server/audio/AudioServiceStubImpl;->markBluetoothheadsetstub:Z */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 699 */
android.util.Log .d ( v1,v4 );
/* .line 702 */
} // .end local v2 # "deviceClass":I
} // .end local v3 # "majorClass":I
/* .line 703 */
} // :cond_0
final String v2 = "bluetoothClass is null"; // const-string v2, "bluetoothClass is null"
android.util.Log .w ( v1,v2 );
/* .line 705 */
} // :goto_0
return;
} // .end method
public void setHifiVolume ( android.content.Context p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "volume" # I */
/* .line 529 */
miui.util.AudioManagerHelper .setHiFiVolume ( p1,p2 );
/* .line 530 */
return;
} // .end method
public void setPreferredCommunicationDevice ( android.media.AudioDeviceAttributes p0, java.util.LinkedList p1 ) {
/* .locals 5 */
/* .param p1, "device" # Landroid/media/AudioDeviceAttributes; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/media/AudioDeviceAttributes;", */
/* "Ljava/util/LinkedList<", */
/* "Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 1900 */
/* .local p2, "routeClients":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;>;" */
/* iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportedReportAudioRouteState:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1901 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 1902 */
/* .local v0, "scoClients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
(( java.util.LinkedList ) p2 ).iterator ( ); // invoke-virtual {p2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient; */
/* .line 1903 */
/* .local v2, "client":Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient; */
(( com.android.server.audio.AudioDeviceBroker$CommunicationRouteClient ) v2 ).getDevice ( ); // invoke-virtual {v2}, Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;->getDevice()Landroid/media/AudioDeviceAttributes;
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1904 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "pid="; // const-string v4, "pid="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = (( com.android.server.audio.AudioDeviceBroker$CommunicationRouteClient ) v2 ).getPid ( ); // invoke-virtual {v2}, Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;->getPid()I
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " deviceType="; // const-string v4, " deviceType="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1905 */
(( com.android.server.audio.AudioDeviceBroker$CommunicationRouteClient ) v2 ).getDevice ( ); // invoke-virtual {v2}, Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;->getDevice()Landroid/media/AudioDeviceAttributes;
v4 = (( android.media.AudioDeviceAttributes ) v4 ).getType ( ); // invoke-virtual {v4}, Landroid/media/AudioDeviceAttributes;->getType()I
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " cb="; // const-string v4, " cb="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1906 */
(( com.android.server.audio.AudioDeviceBroker$CommunicationRouteClient ) v2 ).getBinder ( ); // invoke-virtual {v2}, Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;->getBinder()Landroid/os/IBinder;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1907 */
/* .local v3, "clientInfo":Ljava/lang/String; */
/* .line 1909 */
} // .end local v2 # "client":Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;
} // .end local v3 # "clientInfo":Ljava/lang/String;
} // :cond_0
/* .line 1910 */
} // :cond_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "setPreferredCommunicationDevice\uff1a clients=" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1911 */
java.util.Arrays .toString ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1910 */
final String v2 = "AudioServiceStubImpl"; // const-string v2, "AudioServiceStubImpl"
android.util.Log .d ( v2,v1 );
/* .line 1912 */
this.mPreferredCommunicationDevice = p1;
/* .line 1913 */
this.mCommunicationRouteClients = v0;
/* .line 1915 */
} // .end local v0 # "scoClients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_2
return;
} // .end method
public void setPutGlobalInt ( java.lang.Object p0, com.android.server.audio.SettingsAdapter p1, android.content.ContentResolver p2, Boolean p3 ) {
/* .locals 4 */
/* .param p1, "settingslock" # Ljava/lang/Object; */
/* .param p2, "settings" # Lcom/android/server/audio/SettingsAdapter; */
/* .param p3, "contentresolver" # Landroid/content/ContentResolver; */
/* .param p4, "enabled" # Z */
/* .line 1602 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 1604 */
/* .local v0, "token":J */
try { // :try_start_0
/* monitor-enter p1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 1605 */
try { // :try_start_1
/* const-string/jumbo v2, "spatial_audio_feature_enable" */
/* .line 1606 */
if ( p4 != null) { // if-eqz p4, :cond_0
int v3 = 1; // const/4 v3, 0x1
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
/* .line 1605 */
} // :goto_0
(( com.android.server.audio.SettingsAdapter ) p2 ).putGlobalInt ( p3, v2, v3 ); // invoke-virtual {p2, p3, v2, v3}, Lcom/android/server/audio/SettingsAdapter;->putGlobalInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
/* .line 1607 */
/* monitor-exit p1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1609 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 1610 */
/* nop */
/* .line 1611 */
return;
/* .line 1607 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_2
/* monitor-exit p1 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
} // .end local v0 # "token":J
} // .end local p0 # "this":Lcom/android/server/audio/AudioServiceStubImpl;
} // .end local p1 # "settingslock":Ljava/lang/Object;
} // .end local p2 # "settings":Lcom/android/server/audio/SettingsAdapter;
} // .end local p3 # "contentresolver":Landroid/content/ContentResolver;
} // .end local p4 # "enabled":Z
try { // :try_start_3
/* throw v2 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* .line 1609 */
/* .restart local v0 # "token":J */
/* .restart local p0 # "this":Lcom/android/server/audio/AudioServiceStubImpl; */
/* .restart local p1 # "settingslock":Ljava/lang/Object; */
/* .restart local p2 # "settings":Lcom/android/server/audio/SettingsAdapter; */
/* .restart local p3 # "contentresolver":Landroid/content/ContentResolver; */
/* .restart local p4 # "enabled":Z */
/* :catchall_1 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 1610 */
/* throw v2 */
} // .end method
public void setStreamMusicOrBluetoothScoIndex ( android.content.Context p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "index" # I */
/* .param p3, "stream" # I */
/* .param p4, "device" # I */
/* .line 713 */
/* new-instance v0, Ljava/lang/Thread; */
/* new-instance v1, Lcom/android/server/audio/AudioServiceStubImpl$2; */
/* invoke-direct {v1, p0, p3, p2, p4}, Lcom/android/server/audio/AudioServiceStubImpl$2;-><init>(Lcom/android/server/audio/AudioServiceStubImpl;III)V */
/* invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V */
/* .line 738 */
(( java.lang.Thread ) v0 ).start ( ); // invoke-virtual {v0}, Ljava/lang/Thread;->start()V
/* .line 739 */
return;
} // .end method
public Integer setSuperIndex ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 6 */
/* .param p1, "index" # I */
/* .param p2, "device" # I */
/* .param p3, "mStreamType" # I */
/* .param p4, "mIndexMax" # I */
/* .line 1673 */
/* div-int/lit8 v0, p4, 0xa */
/* .line 1674 */
/* .local v0, "maxIndex":I */
int v1 = 3; // const/4 v1, 0x3
int v2 = 2; // const/4 v2, 0x2
/* if-eq p3, v1, :cond_0 */
/* if-eq p3, v2, :cond_0 */
int v1 = 5; // const/4 v1, 0x5
/* if-ne p3, v1, :cond_2 */
} // :cond_0
/* if-ne p2, v2, :cond_2 */
/* .line 1677 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "SuperVolume: setSuperIndex index="; // const-string v2, "SuperVolume: setSuperIndex index="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " device="; // const-string v2, " device="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " mStreamType="; // const-string v2, " mStreamType="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "AudioServiceStubImpl"; // const-string v2, "AudioServiceStubImpl"
android.util.Log .d ( v2,v1 );
/* .line 1678 */
/* new-instance v1, Ljava/lang/String; */
final String v3 = ""; // const-string v3, ""
/* invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V */
/* .line 1679 */
/* .local v1, "params":Ljava/lang/String; */
final String v3 = ";SuperStream="; // const-string v3, ";SuperStream="
final String v4 = "SpkVolIdx="; // const-string v4, "SpkVolIdx="
/* if-le p1, v0, :cond_1 */
/* .line 1680 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1681 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p3 ); // invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1682 */
int v3 = 1; // const/4 v3, 0x1
/* iput-boolean v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mSuperVolumeOn:Z */
/* .line 1683 */
android.media.AudioSystem .setParameters ( v1 );
/* .line 1684 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "SuperVolume: mSuperVolumeOn = true setParameters: "; // const-string v4, "SuperVolume: mSuperVolumeOn = true setParameters: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v3 );
/* .line 1685 */
} // :cond_1
/* iget-boolean v5, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mSuperVolumeOn:Z */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 1686 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1687 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p3 ); // invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1688 */
int v3 = 0; // const/4 v3, 0x0
/* iput-boolean v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mSuperVolumeOn:Z */
/* .line 1689 */
android.media.AudioSystem .setParameters ( v1 );
/* .line 1690 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "SuperVolume: mSuperVolumeOn = false setParameters: "; // const-string v4, "SuperVolume: mSuperVolumeOn = false setParameters: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v3 );
/* .line 1694 */
} // .end local v1 # "params":Ljava/lang/String;
} // :cond_2
} // :goto_0
/* if-le p1, v0, :cond_3 */
/* move v1, v0 */
} // :cond_3
/* move v1, p1 */
} // :goto_1
} // .end method
public void showDeviceConnectNotification ( android.content.Context p0, Integer p1, Boolean p2 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "device" # I */
/* .param p3, "isShow" # Z */
/* .line 534 */
/* iget v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mReceiveNotificationDevice:I */
/* and-int/2addr v0, p2 */
if ( v0 != null) { // if-eqz v0, :cond_1
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 535 */
v0 = this.mNm;
/* if-nez v0, :cond_0 */
/* .line 536 */
final String v0 = "notification"; // const-string v0, "notification"
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/NotificationManager; */
this.mNm = v0;
/* .line 538 */
} // :cond_0
/* new-instance v0, Landroid/os/Handler; */
/* invoke-direct {v0}, Landroid/os/Handler;-><init>()V */
/* new-instance v1, Lcom/android/server/audio/AudioServiceStubImpl$1; */
/* invoke-direct {v1, p0, p3, p1}, Lcom/android/server/audio/AudioServiceStubImpl$1;-><init>(Lcom/android/server/audio/AudioServiceStubImpl;ZLandroid/content/Context;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 549 */
} // :cond_1
return;
} // .end method
public void showVisualEffect ( android.content.Context p0, java.lang.String p1, java.util.List p2, android.os.Handler p3 ) {
/* .locals 0 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "action" # Ljava/lang/String; */
/* .param p4, "handler" # Landroid/os/Handler; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/content/Context;", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Landroid/media/AudioPlaybackConfiguration;", */
/* ">;", */
/* "Landroid/os/Handler;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 559 */
/* .local p3, "apcList":Ljava/util/List;, "Ljava/util/List<Landroid/media/AudioPlaybackConfiguration;>;" */
android.media.AudioServiceInjector .startAudioVisualIfsatisfiedWith ( p2,p3,p4 );
/* .line 562 */
/* invoke-direct {p0, p1, p3}, Lcom/android/server/audio/AudioServiceStubImpl;->reportAudioStatus(Landroid/content/Context;Ljava/util/List;)V */
/* .line 563 */
return;
} // .end method
public void showVisualEffectNotification ( android.content.Context p0, Integer p1, Integer p2 ) {
/* .locals 0 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "uid" # I */
/* .param p3, "event" # I */
/* .line 553 */
android.media.AudioServiceInjector .showNotification ( p2,p3,p1 );
/* .line 554 */
return;
} // .end method
public Boolean soundLeakProtectionEnabled ( ) {
/* .locals 1 */
/* .line 305 */
/* iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->SOUND_LEAK_PROTECTION_ENABLED:Z */
} // .end method
public void spatializerSetFeature ( com.android.server.audio.SettingsAdapter p0, com.android.server.audio.SpatializerHelper p1, android.content.ContentResolver p2, Integer p3 ) {
/* .locals 2 */
/* .param p1, "settings" # Lcom/android/server/audio/SettingsAdapter; */
/* .param p2, "spatializerhelper" # Lcom/android/server/audio/SpatializerHelper; */
/* .param p3, "contentresolver" # Landroid/content/ContentResolver; */
/* .param p4, "num" # I */
/* .line 1629 */
/* const-string/jumbo v0, "spatial_audio_feature_enable" */
v0 = (( com.android.server.audio.SettingsAdapter ) p1 ).getGlobalInt ( p3, v0, p4 ); // invoke-virtual {p1, p3, v0, p4}, Lcom/android/server/audio/SettingsAdapter;->getGlobalInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* move v0, v1 */
/* .line 1632 */
/* .local v0, "featureEnabled":Z */
(( com.android.server.audio.SpatializerHelper ) p2 ).setFeatureEnabled ( v0 ); // invoke-virtual {p2, v0}, Lcom/android/server/audio/SpatializerHelper;->setFeatureEnabled(Z)V
/* .line 1633 */
return;
} // .end method
public void startAudioGameEffect ( android.content.Context p0 ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 1355 */
/* new-instance v0, Lcom/android/server/audio/AudioGameEffect; */
/* invoke-direct {v0, p1}, Lcom/android/server/audio/AudioGameEffect;-><init>(Landroid/content/Context;)V */
this.mAudioGameEffect = v0;
/* .line 1356 */
return;
} // .end method
public void startAudioPowerSaveModeObserver ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 1367 */
/* iget v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mSettingAudioPowerSave:I */
/* and-int/lit8 v0, v0, 0x1 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1368 */
/* new-instance v0, Lcom/android/server/audio/AudioPowerSaveModeObserver; */
v1 = this.mContext;
/* invoke-direct {v0, v1}, Lcom/android/server/audio/AudioPowerSaveModeObserver;-><init>(Landroid/content/Context;)V */
this.mAudioPowerSaveModeObserver = v0;
/* .line 1370 */
} // :cond_0
return;
} // .end method
public void startAudioQueryWeatherService ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 310 */
this.mContext = p1;
/* .line 311 */
/* new-instance v0, Lcom/android/server/audio/AudioQueryWeatherService; */
v1 = this.mContext;
/* invoke-direct {v0, v1}, Lcom/android/server/audio/AudioQueryWeatherService;-><init>(Landroid/content/Context;)V */
this.mAudioQueryWeatherService = v0;
/* .line 312 */
(( com.android.server.audio.AudioQueryWeatherService ) v0 ).onCreate ( ); // invoke-virtual {v0}, Lcom/android/server/audio/AudioQueryWeatherService;->onCreate()V
/* .line 314 */
/* new-instance v0, Lcom/android/server/audio/CloudServiceThread; */
v1 = this.mContext;
/* invoke-direct {v0, v1}, Lcom/android/server/audio/CloudServiceThread;-><init>(Landroid/content/Context;)V */
this.mCloudService = v0;
/* .line 315 */
(( com.android.server.audio.CloudServiceThread ) v0 ).start ( ); // invoke-virtual {v0}, Lcom/android/server/audio/CloudServiceThread;->start()V
/* .line 316 */
return;
} // .end method
public void startAudioThermalObserver ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 1381 */
/* iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportCinemaMode:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1382 */
/* new-instance v0, Lcom/android/server/audio/AudioThermalObserver; */
v1 = this.mContext;
/* invoke-direct {v0, v1}, Lcom/android/server/audio/AudioThermalObserver;-><init>(Landroid/content/Context;)V */
this.mAudioThermalObserver = v0;
/* .line 1384 */
} // :cond_0
return;
} // .end method
public void startCameraRecordService ( android.content.Context p0, android.media.AudioRecordingConfiguration p1, Integer p2, Integer p3 ) {
/* .locals 9 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "audioConfig" # Landroid/media/AudioRecordingConfiguration; */
/* .param p3, "eventType" # I */
/* .param p4, "riidNow" # I */
/* .line 1204 */
/* iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportedCameraRecord:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
if ( p2 != null) { // if-eqz p2, :cond_2
/* .line 1205 */
/* iget v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->cameraToastServiceRiid:I */
final String v1 = "AudioServiceStubImpl"; // const-string v1, "AudioServiceStubImpl"
/* if-ne p4, v0, :cond_0 */
/* .line 1206 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "the riid is exist, do not startCameraRecordService again " */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v0 );
/* .line 1207 */
return;
/* .line 1209 */
} // :cond_0
/* iput p4, p0, Lcom/android/server/audio/AudioServiceStubImpl;->cameraToastServiceRiid:I */
/* .line 1210 */
/* nop */
/* .line 1211 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1210 */
final String v2 = "audio_headset_state"; // const-string v2, "audio_headset_state"
int v3 = -1; // const/4 v3, -0x1
v0 = android.provider.Settings$Global .getInt ( v0,v2,v3 );
/* .line 1212 */
/* .local v0, "cameraAudioHeadsetState":I */
(( android.media.AudioRecordingConfiguration ) p2 ).getClientPackageName ( ); // invoke-virtual {p2}, Landroid/media/AudioRecordingConfiguration;->getClientPackageName()Ljava/lang/String;
final String v3 = "com.android.camera"; // const-string v3, "com.android.camera"
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_2 */
/* .line 1214 */
v4 = (( android.media.AudioRecordingConfiguration ) p2 ).getClientAudioSource ( ); // invoke-virtual {p2}, Landroid/media/AudioRecordingConfiguration;->getClientAudioSource()I
int v5 = 5; // const/4 v5, 0x5
/* if-ne v4, v5, :cond_2 */
/* .line 1216 */
try { // :try_start_0
/* new-instance v4, Landroid/content/Intent; */
/* invoke-direct {v4}, Landroid/content/Intent;-><init>()V */
/* .line 1217 */
/* .local v4, "intent":Landroid/content/Intent; */
/* new-instance v5, Landroid/content/ComponentName; */
final String v6 = "com.miui.audiomonitor"; // const-string v6, "com.miui.audiomonitor"
final String v7 = "com.miui.audiomonitor.MiuiCameraBTRecordService"; // const-string v7, "com.miui.audiomonitor.MiuiCameraBTRecordService"
/* invoke-direct {v5, v6, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( android.content.Intent ) v4 ).setComponent ( v5 ); // invoke-virtual {v4, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 1219 */
final String v5 = "packageName"; // const-string v5, "packageName"
(( android.content.Intent ) v4 ).putExtra ( v5, v3 ); // invoke-virtual {v4, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1220 */
(( android.media.AudioRecordingConfiguration ) p2 ).getAudioDevice ( ); // invoke-virtual {p2}, Landroid/media/AudioRecordingConfiguration;->getAudioDevice()Landroid/media/AudioDeviceInfo;
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 1221 */
(( android.media.AudioRecordingConfiguration ) p2 ).getAudioDevice ( ); // invoke-virtual {p2}, Landroid/media/AudioRecordingConfiguration;->getAudioDevice()Landroid/media/AudioDeviceInfo;
v5 = (( android.media.AudioDeviceInfo ) v5 ).getType ( ); // invoke-virtual {v5}, Landroid/media/AudioDeviceInfo;->getType()I
/* .line 1222 */
/* .local v5, "deviceType":I */
final String v6 = "deviceType"; // const-string v6, "deviceType"
(( android.content.Intent ) v4 ).putExtra ( v6, v5 ); // invoke-virtual {v4, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1223 */
final String v6 = "packageName %s deviceType %d eventType %d"; // const-string v6, "packageName %s deviceType %d eventType %d"
int v7 = 3; // const/4 v7, 0x3
/* new-array v7, v7, [Ljava/lang/Object; */
int v8 = 0; // const/4 v8, 0x0
/* aput-object v3, v7, v8 */
/* .line 1224 */
java.lang.Integer .valueOf ( v5 );
/* aput-object v3, v7, v2 */
java.lang.Integer .valueOf ( p3 );
int v3 = 2; // const/4 v3, 0x2
/* aput-object v2, v7, v3 */
/* .line 1223 */
java.lang.String .format ( v6,v7 );
android.util.Log .d ( v1,v2 );
/* .line 1226 */
} // .end local v5 # "deviceType":I
} // :cond_1
final String v2 = "eventType"; // const-string v2, "eventType"
(( android.content.Intent ) v4 ).putExtra ( v2, p3 ); // invoke-virtual {v4, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1227 */
(( android.content.Context ) p1 ).startForegroundService ( v4 ); // invoke-virtual {p1, v4}, Landroid/content/Context;->startForegroundService(Landroid/content/Intent;)Landroid/content/ComponentName;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1230 */
/* nop */
} // .end local v4 # "intent":Landroid/content/Intent;
/* .line 1228 */
/* :catch_0 */
/* move-exception v2 */
/* .line 1229 */
/* .local v2, "e":Ljava/lang/Exception; */
final String v3 = "fail to startCameraRecordService "; // const-string v3, "fail to startCameraRecordService "
android.util.Log .e ( v1,v3 );
/* .line 1233 */
} // .end local v0 # "cameraAudioHeadsetState":I
} // .end local v2 # "e":Ljava/lang/Exception;
} // :cond_2
} // :goto_0
return;
} // .end method
public void startDolbyEffectController ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 1650 */
/* iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportedDolbyEffectControl:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1651 */
final String v0 = "AudioServiceStubImpl"; // const-string v0, "AudioServiceStubImpl"
/* const-string/jumbo v1, "startDolbyEffectControl" */
android.util.Log .d ( v0,v1 );
/* .line 1652 */
com.android.server.audio.dolbyeffect.DolbyEffectController .getInstance ( p1 );
(( com.android.server.audio.dolbyeffect.DolbyEffectController ) v0 ).init ( ); // invoke-virtual {v0}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->init()V
/* .line 1654 */
} // :cond_0
return;
} // .end method
public void startGameAudioEnhancer ( ) {
/* .locals 3 */
/* .line 1360 */
final String v0 = "ro.vendor.audio.game.mode"; // const-string v0, "ro.vendor.audio.game.mode"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* if-nez v0, :cond_0 */
/* .line 1361 */
final String v0 = "ro.vendor.audio.game.effect"; // const-string v0, "ro.vendor.audio.game.effect"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1362 */
} // :cond_0
/* new-instance v0, Lcom/android/server/audio/GameAudioEnhancer; */
v1 = this.mContext;
v2 = this.mWorkerThread;
(( android.os.HandlerThread ) v2 ).getLooper ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, v1, v2}, Lcom/android/server/audio/GameAudioEnhancer;-><init>(Landroid/content/Context;Landroid/os/Looper;)V */
this.mGameAudioEnhancer = v0;
/* .line 1364 */
} // :cond_1
return;
} // .end method
public void startMqsServer ( android.content.Context p0 ) {
/* .locals 0 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 679 */
com.android.server.audio.MQSserver .getInstance ( p1 );
/* .line 680 */
return;
} // .end method
public void startPollAudioMicStatus ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 381 */
final String v0 = "AudioServiceStubImpl"; // const-string v0, "AudioServiceStubImpl"
/* const-string/jumbo v1, "startPollAudioMicStatus" */
android.util.Log .d ( v0,v1 );
/* .line 382 */
/* iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->isSupportPollAudioMicStatus:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 383 */
com.android.server.audio.AudioDeviceMoniter .getInstance ( p1 );
(( com.android.server.audio.AudioDeviceMoniter ) v0 ).startPollAudioMicStatus ( ); // invoke-virtual {v0}, Lcom/android/server/audio/AudioDeviceMoniter;->startPollAudioMicStatus()V
/* .line 385 */
} // :cond_0
return;
} // .end method
public void stopCameraRecordService ( android.content.Context p0, android.media.AudioRecordingConfiguration p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "audioConfig" # Landroid/media/AudioRecordingConfiguration; */
/* .param p3, "riid" # I */
/* .line 1237 */
/* iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportedCameraRecord:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1238 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "stopCameraRecordService riidNow " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AudioServiceStubImpl"; // const-string v1, "AudioServiceStubImpl"
android.util.Log .d ( v1,v0 );
/* .line 1239 */
if ( p2 != null) { // if-eqz p2, :cond_0
(( android.media.AudioRecordingConfiguration ) p2 ).getClientPackageName ( ); // invoke-virtual {p2}, Landroid/media/AudioRecordingConfiguration;->getClientPackageName()Ljava/lang/String;
final String v1 = "com.android.camera"; // const-string v1, "com.android.camera"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1240 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->cameraToastServiceRiid:I */
/* .line 1243 */
} // :cond_0
return;
} // .end method
public Boolean superVoiceVolumeChanged ( Integer p0, Integer p1, Integer p2, java.lang.String p3, Integer p4, Integer p5, Integer p6, android.content.Context p7 ) {
/* .locals 3 */
/* .param p1, "device" # I */
/* .param p2, "alias" # I */
/* .param p3, "mode" # I */
/* .param p4, "pkg" # Ljava/lang/String; */
/* .param p5, "dir" # I */
/* .param p6, "curIdx" # I */
/* .param p7, "maxIdx" # I */
/* .param p8, "context" # Landroid/content/Context; */
/* .line 477 */
int v0 = 0; // const/4 v0, 0x0
/* .line 478 */
/* .local v0, "isChanged":Z */
int v1 = 2; // const/4 v1, 0x2
/* if-ne p1, v1, :cond_2 */
/* if-nez p2, :cond_2 */
/* if-eq p3, v1, :cond_0 */
int v1 = 3; // const/4 v1, 0x3
/* if-ne p3, v1, :cond_2 */
} // :cond_0
/* iget-boolean v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mSuperVoiceVolumeSupported:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 481 */
v1 = (( com.android.server.audio.AudioServiceStubImpl ) p0 ).isCtsVerifier ( p4 ); // invoke-virtual {p0, p4}, Lcom/android/server/audio/AudioServiceStubImpl;->isCtsVerifier(Ljava/lang/String;)Z
/* if-nez v1, :cond_2 */
/* .line 482 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "SuperVoiceVolumeChanged device="; // const-string v2, "SuperVoiceVolumeChanged device="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " stream="; // const-string v2, " stream="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " mode="; // const-string v2, " mode="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " dir="; // const-string v2, " dir="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p5 ); // invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " cur="; // const-string v2, " cur="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p6 ); // invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " max="; // const-string v2, " max="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p7 ); // invoke-virtual {v1, p7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "AudioServiceStubImpl"; // const-string v2, "AudioServiceStubImpl"
android.util.Log .d ( v2,v1 );
/* .line 484 */
int v1 = 1; // const/4 v1, 0x1
/* if-ne p5, v1, :cond_1 */
/* add-int/lit8 v2, p6, 0xa */
/* if-ne v2, p7, :cond_1 */
/* iget-boolean v2, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mSuperVoiceVolumeOn:Z */
/* if-nez v2, :cond_1 */
/* .line 485 */
/* invoke-direct {p0, v1, p8}, Lcom/android/server/audio/AudioServiceStubImpl;->setSuperVoiceVolume(ZLandroid/content/Context;)V */
/* .line 486 */
int v0 = 1; // const/4 v0, 0x1
/* .line 487 */
} // :cond_1
int v1 = -1; // const/4 v1, -0x1
/* if-ne p5, v1, :cond_2 */
/* if-ne p6, p7, :cond_2 */
/* iget-boolean v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mSuperVoiceVolumeOn:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 488 */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {p0, v1, p8}, Lcom/android/server/audio/AudioServiceStubImpl;->setSuperVoiceVolume(ZLandroid/content/Context;)V */
/* .line 489 */
int v0 = 1; // const/4 v0, 0x1
/* .line 492 */
} // :cond_2
} // :goto_0
} // .end method
public void updateAudioParameterClients ( android.os.IBinder p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "binder" # Landroid/os/IBinder; */
/* .param p2, "targetParameter" # Ljava/lang/String; */
/* .line 1254 */
/* invoke-direct {p0, p2}, Lcom/android/server/audio/AudioServiceStubImpl;->handleParameters(Ljava/lang/String;)V */
/* .line 1255 */
v0 = com.android.server.audio.AudioServiceStubImpl.TRANSMIT_AUDIO_PARAMETERS;
v0 = java.util.Arrays .asList ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1256 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/audio/AudioServiceStubImpl;->addAudioParameterClient(Landroid/os/IBinder;Ljava/lang/String;)Lcom/android/server/audio/AudioParameterClient; */
/* .line 1257 */
} // :cond_0
v0 = com.android.server.audio.AudioServiceStubImpl.DEFAULT_AUDIO_PARAMETERS;
v0 = java.util.Arrays .asList ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1258 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, p1, p2, v0}, Lcom/android/server/audio/AudioServiceStubImpl;->removeAudioParameterClient(Landroid/os/IBinder;Ljava/lang/String;Z)Lcom/android/server/audio/AudioParameterClient; */
/* .line 1260 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void updateBluetoothActiveDevice ( Integer p0, Integer p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "state" # I */
/* .param p2, "profile" # I */
/* .param p3, "name" # Ljava/lang/String; */
/* .line 1884 */
/* iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mIsSupportedReportAudioRouteState:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1885 */
int v0 = 2; // const/4 v0, 0x2
/* if-ne p2, v0, :cond_1 */
/* .line 1886 */
/* iput p1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mA2dpDeviceConnectedState:I */
/* .line 1887 */
/* if-ne p1, v0, :cond_0 */
/* .line 1888 */
this.mBtName = p3;
/* .line 1889 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mHeadsetDeviceConnectedState:I */
/* if-nez v0, :cond_1 */
/* .line 1890 */
final String v0 = ""; // const-string v0, ""
this.mBtName = v0;
/* .line 1894 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void updateConcurrentVoipInfo ( java.util.List p0 ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/audio/AudioService$SetModeDeathHandler;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 1480 */
/* .local p1, "setModeDeathHandlers":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/audio/AudioService$SetModeDeathHandler;>;" */
v0 = if ( p1 != null) { // if-eqz p1, :cond_1
int v1 = 1; // const/4 v1, 0x1
/* if-le v0, v1, :cond_1 */
/* .line 1481 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 1482 */
/* .local v0, "clients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_0
/* check-cast v3, Lcom/android/server/audio/AudioService$SetModeDeathHandler; */
/* .line 1483 */
/* .local v3, "handler":Lcom/android/server/audio/AudioService$SetModeDeathHandler; */
(( com.android.server.audio.AudioService$SetModeDeathHandler ) v3 ).getPackage ( ); // invoke-virtual {v3}, Lcom/android/server/audio/AudioService$SetModeDeathHandler;->getPackage()Ljava/lang/String;
/* .line 1484 */
} // .end local v3 # "handler":Lcom/android/server/audio/AudioService$SetModeDeathHandler;
/* .line 1485 */
} // :cond_0
v2 = this.mHandler;
(( android.os.Handler ) v2 ).obtainMessage ( v1, v0 ); // invoke-virtual {v2, v1, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( android.os.Message ) v1 ).sendToTarget ( ); // invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V
/* .line 1487 */
} // .end local v0 # "clients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_1
return;
} // .end method
public void updateCumulativePlaybackTime ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "start" # Z */
/* .line 1115 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* .line 1116 */
/* .local v0, "systemTime":J */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " updateCumulativePlaybackTime start="; // const-string v3, " updateCumulativePlaybackTime start="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = " systemTime="; // const-string v3, " systemTime="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "AudioServiceStubImpl"; // const-string v3, "AudioServiceStubImpl"
android.util.Log .d ( v3,v2 );
/* .line 1117 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 1118 */
/* iput-wide v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mCumulativePlaybackStartTime:J */
/* .line 1119 */
/* invoke-direct {p0}, Lcom/android/server/audio/AudioServiceStubImpl;->persistCumulativePlaybackStartMsToSettings()V */
/* .line 1121 */
} // :cond_0
/* iput-wide v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mCumulativePlaybackEndTime:J */
/* .line 1123 */
} // :goto_0
return;
} // .end method
public void updateModeOwnerPid ( Integer p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "pid" # I */
/* .param p2, "eventSource" # Ljava/lang/String; */
/* .line 1799 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "updateModeOwnerPid, pid: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", eventSource: "; // const-string v1, ", eventSource: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AudioServiceStubImpl"; // const-string v1, "AudioServiceStubImpl"
android.util.Log .d ( v1,v0 );
/* .line 1800 */
/* iput p1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mModeOwnerPid:I */
/* .line 1801 */
/* invoke-direct {p0, p1}, Lcom/android/server/audio/AudioServiceStubImpl;->clearSetScoCommunicationDevice(I)V */
/* .line 1802 */
/* invoke-direct {p0, p2}, Lcom/android/server/audio/AudioServiceStubImpl;->notifyBtStopBluetoothSco(Ljava/lang/String;)V */
/* .line 1803 */
return;
} // .end method
public void updateNotificationMode ( android.content.Context p0 ) {
/* .locals 0 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 908 */
android.media.AudioServiceInjector .updateNotificationMode ( p1 );
/* .line 909 */
return;
} // .end method
public void updatePlaybackTime ( Boolean p0 ) {
/* .locals 7 */
/* .param p1, "start" # Z */
/* .line 1127 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* .line 1128 */
/* .local v0, "systemTime":J */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 1129 */
/* iput-wide v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mPlaybackStartTime:J */
/* .line 1131 */
} // :cond_0
/* iput-wide v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mPlaybackEndTime:J */
/* .line 1133 */
} // :goto_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " updatePlaybackTime start="; // const-string v3, " updatePlaybackTime start="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = " systemTime="; // const-string v3, " systemTime="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v3 = " throd="; // const-string v3, " throd="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v3, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mPlaybackEndTime:J */
/* iget-wide v5, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mPlaybackStartTime:J */
/* sub-long/2addr v3, v5 */
/* .line 1134 */
java.lang.Math .abs ( v3,v4 );
/* move-result-wide v3 */
(( java.lang.StringBuilder ) v2 ).append ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1133 */
final String v3 = "AudioServiceStubImpl"; // const-string v3, "AudioServiceStubImpl"
android.util.Log .d ( v3,v2 );
/* .line 1135 */
return;
} // .end method
public void updateVolumeBoostState ( Integer p0, Integer p1, android.content.Context p2 ) {
/* .locals 2 */
/* .param p1, "audioMode" # I */
/* .param p2, "modeOwnerPid" # I */
/* .param p3, "context" # Landroid/content/Context; */
/* .line 463 */
int v0 = 2; // const/4 v0, 0x2
/* if-ne p1, v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mVolumeBoostEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 464 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0, p3}, Lcom/android/server/audio/AudioServiceStubImpl;->setVolumeBoost(ZLandroid/content/Context;)V */
/* .line 465 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* .line 467 */
miui.tipclose.TipHelperProxy .getInstance ( );
(( miui.tipclose.TipHelperProxy ) v0 ).hideTipForPhone ( ); // invoke-virtual {v0}, Lmiui/tipclose/TipHelperProxy;->hideTipForPhone()V
/* .line 469 */
} // :cond_1
} // :goto_0
/* iput p1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioMode:I */
/* .line 471 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "updateVolumeBoostState audiomode " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/audio/AudioServiceStubImpl;->mAudioMode:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AudioServiceStubImpl"; // const-string v1, "AudioServiceStubImpl"
android.util.Log .d ( v1,v0 );
/* .line 472 */
return;
} // .end method
public void uriState ( android.net.Uri p0, android.content.Context p1, android.content.ContentResolver p2 ) {
/* .locals 1 */
/* .param p1, "uri" # Landroid/net/Uri; */
/* .param p2, "context" # Landroid/content/Context; */
/* .param p3, "contentResolver" # Landroid/content/ContentResolver; */
/* .line 599 */
/* const-string/jumbo v0, "sound_assist_key" */
android.provider.Settings$Global .getUriFor ( v0 );
v0 = (( android.net.Uri ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 600 */
com.android.server.audio.PlaybackActivityMonitorStub .get ( );
/* .line 601 */
com.android.server.audio.PlaybackActivityMonitorStub .get ( );
/* .line 602 */
return;
/* .line 605 */
} // :cond_0
final String v0 = "key_ignore_music_focus_req"; // const-string v0, "key_ignore_music_focus_req"
android.provider.Settings$Global .getUriFor ( v0 );
v0 = (( android.net.Uri ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 606 */
com.android.server.audio.PlaybackActivityMonitorStub .get ( );
/* .line 608 */
} // :cond_1
return;
} // .end method
