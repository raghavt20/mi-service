class com.android.server.audio.MiAudioService$1 extends android.database.ContentObserver {
	 /* .source "MiAudioService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/audio/MiAudioService;->SettingsObserver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.audio.MiAudioService this$0; //synthetic
/* # direct methods */
 com.android.server.audio.MiAudioService$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/audio/MiAudioService; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 136 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 5 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 139 */
/* invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V */
/* .line 140 */
v0 = this.this$0;
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "effect_implementer"; // const-string v1, "effect_implementer"
android.provider.Settings$Global .getString ( v0,v1 );
/* .line 141 */
/* .local v0, "effecImplementer":Ljava/lang/String; */
final String v1 = "dolby"; // const-string v1, "dolby"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 142 */
	 v1 = this.this$0;
	 v2 = com.android.server.audio.MiAudioService$SpatializerType.DOLBY;
	 v2 = 	 (( com.android.server.audio.MiAudioService$SpatializerType ) v2 ).ordinal ( ); // invoke-virtual {v2}, Lcom/android/server/audio/MiAudioService$SpatializerType;->ordinal()I
	 com.android.server.audio.MiAudioService .-$$Nest$fputmSpatilizerType ( v1,v2 );
	 /* .line 143 */
} // :cond_0
final String v1 = "misound"; // const-string v1, "misound"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 144 */
	 v1 = this.this$0;
	 v2 = com.android.server.audio.MiAudioService$SpatializerType.MISOUND;
	 v2 = 	 (( com.android.server.audio.MiAudioService$SpatializerType ) v2 ).ordinal ( ); // invoke-virtual {v2}, Lcom/android/server/audio/MiAudioService$SpatializerType;->ordinal()I
	 com.android.server.audio.MiAudioService .-$$Nest$fputmSpatilizerType ( v1,v2 );
	 /* .line 145 */
} // :cond_1
final String v1 = "none"; // const-string v1, "none"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
	 /* .line 146 */
	 v1 = this.this$0;
	 v2 = com.android.server.audio.MiAudioService$SpatializerType.NONE;
	 v2 = 	 (( com.android.server.audio.MiAudioService$SpatializerType ) v2 ).ordinal ( ); // invoke-virtual {v2}, Lcom/android/server/audio/MiAudioService$SpatializerType;->ordinal()I
	 com.android.server.audio.MiAudioService .-$$Nest$fputmSpatilizerType ( v1,v2 );
	 /* .line 148 */
} // :cond_2
} // :goto_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Current EffectImplementer is "; // const-string v2, "Current EffectImplementer is "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiAudioService"; // const-string v2, "MiAudioService"
android.util.Log .d ( v2,v1 );
/* .line 149 */
v1 = this.this$0;
v1 = com.android.server.audio.MiAudioService .-$$Nest$fgetmUseXiaoMiSpatilizer ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_3
v1 = this.this$0;
v1 = (( com.android.server.audio.MiAudioService ) v1 ).isSpatializerEnabled ( ); // invoke-virtual {v1}, Lcom/android/server/audio/MiAudioService;->isSpatializerEnabled()Z
if ( v1 != null) { // if-eqz v1, :cond_3
	 v1 = this.this$0;
	 v1 = 	 (( com.android.server.audio.MiAudioService ) v1 ).isSpatializerAvailable ( ); // invoke-virtual {v1}, Lcom/android/server/audio/MiAudioService;->isSpatializerAvailable()Z
	 if ( v1 != null) { // if-eqz v1, :cond_3
		 /* .line 150 */
		 /* new-instance v1, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v3 = "SettingsObserver setSpatializerParameter spatilizerType = "; // const-string v3, "SettingsObserver setSpatializerParameter spatilizerType = "
		 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v3 = this.this$0;
		 v3 = 		 com.android.server.audio.MiAudioService .-$$Nest$fgetmSpatilizerType ( v3 );
		 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Log .d ( v2,v1 );
		 /* .line 152 */
		 try { // :try_start_0
			 v1 = this.this$0;
			 v3 = 			 com.android.server.audio.MiAudioService .-$$Nest$fgetmSpatilizerType ( v1 );
			 com.android.server.audio.MiAudioService .-$$Nest$mintToBytes ( v1,v3 );
			 /* const/16 v4, 0x120 */
			 (( com.android.server.audio.MiAudioService ) v1 ).setSpatializerParameter ( v4, v3 ); // invoke-virtual {v1, v4, v3}, Lcom/android/server/audio/MiAudioService;->setSpatializerParameter(I[B)V
			 /* :try_end_0 */
			 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 155 */
			 /* .line 153 */
			 /* :catch_0 */
			 /* move-exception v1 */
			 /* .line 154 */
			 /* .local v1, "e":Ljava/lang/Exception; */
			 /* new-instance v3, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v4 = "SettingsObserver Exception "; // const-string v4, "SettingsObserver Exception "
			 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 android.util.Log .e ( v2,v3 );
			 /* .line 157 */
		 } // .end local v1 # "e":Ljava/lang/Exception;
	 } // :cond_3
} // :goto_1
return;
} // .end method
