public class com.android.server.audio.AudioPowerSaveModeObserver {
	 /* .source "AudioPowerSaveModeObserver.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener;, */
	 /* Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String ACTION_POWER_SAVE_MODE_CHANGED;
private static final java.lang.String AUDIO_POWER_SAVE_LEVEL_PATH;
private static final java.lang.String AUDIO_POWER_SAVE_SETTING;
private static final java.lang.String AUDIO_POWER_SAVE_STATE_PATH;
private static final java.lang.String KEY_GLOBAL_VOICE_TRIGGER_ENABLED;
private static final java.lang.String METHOD_START_RECOGNITION;
private static final java.lang.String METHOD_STOP_RECOGNITION;
public static final Integer MSG_AUDIO_POWER_SAVE_POLICY;
public static final Integer MSG_CHECK_AUDIO_POWER_SAVE_POLICY;
public static final Integer MSG_CLEAR_AUDIO_POWER_SAVE_POLICY;
public static final Integer MSG_SET_AUDIO_POWER_SAVE_LEVEL_POLICY;
public static final Integer MSG_SET_AUDIO_POWER_SAVE_MODE_POLICY;
private static final Integer READ_WAIT_TIME_SECONDS;
private static final java.lang.String TAG;
private static final Integer VALUE_GLOBAL_VOICE_TRIGGER_DISABLE;
private static final Integer VALUE_GLOBAL_VOICE_TRIGGER_ENABLED;
private static Integer mIsVoiceTriggerEnableForAudioPowerSave;
private static Integer mIsVoiceTriggerEnableForGlobal;
/* # instance fields */
private com.android.server.audio.AudioPowerSaveModeObserver$AudioPowerSaveStateListener mAudioPowerLevelListener;
private android.content.BroadcastReceiver mAudioPowerSaveBroadcastReceiver;
private com.android.server.audio.AudioPowerSaveModeObserver$AudioPowerSaveStateListener mAudioPowerSaveListener;
private android.content.Context mContext;
private com.android.server.audio.AudioPowerSaveModeObserver$WorkHandler mHandler;
private Integer mLevelRecord;
private Integer mModeRecord;
private android.net.Uri mUri;
private Integer mVoiceTriggerRecord;
/* # direct methods */
static com.android.server.audio.AudioPowerSaveModeObserver$WorkHandler -$$Nest$fgetmHandler ( com.android.server.audio.AudioPowerSaveModeObserver p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mHandler;
} // .end method
static void -$$Nest$mcheckAudioPowerSaveLevel ( com.android.server.audio.AudioPowerSaveModeObserver p0, java.lang.String p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->checkAudioPowerSaveLevel(Ljava/lang/String;)V */
	 return;
} // .end method
static Integer -$$Nest$mcheckAudioPowerSaveState ( com.android.server.audio.AudioPowerSaveModeObserver p0, java.lang.String p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = 	 /* invoke-direct {p0, p1}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->checkAudioPowerSaveState(Ljava/lang/String;)I */
} // .end method
static void -$$Nest$msetAudioPowerSaveLevel ( com.android.server.audio.AudioPowerSaveModeObserver p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->setAudioPowerSaveLevel(I)V */
	 return;
} // .end method
static void -$$Nest$msetAudioPowerSaveMode ( com.android.server.audio.AudioPowerSaveModeObserver p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->setAudioPowerSaveMode(I)V */
	 return;
} // .end method
static void -$$Nest$mtoSleep ( com.android.server.audio.AudioPowerSaveModeObserver p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->toSleep()V */
	 return;
} // .end method
static com.android.server.audio.AudioPowerSaveModeObserver ( ) {
	 /* .locals 1 */
	 /* .line 70 */
	 int v0 = -1; // const/4 v0, -0x1
	 /* .line 72 */
	 return;
} // .end method
public com.android.server.audio.AudioPowerSaveModeObserver ( ) {
	 /* .locals 3 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 76 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 48 */
	 int v0 = 0; // const/4 v0, 0x0
	 this.mAudioPowerSaveListener = v0;
	 /* .line 51 */
	 this.mAudioPowerLevelListener = v0;
	 /* .line 57 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput v0, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mModeRecord:I */
	 /* .line 58 */
	 /* iput v0, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mLevelRecord:I */
	 /* .line 59 */
	 /* iput v0, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mVoiceTriggerRecord:I */
	 /* .line 63 */
	 final String v1 = "content://com.miui.voicetrigger.SmModelProvider/mode/voicetrigger"; // const-string v1, "content://com.miui.voicetrigger.SmModelProvider/mode/voicetrigger"
	 android.net.Uri .parse ( v1 );
	 this.mUri = v1;
	 /* .line 395 */
	 /* new-instance v1, Lcom/android/server/audio/AudioPowerSaveModeObserver$1; */
	 /* invoke-direct {v1, p0}, Lcom/android/server/audio/AudioPowerSaveModeObserver$1;-><init>(Lcom/android/server/audio/AudioPowerSaveModeObserver;)V */
	 this.mAudioPowerSaveBroadcastReceiver = v1;
	 /* .line 77 */
	 /* iput v0, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mModeRecord:I */
	 /* .line 78 */
	 /* iput v0, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mLevelRecord:I */
	 /* .line 79 */
	 this.mContext = p1;
	 /* .line 80 */
	 /* new-instance v0, Landroid/os/HandlerThread; */
	 final String v1 = "AudioPowerSaveModeObserver"; // const-string v1, "AudioPowerSaveModeObserver"
	 /* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
	 /* .line 81 */
	 /* .local v0, "thread":Landroid/os/HandlerThread; */
	 (( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
	 /* .line 82 */
	 /* new-instance v1, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler; */
	 (( android.os.HandlerThread ) v0 ).getLooper ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
	 /* invoke-direct {v1, p0, v2}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;-><init>(Lcom/android/server/audio/AudioPowerSaveModeObserver;Landroid/os/Looper;)V */
	 this.mHandler = v1;
	 /* .line 83 */
	 /* new-instance v1, Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener; */
	 final String v2 = "/sys/class/thermal/power_save/powersave_mode"; // const-string v2, "/sys/class/thermal/power_save/powersave_mode"
	 /* invoke-direct {v1, p0, v2}, Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener;-><init>(Lcom/android/server/audio/AudioPowerSaveModeObserver;Ljava/lang/String;)V */
	 this.mAudioPowerSaveListener = v1;
	 /* .line 84 */
	 /* new-instance v1, Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener; */
	 final String v2 = "/sys/class/thermal/power_save/power_level"; // const-string v2, "/sys/class/thermal/power_save/power_level"
	 /* invoke-direct {v1, p0, v2}, Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener;-><init>(Lcom/android/server/audio/AudioPowerSaveModeObserver;Ljava/lang/String;)V */
	 this.mAudioPowerLevelListener = v1;
	 /* .line 85 */
	 /* invoke-direct {p0}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->watchAudioPowerStateListener()V */
	 /* .line 86 */
	 return;
} // .end method
private void checkAudioPowerSaveLevel ( java.lang.String p0 ) {
	 /* .locals 8 */
	 /* .param p1, "path" # Ljava/lang/String; */
	 /* .line 225 */
	 final String v0 = "AudioPowerSaveModeObserver"; // const-string v0, "AudioPowerSaveModeObserver"
	 int v1 = 0; // const/4 v1, 0x0
	 /* .line 226 */
	 /* .local v1, "mLevel":Ljava/lang/String; */
	 int v2 = 0; // const/4 v2, 0x0
	 /* .line 228 */
	 /* .local v2, "mPowerLevel":I */
	 try { // :try_start_0
		 /* invoke-direct {p0, p1}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->getContentFromFile(Ljava/lang/String;)Ljava/lang/String; */
		 /* move-object v1, v3 */
		 if ( v3 != null) { // if-eqz v3, :cond_2
			 /* .line 229 */
			 final String v3 = "0"; // const-string v3, "0"
			 v3 = 			 (( java.lang.String ) v3 ).equals ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 int v4 = 0; // const/4 v4, 0x0
			 /* if-nez v3, :cond_1 */
			 final String v3 = "-1"; // const-string v3, "-1"
			 v3 = 			 (( java.lang.String ) v3 ).equals ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 if ( v3 != null) { // if-eqz v3, :cond_0
				 /* .line 232 */
			 } // :cond_0
			 int v3 = 7; // const/4 v3, 0x7
			 int v5 = 6; // const/4 v5, 0x6
			 (( java.lang.String ) v1 ).substring ( v5, v3 ); // invoke-virtual {v1, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;
			 v6 = 			 java.lang.Integer .parseInt ( v6 );
			 /* move v2, v6 */
			 /* .line 233 */
			 /* new-instance v6, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v7 = "checkAudioPowerSaveLevel mPowerLevel is: "; // const-string v7, "checkAudioPowerSaveLevel mPowerLevel is: "
			 (( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
			 final String v7 = " in "; // const-string v7, " in "
			 (( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.String ) v1 ).substring ( v4, v5 ); // invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;
			 (( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 /* const-string/jumbo v7, "{" */
			 (( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 /* .line 234 */
			 (( java.lang.String ) v1 ).substring ( v5, v3 ); // invoke-virtual {v1, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;
			 (( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 /* const-string/jumbo v6, "}" */
			 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 /* const/16 v6, 0x9 */
			 (( java.lang.String ) v1 ).substring ( v3, v6 ); // invoke-virtual {v1, v3, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;
			 (( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 /* .line 233 */
			 android.util.Log .d ( v0,v3 );
			 /* .line 230 */
		 } // :cond_1
	 } // :goto_0
	 v3 = 	 java.lang.Integer .parseInt ( v1 );
	 /* move v2, v3 */
	 /* .line 236 */
} // :goto_1
v3 = this.mHandler;
/* const/16 v5, 0x9c6 */
(( com.android.server.audio.AudioPowerSaveModeObserver$WorkHandler ) v3 ).removeMessages ( v5 ); // invoke-virtual {v3, v5}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->removeMessages(I)V
/* .line 237 */
v3 = this.mHandler;
(( com.android.server.audio.AudioPowerSaveModeObserver$WorkHandler ) v3 ).obtainMessage ( v5, v2, v4 ); // invoke-virtual {v3, v5, v2, v4}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->obtainMessage(III)Landroid/os/Message;
/* const-wide/16 v5, 0x3e8 */
(( com.android.server.audio.AudioPowerSaveModeObserver$WorkHandler ) v3 ).sendMessageDelayed ( v4, v5, v6 ); // invoke-virtual {v3, v4, v5, v6}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 240 */
} // :cond_2
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "path:"; // const-string v4, "path:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " is null"; // const-string v4, " is null"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 244 */
} // :goto_2
/* .line 242 */
/* :catch_0 */
/* move-exception v3 */
/* .line 243 */
/* .local v3, "e":Ljava/lang/Exception; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "checkAudioPowerSaveLevel "; // const-string v5, "checkAudioPowerSaveLevel "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v4 );
/* .line 245 */
} // .end local v3 # "e":Ljava/lang/Exception;
} // :goto_3
return;
} // .end method
private Integer checkAudioPowerSaveState ( java.lang.String p0 ) {
/* .locals 8 */
/* .param p1, "path" # Ljava/lang/String; */
/* .line 205 */
final String v0 = "AudioPowerSaveModeObserver"; // const-string v0, "AudioPowerSaveModeObserver"
int v1 = 0; // const/4 v1, 0x0
/* .line 206 */
/* .local v1, "mState":Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* .line 208 */
/* .local v2, "mEnable":I */
try { // :try_start_0
/* invoke-direct {p0, p1}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->getContentFromFile(Ljava/lang/String;)Ljava/lang/String; */
/* move-object v1, v3 */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 209 */
v3 = java.lang.Integer .parseInt ( v1 );
/* .line 210 */
/* .local v3, "mPowerSave":I */
/* move v2, v3 */
/* .line 211 */
/* invoke-direct {p0}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->checkAudioPowerSaveVoiceTrigger()V */
/* .line 212 */
v4 = this.mHandler;
/* const/16 v5, 0x9c5 */
(( com.android.server.audio.AudioPowerSaveModeObserver$WorkHandler ) v4 ).removeMessages ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->removeMessages(I)V
/* .line 213 */
v4 = this.mHandler;
int v6 = 0; // const/4 v6, 0x0
(( com.android.server.audio.AudioPowerSaveModeObserver$WorkHandler ) v4 ).obtainMessage ( v5, v3, v6 ); // invoke-virtual {v4, v5, v3, v6}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->obtainMessage(III)Landroid/os/Message;
/* const-wide/16 v6, 0x3e8 */
(( com.android.server.audio.AudioPowerSaveModeObserver$WorkHandler ) v4 ).sendMessageDelayed ( v5, v6, v7 ); // invoke-virtual {v4, v5, v6, v7}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 215 */
/* nop */
} // .end local v3 # "mPowerSave":I
/* .line 216 */
} // :cond_0
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "path:"; // const-string v4, "path:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " is null"; // const-string v4, " is null"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 220 */
} // :goto_0
/* .line 218 */
/* :catch_0 */
/* move-exception v3 */
/* .line 219 */
/* .local v3, "e":Ljava/lang/Exception; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "checkAudioPowerSaveState "; // const-string v5, "checkAudioPowerSaveState "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v4 );
/* .line 221 */
} // .end local v3 # "e":Ljava/lang/Exception;
} // :goto_1
} // .end method
private void checkAudioPowerSaveVoiceTrigger ( ) {
/* .locals 3 */
/* .line 248 */
final String v0 = "AudioPowerSaveModeObserver"; // const-string v0, "AudioPowerSaveModeObserver"
final String v1 = "enter checkAudioPowerSaveVoiceTrigger"; // const-string v1, "enter checkAudioPowerSaveVoiceTrigger"
android.util.Log .d ( v0,v1 );
/* .line 249 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "global_voice_trigger_enabled"; // const-string v1, "global_voice_trigger_enabled"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$Secure .getInt ( v0,v1,v2 );
/* .line 250 */
/* .local v0, "isVoicetriggerenablel":I */
/* .line 251 */
return;
} // .end method
private java.lang.String getContentFromFile ( java.lang.String p0 ) {
/* .locals 7 */
/* .param p1, "filePath" # Ljava/lang/String; */
/* .line 283 */
final String v0 = "can not get temp state "; // const-string v0, "can not get temp state "
final String v1 = "AudioPowerSaveModeObserver"; // const-string v1, "AudioPowerSaveModeObserver"
int v2 = 0; // const/4 v2, 0x0
/* .line 284 */
/* .local v2, "is":Ljava/io/FileInputStream; */
final String v3 = ""; // const-string v3, ""
/* .line 286 */
/* .local v3, "content":Ljava/lang/String; */
try { // :try_start_0
/* new-instance v4, Ljava/io/File; */
/* invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 287 */
/* .local v4, "file":Ljava/io/File; */
/* new-instance v5, Ljava/io/FileInputStream; */
/* invoke-direct {v5, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* move-object v2, v5 */
/* .line 288 */
com.android.server.audio.AudioPowerSaveModeObserver .readInputStream ( v2 );
/* .line 289 */
/* .local v5, "data":[B */
/* new-instance v6, Ljava/lang/String; */
/* invoke-direct {v6, v5}, Ljava/lang/String;-><init>([B)V */
(( java.lang.String ) v6 ).trim ( ); // invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;
/* :try_end_0 */
/* .catch Ljava/io/FileNotFoundException; {:try_start_0 ..:try_end_0} :catch_5 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_3 */
/* .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move-object v3, v6 */
/* .line 297 */
} // .end local v4 # "file":Ljava/io/File;
} // .end local v5 # "data":[B
/* nop */
/* .line 299 */
try { // :try_start_1
(( java.io.FileInputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 302 */
} // :goto_0
/* goto/16 :goto_2 */
/* .line 300 */
/* :catch_0 */
/* move-exception v4 */
/* .line 301 */
/* .local v4, "e":Ljava/io/IOException; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
} // :goto_1
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .w ( v1,v0 );
} // .end local v4 # "e":Ljava/io/IOException;
/* .line 297 */
/* :catchall_0 */
/* move-exception v4 */
/* goto/16 :goto_3 */
/* .line 294 */
/* :catch_1 */
/* move-exception v4 */
/* .line 295 */
/* .local v4, "e":Ljava/lang/IndexOutOfBoundsException; */
try { // :try_start_2
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "index exception: "; // const-string v6, "index exception: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .w ( v1,v5 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 297 */
/* nop */
} // .end local v4 # "e":Ljava/lang/IndexOutOfBoundsException;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 299 */
try { // :try_start_3
(( java.io.FileInputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_2 */
/* .line 300 */
/* :catch_2 */
/* move-exception v4 */
/* .line 301 */
/* .local v4, "e":Ljava/io/IOException; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 292 */
} // .end local v4 # "e":Ljava/io/IOException;
/* :catch_3 */
/* move-exception v4 */
/* .line 293 */
/* .restart local v4 # "e":Ljava/io/IOException; */
try { // :try_start_4
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "IO exception when read file "; // const-string v6, "IO exception when read file "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .w ( v1,v5 );
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 297 */
/* nop */
} // .end local v4 # "e":Ljava/io/IOException;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 299 */
try { // :try_start_5
(( java.io.FileInputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
/* :try_end_5 */
/* .catch Ljava/io/IOException; {:try_start_5 ..:try_end_5} :catch_4 */
/* .line 300 */
/* :catch_4 */
/* move-exception v4 */
/* .line 301 */
/* .restart local v4 # "e":Ljava/io/IOException; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 290 */
} // .end local v4 # "e":Ljava/io/IOException;
/* :catch_5 */
/* move-exception v4 */
/* .line 291 */
/* .local v4, "e":Ljava/io/FileNotFoundException; */
try { // :try_start_6
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "can\'t find file "; // const-string v6, "can\'t find file "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .w ( v1,v5 );
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_0 */
/* .line 297 */
/* nop */
} // .end local v4 # "e":Ljava/io/FileNotFoundException;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 299 */
try { // :try_start_7
(( java.io.FileInputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
/* :try_end_7 */
/* .catch Ljava/io/IOException; {:try_start_7 ..:try_end_7} :catch_6 */
/* goto/16 :goto_0 */
/* .line 300 */
/* :catch_6 */
/* move-exception v4 */
/* .line 301 */
/* .local v4, "e":Ljava/io/IOException; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* goto/16 :goto_1 */
/* .line 305 */
} // .end local v4 # "e":Ljava/io/IOException;
} // :cond_0
} // :goto_2
/* .line 297 */
} // :goto_3
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 299 */
try { // :try_start_8
(( java.io.FileInputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
/* :try_end_8 */
/* .catch Ljava/io/IOException; {:try_start_8 ..:try_end_8} :catch_7 */
/* .line 302 */
/* .line 300 */
/* :catch_7 */
/* move-exception v5 */
/* .line 301 */
/* .local v5, "e":Ljava/io/IOException; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .w ( v1,v0 );
/* .line 304 */
} // .end local v5 # "e":Ljava/io/IOException;
} // :cond_1
} // :goto_4
/* throw v4 */
} // .end method
private Boolean isSupportSetVoiceTrigger ( java.lang.String p0 ) {
/* .locals 8 */
/* .param p1, "powersavemode" # Ljava/lang/String; */
/* .line 254 */
int v0 = 0; // const/4 v0, 0x0
int v1 = 1; // const/4 v1, 0x1
(( java.lang.String ) p1 ).substring ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* .line 255 */
/* .local v2, "voicetriggermode":Ljava/lang/String; */
int v3 = 2; // const/4 v3, 0x2
(( java.lang.String ) p1 ).substring ( v1, v3 ); // invoke-virtual {p1, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* .line 257 */
/* .local v3, "audiopowersavemode":Ljava/lang/String; */
final String v4 = "1"; // const-string v4, "1"
v5 = (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 258 */
v5 = java.lang.Integer .parseInt ( v2 );
/* iput v5, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mVoiceTriggerRecord:I */
/* .line 261 */
} // :cond_0
/* iget v5, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mVoiceTriggerRecord:I */
int v6 = -1; // const/4 v6, -0x1
final String v7 = "AudioPowerSaveModeObserver"; // const-string v7, "AudioPowerSaveModeObserver"
/* if-nez v5, :cond_1 */
/* .line 262 */
final String v1 = "VoiceTriggerMode is disable now, do not setVoiceTrigger"; // const-string v1, "VoiceTriggerMode is disable now, do not setVoiceTrigger"
android.util.Log .d ( v7,v1 );
/* .line 263 */
/* .line 264 */
/* .line 267 */
} // :cond_1
v4 = (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_2
/* if-nez v4, :cond_2 */
/* .line 268 */
final String v1 = "open audiopowersavemode, VoiceTrigger is disable now, do not setVoiceTrigger"; // const-string v1, "open audiopowersavemode, VoiceTrigger is disable now, do not setVoiceTrigger"
android.util.Log .d ( v7,v1 );
/* .line 269 */
/* .line 270 */
/* .line 273 */
} // :cond_2
final String v4 = "0"; // const-string v4, "0"
v4 = (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_3
/* if-eq v4, v5, :cond_3 */
/* .line 274 */
final String v1 = "close powersavemode, VoiceTrigger is diff now, do not setVoiceTrigger"; // const-string v1, "close powersavemode, VoiceTrigger is diff now, do not setVoiceTrigger"
android.util.Log .d ( v7,v1 );
/* .line 275 */
/* .line 276 */
/* .line 279 */
} // :cond_3
} // .end method
private static readInputStream ( java.io.FileInputStream p0 ) {
/* .locals 9 */
/* .param p0, "is" # Ljava/io/FileInputStream; */
/* .line 354 */
final String v0 = "readInputStream "; // const-string v0, "readInputStream "
final String v1 = "AudioPowerSaveModeObserver"; // const-string v1, "AudioPowerSaveModeObserver"
/* new-instance v2, Ljava/io/ByteArrayOutputStream; */
/* invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V */
/* .line 355 */
/* .local v2, "byteStream":Ljava/io/ByteArrayOutputStream; */
/* const/16 v3, 0x200 */
/* .line 356 */
/* .local v3, "blockSize":I */
/* const/16 v4, 0x200 */
/* new-array v5, v4, [B */
/* .line 357 */
/* .local v5, "buffer":[B */
int v6 = 0; // const/4 v6, 0x0
/* .line 359 */
/* .local v6, "count":I */
} // :goto_0
int v7 = 0; // const/4 v7, 0x0
try { // :try_start_0
v8 = (( java.io.FileInputStream ) p0 ).read ( v5, v7, v4 ); // invoke-virtual {p0, v5, v7, v4}, Ljava/io/FileInputStream;->read([BII)I
/* move v6, v8 */
/* if-lez v8, :cond_0 */
/* .line 360 */
(( java.io.ByteArrayOutputStream ) v2 ).write ( v5, v7, v6 ); // invoke-virtual {v2, v5, v7, v6}, Ljava/io/ByteArrayOutputStream;->write([BII)V
/* .line 362 */
} // :cond_0
(( java.io.ByteArrayOutputStream ) v2 ).toByteArray ( ); // invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 367 */
try { // :try_start_1
(( java.io.ByteArrayOutputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 370 */
/* .line 368 */
/* :catch_0 */
/* move-exception v7 */
/* .line 369 */
/* .local v7, "e":Ljava/io/IOException; */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v0 ); // invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .w ( v1,v0 );
/* .line 362 */
} // .end local v7 # "e":Ljava/io/IOException;
} // :goto_1
/* .line 366 */
/* :catchall_0 */
/* move-exception v4 */
/* .line 363 */
/* :catch_1 */
/* move-exception v4 */
/* .line 364 */
/* .local v4, "e":Ljava/lang/Exception; */
try { // :try_start_2
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .w ( v1,v7 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 367 */
} // .end local v4 # "e":Ljava/lang/Exception;
try { // :try_start_3
(( java.io.ByteArrayOutputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_2 */
/* .line 370 */
/* .line 368 */
/* :catch_2 */
/* move-exception v4 */
/* .line 369 */
/* .local v4, "e":Ljava/io/IOException; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .w ( v1,v0 );
/* .line 371 */
} // .end local v4 # "e":Ljava/io/IOException;
/* nop */
/* .line 372 */
} // :goto_2
int v0 = 0; // const/4 v0, 0x0
/* .line 367 */
} // :goto_3
try { // :try_start_4
(( java.io.ByteArrayOutputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_3 */
/* .line 370 */
/* .line 368 */
/* :catch_3 */
/* move-exception v7 */
/* .line 369 */
/* .restart local v7 # "e":Ljava/io/IOException; */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v0 ); // invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .w ( v1,v0 );
/* .line 371 */
} // .end local v7 # "e":Ljava/io/IOException;
} // :goto_4
/* throw v4 */
} // .end method
private void registerBroadCastReceiver ( ) {
/* .locals 4 */
/* .line 377 */
final String v0 = "AudioPowerSaveModeObserver"; // const-string v0, "AudioPowerSaveModeObserver"
try { // :try_start_0
/* new-instance v1, Landroid/content/IntentFilter; */
/* invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V */
/* .line 378 */
/* .local v1, "mIntentFilter":Landroid/content/IntentFilter; */
final String v2 = "miui.intent.action.POWER_SAVE_MODE_CHANGED"; // const-string v2, "miui.intent.action.POWER_SAVE_MODE_CHANGED"
(( android.content.IntentFilter ) v1 ).addAction ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 379 */
v2 = this.mContext;
v3 = this.mAudioPowerSaveBroadcastReceiver;
(( android.content.Context ) v2 ).registerReceiver ( v3, v1 ); // invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 380 */
final String v2 = "registerBroadCastReceiver"; // const-string v2, "registerBroadCastReceiver"
android.util.Log .d ( v0,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 383 */
/* nop */
} // .end local v1 # "mIntentFilter":Landroid/content/IntentFilter;
/* .line 381 */
/* :catch_0 */
/* move-exception v1 */
/* .line 382 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "registerBroadCastReceiver exception "; // const-string v3, "registerBroadCastReceiver exception "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v2 );
/* .line 384 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void setAudioPowerSaveLevel ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "mPowerSaveLevel" # I */
/* .line 160 */
int v0 = -1; // const/4 v0, -0x1
/* if-ne p1, v0, :cond_0 */
int p1 = 0; // const/4 p1, 0x0
/* .line 162 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mModeRecord:I */
final String v1 = "AudioPowerSaveModeObserver"; // const-string v1, "AudioPowerSaveModeObserver"
/* if-nez v0, :cond_1 */
/* .line 163 */
final String v0 = "PowerMode is close, setPowerLevel 0"; // const-string v0, "PowerMode is close, setPowerLevel 0"
android.util.Log .d ( v1,v0 );
/* .line 164 */
int p1 = 0; // const/4 p1, 0x0
/* .line 166 */
} // :cond_1
/* iget v0, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mLevelRecord:I */
/* if-eq v0, p1, :cond_2 */
/* .line 168 */
try { // :try_start_0
java.lang.String .valueOf ( p1 );
/* .line 169 */
/* .local v0, "level":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Audio_Power_Save_Level="; // const-string v3, "Audio_Power_Save_Level="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 170 */
/* .local v2, "mParameter":Ljava/lang/String; */
android.media.AudioSystem .setParameters ( v2 );
/* .line 171 */
/* iput p1, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mLevelRecord:I */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 174 */
} // .end local v0 # "level":Ljava/lang/String;
} // .end local v2 # "mParameter":Ljava/lang/String;
/* .line 172 */
/* :catch_0 */
/* move-exception v0 */
/* .line 173 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "setAudioPowerSaveLevel exception " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 176 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_2
} // :goto_0
return;
} // .end method
private void setAudioPowerSaveMode ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "mPowerSaveMode" # I */
/* .line 137 */
final String v0 = "AudioPowerSaveModeObserver"; // const-string v0, "AudioPowerSaveModeObserver"
int v1 = -1; // const/4 v1, -0x1
/* if-ne p1, v1, :cond_0 */
int p1 = 0; // const/4 p1, 0x0
/* .line 140 */
} // :cond_0
try { // :try_start_0
java.lang.String .valueOf ( p1 );
/* .line 141 */
/* .local v1, "mode":Ljava/lang/String; */
v2 = (( java.lang.String ) v1 ).length ( ); // invoke-virtual {v1}, Ljava/lang/String;->length()I
int v3 = 1; // const/4 v3, 0x1
/* if-ne v2, v3, :cond_1 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "0"; // const-string v4, "0"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object v1, v2 */
/* .line 142 */
} // :cond_1
/* iget v2, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mModeRecord:I */
/* if-eq v2, p1, :cond_2 */
/* .line 143 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "natural mode = "; // const-string v4, "natural mode = "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v2 );
/* .line 144 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Audio_Power_Save_Mode="; // const-string v4, "Audio_Power_Save_Mode="
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = (( java.lang.String ) v1 ).charAt ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->charAt(I)C
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 145 */
/* .local v2, "mParameter":Ljava/lang/String; */
android.media.AudioSystem .setParameters ( v2 );
/* .line 146 */
/* iput p1, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver;->mModeRecord:I */
/* .line 147 */
} // .end local v2 # "mParameter":Ljava/lang/String;
/* .line 148 */
} // :cond_2
final String v2 = "AudioPowerSaveMode is no need to change"; // const-string v2, "AudioPowerSaveMode is no need to change"
android.util.Log .d ( v0,v2 );
/* .line 150 */
} // :goto_0
final String v2 = "persist.vendor.audio.power.save.setting"; // const-string v2, "persist.vendor.audio.power.save.setting"
int v3 = 0; // const/4 v3, 0x0
v2 = android.os.SystemProperties .getInt ( v2,v3 );
/* .line 151 */
/* .local v2, "currentAudioPowerSave":I */
/* and-int/lit8 v3, v2, 0x8 */
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 152 */
/* invoke-direct {p0, v1}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->setAudioPowerSaveVoiceTrigger(Ljava/lang/String;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 156 */
} // .end local v1 # "mode":Ljava/lang/String;
} // .end local v2 # "currentAudioPowerSave":I
} // :cond_3
/* .line 154 */
/* :catch_0 */
/* move-exception v1 */
/* .line 155 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "setAudioPowerSaveMode exception " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v2 );
/* .line 157 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
private void setAudioPowerSaveVoiceTrigger ( java.lang.String p0 ) {
/* .locals 7 */
/* .param p1, "powersavemode" # Ljava/lang/String; */
/* .line 179 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->isSupportSetVoiceTrigger(Ljava/lang/String;)Z */
final String v1 = "AudioPowerSaveModeObserver"; // const-string v1, "AudioPowerSaveModeObserver"
/* if-nez v0, :cond_0 */
/* .line 180 */
final String v0 = "no need to change VoiceTrigger"; // const-string v0, "no need to change VoiceTrigger"
android.util.Log .d ( v1,v0 );
/* .line 181 */
return;
/* .line 185 */
} // :cond_0
try { // :try_start_0
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 186 */
/* .local v0, "bundle":Landroid/os/Bundle; */
/* nop */
/* .line 187 */
int v2 = 2; // const/4 v2, 0x2
int v3 = 1; // const/4 v3, 0x1
(( java.lang.String ) p1 ).substring ( v3, v2 ); // invoke-virtual {p1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;
final String v4 = "1"; // const-string v4, "1"
v2 = (( java.lang.String ) v2 ).equals ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v4 = 0; // const/4 v4, 0x0
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 188 */
/* const-string/jumbo v2, "setVoiceTrigger stop" */
android.util.Log .d ( v1,v2 );
/* .line 189 */
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v3 = this.mUri;
final String v5 = "method_stop_recognition"; // const-string v5, "method_stop_recognition"
(( android.content.ContentResolver ) v2 ).call ( v3, v5, v4, v0 ); // invoke-virtual {v2, v3, v5, v4, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
/* .line 190 */
int v2 = 0; // const/4 v2, 0x0
/* .line 192 */
} // :cond_1
/* const-string/jumbo v2, "setVoiceTrigger start" */
android.util.Log .d ( v1,v2 );
/* .line 193 */
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v5 = this.mUri;
final String v6 = "method_start_recognition"; // const-string v6, "method_start_recognition"
(( android.content.ContentResolver ) v2 ).call ( v5, v6, v4, v0 ); // invoke-virtual {v2, v5, v6, v4, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
/* .line 194 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 201 */
} // .end local v0 # "bundle":Landroid/os/Bundle;
} // :goto_0
/* .line 199 */
/* :catch_0 */
/* move-exception v0 */
/* .line 200 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "setVoiceTrigger exception " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 202 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
private void toSleep ( ) {
/* .locals 3 */
/* .line 310 */
try { // :try_start_0
v0 = java.util.concurrent.TimeUnit.SECONDS;
/* const-wide/16 v1, 0x3 */
(( java.util.concurrent.TimeUnit ) v0 ).sleep ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->sleep(J)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 313 */
/* .line 311 */
/* :catch_0 */
/* move-exception v0 */
/* .line 312 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "toSleep exception: " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "AudioPowerSaveModeObserver"; // const-string v2, "AudioPowerSaveModeObserver"
android.util.Log .e ( v2,v1 );
/* .line 314 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void unregisterBroadCastReceiver ( ) {
/* .locals 4 */
/* .line 388 */
final String v0 = "AudioPowerSaveModeObserver"; // const-string v0, "AudioPowerSaveModeObserver"
try { // :try_start_0
v1 = this.mContext;
v2 = this.mAudioPowerSaveBroadcastReceiver;
(( android.content.Context ) v1 ).unregisterReceiver ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
/* .line 389 */
/* const-string/jumbo v1, "unregisterBroadCastReceiver" */
android.util.Log .d ( v0,v1 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 392 */
/* .line 390 */
/* :catch_0 */
/* move-exception v1 */
/* .line 391 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "unregisterBroadCastReceiver exception " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v2 );
/* .line 393 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void unwatchAudioPowerStateListener ( ) {
/* .locals 3 */
/* .line 327 */
try { // :try_start_0
v0 = this.mAudioPowerSaveListener;
(( com.android.server.audio.AudioPowerSaveModeObserver$AudioPowerSaveStateListener ) v0 ).stopWatching ( ); // invoke-virtual {v0}, Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener;->stopWatching()V
/* .line 328 */
v0 = this.mAudioPowerLevelListener;
(( com.android.server.audio.AudioPowerSaveModeObserver$AudioPowerSaveStateListener ) v0 ).stopWatching ( ); // invoke-virtual {v0}, Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener;->stopWatching()V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 331 */
/* .line 329 */
/* :catch_0 */
/* move-exception v0 */
/* .line 330 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "unwatchAudioPowerStateListener exception " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "AudioPowerSaveModeObserver"; // const-string v2, "AudioPowerSaveModeObserver"
android.util.Log .e ( v2,v1 );
/* .line 332 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void watchAudioPowerStateListener ( ) {
/* .locals 3 */
/* .line 318 */
try { // :try_start_0
v0 = this.mAudioPowerSaveListener;
(( com.android.server.audio.AudioPowerSaveModeObserver$AudioPowerSaveStateListener ) v0 ).startWatching ( ); // invoke-virtual {v0}, Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener;->startWatching()V
/* .line 319 */
v0 = this.mAudioPowerLevelListener;
(( com.android.server.audio.AudioPowerSaveModeObserver$AudioPowerSaveStateListener ) v0 ).startWatching ( ); // invoke-virtual {v0}, Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener;->startWatching()V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 322 */
/* .line 320 */
/* :catch_0 */
/* move-exception v0 */
/* .line 321 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "watchAudioPowerStateListener exception " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "AudioPowerSaveModeObserver"; // const-string v2, "AudioPowerSaveModeObserver"
android.util.Log .e ( v2,v1 );
/* .line 323 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void reSetAudioPowerParam ( ) {
/* .locals 3 */
/* .line 414 */
try { // :try_start_0
v0 = this.mHandler;
/* const/16 v1, 0x9c8 */
(( com.android.server.audio.AudioPowerSaveModeObserver$WorkHandler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->obtainMessage(I)Landroid/os/Message;
(( com.android.server.audio.AudioPowerSaveModeObserver$WorkHandler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->sendMessage(Landroid/os/Message;)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 417 */
/* .line 415 */
/* :catch_0 */
/* move-exception v0 */
/* .line 416 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "reSetAudioPowerParam exception "; // const-string v2, "reSetAudioPowerParam exception "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "AudioPowerSaveModeObserver"; // const-string v2, "AudioPowerSaveModeObserver"
android.util.Log .e ( v2,v1 );
/* .line 418 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
