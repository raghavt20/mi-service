.class final enum Lcom/android/server/audio/MiAudioService$SpatializerType;
.super Ljava/lang/Enum;
.source "MiAudioService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/MiAudioService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "SpatializerType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/android/server/audio/MiAudioService$SpatializerType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/audio/MiAudioService$SpatializerType;

.field public static final enum DOLBY:Lcom/android/server/audio/MiAudioService$SpatializerType;

.field public static final enum MISOUND:Lcom/android/server/audio/MiAudioService$SpatializerType;

.field public static final enum NONE:Lcom/android/server/audio/MiAudioService$SpatializerType;


# direct methods
.method private static synthetic $values()[Lcom/android/server/audio/MiAudioService$SpatializerType;
    .locals 3

    .line 48
    sget-object v0, Lcom/android/server/audio/MiAudioService$SpatializerType;->DOLBY:Lcom/android/server/audio/MiAudioService$SpatializerType;

    sget-object v1, Lcom/android/server/audio/MiAudioService$SpatializerType;->MISOUND:Lcom/android/server/audio/MiAudioService$SpatializerType;

    sget-object v2, Lcom/android/server/audio/MiAudioService$SpatializerType;->NONE:Lcom/android/server/audio/MiAudioService$SpatializerType;

    filled-new-array {v0, v1, v2}, [Lcom/android/server/audio/MiAudioService$SpatializerType;

    move-result-object v0

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 49
    new-instance v0, Lcom/android/server/audio/MiAudioService$SpatializerType;

    const-string v1, "DOLBY"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/android/server/audio/MiAudioService$SpatializerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/audio/MiAudioService$SpatializerType;->DOLBY:Lcom/android/server/audio/MiAudioService$SpatializerType;

    .line 50
    new-instance v0, Lcom/android/server/audio/MiAudioService$SpatializerType;

    const-string v1, "MISOUND"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/android/server/audio/MiAudioService$SpatializerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/audio/MiAudioService$SpatializerType;->MISOUND:Lcom/android/server/audio/MiAudioService$SpatializerType;

    .line 51
    new-instance v0, Lcom/android/server/audio/MiAudioService$SpatializerType;

    const-string v1, "NONE"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/android/server/audio/MiAudioService$SpatializerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/audio/MiAudioService$SpatializerType;->NONE:Lcom/android/server/audio/MiAudioService$SpatializerType;

    .line 48
    invoke-static {}, Lcom/android/server/audio/MiAudioService$SpatializerType;->$values()[Lcom/android/server/audio/MiAudioService$SpatializerType;

    move-result-object v0

    sput-object v0, Lcom/android/server/audio/MiAudioService$SpatializerType;->$VALUES:[Lcom/android/server/audio/MiAudioService$SpatializerType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/audio/MiAudioService$SpatializerType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 48
    const-class v0, Lcom/android/server/audio/MiAudioService$SpatializerType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/audio/MiAudioService$SpatializerType;

    return-object v0
.end method

.method public static values()[Lcom/android/server/audio/MiAudioService$SpatializerType;
    .locals 1

    .line 48
    sget-object v0, Lcom/android/server/audio/MiAudioService$SpatializerType;->$VALUES:[Lcom/android/server/audio/MiAudioService$SpatializerType;

    invoke-virtual {v0}, [Lcom/android/server/audio/MiAudioService$SpatializerType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/audio/MiAudioService$SpatializerType;

    return-object v0
.end method
