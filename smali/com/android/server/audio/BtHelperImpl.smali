.class public Lcom/android/server/audio/BtHelperImpl;
.super Ljava/lang/Object;
.source "BtHelperImpl.java"

# interfaces
.implements Lcom/android/server/audio/BtHelperStub;


# static fields
.field private static final AUDIO_FORMAT_FORCE_AOSP:I = 0x7f000000

.field private static final SPATIAL_AUDIO_TYPE_SUPPORT_GYRO_AND_ALGO:I = 0x3

.field private static final SPATIAL_AUDIO_TYPE_SUPPORT_GYRO_ONLY:I = 0x2

.field private static final TAG:Ljava/lang/String; = "AS.BtHelperImpl"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public checkEncoderFormat(Landroid/bluetooth/BluetoothCodecConfig;)Z
    .locals 2
    .param p1, "btCodecConfig"    # Landroid/bluetooth/BluetoothCodecConfig;

    .line 30
    invoke-virtual {p0, p1}, Lcom/android/server/audio/BtHelperImpl;->getFormat(Landroid/bluetooth/BluetoothCodecConfig;)I

    move-result v0

    const/high16 v1, 0x7f000000

    if-ne v0, v1, :cond_0

    .line 31
    const/4 v0, 0x1

    return v0

    .line 33
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public connectBtScoHelper(Landroid/bluetooth/BluetoothHeadset;Lcom/android/server/audio/BtHelper;)Z
    .locals 2
    .param p1, "bluetoothheadset"    # Landroid/bluetooth/BluetoothHeadset;
    .param p2, "bthelper"    # Lcom/android/server/audio/BtHelper;

    .line 77
    const-string v0, "bthelper try to connect the sco"

    const-string v1, "AS.BtHelperImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/android/server/audio/BtHelper;->broadcastScoConnectionState(I)V

    .line 79
    if-nez p1, :cond_0

    .line 80
    const-string v0, "mBluetoothHeadset == null"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    const/4 v0, 0x0

    return v0

    .line 83
    :cond_0
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothHeadset;->startScoUsingVirtualVoiceCall()Z

    move-result v0

    return v0
.end method

.method public disconnectBtScoHelper(Landroid/bluetooth/BluetoothHeadset;Lcom/android/server/audio/BtHelper;)Z
    .locals 3
    .param p1, "bluetoothheadset"    # Landroid/bluetooth/BluetoothHeadset;
    .param p2, "bthelper"    # Lcom/android/server/audio/BtHelper;

    .line 67
    const-string v0, "bthelper try to disconnect the sco"

    const-string v1, "AS.BtHelperImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/android/server/audio/BtHelper;->broadcastScoConnectionState(I)V

    .line 69
    if-nez p1, :cond_0

    .line 70
    const-string v2, "mBluetoothHeadset == null"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    return v0

    .line 73
    :cond_0
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothHeadset;->stopScoUsingVirtualVoiceCall()Z

    move-result v0

    return v0
.end method

.method public getFormat(Landroid/bluetooth/BluetoothCodecConfig;)I
    .locals 4
    .param p1, "config"    # Landroid/bluetooth/BluetoothCodecConfig;

    .line 38
    :try_start_0
    const-class v0, Landroid/bluetooth/BluetoothCodecConfig;

    const-string v1, "getEncoderFormat"

    const/4 v2, 0x0

    move-object v3, v2

    check-cast v3, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 39
    .local v0, "isGetFormatMethod":Ljava/lang/reflect/Method;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 40
    move-object v1, v2

    check-cast v1, [Ljava/lang/Object;

    invoke-virtual {v0, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v1

    .line 41
    .end local v0    # "isGetFormatMethod":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v0

    .line 42
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getFormat error "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AS.BtHelperImpl"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    .end local v0    # "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    return v0
.end method

.method public handleSpatialAudioDeviceConnect(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothDevice;I)Z
    .locals 2
    .param p1, "spatialDevice"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "currActiveDevice"    # Landroid/bluetooth/BluetoothDevice;
    .param p3, "spatialAudioType"    # I

    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "spatialAudioType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AS.BtHelperImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    invoke-static {p1, p2}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    if-eq p3, v0, :cond_0

    const/4 v0, 0x3

    if-ne p3, v0, :cond_1

    .line 51
    :cond_0
    const-string/jumbo v0, "spatial audio device connect"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    const-string/jumbo v0, "spatial_audio_headphone_connect=true"

    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 53
    const/4 v0, 0x1

    return v0

    .line 55
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public handleSpatialAudioDeviceDisConnect(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothDevice;)V
    .locals 2
    .param p1, "spatialDevice"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "previousActiveDevice"    # Landroid/bluetooth/BluetoothDevice;

    .line 59
    if-eqz p1, :cond_0

    .line 60
    invoke-static {p1, p2}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    const-string v0, "AS.BtHelperImpl"

    const-string v1, "change to not spatial audio device"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    const-string/jumbo v0, "spatial_audio_headphone_connect=false"

    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 64
    :cond_0
    return-void
.end method

.method public isFakeHfp(Landroid/content/Intent;)Z
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .line 87
    const-string v0, "android.bluetooth.device.extra.NAME"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 88
    .local v0, "broadcastType":Ljava/lang/String;
    const-string v1, "fake_hfp_broadcast"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method
