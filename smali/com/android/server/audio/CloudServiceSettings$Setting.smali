.class public Lcom/android/server/audio/CloudServiceSettings$Setting;
.super Ljava/lang/Object;
.source "CloudServiceSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/CloudServiceSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Setting"
.end annotation


# static fields
.field private static final NAME:Ljava/lang/String; = "name"

.field private static final SECRET:Ljava/lang/String; = "secret"


# instance fields
.field private mLastSetsVersionCode:Ljava/lang/String;

.field protected mSecreted:Z

.field protected mSecretedItem:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/StringBuilder;",
            ">;"
        }
    .end annotation
.end field

.field public mSettingName:Ljava/lang/String;

.field protected mSuccess:Z

.field private mVersionCode:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/server/audio/CloudServiceSettings;


# direct methods
.method public constructor <init>(Lcom/android/server/audio/CloudServiceSettings;Lorg/json/JSONObject;)V
    .locals 3
    .param p1, "this$0"    # Lcom/android/server/audio/CloudServiceSettings;
    .param p2, "json"    # Lorg/json/JSONObject;

    .line 372
    iput-object p1, p0, Lcom/android/server/audio/CloudServiceSettings$Setting;->this$0:Lcom/android/server/audio/CloudServiceSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 368
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/audio/CloudServiceSettings$Setting;->mSuccess:Z

    .line 369
    iput-boolean v0, p0, Lcom/android/server/audio/CloudServiceSettings$Setting;->mSecreted:Z

    .line 370
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$Setting;->mSecretedItem:Ljava/util/HashSet;

    .line 374
    :try_start_0
    const-string v1, "name"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$Setting;->mSettingName:Ljava/lang/String;

    .line 375
    const-string/jumbo v1, "versionCode"

    const-string v2, ""

    invoke-virtual {p2, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$Setting;->mVersionCode:Ljava/lang/String;

    .line 376
    const-string/jumbo v1, "secret"

    invoke-virtual {p2, v1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/audio/CloudServiceSettings$Setting;->mSecreted:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 379
    goto :goto_0

    .line 377
    :catch_0
    move-exception v0

    .line 378
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "CloudServiceSettings"

    const-string v2, "fail to parse Setting"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method


# virtual methods
.method protected decrypt()V
    .locals 5

    .line 408
    iget-object v0, p0, Lcom/android/server/audio/CloudServiceSettings$Setting;->mSecretedItem:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 409
    .local v0, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 410
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/StringBuilder;

    .line 411
    .local v1, "sb":Ljava/lang/StringBuilder;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "decryptPassword :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "CloudServiceSettings"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 412
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/server/audio/CloudServiceSettings;->-$$Nest$smdecryptPassword(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 413
    .local v2, "dec":Ljava/lang/String;
    const/4 v3, 0x0

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 414
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 415
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 416
    .end local v1    # "sb":Ljava/lang/StringBuilder;
    .end local v2    # "dec":Ljava/lang/String;
    goto :goto_0

    .line 417
    :cond_0
    return-void
.end method

.method public set()Z
    .locals 2

    .line 383
    invoke-virtual {p0}, Lcom/android/server/audio/CloudServiceSettings$Setting;->decrypt()V

    .line 384
    iget-boolean v0, p0, Lcom/android/server/audio/CloudServiceSettings$Setting;->mSuccess:Z

    if-eqz v0, :cond_0

    .line 385
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$Setting;->mSettingName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " already sets"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CloudServiceSettings"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/audio/CloudServiceSettings$Setting;->mSuccess:Z

    if-eqz v0, :cond_2

    const-string v0, ""

    iget-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$Setting;->mVersionCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/server/audio/CloudServiceSettings$Setting;->mVersionCode:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$Setting;->mLastSetsVersionCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_1

    goto :goto_0

    .line 391
    :cond_1
    const/4 v0, 0x0

    return v0

    .line 388
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/android/server/audio/CloudServiceSettings$Setting;->mVersionCode:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/server/audio/CloudServiceSettings$Setting;->mLastSetsVersionCode:Ljava/lang/String;

    .line 389
    const/4 v0, 0x1

    return v0
.end method

.method public updateTo(Lcom/android/server/audio/CloudServiceSettings$Setting;)Z
    .locals 2
    .param p1, "s"    # Lcom/android/server/audio/CloudServiceSettings$Setting;

    .line 396
    iget-object v0, p0, Lcom/android/server/audio/CloudServiceSettings$Setting;->mSettingName:Ljava/lang/String;

    iget-object v1, p1, Lcom/android/server/audio/CloudServiceSettings$Setting;->mSettingName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 397
    iget-object v0, p1, Lcom/android/server/audio/CloudServiceSettings$Setting;->mVersionCode:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/server/audio/CloudServiceSettings$Setting;->mVersionCode:Ljava/lang/String;

    .line 398
    iget-boolean v0, p1, Lcom/android/server/audio/CloudServiceSettings$Setting;->mSecreted:Z

    iput-boolean v0, p0, Lcom/android/server/audio/CloudServiceSettings$Setting;->mSecreted:Z

    .line 399
    iget-object v0, p1, Lcom/android/server/audio/CloudServiceSettings$Setting;->mSecretedItem:Ljava/util/HashSet;

    iput-object v0, p0, Lcom/android/server/audio/CloudServiceSettings$Setting;->mSecretedItem:Ljava/util/HashSet;

    .line 400
    const/4 v0, 0x1

    return v0

    .line 402
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$Setting;->mSettingName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " update to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/android/server/audio/CloudServiceSettings$Setting;->mSettingName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " fail, item not match!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CloudServiceSettings"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    const/4 v0, 0x0

    return v0
.end method
