.class Lcom/android/server/audio/foldable/FoldStateHelper$1;
.super Ljava/lang/Object;
.source "FoldStateHelper.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/foldable/FoldStateHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/audio/foldable/FoldStateHelper;


# direct methods
.method constructor <init>(Lcom/android/server/audio/foldable/FoldStateHelper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/audio/foldable/FoldStateHelper;

    .line 40
    iput-object p1, p0, Lcom/android/server/audio/foldable/FoldStateHelper$1;->this$0:Lcom/android/server/audio/foldable/FoldStateHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 2
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onAccuracyChanged() sensor : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", accuracy : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FoldableAdapter.FoldStateHelper"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .line 43
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    float-to-int v0, v0

    .line 44
    .local v0, "hingeAngle":I
    iget-object v1, p0, Lcom/android/server/audio/foldable/FoldStateHelper$1;->this$0:Lcom/android/server/audio/foldable/FoldStateHelper;

    invoke-static {v1, v0}, Lcom/android/server/audio/foldable/FoldStateHelper;->-$$Nest$mhandleHingeAngleChanged(Lcom/android/server/audio/foldable/FoldStateHelper;I)V

    .line 45
    iget-object v1, p0, Lcom/android/server/audio/foldable/FoldStateHelper$1;->this$0:Lcom/android/server/audio/foldable/FoldStateHelper;

    invoke-static {v1}, Lcom/android/server/audio/foldable/FoldStateHelper;->-$$Nest$fgetmListener(Lcom/android/server/audio/foldable/FoldStateHelper;)Lcom/android/server/audio/foldable/FoldStateHelper$AngleChangedListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 46
    iget-object v1, p0, Lcom/android/server/audio/foldable/FoldStateHelper$1;->this$0:Lcom/android/server/audio/foldable/FoldStateHelper;

    invoke-static {v1}, Lcom/android/server/audio/foldable/FoldStateHelper;->-$$Nest$fgetmListener(Lcom/android/server/audio/foldable/FoldStateHelper;)Lcom/android/server/audio/foldable/FoldStateHelper$AngleChangedListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/android/server/audio/foldable/FoldStateHelper$AngleChangedListener;->onAngleChanged(I)V

    .line 48
    :cond_0
    return-void
.end method
