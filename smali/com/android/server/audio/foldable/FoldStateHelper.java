public class com.android.server.audio.foldable.FoldStateHelper {
	 /* .source "FoldStateHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/audio/foldable/FoldStateHelper$AngleChangedListener;, */
	 /* Lcom/android/server/audio/foldable/FoldStateHelper$H; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer FOLD_ANGLE_INIT;
private static final Integer FOLD_ANGLE_THRESHOLD;
private static final Integer MSG_HANDLE_LISTENER_UNREGISTER;
private static final java.lang.String TAG;
/* # instance fields */
private android.media.AudioManager mAudioManager;
private Boolean mCallMode;
private android.content.Context mContext;
private Boolean mFoldedState;
private com.android.server.audio.foldable.FoldStateHelper$H mHandler;
private android.hardware.SensorEventListener mHingeAngleListener;
private Integer mLastFoldAngle;
private com.android.server.audio.foldable.FoldStateHelper$AngleChangedListener mListener;
private Boolean mMediaActive;
private Boolean mRegisterState;
private android.hardware.SensorManager mSensorManager;
/* # direct methods */
static com.android.server.audio.foldable.FoldStateHelper$AngleChangedListener -$$Nest$fgetmListener ( com.android.server.audio.foldable.FoldStateHelper p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mListener;
} // .end method
static void -$$Nest$mhandleHingeAngleChanged ( com.android.server.audio.foldable.FoldStateHelper p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/audio/foldable/FoldStateHelper;->handleHingeAngleChanged(I)V */
	 return;
} // .end method
static void -$$Nest$munregisterHingeAngleListener ( com.android.server.audio.foldable.FoldStateHelper p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/audio/foldable/FoldStateHelper;->unregisterHingeAngleListener()V */
	 return;
} // .end method
public com.android.server.audio.foldable.FoldStateHelper ( ) {
	 /* .locals 2 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .param p2, "looper" # Landroid/os/Looper; */
	 /* .line 59 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 26 */
	 int v0 = -1; // const/4 v0, -0x1
	 /* iput v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mLastFoldAngle:I */
	 /* .line 40 */
	 /* new-instance v0, Lcom/android/server/audio/foldable/FoldStateHelper$1; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/audio/foldable/FoldStateHelper$1;-><init>(Lcom/android/server/audio/foldable/FoldStateHelper;)V */
	 this.mHingeAngleListener = v0;
	 /* .line 60 */
	 final String v0 = "FoldableAdapter.FoldStateHelper"; // const-string v0, "FoldableAdapter.FoldStateHelper"
	 final String v1 = "FoldStateHelper Construct ..."; // const-string v1, "FoldStateHelper Construct ..."
	 android.util.Log .d ( v0,v1 );
	 /* .line 61 */
	 this.mContext = p1;
	 /* .line 62 */
	 /* new-instance v0, Lcom/android/server/audio/foldable/FoldStateHelper$H; */
	 /* invoke-direct {v0, p0, p2}, Lcom/android/server/audio/foldable/FoldStateHelper$H;-><init>(Lcom/android/server/audio/foldable/FoldStateHelper;Landroid/os/Looper;)V */
	 this.mHandler = v0;
	 /* .line 63 */
	 v0 = this.mContext;
	 final String v1 = "audio"; // const-string v1, "audio"
	 (( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
	 /* check-cast v0, Landroid/media/AudioManager; */
	 this.mAudioManager = v0;
	 /* .line 64 */
	 v0 = this.mContext;
	 /* const-string/jumbo v1, "sensor" */
	 (( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
	 /* check-cast v0, Landroid/hardware/SensorManager; */
	 this.mSensorManager = v0;
	 /* .line 65 */
	 return;
} // .end method
private void handleHingeAngleChanged ( Integer p0 ) {
	 /* .locals 2 */
	 /* .param p1, "hingeAngle" # I */
	 /* .line 134 */
	 /* iget v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mLastFoldAngle:I */
	 int v1 = -1; // const/4 v1, -0x1
	 /* if-eq v0, v1, :cond_1 */
	 /* const/16 v1, 0x5a */
	 /* if-ge v0, v1, :cond_0 */
	 /* if-ge p1, v1, :cond_1 */
} // :cond_0
/* if-lt v0, v1, :cond_2 */
/* if-ge p1, v1, :cond_2 */
/* .line 137 */
} // :cond_1
/* invoke-direct {p0, p1}, Lcom/android/server/audio/foldable/FoldStateHelper;->updateFoldedStateParam(I)V */
/* .line 140 */
} // :cond_2
/* iput p1, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mLastFoldAngle:I */
/* .line 141 */
return;
} // .end method
private void registerHingeAngleListener ( ) {
/* .locals 5 */
/* .line 117 */
final String v0 = "FoldableAdapter.FoldStateHelper"; // const-string v0, "FoldableAdapter.FoldStateHelper"
final String v1 = "registerHingeAngleListener() ..."; // const-string v1, "registerHingeAngleListener() ..."
android.util.Log .d ( v0,v1 );
/* .line 118 */
v0 = this.mSensorManager;
/* const/16 v1, 0x24 */
(( android.hardware.SensorManager ) v0 ).getSensorList ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;
/* .line 119 */
/* .local v0, "hingeAngleList":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Sensor;>;" */
v1 = if ( v0 != null) { // if-eqz v0, :cond_0
/* if-lez v1, :cond_0 */
/* .line 120 */
int v1 = 0; // const/4 v1, 0x0
/* check-cast v1, Landroid/hardware/Sensor; */
/* .line 121 */
/* .local v1, "hingeAngleSensor":Landroid/hardware/Sensor; */
v2 = this.mSensorManager;
v3 = this.mHingeAngleListener;
int v4 = 2; // const/4 v4, 0x2
(( android.hardware.SensorManager ) v2 ).registerListener ( v3, v1, v4 ); // invoke-virtual {v2, v3, v1, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z
/* .line 124 */
} // .end local v1 # "hingeAngleSensor":Landroid/hardware/Sensor;
} // :cond_0
return;
} // .end method
private void unregisterHingeAngleListener ( ) {
/* .locals 2 */
/* .line 127 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mRegisterState:Z */
/* .line 128 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mLastFoldAngle:I */
/* .line 129 */
final String v0 = "FoldableAdapter.FoldStateHelper"; // const-string v0, "FoldableAdapter.FoldStateHelper"
/* const-string/jumbo v1, "unregisterHingeAngleListener() ..." */
android.util.Log .d ( v0,v1 );
/* .line 130 */
v0 = this.mSensorManager;
v1 = this.mHingeAngleListener;
(( android.hardware.SensorManager ) v0 ).unregisterListener ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V
/* .line 131 */
return;
} // .end method
private void updateFoldedStateParam ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "hingeAngle" # I */
/* .line 144 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "device_fold_status="; // const-string v1, "device_fold_status="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 145 */
/* const/16 v1, 0x5a */
/* if-ge p1, v1, :cond_0 */
final String v1 = "on"; // const-string v1, "on"
} // :cond_0
final String v1 = "off"; // const-string v1, "off"
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 146 */
/* .local v0, "parameter":Ljava/lang/String; */
v1 = this.mAudioManager;
(( android.media.AudioManager ) v1 ).setParameters ( v0 ); // invoke-virtual {v1, v0}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V
/* .line 147 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "update audio param : " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "FoldableAdapter.FoldStateHelper"; // const-string v2, "FoldableAdapter.FoldStateHelper"
android.util.Log .d ( v2,v1 );
/* .line 148 */
return;
} // .end method
/* # virtual methods */
public void onAudioServerDied ( ) {
/* .locals 3 */
/* .line 109 */
final String v0 = "onAudioServerDied ..."; // const-string v0, "onAudioServerDied ..."
final String v1 = "FoldableAdapter.FoldStateHelper"; // const-string v1, "FoldableAdapter.FoldStateHelper"
android.util.Log .d ( v1,v0 );
/* .line 110 */
/* iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mCallMode:Z */
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mMediaActive:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 111 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "onAudioServerDied mLastFoldAngle = "; // const-string v2, "onAudioServerDied mLastFoldAngle = "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mLastFoldAngle:I */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v0 );
/* .line 112 */
/* iget v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mLastFoldAngle:I */
/* invoke-direct {p0, v0}, Lcom/android/server/audio/foldable/FoldStateHelper;->updateFoldedStateParam(I)V */
/* .line 114 */
} // :cond_1
return;
} // .end method
public void onUpdateAudioMode ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "mode" # I */
/* .line 69 */
v0 = this.mHandler;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.audio.foldable.FoldStateHelper$H ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/audio/foldable/FoldStateHelper$H;->removeMessages(I)V
/* .line 70 */
int v0 = 2; // const/4 v0, 0x2
final String v2 = "FoldableAdapter.FoldStateHelper"; // const-string v2, "FoldableAdapter.FoldStateHelper"
/* if-eq p1, v0, :cond_0 */
int v0 = 3; // const/4 v0, 0x3
/* if-ne p1, v0, :cond_1 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mCallMode:Z */
/* if-nez v0, :cond_1 */
/* .line 72 */
final String v0 = "enter call mode ..."; // const-string v0, "enter call mode ..."
android.util.Log .d ( v2,v0 );
/* .line 73 */
/* iput-boolean v1, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mCallMode:Z */
/* .line 74 */
/* iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mRegisterState:Z */
/* if-nez v0, :cond_1 */
/* .line 75 */
/* iput-boolean v1, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mRegisterState:Z */
/* .line 76 */
final String v0 = "onUpdateAudioMode registerHingeAngleListener"; // const-string v0, "onUpdateAudioMode registerHingeAngleListener"
android.util.Log .d ( v2,v0 );
/* .line 77 */
/* invoke-direct {p0}, Lcom/android/server/audio/foldable/FoldStateHelper;->registerHingeAngleListener()V */
/* .line 82 */
} // :cond_1
/* if-nez p1, :cond_2 */
/* iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mCallMode:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 83 */
final String v0 = "exit call mode ..."; // const-string v0, "exit call mode ..."
android.util.Log .d ( v2,v0 );
/* .line 84 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mCallMode:Z */
/* .line 85 */
/* iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mRegisterState:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mMediaActive:Z */
/* if-nez v0, :cond_2 */
/* .line 86 */
final String v0 = "onUpdateAudioMode sendMessageDelayed 10s"; // const-string v0, "onUpdateAudioMode sendMessageDelayed 10s"
android.util.Log .d ( v2,v0 );
/* .line 87 */
v0 = this.mHandler;
/* .line 88 */
java.lang.Boolean .valueOf ( v1 );
/* .line 87 */
(( com.android.server.audio.foldable.FoldStateHelper$H ) v0 ).obtainMessage ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/audio/foldable/FoldStateHelper$H;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* const-wide/16 v2, 0x2710 */
(( com.android.server.audio.foldable.FoldStateHelper$H ) v0 ).sendMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/audio/foldable/FoldStateHelper$H;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 91 */
} // :cond_2
return;
} // .end method
public void onUpdateMediaState ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "mediaActive" # Z */
/* .line 94 */
/* iput-boolean p1, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mMediaActive:Z */
/* .line 95 */
v0 = this.mHandler;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.audio.foldable.FoldStateHelper$H ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/audio/foldable/FoldStateHelper$H;->removeMessages(I)V
/* .line 96 */
/* iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mMediaActive:Z */
final String v2 = "FoldableAdapter.FoldStateHelper"; // const-string v2, "FoldableAdapter.FoldStateHelper"
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mCallMode:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mRegisterState:Z */
/* if-nez v0, :cond_1 */
/* .line 97 */
/* iput-boolean v1, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mRegisterState:Z */
/* .line 98 */
final String v0 = "onUpdateMediaState registerHingeAngleListener"; // const-string v0, "onUpdateMediaState registerHingeAngleListener"
android.util.Log .d ( v2,v0 );
/* .line 99 */
/* invoke-direct {p0}, Lcom/android/server/audio/foldable/FoldStateHelper;->registerHingeAngleListener()V */
/* .line 101 */
} // :cond_1
/* iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mMediaActive:Z */
/* if-nez v0, :cond_2 */
/* iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mCallMode:Z */
/* if-nez v0, :cond_2 */
/* iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mRegisterState:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 102 */
final String v0 = "onUpdateMediaState sendMessageDelayed 10s"; // const-string v0, "onUpdateMediaState sendMessageDelayed 10s"
android.util.Log .d ( v2,v0 );
/* .line 103 */
v0 = this.mHandler;
/* .line 104 */
java.lang.Boolean .valueOf ( v1 );
/* .line 103 */
(( com.android.server.audio.foldable.FoldStateHelper$H ) v0 ).obtainMessage ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/audio/foldable/FoldStateHelper$H;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* const-wide/16 v2, 0x2710 */
(( com.android.server.audio.foldable.FoldStateHelper$H ) v0 ).sendMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/audio/foldable/FoldStateHelper$H;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 106 */
} // :cond_2
return;
} // .end method
public void setAngleChangedListener ( com.android.server.audio.foldable.FoldStateHelper$AngleChangedListener p0 ) {
/* .locals 0 */
/* .param p1, "listener" # Lcom/android/server/audio/foldable/FoldStateHelper$AngleChangedListener; */
/* .line 37 */
this.mListener = p1;
/* .line 38 */
return;
} // .end method
