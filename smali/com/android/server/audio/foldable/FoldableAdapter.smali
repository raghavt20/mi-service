.class public Lcom/android/server/audio/foldable/FoldableAdapter;
.super Ljava/lang/Object;
.source "FoldableAdapter.java"

# interfaces
.implements Lcom/android/server/audio/foldable/FoldStateHelper$AngleChangedListener;


# static fields
.field private static final CONFIG_PROP:I

.field public static final ENABLE:Z

.field private static final FLAG_FOLDABLE_ADAPTER_ENABLE:I = 0x1

.field private static final FLAG_FOLD_STATE_HELP_ENABLE:I = 0x2

.field private static final TAG:Ljava/lang/String; = "AudioService.FoldableAdapter"


# instance fields
.field private final FOLD_STATE_HELP_ENABLE:Z

.field private mFoldAngle:I

.field private mFoldStateHelper:Lcom/android/server/audio/foldable/FoldStateHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 13
    nop

    .line 14
    const-string v0, "ro.vendor.audio.foldable.adapter"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/server/audio/foldable/FoldableAdapter;->CONFIG_PROP:I

    .line 15
    const/4 v2, 0x1

    and-int/2addr v0, v2

    if-eqz v0, :cond_0

    move v1, v2

    :cond_0
    sput-boolean v1, Lcom/android/server/audio/foldable/FoldableAdapter;->ENABLE:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    sget v0, Lcom/android/server/audio/foldable/FoldableAdapter;->CONFIG_PROP:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/android/server/audio/foldable/FoldableAdapter;->FOLD_STATE_HELP_ENABLE:Z

    .line 21
    const-string v1, "AudioService.FoldableAdapter"

    const-string v2, "FoldableAdapter Construct ..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 22
    if-eqz v0, :cond_1

    .line 23
    new-instance v0, Lcom/android/server/audio/foldable/FoldStateHelper;

    invoke-direct {v0, p1, p2}, Lcom/android/server/audio/foldable/FoldStateHelper;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/audio/foldable/FoldableAdapter;->mFoldStateHelper:Lcom/android/server/audio/foldable/FoldStateHelper;

    .line 24
    invoke-virtual {v0, p0}, Lcom/android/server/audio/foldable/FoldStateHelper;->setAngleChangedListener(Lcom/android/server/audio/foldable/FoldStateHelper$AngleChangedListener;)V

    .line 26
    :cond_1
    return-void
.end method


# virtual methods
.method public onAngleChanged(I)V
    .locals 0
    .param p1, "foldAngle"    # I

    .line 30
    iput p1, p0, Lcom/android/server/audio/foldable/FoldableAdapter;->mFoldAngle:I

    .line 31
    return-void
.end method

.method public onAudioServerDied()V
    .locals 1

    .line 46
    iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldableAdapter;->FOLD_STATE_HELP_ENABLE:Z

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/android/server/audio/foldable/FoldableAdapter;->mFoldStateHelper:Lcom/android/server/audio/foldable/FoldStateHelper;

    invoke-virtual {v0}, Lcom/android/server/audio/foldable/FoldStateHelper;->onAudioServerDied()V

    .line 49
    :cond_0
    return-void
.end method

.method public onUpdateAudioMode(I)V
    .locals 1
    .param p1, "mode"    # I

    .line 34
    iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldableAdapter;->FOLD_STATE_HELP_ENABLE:Z

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/android/server/audio/foldable/FoldableAdapter;->mFoldStateHelper:Lcom/android/server/audio/foldable/FoldStateHelper;

    invoke-virtual {v0, p1}, Lcom/android/server/audio/foldable/FoldStateHelper;->onUpdateAudioMode(I)V

    .line 37
    :cond_0
    return-void
.end method

.method public onUpdateMediaState(Z)V
    .locals 1
    .param p1, "mediaActive"    # Z

    .line 40
    iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldableAdapter;->FOLD_STATE_HELP_ENABLE:Z

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/android/server/audio/foldable/FoldableAdapter;->mFoldStateHelper:Lcom/android/server/audio/foldable/FoldStateHelper;

    invoke-virtual {v0, p1}, Lcom/android/server/audio/foldable/FoldStateHelper;->onUpdateMediaState(Z)V

    .line 43
    :cond_0
    return-void
.end method
