public class com.android.server.audio.foldable.FoldableAdapter implements com.android.server.audio.foldable.FoldStateHelper$AngleChangedListener {
	 /* .source "FoldableAdapter.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final Integer CONFIG_PROP;
	 public static final Boolean ENABLE;
	 private static final Integer FLAG_FOLDABLE_ADAPTER_ENABLE;
	 private static final Integer FLAG_FOLD_STATE_HELP_ENABLE;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private final Boolean FOLD_STATE_HELP_ENABLE;
	 private Integer mFoldAngle;
	 private com.android.server.audio.foldable.FoldStateHelper mFoldStateHelper;
	 /* # direct methods */
	 static com.android.server.audio.foldable.FoldableAdapter ( ) {
		 /* .locals 3 */
		 /* .line 13 */
		 /* nop */
		 /* .line 14 */
		 final String v0 = "ro.vendor.audio.foldable.adapter"; // const-string v0, "ro.vendor.audio.foldable.adapter"
		 int v1 = 0; // const/4 v1, 0x0
		 v0 = 		 android.os.SystemProperties .getInt ( v0,v1 );
		 /* .line 15 */
		 int v2 = 1; // const/4 v2, 0x1
		 /* and-int/2addr v0, v2 */
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* move v1, v2 */
		 } // :cond_0
		 com.android.server.audio.foldable.FoldableAdapter.ENABLE = (v1!= 0);
		 return;
	 } // .end method
	 public com.android.server.audio.foldable.FoldableAdapter ( ) {
		 /* .locals 3 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "looper" # Landroid/os/Looper; */
		 /* .line 20 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 16 */
		 /* and-int/lit8 v0, v0, 0x2 */
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 int v0 = 1; // const/4 v0, 0x1
		 } // :cond_0
		 int v0 = 0; // const/4 v0, 0x0
	 } // :goto_0
	 /* iput-boolean v0, p0, Lcom/android/server/audio/foldable/FoldableAdapter;->FOLD_STATE_HELP_ENABLE:Z */
	 /* .line 21 */
	 final String v1 = "AudioService.FoldableAdapter"; // const-string v1, "AudioService.FoldableAdapter"
	 final String v2 = "FoldableAdapter Construct ..."; // const-string v2, "FoldableAdapter Construct ..."
	 android.util.Log .d ( v1,v2 );
	 /* .line 22 */
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 23 */
		 /* new-instance v0, Lcom/android/server/audio/foldable/FoldStateHelper; */
		 /* invoke-direct {v0, p1, p2}, Lcom/android/server/audio/foldable/FoldStateHelper;-><init>(Landroid/content/Context;Landroid/os/Looper;)V */
		 this.mFoldStateHelper = v0;
		 /* .line 24 */
		 (( com.android.server.audio.foldable.FoldStateHelper ) v0 ).setAngleChangedListener ( p0 ); // invoke-virtual {v0, p0}, Lcom/android/server/audio/foldable/FoldStateHelper;->setAngleChangedListener(Lcom/android/server/audio/foldable/FoldStateHelper$AngleChangedListener;)V
		 /* .line 26 */
	 } // :cond_1
	 return;
} // .end method
/* # virtual methods */
public void onAngleChanged ( Integer p0 ) {
	 /* .locals 0 */
	 /* .param p1, "foldAngle" # I */
	 /* .line 30 */
	 /* iput p1, p0, Lcom/android/server/audio/foldable/FoldableAdapter;->mFoldAngle:I */
	 /* .line 31 */
	 return;
} // .end method
public void onAudioServerDied ( ) {
	 /* .locals 1 */
	 /* .line 46 */
	 /* iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldableAdapter;->FOLD_STATE_HELP_ENABLE:Z */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 47 */
		 v0 = this.mFoldStateHelper;
		 (( com.android.server.audio.foldable.FoldStateHelper ) v0 ).onAudioServerDied ( ); // invoke-virtual {v0}, Lcom/android/server/audio/foldable/FoldStateHelper;->onAudioServerDied()V
		 /* .line 49 */
	 } // :cond_0
	 return;
} // .end method
public void onUpdateAudioMode ( Integer p0 ) {
	 /* .locals 1 */
	 /* .param p1, "mode" # I */
	 /* .line 34 */
	 /* iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldableAdapter;->FOLD_STATE_HELP_ENABLE:Z */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 35 */
		 v0 = this.mFoldStateHelper;
		 (( com.android.server.audio.foldable.FoldStateHelper ) v0 ).onUpdateAudioMode ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/audio/foldable/FoldStateHelper;->onUpdateAudioMode(I)V
		 /* .line 37 */
	 } // :cond_0
	 return;
} // .end method
public void onUpdateMediaState ( Boolean p0 ) {
	 /* .locals 1 */
	 /* .param p1, "mediaActive" # Z */
	 /* .line 40 */
	 /* iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldableAdapter;->FOLD_STATE_HELP_ENABLE:Z */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 41 */
		 v0 = this.mFoldStateHelper;
		 (( com.android.server.audio.foldable.FoldStateHelper ) v0 ).onUpdateMediaState ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/audio/foldable/FoldStateHelper;->onUpdateMediaState(Z)V
		 /* .line 43 */
	 } // :cond_0
	 return;
} // .end method
