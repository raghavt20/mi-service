class com.android.server.audio.foldable.FoldStateHelper$1 implements android.hardware.SensorEventListener {
	 /* .source "FoldStateHelper.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/audio/foldable/FoldStateHelper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.audio.foldable.FoldStateHelper this$0; //synthetic
/* # direct methods */
 com.android.server.audio.foldable.FoldStateHelper$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/audio/foldable/FoldStateHelper; */
/* .line 40 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onAccuracyChanged ( android.hardware.Sensor p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "sensor" # Landroid/hardware/Sensor; */
/* .param p2, "accuracy" # I */
/* .line 52 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "onAccuracyChanged() sensor : "; // const-string v1, "onAccuracyChanged() sensor : "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = ", accuracy : "; // const-string v1, ", accuracy : "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "FoldableAdapter.FoldStateHelper"; // const-string v1, "FoldableAdapter.FoldStateHelper"
android.util.Log .d ( v1,v0 );
/* .line 53 */
return;
} // .end method
public void onSensorChanged ( android.hardware.SensorEvent p0 ) {
/* .locals 2 */
/* .param p1, "event" # Landroid/hardware/SensorEvent; */
/* .line 43 */
v0 = this.values;
int v1 = 0; // const/4 v1, 0x0
/* aget v0, v0, v1 */
/* float-to-int v0, v0 */
/* .line 44 */
/* .local v0, "hingeAngle":I */
v1 = this.this$0;
com.android.server.audio.foldable.FoldStateHelper .-$$Nest$mhandleHingeAngleChanged ( v1,v0 );
/* .line 45 */
v1 = this.this$0;
com.android.server.audio.foldable.FoldStateHelper .-$$Nest$fgetmListener ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 46 */
	 v1 = this.this$0;
	 com.android.server.audio.foldable.FoldStateHelper .-$$Nest$fgetmListener ( v1 );
	 /* .line 48 */
} // :cond_0
return;
} // .end method
