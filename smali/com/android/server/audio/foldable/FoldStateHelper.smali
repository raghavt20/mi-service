.class public Lcom/android/server/audio/foldable/FoldStateHelper;
.super Ljava/lang/Object;
.source "FoldStateHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/audio/foldable/FoldStateHelper$AngleChangedListener;,
        Lcom/android/server/audio/foldable/FoldStateHelper$H;
    }
.end annotation


# static fields
.field private static final FOLD_ANGLE_INIT:I = -0x1

.field private static final FOLD_ANGLE_THRESHOLD:I = 0x5a

.field private static final MSG_HANDLE_LISTENER_UNREGISTER:I = 0x1

.field private static final TAG:Ljava/lang/String; = "FoldableAdapter.FoldStateHelper"


# instance fields
.field private mAudioManager:Landroid/media/AudioManager;

.field private mCallMode:Z

.field private mContext:Landroid/content/Context;

.field private mFoldedState:Z

.field private mHandler:Lcom/android/server/audio/foldable/FoldStateHelper$H;

.field private mHingeAngleListener:Landroid/hardware/SensorEventListener;

.field private mLastFoldAngle:I

.field private mListener:Lcom/android/server/audio/foldable/FoldStateHelper$AngleChangedListener;

.field private mMediaActive:Z

.field private mRegisterState:Z

.field private mSensorManager:Landroid/hardware/SensorManager;


# direct methods
.method static bridge synthetic -$$Nest$fgetmListener(Lcom/android/server/audio/foldable/FoldStateHelper;)Lcom/android/server/audio/foldable/FoldStateHelper$AngleChangedListener;
    .locals 0

    iget-object p0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mListener:Lcom/android/server/audio/foldable/FoldStateHelper$AngleChangedListener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mhandleHingeAngleChanged(Lcom/android/server/audio/foldable/FoldStateHelper;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/audio/foldable/FoldStateHelper;->handleHingeAngleChanged(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$munregisterHingeAngleListener(Lcom/android/server/audio/foldable/FoldStateHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/audio/foldable/FoldStateHelper;->unregisterHingeAngleListener()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mLastFoldAngle:I

    .line 40
    new-instance v0, Lcom/android/server/audio/foldable/FoldStateHelper$1;

    invoke-direct {v0, p0}, Lcom/android/server/audio/foldable/FoldStateHelper$1;-><init>(Lcom/android/server/audio/foldable/FoldStateHelper;)V

    iput-object v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mHingeAngleListener:Landroid/hardware/SensorEventListener;

    .line 60
    const-string v0, "FoldableAdapter.FoldStateHelper"

    const-string v1, "FoldStateHelper Construct ..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    iput-object p1, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mContext:Landroid/content/Context;

    .line 62
    new-instance v0, Lcom/android/server/audio/foldable/FoldStateHelper$H;

    invoke-direct {v0, p0, p2}, Lcom/android/server/audio/foldable/FoldStateHelper$H;-><init>(Lcom/android/server/audio/foldable/FoldStateHelper;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mHandler:Lcom/android/server/audio/foldable/FoldStateHelper$H;

    .line 63
    iget-object v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mAudioManager:Landroid/media/AudioManager;

    .line 64
    iget-object v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mSensorManager:Landroid/hardware/SensorManager;

    .line 65
    return-void
.end method

.method private handleHingeAngleChanged(I)V
    .locals 2
    .param p1, "hingeAngle"    # I

    .line 134
    iget v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mLastFoldAngle:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    const/16 v1, 0x5a

    if-ge v0, v1, :cond_0

    if-ge p1, v1, :cond_1

    :cond_0
    if-lt v0, v1, :cond_2

    if-ge p1, v1, :cond_2

    .line 137
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/server/audio/foldable/FoldStateHelper;->updateFoldedStateParam(I)V

    .line 140
    :cond_2
    iput p1, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mLastFoldAngle:I

    .line 141
    return-void
.end method

.method private registerHingeAngleListener()V
    .locals 5

    .line 117
    const-string v0, "FoldableAdapter.FoldStateHelper"

    const-string v1, "registerHingeAngleListener() ..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    iget-object v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v1, 0x24

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v0

    .line 119
    .local v0, "hingeAngleList":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Sensor;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 120
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/Sensor;

    .line 121
    .local v1, "hingeAngleSensor":Landroid/hardware/Sensor;
    iget-object v2, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mHingeAngleListener:Landroid/hardware/SensorEventListener;

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v1, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 124
    .end local v1    # "hingeAngleSensor":Landroid/hardware/Sensor;
    :cond_0
    return-void
.end method

.method private unregisterHingeAngleListener()V
    .locals 2

    .line 127
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mRegisterState:Z

    .line 128
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mLastFoldAngle:I

    .line 129
    const-string v0, "FoldableAdapter.FoldStateHelper"

    const-string/jumbo v1, "unregisterHingeAngleListener() ..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    iget-object v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mHingeAngleListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 131
    return-void
.end method

.method private updateFoldedStateParam(I)V
    .locals 3
    .param p1, "hingeAngle"    # I

    .line 144
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "device_fold_status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 145
    const/16 v1, 0x5a

    if-ge p1, v1, :cond_0

    const-string v1, "on"

    goto :goto_0

    :cond_0
    const-string v1, "off"

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 146
    .local v0, "parameter":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v0}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 147
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "update audio param : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "FoldableAdapter.FoldStateHelper"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    return-void
.end method


# virtual methods
.method public onAudioServerDied()V
    .locals 3

    .line 109
    const-string v0, "onAudioServerDied ..."

    const-string v1, "FoldableAdapter.FoldStateHelper"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mCallMode:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mMediaActive:Z

    if-eqz v0, :cond_1

    .line 111
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onAudioServerDied mLastFoldAngle = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mLastFoldAngle:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    iget v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mLastFoldAngle:I

    invoke-direct {p0, v0}, Lcom/android/server/audio/foldable/FoldStateHelper;->updateFoldedStateParam(I)V

    .line 114
    :cond_1
    return-void
.end method

.method public onUpdateAudioMode(I)V
    .locals 4
    .param p1, "mode"    # I

    .line 69
    iget-object v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mHandler:Lcom/android/server/audio/foldable/FoldStateHelper$H;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/audio/foldable/FoldStateHelper$H;->removeMessages(I)V

    .line 70
    const/4 v0, 0x2

    const-string v2, "FoldableAdapter.FoldStateHelper"

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mCallMode:Z

    if-nez v0, :cond_1

    .line 72
    const-string v0, "enter call mode ..."

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    iput-boolean v1, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mCallMode:Z

    .line 74
    iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mRegisterState:Z

    if-nez v0, :cond_1

    .line 75
    iput-boolean v1, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mRegisterState:Z

    .line 76
    const-string v0, "onUpdateAudioMode registerHingeAngleListener"

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    invoke-direct {p0}, Lcom/android/server/audio/foldable/FoldStateHelper;->registerHingeAngleListener()V

    .line 82
    :cond_1
    if-nez p1, :cond_2

    iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mCallMode:Z

    if-eqz v0, :cond_2

    .line 83
    const-string v0, "exit call mode ..."

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mCallMode:Z

    .line 85
    iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mRegisterState:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mMediaActive:Z

    if-nez v0, :cond_2

    .line 86
    const-string v0, "onUpdateAudioMode sendMessageDelayed 10s"

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    iget-object v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mHandler:Lcom/android/server/audio/foldable/FoldStateHelper$H;

    .line 88
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 87
    invoke-virtual {v0, v1, v2}, Lcom/android/server/audio/foldable/FoldStateHelper$H;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/audio/foldable/FoldStateHelper$H;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 91
    :cond_2
    return-void
.end method

.method public onUpdateMediaState(Z)V
    .locals 4
    .param p1, "mediaActive"    # Z

    .line 94
    iput-boolean p1, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mMediaActive:Z

    .line 95
    iget-object v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mHandler:Lcom/android/server/audio/foldable/FoldStateHelper$H;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/audio/foldable/FoldStateHelper$H;->removeMessages(I)V

    .line 96
    iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mMediaActive:Z

    const-string v2, "FoldableAdapter.FoldStateHelper"

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mCallMode:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mRegisterState:Z

    if-nez v0, :cond_1

    .line 97
    iput-boolean v1, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mRegisterState:Z

    .line 98
    const-string v0, "onUpdateMediaState registerHingeAngleListener"

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    invoke-direct {p0}, Lcom/android/server/audio/foldable/FoldStateHelper;->registerHingeAngleListener()V

    .line 101
    :cond_1
    iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mMediaActive:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mCallMode:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mRegisterState:Z

    if-eqz v0, :cond_2

    .line 102
    const-string v0, "onUpdateMediaState sendMessageDelayed 10s"

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    iget-object v0, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mHandler:Lcom/android/server/audio/foldable/FoldStateHelper$H;

    .line 104
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 103
    invoke-virtual {v0, v1, v2}, Lcom/android/server/audio/foldable/FoldStateHelper$H;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/audio/foldable/FoldStateHelper$H;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 106
    :cond_2
    return-void
.end method

.method public setAngleChangedListener(Lcom/android/server/audio/foldable/FoldStateHelper$AngleChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/android/server/audio/foldable/FoldStateHelper$AngleChangedListener;

    .line 37
    iput-object p1, p0, Lcom/android/server/audio/foldable/FoldStateHelper;->mListener:Lcom/android/server/audio/foldable/FoldStateHelper$AngleChangedListener;

    .line 38
    return-void
.end method
