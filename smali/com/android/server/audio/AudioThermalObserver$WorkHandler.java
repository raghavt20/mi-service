class com.android.server.audio.AudioThermalObserver$WorkHandler extends android.os.Handler {
	 /* .source "AudioThermalObserver.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/audio/AudioThermalObserver; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "WorkHandler" */
} // .end annotation
/* # instance fields */
final com.android.server.audio.AudioThermalObserver this$0; //synthetic
/* # direct methods */
public com.android.server.audio.AudioThermalObserver$WorkHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 65 */
this.this$0 = p1;
/* .line 66 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 67 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 3 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 71 */
try { // :try_start_0
	 /* iget v0, p1, Landroid/os/Message;->what:I */
	 /* packed-switch v0, :pswitch_data_0 */
	 /* .line 77 */
	 /* :pswitch_0 */
	 v0 = this.this$0;
	 com.android.server.audio.AudioThermalObserver .-$$Nest$fgetmHandler ( v0 );
	 /* const/16 v1, 0x9c5 */
	 (( com.android.server.audio.AudioThermalObserver$WorkHandler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/audio/AudioThermalObserver$WorkHandler;->removeMessages(I)V
	 /* .line 78 */
	 v0 = this.this$0;
	 final String v1 = "/sys/class/thermal/thermal_message/video_mode"; // const-string v1, "/sys/class/thermal/thermal_message/video_mode"
	 com.android.server.audio.AudioThermalObserver .-$$Nest$mcheckAudioHighTemperatureMode ( v0,v1 );
	 /* .line 79 */
	 /* .line 73 */
	 /* :pswitch_1 */
	 /* iget v0, p1, Landroid/os/Message;->arg1:I */
	 /* .line 74 */
	 /* .local v0, "mAudioHighTemperatureMode":I */
	 v1 = this.this$0;
	 com.android.server.audio.AudioThermalObserver .-$$Nest$msetAudioHighTemperatureMode ( v1,v0 );
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 75 */
	 /* nop */
	 /* .line 85 */
} // .end local v0 # "mAudioHighTemperatureMode":I
} // :goto_0
/* .line 83 */
/* :catch_0 */
/* move-exception v0 */
/* .line 84 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "handleMessage exception "; // const-string v2, "handleMessage exception "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "AudioThermalObserver"; // const-string v2, "AudioThermalObserver"
android.util.Log .e ( v2,v1 );
/* .line 86 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
return;
/* :pswitch_data_0 */
/* .packed-switch 0x9c5 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
