.class Lcom/android/server/audio/CloudServiceSettings$PropertySetting;
.super Lcom/android/server/audio/CloudServiceSettings$Setting;
.source "CloudServiceSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/CloudServiceSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PropertySetting"
.end annotation


# static fields
.field private static final KEY:Ljava/lang/String; = "key"

.field private static final VALUE:Ljava/lang/String; = "value"


# instance fields
.field private mKey:Ljava/lang/StringBuilder;

.field private mValue:Ljava/lang/StringBuilder;

.field final synthetic this$0:Lcom/android/server/audio/CloudServiceSettings;


# direct methods
.method constructor <init>(Lcom/android/server/audio/CloudServiceSettings;Lorg/json/JSONObject;)V
    .locals 3
    .param p1, "this$0"    # Lcom/android/server/audio/CloudServiceSettings;
    .param p2, "json"    # Lorg/json/JSONObject;

    .line 582
    iput-object p1, p0, Lcom/android/server/audio/CloudServiceSettings$PropertySetting;->this$0:Lcom/android/server/audio/CloudServiceSettings;

    .line 583
    invoke-direct {p0, p1, p2}, Lcom/android/server/audio/CloudServiceSettings$Setting;-><init>(Lcom/android/server/audio/CloudServiceSettings;Lorg/json/JSONObject;)V

    .line 585
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "key"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/audio/CloudServiceSettings$PropertySetting;->mKey:Ljava/lang/StringBuilder;

    .line 586
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "value"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/audio/CloudServiceSettings$PropertySetting;->mValue:Ljava/lang/StringBuilder;

    .line 587
    iget-boolean v0, p0, Lcom/android/server/audio/CloudServiceSettings$PropertySetting;->mSecreted:Z

    if-eqz v0, :cond_0

    .line 588
    iget-object v0, p0, Lcom/android/server/audio/CloudServiceSettings$PropertySetting;->mSecretedItem:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$PropertySetting;->mKey:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 589
    iget-object v0, p0, Lcom/android/server/audio/CloudServiceSettings$PropertySetting;->mSecretedItem:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$PropertySetting;->mValue:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 593
    :cond_0
    goto :goto_0

    .line 591
    :catch_0
    move-exception v0

    .line 592
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "CloudServiceSettings"

    const-string v2, "fail to parse FileDownloadSetting"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 594
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method


# virtual methods
.method public set()Z
    .locals 3

    .line 598
    invoke-super {p0}, Lcom/android/server/audio/CloudServiceSettings$Setting;->set()Z

    move-result v0

    if-nez v0, :cond_0

    .line 599
    const/4 v0, 0x0

    return v0

    .line 603
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/android/server/audio/CloudServiceSettings$PropertySetting;->mKey:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AudioCloudCtrlProp_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 604
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$PropertySetting;->mKey:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$PropertySetting;->mValue:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    goto :goto_0

    .line 606
    :cond_1
    iget-object v0, p0, Lcom/android/server/audio/CloudServiceSettings$PropertySetting;->mKey:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$PropertySetting;->mValue:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 609
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/audio/CloudServiceSettings$PropertySetting;->mSuccess:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 613
    goto :goto_1

    .line 610
    :catch_0
    move-exception v0

    .line 611
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "fail to set property, key: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/audio/CloudServiceSettings$PropertySetting;->mKey:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/audio/CloudServiceSettings$PropertySetting;->mValue:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "CloudServiceSettings"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 614
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    iget-boolean v0, p0, Lcom/android/server/audio/CloudServiceSettings$PropertySetting;->mSuccess:Z

    return v0
.end method

.method public updateTo(Lcom/android/server/audio/CloudServiceSettings$Setting;)Z
    .locals 2
    .param p1, "s"    # Lcom/android/server/audio/CloudServiceSettings$Setting;

    .line 619
    invoke-super {p0, p1}, Lcom/android/server/audio/CloudServiceSettings$Setting;->updateTo(Lcom/android/server/audio/CloudServiceSettings$Setting;)Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/android/server/audio/CloudServiceSettings$PropertySetting;

    if-eqz v0, :cond_0

    .line 620
    move-object v0, p1

    check-cast v0, Lcom/android/server/audio/CloudServiceSettings$PropertySetting;

    .line 621
    .local v0, "ps":Lcom/android/server/audio/CloudServiceSettings$PropertySetting;
    iget-object v1, v0, Lcom/android/server/audio/CloudServiceSettings$PropertySetting;->mKey:Ljava/lang/StringBuilder;

    iput-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$PropertySetting;->mKey:Ljava/lang/StringBuilder;

    .line 622
    iget-object v1, v0, Lcom/android/server/audio/CloudServiceSettings$PropertySetting;->mValue:Ljava/lang/StringBuilder;

    iput-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$PropertySetting;->mValue:Ljava/lang/StringBuilder;

    .line 623
    const/4 v1, 0x1

    return v1

    .line 625
    .end local v0    # "ps":Lcom/android/server/audio/CloudServiceSettings$PropertySetting;
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "fail to update PropertySetting: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$PropertySetting;->mSettingName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CloudServiceSettings"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 626
    const/4 v0, 0x0

    return v0
.end method
