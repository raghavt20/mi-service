.class public Lcom/android/server/audio/AudioGameEffect;
.super Ljava/lang/Object;
.source "AudioGameEffect.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/audio/AudioGameEffect$GameEffectThread;,
        Lcom/android/server/audio/AudioGameEffect$GameEffectHandler;
    }
.end annotation


# static fields
.field private static final CURRENT_PLATFORM:Ljava/lang/String; = "Build.BRAND"

.field private static final CURRENT_REGION:Ljava/lang/String; = "ro.miui.build.region"

.field private static final HEADSET_PLUG_IN:I = 0x1

.field private static final HEADSET_PLUG_OUT:I = 0x0

.field private static final IS_CEREGION:Ljava/lang/String; = "sys.audio.ceregion"

.field private static final IS_SUPPORT_SPEAKER:Ljava/lang/String; = "persist.vendor.audio.fpsop.game.effect.speaker"

.field private static final MSG_SET_PROCESS_LISTENER:I = 0x1

.field private static final MSG_START_GAME_EFFECT:I = 0x2

.field private static final MSG_STOP_GAME_EFFECT:I = 0x3

.field private static final PM_SERVICE_NAME:Ljava/lang/String; = "ProcessManager"

.field private static final QUERY_PM_SERVICE_DELAY_MS:I = 0x3e8

.field private static final QUERY_PM_SERVICE_MAX_TIMES:I = 0x14

.field private static final SEND_PARAMETER_DELAY_MS:I = 0x1f4

.field private static final SUPPORTED_DEVICES:[I

.field private static final TAG:Ljava/lang/String; = "AudioGameEffect"


# instance fields
.field private final PARAMETER_OFF:Ljava/lang/String;

.field private final PREFIX_PARAMETER_ON:Ljava/lang/String;

.field private final SETTING_PKG_NAME:Ljava/lang/String;

.field private volatile isEffectOn:Z

.field private final mContext:Landroid/content/Context;

.field private mCurrentDevice:Ljava/lang/String;

.field private mCurrentEnablePkg:Ljava/lang/String;

.field private mCurrentPlatform:Ljava/lang/String;

.field private mDeviceEffects:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mEnablePackages:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mForegroundInfoListener:Lmiui/process/IForegroundInfoListener$Stub;

.field private mFpsPackages:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mGameEffects:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Lcom/android/server/audio/AudioGameEffect$GameEffectHandler;

.field private final mHeadSetReceiver:Landroid/content/BroadcastReceiver;

.field private mHeadsetEffects:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mIsSupportSpeaker:Z

.field private mQueryPMServiceTime:I

.field private mSpeakerEffects:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mThread:Ljava/lang/Thread;


# direct methods
.method static bridge synthetic -$$Nest$fgetisEffectOn(Lcom/android/server/audio/AudioGameEffect;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/audio/AudioGameEffect;->isEffectOn:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmCurrentEnablePkg(Lcom/android/server/audio/AudioGameEffect;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/audio/AudioGameEffect;->mCurrentEnablePkg:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmForegroundInfoListener(Lcom/android/server/audio/AudioGameEffect;)Lmiui/process/IForegroundInfoListener$Stub;
    .locals 0

    iget-object p0, p0, Lcom/android/server/audio/AudioGameEffect;->mForegroundInfoListener:Lmiui/process/IForegroundInfoListener$Stub;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmQueryPMServiceTime(Lcom/android/server/audio/AudioGameEffect;)I
    .locals 0

    iget p0, p0, Lcom/android/server/audio/AudioGameEffect;->mQueryPMServiceTime:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmCurrentDevice(Lcom/android/server/audio/AudioGameEffect;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/audio/AudioGameEffect;->mCurrentDevice:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmCurrentEnablePkg(Lcom/android/server/audio/AudioGameEffect;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/audio/AudioGameEffect;->mCurrentEnablePkg:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmHandler(Lcom/android/server/audio/AudioGameEffect;Lcom/android/server/audio/AudioGameEffect$GameEffectHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/audio/AudioGameEffect;->mHandler:Lcom/android/server/audio/AudioGameEffect$GameEffectHandler;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmQueryPMServiceTime(Lcom/android/server/audio/AudioGameEffect;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/audio/AudioGameEffect;->mQueryPMServiceTime:I

    return-void
.end method

.method static bridge synthetic -$$Nest$minitProcessListenerAsync(Lcom/android/server/audio/AudioGameEffect;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/audio/AudioGameEffect;->initProcessListenerAsync()V

    return-void
.end method

.method static bridge synthetic -$$Nest$misCurrentDeviceSupported(Lcom/android/server/audio/AudioGameEffect;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/audio/AudioGameEffect;->isCurrentDeviceSupported()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$misCurrentSceneSupported(Lcom/android/server/audio/AudioGameEffect;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/audio/AudioGameEffect;->isCurrentSceneSupported()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$misPackageEnabled(Lcom/android/server/audio/AudioGameEffect;Ljava/lang/String;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/audio/AudioGameEffect;->isPackageEnabled(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$misSpatialAudioEnabled(Lcom/android/server/audio/AudioGameEffect;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/audio/AudioGameEffect;->isSpatialAudioEnabled()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$msendMsgDelay(Lcom/android/server/audio/AudioGameEffect;IJ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/audio/AudioGameEffect;->sendMsgDelay(IJ)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mstartGameEffect(Lcom/android/server/audio/AudioGameEffect;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/audio/AudioGameEffect;->startGameEffect()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mstopGameEffect(Lcom/android/server/audio/AudioGameEffect;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/audio/AudioGameEffect;->stopGameEffect()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 72
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/server/audio/AudioGameEffect;->SUPPORTED_DEVICES:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x8
        0x7
        0x1a
        0x3
        0x4
        0x16
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "cxt"    # Landroid/content/Context;

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const-string v0, "game_effect_packages"

    iput-object v0, p0, Lcom/android/server/audio/AudioGameEffect;->SETTING_PKG_NAME:Ljava/lang/String;

    .line 51
    const-string v0, "misound_fps_effect=T-"

    iput-object v0, p0, Lcom/android/server/audio/AudioGameEffect;->PREFIX_PARAMETER_ON:Ljava/lang/String;

    .line 52
    const-string v0, "misound_fps_effect=F-0-0-0-0"

    iput-object v0, p0, Lcom/android/server/audio/AudioGameEffect;->PARAMETER_OFF:Ljava/lang/String;

    .line 64
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/audio/AudioGameEffect;->mQueryPMServiceTime:I

    .line 76
    const-string v1, "Build.BRAND"

    const-string v2, ""

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/audio/AudioGameEffect;->mCurrentPlatform:Ljava/lang/String;

    .line 77
    const-string v1, "persist.vendor.audio.fpsop.game.effect.speaker"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/server/audio/AudioGameEffect;->mIsSupportSpeaker:Z

    .line 78
    iput-object v2, p0, Lcom/android/server/audio/AudioGameEffect;->mCurrentEnablePkg:Ljava/lang/String;

    .line 79
    iput-object v2, p0, Lcom/android/server/audio/AudioGameEffect;->mCurrentDevice:Ljava/lang/String;

    .line 80
    iput-boolean v0, p0, Lcom/android/server/audio/AudioGameEffect;->isEffectOn:Z

    .line 94
    new-instance v0, Lcom/android/server/audio/AudioGameEffect$1;

    invoke-direct {v0, p0}, Lcom/android/server/audio/AudioGameEffect$1;-><init>(Lcom/android/server/audio/AudioGameEffect;)V

    iput-object v0, p0, Lcom/android/server/audio/AudioGameEffect;->mForegroundInfoListener:Lmiui/process/IForegroundInfoListener$Stub;

    .line 262
    new-instance v0, Lcom/android/server/audio/AudioGameEffect$2;

    invoke-direct {v0, p0}, Lcom/android/server/audio/AudioGameEffect$2;-><init>(Lcom/android/server/audio/AudioGameEffect;)V

    iput-object v0, p0, Lcom/android/server/audio/AudioGameEffect;->mHeadSetReceiver:Landroid/content/BroadcastReceiver;

    .line 120
    iput-object p1, p0, Lcom/android/server/audio/AudioGameEffect;->mContext:Landroid/content/Context;

    .line 121
    invoke-direct {p0}, Lcom/android/server/audio/AudioGameEffect;->initLocalPackagesAndEffects()V

    .line 122
    new-instance v1, Lcom/android/server/audio/AudioGameEffect$GameEffectThread;

    invoke-direct {v1, p0}, Lcom/android/server/audio/AudioGameEffect$GameEffectThread;-><init>(Lcom/android/server/audio/AudioGameEffect;)V

    iput-object v1, p0, Lcom/android/server/audio/AudioGameEffect;->mThread:Ljava/lang/Thread;

    .line 123
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 125
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 126
    .local v1, "filter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 127
    const-string v2, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 128
    const-string v2, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 129
    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 130
    return-void
.end method

.method private getEnablePackagesFromCloudSettings()V
    .locals 6

    .line 134
    iget-object v0, p0, Lcom/android/server/audio/AudioGameEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "game_effect_packages"

    invoke-static {v0, v1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 135
    .local v0, "pkgs":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "get enable packages from setting: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AudioGameEffect"

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    if-eqz v0, :cond_1

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 137
    iget-object v2, p0, Lcom/android/server/audio/AudioGameEffect;->mEnablePackages:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 138
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 139
    .local v2, "packages":[Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v4, v2

    if-ge v3, v4, :cond_1

    .line 140
    aget-object v4, v2, v3

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 141
    iget-object v4, p0, Lcom/android/server/audio/AudioGameEffect;->mEnablePackages:Ljava/util/Set;

    aget-object v5, v2, v3

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 139
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 145
    .end local v2    # "packages":[Ljava/lang/String;
    .end local v3    # "i":I
    :cond_1
    return-void
.end method

.method private getEnableSpeakerFromCloudSettings()V
    .locals 3

    .line 148
    const-string v0, "persist.vendor.audio.fpsop.game.effect.speaker"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 149
    .local v0, "speaker":Z
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "get speaker from global setting: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AudioGameEffect"

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    iput-boolean v0, p0, Lcom/android/server/audio/AudioGameEffect;->mIsSupportSpeaker:Z

    .line 151
    return-void
.end method

.method private initLocalPackagesAndEffects()V
    .locals 3

    .line 396
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/android/server/audio/AudioGameEffect;->mEnablePackages:Ljava/util/Set;

    .line 397
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/android/server/audio/AudioGameEffect;->mFpsPackages:Ljava/util/Set;

    .line 398
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/audio/AudioGameEffect;->mGameEffects:Ljava/util/Map;

    .line 399
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/audio/AudioGameEffect;->mDeviceEffects:Ljava/util/Map;

    .line 400
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/audio/AudioGameEffect;->mHeadsetEffects:Ljava/util/ArrayList;

    .line 401
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/audio/AudioGameEffect;->mSpeakerEffects:Ljava/util/ArrayList;

    .line 403
    invoke-direct {p0}, Lcom/android/server/audio/AudioGameEffect;->parseAudioGameEffectXml()V

    .line 405
    iget-object v0, p0, Lcom/android/server/audio/AudioGameEffect;->mDeviceEffects:Ljava/util/Map;

    const-string v1, "headsets"

    iget-object v2, p0, Lcom/android/server/audio/AudioGameEffect;->mHeadsetEffects:Ljava/util/ArrayList;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 406
    iget-object v0, p0, Lcom/android/server/audio/AudioGameEffect;->mDeviceEffects:Ljava/util/Map;

    const-string/jumbo v1, "speaker"

    iget-object v2, p0, Lcom/android/server/audio/AudioGameEffect;->mSpeakerEffects:Ljava/util/ArrayList;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 407
    return-void
.end method

.method private initProcessListenerAsync()V
    .locals 3

    .line 213
    const/4 v0, 0x1

    const-wide/16 v1, 0x3e8

    invoke-direct {p0, v0, v1, v2}, Lcom/android/server/audio/AudioGameEffect;->sendMsgDelay(IJ)V

    .line 214
    return-void
.end method

.method private isCurrentDeviceSupported()Z
    .locals 9

    .line 217
    iget-object v0, p0, Lcom/android/server/audio/AudioGameEffect;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 218
    .local v0, "am":Landroid/media/AudioManager;
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getDevices(I)[Landroid/media/AudioDeviceInfo;

    move-result-object v1

    .line 219
    .local v1, "infos":[Landroid/media/AudioDeviceInfo;
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    const/4 v4, 0x1

    if-ge v3, v2, :cond_2

    aget-object v5, v1, v3

    .line 220
    .local v5, "info":Landroid/media/AudioDeviceInfo;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    sget-object v7, Lcom/android/server/audio/AudioGameEffect;->SUPPORTED_DEVICES:[I

    array-length v8, v7

    if-ge v6, v8, :cond_1

    .line 221
    aget v7, v7, v6

    invoke-virtual {v5}, Landroid/media/AudioDeviceInfo;->getType()I

    move-result v8

    if-ne v7, v8, :cond_0

    .line 222
    const-string v2, "headsets"

    iput-object v2, p0, Lcom/android/server/audio/AudioGameEffect;->mCurrentDevice:Ljava/lang/String;

    .line 223
    return v4

    .line 220
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 219
    .end local v5    # "info":Landroid/media/AudioDeviceInfo;
    .end local v6    # "i":I
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 227
    :cond_2
    const-string/jumbo v2, "speaker"

    iput-object v2, p0, Lcom/android/server/audio/AudioGameEffect;->mCurrentDevice:Ljava/lang/String;

    .line 228
    return v4
.end method

.method private isCurrentSceneSupported()Z
    .locals 6

    .line 232
    const-string/jumbo v0, "sys.audio.ceregion"

    const-string v1, "false"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 233
    .local v0, "currentIsCERegion":Ljava/lang/String;
    const-string v2, "ro.miui.build.region"

    const-string v3, ""

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 234
    .local v2, "currentRegion":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v3, 0x0

    const-string v4, "AudioGameEffect"

    if-eqz v1, :cond_0

    const-string v1, "cn"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 235
    const-string v1, "Non ceRegion, return"

    invoke-static {v4, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    return v3

    .line 239
    :cond_0
    invoke-direct {p0}, Lcom/android/server/audio/AudioGameEffect;->isSpatialAudioEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 240
    const-string/jumbo v1, "spatial audio enabled, return"

    invoke-static {v4, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    return v3

    .line 244
    :cond_1
    invoke-direct {p0}, Lcom/android/server/audio/AudioGameEffect;->getEnableSpeakerFromCloudSettings()V

    .line 245
    iget-object v1, p0, Lcom/android/server/audio/AudioGameEffect;->mCurrentDevice:Ljava/lang/String;

    const-string/jumbo v5, "speaker"

    if-ne v1, v5, :cond_2

    iget-boolean v1, p0, Lcom/android/server/audio/AudioGameEffect;->mIsSupportSpeaker:Z

    if-nez v1, :cond_2

    .line 246
    const-string v1, "AudioGameEffect does not support speaker"

    invoke-static {v4, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    return v3

    .line 250
    :cond_2
    iget-object v1, p0, Lcom/android/server/audio/AudioGameEffect;->mCurrentEnablePkg:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/android/server/audio/AudioGameEffect;->isFpsPackages(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/android/server/audio/AudioGameEffect;->mCurrentDevice:Ljava/lang/String;

    if-ne v1, v5, :cond_3

    .line 251
    const-string v1, "not FPS speaker no need to startGameEffect"

    invoke-static {v4, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    return v3

    .line 255
    :cond_3
    const/4 v1, 0x1

    return v1
.end method

.method private isFpsPackages(Ljava/lang/String;)Z
    .locals 1
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 159
    iget-object v0, p0, Lcom/android/server/audio/AudioGameEffect;->mFpsPackages:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private isPackageEnabled(Ljava/lang/String;)Z
    .locals 1
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 154
    invoke-direct {p0}, Lcom/android/server/audio/AudioGameEffect;->getEnablePackagesFromCloudSettings()V

    .line 155
    iget-object v0, p0, Lcom/android/server/audio/AudioGameEffect;->mEnablePackages:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private isSpatialAudioEnabled()Z
    .locals 3

    .line 259
    iget-object v0, p0, Lcom/android/server/audio/AudioGameEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "spatial_audio_feature_enable"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v2, v1

    :cond_0
    return v2
.end method

.method private parseAudioGameEffectXml()V
    .locals 19

    .line 320
    move-object/from16 v1, p0

    const-string v2, "inputStream close fail: "

    const-string v0, "parse parseAudioGameEffectXml: "

    const-string v3, "AudioGameEffect"

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    const/4 v4, 0x0

    .line 322
    .local v4, "items_xml":Ljava/io/File;
    const-string v0, ""

    .line 323
    .local v0, "items_xml_path":Ljava/lang/String;
    const/4 v5, 0x0

    .line 324
    .local v5, "inputStream":Ljava/io/InputStream;
    const/4 v6, 0x0

    .line 326
    .local v6, "document":Lorg/w3c/dom/Document;
    iget-object v7, v1, Lcom/android/server/audio/AudioGameEffect;->mCurrentPlatform:Ljava/lang/String;

    const-string v8, "MTK"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 327
    const-string/jumbo v0, "vendor/etc/audio_game_effect_items.xml"

    move-object v7, v0

    goto :goto_0

    .line 329
    :cond_0
    const-string v0, "odm/etc/audio_game_effect_items.xml"

    move-object v7, v0

    .line 333
    .end local v0    # "items_xml_path":Ljava/lang/String;
    .local v7, "items_xml_path":Ljava/lang/String;
    :goto_0
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    move-object v4, v0

    .line 334
    :try_start_1
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    if-nez v0, :cond_2

    .line 335
    :try_start_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "AudioGameEffect xml doesn\'t exist: "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 385
    if-eqz v5, :cond_1

    .line 386
    :try_start_3
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    .line 388
    :catch_0
    move-exception v0

    move-object v8, v0

    move-object v0, v8

    .line 389
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v3, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 390
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 391
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_1
    nop

    .line 336
    :goto_2
    return-void

    .line 384
    :catchall_0
    move-exception v0

    move-object/from16 v18, v7

    move-object v7, v0

    goto/16 :goto_c

    .line 380
    :catch_1
    move-exception v0

    move-object/from16 v18, v7

    goto/16 :goto_9

    .line 338
    :cond_2
    :try_start_4
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    move-object v5, v0

    .line 339
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v0

    .line 340
    .local v0, "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    invoke-virtual {v0}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v8

    .line 341
    .local v8, "builder":Ljavax/xml/parsers/DocumentBuilder;
    invoke-virtual {v8, v5}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v9

    move-object v6, v9

    .line 344
    const-string v9, "package"

    invoke-interface {v6, v9}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v9

    .line 345
    .local v9, "packageList":Lorg/w3c/dom/NodeList;
    const/4 v10, 0x0

    .local v10, "packageIdx":I
    :goto_3
    invoke-interface {v9}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v11
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    const-string v12, "name"

    const/4 v13, 0x1

    if-ge v10, v11, :cond_8

    .line 346
    :try_start_5
    invoke-interface {v9, v10}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v11

    .line 349
    .local v11, "packageNode":Lorg/w3c/dom/Node;
    invoke-interface {v11}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v14

    if-ne v14, v13, :cond_7

    .line 350
    invoke-interface {v11}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v14

    .line 351
    .local v14, "devicesList":Lorg/w3c/dom/NodeList;
    iget-object v15, v1, Lcom/android/server/audio/AudioGameEffect;->mEnablePackages:Ljava/util/Set;

    move-object v13, v11

    check-cast v13, Lorg/w3c/dom/Element;

    invoke-interface {v13, v12}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-interface {v15, v13}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 352
    iget-object v13, v1, Lcom/android/server/audio/AudioGameEffect;->mGameEffects:Ljava/util/Map;

    move-object v15, v11

    check-cast v15, Lorg/w3c/dom/Element;

    invoke-interface {v15, v12}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v16, v0

    .end local v0    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .local v16, "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v13, v15, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    const/4 v0, 0x0

    .local v0, "deviceIdx":I
    :goto_4
    invoke-interface {v14}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v13

    if-ge v0, v13, :cond_6

    .line 356
    invoke-interface {v14, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v13

    .line 359
    .local v13, "deviceNode":Lorg/w3c/dom/Node;
    invoke-interface {v13}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v15
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    move-object/from16 v17, v4

    const/4 v4, 0x1

    .end local v4    # "items_xml":Ljava/io/File;
    .local v17, "items_xml":Ljava/io/File;
    if-ne v15, v4, :cond_4

    .line 360
    :try_start_6
    move-object v4, v13

    check-cast v4, Lorg/w3c/dom/Element;

    invoke-interface {v4, v12}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v15, "speaker"

    invoke-virtual {v4, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    const-string v15, "param"

    if-eqz v4, :cond_3

    .line 361
    :try_start_7
    iget-object v4, v1, Lcom/android/server/audio/AudioGameEffect;->mSpeakerEffects:Ljava/util/ArrayList;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-object/from16 v18, v7

    .end local v7    # "items_xml_path":Ljava/lang/String;
    .local v18, "items_xml_path":Ljava/lang/String;
    :try_start_8
    move-object v7, v13

    check-cast v7, Lorg/w3c/dom/Element;

    invoke-interface {v7, v15}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 362
    .end local v18    # "items_xml_path":Ljava/lang/String;
    .restart local v7    # "items_xml_path":Ljava/lang/String;
    :cond_3
    move-object/from16 v18, v7

    .end local v7    # "items_xml_path":Ljava/lang/String;
    .restart local v18    # "items_xml_path":Ljava/lang/String;
    move-object v4, v13

    check-cast v4, Lorg/w3c/dom/Element;

    invoke-interface {v4, v12}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v7, "headset"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 363
    iget-object v4, v1, Lcom/android/server/audio/AudioGameEffect;->mHeadsetEffects:Ljava/util/ArrayList;

    move-object v7, v13

    check-cast v7, Lorg/w3c/dom/Element;

    invoke-interface {v7, v15}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 384
    .end local v0    # "deviceIdx":I
    .end local v8    # "builder":Ljavax/xml/parsers/DocumentBuilder;
    .end local v9    # "packageList":Lorg/w3c/dom/NodeList;
    .end local v10    # "packageIdx":I
    .end local v11    # "packageNode":Lorg/w3c/dom/Node;
    .end local v13    # "deviceNode":Lorg/w3c/dom/Node;
    .end local v14    # "devicesList":Lorg/w3c/dom/NodeList;
    .end local v16    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v18    # "items_xml_path":Ljava/lang/String;
    .restart local v7    # "items_xml_path":Ljava/lang/String;
    :catchall_1
    move-exception v0

    move-object/from16 v18, v7

    move-object v7, v0

    move-object/from16 v4, v17

    .end local v7    # "items_xml_path":Ljava/lang/String;
    .restart local v18    # "items_xml_path":Ljava/lang/String;
    goto/16 :goto_c

    .line 380
    .end local v18    # "items_xml_path":Ljava/lang/String;
    .restart local v7    # "items_xml_path":Ljava/lang/String;
    :catch_2
    move-exception v0

    move-object/from16 v18, v7

    move-object/from16 v4, v17

    .end local v7    # "items_xml_path":Ljava/lang/String;
    .restart local v18    # "items_xml_path":Ljava/lang/String;
    goto/16 :goto_9

    .line 359
    .end local v18    # "items_xml_path":Ljava/lang/String;
    .restart local v0    # "deviceIdx":I
    .restart local v7    # "items_xml_path":Ljava/lang/String;
    .restart local v8    # "builder":Ljavax/xml/parsers/DocumentBuilder;
    .restart local v9    # "packageList":Lorg/w3c/dom/NodeList;
    .restart local v10    # "packageIdx":I
    .restart local v11    # "packageNode":Lorg/w3c/dom/Node;
    .restart local v13    # "deviceNode":Lorg/w3c/dom/Node;
    .restart local v14    # "devicesList":Lorg/w3c/dom/NodeList;
    .restart local v16    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    :cond_4
    move-object/from16 v18, v7

    .line 355
    .end local v7    # "items_xml_path":Ljava/lang/String;
    .end local v13    # "deviceNode":Lorg/w3c/dom/Node;
    .restart local v18    # "items_xml_path":Ljava/lang/String;
    :cond_5
    :goto_5
    add-int/lit8 v0, v0, 0x1

    move-object/from16 v4, v17

    move-object/from16 v7, v18

    goto :goto_4

    .end local v17    # "items_xml":Ljava/io/File;
    .end local v18    # "items_xml_path":Ljava/lang/String;
    .restart local v4    # "items_xml":Ljava/io/File;
    .restart local v7    # "items_xml_path":Ljava/lang/String;
    :cond_6
    move-object/from16 v17, v4

    move-object/from16 v18, v7

    .end local v4    # "items_xml":Ljava/io/File;
    .end local v7    # "items_xml_path":Ljava/lang/String;
    .restart local v17    # "items_xml":Ljava/io/File;
    .restart local v18    # "items_xml_path":Ljava/lang/String;
    goto :goto_6

    .line 349
    .end local v14    # "devicesList":Lorg/w3c/dom/NodeList;
    .end local v16    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v17    # "items_xml":Ljava/io/File;
    .end local v18    # "items_xml_path":Ljava/lang/String;
    .local v0, "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .restart local v4    # "items_xml":Ljava/io/File;
    .restart local v7    # "items_xml_path":Ljava/lang/String;
    :cond_7
    move-object/from16 v16, v0

    move-object/from16 v17, v4

    move-object/from16 v18, v7

    .line 345
    .end local v0    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v4    # "items_xml":Ljava/io/File;
    .end local v7    # "items_xml_path":Ljava/lang/String;
    .end local v11    # "packageNode":Lorg/w3c/dom/Node;
    .restart local v16    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .restart local v17    # "items_xml":Ljava/io/File;
    .restart local v18    # "items_xml_path":Ljava/lang/String;
    :goto_6
    add-int/lit8 v10, v10, 0x1

    move-object/from16 v0, v16

    move-object/from16 v4, v17

    move-object/from16 v7, v18

    goto/16 :goto_3

    .end local v16    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v17    # "items_xml":Ljava/io/File;
    .end local v18    # "items_xml_path":Ljava/lang/String;
    .restart local v0    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .restart local v4    # "items_xml":Ljava/io/File;
    .restart local v7    # "items_xml_path":Ljava/lang/String;
    :cond_8
    move-object/from16 v16, v0

    move-object/from16 v17, v4

    move-object/from16 v18, v7

    .line 371
    .end local v0    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v4    # "items_xml":Ljava/io/File;
    .end local v7    # "items_xml_path":Ljava/lang/String;
    .end local v10    # "packageIdx":I
    .restart local v16    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .restart local v17    # "items_xml":Ljava/io/File;
    .restart local v18    # "items_xml_path":Ljava/lang/String;
    const-string v0, "FPSpackage"

    invoke-interface {v6, v0}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    .line 372
    .local v0, "FPSpackageList":Lorg/w3c/dom/NodeList;
    const/4 v4, 0x0

    .local v4, "FPSpackageIdx":I
    :goto_7
    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v7

    if-ge v4, v7, :cond_a

    .line 373
    invoke-interface {v0, v4}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    .line 376
    .local v7, "FPSpackageNode":Lorg/w3c/dom/Node;
    invoke-interface {v7}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_9

    .line 377
    iget-object v10, v1, Lcom/android/server/audio/AudioGameEffect;->mFpsPackages:Ljava/util/Set;

    move-object v13, v7

    check-cast v13, Lorg/w3c/dom/Element;

    invoke-interface {v13, v12}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-interface {v10, v13}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 372
    .end local v7    # "FPSpackageNode":Lorg/w3c/dom/Node;
    :cond_9
    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    .line 385
    .end local v0    # "FPSpackageList":Lorg/w3c/dom/NodeList;
    .end local v4    # "FPSpackageIdx":I
    .end local v8    # "builder":Ljavax/xml/parsers/DocumentBuilder;
    .end local v9    # "packageList":Lorg/w3c/dom/NodeList;
    .end local v16    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    :cond_a
    nop

    .line 386
    :try_start_9
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3

    .line 391
    goto :goto_8

    .line 388
    :catch_3
    move-exception v0

    move-object v4, v0

    move-object v0, v4

    .line 389
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v3, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 390
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 392
    .end local v0    # "e":Ljava/lang/Exception;
    nop

    .line 393
    :goto_8
    move-object/from16 v4, v17

    goto :goto_b

    .line 384
    :catchall_2
    move-exception v0

    move-object v7, v0

    move-object/from16 v4, v17

    goto :goto_c

    .line 380
    :catch_4
    move-exception v0

    move-object/from16 v4, v17

    goto :goto_9

    .line 384
    .end local v17    # "items_xml":Ljava/io/File;
    .end local v18    # "items_xml_path":Ljava/lang/String;
    .local v4, "items_xml":Ljava/io/File;
    .local v7, "items_xml_path":Ljava/lang/String;
    :catchall_3
    move-exception v0

    move-object/from16 v17, v4

    move-object/from16 v18, v7

    move-object v7, v0

    .end local v4    # "items_xml":Ljava/io/File;
    .end local v7    # "items_xml_path":Ljava/lang/String;
    .restart local v17    # "items_xml":Ljava/io/File;
    .restart local v18    # "items_xml_path":Ljava/lang/String;
    goto :goto_c

    .line 380
    .end local v17    # "items_xml":Ljava/io/File;
    .end local v18    # "items_xml_path":Ljava/lang/String;
    .restart local v4    # "items_xml":Ljava/io/File;
    .restart local v7    # "items_xml_path":Ljava/lang/String;
    :catch_5
    move-exception v0

    move-object/from16 v17, v4

    move-object/from16 v18, v7

    .end local v4    # "items_xml":Ljava/io/File;
    .end local v7    # "items_xml_path":Ljava/lang/String;
    .restart local v17    # "items_xml":Ljava/io/File;
    .restart local v18    # "items_xml_path":Ljava/lang/String;
    goto :goto_9

    .line 384
    .end local v17    # "items_xml":Ljava/io/File;
    .end local v18    # "items_xml_path":Ljava/lang/String;
    .restart local v4    # "items_xml":Ljava/io/File;
    .restart local v7    # "items_xml_path":Ljava/lang/String;
    :catchall_4
    move-exception v0

    move-object/from16 v18, v7

    move-object v7, v0

    .end local v7    # "items_xml_path":Ljava/lang/String;
    .restart local v18    # "items_xml_path":Ljava/lang/String;
    goto :goto_c

    .line 380
    .end local v18    # "items_xml_path":Ljava/lang/String;
    .restart local v7    # "items_xml_path":Ljava/lang/String;
    :catch_6
    move-exception v0

    move-object/from16 v18, v7

    .line 381
    .end local v7    # "items_xml_path":Ljava/lang/String;
    .restart local v0    # "e":Ljava/lang/Exception;
    .restart local v18    # "items_xml_path":Ljava/lang/String;
    :goto_9
    :try_start_a
    const-string v7, "Exception: "

    invoke-static {v3, v7, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 382
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_5

    .line 385
    .end local v0    # "e":Ljava/lang/Exception;
    if-eqz v5, :cond_b

    .line 386
    :try_start_b
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_7

    goto :goto_a

    .line 388
    :catch_7
    move-exception v0

    move-object v7, v0

    move-object v0, v7

    .line 389
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-static {v3, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 390
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 392
    .end local v0    # "e":Ljava/lang/Exception;
    goto :goto_b

    .line 391
    :cond_b
    :goto_a
    nop

    .line 393
    :goto_b
    return-void

    .line 384
    :catchall_5
    move-exception v0

    move-object v7, v0

    .line 385
    :goto_c
    if-eqz v5, :cond_c

    .line 386
    :try_start_c
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_8

    goto :goto_d

    .line 388
    :catch_8
    move-exception v0

    move-object v8, v0

    move-object v0, v8

    .line 389
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-static {v3, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 390
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_e

    .line 391
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_c
    :goto_d
    nop

    .line 392
    :goto_e
    throw v7
.end method

.method private sendMsgDelay(IJ)V
    .locals 2
    .param p1, "mesWhat"    # I
    .param p2, "delay"    # J

    .line 410
    iget-object v0, p0, Lcom/android/server/audio/AudioGameEffect;->mHandler:Lcom/android/server/audio/AudioGameEffect$GameEffectHandler;

    invoke-virtual {v0, p1}, Lcom/android/server/audio/AudioGameEffect$GameEffectHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 411
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/audio/AudioGameEffect;->mHandler:Lcom/android/server/audio/AudioGameEffect$GameEffectHandler;

    invoke-virtual {v1, v0, p2, p3}, Lcom/android/server/audio/AudioGameEffect$GameEffectHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 412
    return-void
.end method

.method private startGameEffect()V
    .locals 5

    .line 301
    iget-object v0, p0, Lcom/android/server/audio/AudioGameEffect;->mGameEffects:Ljava/util/Map;

    iget-object v1, p0, Lcom/android/server/audio/AudioGameEffect;->mCurrentEnablePkg:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 303
    .local v0, "paramIndex":Ljava/lang/String;
    const-string v1, "AudioGameEffect"

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/server/audio/AudioGameEffect;->isCurrentSceneSupported()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 304
    iget-object v2, p0, Lcom/android/server/audio/AudioGameEffect;->mDeviceEffects:Ljava/util/Map;

    iget-object v3, p0, Lcom/android/server/audio/AudioGameEffect;->mCurrentDevice:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 305
    .local v2, "parameterSuffix":Ljava/lang/String;
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/server/audio/AudioGameEffect;->isEffectOn:Z

    .line 306
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "startGameEffect parameter=misound_fps_effect=T-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "misound_fps_effect=T-"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 308
    .end local v2    # "parameterSuffix":Ljava/lang/String;
    goto :goto_0

    .line 309
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "no parameter found for package: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/audio/AudioGameEffect;->mCurrentEnablePkg:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    :goto_0
    return-void
.end method

.method private stopGameEffect()V
    .locals 2

    .line 314
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/audio/AudioGameEffect;->isEffectOn:Z

    .line 315
    const-string v0, "AudioGameEffect"

    const-string/jumbo v1, "stopGameEffect parameter=misound_fps_effect=F-0-0-0-0"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    const-string v0, "misound_fps_effect=F-0-0-0-0"

    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 317
    return-void
.end method
