.class Lcom/android/server/audio/MQSUtils$AudioStateTrackData;
.super Ljava/lang/Object;
.source "MQSUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/MQSUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AudioStateTrackData"
.end annotation


# instance fields
.field final mA2dpConnectState:I

.field final mAbnormalReason:Ljava/lang/String;

.field final mAudioStateName:Ljava/lang/String;

.field final mBtName:Ljava/lang/String;

.field final mEventSource:Ljava/lang/String;

.field final mPackageName:Ljava/lang/String;

.field final mScoConnectState:I


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V
    .locals 1
    .param p1, "errorType"    # I
    .param p2, "eventSource"    # Ljava/lang/String;
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "btName"    # Ljava/lang/String;
    .param p5, "a2dpConnectState"    # I
    .param p6, "scoConnectState"    # I
    .param p7, "audioStateName"    # Ljava/lang/String;

    .line 684
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 685
    invoke-static {p1}, Lcom/android/server/audio/MQSUtils$AudioStateTrackData;->audioErrorTypeToString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/audio/MQSUtils$AudioStateTrackData;->mAbnormalReason:Ljava/lang/String;

    .line 686
    iput-object p2, p0, Lcom/android/server/audio/MQSUtils$AudioStateTrackData;->mEventSource:Ljava/lang/String;

    .line 687
    iput-object p3, p0, Lcom/android/server/audio/MQSUtils$AudioStateTrackData;->mPackageName:Ljava/lang/String;

    .line 688
    iput-object p4, p0, Lcom/android/server/audio/MQSUtils$AudioStateTrackData;->mBtName:Ljava/lang/String;

    .line 689
    iput p5, p0, Lcom/android/server/audio/MQSUtils$AudioStateTrackData;->mA2dpConnectState:I

    .line 690
    iput p6, p0, Lcom/android/server/audio/MQSUtils$AudioStateTrackData;->mScoConnectState:I

    .line 691
    iput-object p7, p0, Lcom/android/server/audio/MQSUtils$AudioStateTrackData;->mAudioStateName:Ljava/lang/String;

    .line 692
    return-void
.end method

.method public static audioErrorTypeToString(I)Ljava/lang/String;
    .locals 2
    .param p0, "errorType"    # I

    .line 695
    packed-switch p0, :pswitch_data_0

    .line 715
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "unknown error:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 713
    :pswitch_1
    const-string v0, "audioLeaky"

    return-object v0

    .line 711
    :pswitch_2
    const-string v0, "audioPolicyManagerException"

    return-object v0

    .line 709
    :pswitch_3
    const-string v0, "bluetoothScoException"

    return-object v0

    .line 707
    :pswitch_4
    const-string v0, "audioScoException"

    return-object v0

    .line 705
    :pswitch_5
    const-string v0, "btConnectionException"

    return-object v0

    .line 703
    :pswitch_6
    const-string v0, "externalAppScoException"

    return-object v0

    .line 701
    :pswitch_7
    const-string v0, "ScoTimeOutException"

    return-object v0

    .line 699
    :pswitch_8
    const-string v0, "modeTimeOutException"

    return-object v0

    .line 697
    :pswitch_9
    const-string v0, "ok"

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_9
        :pswitch_0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .line 721
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AudioStateTrackData: mAbnormalReason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/audio/MQSUtils$AudioStateTrackData;->mAbnormalReason:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mEventSource="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/audio/MQSUtils$AudioStateTrackData;->mEventSource:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mPackageName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/audio/MQSUtils$AudioStateTrackData;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mBtName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/audio/MQSUtils$AudioStateTrackData;->mBtName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mScoConnectState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/audio/MQSUtils$AudioStateTrackData;->mScoConnectState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mA2dpConnectState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/audio/MQSUtils$AudioStateTrackData;->mA2dpConnectState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
