public class com.android.server.audio.MediaFocusControlStubImpl implements com.android.server.audio.MediaFocusControlStub {
	 /* .source "MediaFocusControlStubImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final Long CHECK_DELAY_MS;
	 private static final Integer DELAY_COUNT_MAX;
	 private static final Long DELAY_DISPATCH_FOCUS_CHANGE_MS;
	 private static final Integer MSG_L_DELAYED_FOCUS_CHANGE;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private Integer mAudioMode;
	 private Boolean mBtAudioSuspended;
	 private Integer mDelayCount;
	 private Boolean mInDelayDispatch;
	 private Integer mLastReleasedUid;
	 private Integer mPendingAudioMode;
	 /* # direct methods */
	 public com.android.server.audio.MediaFocusControlStubImpl ( ) {
		 /* .locals 1 */
		 /* .line 12 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 19 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput-boolean v0, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mInDelayDispatch:Z */
		 /* .line 20 */
		 /* iput-boolean v0, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mBtAudioSuspended:Z */
		 /* .line 21 */
		 /* iput v0, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mAudioMode:I */
		 /* .line 22 */
		 /* iput v0, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mPendingAudioMode:I */
		 /* .line 26 */
		 /* iput v0, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mDelayCount:I */
		 return;
	 } // .end method
	 private Boolean inModeChanging ( ) {
		 /* .locals 6 */
		 /* .line 64 */
		 /* iget v0, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mPendingAudioMode:I */
		 int v1 = 1; // const/4 v1, 0x1
		 int v2 = 0; // const/4 v2, 0x0
		 int v3 = 3; // const/4 v3, 0x3
		 /* if-ne v0, v3, :cond_0 */
		 /* iget v4, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mAudioMode:I */
		 /* if-nez v4, :cond_0 */
		 /* move v4, v1 */
	 } // :cond_0
	 /* move v4, v2 */
	 /* .line 66 */
	 /* .local v4, "fromCommunicationToNormal":Z */
} // :goto_0
/* if-nez v0, :cond_1 */
/* iget v0, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mAudioMode:I */
/* if-ne v0, v3, :cond_1 */
/* move v0, v1 */
} // :cond_1
/* move v0, v2 */
/* .line 68 */
/* .local v0, "fromNormalToCommunication":Z */
} // :goto_1
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "inModeChanging: fromCommunicationToNormal="; // const-string v5, "inModeChanging: fromCommunicationToNormal="
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v5 = ", fromNormalToCommunication="; // const-string v5, ", fromNormalToCommunication="
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "MediaFocusControlStubImpl"; // const-string v5, "MediaFocusControlStubImpl"
android.util.Log .d ( v5,v3 );
/* .line 70 */
/* if-nez v4, :cond_3 */
if ( v0 != null) { // if-eqz v0, :cond_2
} // :cond_2
/* move v1, v2 */
} // :cond_3
} // :goto_2
} // .end method
/* # virtual methods */
public void delayFocusChangeDispatch ( Integer p0, com.android.server.audio.FocusRequester p1, java.lang.String p2, android.os.Handler p3 ) {
/* .locals 8 */
/* .param p1, "focusGain" # I */
/* .param p2, "fr" # Lcom/android/server/audio/FocusRequester; */
/* .param p3, "clientId" # Ljava/lang/String; */
/* .param p4, "focusHandler" # Landroid/os/Handler; */
/* .line 96 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "delay dispatching "; // const-string v1, "delay dispatching "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " to "; // const-string v1, " to "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " for "; // const-string v1, " for "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-wide/16 v1, 0x190 */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = "ms"; // const-string v1, "ms"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MediaFocusControlStubImpl"; // const-string v1, "MediaFocusControlStubImpl"
android.util.Log .d ( v1,v0 );
/* .line 98 */
/* const-wide/16 v5, 0x190 */
/* move-object v2, p0 */
/* move v3, p1 */
/* move-object v4, p2 */
/* move-object v7, p4 */
/* invoke-virtual/range {v2 ..v7}, Lcom/android/server/audio/MediaFocusControlStubImpl;->postDelayedFocusChange(ILcom/android/server/audio/FocusRequester;JLandroid/os/Handler;)V */
/* .line 99 */
int v0 = 1; // const/4 v0, 0x1
(( com.android.server.audio.MediaFocusControlStubImpl ) p0 ).setAudioFocusDelayDispatchState ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/audio/MediaFocusControlStubImpl;->setAudioFocusDelayDispatchState(Z)V
/* .line 100 */
return;
} // .end method
public void handleDelayedMessage ( android.os.Message p0, android.os.Handler p1 ) {
/* .locals 8 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .param p2, "focusHandler" # Landroid/os/Handler; */
/* .line 127 */
v0 = this.obj;
/* check-cast v0, Lcom/android/server/audio/FocusRequester; */
/* .line 128 */
/* .local v0, "fr":Lcom/android/server/audio/FocusRequester; */
/* iget v7, p1, Landroid/os/Message;->arg1:I */
/* .line 129 */
/* .local v7, "focusChange":I */
/* iget-boolean v1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mBtAudioSuspended:Z */
final String v2 = "MediaFocusControlStubImpl"; // const-string v2, "MediaFocusControlStubImpl"
/* if-nez v1, :cond_0 */
/* iget v1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mAudioMode:I */
/* if-nez v1, :cond_0 */
v1 = /* invoke-direct {p0}, Lcom/android/server/audio/MediaFocusControlStubImpl;->inModeChanging()Z */
if ( v1 != null) { // if-eqz v1, :cond_1
} // :cond_0
/* iget v1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mDelayCount:I */
/* const/16 v3, 0x8 */
/* if-ge v1, v3, :cond_1 */
/* .line 131 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "bt/audioMode switch not ready, delay dispatching focus change 200ms, count "; // const-string v3, "bt/audioMode switch not ready, delay dispatching focus change 200ms, count "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mDelayCount:I */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v1 );
/* .line 133 */
/* iget v1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mDelayCount:I */
/* add-int/lit8 v1, v1, 0x1 */
/* iput v1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mDelayCount:I */
/* .line 134 */
/* const-wide/16 v4, 0xc8 */
/* move-object v1, p0 */
/* move v2, v7 */
/* move-object v3, v0 */
/* move-object v6, p2 */
/* invoke-virtual/range {v1 ..v6}, Lcom/android/server/audio/MediaFocusControlStubImpl;->postDelayedFocusChange(ILcom/android/server/audio/FocusRequester;JLandroid/os/Handler;)V */
/* .line 136 */
} // :cond_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "handling MSG_L_DELAYED_FOCUS_CHANGE focusChange="; // const-string v3, "handling MSG_L_DELAYED_FOCUS_CHANGE focusChange="
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " packageName="; // const-string v3, " packageName="
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 137 */
(( com.android.server.audio.FocusRequester ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Lcom/android/server/audio/FocusRequester;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " clientId="; // const-string v3, " clientId="
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.audio.FocusRequester ) v0 ).getClientId ( ); // invoke-virtual {v0}, Lcom/android/server/audio/FocusRequester;->getClientId()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 136 */
android.util.Log .d ( v2,v1 );
/* .line 138 */
(( com.android.server.audio.FocusRequester ) v0 ).dispatchFocusChange ( v7 ); // invoke-virtual {v0, v7}, Lcom/android/server/audio/FocusRequester;->dispatchFocusChange(I)I
/* .line 139 */
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.audio.MediaFocusControlStubImpl ) p0 ).setAudioFocusDelayDispatchState ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/audio/MediaFocusControlStubImpl;->setAudioFocusDelayDispatchState(Z)V
/* .line 140 */
/* iput v1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mDelayCount:I */
/* .line 142 */
} // :goto_0
return;
} // .end method
public void handleDelayedMessageWithdraw ( com.android.server.audio.FocusRequester p0, android.os.Handler p1 ) {
/* .locals 2 */
/* .param p1, "fr" # Lcom/android/server/audio/FocusRequester; */
/* .param p2, "focusHandler" # Landroid/os/Handler; */
/* .line 117 */
/* iget-boolean v0, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mInDelayDispatch:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 118 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "handleDelayedMessageWithdraw(): withdraw message to "; // const-string v1, "handleDelayedMessageWithdraw(): withdraw message to "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 119 */
(( com.android.server.audio.FocusRequester ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Lcom/android/server/audio/FocusRequester;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 118 */
final String v1 = "MediaFocusControlStubImpl"; // const-string v1, "MediaFocusControlStubImpl"
android.util.Log .d ( v1,v0 );
/* .line 120 */
int v0 = 3; // const/4 v0, 0x3
(( android.os.Handler ) p2 ).removeEqualMessages ( v0, p1 ); // invoke-virtual {p2, v0, p1}, Landroid/os/Handler;->removeEqualMessages(ILjava/lang/Object;)V
/* .line 121 */
int v0 = 0; // const/4 v0, 0x0
(( com.android.server.audio.MediaFocusControlStubImpl ) p0 ).setAudioFocusDelayDispatchState ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/audio/MediaFocusControlStubImpl;->setAudioFocusDelayDispatchState(Z)V
/* .line 123 */
} // :cond_0
return;
} // .end method
public Boolean isDelayedDispatchFocusChangeNeeded ( Integer p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "topUid" # I */
/* .param p2, "focusGain" # I */
/* .line 41 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "isDelayedDispatchFocusChangeNeeded: topUid="; // const-string v1, "isDelayedDispatchFocusChangeNeeded: topUid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mLastReleasedUid="; // const-string v1, ", mLastReleasedUid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mLastReleasedUid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mPendingAudioMode="; // const-string v1, ", mPendingAudioMode="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mPendingAudioMode:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mBtAudioSuspended="; // const-string v1, ", mBtAudioSuspended="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mBtAudioSuspended:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ", focusGain="; // const-string v1, ", focusGain="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mAudioMode="; // const-string v1, ", mAudioMode="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mAudioMode:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MediaFocusControlStubImpl"; // const-string v1, "MediaFocusControlStubImpl"
android.util.Log .d ( v1,v0 );
/* .line 47 */
/* iget v0, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mAudioMode:I */
int v1 = 3; // const/4 v1, 0x3
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v1, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mBtAudioSuspended:Z */
/* if-nez v0, :cond_1 */
/* .line 48 */
} // :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/audio/MediaFocusControlStubImpl;->inModeChanging()Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* if-ne p2, v2, :cond_2 */
/* iget v0, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mLastReleasedUid:I */
/* if-eq p1, v0, :cond_2 */
} // :cond_1
} // :cond_2
int v2 = 0; // const/4 v2, 0x0
/* .line 47 */
} // :goto_0
} // .end method
public void postDelayedFocusChange ( Integer p0, com.android.server.audio.FocusRequester p1, Long p2, android.os.Handler p3 ) {
/* .locals 2 */
/* .param p1, "focusChange" # I */
/* .param p2, "fr" # Lcom/android/server/audio/FocusRequester; */
/* .param p3, "delayMs" # J */
/* .param p5, "focusHandler" # Landroid/os/Handler; */
/* .line 107 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "postDelayedFocusChange(): send focusChange="; // const-string v1, "postDelayedFocusChange(): send focusChange="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " to "; // const-string v1, " to "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 108 */
(( com.android.server.audio.FocusRequester ) p2 ).getPackageName ( ); // invoke-virtual {p2}, Lcom/android/server/audio/FocusRequester;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " delayMs="; // const-string v1, " delayMs="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3, p4 ); // invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 107 */
final String v1 = "MediaFocusControlStubImpl"; // const-string v1, "MediaFocusControlStubImpl"
android.util.Log .d ( v1,v0 );
/* .line 109 */
int v0 = 3; // const/4 v0, 0x3
(( android.os.Handler ) p5 ).removeEqualMessages ( v0, p2 ); // invoke-virtual {p5, v0, p2}, Landroid/os/Handler;->removeEqualMessages(ILjava/lang/Object;)V
/* .line 110 */
/* nop */
/* .line 111 */
int v1 = 0; // const/4 v1, 0x0
(( android.os.Handler ) p5 ).obtainMessage ( v0, p1, v1, p2 ); // invoke-virtual {p5, v0, p1, v1, p2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;
/* .line 110 */
(( android.os.Handler ) p5 ).sendMessageDelayed ( v0, p3, p4 ); // invoke-virtual {p5, v0, p3, p4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 113 */
return;
} // .end method
public void setAudioFocusDelayDispatchState ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "state" # Z */
/* .line 81 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setAudioFocusDelayDispatchState(): prior state=" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mInDelayDispatch:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ", current state="; // const-string v1, ", current state="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MediaFocusControlStubImpl"; // const-string v1, "MediaFocusControlStubImpl"
android.util.Log .d ( v1,v0 );
/* .line 83 */
/* iput-boolean p1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mInDelayDispatch:Z */
/* .line 84 */
return;
} // .end method
public void setAudioMode ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "mode" # I */
/* .line 54 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setAudioMode() " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MediaFocusControlStubImpl"; // const-string v1, "MediaFocusControlStubImpl"
android.util.Log .d ( v1,v0 );
/* .line 55 */
/* iput p1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mAudioMode:I */
/* .line 56 */
return;
} // .end method
public void setBtAudioSuspended ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "btAudioSuspended" # Z */
/* .line 75 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setBtAudioSuspended() " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MediaFocusControlStubImpl"; // const-string v1, "MediaFocusControlStubImpl"
android.util.Log .d ( v1,v0 );
/* .line 76 */
/* iput-boolean p1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mBtAudioSuspended:Z */
/* .line 77 */
return;
} // .end method
public void setLastReleasedUid ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .line 88 */
/* iput p1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mLastReleasedUid:I */
/* .line 89 */
return;
} // .end method
public void setPendingAudioMode ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "mode" # I */
/* .line 60 */
/* iput p1, p0, Lcom/android/server/audio/MediaFocusControlStubImpl;->mPendingAudioMode:I */
/* .line 61 */
return;
} // .end method
