.class Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener;
.super Lcom/android/server/FixedFileObserver;
.source "AudioPowerSaveModeObserver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/AudioPowerSaveModeObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AudioPowerSaveStateListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/audio/AudioPowerSaveModeObserver;


# direct methods
.method public constructor <init>(Lcom/android/server/audio/AudioPowerSaveModeObserver;Ljava/lang/String;)V
    .locals 2
    .param p2, "path"    # Ljava/lang/String;

    .line 335
    iput-object p1, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener;->this$0:Lcom/android/server/audio/AudioPowerSaveModeObserver;

    .line 336
    invoke-direct {p0, p2}, Lcom/android/server/FixedFileObserver;-><init>(Ljava/lang/String;)V

    .line 337
    invoke-static {p1}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->-$$Nest$fgetmHandler(Lcom/android/server/audio/AudioPowerSaveModeObserver;)Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;

    move-result-object v0

    invoke-static {p1}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->-$$Nest$fgetmHandler(Lcom/android/server/audio/AudioPowerSaveModeObserver;)Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;

    move-result-object p1

    const/16 v1, 0x9c8

    invoke-virtual {p1, v1}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->sendMessage(Landroid/os/Message;)Z

    .line 338
    return-void
.end method


# virtual methods
.method public onEvent(ILjava/lang/String;)V
    .locals 3
    .param p1, "event"    # I
    .param p2, "path"    # Ljava/lang/String;

    .line 341
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 343
    :pswitch_0
    const-string v0, "AudioPowerSaveModeObserver"

    const-string v1, "MODIFY event"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    iget-object v0, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener;->this$0:Lcom/android/server/audio/AudioPowerSaveModeObserver;

    invoke-static {v0}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->-$$Nest$fgetmHandler(Lcom/android/server/audio/AudioPowerSaveModeObserver;)Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver$AudioPowerSaveStateListener;->this$0:Lcom/android/server/audio/AudioPowerSaveModeObserver;

    invoke-static {v1}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->-$$Nest$fgetmHandler(Lcom/android/server/audio/AudioPowerSaveModeObserver;)Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;

    move-result-object v1

    const/16 v2, 0x9c8

    invoke-virtual {v1, v2}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->sendMessage(Landroid/os/Message;)Z

    .line 345
    nop

    .line 350
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
