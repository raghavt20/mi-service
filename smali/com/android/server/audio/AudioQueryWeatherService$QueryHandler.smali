.class final Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;
.super Landroid/content/AsyncQueryHandler;
.source "AudioQueryWeatherService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/AudioQueryWeatherService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "QueryHandler"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler$CatchingWorkerHandler;
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/audio/AudioQueryWeatherService;


# direct methods
.method public constructor <init>(Lcom/android/server/audio/AudioQueryWeatherService;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .line 349
    iput-object p1, p0, Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;->this$0:Lcom/android/server/audio/AudioQueryWeatherService;

    .line 350
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    invoke-direct {p0, p1}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 351
    return-void
.end method


# virtual methods
.method protected createHandler(Landroid/os/Looper;)Landroid/os/Handler;
    .locals 1
    .param p1, "looper"    # Landroid/os/Looper;

    .line 346
    new-instance v0, Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler$CatchingWorkerHandler;

    invoke-direct {v0, p0, p1}, Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler$CatchingWorkerHandler;-><init>(Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;Landroid/os/Looper;)V

    return-object v0
.end method

.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 1
    .param p1, "token"    # I
    .param p2, "cookie"    # Ljava/lang/Object;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .line 355
    const/16 v0, 0x3e9

    if-ne p1, v0, :cond_0

    .line 356
    iget-object v0, p0, Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;->this$0:Lcom/android/server/audio/AudioQueryWeatherService;

    invoke-static {v0, p3}, Lcom/android/server/audio/AudioQueryWeatherService;->-$$Nest$mupdateCityInfo(Lcom/android/server/audio/AudioQueryWeatherService;Landroid/database/Cursor;)V

    goto :goto_0

    .line 358
    :cond_0
    iget-object v0, p0, Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;->this$0:Lcom/android/server/audio/AudioQueryWeatherService;

    invoke-static {v0, p3}, Lcom/android/server/audio/AudioQueryWeatherService;->-$$Nest$mupdateWeatherInfo(Lcom/android/server/audio/AudioQueryWeatherService;Landroid/database/Cursor;)V

    .line 360
    :goto_0
    return-void
.end method
