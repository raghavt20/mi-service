public class com.android.server.audio.AudioGameEffect {
	 /* .source "AudioGameEffect.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/audio/AudioGameEffect$GameEffectThread;, */
	 /* Lcom/android/server/audio/AudioGameEffect$GameEffectHandler; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String CURRENT_PLATFORM;
private static final java.lang.String CURRENT_REGION;
private static final Integer HEADSET_PLUG_IN;
private static final Integer HEADSET_PLUG_OUT;
private static final java.lang.String IS_CEREGION;
private static final java.lang.String IS_SUPPORT_SPEAKER;
private static final Integer MSG_SET_PROCESS_LISTENER;
private static final Integer MSG_START_GAME_EFFECT;
private static final Integer MSG_STOP_GAME_EFFECT;
private static final java.lang.String PM_SERVICE_NAME;
private static final Integer QUERY_PM_SERVICE_DELAY_MS;
private static final Integer QUERY_PM_SERVICE_MAX_TIMES;
private static final Integer SEND_PARAMETER_DELAY_MS;
private static final SUPPORTED_DEVICES;
private static final java.lang.String TAG;
/* # instance fields */
private final java.lang.String PARAMETER_OFF;
private final java.lang.String PREFIX_PARAMETER_ON;
private final java.lang.String SETTING_PKG_NAME;
private volatile Boolean isEffectOn;
private final android.content.Context mContext;
private java.lang.String mCurrentDevice;
private java.lang.String mCurrentEnablePkg;
private java.lang.String mCurrentPlatform;
private java.util.Map mDeviceEffects;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private java.util.Set mEnablePackages;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final miui.process.IForegroundInfoListener$Stub mForegroundInfoListener;
private java.util.Set mFpsPackages;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.Map mGameEffects;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.audio.AudioGameEffect$GameEffectHandler mHandler;
private final android.content.BroadcastReceiver mHeadSetReceiver;
private java.util.ArrayList mHeadsetEffects;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mIsSupportSpeaker;
private Integer mQueryPMServiceTime;
private java.util.ArrayList mSpeakerEffects;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.lang.Thread mThread;
/* # direct methods */
static Boolean -$$Nest$fgetisEffectOn ( com.android.server.audio.AudioGameEffect p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/audio/AudioGameEffect;->isEffectOn:Z */
} // .end method
static java.lang.String -$$Nest$fgetmCurrentEnablePkg ( com.android.server.audio.AudioGameEffect p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCurrentEnablePkg;
} // .end method
static miui.process.IForegroundInfoListener$Stub -$$Nest$fgetmForegroundInfoListener ( com.android.server.audio.AudioGameEffect p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mForegroundInfoListener;
} // .end method
static Integer -$$Nest$fgetmQueryPMServiceTime ( com.android.server.audio.AudioGameEffect p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/audio/AudioGameEffect;->mQueryPMServiceTime:I */
} // .end method
static void -$$Nest$fputmCurrentDevice ( com.android.server.audio.AudioGameEffect p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mCurrentDevice = p1;
return;
} // .end method
static void -$$Nest$fputmCurrentEnablePkg ( com.android.server.audio.AudioGameEffect p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mCurrentEnablePkg = p1;
return;
} // .end method
static void -$$Nest$fputmHandler ( com.android.server.audio.AudioGameEffect p0, com.android.server.audio.AudioGameEffect$GameEffectHandler p1 ) { //bridge//synthethic
/* .locals 0 */
this.mHandler = p1;
return;
} // .end method
static void -$$Nest$fputmQueryPMServiceTime ( com.android.server.audio.AudioGameEffect p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/audio/AudioGameEffect;->mQueryPMServiceTime:I */
return;
} // .end method
static void -$$Nest$minitProcessListenerAsync ( com.android.server.audio.AudioGameEffect p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/audio/AudioGameEffect;->initProcessListenerAsync()V */
return;
} // .end method
static Boolean -$$Nest$misCurrentDeviceSupported ( com.android.server.audio.AudioGameEffect p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/audio/AudioGameEffect;->isCurrentDeviceSupported()Z */
} // .end method
static Boolean -$$Nest$misCurrentSceneSupported ( com.android.server.audio.AudioGameEffect p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/audio/AudioGameEffect;->isCurrentSceneSupported()Z */
} // .end method
static Boolean -$$Nest$misPackageEnabled ( com.android.server.audio.AudioGameEffect p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/audio/AudioGameEffect;->isPackageEnabled(Ljava/lang/String;)Z */
} // .end method
static Boolean -$$Nest$misSpatialAudioEnabled ( com.android.server.audio.AudioGameEffect p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/audio/AudioGameEffect;->isSpatialAudioEnabled()Z */
} // .end method
static void -$$Nest$msendMsgDelay ( com.android.server.audio.AudioGameEffect p0, Integer p1, Long p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/audio/AudioGameEffect;->sendMsgDelay(IJ)V */
return;
} // .end method
static void -$$Nest$mstartGameEffect ( com.android.server.audio.AudioGameEffect p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/audio/AudioGameEffect;->startGameEffect()V */
return;
} // .end method
static void -$$Nest$mstopGameEffect ( com.android.server.audio.AudioGameEffect p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/audio/AudioGameEffect;->stopGameEffect()V */
return;
} // .end method
static com.android.server.audio.AudioGameEffect ( ) {
/* .locals 1 */
/* .line 72 */
int v0 = 6; // const/4 v0, 0x6
/* new-array v0, v0, [I */
/* fill-array-data v0, :array_0 */
return;
/* nop */
/* :array_0 */
/* .array-data 4 */
/* 0x8 */
/* 0x7 */
/* 0x1a */
/* 0x3 */
/* 0x4 */
/* 0x16 */
} // .end array-data
} // .end method
public com.android.server.audio.AudioGameEffect ( ) {
/* .locals 3 */
/* .param p1, "cxt" # Landroid/content/Context; */
/* .line 119 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 50 */
final String v0 = "game_effect_packages"; // const-string v0, "game_effect_packages"
this.SETTING_PKG_NAME = v0;
/* .line 51 */
final String v0 = "misound_fps_effect=T-"; // const-string v0, "misound_fps_effect=T-"
this.PREFIX_PARAMETER_ON = v0;
/* .line 52 */
final String v0 = "misound_fps_effect=F-0-0-0-0"; // const-string v0, "misound_fps_effect=F-0-0-0-0"
this.PARAMETER_OFF = v0;
/* .line 64 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/audio/AudioGameEffect;->mQueryPMServiceTime:I */
/* .line 76 */
final String v1 = "Build.BRAND"; // const-string v1, "Build.BRAND"
final String v2 = ""; // const-string v2, ""
android.os.SystemProperties .get ( v1,v2 );
this.mCurrentPlatform = v1;
/* .line 77 */
final String v1 = "persist.vendor.audio.fpsop.game.effect.speaker"; // const-string v1, "persist.vendor.audio.fpsop.game.effect.speaker"
v1 = android.os.SystemProperties .getBoolean ( v1,v0 );
/* iput-boolean v1, p0, Lcom/android/server/audio/AudioGameEffect;->mIsSupportSpeaker:Z */
/* .line 78 */
this.mCurrentEnablePkg = v2;
/* .line 79 */
this.mCurrentDevice = v2;
/* .line 80 */
/* iput-boolean v0, p0, Lcom/android/server/audio/AudioGameEffect;->isEffectOn:Z */
/* .line 94 */
/* new-instance v0, Lcom/android/server/audio/AudioGameEffect$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/audio/AudioGameEffect$1;-><init>(Lcom/android/server/audio/AudioGameEffect;)V */
this.mForegroundInfoListener = v0;
/* .line 262 */
/* new-instance v0, Lcom/android/server/audio/AudioGameEffect$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/audio/AudioGameEffect$2;-><init>(Lcom/android/server/audio/AudioGameEffect;)V */
this.mHeadSetReceiver = v0;
/* .line 120 */
this.mContext = p1;
/* .line 121 */
/* invoke-direct {p0}, Lcom/android/server/audio/AudioGameEffect;->initLocalPackagesAndEffects()V */
/* .line 122 */
/* new-instance v1, Lcom/android/server/audio/AudioGameEffect$GameEffectThread; */
/* invoke-direct {v1, p0}, Lcom/android/server/audio/AudioGameEffect$GameEffectThread;-><init>(Lcom/android/server/audio/AudioGameEffect;)V */
this.mThread = v1;
/* .line 123 */
(( java.lang.Thread ) v1 ).start ( ); // invoke-virtual {v1}, Ljava/lang/Thread;->start()V
/* .line 125 */
/* new-instance v1, Landroid/content/IntentFilter; */
/* invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V */
/* .line 126 */
/* .local v1, "filter":Landroid/content/IntentFilter; */
final String v2 = "android.intent.action.HEADSET_PLUG"; // const-string v2, "android.intent.action.HEADSET_PLUG"
(( android.content.IntentFilter ) v1 ).addAction ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 127 */
final String v2 = "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"; // const-string v2, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"
(( android.content.IntentFilter ) v1 ).addAction ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 128 */
final String v2 = "android.bluetooth.adapter.action.STATE_CHANGED"; // const-string v2, "android.bluetooth.adapter.action.STATE_CHANGED"
(( android.content.IntentFilter ) v1 ).addAction ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 129 */
(( android.content.Context ) p1 ).registerReceiver ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 130 */
return;
} // .end method
private void getEnablePackagesFromCloudSettings ( ) {
/* .locals 6 */
/* .line 134 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "game_effect_packages"; // const-string v1, "game_effect_packages"
android.provider.Settings$Global .getString ( v0,v1 );
/* .line 135 */
/* .local v0, "pkgs":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "get enable packages from setting: "; // const-string v2, "get enable packages from setting: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "AudioGameEffect"; // const-string v2, "AudioGameEffect"
android.util.Log .i ( v2,v1 );
/* .line 136 */
if ( v0 != null) { // if-eqz v0, :cond_1
final String v1 = ""; // const-string v1, ""
v2 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_1 */
/* .line 137 */
v2 = this.mEnablePackages;
/* .line 138 */
final String v2 = ","; // const-string v2, ","
(( java.lang.String ) v0 ).split ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 139 */
/* .local v2, "packages":[Ljava/lang/String; */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
/* array-length v4, v2 */
/* if-ge v3, v4, :cond_1 */
/* .line 140 */
/* aget-object v4, v2, v3 */
v4 = (( java.lang.String ) v1 ).equals ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v4, :cond_0 */
/* .line 141 */
v4 = this.mEnablePackages;
/* aget-object v5, v2, v3 */
/* .line 139 */
} // :cond_0
/* add-int/lit8 v3, v3, 0x1 */
/* .line 145 */
} // .end local v2 # "packages":[Ljava/lang/String;
} // .end local v3 # "i":I
} // :cond_1
return;
} // .end method
private void getEnableSpeakerFromCloudSettings ( ) {
/* .locals 3 */
/* .line 148 */
final String v0 = "persist.vendor.audio.fpsop.game.effect.speaker"; // const-string v0, "persist.vendor.audio.fpsop.game.effect.speaker"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* .line 149 */
/* .local v0, "speaker":Z */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "get speaker from global setting: "; // const-string v2, "get speaker from global setting: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "AudioGameEffect"; // const-string v2, "AudioGameEffect"
android.util.Log .i ( v2,v1 );
/* .line 150 */
/* iput-boolean v0, p0, Lcom/android/server/audio/AudioGameEffect;->mIsSupportSpeaker:Z */
/* .line 151 */
return;
} // .end method
private void initLocalPackagesAndEffects ( ) {
/* .locals 3 */
/* .line 396 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
this.mEnablePackages = v0;
/* .line 397 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
this.mFpsPackages = v0;
/* .line 398 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mGameEffects = v0;
/* .line 399 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mDeviceEffects = v0;
/* .line 400 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mHeadsetEffects = v0;
/* .line 401 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mSpeakerEffects = v0;
/* .line 403 */
/* invoke-direct {p0}, Lcom/android/server/audio/AudioGameEffect;->parseAudioGameEffectXml()V */
/* .line 405 */
v0 = this.mDeviceEffects;
final String v1 = "headsets"; // const-string v1, "headsets"
v2 = this.mHeadsetEffects;
/* .line 406 */
v0 = this.mDeviceEffects;
/* const-string/jumbo v1, "speaker" */
v2 = this.mSpeakerEffects;
/* .line 407 */
return;
} // .end method
private void initProcessListenerAsync ( ) {
/* .locals 3 */
/* .line 213 */
int v0 = 1; // const/4 v0, 0x1
/* const-wide/16 v1, 0x3e8 */
/* invoke-direct {p0, v0, v1, v2}, Lcom/android/server/audio/AudioGameEffect;->sendMsgDelay(IJ)V */
/* .line 214 */
return;
} // .end method
private Boolean isCurrentDeviceSupported ( ) {
/* .locals 9 */
/* .line 217 */
v0 = this.mContext;
final String v1 = "audio"; // const-string v1, "audio"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/media/AudioManager; */
/* .line 218 */
/* .local v0, "am":Landroid/media/AudioManager; */
int v1 = 2; // const/4 v1, 0x2
(( android.media.AudioManager ) v0 ).getDevices ( v1 ); // invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getDevices(I)[Landroid/media/AudioDeviceInfo;
/* .line 219 */
/* .local v1, "infos":[Landroid/media/AudioDeviceInfo; */
/* array-length v2, v1 */
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
int v4 = 1; // const/4 v4, 0x1
/* if-ge v3, v2, :cond_2 */
/* aget-object v5, v1, v3 */
/* .line 220 */
/* .local v5, "info":Landroid/media/AudioDeviceInfo; */
int v6 = 0; // const/4 v6, 0x0
/* .local v6, "i":I */
} // :goto_1
v7 = com.android.server.audio.AudioGameEffect.SUPPORTED_DEVICES;
/* array-length v8, v7 */
/* if-ge v6, v8, :cond_1 */
/* .line 221 */
/* aget v7, v7, v6 */
v8 = (( android.media.AudioDeviceInfo ) v5 ).getType ( ); // invoke-virtual {v5}, Landroid/media/AudioDeviceInfo;->getType()I
/* if-ne v7, v8, :cond_0 */
/* .line 222 */
final String v2 = "headsets"; // const-string v2, "headsets"
this.mCurrentDevice = v2;
/* .line 223 */
/* .line 220 */
} // :cond_0
/* add-int/lit8 v6, v6, 0x1 */
/* .line 219 */
} // .end local v5 # "info":Landroid/media/AudioDeviceInfo;
} // .end local v6 # "i":I
} // :cond_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 227 */
} // :cond_2
/* const-string/jumbo v2, "speaker" */
this.mCurrentDevice = v2;
/* .line 228 */
} // .end method
private Boolean isCurrentSceneSupported ( ) {
/* .locals 6 */
/* .line 232 */
/* const-string/jumbo v0, "sys.audio.ceregion" */
final String v1 = "false"; // const-string v1, "false"
android.os.SystemProperties .get ( v0,v1 );
/* .line 233 */
/* .local v0, "currentIsCERegion":Ljava/lang/String; */
final String v2 = "ro.miui.build.region"; // const-string v2, "ro.miui.build.region"
final String v3 = ""; // const-string v3, ""
android.os.SystemProperties .get ( v2,v3 );
/* .line 234 */
/* .local v2, "currentRegion":Ljava/lang/String; */
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v3 = 0; // const/4 v3, 0x0
final String v4 = "AudioGameEffect"; // const-string v4, "AudioGameEffect"
if ( v1 != null) { // if-eqz v1, :cond_0
final String v1 = "cn"; // const-string v1, "cn"
v1 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_0 */
/* .line 235 */
final String v1 = "Non ceRegion, return"; // const-string v1, "Non ceRegion, return"
android.util.Log .i ( v4,v1 );
/* .line 236 */
/* .line 239 */
} // :cond_0
v1 = /* invoke-direct {p0}, Lcom/android/server/audio/AudioGameEffect;->isSpatialAudioEnabled()Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 240 */
/* const-string/jumbo v1, "spatial audio enabled, return" */
android.util.Log .i ( v4,v1 );
/* .line 241 */
/* .line 244 */
} // :cond_1
/* invoke-direct {p0}, Lcom/android/server/audio/AudioGameEffect;->getEnableSpeakerFromCloudSettings()V */
/* .line 245 */
v1 = this.mCurrentDevice;
/* const-string/jumbo v5, "speaker" */
/* if-ne v1, v5, :cond_2 */
/* iget-boolean v1, p0, Lcom/android/server/audio/AudioGameEffect;->mIsSupportSpeaker:Z */
/* if-nez v1, :cond_2 */
/* .line 246 */
final String v1 = "AudioGameEffect does not support speaker"; // const-string v1, "AudioGameEffect does not support speaker"
android.util.Log .i ( v4,v1 );
/* .line 247 */
/* .line 250 */
} // :cond_2
v1 = this.mCurrentEnablePkg;
v1 = /* invoke-direct {p0, v1}, Lcom/android/server/audio/AudioGameEffect;->isFpsPackages(Ljava/lang/String;)Z */
/* if-nez v1, :cond_3 */
v1 = this.mCurrentDevice;
/* if-ne v1, v5, :cond_3 */
/* .line 251 */
final String v1 = "not FPS speaker no need to startGameEffect"; // const-string v1, "not FPS speaker no need to startGameEffect"
android.util.Log .i ( v4,v1 );
/* .line 252 */
/* .line 255 */
} // :cond_3
int v1 = 1; // const/4 v1, 0x1
} // .end method
private Boolean isFpsPackages ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 159 */
v0 = v0 = this.mFpsPackages;
} // .end method
private Boolean isPackageEnabled ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 154 */
/* invoke-direct {p0}, Lcom/android/server/audio/AudioGameEffect;->getEnablePackagesFromCloudSettings()V */
/* .line 155 */
v0 = v0 = this.mEnablePackages;
} // .end method
private Boolean isSpatialAudioEnabled ( ) {
/* .locals 3 */
/* .line 259 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "spatial_audio_feature_enable" */
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$Global .getInt ( v0,v1,v2 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v2, v1 */
} // :cond_0
} // .end method
private void parseAudioGameEffectXml ( ) {
/* .locals 19 */
/* .line 320 */
/* move-object/from16 v1, p0 */
final String v2 = "inputStream close fail: "; // const-string v2, "inputStream close fail: "
final String v0 = "parse parseAudioGameEffectXml: "; // const-string v0, "parse parseAudioGameEffectXml: "
final String v3 = "AudioGameEffect"; // const-string v3, "AudioGameEffect"
android.util.Log .d ( v3,v0 );
/* .line 321 */
int v4 = 0; // const/4 v4, 0x0
/* .line 322 */
/* .local v4, "items_xml":Ljava/io/File; */
final String v0 = ""; // const-string v0, ""
/* .line 323 */
/* .local v0, "items_xml_path":Ljava/lang/String; */
int v5 = 0; // const/4 v5, 0x0
/* .line 324 */
/* .local v5, "inputStream":Ljava/io/InputStream; */
int v6 = 0; // const/4 v6, 0x0
/* .line 326 */
/* .local v6, "document":Lorg/w3c/dom/Document; */
v7 = this.mCurrentPlatform;
final String v8 = "MTK"; // const-string v8, "MTK"
v7 = (( java.lang.String ) v7 ).equals ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v7 != null) { // if-eqz v7, :cond_0
/* .line 327 */
/* const-string/jumbo v0, "vendor/etc/audio_game_effect_items.xml" */
/* move-object v7, v0 */
/* .line 329 */
} // :cond_0
final String v0 = "odm/etc/audio_game_effect_items.xml"; // const-string v0, "odm/etc/audio_game_effect_items.xml"
/* move-object v7, v0 */
/* .line 333 */
} // .end local v0 # "items_xml_path":Ljava/lang/String;
/* .local v7, "items_xml_path":Ljava/lang/String; */
} // :goto_0
try { // :try_start_0
/* new-instance v0, Ljava/io/File; */
/* invoke-direct {v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_6 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_4 */
/* move-object v4, v0 */
/* .line 334 */
try { // :try_start_1
v0 = (( java.io.File ) v4 ).exists ( ); // invoke-virtual {v4}, Ljava/io/File;->exists()Z
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_5 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_3 */
/* if-nez v0, :cond_2 */
/* .line 335 */
try { // :try_start_2
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "AudioGameEffect xml doesn\'t exist: "; // const-string v8, "AudioGameEffect xml doesn\'t exist: "
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .w ( v3,v0 );
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_1 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 385 */
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 386 */
try { // :try_start_3
(( java.io.InputStream ) v5 ).close ( ); // invoke-virtual {v5}, Ljava/io/InputStream;->close()V
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 388 */
/* :catch_0 */
/* move-exception v0 */
/* move-object v8, v0 */
/* move-object v0, v8 */
/* .line 389 */
/* .local v0, "e":Ljava/lang/Exception; */
android.util.Log .w ( v3,v2,v0 );
/* .line 390 */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 391 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_1
} // :goto_1
/* nop */
/* .line 336 */
} // :goto_2
return;
/* .line 384 */
/* :catchall_0 */
/* move-exception v0 */
/* move-object/from16 v18, v7 */
/* move-object v7, v0 */
/* goto/16 :goto_c */
/* .line 380 */
/* :catch_1 */
/* move-exception v0 */
/* move-object/from16 v18, v7 */
/* goto/16 :goto_9 */
/* .line 338 */
} // :cond_2
try { // :try_start_4
/* new-instance v0, Ljava/io/FileInputStream; */
/* invoke-direct {v0, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* move-object v5, v0 */
/* .line 339 */
javax.xml.parsers.DocumentBuilderFactory .newInstance ( );
/* .line 340 */
/* .local v0, "factory":Ljavax/xml/parsers/DocumentBuilderFactory; */
(( javax.xml.parsers.DocumentBuilderFactory ) v0 ).newDocumentBuilder ( ); // invoke-virtual {v0}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;
/* .line 341 */
/* .local v8, "builder":Ljavax/xml/parsers/DocumentBuilder; */
(( javax.xml.parsers.DocumentBuilder ) v8 ).parse ( v5 ); // invoke-virtual {v8, v5}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;
/* move-object v6, v9 */
/* .line 344 */
final String v9 = "package"; // const-string v9, "package"
/* .line 345 */
/* .local v9, "packageList":Lorg/w3c/dom/NodeList; */
int v10 = 0; // const/4 v10, 0x0
/* .local v10, "packageIdx":I */
v11 = } // :goto_3
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_5 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_3 */
final String v12 = "name"; // const-string v12, "name"
int v13 = 1; // const/4 v13, 0x1
/* if-ge v10, v11, :cond_8 */
/* .line 346 */
try { // :try_start_5
/* .line 349 */
v14 = /* .local v11, "packageNode":Lorg/w3c/dom/Node; */
/* if-ne v14, v13, :cond_7 */
/* .line 350 */
/* .line 351 */
/* .local v14, "devicesList":Lorg/w3c/dom/NodeList; */
v15 = this.mEnablePackages;
/* move-object v13, v11 */
/* check-cast v13, Lorg/w3c/dom/Element; */
/* .line 352 */
v13 = this.mGameEffects;
/* move-object v15, v11 */
/* check-cast v15, Lorg/w3c/dom/Element; */
/* move-object/from16 v16, v0 */
} // .end local v0 # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
/* .local v16, "factory":Ljavax/xml/parsers/DocumentBuilderFactory; */
java.lang.Integer .toString ( v10 );
/* .line 355 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "deviceIdx":I */
v13 = } // :goto_4
/* if-ge v0, v13, :cond_6 */
/* .line 356 */
/* .line 359 */
v15 = /* .local v13, "deviceNode":Lorg/w3c/dom/Node; */
/* :try_end_5 */
/* .catch Ljava/lang/Exception; {:try_start_5 ..:try_end_5} :catch_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_3 */
/* move-object/from16 v17, v4 */
int v4 = 1; // const/4 v4, 0x1
} // .end local v4 # "items_xml":Ljava/io/File;
/* .local v17, "items_xml":Ljava/io/File; */
/* if-ne v15, v4, :cond_4 */
/* .line 360 */
try { // :try_start_6
/* move-object v4, v13 */
/* check-cast v4, Lorg/w3c/dom/Element; */
/* const-string/jumbo v15, "speaker" */
v4 = (( java.lang.String ) v4 ).equals ( v15 ); // invoke-virtual {v4, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_6 */
/* .catch Ljava/lang/Exception; {:try_start_6 ..:try_end_6} :catch_2 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_1 */
final String v15 = "param"; // const-string v15, "param"
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 361 */
try { // :try_start_7
v4 = this.mSpeakerEffects;
/* :try_end_7 */
/* .catch Ljava/lang/Exception; {:try_start_7 ..:try_end_7} :catch_2 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_1 */
/* move-object/from16 v18, v7 */
} // .end local v7 # "items_xml_path":Ljava/lang/String;
/* .local v18, "items_xml_path":Ljava/lang/String; */
try { // :try_start_8
/* move-object v7, v13 */
/* check-cast v7, Lorg/w3c/dom/Element; */
(( java.util.ArrayList ) v4 ).add ( v7 ); // invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 362 */
} // .end local v18 # "items_xml_path":Ljava/lang/String;
/* .restart local v7 # "items_xml_path":Ljava/lang/String; */
} // :cond_3
/* move-object/from16 v18, v7 */
} // .end local v7 # "items_xml_path":Ljava/lang/String;
/* .restart local v18 # "items_xml_path":Ljava/lang/String; */
/* move-object v4, v13 */
/* check-cast v4, Lorg/w3c/dom/Element; */
final String v7 = "headset"; // const-string v7, "headset"
v4 = (( java.lang.String ) v4 ).equals ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 363 */
v4 = this.mHeadsetEffects;
/* move-object v7, v13 */
/* check-cast v7, Lorg/w3c/dom/Element; */
(( java.util.ArrayList ) v4 ).add ( v7 ); // invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 384 */
} // .end local v0 # "deviceIdx":I
} // .end local v8 # "builder":Ljavax/xml/parsers/DocumentBuilder;
} // .end local v9 # "packageList":Lorg/w3c/dom/NodeList;
} // .end local v10 # "packageIdx":I
} // .end local v11 # "packageNode":Lorg/w3c/dom/Node;
} // .end local v13 # "deviceNode":Lorg/w3c/dom/Node;
} // .end local v14 # "devicesList":Lorg/w3c/dom/NodeList;
} // .end local v16 # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
} // .end local v18 # "items_xml_path":Ljava/lang/String;
/* .restart local v7 # "items_xml_path":Ljava/lang/String; */
/* :catchall_1 */
/* move-exception v0 */
/* move-object/from16 v18, v7 */
/* move-object v7, v0 */
/* move-object/from16 v4, v17 */
} // .end local v7 # "items_xml_path":Ljava/lang/String;
/* .restart local v18 # "items_xml_path":Ljava/lang/String; */
/* goto/16 :goto_c */
/* .line 380 */
} // .end local v18 # "items_xml_path":Ljava/lang/String;
/* .restart local v7 # "items_xml_path":Ljava/lang/String; */
/* :catch_2 */
/* move-exception v0 */
/* move-object/from16 v18, v7 */
/* move-object/from16 v4, v17 */
} // .end local v7 # "items_xml_path":Ljava/lang/String;
/* .restart local v18 # "items_xml_path":Ljava/lang/String; */
/* goto/16 :goto_9 */
/* .line 359 */
} // .end local v18 # "items_xml_path":Ljava/lang/String;
/* .restart local v0 # "deviceIdx":I */
/* .restart local v7 # "items_xml_path":Ljava/lang/String; */
/* .restart local v8 # "builder":Ljavax/xml/parsers/DocumentBuilder; */
/* .restart local v9 # "packageList":Lorg/w3c/dom/NodeList; */
/* .restart local v10 # "packageIdx":I */
/* .restart local v11 # "packageNode":Lorg/w3c/dom/Node; */
/* .restart local v13 # "deviceNode":Lorg/w3c/dom/Node; */
/* .restart local v14 # "devicesList":Lorg/w3c/dom/NodeList; */
/* .restart local v16 # "factory":Ljavax/xml/parsers/DocumentBuilderFactory; */
} // :cond_4
/* move-object/from16 v18, v7 */
/* .line 355 */
} // .end local v7 # "items_xml_path":Ljava/lang/String;
} // .end local v13 # "deviceNode":Lorg/w3c/dom/Node;
/* .restart local v18 # "items_xml_path":Ljava/lang/String; */
} // :cond_5
} // :goto_5
/* add-int/lit8 v0, v0, 0x1 */
/* move-object/from16 v4, v17 */
/* move-object/from16 v7, v18 */
} // .end local v17 # "items_xml":Ljava/io/File;
} // .end local v18 # "items_xml_path":Ljava/lang/String;
/* .restart local v4 # "items_xml":Ljava/io/File; */
/* .restart local v7 # "items_xml_path":Ljava/lang/String; */
} // :cond_6
/* move-object/from16 v17, v4 */
/* move-object/from16 v18, v7 */
} // .end local v4 # "items_xml":Ljava/io/File;
} // .end local v7 # "items_xml_path":Ljava/lang/String;
/* .restart local v17 # "items_xml":Ljava/io/File; */
/* .restart local v18 # "items_xml_path":Ljava/lang/String; */
/* .line 349 */
} // .end local v14 # "devicesList":Lorg/w3c/dom/NodeList;
} // .end local v16 # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
} // .end local v17 # "items_xml":Ljava/io/File;
} // .end local v18 # "items_xml_path":Ljava/lang/String;
/* .local v0, "factory":Ljavax/xml/parsers/DocumentBuilderFactory; */
/* .restart local v4 # "items_xml":Ljava/io/File; */
/* .restart local v7 # "items_xml_path":Ljava/lang/String; */
} // :cond_7
/* move-object/from16 v16, v0 */
/* move-object/from16 v17, v4 */
/* move-object/from16 v18, v7 */
/* .line 345 */
} // .end local v0 # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
} // .end local v4 # "items_xml":Ljava/io/File;
} // .end local v7 # "items_xml_path":Ljava/lang/String;
} // .end local v11 # "packageNode":Lorg/w3c/dom/Node;
/* .restart local v16 # "factory":Ljavax/xml/parsers/DocumentBuilderFactory; */
/* .restart local v17 # "items_xml":Ljava/io/File; */
/* .restart local v18 # "items_xml_path":Ljava/lang/String; */
} // :goto_6
/* add-int/lit8 v10, v10, 0x1 */
/* move-object/from16 v0, v16 */
/* move-object/from16 v4, v17 */
/* move-object/from16 v7, v18 */
/* goto/16 :goto_3 */
} // .end local v16 # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
} // .end local v17 # "items_xml":Ljava/io/File;
} // .end local v18 # "items_xml_path":Ljava/lang/String;
/* .restart local v0 # "factory":Ljavax/xml/parsers/DocumentBuilderFactory; */
/* .restart local v4 # "items_xml":Ljava/io/File; */
/* .restart local v7 # "items_xml_path":Ljava/lang/String; */
} // :cond_8
/* move-object/from16 v16, v0 */
/* move-object/from16 v17, v4 */
/* move-object/from16 v18, v7 */
/* .line 371 */
} // .end local v0 # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
} // .end local v4 # "items_xml":Ljava/io/File;
} // .end local v7 # "items_xml_path":Ljava/lang/String;
} // .end local v10 # "packageIdx":I
/* .restart local v16 # "factory":Ljavax/xml/parsers/DocumentBuilderFactory; */
/* .restart local v17 # "items_xml":Ljava/io/File; */
/* .restart local v18 # "items_xml_path":Ljava/lang/String; */
final String v0 = "FPSpackage"; // const-string v0, "FPSpackage"
/* .line 372 */
/* .local v0, "FPSpackageList":Lorg/w3c/dom/NodeList; */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "FPSpackageIdx":I */
v7 = } // :goto_7
/* if-ge v4, v7, :cond_a */
/* .line 373 */
/* .line 376 */
v10 = /* .local v7, "FPSpackageNode":Lorg/w3c/dom/Node; */
int v11 = 1; // const/4 v11, 0x1
/* if-ne v10, v11, :cond_9 */
/* .line 377 */
v10 = this.mFpsPackages;
/* move-object v13, v7 */
/* check-cast v13, Lorg/w3c/dom/Element; */
/* :try_end_8 */
/* .catch Ljava/lang/Exception; {:try_start_8 ..:try_end_8} :catch_4 */
/* .catchall {:try_start_8 ..:try_end_8} :catchall_2 */
/* .line 372 */
} // .end local v7 # "FPSpackageNode":Lorg/w3c/dom/Node;
} // :cond_9
/* add-int/lit8 v4, v4, 0x1 */
/* .line 385 */
} // .end local v0 # "FPSpackageList":Lorg/w3c/dom/NodeList;
} // .end local v4 # "FPSpackageIdx":I
} // .end local v8 # "builder":Ljavax/xml/parsers/DocumentBuilder;
} // .end local v9 # "packageList":Lorg/w3c/dom/NodeList;
} // .end local v16 # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
} // :cond_a
/* nop */
/* .line 386 */
try { // :try_start_9
(( java.io.InputStream ) v5 ).close ( ); // invoke-virtual {v5}, Ljava/io/InputStream;->close()V
/* :try_end_9 */
/* .catch Ljava/lang/Exception; {:try_start_9 ..:try_end_9} :catch_3 */
/* .line 391 */
/* .line 388 */
/* :catch_3 */
/* move-exception v0 */
/* move-object v4, v0 */
/* move-object v0, v4 */
/* .line 389 */
/* .local v0, "e":Ljava/lang/Exception; */
android.util.Log .w ( v3,v2,v0 );
/* .line 390 */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 392 */
} // .end local v0 # "e":Ljava/lang/Exception;
/* nop */
/* .line 393 */
} // :goto_8
/* move-object/from16 v4, v17 */
/* .line 384 */
/* :catchall_2 */
/* move-exception v0 */
/* move-object v7, v0 */
/* move-object/from16 v4, v17 */
/* .line 380 */
/* :catch_4 */
/* move-exception v0 */
/* move-object/from16 v4, v17 */
/* .line 384 */
} // .end local v17 # "items_xml":Ljava/io/File;
} // .end local v18 # "items_xml_path":Ljava/lang/String;
/* .local v4, "items_xml":Ljava/io/File; */
/* .local v7, "items_xml_path":Ljava/lang/String; */
/* :catchall_3 */
/* move-exception v0 */
/* move-object/from16 v17, v4 */
/* move-object/from16 v18, v7 */
/* move-object v7, v0 */
} // .end local v4 # "items_xml":Ljava/io/File;
} // .end local v7 # "items_xml_path":Ljava/lang/String;
/* .restart local v17 # "items_xml":Ljava/io/File; */
/* .restart local v18 # "items_xml_path":Ljava/lang/String; */
/* .line 380 */
} // .end local v17 # "items_xml":Ljava/io/File;
} // .end local v18 # "items_xml_path":Ljava/lang/String;
/* .restart local v4 # "items_xml":Ljava/io/File; */
/* .restart local v7 # "items_xml_path":Ljava/lang/String; */
/* :catch_5 */
/* move-exception v0 */
/* move-object/from16 v17, v4 */
/* move-object/from16 v18, v7 */
} // .end local v4 # "items_xml":Ljava/io/File;
} // .end local v7 # "items_xml_path":Ljava/lang/String;
/* .restart local v17 # "items_xml":Ljava/io/File; */
/* .restart local v18 # "items_xml_path":Ljava/lang/String; */
/* .line 384 */
} // .end local v17 # "items_xml":Ljava/io/File;
} // .end local v18 # "items_xml_path":Ljava/lang/String;
/* .restart local v4 # "items_xml":Ljava/io/File; */
/* .restart local v7 # "items_xml_path":Ljava/lang/String; */
/* :catchall_4 */
/* move-exception v0 */
/* move-object/from16 v18, v7 */
/* move-object v7, v0 */
} // .end local v7 # "items_xml_path":Ljava/lang/String;
/* .restart local v18 # "items_xml_path":Ljava/lang/String; */
/* .line 380 */
} // .end local v18 # "items_xml_path":Ljava/lang/String;
/* .restart local v7 # "items_xml_path":Ljava/lang/String; */
/* :catch_6 */
/* move-exception v0 */
/* move-object/from16 v18, v7 */
/* .line 381 */
} // .end local v7 # "items_xml_path":Ljava/lang/String;
/* .restart local v0 # "e":Ljava/lang/Exception; */
/* .restart local v18 # "items_xml_path":Ljava/lang/String; */
} // :goto_9
try { // :try_start_a
final String v7 = "Exception: "; // const-string v7, "Exception: "
android.util.Log .w ( v3,v7,v0 );
/* .line 382 */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_a */
/* .catchall {:try_start_a ..:try_end_a} :catchall_5 */
/* .line 385 */
} // .end local v0 # "e":Ljava/lang/Exception;
if ( v5 != null) { // if-eqz v5, :cond_b
/* .line 386 */
try { // :try_start_b
(( java.io.InputStream ) v5 ).close ( ); // invoke-virtual {v5}, Ljava/io/InputStream;->close()V
/* :try_end_b */
/* .catch Ljava/lang/Exception; {:try_start_b ..:try_end_b} :catch_7 */
/* .line 388 */
/* :catch_7 */
/* move-exception v0 */
/* move-object v7, v0 */
/* move-object v0, v7 */
/* .line 389 */
/* .restart local v0 # "e":Ljava/lang/Exception; */
android.util.Log .w ( v3,v2,v0 );
/* .line 390 */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 392 */
} // .end local v0 # "e":Ljava/lang/Exception;
/* .line 391 */
} // :cond_b
} // :goto_a
/* nop */
/* .line 393 */
} // :goto_b
return;
/* .line 384 */
/* :catchall_5 */
/* move-exception v0 */
/* move-object v7, v0 */
/* .line 385 */
} // :goto_c
if ( v5 != null) { // if-eqz v5, :cond_c
/* .line 386 */
try { // :try_start_c
(( java.io.InputStream ) v5 ).close ( ); // invoke-virtual {v5}, Ljava/io/InputStream;->close()V
/* :try_end_c */
/* .catch Ljava/lang/Exception; {:try_start_c ..:try_end_c} :catch_8 */
/* .line 388 */
/* :catch_8 */
/* move-exception v0 */
/* move-object v8, v0 */
/* move-object v0, v8 */
/* .line 389 */
/* .restart local v0 # "e":Ljava/lang/Exception; */
android.util.Log .w ( v3,v2,v0 );
/* .line 390 */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 391 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_c
} // :goto_d
/* nop */
/* .line 392 */
} // :goto_e
/* throw v7 */
} // .end method
private void sendMsgDelay ( Integer p0, Long p1 ) {
/* .locals 2 */
/* .param p1, "mesWhat" # I */
/* .param p2, "delay" # J */
/* .line 410 */
v0 = this.mHandler;
(( com.android.server.audio.AudioGameEffect$GameEffectHandler ) v0 ).obtainMessage ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/audio/AudioGameEffect$GameEffectHandler;->obtainMessage(I)Landroid/os/Message;
/* .line 411 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.mHandler;
(( com.android.server.audio.AudioGameEffect$GameEffectHandler ) v1 ).sendMessageDelayed ( v0, p2, p3 ); // invoke-virtual {v1, v0, p2, p3}, Lcom/android/server/audio/AudioGameEffect$GameEffectHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 412 */
return;
} // .end method
private void startGameEffect ( ) {
/* .locals 5 */
/* .line 301 */
v0 = this.mGameEffects;
v1 = this.mCurrentEnablePkg;
/* check-cast v0, Ljava/lang/String; */
/* .line 303 */
/* .local v0, "paramIndex":Ljava/lang/String; */
final String v1 = "AudioGameEffect"; // const-string v1, "AudioGameEffect"
if ( v0 != null) { // if-eqz v0, :cond_0
v2 = /* invoke-direct {p0}, Lcom/android/server/audio/AudioGameEffect;->isCurrentSceneSupported()Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 304 */
v2 = this.mDeviceEffects;
v3 = this.mCurrentDevice;
/* check-cast v2, Ljava/util/ArrayList; */
v3 = java.lang.Integer .parseInt ( v0 );
(( java.util.ArrayList ) v2 ).get ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/String; */
/* .line 305 */
/* .local v2, "parameterSuffix":Ljava/lang/String; */
int v3 = 1; // const/4 v3, 0x1
/* iput-boolean v3, p0, Lcom/android/server/audio/AudioGameEffect;->isEffectOn:Z */
/* .line 306 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "startGameEffect parameter=misound_fps_effect=T-" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v1,v3 );
/* .line 307 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "misound_fps_effect=T-"; // const-string v3, "misound_fps_effect=T-"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.media.AudioSystem .setParameters ( v1 );
/* .line 308 */
} // .end local v2 # "parameterSuffix":Ljava/lang/String;
/* .line 309 */
} // :cond_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "no parameter found for package: "; // const-string v3, "no parameter found for package: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mCurrentEnablePkg;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 311 */
} // :goto_0
return;
} // .end method
private void stopGameEffect ( ) {
/* .locals 2 */
/* .line 314 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/audio/AudioGameEffect;->isEffectOn:Z */
/* .line 315 */
final String v0 = "AudioGameEffect"; // const-string v0, "AudioGameEffect"
/* const-string/jumbo v1, "stopGameEffect parameter=misound_fps_effect=F-0-0-0-0" */
android.util.Log .i ( v0,v1 );
/* .line 316 */
final String v0 = "misound_fps_effect=F-0-0-0-0"; // const-string v0, "misound_fps_effect=F-0-0-0-0"
android.media.AudioSystem .setParameters ( v0 );
/* .line 317 */
return;
} // .end method
