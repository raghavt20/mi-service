public class com.android.server.audio.AudioQueryWeatherService {
	 /* .source "AudioQueryWeatherService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;, */
	 /* Lcom/android/server/audio/AudioQueryWeatherService$LocationObserver; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String CITY_CONTENT_URI;
private static final java.lang.String CITY_PROJECTION;
private static final Integer CITY_QUERY_TOKEN;
private static final Boolean DEBUG;
private static final Integer FLAG_LOCATION_TRUE;
private static final Integer MAX_SUNRISE_TIME;
private static final Integer MAX_SUNSET_TIME;
private static final Integer MIN_SUNRISE_TIME;
private static final Integer MIN_SUNSET_TIME;
private static final Integer NO_FIND_OUT_RESULT;
private static final java.lang.String TAG;
private static final java.lang.String WEATHER_CONTENT_URI;
private static final java.lang.String WEATHER_PROJECTION;
private static final Integer WEATHER_QUERY_TOKEN;
/* # instance fields */
private android.content.BroadcastReceiver mBootCompleteReceiver;
private final android.content.ContentResolver mContentResolver;
private android.content.Context mContext;
private Boolean mInternationalLocation;
private com.android.server.audio.AudioQueryWeatherService$LocationObserver mLocationObserver;
private Boolean mNextSunriseSunsetTime;
private com.android.server.audio.AudioQueryWeatherService$QueryHandler mQueryHandler;
private Integer mSunriseTimeHours;
private Integer mSunriseTimeMins;
private Integer mSunsetTimeHours;
private Integer mSunsetTimeMins;
private android.content.BroadcastReceiver mUpdateTimeReceiver;
/* # direct methods */
static android.content.ContentResolver -$$Nest$fgetmContentResolver ( com.android.server.audio.AudioQueryWeatherService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContentResolver;
} // .end method
static void -$$Nest$fputmNextSunriseSunsetTime ( com.android.server.audio.AudioQueryWeatherService p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mNextSunriseSunsetTime:Z */
	 return;
} // .end method
static void -$$Nest$mstartCityQuery ( com.android.server.audio.AudioQueryWeatherService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/audio/AudioQueryWeatherService;->startCityQuery()V */
	 return;
} // .end method
static void -$$Nest$mupdateCityInfo ( com.android.server.audio.AudioQueryWeatherService p0, android.database.Cursor p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/audio/AudioQueryWeatherService;->updateCityInfo(Landroid/database/Cursor;)V */
	 return;
} // .end method
static void -$$Nest$mupdateWeatherInfo ( com.android.server.audio.AudioQueryWeatherService p0, android.database.Cursor p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/audio/AudioQueryWeatherService;->updateWeatherInfo(Landroid/database/Cursor;)V */
	 return;
} // .end method
static com.android.server.audio.AudioQueryWeatherService ( ) {
	 /* .locals 4 */
	 /* .line 36 */
	 /* const-string/jumbo v0, "sunrise" */
	 /* const-string/jumbo v1, "sunset" */
	 final String v2 = "city_id"; // const-string v2, "city_id"
	 /* filled-new-array {v2, v0, v1}, [Ljava/lang/String; */
	 /* .line 41 */
	 final String v0 = "flag"; // const-string v0, "flag"
	 final String v1 = "belongings"; // const-string v1, "belongings"
	 final String v2 = "posID"; // const-string v2, "posID"
	 final String v3 = "position"; // const-string v3, "position"
	 /* filled-new-array {v2, v3, v0, v1}, [Ljava/lang/String; */
	 return;
} // .end method
public com.android.server.audio.AudioQueryWeatherService ( ) {
	 /* .locals 3 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 76 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 58 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput-boolean v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mNextSunriseSunsetTime:Z */
	 /* .line 78 */
	 final String v1 = "AudioQueryWeatherService"; // const-string v1, "AudioQueryWeatherService"
	 final String v2 = "construct!!!"; // const-string v2, "construct!!!"
	 android.util.Log .d ( v1,v2 );
	 /* .line 80 */
	 this.mContext = p1;
	 /* .line 81 */
	 (( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 this.mContentResolver = v1;
	 /* .line 82 */
	 v1 = this.mQueryHandler;
	 /* if-nez v1, :cond_0 */
	 /* .line 83 */
	 /* new-instance v1, Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler; */
	 v2 = this.mContext;
	 /* invoke-direct {v1, p0, v2}, Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;-><init>(Lcom/android/server/audio/AudioQueryWeatherService;Landroid/content/Context;)V */
	 this.mQueryHandler = v1;
	 /* .line 85 */
} // :cond_0
/* iput v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunriseTimeHours:I */
/* .line 86 */
/* iput v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunsetTimeHours:I */
/* .line 87 */
/* iput v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunriseTimeMins:I */
/* .line 88 */
/* iput v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunsetTimeMins:I */
/* .line 89 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mInternationalLocation:Z */
/* .line 90 */
return;
} // .end method
private void CalculateLocationAndQuery ( Integer p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 4 */
/* .param p1, "flag" # I */
/* .param p2, "cityId" # Ljava/lang/String; */
/* .param p3, "belongings" # Ljava/lang/String; */
/* .line 172 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x110f01a3 */
(( android.content.res.Resources ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 173 */
/* .local v0, "LocationJudgment":Ljava/lang/String; */
int v1 = 1; // const/4 v1, 0x1
/* if-ne p1, v1, :cond_1 */
/* .line 174 */
if ( p3 != null) { // if-eqz p3, :cond_0
	 v2 = 	 (( java.lang.String ) p3 ).indexOf ( v0 ); // invoke-virtual {p3, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
	 int v3 = -1; // const/4 v3, -0x1
	 /* if-eq v2, v3, :cond_0 */
	 /* .line 175 */
	 int v1 = 0; // const/4 v1, 0x0
	 /* iput-boolean v1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mInternationalLocation:Z */
	 /* .line 177 */
} // :cond_0
/* iput-boolean v1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mInternationalLocation:Z */
/* .line 179 */
} // :goto_0
/* iget-boolean v1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mInternationalLocation:Z */
/* if-nez v1, :cond_2 */
v1 = android.text.TextUtils .isEmpty ( p2 );
/* if-nez v1, :cond_2 */
/* .line 180 */
/* invoke-direct {p0, p2}, Lcom/android/server/audio/AudioQueryWeatherService;->startWeatherQuery(Ljava/lang/String;)V */
/* .line 183 */
} // :cond_1
/* iput-boolean v1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mInternationalLocation:Z */
/* .line 185 */
} // :cond_2
} // :goto_1
return;
} // .end method
private void CalculateSunriseAndSunsetTime ( Long p0, Long p1 ) {
/* .locals 6 */
/* .param p1, "sunriseTime" # J */
/* .param p3, "sunsetTime" # J */
/* .line 236 */
android.icu.util.Calendar .getInstance ( );
/* .line 237 */
/* .local v0, "SunCalendar":Landroid/icu/util/Calendar; */
/* const-wide/16 v1, 0x0 */
/* cmp-long v3, p1, v1 */
if ( v3 != null) { // if-eqz v3, :cond_4
/* cmp-long v1, p3, v1 */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 238 */
(( android.icu.util.Calendar ) v0 ).setTimeInMillis ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Landroid/icu/util/Calendar;->setTimeInMillis(J)V
/* .line 239 */
/* const/16 v1, 0xb */
v2 = (( android.icu.util.Calendar ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Landroid/icu/util/Calendar;->get(I)I
/* iput v2, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunriseTimeHours:I */
/* .line 240 */
/* const/16 v2, 0xc */
v3 = (( android.icu.util.Calendar ) v0 ).get ( v2 ); // invoke-virtual {v0, v2}, Landroid/icu/util/Calendar;->get(I)I
/* iput v3, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunriseTimeMins:I */
/* .line 242 */
/* iget v3, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunriseTimeHours:I */
int v4 = 3; // const/4 v4, 0x3
int v5 = 0; // const/4 v5, 0x0
/* if-ge v3, v4, :cond_0 */
/* .line 243 */
/* iput v4, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunriseTimeHours:I */
/* .line 244 */
/* iput v5, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunriseTimeMins:I */
/* .line 246 */
} // :cond_0
/* iget v3, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunriseTimeHours:I */
/* const/16 v4, 0xa */
/* if-le v3, v4, :cond_1 */
/* .line 247 */
/* iput v1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunriseTimeHours:I */
/* .line 248 */
/* iput v5, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunriseTimeMins:I */
/* .line 250 */
} // :cond_1
(( android.icu.util.Calendar ) v0 ).setTimeInMillis ( p3, p4 ); // invoke-virtual {v0, p3, p4}, Landroid/icu/util/Calendar;->setTimeInMillis(J)V
/* .line 251 */
v1 = (( android.icu.util.Calendar ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Landroid/icu/util/Calendar;->get(I)I
/* iput v1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunsetTimeHours:I */
/* .line 252 */
v1 = (( android.icu.util.Calendar ) v0 ).get ( v2 ); // invoke-virtual {v0, v2}, Landroid/icu/util/Calendar;->get(I)I
/* iput v1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunsetTimeMins:I */
/* .line 254 */
/* iget v1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunsetTimeHours:I */
/* const/16 v2, 0xf */
/* if-ge v1, v2, :cond_2 */
/* .line 255 */
/* iput v2, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunsetTimeHours:I */
/* .line 256 */
/* iput v5, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunsetTimeMins:I */
/* .line 258 */
} // :cond_2
/* iget v1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunsetTimeHours:I */
/* const/16 v2, 0x16 */
/* if-le v1, v2, :cond_3 */
/* .line 259 */
/* const/16 v1, 0x17 */
/* iput v1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunsetTimeHours:I */
/* .line 260 */
/* iput v5, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunsetTimeMins:I */
/* .line 262 */
} // :cond_3
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "updateWeatherInfo sunriseHour=" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunriseTimeHours:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " sunriseMin="; // const-string v2, " sunriseMin="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunriseTimeMins:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " mNextSunriseSunsetTime="; // const-string v2, " mNextSunriseSunsetTime="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mNextSunriseSunsetTime:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "AudioQueryWeatherService"; // const-string v2, "AudioQueryWeatherService"
android.util.Log .d ( v2,v1 );
/* .line 264 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "updateWeatherInfo sunsetHour=" */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunsetTimeHours:I */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " sunsetMin="; // const-string v3, " sunsetMin="
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunsetTimeMins:I */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v1 );
/* .line 266 */
} // :cond_4
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mInternationalLocation:Z */
/* .line 268 */
} // :goto_0
return;
} // .end method
private void startCityQuery ( ) {
/* .locals 10 */
/* .line 157 */
/* const-class v0, Lcom/android/server/audio/AudioQueryWeatherService; */
/* monitor-enter v0 */
/* .line 158 */
try { // :try_start_0
v1 = this.mQueryHandler;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 159 */
/* const/16 v2, 0x3e9 */
(( com.android.server.audio.AudioQueryWeatherService$QueryHandler ) v1 ).cancelOperation ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;->cancelOperation(I)V
/* .line 160 */
v1 = this.mQueryHandler;
/* const/16 v2, 0x3e8 */
(( com.android.server.audio.AudioQueryWeatherService$QueryHandler ) v1 ).cancelOperation ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;->cancelOperation(I)V
/* .line 161 */
final String v1 = "content://weather/selected_city"; // const-string v1, "content://weather/selected_city"
android.net.Uri .parse ( v1 );
(( android.net.Uri ) v1 ).buildUpon ( ); // invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;
(( android.net.Uri$Builder ) v1 ).build ( ); // invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;
/* .line 163 */
/* .local v5, "contentUri":Landroid/net/Uri; */
v2 = this.mQueryHandler;
/* const/16 v3, 0x3e9 */
int v4 = 0; // const/4 v4, 0x0
v6 = com.android.server.audio.AudioQueryWeatherService.CITY_PROJECTION;
int v7 = 0; // const/4 v7, 0x0
int v8 = 0; // const/4 v8, 0x0
int v9 = 0; // const/4 v9, 0x0
/* invoke-virtual/range {v2 ..v9}, Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V */
/* .line 167 */
} // .end local v5 # "contentUri":Landroid/net/Uri;
} // :cond_0
/* monitor-exit v0 */
/* .line 168 */
return;
/* .line 167 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void startWeatherQuery ( java.lang.String p0 ) {
/* .locals 10 */
/* .param p1, "cityId" # Ljava/lang/String; */
/* .line 221 */
final String v0 = "AudioQueryWeatherService"; // const-string v0, "AudioQueryWeatherService"
/* const-string/jumbo v1, "startWeatherQuery start!" */
android.util.Log .d ( v0,v1 );
/* .line 223 */
/* const-class v0, Lcom/android/server/audio/AudioQueryWeatherService; */
/* monitor-enter v0 */
/* .line 224 */
try { // :try_start_0
v1 = this.mQueryHandler;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 225 */
/* const/16 v2, 0x3e8 */
(( com.android.server.audio.AudioQueryWeatherService$QueryHandler ) v1 ).cancelOperation ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;->cancelOperation(I)V
/* .line 226 */
final String v1 = "content://weather/actualWeatherData"; // const-string v1, "content://weather/actualWeatherData"
android.net.Uri .parse ( v1 );
(( android.net.Uri ) v1 ).buildUpon ( ); // invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;
final String v2 = "2"; // const-string v2, "2"
(( android.net.Uri$Builder ) v1 ).appendPath ( v2 ); // invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;
final String v2 = "0"; // const-string v2, "0"
(( android.net.Uri$Builder ) v1 ).appendPath ( v2 ); // invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;
(( android.net.Uri$Builder ) v1 ).build ( ); // invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;
/* .line 227 */
/* .local v5, "contentUri":Landroid/net/Uri; */
/* filled-new-array {p1}, [Ljava/lang/String; */
/* .line 228 */
/* .local v8, "selectionArgs":[Ljava/lang/String; */
v2 = this.mQueryHandler;
/* const/16 v3, 0x3e8 */
int v4 = 0; // const/4 v4, 0x0
v6 = com.android.server.audio.AudioQueryWeatherService.WEATHER_PROJECTION;
int v7 = 0; // const/4 v7, 0x0
int v9 = 0; // const/4 v9, 0x0
/* invoke-virtual/range {v2 ..v9}, Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V */
/* .line 232 */
} // .end local v5 # "contentUri":Landroid/net/Uri;
} // .end local v8 # "selectionArgs":[Ljava/lang/String;
} // :cond_0
/* monitor-exit v0 */
/* .line 233 */
return;
/* .line 232 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void updateCityInfo ( android.database.Cursor p0 ) {
/* .locals 6 */
/* .param p1, "cursor" # Landroid/database/Cursor; */
/* .line 188 */
int v0 = 0; // const/4 v0, 0x0
/* .line 189 */
/* .local v0, "cityId":Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .line 190 */
/* .local v1, "belongings":Ljava/lang/String; */
int v2 = -1; // const/4 v2, -0x1
/* .line 192 */
/* .local v2, "flag":I */
final String v3 = "AudioQueryWeatherService"; // const-string v3, "AudioQueryWeatherService"
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 194 */
v4 = try { // :try_start_0
v4 = if ( v4 != null) { // if-eqz v4, :cond_0
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 195 */
v4 = final String v4 = "posID"; // const-string v4, "posID"
/* move-object v0, v4 */
/* .line 196 */
v4 = final String v4 = "belongings"; // const-string v4, "belongings"
/* move-object v1, v4 */
/* .line 197 */
v4 = v4 = final String v4 = "flag"; // const-string v4, "flag"
/* move v2, v4 */
/* .line 198 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "updateCityInfo flag:" */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v3,v4 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 204 */
} // :cond_0
try { // :try_start_1
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 207 */
} // :goto_0
/* .line 205 */
/* :catch_0 */
/* move-exception v3 */
/* .line 206 */
/* .local v3, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v3 ).printStackTrace ( ); // invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
/* .line 208 */
} // .end local v3 # "e":Ljava/lang/Exception;
/* .line 203 */
/* :catchall_0 */
/* move-exception v3 */
/* .line 200 */
/* :catch_1 */
/* move-exception v3 */
/* .line 201 */
/* .restart local v3 # "e":Ljava/lang/Exception; */
try { // :try_start_2
(( java.lang.Exception ) v3 ).printStackTrace ( ); // invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 204 */
} // .end local v3 # "e":Ljava/lang/Exception;
try { // :try_start_3
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 208 */
} // :goto_1
/* nop */
/* .line 216 */
/* invoke-direct {p0, v2, v0, v1}, Lcom/android/server/audio/AudioQueryWeatherService;->CalculateLocationAndQuery(ILjava/lang/String;Ljava/lang/String;)V */
/* .line 217 */
return;
/* .line 204 */
} // :goto_2
try { // :try_start_4
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_2 */
/* .line 207 */
/* .line 205 */
/* :catch_2 */
/* move-exception v4 */
/* .line 206 */
/* .local v4, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v4 ).printStackTrace ( ); // invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
/* .line 208 */
} // .end local v4 # "e":Ljava/lang/Exception;
} // :goto_3
/* throw v3 */
/* .line 211 */
} // :cond_1
final String v4 = "location: weather maybe uninstalled"; // const-string v4, "location: weather maybe uninstalled"
android.util.Log .d ( v3,v4 );
/* .line 213 */
int v3 = 1; // const/4 v3, 0x1
/* iput-boolean v3, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mInternationalLocation:Z */
/* .line 214 */
return;
} // .end method
private void updateWeatherInfo ( android.database.Cursor p0 ) {
/* .locals 7 */
/* .param p1, "cursor" # Landroid/database/Cursor; */
/* .line 271 */
/* const-string/jumbo v0, "updateWeatherInfo start!" */
final String v1 = "AudioQueryWeatherService"; // const-string v1, "AudioQueryWeatherService"
android.util.Log .d ( v1,v0 );
/* .line 272 */
int v0 = 1; // const/4 v0, 0x1
if ( p1 != null) { // if-eqz p1, :cond_2
/* .line 274 */
/* const-wide/16 v1, 0x0 */
/* .line 275 */
/* .local v1, "sunriseTime":J */
/* const-wide/16 v3, 0x0 */
/* .line 276 */
/* .local v3, "sunsetTime":J */
v5 = try { // :try_start_0
v5 = if ( v5 != null) { // if-eqz v5, :cond_1
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 277 */
/* iget-boolean v5, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mNextSunriseSunsetTime:Z */
/* if-ne v5, v0, :cond_0 */
/* .line 278 */
/* .line 280 */
} // :cond_0
v5 = /* const-string/jumbo v5, "sunrise" */
/* move-result-wide v5 */
/* move-wide v1, v5 */
/* .line 281 */
v5 = /* const-string/jumbo v5, "sunset" */
/* move-result-wide v5 */
/* move-wide v3, v5 */
/* .line 283 */
} // :cond_1
/* iput-boolean v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mInternationalLocation:Z */
/* .line 285 */
} // :goto_0
/* invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/audio/AudioQueryWeatherService;->CalculateSunriseAndSunsetTime(JJ)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 291 */
} // .end local v1 # "sunriseTime":J
} // .end local v3 # "sunsetTime":J
try { // :try_start_1
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 290 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 286 */
/* :catch_0 */
/* move-exception v1 */
/* .line 287 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_2
/* iput-boolean v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mInternationalLocation:Z */
/* .line 288 */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 291 */
} // .end local v1 # "e":Ljava/lang/Exception;
try { // :try_start_3
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_1 */
/* .line 294 */
} // :goto_1
/* .line 292 */
/* :catch_1 */
/* move-exception v0 */
/* .line 293 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 295 */
} // .end local v0 # "e":Ljava/lang/Exception;
/* nop */
} // :goto_2
/* .line 291 */
} // :goto_3
try { // :try_start_4
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_2 */
/* .line 294 */
/* .line 292 */
/* :catch_2 */
/* move-exception v1 */
/* .line 293 */
/* .restart local v1 # "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 295 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_4
/* throw v0 */
/* .line 297 */
} // :cond_2
/* const-string/jumbo v2, "weather maybe uninstalled" */
android.util.Log .d ( v1,v2 );
/* .line 298 */
/* iput-boolean v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mInternationalLocation:Z */
/* .line 300 */
} // :goto_5
return;
} // .end method
/* # virtual methods */
public Boolean getDefaultTimeZoneStatus ( ) {
/* .locals 1 */
/* .line 303 */
/* iget-boolean v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mInternationalLocation:Z */
} // .end method
public Integer getSunriseTimeHours ( ) {
/* .locals 1 */
/* .line 307 */
/* iget v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunriseTimeHours:I */
} // .end method
public Integer getSunriseTimeMins ( ) {
/* .locals 1 */
/* .line 315 */
/* iget v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunriseTimeMins:I */
} // .end method
public Integer getSunsetTimeHours ( ) {
/* .locals 1 */
/* .line 311 */
/* iget v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunsetTimeHours:I */
} // .end method
public Integer getSunsetTimeMins ( ) {
/* .locals 1 */
/* .line 319 */
/* iget v0, p0, Lcom/android/server/audio/AudioQueryWeatherService;->mSunsetTimeMins:I */
} // .end method
public void onCreate ( ) {
/* .locals 4 */
/* .line 94 */
final String v0 = "AudioQueryWeatherService"; // const-string v0, "AudioQueryWeatherService"
final String v1 = "onCreate!!!"; // const-string v1, "onCreate!!!"
android.util.Log .d ( v0,v1 );
/* .line 97 */
v0 = this.mBootCompleteReceiver;
/* const/16 v1, 0x3e8 */
/* if-nez v0, :cond_0 */
/* .line 98 */
/* new-instance v0, Lcom/android/server/audio/AudioQueryWeatherService$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/audio/AudioQueryWeatherService$1;-><init>(Lcom/android/server/audio/AudioQueryWeatherService;)V */
this.mBootCompleteReceiver = v0;
/* .line 109 */
/* new-instance v0, Landroid/content/IntentFilter; */
final String v2 = "android.intent.action.BOOT_COMPLETED"; // const-string v2, "android.intent.action.BOOT_COMPLETED"
/* invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
/* .line 110 */
/* .local v0, "intentFilterBootComplete":Landroid/content/IntentFilter; */
(( android.content.IntentFilter ) v0 ).setPriority ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->setPriority(I)V
/* .line 111 */
v2 = this.mContext;
v3 = this.mBootCompleteReceiver;
(( android.content.Context ) v2 ).registerReceiver ( v3, v0 ); // invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 115 */
} // .end local v0 # "intentFilterBootComplete":Landroid/content/IntentFilter;
} // :cond_0
v0 = this.mUpdateTimeReceiver;
/* if-nez v0, :cond_1 */
/* .line 116 */
/* new-instance v0, Lcom/android/server/audio/AudioQueryWeatherService$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/audio/AudioQueryWeatherService$2;-><init>(Lcom/android/server/audio/AudioQueryWeatherService;)V */
this.mUpdateTimeReceiver = v0;
/* .line 128 */
/* new-instance v0, Landroid/content/IntentFilter; */
final String v2 = "com.android.media.update.sunrise.sunset.time"; // const-string v2, "com.android.media.update.sunrise.sunset.time"
/* invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
/* .line 129 */
/* .local v0, "intentFilterUpdateTime":Landroid/content/IntentFilter; */
(( android.content.IntentFilter ) v0 ).setPriority ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->setPriority(I)V
/* .line 130 */
v1 = this.mContext;
v2 = this.mUpdateTimeReceiver;
(( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 132 */
} // .end local v0 # "intentFilterUpdateTime":Landroid/content/IntentFilter;
} // :cond_1
/* new-instance v0, Lcom/android/server/audio/AudioQueryWeatherService$LocationObserver; */
/* invoke-direct {v0, p0}, Lcom/android/server/audio/AudioQueryWeatherService$LocationObserver;-><init>(Lcom/android/server/audio/AudioQueryWeatherService;)V */
this.mLocationObserver = v0;
/* .line 133 */
(( com.android.server.audio.AudioQueryWeatherService$LocationObserver ) v0 ).onCreate ( ); // invoke-virtual {v0}, Lcom/android/server/audio/AudioQueryWeatherService$LocationObserver;->onCreate()V
/* .line 134 */
return;
} // .end method
