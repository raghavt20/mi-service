class com.android.server.audio.AudioPowerSaveModeObserver$WorkHandler extends android.os.Handler {
	 /* .source "AudioPowerSaveModeObserver.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/audio/AudioPowerSaveModeObserver; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "WorkHandler" */
} // .end annotation
/* # instance fields */
final com.android.server.audio.AudioPowerSaveModeObserver this$0; //synthetic
/* # direct methods */
public com.android.server.audio.AudioPowerSaveModeObserver$WorkHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 89 */
this.this$0 = p1;
/* .line 90 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 91 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 4 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 95 */
final String v0 = "AudioPowerSaveModeObserver"; // const-string v0, "AudioPowerSaveModeObserver"
try { // :try_start_0
	 /* iget v1, p1, Landroid/os/Message;->what:I */
	 /* const/16 v2, 0x9c6 */
	 /* const/16 v3, 0x9c5 */
	 /* packed-switch v1, :pswitch_data_0 */
	 /* goto/16 :goto_0 */
	 /* .line 116 */
	 /* :pswitch_0 */
	 final String v1 = "MSG_CHECK_AUDIO_POWER_SAVE_POLICY"; // const-string v1, "MSG_CHECK_AUDIO_POWER_SAVE_POLICY"
	 android.util.Log .d ( v0,v1 );
	 /* .line 117 */
	 v1 = this.this$0;
	 com.android.server.audio.AudioPowerSaveModeObserver .-$$Nest$fgetmHandler ( v1 );
	 (( com.android.server.audio.AudioPowerSaveModeObserver$WorkHandler ) v1 ).removeMessages ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->removeMessages(I)V
	 /* .line 118 */
	 v1 = this.this$0;
	 com.android.server.audio.AudioPowerSaveModeObserver .-$$Nest$fgetmHandler ( v1 );
	 (( com.android.server.audio.AudioPowerSaveModeObserver$WorkHandler ) v1 ).removeMessages ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->removeMessages(I)V
	 /* .line 119 */
	 v1 = this.this$0;
	 final String v2 = "/sys/class/thermal/power_save/powersave_mode"; // const-string v2, "/sys/class/thermal/power_save/powersave_mode"
	 v1 = 	 com.android.server.audio.AudioPowerSaveModeObserver .-$$Nest$mcheckAudioPowerSaveState ( v1,v2 );
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 120 */
		 v1 = this.this$0;
		 com.android.server.audio.AudioPowerSaveModeObserver .-$$Nest$mtoSleep ( v1 );
		 /* .line 121 */
		 v1 = this.this$0;
		 final String v2 = "/sys/class/thermal/power_save/power_level"; // const-string v2, "/sys/class/thermal/power_save/power_level"
		 com.android.server.audio.AudioPowerSaveModeObserver .-$$Nest$mcheckAudioPowerSaveLevel ( v1,v2 );
		 /* goto/16 :goto_0 */
		 /* .line 123 */
	 } // :cond_0
	 v1 = this.this$0;
	 com.android.server.audio.AudioPowerSaveModeObserver .-$$Nest$fgetmHandler ( v1 );
	 v2 = this.this$0;
	 com.android.server.audio.AudioPowerSaveModeObserver .-$$Nest$fgetmHandler ( v2 );
	 /* const/16 v3, 0x9c7 */
	 (( com.android.server.audio.AudioPowerSaveModeObserver$WorkHandler ) v2 ).obtainMessage ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->obtainMessage(I)Landroid/os/Message;
	 (( com.android.server.audio.AudioPowerSaveModeObserver$WorkHandler ) v1 ).sendMessage ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->sendMessage(Landroid/os/Message;)Z
	 /* .line 124 */
	 final String v1 = "PowerSaveMode is close now"; // const-string v1, "PowerSaveMode is close now"
	 android.util.Log .d ( v0,v1 );
	 /* .line 126 */
	 /* .line 109 */
	 /* :pswitch_1 */
	 final String v1 = "MSG_CLEAR_AUDIO_POWER_SAVE_POLICY"; // const-string v1, "MSG_CLEAR_AUDIO_POWER_SAVE_POLICY"
	 android.util.Log .d ( v0,v1 );
	 /* .line 110 */
	 v1 = this.this$0;
	 com.android.server.audio.AudioPowerSaveModeObserver .-$$Nest$fgetmHandler ( v1 );
	 (( com.android.server.audio.AudioPowerSaveModeObserver$WorkHandler ) v1 ).removeMessages ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->removeMessages(I)V
	 /* .line 111 */
	 v1 = this.this$0;
	 com.android.server.audio.AudioPowerSaveModeObserver .-$$Nest$fgetmHandler ( v1 );
	 (( com.android.server.audio.AudioPowerSaveModeObserver$WorkHandler ) v1 ).removeMessages ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->removeMessages(I)V
	 /* .line 112 */
	 v1 = this.this$0;
	 int v2 = 0; // const/4 v2, 0x0
	 com.android.server.audio.AudioPowerSaveModeObserver .-$$Nest$msetAudioPowerSaveMode ( v1,v2 );
	 /* .line 113 */
	 v1 = this.this$0;
	 com.android.server.audio.AudioPowerSaveModeObserver .-$$Nest$msetAudioPowerSaveLevel ( v1,v2 );
	 /* .line 114 */
	 /* .line 103 */
	 /* :pswitch_2 */
	 /* iget v1, p1, Landroid/os/Message;->arg1:I */
	 /* .line 104 */
	 /* .local v1, "mAudioPowerSaveLevel":I */
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v3 = "MSG_SET_AUDIO_POWER_SAVE_LEVEL_POLICY, AudioPowerSaveLevel is "; // const-string v3, "MSG_SET_AUDIO_POWER_SAVE_LEVEL_POLICY, AudioPowerSaveLevel is "
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Log .d ( v0,v2 );
	 /* .line 106 */
	 v2 = this.this$0;
	 com.android.server.audio.AudioPowerSaveModeObserver .-$$Nest$msetAudioPowerSaveLevel ( v2,v1 );
	 /* .line 107 */
	 /* .line 97 */
} // .end local v1 # "mAudioPowerSaveLevel":I
/* :pswitch_3 */
/* iget v1, p1, Landroid/os/Message;->arg1:I */
/* .line 98 */
/* .local v1, "mAudioPowerSaveMode":I */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "MSG_SET_AUDIO_POWER_SAVE_MODE_POLICY, AudioPowerSaveMode is "; // const-string v3, "MSG_SET_AUDIO_POWER_SAVE_MODE_POLICY, AudioPowerSaveMode is "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v2 );
/* .line 100 */
v2 = this.this$0;
com.android.server.audio.AudioPowerSaveModeObserver .-$$Nest$msetAudioPowerSaveMode ( v2,v1 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 101 */
/* nop */
/* .line 132 */
} // .end local v1 # "mAudioPowerSaveMode":I
} // :goto_0
/* .line 130 */
/* :catch_0 */
/* move-exception v1 */
/* .line 131 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "handleMessage exception "; // const-string v3, "handleMessage exception "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v2 );
/* .line 133 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x9c5 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
