public class com.android.server.audio.GameAudioEnhancer {
	 /* .source "GameAudioEnhancer.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/audio/GameAudioEnhancer$SettingsObserver;, */
	 /* Lcom/android/server/audio/GameAudioEnhancer$WorkHandler; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String ADUIO_GAME_SOUND_EFFECT_SWITCH;
private static final java.lang.String ADUIO_GAME_SOUND_NEW_EFFECT_SWITCH;
private static final java.lang.String AUDIO_GAME_PACKAGE_NAME;
public static final java.lang.String GAME_MODE_ENABLE;
public static final java.lang.String GAME_MODE_PKGS;
private static final Integer MSG_SET_PROCESS_LISTENER;
private static final Integer MSG_START_GAME_EFFECT;
private static final Integer MSG_STOP_GAME_EFFECT;
private static final java.lang.String PM_SERVICE_NAME;
private static final Integer QUERY_PM_SERVICE_DELAY_MS;
private static final Integer QUERY_PM_SERVICE_MAX_TIMES;
private static final Integer SEND_PARAMETER_DELAY_MS;
private static final java.lang.String TAG;
/* # instance fields */
private final java.lang.String LOCAL_ENABLED_PACKAGES;
private android.media.AudioManager mAudioManager;
private final android.content.ContentResolver mContentResolver;
private android.content.Context mContext;
private java.lang.String mCurForegroundPkg;
private java.lang.String mCurrentEnablePkg;
private java.util.Set mEnabledPackages;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final miui.process.IForegroundInfoListener$Stub mForegroundInfoListener;
private java.lang.String mFreeFormWindowPkg;
private Integer mFreeWinVersion;
private final miui.app.IFreeformCallback mFreeformCallBack;
private Integer mGameModeSwitchStatus;
private com.android.server.audio.GameAudioEnhancer$WorkHandler mHandler;
private final java.lang.Object mLock;
private volatile Boolean mParamSet;
private Integer mQueryPMServiceTime;
private com.android.server.audio.GameAudioEnhancer$SettingsObserver mSettingsObserver;
/* # direct methods */
static android.content.ContentResolver -$$Nest$fgetmContentResolver ( com.android.server.audio.GameAudioEnhancer p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContentResolver;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.audio.GameAudioEnhancer p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static java.lang.String -$$Nest$fgetmCurForegroundPkg ( com.android.server.audio.GameAudioEnhancer p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCurForegroundPkg;
} // .end method
static miui.process.IForegroundInfoListener$Stub -$$Nest$fgetmForegroundInfoListener ( com.android.server.audio.GameAudioEnhancer p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mForegroundInfoListener;
} // .end method
static Integer -$$Nest$fgetmFreeWinVersion ( com.android.server.audio.GameAudioEnhancer p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mFreeWinVersion:I */
} // .end method
static Integer -$$Nest$fgetmGameModeSwitchStatus ( com.android.server.audio.GameAudioEnhancer p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mGameModeSwitchStatus:I */
} // .end method
static com.android.server.audio.GameAudioEnhancer$WorkHandler -$$Nest$fgetmHandler ( com.android.server.audio.GameAudioEnhancer p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static java.lang.Object -$$Nest$fgetmLock ( com.android.server.audio.GameAudioEnhancer p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mLock;
} // .end method
static Boolean -$$Nest$fgetmParamSet ( com.android.server.audio.GameAudioEnhancer p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mParamSet:Z */
} // .end method
static Integer -$$Nest$fgetmQueryPMServiceTime ( com.android.server.audio.GameAudioEnhancer p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mQueryPMServiceTime:I */
} // .end method
static void -$$Nest$fputmCurForegroundPkg ( com.android.server.audio.GameAudioEnhancer p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mCurForegroundPkg = p1;
return;
} // .end method
static void -$$Nest$fputmFreeFormWindowPkg ( com.android.server.audio.GameAudioEnhancer p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mFreeFormWindowPkg = p1;
return;
} // .end method
static void -$$Nest$fputmParamSet ( com.android.server.audio.GameAudioEnhancer p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/audio/GameAudioEnhancer;->mParamSet:Z */
return;
} // .end method
static void -$$Nest$fputmQueryPMServiceTime ( com.android.server.audio.GameAudioEnhancer p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/audio/GameAudioEnhancer;->mQueryPMServiceTime:I */
return;
} // .end method
static void -$$Nest$minitProcessListenerAsync ( com.android.server.audio.GameAudioEnhancer p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/audio/GameAudioEnhancer;->initProcessListenerAsync()V */
return;
} // .end method
static Boolean -$$Nest$misPackageEnabled ( com.android.server.audio.GameAudioEnhancer p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/audio/GameAudioEnhancer;->isPackageEnabled(Ljava/lang/String;)Z */
} // .end method
static void -$$Nest$msetGameMode ( com.android.server.audio.GameAudioEnhancer p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/audio/GameAudioEnhancer;->setGameMode(Z)V */
return;
} // .end method
static void -$$Nest$msetParamSend ( com.android.server.audio.GameAudioEnhancer p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/audio/GameAudioEnhancer;->setParamSend(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$mupdateGameModeSettingstatus ( com.android.server.audio.GameAudioEnhancer p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/audio/GameAudioEnhancer;->updateGameModeSettingstatus()V */
return;
} // .end method
static void -$$Nest$mupdateWhiteList ( com.android.server.audio.GameAudioEnhancer p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/audio/GameAudioEnhancer;->updateWhiteList()V */
return;
} // .end method
public com.android.server.audio.GameAudioEnhancer ( ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 80 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 55 */
final String v0 = "com.tencent.tmgp.cf"; // const-string v0, "com.tencent.tmgp.cf"
final String v1 = "com.tencent.mf.uam"; // const-string v1, "com.tencent.mf.uam"
final String v2 = "com.tencent.toaa"; // const-string v2, "com.tencent.toaa"
final String v3 = "com.tencent.tmgp.cod"; // const-string v3, "com.tencent.tmgp.cod"
final String v4 = "com.miHoYo.ys.mi"; // const-string v4, "com.miHoYo.ys.mi"
final String v5 = "com.tencent.tmgp.sgame"; // const-string v5, "com.tencent.tmgp.sgame"
/* filled-new-array/range {v0 ..v5}, [Ljava/lang/String; */
this.LOCAL_ENABLED_PACKAGES = v0;
/* .line 59 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
this.mEnabledPackages = v0;
/* .line 64 */
final String v0 = ""; // const-string v0, ""
this.mCurrentEnablePkg = v0;
/* .line 65 */
this.mCurForegroundPkg = v0;
/* .line 66 */
this.mFreeFormWindowPkg = v0;
/* .line 67 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mParamSet:Z */
/* .line 68 */
int v1 = 1; // const/4 v1, 0x1
/* iput v1, p0, Lcom/android/server/audio/GameAudioEnhancer;->mGameModeSwitchStatus:I */
/* .line 69 */
/* iput v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mQueryPMServiceTime:I */
/* .line 70 */
v1 = miui.app.MiuiFreeFormManager .getMiuiFreeformVersion ( );
/* iput v1, p0, Lcom/android/server/audio/GameAudioEnhancer;->mFreeWinVersion:I */
/* .line 72 */
/* new-instance v1, Ljava/lang/Object; */
/* invoke-direct {v1}, Ljava/lang/Object;-><init>()V */
this.mLock = v1;
/* .line 146 */
/* new-instance v1, Lcom/android/server/audio/GameAudioEnhancer$1; */
/* invoke-direct {v1, p0}, Lcom/android/server/audio/GameAudioEnhancer$1;-><init>(Lcom/android/server/audio/GameAudioEnhancer;)V */
this.mFreeformCallBack = v1;
/* .line 209 */
/* new-instance v1, Lcom/android/server/audio/GameAudioEnhancer$2; */
/* invoke-direct {v1, p0}, Lcom/android/server/audio/GameAudioEnhancer$2;-><init>(Lcom/android/server/audio/GameAudioEnhancer;)V */
this.mForegroundInfoListener = v1;
/* .line 81 */
this.mContext = p1;
/* .line 82 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
this.mContentResolver = v1;
/* .line 83 */
/* new-instance v1, Lcom/android/server/audio/GameAudioEnhancer$SettingsObserver; */
/* invoke-direct {v1, p0}, Lcom/android/server/audio/GameAudioEnhancer$SettingsObserver;-><init>(Lcom/android/server/audio/GameAudioEnhancer;)V */
this.mSettingsObserver = v1;
/* .line 84 */
v1 = this.mContext;
final String v2 = "audio"; // const-string v2, "audio"
(( android.content.Context ) v1 ).getSystemService ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/media/AudioManager; */
this.mAudioManager = v1;
/* .line 85 */
/* new-instance v1, Lcom/android/server/audio/GameAudioEnhancer$WorkHandler; */
/* invoke-direct {v1, p0, p2}, Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;-><init>(Lcom/android/server/audio/GameAudioEnhancer;Landroid/os/Looper;)V */
this.mHandler = v1;
/* .line 86 */
/* iput v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mQueryPMServiceTime:I */
/* .line 87 */
/* invoke-direct {p0}, Lcom/android/server/audio/GameAudioEnhancer;->initSettingStatus()V */
/* .line 88 */
/* invoke-direct {p0}, Lcom/android/server/audio/GameAudioEnhancer;->initEnabledPackages()V */
/* .line 89 */
/* invoke-direct {p0}, Lcom/android/server/audio/GameAudioEnhancer;->initProcessListenerAsync()V */
/* .line 90 */
/* invoke-direct {p0}, Lcom/android/server/audio/GameAudioEnhancer;->registerForegroundObserver()V */
/* .line 91 */
return;
} // .end method
private void beClosed ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 292 */
/* iget-boolean v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mParamSet:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/audio/GameAudioEnhancer;->isPackageEnabled(Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 293 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput-boolean v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mParamSet:Z */
	 /* .line 294 */
	 v0 = this.mHandler;
	 int v1 = 3; // const/4 v1, 0x3
	 (( com.android.server.audio.GameAudioEnhancer$WorkHandler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;->obtainMessage(I)Landroid/os/Message;
	 /* const-wide/16 v2, 0x1f4 */
	 (( com.android.server.audio.GameAudioEnhancer$WorkHandler ) v0 ).sendMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
	 /* .line 297 */
} // :cond_0
return;
} // .end method
private void beOpened ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 277 */
/* iget-boolean v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mParamSet:Z */
/* const-wide/16 v1, 0x1f4 */
/* if-nez v0, :cond_0 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/audio/GameAudioEnhancer;->isPackageEnabled(Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 278 */
	 int v0 = 1; // const/4 v0, 0x1
	 /* iput-boolean v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mParamSet:Z */
	 /* .line 279 */
	 this.mCurrentEnablePkg = p1;
	 /* .line 280 */
	 v0 = this.mHandler;
	 int v3 = 2; // const/4 v3, 0x2
	 (( com.android.server.audio.GameAudioEnhancer$WorkHandler ) v0 ).obtainMessage ( v3 ); // invoke-virtual {v0, v3}, Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;->obtainMessage(I)Landroid/os/Message;
	 (( com.android.server.audio.GameAudioEnhancer$WorkHandler ) v0 ).sendMessageDelayed ( v3, v1, v2 ); // invoke-virtual {v0, v3, v1, v2}, Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
	 /* .line 282 */
	 return;
	 /* .line 284 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mParamSet:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
	 v0 = 	 /* invoke-direct {p0, p1}, Lcom/android/server/audio/GameAudioEnhancer;->isPackageEnabled(Ljava/lang/String;)Z */
	 /* if-nez v0, :cond_1 */
	 /* .line 285 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput-boolean v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mParamSet:Z */
	 /* .line 286 */
	 v0 = this.mHandler;
	 int v3 = 3; // const/4 v3, 0x3
	 (( com.android.server.audio.GameAudioEnhancer$WorkHandler ) v0 ).obtainMessage ( v3 ); // invoke-virtual {v0, v3}, Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;->obtainMessage(I)Landroid/os/Message;
	 (( com.android.server.audio.GameAudioEnhancer$WorkHandler ) v0 ).sendMessageDelayed ( v3, v1, v2 ); // invoke-virtual {v0, v3, v1, v2}, Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
	 /* .line 289 */
} // :cond_1
return;
} // .end method
private void initEnabledPackages ( ) {
/* .locals 1 */
/* .line 348 */
v0 = /* invoke-direct {p0}, Lcom/android/server/audio/GameAudioEnhancer;->initEnabledPackagesFromCloudSettings()Z */
/* if-nez v0, :cond_0 */
/* .line 349 */
/* invoke-direct {p0}, Lcom/android/server/audio/GameAudioEnhancer;->initLocalPackages()V */
/* .line 351 */
} // :cond_0
return;
} // .end method
private Boolean initEnabledPackagesFromCloudSettings ( ) {
/* .locals 9 */
/* .line 354 */
v0 = this.mContentResolver;
final String v1 = "game_mode_packages"; // const-string v1, "game_mode_packages"
android.provider.Settings$Global .getString ( v0,v1 );
/* .line 355 */
/* .local v0, "pkgs":Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_2
v2 = (( java.lang.String ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z
if ( v2 != null) { // if-eqz v2, :cond_0
	 /* .line 358 */
} // :cond_0
v2 = this.mEnabledPackages;
/* .line 359 */
v2 = (( java.lang.String ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/String;->length()I
int v3 = 1; // const/4 v3, 0x1
/* sub-int/2addr v2, v3 */
(( java.lang.String ) v0 ).substring ( v3, v2 ); // invoke-virtual {v0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* .line 360 */
/* .local v2, "strPkgs":Ljava/lang/String; */
final String v4 = ","; // const-string v4, ","
(( java.lang.String ) v2 ).split ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 361 */
/* .local v4, "jsonStrs":[Ljava/lang/String; */
/* array-length v5, v4 */
} // :goto_0
/* if-ge v1, v5, :cond_1 */
/* aget-object v6, v4, v1 */
/* .line 362 */
/* .local v6, "jsonStr":Ljava/lang/String; */
v7 = this.mEnabledPackages;
v8 = (( java.lang.String ) v6 ).length ( ); // invoke-virtual {v6}, Ljava/lang/String;->length()I
/* sub-int/2addr v8, v3 */
(( java.lang.String ) v6 ).substring ( v3, v8 ); // invoke-virtual {v6, v3, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* .line 361 */
} // .end local v6 # "jsonStr":Ljava/lang/String;
/* add-int/lit8 v1, v1, 0x1 */
/* .line 364 */
} // :cond_1
/* .line 356 */
} // .end local v2 # "strPkgs":Ljava/lang/String;
} // .end local v4 # "jsonStrs":[Ljava/lang/String;
} // :cond_2
} // :goto_1
} // .end method
private void initLocalPackages ( ) {
/* .locals 2 */
/* .line 369 */
v0 = this.mEnabledPackages;
v1 = this.LOCAL_ENABLED_PACKAGES;
java.util.Collections .addAll ( v0,v1 );
/* .line 370 */
return;
} // .end method
private void initProcessListenerAsync ( ) {
/* .locals 4 */
/* .line 330 */
v0 = this.mHandler;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.audio.GameAudioEnhancer$WorkHandler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;->obtainMessage(I)Landroid/os/Message;
/* const-wide/16 v2, 0x3e8 */
(( com.android.server.audio.GameAudioEnhancer$WorkHandler ) v0 ).sendMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 332 */
return;
} // .end method
private void initSettingStatus ( ) {
/* .locals 5 */
/* .line 335 */
final String v0 = "game_mode_enable"; // const-string v0, "game_mode_enable"
int v1 = 0; // const/4 v1, 0x0
/* .line 337 */
/* .local v1, "enable":I */
try { // :try_start_0
v2 = this.mContentResolver;
v0 = android.provider.Settings$Global .getInt ( v2,v0 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v1, v0 */
/* .line 341 */
/* .line 338 */
/* :catch_0 */
/* move-exception v2 */
/* .line 340 */
/* .local v2, "e":Ljava/lang/Exception; */
v3 = this.mContentResolver;
int v4 = 1; // const/4 v4, 0x1
android.provider.Settings$Global .putInt ( v3,v0,v4 );
/* .line 342 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_0
/* nop */
/* .line 343 */
return;
} // .end method
private Boolean isPackageEnabled ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 259 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 260 */
try { // :try_start_0
v1 = v1 = this.mEnabledPackages;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 261 */
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 263 */
} // :cond_0
/* monitor-exit v0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 264 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void registerForegroundObserver ( ) {
/* .locals 1 */
/* .line 94 */
v0 = this.mFreeformCallBack;
miui.app.MiuiFreeFormManager .registerFreeformCallback ( v0 );
/* .line 95 */
v0 = this.mForegroundInfoListener;
miui.process.ProcessManager .registerForegroundInfoListener ( v0 );
/* .line 96 */
return;
} // .end method
private void setGameMode ( Boolean p0 ) {
/* .locals 7 */
/* .param p1, "on" # Z */
/* .line 314 */
final String v0 = "ro.vendor.audio.game.mode"; // const-string v0, "ro.vendor.audio.game.mode"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
final String v1 = "GameAudioEnhancer"; // const-string v1, "GameAudioEnhancer"
final String v2 = "="; // const-string v2, "="
final String v3 = "audio_game_package_name"; // const-string v3, "audio_game_package_name"
final String v4 = "on;"; // const-string v4, "on;"
final String v5 = "off;"; // const-string v5, "off;"
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 315 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "sendparam=AudioGameEnhance;audio_game_sound_mode_switch=" */
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 316 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* move-object v6, v4 */
} // :cond_0
/* move-object v6, v5 */
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.mCurrentEnablePkg;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 315 */
android.media.AudioSystem .setParameters ( v0 );
/* .line 318 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "audio_game_sound_mode_switch="; // const-string v6, "audio_game_sound_mode_switch="
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
if ( p1 != null) { // if-eqz p1, :cond_1
} // :cond_1
/* move-object v4, v5 */
} // :goto_1
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mCurrentEnablePkg;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v1,v0 );
/* .line 321 */
} // :cond_2
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "sendparam=AudioGameEnhance;audio_game_sound_effect_switch=" */
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 322 */
if ( p1 != null) { // if-eqz p1, :cond_3
/* move-object v6, v4 */
} // :cond_3
/* move-object v6, v5 */
} // :goto_2
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.mCurrentEnablePkg;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 321 */
android.media.AudioSystem .setParameters ( v0 );
/* .line 324 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "audio_game_sound_effect_switch="; // const-string v6, "audio_game_sound_effect_switch="
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
if ( p1 != null) { // if-eqz p1, :cond_4
} // :cond_4
/* move-object v4, v5 */
} // :goto_3
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mCurrentEnablePkg;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v1,v0 );
/* .line 327 */
} // :goto_4
return;
} // .end method
private void setGameModeEnabled ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "enable" # I */
/* .line 305 */
/* iput p1, p0, Lcom/android/server/audio/GameAudioEnhancer;->mGameModeSwitchStatus:I */
/* .line 306 */
v0 = this.mFreeFormWindowPkg;
v0 = (( java.lang.String ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 307 */
return;
/* .line 309 */
} // :cond_0
v0 = this.mFreeFormWindowPkg;
/* invoke-direct {p0, v0}, Lcom/android/server/audio/GameAudioEnhancer;->setParamSend(Ljava/lang/String;)V */
/* .line 311 */
return;
} // .end method
private void setParamSend ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 268 */
/* iget v0, p0, Lcom/android/server/audio/GameAudioEnhancer;->mGameModeSwitchStatus:I */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* .line 269 */
/* invoke-direct {p0, p1}, Lcom/android/server/audio/GameAudioEnhancer;->beOpened(Ljava/lang/String;)V */
/* .line 271 */
} // :cond_0
/* invoke-direct {p0, p1}, Lcom/android/server/audio/GameAudioEnhancer;->beClosed(Ljava/lang/String;)V */
/* .line 272 */
/* invoke-direct {p0}, Lcom/android/server/audio/GameAudioEnhancer;->unregisterForegroundObserver()V */
/* .line 274 */
} // :goto_0
return;
} // .end method
private void setWhiteList ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 300 */
v0 = this.mEnabledPackages;
/* .line 301 */
/* invoke-direct {p0}, Lcom/android/server/audio/GameAudioEnhancer;->initEnabledPackagesFromCloudSettings()Z */
/* .line 302 */
return;
} // .end method
private void unregisterForegroundObserver ( ) {
/* .locals 1 */
/* .line 99 */
v0 = this.mFreeformCallBack;
miui.app.MiuiFreeFormManager .unregisterFreeformCallback ( v0 );
/* .line 100 */
v0 = this.mForegroundInfoListener;
miui.process.ProcessManager .unregisterForegroundInfoListener ( v0 );
/* .line 101 */
return;
} // .end method
private void updateGameModeSettingstatus ( ) {
/* .locals 3 */
/* .line 130 */
try { // :try_start_0
v0 = this.mContentResolver;
final String v1 = "game_mode_enable"; // const-string v1, "game_mode_enable"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$Global .getInt ( v0,v1,v2 );
/* .line 131 */
/* .local v0, "gameModeEnable":I */
/* invoke-direct {p0, v0}, Lcom/android/server/audio/GameAudioEnhancer;->setGameModeEnabled(I)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 134 */
} // .end local v0 # "gameModeEnable":I
/* .line 132 */
/* :catch_0 */
/* move-exception v0 */
/* .line 133 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "updateGameModeSettingstatus: Exception " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "GameAudioEnhancer"; // const-string v2, "GameAudioEnhancer"
android.util.Log .e ( v2,v1 );
/* .line 135 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void updateWhiteList ( ) {
/* .locals 3 */
/* .line 139 */
try { // :try_start_0
v0 = this.mContentResolver;
final String v1 = "game_mode_packages"; // const-string v1, "game_mode_packages"
android.provider.Settings$Global .getString ( v0,v1 );
/* .line 140 */
/* .local v0, "gameModeEnablePkgs":Ljava/lang/String; */
/* invoke-direct {p0, v0}, Lcom/android/server/audio/GameAudioEnhancer;->setWhiteList(Ljava/lang/String;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 143 */
} // .end local v0 # "gameModeEnablePkgs":Ljava/lang/String;
/* .line 141 */
/* :catch_0 */
/* move-exception v0 */
/* .line 142 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "updateGameModeSettingstatus: Exception " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "GameAudioEnhancer"; // const-string v2, "GameAudioEnhancer"
android.util.Log .e ( v2,v1 );
/* .line 144 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
