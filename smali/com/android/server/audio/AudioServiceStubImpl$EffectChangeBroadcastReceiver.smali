.class Lcom/android/server/audio/AudioServiceStubImpl$EffectChangeBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AudioServiceStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/AudioServiceStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EffectChangeBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/audio/AudioServiceStubImpl;


# direct methods
.method private constructor <init>(Lcom/android/server/audio/AudioServiceStubImpl;)V
    .locals 0

    .line 276
    iput-object p1, p0, Lcom/android/server/audio/AudioServiceStubImpl$EffectChangeBroadcastReceiver;->this$0:Lcom/android/server/audio/AudioServiceStubImpl;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/audio/AudioServiceStubImpl;Lcom/android/server/audio/AudioServiceStubImpl$EffectChangeBroadcastReceiver-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/audio/AudioServiceStubImpl$EffectChangeBroadcastReceiver;-><init>(Lcom/android/server/audio/AudioServiceStubImpl;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 279
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 280
    .local v0, "action":Ljava/lang/String;
    const-string v1, "miui.intent.action.ACTION_AUDIO_EFFECT_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 281
    iget-object v1, p0, Lcom/android/server/audio/AudioServiceStubImpl$EffectChangeBroadcastReceiver;->this$0:Lcom/android/server/audio/AudioServiceStubImpl;

    invoke-static {v1}, Lcom/android/server/audio/AudioServiceStubImpl;->-$$Nest$msendEffectRefresh(Lcom/android/server/audio/AudioServiceStubImpl;)V

    .line 283
    :cond_0
    return-void
.end method
