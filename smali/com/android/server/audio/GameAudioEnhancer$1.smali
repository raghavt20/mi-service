.class Lcom/android/server/audio/GameAudioEnhancer$1;
.super Lmiui/app/IFreeformCallback$Stub;
.source "GameAudioEnhancer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/GameAudioEnhancer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/audio/GameAudioEnhancer;


# direct methods
.method constructor <init>(Lcom/android/server/audio/GameAudioEnhancer;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/audio/GameAudioEnhancer;

    .line 146
    iput-object p1, p0, Lcom/android/server/audio/GameAudioEnhancer$1;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    invoke-direct {p0}, Lmiui/app/IFreeformCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public dispatchFreeFormStackModeChanged(ILmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;)V
    .locals 6
    .param p1, "action"    # I
    .param p2, "stackInfo"    # Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;

    .line 150
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Enter dispatchFreeFormStackModeChanged action: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GameAudioEnhancer"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 153
    .local v0, "freeFormStackInfoList":Ljava/util/List;, "Ljava/util/List<Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;>;"
    const-string v1, ""

    .line 154
    .local v1, "freeFormPkg":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/audio/GameAudioEnhancer$1;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    const-string v3, ""

    invoke-static {v2, v3}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$fputmFreeFormWindowPkg(Lcom/android/server/audio/GameAudioEnhancer;Ljava/lang/String;)V

    .line 155
    iget-object v2, p0, Lcom/android/server/audio/GameAudioEnhancer$1;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    invoke-static {v2}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$fgetmGameModeSwitchStatus(Lcom/android/server/audio/GameAudioEnhancer;)I

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/server/audio/GameAudioEnhancer$1;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    invoke-static {v2}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$fgetmParamSet(Lcom/android/server/audio/GameAudioEnhancer;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 156
    return-void

    .line 158
    :cond_0
    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x3

    sparse-switch p1, :sswitch_data_0

    goto/16 :goto_4

    .line 202
    :sswitch_0
    iget-object v2, p0, Lcom/android/server/audio/GameAudioEnhancer$1;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    invoke-static {v2}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$fgetmCurForegroundPkg(Lcom/android/server/audio/GameAudioEnhancer;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$msetParamSend(Lcom/android/server/audio/GameAudioEnhancer;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 181
    :sswitch_1
    iget-object v5, p0, Lcom/android/server/audio/GameAudioEnhancer$1;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    invoke-static {v5}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$fgetmFreeWinVersion(Lcom/android/server/audio/GameAudioEnhancer;)I

    move-result v5

    if-ne v5, v4, :cond_4

    .line 184
    iget-object v3, p0, Lcom/android/server/audio/GameAudioEnhancer$1;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    invoke-static {v3}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$fgetmContext(Lcom/android/server/audio/GameAudioEnhancer;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getDisplay()Landroid/view/Display;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 185
    iget-object v2, p0, Lcom/android/server/audio/GameAudioEnhancer$1;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    invoke-static {v2}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$fgetmContext(Lcom/android/server/audio/GameAudioEnhancer;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getDisplayId()I

    move-result v2

    goto :goto_0

    .line 186
    :cond_1
    nop

    .line 183
    :goto_0
    invoke-static {v2}, Lmiui/app/MiuiFreeFormManager;->getAllFreeFormStackInfosOnDisplay(I)Ljava/util/List;

    move-result-object v0

    .line 188
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;

    .line 189
    .local v3, "miuiFreeFormStackInfo":Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;
    iget-object v4, p0, Lcom/android/server/audio/GameAudioEnhancer$1;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    iget-object v5, v3, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->packageName:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$misPackageEnabled(Lcom/android/server/audio/GameAudioEnhancer;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 190
    iget-object v4, p0, Lcom/android/server/audio/GameAudioEnhancer$1;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    iget-object v5, v3, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->packageName:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$fputmFreeFormWindowPkg(Lcom/android/server/audio/GameAudioEnhancer;Ljava/lang/String;)V

    .line 192
    .end local v3    # "miuiFreeFormStackInfo":Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;
    :cond_2
    goto :goto_1

    :cond_3
    goto/16 :goto_4

    .line 193
    :cond_4
    iget-object v2, p0, Lcom/android/server/audio/GameAudioEnhancer$1;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    invoke-static {v2}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$fgetmFreeWinVersion(Lcom/android/server/audio/GameAudioEnhancer;)I

    move-result v2

    if-ne v2, v3, :cond_8

    .line 194
    iget-object v2, p0, Lcom/android/server/audio/GameAudioEnhancer$1;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    invoke-static {v2}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$fgetmContext(Lcom/android/server/audio/GameAudioEnhancer;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lmiui/app/MiuiFreeFormManager;->getMiuiFreeformStackPackageName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 195
    iget-object v2, p0, Lcom/android/server/audio/GameAudioEnhancer$1;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    invoke-static {v2, v1}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$misPackageEnabled(Lcom/android/server/audio/GameAudioEnhancer;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 196
    iget-object v2, p0, Lcom/android/server/audio/GameAudioEnhancer$1;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    invoke-static {v2, v1}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$fputmFreeFormWindowPkg(Lcom/android/server/audio/GameAudioEnhancer;Ljava/lang/String;)V

    goto :goto_4

    .line 162
    :sswitch_2
    iget-object v5, p0, Lcom/android/server/audio/GameAudioEnhancer$1;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    invoke-static {v5}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$fgetmFreeWinVersion(Lcom/android/server/audio/GameAudioEnhancer;)I

    move-result v5

    if-ne v5, v4, :cond_7

    .line 165
    iget-object v3, p0, Lcom/android/server/audio/GameAudioEnhancer$1;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    invoke-static {v3}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$fgetmContext(Lcom/android/server/audio/GameAudioEnhancer;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getDisplay()Landroid/view/Display;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 166
    iget-object v2, p0, Lcom/android/server/audio/GameAudioEnhancer$1;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    invoke-static {v2}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$fgetmContext(Lcom/android/server/audio/GameAudioEnhancer;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getDisplayId()I

    move-result v2

    goto :goto_2

    .line 167
    :cond_5
    nop

    .line 164
    :goto_2
    invoke-static {v2}, Lmiui/app/MiuiFreeFormManager;->getAllFreeFormStackInfosOnDisplay(I)Ljava/util/List;

    move-result-object v0

    .line 169
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;

    .line 170
    .restart local v3    # "miuiFreeFormStackInfo":Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;
    iget-object v4, p0, Lcom/android/server/audio/GameAudioEnhancer$1;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    iget-object v5, v3, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->packageName:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$fputmFreeFormWindowPkg(Lcom/android/server/audio/GameAudioEnhancer;Ljava/lang/String;)V

    .line 171
    iget-object v4, p0, Lcom/android/server/audio/GameAudioEnhancer$1;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    iget-object v5, v3, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->packageName:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$msetParamSend(Lcom/android/server/audio/GameAudioEnhancer;Ljava/lang/String;)V

    .line 172
    .end local v3    # "miuiFreeFormStackInfo":Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;
    goto :goto_3

    :cond_6
    goto :goto_4

    .line 173
    :cond_7
    iget-object v2, p0, Lcom/android/server/audio/GameAudioEnhancer$1;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    invoke-static {v2}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$fgetmFreeWinVersion(Lcom/android/server/audio/GameAudioEnhancer;)I

    move-result v2

    if-ne v2, v3, :cond_8

    .line 174
    iget-object v2, p0, Lcom/android/server/audio/GameAudioEnhancer$1;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    invoke-static {v2}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$fgetmContext(Lcom/android/server/audio/GameAudioEnhancer;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lmiui/app/MiuiFreeFormManager;->getMiuiFreeformStackPackageName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 175
    iget-object v2, p0, Lcom/android/server/audio/GameAudioEnhancer$1;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    invoke-static {v2, v1}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$fputmFreeFormWindowPkg(Lcom/android/server/audio/GameAudioEnhancer;Ljava/lang/String;)V

    .line 176
    iget-object v2, p0, Lcom/android/server/audio/GameAudioEnhancer$1;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    invoke-static {v2, v1}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$msetParamSend(Lcom/android/server/audio/GameAudioEnhancer;Ljava/lang/String;)V

    .line 206
    :cond_8
    :goto_4
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x1 -> :sswitch_2
        0x2 -> :sswitch_1
        0x3 -> :sswitch_0
        0x4 -> :sswitch_1
        0x5 -> :sswitch_0
        0x15 -> :sswitch_2
    .end sparse-switch
.end method
