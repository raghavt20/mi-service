class com.android.server.audio.AudioGameEffect$GameEffectThread extends java.lang.Thread {
	 /* .source "AudioGameEffect.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/audio/AudioGameEffect; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "GameEffectThread" */
} // .end annotation
/* # instance fields */
final com.android.server.audio.AudioGameEffect this$0; //synthetic
/* # direct methods */
 com.android.server.audio.AudioGameEffect$GameEffectThread ( ) {
/* .locals 0 */
/* .line 164 */
this.this$0 = p1;
/* .line 165 */
final String p1 = "GameEffectThread"; // const-string p1, "GameEffectThread"
/* invoke-direct {p0, p1}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V */
/* .line 166 */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 3 */
/* .line 170 */
/* invoke-super {p0}, Ljava/lang/Thread;->run()V */
/* .line 171 */
android.os.Looper .prepare ( );
/* .line 172 */
v0 = this.this$0;
/* new-instance v1, Lcom/android/server/audio/AudioGameEffect$GameEffectHandler; */
v2 = this.this$0;
/* invoke-direct {v1, v2}, Lcom/android/server/audio/AudioGameEffect$GameEffectHandler;-><init>(Lcom/android/server/audio/AudioGameEffect;)V */
com.android.server.audio.AudioGameEffect .-$$Nest$fputmHandler ( v0,v1 );
/* .line 173 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.android.server.audio.AudioGameEffect .-$$Nest$fputmQueryPMServiceTime ( v0,v1 );
/* .line 174 */
v0 = this.this$0;
com.android.server.audio.AudioGameEffect .-$$Nest$minitProcessListenerAsync ( v0 );
/* .line 175 */
android.os.Looper .loop ( );
/* .line 177 */
return;
} // .end method
