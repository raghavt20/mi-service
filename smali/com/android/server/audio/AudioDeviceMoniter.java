public class com.android.server.audio.AudioDeviceMoniter {
	 /* .source "AudioDeviceMoniter.java" */
	 /* # static fields */
	 private static final java.lang.String ACTION_DISABLE_NFC_POLLING;
	 private static final java.lang.String ACTION_ENABLE_NFC_POLLING;
	 public static final Integer AUDIO_STATE_OFF;
	 public static final Integer AUDIO_STATE_ON;
	 private static final java.lang.String PROPERTY_MIC_STATUS;
	 private static final java.lang.String PROPERTY_RCV_STATUS;
	 private static final java.lang.String TAG;
	 private static volatile com.android.server.audio.AudioDeviceMoniter sInstance;
	 /* # instance fields */
	 private android.content.Intent mAction;
	 private android.content.Context mContext;
	 private Integer mCurrentNfcPollState;
	 /* # direct methods */
	 static void -$$Nest$mpollMicAndRcv ( com.android.server.audio.AudioDeviceMoniter p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0}, Lcom/android/server/audio/AudioDeviceMoniter;->pollMicAndRcv()V */
		 return;
	 } // .end method
	 private com.android.server.audio.AudioDeviceMoniter ( ) {
		 /* .locals 2 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 45 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 46 */
		 final String v0 = "AudioDeviceMoniter init..."; // const-string v0, "AudioDeviceMoniter init..."
		 final String v1 = "AudioDeviceMoniter"; // const-string v1, "AudioDeviceMoniter"
		 android.util.Log .d ( v1,v0 );
		 /* .line 47 */
		 this.mContext = p1;
		 /* .line 48 */
		 final String v0 = "AudioDeviceMoniter init done"; // const-string v0, "AudioDeviceMoniter init done"
		 android.util.Log .d ( v1,v0 );
		 /* .line 49 */
		 return;
	 } // .end method
	 public static com.android.server.audio.AudioDeviceMoniter getInstance ( android.content.Context p0 ) {
		 /* .locals 1 */
		 /* .param p0, "context" # Landroid/content/Context; */
		 /* .line 55 */
		 v0 = com.android.server.audio.AudioDeviceMoniter.sInstance;
		 /* if-nez v0, :cond_0 */
		 /* .line 56 */
		 /* new-instance v0, Lcom/android/server/audio/AudioDeviceMoniter; */
		 /* invoke-direct {v0, p0}, Lcom/android/server/audio/AudioDeviceMoniter;-><init>(Landroid/content/Context;)V */
		 /* .line 58 */
	 } // :cond_0
	 v0 = com.android.server.audio.AudioDeviceMoniter.sInstance;
} // .end method
private void pollMicAndRcv ( ) {
	 /* .locals 10 */
	 /* .line 76 */
	 /* nop */
} // :goto_0
/* const-string/jumbo v0, "vendor.audio.mic.status" */
android.os.SystemProperties .get ( v0 );
/* .line 77 */
/* .local v0, "AudioMicState":Ljava/lang/String; */
/* const-string/jumbo v1, "vendor.audio.receiver.status" */
android.os.SystemProperties .get ( v1 );
/* .line 78 */
/* .local v1, "AudioRcvState":Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* .line 79 */
/* .local v2, "micState":I */
int v3 = 0; // const/4 v3, 0x0
/* .line 80 */
/* .local v3, "rcvState":I */
int v4 = 0; // const/4 v4, 0x0
/* .line 81 */
/* .local v4, "isMicStatusInvalid":Z */
int v5 = 0; // const/4 v5, 0x0
/* .line 83 */
/* .local v5, "isRcvStatusInvalid":Z */
final String v6 = "on"; // const-string v6, "on"
v7 = (( java.lang.String ) v0 ).equals ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
final String v8 = "off"; // const-string v8, "off"
if ( v7 != null) { // if-eqz v7, :cond_0
	 /* .line 84 */
	 int v2 = 1; // const/4 v2, 0x1
	 /* .line 85 */
} // :cond_0
v7 = (( java.lang.String ) v0 ).equals ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v7 != null) { // if-eqz v7, :cond_1
	 /* .line 86 */
	 int v2 = 0; // const/4 v2, 0x0
	 /* .line 88 */
} // :cond_1
int v4 = 1; // const/4 v4, 0x1
/* .line 91 */
} // :goto_1
v6 = (( java.lang.String ) v1 ).equals ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 92 */
int v3 = 1; // const/4 v3, 0x1
/* .line 93 */
} // :cond_2
v6 = (( java.lang.String ) v1 ).equals ( v8 ); // invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 94 */
int v3 = 0; // const/4 v3, 0x0
/* .line 96 */
} // :cond_3
int v5 = 1; // const/4 v5, 0x1
/* .line 99 */
} // :goto_2
final String v6 = "AudioDeviceMoniter"; // const-string v6, "AudioDeviceMoniter"
if ( v4 != null) { // if-eqz v4, :cond_4
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 100 */
/* const-string/jumbo v7, "unexpected value for AudioRcvState and AudioMicState." */
android.util.Log .w ( v6,v7 );
/* .line 101 */
/* nop */
/* .line 118 */
} // .end local v0 # "AudioMicState":Ljava/lang/String;
} // .end local v1 # "AudioRcvState":Ljava/lang/String;
} // .end local v2 # "micState":I
} // .end local v3 # "rcvState":I
} // .end local v4 # "isMicStatusInvalid":Z
} // .end local v5 # "isRcvStatusInvalid":Z
return;
/* .line 104 */
/* .restart local v0 # "AudioMicState":Ljava/lang/String; */
/* .restart local v1 # "AudioRcvState":Ljava/lang/String; */
/* .restart local v2 # "micState":I */
/* .restart local v3 # "rcvState":I */
/* .restart local v4 # "isMicStatusInvalid":Z */
/* .restart local v5 # "isRcvStatusInvalid":Z */
} // :cond_4
/* or-int v7, v3, v2 */
/* .line 105 */
/* .local v7, "mPollState":I */
/* iget v8, p0, Lcom/android/server/audio/AudioDeviceMoniter;->mCurrentNfcPollState:I */
/* if-eq v7, v8, :cond_6 */
/* .line 106 */
if ( v7 != null) { // if-eqz v7, :cond_5
/* .line 107 */
/* new-instance v8, Landroid/content/Intent; */
final String v9 = "com.android.nfc.action.DISABLE_POLLING"; // const-string v9, "com.android.nfc.action.DISABLE_POLLING"
/* invoke-direct {v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
this.mAction = v8;
/* .line 109 */
} // :cond_5
/* new-instance v8, Landroid/content/Intent; */
final String v9 = "com.android.nfc.action.ENABLE_POLLING"; // const-string v9, "com.android.nfc.action.ENABLE_POLLING"
/* invoke-direct {v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
this.mAction = v8;
/* .line 111 */
} // :goto_3
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "nfc status be changed to "; // const-string v9, "nfc status be changed to "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = ", sent broadcast to nfc..."; // const-string v9, ", sent broadcast to nfc..."
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v6,v8 );
/* .line 112 */
v6 = this.mContext;
v8 = this.mAction;
v9 = android.os.UserHandle.ALL;
(( android.content.Context ) v6 ).sendBroadcastAsUser ( v8, v9 ); // invoke-virtual {v6, v8, v9}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 113 */
/* iput v7, p0, Lcom/android/server/audio/AudioDeviceMoniter;->mCurrentNfcPollState:I */
/* .line 116 */
} // :cond_6
/* const-wide/16 v8, 0x1f4 */
android.os.SystemClock .sleep ( v8,v9 );
/* .line 117 */
} // .end local v0 # "AudioMicState":Ljava/lang/String;
} // .end local v1 # "AudioRcvState":Ljava/lang/String;
} // .end local v2 # "micState":I
} // .end local v3 # "rcvState":I
} // .end local v4 # "isMicStatusInvalid":Z
} // .end local v5 # "isRcvStatusInvalid":Z
} // .end local v7 # "mPollState":I
/* goto/16 :goto_0 */
} // .end method
/* # virtual methods */
public final void startPollAudioMicStatus ( ) {
/* .locals 1 */
/* .line 65 */
/* new-instance v0, Lcom/android/server/audio/AudioDeviceMoniter$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/audio/AudioDeviceMoniter$1;-><init>(Lcom/android/server/audio/AudioDeviceMoniter;)V */
/* .line 71 */
/* .local v0, "MicAndRcvPollThread":Ljava/lang/Thread; */
(( java.lang.Thread ) v0 ).start ( ); // invoke-virtual {v0}, Ljava/lang/Thread;->start()V
/* .line 72 */
return;
} // .end method
