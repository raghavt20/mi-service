.class Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;
.super Landroid/os/Handler;
.source "GameAudioEnhancer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/GameAudioEnhancer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WorkHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/audio/GameAudioEnhancer;


# direct methods
.method public constructor <init>(Lcom/android/server/audio/GameAudioEnhancer;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 227
    iput-object p1, p0, Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    .line 228
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 229
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .line 233
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 250
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$msetGameMode(Lcom/android/server/audio/GameAudioEnhancer;Z)V

    .line 251
    goto :goto_0

    .line 247
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$msetGameMode(Lcom/android/server/audio/GameAudioEnhancer;Z)V

    .line 248
    goto :goto_0

    .line 235
    :pswitch_2
    const-string v0, "ProcessManager"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    invoke-static {v0}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$fgetmForegroundInfoListener(Lcom/android/server/audio/GameAudioEnhancer;)Lmiui/process/IForegroundInfoListener$Stub;

    move-result-object v0

    invoke-static {v0}, Lmiui/process/ProcessManager;->registerForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V

    goto :goto_0

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    invoke-static {v0}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$fgetmQueryPMServiceTime(Lcom/android/server/audio/GameAudioEnhancer;)I

    move-result v1

    add-int/lit8 v2, v1, 0x1

    invoke-static {v0, v2}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$fputmQueryPMServiceTime(Lcom/android/server/audio/GameAudioEnhancer;I)V

    const/16 v0, 0x14

    const-string v2, "GameAudioEnhancer"

    if-ge v1, v0, :cond_1

    .line 239
    const-string v0, "process manager service not published, wait 1 second"

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    iget-object v0, p0, Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    invoke-static {v0}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$minitProcessListenerAsync(Lcom/android/server/audio/GameAudioEnhancer;)V

    goto :goto_0

    .line 242
    :cond_1
    const-string v0, "failed to get ProcessManager service"

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    nop

    .line 255
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
