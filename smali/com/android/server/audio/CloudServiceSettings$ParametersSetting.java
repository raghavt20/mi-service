class com.android.server.audio.CloudServiceSettings$ParametersSetting extends com.android.server.audio.CloudServiceSettings$Setting {
	 /* .source "CloudServiceSettings.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/audio/CloudServiceSettings; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "ParametersSetting" */
} // .end annotation
/* # static fields */
private static final java.lang.String KVPAIRS;
/* # instance fields */
private java.lang.StringBuilder mKvpairs;
final com.android.server.audio.CloudServiceSettings this$0; //synthetic
/* # direct methods */
 com.android.server.audio.CloudServiceSettings$ParametersSetting ( ) {
/* .locals 3 */
/* .param p1, "this$0" # Lcom/android/server/audio/CloudServiceSettings; */
/* .param p2, "json" # Lorg/json/JSONObject; */
/* .line 542 */
this.this$0 = p1;
/* .line 543 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/audio/CloudServiceSettings$Setting;-><init>(Lcom/android/server/audio/CloudServiceSettings;Lorg/json/JSONObject;)V */
/* .line 545 */
try { // :try_start_0
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 final String v1 = "kvpairs"; // const-string v1, "kvpairs"
	 (( org.json.JSONObject ) p2 ).getString ( v1 ); // invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
	 /* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
	 this.mKvpairs = v0;
	 /* .line 546 */
	 /* iget-boolean v0, p0, Lcom/android/server/audio/CloudServiceSettings$ParametersSetting;->mSecreted:Z */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 547 */
		 v0 = this.mSecretedItem;
		 v1 = this.mKvpairs;
		 (( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 551 */
	 } // :cond_0
	 /* .line 549 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 550 */
	 /* .local v0, "e":Ljava/lang/Exception; */
	 final String v1 = "CloudServiceSettings"; // const-string v1, "CloudServiceSettings"
	 final String v2 = "fail to parse ParametersSetting"; // const-string v2, "fail to parse ParametersSetting"
	 android.util.Log .e ( v1,v2 );
	 /* .line 552 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
/* # virtual methods */
public Boolean set ( ) {
/* .locals 2 */
/* .line 556 */
v0 = /* invoke-super {p0}, Lcom/android/server/audio/CloudServiceSettings$Setting;->set()Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 557 */
/* .line 559 */
} // :cond_0
v0 = this.mKvpairs;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v0 = android.media.AudioSystem .setParameters ( v0 );
/* if-nez v0, :cond_1 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
/* iput-boolean v1, p0, Lcom/android/server/audio/CloudServiceSettings$ParametersSetting;->mSuccess:Z */
/* .line 560 */
/* iget-boolean v0, p0, Lcom/android/server/audio/CloudServiceSettings$ParametersSetting;->mSuccess:Z */
} // .end method
public Boolean updateTo ( com.android.server.audio.CloudServiceSettings$Setting p0 ) {
/* .locals 2 */
/* .param p1, "s" # Lcom/android/server/audio/CloudServiceSettings$Setting; */
/* .line 565 */
v0 = /* invoke-super {p0, p1}, Lcom/android/server/audio/CloudServiceSettings$Setting;->updateTo(Lcom/android/server/audio/CloudServiceSettings$Setting;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* instance-of v0, p1, Lcom/android/server/audio/CloudServiceSettings$ParametersSetting; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 566 */
/* move-object v0, p1 */
/* check-cast v0, Lcom/android/server/audio/CloudServiceSettings$ParametersSetting; */
/* .line 567 */
/* .local v0, "ps":Lcom/android/server/audio/CloudServiceSettings$ParametersSetting; */
v1 = this.mKvpairs;
this.mKvpairs = v1;
/* .line 568 */
int v1 = 1; // const/4 v1, 0x1
/* .line 570 */
} // .end local v0 # "ps":Lcom/android/server/audio/CloudServiceSettings$ParametersSetting;
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "fail to update ParametersSetting: "; // const-string v1, "fail to update ParametersSetting: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mSettingName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "CloudServiceSettings"; // const-string v1, "CloudServiceSettings"
android.util.Log .e ( v1,v0 );
/* .line 571 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
