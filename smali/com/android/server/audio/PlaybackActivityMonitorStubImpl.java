public class com.android.server.audio.PlaybackActivityMonitorStubImpl implements com.android.server.audio.PlaybackActivityMonitorStub {
	 /* .source "PlaybackActivityMonitorStubImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String APPS_VOLUME_AUTHORITY;
	 private static final android.net.Uri APPS_VOLUME_CONTENT_URI;
	 private static final java.lang.String APPS_VOLUME_PATH;
	 private static final android.net.Uri APPS_VOLUME_URI;
	 private static final Integer LOAD_APPS_VOLUME_DELAY;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private Boolean mIsForceIgnoreGranted;
	 private final java.util.HashMap mMusicVolumeMap;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/HashMap<", */
	 /* "Ljava/lang/String;", */
	 /* "Ljava/lang/Float;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private Boolean mOpenSoundAssist;
private Integer mRouteCastUID;
private java.lang.Object objKeyLock;
/* # direct methods */
static com.android.server.audio.PlaybackActivityMonitorStubImpl ( ) {
/* .locals 2 */
/* .line 35 */
/* nop */
/* .line 36 */
final String v0 = "content://com.miui.misound.AppVolumeProvider"; // const-string v0, "content://com.miui.misound.AppVolumeProvider"
android.net.Uri .parse ( v0 );
/* .line 38 */
(( android.net.Uri ) v0 ).buildUpon ( ); // invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;
/* .line 39 */
final String v1 = "assistant"; // const-string v1, "assistant"
(( android.net.Uri$Builder ) v0 ).appendPath ( v1 ); // invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;
(( android.net.Uri$Builder ) v0 ).build ( ); // invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;
/* .line 38 */
return;
} // .end method
public com.android.server.audio.PlaybackActivityMonitorStubImpl ( ) {
/* .locals 2 */
/* .line 29 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 31 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mOpenSoundAssist:Z */
/* .line 32 */
/* new-instance v1, Ljava/lang/Object; */
/* invoke-direct {v1}, Ljava/lang/Object;-><init>()V */
this.objKeyLock = v1;
/* .line 33 */
/* iput-boolean v0, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mIsForceIgnoreGranted:Z */
/* .line 41 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mMusicVolumeMap = v0;
/* .line 42 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mRouteCastUID:I */
return;
} // .end method
private java.lang.String getPkgName ( android.media.AudioPlaybackConfiguration p0, android.content.Context p1 ) {
/* .locals 2 */
/* .param p1, "apc" # Landroid/media/AudioPlaybackConfiguration; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 127 */
if ( p1 != null) { // if-eqz p1, :cond_1
	 /* if-nez p2, :cond_0 */
	 /* .line 130 */
} // :cond_0
(( android.content.Context ) p2 ).getPackageManager ( ); // invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 131 */
/* .local v0, "pm":Landroid/content/pm/PackageManager; */
v1 = (( android.media.AudioPlaybackConfiguration ) p1 ).getClientUid ( ); // invoke-virtual {p1}, Landroid/media/AudioPlaybackConfiguration;->getClientUid()I
(( android.content.pm.PackageManager ) v0 ).getNameForUid ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;
/* .line 128 */
} // .end local v0 # "pm":Landroid/content/pm/PackageManager;
} // :cond_1
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void initPlayerVolume ( java.lang.String p0, Float p1 ) {
/* .locals 3 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "volume" # F */
/* .line 110 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 111 */
v0 = this.mMusicVolumeMap;
/* monitor-enter v0 */
/* .line 112 */
try { // :try_start_0
v1 = this.mMusicVolumeMap;
java.lang.Float .valueOf ( p2 );
(( java.util.HashMap ) v1 ).put ( p1, v2 ); // invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 113 */
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 115 */
} // :cond_0
} // :goto_0
return;
} // .end method
private Boolean isMusicPlayerActive ( android.media.AudioPlaybackConfiguration p0 ) {
/* .locals 4 */
/* .param p1, "apc" # Landroid/media/AudioPlaybackConfiguration; */
/* .line 118 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 119 */
} // :cond_0
(( android.media.AudioPlaybackConfiguration ) p1 ).getAudioAttributes ( ); // invoke-virtual {p1}, Landroid/media/AudioPlaybackConfiguration;->getAudioAttributes()Landroid/media/AudioAttributes;
v1 = (( android.media.AudioAttributes ) v1 ).getUsage ( ); // invoke-virtual {v1}, Landroid/media/AudioAttributes;->getUsage()I
int v2 = 1; // const/4 v2, 0x1
/* if-eq v1, v2, :cond_2 */
/* .line 120 */
(( android.media.AudioPlaybackConfiguration ) p1 ).getAudioAttributes ( ); // invoke-virtual {p1}, Landroid/media/AudioPlaybackConfiguration;->getAudioAttributes()Landroid/media/AudioAttributes;
v1 = (( android.media.AudioAttributes ) v1 ).getVolumeControlStream ( ); // invoke-virtual {v1}, Landroid/media/AudioAttributes;->getVolumeControlStream()I
int v3 = 3; // const/4 v3, 0x3
/* if-ne v1, v3, :cond_1 */
/* .line 123 */
} // :cond_1
/* .line 121 */
} // :cond_2
} // :goto_0
} // .end method
/* # virtual methods */
public void dumpPlayersVolume ( java.io.PrintWriter p0 ) {
/* .locals 7 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 261 */
final String v0 = "\n volume players:"; // const-string v0, "\n volume players:"
(( java.io.PrintWriter ) p1 ).print ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 262 */
(( com.android.server.audio.PlaybackActivityMonitorStubImpl ) p0 ).getPkgVolumes ( ); // invoke-virtual {p0}, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->getPkgVolumes()Ljava/util/HashMap;
/* .line 263 */
/* .local v0, "volumeMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Float;>;" */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 264 */
/* new-instance v1, Ljava/util/ArrayList; */
(( java.util.HashMap ) v0 ).keySet ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;
/* invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 265 */
/* .local v1, "pkgList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_0
/* check-cast v3, Ljava/lang/String; */
/* .line 266 */
/* .local v3, "pkg":Ljava/lang/String; */
(( java.util.HashMap ) v0 ).get ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v4, Ljava/lang/Float; */
v4 = (( java.lang.Float ) v4 ).floatValue ( ); // invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F
/* .line 267 */
/* .local v4, "playerVolume":F */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "\n pkg="; // const-string v6, "\n pkg="
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " volume="; // const-string v6, " volume="
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v5 ); // invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 268 */
} // .end local v3 # "pkg":Ljava/lang/String;
} // .end local v4 # "playerVolume":F
/* .line 269 */
} // :cond_0
final String v2 = "\n"; // const-string v2, "\n"
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 271 */
} // .end local v1 # "pkgList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_1
return;
} // .end method
public java.util.HashMap getPkgVolumes ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Float;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 104 */
v0 = this.mMusicVolumeMap;
/* monitor-enter v0 */
/* .line 105 */
try { // :try_start_0
v1 = this.mMusicVolumeMap;
/* monitor-exit v0 */
/* .line 106 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Float getPlayerVolume ( android.content.Context p0, android.media.AudioPlaybackConfiguration p1 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "apc" # Landroid/media/AudioPlaybackConfiguration; */
/* .line 92 */
/* iget-boolean v0, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mOpenSoundAssist:Z */
/* const/high16 v1, 0x3f800000 # 1.0f */
if ( v0 != null) { // if-eqz v0, :cond_1
if ( p1 != null) { // if-eqz p1, :cond_1
/* if-nez p2, :cond_0 */
/* .line 95 */
} // :cond_0
(( android.content.Context ) p1 ).getPackageManager ( ); // invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 96 */
/* .local v0, "pm":Landroid/content/pm/PackageManager; */
v2 = (( android.media.AudioPlaybackConfiguration ) p2 ).getClientUid ( ); // invoke-virtual {p2}, Landroid/media/AudioPlaybackConfiguration;->getClientUid()I
(( android.content.pm.PackageManager ) v0 ).getNameForUid ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;
/* .line 97 */
/* .local v2, "pkg":Ljava/lang/String; */
v3 = this.mMusicVolumeMap;
/* monitor-enter v3 */
/* .line 98 */
try { // :try_start_0
v4 = this.mMusicVolumeMap;
java.lang.Float .valueOf ( v1 );
(( java.util.HashMap ) v4 ).getOrDefault ( v2, v1 ); // invoke-virtual {v4, v2, v1}, Ljava/util/HashMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
/* monitor-exit v3 */
/* .line 99 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v3 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 93 */
} // .end local v0 # "pm":Landroid/content/pm/PackageManager;
} // .end local v2 # "pkg":Ljava/lang/String;
} // :cond_1
} // :goto_0
} // .end method
public Float getPlayerVolume ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 79 */
/* iget-boolean v0, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mOpenSoundAssist:Z */
/* const/high16 v1, 0x3f800000 # 1.0f */
/* if-nez v0, :cond_0 */
/* .line 80 */
/* .line 82 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* .line 83 */
/* .line 85 */
} // :cond_1
v0 = this.mMusicVolumeMap;
/* monitor-enter v0 */
/* .line 86 */
try { // :try_start_0
v2 = this.mMusicVolumeMap;
java.lang.Float .valueOf ( v1 );
(( java.util.HashMap ) v2 ).getOrDefault ( p1, v1 ); // invoke-virtual {v2, p1, v1}, Ljava/util/HashMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
/* monitor-exit v0 */
/* .line 87 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Integer getRouteCastUID ( ) {
/* .locals 1 */
/* .line 319 */
/* iget v0, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mRouteCastUID:I */
} // .end method
public Boolean ignoreFocusRequest ( com.android.server.audio.FocusRequester p0, Integer p1 ) {
/* .locals 7 */
/* .param p1, "frWinner" # Lcom/android/server/audio/FocusRequester; */
/* .param p2, "uid" # I */
/* .line 324 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 325 */
/* .line 328 */
} // :cond_0
v1 = (( com.android.server.audio.PlaybackActivityMonitorStubImpl ) p0 ).interruptMusicPlayback ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->interruptMusicPlayback(I)Z
int v2 = 1; // const/4 v2, 0x1
/* if-nez v1, :cond_1 */
/* .line 329 */
/* .line 332 */
} // :cond_1
(( com.android.server.audio.FocusRequester ) p1 ).getAudioAttributes ( ); // invoke-virtual {p1}, Lcom/android/server/audio/FocusRequester;->getAudioAttributes()Landroid/media/AudioAttributes;
v1 = (( android.media.AudioAttributes ) v1 ).getUsage ( ); // invoke-virtual {v1}, Landroid/media/AudioAttributes;->getUsage()I
/* .line 333 */
/* .local v1, "usage":I */
/* if-eq v1, v2, :cond_3 */
int v3 = 5; // const/4 v3, 0x5
/* if-ne v1, v3, :cond_2 */
} // :cond_2
/* move v3, v0 */
} // :cond_3
} // :goto_0
/* move v3, v2 */
/* .line 337 */
/* .local v3, "usageMatched":Z */
} // :goto_1
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " mRouteCast: "; // const-string v5, " mRouteCast: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v5, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mRouteCastUID:I */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = " uid: "; // const-string v5, " uid: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p2 ); // invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "PlaybackActivityMonitorStubImpl"; // const-string v5, "PlaybackActivityMonitorStubImpl"
android.util.Log .d ( v5,v4 );
/* .line 338 */
int v4 = 0; // const/4 v4, 0x0
/* .line 339 */
/* .local v4, "openSmallWindowMediaProjection":Z */
v5 = (( com.android.server.audio.FocusRequester ) p1 ).getClientUid ( ); // invoke-virtual {p1}, Lcom/android/server/audio/FocusRequester;->getClientUid()I
/* iget v6, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mRouteCastUID:I */
/* if-eq v5, v6, :cond_4 */
/* if-ne v6, p2, :cond_7 */
/* .line 340 */
} // :cond_4
/* if-nez v3, :cond_6 */
int v5 = 4; // const/4 v5, 0x4
/* if-ne v1, v5, :cond_5 */
} // :cond_5
/* move v5, v0 */
} // :cond_6
} // :goto_2
/* move v5, v2 */
} // :goto_3
/* move v3, v5 */
/* .line 341 */
int v4 = 1; // const/4 v4, 0x1
/* .line 344 */
} // :cond_7
if ( v3 != null) { // if-eqz v3, :cond_9
/* iget-boolean v5, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mIsForceIgnoreGranted:Z */
/* if-nez v5, :cond_8 */
if ( v4 != null) { // if-eqz v4, :cond_9
} // :cond_8
/* move v0, v2 */
} // :cond_9
} // .end method
public Boolean interruptMusicPlayback ( Integer p0 ) {
/* .locals 8 */
/* .param p1, "uid" # I */
/* .line 349 */
final String v0 = "audio"; // const-string v0, "audio"
android.os.ServiceManager .getService ( v0 );
/* .line 350 */
/* .local v0, "b":Landroid/os/IBinder; */
android.media.IAudioService$Stub .asInterface ( v0 );
/* .line 351 */
/* .local v1, "audioService":Landroid/media/IAudioService; */
int v2 = 0; // const/4 v2, 0x0
/* .line 353 */
/* .local v2, "uidArray":[I */
try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v2, v3 */
/* .line 356 */
/* nop */
/* .line 359 */
int v3 = 0; // const/4 v3, 0x0
/* .line 360 */
/* .local v3, "hasUid":Z */
final String v4 = "PlaybackActivityMonitorStubImpl"; // const-string v4, "PlaybackActivityMonitorStubImpl"
if ( v2 != null) { // if-eqz v2, :cond_1
/* array-length v5, v2 */
/* if-lez v5, :cond_1 */
/* .line 361 */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "i":I */
} // :goto_0
/* array-length v6, v2 */
/* if-ge v5, v6, :cond_1 */
/* .line 362 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "getAudioPolicyMatchUids="; // const-string v7, "getAudioPolicyMatchUids="
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget v7, v2, v5 */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v4,v6 );
/* .line 363 */
/* aget v6, v2, v5 */
/* if-ne v6, p1, :cond_0 */
/* .line 364 */
int v3 = 1; // const/4 v3, 0x1
/* .line 365 */
/* .line 361 */
} // :cond_0
/* add-int/lit8 v5, v5, 0x1 */
/* .line 369 */
} // .end local v5 # "i":I
} // :cond_1
} // :goto_1
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "interruptMusicPlayback uid="; // const-string v6, "interruptMusicPlayback uid="
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = " hasUid="; // const-string v6, " hasUid="
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v4,v5 );
/* .line 370 */
/* xor-int/lit8 v4, v3, 0x1 */
/* .line 354 */
} // .end local v3 # "hasUid":Z
/* :catch_0 */
/* move-exception v3 */
/* .line 355 */
/* .local v3, "e":Landroid/os/RemoteException; */
int v4 = 1; // const/4 v4, 0x1
} // .end method
public Boolean isForceIgnoreGranted ( ) {
/* .locals 1 */
/* .line 69 */
/* iget-boolean v0, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mIsForceIgnoreGranted:Z */
} // .end method
public Boolean isSoundAssistOpen ( ) {
/* .locals 2 */
/* .line 46 */
v0 = this.objKeyLock;
/* monitor-enter v0 */
/* .line 47 */
try { // :try_start_0
/* iget-boolean v1, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mOpenSoundAssist:Z */
/* monitor-exit v0 */
/* .line 48 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean loadAppsVolume ( android.content.ContentResolver p0 ) {
/* .locals 9 */
/* .param p1, "cr" # Landroid/content/ContentResolver; */
/* .line 158 */
final String v0 = "PlaybackActivityMonitorStubImpl"; // const-string v0, "PlaybackActivityMonitorStubImpl"
int v1 = 0; // const/4 v1, 0x0
/* .line 159 */
/* .local v1, "cs":Landroid/database/Cursor; */
int v2 = 0; // const/4 v2, 0x0
/* if-nez p1, :cond_0 */
/* .line 160 */
/* .line 163 */
} // :cond_0
try { // :try_start_0
v4 = com.android.server.audio.PlaybackActivityMonitorStubImpl.APPS_VOLUME_URI;
int v5 = 0; // const/4 v5, 0x0
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
int v8 = 0; // const/4 v8, 0x0
/* move-object v3, p1 */
/* invoke-virtual/range {v3 ..v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor; */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v1, v3 */
/* .line 167 */
/* .line 165 */
/* :catch_0 */
/* move-exception v3 */
/* .line 166 */
/* .local v3, "e":Ljava/lang/Exception; */
final String v4 = "loadAppsVolume: query exception"; // const-string v4, "loadAppsVolume: query exception"
android.util.Log .e ( v0,v4,v3 );
/* .line 169 */
} // .end local v3 # "e":Ljava/lang/Exception;
} // :goto_0
/* if-nez v1, :cond_1 */
/* .line 170 */
/* .line 173 */
} // :cond_1
} // :goto_1
v3 = try { // :try_start_1
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 174 */
v3 = final String v3 = "_package_name"; // const-string v3, "_package_name"
/* .line 175 */
/* .local v3, "pkgName":Ljava/lang/String; */
v4 = v4 = final String v4 = "_volume"; // const-string v4, "_volume"
/* int-to-float v4, v4 */
/* const/high16 v5, 0x42c80000 # 100.0f */
/* div-float/2addr v4, v5 */
/* .line 176 */
/* .local v4, "volume":F */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "loadAppsVolume pkgName "; // const-string v6, "loadAppsVolume pkgName "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " volume="; // const-string v6, " volume="
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v5 );
/* .line 177 */
/* invoke-direct {p0, v3, v4}, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->initPlayerVolume(Ljava/lang/String;F)V */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 178 */
} // .end local v3 # "pkgName":Ljava/lang/String;
} // .end local v4 # "volume":F
/* .line 179 */
} // :cond_2
/* nop */
/* .line 183 */
/* .line 179 */
int v0 = 1; // const/4 v0, 0x1
/* .line 183 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 180 */
/* :catch_1 */
/* move-exception v3 */
/* .line 181 */
/* .local v3, "e":Ljava/lang/Exception; */
try { // :try_start_2
final String v4 = "loadAppsVolume: init player Volume error"; // const-string v4, "loadAppsVolume: init player Volume error"
android.util.Log .e ( v0,v4,v3 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 183 */
/* nop */
} // .end local v3 # "e":Ljava/lang/Exception;
/* .line 184 */
/* nop */
/* .line 186 */
/* .line 183 */
} // :goto_2
/* .line 184 */
/* throw v0 */
} // .end method
public void loadSoundAssistSettings ( android.content.ContentResolver p0 ) {
/* .locals 3 */
/* .param p1, "cr" # Landroid/content/ContentResolver; */
/* .line 288 */
/* if-nez p1, :cond_0 */
/* .line 289 */
return;
/* .line 291 */
} // :cond_0
/* const-string/jumbo v0, "sound_assist_key" */
int v1 = 0; // const/4 v1, 0x0
v0 = android.provider.Settings$Global .getInt ( p1,v0,v1 );
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_1 */
/* move v0, v2 */
} // :cond_1
/* move v0, v1 */
} // :goto_0
/* iput-boolean v0, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mOpenSoundAssist:Z */
/* .line 293 */
final String v0 = "key_ignore_music_focus_req"; // const-string v0, "key_ignore_music_focus_req"
v0 = android.provider.Settings$Global .getInt ( p1,v0,v1 );
/* if-ne v0, v2, :cond_2 */
/* move v1, v2 */
} // :cond_2
/* iput-boolean v1, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mIsForceIgnoreGranted:Z */
/* .line 295 */
return;
} // .end method
public void resetPlayerVolume ( android.content.Context p0 ) {
/* .locals 8 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 136 */
/* if-nez p1, :cond_0 */
/* .line 137 */
return;
/* .line 139 */
} // :cond_0
final String v0 = "audio"; // const-string v0, "audio"
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/media/AudioManager; */
/* .line 140 */
/* .local v0, "am":Landroid/media/AudioManager; */
(( android.media.AudioManager ) v0 ).getActivePlaybackConfigurations ( ); // invoke-virtual {v0}, Landroid/media/AudioManager;->getActivePlaybackConfigurations()Ljava/util/List;
/* .line 141 */
/* .local v1, "apcList":Ljava/util/List;, "Ljava/util/List<Landroid/media/AudioPlaybackConfiguration;>;" */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "resetPlayerVolume size="; // const-string v3, "resetPlayerVolume size="
v3 = (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "PlaybackActivityMonitorStubImpl"; // const-string v3, "PlaybackActivityMonitorStubImpl"
android.util.Log .d ( v3,v2 );
/* .line 142 */
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_2
/* check-cast v4, Landroid/media/AudioPlaybackConfiguration; */
/* .line 143 */
/* .local v4, "apc":Landroid/media/AudioPlaybackConfiguration; */
v5 = (( android.media.AudioPlaybackConfiguration ) v4 ).getClientUid ( ); // invoke-virtual {v4}, Landroid/media/AudioPlaybackConfiguration;->getClientUid()I
/* const/16 v6, 0x2710 */
/* if-le v5, v6, :cond_1 */
/* .line 144 */
v5 = /* invoke-direct {p0, v4}, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->isMusicPlayerActive(Landroid/media/AudioPlaybackConfiguration;)Z */
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 146 */
try { // :try_start_0
/* invoke-direct {p0, v4, p1}, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->getPkgName(Landroid/media/AudioPlaybackConfiguration;Landroid/content/Context;)Ljava/lang/String; */
/* .line 147 */
/* .local v5, "pkgName":Ljava/lang/String; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "resetPlayerVolume pkgName="; // const-string v7, "resetPlayerVolume pkgName="
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v3,v6 );
/* .line 148 */
(( android.media.AudioPlaybackConfiguration ) v4 ).getPlayerProxy ( ); // invoke-virtual {v4}, Landroid/media/AudioPlaybackConfiguration;->getPlayerProxy()Landroid/media/PlayerProxy;
v7 = (( com.android.server.audio.PlaybackActivityMonitorStubImpl ) p0 ).getPlayerVolume ( v5 ); // invoke-virtual {p0, v5}, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->getPlayerVolume(Ljava/lang/String;)F
(( android.media.PlayerProxy ) v6 ).setVolume ( v7 ); // invoke-virtual {v6, v7}, Landroid/media/PlayerProxy;->setVolume(F)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 151 */
} // .end local v5 # "pkgName":Ljava/lang/String;
/* .line 149 */
/* :catch_0 */
/* move-exception v5 */
/* .line 150 */
/* .local v5, "e":Ljava/lang/Exception; */
/* const-string/jumbo v6, "setPlayerVolume error in resetPlayerVolume" */
android.util.Log .e ( v3,v6,v5 );
/* .line 153 */
} // .end local v4 # "apc":Landroid/media/AudioPlaybackConfiguration;
} // .end local v5 # "e":Ljava/lang/Exception;
} // :cond_1
} // :goto_1
/* .line 154 */
} // :cond_2
return;
} // .end method
public void setForceIgnoreGrantedStatus ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "open" # Z */
/* .line 74 */
/* iput-boolean p1, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mIsForceIgnoreGranted:Z */
/* .line 75 */
return;
} // .end method
public void setPlayerVolume ( android.media.AudioPlaybackConfiguration p0, Float p1, java.lang.String p2, android.content.Context p3 ) {
/* .locals 8 */
/* .param p1, "apc" # Landroid/media/AudioPlaybackConfiguration; */
/* .param p2, "volume" # F */
/* .param p3, "from" # Ljava/lang/String; */
/* .param p4, "context" # Landroid/content/Context; */
/* .line 210 */
/* iget-boolean v0, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mOpenSoundAssist:Z */
if ( v0 != null) { // if-eqz v0, :cond_8
/* if-nez p4, :cond_0 */
/* goto/16 :goto_5 */
/* .line 213 */
} // :cond_0
v0 = this.mMusicVolumeMap;
/* monitor-enter v0 */
/* .line 214 */
try { // :try_start_0
v1 = (( android.media.AudioPlaybackConfiguration ) p1 ).getClientUid ( ); // invoke-virtual {p1}, Landroid/media/AudioPlaybackConfiguration;->getClientUid()I
/* const/16 v2, 0x2710 */
/* if-ge v1, v2, :cond_1 */
/* .line 215 */
final String v1 = "PlaybackActivityMonitorStubImpl"; // const-string v1, "PlaybackActivityMonitorStubImpl"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "setPlayerVolume skip system uid=" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = (( android.media.AudioPlaybackConfiguration ) p1 ).getClientUid ( ); // invoke-virtual {p1}, Landroid/media/AudioPlaybackConfiguration;->getClientUid()I
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 216 */
/* monitor-exit v0 */
return;
/* .line 218 */
} // :cond_1
/* invoke-direct {p0, p1, p4}, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->getPkgName(Landroid/media/AudioPlaybackConfiguration;Landroid/content/Context;)Ljava/lang/String; */
/* .line 219 */
/* .local v1, "pkg":Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 220 */
v2 = this.mMusicVolumeMap;
java.lang.Float .valueOf ( p2 );
(( java.util.HashMap ) v2 ).put ( v1, v3 ); // invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 222 */
} // .end local v1 # "pkg":Ljava/lang/String;
} // :cond_2
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 225 */
/* const/high16 v0, 0x3f800000 # 1.0f */
try { // :try_start_1
v1 = /* invoke-direct {p0, p1}, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->isMusicPlayerActive(Landroid/media/AudioPlaybackConfiguration;)Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 226 */
(( android.media.AudioPlaybackConfiguration ) p1 ).getPlayerProxy ( ); // invoke-virtual {p1}, Landroid/media/AudioPlaybackConfiguration;->getPlayerProxy()Landroid/media/PlayerProxy;
(( android.media.PlayerProxy ) v1 ).setVolume ( p2 ); // invoke-virtual {v1, p2}, Landroid/media/PlayerProxy;->setVolume(F)V
/* .line 228 */
} // :cond_3
(( android.media.AudioPlaybackConfiguration ) p1 ).getPlayerProxy ( ); // invoke-virtual {p1}, Landroid/media/AudioPlaybackConfiguration;->getPlayerProxy()Landroid/media/PlayerProxy;
(( android.media.PlayerProxy ) v1 ).setVolume ( v0 ); // invoke-virtual {v1, v0}, Landroid/media/PlayerProxy;->setVolume(F)V
/* .line 229 */
final String v1 = "PlaybackActivityMonitorStubImpl"; // const-string v1, "PlaybackActivityMonitorStubImpl"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "set volume is only for active music player" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p3 ); // invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 233 */
} // :goto_0
/* .line 231 */
/* :catch_0 */
/* move-exception v1 */
/* .line 232 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "PlaybackActivityMonitorStubImpl"; // const-string v2, "PlaybackActivityMonitorStubImpl"
/* const-string/jumbo v3, "setPlayerVolume " */
android.util.Log .e ( v2,v3,v1 );
/* .line 234 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
final String v1 = "audio"; // const-string v1, "audio"
(( android.content.Context ) p4 ).getSystemService ( v1 ); // invoke-virtual {p4, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/media/AudioManager; */
/* .line 235 */
/* .local v1, "am":Landroid/media/AudioManager; */
(( android.media.AudioManager ) v1 ).getActivePlaybackConfigurations ( ); // invoke-virtual {v1}, Landroid/media/AudioManager;->getActivePlaybackConfigurations()Ljava/util/List;
/* .line 236 */
/* .local v2, "apcList":Ljava/util/List;, "Ljava/util/List<Landroid/media/AudioPlaybackConfiguration;>;" */
} // :cond_4
v4 = } // :goto_2
if ( v4 != null) { // if-eqz v4, :cond_7
/* check-cast v4, Landroid/media/AudioPlaybackConfiguration; */
/* .line 237 */
/* .local v4, "apcNew":Landroid/media/AudioPlaybackConfiguration; */
v5 = (( android.media.AudioPlaybackConfiguration ) p1 ).equals ( v4 ); // invoke-virtual {p1, v4}, Landroid/media/AudioPlaybackConfiguration;->equals(Ljava/lang/Object;)Z
/* if-nez v5, :cond_4 */
v5 = (( android.media.AudioPlaybackConfiguration ) p1 ).getClientUid ( ); // invoke-virtual {p1}, Landroid/media/AudioPlaybackConfiguration;->getClientUid()I
v6 = (( android.media.AudioPlaybackConfiguration ) v4 ).getClientUid ( ); // invoke-virtual {v4}, Landroid/media/AudioPlaybackConfiguration;->getClientUid()I
/* if-eq v5, v6, :cond_5 */
/* .line 238 */
/* .line 241 */
} // :cond_5
try { // :try_start_2
v5 = /* invoke-direct {p0, v4}, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->isMusicPlayerActive(Landroid/media/AudioPlaybackConfiguration;)Z */
if ( v5 != null) { // if-eqz v5, :cond_6
/* .line 242 */
(( android.media.AudioPlaybackConfiguration ) v4 ).getPlayerProxy ( ); // invoke-virtual {v4}, Landroid/media/AudioPlaybackConfiguration;->getPlayerProxy()Landroid/media/PlayerProxy;
(( android.media.PlayerProxy ) v5 ).setVolume ( p2 ); // invoke-virtual {v5, p2}, Landroid/media/PlayerProxy;->setVolume(F)V
/* .line 244 */
} // :cond_6
(( android.media.AudioPlaybackConfiguration ) v4 ).getPlayerProxy ( ); // invoke-virtual {v4}, Landroid/media/AudioPlaybackConfiguration;->getPlayerProxy()Landroid/media/PlayerProxy;
(( android.media.PlayerProxy ) v5 ).setVolume ( v0 ); // invoke-virtual {v5, v0}, Landroid/media/PlayerProxy;->setVolume(F)V
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_1 */
/* .line 248 */
} // :goto_3
/* .line 246 */
/* :catch_1 */
/* move-exception v5 */
/* .line 247 */
/* .local v5, "e":Ljava/lang/Exception; */
final String v6 = "PlaybackActivityMonitorStubImpl"; // const-string v6, "PlaybackActivityMonitorStubImpl"
/* const-string/jumbo v7, "setPlayerVolume in same process error" */
android.util.Log .e ( v6,v7,v5 );
/* .line 249 */
} // .end local v4 # "apcNew":Landroid/media/AudioPlaybackConfiguration;
} // .end local v5 # "e":Ljava/lang/Exception;
} // :goto_4
/* .line 250 */
} // :cond_7
return;
/* .line 222 */
} // .end local v1 # "am":Landroid/media/AudioManager;
} // .end local v2 # "apcList":Ljava/util/List;, "Ljava/util/List<Landroid/media/AudioPlaybackConfiguration;>;"
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_3
/* monitor-exit v0 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* throw v1 */
/* .line 211 */
} // :cond_8
} // :goto_5
return;
} // .end method
public void setRouteCastUID ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "keyValuePairs" # Ljava/lang/String; */
/* .line 299 */
final String v0 = "routecast=off"; // const-string v0, "routecast=off"
v0 = (( java.lang.String ) p1 ).startsWith ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
int v1 = -1; // const/4 v1, -0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 300 */
/* iput v1, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mRouteCastUID:I */
/* .line 301 */
return;
/* .line 303 */
} // :cond_0
final String v0 = "routecast=on"; // const-string v0, "routecast=on"
v0 = (( java.lang.String ) p1 ).startsWith ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 305 */
try { // :try_start_0
final String v0 = ";"; // const-string v0, ";"
(( java.lang.String ) p1 ).split ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 306 */
/* .local v0, "params":[Ljava/lang/String; */
/* array-length v2, v0 */
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
/* if-ge v3, v2, :cond_2 */
/* aget-object v4, v0, v3 */
/* .line 307 */
/* .local v4, "str":Ljava/lang/String; */
/* const-string/jumbo v5, "uid=" */
v5 = (( java.lang.String ) v4 ).startsWith ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 308 */
int v5 = 4; // const/4 v5, 0x4
(( java.lang.String ) v4 ).substring ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;
v5 = java.lang.Integer .parseInt ( v5 );
/* iput v5, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mRouteCastUID:I */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 306 */
} // .end local v4 # "str":Ljava/lang/String;
} // :cond_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 314 */
} // .end local v0 # "params":[Ljava/lang/String;
} // :cond_2
/* .line 311 */
/* :catch_0 */
/* move-exception v0 */
/* .line 312 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 313 */
/* iput v1, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mRouteCastUID:I */
/* .line 316 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_3
} // :goto_1
return;
} // .end method
public void setSoundAssistStatus ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "open" # Z */
/* .line 62 */
v0 = this.objKeyLock;
/* monitor-enter v0 */
/* .line 63 */
try { // :try_start_0
/* iput-boolean p1, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mOpenSoundAssist:Z */
/* .line 64 */
/* monitor-exit v0 */
/* .line 65 */
return;
/* .line 64 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void soundAssistPlayState ( android.media.AudioPlaybackConfiguration p0, Integer p1, Integer p2, android.content.Context p3 ) {
/* .locals 1 */
/* .param p1, "apc" # Landroid/media/AudioPlaybackConfiguration; */
/* .param p2, "event" # I */
/* .param p3, "eventValue" # I */
/* .param p4, "context" # Landroid/content/Context; */
/* .line 53 */
v0 = (( com.android.server.audio.PlaybackActivityMonitorStubImpl ) p0 ).isSoundAssistOpen ( ); // invoke-virtual {p0}, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->isSoundAssistOpen()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 55 */
(( android.media.AudioPlaybackConfiguration ) p1 ).handleStateEvent ( p2, p3 ); // invoke-virtual {p1, p2, p3}, Landroid/media/AudioPlaybackConfiguration;->handleStateEvent(II)Z
/* .line 56 */
final String v0 = "playerEvent"; // const-string v0, "playerEvent"
(( com.android.server.audio.PlaybackActivityMonitorStubImpl ) p0 ).updatePlayerVolumeByApc ( p1, v0, p4 ); // invoke-virtual {p0, p1, v0, p4}, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->updatePlayerVolumeByApc(Landroid/media/AudioPlaybackConfiguration;Ljava/lang/String;Landroid/content/Context;)V
/* .line 58 */
} // :cond_0
return;
} // .end method
public void startPlayerVolumeService ( android.content.Context p0, Integer p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "streamType" # I */
/* .param p3, "flags" # I */
/* .line 192 */
final String v0 = "com.miui.misound.playervolume.VolumeUIService"; // const-string v0, "com.miui.misound.playervolume.VolumeUIService"
/* iget-boolean v1, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mOpenSoundAssist:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 194 */
try { // :try_start_0
/* new-instance v1, Landroid/content/Intent; */
/* invoke-direct {v1}, Landroid/content/Intent;-><init>()V */
/* .line 195 */
/* .local v1, "intent":Landroid/content/Intent; */
(( android.content.Intent ) v1 ).setAction ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 196 */
/* new-instance v2, Landroid/content/ComponentName; */
final String v3 = "com.miui.misound"; // const-string v3, "com.miui.misound"
/* invoke-direct {v2, v3, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( android.content.Intent ) v1 ).setComponent ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 198 */
/* const-string/jumbo v0, "streamType" */
(( android.content.Intent ) v1 ).putExtra ( v0, p2 ); // invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 199 */
final String v0 = "flags"; // const-string v0, "flags"
(( android.content.Intent ) v1 ).putExtra ( v0, p3 ); // invoke-virtual {v1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 200 */
(( android.content.Context ) p1 ).startForegroundService ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/Context;->startForegroundService(Landroid/content/Intent;)Landroid/content/ComponentName;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 203 */
/* nop */
} // .end local v1 # "intent":Landroid/content/Intent;
/* .line 201 */
/* :catch_0 */
/* move-exception v0 */
/* .line 202 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "PlaybackActivityMonitorStubImpl"; // const-string v1, "PlaybackActivityMonitorStubImpl"
final String v2 = "fail to start VolumeUIService"; // const-string v2, "fail to start VolumeUIService"
android.util.Log .e ( v1,v2 );
/* .line 205 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
return;
} // .end method
public void updatePlayerVolume ( android.media.AudioPlaybackConfiguration p0, Integer p1, android.content.Context p2 ) {
/* .locals 1 */
/* .param p1, "apc" # Landroid/media/AudioPlaybackConfiguration; */
/* .param p2, "event" # I */
/* .param p3, "context" # Landroid/content/Context; */
/* .line 254 */
int v0 = 2; // const/4 v0, 0x2
/* if-ne p2, v0, :cond_0 */
/* .line 255 */
final String v0 = "playerEvent"; // const-string v0, "playerEvent"
(( com.android.server.audio.PlaybackActivityMonitorStubImpl ) p0 ).updatePlayerVolumeByApc ( p1, v0, p3 ); // invoke-virtual {p0, p1, v0, p3}, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->updatePlayerVolumeByApc(Landroid/media/AudioPlaybackConfiguration;Ljava/lang/String;Landroid/content/Context;)V
/* .line 257 */
} // :cond_0
return;
} // .end method
public void updatePlayerVolumeByApc ( android.media.AudioPlaybackConfiguration p0, java.lang.String p1, android.content.Context p2 ) {
/* .locals 2 */
/* .param p1, "apc" # Landroid/media/AudioPlaybackConfiguration; */
/* .param p2, "from" # Ljava/lang/String; */
/* .param p3, "context" # Landroid/content/Context; */
/* .line 276 */
/* iget-boolean v0, p0, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->mOpenSoundAssist:Z */
/* if-nez v0, :cond_0 */
/* .line 277 */
return;
/* .line 279 */
} // :cond_0
/* invoke-direct {p0, p1, p3}, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->getPkgName(Landroid/media/AudioPlaybackConfiguration;Landroid/content/Context;)Ljava/lang/String; */
/* .line 280 */
/* .local v0, "pkgName":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 281 */
v1 = (( com.android.server.audio.PlaybackActivityMonitorStubImpl ) p0 ).getPlayerVolume ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->getPlayerVolume(Ljava/lang/String;)F
/* .line 282 */
/* .local v1, "vol":F */
(( com.android.server.audio.PlaybackActivityMonitorStubImpl ) p0 ).setPlayerVolume ( p1, v1, p2, p3 ); // invoke-virtual {p0, p1, v1, p2, p3}, Lcom/android/server/audio/PlaybackActivityMonitorStubImpl;->setPlayerVolume(Landroid/media/AudioPlaybackConfiguration;FLjava/lang/String;Landroid/content/Context;)V
/* .line 284 */
} // .end local v1 # "vol":F
} // :cond_1
return;
} // .end method
