public class com.android.server.audio.MQSAudioHardware {
	 /* .source "MQSAudioHardware.java" */
	 /* # static fields */
	 private static final java.lang.String AUDIO_HW;
	 private static final java.lang.String HW_CHANGE;
	 private static final java.lang.String TAG;
	 private static volatile com.android.server.audio.MQSAudioHardware sInstance;
	 /* # instance fields */
	 private android.content.Context mContext;
	 /* # direct methods */
	 private com.android.server.audio.MQSAudioHardware ( ) {
		 /* .locals 0 */
		 /* .line 22 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 private static Integer addStateChange ( java.util.Hashtable p0, java.lang.String p1, java.lang.String p2, java.lang.String p3, Integer p4, Integer p5 ) {
		 /* .locals 5 */
		 /* .param p1, "hwType" # Ljava/lang/String; */
		 /* .param p2, "oldHwState" # Ljava/lang/String; */
		 /* .param p3, "newHwState" # Ljava/lang/String; */
		 /* .param p4, "posStart" # I */
		 /* .param p5, "len" # I */
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(", */
		 /* "Ljava/util/Hashtable<", */
		 /* "Ljava/lang/String;", */
		 /* "Ljava/lang/Integer;", */
		 /* ">;", */
		 /* "Ljava/lang/String;", */
		 /* "Ljava/lang/String;", */
		 /* "Ljava/lang/String;", */
		 /* "II)I" */
		 /* } */
	 } // .end annotation
	 /* .line 111 */
	 /* .local p0, "hws":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/Integer;>;" */
	 /* add-int v0, p4, p5 */
	 (( java.lang.String ) p2 ).substring ( p4, v0 ); // invoke-virtual {p2, p4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;
	 /* .line 112 */
	 /* .local v0, "oldState":Ljava/lang/String; */
	 /* add-int v1, p4, p5 */
	 (( java.lang.String ) p3 ).substring ( p4, v1 ); // invoke-virtual {p3, p4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;
	 /* .line 113 */
	 /* .local v1, "newState":Ljava/lang/String; */
	 v2 = 	 (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 /* if-nez v2, :cond_1 */
	 final String v2 = "0"; // const-string v2, "0"
	 v3 = 	 (( java.lang.String ) v1 ).contains ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
	 v4 = 	 (( java.lang.String ) v0 ).contains ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
	 /* and-int/2addr v3, v4 */
	 /* if-nez v3, :cond_1 */
	 /* .line 114 */
	 v2 = 	 (( java.lang.String ) v1 ).contains ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 /* .line 115 */
		 int v2 = 0; // const/4 v2, 0x0
		 java.lang.Integer .valueOf ( v2 );
		 (( java.util.Hashtable ) p0 ).put ( p1, v3 ); // invoke-virtual {p0, p1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
		 /* .line 116 */
		 /* .line 118 */
	 } // :cond_0
	 int v2 = 1; // const/4 v2, 0x1
	 java.lang.Integer .valueOf ( v2 );
	 (( java.util.Hashtable ) p0 ).put ( p1, v3 ); // invoke-virtual {p0, p1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
	 /* .line 119 */
	 /* .line 122 */
} // :cond_1
int v2 = -1; // const/4 v2, -0x1
} // .end method
private Boolean canCheck ( ) {
/* .locals 7 */
/* .line 69 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
	 v1 = this.mContext;
	 (( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 /* .line 70 */
	 /* .local v1, "contentResolver":Landroid/content/ContentResolver; */
	 /* const-string/jumbo v2, "user_setup_complete" */
	 v2 = 	 android.provider.Settings$Secure .getInt ( v1,v2,v0 );
	 /* .line 71 */
	 /* .local v2, "userSetUpComplete":I */
	 v3 = 	 android.provider.MiuiSettings$Secure .isUserExperienceProgramEnable ( v1 );
	 /* .line 72 */
	 /* .local v3, "userExperienceEnable":Z */
	 final String v4 = "MQSAudioHardware"; // const-string v4, "MQSAudioHardware"
	 /* new-instance v5, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
	 /* const-string/jumbo v6, "userSetUpComplete = " */
	 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v6 = ", userExperienceEnable = "; // const-string v6, ", userExperienceEnable = "
	 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v4,v5 );
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 73 */
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 if ( v3 != null) { // if-eqz v3, :cond_0
			 int v0 = 1; // const/4 v0, 0x1
		 } // :cond_0
		 /* .line 74 */
	 } // .end local v1 # "contentResolver":Landroid/content/ContentResolver;
} // .end local v2 # "userSetUpComplete":I
} // .end local v3 # "userExperienceEnable":Z
/* :catch_0 */
/* move-exception v1 */
/* .line 75 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 77 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // .end method
public static com.android.server.audio.MQSAudioHardware getInstance ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 24 */
v0 = com.android.server.audio.MQSAudioHardware.sInstance;
/* if-nez v0, :cond_1 */
/* .line 25 */
/* const-class v0, Lcom/android/server/audio/MQSAudioHardware; */
/* monitor-enter v0 */
/* .line 26 */
try { // :try_start_0
v1 = com.android.server.audio.MQSAudioHardware.sInstance;
/* if-nez v1, :cond_0 */
/* .line 27 */
/* new-instance v1, Lcom/android/server/audio/MQSAudioHardware; */
/* invoke-direct {v1}, Lcom/android/server/audio/MQSAudioHardware;-><init>()V */
/* .line 28 */
v1 = com.android.server.audio.MQSAudioHardware.sInstance;
this.mContext = p0;
/* .line 30 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 32 */
} // :cond_1
} // :goto_0
v0 = com.android.server.audio.MQSAudioHardware.sInstance;
} // .end method
private static java.lang.String getTimeStamp ( ) {
/* .locals 4 */
/* .line 125 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* .line 126 */
/* .local v0, "time":J */
/* const-wide/16 v2, 0x3e8 */
/* div-long v2, v0, v2 */
java.lang.String .valueOf ( v2,v3 );
} // .end method
private static java.lang.String getTimeZoneId ( ) {
/* .locals 2 */
/* .line 129 */
java.util.TimeZone .getDefault ( );
/* .line 130 */
/* .local v0, "tz":Ljava/util/TimeZone; */
(( java.util.TimeZone ) v0 ).getID ( ); // invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;
} // .end method
private static java.util.Hashtable parseHwState ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 20 */
/* .param p0, "oldAudioHwState" # Ljava/lang/String; */
/* .param p1, "newAudioHwState" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/Hashtable<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 81 */
final String v0 = "0"; // const-string v0, "0"
/* new-instance v1, Ljava/util/Hashtable; */
/* invoke-direct {v1}, Ljava/util/Hashtable;-><init>()V */
/* .line 82 */
/* .local v1, "hws":Ljava/util/Hashtable; */
/* move-object/from16 v15, p0 */
/* move-object/from16 v14, p1 */
v2 = (( java.lang.String ) v14 ).equals ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 83 */
/* .line 85 */
} // :cond_0
final String v2 = "audio hw state changed from %s to %s"; // const-string v2, "audio hw state changed from %s to %s"
/* filled-new-array/range {p0 ..p1}, [Ljava/lang/Object; */
java.lang.String .format ( v2,v3 );
final String v3 = "MQSAudioHardware"; // const-string v3, "MQSAudioHardware"
android.util.Slog .d ( v3,v2 );
/* .line 88 */
try { // :try_start_0
final String v2 = "ro.vendor.audio.haptic.number"; // const-string v2, "ro.vendor.audio.haptic.number"
android.os.SystemProperties .get ( v2,v0 );
v7 = java.lang.Integer .parseInt ( v2 );
/* .line 89 */
/* .local v7, "hapticNum":I */
final String v3 = "haptic"; // const-string v3, "haptic"
int v6 = 1; // const/4 v6, 0x1
/* move-object v2, v1 */
/* move-object/from16 v4, p0 */
/* move-object/from16 v5, p1 */
/* invoke-static/range {v2 ..v7}, Lcom/android/server/audio/MQSAudioHardware;->addStateChange(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)I */
/* .line 91 */
final String v2 = "ro.vendor.audio.smartPA.number"; // const-string v2, "ro.vendor.audio.smartPA.number"
android.os.SystemProperties .get ( v2,v0 );
v13 = java.lang.Integer .parseInt ( v2 );
/* .line 92 */
/* .local v13, "paNum":I */
final String v9 = "pa"; // const-string v9, "pa"
int v12 = 3; // const/4 v12, 0x3
/* move-object v8, v1 */
/* move-object/from16 v10, p0 */
/* move-object/from16 v11, p1 */
/* invoke-static/range {v8 ..v13}, Lcom/android/server/audio/MQSAudioHardware;->addStateChange(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)I */
/* .line 94 */
final String v2 = "adsp"; // const-string v2, "adsp"
/* const/16 v18, 0xb */
/* const/16 v19, 0x1 */
/* move-object v14, v1 */
/* move-object v15, v2 */
/* move-object/from16 v16, p0 */
/* move-object/from16 v17, p1 */
v2 = /* invoke-static/range {v14 ..v19}, Lcom/android/server/audio/MQSAudioHardware;->addStateChange(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)I */
/* if-nez v2, :cond_1 */
/* .line 96 */
/* .line 98 */
} // :cond_1
final String v15 = "codec"; // const-string v15, "codec"
/* const/16 v18, 0xc */
/* const/16 v19, 0x1 */
/* move-object v14, v1 */
/* move-object/from16 v16, p0 */
/* move-object/from16 v17, p1 */
/* invoke-static/range {v14 ..v19}, Lcom/android/server/audio/MQSAudioHardware;->addStateChange(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)I */
/* .line 100 */
final String v2 = "ro.vendor.audio.audioswitch.number"; // const-string v2, "ro.vendor.audio.audioswitch.number"
android.os.SystemProperties .get ( v2,v0 );
v19 = java.lang.Integer .parseInt ( v0 );
/* .line 101 */
/* .local v19, "audioswitchNum":I */
final String v15 = "audioswitch"; // const-string v15, "audioswitch"
/* const/16 v18, 0xd */
/* move-object v14, v1 */
/* move-object/from16 v16, p0 */
/* move-object/from16 v17, p1 */
/* invoke-static/range {v14 ..v19}, Lcom/android/server/audio/MQSAudioHardware;->addStateChange(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)I */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 103 */
/* .line 104 */
} // .end local v7 # "hapticNum":I
} // .end local v13 # "paNum":I
} // .end local v19 # "audioswitchNum":I
/* :catch_0 */
/* move-exception v0 */
/* .line 105 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 107 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // .end method
/* # virtual methods */
public void onetrack ( ) {
/* .locals 11 */
/* .line 36 */
v0 = /* invoke-direct {p0}, Lcom/android/server/audio/MQSAudioHardware;->canCheck()Z */
/* if-nez v0, :cond_0 */
/* .line 37 */
final String v0 = "MQSAudioHardware"; // const-string v0, "MQSAudioHardware"
/* const-string/jumbo v1, "shouldn\'t check" */
android.util.Slog .d ( v0,v1 );
/* .line 38 */
return;
/* .line 40 */
} // :cond_0
/* const-string/jumbo v0, "vendor.audio.hw.state" */
android.os.SystemProperties .get ( v0 );
/* .line 41 */
/* .local v0, "newAudioHwState":Ljava/lang/String; */
/* if-nez v0, :cond_1 */
/* .line 42 */
return;
/* .line 44 */
} // :cond_1
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 45 */
/* .local v1, "contentResolver":Landroid/content/ContentResolver; */
final String v2 = "audio_hw_state"; // const-string v2, "audio_hw_state"
android.provider.Settings$Global .getString ( v1,v2 );
/* .line 46 */
/* .local v3, "oldAudioHwState":Ljava/lang/String; */
/* if-nez v3, :cond_2 */
/* .line 47 */
final String v3 = "00000000000000"; // const-string v3, "00000000000000"
/* .line 49 */
} // :cond_2
com.android.server.audio.MQSAudioHardware .parseHwState ( v3,v0 );
/* .line 51 */
/* .local v4, "hwState":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/Integer;>;" */
(( java.util.Hashtable ) v4 ).keys ( ); // invoke-virtual {v4}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;
/* .line 52 */
/* .local v5, "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;" */
/* new-instance v6, Ljava/util/ArrayList; */
/* invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V */
/* .line 53 */
/* .local v6, "changes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
v7 = } // :goto_0
if ( v7 != null) { // if-eqz v7, :cond_3
/* .line 54 */
/* check-cast v7, Ljava/lang/String; */
/* .line 55 */
/* .local v7, "key":Ljava/lang/String; */
(( java.util.Hashtable ) v4 ).get ( v7 ); // invoke-virtual {v4, v7}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* filled-new-array {v7, v8}, [Ljava/lang/Object; */
/* const-string/jumbo v9, "{\"audio_hw_type\":\"%s\",\"audio_hw_state\":\"%s\"}" */
java.lang.String .format ( v9,v8 );
(( java.util.ArrayList ) v6 ).add ( v8 ); // invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 56 */
} // .end local v7 # "key":Ljava/lang/String;
/* .line 57 */
} // :cond_3
v7 = (( java.util.ArrayList ) v6 ).size ( ); // invoke-virtual {v6}, Ljava/util/ArrayList;->size()I
/* if-lez v7, :cond_4 */
/* .line 58 */
final String v7 = ","; // const-string v7, ","
java.lang.String .join ( v7,v6 );
/* .line 59 */
/* .local v7, "changesStr":Ljava/lang/String; */
/* nop */
/* .line 60 */
com.android.server.audio.MQSAudioHardware .getTimeStamp ( );
com.android.server.audio.MQSAudioHardware .getTimeZoneId ( );
/* filled-new-array {v7, v8, v9}, [Ljava/lang/Object; */
/* .line 59 */
/* const-string/jumbo v9, "{\"onetrack_count\":\"1\",\"name\":\"audio_hw\",\"audio_event\":{\"audio_hw_changes\":[%s],\"detect_time\":\"%s\", \"time_zone\":\"%s\"},\"dgt\":\"null\",\"audio_ext\":\"null\"}" */
java.lang.String .format ( v9,v8 );
/* .line 61 */
/* .local v8, "message":Ljava/lang/String; */
v9 = this.mContext;
com.android.server.audio.MQSserver .getInstance ( v9 );
/* .line 62 */
/* .local v9, "mqs":Lcom/android/server/audio/MQSserver; */
v10 = (( com.android.server.audio.MQSserver ) v9 ).onetrack_report ( v8 ); // invoke-virtual {v9, v8}, Lcom/android/server/audio/MQSserver;->onetrack_report(Ljava/lang/String;)Z
if ( v10 != null) { // if-eqz v10, :cond_4
/* .line 63 */
android.provider.Settings$Global .putString ( v1,v2,v0 );
/* .line 66 */
} // .end local v7 # "changesStr":Ljava/lang/String;
} // .end local v8 # "message":Ljava/lang/String;
} // .end local v9 # "mqs":Lcom/android/server/audio/MQSserver;
} // :cond_4
return;
} // .end method
