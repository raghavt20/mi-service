.class Lcom/android/server/audio/AudioPowerSaveModeObserver$1;
.super Landroid/content/BroadcastReceiver;
.source "AudioPowerSaveModeObserver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/AudioPowerSaveModeObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/audio/AudioPowerSaveModeObserver;


# direct methods
.method constructor <init>(Lcom/android/server/audio/AudioPowerSaveModeObserver;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/audio/AudioPowerSaveModeObserver;

    .line 395
    iput-object p1, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver$1;->this$0:Lcom/android/server/audio/AudioPowerSaveModeObserver;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 398
    const-string v0, "AudioPowerSaveModeObserver"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 400
    .local v1, "action":Ljava/lang/String;
    :try_start_0
    const-string v2, "miui.intent.action.POWER_SAVE_MODE_CHANGED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 401
    const-string v2, "mAudioPowerSaveBroadcastReceiver: ACTION_POWER_SAVE_MODE_CHANGED"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    iget-object v2, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver$1;->this$0:Lcom/android/server/audio/AudioPowerSaveModeObserver;

    invoke-static {v2}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->-$$Nest$fgetmHandler(Lcom/android/server/audio/AudioPowerSaveModeObserver;)Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/audio/AudioPowerSaveModeObserver$1;->this$0:Lcom/android/server/audio/AudioPowerSaveModeObserver;

    invoke-static {v3}, Lcom/android/server/audio/AudioPowerSaveModeObserver;->-$$Nest$fgetmHandler(Lcom/android/server/audio/AudioPowerSaveModeObserver;)Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;

    move-result-object v3

    const/16 v4, 0x9c8

    invoke-virtual {v3, v4}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 404
    :cond_0
    const-string v2, "mAudioPowerSaveBroadcastReceiver error "

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 408
    :goto_0
    goto :goto_1

    .line 406
    :catch_0
    move-exception v2

    .line 407
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mAudioPowerSaveBroadcastReceiver exception "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method
