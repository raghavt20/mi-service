public class com.android.server.audio.AudioDeviceInventoryStubImpl implements com.android.server.audio.AudioDeviceInventoryStub {
	 /* .source "AudioDeviceInventoryStubImpl.java" */
	 /* # interfaces */
	 /* # direct methods */
	 public com.android.server.audio.AudioDeviceInventoryStubImpl ( ) {
		 /* .locals 0 */
		 /* .line 14 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public java.lang.String getLeAddr ( java.util.LinkedHashMap p0 ) {
		 /* .locals 5 */
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(", */
		 /* "Ljava/util/LinkedHashMap<", */
		 /* "Ljava/lang/String;", */
		 /* "Lcom/android/server/audio/AudioDeviceInventory$DeviceInfo;", */
		 /* ">;)", */
		 /* "Ljava/lang/String;" */
		 /* } */
	 } // .end annotation
	 /* .line 30 */
	 /* .local p1, "connecteddevices":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/String;Lcom/android/server/audio/AudioDeviceInventory$DeviceInfo;>;" */
	 if ( p1 != null) { // if-eqz p1, :cond_3
		 v0 = 		 (( java.util.LinkedHashMap ) p1 ).isEmpty ( ); // invoke-virtual {p1}, Ljava/util/LinkedHashMap;->isEmpty()Z
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 33 */
		 } // :cond_0
		 int v0 = 0; // const/4 v0, 0x0
		 /* .line 34 */
		 /* .local v0, "addr":Ljava/lang/String; */
		 (( java.util.LinkedHashMap ) p1 ).values ( ); // invoke-virtual {p1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;
	 v2 = 	 } // :goto_0
	 if ( v2 != null) { // if-eqz v2, :cond_2
		 /* check-cast v2, Lcom/android/server/audio/AudioDeviceInventory$DeviceInfo; */
		 /* .line 35 */
		 /* .local v2, "di":Lcom/android/server/audio/AudioDeviceInventory$DeviceInfo; */
		 /* iget v3, v2, Lcom/android/server/audio/AudioDeviceInventory$DeviceInfo;->mDeviceType:I */
		 /* const/high16 v4, 0x20000000 */
		 /* if-ne v3, v4, :cond_1 */
		 /* .line 36 */
		 v0 = this.mDeviceAddress;
		 /* .line 37 */
		 /* .line 39 */
	 } // .end local v2 # "di":Lcom/android/server/audio/AudioDeviceInventory$DeviceInfo;
} // :cond_1
/* .line 40 */
} // :cond_2
} // :goto_1
/* .line 31 */
} // .end local v0 # "addr":Ljava/lang/String;
} // :cond_3
} // :goto_2
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void postSetVolumeIndex ( com.android.server.audio.AudioDeviceBroker$BtDeviceInfo p0, com.android.server.audio.AudioDeviceBroker p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "btInfo" # Lcom/android/server/audio/AudioDeviceBroker$BtDeviceInfo; */
/* .param p2, "audiodevicebroker" # Lcom/android/server/audio/AudioDeviceBroker; */
/* .param p3, "streammusic" # I */
/* .line 19 */
/* if-nez p2, :cond_0 */
/* .line 20 */
return;
/* .line 22 */
} // :cond_0
/* iget v0, p1, Lcom/android/server/audio/AudioDeviceBroker$BtDeviceInfo;->mVolume:I */
int v1 = -1; // const/4 v1, -0x1
/* if-eq v0, v1, :cond_1 */
/* .line 23 */
/* iget v0, p1, Lcom/android/server/audio/AudioDeviceBroker$BtDeviceInfo;->mVolume:I */
/* mul-int/lit8 v0, v0, 0xa */
/* iget v1, p1, Lcom/android/server/audio/AudioDeviceBroker$BtDeviceInfo;->mAudioSystemDevice:I */
final String v2 = "onSetBtActiveDevice"; // const-string v2, "onSetBtActiveDevice"
(( com.android.server.audio.AudioDeviceBroker ) p2 ).postSetVolumeIndexOnDevice ( p3, v0, v1, v2 ); // invoke-virtual {p2, p3, v0, v1, v2}, Lcom/android/server/audio/AudioDeviceBroker;->postSetVolumeIndexOnDevice(IIILjava/lang/String;)V
/* .line 27 */
} // :cond_1
return;
} // .end method
