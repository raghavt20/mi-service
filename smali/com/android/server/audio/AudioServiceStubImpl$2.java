class com.android.server.audio.AudioServiceStubImpl$2 implements java.lang.Runnable {
	 /* .source "AudioServiceStubImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/audio/AudioServiceStubImpl;->setStreamMusicOrBluetoothScoIndex(Landroid/content/Context;III)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.audio.AudioServiceStubImpl this$0; //synthetic
final Integer val$device; //synthetic
final Integer val$index; //synthetic
final Integer val$stream; //synthetic
/* # direct methods */
 com.android.server.audio.AudioServiceStubImpl$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/audio/AudioServiceStubImpl; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 713 */
this.this$0 = p1;
/* iput p2, p0, Lcom/android/server/audio/AudioServiceStubImpl$2;->val$stream:I */
/* iput p3, p0, Lcom/android/server/audio/AudioServiceStubImpl$2;->val$index:I */
/* iput p4, p0, Lcom/android/server/audio/AudioServiceStubImpl$2;->val$device:I */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 4 */
/* .line 717 */
final String v0 = "AudioServiceStubImpl"; // const-string v0, "AudioServiceStubImpl"
try { // :try_start_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "setStreamMusicOrBluetoothScoIndex : stream=" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/audio/AudioServiceStubImpl$2;->val$stream:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " index="; // const-string v2, " index="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/audio/AudioServiceStubImpl$2;->val$index:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " device="; // const-string v2, " device="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/audio/AudioServiceStubImpl$2;->val$device:I */
java.lang.Integer .toHexString ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v1 );
/* .line 718 */
/* iget v1, p0, Lcom/android/server/audio/AudioServiceStubImpl$2;->val$stream:I */
int v2 = 3; // const/4 v2, 0x3
/* if-ne v1, v2, :cond_1 */
v1 = android.media.AudioSystem.DEVICE_OUT_ALL_A2DP_SET;
/* iget v2, p0, Lcom/android/server/audio/AudioServiceStubImpl$2;->val$device:I */
v1 = java.lang.Integer .valueOf ( v2 );
/* if-nez v1, :cond_0 */
/* iget v1, p0, Lcom/android/server/audio/AudioServiceStubImpl$2;->val$device:I */
int v2 = 2; // const/4 v2, 0x2
/* if-eq v1, v2, :cond_0 */
/* const/high16 v2, 0x4000000 */
/* if-eq v1, v2, :cond_0 */
int v2 = 4; // const/4 v2, 0x4
/* if-eq v1, v2, :cond_0 */
/* const/16 v2, 0x8 */
/* if-ne v1, v2, :cond_1 */
/* .line 723 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "audio.volume.stream.music.device."; // const-string v2, "audio.volume.stream.music.device."
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
/* iget v3, p0, Lcom/android/server/audio/AudioServiceStubImpl$2;->val$device:I */
com.android.server.audio.AudioServiceStubImpl .-$$Nest$mgetDeviceToStringForStreamMusic ( v2,v3 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "="; // const-string v2, "="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/audio/AudioServiceStubImpl$2;->val$index:I */
/* div-int/lit8 v2, v2, 0xa */
java.lang.Integer .toString ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 724 */
/* .local v1, "param":Ljava/lang/String; */
android.media.AudioSystem .setParameters ( v1 );
/* .line 726 */
/* nop */
} // .end local v1 # "param":Ljava/lang/String;
} // :cond_1
/* iget v1, p0, Lcom/android/server/audio/AudioServiceStubImpl$2;->val$stream:I */
int v2 = 6; // const/4 v2, 0x6
/* if-ne v1, v2, :cond_2 */
v1 = android.media.AudioSystem.DEVICE_OUT_ALL_SCO_SET;
/* iget v2, p0, Lcom/android/server/audio/AudioServiceStubImpl$2;->val$device:I */
v1 = java.lang.Integer .valueOf ( v2 );
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 727 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "audio.volume.stream.bluetoothsco="; // const-string v2, "audio.volume.stream.bluetoothsco="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/audio/AudioServiceStubImpl$2;->val$index:I */
java.lang.Integer .toString ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 728 */
/* .restart local v1 # "param":Ljava/lang/String; */
android.media.AudioSystem .setParameters ( v1 );
/* .line 730 */
/* nop */
} // .end local v1 # "param":Ljava/lang/String;
/* .line 731 */
} // :cond_2
final String v1 = "other device"; // const-string v1, "other device"
android.util.Log .e ( v0,v1 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 736 */
} // :goto_0
/* .line 733 */
/* :catch_0 */
/* move-exception v1 */
/* .line 734 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 735 */
final String v2 = "erroe for setStreamMusicOrBluetoothScoIndex"; // const-string v2, "erroe for setStreamMusicOrBluetoothScoIndex"
android.util.Log .e ( v0,v2 );
/* .line 737 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
