.class Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;
.super Lcom/android/server/audio/CloudServiceSettings$Setting;
.source "CloudServiceSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/CloudServiceSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "FileDownloadSetting"
.end annotation


# static fields
.field private static final DATA:Ljava/lang/String; = "data"

.field private static final ENCODING:Ljava/lang/String; = "encoding"

.field private static final PATH:Ljava/lang/String; = "absolutePath"


# instance fields
.field private mAbsolutePath:Ljava/lang/StringBuilder;

.field private mData:Ljava/lang/StringBuilder;

.field private mEncoding:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/server/audio/CloudServiceSettings;


# direct methods
.method constructor <init>(Lcom/android/server/audio/CloudServiceSettings;Lorg/json/JSONObject;)V
    .locals 3
    .param p1, "this$0"    # Lcom/android/server/audio/CloudServiceSettings;
    .param p2, "json"    # Lorg/json/JSONObject;

    .line 429
    iput-object p1, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->this$0:Lcom/android/server/audio/CloudServiceSettings;

    .line 430
    invoke-direct {p0, p1, p2}, Lcom/android/server/audio/CloudServiceSettings$Setting;-><init>(Lcom/android/server/audio/CloudServiceSettings;Lorg/json/JSONObject;)V

    .line 432
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "absolutePath"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mAbsolutePath:Ljava/lang/StringBuilder;

    .line 433
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "data"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mData:Ljava/lang/StringBuilder;

    .line 434
    iget-boolean v0, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mSecreted:Z

    if-eqz v0, :cond_0

    .line 435
    iget-object v0, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mSecretedItem:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mAbsolutePath:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 436
    iget-object v0, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mSecretedItem:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mData:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 438
    :cond_0
    const-string v0, "encoding"

    const-string v1, "B64"

    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mEncoding:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 441
    goto :goto_0

    .line 439
    :catch_0
    move-exception v0

    .line 440
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "CloudServiceSettings"

    const-string v2, "fail to parse FileDownloadSetting"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 442
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method


# virtual methods
.method public set()Z
    .locals 15

    .line 446
    invoke-super {p0}, Lcom/android/server/audio/CloudServiceSettings$Setting;->set()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 447
    return v1

    .line 448
    :cond_0
    iget-object v0, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->this$0:Lcom/android/server/audio/CloudServiceSettings;

    invoke-static {v0}, Lcom/android/server/audio/CloudServiceSettings;->-$$Nest$fgetiQConfig(Lcom/android/server/audio/CloudServiceSettings;)Ljava/lang/Object;

    move-result-object v0

    const-string v2, "CloudServiceSettings"

    if-nez v0, :cond_1

    sget-object v0, Lcom/android/server/audio/CloudServiceSettings;->mHidlmisysV2:Lvendor/xiaomi/hardware/misys/V2_0/IMiSys;

    if-nez v0, :cond_1

    .line 449
    const-string v0, "fail to write file, misys is null"

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    return v1

    .line 452
    :cond_1
    iget-boolean v0, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mSecreted:Z

    if-nez v0, :cond_2

    .line 453
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "start downloading: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mSettingName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", mAbsolutePath: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mAbsolutePath:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", mData: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mData:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", mEncoding: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mEncoding:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 455
    :cond_2
    iget-object v0, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mAbsolutePath:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_3

    .line 456
    const-string v0, "error path, return !"

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    return v1

    .line 459
    :cond_3
    iget-object v0, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mAbsolutePath:Ljava/lang/StringBuilder;

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0, v1, v4}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 460
    .local v0, "path":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mAbsolutePath:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v11, 0x1

    add-int/2addr v3, v11

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 461
    .local v3, "fileName":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "path :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " fileName: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    :try_start_0
    iget-object v4, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->this$0:Lcom/android/server/audio/CloudServiceSettings;

    invoke-static {v4}, Lcom/android/server/audio/CloudServiceSettings;->-$$Nest$fgetiMisys_V2(Lcom/android/server/audio/CloudServiceSettings;)Ljava/lang/Class;

    move-result-object v5

    invoke-static {v4, v5, v0, v3}, Lcom/android/server/audio/CloudServiceSettings;->-$$Nest$minvokeIsExists(Lcom/android/server/audio/CloudServiceSettings;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 465
    const-string v4, "file already exits, override!"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    iget-object v4, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->this$0:Lcom/android/server/audio/CloudServiceSettings;

    invoke-static {v4}, Lcom/android/server/audio/CloudServiceSettings;->-$$Nest$fgetiMisys_V1(Lcom/android/server/audio/CloudServiceSettings;)Ljava/lang/Class;

    move-result-object v5

    invoke-static {v4, v5, v0, v3}, Lcom/android/server/audio/CloudServiceSettings;->-$$Nest$minvokeEraseFileOrDirectory(Lcom/android/server/audio/CloudServiceSettings;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    :cond_4
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 469
    .local v4, "byteData":Ljava/io/ByteArrayOutputStream;
    const/4 v5, 0x0

    .line 470
    .local v5, "dataTowrite":[B
    iget-object v6, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mEncoding:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v7

    const/4 v8, -0x1

    sparse-switch v7, :sswitch_data_0

    :cond_5
    goto :goto_0

    :sswitch_0
    const-string v7, "FDS"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    move v6, v11

    goto :goto_1

    :sswitch_1
    const-string v7, "B64"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    move v6, v1

    goto :goto_1

    :goto_0
    move v6, v8

    :goto_1
    packed-switch v6, :pswitch_data_0

    .line 493
    goto :goto_3

    .line 479
    :pswitch_0
    new-instance v6, Ljava/net/URL;

    iget-object v7, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mData:Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 480
    .local v6, "url":Ljava/net/URL;
    invoke-virtual {v6}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v7

    check-cast v7, Ljava/net/HttpURLConnection;

    .line 481
    .local v7, "urlConnection":Ljava/net/HttpURLConnection;
    const v9, 0x1e8480

    invoke-virtual {v7, v9}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 482
    invoke-virtual {v7, v9}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 483
    const/16 v9, 0x400

    new-array v9, v9, [B

    move-object v5, v9

    .line 484
    new-instance v9, Ljava/io/BufferedInputStream;

    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 485
    .local v9, "in":Ljava/io/InputStream;
    const/4 v10, 0x0

    .line 486
    .local v10, "len":I
    :goto_2
    invoke-virtual {v9, v5}, Ljava/io/InputStream;->read([B)I

    move-result v12

    move v10, v12

    if-eq v12, v8, :cond_6

    .line 487
    invoke-virtual {v4, v5, v1, v10}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_2

    .line 489
    :cond_6
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V

    .line 490
    move-object v12, v5

    goto :goto_4

    .line 472
    .end local v6    # "url":Ljava/net/URL;
    .end local v7    # "urlConnection":Ljava/net/HttpURLConnection;
    .end local v9    # "in":Ljava/io/InputStream;
    .end local v10    # "len":I
    :pswitch_1
    iget-object v6, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mData:Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v6

    move-object v5, v6

    .line 473
    if-eqz v5, :cond_7

    .line 474
    array-length v6, v5

    invoke-virtual {v4, v5, v1, v6}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 495
    :cond_7
    move-object v12, v5

    goto :goto_4

    .line 493
    :goto_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "fail to download "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mSettingName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " encoding type not recognized"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v12, v5

    .line 495
    .end local v5    # "dataTowrite":[B
    .local v12, "dataTowrite":[B
    :goto_4
    iget-object v5, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mAbsolutePath:Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "/data/vendor/video/videobox.json"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 496
    sget-object v5, Lcom/android/server/audio/CloudServiceSettings;->mHidlmisysV2:Lvendor/xiaomi/hardware/misys/V2_0/IMiSys;

    if-eqz v5, :cond_9

    .line 497
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move-object v13, v5

    .line 498
    .local v13, "writeData":Ljava/util/ArrayList;
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    move-object v14, v5

    .line 499
    .local v14, "byteDataArray":[B
    array-length v5, v14

    :goto_5
    if-ge v1, v5, :cond_8

    aget-byte v6, v14, v1

    .line 500
    .local v6, "bt":B
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v7

    invoke-virtual {v13, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 499
    nop

    .end local v6    # "bt":B
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 502
    :cond_8
    sget-object v5, Lcom/android/server/audio/CloudServiceSettings;->mHidlmisysV2:Lvendor/xiaomi/hardware/misys/V2_0/IMiSys;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v1

    int-to-long v9, v1

    move-object v6, v0

    move-object v7, v3

    move-object v8, v13

    invoke-interface/range {v5 .. v10}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys;->MiSysWriteBuffer(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;J)I

    .line 503
    iput-boolean v11, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mSuccess:Z

    .end local v13    # "writeData":Ljava/util/ArrayList;
    .end local v14    # "byteDataArray":[B
    goto :goto_7

    .line 504
    :cond_9
    iget-object v5, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->this$0:Lcom/android/server/audio/CloudServiceSettings;

    invoke-static {v5}, Lcom/android/server/audio/CloudServiceSettings;->-$$Nest$fgetiQConfig(Lcom/android/server/audio/CloudServiceSettings;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_b

    .line 505
    iget-object v5, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->this$0:Lcom/android/server/audio/CloudServiceSettings;

    invoke-static {v5}, Lcom/android/server/audio/CloudServiceSettings;->-$$Nest$fgetiQConfig(Lcom/android/server/audio/CloudServiceSettings;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "WriteToFile"

    new-array v7, v11, [Ljava/lang/Class;

    const-class v8, [C

    aput-object v8, v7, v1

    invoke-virtual {v5, v6, v7}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 506
    .local v1, "writeMethod":Ljava/lang/reflect/Method;
    if-eqz v1, :cond_c

    .line 507
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v5

    new-array v5, v5, [C

    .line 508
    .local v5, "charData":[C
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    .line 509
    .local v6, "byteDataArray":[B
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_6
    array-length v8, v6

    sub-int/2addr v8, v11

    if-ge v7, v8, :cond_a

    .line 510
    div-int/lit8 v8, v7, 0x2

    add-int/lit8 v9, v7, 0x1

    aget-byte v9, v6, v9

    and-int/lit16 v9, v9, 0xff

    shl-int/lit8 v9, v9, 0x8

    aget-byte v10, v6, v7

    and-int/lit16 v10, v10, 0xff

    or-int/2addr v9, v10

    int-to-char v9, v9

    aput-char v9, v5, v8

    .line 509
    add-int/lit8 v7, v7, 0x2

    goto :goto_6

    .line 512
    .end local v7    # "i":I
    :cond_a
    iget-object v7, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->this$0:Lcom/android/server/audio/CloudServiceSettings;

    invoke-static {v7}, Lcom/android/server/audio/CloudServiceSettings;->-$$Nest$fgetiQConfig(Lcom/android/server/audio/CloudServiceSettings;)Ljava/lang/Object;

    move-result-object v7

    filled-new-array {v5}, [Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 513
    iput-boolean v11, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mSuccess:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_8

    .line 504
    .end local v1    # "writeMethod":Ljava/lang/reflect/Method;
    .end local v5    # "charData":[C
    .end local v6    # "byteDataArray":[B
    :cond_b
    :goto_7
    nop

    .line 519
    .end local v4    # "byteData":Ljava/io/ByteArrayOutputStream;
    .end local v12    # "dataTowrite":[B
    :cond_c
    :goto_8
    goto :goto_9

    .line 517
    :catch_0
    move-exception v1

    .line 518
    .local v1, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "fail to write data: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 520
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_9
    iget-boolean v1, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mSuccess:Z

    return v1

    nop

    :sswitch_data_0
    .sparse-switch
        0xfe80 -> :sswitch_1
        0x10f55 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public updateTo(Lcom/android/server/audio/CloudServiceSettings$Setting;)Z
    .locals 2
    .param p1, "s"    # Lcom/android/server/audio/CloudServiceSettings$Setting;

    .line 525
    invoke-super {p0, p1}, Lcom/android/server/audio/CloudServiceSettings$Setting;->updateTo(Lcom/android/server/audio/CloudServiceSettings$Setting;)Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;

    if-eqz v0, :cond_0

    .line 526
    move-object v0, p1

    check-cast v0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;

    .line 527
    .local v0, "fs":Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;
    iget-object v1, v0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mAbsolutePath:Ljava/lang/StringBuilder;

    iput-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mAbsolutePath:Ljava/lang/StringBuilder;

    .line 528
    iget-object v1, v0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mData:Ljava/lang/StringBuilder;

    iput-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mData:Ljava/lang/StringBuilder;

    .line 529
    iget-object v1, v0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mEncoding:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mEncoding:Ljava/lang/String;

    .line 530
    const/4 v1, 0x1

    return v1

    .line 532
    .end local v0    # "fs":Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "fail to update FileDownloadSetting: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$FileDownloadSetting;->mSettingName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CloudServiceSettings"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    const/4 v0, 0x0

    return v0
.end method
