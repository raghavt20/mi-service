.class public Lcom/android/server/audio/MQSAudioHardware;
.super Ljava/lang/Object;
.source "MQSAudioHardware.java"


# static fields
.field private static final AUDIO_HW:Ljava/lang/String; = "{\"onetrack_count\":\"1\",\"name\":\"audio_hw\",\"audio_event\":{\"audio_hw_changes\":[%s],\"detect_time\":\"%s\", \"time_zone\":\"%s\"},\"dgt\":\"null\",\"audio_ext\":\"null\"}"

.field private static final HW_CHANGE:Ljava/lang/String; = "{\"audio_hw_type\":\"%s\",\"audio_hw_state\":\"%s\"}"

.field private static final TAG:Ljava/lang/String; = "MQSAudioHardware"

.field private static volatile sInstance:Lcom/android/server/audio/MQSAudioHardware;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static addStateChange(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)I
    .locals 5
    .param p1, "hwType"    # Ljava/lang/String;
    .param p2, "oldHwState"    # Ljava/lang/String;
    .param p3, "newHwState"    # Ljava/lang/String;
    .param p4, "posStart"    # I
    .param p5, "len"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II)I"
        }
    .end annotation

    .line 111
    .local p0, "hws":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/Integer;>;"
    add-int v0, p4, p5

    invoke-virtual {p2, p4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 112
    .local v0, "oldState":Ljava/lang/String;
    add-int v1, p4, p5

    invoke-virtual {p3, p4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 113
    .local v1, "newState":Ljava/lang/String;
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    and-int/2addr v3, v4

    if-nez v3, :cond_1

    .line 114
    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 115
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p0, p1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    return v2

    .line 118
    :cond_0
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p0, p1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    return v2

    .line 122
    :cond_1
    const/4 v2, -0x1

    return v2
.end method

.method private canCheck()Z
    .locals 7

    .line 69
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/android/server/audio/MQSAudioHardware;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 70
    .local v1, "contentResolver":Landroid/content/ContentResolver;
    const-string/jumbo v2, "user_setup_complete"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 71
    .local v2, "userSetUpComplete":I
    invoke-static {v1}, Landroid/provider/MiuiSettings$Secure;->isUserExperienceProgramEnable(Landroid/content/ContentResolver;)Z

    move-result v3

    .line 72
    .local v3, "userExperienceEnable":Z
    const-string v4, "MQSAudioHardware"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "userSetUpComplete = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", userExperienceEnable = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    .line 74
    .end local v1    # "contentResolver":Landroid/content/ContentResolver;
    .end local v2    # "userSetUpComplete":I
    .end local v3    # "userExperienceEnable":Z
    :catch_0
    move-exception v1

    .line 75
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 77
    .end local v1    # "e":Ljava/lang/Exception;
    return v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/server/audio/MQSAudioHardware;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 24
    sget-object v0, Lcom/android/server/audio/MQSAudioHardware;->sInstance:Lcom/android/server/audio/MQSAudioHardware;

    if-nez v0, :cond_1

    .line 25
    const-class v0, Lcom/android/server/audio/MQSAudioHardware;

    monitor-enter v0

    .line 26
    :try_start_0
    sget-object v1, Lcom/android/server/audio/MQSAudioHardware;->sInstance:Lcom/android/server/audio/MQSAudioHardware;

    if-nez v1, :cond_0

    .line 27
    new-instance v1, Lcom/android/server/audio/MQSAudioHardware;

    invoke-direct {v1}, Lcom/android/server/audio/MQSAudioHardware;-><init>()V

    sput-object v1, Lcom/android/server/audio/MQSAudioHardware;->sInstance:Lcom/android/server/audio/MQSAudioHardware;

    .line 28
    sget-object v1, Lcom/android/server/audio/MQSAudioHardware;->sInstance:Lcom/android/server/audio/MQSAudioHardware;

    iput-object p0, v1, Lcom/android/server/audio/MQSAudioHardware;->mContext:Landroid/content/Context;

    .line 30
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 32
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/audio/MQSAudioHardware;->sInstance:Lcom/android/server/audio/MQSAudioHardware;

    return-object v0
.end method

.method private static getTimeStamp()Ljava/lang/String;
    .locals 4

    .line 125
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 126
    .local v0, "time":J
    const-wide/16 v2, 0x3e8

    div-long v2, v0, v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private static getTimeZoneId()Ljava/lang/String;
    .locals 2

    .line 129
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    .line 130
    .local v0, "tz":Ljava/util/TimeZone;
    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static parseHwState(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Hashtable;
    .locals 20
    .param p0, "oldAudioHwState"    # Ljava/lang/String;
    .param p1, "newAudioHwState"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Hashtable<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 81
    const-string v0, "0"

    new-instance v1, Ljava/util/Hashtable;

    invoke-direct {v1}, Ljava/util/Hashtable;-><init>()V

    .line 82
    .local v1, "hws":Ljava/util/Hashtable;
    move-object/from16 v15, p0

    move-object/from16 v14, p1

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 83
    return-object v1

    .line 85
    :cond_0
    const-string v2, "audio hw state changed from %s to %s"

    filled-new-array/range {p0 .. p1}, [Ljava/lang/Object;

    move-result-object v3

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "MQSAudioHardware"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    :try_start_0
    const-string v2, "ro.vendor.audio.haptic.number"

    invoke-static {v2, v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 89
    .local v7, "hapticNum":I
    const-string v3, "haptic"

    const/4 v6, 0x1

    move-object v2, v1

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    invoke-static/range {v2 .. v7}, Lcom/android/server/audio/MQSAudioHardware;->addStateChange(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)I

    .line 91
    const-string v2, "ro.vendor.audio.smartPA.number"

    invoke-static {v2, v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    .line 92
    .local v13, "paNum":I
    const-string v9, "pa"

    const/4 v12, 0x3

    move-object v8, v1

    move-object/from16 v10, p0

    move-object/from16 v11, p1

    invoke-static/range {v8 .. v13}, Lcom/android/server/audio/MQSAudioHardware;->addStateChange(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)I

    .line 94
    const-string v2, "adsp"

    const/16 v18, 0xb

    const/16 v19, 0x1

    move-object v14, v1

    move-object v15, v2

    move-object/from16 v16, p0

    move-object/from16 v17, p1

    invoke-static/range {v14 .. v19}, Lcom/android/server/audio/MQSAudioHardware;->addStateChange(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)I

    move-result v2

    if-nez v2, :cond_1

    .line 96
    return-object v1

    .line 98
    :cond_1
    const-string v15, "codec"

    const/16 v18, 0xc

    const/16 v19, 0x1

    move-object v14, v1

    move-object/from16 v16, p0

    move-object/from16 v17, p1

    invoke-static/range {v14 .. v19}, Lcom/android/server/audio/MQSAudioHardware;->addStateChange(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)I

    .line 100
    const-string v2, "ro.vendor.audio.audioswitch.number"

    invoke-static {v2, v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    .line 101
    .local v19, "audioswitchNum":I
    const-string v15, "audioswitch"

    const/16 v18, 0xd

    move-object v14, v1

    move-object/from16 v16, p0

    move-object/from16 v17, p1

    invoke-static/range {v14 .. v19}, Lcom/android/server/audio/MQSAudioHardware;->addStateChange(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 103
    return-object v1

    .line 104
    .end local v7    # "hapticNum":I
    .end local v13    # "paNum":I
    .end local v19    # "audioswitchNum":I
    :catch_0
    move-exception v0

    .line 105
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 107
    .end local v0    # "e":Ljava/lang/Exception;
    return-object v1
.end method


# virtual methods
.method public onetrack()V
    .locals 11

    .line 36
    invoke-direct {p0}, Lcom/android/server/audio/MQSAudioHardware;->canCheck()Z

    move-result v0

    if-nez v0, :cond_0

    .line 37
    const-string v0, "MQSAudioHardware"

    const-string/jumbo v1, "shouldn\'t check"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    return-void

    .line 40
    :cond_0
    const-string/jumbo v0, "vendor.audio.hw.state"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 41
    .local v0, "newAudioHwState":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 42
    return-void

    .line 44
    :cond_1
    iget-object v1, p0, Lcom/android/server/audio/MQSAudioHardware;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 45
    .local v1, "contentResolver":Landroid/content/ContentResolver;
    const-string v2, "audio_hw_state"

    invoke-static {v1, v2}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 46
    .local v3, "oldAudioHwState":Ljava/lang/String;
    if-nez v3, :cond_2

    .line 47
    const-string v3, "00000000000000"

    .line 49
    :cond_2
    invoke-static {v3, v0}, Lcom/android/server/audio/MQSAudioHardware;->parseHwState(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Hashtable;

    move-result-object v4

    .line 51
    .local v4, "hwState":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-virtual {v4}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v5

    .line 52
    .local v5, "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 53
    .local v6, "changes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v5}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 54
    invoke-interface {v5}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 55
    .local v7, "key":Ljava/lang/String;
    invoke-virtual {v4, v7}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    filled-new-array {v7, v8}, [Ljava/lang/Object;

    move-result-object v8

    const-string/jumbo v9, "{\"audio_hw_type\":\"%s\",\"audio_hw_state\":\"%s\"}"

    invoke-static {v9, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 56
    .end local v7    # "key":Ljava/lang/String;
    goto :goto_0

    .line 57
    :cond_3
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_4

    .line 58
    const-string v7, ","

    invoke-static {v7, v6}, Ljava/lang/String;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v7

    .line 59
    .local v7, "changesStr":Ljava/lang/String;
    nop

    .line 60
    invoke-static {}, Lcom/android/server/audio/MQSAudioHardware;->getTimeStamp()Ljava/lang/String;

    move-result-object v8

    invoke-static {}, Lcom/android/server/audio/MQSAudioHardware;->getTimeZoneId()Ljava/lang/String;

    move-result-object v9

    filled-new-array {v7, v8, v9}, [Ljava/lang/Object;

    move-result-object v8

    .line 59
    const-string/jumbo v9, "{\"onetrack_count\":\"1\",\"name\":\"audio_hw\",\"audio_event\":{\"audio_hw_changes\":[%s],\"detect_time\":\"%s\", \"time_zone\":\"%s\"},\"dgt\":\"null\",\"audio_ext\":\"null\"}"

    invoke-static {v9, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 61
    .local v8, "message":Ljava/lang/String;
    iget-object v9, p0, Lcom/android/server/audio/MQSAudioHardware;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/android/server/audio/MQSserver;->getInstance(Landroid/content/Context;)Lcom/android/server/audio/MQSserver;

    move-result-object v9

    .line 62
    .local v9, "mqs":Lcom/android/server/audio/MQSserver;
    invoke-virtual {v9, v8}, Lcom/android/server/audio/MQSserver;->onetrack_report(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 63
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 66
    .end local v7    # "changesStr":Ljava/lang/String;
    .end local v8    # "message":Ljava/lang/String;
    .end local v9    # "mqs":Lcom/android/server/audio/MQSserver;
    :cond_4
    return-void
.end method
