class com.android.server.audio.AudioPowerSaveModeObserver$1 extends android.content.BroadcastReceiver {
	 /* .source "AudioPowerSaveModeObserver.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/audio/AudioPowerSaveModeObserver; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.audio.AudioPowerSaveModeObserver this$0; //synthetic
/* # direct methods */
 com.android.server.audio.AudioPowerSaveModeObserver$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/audio/AudioPowerSaveModeObserver; */
/* .line 395 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 398 */
final String v0 = "AudioPowerSaveModeObserver"; // const-string v0, "AudioPowerSaveModeObserver"
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 400 */
/* .local v1, "action":Ljava/lang/String; */
try { // :try_start_0
	 final String v2 = "miui.intent.action.POWER_SAVE_MODE_CHANGED"; // const-string v2, "miui.intent.action.POWER_SAVE_MODE_CHANGED"
	 v2 = 	 (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 /* .line 401 */
		 final String v2 = "mAudioPowerSaveBroadcastReceiver: ACTION_POWER_SAVE_MODE_CHANGED"; // const-string v2, "mAudioPowerSaveBroadcastReceiver: ACTION_POWER_SAVE_MODE_CHANGED"
		 android.util.Log .d ( v0,v2 );
		 /* .line 402 */
		 v2 = this.this$0;
		 com.android.server.audio.AudioPowerSaveModeObserver .-$$Nest$fgetmHandler ( v2 );
		 v3 = this.this$0;
		 com.android.server.audio.AudioPowerSaveModeObserver .-$$Nest$fgetmHandler ( v3 );
		 /* const/16 v4, 0x9c8 */
		 (( com.android.server.audio.AudioPowerSaveModeObserver$WorkHandler ) v3 ).obtainMessage ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->obtainMessage(I)Landroid/os/Message;
		 (( com.android.server.audio.AudioPowerSaveModeObserver$WorkHandler ) v2 ).sendMessage ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/audio/AudioPowerSaveModeObserver$WorkHandler;->sendMessage(Landroid/os/Message;)Z
		 /* .line 404 */
	 } // :cond_0
	 final String v2 = "mAudioPowerSaveBroadcastReceiver error "; // const-string v2, "mAudioPowerSaveBroadcastReceiver error "
	 android.util.Log .e ( v0,v2 );
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 408 */
} // :goto_0
/* .line 406 */
/* :catch_0 */
/* move-exception v2 */
/* .line 407 */
/* .local v2, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "mAudioPowerSaveBroadcastReceiver exception "; // const-string v4, "mAudioPowerSaveBroadcastReceiver exception "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v3 );
/* .line 409 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
