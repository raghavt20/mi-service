.class public Lcom/android/server/audio/FoldHelper;
.super Ljava/lang/Object;
.source "FoldHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/audio/FoldHelper$AudioFoldListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "AudioService.FoldHelper"

.field private static sFoldListener:Lcom/android/server/audio/FoldHelper$AudioFoldListener;


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    return-void
.end method

.method static disable()V
    .locals 3

    .line 52
    sget-object v0, Lcom/android/server/audio/FoldHelper;->sFoldListener:Lcom/android/server/audio/FoldHelper$AudioFoldListener;

    if-eqz v0, :cond_0

    .line 54
    :try_start_0
    invoke-static {}, Landroid/view/WindowManagerGlobal;->getWindowManagerService()Landroid/view/IWindowManager;

    move-result-object v0

    sget-object v1, Lcom/android/server/audio/FoldHelper;->sFoldListener:Lcom/android/server/audio/FoldHelper$AudioFoldListener;

    .line 55
    invoke-interface {v0, v1}, Landroid/view/IWindowManager;->unregisterDisplayFoldListener(Landroid/view/IDisplayFoldListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    goto :goto_0

    .line 56
    :catch_0
    move-exception v0

    .line 57
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unregisterDisplayFoldListener error "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AudioService.FoldHelper"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return-void
.end method

.method static enable()V
    .locals 3

    .line 44
    :try_start_0
    invoke-static {}, Landroid/view/WindowManagerGlobal;->getWindowManagerService()Landroid/view/IWindowManager;

    move-result-object v0

    sget-object v1, Lcom/android/server/audio/FoldHelper;->sFoldListener:Lcom/android/server/audio/FoldHelper$AudioFoldListener;

    .line 45
    invoke-interface {v0, v1}, Landroid/view/IWindowManager;->registerDisplayFoldListener(Landroid/view/IDisplayFoldListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    goto :goto_0

    .line 46
    :catch_0
    move-exception v0

    .line 47
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "registerDisplayFoldListener error "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AudioService.FoldHelper"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method static init()V
    .locals 1

    .line 38
    new-instance v0, Lcom/android/server/audio/FoldHelper$AudioFoldListener;

    invoke-direct {v0}, Lcom/android/server/audio/FoldHelper$AudioFoldListener;-><init>()V

    sput-object v0, Lcom/android/server/audio/FoldHelper;->sFoldListener:Lcom/android/server/audio/FoldHelper$AudioFoldListener;

    .line 39
    invoke-static {}, Lcom/android/server/audio/FoldHelper;->enable()V

    .line 40
    return-void
.end method
