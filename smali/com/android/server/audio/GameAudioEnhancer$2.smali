.class Lcom/android/server/audio/GameAudioEnhancer$2;
.super Lmiui/process/IForegroundInfoListener$Stub;
.source "GameAudioEnhancer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/GameAudioEnhancer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/audio/GameAudioEnhancer;


# direct methods
.method constructor <init>(Lcom/android/server/audio/GameAudioEnhancer;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/audio/GameAudioEnhancer;

    .line 210
    iput-object p1, p0, Lcom/android/server/audio/GameAudioEnhancer$2;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    invoke-direct {p0}, Lmiui/process/IForegroundInfoListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onForegroundInfoChanged(Lmiui/process/ForegroundInfo;)V
    .locals 5
    .param p1, "foregroundInfo"    # Lmiui/process/ForegroundInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 213
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "foreground change to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", last foreground is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lmiui/process/ForegroundInfo;->mLastForegroundPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GameAudioEnhancer"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    iget-object v0, p0, Lcom/android/server/audio/GameAudioEnhancer$2;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    iget-object v1, p1, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$fputmCurForegroundPkg(Lcom/android/server/audio/GameAudioEnhancer;Ljava/lang/String;)V

    .line 216
    iget-object v0, p1, Lmiui/process/ForegroundInfo;->mLastForegroundPackageName:Ljava/lang/String;

    .line 217
    .local v0, "LastForegroundPkg":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/audio/GameAudioEnhancer$2;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    invoke-static {v1}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$fgetmParamSet(Lcom/android/server/audio/GameAudioEnhancer;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/server/audio/GameAudioEnhancer$2;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    invoke-static {v1, v0}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$misPackageEnabled(Lcom/android/server/audio/GameAudioEnhancer;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 218
    iget-object v1, p0, Lcom/android/server/audio/GameAudioEnhancer$2;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$fputmParamSet(Lcom/android/server/audio/GameAudioEnhancer;Z)V

    .line 219
    iget-object v1, p0, Lcom/android/server/audio/GameAudioEnhancer$2;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    invoke-static {v1}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$fgetmHandler(Lcom/android/server/audio/GameAudioEnhancer;)Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/audio/GameAudioEnhancer$2;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    invoke-static {v2}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$fgetmHandler(Lcom/android/server/audio/GameAudioEnhancer;)Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v3, 0x1f4

    invoke-virtual {v1, v2, v3, v4}, Lcom/android/server/audio/GameAudioEnhancer$WorkHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 222
    :cond_0
    iget-object v1, p0, Lcom/android/server/audio/GameAudioEnhancer$2;->this$0:Lcom/android/server/audio/GameAudioEnhancer;

    invoke-static {v1}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$fgetmCurForegroundPkg(Lcom/android/server/audio/GameAudioEnhancer;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/server/audio/GameAudioEnhancer;->-$$Nest$msetParamSend(Lcom/android/server/audio/GameAudioEnhancer;Ljava/lang/String;)V

    .line 223
    return-void
.end method
