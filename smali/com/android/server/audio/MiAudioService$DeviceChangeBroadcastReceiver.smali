.class Lcom/android/server/audio/MiAudioService$DeviceChangeBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MiAudioService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/MiAudioService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeviceChangeBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/audio/MiAudioService;


# direct methods
.method private constructor <init>(Lcom/android/server/audio/MiAudioService;)V
    .locals 0

    .line 124
    iput-object p1, p0, Lcom/android/server/audio/MiAudioService$DeviceChangeBroadcastReceiver;->this$0:Lcom/android/server/audio/MiAudioService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/audio/MiAudioService;Lcom/android/server/audio/MiAudioService$DeviceChangeBroadcastReceiver-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/audio/MiAudioService$DeviceChangeBroadcastReceiver;-><init>(Lcom/android/server/audio/MiAudioService;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 127
    iget-object v0, p0, Lcom/android/server/audio/MiAudioService$DeviceChangeBroadcastReceiver;->this$0:Lcom/android/server/audio/MiAudioService;

    iget-object v0, v0, Lcom/android/server/audio/MiAudioService;->mDolbyEffectController:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    if-eqz v0, :cond_0

    .line 128
    const-string v0, "MiAudioService"

    const-string v1, "DeviceChangeBroadcastReceiver onReceive"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    iget-object v0, p0, Lcom/android/server/audio/MiAudioService$DeviceChangeBroadcastReceiver;->this$0:Lcom/android/server/audio/MiAudioService;

    iget-object v0, v0, Lcom/android/server/audio/MiAudioService;->mDolbyEffectController:Lcom/android/server/audio/dolbyeffect/DolbyEffectController;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/audio/dolbyeffect/DolbyEffectController;->receiveDeviceConnectStateChanged(Landroid/content/Context;Landroid/content/Intent;)V

    .line 131
    :cond_0
    return-void
.end method
