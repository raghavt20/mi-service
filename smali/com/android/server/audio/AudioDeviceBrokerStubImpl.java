public class com.android.server.audio.AudioDeviceBrokerStubImpl implements com.android.server.audio.AudioDeviceBrokerStub {
	 /* .source "AudioDeviceBrokerStubImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private Boolean isappbleneed;
	 private android.media.audiofx.MiSound mMiSound;
	 /* # direct methods */
	 public com.android.server.audio.AudioDeviceBrokerStubImpl ( ) {
		 /* .locals 1 */
		 /* .line 20 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 24 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput-boolean v0, p0, Lcom/android/server/audio/AudioDeviceBrokerStubImpl;->isappbleneed:Z */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public java.lang.String getDeviceAttrAddr ( java.util.LinkedList p0, Integer p1, com.android.server.audio.AudioDeviceInventory p2 ) {
		 /* .locals 5 */
		 /* .param p2, "audiodeviceBLEHeadset" # I */
		 /* .param p3, "deviceinventory" # Lcom/android/server/audio/AudioDeviceInventory; */
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(", */
		 /* "Ljava/util/LinkedList<", */
		 /* "Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;", */
		 /* ">;I", */
		 /* "Lcom/android/server/audio/AudioDeviceInventory;", */
		 /* ")", */
		 /* "Ljava/lang/String;" */
		 /* } */
	 } // .end annotation
	 /* .line 61 */
	 /* .local p1, "mCommunicationRouteClients":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;>;" */
	 v0 = 	 (( java.util.LinkedList ) p1 ).isEmpty ( ); // invoke-virtual {p1}, Ljava/util/LinkedList;->isEmpty()Z
	 /* if-nez v0, :cond_1 */
	 /* .line 62 */
	 (( java.util.LinkedList ) p1 ).iterator ( ); // invoke-virtual {p1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;
	 /* .line 63 */
	 /* .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;>;" */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 64 */
	 /* check-cast v1, Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient; */
	 /* .line 65 */
	 /* .local v1, "cl":Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient; */
	 (( com.android.server.audio.AudioDeviceBroker$CommunicationRouteClient ) v1 ).getDevice ( ); // invoke-virtual {v1}, Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;->getDevice()Landroid/media/AudioDeviceAttributes;
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 (( com.android.server.audio.AudioDeviceBroker$CommunicationRouteClient ) v1 ).getDevice ( ); // invoke-virtual {v1}, Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;->getDevice()Landroid/media/AudioDeviceAttributes;
		 v2 = 		 (( android.media.AudioDeviceAttributes ) v2 ).getType ( ); // invoke-virtual {v2}, Landroid/media/AudioDeviceAttributes;->getType()I
		 /* if-ne v2, p2, :cond_0 */
		 /* .line 66 */
		 (( com.android.server.audio.AudioDeviceInventory ) p3 ).getLeAudioAddress ( ); // invoke-virtual {p3}, Lcom/android/server/audio/AudioDeviceInventory;->getLeAudioAddress()Ljava/lang/String;
		 /* .line 67 */
		 /* .local v2, "address":Ljava/lang/String; */
		 if ( v2 != null) { // if-eqz v2, :cond_0
			 /* .line 68 */
			 final String v3 = "AudioDeviceBrokerStubImpl"; // const-string v3, "AudioDeviceBrokerStubImpl"
			 final String v4 = "preferredCommunicationDevice, return ble device when ble is selected by other app"; // const-string v4, "preferredCommunicationDevice, return ble device when ble is selected by other app"
			 android.util.Log .v ( v3,v4 );
			 /* .line 69 */
			 /* .line 72 */
		 } // .end local v1 # "cl":Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;
	 } // .end local v2 # "address":Ljava/lang/String;
} // :cond_0
/* .line 74 */
} // .end local v0 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/audio/AudioDeviceBroker$CommunicationRouteClient;>;"
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isSetSpeakerphoneOn ( Boolean p0, Boolean p1, Integer p2, Boolean p3 ) {
/* .locals 4 */
/* .param p1, "isLeDeviceConnected" # Z */
/* .param p2, "speakerOn" # Z */
/* .param p3, "pid" # I */
/* .param p4, "isBuiltinSpkType" # Z */
/* .line 33 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* if-nez p2, :cond_0 */
/* .line 34 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setSpeakerphoneOn, on: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = " pid: "; // const-string v2, " pid: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "when ble is connect.getCommunicationRouteClientForPid directly to avoid waiting too long" */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "AudioDeviceBrokerStubImpl"; // const-string v3, "AudioDeviceBrokerStubImpl"
android.util.Log .v ( v3,v0 );
/* .line 36 */
if ( p4 != null) { // if-eqz p4, :cond_0
/* .line 37 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "when ble is connect return." */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .v ( v3,v0 );
/* .line 39 */
int v0 = 1; // const/4 v0, 0x1
/* .line 42 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void setBTBLEParameters ( android.media.AudioDeviceAttributes p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "preferredCommunicationDevice" # Landroid/media/AudioDeviceAttributes; */
/* .param p2, "audiodeviceBLEHeadset" # I */
/* .line 79 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 80 */
v0 = (( android.media.AudioDeviceAttributes ) p1 ).getType ( ); // invoke-virtual {p1}, Landroid/media/AudioDeviceAttributes;->getType()I
/* if-eq v0, p2, :cond_1 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/audio/AudioDeviceBrokerStubImpl;->isappbleneed:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 81 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/audio/AudioDeviceBrokerStubImpl;->isappbleneed:Z */
/* .line 82 */
final String v0 = "BT_BLE=off"; // const-string v0, "BT_BLE=off"
android.media.AudioSystem .setParameters ( v0 );
/* .line 83 */
} // :cond_1
if ( p1 != null) { // if-eqz p1, :cond_2
v0 = (( android.media.AudioDeviceAttributes ) p1 ).getType ( ); // invoke-virtual {p1}, Landroid/media/AudioDeviceAttributes;->getType()I
/* if-ne v0, p2, :cond_2 */
/* iget-boolean v0, p0, Lcom/android/server/audio/AudioDeviceBrokerStubImpl;->isappbleneed:Z */
/* if-nez v0, :cond_2 */
/* .line 84 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/audio/AudioDeviceBrokerStubImpl;->isappbleneed:Z */
/* .line 85 */
final String v0 = "BT_BLE=on"; // const-string v0, "BT_BLE=on"
android.media.AudioSystem .setParameters ( v0 );
/* .line 87 */
} // :cond_2
} // :goto_0
return;
} // .end method
public void setMiSoundEnable ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "enable" # Z */
/* .line 92 */
final String v0 = "AudioDeviceBrokerStubImpl"; // const-string v0, "AudioDeviceBrokerStubImpl"
try { // :try_start_0
/* new-instance v1, Landroid/media/audiofx/MiSound; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v1, v2, v2}, Landroid/media/audiofx/MiSound;-><init>(II)V */
this.mMiSound = v1;
/* .line 93 */
(( android.media.audiofx.MiSound ) v1 ).setEnabled ( p1 ); // invoke-virtual {v1, p1}, Landroid/media/audiofx/MiSound;->setEnabled(Z)I
/* :try_end_0 */
/* .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/RuntimeException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 101 */
v0 = this.mMiSound;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 102 */
} // :goto_0
(( android.media.audiofx.MiSound ) v0 ).release ( ); // invoke-virtual {v0}, Landroid/media/audiofx/MiSound;->release()V
/* .line 101 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 98 */
/* :catch_0 */
/* move-exception v1 */
/* .line 99 */
/* .local v1, "e":Ljava/lang/RuntimeException; */
try { // :try_start_1
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "updateMisoundTunning: RuntimeException" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v2 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 101 */
/* nop */
} // .end local v1 # "e":Ljava/lang/RuntimeException;
v0 = this.mMiSound;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 102 */
/* .line 96 */
/* :catch_1 */
/* move-exception v1 */
/* .line 97 */
/* .local v1, "e":Ljava/lang/IllegalArgumentException; */
try { // :try_start_2
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 /* const-string/jumbo v3, "updateMisoundTunning: IllegalArgumentException" */
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Log .e ( v0,v2 );
	 /* :try_end_2 */
	 /* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
	 /* .line 101 */
	 /* nop */
} // .end local v1 # "e":Ljava/lang/IllegalArgumentException;
v0 = this.mMiSound;
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 102 */
	 /* .line 94 */
	 /* :catch_2 */
	 /* move-exception v1 */
	 /* .line 95 */
	 /* .local v1, "e":Ljava/lang/UnsupportedOperationException; */
	 try { // :try_start_3
		 /* new-instance v2, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
		 /* const-string/jumbo v3, "updateMisoundTunning: UnsupportedOperationException" */
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Log .e ( v0,v2 );
		 /* :try_end_3 */
		 /* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
		 /* .line 101 */
		 /* nop */
	 } // .end local v1 # "e":Ljava/lang/UnsupportedOperationException;
	 v0 = this.mMiSound;
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 102 */
		 /* .line 105 */
	 } // :cond_0
} // :goto_1
return;
/* .line 101 */
} // :goto_2
v1 = this.mMiSound;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 102 */
(( android.media.audiofx.MiSound ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/media/audiofx/MiSound;->release()V
/* .line 104 */
} // :cond_1
/* throw v0 */
} // .end method
public void updateBtScoAudioHelper ( Boolean p0, Boolean p1, com.android.server.audio.BtHelper p2, Boolean p3 ) {
/* .locals 2 */
/* .param p1, "wasblescorequested" # Z */
/* .param p2, "isblescorequested" # Z */
/* .param p3, "bthelper" # Lcom/android/server/audio/BtHelper; */
/* .param p4, "iseventsourcestartswith" # Z */
/* .line 49 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setCommunicationRouteForClient for LEA " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AudioDeviceBrokerStubImpl"; // const-string v1, "AudioDeviceBrokerStubImpl"
android.util.Log .d ( v1,v0 );
/* .line 51 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* if-nez p2, :cond_0 */
/* if-nez p4, :cond_0 */
/* .line 52 */
(( com.android.server.audio.BtHelper ) p3 ).disconnectBluetoothScoAudioHelper ( ); // invoke-virtual {p3}, Lcom/android/server/audio/BtHelper;->disconnectBluetoothScoAudioHelper()Z
/* .line 53 */
} // :cond_0
/* if-nez p1, :cond_1 */
if ( p2 != null) { // if-eqz p2, :cond_1
/* if-nez p4, :cond_1 */
/* .line 54 */
(( com.android.server.audio.BtHelper ) p3 ).connectBluetoothScoAudioHelper ( ); // invoke-virtual {p3}, Lcom/android/server/audio/BtHelper;->connectBluetoothScoAudioHelper()Z
/* .line 56 */
} // :cond_1
} // :goto_0
return;
} // .end method
