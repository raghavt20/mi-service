.class public Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler$CatchingWorkerHandler;
.super Landroid/content/AsyncQueryHandler$WorkerHandler;
.source "AudioQueryWeatherService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "CatchingWorkerHandler"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;


# direct methods
.method public constructor <init>(Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$1"    # Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 324
    iput-object p1, p0, Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler$CatchingWorkerHandler;->this$1:Lcom/android/server/audio/AudioQueryWeatherService$QueryHandler;

    .line 325
    invoke-direct {p0, p1, p2}, Landroid/content/AsyncQueryHandler$WorkerHandler;-><init>(Landroid/content/AsyncQueryHandler;Landroid/os/Looper;)V

    .line 326
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .line 332
    const-string v0, "AudioQueryWeatherService"

    :try_start_0
    invoke-super {p0, p1}, Landroid/content/AsyncQueryHandler$WorkerHandler;->handleMessage(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/database/sqlite/SQLiteDatabaseCorruptException; {:try_start_0 .. :try_end_0} :catch_0

    .line 339
    :goto_0
    goto :goto_1

    .line 337
    :catch_0
    move-exception v1

    .line 338
    .local v1, "e":Landroid/database/sqlite/SQLiteDatabaseCorruptException;
    const-string v2, "Exception on background"

    invoke-static {v0, v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 335
    .end local v1    # "e":Landroid/database/sqlite/SQLiteDatabaseCorruptException;
    :catch_1
    move-exception v1

    .line 336
    .local v1, "e":Landroid/database/sqlite/SQLiteFullException;
    const-string v2, "Exception worker thread"

    invoke-static {v0, v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .end local v1    # "e":Landroid/database/sqlite/SQLiteFullException;
    goto :goto_0

    .line 333
    :catch_2
    move-exception v1

    .line 334
    .local v1, "e":Landroid/database/sqlite/SQLiteDiskIOException;
    const-string v2, "Exception background worker thread"

    invoke-static {v0, v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .end local v1    # "e":Landroid/database/sqlite/SQLiteDiskIOException;
    goto :goto_0

    .line 340
    :goto_1
    return-void
.end method
