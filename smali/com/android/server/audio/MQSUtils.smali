.class public Lcom/android/server/audio/MQSUtils;
.super Ljava/lang/Object;
.source "MQSUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/audio/MQSUtils$AudioStateTrackData;,
        Lcom/android/server/audio/MQSUtils$WaveformInfo;
    }
.end annotation


# static fields
.field private static final AUDIO_EVENT_ID:Ljava/lang/String; = "31000000086"

.field private static final AUDIO_EVENT_NAME:Ljava/lang/String; = "audio_button"

.field private static final AUDIO_VISUAL_ENABLED:I = 0x1

.field private static final FLAG_NON_ANONYMOUS:I = 0x2

.field private static final FLAG_NOT_LIMITED_BY_USER_EXPERIENCE_PLAN:I = 0x1

.field private static final HAPTIC_FEEDBACK_INFINITE_INTENSITY:Ljava/lang/String; = "haptic_feedback_infinite_intensity"

.field private static final IS_HEARING_ASSIST_SUPPORT:Ljava/lang/String; = "sound_transmit_ha_support"

.field private static final IS_TRANSMIT_SUPPORT:Ljava/lang/String; = "sound_transmit_support"

.field private static final KEY_PARAM_RINGTONE_DEVICE:Ljava/lang/String; = "set_spk_ring_filter_mask"

.field private static final ONE_TRACK_ACTION:Ljava/lang/String; = "onetrack.action.TRACK_EVENT"

.field private static final PACKAGE_NAME:Ljava/lang/String; = "com.miui.analytics"

.field private static final PARAM_RINGTONE_DEVICE_OFF:Ljava/lang/String; = "set_spk_ring_filter_mask=3"

.field private static final PARAM_RINGTONE_DEVICE_ON:Ljava/lang/String; = "set_spk_ring_filter_mask=0"

.field private static final SOUND_ASSIST_ENABLED:I = 0x1

.field private static final SUPPORT_HEARING_ASSIST:Ljava/lang/String; = "sound_transmit_ha_support=true"

.field private static final SUPPORT_TRANSMIT:Ljava/lang/String; = "sound_transmit_support=true"

.field private static final TAG:Ljava/lang/String; = "MiSound.MQSUtils"

.field private static VIBRATION_DEFAULT_INFINITE_INTENSITY:F = 0.0f

.field private static final VIBRATOR_EVENT_ID:Ljava/lang/String; = "31000000089"

.field private static final VIBRATOR_EVENT_NAME:Ljava/lang/String; = "haptic_status"

.field private static day:I = 0x0

.field private static mMqsModuleId:Ljava/lang/String; = null

.field private static final mMusicWhiteList:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static month:I = 0x0

.field private static final none:Ljava/lang/String; = "none"

.field private static year:I


# instance fields
.field private mContext:Landroid/content/Context;

.field private mMiuiXlog:Landroid/media/MiuiXlog;


# direct methods
.method static constructor <clinit>()V
    .locals 50

    .line 39
    const-string v0, "mqs_audio_data_21031000"

    sput-object v0, Lcom/android/server/audio/MQSUtils;->mMqsModuleId:Ljava/lang/String;

    .line 41
    const/high16 v0, 0x3f800000    # 1.0f

    sput v0, Lcom/android/server/audio/MQSUtils;->VIBRATION_DEFAULT_INFINITE_INTENSITY:F

    .line 60
    new-instance v0, Ljava/util/HashSet;

    const-string v1, "com.netease.cloudmusic"

    const-string v2, "com.tencent.qqmusic"

    const-string v3, "com.iloen.melon"

    const-string v4, "mp3.player.freemusic"

    const-string v5, "com.kugou.android"

    const-string v6, "cn.kuwo.player"

    const-string v7, "com.google.android.apps.youtube.music"

    const-string v8, "com.tencent.blackkey"

    const-string v9, "cmccwm.mobilemusic"

    const-string v10, "com.migu.music.mini"

    const-string v11, "com.ting.mp3.android"

    const-string v12, "com.blueocean.musicplayer"

    const-string v13, "com.tencent.ibg.joox"

    const-string v14, "com.kugou.android.ringtone"

    const-string v15, "com.shoujiduoduo.dj"

    const-string v16, "com.spotify.music"

    const-string v17, "com.shoujiduoduo.ringtone"

    const-string v18, "com.hiby.music"

    const-string v19, "com.miui.player"

    const-string v20, "com.google.android.music"

    const-string v21, "com.tencent.ibg.joox"

    const-string v22, "com.skysoft.kkbox.android"

    const-string v23, "com.sofeh.android.musicstudio3"

    const-string v24, "com.gamestar.perfectpiano"

    const-string v25, "com.opalastudios.pads"

    const-string v26, "com.magix.android.mmjam"

    const-string v27, "com.musicplayer.playermusic"

    const-string v28, "com.gaana"

    const-string v29, "com.maxmpz.audioplayer"

    const-string v30, "com.melodis.midomiMusicIdentifier.freemium"

    const-string v31, "com.mixvibes.remixlive"

    const-string v32, "com.starmakerinteractive.starmaker"

    const-string v33, "com.smule.singandroid"

    const-string v34, "com.djit.apps.stream"

    const-string/jumbo v35, "tunein.service"

    const-string v36, "com.shazam.android"

    const-string v37, "com.jangomobile.android"

    const-string v38, "com.pandoralite"

    const-string v39, "com.tube.hqmusic"

    const-string v40, "com.amazon.avod.thirdpartyclient"

    const-string v41, "com.atmusic.app"

    const-string v42, "com.rubycell.pianisthd"

    const-string v43, "com.agminstruments.drumpadmachine"

    const-string v44, "com.playermusic.musicplayerapp"

    const-string v45, "com.famousbluemedia.piano"

    const-string v46, "com.apple.android.music"

    const-string v47, "mb32r.musica.gratis.music.player.free.download"

    const-string v48, "com.famousbluemedia.yokee"

    const-string v49, "com.ss.android.ugc.trill"

    filled-new-array/range {v1 .. v49}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/android/server/audio/MQSUtils;->mMusicWhiteList:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Landroid/media/MiuiXlog;

    invoke-direct {v0}, Landroid/media/MiuiXlog;-><init>()V

    iput-object v0, p0, Lcom/android/server/audio/MQSUtils;->mMiuiXlog:Landroid/media/MiuiXlog;

    .line 90
    iput-object p1, p0, Lcom/android/server/audio/MQSUtils;->mContext:Landroid/content/Context;

    .line 91
    return-void
.end method

.method private check3DAudioSwitchStatus()Ljava/lang/String;
    .locals 11

    .line 296
    iget-object v0, p0, Lcom/android/server/audio/MQSUtils;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 297
    .local v0, "mAudioManager":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->getSpatializer()Landroid/media/Spatializer;

    move-result-object v1

    .line 298
    .local v1, "mSpatializer":Landroid/media/Spatializer;
    invoke-virtual {v1}, Landroid/media/Spatializer;->getImmersiveAudioLevel()I

    move-result v2

    const-string v3, "open"

    const-string v4, "close"

    const-string v5, "3D Audio Switch status is :"

    const-string v6, "none"

    const-string v7, "device not support 3D audio"

    const/4 v8, 0x1

    const-string v9, "MiSound.MQSUtils"

    const/4 v10, 0x0

    if-lez v2, :cond_3

    .line 299
    const-string v2, "ro.audio.spatializer_enabled"

    invoke-static {v2, v10}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 300
    .local v2, "spatializer_enabled":Z
    if-nez v2, :cond_0

    .line 302
    invoke-static {v9, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    return-object v6

    .line 305
    :cond_0
    iget-object v6, p0, Lcom/android/server/audio/MQSUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string/jumbo v7, "spatial_audio_feature_enable"

    invoke-static {v6, v7, v10}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-ne v6, v8, :cond_1

    goto :goto_0

    :cond_1
    move v8, v10

    :goto_0
    move v6, v8

    .line 306
    .local v6, "status":Z
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v9, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    if-eqz v6, :cond_2

    .line 308
    return-object v3

    .line 310
    :cond_2
    return-object v4

    .line 313
    .end local v2    # "spatializer_enabled":Z
    .end local v6    # "status":Z
    :cond_3
    const-string v2, "ro.vendor.audio.feature.spatial"

    invoke-static {v2, v10}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 314
    .local v2, "flag":I
    if-eqz v2, :cond_6

    if-ne v8, v2, :cond_4

    goto :goto_1

    .line 319
    :cond_4
    const-string v6, "persist.vendor.audio.3dsurround.enable"

    invoke-static {v6, v10}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    .line 320
    .restart local v6    # "status":Z
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v9, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    if-eqz v6, :cond_5

    .line 322
    return-object v3

    .line 324
    :cond_5
    return-object v4

    .line 316
    .end local v6    # "status":Z
    :cond_6
    :goto_1
    invoke-static {v9, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    return-object v6
.end method

.method private checkAllowSpeakerToRing()Ljava/lang/String;
    .locals 3

    .line 249
    const-string v0, "ro.vendor.audio.ring.filter"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 250
    const-string v0, "MiSound.MQSUtils"

    const-string v1, "device not support AllowSpeakerToRing"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    const-string v0, "none"

    return-object v0

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/android/server/audio/MQSUtils;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 255
    .local v0, "mAudioManager":Landroid/media/AudioManager;
    const-string/jumbo v1, "set_spk_ring_filter_mask"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 257
    .local v1, "paramStr":Ljava/lang/String;
    const-string/jumbo v2, "set_spk_ring_filter_mask=0"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 258
    const-string v2, "open"

    return-object v2

    .line 259
    :cond_1
    const-string/jumbo v2, "set_spk_ring_filter_mask=3"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 260
    const-string v2, "close"

    return-object v2

    .line 262
    :cond_2
    const-string v2, "Undefined"

    return-object v2
.end method

.method private checkAudioVisualStatus()Ljava/lang/String;
    .locals 3

    .line 192
    const-string v0, "ro.vendor.audio.sfx.audiovisual"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 193
    const-string v0, "MiSound.MQSUtils"

    const-string v1, "device not support AudioVisual"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    const-string v0, "none"

    return-object v0

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/android/server/audio/MQSUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "audio_visual_screen_lock_on"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 197
    const-string v0, "open"

    return-object v0

    .line 199
    :cond_1
    const-string v0, "close"

    return-object v0
.end method

.method private checkDolbySwitchStatus()Ljava/lang/String;
    .locals 4

    .line 282
    const-string v0, "ro.vendor.audio.dolby.dax.support"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    const-string v2, "MiSound.MQSUtils"

    if-nez v0, :cond_0

    .line 283
    const-string v0, "device not support dolby audio"

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    const-string v0, "none"

    return-object v0

    .line 286
    :cond_0
    const-string v0, "persist.vendor.audio.misound.disable"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 287
    .local v0, "status":Z
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Dolby Switch status is :"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    if-eqz v0, :cond_1

    .line 289
    const-string v1, "open"

    return-object v1

    .line 291
    :cond_1
    const-string v1, "close"

    return-object v1
.end method

.method private checkEarsCompensationStatus()Ljava/lang/String;
    .locals 4

    .line 268
    const-string v0, "ro.vendor.audio.sfx.earadj"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    const-string v1, "MiSound.MQSUtils"

    if-nez v0, :cond_0

    .line 269
    const-string v0, "device not support EarsCompensation"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    const-string v0, "none"

    return-object v0

    .line 272
    :cond_0
    const-string v0, "persist.vendor.audio.ears.compensation.state"

    const-string v2, ""

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 273
    .local v0, "status":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ears compensation status is :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 275
    const-string v1, "open"

    return-object v1

    .line 277
    :cond_1
    const-string v1, "close"

    return-object v1
.end method

.method private checkHIFIStatus()Ljava/lang/String;
    .locals 4

    .line 226
    const-string v0, "ro.vendor.audio.hifi"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    const-string v2, "MiSound.MQSUtils"

    if-nez v0, :cond_0

    .line 227
    const-string v0, "device not support HIFI"

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    const-string v0, "none"

    return-object v0

    .line 230
    :cond_0
    const-string v0, "persist.vendor.audio.hifi"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 231
    .local v0, "status":Z
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "HiFi Switch status is :"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 233
    const-string v1, "open"

    return-object v1

    .line 235
    :cond_1
    const-string v1, "close"

    return-object v1
.end method

.method private checkHapticStatus()Ljava/lang/String;
    .locals 4

    .line 562
    iget-object v0, p0, Lcom/android/server/audio/MQSUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/16 v1, 0x3e8

    const/4 v2, -0x2

    const-string v3, "haptic_feedback_enabled"

    invoke-static {v0, v3, v1, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    .line 563
    .local v0, "status":I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 564
    const-string v1, "open"

    return-object v1

    .line 565
    :cond_0
    if-nez v0, :cond_1

    .line 566
    const-string v1, "close"

    return-object v1

    .line 568
    :cond_1
    const-string v1, "none"

    return-object v1
.end method

.method private checkHarmankardonStatus()Ljava/lang/String;
    .locals 3

    .line 204
    const-string v0, "ro.vendor.audio.sfx.harmankardon"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 205
    const-string v0, "MiSound.MQSUtils"

    const-string v1, "device not support harmankardon"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    const-string v0, "none"

    return-object v0

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/android/server/audio/MQSUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v2, "settings_system_harman_kardon_enable"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 209
    .local v0, "flag":I
    const/4 v1, 0x1

    if-ne v1, v0, :cond_1

    .line 210
    const-string v1, "open"

    return-object v1

    .line 212
    :cond_1
    const-string v1, "close"

    return-object v1
.end method

.method private checkHearingAssistSupport()Z
    .locals 4

    .line 175
    iget-object v0, p0, Lcom/android/server/audio/MQSUtils;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 176
    .local v0, "audioManager":Landroid/media/AudioManager;
    const-string/jumbo v1, "sound_transmit_ha_support"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 177
    .local v1, "hearingAssistSupport":Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    if-lt v2, v3, :cond_1

    const-string/jumbo v2, "sound_transmit_ha_support=true"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 180
    :cond_0
    return v3

    .line 178
    :cond_1
    :goto_0
    const/4 v2, 0x0

    return v2
.end method

.method private checkMisoundEqStatus()Ljava/lang/String;
    .locals 5

    .line 377
    const-string v0, "persist.vendor.audio.sfx.hd.eq"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 378
    .local v0, "eq":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "misound eq is :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiSound.MQSUtils"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 381
    const-string v1, "null"

    .local v1, "status":Ljava/lang/String;
    goto :goto_0

    .line 382
    .end local v1    # "status":Ljava/lang/String;
    :cond_0
    const-string v1, "0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 383
    const-string v1, "close"

    .restart local v1    # "status":Ljava/lang/String;
    goto :goto_0

    .line 385
    .end local v1    # "status":Ljava/lang/String;
    :cond_1
    const-string v1, "open"

    .line 388
    .restart local v1    # "status":Ljava/lang/String;
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "misound eq status is :"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    return-object v1
.end method

.method private checkMisoundHeadphoneType()Ljava/lang/String;
    .locals 3

    .line 371
    const-string v0, "persist.vendor.audio.sfx.hd.type"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 372
    .local v0, "type":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "misound headphone type is :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiSound.MQSUtils"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    return-object v0
.end method

.method private checkMisoundStatus()Ljava/lang/String;
    .locals 3

    .line 359
    const-string v0, "persist.vendor.audio.sfx.hd.music.state"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 360
    .local v0, "status":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "misound status is :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiSound.MQSUtils"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 362
    const-string v1, "open"

    return-object v1

    .line 363
    :cond_0
    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 364
    const-string v1, "close"

    return-object v1

    .line 366
    :cond_1
    const-string v1, "none"

    return-object v1
.end method

.method private checkMultiAppVolumeStatus()Ljava/lang/String;
    .locals 3

    .line 217
    iget-object v0, p0, Lcom/android/server/audio/MQSUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "sound_assist_key"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 218
    .local v0, "status":I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 219
    const-string v1, "open"

    return-object v1

    .line 221
    :cond_0
    const-string v1, "close"

    return-object v1
.end method

.method private checkMultiSoundStatus()Ljava/lang/String;
    .locals 3

    .line 240
    iget-object v0, p0, Lcom/android/server/audio/MQSUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "key_ignore_music_focus_req"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 241
    .local v0, "status":I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 242
    const-string v1, "open"

    return-object v1

    .line 244
    :cond_0
    const-string v1, "close"

    return-object v1
.end method

.method private checkSpatialAudioSwitchStatus()Ljava/lang/String;
    .locals 9

    .line 330
    iget-object v0, p0, Lcom/android/server/audio/MQSUtils;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 331
    .local v0, "mAudioManager":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->getSpatializer()Landroid/media/Spatializer;

    move-result-object v1

    .line 332
    .local v1, "mSpatializer":Landroid/media/Spatializer;
    invoke-virtual {v1}, Landroid/media/Spatializer;->getImmersiveAudioLevel()I

    move-result v2

    const-string v3, "open"

    const-string v4, "close"

    const-string v5, "Spatial Audio Switch status is :"

    const-string v6, "MiSound.MQSUtils"

    const/4 v7, 0x0

    if-lez v2, :cond_2

    .line 333
    invoke-virtual {v1}, Landroid/media/Spatializer;->getDesiredHeadTrackingMode()I

    move-result v2

    .line 334
    .local v2, "mode":I
    const/4 v8, 0x1

    if-ne v2, v8, :cond_0

    move v7, v8

    .line 335
    .local v7, "status":Z
    :cond_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    if-eqz v7, :cond_1

    .line 337
    return-object v3

    .line 339
    :cond_1
    return-object v4

    .line 342
    .end local v2    # "mode":I
    .end local v7    # "status":Z
    :cond_2
    const-string v2, "ro.vendor.audio.feature.spatial"

    invoke-static {v2, v7}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 343
    .local v2, "flag":I
    if-eqz v2, :cond_5

    const/4 v8, 0x2

    if-ne v8, v2, :cond_3

    goto :goto_0

    .line 348
    :cond_3
    const-string v8, "persist.vendor.audio.spatial.enable"

    invoke-static {v8, v7}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    .line 349
    .restart local v7    # "status":Z
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    if-eqz v7, :cond_4

    .line 351
    return-object v3

    .line 353
    :cond_4
    return-object v4

    .line 345
    .end local v7    # "status":Z
    :cond_5
    :goto_0
    const-string v3, "device not support spatial audio"

    invoke-static {v6, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    const-string v3, "none"

    return-object v3
.end method

.method private checkVoiceprintNoiseReductionSupport()Z
    .locals 2

    .line 184
    const-string v0, "ro.vendor.audio.voip.assistant"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 185
    const/4 v0, 0x1

    return v0

    .line 187
    :cond_0
    return v1
.end method

.method private checkWirelessTransmissionSupport()Z
    .locals 4

    .line 166
    iget-object v0, p0, Lcom/android/server/audio/MQSUtils;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 167
    .local v0, "audioManager":Landroid/media/AudioManager;
    const-string/jumbo v1, "sound_transmit_support"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 168
    .local v1, "transmitSupport":Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    if-lt v2, v3, :cond_1

    const-string/jumbo v2, "sound_transmit_support=true"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 171
    :cond_0
    return v3

    .line 169
    :cond_1
    :goto_0
    const/4 v2, 0x0

    return v2
.end method

.method private static configureIntent(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 3
    .param p0, "appId"    # Ljava/lang/String;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "params"    # Landroid/os/Bundle;

    .line 143
    new-instance v0, Landroid/content/Intent;

    const-string v1, "onetrack.action.TRACK_EVENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 144
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.miui.analytics"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 145
    const-string v1, "PACKAGE"

    const-string v2, "android"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 146
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 147
    const-string v1, "APP_ID"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 148
    const-string v1, "EVENT_NAME"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 149
    invoke-virtual {v0, p2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 151
    return-object v0
.end method

.method private getHapticFeedbackFloatLevel()F
    .locals 4

    .line 573
    iget-object v0, p0, Lcom/android/server/audio/MQSUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget v1, Lcom/android/server/audio/MQSUtils;->VIBRATION_DEFAULT_INFINITE_INTENSITY:F

    const/4 v2, -0x2

    const-string v3, "haptic_feedback_infinite_intensity"

    invoke-static {v0, v3, v1, v2}, Landroid/provider/Settings$System;->getFloatForUser(Landroid/content/ContentResolver;Ljava/lang/String;FI)F

    move-result v0

    return v0
.end method

.method private isGlobalBuild()Z
    .locals 1

    .line 658
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    return v0
.end method

.method private isNotAllowedRegion()Z
    .locals 4

    .line 662
    const-string v0, "ro.miui.region"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 663
    .local v0, "region":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "the region is :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiSound.MQSUtils"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 664
    const-string v2, "IN"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    return v1
.end method

.method private isReportXiaomiServer()Z
    .locals 3

    .line 668
    const-string v0, "ro.miui.region"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 669
    .local v0, "region":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "the region is :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiSound.MQSUtils"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 670
    const-string v1, "CN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "RU"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    return v1
.end method

.method public static trackConcurrentVoipInfo(Landroid/content/Context;Ljava/util/List;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .line 94
    .local p1, "clients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    if-eqz p0, :cond_2

    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 99
    :cond_0
    :try_start_0
    new-instance v1, Lcom/android/server/audio/MQSUtils;

    invoke-direct {v1, p0}, Lcom/android/server/audio/MQSUtils;-><init>(Landroid/content/Context;)V

    .line 100
    .local v1, "util":Lcom/android/server/audio/MQSUtils;
    invoke-direct {v1}, Lcom/android/server/audio/MQSUtils;->isReportXiaomiServer()Z

    move-result v2

    if-nez v2, :cond_1

    .line 101
    return v0

    .line 104
    :cond_1
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 105
    .local v2, "params":Landroid/os/Bundle;
    const-string/jumbo v3, "voip_packages"

    invoke-interface {p1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    const-string v3, "31000000086"

    const-string v4, "concurrent_voip"

    invoke-static {v3, v4, v2}, Lcom/android/server/audio/MQSUtils;->configureIntent(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    const/4 v0, 0x1

    return v0

    .line 108
    .end local v1    # "util":Lcom/android/server/audio/MQSUtils;
    .end local v2    # "params":Landroid/os/Bundle;
    :catch_0
    move-exception v1

    .line 109
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "trackConcurrentVoipInfo exception : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiSound.MQSUtils"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    .end local v1    # "e":Ljava/lang/Exception;
    return v0

    .line 95
    :cond_2
    :goto_0
    return v0
.end method


# virtual methods
.method public needToReport()Z
    .locals 6

    .line 155
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 156
    .local v0, "calendar":Ljava/util/Calendar;
    sget v1, Lcom/android/server/audio/MQSUtils;->year:I

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v4, 0x5

    const/4 v5, 0x2

    if-ne v1, v3, :cond_0

    sget v1, Lcom/android/server/audio/MQSUtils;->month:I

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    add-int/2addr v3, v2

    if-ne v1, v3, :cond_0

    sget v1, Lcom/android/server/audio/MQSUtils;->day:I

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ne v1, v3, :cond_0

    .line 157
    const/4 v1, 0x0

    return v1

    .line 158
    :cond_0
    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    sput v1, Lcom/android/server/audio/MQSUtils;->year:I

    .line 159
    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v1

    add-int/2addr v1, v2

    sput v1, Lcom/android/server/audio/MQSUtils;->month:I

    .line 160
    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    sput v1, Lcom/android/server/audio/MQSUtils;->day:I

    .line 161
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "needToReport year: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v3, Lcom/android/server/audio/MQSUtils;->year:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "month: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v3, Lcom/android/server/audio/MQSUtils;->month:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "day: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v3, Lcom/android/server/audio/MQSUtils;->day:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "MiSound.MQSUtils"

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    return v2
.end method

.method public reportAbnormalAudioStatus(Lcom/android/server/audio/MQSUtils$AudioStateTrackData;)V
    .locals 9
    .param p1, "audioStateTrackData"    # Lcom/android/server/audio/MQSUtils$AudioStateTrackData;

    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[TF-BT] reportAbnormalAudioStatus: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiSound.MQSUtils"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    iget-object v0, p0, Lcom/android/server/audio/MQSUtils;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_2

    if-nez p1, :cond_0

    goto :goto_1

    .line 119
    :cond_0
    invoke-direct {p0}, Lcom/android/server/audio/MQSUtils;->isReportXiaomiServer()Z

    move-result v0

    if-nez v0, :cond_1

    .line 120
    return-void

    .line 122
    :cond_1
    iget-object v2, p1, Lcom/android/server/audio/MQSUtils$AudioStateTrackData;->mAbnormalReason:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/server/audio/MQSUtils$AudioStateTrackData;->mEventSource:Ljava/lang/String;

    iget-object v4, p1, Lcom/android/server/audio/MQSUtils$AudioStateTrackData;->mPackageName:Ljava/lang/String;

    iget-object v5, p1, Lcom/android/server/audio/MQSUtils$AudioStateTrackData;->mBtName:Ljava/lang/String;

    iget v0, p1, Lcom/android/server/audio/MQSUtils$AudioStateTrackData;->mA2dpConnectState:I

    .line 133
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iget v0, p1, Lcom/android/server/audio/MQSUtils$AudioStateTrackData;->mScoConnectState:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    iget-object v8, p1, Lcom/android/server/audio/MQSUtils$AudioStateTrackData;->mAudioStateName:Ljava/lang/String;

    filled-new-array/range {v2 .. v8}, [Ljava/lang/Object;

    move-result-object v0

    .line 122
    const-string/jumbo v2, "{\"name\":\"check_audio_route_for_bluetooth\",\"audio_event\":{\"audio_route_abnormal_reason\":\"%s\",\"audio_exception_details\":\"%s\",\"package_name\":\"%s\",\"btName\":\"%s\",\"a2dp_connect_state\":\"%d\",\"sco_connect_state\":\"%d\",\"audio_status_name\":\"%s\"},\"dgt\":\"null\",\"audio_ext\":\"null\" }"

    invoke-static {v2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 136
    .local v0, "result":Ljava/lang/String;
    :try_start_0
    iget-object v2, p0, Lcom/android/server/audio/MQSUtils;->mMiuiXlog:Landroid/media/MiuiXlog;

    invoke-virtual {v2, v0}, Landroid/media/MiuiXlog;->miuiXlogSend(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 139
    goto :goto_0

    .line 137
    :catch_0
    move-exception v2

    .line 138
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[TF-BT] reportAbnormalAudioStatus exception : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 117
    .end local v0    # "result":Ljava/lang/String;
    :cond_2
    :goto_1
    return-void
.end method

.method public reportAudioButtonStatus()V
    .locals 21

    .line 410
    const-string v0, "open"

    const-string v1, "reportAudioButtonStatus start."

    const-string v2, "MiSound.MQSUtils"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 411
    invoke-direct/range {p0 .. p0}, Lcom/android/server/audio/MQSUtils;->checkAudioVisualStatus()Ljava/lang/String;

    move-result-object v1

    .line 412
    .local v1, "audio_visual_status":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/android/server/audio/MQSUtils;->checkMultiAppVolumeStatus()Ljava/lang/String;

    move-result-object v3

    .line 413
    .local v3, "multi_app_volume_button":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/android/server/audio/MQSUtils;->checkHIFIStatus()Ljava/lang/String;

    move-result-object v4

    .line 414
    .local v4, "HIFI_button":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/android/server/audio/MQSUtils;->checkMultiSoundStatus()Ljava/lang/String;

    move-result-object v5

    .line 415
    .local v5, "multi_sound_button":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/android/server/audio/MQSUtils;->checkAllowSpeakerToRing()Ljava/lang/String;

    move-result-object v6

    .line 416
    .local v6, "allow_speaker_to_ring_button":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/android/server/audio/MQSUtils;->checkEarsCompensationStatus()Ljava/lang/String;

    move-result-object v7

    .line 417
    .local v7, "ears_compensation_button":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/android/server/audio/MQSUtils;->checkDolbySwitchStatus()Ljava/lang/String;

    move-result-object v8

    .line 418
    .local v8, "dolby_button":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/android/server/audio/MQSUtils;->check3DAudioSwitchStatus()Ljava/lang/String;

    move-result-object v9

    .line 419
    .local v9, "_3D_audio_button":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/android/server/audio/MQSUtils;->checkSpatialAudioSwitchStatus()Ljava/lang/String;

    move-result-object v10

    .line 420
    .local v10, "spatial_audio_button":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/android/server/audio/MQSUtils;->checkMisoundStatus()Ljava/lang/String;

    move-result-object v11

    .line 421
    .local v11, "misound_status_button":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/android/server/audio/MQSUtils;->checkMisoundEqStatus()Ljava/lang/String;

    move-result-object v12

    .line 422
    .local v12, "misound_eq_button":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/android/server/audio/MQSUtils;->checkHarmankardonStatus()Ljava/lang/String;

    move-result-object v13

    .line 423
    .local v13, "harmankardon_status":Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "ears compensation button is :"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v2, v14}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 426
    :try_start_0
    new-instance v14, Landroid/content/Intent;

    const-string v15, "onetrack.action.TRACK_EVENT"

    invoke-direct {v14, v15}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 427
    .local v14, "intent":Landroid/content/Intent;
    invoke-direct/range {p0 .. p0}, Lcom/android/server/audio/MQSUtils;->isGlobalBuild()Z

    move-result v15
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_a

    move-object/from16 v16, v2

    const/4 v2, 0x2

    if-nez v15, :cond_0

    .line 428
    :try_start_1
    invoke-virtual {v14, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 548
    .end local v14    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    move-object v2, v10

    move-object v10, v11

    move-object v11, v12

    move-object/from16 v12, p0

    goto/16 :goto_11

    .line 430
    .restart local v14    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    :try_start_2
    const-string v15, "com.miui.analytics"

    invoke-virtual {v14, v15}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 431
    const-string v15, "APP_ID"

    const-string v2, "31000000086"

    invoke-virtual {v14, v15, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 432
    const-string v2, "EVENT_NAME"

    const-string v15, "audio_button"

    invoke-virtual {v14, v2, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 433
    const-string v2, "PACKAGE"

    const-string v15, "android"

    invoke-virtual {v14, v2, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 434
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 435
    .local v2, "params":Landroid/os/Bundle;
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_9

    move-object/from16 v17, v2

    .end local v2    # "params":Landroid/os/Bundle;
    .local v17, "params":Landroid/os/Bundle;
    const-string v2, "audio_visual_status"

    move-object/from16 v18, v12

    .end local v12    # "misound_eq_button":Ljava/lang/String;
    .local v18, "misound_eq_button":Ljava/lang/String;
    const-string v12, "none"

    move-object/from16 v19, v11

    .end local v11    # "misound_status_button":Ljava/lang/String;
    .local v19, "misound_status_button":Ljava/lang/String;
    const-string v11, "close"

    move-object/from16 v20, v10

    .end local v10    # "spatial_audio_button":Ljava/lang/String;
    .local v20, "spatial_audio_button":Ljava/lang/String;
    const/4 v10, 0x1

    if-eqz v15, :cond_1

    .line 436
    :try_start_3
    invoke-virtual {v14, v2, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 548
    .end local v14    # "intent":Landroid/content/Intent;
    .end local v17    # "params":Landroid/os/Bundle;
    :catch_1
    move-exception v0

    move-object/from16 v12, p0

    move-object/from16 v11, v18

    move-object/from16 v10, v19

    move-object/from16 v2, v20

    goto/16 :goto_11

    .line 437
    .restart local v14    # "intent":Landroid/content/Intent;
    .restart local v17    # "params":Landroid/os/Bundle;
    :cond_1
    :try_start_4
    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_8

    if-eqz v15, :cond_2

    .line 438
    const/4 v15, 0x0

    :try_start_5
    invoke-virtual {v14, v2, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    .line 440
    :cond_2
    :try_start_6
    invoke-virtual {v14, v2, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 443
    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_8

    const-string v15, "multi_app_volume_button"

    if-eqz v2, :cond_3

    .line 444
    :try_start_7
    invoke-virtual {v14, v15, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_2

    .line 446
    :cond_3
    const/4 v2, 0x0

    :try_start_8
    invoke-virtual {v14, v15, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 449
    :goto_2
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_8

    const-string v15, "HIFI_button"

    if-eqz v2, :cond_4

    .line 450
    :try_start_9
    invoke-virtual {v14, v15, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    goto :goto_3

    .line 451
    :cond_4
    :try_start_a
    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_8

    if-eqz v2, :cond_5

    .line 452
    const/4 v2, 0x0

    :try_start_b
    invoke-virtual {v14, v15, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1

    goto :goto_3

    .line 454
    :cond_5
    :try_start_c
    invoke-virtual {v14, v15, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 457
    :goto_3
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_8

    const-string v15, "multi_sound_button"

    if-eqz v2, :cond_6

    .line 458
    :try_start_d
    invoke-virtual {v14, v15, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_1

    goto :goto_4

    .line 460
    :cond_6
    const/4 v2, 0x0

    :try_start_e
    invoke-virtual {v14, v15, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 463
    :goto_4
    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_8

    const-string v15, "allow_speaker_to_ring_button"

    if-eqz v2, :cond_7

    .line 464
    :try_start_f
    invoke-virtual {v14, v15, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_1

    goto :goto_5

    .line 465
    :cond_7
    :try_start_10
    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_8

    if-eqz v2, :cond_8

    .line 466
    const/4 v2, 0x0

    :try_start_11
    invoke-virtual {v14, v15, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_1

    goto :goto_5

    .line 468
    :cond_8
    :try_start_12
    invoke-virtual {v14, v15, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 471
    :goto_5
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_8

    const-string v15, "hear_compensation_state"

    if-eqz v2, :cond_9

    .line 472
    :try_start_13
    invoke-virtual {v14, v15, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_1

    goto :goto_6

    .line 473
    :cond_9
    :try_start_14
    invoke-virtual {v7, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_8

    if-eqz v2, :cond_a

    .line 474
    const/4 v2, 0x0

    :try_start_15
    invoke-virtual {v14, v15, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_1

    goto :goto_6

    .line 476
    :cond_a
    :try_start_16
    invoke-virtual {v14, v15, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 479
    :goto_6
    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_8

    const-string v15, "harmankardon_status"

    if-eqz v2, :cond_b

    .line 480
    :try_start_17
    invoke-virtual {v14, v15, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_1

    goto :goto_7

    .line 481
    :cond_b
    :try_start_18
    invoke-virtual {v13, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_8

    if-eqz v2, :cond_c

    .line 482
    const/4 v2, 0x0

    :try_start_19
    invoke-virtual {v14, v15, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_1

    goto :goto_7

    .line 484
    :cond_c
    :try_start_1a
    invoke-virtual {v14, v15, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 487
    :goto_7
    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_8

    const-string v15, "dolby_state"

    if-eqz v2, :cond_d

    .line 488
    :try_start_1b
    invoke-virtual {v14, v15, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    :try_end_1b
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1b} :catch_1

    goto :goto_8

    .line 489
    :cond_d
    :try_start_1c
    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2
    :try_end_1c
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1c} :catch_8

    if-eqz v2, :cond_e

    .line 490
    const/4 v2, 0x0

    :try_start_1d
    invoke-virtual {v14, v15, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    :try_end_1d
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_1d} :catch_1

    goto :goto_8

    .line 492
    :cond_e
    :try_start_1e
    invoke-virtual {v14, v15, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 495
    :goto_8
    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_1e} :catch_8

    const-string v15, "audio_3D_state"

    if-eqz v2, :cond_f

    .line 496
    :try_start_1f
    invoke-virtual {v14, v15, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_1f} :catch_1

    goto :goto_9

    .line 497
    :cond_f
    :try_start_20
    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2
    :try_end_20
    .catch Ljava/lang/Exception; {:try_start_20 .. :try_end_20} :catch_8

    if-eqz v2, :cond_10

    .line 498
    const/4 v2, 0x0

    :try_start_21
    invoke-virtual {v14, v15, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    :try_end_21
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_21} :catch_1

    goto :goto_9

    .line 500
    :cond_10
    :try_start_22
    invoke-virtual {v14, v15, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_22
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_22} :catch_8

    .line 503
    :goto_9
    move-object/from16 v2, v20

    .end local v20    # "spatial_audio_button":Ljava/lang/String;
    .local v2, "spatial_audio_button":Ljava/lang/String;
    :try_start_23
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15
    :try_end_23
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_23} :catch_7

    const-string/jumbo v10, "spatial_audio_state"

    if-eqz v15, :cond_11

    .line 504
    const/4 v11, 0x1

    :try_start_24
    invoke-virtual {v14, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    :try_end_24
    .catch Ljava/lang/Exception; {:try_start_24 .. :try_end_24} :catch_2

    goto :goto_a

    .line 548
    .end local v14    # "intent":Landroid/content/Intent;
    .end local v17    # "params":Landroid/os/Bundle;
    :catch_2
    move-exception v0

    move-object/from16 v12, p0

    move-object/from16 v11, v18

    move-object/from16 v10, v19

    goto/16 :goto_11

    .line 505
    .restart local v14    # "intent":Landroid/content/Intent;
    .restart local v17    # "params":Landroid/os/Bundle;
    :cond_11
    :try_start_25
    invoke-virtual {v2, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11
    :try_end_25
    .catch Ljava/lang/Exception; {:try_start_25 .. :try_end_25} :catch_7

    if-eqz v11, :cond_12

    .line 506
    const/4 v11, 0x0

    :try_start_26
    invoke-virtual {v14, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    :try_end_26
    .catch Ljava/lang/Exception; {:try_start_26 .. :try_end_26} :catch_2

    goto :goto_a

    .line 508
    :cond_12
    :try_start_27
    invoke-virtual {v14, v10, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_27
    .catch Ljava/lang/Exception; {:try_start_27 .. :try_end_27} :catch_7

    .line 511
    :goto_a
    move-object/from16 v10, v19

    .end local v19    # "misound_status_button":Ljava/lang/String;
    .local v10, "misound_status_button":Ljava/lang/String;
    :try_start_28
    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11
    :try_end_28
    .catch Ljava/lang/Exception; {:try_start_28 .. :try_end_28} :catch_6

    const-string v12, "misound_status_button"

    if-eqz v11, :cond_13

    .line 512
    const/4 v11, 0x1

    :try_start_29
    invoke-virtual {v14, v12, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 513
    invoke-direct/range {p0 .. p0}, Lcom/android/server/audio/MQSUtils;->checkMisoundHeadphoneType()Ljava/lang/String;

    move-result-object v11

    .line 514
    .local v11, "type":Ljava/lang/String;
    const-string v12, "misound_headphone_type"

    invoke-virtual {v14, v12, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_29
    .catch Ljava/lang/Exception; {:try_start_29 .. :try_end_29} :catch_3

    .line 515
    nop

    .end local v11    # "type":Ljava/lang/String;
    goto :goto_b

    .line 548
    .end local v14    # "intent":Landroid/content/Intent;
    .end local v17    # "params":Landroid/os/Bundle;
    :catch_3
    move-exception v0

    move-object/from16 v12, p0

    move-object/from16 v11, v18

    goto/16 :goto_11

    .line 516
    .restart local v14    # "intent":Landroid/content/Intent;
    .restart local v17    # "params":Landroid/os/Bundle;
    :cond_13
    const/4 v11, 0x0

    :try_start_2a
    invoke-virtual {v14, v12, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    :try_end_2a
    .catch Ljava/lang/Exception; {:try_start_2a .. :try_end_2a} :catch_6

    .line 519
    :goto_b
    move-object/from16 v11, v18

    .end local v18    # "misound_eq_button":Ljava/lang/String;
    .local v11, "misound_eq_button":Ljava/lang/String;
    :try_start_2b
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_2b
    .catch Ljava/lang/Exception; {:try_start_2b .. :try_end_2b} :catch_5

    const-string v12, "misound_eq_button"

    if-eqz v0, :cond_14

    .line 520
    const/4 v0, 0x1

    :try_start_2c
    invoke-virtual {v14, v12, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_c

    .line 522
    :cond_14
    const/4 v0, 0x0

    invoke-virtual {v14, v12, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 525
    :goto_c
    invoke-direct/range {p0 .. p0}, Lcom/android/server/audio/MQSUtils;->checkWirelessTransmissionSupport()Z

    move-result v0
    :try_end_2c
    .catch Ljava/lang/Exception; {:try_start_2c .. :try_end_2c} :catch_5

    const-string/jumbo v12, "support_wireless_transmission"

    if-eqz v0, :cond_15

    .line 526
    const/4 v0, 0x1

    :try_start_2d
    invoke-virtual {v14, v12, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_d

    .line 528
    :cond_15
    const/4 v0, 0x0

    invoke-virtual {v14, v12, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 531
    :goto_d
    invoke-direct/range {p0 .. p0}, Lcom/android/server/audio/MQSUtils;->checkHearingAssistSupport()Z

    move-result v0
    :try_end_2d
    .catch Ljava/lang/Exception; {:try_start_2d .. :try_end_2d} :catch_5

    const-string/jumbo v12, "support_hearing_assist"

    if-eqz v0, :cond_16

    .line 532
    const/4 v0, 0x1

    :try_start_2e
    invoke-virtual {v14, v12, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_e

    .line 534
    :cond_16
    const/4 v0, 0x0

    invoke-virtual {v14, v12, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 537
    :goto_e
    invoke-direct/range {p0 .. p0}, Lcom/android/server/audio/MQSUtils;->checkVoiceprintNoiseReductionSupport()Z

    move-result v0
    :try_end_2e
    .catch Ljava/lang/Exception; {:try_start_2e .. :try_end_2e} :catch_5

    const-string/jumbo v12, "support_voiceprint_noise_reduction"

    if-eqz v0, :cond_17

    .line 538
    const/4 v0, 0x1

    :try_start_2f
    invoke-virtual {v14, v12, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_f

    .line 540
    :cond_17
    const/4 v0, 0x0

    invoke-virtual {v14, v12, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 543
    :goto_f
    move-object/from16 v0, v17

    .end local v17    # "params":Landroid/os/Bundle;
    .local v0, "params":Landroid/os/Bundle;
    invoke-virtual {v14, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 544
    sget-boolean v12, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v12, :cond_18

    .line 545
    const/4 v12, 0x2

    invoke-virtual {v14, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
    :try_end_2f
    .catch Ljava/lang/Exception; {:try_start_2f .. :try_end_2f} :catch_5

    .line 547
    :cond_18
    move-object/from16 v12, p0

    :try_start_30
    iget-object v15, v12, Lcom/android/server/audio/MQSUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v15, v14}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_30
    .catch Ljava/lang/Exception; {:try_start_30 .. :try_end_30} :catch_4

    .line 551
    nop

    .end local v0    # "params":Landroid/os/Bundle;
    .end local v14    # "intent":Landroid/content/Intent;
    goto :goto_12

    .line 548
    :catch_4
    move-exception v0

    goto :goto_11

    :catch_5
    move-exception v0

    move-object/from16 v12, p0

    goto :goto_11

    .end local v11    # "misound_eq_button":Ljava/lang/String;
    .restart local v18    # "misound_eq_button":Ljava/lang/String;
    :catch_6
    move-exception v0

    move-object/from16 v12, p0

    move-object/from16 v11, v18

    .end local v18    # "misound_eq_button":Ljava/lang/String;
    .restart local v11    # "misound_eq_button":Ljava/lang/String;
    goto :goto_11

    .end local v10    # "misound_status_button":Ljava/lang/String;
    .end local v11    # "misound_eq_button":Ljava/lang/String;
    .restart local v18    # "misound_eq_button":Ljava/lang/String;
    .restart local v19    # "misound_status_button":Ljava/lang/String;
    :catch_7
    move-exception v0

    move-object/from16 v12, p0

    move-object/from16 v11, v18

    move-object/from16 v10, v19

    .end local v18    # "misound_eq_button":Ljava/lang/String;
    .end local v19    # "misound_status_button":Ljava/lang/String;
    .restart local v10    # "misound_status_button":Ljava/lang/String;
    .restart local v11    # "misound_eq_button":Ljava/lang/String;
    goto :goto_11

    .end local v2    # "spatial_audio_button":Ljava/lang/String;
    .end local v10    # "misound_status_button":Ljava/lang/String;
    .end local v11    # "misound_eq_button":Ljava/lang/String;
    .restart local v18    # "misound_eq_button":Ljava/lang/String;
    .restart local v19    # "misound_status_button":Ljava/lang/String;
    .restart local v20    # "spatial_audio_button":Ljava/lang/String;
    :catch_8
    move-exception v0

    move-object/from16 v12, p0

    move-object/from16 v11, v18

    move-object/from16 v10, v19

    move-object/from16 v2, v20

    .end local v18    # "misound_eq_button":Ljava/lang/String;
    .end local v19    # "misound_status_button":Ljava/lang/String;
    .end local v20    # "spatial_audio_button":Ljava/lang/String;
    .restart local v2    # "spatial_audio_button":Ljava/lang/String;
    .restart local v10    # "misound_status_button":Ljava/lang/String;
    .restart local v11    # "misound_eq_button":Ljava/lang/String;
    goto :goto_11

    .end local v2    # "spatial_audio_button":Ljava/lang/String;
    .local v10, "spatial_audio_button":Ljava/lang/String;
    .local v11, "misound_status_button":Ljava/lang/String;
    .restart local v12    # "misound_eq_button":Ljava/lang/String;
    :catch_9
    move-exception v0

    goto :goto_10

    :catch_a
    move-exception v0

    move-object/from16 v16, v2

    :goto_10
    move-object v2, v10

    move-object v10, v11

    move-object v11, v12

    move-object/from16 v12, p0

    .line 549
    .end local v12    # "misound_eq_button":Ljava/lang/String;
    .local v0, "e":Ljava/lang/Exception;
    .restart local v2    # "spatial_audio_button":Ljava/lang/String;
    .local v10, "misound_status_button":Ljava/lang/String;
    .local v11, "misound_eq_button":Ljava/lang/String;
    :goto_11
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 550
    const-string v14, "erroe for reportAudioButton"

    move-object/from16 v15, v16

    invoke-static {v15, v14}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 553
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_12
    return-void
.end method

.method public reportAudioSilentObserverToOnetrack(ILjava/lang/String;Ljava/lang/String;I)V
    .locals 17
    .param p1, "level"    # I
    .param p2, "location"    # Ljava/lang/String;
    .param p3, "reason"    # Ljava/lang/String;
    .param p4, "type"    # I

    .line 395
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "reportAudioSilentObserverToOnetrack, level: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", location: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object/from16 v10, p2

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", reason: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object/from16 v11, p3

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", type: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move/from16 v12, p4

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v13, "MiSound.MQSUtils"

    invoke-static {v13, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    invoke-static {}, Ljava/time/LocalDateTime;->now()Ljava/time/LocalDateTime;

    move-result-object v14

    .line 398
    .local v14, "date_time":Ljava/time/LocalDateTime;
    const-string/jumbo v0, "yyyy-MM-dd HH:mm:ss.SSS"

    invoke-static {v0}, Ljava/time/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Ljava/time/format/DateTimeFormatter;

    move-result-object v15

    .line 399
    .local v15, "formatter":Ljava/time/format/DateTimeFormatter;
    invoke-virtual {v14, v15}, Ljava/time/LocalDateTime;->format(Ljava/time/format/DateTimeFormatter;)Ljava/lang/String;

    move-result-object v16

    .line 400
    .local v16, "current_time":Ljava/lang/String;
    const-string v2, ""

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const-string v7, ""

    const-string v8, ""

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v9, v16

    filled-new-array/range {v2 .. v9}, [Ljava/lang/Object;

    move-result-object v0

    const-string/jumbo v2, "{\"name\":\"audio_silent_observer\",\"audio_event\":{\"scenario\":\"%s\", \"location\":\"%s\", \"silent_reason\":\"%s\", \"level\":\"%d\", \"silent_type\":\"%d\", \"source_sink\":\"%s\", \"audio_device\":\"%s\", \"extra_info\":\"%s\"}, \"dgt\":\"null\",\"audio_ext\":\"null\" }"

    invoke-static {v2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 402
    .local v2, "result":Ljava/lang/String;
    move-object/from16 v3, p0

    :try_start_0
    iget-object v0, v3, Lcom/android/server/audio/MQSUtils;->mMiuiXlog:Landroid/media/MiuiXlog;

    invoke-virtual {v0, v2}, Landroid/media/MiuiXlog;->miuiXlogSend(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 405
    goto :goto_0

    .line 403
    :catch_0
    move-exception v0

    .line 404
    .local v0, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "reportAudioSilentObserverToOnetrack exception : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v13, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public reportAudioVisualDailyUse(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/media/AudioPlaybackConfiguration;",
            ">;)V"
        }
    .end annotation

    .line 556
    .local p1, "currentPlayback":Ljava/util/List;, "Ljava/util/List<Landroid/media/AudioPlaybackConfiguration;>;"
    invoke-direct {p0}, Lcom/android/server/audio/MQSUtils;->checkAudioVisualStatus()Ljava/lang/String;

    move-result-object v0

    .line 557
    .local v0, "status":Ljava/lang/String;
    const-string v1, "open"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 558
    return-void
.end method

.method public reportBtMultiVoipDailyUse(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "scoState"    # Ljava/lang/String;
    .param p3, "multiVoipPackages"    # Ljava/lang/String;

    .line 580
    if-nez p1, :cond_0

    .line 581
    return-void

    .line 583
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "reportBtMultiVoipDailyUse start, scoState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", multiVoipPackages="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiSound.MQSUtils"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 586
    :try_start_0
    invoke-direct {p0}, Lcom/android/server/audio/MQSUtils;->isReportXiaomiServer()Z

    move-result v0

    if-nez v0, :cond_1

    .line 587
    return-void

    .line 589
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 590
    .local v0, "params":Landroid/os/Bundle;
    const-string v2, "sco_state_internal"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 591
    const-string/jumbo v2, "voip_packages"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    const-string v2, "31000000086"

    const-string v3, "multi_voip_communication_for_bluetooth_device"

    invoke-static {v2, v3, v0}, Lcom/android/server/audio/MQSUtils;->configureIntent(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 597
    nop

    .end local v0    # "params":Landroid/os/Bundle;
    goto :goto_0

    .line 594
    :catch_0
    move-exception v0

    .line 595
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 596
    const-string v2, "error for reportBtMultiVoipDailyUse"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 598
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public reportVibrateStatus()V
    .locals 11

    .line 612
    invoke-direct {p0}, Lcom/android/server/audio/MQSUtils;->isReportXiaomiServer()Z

    move-result v0

    if-nez v0, :cond_0

    .line 613
    return-void

    .line 615
    :cond_0
    const-string v0, "reportVibrateStatus start."

    const-string v1, "MiSound.MQSUtils"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 616
    iget-object v0, p0, Lcom/android/server/audio/MQSUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v2, "vibrate_when_ringing"

    const/4 v3, 0x1

    invoke-static {v0, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 617
    .local v0, "vibrate_ring":I
    iget-object v2, p0, Lcom/android/server/audio/MQSUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v4, "vibrate_in_silent"

    invoke-static {v2, v4, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 618
    .local v2, "vibrate_silent":I
    invoke-direct {p0}, Lcom/android/server/audio/MQSUtils;->checkHapticStatus()Ljava/lang/String;

    move-result-object v4

    .line 619
    .local v4, "haptic_status":Ljava/lang/String;
    invoke-direct {p0}, Lcom/android/server/audio/MQSUtils;->getHapticFeedbackFloatLevel()F

    move-result v5

    .line 622
    .local v5, "haptic_level":F
    :try_start_0
    new-instance v6, Landroid/content/Intent;

    const-string v7, "onetrack.action.TRACK_EVENT"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 623
    .local v6, "intent":Landroid/content/Intent;
    invoke-direct {p0}, Lcom/android/server/audio/MQSUtils;->isGlobalBuild()Z

    move-result v7

    const/4 v8, 0x2

    if-nez v7, :cond_1

    .line 624
    invoke-virtual {v6, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 626
    :cond_1
    const-string v7, "com.miui.analytics"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 627
    const-string v7, "APP_ID"

    const-string v9, "31000000089"

    invoke-virtual {v6, v7, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 628
    const-string v7, "EVENT_NAME"

    const-string v9, "haptic_status"

    invoke-virtual {v6, v7, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 629
    const-string v7, "PACKAGE"

    const-string v9, "android"

    invoke-virtual {v6, v7, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 630
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 631
    .local v7, "params":Landroid/os/Bundle;
    const/4 v9, 0x0

    const-string/jumbo v10, "status_ring"

    if-ne v0, v3, :cond_2

    .line 632
    :try_start_1
    invoke-virtual {v6, v10, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0

    .line 634
    :cond_2
    invoke-virtual {v6, v10, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 636
    :goto_0
    const-string/jumbo v10, "status_silent"

    if-ne v2, v3, :cond_3

    .line 637
    :try_start_2
    invoke-virtual {v6, v10, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_1

    .line 639
    :cond_3
    invoke-virtual {v6, v10, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 641
    :goto_1
    const-string v9, "open"

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 642
    const-string v9, "haptic_intensity_status"

    invoke-virtual {v6, v9, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 643
    const-string v3, "haptic_intensity_position"

    invoke-virtual {v6, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 645
    :cond_4
    invoke-virtual {v6, v7}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 646
    sget-boolean v3, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v3, :cond_5

    .line 647
    invoke-virtual {v6, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 649
    :cond_5
    iget-object v3, p0, Lcom/android/server/audio/MQSUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v6}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 653
    nop

    .end local v6    # "intent":Landroid/content/Intent;
    .end local v7    # "params":Landroid/os/Bundle;
    goto :goto_2

    .line 650
    :catch_0
    move-exception v3

    .line 651
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 652
    const-string v6, "erroe for reportVibrate"

    invoke-static {v1, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 655
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_2
    return-void
.end method

.method public reportWaveformInfo(Lcom/android/server/audio/MQSUtils$WaveformInfo;)V
    .locals 4
    .param p1, "vibratorInfo"    # Lcom/android/server/audio/MQSUtils$WaveformInfo;

    .line 601
    const-string v0, "report WaveformInfo start."

    const-string v1, "MiSound.MQSUtils"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 602
    iget-object v0, p1, Lcom/android/server/audio/MQSUtils$WaveformInfo;->opPkg:Ljava/lang/String;

    iget-object v2, p1, Lcom/android/server/audio/MQSUtils$WaveformInfo;->attrs:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/server/audio/MQSUtils$WaveformInfo;->effect:Ljava/lang/String;

    filled-new-array {v0, v2, v3}, [Ljava/lang/Object;

    move-result-object v0

    const-string/jumbo v2, "{\"name\":\"waveform_info\",\"audio_event\":{\"waveform_package_name\":\"%s\",\"waveform_usage\":\"%s\",\"waveform_effect\":\"%s\"},\"dgt\":\"null\",\"audio_ext\":\"null\" }"

    invoke-static {v2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 603
    .local v0, "result":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "reportWaveformInfo:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 605
    :try_start_0
    iget-object v2, p0, Lcom/android/server/audio/MQSUtils;->mMiuiXlog:Landroid/media/MiuiXlog;

    invoke-virtual {v2, v0}, Landroid/media/MiuiXlog;->miuiXlogSend(Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 608
    goto :goto_0

    .line 606
    :catchall_0
    move-exception v2

    .line 607
    .local v2, "e":Ljava/lang/Throwable;
    const-string v3, "can not use miuiXlogSend!!!"

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 609
    .end local v2    # "e":Ljava/lang/Throwable;
    :goto_0
    return-void
.end method
