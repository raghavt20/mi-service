class com.android.server.audio.AudioServiceStubImpl$H extends android.os.Handler {
	 /* .source "AudioServiceStubImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/audio/AudioServiceStubImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "H" */
} // .end annotation
/* # instance fields */
final com.android.server.audio.AudioServiceStubImpl this$0; //synthetic
/* # direct methods */
public com.android.server.audio.AudioServiceStubImpl$H ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 2159 */
this.this$0 = p1;
/* .line 2160 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 2161 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 6 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 2164 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* goto/16 :goto_0 */
/* .line 2207 */
/* :pswitch_0 */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Ljava/lang/Boolean; */
v1 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
com.android.server.audio.AudioServiceStubImpl .-$$Nest$mhandleMediaStateUpdate ( v0,v1 );
/* .line 2208 */
/* goto/16 :goto_0 */
/* .line 2204 */
/* :pswitch_1 */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Ljava/lang/String; */
com.android.server.audio.AudioServiceStubImpl .-$$Nest$mhandleMeetingModeUpdate ( v0,v1 );
/* .line 2205 */
/* goto/16 :goto_0 */
/* .line 2201 */
/* :pswitch_2 */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
com.android.server.audio.AudioServiceStubImpl .-$$Nest$mhandleAudioModeUpdate ( v0,v1 );
/* .line 2202 */
/* goto/16 :goto_0 */
/* .line 2193 */
/* :pswitch_3 */
v0 = this.this$0;
com.android.server.audio.AudioServiceStubImpl .-$$Nest$fgetmContext ( v0 );
com.android.server.audio.MQSserver .getInstance ( v0 );
(( com.android.server.audio.MQSserver ) v0 ).asynReportData ( ); // invoke-virtual {v0}, Lcom/android/server/audio/MQSserver;->asynReportData()V
/* .line 2194 */
v0 = this.this$0;
com.android.server.audio.AudioServiceStubImpl .-$$Nest$fgetmHandler ( v0 );
v1 = this.this$0;
com.android.server.audio.AudioServiceStubImpl .-$$Nest$fgetmHandler ( v1 );
int v2 = 6; // const/4 v2, 0x6
(( android.os.Handler ) v1 ).obtainMessage ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
v2 = com.android.server.audio.AudioServiceStubImpl .-$$Nest$sfgetMQSSERVER_REPORT_RATE_MS ( );
/* int-to-long v2, v2 */
(( android.os.Handler ) v0 ).sendMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 2196 */
/* goto/16 :goto_0 */
/* .line 2198 */
/* :pswitch_4 */
v0 = this.this$0;
com.android.server.audio.AudioServiceStubImpl .-$$Nest$fgetmMqsUtils ( v0 );
v1 = this.obj;
/* check-cast v1, Lcom/android/server/audio/MQSUtils$AudioStateTrackData; */
(( com.android.server.audio.MQSUtils ) v0 ).reportAbnormalAudioStatus ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/audio/MQSUtils;->reportAbnormalAudioStatus(Lcom/android/server/audio/MQSUtils$AudioStateTrackData;)V
/* .line 2199 */
/* goto/16 :goto_0 */
/* .line 2186 */
/* :pswitch_5 */
v0 = this.this$0;
com.android.server.audio.AudioServiceStubImpl .-$$Nest$fgetmMqsUtils ( v0 );
/* .line 2187 */
(( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
final String v2 = "audio_silent_level"; // const-string v2, "audio_silent_level"
v1 = (( android.os.Bundle ) v1 ).getInt ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I
/* .line 2188 */
(( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
final String v3 = "audio_silent_location"; // const-string v3, "audio_silent_location"
(( android.os.Bundle ) v2 ).getString ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 2189 */
(( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
final String v4 = "audio_silent_reason"; // const-string v4, "audio_silent_reason"
(( android.os.Bundle ) v3 ).getString ( v4 ); // invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 2190 */
(( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
final String v5 = "audio_silent_type"; // const-string v5, "audio_silent_type"
v4 = (( android.os.Bundle ) v4 ).getInt ( v5 ); // invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I
/* .line 2186 */
(( com.android.server.audio.MQSUtils ) v0 ).reportAudioSilentObserverToOnetrack ( v1, v2, v3, v4 ); // invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/server/audio/MQSUtils;->reportAudioSilentObserverToOnetrack(ILjava/lang/String;Ljava/lang/String;I)V
/* .line 2191 */
/* goto/16 :goto_0 */
/* .line 2181 */
/* :pswitch_6 */
v0 = this.this$0;
com.android.server.audio.AudioServiceStubImpl .-$$Nest$fgetmMqsUtils ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 2182 */
	 v0 = this.this$0;
	 com.android.server.audio.AudioServiceStubImpl .-$$Nest$fgetmMqsUtils ( v0 );
	 v1 = this.obj;
	 /* check-cast v1, Lcom/android/server/audio/MQSUtils$WaveformInfo; */
	 (( com.android.server.audio.MQSUtils ) v0 ).reportWaveformInfo ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/audio/MQSUtils;->reportWaveformInfo(Lcom/android/server/audio/MQSUtils$WaveformInfo;)V
	 /* .line 2175 */
	 /* :pswitch_7 */
	 v0 = this.this$0;
	 com.android.server.audio.AudioServiceStubImpl .-$$Nest$fgetmMqsUtils ( v0 );
	 v1 = this.this$0;
	 com.android.server.audio.AudioServiceStubImpl .-$$Nest$fgetmContext ( v1 );
	 v2 = this.obj;
	 /* check-cast v2, Ljava/lang/String; */
	 v3 = this.this$0;
	 com.android.server.audio.AudioServiceStubImpl .-$$Nest$fgetmMultiVoipPackages ( v3 );
	 (( com.android.server.audio.MQSUtils ) v0 ).reportBtMultiVoipDailyUse ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/audio/MQSUtils;->reportBtMultiVoipDailyUse(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
	 /* .line 2177 */
	 /* .line 2166 */
	 /* :pswitch_8 */
	 v0 = this.obj;
	 /* check-cast v0, Ljava/util/List; */
	 /* .line 2167 */
	 /* .local v0, "clients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 /* const-string/jumbo v2, "updateConcurrentVoipInfo voip apps : " */
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* .line 2168 */
	 java.util.Arrays .toString ( v2 );
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 /* .line 2167 */
	 final String v2 = "AudioServiceStubImpl"; // const-string v2, "AudioServiceStubImpl"
	 android.util.Log .d ( v2,v1 );
	 /* .line 2169 */
	 v1 = this.this$0;
	 com.android.server.audio.AudioServiceStubImpl .-$$Nest$fgetmAudioService ( v1 );
	 v1 = 	 (( com.android.server.audio.AudioService ) v1 ).isBluetoothA2dpOn ( ); // invoke-virtual {v1}, Lcom/android/server/audio/AudioService;->isBluetoothA2dpOn()Z
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 2170 */
		 v1 = this.this$0;
		 java.util.Arrays .toString ( v2 );
		 com.android.server.audio.AudioServiceStubImpl .-$$Nest$fputmMultiVoipPackages ( v1,v2 );
		 /* .line 2172 */
	 } // :cond_0
	 v1 = this.this$0;
	 com.android.server.audio.AudioServiceStubImpl .-$$Nest$fgetmContext ( v1 );
	 com.android.server.audio.MQSUtils .trackConcurrentVoipInfo ( v1,v0 );
	 /* .line 2173 */
	 /* nop */
	 /* .line 2212 */
} // .end local v0 # "clients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_1
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
