.class Lcom/android/server/audio/AudioGameEffect$GameEffectThread;
.super Ljava/lang/Thread;
.source "AudioGameEffect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/AudioGameEffect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GameEffectThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/audio/AudioGameEffect;


# direct methods
.method constructor <init>(Lcom/android/server/audio/AudioGameEffect;)V
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/android/server/audio/AudioGameEffect$GameEffectThread;->this$0:Lcom/android/server/audio/AudioGameEffect;

    .line 165
    const-string p1, "GameEffectThread"

    invoke-direct {p0, p1}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 166
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 170
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 171
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 172
    iget-object v0, p0, Lcom/android/server/audio/AudioGameEffect$GameEffectThread;->this$0:Lcom/android/server/audio/AudioGameEffect;

    new-instance v1, Lcom/android/server/audio/AudioGameEffect$GameEffectHandler;

    iget-object v2, p0, Lcom/android/server/audio/AudioGameEffect$GameEffectThread;->this$0:Lcom/android/server/audio/AudioGameEffect;

    invoke-direct {v1, v2}, Lcom/android/server/audio/AudioGameEffect$GameEffectHandler;-><init>(Lcom/android/server/audio/AudioGameEffect;)V

    invoke-static {v0, v1}, Lcom/android/server/audio/AudioGameEffect;->-$$Nest$fputmHandler(Lcom/android/server/audio/AudioGameEffect;Lcom/android/server/audio/AudioGameEffect$GameEffectHandler;)V

    .line 173
    iget-object v0, p0, Lcom/android/server/audio/AudioGameEffect$GameEffectThread;->this$0:Lcom/android/server/audio/AudioGameEffect;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/audio/AudioGameEffect;->-$$Nest$fputmQueryPMServiceTime(Lcom/android/server/audio/AudioGameEffect;I)V

    .line 174
    iget-object v0, p0, Lcom/android/server/audio/AudioGameEffect$GameEffectThread;->this$0:Lcom/android/server/audio/AudioGameEffect;

    invoke-static {v0}, Lcom/android/server/audio/AudioGameEffect;->-$$Nest$minitProcessListenerAsync(Lcom/android/server/audio/AudioGameEffect;)V

    .line 175
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 177
    return-void
.end method
