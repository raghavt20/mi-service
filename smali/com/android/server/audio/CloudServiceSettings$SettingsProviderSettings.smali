.class Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;
.super Lcom/android/server/audio/CloudServiceSettings$Setting;
.source "CloudServiceSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/audio/CloudServiceSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SettingsProviderSettings"
.end annotation


# static fields
.field private static final KEY:Ljava/lang/String; = "key"

.field private static final SCOPE:Ljava/lang/String; = "scope"

.field private static final VALUE:Ljava/lang/String; = "value"


# instance fields
.field private mKey:Ljava/lang/StringBuilder;

.field private mScope:Ljava/lang/StringBuilder;

.field private mValue:Ljava/lang/StringBuilder;

.field final synthetic this$0:Lcom/android/server/audio/CloudServiceSettings;


# direct methods
.method constructor <init>(Lcom/android/server/audio/CloudServiceSettings;Lorg/json/JSONObject;)V
    .locals 3
    .param p1, "this$0"    # Lcom/android/server/audio/CloudServiceSettings;
    .param p2, "json"    # Lorg/json/JSONObject;

    .line 640
    iput-object p1, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->this$0:Lcom/android/server/audio/CloudServiceSettings;

    .line 641
    invoke-direct {p0, p1, p2}, Lcom/android/server/audio/CloudServiceSettings$Setting;-><init>(Lcom/android/server/audio/CloudServiceSettings;Lorg/json/JSONObject;)V

    .line 643
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "key"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mKey:Ljava/lang/StringBuilder;

    .line 644
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "value"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mValue:Ljava/lang/StringBuilder;

    .line 645
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "scope"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mScope:Ljava/lang/StringBuilder;

    .line 646
    iget-boolean v0, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mSecreted:Z

    if-eqz v0, :cond_0

    .line 647
    iget-object v0, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mSecretedItem:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mKey:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 648
    iget-object v0, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mSecretedItem:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mValue:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 649
    iget-object v0, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mSecretedItem:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mScope:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 653
    :cond_0
    goto :goto_0

    .line 651
    :catch_0
    move-exception v0

    .line 652
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "CloudServiceSettings"

    const-string v2, "fail to parse FileDownloadSetting"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 654
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method


# virtual methods
.method public set()Z
    .locals 4

    .line 658
    invoke-super {p0}, Lcom/android/server/audio/CloudServiceSettings$Setting;->set()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 659
    return v1

    .line 661
    :cond_0
    iget-object v0, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->this$0:Lcom/android/server/audio/CloudServiceSettings;

    invoke-static {v0}, Lcom/android/server/audio/CloudServiceSettings;->-$$Nest$fgetmContext(Lcom/android/server/audio/CloudServiceSettings;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 662
    .local v0, "cr":Landroid/content/ContentResolver;
    iget-object v2, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mScope:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_1
    goto :goto_0

    :sswitch_0
    const-string/jumbo v1, "system"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :sswitch_1
    const-string/jumbo v1, "secure"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    goto :goto_1

    :sswitch_2
    const-string v3, "global"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_1

    :goto_0
    const/4 v1, -0x1

    :goto_1
    packed-switch v1, :pswitch_data_0

    .line 673
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "fail to set: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mSettingName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " error scope!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "CloudServiceSettings"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 670
    :pswitch_0
    iget-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mKey:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mValue:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mSuccess:Z

    .line 671
    goto :goto_2

    .line 667
    :pswitch_1
    iget-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mKey:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mValue:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mSuccess:Z

    .line 668
    goto :goto_2

    .line 664
    :pswitch_2
    iget-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mKey:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mValue:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mSuccess:Z

    .line 665
    nop

    .line 675
    :goto_2
    iget-boolean v1, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mSuccess:Z

    return v1

    :sswitch_data_0
    .sparse-switch
        -0x4a16fc5d -> :sswitch_2
        -0x3604a489 -> :sswitch_1
        -0x34e38dd1 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public updateTo(Lcom/android/server/audio/CloudServiceSettings$Setting;)Z
    .locals 2
    .param p1, "s"    # Lcom/android/server/audio/CloudServiceSettings$Setting;

    .line 680
    invoke-super {p0, p1}, Lcom/android/server/audio/CloudServiceSettings$Setting;->updateTo(Lcom/android/server/audio/CloudServiceSettings$Setting;)Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;

    if-eqz v0, :cond_0

    .line 681
    move-object v0, p1

    check-cast v0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;

    .line 682
    .local v0, "ss":Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;
    iget-object v1, v0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mKey:Ljava/lang/StringBuilder;

    iput-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mKey:Ljava/lang/StringBuilder;

    .line 683
    iget-object v1, v0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mValue:Ljava/lang/StringBuilder;

    iput-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mValue:Ljava/lang/StringBuilder;

    .line 684
    iget-object v1, v0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mScope:Ljava/lang/StringBuilder;

    iput-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mScope:Ljava/lang/StringBuilder;

    .line 685
    const/4 v1, 0x1

    return v1

    .line 687
    .end local v0    # "ss":Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "fail to update SettingsProviderSettings: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/audio/CloudServiceSettings$SettingsProviderSettings;->mSettingName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CloudServiceSettings"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 688
    const/4 v0, 0x0

    return v0
.end method
