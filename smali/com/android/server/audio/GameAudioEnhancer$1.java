class com.android.server.audio.GameAudioEnhancer$1 extends miui.app.IFreeformCallback$Stub {
	 /* .source "GameAudioEnhancer.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/audio/GameAudioEnhancer; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.audio.GameAudioEnhancer this$0; //synthetic
/* # direct methods */
 com.android.server.audio.GameAudioEnhancer$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/audio/GameAudioEnhancer; */
/* .line 146 */
this.this$0 = p1;
/* invoke-direct {p0}, Lmiui/app/IFreeformCallback$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void dispatchFreeFormStackModeChanged ( Integer p0, miui.app.MiuiFreeFormManager$MiuiFreeFormStackInfo p1 ) {
/* .locals 6 */
/* .param p1, "action" # I */
/* .param p2, "stackInfo" # Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo; */
/* .line 150 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Enter dispatchFreeFormStackModeChanged action: "; // const-string v1, "Enter dispatchFreeFormStackModeChanged action: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "GameAudioEnhancer"; // const-string v1, "GameAudioEnhancer"
android.util.Log .d ( v1,v0 );
/* .line 151 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 153 */
/* .local v0, "freeFormStackInfoList":Ljava/util/List;, "Ljava/util/List<Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;>;" */
final String v1 = ""; // const-string v1, ""
/* .line 154 */
/* .local v1, "freeFormPkg":Ljava/lang/String; */
v2 = this.this$0;
final String v3 = ""; // const-string v3, ""
com.android.server.audio.GameAudioEnhancer .-$$Nest$fputmFreeFormWindowPkg ( v2,v3 );
/* .line 155 */
v2 = this.this$0;
v2 = com.android.server.audio.GameAudioEnhancer .-$$Nest$fgetmGameModeSwitchStatus ( v2 );
/* if-nez v2, :cond_0 */
v2 = this.this$0;
v2 = com.android.server.audio.GameAudioEnhancer .-$$Nest$fgetmParamSet ( v2 );
/* if-nez v2, :cond_0 */
/* .line 156 */
return;
/* .line 158 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
int v3 = 2; // const/4 v3, 0x2
int v4 = 3; // const/4 v4, 0x3
/* sparse-switch p1, :sswitch_data_0 */
/* goto/16 :goto_4 */
/* .line 202 */
/* :sswitch_0 */
v2 = this.this$0;
com.android.server.audio.GameAudioEnhancer .-$$Nest$fgetmCurForegroundPkg ( v2 );
com.android.server.audio.GameAudioEnhancer .-$$Nest$msetParamSend ( v2,v3 );
/* goto/16 :goto_4 */
/* .line 181 */
/* :sswitch_1 */
v5 = this.this$0;
v5 = com.android.server.audio.GameAudioEnhancer .-$$Nest$fgetmFreeWinVersion ( v5 );
/* if-ne v5, v4, :cond_4 */
/* .line 184 */
v3 = this.this$0;
com.android.server.audio.GameAudioEnhancer .-$$Nest$fgetmContext ( v3 );
(( android.content.Context ) v3 ).getDisplay ( ); // invoke-virtual {v3}, Landroid/content/Context;->getDisplay()Landroid/view/Display;
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 185 */
v2 = this.this$0;
com.android.server.audio.GameAudioEnhancer .-$$Nest$fgetmContext ( v2 );
(( android.content.Context ) v2 ).getDisplay ( ); // invoke-virtual {v2}, Landroid/content/Context;->getDisplay()Landroid/view/Display;
v2 = (( android.view.Display ) v2 ).getDisplayId ( ); // invoke-virtual {v2}, Landroid/view/Display;->getDisplayId()I
/* .line 186 */
} // :cond_1
/* nop */
/* .line 183 */
} // :goto_0
miui.app.MiuiFreeFormManager .getAllFreeFormStackInfosOnDisplay ( v2 );
/* .line 188 */
v3 = } // :goto_1
if ( v3 != null) { // if-eqz v3, :cond_3
/* check-cast v3, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo; */
/* .line 189 */
/* .local v3, "miuiFreeFormStackInfo":Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo; */
v4 = this.this$0;
v5 = this.packageName;
v4 = com.android.server.audio.GameAudioEnhancer .-$$Nest$misPackageEnabled ( v4,v5 );
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 190 */
v4 = this.this$0;
v5 = this.packageName;
com.android.server.audio.GameAudioEnhancer .-$$Nest$fputmFreeFormWindowPkg ( v4,v5 );
/* .line 192 */
} // .end local v3 # "miuiFreeFormStackInfo":Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;
} // :cond_2
} // :cond_3
/* goto/16 :goto_4 */
/* .line 193 */
} // :cond_4
v2 = this.this$0;
v2 = com.android.server.audio.GameAudioEnhancer .-$$Nest$fgetmFreeWinVersion ( v2 );
/* if-ne v2, v3, :cond_8 */
/* .line 194 */
v2 = this.this$0;
com.android.server.audio.GameAudioEnhancer .-$$Nest$fgetmContext ( v2 );
miui.app.MiuiFreeFormManager .getMiuiFreeformStackPackageName ( v2 );
/* .line 195 */
v2 = this.this$0;
v2 = com.android.server.audio.GameAudioEnhancer .-$$Nest$misPackageEnabled ( v2,v1 );
if ( v2 != null) { // if-eqz v2, :cond_8
/* .line 196 */
v2 = this.this$0;
com.android.server.audio.GameAudioEnhancer .-$$Nest$fputmFreeFormWindowPkg ( v2,v1 );
/* .line 162 */
/* :sswitch_2 */
v5 = this.this$0;
v5 = com.android.server.audio.GameAudioEnhancer .-$$Nest$fgetmFreeWinVersion ( v5 );
/* if-ne v5, v4, :cond_7 */
/* .line 165 */
v3 = this.this$0;
com.android.server.audio.GameAudioEnhancer .-$$Nest$fgetmContext ( v3 );
(( android.content.Context ) v3 ).getDisplay ( ); // invoke-virtual {v3}, Landroid/content/Context;->getDisplay()Landroid/view/Display;
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 166 */
v2 = this.this$0;
com.android.server.audio.GameAudioEnhancer .-$$Nest$fgetmContext ( v2 );
(( android.content.Context ) v2 ).getDisplay ( ); // invoke-virtual {v2}, Landroid/content/Context;->getDisplay()Landroid/view/Display;
v2 = (( android.view.Display ) v2 ).getDisplayId ( ); // invoke-virtual {v2}, Landroid/view/Display;->getDisplayId()I
/* .line 167 */
} // :cond_5
/* nop */
/* .line 164 */
} // :goto_2
miui.app.MiuiFreeFormManager .getAllFreeFormStackInfosOnDisplay ( v2 );
/* .line 169 */
v3 = } // :goto_3
if ( v3 != null) { // if-eqz v3, :cond_6
/* check-cast v3, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo; */
/* .line 170 */
/* .restart local v3 # "miuiFreeFormStackInfo":Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo; */
v4 = this.this$0;
v5 = this.packageName;
com.android.server.audio.GameAudioEnhancer .-$$Nest$fputmFreeFormWindowPkg ( v4,v5 );
/* .line 171 */
v4 = this.this$0;
v5 = this.packageName;
com.android.server.audio.GameAudioEnhancer .-$$Nest$msetParamSend ( v4,v5 );
/* .line 172 */
} // .end local v3 # "miuiFreeFormStackInfo":Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;
} // :cond_6
/* .line 173 */
} // :cond_7
v2 = this.this$0;
v2 = com.android.server.audio.GameAudioEnhancer .-$$Nest$fgetmFreeWinVersion ( v2 );
/* if-ne v2, v3, :cond_8 */
/* .line 174 */
v2 = this.this$0;
com.android.server.audio.GameAudioEnhancer .-$$Nest$fgetmContext ( v2 );
miui.app.MiuiFreeFormManager .getMiuiFreeformStackPackageName ( v2 );
/* .line 175 */
v2 = this.this$0;
com.android.server.audio.GameAudioEnhancer .-$$Nest$fputmFreeFormWindowPkg ( v2,v1 );
/* .line 176 */
v2 = this.this$0;
com.android.server.audio.GameAudioEnhancer .-$$Nest$msetParamSend ( v2,v1 );
/* .line 206 */
} // :cond_8
} // :goto_4
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x0 -> :sswitch_2 */
/* 0x1 -> :sswitch_2 */
/* 0x2 -> :sswitch_1 */
/* 0x3 -> :sswitch_0 */
/* 0x4 -> :sswitch_1 */
/* 0x5 -> :sswitch_0 */
/* 0x15 -> :sswitch_2 */
} // .end sparse-switch
} // .end method
