public abstract class com.android.server.audio.AudioServiceStubImpl$AudioPowerSaveEnable {
	 /* .source "AudioServiceStubImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/audio/AudioServiceStubImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x609 */
/* name = "AudioPowerSaveEnable" */
} // .end annotation
/* # static fields */
public static final Integer AUDIOPOWERSAVE_AUDIOBIN_ENABLE;
public static final Integer AUDIOPOWERSAVE_MAIN_ENABLE;
public static final Integer AUDIOPOWERSAVE_OUTPUT_ENABLE;
public static final Integer AUDIOPOWERSAVE_VOICETRIGGER_ENABLE;
public static final Integer AUDIOPOWERSAVE_VOLUME_ENABLE;
