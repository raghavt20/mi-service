.class Lcom/android/server/audio/FocusRequesterStubImpl$1;
.super Ljava/lang/Object;
.source "FocusRequesterStubImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/audio/FocusRequesterStubImpl;->notifyFocusChange(ILandroid/os/Handler;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/audio/FocusRequesterStubImpl;

.field final synthetic val$reason:Ljava/lang/String;

.field final synthetic val$uid:I


# direct methods
.method constructor <init>(Lcom/android/server/audio/FocusRequesterStubImpl;ILjava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/audio/FocusRequesterStubImpl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 34
    iput-object p1, p0, Lcom/android/server/audio/FocusRequesterStubImpl$1;->this$0:Lcom/android/server/audio/FocusRequesterStubImpl;

    iput p2, p0, Lcom/android/server/audio/FocusRequesterStubImpl$1;->val$uid:I

    iput-object p3, p0, Lcom/android/server/audio/FocusRequesterStubImpl$1;->val$reason:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 37
    const/4 v0, 0x1

    new-array v0, v0, [I

    .line 38
    .local v0, "uids":[I
    const/4 v1, 0x0

    iget v2, p0, Lcom/android/server/audio/FocusRequesterStubImpl$1;->val$uid:I

    aput v2, v0, v1

    .line 40
    :try_start_0
    iget-object v1, p0, Lcom/android/server/audio/FocusRequesterStubImpl$1;->this$0:Lcom/android/server/audio/FocusRequesterStubImpl;

    invoke-static {v1}, Lcom/android/server/audio/FocusRequesterStubImpl;->-$$Nest$mgetGreeze(Lcom/android/server/audio/FocusRequesterStubImpl;)Lmiui/greeze/IGreezeManager;

    move-result-object v1

    iget v2, p0, Lcom/android/server/audio/FocusRequesterStubImpl$1;->val$uid:I

    invoke-interface {v1, v2}, Lmiui/greeze/IGreezeManager;->isUidFrozen(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 41
    iget-object v1, p0, Lcom/android/server/audio/FocusRequesterStubImpl$1;->this$0:Lcom/android/server/audio/FocusRequesterStubImpl;

    invoke-static {v1}, Lcom/android/server/audio/FocusRequesterStubImpl;->-$$Nest$mgetGreeze(Lcom/android/server/audio/FocusRequesterStubImpl;)Lmiui/greeze/IGreezeManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/audio/FocusRequesterStubImpl$1;->val$reason:Ljava/lang/String;

    const/16 v3, 0x3e8

    invoke-interface {v1, v0, v3, v2}, Lmiui/greeze/IGreezeManager;->thawUids([IILjava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    :cond_0
    goto :goto_0

    .line 42
    :catch_0
    move-exception v1

    .line 43
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "FocusRequesterStubImpl"

    const-string v3, "notifyFocusChange err:"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 45
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method
