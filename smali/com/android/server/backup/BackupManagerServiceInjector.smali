.class public Lcom/android/server/backup/BackupManagerServiceInjector;
.super Lcom/android/server/backup/BackupManagerServiceStub;
.source "BackupManagerServiceInjector.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.backup.BackupManagerServiceStub$$"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/backup/BackupManagerServiceInjector$HuanjiReceiver;,
        Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;
    }
.end annotation


# static fields
.field private static final INSTALL_ALL_WHITELIST_RESTRICTED_PERMISSIONS:I = 0x400000

.field private static final INSTALL_FULL_APP:I = 0x4000

.field private static final INSTALL_REASON_USER:I = 0x4

.field private static final TAG:Ljava/lang/String; = "Backup:BackupManagerServiceInjector"

.field private static final XMSF_PKG_NAME:Ljava/lang/String; = "com.xiaomi.xmsf"

.field private static sBinderDeathLinker:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Landroid/os/IBinder;",
            "Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mHuanjiReceiver:Lcom/android/server/backup/BackupManagerServiceInjector$HuanjiReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 148
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/server/backup/BackupManagerServiceInjector;->sBinderDeathLinker:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 55
    invoke-direct {p0}, Lcom/android/server/backup/BackupManagerServiceStub;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/backup/BackupManagerServiceInjector;->mHuanjiReceiver:Lcom/android/server/backup/BackupManagerServiceInjector$HuanjiReceiver;

    return-void
.end method


# virtual methods
.method public addRestoredSize(JI)V
    .locals 4
    .param p1, "size"    # J
    .param p3, "fd"    # I

    .line 126
    const-string v0, "MiuiBackup"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lmiui/app/backup/IBackupManager;

    .line 128
    .local v0, "bm":Lmiui/app/backup/IBackupManager;
    :try_start_0
    invoke-interface {v0, p3}, Lmiui/app/backup/IBackupManager;->isRunningFromMiui(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 129
    invoke-interface {v0, p1, p2}, Lmiui/app/backup/IBackupManager;->addCompletedSize(J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    :cond_0
    goto :goto_0

    .line 131
    :catch_0
    move-exception v1

    .line 132
    .local v1, "e":Landroid/os/RemoteException;
    const-string v2, "Backup:BackupManagerServiceInjector"

    const-string v3, "addRestoredSize failed"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 134
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method public cancelBackups(Lcom/android/server/backup/UserBackupManagerService;)V
    .locals 14
    .param p1, "thiz"    # Lcom/android/server/backup/UserBackupManagerService;

    .line 525
    const-string v0, "Backup:BackupManagerServiceInjector"

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1

    .line 527
    .local v1, "oldToken":J
    :try_start_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 528
    .local v3, "operationsToCancel":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-virtual {p1}, Lcom/android/server/backup/UserBackupManagerService;->getOperationStorage()Lcom/android/server/backup/OperationStorage;

    move-result-object v4

    .line 531
    .local v4, "operationStorage":Lcom/android/server/backup/OperationStorage;
    const/4 v5, 0x0

    invoke-interface {v4, v5}, Lcom/android/server/backup/OperationStorage;->operationTokensForOpType(I)Ljava/util/Set;

    move-result-object v6

    .line 532
    .local v6, "backupWaitOperations":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    const/4 v7, 0x1

    invoke-interface {v4, v7}, Lcom/android/server/backup/OperationStorage;->operationTokensForOpType(I)Ljava/util/Set;

    move-result-object v8

    .line 533
    .local v8, "restoreWaitOperations":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-interface {v3, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 534
    invoke-interface {v3, v8}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 536
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    .line 537
    .local v10, "token":Ljava/lang/Integer;
    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v11

    invoke-virtual {p1, v11, v7}, Lcom/android/server/backup/UserBackupManagerService;->handleCancel(IZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 538
    .end local v10    # "token":Ljava/lang/Integer;
    goto :goto_0

    .line 541
    :cond_0
    :try_start_1
    const-string v7, "MiuiBackup"

    invoke-static {v7}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v7

    check-cast v7, Lmiui/app/backup/IBackupManager;

    .line 542
    .local v7, "bm":Lmiui/app/backup/IBackupManager;
    invoke-interface {v7}, Lmiui/app/backup/IBackupManager;->getCurrentRunningPackage()Ljava/lang/String;

    move-result-object v9

    .line 543
    .local v9, "targetPkj":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/android/server/backup/UserBackupManagerService;->getContext()Landroid/content/Context;

    move-result-object v10

    .line 544
    .local v10, "context":Landroid/content/Context;
    invoke-virtual {v10}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v11

    .line 547
    .local v11, "pm":Landroid/content/pm/PackageManager;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "sBinderDeathLinker size:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Lcom/android/server/backup/BackupManagerServiceInjector;->sBinderDeathLinker:Ljava/util/HashMap;

    invoke-virtual {v13}, Ljava/util/HashMap;->size()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v0, v12}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 548
    invoke-static {v10, v9}, Lmiui/app/backup/BackupManager;->isSysAppForBackup(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 549
    nop

    .line 550
    invoke-virtual {p1}, Lcom/android/server/backup/UserBackupManagerService;->getUserId()I

    move-result v5

    .line 549
    const/16 v12, 0x400

    invoke-virtual {v11, v9, v12, v5}, Landroid/content/pm/PackageManager;->getApplicationInfoAsUser(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    .local v5, "mTargetAppInfo":Landroid/content/pm/ApplicationInfo;
    goto :goto_1

    .line 552
    .end local v5    # "mTargetAppInfo":Landroid/content/pm/ApplicationInfo;
    :cond_1
    invoke-virtual {p1}, Lcom/android/server/backup/UserBackupManagerService;->getUserId()I

    move-result v12

    invoke-virtual {v11, v9, v5, v12}, Landroid/content/pm/PackageManager;->getApplicationInfoAsUser(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    .line 554
    .restart local v5    # "mTargetAppInfo":Landroid/content/pm/ApplicationInfo;
    :goto_1
    invoke-virtual {p1, v5}, Lcom/android/server/backup/UserBackupManagerService;->tearDownAgentAndKill(Landroid/content/pm/ApplicationInfo;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 557
    .end local v5    # "mTargetAppInfo":Landroid/content/pm/ApplicationInfo;
    .end local v7    # "bm":Lmiui/app/backup/IBackupManager;
    .end local v9    # "targetPkj":Ljava/lang/String;
    .end local v10    # "context":Landroid/content/Context;
    .end local v11    # "pm":Landroid/content/pm/PackageManager;
    goto :goto_2

    .line 555
    :catch_0
    move-exception v5

    .line 556
    .local v5, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v7, "cancelBackups error"

    invoke-static {v0, v7, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 560
    .end local v3    # "operationsToCancel":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v4    # "operationStorage":Lcom/android/server/backup/OperationStorage;
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v6    # "backupWaitOperations":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .end local v8    # "restoreWaitOperations":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :goto_2
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 561
    nop

    .line 562
    return-void

    .line 560
    :catchall_0
    move-exception v0

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 561
    throw v0
.end method

.method public doFullBackup(Landroid/app/IBackupAgent;Landroid/os/ParcelFileDescriptor;JILandroid/app/backup/IBackupManager;II)V
    .locals 5
    .param p1, "agent"    # Landroid/app/IBackupAgent;
    .param p2, "pipe"    # Landroid/os/ParcelFileDescriptor;
    .param p3, "quotaBytes"    # J
    .param p5, "token"    # I
    .param p6, "backupManagerBinder"    # Landroid/app/backup/IBackupManager;
    .param p7, "transportFlags"    # I
    .param p8, "fd"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 469
    const-string v0, "MiuiBackup"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lmiui/app/backup/IBackupManager;

    .line 470
    .local v0, "bm":Lmiui/app/backup/IBackupManager;
    invoke-interface {v0, p8}, Lmiui/app/backup/IBackupManager;->isRunningFromMiui(I)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Lmiui/app/backup/IBackupManager;->shouldSkipData()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_4

    .line 473
    :cond_0
    const-string/jumbo v1, "skip app data"

    const-string v2, "Backup:BackupManagerServiceInjector"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    const/4 v1, 0x0

    .line 479
    .local v1, "out":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    move-object v1, v3

    .line 480
    const/4 v3, 0x4

    new-array v3, v3, [B

    .line 481
    .local v3, "buf":[B
    invoke-virtual {v1, v3}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 485
    .end local v3    # "buf":[B
    nop

    .line 487
    :try_start_1
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 490
    :goto_0
    goto :goto_1

    .line 488
    :catch_0
    move-exception v2

    .line 489
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .end local v2    # "e":Ljava/io/IOException;
    goto :goto_0

    .line 485
    :catchall_0
    move-exception v2

    goto :goto_2

    .line 482
    :catch_1
    move-exception v3

    .line 483
    .local v3, "e":Ljava/io/IOException;
    :try_start_2
    const-string v4, "Unable to finalize backup stream!"

    invoke-static {v2, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 485
    nop

    .end local v3    # "e":Ljava/io/IOException;
    if-eqz v1, :cond_1

    .line 487
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 493
    :cond_1
    :goto_1
    const-wide/16 v2, 0x0

    invoke-interface {p6, p5, v2, v3}, Landroid/app/backup/IBackupManager;->opComplete(IJ)V

    goto :goto_5

    .line 485
    :goto_2
    if-eqz v1, :cond_2

    .line 487
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 490
    goto :goto_3

    .line 488
    :catch_2
    move-exception v3

    .line 489
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    .line 492
    .end local v3    # "e":Ljava/io/IOException;
    :cond_2
    :goto_3
    throw v2

    .line 471
    .end local v1    # "out":Ljava/io/FileOutputStream;
    :cond_3
    :goto_4
    invoke-interface/range {p1 .. p7}, Landroid/app/IBackupAgent;->doFullBackup(Landroid/os/ParcelFileDescriptor;JILandroid/app/backup/IBackupManager;I)V

    .line 495
    :goto_5
    return-void
.end method

.method public errorOccur(II)V
    .locals 4
    .param p1, "errCode"    # I
    .param p2, "fd"    # I

    .line 77
    const-string v0, "MiuiBackup"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lmiui/app/backup/IBackupManager;

    .line 79
    .local v0, "bm":Lmiui/app/backup/IBackupManager;
    :try_start_0
    invoke-interface {v0, p2}, Lmiui/app/backup/IBackupManager;->isRunningFromMiui(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 80
    invoke-interface {v0, p1}, Lmiui/app/backup/IBackupManager;->errorOccur(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :cond_0
    goto :goto_0

    .line 82
    :catch_0
    move-exception v1

    .line 83
    .local v1, "e":Landroid/os/RemoteException;
    const-string v2, "Backup:BackupManagerServiceInjector"

    const-string v3, "errorOccur failed"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 85
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method public errorOccur(ILjava/io/InputStream;)V
    .locals 2
    .param p1, "errCode"    # I
    .param p2, "inputStream"    # Ljava/io/InputStream;

    .line 89
    instance-of v0, p2, Ljava/io/FileInputStream;

    if-eqz v0, :cond_0

    .line 90
    move-object v0, p2

    check-cast v0, Ljava/io/FileInputStream;

    .line 92
    .local v0, "fileInputStream":Ljava/io/FileInputStream;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/FileDescriptor;->getInt$()I

    move-result v1

    .line 93
    .local v1, "fd":I
    invoke-virtual {p0, p1, v1}, Lcom/android/server/backup/BackupManagerServiceInjector;->errorOccur(II)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    .end local v1    # "fd":I
    goto :goto_0

    .line 94
    :catch_0
    move-exception v1

    .line 95
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 98
    .end local v0    # "fileInputStream":Ljava/io/FileInputStream;
    .end local v1    # "e":Ljava/io/IOException;
    :cond_0
    :goto_0
    return-void
.end method

.method public getAppUserId(II)I
    .locals 4
    .param p1, "fd"    # I
    .param p2, "def"    # I

    .line 275
    const-string v0, "MiuiBackup"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lmiui/app/backup/IBackupManager;

    .line 277
    .local v0, "bm":Lmiui/app/backup/IBackupManager;
    :try_start_0
    invoke-interface {v0, p1}, Lmiui/app/backup/IBackupManager;->isRunningFromMiui(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 278
    invoke-interface {v0}, Lmiui/app/backup/IBackupManager;->getAppUserId()I

    move-result v1
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return v1

    .line 282
    :cond_0
    goto :goto_0

    .line 280
    :catch_0
    move-exception v1

    .line 281
    .local v1, "e":Landroid/os/RemoteException;
    const-string v2, "Backup:BackupManagerServiceInjector"

    const-string v3, "getAppUserId failed"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 283
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_0
    return p2
.end method

.method public getApplicationInfo(Landroid/content/Context;Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pkgName"    # Ljava/lang/String;
    .param p3, "fd"    # I
    .param p4, "userIdDef"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/pm/PackageManager$NameNotFoundException;
        }
    .end annotation

    .line 288
    const-string v0, "MiuiBackup"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lmiui/app/backup/IBackupManager;

    .line 289
    .local v0, "bm":Lmiui/app/backup/IBackupManager;
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 291
    .local v1, "pm":Landroid/content/pm/PackageManager;
    const/4 v2, 0x0

    :try_start_0
    invoke-interface {v0, p3}, Lmiui/app/backup/IBackupManager;->isRunningFromMiui(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 292
    invoke-interface {v0}, Lmiui/app/backup/IBackupManager;->getAppUserId()I

    move-result v3

    .line 293
    .local v3, "userId":I
    invoke-static {p1, p2}, Lmiui/app/backup/BackupManager;->isSysAppForBackup(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 294
    const/16 v4, 0x400

    invoke-virtual {v1, p2, v4, v3}, Landroid/content/pm/PackageManager;->getApplicationInfoAsUser(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    return-object v2

    .line 296
    :cond_0
    invoke-virtual {v1, p2, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfoAsUser(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;

    move-result-object v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    .line 301
    .end local v3    # "userId":I
    :cond_1
    goto :goto_0

    .line 299
    :catch_0
    move-exception v3

    .line 300
    .local v3, "e":Landroid/os/RemoteException;
    const-string v4, "Backup:BackupManagerServiceInjector"

    const-string v5, "getApplicationInfo failed"

    invoke-static {v4, v5, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 302
    .end local v3    # "e":Landroid/os/RemoteException;
    :goto_0
    invoke-virtual {v1, p2, v2, p4}, Landroid/content/pm/PackageManager;->getApplicationInfoAsUser(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    return-object v2
.end method

.method public getPackageInfo(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pkgName"    # Ljava/lang/String;
    .param p3, "fd"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/pm/PackageManager$NameNotFoundException;
        }
    .end annotation

    .line 307
    const-string v0, "MiuiBackup"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lmiui/app/backup/IBackupManager;

    .line 308
    .local v0, "bm":Lmiui/app/backup/IBackupManager;
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 310
    .local v1, "pm":Landroid/content/pm/PackageManager;
    const/high16 v2, 0x8000000

    :try_start_0
    invoke-interface {v0, p3}, Lmiui/app/backup/IBackupManager;->isRunningFromMiui(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 311
    invoke-interface {v0}, Lmiui/app/backup/IBackupManager;->getAppUserId()I

    move-result v3

    .line 312
    .local v3, "userId":I
    invoke-static {p1, p2}, Lmiui/app/backup/BackupManager;->isSysAppForBackup(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 313
    const v4, 0x8000400

    invoke-virtual {v1, p2, v4, v3}, Landroid/content/pm/PackageManager;->getPackageInfoAsUser(Ljava/lang/String;II)Landroid/content/pm/PackageInfo;

    move-result-object v2

    return-object v2

    .line 315
    :cond_0
    invoke-virtual {v1, p2, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfoAsUser(Ljava/lang/String;II)Landroid/content/pm/PackageInfo;

    move-result-object v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    .line 320
    .end local v3    # "userId":I
    :cond_1
    goto :goto_0

    .line 318
    :catch_0
    move-exception v3

    .line 319
    .local v3, "e":Landroid/os/RemoteException;
    const-string v4, "Backup:BackupManagerServiceInjector"

    const-string v5, "getPackageInfo failed"

    invoke-static {v4, v5, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 321
    .end local v3    # "e":Landroid/os/RemoteException;
    :goto_0
    invoke-virtual {v1, p2, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    return-object v2
.end method

.method public hookUserIdForXSpace(I)I
    .locals 1
    .param p1, "userID"    # I

    .line 613
    invoke-virtual {p0, p1}, Lcom/android/server/backup/BackupManagerServiceInjector;->isXSpaceUser(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    move v0, p1

    :goto_0
    return v0
.end method

.method public installExistingPackageAsUser(Ljava/lang/String;I)Z
    .locals 10
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 588
    const-string v0, "Backup:BackupManagerServiceInjector"

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v2

    .line 589
    .local v2, "packageManager":Landroid/content/pm/IPackageManager;
    invoke-static {v2, p1}, Lcom/miui/server/BackupProxyHelper;->isAppInXSpace(Landroid/content/pm/IPackageManager;Ljava/lang/String;)Z

    move-result v3

    const/4 v9, 0x1

    if-nez v3, :cond_2

    .line 591
    invoke-static {v2, p1}, Lcom/miui/server/BackupProxyHelper;->isMiPushRequired(Landroid/content/pm/IPackageManager;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "com.xiaomi.xmsf"

    .line 592
    invoke-static {v2, v3}, Lcom/miui/server/BackupProxyHelper;->isAppInXSpace(Landroid/content/pm/IPackageManager;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 593
    const-string v4, "com.xiaomi.xmsf"

    const v6, 0x404000

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v3, v2

    move v5, p2

    invoke-interface/range {v3 .. v8}, Landroid/content/pm/IPackageManager;->installExistingPackageAsUser(Ljava/lang/String;IIILjava/util/List;)I

    .line 595
    const-string v3, "Require XMSF, auto installed! "

    invoke-static {v0, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 597
    :cond_0
    const v6, 0x404000

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v3, v2

    move-object v4, p1

    move v5, p2

    invoke-interface/range {v3 .. v8}, Landroid/content/pm/IPackageManager;->installExistingPackageAsUser(Ljava/lang/String;IIILjava/util/List;)I

    move-result v0

    .line 599
    .local v0, "result":I
    if-ne v0, v9, :cond_1

    move v1, v9

    :cond_1
    return v1

    .line 601
    .end local v0    # "result":I
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "has been installed, pkgName:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", userId:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 602
    return v9

    .line 603
    .end local v2    # "packageManager":Landroid/content/pm/IPackageManager;
    :catch_0
    move-exception v2

    .line 604
    .local v2, "e":Landroid/os/RemoteException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to install package: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 606
    .end local v2    # "e":Landroid/os/RemoteException;
    return v1
.end method

.method public isCanceling(I)Z
    .locals 4
    .param p1, "fd"    # I

    .line 512
    const-string v0, "MiuiBackup"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lmiui/app/backup/IBackupManager;

    .line 514
    .local v0, "bm":Lmiui/app/backup/IBackupManager;
    :try_start_0
    invoke-interface {v0, p1}, Lmiui/app/backup/IBackupManager;->isRunningFromMiui(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 515
    invoke-interface {v0}, Lmiui/app/backup/IBackupManager;->isCanceling()Z

    move-result v1
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return v1

    .line 519
    :cond_0
    goto :goto_0

    .line 517
    :catch_0
    move-exception v1

    .line 518
    .local v1, "e":Landroid/os/RemoteException;
    const-string v2, "Backup:BackupManagerServiceInjector"

    const-string v3, "isCanceling error"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 520
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_0
    const/4 v1, 0x0

    return v1
.end method

.method public isForceAllowBackup(Landroid/content/pm/PackageInfo;I)Z
    .locals 1
    .param p1, "info"    # Landroid/content/pm/PackageInfo;
    .param p2, "fd"    # I

    .line 267
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_1

    invoke-static {p1}, Lmiui/app/backup/BackupManager;->isSysAppForBackup(Landroid/content/pm/PackageInfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 270
    :cond_0
    const/4 v0, 0x0

    return v0

    .line 268
    :cond_1
    :goto_0
    invoke-virtual {p0, p2}, Lcom/android/server/backup/BackupManagerServiceInjector;->isRunningFromMiui(I)Z

    move-result v0

    return v0
.end method

.method public isNeedBeKilled(Ljava/lang/String;I)Z
    .locals 5
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "fd"    # I

    .line 326
    const/4 v0, 0x1

    .line 327
    .local v0, "is":Z
    const-string v1, "MiuiBackup"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    check-cast v1, Lmiui/app/backup/IBackupManager;

    .line 329
    .local v1, "bm":Lmiui/app/backup/IBackupManager;
    :try_start_0
    invoke-interface {v1, p2}, Lmiui/app/backup/IBackupManager;->isRunningFromMiui(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 330
    invoke-interface {v1, p1}, Lmiui/app/backup/IBackupManager;->isNeedBeKilled(Ljava/lang/String;)Z

    move-result v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v2

    .line 334
    :cond_0
    goto :goto_0

    .line 332
    :catch_0
    move-exception v2

    .line 333
    .local v2, "e":Landroid/os/RemoteException;
    const-string v3, "Backup:BackupManagerServiceInjector"

    const-string v4, "isNeedBeKilled failed"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 335
    .end local v2    # "e":Landroid/os/RemoteException;
    :goto_0
    return v0
.end method

.method public isRunningFromMiui(I)Z
    .locals 4
    .param p1, "fd"    # I

    .line 235
    const-string v0, "MiuiBackup"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lmiui/app/backup/IBackupManager;

    .line 237
    .local v0, "bm":Lmiui/app/backup/IBackupManager;
    :try_start_0
    invoke-interface {v0, p1}, Lmiui/app/backup/IBackupManager;->isRunningFromMiui(I)Z

    move-result v1
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    .line 238
    const/4 v1, 0x1

    return v1

    .line 242
    :cond_0
    goto :goto_0

    .line 240
    :catch_0
    move-exception v1

    .line 241
    .local v1, "e":Landroid/os/RemoteException;
    const-string v2, "Backup:BackupManagerServiceInjector"

    const-string v3, "isRunningFromMiui error"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 243
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_0
    const/4 v1, 0x0

    return v1
.end method

.method public isRunningFromMiui(Ljava/io/InputStream;)Z
    .locals 4
    .param p1, "inputStream"    # Ljava/io/InputStream;

    .line 248
    instance-of v0, p1, Ljava/io/FileInputStream;

    if-eqz v0, :cond_0

    .line 249
    move-object v0, p1

    check-cast v0, Ljava/io/FileInputStream;

    .line 251
    .local v0, "fileInputStream":Ljava/io/FileInputStream;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/FileDescriptor;->getInt$()I

    move-result v1

    .line 252
    .local v1, "fd":I
    invoke-virtual {p0, v1}, Lcom/android/server/backup/BackupManagerServiceInjector;->isRunningFromMiui(I)Z

    move-result v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return v2

    .line 253
    .end local v1    # "fd":I
    :catch_0
    move-exception v1

    .line 254
    .local v1, "e":Ljava/io/IOException;
    const-string v2, "Backup:BackupManagerServiceInjector"

    const-string v3, "isRunningFromMiui error"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 257
    .end local v0    # "fileInputStream":Ljava/io/FileInputStream;
    .end local v1    # "e":Ljava/io/IOException;
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isSysAppRunningFromMiui(Landroid/content/pm/PackageInfo;I)Z
    .locals 1
    .param p1, "info"    # Landroid/content/pm/PackageInfo;
    .param p2, "fd"    # I

    .line 262
    invoke-static {p1}, Lmiui/app/backup/BackupManager;->isSysAppForBackup(Landroid/content/pm/PackageInfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p2}, Lcom/android/server/backup/BackupManagerServiceInjector;->isRunningFromMiui(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isXSpaceUser(I)Z
    .locals 1
    .param p1, "userId"    # I

    .line 566
    const/16 v0, 0x3e7

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isXSpaceUserRunning()Z
    .locals 3

    .line 572
    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v1

    .line 573
    .local v1, "activityManager":Landroid/app/IActivityManager;
    const/16 v2, 0x3e7

    invoke-interface {v1, v2, v0}, Landroid/app/IActivityManager;->isUserRunning(II)Z

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 574
    .end local v1    # "activityManager":Landroid/app/IActivityManager;
    :catch_0
    move-exception v1

    .line 577
    return v0
.end method

.method public linkToDeath(Landroid/app/IBackupAgent;ILandroid/os/ParcelFileDescriptor;)V
    .locals 5
    .param p1, "backupAgent"    # Landroid/app/IBackupAgent;
    .param p2, "fd"    # I
    .param p3, "outPipe"    # Landroid/os/ParcelFileDescriptor;

    .line 212
    if-eqz p1, :cond_0

    .line 213
    invoke-interface {p1}, Landroid/app/IBackupAgent;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 214
    .local v0, "agentBinder":Landroid/os/IBinder;
    new-instance v1, Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;

    invoke-direct {v1, p0, v0, p2, p3}, Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;-><init>(Lcom/android/server/backup/BackupManagerServiceInjector;Landroid/os/IBinder;ILandroid/os/ParcelFileDescriptor;)V

    .line 215
    .local v1, "deathLinker":Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;
    sget-object v2, Lcom/android/server/backup/BackupManagerServiceInjector;->sBinderDeathLinker:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    const/4 v2, 0x0

    :try_start_0
    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    goto :goto_0

    .line 218
    :catch_0
    move-exception v2

    .line 219
    .local v2, "e":Landroid/os/RemoteException;
    const-string v3, "Backup:BackupManagerServiceInjector"

    const-string v4, "linkToDeath failed"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 222
    .end local v0    # "agentBinder":Landroid/os/IBinder;
    .end local v1    # "deathLinker":Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;
    .end local v2    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_0
    return-void
.end method

.method public needUpdateToken(Landroid/app/IBackupAgent;I)Z
    .locals 3
    .param p1, "backupAgent"    # Landroid/app/IBackupAgent;
    .param p2, "token"    # I

    .line 199
    const/4 v0, 0x0

    .line 200
    .local v0, "needUpdateToken":Z
    if-eqz p1, :cond_0

    .line 201
    invoke-interface {p1}, Landroid/app/IBackupAgent;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-interface {v1}, Landroid/os/IBinder;->isBinderAlive()Z

    move-result v0

    .line 202
    sget-object v1, Lcom/android/server/backup/BackupManagerServiceInjector;->sBinderDeathLinker:Ljava/util/HashMap;

    invoke-interface {p1}, Landroid/app/IBackupAgent;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;

    .line 203
    .local v1, "deathLinker":Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;
    if-eqz v1, :cond_0

    .line 204
    invoke-virtual {v1, p2}, Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;->setToken(I)V

    .line 207
    .end local v1    # "deathLinker":Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;
    :cond_0
    return v0
.end method

.method public onApkInstalled(I)V
    .locals 4
    .param p1, "fd"    # I

    .line 138
    const-string v0, "MiuiBackup"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lmiui/app/backup/IBackupManager;

    .line 140
    .local v0, "bm":Lmiui/app/backup/IBackupManager;
    :try_start_0
    invoke-interface {v0, p1}, Lmiui/app/backup/IBackupManager;->isRunningFromMiui(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 141
    invoke-interface {v0}, Lmiui/app/backup/IBackupManager;->onApkInstalled()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 145
    :cond_0
    goto :goto_0

    .line 143
    :catch_0
    move-exception v1

    .line 144
    .local v1, "e":Landroid/os/RemoteException;
    const-string v2, "Backup:BackupManagerServiceInjector"

    const-string v3, "onApkInstalled failed"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 146
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method public prepareOperationTimeout(Lcom/android/server/backup/UserBackupManagerService;IJLcom/android/server/backup/BackupRestoreTask;II)V
    .locals 11
    .param p1, "thiz"    # Lcom/android/server/backup/UserBackupManagerService;
    .param p2, "token"    # I
    .param p3, "interval"    # J
    .param p5, "callback"    # Lcom/android/server/backup/BackupRestoreTask;
    .param p6, "operationType"    # I
    .param p7, "fd"    # I

    .line 454
    const-string v1, "Backup:BackupManagerServiceInjector"

    const-string v0, "MiuiBackup"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lmiui/app/backup/IBackupManager;

    .line 455
    .local v2, "bm":Lmiui/app/backup/IBackupManager;
    const/4 v3, 0x1

    .line 457
    .local v3, "backupTimeoutScale":I
    move/from16 v4, p7

    :try_start_0
    invoke-interface {v2, v4}, Lmiui/app/backup/IBackupManager;->isRunningFromMiui(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 458
    invoke-interface {v2}, Lmiui/app/backup/IBackupManager;->getBackupTimeoutScale()I

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move v3, v0

    .line 462
    :cond_0
    goto :goto_0

    .line 460
    :catch_0
    move-exception v0

    .line 461
    .local v0, "e":Landroid/os/RemoteException;
    const-string v5, "prepareOperationTimeout failed"

    invoke-static {v1, v5, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 463
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "prepareOperationTimeout backupTimeoutScale = "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    int-to-long v0, v3

    mul-long v7, v0, p3

    move-object v5, p1

    move v6, p2

    move-object/from16 v9, p5

    move/from16 v10, p6

    invoke-virtual/range {v5 .. v10}, Lcom/android/server/backup/UserBackupManagerService;->prepareOperationTimeout(IJLcom/android/server/backup/BackupRestoreTask;I)V

    .line 465
    return-void
.end method

.method public readMiuiBackupHeader(Landroid/os/ParcelFileDescriptor;I)V
    .locals 4
    .param p1, "in"    # Landroid/os/ParcelFileDescriptor;
    .param p2, "fd"    # I

    .line 114
    const-string v0, "MiuiBackup"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lmiui/app/backup/IBackupManager;

    .line 116
    .local v0, "bm":Lmiui/app/backup/IBackupManager;
    :try_start_0
    invoke-interface {v0, p2}, Lmiui/app/backup/IBackupManager;->isRunningFromMiui(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 117
    invoke-interface {v0, p1}, Lmiui/app/backup/IBackupManager;->readMiuiBackupHeader(Landroid/os/ParcelFileDescriptor;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :cond_0
    goto :goto_0

    .line 119
    :catch_0
    move-exception v1

    .line 120
    .local v1, "e":Landroid/os/RemoteException;
    const-string v2, "Backup:BackupManagerServiceInjector"

    const-string v3, "readMiuiBackupHeader failed"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 122
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method public registerHuanjiReceiver(Lcom/android/server/backup/UserBackupManagerService;Landroid/content/Context;)V
    .locals 2
    .param p1, "service"    # Lcom/android/server/backup/UserBackupManagerService;
    .param p2, "context"    # Landroid/content/Context;

    .line 618
    const-string v0, "Backup:BackupManagerServiceInjector"

    const-string v1, "registerHuanjiReceiver."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 619
    iget-object v0, p0, Lcom/android/server/backup/BackupManagerServiceInjector;->mHuanjiReceiver:Lcom/android/server/backup/BackupManagerServiceInjector$HuanjiReceiver;

    if-nez v0, :cond_0

    .line 620
    new-instance v0, Lcom/android/server/backup/BackupManagerServiceInjector$HuanjiReceiver;

    invoke-direct {v0, p1}, Lcom/android/server/backup/BackupManagerServiceInjector$HuanjiReceiver;-><init>(Lcom/android/server/backup/UserBackupManagerService;)V

    iput-object v0, p0, Lcom/android/server/backup/BackupManagerServiceInjector;->mHuanjiReceiver:Lcom/android/server/backup/BackupManagerServiceInjector$HuanjiReceiver;

    .line 621
    invoke-virtual {v0, p2}, Lcom/android/server/backup/BackupManagerServiceInjector$HuanjiReceiver;->register(Landroid/content/Context;)V

    .line 623
    :cond_0
    return-void
.end method

.method public restoreFileEnd(Lcom/android/server/backup/UserBackupManagerService;Landroid/app/IBackupAgent;Landroid/app/backup/IBackupManager;ILandroid/os/Handler;I)V
    .locals 16
    .param p1, "thiz"    # Lcom/android/server/backup/UserBackupManagerService;
    .param p2, "agent"    # Landroid/app/IBackupAgent;
    .param p3, "backupManagerBinder"    # Landroid/app/backup/IBackupManager;
    .param p4, "fd"    # I
    .param p5, "backupHandler"    # Landroid/os/Handler;
    .param p6, "restoreTimeoutMsg"    # I

    .line 341
    move-object/from16 v14, p2

    if-eqz v14, :cond_2

    .line 342
    const-string v0, "MiuiBackup"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    move-object v15, v0

    check-cast v15, Lmiui/app/backup/IBackupManager;

    .line 344
    .local v15, "bm":Lmiui/app/backup/IBackupManager;
    move/from16 v13, p4

    :try_start_0
    invoke-interface {v15, v13}, Lmiui/app/backup/IBackupManager;->isRunningFromMiui(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 345
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/backup/UserBackupManagerService;->generateRandomIntegerToken()I

    move-result v0

    move v12, v0

    .line 346
    .local v12, "token":I
    invoke-virtual/range {p5 .. p6}, Landroid/os/Handler;->removeMessages(I)V

    .line 347
    const-wide/32 v3, 0x493e0

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move v2, v12

    move/from16 v7, p4

    invoke-virtual/range {v0 .. v7}, Lcom/android/server/backup/BackupManagerServiceInjector;->prepareOperationTimeout(Lcom/android/server/backup/UserBackupManagerService;IJLcom/android/server/backup/BackupRestoreTask;II)V

    .line 348
    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    sget-object v6, Lmiui/app/backup/BackupManager;->DOMAIN_END:Ljava/lang/String;

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    const-wide/16 v10, 0x0

    move-object/from16 v1, p2

    move v0, v12

    .end local v12    # "token":I
    .local v0, "token":I
    move-object/from16 v13, p3

    invoke-interface/range {v1 .. v13}, Landroid/app/IBackupAgent;->doRestoreFile(Landroid/os/ParcelFileDescriptor;JILjava/lang/String;Ljava/lang/String;JJILandroid/app/backup/IBackupManager;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_2

    .line 349
    move-object/from16 v1, p0

    :try_start_1
    invoke-virtual {v1, v14, v0}, Lcom/android/server/backup/BackupManagerServiceInjector;->needUpdateToken(Landroid/app/IBackupAgent;I)Z

    move-result v2
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v2, :cond_0

    .line 350
    move-object/from16 v2, p1

    :try_start_2
    invoke-virtual {v2, v0}, Lcom/android/server/backup/UserBackupManagerService;->waitUntilOperationComplete(I)Z
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 353
    .end local v0    # "token":I
    :catch_0
    move-exception v0

    goto :goto_2

    .line 349
    .restart local v0    # "token":I
    :cond_0
    move-object/from16 v2, p1

    goto :goto_0

    .line 353
    .end local v0    # "token":I
    :catch_1
    move-exception v0

    goto :goto_1

    .line 344
    :cond_1
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    .line 355
    :goto_0
    goto :goto_3

    .line 353
    :catch_2
    move-exception v0

    move-object/from16 v1, p0

    :goto_1
    move-object/from16 v2, p1

    .line 354
    .local v0, "e":Landroid/os/RemoteException;
    :goto_2
    const-string v3, "Backup:BackupManagerServiceInjector"

    const-string v4, "restoreFileEnd failed"

    invoke-static {v3, v4, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    .line 341
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v15    # "bm":Lmiui/app/backup/IBackupManager;
    :cond_2
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    .line 357
    :goto_3
    return-void
.end method

.method public routeSocketDataToOutput(Landroid/os/ParcelFileDescriptor;Ljava/io/OutputStream;I)V
    .locals 12
    .param p1, "inPipe"    # Landroid/os/ParcelFileDescriptor;
    .param p2, "out"    # Ljava/io/OutputStream;
    .param p3, "outFd"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 362
    const-string v0, "routeSocketDataToOutput failed"

    const-string v1, "Backup:BackupManagerServiceInjector"

    const-string v2, "MiuiBackup"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    check-cast v2, Lmiui/app/backup/IBackupManager;

    .line 363
    .local v2, "bm":Lmiui/app/backup/IBackupManager;
    const/4 v3, 0x0

    .line 364
    .local v3, "raw":Ljava/io/FileInputStream;
    const/4 v4, 0x0

    .line 366
    .local v4, "in":Ljava/io/DataInputStream;
    :try_start_0
    invoke-interface {v2, p3}, Lmiui/app/backup/IBackupManager;->isRunningFromMiui(I)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 367
    invoke-interface {v2}, Lmiui/app/backup/IBackupManager;->isCanceling()Z

    move-result v5
    :try_end_0
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v5, :cond_2

    .line 397
    if-eqz v3, :cond_0

    .line 398
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 400
    :cond_0
    if-eqz v4, :cond_1

    .line 401
    invoke-virtual {v4}, Ljava/io/DataInputStream;->close()V

    .line 368
    :cond_1
    return-void

    .line 370
    :cond_2
    :try_start_1
    new-instance v5, Ljava/io/FileInputStream;

    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    move-object v3, v5

    .line 371
    new-instance v5, Ljava/io/DataInputStream;

    invoke-direct {v5, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    move-object v4, v5

    .line 373
    const v5, 0x8000

    new-array v5, v5, [B

    .line 375
    .local v5, "buffer":[B
    :cond_3
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    move v7, v6

    .local v7, "chunkTotal":I
    if-lez v6, :cond_6

    .line 376
    :goto_0
    if-lez v7, :cond_3

    .line 377
    array-length v6, v5

    if-le v7, v6, :cond_4

    array-length v6, v5

    goto :goto_1

    :cond_4
    move v6, v7

    .line 378
    .local v6, "toRead":I
    :goto_1
    const/4 v8, 0x0

    invoke-virtual {v4, v5, v8, v6}, Ljava/io/DataInputStream;->read([BII)I

    move-result v9

    .line 379
    .local v9, "nRead":I
    if-ltz v9, :cond_5

    .line 383
    invoke-virtual {p2, v5, v8, v9}, Ljava/io/OutputStream;->write([BII)V

    .line 384
    int-to-long v10, v9

    invoke-interface {v2, v10, v11}, Lmiui/app/backup/IBackupManager;->addCompletedSize(J)V

    .line 385
    sub-int/2addr v7, v9

    .line 386
    .end local v6    # "toRead":I
    .end local v9    # "nRead":I
    goto :goto_0

    .line 380
    .restart local v6    # "toRead":I
    .restart local v9    # "nRead":I
    :cond_5
    const-string v8, "Unexpectedly reached end of file while reading data"

    invoke-static {v1, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    new-instance v8, Ljava/io/EOFException;

    invoke-direct {v8}, Ljava/io/EOFException;-><init>()V

    .end local v2    # "bm":Lmiui/app/backup/IBackupManager;
    .end local v3    # "raw":Ljava/io/FileInputStream;
    .end local v4    # "in":Ljava/io/DataInputStream;
    .end local p0    # "this":Lcom/android/server/backup/BackupManagerServiceInjector;
    .end local p1    # "inPipe":Landroid/os/ParcelFileDescriptor;
    .end local p2    # "out":Ljava/io/OutputStream;
    .end local p3    # "outFd":I
    throw v8

    .line 388
    .end local v5    # "buffer":[B
    .end local v6    # "toRead":I
    .end local v7    # "chunkTotal":I
    .end local v9    # "nRead":I
    .restart local v2    # "bm":Lmiui/app/backup/IBackupManager;
    .restart local v3    # "raw":Ljava/io/FileInputStream;
    .restart local v4    # "in":Ljava/io/DataInputStream;
    .restart local p0    # "this":Lcom/android/server/backup/BackupManagerServiceInjector;
    .restart local p1    # "inPipe":Landroid/os/ParcelFileDescriptor;
    .restart local p2    # "out":Ljava/io/OutputStream;
    .restart local p3    # "outFd":I
    :cond_6
    goto :goto_2

    .line 389
    :cond_7
    invoke-static {p1, p2}, Lcom/android/server/backup/utils/FullBackupUtils;->routeSocketDataToOutput(Landroid/os/ParcelFileDescriptor;Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/InterruptedIOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 397
    :goto_2
    if-eqz v3, :cond_8

    .line 398
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 400
    :cond_8
    if-eqz v4, :cond_a

    .line 401
    :goto_3
    invoke-virtual {v4}, Ljava/io/DataInputStream;->close()V

    goto :goto_4

    .line 397
    :catchall_0
    move-exception v0

    goto :goto_5

    .line 393
    :catch_0
    move-exception v5

    .line 394
    .local v5, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :try_start_2
    invoke-static {v1, v0, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 395
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    .end local v2    # "bm":Lmiui/app/backup/IBackupManager;
    .end local v3    # "raw":Ljava/io/FileInputStream;
    .end local v4    # "in":Ljava/io/DataInputStream;
    .end local p0    # "this":Lcom/android/server/backup/BackupManagerServiceInjector;
    .end local p1    # "inPipe":Landroid/os/ParcelFileDescriptor;
    .end local p2    # "out":Ljava/io/OutputStream;
    .end local p3    # "outFd":I
    throw v0

    .line 391
    .end local v5    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    .restart local v2    # "bm":Lmiui/app/backup/IBackupManager;
    .restart local v3    # "raw":Ljava/io/FileInputStream;
    .restart local v4    # "in":Ljava/io/DataInputStream;
    .restart local p0    # "this":Lcom/android/server/backup/BackupManagerServiceInjector;
    .restart local p1    # "inPipe":Landroid/os/ParcelFileDescriptor;
    .restart local p2    # "out":Ljava/io/OutputStream;
    .restart local p3    # "outFd":I
    :catch_1
    move-exception v5

    .line 392
    .local v5, "e":Ljava/lang/Exception;
    invoke-static {v1, v0, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 397
    nop

    .end local v5    # "e":Ljava/lang/Exception;
    if-eqz v3, :cond_9

    .line 398
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 400
    :cond_9
    if-eqz v4, :cond_a

    .line 401
    goto :goto_3

    .line 404
    :cond_a
    :goto_4
    return-void

    .line 397
    :goto_5
    if-eqz v3, :cond_b

    .line 398
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 400
    :cond_b
    if-eqz v4, :cond_c

    .line 401
    invoke-virtual {v4}, Ljava/io/DataInputStream;->close()V

    .line 403
    :cond_c
    throw v0
.end method

.method public setInputFileDescriptor(Lcom/android/server/backup/restore/FullRestoreEngine;I)V
    .locals 0
    .param p1, "engine"    # Lcom/android/server/backup/restore/FullRestoreEngine;
    .param p2, "fd"    # I

    .line 408
    if-eqz p1, :cond_0

    .line 409
    iput p2, p1, Lcom/android/server/backup/restore/FullRestoreEngine;->mInputFD:I

    .line 411
    :cond_0
    return-void
.end method

.method public setOutputFileDescriptor(Lcom/android/server/backup/fullbackup/FullBackupEngine;I)V
    .locals 0
    .param p1, "engine"    # Lcom/android/server/backup/fullbackup/FullBackupEngine;
    .param p2, "fd"    # I

    .line 415
    if-eqz p1, :cond_0

    .line 416
    iput p2, p1, Lcom/android/server/backup/fullbackup/FullBackupEngine;->mOutputFD:I

    .line 418
    :cond_0
    return-void
.end method

.method public setOutputFileDescriptor(Lcom/android/server/backup/fullbackup/PerformAdbBackupTask;Landroid/os/ParcelFileDescriptor;)V
    .locals 4
    .param p1, "task"    # Lcom/android/server/backup/fullbackup/PerformAdbBackupTask;
    .param p2, "fileDescriptor"    # Landroid/os/ParcelFileDescriptor;

    .line 422
    if-eqz p1, :cond_0

    .line 425
    :try_start_0
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->getFd()I

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 429
    .local v0, "fd":I
    goto :goto_0

    .line 426
    .end local v0    # "fd":I
    :catch_0
    move-exception v0

    .line 427
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, -0x2

    .line 428
    .local v1, "fd":I
    const-string v2, "Backup:BackupManagerServiceInjector"

    const-string/jumbo v3, "setOutputFileDescriptor failed"

    invoke-static {v2, v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v1

    .line 430
    .end local v1    # "fd":I
    .local v0, "fd":I
    :goto_0
    iput v0, p1, Lcom/android/server/backup/fullbackup/PerformAdbBackupTask;->mOutputFD:I

    .line 432
    .end local v0    # "fd":I
    :cond_0
    return-void
.end method

.method public skipConfirmationUi(ILjava/lang/String;)Z
    .locals 4
    .param p1, "token"    # I
    .param p2, "action"    # Ljava/lang/String;

    .line 65
    const-string v0, "MiuiBackup"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lmiui/app/backup/IBackupManager;

    .line 67
    .local v0, "bm":Lmiui/app/backup/IBackupManager;
    :try_start_0
    invoke-interface {v0, p1, p2}, Lmiui/app/backup/IBackupManager;->startConfirmationUi(ILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    nop

    .line 72
    const/4 v1, 0x1

    return v1

    .line 68
    :catch_0
    move-exception v1

    .line 69
    .local v1, "e":Landroid/os/RemoteException;
    const-string v2, "Backup:BackupManagerServiceInjector"

    const-string v3, "confirmation failed"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 70
    const/4 v2, 0x0

    return v2
.end method

.method public tearDownAgentAndKill(Landroid/app/IActivityManager;Landroid/content/pm/ApplicationInfo;I)Z
    .locals 5
    .param p1, "activityManager"    # Landroid/app/IActivityManager;
    .param p2, "appInfo"    # Landroid/content/pm/ApplicationInfo;
    .param p3, "fd"    # I

    .line 436
    const/4 v0, 0x0

    .line 437
    .local v0, "handle":Z
    const-string v1, "MiuiBackup"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    check-cast v1, Lmiui/app/backup/IBackupManager;

    .line 439
    .local v1, "bm":Lmiui/app/backup/IBackupManager;
    :try_start_0
    invoke-interface {v1, p3}, Lmiui/app/backup/IBackupManager;->isRunningFromMiui(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 440
    iget-object v2, p2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p0, v2, p3}, Lcom/android/server/backup/BackupManagerServiceInjector;->isNeedBeKilled(Ljava/lang/String;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 441
    const/4 v0, 0x1

    .line 442
    invoke-interface {p1, p2}, Landroid/app/IActivityManager;->unbindBackupAgent(Landroid/content/pm/ApplicationInfo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 447
    :cond_0
    goto :goto_0

    .line 445
    :catch_0
    move-exception v2

    .line 446
    .local v2, "e":Landroid/os/RemoteException;
    const-string v3, "Backup:BackupManagerServiceInjector"

    const-string v4, "isNeedBeKilled failed"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 448
    .end local v2    # "e":Landroid/os/RemoteException;
    :goto_0
    return v0
.end method

.method public unlinkToDeath(Landroid/app/IBackupAgent;)V
    .locals 3
    .param p1, "backupAgent"    # Landroid/app/IBackupAgent;

    .line 226
    if-eqz p1, :cond_0

    .line 227
    invoke-interface {p1}, Landroid/app/IBackupAgent;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 228
    .local v0, "agentBinder":Landroid/os/IBinder;
    sget-object v1, Lcom/android/server/backup/BackupManagerServiceInjector;->sBinderDeathLinker:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/IBinder$DeathRecipient;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 229
    sget-object v1, Lcom/android/server/backup/BackupManagerServiceInjector;->sBinderDeathLinker:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    .end local v0    # "agentBinder":Landroid/os/IBinder;
    :cond_0
    return-void
.end method

.method public unregisterHuanjiReceiver(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 627
    const-string v0, "Backup:BackupManagerServiceInjector"

    const-string/jumbo v1, "unregisterHuanjiReceiver."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 628
    iget-object v0, p0, Lcom/android/server/backup/BackupManagerServiceInjector;->mHuanjiReceiver:Lcom/android/server/backup/BackupManagerServiceInjector$HuanjiReceiver;

    if-eqz v0, :cond_0

    .line 629
    invoke-virtual {v0, p1}, Lcom/android/server/backup/BackupManagerServiceInjector$HuanjiReceiver;->unregister(Landroid/content/Context;)V

    .line 631
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/backup/BackupManagerServiceInjector;->mHuanjiReceiver:Lcom/android/server/backup/BackupManagerServiceInjector$HuanjiReceiver;

    .line 632
    return-void
.end method

.method public waitingBeforeGetAgent()V
    .locals 2

    .line 503
    const-wide/16 v0, 0xc8

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 506
    goto :goto_0

    .line 504
    :catch_0
    move-exception v0

    .line 505
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 507
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :goto_0
    return-void
.end method

.method public writeMiuiBackupHeader(Landroid/os/ParcelFileDescriptor;I)V
    .locals 4
    .param p1, "out"    # Landroid/os/ParcelFileDescriptor;
    .param p2, "fd"    # I

    .line 102
    const-string v0, "MiuiBackup"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lmiui/app/backup/IBackupManager;

    .line 104
    .local v0, "bm":Lmiui/app/backup/IBackupManager;
    :try_start_0
    invoke-interface {v0, p2}, Lmiui/app/backup/IBackupManager;->isRunningFromMiui(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 105
    invoke-interface {v0, p1}, Lmiui/app/backup/IBackupManager;->writeMiuiBackupHeader(Landroid/os/ParcelFileDescriptor;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    :cond_0
    goto :goto_0

    .line 107
    :catch_0
    move-exception v1

    .line 108
    .local v1, "e":Landroid/os/RemoteException;
    const-string v2, "Backup:BackupManagerServiceInjector"

    const-string/jumbo v3, "writeMiuiBackupHeader failed"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 110
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method
