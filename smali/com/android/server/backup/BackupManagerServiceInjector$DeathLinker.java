class com.android.server.backup.BackupManagerServiceInjector$DeathLinker implements android.os.IBinder$DeathRecipient {
	 /* .source "BackupManagerServiceInjector.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/backup/BackupManagerServiceInjector; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "DeathLinker" */
} // .end annotation
/* # instance fields */
private android.os.IBinder mAgentBinder;
private Integer mCallerFd;
private android.os.ParcelFileDescriptor mOutPipe;
private Integer mToken;
final com.android.server.backup.BackupManagerServiceInjector this$0; //synthetic
/* # direct methods */
public com.android.server.backup.BackupManagerServiceInjector$DeathLinker ( ) {
/* .locals 0 */
/* .param p2, "agentBinder" # Landroid/os/IBinder; */
/* .param p3, "fd" # I */
/* .param p4, "outPipe" # Landroid/os/ParcelFileDescriptor; */
/* .line 156 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 154 */
int p1 = -1; // const/4 p1, -0x1
/* iput p1, p0, Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;->mToken:I */
/* .line 157 */
this.mAgentBinder = p2;
/* .line 158 */
/* iput p3, p0, Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;->mCallerFd:I */
/* .line 159 */
this.mOutPipe = p4;
/* .line 160 */
return;
} // .end method
private void tearDownPipes ( ) {
/* .locals 4 */
/* .line 179 */
final String v0 = "Backup:BackupManagerServiceInjector"; // const-string v0, "Backup:BackupManagerServiceInjector"
final String v1 = "MiuiBackup"; // const-string v1, "MiuiBackup"
android.os.ServiceManager .getService ( v1 );
/* check-cast v1, Lmiui/app/backup/IBackupManager; */
/* .line 181 */
/* .local v1, "bm":Lmiui/app/backup/IBackupManager; */
try { // :try_start_0
	 v2 = 	 /* iget v2, p0, Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;->mCallerFd:I */
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 /* .line 182 */
		 v2 = this.mOutPipe;
		 /* :try_end_0 */
		 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_1 */
		 if ( v2 != null) { // if-eqz v2, :cond_0
			 /* .line 184 */
			 try { // :try_start_1
				 (( android.os.ParcelFileDescriptor ) v2 ).getFileDescriptor ( ); // invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;
				 libcore.io.IoBridge .closeAndSignalBlockedThreads ( v2 );
				 /* .line 185 */
				 int v2 = 0; // const/4 v2, 0x0
				 this.mOutPipe = v2;
				 /* :try_end_1 */
				 /* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
				 /* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_1 */
				 /* .line 188 */
				 /* .line 186 */
				 /* :catch_0 */
				 /* move-exception v2 */
				 /* .line 187 */
				 /* .local v2, "e":Ljava/io/IOException; */
				 try { // :try_start_2
					 final String v3 = "Couldn\'t close agent pipes"; // const-string v3, "Couldn\'t close agent pipes"
					 android.util.Slog .w ( v0,v3,v2 );
					 /* :try_end_2 */
					 /* .catch Landroid/os/RemoteException; {:try_start_2 ..:try_end_2} :catch_1 */
					 /* .line 193 */
				 } // .end local v2 # "e":Ljava/io/IOException;
			 } // :cond_0
		 } // :goto_0
		 /* .line 191 */
		 /* :catch_1 */
		 /* move-exception v2 */
		 /* .line 192 */
		 /* .local v2, "e":Landroid/os/RemoteException; */
		 final String v3 = "errorOccur failed"; // const-string v3, "errorOccur failed"
		 android.util.Slog .e ( v0,v3,v2 );
		 /* .line 194 */
	 } // .end local v2 # "e":Landroid/os/RemoteException;
} // :goto_1
return;
} // .end method
/* # virtual methods */
public void binderDied ( ) {
/* .locals 4 */
/* .line 168 */
/* invoke-direct {p0}, Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;->tearDownPipes()V */
/* .line 169 */
final String v0 = "backup"; // const-string v0, "backup"
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Landroid/app/backup/IBackupManager; */
/* .line 171 */
/* .local v0, "bm":Landroid/app/backup/IBackupManager; */
try { // :try_start_0
	 /* iget v1, p0, Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;->mToken:I */
	 /* const-wide/16 v2, 0x0 */
	 /* :try_end_0 */
	 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 174 */
	 /* .line 172 */
	 /* :catch_0 */
	 /* move-exception v1 */
	 /* .line 173 */
	 /* .local v1, "e":Landroid/os/RemoteException; */
	 final String v2 = "Backup:BackupManagerServiceInjector"; // const-string v2, "Backup:BackupManagerServiceInjector"
	 final String v3 = "binderDied failed"; // const-string v3, "binderDied failed"
	 android.util.Slog .e ( v2,v3,v1 );
	 /* .line 175 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_0
v1 = this.this$0;
/* const/16 v2, 0x8 */
/* iget v3, p0, Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;->mCallerFd:I */
(( com.android.server.backup.BackupManagerServiceInjector ) v1 ).errorOccur ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/backup/BackupManagerServiceInjector;->errorOccur(II)V
/* .line 176 */
return;
} // .end method
public void setToken ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "token" # I */
/* .line 163 */
/* iput p1, p0, Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;->mToken:I */
/* .line 164 */
return;
} // .end method
