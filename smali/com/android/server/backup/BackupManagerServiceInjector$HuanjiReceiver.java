public class com.android.server.backup.BackupManagerServiceInjector$HuanjiReceiver extends android.content.BroadcastReceiver {
	 /* .source "BackupManagerServiceInjector.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/backup/BackupManagerServiceInjector; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "HuanjiReceiver" */
} // .end annotation
/* # static fields */
static final java.lang.String ACTION_HUANJI_END_RESTORE;
static final java.lang.String ACTION_HUANJI_RESTORE;
private static final java.lang.String TAG;
/* # instance fields */
private com.android.server.backup.UserBackupManagerService mUserBackupManagerService;
/* # direct methods */
public com.android.server.backup.BackupManagerServiceInjector$HuanjiReceiver ( ) {
/* .locals 0 */
/* .param p1, "userBackupManagerService" # Lcom/android/server/backup/UserBackupManagerService; */
/* .line 640 */
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
/* .line 641 */
this.mUserBackupManagerService = p1;
/* .line 642 */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 646 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 647 */
/* .local v0, "action":Ljava/lang/String; */
/* if-nez v0, :cond_0 */
/* .line 648 */
return;
/* .line 650 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "get action: "; // const-string v2, "get action: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "HuanjiReceiver"; // const-string v2, "HuanjiReceiver"
android.util.Log .i ( v2,v1 );
/* .line 651 */
v1 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
/* sparse-switch v1, :sswitch_data_0 */
} // :cond_1
/* :sswitch_0 */
final String v1 = "com.miui.huanji.END_RESTORE"; // const-string v1, "com.miui.huanji.END_RESTORE"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* move v1, v3 */
/* :sswitch_1 */
final String v1 = "com.miui.huanji.START_RESTORE"; // const-string v1, "com.miui.huanji.START_RESTORE"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* move v1, v2 */
} // :goto_0
int v1 = -1; // const/4 v1, -0x1
} // :goto_1
/* packed-switch v1, :pswitch_data_0 */
/* .line 656 */
/* :pswitch_0 */
v1 = this.mUserBackupManagerService;
(( com.android.server.backup.UserBackupManagerService ) v1 ).setSetupComplete ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/backup/UserBackupManagerService;->setSetupComplete(Z)V
/* .line 653 */
/* :pswitch_1 */
v1 = this.mUserBackupManagerService;
(( com.android.server.backup.UserBackupManagerService ) v1 ).setSetupComplete ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/backup/UserBackupManagerService;->setSetupComplete(Z)V
/* .line 654 */
/* nop */
/* .line 659 */
} // :goto_2
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x44fa5533 -> :sswitch_1 */
/* 0x78358946 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void register ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 662 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 663 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v1 = "com.miui.huanji.START_RESTORE"; // const-string v1, "com.miui.huanji.START_RESTORE"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 664 */
final String v1 = "com.miui.huanji.END_RESTORE"; // const-string v1, "com.miui.huanji.END_RESTORE"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 665 */
final String v1 = "android.permission.BACKUP"; // const-string v1, "android.permission.BACKUP"
int v2 = 0; // const/4 v2, 0x0
(( android.content.Context ) p1 ).registerReceiver ( p0, v0, v1, v2 ); // invoke-virtual {p1, p0, v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;
/* .line 667 */
return;
} // .end method
public void unregister ( android.content.Context p0 ) {
/* .locals 0 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 670 */
(( android.content.Context ) p1 ).unregisterReceiver ( p0 ); // invoke-virtual {p1, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
/* .line 671 */
return;
} // .end method
