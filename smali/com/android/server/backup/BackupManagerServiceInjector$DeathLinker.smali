.class Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;
.super Ljava/lang/Object;
.source "BackupManagerServiceInjector.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/backup/BackupManagerServiceInjector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeathLinker"
.end annotation


# instance fields
.field private mAgentBinder:Landroid/os/IBinder;

.field private mCallerFd:I

.field private mOutPipe:Landroid/os/ParcelFileDescriptor;

.field private mToken:I

.field final synthetic this$0:Lcom/android/server/backup/BackupManagerServiceInjector;


# direct methods
.method public constructor <init>(Lcom/android/server/backup/BackupManagerServiceInjector;Landroid/os/IBinder;ILandroid/os/ParcelFileDescriptor;)V
    .locals 0
    .param p2, "agentBinder"    # Landroid/os/IBinder;
    .param p3, "fd"    # I
    .param p4, "outPipe"    # Landroid/os/ParcelFileDescriptor;

    .line 156
    iput-object p1, p0, Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;->this$0:Lcom/android/server/backup/BackupManagerServiceInjector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    const/4 p1, -0x1

    iput p1, p0, Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;->mToken:I

    .line 157
    iput-object p2, p0, Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;->mAgentBinder:Landroid/os/IBinder;

    .line 158
    iput p3, p0, Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;->mCallerFd:I

    .line 159
    iput-object p4, p0, Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;->mOutPipe:Landroid/os/ParcelFileDescriptor;

    .line 160
    return-void
.end method

.method private tearDownPipes()V
    .locals 4

    .line 179
    const-string v0, "Backup:BackupManagerServiceInjector"

    const-string v1, "MiuiBackup"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    check-cast v1, Lmiui/app/backup/IBackupManager;

    .line 181
    .local v1, "bm":Lmiui/app/backup/IBackupManager;
    :try_start_0
    iget v2, p0, Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;->mCallerFd:I

    invoke-interface {v1, v2}, Lmiui/app/backup/IBackupManager;->isRunningFromMiui(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 182
    iget-object v2, p0, Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;->mOutPipe:Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v2, :cond_0

    .line 184
    :try_start_1
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-static {v2}, Llibcore/io/IoBridge;->closeAndSignalBlockedThreads(Ljava/io/FileDescriptor;)V

    .line 185
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;->mOutPipe:Landroid/os/ParcelFileDescriptor;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 188
    goto :goto_0

    .line 186
    :catch_0
    move-exception v2

    .line 187
    .local v2, "e":Ljava/io/IOException;
    :try_start_2
    const-string v3, "Couldn\'t close agent pipes"

    invoke-static {v0, v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    .line 193
    .end local v2    # "e":Ljava/io/IOException;
    :cond_0
    :goto_0
    goto :goto_1

    .line 191
    :catch_1
    move-exception v2

    .line 192
    .local v2, "e":Landroid/os/RemoteException;
    const-string v3, "errorOccur failed"

    invoke-static {v0, v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 194
    .end local v2    # "e":Landroid/os/RemoteException;
    :goto_1
    return-void
.end method


# virtual methods
.method public binderDied()V
    .locals 4

    .line 168
    invoke-direct {p0}, Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;->tearDownPipes()V

    .line 169
    const-string v0, "backup"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Landroid/app/backup/IBackupManager;

    .line 171
    .local v0, "bm":Landroid/app/backup/IBackupManager;
    :try_start_0
    iget v1, p0, Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;->mToken:I

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/app/backup/IBackupManager;->opComplete(IJ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 174
    goto :goto_0

    .line 172
    :catch_0
    move-exception v1

    .line 173
    .local v1, "e":Landroid/os/RemoteException;
    const-string v2, "Backup:BackupManagerServiceInjector"

    const-string v3, "binderDied failed"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 175
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_0
    iget-object v1, p0, Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;->this$0:Lcom/android/server/backup/BackupManagerServiceInjector;

    const/16 v2, 0x8

    iget v3, p0, Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;->mCallerFd:I

    invoke-virtual {v1, v2, v3}, Lcom/android/server/backup/BackupManagerServiceInjector;->errorOccur(II)V

    .line 176
    return-void
.end method

.method public setToken(I)V
    .locals 0
    .param p1, "token"    # I

    .line 163
    iput p1, p0, Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;->mToken:I

    .line 164
    return-void
.end method
