.class public Lcom/android/server/backup/SystemBackupAgentImpl;
.super Lcom/android/server/backup/SystemBackupAgentStub;
.source "SystemBackupAgentImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.backup.SystemBackupAgentStub$$"
.end annotation


# static fields
.field private static final STEP_COUNTER_HELPER:Ljava/lang/String; = "step_counter"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Lcom/android/server/backup/SystemBackupAgentStub;-><init>()V

    return-void
.end method


# virtual methods
.method public createBackupHelper(Landroid/content/Context;)Landroid/app/backup/BackupHelper;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 23
    new-instance v0, Lmiui/stepcounter/backup/StepBackupHelper;

    invoke-direct {v0, p1}, Lmiui/stepcounter/backup/StepBackupHelper;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public getHelperName()Ljava/lang/String;
    .locals 1

    .line 18
    const-string/jumbo v0, "step_counter"

    return-object v0
.end method
