public class com.android.server.backup.BackupManagerServiceInjector extends com.android.server.backup.BackupManagerServiceStub {
	 /* .source "BackupManagerServiceInjector.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.backup.BackupManagerServiceStub$$" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/android/server/backup/BackupManagerServiceInjector$HuanjiReceiver;, */
/* Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker; */
/* } */
} // .end annotation
/* # static fields */
private static final Integer INSTALL_ALL_WHITELIST_RESTRICTED_PERMISSIONS;
private static final Integer INSTALL_FULL_APP;
private static final Integer INSTALL_REASON_USER;
private static final java.lang.String TAG;
private static final java.lang.String XMSF_PKG_NAME;
private static java.util.HashMap sBinderDeathLinker;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Landroid/os/IBinder;", */
/* "Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private com.android.server.backup.BackupManagerServiceInjector$HuanjiReceiver mHuanjiReceiver;
/* # direct methods */
static com.android.server.backup.BackupManagerServiceInjector ( ) {
/* .locals 1 */
/* .line 148 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
return;
} // .end method
public com.android.server.backup.BackupManagerServiceInjector ( ) {
/* .locals 1 */
/* .line 55 */
/* invoke-direct {p0}, Lcom/android/server/backup/BackupManagerServiceStub;-><init>()V */
/* .line 58 */
int v0 = 0; // const/4 v0, 0x0
this.mHuanjiReceiver = v0;
return;
} // .end method
/* # virtual methods */
public void addRestoredSize ( Long p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "size" # J */
/* .param p3, "fd" # I */
/* .line 126 */
final String v0 = "MiuiBackup"; // const-string v0, "MiuiBackup"
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lmiui/app/backup/IBackupManager; */
/* .line 128 */
/* .local v0, "bm":Lmiui/app/backup/IBackupManager; */
v1 = try { // :try_start_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 129 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 133 */
} // :cond_0
/* .line 131 */
/* :catch_0 */
/* move-exception v1 */
/* .line 132 */
/* .local v1, "e":Landroid/os/RemoteException; */
final String v2 = "Backup:BackupManagerServiceInjector"; // const-string v2, "Backup:BackupManagerServiceInjector"
final String v3 = "addRestoredSize failed"; // const-string v3, "addRestoredSize failed"
android.util.Slog .e ( v2,v3,v1 );
/* .line 134 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
public void cancelBackups ( com.android.server.backup.UserBackupManagerService p0 ) {
/* .locals 14 */
/* .param p1, "thiz" # Lcom/android/server/backup/UserBackupManagerService; */
/* .line 525 */
final String v0 = "Backup:BackupManagerServiceInjector"; // const-string v0, "Backup:BackupManagerServiceInjector"
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v1 */
/* .line 527 */
/* .local v1, "oldToken":J */
try { // :try_start_0
/* new-instance v3, Ljava/util/ArrayList; */
/* invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V */
/* .line 528 */
/* .local v3, "operationsToCancel":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
(( com.android.server.backup.UserBackupManagerService ) p1 ).getOperationStorage ( ); // invoke-virtual {p1}, Lcom/android/server/backup/UserBackupManagerService;->getOperationStorage()Lcom/android/server/backup/OperationStorage;
/* .line 531 */
/* .local v4, "operationStorage":Lcom/android/server/backup/OperationStorage; */
int v5 = 0; // const/4 v5, 0x0
/* .line 532 */
/* .local v6, "backupWaitOperations":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
int v7 = 1; // const/4 v7, 0x1
/* .line 533 */
/* .local v8, "restoreWaitOperations":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
/* .line 534 */
/* .line 536 */
v10 = } // :goto_0
if ( v10 != null) { // if-eqz v10, :cond_0
/* check-cast v10, Ljava/lang/Integer; */
/* .line 537 */
/* .local v10, "token":Ljava/lang/Integer; */
v11 = (( java.lang.Integer ) v10 ).intValue ( ); // invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I
(( com.android.server.backup.UserBackupManagerService ) p1 ).handleCancel ( v11, v7 ); // invoke-virtual {p1, v11, v7}, Lcom/android/server/backup/UserBackupManagerService;->handleCancel(IZ)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 538 */
} // .end local v10 # "token":Ljava/lang/Integer;
/* .line 541 */
} // :cond_0
try { // :try_start_1
final String v7 = "MiuiBackup"; // const-string v7, "MiuiBackup"
android.os.ServiceManager .getService ( v7 );
/* check-cast v7, Lmiui/app/backup/IBackupManager; */
/* .line 542 */
/* .local v7, "bm":Lmiui/app/backup/IBackupManager; */
/* .line 543 */
/* .local v9, "targetPkj":Ljava/lang/String; */
(( com.android.server.backup.UserBackupManagerService ) p1 ).getContext ( ); // invoke-virtual {p1}, Lcom/android/server/backup/UserBackupManagerService;->getContext()Landroid/content/Context;
/* .line 544 */
/* .local v10, "context":Landroid/content/Context; */
(( android.content.Context ) v10 ).getPackageManager ( ); // invoke-virtual {v10}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 547 */
/* .local v11, "pm":Landroid/content/pm/PackageManager; */
/* new-instance v12, Ljava/lang/StringBuilder; */
/* invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V */
final String v13 = "sBinderDeathLinker size:"; // const-string v13, "sBinderDeathLinker size:"
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v13 = com.android.server.backup.BackupManagerServiceInjector.sBinderDeathLinker;
v13 = (( java.util.HashMap ) v13 ).size ( ); // invoke-virtual {v13}, Ljava/util/HashMap;->size()I
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).toString ( ); // invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v12 );
/* .line 548 */
v12 = miui.app.backup.BackupManager .isSysAppForBackup ( v10,v9 );
if ( v12 != null) { // if-eqz v12, :cond_1
/* .line 549 */
/* nop */
/* .line 550 */
v5 = (( com.android.server.backup.UserBackupManagerService ) p1 ).getUserId ( ); // invoke-virtual {p1}, Lcom/android/server/backup/UserBackupManagerService;->getUserId()I
/* .line 549 */
/* const/16 v12, 0x400 */
(( android.content.pm.PackageManager ) v11 ).getApplicationInfoAsUser ( v9, v12, v5 ); // invoke-virtual {v11, v9, v12, v5}, Landroid/content/pm/PackageManager;->getApplicationInfoAsUser(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;
/* .local v5, "mTargetAppInfo":Landroid/content/pm/ApplicationInfo; */
/* .line 552 */
} // .end local v5 # "mTargetAppInfo":Landroid/content/pm/ApplicationInfo;
} // :cond_1
v12 = (( com.android.server.backup.UserBackupManagerService ) p1 ).getUserId ( ); // invoke-virtual {p1}, Lcom/android/server/backup/UserBackupManagerService;->getUserId()I
(( android.content.pm.PackageManager ) v11 ).getApplicationInfoAsUser ( v9, v5, v12 ); // invoke-virtual {v11, v9, v5, v12}, Landroid/content/pm/PackageManager;->getApplicationInfoAsUser(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;
/* .line 554 */
/* .restart local v5 # "mTargetAppInfo":Landroid/content/pm/ApplicationInfo; */
} // :goto_1
(( com.android.server.backup.UserBackupManagerService ) p1 ).tearDownAgentAndKill ( v5 ); // invoke-virtual {p1, v5}, Lcom/android/server/backup/UserBackupManagerService;->tearDownAgentAndKill(Landroid/content/pm/ApplicationInfo;)V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 557 */
} // .end local v5 # "mTargetAppInfo":Landroid/content/pm/ApplicationInfo;
} // .end local v7 # "bm":Lmiui/app/backup/IBackupManager;
} // .end local v9 # "targetPkj":Ljava/lang/String;
} // .end local v10 # "context":Landroid/content/Context;
} // .end local v11 # "pm":Landroid/content/pm/PackageManager;
/* .line 555 */
/* :catch_0 */
/* move-exception v5 */
/* .line 556 */
/* .local v5, "e":Ljava/lang/Exception; */
try { // :try_start_2
final String v7 = "cancelBackups error"; // const-string v7, "cancelBackups error"
android.util.Slog .e ( v0,v7,v5 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 560 */
} // .end local v3 # "operationsToCancel":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v4 # "operationStorage":Lcom/android/server/backup/OperationStorage;
} // .end local v5 # "e":Ljava/lang/Exception;
} // .end local v6 # "backupWaitOperations":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
} // .end local v8 # "restoreWaitOperations":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
} // :goto_2
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 561 */
/* nop */
/* .line 562 */
return;
/* .line 560 */
/* :catchall_0 */
/* move-exception v0 */
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 561 */
/* throw v0 */
} // .end method
public void doFullBackup ( android.app.IBackupAgent p0, android.os.ParcelFileDescriptor p1, Long p2, Integer p3, android.app.backup.IBackupManager p4, Integer p5, Integer p6 ) {
/* .locals 5 */
/* .param p1, "agent" # Landroid/app/IBackupAgent; */
/* .param p2, "pipe" # Landroid/os/ParcelFileDescriptor; */
/* .param p3, "quotaBytes" # J */
/* .param p5, "token" # I */
/* .param p6, "backupManagerBinder" # Landroid/app/backup/IBackupManager; */
/* .param p7, "transportFlags" # I */
/* .param p8, "fd" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 469 */
final String v0 = "MiuiBackup"; // const-string v0, "MiuiBackup"
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lmiui/app/backup/IBackupManager; */
/* .line 470 */
v1 = /* .local v0, "bm":Lmiui/app/backup/IBackupManager; */
v1 = if ( v1 != null) { // if-eqz v1, :cond_3
/* if-nez v1, :cond_0 */
/* .line 473 */
} // :cond_0
/* const-string/jumbo v1, "skip app data" */
final String v2 = "Backup:BackupManagerServiceInjector"; // const-string v2, "Backup:BackupManagerServiceInjector"
android.util.Slog .i ( v2,v1 );
/* .line 477 */
int v1 = 0; // const/4 v1, 0x0
/* .line 479 */
/* .local v1, "out":Ljava/io/FileOutputStream; */
try { // :try_start_0
/* new-instance v3, Ljava/io/FileOutputStream; */
(( android.os.ParcelFileDescriptor ) p2 ).getFileDescriptor ( ); // invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;
/* invoke-direct {v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V */
/* move-object v1, v3 */
/* .line 480 */
int v3 = 4; // const/4 v3, 0x4
/* new-array v3, v3, [B */
/* .line 481 */
/* .local v3, "buf":[B */
(( java.io.FileOutputStream ) v1 ).write ( v3 ); // invoke-virtual {v1, v3}, Ljava/io/FileOutputStream;->write([B)V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 485 */
} // .end local v3 # "buf":[B
/* nop */
/* .line 487 */
try { // :try_start_1
(( java.io.FileOutputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 490 */
} // :goto_0
/* .line 488 */
/* :catch_0 */
/* move-exception v2 */
/* .line 489 */
/* .local v2, "e":Ljava/io/IOException; */
(( java.io.IOException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
} // .end local v2 # "e":Ljava/io/IOException;
/* .line 485 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 482 */
/* :catch_1 */
/* move-exception v3 */
/* .line 483 */
/* .local v3, "e":Ljava/io/IOException; */
try { // :try_start_2
final String v4 = "Unable to finalize backup stream!"; // const-string v4, "Unable to finalize backup stream!"
android.util.Slog .e ( v2,v4 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 485 */
/* nop */
} // .end local v3 # "e":Ljava/io/IOException;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 487 */
try { // :try_start_3
(( java.io.FileOutputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 493 */
} // :cond_1
} // :goto_1
/* const-wide/16 v2, 0x0 */
/* .line 485 */
} // :goto_2
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 487 */
try { // :try_start_4
(( java.io.FileOutputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_2 */
/* .line 490 */
/* .line 488 */
/* :catch_2 */
/* move-exception v3 */
/* .line 489 */
/* .restart local v3 # "e":Ljava/io/IOException; */
(( java.io.IOException ) v3 ).printStackTrace ( ); // invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
/* .line 492 */
} // .end local v3 # "e":Ljava/io/IOException;
} // :cond_2
} // :goto_3
/* throw v2 */
/* .line 471 */
} // .end local v1 # "out":Ljava/io/FileOutputStream;
} // :cond_3
} // :goto_4
/* invoke-interface/range {p1 ..p7}, Landroid/app/IBackupAgent;->doFullBackup(Landroid/os/ParcelFileDescriptor;JILandroid/app/backup/IBackupManager;I)V */
/* .line 495 */
} // :goto_5
return;
} // .end method
public void errorOccur ( Integer p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "errCode" # I */
/* .param p2, "fd" # I */
/* .line 77 */
final String v0 = "MiuiBackup"; // const-string v0, "MiuiBackup"
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lmiui/app/backup/IBackupManager; */
/* .line 79 */
/* .local v0, "bm":Lmiui/app/backup/IBackupManager; */
v1 = try { // :try_start_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 80 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 84 */
} // :cond_0
/* .line 82 */
/* :catch_0 */
/* move-exception v1 */
/* .line 83 */
/* .local v1, "e":Landroid/os/RemoteException; */
final String v2 = "Backup:BackupManagerServiceInjector"; // const-string v2, "Backup:BackupManagerServiceInjector"
final String v3 = "errorOccur failed"; // const-string v3, "errorOccur failed"
android.util.Slog .e ( v2,v3,v1 );
/* .line 85 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
public void errorOccur ( Integer p0, java.io.InputStream p1 ) {
/* .locals 2 */
/* .param p1, "errCode" # I */
/* .param p2, "inputStream" # Ljava/io/InputStream; */
/* .line 89 */
/* instance-of v0, p2, Ljava/io/FileInputStream; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 90 */
/* move-object v0, p2 */
/* check-cast v0, Ljava/io/FileInputStream; */
/* .line 92 */
/* .local v0, "fileInputStream":Ljava/io/FileInputStream; */
try { // :try_start_0
(( java.io.FileInputStream ) v0 ).getFD ( ); // invoke-virtual {v0}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;
v1 = (( java.io.FileDescriptor ) v1 ).getInt$ ( ); // invoke-virtual {v1}, Ljava/io/FileDescriptor;->getInt$()I
/* .line 93 */
/* .local v1, "fd":I */
(( com.android.server.backup.BackupManagerServiceInjector ) p0 ).errorOccur ( p1, v1 ); // invoke-virtual {p0, p1, v1}, Lcom/android/server/backup/BackupManagerServiceInjector;->errorOccur(II)V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 96 */
} // .end local v1 # "fd":I
/* .line 94 */
/* :catch_0 */
/* move-exception v1 */
/* .line 95 */
/* .local v1, "e":Ljava/io/IOException; */
(( java.io.IOException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
/* .line 98 */
} // .end local v0 # "fileInputStream":Ljava/io/FileInputStream;
} // .end local v1 # "e":Ljava/io/IOException;
} // :cond_0
} // :goto_0
return;
} // .end method
public Integer getAppUserId ( Integer p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "fd" # I */
/* .param p2, "def" # I */
/* .line 275 */
final String v0 = "MiuiBackup"; // const-string v0, "MiuiBackup"
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lmiui/app/backup/IBackupManager; */
/* .line 277 */
/* .local v0, "bm":Lmiui/app/backup/IBackupManager; */
v1 = try { // :try_start_0
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = /* .line 278 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 282 */
} // :cond_0
/* .line 280 */
/* :catch_0 */
/* move-exception v1 */
/* .line 281 */
/* .local v1, "e":Landroid/os/RemoteException; */
final String v2 = "Backup:BackupManagerServiceInjector"; // const-string v2, "Backup:BackupManagerServiceInjector"
final String v3 = "getAppUserId failed"; // const-string v3, "getAppUserId failed"
android.util.Slog .e ( v2,v3,v1 );
/* .line 283 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_0
} // .end method
public android.content.pm.ApplicationInfo getApplicationInfo ( android.content.Context p0, java.lang.String p1, Integer p2, Integer p3 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "pkgName" # Ljava/lang/String; */
/* .param p3, "fd" # I */
/* .param p4, "userIdDef" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/content/pm/PackageManager$NameNotFoundException; */
/* } */
} // .end annotation
/* .line 288 */
final String v0 = "MiuiBackup"; // const-string v0, "MiuiBackup"
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lmiui/app/backup/IBackupManager; */
/* .line 289 */
/* .local v0, "bm":Lmiui/app/backup/IBackupManager; */
(( android.content.Context ) p1 ).getPackageManager ( ); // invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 291 */
/* .local v1, "pm":Landroid/content/pm/PackageManager; */
int v2 = 0; // const/4 v2, 0x0
v3 = try { // :try_start_0
if ( v3 != null) { // if-eqz v3, :cond_1
v3 = /* .line 292 */
/* .line 293 */
/* .local v3, "userId":I */
v4 = miui.app.backup.BackupManager .isSysAppForBackup ( p1,p2 );
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 294 */
/* const/16 v4, 0x400 */
(( android.content.pm.PackageManager ) v1 ).getApplicationInfoAsUser ( p2, v4, v3 ); // invoke-virtual {v1, p2, v4, v3}, Landroid/content/pm/PackageManager;->getApplicationInfoAsUser(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;
/* .line 296 */
} // :cond_0
(( android.content.pm.PackageManager ) v1 ).getApplicationInfoAsUser ( p2, v2, v3 ); // invoke-virtual {v1, p2, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfoAsUser(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 301 */
} // .end local v3 # "userId":I
} // :cond_1
/* .line 299 */
/* :catch_0 */
/* move-exception v3 */
/* .line 300 */
/* .local v3, "e":Landroid/os/RemoteException; */
final String v4 = "Backup:BackupManagerServiceInjector"; // const-string v4, "Backup:BackupManagerServiceInjector"
final String v5 = "getApplicationInfo failed"; // const-string v5, "getApplicationInfo failed"
android.util.Slog .e ( v4,v5,v3 );
/* .line 302 */
} // .end local v3 # "e":Landroid/os/RemoteException;
} // :goto_0
(( android.content.pm.PackageManager ) v1 ).getApplicationInfoAsUser ( p2, v2, p4 ); // invoke-virtual {v1, p2, v2, p4}, Landroid/content/pm/PackageManager;->getApplicationInfoAsUser(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;
} // .end method
public android.content.pm.PackageInfo getPackageInfo ( android.content.Context p0, java.lang.String p1, Integer p2 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "pkgName" # Ljava/lang/String; */
/* .param p3, "fd" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/content/pm/PackageManager$NameNotFoundException; */
/* } */
} // .end annotation
/* .line 307 */
final String v0 = "MiuiBackup"; // const-string v0, "MiuiBackup"
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lmiui/app/backup/IBackupManager; */
/* .line 308 */
/* .local v0, "bm":Lmiui/app/backup/IBackupManager; */
(( android.content.Context ) p1 ).getPackageManager ( ); // invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 310 */
/* .local v1, "pm":Landroid/content/pm/PackageManager; */
/* const/high16 v2, 0x8000000 */
v3 = try { // :try_start_0
if ( v3 != null) { // if-eqz v3, :cond_1
v3 = /* .line 311 */
/* .line 312 */
/* .local v3, "userId":I */
v4 = miui.app.backup.BackupManager .isSysAppForBackup ( p1,p2 );
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 313 */
/* const v4, 0x8000400 */
(( android.content.pm.PackageManager ) v1 ).getPackageInfoAsUser ( p2, v4, v3 ); // invoke-virtual {v1, p2, v4, v3}, Landroid/content/pm/PackageManager;->getPackageInfoAsUser(Ljava/lang/String;II)Landroid/content/pm/PackageInfo;
/* .line 315 */
} // :cond_0
(( android.content.pm.PackageManager ) v1 ).getPackageInfoAsUser ( p2, v2, v3 ); // invoke-virtual {v1, p2, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfoAsUser(Ljava/lang/String;II)Landroid/content/pm/PackageInfo;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 320 */
} // .end local v3 # "userId":I
} // :cond_1
/* .line 318 */
/* :catch_0 */
/* move-exception v3 */
/* .line 319 */
/* .local v3, "e":Landroid/os/RemoteException; */
final String v4 = "Backup:BackupManagerServiceInjector"; // const-string v4, "Backup:BackupManagerServiceInjector"
final String v5 = "getPackageInfo failed"; // const-string v5, "getPackageInfo failed"
android.util.Slog .e ( v4,v5,v3 );
/* .line 321 */
} // .end local v3 # "e":Landroid/os/RemoteException;
} // :goto_0
(( android.content.pm.PackageManager ) v1 ).getPackageInfo ( p2, v2 ); // invoke-virtual {v1, p2, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
} // .end method
public Integer hookUserIdForXSpace ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "userID" # I */
/* .line 613 */
v0 = (( com.android.server.backup.BackupManagerServiceInjector ) p0 ).isXSpaceUser ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/backup/BackupManagerServiceInjector;->isXSpaceUser(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_0
/* move v0, p1 */
} // :goto_0
} // .end method
public Boolean installExistingPackageAsUser ( java.lang.String p0, Integer p1 ) {
/* .locals 10 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 588 */
final String v0 = "Backup:BackupManagerServiceInjector"; // const-string v0, "Backup:BackupManagerServiceInjector"
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
android.app.AppGlobals .getPackageManager ( );
/* .line 589 */
/* .local v2, "packageManager":Landroid/content/pm/IPackageManager; */
v3 = com.miui.server.BackupProxyHelper .isAppInXSpace ( v2,p1 );
int v9 = 1; // const/4 v9, 0x1
/* if-nez v3, :cond_2 */
/* .line 591 */
v3 = com.miui.server.BackupProxyHelper .isMiPushRequired ( v2,p1 );
if ( v3 != null) { // if-eqz v3, :cond_0
final String v3 = "com.xiaomi.xmsf"; // const-string v3, "com.xiaomi.xmsf"
/* .line 592 */
v3 = com.miui.server.BackupProxyHelper .isAppInXSpace ( v2,v3 );
/* if-nez v3, :cond_0 */
/* .line 593 */
final String v4 = "com.xiaomi.xmsf"; // const-string v4, "com.xiaomi.xmsf"
/* const v6, 0x404000 */
int v7 = 4; // const/4 v7, 0x4
int v8 = 0; // const/4 v8, 0x0
/* move-object v3, v2 */
/* move v5, p2 */
/* invoke-interface/range {v3 ..v8}, Landroid/content/pm/IPackageManager;->installExistingPackageAsUser(Ljava/lang/String;IIILjava/util/List;)I */
/* .line 595 */
final String v3 = "Require XMSF, auto installed! "; // const-string v3, "Require XMSF, auto installed! "
android.util.Slog .d ( v0,v3 );
/* .line 597 */
} // :cond_0
/* const v6, 0x404000 */
int v7 = 4; // const/4 v7, 0x4
int v8 = 0; // const/4 v8, 0x0
/* move-object v3, v2 */
/* move-object v4, p1 */
/* move v5, p2 */
v0 = /* invoke-interface/range {v3 ..v8}, Landroid/content/pm/IPackageManager;->installExistingPackageAsUser(Ljava/lang/String;IIILjava/util/List;)I */
/* .line 599 */
/* .local v0, "result":I */
/* if-ne v0, v9, :cond_1 */
/* move v1, v9 */
} // :cond_1
/* .line 601 */
} // .end local v0 # "result":I
} // :cond_2
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "has been installed, pkgName:"; // const-string v4, "has been installed, pkgName:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", userId:"; // const-string v4, ", userId:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v3 );
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 602 */
/* .line 603 */
} // .end local v2 # "packageManager":Landroid/content/pm/IPackageManager;
/* :catch_0 */
/* move-exception v2 */
/* .line 604 */
/* .local v2, "e":Landroid/os/RemoteException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Unable to install package: "; // const-string v4, "Unable to install package: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v3,v2 );
/* .line 606 */
} // .end local v2 # "e":Landroid/os/RemoteException;
} // .end method
public Boolean isCanceling ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "fd" # I */
/* .line 512 */
final String v0 = "MiuiBackup"; // const-string v0, "MiuiBackup"
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lmiui/app/backup/IBackupManager; */
/* .line 514 */
/* .local v0, "bm":Lmiui/app/backup/IBackupManager; */
v1 = try { // :try_start_0
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = /* .line 515 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 519 */
} // :cond_0
/* .line 517 */
/* :catch_0 */
/* move-exception v1 */
/* .line 518 */
/* .local v1, "e":Landroid/os/RemoteException; */
final String v2 = "Backup:BackupManagerServiceInjector"; // const-string v2, "Backup:BackupManagerServiceInjector"
final String v3 = "isCanceling error"; // const-string v3, "isCanceling error"
android.util.Slog .e ( v2,v3,v1 );
/* .line 520 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean isForceAllowBackup ( android.content.pm.PackageInfo p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "info" # Landroid/content/pm/PackageInfo; */
/* .param p2, "fd" # I */
/* .line 267 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = miui.app.backup.BackupManager .isSysAppForBackup ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 270 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 268 */
} // :cond_1
} // :goto_0
v0 = (( com.android.server.backup.BackupManagerServiceInjector ) p0 ).isRunningFromMiui ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/backup/BackupManagerServiceInjector;->isRunningFromMiui(I)Z
} // .end method
public Boolean isNeedBeKilled ( java.lang.String p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "fd" # I */
/* .line 326 */
int v0 = 1; // const/4 v0, 0x1
/* .line 327 */
/* .local v0, "is":Z */
final String v1 = "MiuiBackup"; // const-string v1, "MiuiBackup"
android.os.ServiceManager .getService ( v1 );
/* check-cast v1, Lmiui/app/backup/IBackupManager; */
/* .line 329 */
/* .local v1, "bm":Lmiui/app/backup/IBackupManager; */
v2 = try { // :try_start_0
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = /* .line 330 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v0, v2 */
/* .line 334 */
} // :cond_0
/* .line 332 */
/* :catch_0 */
/* move-exception v2 */
/* .line 333 */
/* .local v2, "e":Landroid/os/RemoteException; */
final String v3 = "Backup:BackupManagerServiceInjector"; // const-string v3, "Backup:BackupManagerServiceInjector"
final String v4 = "isNeedBeKilled failed"; // const-string v4, "isNeedBeKilled failed"
android.util.Slog .e ( v3,v4,v2 );
/* .line 335 */
} // .end local v2 # "e":Landroid/os/RemoteException;
} // :goto_0
} // .end method
public Boolean isRunningFromMiui ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "fd" # I */
/* .line 235 */
final String v0 = "MiuiBackup"; // const-string v0, "MiuiBackup"
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lmiui/app/backup/IBackupManager; */
/* .line 237 */
/* .local v0, "bm":Lmiui/app/backup/IBackupManager; */
v1 = try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 238 */
int v1 = 1; // const/4 v1, 0x1
/* .line 242 */
} // :cond_0
/* .line 240 */
/* :catch_0 */
/* move-exception v1 */
/* .line 241 */
/* .local v1, "e":Landroid/os/RemoteException; */
final String v2 = "Backup:BackupManagerServiceInjector"; // const-string v2, "Backup:BackupManagerServiceInjector"
final String v3 = "isRunningFromMiui error"; // const-string v3, "isRunningFromMiui error"
android.util.Slog .e ( v2,v3,v1 );
/* .line 243 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean isRunningFromMiui ( java.io.InputStream p0 ) {
/* .locals 4 */
/* .param p1, "inputStream" # Ljava/io/InputStream; */
/* .line 248 */
/* instance-of v0, p1, Ljava/io/FileInputStream; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 249 */
/* move-object v0, p1 */
/* check-cast v0, Ljava/io/FileInputStream; */
/* .line 251 */
/* .local v0, "fileInputStream":Ljava/io/FileInputStream; */
try { // :try_start_0
(( java.io.FileInputStream ) v0 ).getFD ( ); // invoke-virtual {v0}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;
v1 = (( java.io.FileDescriptor ) v1 ).getInt$ ( ); // invoke-virtual {v1}, Ljava/io/FileDescriptor;->getInt$()I
/* .line 252 */
/* .local v1, "fd":I */
v2 = (( com.android.server.backup.BackupManagerServiceInjector ) p0 ).isRunningFromMiui ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/backup/BackupManagerServiceInjector;->isRunningFromMiui(I)Z
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 253 */
} // .end local v1 # "fd":I
/* :catch_0 */
/* move-exception v1 */
/* .line 254 */
/* .local v1, "e":Ljava/io/IOException; */
final String v2 = "Backup:BackupManagerServiceInjector"; // const-string v2, "Backup:BackupManagerServiceInjector"
final String v3 = "isRunningFromMiui error"; // const-string v3, "isRunningFromMiui error"
android.util.Slog .e ( v2,v3,v1 );
/* .line 257 */
} // .end local v0 # "fileInputStream":Ljava/io/FileInputStream;
} // .end local v1 # "e":Ljava/io/IOException;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isSysAppRunningFromMiui ( android.content.pm.PackageInfo p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "info" # Landroid/content/pm/PackageInfo; */
/* .param p2, "fd" # I */
/* .line 262 */
v0 = miui.app.backup.BackupManager .isSysAppForBackup ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( com.android.server.backup.BackupManagerServiceInjector ) p0 ).isRunningFromMiui ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/backup/BackupManagerServiceInjector;->isRunningFromMiui(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isXSpaceUser ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "userId" # I */
/* .line 566 */
/* const/16 v0, 0x3e7 */
/* if-ne p1, v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isXSpaceUserRunning ( ) {
/* .locals 3 */
/* .line 572 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
android.app.ActivityManager .getService ( );
/* .line 573 */
/* .local v1, "activityManager":Landroid/app/IActivityManager; */
v0 = /* const/16 v2, 0x3e7 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 574 */
} // .end local v1 # "activityManager":Landroid/app/IActivityManager;
/* :catch_0 */
/* move-exception v1 */
/* .line 577 */
} // .end method
public void linkToDeath ( android.app.IBackupAgent p0, Integer p1, android.os.ParcelFileDescriptor p2 ) {
/* .locals 5 */
/* .param p1, "backupAgent" # Landroid/app/IBackupAgent; */
/* .param p2, "fd" # I */
/* .param p3, "outPipe" # Landroid/os/ParcelFileDescriptor; */
/* .line 212 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 213 */
/* .line 214 */
/* .local v0, "agentBinder":Landroid/os/IBinder; */
/* new-instance v1, Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker; */
/* invoke-direct {v1, p0, v0, p2, p3}, Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;-><init>(Lcom/android/server/backup/BackupManagerServiceInjector;Landroid/os/IBinder;ILandroid/os/ParcelFileDescriptor;)V */
/* .line 215 */
/* .local v1, "deathLinker":Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker; */
v2 = com.android.server.backup.BackupManagerServiceInjector.sBinderDeathLinker;
(( java.util.HashMap ) v2 ).put ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 217 */
int v2 = 0; // const/4 v2, 0x0
try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 220 */
/* .line 218 */
/* :catch_0 */
/* move-exception v2 */
/* .line 219 */
/* .local v2, "e":Landroid/os/RemoteException; */
final String v3 = "Backup:BackupManagerServiceInjector"; // const-string v3, "Backup:BackupManagerServiceInjector"
final String v4 = "linkToDeath failed"; // const-string v4, "linkToDeath failed"
android.util.Slog .e ( v3,v4,v2 );
/* .line 222 */
} // .end local v0 # "agentBinder":Landroid/os/IBinder;
} // .end local v1 # "deathLinker":Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;
} // .end local v2 # "e":Landroid/os/RemoteException;
} // :cond_0
} // :goto_0
return;
} // .end method
public Boolean needUpdateToken ( android.app.IBackupAgent p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "backupAgent" # Landroid/app/IBackupAgent; */
/* .param p2, "token" # I */
/* .line 199 */
int v0 = 0; // const/4 v0, 0x0
/* .line 200 */
/* .local v0, "needUpdateToken":Z */
if ( p1 != null) { // if-eqz p1, :cond_0
v0 = /* .line 201 */
/* .line 202 */
v1 = com.android.server.backup.BackupManagerServiceInjector.sBinderDeathLinker;
(( java.util.HashMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker; */
/* .line 203 */
/* .local v1, "deathLinker":Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 204 */
(( com.android.server.backup.BackupManagerServiceInjector$DeathLinker ) v1 ).setToken ( p2 ); // invoke-virtual {v1, p2}, Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;->setToken(I)V
/* .line 207 */
} // .end local v1 # "deathLinker":Lcom/android/server/backup/BackupManagerServiceInjector$DeathLinker;
} // :cond_0
} // .end method
public void onApkInstalled ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "fd" # I */
/* .line 138 */
final String v0 = "MiuiBackup"; // const-string v0, "MiuiBackup"
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lmiui/app/backup/IBackupManager; */
/* .line 140 */
/* .local v0, "bm":Lmiui/app/backup/IBackupManager; */
v1 = try { // :try_start_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 141 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 145 */
} // :cond_0
/* .line 143 */
/* :catch_0 */
/* move-exception v1 */
/* .line 144 */
/* .local v1, "e":Landroid/os/RemoteException; */
final String v2 = "Backup:BackupManagerServiceInjector"; // const-string v2, "Backup:BackupManagerServiceInjector"
final String v3 = "onApkInstalled failed"; // const-string v3, "onApkInstalled failed"
android.util.Slog .e ( v2,v3,v1 );
/* .line 146 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
public void prepareOperationTimeout ( com.android.server.backup.UserBackupManagerService p0, Integer p1, Long p2, com.android.server.backup.BackupRestoreTask p3, Integer p4, Integer p5 ) {
/* .locals 11 */
/* .param p1, "thiz" # Lcom/android/server/backup/UserBackupManagerService; */
/* .param p2, "token" # I */
/* .param p3, "interval" # J */
/* .param p5, "callback" # Lcom/android/server/backup/BackupRestoreTask; */
/* .param p6, "operationType" # I */
/* .param p7, "fd" # I */
/* .line 454 */
final String v1 = "Backup:BackupManagerServiceInjector"; // const-string v1, "Backup:BackupManagerServiceInjector"
final String v0 = "MiuiBackup"; // const-string v0, "MiuiBackup"
android.os.ServiceManager .getService ( v0 );
/* move-object v2, v0 */
/* check-cast v2, Lmiui/app/backup/IBackupManager; */
/* .line 455 */
/* .local v2, "bm":Lmiui/app/backup/IBackupManager; */
int v3 = 1; // const/4 v3, 0x1
/* .line 457 */
/* .local v3, "backupTimeoutScale":I */
/* move/from16 v4, p7 */
v0 = try { // :try_start_0
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = /* .line 458 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v3, v0 */
/* .line 462 */
} // :cond_0
/* .line 460 */
/* :catch_0 */
/* move-exception v0 */
/* .line 461 */
/* .local v0, "e":Landroid/os/RemoteException; */
final String v5 = "prepareOperationTimeout failed"; // const-string v5, "prepareOperationTimeout failed"
android.util.Slog .e ( v1,v5,v0 );
/* .line 463 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "prepareOperationTimeout backupTimeoutScale = "; // const-string v5, "prepareOperationTimeout backupTimeoutScale = "
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 464 */
/* int-to-long v0, v3 */
/* mul-long v7, v0, p3 */
/* move-object v5, p1 */
/* move v6, p2 */
/* move-object/from16 v9, p5 */
/* move/from16 v10, p6 */
/* invoke-virtual/range {v5 ..v10}, Lcom/android/server/backup/UserBackupManagerService;->prepareOperationTimeout(IJLcom/android/server/backup/BackupRestoreTask;I)V */
/* .line 465 */
return;
} // .end method
public void readMiuiBackupHeader ( android.os.ParcelFileDescriptor p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "in" # Landroid/os/ParcelFileDescriptor; */
/* .param p2, "fd" # I */
/* .line 114 */
final String v0 = "MiuiBackup"; // const-string v0, "MiuiBackup"
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lmiui/app/backup/IBackupManager; */
/* .line 116 */
/* .local v0, "bm":Lmiui/app/backup/IBackupManager; */
v1 = try { // :try_start_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 117 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 121 */
} // :cond_0
/* .line 119 */
/* :catch_0 */
/* move-exception v1 */
/* .line 120 */
/* .local v1, "e":Landroid/os/RemoteException; */
final String v2 = "Backup:BackupManagerServiceInjector"; // const-string v2, "Backup:BackupManagerServiceInjector"
final String v3 = "readMiuiBackupHeader failed"; // const-string v3, "readMiuiBackupHeader failed"
android.util.Slog .e ( v2,v3,v1 );
/* .line 122 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
public void registerHuanjiReceiver ( com.android.server.backup.UserBackupManagerService p0, android.content.Context p1 ) {
/* .locals 2 */
/* .param p1, "service" # Lcom/android/server/backup/UserBackupManagerService; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 618 */
final String v0 = "Backup:BackupManagerServiceInjector"; // const-string v0, "Backup:BackupManagerServiceInjector"
final String v1 = "registerHuanjiReceiver."; // const-string v1, "registerHuanjiReceiver."
android.util.Log .i ( v0,v1 );
/* .line 619 */
v0 = this.mHuanjiReceiver;
/* if-nez v0, :cond_0 */
/* .line 620 */
/* new-instance v0, Lcom/android/server/backup/BackupManagerServiceInjector$HuanjiReceiver; */
/* invoke-direct {v0, p1}, Lcom/android/server/backup/BackupManagerServiceInjector$HuanjiReceiver;-><init>(Lcom/android/server/backup/UserBackupManagerService;)V */
this.mHuanjiReceiver = v0;
/* .line 621 */
(( com.android.server.backup.BackupManagerServiceInjector$HuanjiReceiver ) v0 ).register ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/backup/BackupManagerServiceInjector$HuanjiReceiver;->register(Landroid/content/Context;)V
/* .line 623 */
} // :cond_0
return;
} // .end method
public void restoreFileEnd ( com.android.server.backup.UserBackupManagerService p0, android.app.IBackupAgent p1, android.app.backup.IBackupManager p2, Integer p3, android.os.Handler p4, Integer p5 ) {
/* .locals 16 */
/* .param p1, "thiz" # Lcom/android/server/backup/UserBackupManagerService; */
/* .param p2, "agent" # Landroid/app/IBackupAgent; */
/* .param p3, "backupManagerBinder" # Landroid/app/backup/IBackupManager; */
/* .param p4, "fd" # I */
/* .param p5, "backupHandler" # Landroid/os/Handler; */
/* .param p6, "restoreTimeoutMsg" # I */
/* .line 341 */
/* move-object/from16 v14, p2 */
if ( v14 != null) { // if-eqz v14, :cond_2
/* .line 342 */
final String v0 = "MiuiBackup"; // const-string v0, "MiuiBackup"
android.os.ServiceManager .getService ( v0 );
/* move-object v15, v0 */
/* check-cast v15, Lmiui/app/backup/IBackupManager; */
/* .line 344 */
/* .local v15, "bm":Lmiui/app/backup/IBackupManager; */
/* move/from16 v13, p4 */
v0 = try { // :try_start_0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 345 */
v0 = /* invoke-virtual/range {p1 ..p1}, Lcom/android/server/backup/UserBackupManagerService;->generateRandomIntegerToken()I */
/* move v12, v0 */
/* .line 346 */
/* .local v12, "token":I */
/* invoke-virtual/range {p5 ..p6}, Landroid/os/Handler;->removeMessages(I)V */
/* .line 347 */
/* const-wide/32 v3, 0x493e0 */
int v5 = 0; // const/4 v5, 0x0
int v6 = 1; // const/4 v6, 0x1
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
/* move v2, v12 */
/* move/from16 v7, p4 */
/* invoke-virtual/range {v0 ..v7}, Lcom/android/server/backup/BackupManagerServiceInjector;->prepareOperationTimeout(Lcom/android/server/backup/UserBackupManagerService;IJLcom/android/server/backup/BackupRestoreTask;II)V */
/* .line 348 */
int v2 = 0; // const/4 v2, 0x0
/* const-wide/16 v3, 0x0 */
int v5 = 0; // const/4 v5, 0x0
v6 = miui.app.backup.BackupManager.DOMAIN_END;
int v7 = 0; // const/4 v7, 0x0
/* const-wide/16 v8, 0x0 */
/* const-wide/16 v10, 0x0 */
/* move-object/from16 v1, p2 */
/* move v0, v12 */
} // .end local v12 # "token":I
/* .local v0, "token":I */
/* move-object/from16 v13, p3 */
/* invoke-interface/range {v1 ..v13}, Landroid/app/IBackupAgent;->doRestoreFile(Landroid/os/ParcelFileDescriptor;JILjava/lang/String;Ljava/lang/String;JJILandroid/app/backup/IBackupManager;)V */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .line 349 */
/* move-object/from16 v1, p0 */
try { // :try_start_1
v2 = (( com.android.server.backup.BackupManagerServiceInjector ) v1 ).needUpdateToken ( v14, v0 ); // invoke-virtual {v1, v14, v0}, Lcom/android/server/backup/BackupManagerServiceInjector;->needUpdateToken(Landroid/app/IBackupAgent;I)Z
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_1 */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 350 */
/* move-object/from16 v2, p1 */
try { // :try_start_2
(( com.android.server.backup.UserBackupManagerService ) v2 ).waitUntilOperationComplete ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/backup/UserBackupManagerService;->waitUntilOperationComplete(I)Z
/* :try_end_2 */
/* .catch Landroid/os/RemoteException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 353 */
} // .end local v0 # "token":I
/* :catch_0 */
/* move-exception v0 */
/* .line 349 */
/* .restart local v0 # "token":I */
} // :cond_0
/* move-object/from16 v2, p1 */
/* .line 353 */
} // .end local v0 # "token":I
/* :catch_1 */
/* move-exception v0 */
/* .line 344 */
} // :cond_1
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
/* .line 355 */
} // :goto_0
/* .line 353 */
/* :catch_2 */
/* move-exception v0 */
/* move-object/from16 v1, p0 */
} // :goto_1
/* move-object/from16 v2, p1 */
/* .line 354 */
/* .local v0, "e":Landroid/os/RemoteException; */
} // :goto_2
final String v3 = "Backup:BackupManagerServiceInjector"; // const-string v3, "Backup:BackupManagerServiceInjector"
final String v4 = "restoreFileEnd failed"; // const-string v4, "restoreFileEnd failed"
android.util.Slog .e ( v3,v4,v0 );
/* .line 341 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // .end local v15 # "bm":Lmiui/app/backup/IBackupManager;
} // :cond_2
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
/* .line 357 */
} // :goto_3
return;
} // .end method
public void routeSocketDataToOutput ( android.os.ParcelFileDescriptor p0, java.io.OutputStream p1, Integer p2 ) {
/* .locals 12 */
/* .param p1, "inPipe" # Landroid/os/ParcelFileDescriptor; */
/* .param p2, "out" # Ljava/io/OutputStream; */
/* .param p3, "outFd" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 362 */
final String v0 = "routeSocketDataToOutput failed"; // const-string v0, "routeSocketDataToOutput failed"
final String v1 = "Backup:BackupManagerServiceInjector"; // const-string v1, "Backup:BackupManagerServiceInjector"
final String v2 = "MiuiBackup"; // const-string v2, "MiuiBackup"
android.os.ServiceManager .getService ( v2 );
/* check-cast v2, Lmiui/app/backup/IBackupManager; */
/* .line 363 */
/* .local v2, "bm":Lmiui/app/backup/IBackupManager; */
int v3 = 0; // const/4 v3, 0x0
/* .line 364 */
/* .local v3, "raw":Ljava/io/FileInputStream; */
int v4 = 0; // const/4 v4, 0x0
/* .line 366 */
/* .local v4, "in":Ljava/io/DataInputStream; */
v5 = try { // :try_start_0
if ( v5 != null) { // if-eqz v5, :cond_7
v5 = /* .line 367 */
/* :try_end_0 */
/* .catch Ljava/io/InterruptedIOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 397 */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 398 */
(( java.io.FileInputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
/* .line 400 */
} // :cond_0
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 401 */
(( java.io.DataInputStream ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/DataInputStream;->close()V
/* .line 368 */
} // :cond_1
return;
/* .line 370 */
} // :cond_2
try { // :try_start_1
/* new-instance v5, Ljava/io/FileInputStream; */
(( android.os.ParcelFileDescriptor ) p1 ).getFileDescriptor ( ); // invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;
/* invoke-direct {v5, v6}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V */
/* move-object v3, v5 */
/* .line 371 */
/* new-instance v5, Ljava/io/DataInputStream; */
/* invoke-direct {v5, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V */
/* move-object v4, v5 */
/* .line 373 */
/* const v5, 0x8000 */
/* new-array v5, v5, [B */
/* .line 375 */
/* .local v5, "buffer":[B */
} // :cond_3
v6 = (( java.io.DataInputStream ) v4 ).readInt ( ); // invoke-virtual {v4}, Ljava/io/DataInputStream;->readInt()I
/* move v7, v6 */
/* .local v7, "chunkTotal":I */
/* if-lez v6, :cond_6 */
/* .line 376 */
} // :goto_0
/* if-lez v7, :cond_3 */
/* .line 377 */
/* array-length v6, v5 */
/* if-le v7, v6, :cond_4 */
/* array-length v6, v5 */
} // :cond_4
/* move v6, v7 */
/* .line 378 */
/* .local v6, "toRead":I */
} // :goto_1
int v8 = 0; // const/4 v8, 0x0
v9 = (( java.io.DataInputStream ) v4 ).read ( v5, v8, v6 ); // invoke-virtual {v4, v5, v8, v6}, Ljava/io/DataInputStream;->read([BII)I
/* .line 379 */
/* .local v9, "nRead":I */
/* if-ltz v9, :cond_5 */
/* .line 383 */
(( java.io.OutputStream ) p2 ).write ( v5, v8, v9 ); // invoke-virtual {p2, v5, v8, v9}, Ljava/io/OutputStream;->write([BII)V
/* .line 384 */
/* int-to-long v10, v9 */
/* .line 385 */
/* sub-int/2addr v7, v9 */
/* .line 386 */
} // .end local v6 # "toRead":I
} // .end local v9 # "nRead":I
/* .line 380 */
/* .restart local v6 # "toRead":I */
/* .restart local v9 # "nRead":I */
} // :cond_5
final String v8 = "Unexpectedly reached end of file while reading data"; // const-string v8, "Unexpectedly reached end of file while reading data"
android.util.Slog .e ( v1,v8 );
/* .line 381 */
/* new-instance v8, Ljava/io/EOFException; */
/* invoke-direct {v8}, Ljava/io/EOFException;-><init>()V */
} // .end local v2 # "bm":Lmiui/app/backup/IBackupManager;
} // .end local v3 # "raw":Ljava/io/FileInputStream;
} // .end local v4 # "in":Ljava/io/DataInputStream;
} // .end local p0 # "this":Lcom/android/server/backup/BackupManagerServiceInjector;
} // .end local p1 # "inPipe":Landroid/os/ParcelFileDescriptor;
} // .end local p2 # "out":Ljava/io/OutputStream;
} // .end local p3 # "outFd":I
/* throw v8 */
/* .line 388 */
} // .end local v5 # "buffer":[B
} // .end local v6 # "toRead":I
} // .end local v7 # "chunkTotal":I
} // .end local v9 # "nRead":I
/* .restart local v2 # "bm":Lmiui/app/backup/IBackupManager; */
/* .restart local v3 # "raw":Ljava/io/FileInputStream; */
/* .restart local v4 # "in":Ljava/io/DataInputStream; */
/* .restart local p0 # "this":Lcom/android/server/backup/BackupManagerServiceInjector; */
/* .restart local p1 # "inPipe":Landroid/os/ParcelFileDescriptor; */
/* .restart local p2 # "out":Ljava/io/OutputStream; */
/* .restart local p3 # "outFd":I */
} // :cond_6
/* .line 389 */
} // :cond_7
com.android.server.backup.utils.FullBackupUtils .routeSocketDataToOutput ( p1,p2 );
/* :try_end_1 */
/* .catch Ljava/io/InterruptedIOException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 397 */
} // :goto_2
if ( v3 != null) { // if-eqz v3, :cond_8
/* .line 398 */
(( java.io.FileInputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
/* .line 400 */
} // :cond_8
if ( v4 != null) { // if-eqz v4, :cond_a
/* .line 401 */
} // :goto_3
(( java.io.DataInputStream ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/DataInputStream;->close()V
/* .line 397 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 393 */
/* :catch_0 */
/* move-exception v5 */
/* .line 394 */
/* .local v5, "e":Ljava/lang/ArrayIndexOutOfBoundsException; */
try { // :try_start_2
android.util.Slog .e ( v1,v0,v5 );
/* .line 395 */
/* new-instance v0, Ljava/io/IOException; */
/* invoke-direct {v0}, Ljava/io/IOException;-><init>()V */
} // .end local v2 # "bm":Lmiui/app/backup/IBackupManager;
} // .end local v3 # "raw":Ljava/io/FileInputStream;
} // .end local v4 # "in":Ljava/io/DataInputStream;
} // .end local p0 # "this":Lcom/android/server/backup/BackupManagerServiceInjector;
} // .end local p1 # "inPipe":Landroid/os/ParcelFileDescriptor;
} // .end local p2 # "out":Ljava/io/OutputStream;
} // .end local p3 # "outFd":I
/* throw v0 */
/* .line 391 */
} // .end local v5 # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
/* .restart local v2 # "bm":Lmiui/app/backup/IBackupManager; */
/* .restart local v3 # "raw":Ljava/io/FileInputStream; */
/* .restart local v4 # "in":Ljava/io/DataInputStream; */
/* .restart local p0 # "this":Lcom/android/server/backup/BackupManagerServiceInjector; */
/* .restart local p1 # "inPipe":Landroid/os/ParcelFileDescriptor; */
/* .restart local p2 # "out":Ljava/io/OutputStream; */
/* .restart local p3 # "outFd":I */
/* :catch_1 */
/* move-exception v5 */
/* .line 392 */
/* .local v5, "e":Ljava/lang/Exception; */
android.util.Slog .e ( v1,v0,v5 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 397 */
/* nop */
} // .end local v5 # "e":Ljava/lang/Exception;
if ( v3 != null) { // if-eqz v3, :cond_9
/* .line 398 */
(( java.io.FileInputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
/* .line 400 */
} // :cond_9
if ( v4 != null) { // if-eqz v4, :cond_a
/* .line 401 */
/* .line 404 */
} // :cond_a
} // :goto_4
return;
/* .line 397 */
} // :goto_5
if ( v3 != null) { // if-eqz v3, :cond_b
/* .line 398 */
(( java.io.FileInputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
/* .line 400 */
} // :cond_b
if ( v4 != null) { // if-eqz v4, :cond_c
/* .line 401 */
(( java.io.DataInputStream ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/DataInputStream;->close()V
/* .line 403 */
} // :cond_c
/* throw v0 */
} // .end method
public void setInputFileDescriptor ( com.android.server.backup.restore.FullRestoreEngine p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "engine" # Lcom/android/server/backup/restore/FullRestoreEngine; */
/* .param p2, "fd" # I */
/* .line 408 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 409 */
/* iput p2, p1, Lcom/android/server/backup/restore/FullRestoreEngine;->mInputFD:I */
/* .line 411 */
} // :cond_0
return;
} // .end method
public void setOutputFileDescriptor ( com.android.server.backup.fullbackup.FullBackupEngine p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "engine" # Lcom/android/server/backup/fullbackup/FullBackupEngine; */
/* .param p2, "fd" # I */
/* .line 415 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 416 */
/* iput p2, p1, Lcom/android/server/backup/fullbackup/FullBackupEngine;->mOutputFD:I */
/* .line 418 */
} // :cond_0
return;
} // .end method
public void setOutputFileDescriptor ( com.android.server.backup.fullbackup.PerformAdbBackupTask p0, android.os.ParcelFileDescriptor p1 ) {
/* .locals 4 */
/* .param p1, "task" # Lcom/android/server/backup/fullbackup/PerformAdbBackupTask; */
/* .param p2, "fileDescriptor" # Landroid/os/ParcelFileDescriptor; */
/* .line 422 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 425 */
try { // :try_start_0
v0 = (( android.os.ParcelFileDescriptor ) p2 ).getFd ( ); // invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->getFd()I
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 429 */
/* .local v0, "fd":I */
/* .line 426 */
} // .end local v0 # "fd":I
/* :catch_0 */
/* move-exception v0 */
/* .line 427 */
/* .local v0, "e":Ljava/lang/Exception; */
int v1 = -2; // const/4 v1, -0x2
/* .line 428 */
/* .local v1, "fd":I */
final String v2 = "Backup:BackupManagerServiceInjector"; // const-string v2, "Backup:BackupManagerServiceInjector"
/* const-string/jumbo v3, "setOutputFileDescriptor failed" */
android.util.Slog .e ( v2,v3,v0 );
/* move v0, v1 */
/* .line 430 */
} // .end local v1 # "fd":I
/* .local v0, "fd":I */
} // :goto_0
/* iput v0, p1, Lcom/android/server/backup/fullbackup/PerformAdbBackupTask;->mOutputFD:I */
/* .line 432 */
} // .end local v0 # "fd":I
} // :cond_0
return;
} // .end method
public Boolean skipConfirmationUi ( Integer p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "token" # I */
/* .param p2, "action" # Ljava/lang/String; */
/* .line 65 */
final String v0 = "MiuiBackup"; // const-string v0, "MiuiBackup"
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lmiui/app/backup/IBackupManager; */
/* .line 67 */
/* .local v0, "bm":Lmiui/app/backup/IBackupManager; */
try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 71 */
/* nop */
/* .line 72 */
int v1 = 1; // const/4 v1, 0x1
/* .line 68 */
/* :catch_0 */
/* move-exception v1 */
/* .line 69 */
/* .local v1, "e":Landroid/os/RemoteException; */
final String v2 = "Backup:BackupManagerServiceInjector"; // const-string v2, "Backup:BackupManagerServiceInjector"
final String v3 = "confirmation failed"; // const-string v3, "confirmation failed"
android.util.Slog .e ( v2,v3,v1 );
/* .line 70 */
int v2 = 0; // const/4 v2, 0x0
} // .end method
public Boolean tearDownAgentAndKill ( android.app.IActivityManager p0, android.content.pm.ApplicationInfo p1, Integer p2 ) {
/* .locals 5 */
/* .param p1, "activityManager" # Landroid/app/IActivityManager; */
/* .param p2, "appInfo" # Landroid/content/pm/ApplicationInfo; */
/* .param p3, "fd" # I */
/* .line 436 */
int v0 = 0; // const/4 v0, 0x0
/* .line 437 */
/* .local v0, "handle":Z */
final String v1 = "MiuiBackup"; // const-string v1, "MiuiBackup"
android.os.ServiceManager .getService ( v1 );
/* check-cast v1, Lmiui/app/backup/IBackupManager; */
/* .line 439 */
/* .local v1, "bm":Lmiui/app/backup/IBackupManager; */
v2 = try { // :try_start_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 440 */
v2 = this.packageName;
v2 = (( com.android.server.backup.BackupManagerServiceInjector ) p0 ).isNeedBeKilled ( v2, p3 ); // invoke-virtual {p0, v2, p3}, Lcom/android/server/backup/BackupManagerServiceInjector;->isNeedBeKilled(Ljava/lang/String;I)Z
/* if-nez v2, :cond_0 */
/* .line 441 */
int v0 = 1; // const/4 v0, 0x1
/* .line 442 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 447 */
} // :cond_0
/* .line 445 */
/* :catch_0 */
/* move-exception v2 */
/* .line 446 */
/* .local v2, "e":Landroid/os/RemoteException; */
final String v3 = "Backup:BackupManagerServiceInjector"; // const-string v3, "Backup:BackupManagerServiceInjector"
final String v4 = "isNeedBeKilled failed"; // const-string v4, "isNeedBeKilled failed"
android.util.Slog .e ( v3,v4,v2 );
/* .line 448 */
} // .end local v2 # "e":Landroid/os/RemoteException;
} // :goto_0
} // .end method
public void unlinkToDeath ( android.app.IBackupAgent p0 ) {
/* .locals 3 */
/* .param p1, "backupAgent" # Landroid/app/IBackupAgent; */
/* .line 226 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 227 */
/* .line 228 */
/* .local v0, "agentBinder":Landroid/os/IBinder; */
v1 = com.android.server.backup.BackupManagerServiceInjector.sBinderDeathLinker;
(( java.util.HashMap ) v1 ).get ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Landroid/os/IBinder$DeathRecipient; */
int v2 = 0; // const/4 v2, 0x0
/* .line 229 */
v1 = com.android.server.backup.BackupManagerServiceInjector.sBinderDeathLinker;
(( java.util.HashMap ) v1 ).remove ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 231 */
} // .end local v0 # "agentBinder":Landroid/os/IBinder;
} // :cond_0
return;
} // .end method
public void unregisterHuanjiReceiver ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 627 */
final String v0 = "Backup:BackupManagerServiceInjector"; // const-string v0, "Backup:BackupManagerServiceInjector"
/* const-string/jumbo v1, "unregisterHuanjiReceiver." */
android.util.Log .i ( v0,v1 );
/* .line 628 */
v0 = this.mHuanjiReceiver;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 629 */
(( com.android.server.backup.BackupManagerServiceInjector$HuanjiReceiver ) v0 ).unregister ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/backup/BackupManagerServiceInjector$HuanjiReceiver;->unregister(Landroid/content/Context;)V
/* .line 631 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
this.mHuanjiReceiver = v0;
/* .line 632 */
return;
} // .end method
public void waitingBeforeGetAgent ( ) {
/* .locals 2 */
/* .line 503 */
/* const-wide/16 v0, 0xc8 */
try { // :try_start_0
java.lang.Thread .sleep ( v0,v1 );
/* :try_end_0 */
/* .catch Ljava/lang/InterruptedException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 506 */
/* .line 504 */
/* :catch_0 */
/* move-exception v0 */
/* .line 505 */
/* .local v0, "e":Ljava/lang/InterruptedException; */
(( java.lang.InterruptedException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
/* .line 507 */
} // .end local v0 # "e":Ljava/lang/InterruptedException;
} // :goto_0
return;
} // .end method
public void writeMiuiBackupHeader ( android.os.ParcelFileDescriptor p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "out" # Landroid/os/ParcelFileDescriptor; */
/* .param p2, "fd" # I */
/* .line 102 */
final String v0 = "MiuiBackup"; // const-string v0, "MiuiBackup"
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lmiui/app/backup/IBackupManager; */
/* .line 104 */
/* .local v0, "bm":Lmiui/app/backup/IBackupManager; */
v1 = try { // :try_start_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 105 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 109 */
} // :cond_0
/* .line 107 */
/* :catch_0 */
/* move-exception v1 */
/* .line 108 */
/* .local v1, "e":Landroid/os/RemoteException; */
final String v2 = "Backup:BackupManagerServiceInjector"; // const-string v2, "Backup:BackupManagerServiceInjector"
/* const-string/jumbo v3, "writeMiuiBackupHeader failed" */
android.util.Slog .e ( v2,v3,v1 );
/* .line 110 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
