.class public Lcom/android/server/backup/BackupManagerServiceInjector$HuanjiReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BackupManagerServiceInjector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/backup/BackupManagerServiceInjector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HuanjiReceiver"
.end annotation


# static fields
.field static final ACTION_HUANJI_END_RESTORE:Ljava/lang/String; = "com.miui.huanji.END_RESTORE"

.field static final ACTION_HUANJI_RESTORE:Ljava/lang/String; = "com.miui.huanji.START_RESTORE"

.field private static final TAG:Ljava/lang/String; = "HuanjiReceiver"


# instance fields
.field private mUserBackupManagerService:Lcom/android/server/backup/UserBackupManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/backup/UserBackupManagerService;)V
    .locals 0
    .param p1, "userBackupManagerService"    # Lcom/android/server/backup/UserBackupManagerService;

    .line 640
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 641
    iput-object p1, p0, Lcom/android/server/backup/BackupManagerServiceInjector$HuanjiReceiver;->mUserBackupManagerService:Lcom/android/server/backup/UserBackupManagerService;

    .line 642
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 646
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 647
    .local v0, "action":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 648
    return-void

    .line 650
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "get action: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "HuanjiReceiver"

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 651
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    sparse-switch v1, :sswitch_data_0

    :cond_1
    goto :goto_0

    :sswitch_0
    const-string v1, "com.miui.huanji.END_RESTORE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v3

    goto :goto_1

    :sswitch_1
    const-string v1, "com.miui.huanji.START_RESTORE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    goto :goto_1

    :goto_0
    const/4 v1, -0x1

    :goto_1
    packed-switch v1, :pswitch_data_0

    goto :goto_2

    .line 656
    :pswitch_0
    iget-object v1, p0, Lcom/android/server/backup/BackupManagerServiceInjector$HuanjiReceiver;->mUserBackupManagerService:Lcom/android/server/backup/UserBackupManagerService;

    invoke-virtual {v1, v2}, Lcom/android/server/backup/UserBackupManagerService;->setSetupComplete(Z)V

    goto :goto_2

    .line 653
    :pswitch_1
    iget-object v1, p0, Lcom/android/server/backup/BackupManagerServiceInjector$HuanjiReceiver;->mUserBackupManagerService:Lcom/android/server/backup/UserBackupManagerService;

    invoke-virtual {v1, v3}, Lcom/android/server/backup/UserBackupManagerService;->setSetupComplete(Z)V

    .line 654
    nop

    .line 659
    :goto_2
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x44fa5533 -> :sswitch_1
        0x78358946 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public register(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 662
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 663
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.miui.huanji.START_RESTORE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 664
    const-string v1, "com.miui.huanji.END_RESTORE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 665
    const-string v1, "android.permission.BACKUP"

    const/4 v2, 0x0

    invoke-virtual {p1, p0, v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 667
    return-void
.end method

.method public unregister(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .line 670
    invoke-virtual {p1, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 671
    return-void
.end method
