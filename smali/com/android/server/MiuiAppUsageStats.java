public class com.android.server.MiuiAppUsageStats {
	 /* .source "MiuiAppUsageStats.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/MiuiAppUsageStats$AppUsageStats; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer ACTIVITY_DESTROYED;
private static final Integer ACTIVITY_STOPPED;
private static final java.util.List APP_STAT_TO_SETTINGS;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final Boolean DEBUG;
private static Long INTERVAL_HOUR;
private static final java.util.List SPECIAL_APP_LIST;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final Integer STAT_TYPE_DAY;
private static final Integer STAT_TYPE_HOUR;
private static final java.lang.String TAG;
/* # direct methods */
static com.android.server.MiuiAppUsageStats ( ) {
/* .locals 2 */
/* .line 24 */
/* const-wide/32 v0, 0x36ee80 */
/* sput-wide v0, Lcom/android/server/MiuiAppUsageStats;->INTERVAL_HOUR:J */
/* .line 31 */
/* new-instance v0, Lcom/android/server/MiuiAppUsageStats$1; */
/* invoke-direct {v0}, Lcom/android/server/MiuiAppUsageStats$1;-><init>()V */
/* .line 38 */
/* new-instance v0, Lcom/android/server/MiuiAppUsageStats$2; */
/* invoke-direct {v0}, Lcom/android/server/MiuiAppUsageStats$2;-><init>()V */
return;
} // .end method
public com.android.server.MiuiAppUsageStats ( ) {
/* .locals 0 */
/* .line 20 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
private Boolean aggregate ( android.content.Context p0, java.util.List p1, Long p2, Long p3, com.android.server.MiuiAppUsageStats$AppUsageStats p4 ) {
/* .locals 21 */
/* .param p1, "ctx" # Landroid/content/Context; */
/* .param p3, "statBegin" # J */
/* .param p5, "statEnd" # J */
/* .param p7, "stat" # Lcom/android/server/MiuiAppUsageStats$AppUsageStats; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/content/Context;", */
/* "Ljava/util/List<", */
/* "Landroid/app/usage/UsageEvents$Event;", */
/* ">;JJ", */
/* "Lcom/android/server/MiuiAppUsageStats$AppUsageStats;", */
/* ")Z" */
/* } */
} // .end annotation
/* .line 298 */
/* .local p2, "events":Ljava/util/List;, "Ljava/util/List<Landroid/app/usage/UsageEvents$Event;>;" */
/* move-object/from16 v0, p2 */
/* move-object/from16 v8, p7 */
int v1 = 0; // const/4 v1, 0x0
final String v2 = "MiuiAppUsageStats"; // const-string v2, "MiuiAppUsageStats"
if ( v0 != null) { // if-eqz v0, :cond_b
v3 = /* invoke-interface/range {p2 ..p2}, Ljava/util/List;->isEmpty()Z */
/* if-nez v3, :cond_b */
/* if-nez v8, :cond_0 */
/* goto/16 :goto_5 */
/* .line 304 */
} // :cond_0
/* sub-long v3, p5, p3 */
/* sget-wide v5, Lcom/android/server/MiuiAppUsageStats;->INTERVAL_HOUR:J */
/* cmp-long v3, v3, v5 */
/* const/16 v16, 0x1 */
/* if-lez v3, :cond_1 */
/* .line 305 */
/* move v15, v1 */
} // :cond_1
/* move/from16 v15, v16 */
/* .line 309 */
/* .local v15, "statType":I */
} // :goto_0
/* const-wide/16 v3, 0x0 */
/* .line 310 */
/* .local v3, "start":J */
/* invoke-virtual/range {p7 ..p7}, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->getPkgName()Ljava/lang/String; */
/* .line 311 */
/* .local v7, "pkgName":Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* move-wide v5, v3 */
} // .end local v3 # "start":J
/* .local v1, "idx":I */
/* .local v5, "start":J */
} // :goto_1
v3 = /* invoke-interface/range {p2 ..p2}, Ljava/util/List;->size()I */
/* const-wide/16 v17, 0x0 */
/* if-ge v1, v3, :cond_9 */
/* .line 312 */
/* check-cast v3, Landroid/app/usage/UsageEvents$Event; */
/* .line 314 */
/* .local v3, "event":Landroid/app/usage/UsageEvents$Event; */
(( android.app.usage.UsageEvents$Event ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Landroid/app/usage/UsageEvents$Event;->getPackageName()Ljava/lang/String;
v4 = android.text.TextUtils .equals ( v4,v7 );
/* if-nez v4, :cond_2 */
/* .line 315 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "Ops! Fail to aggregate due to different package.event.pkgName="; // const-string v9, "Ops! Fail to aggregate due to different package.event.pkgName="
(( java.lang.StringBuilder ) v4 ).append ( v9 ); // invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 316 */
(( android.app.usage.UsageEvents$Event ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Landroid/app/usage/UsageEvents$Event;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v9 ); // invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = ", stat.pkgName="; // const-string v9, ", stat.pkgName="
(( java.lang.StringBuilder ) v4 ).append ( v9 ); // invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 315 */
android.util.Slog .w ( v2,v4 );
/* .line 319 */
} // :cond_2
v4 = (( android.app.usage.UsageEvents$Event ) v3 ).getEventType ( ); // invoke-virtual {v3}, Landroid/app/usage/UsageEvents$Event;->getEventType()I
/* .line 320 */
/* .local v4, "eventType":I */
/* packed-switch v4, :pswitch_data_0 */
/* .line 353 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "Ops! Invalid eventType for aggregate.pkgName="; // const-string v10, "Ops! Invalid eventType for aggregate.pkgName="
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* invoke-virtual/range {p7 ..p7}, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->getPkgName()Ljava/lang/String; */
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v10 = ", eventType="; // const-string v10, ", eventType="
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v4 ); // invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = ",start="; // const-string v10, ",start="
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v5, v6 ); // invoke-virtual {v9, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v9 );
/* goto/16 :goto_3 */
/* .line 330 */
/* :pswitch_0 */
/* cmp-long v9, v5, v17 */
/* if-gtz v9, :cond_3 */
/* if-lez v1, :cond_3 */
/* .line 331 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "aggregate()...start <= 0, This is not the first MOVE_TO_BACKGROUND."; // const-string v10, "aggregate()...start <= 0, This is not the first MOVE_TO_BACKGROUND."
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v7 ); // invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v9 );
/* .line 333 */
/* .line 335 */
} // :cond_3
(( android.app.usage.UsageEvents$Event ) v3 ).getTimeStamp ( ); // invoke-virtual {v3}, Landroid/app/usage/UsageEvents$Event;->getTimeStamp()J
/* move-result-wide v13 */
/* .line 337 */
/* .local v13, "timeStamp":J */
/* cmp-long v9, v5, v17 */
/* if-gtz v9, :cond_4 */
/* .line 338 */
/* move-object/from16 v9, p0 */
/* move-object v10, v7 */
/* move-wide/from16 v11, p3 */
/* move-wide/from16 v19, v13 */
} // .end local v13 # "timeStamp":J
/* .local v19, "timeStamp":J */
/* invoke-direct/range {v9 ..v15}, Lcom/android/server/MiuiAppUsageStats;->handleCrossUsage(Ljava/lang/String;JJI)J */
/* move-result-wide v9 */
/* move-wide v11, v9 */
/* move-wide/from16 v9, v19 */
/* .line 339 */
} // .end local v19 # "timeStamp":J
/* .restart local v13 # "timeStamp":J */
} // :cond_4
/* move-wide/from16 v19, v13 */
} // .end local v13 # "timeStamp":J
/* .restart local v19 # "timeStamp":J */
/* move-wide/from16 v9, v19 */
} // .end local v19 # "timeStamp":J
/* .local v9, "timeStamp":J */
/* sub-long v11, v9, v5 */
} // :goto_2
/* nop */
/* .line 340 */
/* .local v11, "diff":J */
/* const-wide/16 v5, 0x0 */
/* .line 341 */
/* cmp-long v13, v11, v17 */
/* if-gtz v13, :cond_5 */
/* .line 342 */
/* new-instance v13, Ljava/lang/StringBuilder; */
/* invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V */
final String v14 = "aggregate()...Skip this aggregate, diff is invalid diff= "; // const-string v14, "aggregate()...Skip this aggregate, diff is invalid diff= "
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v11, v12 ); // invoke-virtual {v13, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).toString ( ); // invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v13 );
/* .line 343 */
/* .line 345 */
} // :cond_5
(( com.android.server.MiuiAppUsageStats$AppUsageStats ) v8 ).addForegroundTime ( v11, v12 ); // invoke-virtual {v8, v11, v12}, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->addForegroundTime(J)V
/* .line 346 */
(( com.android.server.MiuiAppUsageStats$AppUsageStats ) v8 ).updateLastUsageTime ( v9, v10 ); // invoke-virtual {v8, v9, v10}, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->updateLastUsageTime(J)V
/* .line 348 */
/* iget-wide v13, v8, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->lastBackGroundTime:J */
/* cmp-long v13, v13, v17 */
if ( v13 != null) { // if-eqz v13, :cond_6
/* iget-wide v13, v8, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->lastBackGroundTime:J */
/* cmp-long v13, v13, v9 */
/* if-gez v13, :cond_8 */
/* .line 349 */
} // :cond_6
/* iput-wide v9, v8, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->lastBackGroundTime:J */
/* .line 322 */
} // .end local v9 # "timeStamp":J
} // .end local v11 # "diff":J
/* :pswitch_1 */
(( android.app.usage.UsageEvents$Event ) v3 ).getTimeStamp ( ); // invoke-virtual {v3}, Landroid/app/usage/UsageEvents$Event;->getTimeStamp()J
/* move-result-wide v5 */
/* .line 323 */
/* invoke-virtual/range {p7 ..p7}, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->increaseForegroundCount()V */
/* .line 324 */
/* iget-wide v9, v8, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->firstForeGroundTime:J */
/* cmp-long v9, v9, v17 */
if ( v9 != null) { // if-eqz v9, :cond_7
/* iget-wide v9, v8, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->firstForeGroundTime:J */
/* cmp-long v9, v9, v5 */
/* if-lez v9, :cond_8 */
/* .line 325 */
} // :cond_7
/* iput-wide v5, v8, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->firstForeGroundTime:J */
/* .line 311 */
} // .end local v3 # "event":Landroid/app/usage/UsageEvents$Event;
} // .end local v4 # "eventType":I
} // :cond_8
} // :goto_3
/* add-int/lit8 v1, v1, 0x1 */
/* goto/16 :goto_1 */
/* .line 360 */
} // .end local v1 # "idx":I
} // :cond_9
/* cmp-long v1, v5, v17 */
/* if-lez v1, :cond_a */
/* .line 361 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
/* move-wide v3, v5 */
/* move-wide v9, v5 */
} // .end local v5 # "start":J
/* .local v9, "start":J */
/* move-wide/from16 v5, p5 */
/* move-object v11, v7 */
} // .end local v7 # "pkgName":Ljava/lang/String;
/* .local v11, "pkgName":Ljava/lang/String; */
/* move-object/from16 v7, p7 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/MiuiAppUsageStats;->guess(Landroid/content/Context;JJLcom/android/server/MiuiAppUsageStats$AppUsageStats;)V */
/* .line 360 */
} // .end local v9 # "start":J
} // .end local v11 # "pkgName":Ljava/lang/String;
/* .restart local v5 # "start":J */
/* .restart local v7 # "pkgName":Ljava/lang/String; */
} // :cond_a
/* move-wide v9, v5 */
/* move-object v11, v7 */
/* .line 363 */
} // .end local v5 # "start":J
} // .end local v7 # "pkgName":Ljava/lang/String;
/* .restart local v9 # "start":J */
/* .restart local v11 # "pkgName":Ljava/lang/String; */
} // :goto_4
/* .line 299 */
} // .end local v9 # "start":J
} // .end local v11 # "pkgName":Ljava/lang/String;
} // .end local v15 # "statType":I
} // :cond_b
} // :goto_5
final String v3 = "aggregate()......Fail since invalid params."; // const-string v3, "aggregate()......Fail since invalid params."
android.util.Slog .e ( v2,v3 );
/* .line 300 */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void aggregateEventByPackage ( android.app.usage.UsageEvents p0, android.util.ArrayMap p1 ) {
/* .locals 4 */
/* .param p1, "events" # Landroid/app/usage/UsageEvents; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/app/usage/UsageEvents;", */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Landroid/app/usage/UsageEvents$Event;", */
/* ">;>;)V" */
/* } */
} // .end annotation
/* .line 157 */
/* .local p2, "aggregateEvents":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Ljava/util/List<Landroid/app/usage/UsageEvents$Event;>;>;" */
if ( p1 != null) { // if-eqz p1, :cond_6
/* if-nez p2, :cond_0 */
/* .line 161 */
} // :cond_0
} // :goto_0
v0 = (( android.app.usage.UsageEvents ) p1 ).hasNextEvent ( ); // invoke-virtual {p1}, Landroid/app/usage/UsageEvents;->hasNextEvent()Z
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 162 */
/* new-instance v0, Landroid/app/usage/UsageEvents$Event; */
/* invoke-direct {v0}, Landroid/app/usage/UsageEvents$Event;-><init>()V */
/* .line 163 */
/* .local v0, "event":Landroid/app/usage/UsageEvents$Event; */
v1 = (( android.app.usage.UsageEvents ) p1 ).getNextEvent ( v0 ); // invoke-virtual {p1, v0}, Landroid/app/usage/UsageEvents;->getNextEvent(Landroid/app/usage/UsageEvents$Event;)Z
if ( v1 != null) { // if-eqz v1, :cond_4
v1 = /* invoke-direct {p0, v0}, Lcom/android/server/MiuiAppUsageStats;->valid(Landroid/app/usage/UsageEvents$Event;)Z */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 168 */
v1 = com.android.server.MiuiAppUsageStats.APP_STAT_TO_SETTINGS;
v1 = (( android.app.usage.UsageEvents$Event ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/app/usage/UsageEvents$Event;->getPackageName()Ljava/lang/String;
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 169 */
final String v1 = "com.android.settings"; // const-string v1, "com.android.settings"
(( android.util.ArrayMap ) p2 ).get ( v1 ); // invoke-virtual {p2, v1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Ljava/util/List; */
/* .line 170 */
/* .local v2, "list":Ljava/util/List;, "Ljava/util/List<Landroid/app/usage/UsageEvents$Event;>;" */
/* if-nez v2, :cond_1 */
/* .line 171 */
/* new-instance v3, Ljava/util/ArrayList; */
/* invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V */
/* move-object v2, v3 */
/* .line 173 */
} // :cond_1
/* .line 174 */
(( android.util.ArrayMap ) p2 ).put ( v1, v2 ); // invoke-virtual {p2, v1, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 175 */
/* .line 177 */
} // .end local v2 # "list":Ljava/util/List;, "Ljava/util/List<Landroid/app/usage/UsageEvents$Event;>;"
} // :cond_2
(( android.app.usage.UsageEvents$Event ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/app/usage/UsageEvents$Event;->getPackageName()Ljava/lang/String;
v1 = (( android.util.ArrayMap ) p2 ).containsKey ( v1 ); // invoke-virtual {p2, v1}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z
/* .line 179 */
/* .local v1, "exist":Z */
if ( v1 != null) { // if-eqz v1, :cond_3
(( android.app.usage.UsageEvents$Event ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/app/usage/UsageEvents$Event;->getPackageName()Ljava/lang/String;
(( android.util.ArrayMap ) p2 ).get ( v2 ); // invoke-virtual {p2, v2}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Ljava/util/List; */
} // :cond_3
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* .line 181 */
/* .local v2, "eventList":Ljava/util/List;, "Ljava/util/List<Landroid/app/usage/UsageEvents$Event;>;" */
} // :goto_1
/* .line 182 */
/* if-nez v1, :cond_4 */
/* .line 183 */
(( android.app.usage.UsageEvents$Event ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/app/usage/UsageEvents$Event;->getPackageName()Ljava/lang/String;
(( android.util.ArrayMap ) p2 ).put ( v3, v2 ); // invoke-virtual {p2, v3, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 186 */
} // .end local v0 # "event":Landroid/app/usage/UsageEvents$Event;
} // .end local v1 # "exist":Z
} // .end local v2 # "eventList":Ljava/util/List;, "Ljava/util/List<Landroid/app/usage/UsageEvents$Event;>;"
} // :cond_4
/* .line 187 */
} // :cond_5
return;
/* .line 158 */
} // :cond_6
} // :goto_2
return;
} // .end method
private void aggregateUsageStatsByEvent ( android.content.Context p0, android.app.usage.UsageEvents p1, Long p2, Long p3, android.util.ArrayMap p4 ) {
/* .locals 16 */
/* .param p1, "ctx" # Landroid/content/Context; */
/* .param p2, "events" # Landroid/app/usage/UsageEvents; */
/* .param p3, "start" # J */
/* .param p5, "end" # J */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/content/Context;", */
/* "Landroid/app/usage/UsageEvents;", */
/* "JJ", */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/MiuiAppUsageStats$AppUsageStats;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 371 */
/* .local p7, "result":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Lcom/android/server/MiuiAppUsageStats$AppUsageStats;>;" */
/* move-object/from16 v8, p0 */
/* move-object/from16 v9, p2 */
/* move-object/from16 v10, p7 */
if ( v9 != null) { // if-eqz v9, :cond_3
if ( v10 != null) { // if-eqz v10, :cond_3
/* if-nez p1, :cond_0 */
/* .line 375 */
} // :cond_0
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
/* move-object v11, v0 */
/* .line 378 */
/* .local v11, "aggregateEvents":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Ljava/util/List<Landroid/app/usage/UsageEvents$Event;>;>;" */
/* invoke-direct {v8, v9, v11}, Lcom/android/server/MiuiAppUsageStats;->aggregateEventByPackage(Landroid/app/usage/UsageEvents;Landroid/util/ArrayMap;)V */
/* .line 381 */
(( android.util.ArrayMap ) v11 ).keySet ( ); // invoke-virtual {v11}, Landroid/util/ArrayMap;->keySet()Ljava/util/Set;
/* .line 382 */
/* .local v12, "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
v0 = } // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_2
/* move-object v14, v0 */
/* check-cast v14, Ljava/lang/String; */
/* .line 383 */
/* .local v14, "key":Ljava/lang/String; */
/* new-instance v0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats; */
/* invoke-direct {v0, v8, v14}, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;-><init>(Lcom/android/server/MiuiAppUsageStats;Ljava/lang/String;)V */
/* move-object v15, v0 */
/* .line 385 */
/* .local v15, "stat":Lcom/android/server/MiuiAppUsageStats$AppUsageStats; */
(( android.util.ArrayMap ) v11 ).get ( v14 ); // invoke-virtual {v11, v14}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* move-object v2, v0 */
/* check-cast v2, Ljava/util/List; */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
/* move-wide/from16 v3, p3 */
/* move-wide/from16 v5, p5 */
/* move-object v7, v15 */
v0 = /* invoke-direct/range {v0 ..v7}, Lcom/android/server/MiuiAppUsageStats;->aggregate(Landroid/content/Context;Ljava/util/List;JJLcom/android/server/MiuiAppUsageStats$AppUsageStats;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 386 */
(( android.util.ArrayMap ) v10 ).put ( v14, v15 ); // invoke-virtual {v10, v14, v15}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 388 */
} // .end local v14 # "key":Ljava/lang/String;
} // .end local v15 # "stat":Lcom/android/server/MiuiAppUsageStats$AppUsageStats;
} // :cond_1
/* .line 389 */
} // :cond_2
return;
/* .line 372 */
} // .end local v11 # "aggregateEvents":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Ljava/util/List<Landroid/app/usage/UsageEvents$Event;>;>;"
} // .end local v12 # "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
} // :cond_3
} // :goto_1
return;
} // .end method
private Boolean checkStopped ( android.app.usage.UsageEvents p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "usageEvents" # Landroid/app/usage/UsageEvents; */
/* .param p2, "pkgName" # Ljava/lang/String; */
/* .line 282 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_3
v1 = android.text.TextUtils .isEmpty ( p2 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 286 */
} // :cond_0
} // :goto_0
v1 = (( android.app.usage.UsageEvents ) p1 ).hasNextEvent ( ); // invoke-virtual {p1}, Landroid/app/usage/UsageEvents;->hasNextEvent()Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 287 */
/* new-instance v1, Landroid/app/usage/UsageEvents$Event; */
/* invoke-direct {v1}, Landroid/app/usage/UsageEvents$Event;-><init>()V */
/* .line 288 */
/* .local v1, "event":Landroid/app/usage/UsageEvents$Event; */
v2 = (( android.app.usage.UsageEvents ) p1 ).getNextEvent ( v1 ); // invoke-virtual {p1, v1}, Landroid/app/usage/UsageEvents;->getNextEvent(Landroid/app/usage/UsageEvents$Event;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 289 */
(( android.app.usage.UsageEvents$Event ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/app/usage/UsageEvents$Event;->getPackageName()Ljava/lang/String;
v2 = (( java.lang.String ) p2 ).equals ( v2 ); // invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 290 */
v0 = /* invoke-direct {p0, v1}, Lcom/android/server/MiuiAppUsageStats;->vaildStopEvent(Landroid/app/usage/UsageEvents$Event;)Z */
/* .line 292 */
} // .end local v1 # "event":Landroid/app/usage/UsageEvents$Event;
} // :cond_1
/* .line 293 */
} // :cond_2
/* .line 283 */
} // :cond_3
} // :goto_1
final String v1 = "MiuiAppUsageStats"; // const-string v1, "MiuiAppUsageStats"
final String v2 = "checkStopped()......return since invalid params."; // const-string v2, "checkStopped()......return since invalid params."
android.util.Slog .e ( v1,v2 );
/* .line 284 */
} // .end method
private android.app.usage.UsageEvents getEventStats ( android.content.Context p0, Long p1, Long p2 ) {
/* .locals 4 */
/* .param p1, "ctx" # Landroid/content/Context; */
/* .param p2, "start" # J */
/* .param p4, "end" # J */
/* .line 128 */
int v0 = 0; // const/4 v0, 0x0
/* .line 130 */
/* .local v0, "events":Landroid/app/usage/UsageEvents; */
/* const-string/jumbo v1, "usagestats" */
(( android.content.Context ) p1 ).getSystemService ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/app/usage/UsageStatsManager; */
/* .line 132 */
/* .local v1, "manager":Landroid/app/usage/UsageStatsManager; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 133 */
(( android.app.usage.UsageStatsManager ) v1 ).queryEvents ( p2, p3, p4, p5 ); // invoke-virtual {v1, p2, p3, p4, p5}, Landroid/app/usage/UsageStatsManager;->queryEvents(JJ)Landroid/app/usage/UsageEvents;
/* .line 135 */
} // :cond_0
final String v2 = "MiuiAppUsageStats"; // const-string v2, "MiuiAppUsageStats"
final String v3 = "getEventStats()......manager is null!"; // const-string v3, "getEventStats()......manager is null!"
android.util.Slog .e ( v2,v3 );
/* .line 137 */
} // :goto_0
} // .end method
private void guess ( android.content.Context p0, Long p1, Long p2, com.android.server.MiuiAppUsageStats$AppUsageStats p3 ) {
/* .locals 24 */
/* .param p1, "ctx" # Landroid/content/Context; */
/* .param p2, "start" # J */
/* .param p4, "statEnd" # J */
/* .param p6, "stat" # Lcom/android/server/MiuiAppUsageStats$AppUsageStats; */
/* .line 215 */
/* move-object/from16 v6, p0 */
/* move-wide/from16 v7, p4 */
/* move-object/from16 v9, p6 */
if ( p1 != null) { // if-eqz p1, :cond_b
/* if-nez v9, :cond_0 */
/* goto/16 :goto_7 */
/* .line 218 */
} // :cond_0
/* invoke-virtual/range {p6 ..p6}, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->getPkgName()Ljava/lang/String; */
/* .line 224 */
/* .local v10, "pkgName":Ljava/lang/String; */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v11 */
/* .line 225 */
/* .local v11, "currentTime":J */
java.lang.Math .min ( v7,v8,v11,v12 );
/* move-result-wide v13 */
/* .line 228 */
/* .local v13, "end":J */
/* const-wide/16 v0, 0x1 */
/* add-long v2, p2, v0 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
/* move-wide v4, v13 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/MiuiAppUsageStats;->getEventStats(Landroid/content/Context;JJ)Landroid/app/usage/UsageEvents; */
/* .line 229 */
/* .local v15, "usageEvents":Landroid/app/usage/UsageEvents; */
/* if-nez v15, :cond_1 */
return;
/* .line 230 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 231 */
/* .local v0, "selfStopped":Z */
int v1 = 0; // const/4 v1, 0x0
/* .line 232 */
/* .local v1, "otherStarted":Z */
/* const-wide/16 v2, 0x0 */
/* move/from16 v16, v1 */
/* move-wide/from16 v17, v2 */
/* .line 233 */
} // .end local v1 # "otherStarted":Z
/* .local v16, "otherStarted":Z */
/* .local v17, "otherStartedTimeStamp":J */
} // :goto_0
v1 = (( android.app.usage.UsageEvents ) v15 ).hasNextEvent ( ); // invoke-virtual {v15}, Landroid/app/usage/UsageEvents;->hasNextEvent()Z
final String v4 = "MiuiAppUsageStats"; // const-string v4, "MiuiAppUsageStats"
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 234 */
/* new-instance v1, Landroid/app/usage/UsageEvents$Event; */
/* invoke-direct {v1}, Landroid/app/usage/UsageEvents$Event;-><init>()V */
/* .line 235 */
/* .local v1, "event":Landroid/app/usage/UsageEvents$Event; */
v2 = (( android.app.usage.UsageEvents ) v15 ).getNextEvent ( v1 ); // invoke-virtual {v15, v1}, Landroid/app/usage/UsageEvents;->getNextEvent(Landroid/app/usage/UsageEvents$Event;)Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 236 */
(( android.app.usage.UsageEvents$Event ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/app/usage/UsageEvents$Event;->getPackageName()Ljava/lang/String;
v2 = (( java.lang.String ) v10 ).equals ( v2 ); // invoke-virtual {v10, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
v2 = /* invoke-direct {v6, v1}, Lcom/android/server/MiuiAppUsageStats;->vaildStopEvent(Landroid/app/usage/UsageEvents$Event;)Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 237 */
(( android.app.usage.UsageEvents$Event ) v1 ).getTimeStamp ( ); // invoke-virtual {v1}, Landroid/app/usage/UsageEvents$Event;->getTimeStamp()J
/* move-result-wide v13 */
/* .line 238 */
int v0 = 1; // const/4 v0, 0x1
/* .line 239 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "guess()...selfStopped! eventType="; // const-string v3, "guess()...selfStopped! eventType="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = (( android.app.usage.UsageEvents$Event ) v1 ).getEventType ( ); // invoke-virtual {v1}, Landroid/app/usage/UsageEvents$Event;->getEventType()I
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v2 );
/* .line 240 */
/* move/from16 v19, v0 */
/* .line 241 */
} // :cond_2
/* if-nez v16, :cond_3 */
v2 = (( com.android.server.MiuiAppUsageStats ) v6 ).validStartEvent ( v1 ); // invoke-virtual {v6, v1}, Lcom/android/server/MiuiAppUsageStats;->validStartEvent(Landroid/app/usage/UsageEvents$Event;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 242 */
int v2 = 1; // const/4 v2, 0x1
/* .line 243 */
} // .end local v16 # "otherStarted":Z
/* .local v2, "otherStarted":Z */
/* move v5, v2 */
} // .end local v2 # "otherStarted":Z
/* .local v5, "otherStarted":Z */
(( android.app.usage.UsageEvents$Event ) v1 ).getTimeStamp ( ); // invoke-virtual {v1}, Landroid/app/usage/UsageEvents$Event;->getTimeStamp()J
/* move-result-wide v2 */
/* .line 244 */
} // .end local v17 # "otherStartedTimeStamp":J
/* .local v2, "otherStartedTimeStamp":J */
/* move/from16 v19, v0 */
} // .end local v0 # "selfStopped":Z
/* .local v19, "selfStopped":Z */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* move/from16 v16, v5 */
} // .end local v5 # "otherStarted":Z
/* .restart local v16 # "otherStarted":Z */
final String v5 = "guess()...otherStarted! other="; // const-string v5, "guess()...otherStarted! other="
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 245 */
(( android.app.usage.UsageEvents$Event ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/app/usage/UsageEvents$Event;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ",timestamp="; // const-string v5, ",timestamp="
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 244 */
android.util.Slog .d ( v4,v0 );
/* move-wide/from16 v17, v2 */
/* .line 241 */
} // .end local v2 # "otherStartedTimeStamp":J
} // .end local v19 # "selfStopped":Z
/* .restart local v0 # "selfStopped":Z */
/* .restart local v17 # "otherStartedTimeStamp":J */
} // :cond_3
/* move/from16 v19, v0 */
} // .end local v0 # "selfStopped":Z
/* .restart local v19 # "selfStopped":Z */
/* .line 235 */
} // .end local v19 # "selfStopped":Z
/* .restart local v0 # "selfStopped":Z */
} // :cond_4
/* move/from16 v19, v0 */
/* .line 248 */
} // .end local v0 # "selfStopped":Z
} // .end local v1 # "event":Landroid/app/usage/UsageEvents$Event;
/* .restart local v19 # "selfStopped":Z */
} // :goto_1
/* move/from16 v0, v19 */
/* goto/16 :goto_0 */
/* .line 233 */
} // .end local v19 # "selfStopped":Z
/* .restart local v0 # "selfStopped":Z */
} // :cond_5
/* move/from16 v19, v0 */
/* .line 252 */
} // .end local v0 # "selfStopped":Z
/* .restart local v19 # "selfStopped":Z */
} // :goto_2
/* if-nez v19, :cond_7 */
if ( v16 != null) { // if-eqz v16, :cond_7
/* .line 253 */
/* sget-wide v0, Lcom/android/server/MiuiAppUsageStats;->INTERVAL_HOUR:J */
/* const-wide/16 v2, 0x2 */
/* mul-long/2addr v0, v2 */
/* add-long v20, v13, v0 */
/* .line 254 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
/* move-wide v2, v13 */
/* move-wide/from16 v22, v13 */
/* move-object v13, v4 */
} // .end local v13 # "end":J
/* .local v22, "end":J */
/* move-wide/from16 v4, v20 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/MiuiAppUsageStats;->getEventStats(Landroid/content/Context;JJ)Landroid/app/usage/UsageEvents; */
/* .line 253 */
v0 = /* invoke-direct {v6, v0, v10}, Lcom/android/server/MiuiAppUsageStats;->checkStopped(Landroid/app/usage/UsageEvents;Ljava/lang/String;)Z */
/* if-nez v0, :cond_6 */
/* .line 256 */
/* move-wide/from16 v0, v17 */
/* .line 257 */
} // .end local v22 # "end":J
/* .local v0, "end":J */
final String v2 = "guess()...This is a mis-event, and we treat it end while other started"; // const-string v2, "guess()...This is a mis-event, and we treat it end while other started"
android.util.Slog .d ( v13,v2 );
/* .line 259 */
} // .end local v0 # "end":J
/* .restart local v22 # "end":J */
} // :cond_6
final String v0 = "guess()...This is a cross usage, because we find it is stopped in the future."; // const-string v0, "guess()...This is a cross usage, because we find it is stopped in the future."
android.util.Slog .d ( v13,v0 );
/* .line 252 */
} // .end local v22 # "end":J
/* .restart local v13 # "end":J */
} // :cond_7
/* move-wide/from16 v22, v13 */
/* move-object v13, v4 */
/* .line 264 */
} // .end local v13 # "end":J
/* .restart local v22 # "end":J */
} // :goto_3
/* move-wide/from16 v0, v22 */
} // .end local v22 # "end":J
/* .restart local v0 # "end":J */
} // :goto_4
/* sub-long v2, v0, p2 */
/* .line 265 */
/* .local v2, "gap":J */
/* iput-wide v0, v9, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->lastBackGroundTime:J */
/* .line 267 */
/* cmp-long v4, v11, v7 */
/* if-gtz v4, :cond_8 */
/* .line 268 */
final String v4 = "guess()...Should not go here!"; // const-string v4, "guess()...Should not go here!"
android.util.Slog .e ( v13,v4 );
/* .line 272 */
} // :cond_8
/* sget-wide v4, Lcom/android/server/MiuiAppUsageStats;->INTERVAL_HOUR:J */
/* const-wide/16 v20, 0x3 */
/* mul-long v4, v4, v20 */
/* cmp-long v4, v2, v4 */
/* if-gtz v4, :cond_a */
/* const-wide/16 v4, 0x0 */
/* cmp-long v4, v2, v4 */
/* if-gez v4, :cond_9 */
/* move-wide/from16 v22, v0 */
/* .line 275 */
} // :cond_9
(( com.android.server.MiuiAppUsageStats$AppUsageStats ) v9 ).addForegroundTime ( v2, v3 ); // invoke-virtual {v9, v2, v3}, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->addForegroundTime(J)V
/* .line 276 */
(( com.android.server.MiuiAppUsageStats$AppUsageStats ) v9 ).setLastUsageTime ( v0, v1 ); // invoke-virtual {v9, v0, v1}, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->setLastUsageTime(J)V
/* .line 277 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "guess()...gap="; // const-string v5, "guess()...gap="
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-wide/32 v20, 0xea60 */
/* move-wide/from16 v22, v0 */
} // .end local v0 # "end":J
/* .restart local v22 # "end":J */
/* div-long v0, v2, v20 */
(( java.lang.StringBuilder ) v4 ).append ( v0, v1 ); // invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = "min("; // const-string v1, "min("
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = "ms)"; // const-string v1, "ms)"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v13,v0 );
/* .line 272 */
} // .end local v22 # "end":J
/* .restart local v0 # "end":J */
} // :cond_a
/* move-wide/from16 v22, v0 */
/* .line 273 */
} // .end local v0 # "end":J
/* .restart local v22 # "end":J */
} // :goto_5
final String v0 = "guess()...the gap is invalid and we treat it as a mis-event"; // const-string v0, "guess()...the gap is invalid and we treat it as a mis-event"
android.util.Slog .e ( v13,v0 );
/* .line 279 */
} // :goto_6
return;
/* .line 216 */
} // .end local v2 # "gap":J
} // .end local v10 # "pkgName":Ljava/lang/String;
} // .end local v11 # "currentTime":J
} // .end local v15 # "usageEvents":Landroid/app/usage/UsageEvents;
} // .end local v16 # "otherStarted":Z
} // .end local v17 # "otherStartedTimeStamp":J
} // .end local v19 # "selfStopped":Z
} // .end local v22 # "end":J
} // :cond_b
} // :goto_7
return;
} // .end method
private Long handleCrossUsage ( java.lang.String p0, Long p1, Long p2, Integer p3 ) {
/* .locals 8 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "start" # J */
/* .param p4, "end" # J */
/* .param p6, "statType" # I */
/* .line 194 */
/* sub-long v0, p4, p2 */
/* .line 196 */
/* .local v0, "diff":J */
/* const-wide/16 v2, 0x0 */
/* .line 197 */
/* .local v2, "maxDiff":J */
/* packed-switch p6, :pswitch_data_0 */
/* .line 205 */
/* const-wide/16 v2, 0x0 */
/* .line 202 */
/* :pswitch_0 */
/* sget-wide v2, Lcom/android/server/MiuiAppUsageStats;->INTERVAL_HOUR:J */
/* .line 203 */
/* .line 199 */
/* :pswitch_1 */
/* const-wide/16 v4, 0x4 */
/* sget-wide v6, Lcom/android/server/MiuiAppUsageStats;->INTERVAL_HOUR:J */
/* mul-long v2, v6, v4 */
/* .line 200 */
/* nop */
/* .line 209 */
} // :goto_0
/* cmp-long v4, v0, v2 */
/* if-lez v4, :cond_0 */
/* const-wide/16 v4, 0x0 */
} // :cond_0
/* move-wide v4, v0 */
} // :goto_1
/* move-wide v0, v4 */
/* .line 211 */
/* return-wide v0 */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private Boolean isSpecialApp ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 121 */
v0 = v0 = com.android.server.MiuiAppUsageStats.SPECIAL_APP_LIST;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 122 */
int v0 = 1; // const/4 v0, 0x1
/* .line 124 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean vaildStopEvent ( android.app.usage.UsageEvents$Event p0 ) {
/* .locals 2 */
/* .param p1, "event" # Landroid/app/usage/UsageEvents$Event; */
/* .line 146 */
v0 = (( android.app.usage.UsageEvents$Event ) p1 ).getEventType ( ); // invoke-virtual {p1}, Landroid/app/usage/UsageEvents$Event;->getEventType()I
int v1 = 2; // const/4 v1, 0x2
/* if-eq v0, v1, :cond_1 */
/* .line 147 */
v0 = (( android.app.usage.UsageEvents$Event ) p1 ).getEventType ( ); // invoke-virtual {p1}, Landroid/app/usage/UsageEvents$Event;->getEventType()I
/* const/16 v1, 0x17 */
/* if-eq v0, v1, :cond_1 */
/* .line 148 */
v0 = (( android.app.usage.UsageEvents$Event ) p1 ).getEventType ( ); // invoke-virtual {p1}, Landroid/app/usage/UsageEvents$Event;->getEventType()I
/* const/16 v1, 0x18 */
/* if-ne v0, v1, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 146 */
} // :goto_1
} // .end method
private Boolean valid ( android.app.usage.UsageEvents$Event p0 ) {
/* .locals 3 */
/* .param p1, "event" # Landroid/app/usage/UsageEvents$Event; */
/* .line 141 */
v0 = (( android.app.usage.UsageEvents$Event ) p1 ).getEventType ( ); // invoke-virtual {p1}, Landroid/app/usage/UsageEvents$Event;->getEventType()I
int v1 = 1; // const/4 v1, 0x1
/* if-eq v0, v1, :cond_1 */
/* .line 142 */
v0 = (( android.app.usage.UsageEvents$Event ) p1 ).getEventType ( ); // invoke-virtual {p1}, Landroid/app/usage/UsageEvents$Event;->getEventType()I
int v2 = 2; // const/4 v2, 0x2
/* if-ne v0, v2, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :cond_1
} // :goto_0
/* nop */
/* .line 141 */
} // :goto_1
} // .end method
/* # virtual methods */
public void filterUsageEventResult ( android.content.Context p0, Long p1, Long p2, android.util.ArrayMap p3, java.util.List p4 ) {
/* .locals 11 */
/* .param p1, "ctx" # Landroid/content/Context; */
/* .param p2, "start" # J */
/* .param p4, "end" # J */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/content/Context;", */
/* "JJ", */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/MiuiAppUsageStats$AppUsageStats;", */
/* ">;", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/MiuiAppUsageStats$AppUsageStats;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 74 */
/* .local p6, "result":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Lcom/android/server/MiuiAppUsageStats$AppUsageStats;>;" */
/* .local p7, "list":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/MiuiAppUsageStats$AppUsageStats;>;" */
/* move-object/from16 v0, p6 */
if ( p1 != null) { // if-eqz p1, :cond_7
/* if-nez v0, :cond_0 */
/* move-object v8, p0 */
/* move-object/from16 v9, p7 */
/* goto/16 :goto_3 */
/* .line 78 */
} // :cond_0
v1 = /* invoke-virtual/range {p6 ..p6}, Landroid/util/ArrayMap;->keySet()Ljava/util/Set; */
/* new-array v1, v1, [Ljava/lang/String; */
/* .line 79 */
/* .local v1, "keys":[Ljava/lang/String; */
/* invoke-virtual/range {p6 ..p6}, Landroid/util/ArrayMap;->keySet()Ljava/util/Set; */
/* .line 80 */
/* array-length v2, v1 */
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
/* if-ge v3, v2, :cond_6 */
/* aget-object v4, v1, v3 */
/* .line 81 */
/* .local v4, "key":Ljava/lang/String; */
(( android.util.ArrayMap ) v0 ).get ( v4 ); // invoke-virtual {v0, v4}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v5, Lcom/android/server/MiuiAppUsageStats$AppUsageStats; */
/* .line 83 */
/* .local v5, "stat":Lcom/android/server/MiuiAppUsageStats$AppUsageStats; */
/* if-nez v5, :cond_1 */
/* .line 85 */
/* move-object v8, p0 */
/* move-object/from16 v9, p7 */
/* goto/16 :goto_2 */
/* .line 89 */
} // :cond_1
(( com.android.server.MiuiAppUsageStats$AppUsageStats ) v5 ).getLastUsageTime ( ); // invoke-virtual {v5}, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->getLastUsageTime()J
/* move-result-wide v6 */
/* cmp-long v6, v6, p2 */
final String v7 = "MiuiAppUsageStats"; // const-string v7, "MiuiAppUsageStats"
/* if-ltz v6, :cond_5 */
(( com.android.server.MiuiAppUsageStats$AppUsageStats ) v5 ).getLastUsageTime ( ); // invoke-virtual {v5}, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->getLastUsageTime()J
/* move-result-wide v8 */
/* cmp-long v6, v8, p4 */
/* if-lez v6, :cond_2 */
/* move-object v8, p0 */
/* move-object/from16 v9, p7 */
/* .line 95 */
} // :cond_2
(( com.android.server.MiuiAppUsageStats$AppUsageStats ) v5 ).getPkgName ( ); // invoke-virtual {v5}, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->getPkgName()Ljava/lang/String;
/* .line 98 */
/* .local v6, "pkgName":Ljava/lang/String; */
v8 = (( com.android.server.MiuiAppUsageStats$AppUsageStats ) v5 ).isValid ( ); // invoke-virtual {v5}, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->isValid()Z
/* if-nez v8, :cond_3 */
/* .line 99 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "filterUsageEventResult()......Skip, invalid stats.pkgName="; // const-string v9, "filterUsageEventResult()......Skip, invalid stats.pkgName="
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v7,v8 );
/* .line 100 */
(( android.util.ArrayMap ) v0 ).remove ( v4 ); // invoke-virtual {v0, v4}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 101 */
/* move-object v8, p0 */
/* move-object/from16 v9, p7 */
/* .line 105 */
} // :cond_3
/* move-object v8, p0 */
v7 = /* invoke-direct {p0, v6}, Lcom/android/server/MiuiAppUsageStats;->isSpecialApp(Ljava/lang/String;)Z */
if ( v7 != null) { // if-eqz v7, :cond_4
/* .line 106 */
(( android.util.ArrayMap ) v0 ).remove ( v4 ); // invoke-virtual {v0, v4}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 110 */
/* move-object/from16 v9, p7 */
/* .line 116 */
} // :cond_4
/* move-object/from16 v9, p7 */
/* .line 89 */
} // .end local v6 # "pkgName":Ljava/lang/String;
} // :cond_5
/* move-object v8, p0 */
/* move-object/from16 v9, p7 */
/* .line 90 */
} // :goto_1
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "Wow! We filter out it again? pkgName="; // const-string v10, "Wow! We filter out it again? pkgName="
(( java.lang.StringBuilder ) v6 ).append ( v10 ); // invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.MiuiAppUsageStats$AppUsageStats ) v5 ).getPkgName ( ); // invoke-virtual {v5}, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->getPkgName()Ljava/lang/String;
(( java.lang.StringBuilder ) v6 ).append ( v10 ); // invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v7,v6 );
/* .line 91 */
(( android.util.ArrayMap ) v0 ).remove ( v4 ); // invoke-virtual {v0, v4}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 92 */
/* nop */
/* .line 80 */
} // .end local v4 # "key":Ljava/lang/String;
} // .end local v5 # "stat":Lcom/android/server/MiuiAppUsageStats$AppUsageStats;
} // :goto_2
/* add-int/lit8 v3, v3, 0x1 */
/* goto/16 :goto_0 */
/* .line 118 */
} // :cond_6
/* move-object v8, p0 */
/* move-object/from16 v9, p7 */
return;
/* .line 74 */
} // .end local v1 # "keys":[Ljava/lang/String;
} // :cond_7
/* move-object v8, p0 */
/* move-object/from16 v9, p7 */
/* .line 75 */
} // :goto_3
return;
} // .end method
public java.util.ArrayList getTop3Apps ( android.content.Context p0, Long p1, Long p2 ) {
/* .locals 16 */
/* .param p1, "ctx" # Landroid/content/Context; */
/* .param p2, "start" # J */
/* .param p4, "end" # J */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/content/Context;", */
/* "JJ)", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 50 */
/* new-instance v7, Landroid/util/ArrayMap; */
/* invoke-direct {v7}, Landroid/util/ArrayMap;-><init>()V */
/* .line 51 */
/* .local v7, "ret":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Lcom/android/server/MiuiAppUsageStats$AppUsageStats;>;" */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* move-object v15, v0 */
/* .line 52 */
/* .local v15, "list":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/MiuiAppUsageStats$AppUsageStats;>;" */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* move-object v14, v0 */
/* .line 54 */
/* .local v14, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* invoke-direct/range {p0 ..p5}, Lcom/android/server/MiuiAppUsageStats;->getEventStats(Landroid/content/Context;JJ)Landroid/app/usage/UsageEvents; */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
/* move-wide/from16 v3, p2 */
/* move-wide/from16 v5, p4 */
/* invoke-direct/range {v0 ..v7}, Lcom/android/server/MiuiAppUsageStats;->aggregateUsageStatsByEvent(Landroid/content/Context;Landroid/app/usage/UsageEvents;JJLandroid/util/ArrayMap;)V */
/* .line 55 */
/* move-object/from16 v8, p0 */
/* move-object/from16 v9, p1 */
/* move-wide/from16 v10, p2 */
/* move-wide/from16 v12, p4 */
/* move-object v0, v14 */
} // .end local v14 # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* .local v0, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* move-object v14, v7 */
/* move-object v1, v15 */
} // .end local v15 # "list":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/MiuiAppUsageStats$AppUsageStats;>;"
/* .local v1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/MiuiAppUsageStats$AppUsageStats;>;" */
/* invoke-virtual/range {v8 ..v15}, Lcom/android/server/MiuiAppUsageStats;->filterUsageEventResult(Landroid/content/Context;JJLandroid/util/ArrayMap;Ljava/util/List;)V */
/* .line 56 */
java.util.Collections .sort ( v1 );
/* .line 58 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
v3 = } // :goto_0
/* if-ge v2, v3, :cond_1 */
/* .line 59 */
int v3 = 3; // const/4 v3, 0x3
/* if-lt v2, v3, :cond_0 */
/* .line 60 */
/* .line 62 */
} // :cond_0
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* check-cast v4, Lcom/android/server/MiuiAppUsageStats$AppUsageStats; */
(( com.android.server.MiuiAppUsageStats$AppUsageStats ) v4 ).getPkgName ( ); // invoke-virtual {v4}, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->getPkgName()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "="; // const-string v4, "="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v4, Lcom/android/server/MiuiAppUsageStats$AppUsageStats; */
(( com.android.server.MiuiAppUsageStats$AppUsageStats ) v4 ).getTotalForegroundTime ( ); // invoke-virtual {v4}, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->getTotalForegroundTime()J
/* move-result-wide v4 */
/* const-wide/16 v8, 0x3e8 */
/* div-long/2addr v4, v8 */
(( java.lang.StringBuilder ) v3 ).append ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 66 */
/* .local v3, "value":Ljava/lang/String; */
(( java.util.ArrayList ) v0 ).add ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 58 */
} // .end local v3 # "value":Ljava/lang/String;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 68 */
} // .end local v2 # "i":I
} // :cond_1
} // :goto_1
} // .end method
public Boolean validStartEvent ( android.app.usage.UsageEvents$Event p0 ) {
/* .locals 2 */
/* .param p1, "event" # Landroid/app/usage/UsageEvents$Event; */
/* .line 152 */
v0 = (( android.app.usage.UsageEvents$Event ) p1 ).getEventType ( ); // invoke-virtual {p1}, Landroid/app/usage/UsageEvents$Event;->getEventType()I
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
