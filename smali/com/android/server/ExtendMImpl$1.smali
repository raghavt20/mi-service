.class Lcom/android/server/ExtendMImpl$1;
.super Landroid/content/BroadcastReceiver;
.source "ExtendMImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/ExtendMImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/ExtendMImpl;


# direct methods
.method constructor <init>(Lcom/android/server/ExtendMImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/ExtendMImpl;

    .line 946
    iput-object p1, p0, Lcom/android/server/ExtendMImpl$1;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 949
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 950
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-wide/16 v2, 0x0

    const-wide/32 v4, 0xea60

    const-string v6, "ExtM"

    if-eqz v1, :cond_4

    .line 951
    invoke-static {}, Lcom/android/server/ExtendMImpl;->-$$Nest$sfgetLOG_VERBOSE()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 952
    const-string v1, "-----------------------"

    invoke-static {v6, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 953
    const-string v1, "Screen off"

    invoke-static {v6, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 955
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    .line 957
    .local v7, "mScreenOffTime":J
    iget-object v1, p0, Lcom/android/server/ExtendMImpl$1;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {v1}, Lcom/android/server/ExtendMImpl;->-$$Nest$fgetmScreenOnTime(Lcom/android/server/ExtendMImpl;)J

    move-result-wide v9

    sub-long v9, v7, v9

    const-wide/32 v11, 0x2bf20

    cmp-long v1, v9, v11

    if-lez v1, :cond_1

    .line 958
    iget-object v1, p0, Lcom/android/server/ExtendMImpl$1;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {v1}, Lcom/android/server/ExtendMImpl;->-$$Nest$fgetmMarkDiffTime(Lcom/android/server/ExtendMImpl;)J

    move-result-wide v9

    iget-object v11, p0, Lcom/android/server/ExtendMImpl$1;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {v11}, Lcom/android/server/ExtendMImpl;->-$$Nest$fgetmMarkStartTime(Lcom/android/server/ExtendMImpl;)J

    move-result-wide v11

    sub-long v11, v7, v11

    add-long/2addr v9, v11

    invoke-static {}, Lcom/android/server/ExtendMImpl;->-$$Nest$sfgetMARK_DURATION()J

    move-result-wide v11

    rem-long/2addr v9, v11

    invoke-static {v1, v9, v10}, Lcom/android/server/ExtendMImpl;->-$$Nest$fputmMarkDiffTime(Lcom/android/server/ExtendMImpl;J)V

    .line 959
    iget-object v1, p0, Lcom/android/server/ExtendMImpl$1;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {v1}, Lcom/android/server/ExtendMImpl;->-$$Nest$fgetmFlushDiffTime(Lcom/android/server/ExtendMImpl;)J

    move-result-wide v9

    iget-object v11, p0, Lcom/android/server/ExtendMImpl$1;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {v11}, Lcom/android/server/ExtendMImpl;->-$$Nest$fgetmScreenOnTime(Lcom/android/server/ExtendMImpl;)J

    move-result-wide v11

    sub-long v11, v7, v11

    add-long/2addr v9, v11

    invoke-static {v1, v9, v10}, Lcom/android/server/ExtendMImpl;->-$$Nest$fputmFlushDiffTime(Lcom/android/server/ExtendMImpl;J)V

    .line 960
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "mark: already wait "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v9, p0, Lcom/android/server/ExtendMImpl$1;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {v9}, Lcom/android/server/ExtendMImpl;->-$$Nest$fgetmMarkDiffTime(Lcom/android/server/ExtendMImpl;)J

    move-result-wide v9

    div-long/2addr v9, v4

    invoke-virtual {v1, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v9, "m flush: already wait "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v9, p0, Lcom/android/server/ExtendMImpl$1;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {v9}, Lcom/android/server/ExtendMImpl;->-$$Nest$fgetmFlushDiffTime(Lcom/android/server/ExtendMImpl;)J

    move-result-wide v9

    div-long/2addr v9, v4

    invoke-virtual {v1, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v9, "m"

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 963
    :cond_1
    const-string v1, "Screen on--> Screen off diff < 2 minutes"

    invoke-static {v6, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 965
    :goto_0
    iget-object v1, p0, Lcom/android/server/ExtendMImpl$1;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {v1}, Lcom/android/server/ExtendMImpl;->-$$Nest$munregisterAlarmClock(Lcom/android/server/ExtendMImpl;)V

    .line 967
    iget-object v1, p0, Lcom/android/server/ExtendMImpl$1;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {v1}, Lcom/android/server/ExtendMImpl;->-$$Nest$fgetmFlushDiffTime(Lcom/android/server/ExtendMImpl;)J

    move-result-wide v9

    invoke-static {}, Lcom/android/server/ExtendMImpl;->-$$Nest$sfgetFLUSH_DURATION()J

    move-result-wide v11

    cmp-long v1, v9, v11

    if-gez v1, :cond_2

    iget-object v1, p0, Lcom/android/server/ExtendMImpl$1;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {v1}, Lcom/android/server/ExtendMImpl;->-$$Nest$fgetmCurState(Lcom/android/server/ExtendMImpl;)I

    move-result v1

    const/4 v9, 0x3

    if-ne v1, v9, :cond_3

    .line 968
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "screen on up to "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/android/server/ExtendMImpl;->-$$Nest$sfgetFLUSH_DURATION()J

    move-result-wide v9

    div-long/2addr v9, v4

    invoke-virtual {v1, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "m , try to flush."

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 970
    iget-object v1, p0, Lcom/android/server/ExtendMImpl$1;->this$0:Lcom/android/server/ExtendMImpl;

    const/4 v4, 0x1

    invoke-static {v1, v4}, Lcom/android/server/ExtendMImpl;->-$$Nest$mupdateState(Lcom/android/server/ExtendMImpl;I)V

    .line 971
    iget-object v1, p0, Lcom/android/server/ExtendMImpl$1;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {v1}, Lcom/android/server/ExtendMImpl;->-$$Nest$mtryToFlushPages(Lcom/android/server/ExtendMImpl;)Z

    .line 972
    iget-object v1, p0, Lcom/android/server/ExtendMImpl$1;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {v1, v2, v3}, Lcom/android/server/ExtendMImpl;->-$$Nest$fputmFlushDiffTime(Lcom/android/server/ExtendMImpl;J)V

    .line 974
    .end local v7    # "mScreenOffTime":J
    :cond_3
    goto/16 :goto_2

    :cond_4
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 975
    iget-object v1, p0, Lcom/android/server/ExtendMImpl$1;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {v1}, Lcom/android/server/ExtendMImpl;->-$$Nest$mtryToStopFlush(Lcom/android/server/ExtendMImpl;)V

    .line 976
    iget-object v1, p0, Lcom/android/server/ExtendMImpl$1;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/android/server/ExtendMImpl;->-$$Nest$fputmScreenOnTime(Lcom/android/server/ExtendMImpl;J)V

    .line 977
    iget-object v1, p0, Lcom/android/server/ExtendMImpl$1;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {v1}, Lcom/android/server/ExtendMImpl;->-$$Nest$mgetQuotaLeft(Lcom/android/server/ExtendMImpl;)I

    move-result v1

    if-lez v1, :cond_8

    .line 978
    invoke-static {}, Lcom/android/server/ExtendMImpl;->-$$Nest$sfgetLOG_VERBOSE()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 979
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "registerAlarmClock: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/android/server/ExtendMImpl;->-$$Nest$sfgetMARK_DURATION()J

    move-result-wide v2

    iget-object v7, p0, Lcom/android/server/ExtendMImpl$1;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {v7}, Lcom/android/server/ExtendMImpl;->-$$Nest$fgetmMarkDiffTime(Lcom/android/server/ExtendMImpl;)J

    move-result-wide v7

    sub-long/2addr v2, v7

    div-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " m"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 982
    :cond_5
    iget-object v1, p0, Lcom/android/server/ExtendMImpl$1;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {}, Lcom/android/server/ExtendMImpl;->-$$Nest$sfgetMARK_DURATION()J

    move-result-wide v4

    iget-object v6, p0, Lcom/android/server/ExtendMImpl$1;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {v6}, Lcom/android/server/ExtendMImpl;->-$$Nest$fgetmMarkDiffTime(Lcom/android/server/ExtendMImpl;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    add-long/2addr v2, v4

    invoke-static {v1, v2, v3}, Lcom/android/server/ExtendMImpl;->-$$Nest$mregisterAlarmClock(Lcom/android/server/ExtendMImpl;J)V

    goto/16 :goto_2

    .line 984
    :cond_6
    const-string v1, "miui.extm.action.mark"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 986
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mark duration up to "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/android/server/ExtendMImpl;->-$$Nest$sfgetMARK_DURATION()J

    move-result-wide v7

    div-long/2addr v7, v4

    invoke-virtual {v1, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "m , try to mark."

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 988
    const-string v1, "Start Mark"

    const-string v4, "MFZ"

    invoke-static {v4, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 989
    iget-object v1, p0, Lcom/android/server/ExtendMImpl$1;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {v1}, Lcom/android/server/ExtendMImpl;->-$$Nest$fgetmHandler(Lcom/android/server/ExtendMImpl;)Lcom/android/server/ExtendMImpl$MyHandler;

    move-result-object v1

    const/4 v5, 0x6

    invoke-virtual {v1, v5}, Lcom/android/server/ExtendMImpl$MyHandler;->sendEmptyMessage(I)Z

    .line 991
    :try_start_0
    new-instance v1, Ljava/io/File;

    const-string v5, "/sys/block/zram0/idle_stat"

    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v5, ""

    .line 992
    const/16 v7, 0x80

    invoke-static {v1, v7, v5}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 993
    .local v1, "idle_stats":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Mark Finshed ! idle_stats: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 996
    nop

    .end local v1    # "idle_stats":Ljava/lang/String;
    goto :goto_1

    .line 994
    :catch_0
    move-exception v1

    .line 995
    .local v1, "e":Ljava/io/IOException;
    const-string v5, "idle_stats: error"

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 997
    .end local v1    # "e":Ljava/io/IOException;
    :goto_1
    iget-object v1, p0, Lcom/android/server/ExtendMImpl$1;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {v1, v2, v3}, Lcom/android/server/ExtendMImpl;->-$$Nest$fputmMarkDiffTime(Lcom/android/server/ExtendMImpl;J)V

    .line 998
    iget-object v1, p0, Lcom/android/server/ExtendMImpl$1;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/android/server/ExtendMImpl;->-$$Nest$fputmMarkStartTime(Lcom/android/server/ExtendMImpl;J)V

    .line 999
    invoke-static {}, Lcom/android/server/ExtendMImpl;->-$$Nest$sfgetLOG_VERBOSE()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1000
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "yyyy-MM-dd HH:mm:ss"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1001
    .local v1, "mFmt":Ljava/text/SimpleDateFormat;
    new-instance v2, Ljava/util/Date;

    iget-object v3, p0, Lcom/android/server/ExtendMImpl$1;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {v3}, Lcom/android/server/ExtendMImpl;->-$$Nest$fgetmMarkStartTime(Lcom/android/server/ExtendMImpl;)J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 1002
    .local v2, "date":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mMarkStartTime: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,reset it"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1004
    .end local v1    # "mFmt":Ljava/text/SimpleDateFormat;
    .end local v2    # "date":Ljava/lang/String;
    :cond_7
    iget-object v1, p0, Lcom/android/server/ExtendMImpl$1;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {v1}, Lcom/android/server/ExtendMImpl;->-$$Nest$mgetQuotaLeft(Lcom/android/server/ExtendMImpl;)I

    move-result v1

    if-lez v1, :cond_8

    .line 1005
    iget-object v1, p0, Lcom/android/server/ExtendMImpl$1;->this$0:Lcom/android/server/ExtendMImpl;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {}, Lcom/android/server/ExtendMImpl;->-$$Nest$sfgetMARK_DURATION()J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-static {v1, v2, v3}, Lcom/android/server/ExtendMImpl;->-$$Nest$mregisterAlarmClock(Lcom/android/server/ExtendMImpl;J)V

    .line 1008
    :cond_8
    :goto_2
    return-void
.end method
