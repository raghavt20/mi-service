.class Lcom/android/server/ForceDarkAppListManager$1;
.super Ljava/lang/Object;
.source "ForceDarkAppListManager.java"

# interfaces
.implements Lcom/android/server/ForceDarkAppListProvider$ForceDarkAppListChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/ForceDarkAppListManager;-><init>(Landroid/content/Context;Lcom/android/server/ForceDarkUiModeModeManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/ForceDarkAppListManager;


# direct methods
.method constructor <init>(Lcom/android/server/ForceDarkAppListManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/ForceDarkAppListManager;

    .line 57
    iput-object p1, p0, Lcom/android/server/ForceDarkAppListManager$1;->this$0:Lcom/android/server/ForceDarkAppListManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChange()V
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/android/server/ForceDarkAppListManager$1;->this$0:Lcom/android/server/ForceDarkAppListManager;

    invoke-static {v0}, Lcom/android/server/ForceDarkAppListManager;->-$$Nest$fgetmForceDarkAppListProvider(Lcom/android/server/ForceDarkAppListManager;)Lcom/android/server/ForceDarkAppListProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/ForceDarkAppListProvider;->getForceDarkAppSettings()Ljava/util/HashMap;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ForceDarkAppListManager;->-$$Nest$fputmForceDarkAppSettings(Lcom/android/server/ForceDarkAppListManager;Ljava/util/HashMap;)V

    .line 61
    iget-object v0, p0, Lcom/android/server/ForceDarkAppListManager$1;->this$0:Lcom/android/server/ForceDarkAppListManager;

    invoke-static {v0}, Lcom/android/server/ForceDarkAppListManager;->-$$Nest$fgetmForceDarkUiModeModeManager(Lcom/android/server/ForceDarkAppListManager;)Lcom/android/server/ForceDarkUiModeModeManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/ForceDarkUiModeModeManager;->notifyAppListChanged()V

    .line 62
    iget-object v0, p0, Lcom/android/server/ForceDarkAppListManager$1;->this$0:Lcom/android/server/ForceDarkAppListManager;

    invoke-static {v0}, Lcom/android/server/ForceDarkAppListManager;->-$$Nest$mclearCache(Lcom/android/server/ForceDarkAppListManager;)V

    .line 63
    return-void
.end method
