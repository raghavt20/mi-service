.class public Lcom/android/server/ConsumerIrServiceStubImpl;
.super Lcom/android/server/ConsumerIrServiceStub;
.source "ConsumerIrServiceStubImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.ConsumerIrServiceStub$$"
.end annotation


# static fields
.field private static final DEVICE_REGION:Ljava/lang/String;

.field private static final EVENT_APP_ID:Ljava/lang/String; = "31000000091"

.field private static final EVENT_NAME:Ljava/lang/String; = "ir"

.field private static final FLAG_NON_ANONYMOUS:I = 0x2

.field private static final KEY_EVENT_TYPE:Ljava/lang/String; = "ir_status"

.field private static final ONE_TRACK_ACTION:Ljava/lang/String; = "onetrack.action.TRACK_EVENT"

.field private static final TAG:Ljava/lang/String; = "ConsumerIrService"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public static synthetic $r8$lambda$rIS-hcid3WK0naZPYhQszbhFe_U(Lcom/android/server/ConsumerIrServiceStubImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/ConsumerIrServiceStubImpl;->lambda$reportIrToOneTrack$0()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 29
    const-string v0, "ro.miui.region"

    const-string v1, "CN"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ConsumerIrServiceStubImpl;->DEVICE_REGION:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/android/server/ConsumerIrServiceStub;-><init>()V

    return-void
.end method

.method private synthetic lambda$reportIrToOneTrack$0()V
    .locals 5

    .line 39
    const-string v0, "ConsumerIrService"

    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "onetrack.action.TRACK_EVENT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 40
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "com.miui.analytics"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 41
    const-string v2, "APP_ID"

    const-string v3, "31000000091"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 42
    const-string v2, "EVENT_NAME"

    const-string v3, "ir"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 43
    const-string v2, "PACKAGE"

    iget-object v3, p0, Lcom/android/server/ConsumerIrServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 44
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 45
    .local v2, "params":Landroid/os/Bundle;
    sget-object v3, Lcom/android/server/ConsumerIrServiceStubImpl;->DEVICE_REGION:Ljava/lang/String;

    const-string v4, "CN"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 46
    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 48
    :cond_0
    const-string v3, "ir_status"

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 49
    invoke-virtual {v1, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 50
    iget-object v3, p0, Lcom/android/server/ConsumerIrServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    nop

    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "params":Landroid/os/Bundle;
    goto :goto_0

    .line 51
    :catch_0
    move-exception v1

    .line 52
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error reportIrToOneTrack:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    const-string v1, "reportIrToOneTrack"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    return-void
.end method


# virtual methods
.method public reportIrToOneTrack(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 33
    sget-object v0, Lcom/android/server/ConsumerIrServiceStubImpl;->DEVICE_REGION:Ljava/lang/String;

    const-string v1, "IN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    return-void

    .line 36
    :cond_0
    iput-object p1, p0, Lcom/android/server/ConsumerIrServiceStubImpl;->mContext:Landroid/content/Context;

    .line 37
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/ConsumerIrServiceStubImpl$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/ConsumerIrServiceStubImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/ConsumerIrServiceStubImpl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 56
    return-void
.end method
