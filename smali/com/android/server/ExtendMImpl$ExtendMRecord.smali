.class Lcom/android/server/ExtendMImpl$ExtendMRecord;
.super Ljava/lang/Object;
.source "ExtendMImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/ExtendMImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ExtendMRecord"
.end annotation


# static fields
.field private static final FILE_PATH:Ljava/lang/String; = "/data/mqsas/extm/dailyrecord.txt"


# instance fields
.field private MAX_DAILY_RECORD_COUNT:I

.field private MAX_RECORD_COUNT:I

.field public dailyRecords:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDailyRecordData:Ljava/lang/String;

.field private mFlushCount:I

.field private mMarkCount:I

.field private mReadBackRateTotal:D

.field private mRecordData:Ljava/lang/String;

.field private mTimeStamp:Ljava/lang/String;

.field public records:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 2

    .line 1372
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1373
    const/16 v0, 0x12c

    iput v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->MAX_RECORD_COUNT:I

    .line 1374
    const/4 v0, 0x7

    iput v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->MAX_DAILY_RECORD_COUNT:I

    .line 1378
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mMarkCount:I

    .line 1379
    iput v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mFlushCount:I

    .line 1380
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mReadBackRateTotal:D

    .line 1382
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->records:Ljava/util/LinkedList;

    .line 1384
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->dailyRecords:Ljava/util/LinkedList;

    return-void
.end method


# virtual methods
.method public getDailyRecords()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1392
    iget-object v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->dailyRecords:Ljava/util/LinkedList;

    return-object v0
.end method

.method public getSingleRecord()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1388
    iget-object v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->records:Ljava/util/LinkedList;

    return-object v0
.end method

.method public setDailyRecord(JJ)V
    .locals 6
    .param p1, "totalMarked"    # J
    .param p3, "totalFlushed"    # J

    .line 1408
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1409
    .local v0, "calendar":Ljava/util/Calendar;
    const/4 v1, 0x5

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 1410
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    .line 1411
    .local v1, "date":Ljava/util/Date;
    iget-wide v2, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mReadBackRateTotal:D

    iget v4, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mFlushCount:I

    int-to-double v4, v4

    div-double/2addr v2, v4

    double-to-int v2, v2

    .line 1412
    .local v2, "avgRBRate":I
    const/4 v3, 0x0

    .line 1415
    .local v3, "obj":Lorg/json/JSONObject;
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    move-object v3, v4

    .line 1416
    const-string v4, "date"

    invoke-virtual {v3, v4, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1417
    const-string v4, "markCount"

    iget v5, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mMarkCount:I

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1418
    const-string v4, "markTotal"

    invoke-virtual {v3, v4, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 1419
    const-string v4, "flushCount"

    iget v5, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mFlushCount:I

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1420
    const-string v4, "flushTotal"

    invoke-virtual {v3, v4, p3, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 1421
    const-string v4, "readBackRate"

    invoke-virtual {v3, v4, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1424
    goto :goto_0

    .line 1422
    :catch_0
    move-exception v4

    .line 1423
    .local v4, "ignored":Lorg/json/JSONException;
    invoke-virtual {v4}, Lorg/json/JSONException;->printStackTrace()V

    .line 1425
    .end local v4    # "ignored":Lorg/json/JSONException;
    :goto_0
    if-nez v3, :cond_0

    return-void

    .line 1426
    :cond_0
    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mDailyRecordData:Ljava/lang/String;

    .line 1427
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mDailyRecordData:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "/data/mqsas/extm/dailyrecord.txt"

    invoke-virtual {p0, v4, v5}, Lcom/android/server/ExtendMImpl$ExtendMRecord;->writeToFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 1428
    invoke-static {}, Lcom/android/server/ExtendMImpl;->-$$Nest$sfgetLOG_VERBOSE()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1429
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "daily record data : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mDailyRecordData:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ExtM"

    invoke-static {v5, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1431
    :cond_1
    iget-object v4, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mDailyRecordData:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lcom/android/server/ExtendMImpl$ExtendMRecord;->updateDailyRecord(Ljava/lang/String;)V

    .line 1432
    return-void
.end method

.method public setFlushRecord(IILjava/lang/String;J)V
    .locals 4
    .param p1, "writeCount"    # I
    .param p2, "writeTotal"    # I
    .param p3, "readBackRate"    # Ljava/lang/String;
    .param p4, "costTime"    # J

    .line 1396
    iget v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mFlushCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mFlushCount:I

    .line 1397
    iget-wide v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mReadBackRateTotal:D

    invoke-static {p3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    add-double/2addr v0, v2

    iput-wide v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mReadBackRateTotal:D

    .line 1398
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Flushed finished: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " m) flushed, and read back rate is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " , flush costs "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1401
    .local v0, "recordData":Ljava/lang/String;
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "yyyy/MM/dd HH:mm:ss"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mTimeStamp:Ljava/lang/String;

    .line 1402
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mTimeStamp:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mRecordData:Ljava/lang/String;

    .line 1403
    invoke-virtual {p0, v1}, Lcom/android/server/ExtendMImpl$ExtendMRecord;->updateSingleRecord(Ljava/lang/String;)V

    .line 1404
    return-void
.end method

.method public updateDailyRecord(Ljava/lang/String;)V
    .locals 2
    .param p1, "dailyRecord"    # Ljava/lang/String;

    .line 1444
    iget-object v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->dailyRecords:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    iget v1, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->MAX_DAILY_RECORD_COUNT:I

    if-ge v0, v1, :cond_0

    .line 1445
    iget-object v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->dailyRecords:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    goto :goto_0

    .line 1447
    :cond_0
    iget-object v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->dailyRecords:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    .line 1448
    iget-object v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->dailyRecords:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 1451
    :goto_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mMarkCount:I

    .line 1452
    iput v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mFlushCount:I

    .line 1453
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mReadBackRateTotal:D

    .line 1454
    return-void
.end method

.method public updateSingleRecord(Ljava/lang/String;)V
    .locals 2
    .param p1, "recordData"    # Ljava/lang/String;

    .line 1435
    iget-object v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->records:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    iget v1, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->MAX_RECORD_COUNT:I

    if-ge v0, v1, :cond_0

    .line 1436
    iget-object v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->records:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    goto :goto_0

    .line 1438
    :cond_0
    iget-object v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->records:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    .line 1439
    iget-object v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->records:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 1441
    :goto_0
    return-void
.end method

.method public writeToFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "line"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;

    .line 1457
    const-string v0, "ExtM"

    const-string v1, " write daily extm usage to /data/mqsas/extm/dailyrecord.txt"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1458
    const/4 v0, 0x0

    .line 1459
    .local v0, "fos":Ljava/io/FileOutputStream;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1460
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1462
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->mkdir()Z

    .line 1463
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1466
    goto :goto_0

    .line 1464
    :catch_0
    move-exception v2

    .line 1465
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 1469
    .end local v2    # "e":Ljava/io/IOException;
    :cond_0
    :goto_0
    :try_start_1
    new-instance v2, Ljava/io/FileOutputStream;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    move-object v0, v2

    .line 1470
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    .line 1471
    .local v2, "bytes":[B
    invoke-virtual {v0, v2}, Ljava/io/FileOutputStream;->write([B)V

    .line 1472
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1476
    .end local v2    # "bytes":[B
    nop

    .line 1478
    :try_start_2
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 1481
    :goto_1
    goto :goto_2

    .line 1479
    :catch_1
    move-exception v2

    .line 1480
    .local v2, "e2":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .end local v2    # "e2":Ljava/lang/Exception;
    goto :goto_1

    .line 1476
    :catchall_0
    move-exception v2

    goto :goto_3

    .line 1473
    :catch_2
    move-exception v2

    .line 1474
    .local v2, "e1":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1476
    .end local v2    # "e1":Ljava/lang/Exception;
    if-eqz v0, :cond_1

    .line 1478
    :try_start_4
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 1484
    :cond_1
    :goto_2
    return-void

    .line 1476
    :goto_3
    if-eqz v0, :cond_2

    .line 1478
    :try_start_5
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 1481
    goto :goto_4

    .line 1479
    :catch_3
    move-exception v3

    .line 1480
    .local v3, "e2":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 1483
    .end local v3    # "e2":Ljava/lang/Exception;
    :cond_2
    :goto_4
    throw v2
.end method
