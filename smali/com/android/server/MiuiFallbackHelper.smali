.class public Lcom/android/server/MiuiFallbackHelper;
.super Lcom/android/server/MiuiFallbackHelperStub;
.source "MiuiFallbackHelper.java"


# static fields
.field private static final FALLBACK_FILE_SUFFIX:Ljava/lang/String; = ".fallback"

.field private static final FALLBACK_TEMP_FILE_SUFFIX:Ljava/lang/String; = ".fallbacktemp"

.field private static final TAG:Ljava/lang/String;

.field private static final XATTR_MD5:Ljava/lang/String; = "user.md5"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    const-class v0, Lcom/android/server/MiuiFallbackHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/MiuiFallbackHelper;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/android/server/MiuiFallbackHelperStub;-><init>()V

    return-void
.end method

.method private calculateMD5(Ljava/io/File;Z)[B
    .locals 8
    .param p1, "file"    # Ljava/io/File;
    .param p2, "setXattr"    # Z

    .line 129
    const/4 v0, 0x0

    .line 130
    .local v0, "md5sum":[B
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Landroid/system/ErrnoException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    .local v1, "fis":Ljava/io/FileInputStream;
    :try_start_1
    const-string v2, "MD5"

    invoke-static {v2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v2

    .line 133
    .local v2, "digest":Ljava/security/MessageDigest;
    const/16 v3, 0x2000

    new-array v3, v3, [B

    .line 134
    .local v3, "buffer":[B
    const/4 v4, 0x0

    .line 135
    .local v4, "read":I
    :goto_0
    invoke-virtual {v1, v3}, Ljava/io/FileInputStream;->read([B)I

    move-result v5

    move v4, v5

    const/4 v6, 0x0

    if-lez v5, :cond_0

    .line 136
    invoke-virtual {v2, v3, v6, v4}, Ljava/security/MessageDigest;->update([BII)V

    goto :goto_0

    .line 138
    :cond_0
    invoke-virtual {v2}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v5

    move-object v0, v5

    .line 139
    if-eqz p2, :cond_1

    .line 140
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v7, "user.md5"

    invoke-static {v5, v7, v0, v6}, Landroid/system/Os;->setxattr(Ljava/lang/String;Ljava/lang/String;[BI)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 142
    .end local v2    # "digest":Ljava/security/MessageDigest;
    .end local v3    # "buffer":[B
    .end local v4    # "read":I
    :cond_1
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Landroid/system/ErrnoException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 130
    :catchall_0
    move-exception v2

    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v3

    :try_start_4
    invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "md5sum":[B
    .end local p0    # "this":Lcom/android/server/MiuiFallbackHelper;
    .end local p1    # "file":Ljava/io/File;
    .end local p2    # "setXattr":Z
    :goto_1
    throw v2
    :try_end_4
    .catch Landroid/system/ErrnoException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_4 .. :try_end_4} :catch_0

    .line 146
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .restart local v0    # "md5sum":[B
    .restart local p0    # "this":Lcom/android/server/MiuiFallbackHelper;
    .restart local p1    # "file":Ljava/io/File;
    .restart local p2    # "setXattr":Z
    :catch_0
    move-exception v1

    .line 147
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    sget-object v2, Lcom/android/server/MiuiFallbackHelper;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "calculateMD5 NoSuchAlgorithmException: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 144
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v1

    .line 145
    .local v1, "e":Ljava/io/IOException;
    sget-object v2, Lcom/android/server/MiuiFallbackHelper;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "calculateMD5 IOException: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .end local v1    # "e":Ljava/io/IOException;
    goto :goto_2

    .line 142
    :catch_2
    move-exception v1

    .line 143
    .local v1, "e":Landroid/system/ErrnoException;
    sget-object v2, Lcom/android/server/MiuiFallbackHelper;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "calculateMD5 ErrnoException: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    .end local v1    # "e":Landroid/system/ErrnoException;
    :goto_2
    nop

    .line 150
    :goto_3
    return-object v0
.end method

.method private checkNeedCopy(Ljava/lang/String;J)Z
    .locals 7
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "periodTime"    # J

    .line 119
    invoke-direct {p0, p1}, Lcom/android/server/MiuiFallbackHelper;->getFallbackFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 120
    .local v0, "fallbackFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    .line 121
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v5

    sub-long/2addr v3, v5

    cmp-long v1, v3, p2

    if-lez v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return v2

    .line 124
    :cond_1
    return v2
.end method

.method private getFallbackFile(Ljava/lang/String;)Ljava/io/File;
    .locals 3
    .param p1, "filePath"    # Ljava/lang/String;

    .line 154
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".fallback"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private getFallbackTempFile(Ljava/lang/String;)Ljava/io/File;
    .locals 3
    .param p1, "filePath"    # Ljava/lang/String;

    .line 158
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".fallbacktemp"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private reportFileCorruptionForMqs(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "timeStamp"    # Ljava/lang/String;

    .line 162
    new-instance v0, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;

    invoke-direct {v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;-><init>()V

    .line 163
    .local v0, "event":Lmiui/mqsas/sdk/event/GeneralExceptionEvent;
    const/16 v1, 0x1b7

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setType(I)V

    .line 164
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setSystem(Z)V

    .line 165
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setTimeStamp(J)V

    .line 166
    invoke-virtual {v0, p1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setSummary(Ljava/lang/String;)V

    .line 167
    const-string v2, "android"

    invoke-virtual {v0, v2}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setPackageName(Ljava/lang/String;)V

    .line 168
    invoke-virtual {v0, p2}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setDetails(Ljava/lang/String;)V

    .line 169
    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setUpload(Z)V

    .line 170
    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setEnsureReport(Z)V

    .line 171
    invoke-static {}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->getInstance()Lmiui/mqsas/sdk/MQSEventManagerDelegate;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportGeneralException(Lmiui/mqsas/sdk/event/GeneralExceptionEvent;)Z

    .line 172
    return-void
.end method


# virtual methods
.method public restoreFile(Ljava/lang/String;)Z
    .locals 10
    .param p1, "filePath"    # Ljava/lang/String;

    .line 78
    const-string v0, "restoreFile: "

    const/4 v1, 0x0

    .line 79
    .local v1, "result":Z
    invoke-direct {p0, p1}, Lcom/android/server/MiuiFallbackHelper;->getFallbackFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 80
    .local v2, "fallbackFile":Ljava/io/File;
    invoke-direct {p0, p1}, Lcom/android/server/MiuiFallbackHelper;->getFallbackTempFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    .line 81
    .local v3, "fallbackTempFile":Ljava/io/File;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 82
    .local v4, "originalFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    const/4 v6, 0x0

    if-eqz v5, :cond_1

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 83
    invoke-virtual {v3, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 84
    sget-object v5, Lcom/android/server/MiuiFallbackHelper;->TAG:Ljava/lang/String;

    const-string v7, "fallback file maybe saved abnormally last time. trust temp file"

    invoke-static {v5, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 86
    :cond_0
    sget-object v0, Lcom/android/server/MiuiFallbackHelper;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "fallback temp file rename failed: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 87
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 86
    invoke-static {v0, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    return v6

    .line 91
    :cond_1
    :goto_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 92
    invoke-direct {p0, v2, v6}, Lcom/android/server/MiuiFallbackHelper;->calculateMD5(Ljava/io/File;Z)[B

    move-result-object v5

    .line 93
    .local v5, "md5sum":[B
    const/4 v6, 0x0

    .line 95
    .local v6, "xattr":[B
    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "user.md5"

    invoke-static {v7, v8}, Landroid/system/Os;->getxattr(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v7
    :try_end_0
    .catch Landroid/system/ErrnoException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v6, v7

    .line 98
    goto :goto_1

    .line 96
    :catch_0
    move-exception v7

    .line 97
    .local v7, "e":Landroid/system/ErrnoException;
    sget-object v8, Lcom/android/server/MiuiFallbackHelper;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    .end local v7    # "e":Landroid/system/ErrnoException;
    :goto_1
    if-eqz v5, :cond_3

    invoke-static {v5, v6}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " exists but is corrupted. Fallback file will be used. lastModified: "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 102
    invoke-virtual {v2}, Ljava/io/File;->lastModified()J

    move-result-wide v7

    invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 103
    .local v0, "msg":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/io/File;->lastModified()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    .line 104
    .local v7, "timeStamp":Ljava/lang/String;
    const/4 v8, 0x3

    invoke-static {v8, v0}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    .line 105
    invoke-virtual {v2, v4}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 106
    const/4 v1, 0x1

    .line 107
    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v8

    .line 108
    .local v8, "restoreFilePath":Ljava/lang/String;
    invoke-direct {p0, v8, v7}, Lcom/android/server/MiuiFallbackHelper;->reportFileCorruptionForMqs(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    .end local v0    # "msg":Ljava/lang/String;
    .end local v7    # "timeStamp":Ljava/lang/String;
    .end local v8    # "restoreFilePath":Ljava/lang/String;
    :cond_2
    goto :goto_2

    .line 111
    :cond_3
    sget-object v7, Lcom/android/server/MiuiFallbackHelper;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, ". but MD5 is not equal!"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    .end local v5    # "md5sum":[B
    .end local v6    # "xattr":[B
    :cond_4
    :goto_2
    return v1
.end method

.method public snapshotFile(Ljava/lang/String;J)V
    .locals 10
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "periodTime"    # J

    .line 34
    const-wide/16 v0, 0x0

    cmp-long v2, p2, v0

    if-ltz v2, :cond_5

    .line 37
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 38
    .local v2, "originalFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_4

    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/MiuiFallbackHelper;->checkNeedCopy(Ljava/lang/String;J)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 39
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 40
    .local v0, "startTime":J
    invoke-direct {p0, p1}, Lcom/android/server/MiuiFallbackHelper;->getFallbackFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    .line 41
    .local v3, "fallbackFile":Ljava/io/File;
    invoke-direct {p0, p1}, Lcom/android/server/MiuiFallbackHelper;->getFallbackTempFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    .line 43
    .local v4, "fallbackTempFile":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 44
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 45
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 46
    sget-object v5, Lcom/android/server/MiuiFallbackHelper;->TAG:Ljava/lang/String;

    const-string v6, "Preserving older fallback file backup"

    invoke-static {v5, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 47
    :cond_1
    invoke-virtual {v3, v4}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 48
    sget-object v5, Lcom/android/server/MiuiFallbackHelper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unable to backup "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    return-void

    .line 53
    :cond_2
    :goto_0
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    .local v5, "fis":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 55
    .local v6, "fos":Ljava/io/FileOutputStream;
    :try_start_2
    invoke-static {v5, v6}, Landroid/os/FileUtils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    .line 56
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->flush()V

    .line 57
    const/4 v7, 0x1

    invoke-direct {p0, v3, v7}, Lcom/android/server/MiuiFallbackHelper;->calculateMD5(Ljava/io/File;Z)[B

    .line 58
    invoke-static {v6}, Landroid/os/FileUtils;->sync(Ljava/io/FileOutputStream;)Z

    .line 60
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 61
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 65
    :cond_3
    invoke-virtual {v3}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x120

    const/4 v9, -0x1

    invoke-static {v7, v8, v9, v9}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 67
    :try_start_3
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .end local v6    # "fos":Ljava/io/FileOutputStream;
    :try_start_4
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 69
    .end local v5    # "fis":Ljava/io/FileInputStream;
    goto :goto_3

    .line 53
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    :catchall_0
    move-exception v7

    :try_start_5
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v8

    :try_start_6
    invoke-virtual {v7, v8}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "startTime":J
    .end local v2    # "originalFile":Ljava/io/File;
    .end local v3    # "fallbackFile":Ljava/io/File;
    .end local v4    # "fallbackTempFile":Ljava/io/File;
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .end local p0    # "this":Lcom/android/server/MiuiFallbackHelper;
    .end local p1    # "filePath":Ljava/lang/String;
    .end local p2    # "periodTime":J
    :goto_1
    throw v7
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v0    # "startTime":J
    .restart local v2    # "originalFile":Ljava/io/File;
    .restart local v3    # "fallbackFile":Ljava/io/File;
    .restart local v4    # "fallbackTempFile":Ljava/io/File;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    .restart local p0    # "this":Lcom/android/server/MiuiFallbackHelper;
    .restart local p1    # "filePath":Ljava/lang/String;
    .restart local p2    # "periodTime":J
    :catchall_2
    move-exception v6

    :try_start_7
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    goto :goto_2

    :catchall_3
    move-exception v7

    :try_start_8
    invoke-virtual {v6, v7}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "startTime":J
    .end local v2    # "originalFile":Ljava/io/File;
    .end local v3    # "fallbackFile":Ljava/io/File;
    .end local v4    # "fallbackTempFile":Ljava/io/File;
    .end local p0    # "this":Lcom/android/server/MiuiFallbackHelper;
    .end local p1    # "filePath":Ljava/lang/String;
    .end local p2    # "periodTime":J
    :goto_2
    throw v6
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0

    .line 67
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v0    # "startTime":J
    .restart local v2    # "originalFile":Ljava/io/File;
    .restart local v3    # "fallbackFile":Ljava/io/File;
    .restart local v4    # "fallbackTempFile":Ljava/io/File;
    .restart local p0    # "this":Lcom/android/server/MiuiFallbackHelper;
    .restart local p1    # "filePath":Ljava/lang/String;
    .restart local p2    # "periodTime":J
    :catch_0
    move-exception v5

    .line 68
    .local v5, "e":Ljava/io/IOException;
    sget-object v6, Lcom/android/server/MiuiFallbackHelper;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to write fallback file for: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    .end local v5    # "e":Ljava/io/IOException;
    :goto_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "snapshotFile: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 72
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v0

    .line 71
    invoke-static {v5, v6, v7}, Lcom/android/internal/logging/EventLogTags;->writeCommitSysConfigFile(Ljava/lang/String;J)V

    .line 74
    .end local v0    # "startTime":J
    .end local v3    # "fallbackFile":Ljava/io/File;
    .end local v4    # "fallbackTempFile":Ljava/io/File;
    :cond_4
    return-void

    .line 35
    .end local v2    # "originalFile":Ljava/io/File;
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "snapshotFile periodTime must >= 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
