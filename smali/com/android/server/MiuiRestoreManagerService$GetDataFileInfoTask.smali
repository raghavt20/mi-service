.class Lcom/android/server/MiuiRestoreManagerService$GetDataFileInfoTask;
.super Ljava/lang/Object;
.source "MiuiRestoreManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MiuiRestoreManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetDataFileInfoTask"
.end annotation


# instance fields
.field final callback:Lmiui/app/backup/IGetFileInfoCallback;

.field final path:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/server/MiuiRestoreManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/MiuiRestoreManagerService;Ljava/lang/String;Lmiui/app/backup/IGetFileInfoCallback;)V
    .locals 0
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "callback"    # Lmiui/app/backup/IGetFileInfoCallback;

    .line 585
    iput-object p1, p0, Lcom/android/server/MiuiRestoreManagerService$GetDataFileInfoTask;->this$0:Lcom/android/server/MiuiRestoreManagerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 586
    iput-object p2, p0, Lcom/android/server/MiuiRestoreManagerService$GetDataFileInfoTask;->path:Ljava/lang/String;

    .line 587
    iput-object p3, p0, Lcom/android/server/MiuiRestoreManagerService$GetDataFileInfoTask;->callback:Lmiui/app/backup/IGetFileInfoCallback;

    .line 588
    return-void
.end method

.method private hex2Base64(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "hex"    # Ljava/lang/String;

    .line 625
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 626
    const-string v0, ""

    return-object v0

    .line 628
    :cond_0
    invoke-static {}, Ljava/util/HexFormat;->of()Ljava/util/HexFormat;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/HexFormat;->parseHex(Ljava/lang/CharSequence;)[B

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 15

    .line 593
    const-string v0, "MiuiRestoreManagerService"

    const/4 v1, 0x0

    .line 595
    .local v1, "info":Lmiui/app/backup/BackupFileInfo;
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 596
    .local v2, "infoList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/android/server/MiuiRestoreManagerService$GetDataFileInfoTask;->this$0:Lcom/android/server/MiuiRestoreManagerService;

    invoke-static {v3}, Lcom/android/server/MiuiRestoreManagerService;->-$$Nest$fgetmInstaller(Lcom/android/server/MiuiRestoreManagerService;)Lcom/android/server/pm/Installer;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/MiuiRestoreManagerService$GetDataFileInfoTask;->path:Ljava/lang/String;

    invoke-virtual {v3, v4, v2}, Lcom/android/server/pm/Installer;->getDataFileInfo(Ljava/lang/String;Ljava/util/List;)I

    move-result v3

    .line 597
    .local v3, "errorCode":I
    if-nez v3, :cond_1

    .line 598
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    const/4 v5, 0x1

    if-eqz v4, :cond_0

    .line 599
    new-instance v4, Lmiui/app/backup/BackupFileInfo;

    iget-object v6, p0, Lcom/android/server/MiuiRestoreManagerService$GetDataFileInfoTask;->path:Ljava/lang/String;

    invoke-direct {v4, v6, v5}, Lmiui/app/backup/BackupFileInfo;-><init>(Ljava/lang/String;Z)V

    move-object v1, v4

    goto :goto_0

    .line 601
    :cond_0
    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 602
    .local v4, "md5Hex":Ljava/lang/String;
    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 603
    .local v5, "sha1Hex":Ljava/lang/String;
    const/4 v6, 0x2

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 604
    .local v6, "size":Ljava/lang/String;
    new-instance v14, Lmiui/app/backup/BackupFileInfo;

    iget-object v8, p0, Lcom/android/server/MiuiRestoreManagerService$GetDataFileInfoTask;->path:Ljava/lang/String;

    const/4 v9, 0x0

    .line 607
    invoke-direct {p0, v4}, Lcom/android/server/MiuiRestoreManagerService$GetDataFileInfoTask;->hex2Base64(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 608
    invoke-direct {p0, v5}, Lcom/android/server/MiuiRestoreManagerService$GetDataFileInfoTask;->hex2Base64(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 609
    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    move-object v7, v14

    invoke-direct/range {v7 .. v13}, Lmiui/app/backup/BackupFileInfo;-><init>(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;J)V
    :try_end_0
    .catch Lcom/android/server/pm/Installer$InstallerException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v14

    .line 615
    .end local v2    # "infoList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v4    # "md5Hex":Ljava/lang/String;
    .end local v5    # "sha1Hex":Ljava/lang/String;
    .end local v6    # "size":Ljava/lang/String;
    :cond_1
    :goto_0
    goto :goto_1

    .line 612
    .end local v3    # "errorCode":I
    :catch_0
    move-exception v2

    .line 613
    .local v2, "e":Lcom/android/server/pm/Installer$InstallerException;
    const/4 v3, -0x1

    .line 614
    .restart local v3    # "errorCode":I
    const-string v4, "get data file info error "

    invoke-static {v0, v4, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 618
    .end local v2    # "e":Lcom/android/server/pm/Installer$InstallerException;
    :goto_1
    :try_start_1
    iget-object v2, p0, Lcom/android/server/MiuiRestoreManagerService$GetDataFileInfoTask;->callback:Lmiui/app/backup/IGetFileInfoCallback;

    iget-object v4, p0, Lcom/android/server/MiuiRestoreManagerService$GetDataFileInfoTask;->path:Ljava/lang/String;

    invoke-interface {v2, v3, v4, v1}, Lmiui/app/backup/IGetFileInfoCallback;->onGetDataFileInfoEnd(ILjava/lang/String;Lmiui/app/backup/BackupFileInfo;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 621
    goto :goto_2

    .line 619
    :catch_1
    move-exception v2

    .line 620
    .local v2, "e":Landroid/os/RemoteException;
    const-string v4, "error when notify onGetDataFileInfoEnd "

    invoke-static {v0, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 622
    .end local v2    # "e":Landroid/os/RemoteException;
    :goto_2
    return-void
.end method
