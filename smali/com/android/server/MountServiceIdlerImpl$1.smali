.class Lcom/android/server/MountServiceIdlerImpl$1;
.super Landroid/content/BroadcastReceiver;
.source "MountServiceIdlerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MountServiceIdlerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/MountServiceIdlerImpl;


# direct methods
.method constructor <init>(Lcom/android/server/MountServiceIdlerImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/MountServiceIdlerImpl;

    .line 34
    iput-object p1, p0, Lcom/android/server/MountServiceIdlerImpl$1;->this$0:Lcom/android/server/MountServiceIdlerImpl;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 37
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 38
    .local v0, "action":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/MountServiceIdlerImpl$1;->this$0:Lcom/android/server/MountServiceIdlerImpl;

    iget-object v1, v1, Lcom/android/server/MountServiceIdlerImpl;->mSm:Landroid/os/storage/IStorageManager;

    if-nez v1, :cond_0

    .line 39
    iget-object v1, p0, Lcom/android/server/MountServiceIdlerImpl$1;->this$0:Lcom/android/server/MountServiceIdlerImpl;

    const-string v2, "mount"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/os/storage/IStorageManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/storage/IStorageManager;

    move-result-object v2

    iput-object v2, v1, Lcom/android/server/MountServiceIdlerImpl;->mSm:Landroid/os/storage/IStorageManager;

    .line 41
    :cond_0
    iget-object v1, p0, Lcom/android/server/MountServiceIdlerImpl$1;->this$0:Lcom/android/server/MountServiceIdlerImpl;

    iget-object v1, v1, Lcom/android/server/MountServiceIdlerImpl;->mSm:Landroid/os/storage/IStorageManager;

    const-string v2, "MountServiceIdlerImpl"

    if-nez v1, :cond_1

    .line 42
    const-string v1, "Failed to find running mount service"

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    return-void

    .line 47
    :cond_1
    :try_start_0
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 48
    const-string v1, "Get the action of screen on"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    iget-object v1, p0, Lcom/android/server/MountServiceIdlerImpl$1;->this$0:Lcom/android/server/MountServiceIdlerImpl;

    iget-object v1, v1, Lcom/android/server/MountServiceIdlerImpl;->mSm:Landroid/os/storage/IStorageManager;

    invoke-interface {v1}, Landroid/os/storage/IStorageManager;->abortIdleMaintenance()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    :cond_2
    goto :goto_0

    .line 51
    :catch_0
    move-exception v1

    .line 52
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "Failed to send stop defrag or trim command to vold"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 54
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method
