.class Lcom/android/server/MiuiBatteryServiceImpl$5;
.super Landroid/database/ContentObserver;
.source "MiuiBatteryServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/MiuiBatteryServiceImpl;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/MiuiBatteryServiceImpl;


# direct methods
.method constructor <init>(Lcom/android/server/MiuiBatteryServiceImpl;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/MiuiBatteryServiceImpl;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 244
    iput-object p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$5;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 6
    .param p1, "selfChange"    # Z

    .line 247
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$5;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Lmiui/util/IMiCharge;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/util/IMiCharge;->getSocDecimal()Ljava/lang/String;

    move-result-object v0

    .line 248
    .local v0, "socDecimal":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$5;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Lmiui/util/IMiCharge;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/util/IMiCharge;->getSocDecimalRate()Ljava/lang/String;

    move-result-object v1

    .line 249
    .local v1, "socDecimalRate":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 250
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 251
    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$5;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryServiceImpl;)Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 252
    .local v2, "socDecimalValue":I
    iget-object v3, p0, Lcom/android/server/MiuiBatteryServiceImpl$5;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryServiceImpl;)Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 253
    .local v3, "socDecimalRateVaule":I
    iget-object v4, p0, Lcom/android/server/MiuiBatteryServiceImpl$5;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v4}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryServiceImpl;)Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    move-result-object v4

    const/4 v5, 0x4

    invoke-virtual {v4, v5, v2, v3}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(III)V

    .line 255
    .end local v2    # "socDecimalValue":I
    .end local v3    # "socDecimalRateVaule":I
    :cond_0
    return-void
.end method
