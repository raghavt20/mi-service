class com.android.server.contentcatcher.WakeUpContentCatcherManager$1 implements android.content.ServiceConnection {
	 /* .source "WakeUpContentCatcherManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/contentcatcher/WakeUpContentCatcherManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.contentcatcher.WakeUpContentCatcherManager this$0; //synthetic
/* # direct methods */
 com.android.server.contentcatcher.WakeUpContentCatcherManager$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/contentcatcher/WakeUpContentCatcherManager; */
/* .line 43 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onServiceConnected ( android.content.ComponentName p0, android.os.IBinder p1 ) {
/* .locals 2 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .param p2, "service" # Landroid/os/IBinder; */
/* .line 46 */
final String v0 = "WakeUpContentCatcherManager"; // const-string v0, "WakeUpContentCatcherManager"
/* const-string/jumbo v1, "service connected" */
android.util.Slog .i ( v0,v1 );
/* .line 47 */
v0 = this.this$0;
int v1 = 1; // const/4 v1, 0x1
com.android.server.contentcatcher.WakeUpContentCatcherManager .-$$Nest$fputmIsExistCatcherProcess ( v0,v1 );
/* .line 48 */
return;
} // .end method
public void onServiceDisconnected ( android.content.ComponentName p0 ) {
/* .locals 2 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .line 52 */
final String v0 = "WakeUpContentCatcherManager"; // const-string v0, "WakeUpContentCatcherManager"
/* const-string/jumbo v1, "service disConnected, so keep alive" */
android.util.Slog .i ( v0,v1 );
/* .line 53 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.android.server.contentcatcher.WakeUpContentCatcherManager .-$$Nest$fputmIsExistCatcherProcess ( v0,v1 );
/* .line 54 */
v0 = this.this$0;
com.android.server.contentcatcher.WakeUpContentCatcherManager .-$$Nest$mstartKeepAlive ( v0 );
/* .line 55 */
return;
} // .end method
