.class Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$4;
.super Landroid/content/BroadcastReceiver;
.source "WakeUpContentCatcherManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;


# direct methods
.method public static synthetic $r8$lambda$Rkh0kddtoNWDOhbu_g2vUDmGp7o(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$4;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$4;->lambda$onReceive$0()V

    return-void
.end method

.method constructor <init>(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;

    .line 88
    iput-object p1, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$4;->this$0:Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private synthetic lambda$onReceive$0()V
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$4;->this$0:Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;

    invoke-static {v0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->-$$Nest$mstartContentCatcherService(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 91
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 92
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 93
    .local v1, "data":Landroid/net/Uri;
    if-nez v1, :cond_0

    .line 94
    return-void

    .line 96
    :cond_0
    invoke-virtual {v1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v2

    .line 97
    .local v2, "packageName":Ljava/lang/String;
    const-string v3, "com.miui.contentcatcher"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 98
    return-void

    .line 100
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_2
    goto :goto_0

    :sswitch_0
    const-string v3, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    goto :goto_1

    :sswitch_1
    const-string v3, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x0

    goto :goto_1

    :sswitch_2
    const-string v3, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    goto :goto_1

    :sswitch_3
    const-string v3, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x2

    goto :goto_1

    :goto_0
    const/4 v3, -0x1

    :goto_1
    packed-switch v3, :pswitch_data_0

    goto :goto_2

    .line 102
    :pswitch_0
    const-string v3, "WakeUpContentCatcherManager"

    const-string v4, "contentcatcher apk has remove\uff0c due to install"

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    :pswitch_1
    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v3

    new-instance v4, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$4$$ExternalSyntheticLambda0;

    invoke-direct {v4, p0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$4$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$4;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 108
    :goto_2
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x304ed112 -> :sswitch_3
        0xa480416 -> :sswitch_2
        0x1f50b9c2 -> :sswitch_1
        0x5c1076e2 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
