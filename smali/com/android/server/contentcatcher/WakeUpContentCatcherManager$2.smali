.class Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$2;
.super Landroid/content/BroadcastReceiver;
.source "WakeUpContentCatcherManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;


# direct methods
.method public static synthetic $r8$lambda$I0nj_5aLFXhnAfkZZJbAGsqCZFA(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$2;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$2;->lambda$onReceive$0()V

    return-void
.end method

.method constructor <init>(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;

    .line 61
    iput-object p1, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$2;->this$0:Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private synthetic lambda$onReceive$0()V
    .locals 2

    .line 65
    iget-object v0, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$2;->this$0:Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;

    invoke-static {v0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->-$$Nest$mfunctionEnable(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 66
    return-void

    .line 68
    :cond_0
    const-string v0, "WakeUpContentCatcherManager"

    const-string v1, "catcher function has been enable"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    iget-object v0, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$2;->this$0:Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;

    invoke-static {v0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->-$$Nest$mstartContentCatcherService(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;)V

    .line 70
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 64
    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$2$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$2$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$2;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 71
    return-void
.end method
