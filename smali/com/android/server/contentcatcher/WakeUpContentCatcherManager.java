public class com.android.server.contentcatcher.WakeUpContentCatcherManager {
	 /* .source "WakeUpContentCatcherManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$MiuiSettingsObserver; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String CONTENTCATCHER_COMPONENT;
private static final java.lang.String PACKAGE_CONTENTCATCHER;
private static final java.lang.String PACKAGE_CONTENTEXTENSION;
private static final java.lang.String SUPPORT_CLIPBOARD_MODE;
private static final java.lang.String SUPPORT_DOUBLE_PRESS_MODE;
private static final java.lang.String SUPPORT_EXTENSION_PRESS_MODE;
private static final java.lang.String TAG;
private static final java.lang.String TAPLUS_CLIPBOARD_MODE;
/* # instance fields */
android.content.BroadcastReceiver mAppChangedReceiver;
android.content.BroadcastReceiver mBootCompleteReceiver;
private android.content.Context mContext;
private Integer mCurrentUserId;
private Boolean mIsExistCatcherProcess;
private android.content.ServiceConnection mServiceConnection;
android.content.BroadcastReceiver mUserSwitchReceiver;
/* # direct methods */
static android.content.Context -$$Nest$fgetmContext ( com.android.server.contentcatcher.WakeUpContentCatcherManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContext;
} // .end method
static Boolean -$$Nest$fgetmIsExistCatcherProcess ( com.android.server.contentcatcher.WakeUpContentCatcherManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mIsExistCatcherProcess:Z */
} // .end method
static void -$$Nest$fputmCurrentUserId ( com.android.server.contentcatcher.WakeUpContentCatcherManager p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput p1, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mCurrentUserId:I */
	 return;
} // .end method
static void -$$Nest$fputmIsExistCatcherProcess ( com.android.server.contentcatcher.WakeUpContentCatcherManager p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mIsExistCatcherProcess:Z */
	 return;
} // .end method
static Boolean -$$Nest$mfunctionEnable ( com.android.server.contentcatcher.WakeUpContentCatcherManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = 	 /* invoke-direct {p0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->functionEnable()Z */
} // .end method
static void -$$Nest$mstartContentCatcherService ( com.android.server.contentcatcher.WakeUpContentCatcherManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->startContentCatcherService()V */
	 return;
} // .end method
static void -$$Nest$mstartKeepAlive ( com.android.server.contentcatcher.WakeUpContentCatcherManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->startKeepAlive()V */
	 return;
} // .end method
public com.android.server.contentcatcher.WakeUpContentCatcherManager ( ) {
	 /* .locals 4 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 111 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 40 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput-boolean v0, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mIsExistCatcherProcess:Z */
	 /* .line 43 */
	 /* new-instance v0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$1; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$1;-><init>(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;)V */
	 this.mServiceConnection = v0;
	 /* .line 61 */
	 /* new-instance v0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$2; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$2;-><init>(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;)V */
	 this.mBootCompleteReceiver = v0;
	 /* .line 78 */
	 /* new-instance v0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$3; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$3;-><init>(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;)V */
	 this.mUserSwitchReceiver = v0;
	 /* .line 88 */
	 /* new-instance v0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$4; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$4;-><init>(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;)V */
	 this.mAppChangedReceiver = v0;
	 /* .line 112 */
	 this.mContext = p1;
	 /* .line 113 */
	 v0 = 	 android.os.UserHandle .myUserId ( );
	 /* iput v0, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mCurrentUserId:I */
	 /* .line 115 */
	 v0 = this.mContext;
	 (( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 /* iget v1, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mCurrentUserId:I */
	 /* const-string/jumbo v2, "support_clipboard_mode" */
	 int v3 = 1; // const/4 v3, 0x1
	 android.provider.Settings$System .putIntForUser ( v0,v2,v3,v1 );
	 /* .line 118 */
	 (( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 /* const-string/jumbo v1, "support_double_press_mode" */
	 /* iget v2, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mCurrentUserId:I */
	 android.provider.Settings$System .putIntForUser ( v0,v1,v3,v2 );
	 /* .line 120 */
	 /* new-instance v0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$MiuiSettingsObserver; */
	 /* .line 121 */
	 com.android.server.input.MiuiInputThread .getHandler ( );
	 /* invoke-direct {v0, p0, v1}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$MiuiSettingsObserver;-><init>(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;Landroid/os/Handler;)V */
	 /* .line 122 */
	 /* .local v0, "settingsObserver":Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$MiuiSettingsObserver; */
	 (( com.android.server.contentcatcher.WakeUpContentCatcherManager$MiuiSettingsObserver ) v0 ).observe ( ); // invoke-virtual {v0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$MiuiSettingsObserver;->observe()V
	 /* .line 123 */
	 /* invoke-direct {p0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->registerBootCompletedReceiver()V */
	 /* .line 124 */
	 v1 = this.mContext;
	 /* invoke-direct {p0, v1}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->registerUserSwitchReceiver(Landroid/content/Context;)V */
	 /* .line 125 */
	 /* invoke-direct {p0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->registerPackageInstallCompletedReceiver()V */
	 /* .line 126 */
	 return;
} // .end method
private Boolean functionEnable ( ) {
	 /* .locals 5 */
	 /* .line 195 */
	 v0 = this.mContext;
	 (( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 /* iget v1, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mCurrentUserId:I */
	 /* const-string/jumbo v2, "taplus_clipboard_mode" */
	 int v3 = 0; // const/4 v3, 0x0
	 v0 = 	 android.provider.Settings$System .getIntForUser ( v0,v2,v3,v1 );
	 int v1 = 1; // const/4 v1, 0x1
	 /* if-eq v0, v1, :cond_0 */
	 v0 = this.mContext;
	 /* .line 197 */
	 (( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 final String v2 = "content_catcher_network_enabled_content_extension"; // const-string v2, "content_catcher_network_enabled_content_extension"
	 /* iget v4, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mCurrentUserId:I */
	 v0 = 	 android.provider.Settings$System .getIntForUser ( v0,v2,v3,v4 );
	 /* if-ne v0, v1, :cond_1 */
} // :cond_0
/* move v3, v1 */
/* .line 195 */
} // :cond_1
} // .end method
private Boolean isExistContentExtension ( ) {
/* .locals 5 */
/* .line 159 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
v1 = this.mContext;
(( android.content.Context ) v1 ).getPackageManager ( ); // invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
final String v2 = "com.miui.contentextension"; // const-string v2, "com.miui.contentextension"
/* .line 160 */
(( android.content.pm.PackageManager ) v1 ).getPackageInfo ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
/* :try_end_0 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 164 */
/* .local v1, "packageInfo":Landroid/content/pm/PackageInfo; */
/* .line 161 */
} // .end local v1 # "packageInfo":Landroid/content/pm/PackageInfo;
/* :catch_0 */
/* move-exception v1 */
/* .line 162 */
/* .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
int v2 = 0; // const/4 v2, 0x0
/* .line 163 */
/* .local v2, "packageInfo":Landroid/content/pm/PackageInfo; */
final String v3 = "WakeUpContentCatcherManager"; // const-string v3, "WakeUpContentCatcherManager"
(( android.content.pm.PackageManager$NameNotFoundException ) v1 ).getMessage ( ); // invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;
android.util.Slog .e ( v3,v4,v1 );
/* move-object v1, v2 */
/* .line 165 */
} // .end local v2 # "packageInfo":Landroid/content/pm/PackageInfo;
/* .local v1, "packageInfo":Landroid/content/pm/PackageInfo; */
} // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
} // .end method
private void registerBootCompletedReceiver ( ) {
/* .locals 3 */
/* .line 129 */
/* new-instance v0, Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.BOOT_COMPLETED"; // const-string v1, "android.intent.action.BOOT_COMPLETED"
/* invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
/* .line 130 */
/* .local v0, "bootCompletedFilter":Landroid/content/IntentFilter; */
v1 = this.mContext;
v2 = this.mBootCompleteReceiver;
(( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 131 */
return;
} // .end method
private void registerPackageInstallCompletedReceiver ( ) {
/* .locals 3 */
/* .line 144 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 145 */
/* .local v0, "appChangedFilter":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.PACKAGE_CHANGED"; // const-string v1, "android.intent.action.PACKAGE_CHANGED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 146 */
final String v1 = "android.intent.action.PACKAGE_REPLACED"; // const-string v1, "android.intent.action.PACKAGE_REPLACED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 147 */
final String v1 = "android.intent.action.PACKAGE_REMOVED"; // const-string v1, "android.intent.action.PACKAGE_REMOVED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 148 */
final String v1 = "android.intent.action.PACKAGE_ADDED"; // const-string v1, "android.intent.action.PACKAGE_ADDED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 149 */
final String v1 = "package"; // const-string v1, "package"
(( android.content.IntentFilter ) v0 ).addDataScheme ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V
/* .line 150 */
v1 = this.mContext;
v2 = this.mAppChangedReceiver;
(( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 151 */
return;
} // .end method
private void registerUserSwitchReceiver ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 134 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 135 */
/* .local v0, "userSwitchFiler":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.USER_SWITCHED"; // const-string v1, "android.intent.action.USER_SWITCHED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 136 */
v1 = this.mUserSwitchReceiver;
int v2 = 0; // const/4 v2, 0x0
(( android.content.Context ) p1 ).registerReceiverForAllUsers ( v1, v0, v2, v2 ); // invoke-virtual {p1, v1, v0, v2, v2}, Landroid/content/Context;->registerReceiverForAllUsers(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;
/* .line 138 */
return;
} // .end method
private void startContentCatcherService ( ) {
/* .locals 2 */
/* .line 170 */
v0 = /* invoke-direct {p0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->isExistContentExtension()Z */
final String v1 = "WakeUpContentCatcherManager"; // const-string v1, "WakeUpContentCatcherManager"
/* if-nez v0, :cond_0 */
/* .line 171 */
final String v0 = "device not support, so return"; // const-string v0, "device not support, so return"
android.util.Slog .i ( v1,v0 );
/* .line 172 */
return;
/* .line 174 */
} // :cond_0
/* const-string/jumbo v0, "startContentCatcherService" */
android.util.Slog .i ( v1,v0 );
/* .line 175 */
/* invoke-direct {p0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->startKeepAlive()V */
/* .line 176 */
return;
} // .end method
private void startKeepAlive ( ) {
/* .locals 6 */
/* .line 179 */
final String v0 = "WakeUpContentCatcherManager"; // const-string v0, "WakeUpContentCatcherManager"
/* const-string/jumbo v1, "start contentcatcher process keep alive" */
android.util.Slog .i ( v0,v1 );
/* .line 180 */
/* nop */
/* .line 181 */
final String v0 = "com.miui.contentcatcher/com.miui.contentcatcher.service.StartContentCatcherService"; // const-string v0, "com.miui.contentcatcher/com.miui.contentcatcher.service.StartContentCatcherService"
android.content.ComponentName .unflattenFromString ( v0 );
/* .line 182 */
/* .local v0, "serviceComponent":Landroid/content/ComponentName; */
/* new-instance v1, Landroid/content/Intent; */
/* invoke-direct {v1}, Landroid/content/Intent;-><init>()V */
/* .line 183 */
/* .local v1, "intent":Landroid/content/Intent; */
(( android.content.Intent ) v1 ).setComponent ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 184 */
v2 = this.mContext;
v3 = this.mServiceConnection;
int v4 = 1; // const/4 v4, 0x1
v5 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v2 ).bindServiceAsUser ( v1, v3, v4, v5 ); // invoke-virtual {v2, v1, v3, v4, v5}, Landroid/content/Context;->bindServiceAsUser(Landroid/content/Intent;Landroid/content/ServiceConnection;ILandroid/os/UserHandle;)Z
/* .line 186 */
return;
} // .end method
