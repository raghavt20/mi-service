.class public Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;
.super Ljava/lang/Object;
.source "WakeUpContentCatcherManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$MiuiSettingsObserver;
    }
.end annotation


# static fields
.field private static final CONTENTCATCHER_COMPONENT:Ljava/lang/String; = "com.miui.contentcatcher/com.miui.contentcatcher.service.StartContentCatcherService"

.field private static final PACKAGE_CONTENTCATCHER:Ljava/lang/String; = "com.miui.contentcatcher"

.field private static final PACKAGE_CONTENTEXTENSION:Ljava/lang/String; = "com.miui.contentextension"

.field private static final SUPPORT_CLIPBOARD_MODE:Ljava/lang/String; = "support_clipboard_mode"

.field private static final SUPPORT_DOUBLE_PRESS_MODE:Ljava/lang/String; = "support_double_press_mode"

.field private static final SUPPORT_EXTENSION_PRESS_MODE:Ljava/lang/String; = "content_catcher_network_enabled_content_extension"

.field private static final TAG:Ljava/lang/String; = "WakeUpContentCatcherManager"

.field private static final TAPLUS_CLIPBOARD_MODE:Ljava/lang/String; = "taplus_clipboard_mode"


# instance fields
.field mAppChangedReceiver:Landroid/content/BroadcastReceiver;

.field mBootCompleteReceiver:Landroid/content/BroadcastReceiver;

.field private mContext:Landroid/content/Context;

.field private mCurrentUserId:I

.field private mIsExistCatcherProcess:Z

.field private mServiceConnection:Landroid/content/ServiceConnection;

.field mUserSwitchReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsExistCatcherProcess(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mIsExistCatcherProcess:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmCurrentUserId(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mCurrentUserId:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsExistCatcherProcess(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mIsExistCatcherProcess:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mfunctionEnable(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->functionEnable()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mstartContentCatcherService(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->startContentCatcherService()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mstartKeepAlive(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->startKeepAlive()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mIsExistCatcherProcess:Z

    .line 43
    new-instance v0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$1;

    invoke-direct {v0, p0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$1;-><init>(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;)V

    iput-object v0, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 61
    new-instance v0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$2;

    invoke-direct {v0, p0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$2;-><init>(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;)V

    iput-object v0, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mBootCompleteReceiver:Landroid/content/BroadcastReceiver;

    .line 78
    new-instance v0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$3;

    invoke-direct {v0, p0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$3;-><init>(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;)V

    iput-object v0, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mUserSwitchReceiver:Landroid/content/BroadcastReceiver;

    .line 88
    new-instance v0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$4;

    invoke-direct {v0, p0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$4;-><init>(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;)V

    iput-object v0, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mAppChangedReceiver:Landroid/content/BroadcastReceiver;

    .line 112
    iput-object p1, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mContext:Landroid/content/Context;

    .line 113
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    iput v0, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mCurrentUserId:I

    .line 115
    iget-object v0, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget v1, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mCurrentUserId:I

    const-string/jumbo v2, "support_clipboard_mode"

    const/4 v3, 0x1

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 118
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "support_double_press_mode"

    iget v2, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mCurrentUserId:I

    invoke-static {v0, v1, v3, v2}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 120
    new-instance v0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$MiuiSettingsObserver;

    .line 121
    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$MiuiSettingsObserver;-><init>(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;Landroid/os/Handler;)V

    .line 122
    .local v0, "settingsObserver":Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$MiuiSettingsObserver;
    invoke-virtual {v0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$MiuiSettingsObserver;->observe()V

    .line 123
    invoke-direct {p0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->registerBootCompletedReceiver()V

    .line 124
    iget-object v1, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mContext:Landroid/content/Context;

    invoke-direct {p0, v1}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->registerUserSwitchReceiver(Landroid/content/Context;)V

    .line 125
    invoke-direct {p0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->registerPackageInstallCompletedReceiver()V

    .line 126
    return-void
.end method

.method private functionEnable()Z
    .locals 5

    .line 195
    iget-object v0, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget v1, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mCurrentUserId:I

    const-string/jumbo v2, "taplus_clipboard_mode"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mContext:Landroid/content/Context;

    .line 197
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "content_catcher_network_enabled_content_extension"

    iget v4, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mCurrentUserId:I

    invoke-static {v0, v2, v3, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v1, :cond_1

    :cond_0
    move v3, v1

    .line 195
    :cond_1
    return v3
.end method

.method private isExistContentExtension()Z
    .locals 5

    .line 159
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.miui.contentextension"

    .line 160
    invoke-virtual {v1, v2, v0}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 164
    .local v1, "packageInfo":Landroid/content/pm/PackageInfo;
    goto :goto_0

    .line 161
    .end local v1    # "packageInfo":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v1

    .line 162
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v2, 0x0

    .line 163
    .local v2, "packageInfo":Landroid/content/pm/PackageInfo;
    const-string v3, "WakeUpContentCatcherManager"

    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v1, v2

    .line 165
    .end local v2    # "packageInfo":Landroid/content/pm/PackageInfo;
    .local v1, "packageInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private registerBootCompletedReceiver()V
    .locals 3

    .line 129
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 130
    .local v0, "bootCompletedFilter":Landroid/content/IntentFilter;
    iget-object v1, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mBootCompleteReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 131
    return-void
.end method

.method private registerPackageInstallCompletedReceiver()V
    .locals 3

    .line 144
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 145
    .local v0, "appChangedFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 146
    const-string v1, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 147
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 148
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 149
    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 150
    iget-object v1, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mAppChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 151
    return-void
.end method

.method private registerUserSwitchReceiver(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 134
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 135
    .local v0, "userSwitchFiler":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.USER_SWITCHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 136
    iget-object v1, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mUserSwitchReceiver:Landroid/content/BroadcastReceiver;

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v0, v2, v2}, Landroid/content/Context;->registerReceiverForAllUsers(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 138
    return-void
.end method

.method private startContentCatcherService()V
    .locals 2

    .line 170
    invoke-direct {p0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->isExistContentExtension()Z

    move-result v0

    const-string v1, "WakeUpContentCatcherManager"

    if-nez v0, :cond_0

    .line 171
    const-string v0, "device not support, so return"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    return-void

    .line 174
    :cond_0
    const-string/jumbo v0, "startContentCatcherService"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    invoke-direct {p0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->startKeepAlive()V

    .line 176
    return-void
.end method

.method private startKeepAlive()V
    .locals 6

    .line 179
    const-string v0, "WakeUpContentCatcherManager"

    const-string/jumbo v1, "start contentcatcher process keep alive"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    nop

    .line 181
    const-string v0, "com.miui.contentcatcher/com.miui.contentcatcher.service.StartContentCatcherService"

    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    .line 182
    .local v0, "serviceComponent":Landroid/content/ComponentName;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 183
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 184
    iget-object v2, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->mServiceConnection:Landroid/content/ServiceConnection;

    const/4 v4, 0x1

    sget-object v5, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v2, v1, v3, v4, v5}, Landroid/content/Context;->bindServiceAsUser(Landroid/content/Intent;Landroid/content/ServiceConnection;ILandroid/os/UserHandle;)Z

    .line 186
    return-void
.end method
