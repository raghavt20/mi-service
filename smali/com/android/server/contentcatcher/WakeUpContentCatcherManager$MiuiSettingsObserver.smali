.class Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$MiuiSettingsObserver;
.super Landroid/database/ContentObserver;
.source "WakeUpContentCatcherManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MiuiSettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;


# direct methods
.method public constructor <init>(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .line 202
    iput-object p1, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$MiuiSettingsObserver;->this$0:Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;

    .line 203
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 204
    return-void
.end method


# virtual methods
.method observe()V
    .locals 4

    .line 207
    iget-object v0, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$MiuiSettingsObserver;->this$0:Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;

    invoke-static {v0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->-$$Nest$fgetmContext(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 208
    .local v0, "resolver":Landroid/content/ContentResolver;
    nop

    .line 209
    const-string/jumbo v1, "taplus_clipboard_mode"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 208
    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 211
    nop

    .line 212
    const-string v1, "content_catcher_network_enabled_content_extension"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 211
    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 214
    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 2
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 219
    iget-object v0, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$MiuiSettingsObserver;->this$0:Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;

    invoke-static {v0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->-$$Nest$mfunctionEnable(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;)Z

    move-result v0

    .line 220
    .local v0, "enable":Z
    iget-object v1, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$MiuiSettingsObserver;->this$0:Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;

    invoke-static {v1}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->-$$Nest$fgetmIsExistCatcherProcess(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    .line 221
    iget-object v1, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$MiuiSettingsObserver;->this$0:Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;

    invoke-static {v1}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->-$$Nest$mstartContentCatcherService(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;)V

    .line 223
    :cond_0
    return-void
.end method
