class com.android.server.contentcatcher.WakeUpContentCatcherManager$MiuiSettingsObserver extends android.database.ContentObserver {
	 /* .source "WakeUpContentCatcherManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/contentcatcher/WakeUpContentCatcherManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "MiuiSettingsObserver" */
} // .end annotation
/* # instance fields */
final com.android.server.contentcatcher.WakeUpContentCatcherManager this$0; //synthetic
/* # direct methods */
public com.android.server.contentcatcher.WakeUpContentCatcherManager$MiuiSettingsObserver ( ) {
/* .locals 0 */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 202 */
this.this$0 = p1;
/* .line 203 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 204 */
return;
} // .end method
/* # virtual methods */
void observe ( ) {
/* .locals 4 */
/* .line 207 */
v0 = this.this$0;
com.android.server.contentcatcher.WakeUpContentCatcherManager .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 208 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
/* nop */
/* .line 209 */
/* const-string/jumbo v1, "taplus_clipboard_mode" */
android.provider.Settings$System .getUriFor ( v1 );
/* .line 208 */
int v2 = 0; // const/4 v2, 0x0
int v3 = -1; // const/4 v3, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 211 */
/* nop */
/* .line 212 */
final String v1 = "content_catcher_network_enabled_content_extension"; // const-string v1, "content_catcher_network_enabled_content_extension"
android.provider.Settings$System .getUriFor ( v1 );
/* .line 211 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 214 */
return;
} // .end method
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 2 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 219 */
v0 = this.this$0;
v0 = com.android.server.contentcatcher.WakeUpContentCatcherManager .-$$Nest$mfunctionEnable ( v0 );
/* .line 220 */
/* .local v0, "enable":Z */
v1 = this.this$0;
v1 = com.android.server.contentcatcher.WakeUpContentCatcherManager .-$$Nest$fgetmIsExistCatcherProcess ( v1 );
/* if-nez v1, :cond_0 */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 221 */
	 v1 = this.this$0;
	 com.android.server.contentcatcher.WakeUpContentCatcherManager .-$$Nest$mstartContentCatcherService ( v1 );
	 /* .line 223 */
} // :cond_0
return;
} // .end method
