.class Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$1;
.super Ljava/lang/Object;
.source "WakeUpContentCatcherManager.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;


# direct methods
.method constructor <init>(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;

    .line 43
    iput-object p1, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$1;->this$0:Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .line 46
    const-string v0, "WakeUpContentCatcherManager"

    const-string/jumbo v1, "service connected"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    iget-object v0, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$1;->this$0:Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->-$$Nest$fputmIsExistCatcherProcess(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;Z)V

    .line 48
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .line 52
    const-string v0, "WakeUpContentCatcherManager"

    const-string/jumbo v1, "service disConnected, so keep alive"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    iget-object v0, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$1;->this$0:Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->-$$Nest$fputmIsExistCatcherProcess(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;Z)V

    .line 54
    iget-object v0, p0, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$1;->this$0:Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;

    invoke-static {v0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;->-$$Nest$mstartKeepAlive(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager;)V

    .line 55
    return-void
.end method
