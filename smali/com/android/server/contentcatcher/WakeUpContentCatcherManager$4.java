class com.android.server.contentcatcher.WakeUpContentCatcherManager$4 extends android.content.BroadcastReceiver {
	 /* .source "WakeUpContentCatcherManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/contentcatcher/WakeUpContentCatcherManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.contentcatcher.WakeUpContentCatcherManager this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$Rkh0kddtoNWDOhbu_g2vUDmGp7o ( com.android.server.contentcatcher.WakeUpContentCatcherManager$4 p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$4;->lambda$onReceive$0()V */
return;
} // .end method
 com.android.server.contentcatcher.WakeUpContentCatcherManager$4 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/contentcatcher/WakeUpContentCatcherManager; */
/* .line 88 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
private void lambda$onReceive$0 ( ) { //synthethic
/* .locals 1 */
/* .line 106 */
v0 = this.this$0;
com.android.server.contentcatcher.WakeUpContentCatcherManager .-$$Nest$mstartContentCatcherService ( v0 );
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 91 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 92 */
/* .local v0, "action":Ljava/lang/String; */
(( android.content.Intent ) p2 ).getData ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;
/* .line 93 */
/* .local v1, "data":Landroid/net/Uri; */
/* if-nez v1, :cond_0 */
/* .line 94 */
return;
/* .line 96 */
} // :cond_0
(( android.net.Uri ) v1 ).getSchemeSpecificPart ( ); // invoke-virtual {v1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;
/* .line 97 */
/* .local v2, "packageName":Ljava/lang/String; */
final String v3 = "com.miui.contentcatcher"; // const-string v3, "com.miui.contentcatcher"
v3 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v3, :cond_1 */
/* .line 98 */
return;
/* .line 100 */
} // :cond_1
v3 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
/* sparse-switch v3, :sswitch_data_0 */
} // :cond_2
/* :sswitch_0 */
final String v3 = "android.intent.action.PACKAGE_ADDED"; // const-string v3, "android.intent.action.PACKAGE_ADDED"
v3 = (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_2
int v3 = 3; // const/4 v3, 0x3
/* :sswitch_1 */
final String v3 = "android.intent.action.PACKAGE_REMOVED"; // const-string v3, "android.intent.action.PACKAGE_REMOVED"
v3 = (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_2
int v3 = 0; // const/4 v3, 0x0
/* :sswitch_2 */
final String v3 = "android.intent.action.PACKAGE_CHANGED"; // const-string v3, "android.intent.action.PACKAGE_CHANGED"
v3 = (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_2
int v3 = 1; // const/4 v3, 0x1
/* :sswitch_3 */
final String v3 = "android.intent.action.PACKAGE_REPLACED"; // const-string v3, "android.intent.action.PACKAGE_REPLACED"
v3 = (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_2
	 int v3 = 2; // const/4 v3, 0x2
} // :goto_0
int v3 = -1; // const/4 v3, -0x1
} // :goto_1
/* packed-switch v3, :pswitch_data_0 */
/* .line 102 */
/* :pswitch_0 */
final String v3 = "WakeUpContentCatcherManager"; // const-string v3, "WakeUpContentCatcherManager"
final String v4 = "contentcatcher apk has remove\uff0c due to install"; // const-string v4, "contentcatcher apk has remove\uff0c due to install"
android.util.Slog .i ( v3,v4 );
/* .line 106 */
/* :pswitch_1 */
com.android.server.input.MiuiInputThread .getHandler ( );
/* new-instance v4, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$4$$ExternalSyntheticLambda0; */
/* invoke-direct {v4, p0}, Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$4$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/contentcatcher/WakeUpContentCatcherManager$4;)V */
(( android.os.Handler ) v3 ).post ( v4 ); // invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 108 */
} // :goto_2
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x304ed112 -> :sswitch_3 */
/* 0xa480416 -> :sswitch_2 */
/* 0x1f50b9c2 -> :sswitch_1 */
/* 0x5c1076e2 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_0 */
/* :pswitch_1 */
/* :pswitch_1 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
