class com.android.server.MiuiBatteryStatsService$BatteryTempVoltageTimeInfo {
	 /* .source "MiuiBatteryStatsService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/MiuiBatteryStatsService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "BatteryTempVoltageTimeInfo" */
} // .end annotation
/* # instance fields */
private java.lang.String condition;
private Long fullChargeTotalTime;
private Long highTempTotalTime;
private Long highTempVoltageTotalTime;
private Long highVoltageTotalTime;
private Long startFullChargeTime;
private Long startHighTempTime;
private Long startHighTempVoltageTime;
private Long startHighVoltageTime;
final com.android.server.MiuiBatteryStatsService this$0; //synthetic
/* # direct methods */
static void -$$Nest$mclearTime ( com.android.server.MiuiBatteryStatsService$BatteryTempVoltageTimeInfo p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->clearTime()V */
return;
} // .end method
static void -$$Nest$mstopTime ( com.android.server.MiuiBatteryStatsService$BatteryTempVoltageTimeInfo p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->stopTime()V */
return;
} // .end method
public com.android.server.MiuiBatteryStatsService$BatteryTempVoltageTimeInfo ( ) {
/* .locals 2 */
/* .param p1, "this$0" # Lcom/android/server/MiuiBatteryStatsService; */
/* .param p2, "status" # Ljava/lang/String; */
/* .line 1677 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 1672 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startHighTempTime:J */
/* .line 1673 */
/* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startHighVoltageTime:J */
/* .line 1674 */
/* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startHighTempVoltageTime:J */
/* .line 1675 */
/* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startFullChargeTime:J */
/* .line 1678 */
/* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->highTempTotalTime:J */
/* .line 1679 */
/* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->highVoltageTotalTime:J */
/* .line 1680 */
/* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->fullChargeTotalTime:J */
/* .line 1681 */
/* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->highTempVoltageTotalTime:J */
/* .line 1682 */
this.condition = p2;
/* .line 1683 */
return;
} // .end method
private void clearTime ( ) {
/* .locals 2 */
/* .line 1743 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->highTempTotalTime:J */
/* .line 1744 */
/* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->highVoltageTotalTime:J */
/* .line 1745 */
/* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->highTempVoltageTotalTime:J */
/* .line 1746 */
/* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->fullChargeTotalTime:J */
/* .line 1747 */
return;
} // .end method
private void stopTime ( ) {
/* .locals 8 */
/* .line 1721 */
/* iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startHighTempTime:J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 1722 */
	 /* iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->highTempTotalTime:J */
	 java.lang.System .currentTimeMillis ( );
	 /* move-result-wide v4 */
	 /* iget-wide v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startHighTempTime:J */
	 /* sub-long/2addr v4, v6 */
	 /* add-long/2addr v0, v4 */
	 /* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->highTempTotalTime:J */
	 /* .line 1723 */
	 /* iput-wide v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startHighTempTime:J */
	 /* .line 1726 */
} // :cond_0
/* iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startHighVoltageTime:J */
/* cmp-long v0, v0, v2 */
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 1727 */
	 /* iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->highVoltageTotalTime:J */
	 java.lang.System .currentTimeMillis ( );
	 /* move-result-wide v4 */
	 /* iget-wide v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startHighVoltageTime:J */
	 /* sub-long/2addr v4, v6 */
	 /* add-long/2addr v0, v4 */
	 /* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->highVoltageTotalTime:J */
	 /* .line 1728 */
	 /* iput-wide v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startHighVoltageTime:J */
	 /* .line 1731 */
} // :cond_1
/* iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startHighTempVoltageTime:J */
/* cmp-long v0, v0, v2 */
if ( v0 != null) { // if-eqz v0, :cond_2
	 /* .line 1732 */
	 /* iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->highTempVoltageTotalTime:J */
	 java.lang.System .currentTimeMillis ( );
	 /* move-result-wide v4 */
	 /* iget-wide v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startHighTempVoltageTime:J */
	 /* sub-long/2addr v4, v6 */
	 /* add-long/2addr v0, v4 */
	 /* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->highTempVoltageTotalTime:J */
	 /* .line 1733 */
	 /* iput-wide v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startHighTempVoltageTime:J */
	 /* .line 1736 */
} // :cond_2
/* iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startFullChargeTime:J */
/* cmp-long v0, v0, v2 */
if ( v0 != null) { // if-eqz v0, :cond_3
	 /* .line 1737 */
	 /* iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->fullChargeTotalTime:J */
	 java.lang.System .currentTimeMillis ( );
	 /* move-result-wide v4 */
	 /* iget-wide v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startFullChargeTime:J */
	 /* sub-long/2addr v4, v6 */
	 /* add-long/2addr v0, v4 */
	 /* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->fullChargeTotalTime:J */
	 /* .line 1738 */
	 /* iput-wide v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startFullChargeTime:J */
	 /* .line 1740 */
} // :cond_3
return;
} // .end method
/* # virtual methods */
public void collectTime ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 11 */
/* .param p1, "temp" # I */
/* .param p2, "voltage" # I */
/* .param p3, "chargeStatus" # I */
/* .line 1686 */
/* const-wide/16 v0, 0x0 */
/* const/16 v2, 0x190 */
/* if-le p1, v2, :cond_0 */
/* iget-wide v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startHighTempTime:J */
/* cmp-long v3, v3, v0 */
/* if-nez v3, :cond_0 */
/* .line 1687 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
/* iput-wide v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startHighTempTime:J */
/* .line 1689 */
} // :cond_0
/* const/16 v3, 0x1004 */
/* if-le p2, v3, :cond_1 */
/* iget-wide v4, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startHighVoltageTime:J */
/* cmp-long v4, v4, v0 */
/* if-nez v4, :cond_1 */
/* .line 1690 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v4 */
/* iput-wide v4, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startHighVoltageTime:J */
/* .line 1692 */
} // :cond_1
/* if-le p1, v2, :cond_2 */
/* if-le p2, v3, :cond_2 */
/* iget-wide v4, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startHighTempVoltageTime:J */
/* cmp-long v4, v4, v0 */
/* if-nez v4, :cond_2 */
/* .line 1693 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v4 */
/* iput-wide v4, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startHighTempVoltageTime:J */
/* .line 1695 */
} // :cond_2
int v4 = 5; // const/4 v4, 0x5
/* if-ne p3, v4, :cond_3 */
/* iget-wide v5, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startFullChargeTime:J */
/* cmp-long v5, v5, v0 */
/* if-nez v5, :cond_3 */
/* .line 1696 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v5 */
/* iput-wide v5, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startFullChargeTime:J */
/* .line 1699 */
} // :cond_3
/* if-gt p1, v2, :cond_4 */
/* iget-wide v5, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startHighTempTime:J */
/* cmp-long v5, v5, v0 */
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 1700 */
/* iget-wide v5, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->highTempTotalTime:J */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v7 */
/* iget-wide v9, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startHighTempTime:J */
/* sub-long/2addr v7, v9 */
/* add-long/2addr v5, v7 */
/* iput-wide v5, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->highTempTotalTime:J */
/* .line 1701 */
/* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startHighTempTime:J */
/* .line 1704 */
} // :cond_4
/* if-gt p2, v3, :cond_5 */
/* iget-wide v5, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startHighVoltageTime:J */
/* cmp-long v5, v5, v0 */
if ( v5 != null) { // if-eqz v5, :cond_5
/* .line 1705 */
/* iget-wide v5, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->highVoltageTotalTime:J */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v7 */
/* iget-wide v9, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startHighVoltageTime:J */
/* sub-long/2addr v7, v9 */
/* add-long/2addr v5, v7 */
/* iput-wide v5, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->highVoltageTotalTime:J */
/* .line 1706 */
/* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startHighVoltageTime:J */
/* .line 1709 */
} // :cond_5
/* if-le p1, v2, :cond_6 */
/* if-gt p2, v3, :cond_7 */
} // :cond_6
/* iget-wide v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startHighTempVoltageTime:J */
/* cmp-long v2, v2, v0 */
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 1710 */
/* iget-wide v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->highTempVoltageTotalTime:J */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v5 */
/* iget-wide v7, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startHighTempVoltageTime:J */
/* sub-long/2addr v5, v7 */
/* add-long/2addr v2, v5 */
/* iput-wide v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->highTempVoltageTotalTime:J */
/* .line 1711 */
/* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startHighTempVoltageTime:J */
/* .line 1714 */
} // :cond_7
/* if-eq p3, v4, :cond_8 */
/* iget-wide v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startFullChargeTime:J */
/* cmp-long v2, v2, v0 */
if ( v2 != null) { // if-eqz v2, :cond_8
/* .line 1715 */
/* iget-wide v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->fullChargeTotalTime:J */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v4 */
/* iget-wide v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startFullChargeTime:J */
/* sub-long/2addr v4, v6 */
/* add-long/2addr v2, v4 */
/* iput-wide v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->fullChargeTotalTime:J */
/* .line 1716 */
/* iput-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->startFullChargeTime:J */
/* .line 1718 */
} // :cond_8
return;
} // .end method
public java.lang.String getCondition ( ) {
/* .locals 1 */
/* .line 1766 */
v0 = this.condition;
} // .end method
public Long getFullChargeTotalTime ( ) {
/* .locals 2 */
/* .line 1762 */
/* iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->fullChargeTotalTime:J */
/* return-wide v0 */
} // .end method
public Long getHighTempTotalTime ( ) {
/* .locals 2 */
/* .line 1750 */
/* iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->highTempTotalTime:J */
/* return-wide v0 */
} // .end method
public Long getHighTempVoltageTotalTime ( ) {
/* .locals 2 */
/* .line 1758 */
/* iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->highTempVoltageTotalTime:J */
/* return-wide v0 */
} // .end method
public Long getHighVoltageTotalTime ( ) {
/* .locals 2 */
/* .line 1754 */
/* iget-wide v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->highVoltageTotalTime:J */
/* return-wide v0 */
} // .end method
