class com.android.server.WatchdogImpl$1 implements java.lang.Runnable {
	 /* .source "WatchdogImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/WatchdogImpl;->scheduleLayerLeakCheck()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final android.os.Handler val$h; //synthetic
/* # direct methods */
 com.android.server.WatchdogImpl$1 ( ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 123 */
this.val$h = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 3 */
/* .line 126 */
v0 = android.view.SurfaceControl .checkLayersLeaked ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 127 */
android.view.SurfaceControl .getLeakedLayers ( );
/* .line 128 */
/* .local v0, "leakedLayers":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Leaked layers: "; // const-string v2, "Leaked layers: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "Watchdog"; // const-string v2, "Watchdog"
android.util.Slog .w ( v2,v1 );
/* .line 129 */
android.view.SurfaceControlStub .getInstance ( );
int v2 = 0; // const/4 v2, 0x0
(( android.view.SurfaceControlStub ) v1 ).reportOORException ( v2 ); // invoke-virtual {v1, v2}, Landroid/view/SurfaceControlStub;->reportOORException(Z)V
/* .line 133 */
} // .end local v0 # "leakedLayers":Ljava/lang/String;
} // :cond_0
v0 = this.val$h;
/* const-wide/32 v1, 0x249f0 */
(( android.os.Handler ) v0 ).postDelayed ( p0, v1, v2 ); // invoke-virtual {v0, p0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 134 */
return;
} // .end method
