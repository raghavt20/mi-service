class com.android.server.MiuiBatteryIntelligence$BatteryNotificationListernerService extends android.service.notification.NotificationListenerService {
	 /* .source "MiuiBatteryIntelligence.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/MiuiBatteryIntelligence; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "BatteryNotificationListernerService" */
} // .end annotation
/* # static fields */
private static final java.lang.String ACTION_FULL_CHARGER_NAVIGATION;
private static final java.lang.String EXTRA_FULL_CHARGER_NAVIGATION;
/* # instance fields */
private java.util.ArrayList mAlreadyStartApp;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.ArrayList mAlreadyStartAppXSpace;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public Boolean mIsNavigation;
final com.android.server.MiuiBatteryIntelligence this$0; //synthetic
/* # direct methods */
private com.android.server.MiuiBatteryIntelligence$BatteryNotificationListernerService ( ) {
/* .locals 0 */
/* .line 450 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/service/notification/NotificationListenerService;-><init>()V */
/* .line 453 */
/* new-instance p1, Ljava/util/ArrayList; */
/* invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V */
this.mAlreadyStartApp = p1;
/* .line 454 */
/* new-instance p1, Ljava/util/ArrayList; */
/* invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V */
this.mAlreadyStartAppXSpace = p1;
return;
} // .end method
 com.android.server.MiuiBatteryIntelligence$BatteryNotificationListernerService ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;-><init>(Lcom/android/server/MiuiBatteryIntelligence;)V */
return;
} // .end method
private void checkNavigationEnd ( android.service.notification.StatusBarNotification p0 ) {
/* .locals 4 */
/* .param p1, "sbn" # Landroid/service/notification/StatusBarNotification; */
/* .line 505 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->isNavigationStatus(Landroid/service/notification/StatusBarNotification;)Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 506 */
(( android.service.notification.StatusBarNotification ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;
/* .line 507 */
/* .local v0, "packageName":Ljava/lang/String; */
v1 = (( android.service.notification.StatusBarNotification ) p1 ).getUserId ( ); // invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getUserId()I
/* .line 508 */
/* .local v1, "appUserId":I */
/* if-nez v1, :cond_0 */
v2 = this.mAlreadyStartApp;
v2 = (( java.util.ArrayList ) v2 ).contains ( v0 ); // invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 509 */
v2 = this.mAlreadyStartApp;
(( java.util.ArrayList ) v2 ).remove ( v0 ); // invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
/* .line 510 */
} // :cond_0
/* const/16 v2, 0x3e7 */
/* if-ne v1, v2, :cond_1 */
v2 = this.mAlreadyStartAppXSpace;
v2 = (( java.util.ArrayList ) v2 ).contains ( v0 ); // invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 511 */
v2 = this.mAlreadyStartAppXSpace;
(( java.util.ArrayList ) v2 ).remove ( v0 ); // invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
/* .line 513 */
} // :cond_1
} // :goto_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Owner Space Start APP list = "; // const-string v3, "Owner Space Start APP list = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mAlreadyStartApp;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v3 = " XSpace Start APP list = "; // const-string v3, " XSpace Start APP list = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mAlreadyStartAppXSpace;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiBatteryIntelligence"; // const-string v3, "MiuiBatteryIntelligence"
android.util.Slog .d ( v3,v2 );
/* .line 514 */
v2 = this.mAlreadyStartApp;
v2 = (( java.util.ArrayList ) v2 ).isEmpty ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z
if ( v2 != null) { // if-eqz v2, :cond_2
v2 = this.mAlreadyStartAppXSpace;
v2 = (( java.util.ArrayList ) v2 ).isEmpty ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 515 */
int v2 = 0; // const/4 v2, 0x0
/* iput-boolean v2, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->mIsNavigation:Z */
/* .line 516 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->sendNavigationBroadcast()V */
/* .line 519 */
} // .end local v0 # "packageName":Ljava/lang/String;
} // .end local v1 # "appUserId":I
} // :cond_2
return;
} // .end method
private void checkNavigationEntry ( android.service.notification.StatusBarNotification p0 ) {
/* .locals 4 */
/* .param p1, "sbn" # Landroid/service/notification/StatusBarNotification; */
/* .line 491 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->isNavigationStatus(Landroid/service/notification/StatusBarNotification;)Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 492 */
(( android.service.notification.StatusBarNotification ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;
/* .line 493 */
/* .local v0, "packageName":Ljava/lang/String; */
v1 = (( android.service.notification.StatusBarNotification ) p1 ).getUserId ( ); // invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getUserId()I
/* .line 494 */
/* .local v1, "appUserId":I */
/* if-nez v1, :cond_0 */
v2 = this.mAlreadyStartApp;
v2 = (( java.util.ArrayList ) v2 ).contains ( v0 ); // invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v2, :cond_0 */
/* .line 495 */
v2 = this.mAlreadyStartApp;
(( java.util.ArrayList ) v2 ).add ( v0 ); // invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 496 */
} // :cond_0
/* const/16 v2, 0x3e7 */
/* if-ne v1, v2, :cond_1 */
v2 = this.mAlreadyStartAppXSpace;
v2 = (( java.util.ArrayList ) v2 ).contains ( v0 ); // invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v2, :cond_1 */
/* .line 497 */
v2 = this.mAlreadyStartAppXSpace;
(( java.util.ArrayList ) v2 ).add ( v0 ); // invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 499 */
} // :cond_1
} // :goto_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Owner Space Start APP list = "; // const-string v3, "Owner Space Start APP list = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mAlreadyStartApp;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v3 = " XSpace Start APP list = "; // const-string v3, " XSpace Start APP list = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mAlreadyStartAppXSpace;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiBatteryIntelligence"; // const-string v3, "MiuiBatteryIntelligence"
android.util.Slog .d ( v3,v2 );
/* .line 500 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->processNavigation()V */
/* .line 502 */
} // .end local v0 # "packageName":Ljava/lang/String;
} // .end local v1 # "appUserId":I
} // :cond_2
return;
} // .end method
private Boolean isNavigationStatus ( android.service.notification.StatusBarNotification p0 ) {
/* .locals 4 */
/* .param p1, "sbn" # Landroid/service/notification/StatusBarNotification; */
/* .line 522 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_2
v1 = /* invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->isOnwerUser()Z */
/* if-nez v1, :cond_0 */
/* .line 525 */
} // :cond_0
(( android.service.notification.StatusBarNotification ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;
/* .line 526 */
/* .local v1, "packName":Ljava/lang/String; */
v2 = (( android.service.notification.StatusBarNotification ) p1 ).isClearable ( ); // invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->isClearable()Z
/* .line 527 */
/* .local v2, "isClearable":Z */
/* if-nez v2, :cond_1 */
v3 = this.this$0;
com.android.server.MiuiBatteryIntelligence .-$$Nest$fgetmMapApplist ( v3 );
v3 = (( java.util.ArrayList ) v3 ).contains ( v1 ); // invoke-virtual {v3, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 528 */
int v0 = 1; // const/4 v0, 0x1
/* .line 530 */
} // :cond_1
/* .line 523 */
} // .end local v1 # "packName":Ljava/lang/String;
} // .end local v2 # "isClearable":Z
} // :cond_2
} // :goto_0
} // .end method
private Boolean isOnwerUser ( ) {
/* .locals 1 */
/* .line 552 */
v0 = miui.securityspace.CrossUserUtils .getCurrentUserId ( );
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private Boolean isTargetEnvironment ( ) {
/* .locals 2 */
/* .line 546 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Plugged? "; // const-string v1, "Plugged? "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.this$0;
v1 = com.android.server.MiuiBatteryIntelligence .-$$Nest$fgetmPlugged ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " isNavigation? "; // const-string v1, " isNavigation? "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->mIsNavigation:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiBatteryIntelligence"; // const-string v1, "MiuiBatteryIntelligence"
android.util.Slog .d ( v1,v0 );
/* .line 547 */
v0 = this.this$0;
v0 = com.android.server.MiuiBatteryIntelligence .-$$Nest$fgetmPlugged ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->mIsNavigation:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private void processNavigation ( ) {
/* .locals 1 */
/* .line 534 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->mIsNavigation:Z */
/* .line 535 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->sendNavigationBroadcast()V */
/* .line 536 */
return;
} // .end method
private void sendNavigationBroadcast ( ) {
/* .locals 4 */
/* .line 539 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "miui.intent.action.ACTION_FULL_CHARGE_NAVIGATION"; // const-string v1, "miui.intent.action.ACTION_FULL_CHARGE_NAVIGATION"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 540 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "miui.intent.extra.FULL_CHARGER_NAVIGATION"; // const-string v1, "miui.intent.extra.FULL_CHARGER_NAVIGATION"
v2 = /* invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->isTargetEnvironment()Z */
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 541 */
final String v1 = "com.miui.securitycenter"; // const-string v1, "com.miui.securitycenter"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 542 */
v1 = this.this$0;
v1 = this.mContext;
v2 = android.os.UserHandle.CURRENT;
final String v3 = "com.xiaomi.permission.miuibatteryintelligence"; // const-string v3, "com.xiaomi.permission.miuibatteryintelligence"
(( android.content.Context ) v1 ).sendBroadcastAsUser ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;)V
/* .line 543 */
return;
} // .end method
/* # virtual methods */
public void clearStartAppList ( ) {
/* .locals 1 */
/* .line 556 */
v0 = this.mAlreadyStartApp;
(( java.util.ArrayList ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
/* .line 557 */
v0 = this.mAlreadyStartAppXSpace;
(( java.util.ArrayList ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
/* .line 558 */
return;
} // .end method
public void onListenerConnected ( ) {
/* .locals 4 */
/* .line 460 */
int v0 = 0; // const/4 v0, 0x0
/* .line 462 */
/* .local v0, "postedNotifications":[Landroid/service/notification/StatusBarNotification; */
try { // :try_start_0
(( com.android.server.MiuiBatteryIntelligence$BatteryNotificationListernerService ) p0 ).getActiveNotifications ( ); // invoke-virtual {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->getActiveNotifications()[Landroid/service/notification/StatusBarNotification;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v0, v1 */
/* .line 465 */
/* .line 463 */
/* :catch_0 */
/* move-exception v1 */
/* .line 464 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "MiuiBatteryIntelligence"; // const-string v2, "MiuiBatteryIntelligence"
final String v3 = "Get Notification Faild"; // const-string v3, "Get Notification Faild"
android.util.Slog .e ( v2,v3 );
/* .line 466 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 467 */
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
} // :goto_1
/* if-ge v2, v1, :cond_0 */
/* aget-object v3, v0, v2 */
/* .line 468 */
/* .local v3, "sbn":Landroid/service/notification/StatusBarNotification; */
/* invoke-direct {p0, v3}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->checkNavigationEntry(Landroid/service/notification/StatusBarNotification;)V */
/* .line 467 */
} // .end local v3 # "sbn":Landroid/service/notification/StatusBarNotification;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 471 */
} // :cond_0
return;
} // .end method
public void onListenerDisconnected ( ) {
/* .locals 2 */
/* .line 475 */
final String v0 = "MiuiBatteryIntelligence"; // const-string v0, "MiuiBatteryIntelligence"
final String v1 = "destory?"; // const-string v1, "destory?"
android.util.Slog .d ( v0,v1 );
/* .line 476 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->mIsNavigation:Z */
/* .line 477 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->sendNavigationBroadcast()V */
/* .line 478 */
return;
} // .end method
public void onNotificationPosted ( android.service.notification.StatusBarNotification p0 ) {
/* .locals 0 */
/* .param p1, "sbn" # Landroid/service/notification/StatusBarNotification; */
/* .line 482 */
/* invoke-direct {p0, p1}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->checkNavigationEntry(Landroid/service/notification/StatusBarNotification;)V */
/* .line 483 */
return;
} // .end method
public void onNotificationRemoved ( android.service.notification.StatusBarNotification p0 ) {
/* .locals 0 */
/* .param p1, "sbn" # Landroid/service/notification/StatusBarNotification; */
/* .line 487 */
/* invoke-direct {p0, p1}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->checkNavigationEnd(Landroid/service/notification/StatusBarNotification;)V */
/* .line 488 */
return;
} // .end method
