class com.android.server.MountServiceIdlerImpl$1 extends android.content.BroadcastReceiver {
	 /* .source "MountServiceIdlerImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/MountServiceIdlerImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.MountServiceIdlerImpl this$0; //synthetic
/* # direct methods */
 com.android.server.MountServiceIdlerImpl$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/MountServiceIdlerImpl; */
/* .line 34 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 37 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 38 */
/* .local v0, "action":Ljava/lang/String; */
v1 = this.this$0;
v1 = this.mSm;
/* if-nez v1, :cond_0 */
/* .line 39 */
v1 = this.this$0;
final String v2 = "mount"; // const-string v2, "mount"
android.os.ServiceManager .getService ( v2 );
android.os.storage.IStorageManager$Stub .asInterface ( v2 );
this.mSm = v2;
/* .line 41 */
} // :cond_0
v1 = this.this$0;
v1 = this.mSm;
final String v2 = "MountServiceIdlerImpl"; // const-string v2, "MountServiceIdlerImpl"
/* if-nez v1, :cond_1 */
/* .line 42 */
final String v1 = "Failed to find running mount service"; // const-string v1, "Failed to find running mount service"
android.util.Slog .w ( v2,v1 );
/* .line 43 */
return;
/* .line 47 */
} // :cond_1
try { // :try_start_0
final String v1 = "android.intent.action.SCREEN_ON"; // const-string v1, "android.intent.action.SCREEN_ON"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 48 */
final String v1 = "Get the action of screen on"; // const-string v1, "Get the action of screen on"
android.util.Slog .d ( v2,v1 );
/* .line 49 */
v1 = this.this$0;
v1 = this.mSm;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 53 */
} // :cond_2
/* .line 51 */
/* :catch_0 */
/* move-exception v1 */
/* .line 52 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v3 = "Failed to send stop defrag or trim command to vold"; // const-string v3, "Failed to send stop defrag or trim command to vold"
android.util.Slog .w ( v2,v3,v1 );
/* .line 54 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
