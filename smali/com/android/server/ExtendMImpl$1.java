class com.android.server.ExtendMImpl$1 extends android.content.BroadcastReceiver {
	 /* .source "ExtendMImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/ExtendMImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.ExtendMImpl this$0; //synthetic
/* # direct methods */
 com.android.server.ExtendMImpl$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/ExtendMImpl; */
/* .line 946 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 13 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 949 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 950 */
/* .local v0, "action":Ljava/lang/String; */
final String v1 = "android.intent.action.SCREEN_OFF"; // const-string v1, "android.intent.action.SCREEN_OFF"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* const-wide/16 v2, 0x0 */
/* const-wide/32 v4, 0xea60 */
final String v6 = "ExtM"; // const-string v6, "ExtM"
if ( v1 != null) { // if-eqz v1, :cond_4
	 /* .line 951 */
	 v1 = 	 com.android.server.ExtendMImpl .-$$Nest$sfgetLOG_VERBOSE ( );
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 952 */
		 final String v1 = "-----------------------"; // const-string v1, "-----------------------"
		 android.util.Slog .d ( v6,v1 );
		 /* .line 953 */
		 final String v1 = "Screen off"; // const-string v1, "Screen off"
		 android.util.Slog .d ( v6,v1 );
		 /* .line 955 */
	 } // :cond_0
	 java.lang.System .currentTimeMillis ( );
	 /* move-result-wide v7 */
	 /* .line 957 */
	 /* .local v7, "mScreenOffTime":J */
	 v1 = this.this$0;
	 com.android.server.ExtendMImpl .-$$Nest$fgetmScreenOnTime ( v1 );
	 /* move-result-wide v9 */
	 /* sub-long v9, v7, v9 */
	 /* const-wide/32 v11, 0x2bf20 */
	 /* cmp-long v1, v9, v11 */
	 /* if-lez v1, :cond_1 */
	 /* .line 958 */
	 v1 = this.this$0;
	 com.android.server.ExtendMImpl .-$$Nest$fgetmMarkDiffTime ( v1 );
	 /* move-result-wide v9 */
	 v11 = this.this$0;
	 com.android.server.ExtendMImpl .-$$Nest$fgetmMarkStartTime ( v11 );
	 /* move-result-wide v11 */
	 /* sub-long v11, v7, v11 */
	 /* add-long/2addr v9, v11 */
	 com.android.server.ExtendMImpl .-$$Nest$sfgetMARK_DURATION ( );
	 /* move-result-wide v11 */
	 /* rem-long/2addr v9, v11 */
	 com.android.server.ExtendMImpl .-$$Nest$fputmMarkDiffTime ( v1,v9,v10 );
	 /* .line 959 */
	 v1 = this.this$0;
	 com.android.server.ExtendMImpl .-$$Nest$fgetmFlushDiffTime ( v1 );
	 /* move-result-wide v9 */
	 v11 = this.this$0;
	 com.android.server.ExtendMImpl .-$$Nest$fgetmScreenOnTime ( v11 );
	 /* move-result-wide v11 */
	 /* sub-long v11, v7, v11 */
	 /* add-long/2addr v9, v11 */
	 com.android.server.ExtendMImpl .-$$Nest$fputmFlushDiffTime ( v1,v9,v10 );
	 /* .line 960 */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v9 = "mark: already wait "; // const-string v9, "mark: already wait "
	 (( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v9 = this.this$0;
	 com.android.server.ExtendMImpl .-$$Nest$fgetmMarkDiffTime ( v9 );
	 /* move-result-wide v9 */
	 /* div-long/2addr v9, v4 */
	 (( java.lang.StringBuilder ) v1 ).append ( v9, v10 ); // invoke-virtual {v1, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
	 final String v9 = "m flush: already wait "; // const-string v9, "m flush: already wait "
	 (( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v9 = this.this$0;
	 com.android.server.ExtendMImpl .-$$Nest$fgetmFlushDiffTime ( v9 );
	 /* move-result-wide v9 */
	 /* div-long/2addr v9, v4 */
	 (( java.lang.StringBuilder ) v1 ).append ( v9, v10 ); // invoke-virtual {v1, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
	 final String v9 = "m"; // const-string v9, "m"
	 (( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .i ( v6,v1 );
	 /* .line 963 */
} // :cond_1
final String v1 = "Screen on--> Screen off diff < 2 minutes"; // const-string v1, "Screen on--> Screen off diff < 2 minutes"
android.util.Slog .i ( v6,v1 );
/* .line 965 */
} // :goto_0
v1 = this.this$0;
com.android.server.ExtendMImpl .-$$Nest$munregisterAlarmClock ( v1 );
/* .line 967 */
v1 = this.this$0;
com.android.server.ExtendMImpl .-$$Nest$fgetmFlushDiffTime ( v1 );
/* move-result-wide v9 */
com.android.server.ExtendMImpl .-$$Nest$sfgetFLUSH_DURATION ( );
/* move-result-wide v11 */
/* cmp-long v1, v9, v11 */
/* if-gez v1, :cond_2 */
v1 = this.this$0;
v1 = com.android.server.ExtendMImpl .-$$Nest$fgetmCurState ( v1 );
int v9 = 3; // const/4 v9, 0x3
/* if-ne v1, v9, :cond_3 */
/* .line 968 */
} // :cond_2
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "screen on up to "; // const-string v9, "screen on up to "
(( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.ExtendMImpl .-$$Nest$sfgetFLUSH_DURATION ( );
/* move-result-wide v9 */
/* div-long/2addr v9, v4 */
(( java.lang.StringBuilder ) v1 ).append ( v9, v10 ); // invoke-virtual {v1, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v4 = "m , try to flush."; // const-string v4, "m , try to flush."
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v6,v1 );
/* .line 970 */
v1 = this.this$0;
int v4 = 1; // const/4 v4, 0x1
com.android.server.ExtendMImpl .-$$Nest$mupdateState ( v1,v4 );
/* .line 971 */
v1 = this.this$0;
com.android.server.ExtendMImpl .-$$Nest$mtryToFlushPages ( v1 );
/* .line 972 */
v1 = this.this$0;
com.android.server.ExtendMImpl .-$$Nest$fputmFlushDiffTime ( v1,v2,v3 );
/* .line 974 */
} // .end local v7 # "mScreenOffTime":J
} // :cond_3
/* goto/16 :goto_2 */
} // :cond_4
final String v1 = "android.intent.action.SCREEN_ON"; // const-string v1, "android.intent.action.SCREEN_ON"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 975 */
v1 = this.this$0;
com.android.server.ExtendMImpl .-$$Nest$mtryToStopFlush ( v1 );
/* .line 976 */
v1 = this.this$0;
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
com.android.server.ExtendMImpl .-$$Nest$fputmScreenOnTime ( v1,v2,v3 );
/* .line 977 */
v1 = this.this$0;
v1 = com.android.server.ExtendMImpl .-$$Nest$mgetQuotaLeft ( v1 );
/* if-lez v1, :cond_8 */
/* .line 978 */
v1 = com.android.server.ExtendMImpl .-$$Nest$sfgetLOG_VERBOSE ( );
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 979 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "registerAlarmClock: "; // const-string v2, "registerAlarmClock: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.ExtendMImpl .-$$Nest$sfgetMARK_DURATION ( );
/* move-result-wide v2 */
v7 = this.this$0;
com.android.server.ExtendMImpl .-$$Nest$fgetmMarkDiffTime ( v7 );
/* move-result-wide v7 */
/* sub-long/2addr v2, v7 */
/* div-long/2addr v2, v4 */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = " m"; // const-string v2, " m"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v6,v1 );
/* .line 982 */
} // :cond_5
v1 = this.this$0;
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
com.android.server.ExtendMImpl .-$$Nest$sfgetMARK_DURATION ( );
/* move-result-wide v4 */
v6 = this.this$0;
com.android.server.ExtendMImpl .-$$Nest$fgetmMarkDiffTime ( v6 );
/* move-result-wide v6 */
/* sub-long/2addr v4, v6 */
/* add-long/2addr v2, v4 */
com.android.server.ExtendMImpl .-$$Nest$mregisterAlarmClock ( v1,v2,v3 );
/* goto/16 :goto_2 */
/* .line 984 */
} // :cond_6
final String v1 = "miui.extm.action.mark"; // const-string v1, "miui.extm.action.mark"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 986 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "mark duration up to "; // const-string v7, "mark duration up to "
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.ExtendMImpl .-$$Nest$sfgetMARK_DURATION ( );
/* move-result-wide v7 */
/* div-long/2addr v7, v4 */
(( java.lang.StringBuilder ) v1 ).append ( v7, v8 ); // invoke-virtual {v1, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v4 = "m , try to mark."; // const-string v4, "m , try to mark."
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v6,v1 );
/* .line 988 */
final String v1 = "Start Mark"; // const-string v1, "Start Mark"
final String v4 = "MFZ"; // const-string v4, "MFZ"
android.util.Slog .d ( v4,v1 );
/* .line 989 */
v1 = this.this$0;
com.android.server.ExtendMImpl .-$$Nest$fgetmHandler ( v1 );
int v5 = 6; // const/4 v5, 0x6
(( com.android.server.ExtendMImpl$MyHandler ) v1 ).sendEmptyMessage ( v5 ); // invoke-virtual {v1, v5}, Lcom/android/server/ExtendMImpl$MyHandler;->sendEmptyMessage(I)Z
/* .line 991 */
try { // :try_start_0
/* new-instance v1, Ljava/io/File; */
final String v5 = "/sys/block/zram0/idle_stat"; // const-string v5, "/sys/block/zram0/idle_stat"
/* invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
final String v5 = ""; // const-string v5, ""
/* .line 992 */
/* const/16 v7, 0x80 */
android.os.FileUtils .readTextFile ( v1,v7,v5 );
/* .line 993 */
/* .local v1, "idle_stats":Ljava/lang/String; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Mark Finshed ! idle_stats: "; // const-string v7, "Mark Finshed ! idle_stats: "
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v5 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 996 */
/* nop */
} // .end local v1 # "idle_stats":Ljava/lang/String;
/* .line 994 */
/* :catch_0 */
/* move-exception v1 */
/* .line 995 */
/* .local v1, "e":Ljava/io/IOException; */
final String v5 = "idle_stats: error"; // const-string v5, "idle_stats: error"
android.util.Slog .d ( v4,v5 );
/* .line 997 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :goto_1
v1 = this.this$0;
com.android.server.ExtendMImpl .-$$Nest$fputmMarkDiffTime ( v1,v2,v3 );
/* .line 998 */
v1 = this.this$0;
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
com.android.server.ExtendMImpl .-$$Nest$fputmMarkStartTime ( v1,v2,v3 );
/* .line 999 */
v1 = com.android.server.ExtendMImpl .-$$Nest$sfgetLOG_VERBOSE ( );
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 1000 */
/* new-instance v1, Ljava/text/SimpleDateFormat; */
/* const-string/jumbo v2, "yyyy-MM-dd HH:mm:ss" */
/* invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V */
/* .line 1001 */
/* .local v1, "mFmt":Ljava/text/SimpleDateFormat; */
/* new-instance v2, Ljava/util/Date; */
v3 = this.this$0;
com.android.server.ExtendMImpl .-$$Nest$fgetmMarkStartTime ( v3 );
/* move-result-wide v3 */
/* invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V */
(( java.text.SimpleDateFormat ) v1 ).format ( v2 ); // invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
/* .line 1002 */
/* .local v2, "date":Ljava/lang/String; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "mMarkStartTime: "; // const-string v4, "mMarkStartTime: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " ,reset it"; // const-string v4, " ,reset it"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v6,v3 );
/* .line 1004 */
} // .end local v1 # "mFmt":Ljava/text/SimpleDateFormat;
} // .end local v2 # "date":Ljava/lang/String;
} // :cond_7
v1 = this.this$0;
v1 = com.android.server.ExtendMImpl .-$$Nest$mgetQuotaLeft ( v1 );
/* if-lez v1, :cond_8 */
/* .line 1005 */
v1 = this.this$0;
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
com.android.server.ExtendMImpl .-$$Nest$sfgetMARK_DURATION ( );
/* move-result-wide v4 */
/* add-long/2addr v2, v4 */
com.android.server.ExtendMImpl .-$$Nest$mregisterAlarmClock ( v1,v2,v3 );
/* .line 1008 */
} // :cond_8
} // :goto_2
return;
} // .end method
