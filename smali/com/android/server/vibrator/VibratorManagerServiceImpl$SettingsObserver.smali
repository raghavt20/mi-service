.class final Lcom/android/server/vibrator/VibratorManagerServiceImpl$SettingsObserver;
.super Landroid/database/ContentObserver;
.source "VibratorManagerServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/vibrator/VibratorManagerServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/vibrator/VibratorManagerServiceImpl;


# direct methods
.method constructor <init>(Lcom/android/server/vibrator/VibratorManagerServiceImpl;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .line 442
    iput-object p1, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/vibrator/VibratorManagerServiceImpl;

    .line 443
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 444
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 6
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 448
    const-string v0, "VibratorManagerServiceImpl"

    iget-object v1, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/vibrator/VibratorManagerServiceImpl;

    invoke-static {v1}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->-$$Nest$fgetmVibrator(Lcom/android/server/vibrator/VibratorManagerServiceImpl;)Landroid/os/Vibrator;

    move-result-object v1

    if-nez v1, :cond_0

    .line 449
    iget-object v1, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/vibrator/VibratorManagerServiceImpl;

    invoke-static {v1}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->-$$Nest$fgetmContext(Lcom/android/server/vibrator/VibratorManagerServiceImpl;)Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "vibrator"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Vibrator;

    invoke-static {v1, v2}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->-$$Nest$fputmVibrator(Lcom/android/server/vibrator/VibratorManagerServiceImpl;Landroid/os/Vibrator;)V

    .line 452
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/vibrator/VibratorManagerServiceImpl;

    invoke-static {v1}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->-$$Nest$fgetmHapticFeedbackInfiniteIntensityUri(Lcom/android/server/vibrator/VibratorManagerServiceImpl;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, -0x2

    if-eqz v1, :cond_1

    .line 453
    iget-object v1, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/vibrator/VibratorManagerServiceImpl;

    invoke-static {v1}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->-$$Nest$fgetmHalExt(Lcom/android/server/vibrator/VibratorManagerServiceImpl;)Lvendor/hardware/vibratorfeature/IVibratorExt;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/vibrator/VibratorManagerServiceImpl;

    invoke-static {v3}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->-$$Nest$fgetmContext(Lcom/android/server/vibrator/VibratorManagerServiceImpl;)Landroid/content/Context;

    move-result-object v3

    .line 454
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "haptic_feedback_infinite_intensity"

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static {v3, v4, v5, v2}, Landroid/provider/Settings$System;->getFloatForUser(Landroid/content/ContentResolver;Ljava/lang/String;FI)F

    move-result v2

    .line 453
    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lvendor/hardware/vibratorfeature/IVibratorExt;->setAmplitudeExt(FI)V

    goto/16 :goto_0

    .line 456
    :cond_1
    iget-object v1, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/vibrator/VibratorManagerServiceImpl;

    invoke-static {v1}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->-$$Nest$fgetmHapticFeedbackDisableUri(Lcom/android/server/vibrator/VibratorManagerServiceImpl;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 457
    iget-object v1, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/vibrator/VibratorManagerServiceImpl;

    invoke-static {v1}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->-$$Nest$fgetmContext(Lcom/android/server/vibrator/VibratorManagerServiceImpl;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v3, "haptic_feedback_disable"

    invoke-static {v1, v3, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    invoke-static {v1}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->-$$Nest$sfputhapticFeedbackDisable(I)V

    .line 459
    invoke-static {}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->-$$Nest$sfgethapticFeedbackDisable()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 460
    iget-object v1, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/vibrator/VibratorManagerServiceImpl;

    invoke-static {v1}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->-$$Nest$fgetmHal(Lcom/android/server/vibrator/VibratorManagerServiceImpl;)Landroid/hardware/vibrator/IVibrator;

    move-result-object v1

    invoke-interface {v1}, Landroid/hardware/vibrator/IVibrator;->off()V

    .line 461
    iget-object v1, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/vibrator/VibratorManagerServiceImpl;

    invoke-static {v1}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->-$$Nest$fgetmHalExt(Lcom/android/server/vibrator/VibratorManagerServiceImpl;)Lvendor/hardware/vibratorfeature/IVibratorExt;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-interface {v1, v2, v3}, Lvendor/hardware/vibratorfeature/IVibratorExt;->setAmplitudeExt(FI)V

    .line 463
    iget-object v1, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/vibrator/VibratorManagerServiceImpl;

    invoke-static {v1}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->-$$Nest$fgetmVibrator(Lcom/android/server/vibrator/VibratorManagerServiceImpl;)Landroid/os/Vibrator;

    move-result-object v1

    const/16 v2, 0x21

    invoke-virtual {v1, v2}, Landroid/os/Vibrator;->cancel(I)V

    .line 464
    const-string/jumbo v1, "start disable haptic feedback"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 470
    :catch_0
    move-exception v1

    .line 471
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "skip setAmplitudeExt with flag "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 467
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    .line 468
    .local v1, "e":Landroid/os/DeadObjectException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "fail to setAmplitudeExt, reset mHalExt "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 469
    iget-object v0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl$SettingsObserver;->this$0:Lcom/android/server/vibrator/VibratorManagerServiceImpl;

    invoke-static {v0}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->-$$Nest$mgetHalExt(Lcom/android/server/vibrator/VibratorManagerServiceImpl;)Lvendor/hardware/vibratorfeature/IVibratorExt;

    .line 472
    .end local v1    # "e":Landroid/os/DeadObjectException;
    :cond_2
    :goto_0
    nop

    .line 473
    :goto_1
    return-void
.end method
