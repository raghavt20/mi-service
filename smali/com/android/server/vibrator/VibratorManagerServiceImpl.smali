.class public Lcom/android/server/vibrator/VibratorManagerServiceImpl;
.super Lcom/android/server/vibrator/VibratorManagerServiceStub;
.source "VibratorManagerServiceImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.vibrator.VibratorManagerServiceStub$$"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/vibrator/VibratorManagerServiceImpl$SettingsObserver;
    }
.end annotation


# static fields
.field private static final DEVICE:Ljava/lang/String;

.field private static final DYNAMIC_EFFECT_IGNORE_TIMEOUT:J = 0xbb8L

.field private static final HAPTIC_EFFECT_CONFIG_STRENGTH:Ljava/lang/String; = "haptic_feedback_config_strength"

.field private static final HAPTIC_FEEDBACK_DISABLE:Ljava/lang/String; = "haptic_feedback_disable"

.field private static final HAPTIC_FEEDBACK_INFINITE_INTENSITY:Ljava/lang/String; = "haptic_feedback_infinite_intensity"

.field private static final MAX_VIBRATOR_TIMEOUT:J = 0x2710L

.field private static final PERFECT_VIBRATOR_TIMEOUT:J = 0x3e8L

.field private static final TAG:Ljava/lang/String; = "VibratorManagerServiceImpl"

.field private static final USAGE_DYNAMICEFFECT:I = -0x2

.field private static VIBRATION_THRESHOLD_IN_CALL_LINEAR:J

.field private static VIBRATION_THRESHOLD_IN_CALL_MOTOR:J

.field private static hapticFeedbackDisable:I

.field private static lastEffectID:Ljava/lang/String;

.field private static lastPackageName:Ljava/lang/String;

.field private static final mIgnoreVibrationWhenCamera:Z

.field private static final mSupportVibrationOneTrack:Z

.field private static final pattern:Ljava/util/regex/Pattern;

.field private static vibrationCount:I


# instance fields
.field private final VIBRATOR_IGNORE_LIST:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private isLinearOrZLinear:Z

.field private final mAvailabilityCallback:Landroid/hardware/camera2/CameraManager$AvailabilityCallback;

.field private mCameraManager:Landroid/hardware/camera2/CameraManager;

.field private mCameraUnAvailableSets:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mCurrentDynamicEffect:Lcom/android/server/vibrator/Vibration;

.field private mHal:Landroid/hardware/vibrator/IVibrator;

.field private mHalExt:Lvendor/hardware/vibratorfeature/IVibratorExt;

.field private mHapticFeedbackDisableUri:Landroid/net/Uri;

.field private mHapticFeedbackInfiniteIntensityUri:Landroid/net/Uri;

.field private mIncall:Z

.field private mListener:Landroid/telephony/PhoneStateListener;

.field private volatile mMQSThread:Lcom/android/server/MQSThread;

.field private mSettingObserver:Lcom/android/server/vibrator/VibratorManagerServiceImpl$SettingsObserver;

.field private mVibrator:Landroid/os/Vibrator;


# direct methods
.method static bridge synthetic -$$Nest$fgetmCameraUnAvailableSets(Lcom/android/server/vibrator/VibratorManagerServiceImpl;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mCameraUnAvailableSets:Ljava/util/Set;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/vibrator/VibratorManagerServiceImpl;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHal(Lcom/android/server/vibrator/VibratorManagerServiceImpl;)Landroid/hardware/vibrator/IVibrator;
    .locals 0

    iget-object p0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mHal:Landroid/hardware/vibrator/IVibrator;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHalExt(Lcom/android/server/vibrator/VibratorManagerServiceImpl;)Lvendor/hardware/vibratorfeature/IVibratorExt;
    .locals 0

    iget-object p0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mHalExt:Lvendor/hardware/vibratorfeature/IVibratorExt;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHapticFeedbackDisableUri(Lcom/android/server/vibrator/VibratorManagerServiceImpl;)Landroid/net/Uri;
    .locals 0

    iget-object p0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mHapticFeedbackDisableUri:Landroid/net/Uri;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHapticFeedbackInfiniteIntensityUri(Lcom/android/server/vibrator/VibratorManagerServiceImpl;)Landroid/net/Uri;
    .locals 0

    iget-object p0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mHapticFeedbackInfiniteIntensityUri:Landroid/net/Uri;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmVibrator(Lcom/android/server/vibrator/VibratorManagerServiceImpl;)Landroid/os/Vibrator;
    .locals 0

    iget-object p0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mVibrator:Landroid/os/Vibrator;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmIncall(Lcom/android/server/vibrator/VibratorManagerServiceImpl;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mIncall:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmVibrator(Lcom/android/server/vibrator/VibratorManagerServiceImpl;Landroid/os/Vibrator;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mVibrator:Landroid/os/Vibrator;

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetHalExt(Lcom/android/server/vibrator/VibratorManagerServiceImpl;)Lvendor/hardware/vibratorfeature/IVibratorExt;
    .locals 0

    invoke-direct {p0}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->getHalExt()Lvendor/hardware/vibratorfeature/IVibratorExt;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$sfgethapticFeedbackDisable()I
    .locals 1

    sget v0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->hapticFeedbackDisable:I

    return v0
.end method

.method static bridge synthetic -$$Nest$sfputhapticFeedbackDisable(I)V
    .locals 0

    sput p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->hapticFeedbackDisable:I

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 47
    const-wide/16 v0, 0x1e

    sput-wide v0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->VIBRATION_THRESHOLD_IN_CALL_LINEAR:J

    .line 49
    const-wide/16 v0, 0x64

    sput-wide v0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->VIBRATION_THRESHOLD_IN_CALL_MOTOR:J

    .line 84
    const-string v0, ""

    sput-object v0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->lastEffectID:Ljava/lang/String;

    .line 86
    sput-object v0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->lastPackageName:Ljava/lang/String;

    .line 88
    const/4 v0, 0x1

    sput v0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->vibrationCount:I

    .line 90
    const-string v0, "effect=(\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->pattern:Ljava/util/regex/Pattern;

    .line 100
    sget-object v0, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->DEVICE:Ljava/lang/String;

    .line 102
    const-string/jumbo v0, "sys.haptic.ignoreWhenCamera"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mIgnoreVibrationWhenCamera:Z

    .line 104
    const-string/jumbo v0, "sys.haptic.onetrack"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mSupportVibrationOneTrack:Z

    .line 131
    sput v1, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->hapticFeedbackDisable:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 46
    invoke-direct {p0}, Lcom/android/server/vibrator/VibratorManagerServiceStub;-><init>()V

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mHal:Landroid/hardware/vibrator/IVibrator;

    .line 72
    iput-object v0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mHalExt:Lvendor/hardware/vibratorfeature/IVibratorExt;

    .line 107
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mCameraUnAvailableSets:Ljava/util/Set;

    .line 109
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->isLinearOrZLinear:Z

    .line 112
    new-instance v0, Lcom/android/server/vibrator/VibratorManagerServiceImpl$1;

    invoke-direct {v0, p0}, Lcom/android/server/vibrator/VibratorManagerServiceImpl$1;-><init>(Lcom/android/server/vibrator/VibratorManagerServiceImpl;)V

    iput-object v0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mAvailabilityCallback:Landroid/hardware/camera2/CameraManager$AvailabilityCallback;

    .line 128
    const-string v0, "com.android.systemui"

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->VIBRATOR_IGNORE_LIST:Ljava/util/List;

    return-void
.end method

.method private getHal()Landroid/hardware/vibrator/IVibrator;
    .locals 5

    .line 409
    const-string v0, "VibratorManagerServiceImpl"

    const/4 v1, 0x0

    :try_start_0
    iget-boolean v2, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->isLinearOrZLinear:Z

    if-nez v2, :cond_0

    .line 410
    const-string/jumbo v2, "skip vibrator hal"

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 411
    return-object v1

    .line 413
    :cond_0
    const-string/jumbo v2, "waiting for vibratorfeature hal begin"

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    const-string v2, "android.hardware.vibrator.IVibrator/vibratorfeature"

    invoke-static {v2}, Landroid/os/ServiceManager;->waitForService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/hardware/vibrator/IVibrator$Stub;->asInterface(Landroid/os/IBinder;)Landroid/hardware/vibrator/IVibrator;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mHal:Landroid/hardware/vibrator/IVibrator;

    .line 415
    const-string/jumbo v2, "waiting for vibratorfeature hal end"

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 420
    goto :goto_0

    .line 417
    :catch_0
    move-exception v2

    .line 418
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "fail to get vibrator hal server "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    iput-object v1, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mHal:Landroid/hardware/vibrator/IVibrator;

    .line 421
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_0
    iget-object v0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mHal:Landroid/hardware/vibrator/IVibrator;

    return-object v0
.end method

.method private getHalExt()Lvendor/hardware/vibratorfeature/IVibratorExt;
    .locals 5

    .line 425
    const-string v0, "VibratorManagerServiceImpl"

    invoke-direct {p0}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->getHal()Landroid/hardware/vibrator/IVibrator;

    .line 427
    const/4 v1, 0x0

    :try_start_0
    iget-boolean v2, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->isLinearOrZLinear:Z

    if-nez v2, :cond_0

    .line 428
    const-string/jumbo v2, "skip vibrator halExt"

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    return-object v1

    .line 431
    :cond_0
    invoke-direct {p0}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->getHal()Landroid/hardware/vibrator/IVibrator;

    move-result-object v2

    invoke-interface {v2}, Landroid/hardware/vibrator/IVibrator;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-interface {v2}, Landroid/os/IBinder;->getExtension()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub;->asInterface(Landroid/os/IBinder;)Lvendor/hardware/vibratorfeature/IVibratorExt;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mHalExt:Lvendor/hardware/vibratorfeature/IVibratorExt;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 436
    goto :goto_0

    .line 433
    :catch_0
    move-exception v2

    .line 434
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "fail to get VibratorExt server "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 435
    iput-object v1, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mHalExt:Lvendor/hardware/vibratorfeature/IVibratorExt;

    .line 437
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_0
    iget-object v0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mHalExt:Lvendor/hardware/vibratorfeature/IVibratorExt;

    return-object v0
.end method

.method private listenForCallState()V
    .locals 3

    .line 175
    new-instance v0, Lcom/android/server/vibrator/VibratorManagerServiceImpl$2;

    invoke-direct {v0, p0}, Lcom/android/server/vibrator/VibratorManagerServiceImpl$2;-><init>(Lcom/android/server/vibrator/VibratorManagerServiceImpl;)V

    iput-object v0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mListener:Landroid/telephony/PhoneStateListener;

    .line 181
    iget-object v0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mListener:Landroid/telephony/PhoneStateListener;

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 182
    return-void
.end method

.method private registerSettingsObserver(Landroid/net/Uri;)V
    .locals 4
    .param p1, "settingUri"    # Landroid/net/Uri;

    .line 169
    iget-object v0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mSettingObserver:Lcom/android/server/vibrator/VibratorManagerServiceImpl$SettingsObserver;

    const/4 v2, -0x1

    const/4 v3, 0x1

    invoke-virtual {v0, p1, v3, v1, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 172
    return-void
.end method


# virtual methods
.method public calculateVibrateForMiui(Landroid/os/CombinedVibration;Landroid/os/VibrationAttributes;Ljava/lang/String;)Landroid/os/CombinedVibration;
    .locals 1
    .param p1, "effect"    # Landroid/os/CombinedVibration;
    .param p2, "attrs"    # Landroid/os/VibrationAttributes;
    .param p3, "opPkg"    # Ljava/lang/String;

    .line 186
    if-nez p2, :cond_0

    new-instance v0, Landroid/os/VibrationAttributes$Builder;

    invoke-direct {v0}, Landroid/os/VibrationAttributes$Builder;-><init>()V

    invoke-virtual {v0}, Landroid/os/VibrationAttributes$Builder;->build()Landroid/os/VibrationAttributes;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, p2

    :goto_0
    invoke-virtual {p0, v0, p3}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->shouldIgnoredVibration(Landroid/os/VibrationAttributes;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 187
    const/4 v0, 0x0

    return-object v0

    .line 189
    :cond_1
    return-object p1
.end method

.method public cancelDynamicEffectIfItIs(I)Z
    .locals 5
    .param p1, "usageFilter"    # I

    .line 369
    const-string v0, "VibratorManagerServiceImpl"

    const/4 v1, 0x0

    .line 370
    .local v1, "success":Z
    const/4 v2, -0x2

    if-ne p1, v2, :cond_0

    iget-object v2, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mCurrentDynamicEffect:Lcom/android/server/vibrator/Vibration;

    if-eqz v2, :cond_0

    .line 372
    :try_start_0
    iget-object v2, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mHal:Landroid/hardware/vibrator/IVibrator;

    invoke-interface {v2}, Landroid/hardware/vibrator/IVibrator;->off()V

    .line 373
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mCurrentDynamicEffect:Lcom/android/server/vibrator/Vibration;
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 374
    const/4 v1, 0x1

    .line 380
    :goto_0
    goto :goto_1

    .line 378
    :catch_0
    move-exception v2

    .line 379
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "skip cancel dynamicEffect "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 375
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v2

    .line 376
    .local v2, "e":Landroid/os/DeadObjectException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "fail to cancel dynamicEffect, reset mHalExt "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    invoke-direct {p0}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->getHalExt()Lvendor/hardware/vibratorfeature/IVibratorExt;

    .end local v2    # "e":Landroid/os/DeadObjectException;
    goto :goto_0

    .line 382
    :cond_0
    :goto_1
    return v1
.end method

.method public init(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .line 135
    iput-object p1, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mContext:Landroid/content/Context;

    .line 136
    const-string/jumbo v0, "sys.haptic.motor"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "linear"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x0

    if-nez v2, :cond_1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "zlinear"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    move v0, v3

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->isLinearOrZLinear:Z

    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isLinearOrZLinear is "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->isLinearOrZLinear:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "VibratorManagerServiceImpl"

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    invoke-direct {p0}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->listenForCallState()V

    .line 139
    sget-boolean v0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mIgnoreVibrationWhenCamera:Z

    if-eqz v0, :cond_2

    .line 140
    const-string v0, "camera"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/camera2/CameraManager;

    iput-object v0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mCameraManager:Landroid/hardware/camera2/CameraManager;

    .line 141
    iget-object v4, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mAvailabilityCallback:Landroid/hardware/camera2/CameraManager$AvailabilityCallback;

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/hardware/camera2/CameraManager;->registerAvailabilityCallback(Landroid/hardware/camera2/CameraManager$AvailabilityCallback;Landroid/os/Handler;)V

    .line 143
    :cond_2
    new-instance v0, Lcom/android/server/vibrator/VibratorManagerServiceImpl$SettingsObserver;

    new-instance v4, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, p0, v4}, Lcom/android/server/vibrator/VibratorManagerServiceImpl$SettingsObserver;-><init>(Lcom/android/server/vibrator/VibratorManagerServiceImpl;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mSettingObserver:Lcom/android/server/vibrator/VibratorManagerServiceImpl$SettingsObserver;

    .line 144
    const-string v0, "haptic_feedback_infinite_intensity"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    iput-object v4, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mHapticFeedbackInfiniteIntensityUri:Landroid/net/Uri;

    .line 145
    invoke-direct {p0, v4}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->registerSettingsObserver(Landroid/net/Uri;)V

    .line 146
    invoke-direct {p0}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->getHalExt()Lvendor/hardware/vibratorfeature/IVibratorExt;

    .line 148
    :try_start_0
    const-string/jumbo v4, "sys.haptic.slide_version"

    invoke-static {v4, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "2.0"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_2

    :cond_3
    const/high16 v1, 0x3fc00000    # 1.5f

    .line 149
    .local v1, "amplitudeDefault":F
    :goto_2
    iget-object v4, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mHalExt:Lvendor/hardware/vibratorfeature/IVibratorExt;

    iget-object v5, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mContext:Landroid/content/Context;

    .line 150
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const/4 v6, -0x2

    invoke-static {v5, v0, v1, v6}, Landroid/provider/Settings$System;->getFloatForUser(Landroid/content/ContentResolver;Ljava/lang/String;FI)F

    move-result v0

    .line 149
    invoke-interface {v4, v0, v3}, Lvendor/hardware/vibratorfeature/IVibratorExt;->setAmplitudeExt(FI)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "amplitudeDefault":F
    goto :goto_3

    .line 155
    :catch_0
    move-exception v0

    .line 156
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "skip init amplitude ext "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 152
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 153
    .local v0, "e":Landroid/os/DeadObjectException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "fail to init amplitude ext, reset mHalExt "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    invoke-direct {p0}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->getHalExt()Lvendor/hardware/vibratorfeature/IVibratorExt;

    .line 157
    .end local v0    # "e":Landroid/os/DeadObjectException;
    :goto_3
    nop

    .line 161
    :goto_4
    iget-object v0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mContext:Landroid/content/Context;

    .line 162
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 161
    const-string v1, "haptic_feedback_disable"

    invoke-static {v0, v1, v3, v3}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 164
    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mHapticFeedbackDisableUri:Landroid/net/Uri;

    .line 165
    invoke-direct {p0, v0}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->registerSettingsObserver(Landroid/net/Uri;)V

    .line 166
    return-void
.end method

.method public playDynamicEffectIfItIs(Lcom/android/server/vibrator/Vibration;)Z
    .locals 8
    .param p1, "vib"    # Lcom/android/server/vibrator/Vibration;

    .line 349
    const-string v0, "VibratorManagerServiceImpl"

    invoke-virtual {p1}, Lcom/android/server/vibrator/Vibration;->getEffect()Landroid/os/CombinedVibration;

    move-result-object v1

    instance-of v1, v1, Landroid/os/DynamicEffect;

    if-eqz v1, :cond_0

    .line 350
    iput-object p1, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mCurrentDynamicEffect:Lcom/android/server/vibrator/Vibration;

    .line 351
    invoke-virtual {p1}, Lcom/android/server/vibrator/Vibration;->getEffect()Landroid/os/CombinedVibration;

    move-result-object v1

    check-cast v1, Landroid/os/DynamicEffect;

    .line 353
    .local v1, "d":Landroid/os/DynamicEffect;
    :try_start_0
    iget-object v2, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mHalExt:Lvendor/hardware/vibratorfeature/IVibratorExt;

    invoke-virtual {v1}, Landroid/os/DynamicEffect;->encapsulate()[I

    move-result-object v3

    iget v4, v1, Landroid/os/DynamicEffect;->mLoop:I

    iget v5, v1, Landroid/os/DynamicEffect;->mInterval:I

    int-to-long v5, v5

    const/4 v7, -0x1

    invoke-interface/range {v2 .. v7}, Lvendor/hardware/vibratorfeature/IVibratorExt;->play([IIJI)V

    .line 354
    new-instance v2, Lcom/android/server/MQSThread;

    iget-object v3, p1, Lcom/android/server/vibrator/Vibration;->callerInfo:Lcom/android/server/vibrator/Vibration$CallerInfo;

    iget-object v3, v3, Lcom/android/server/vibrator/Vibration$CallerInfo;->opPkg:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/android/server/MQSThread;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mMQSThread:Lcom/android/server/MQSThread;

    .line 355
    iget-object v2, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mMQSThread:Lcom/android/server/MQSThread;

    invoke-virtual {v2}, Lcom/android/server/MQSThread;->start()V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 359
    :catch_0
    move-exception v2

    .line 360
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "skip play dynamicEffect "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 356
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v2

    .line 357
    .local v2, "e":Landroid/os/DeadObjectException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "fail to play dynamicEffect, reset mHalExt "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    invoke-direct {p0}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->getHalExt()Lvendor/hardware/vibratorfeature/IVibratorExt;

    .line 361
    .end local v2    # "e":Landroid/os/DeadObjectException;
    :goto_0
    nop

    .line 362
    :goto_1
    const/4 v0, 0x1

    return v0

    .line 364
    .end local v1    # "d":Landroid/os/DynamicEffect;
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public processTaggedReasonIfItHas(Lcom/android/server/vibrator/Vibration;)Z
    .locals 12
    .param p1, "vib"    # Lcom/android/server/vibrator/Vibration;

    .line 304
    iget-object v0, p1, Lcom/android/server/vibrator/Vibration;->callerInfo:Lcom/android/server/vibrator/Vibration$CallerInfo;

    iget-object v0, v0, Lcom/android/server/vibrator/Vibration$CallerInfo;->reason:Ljava/lang/String;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 305
    return v1

    .line 307
    :cond_0
    iget-object v0, p1, Lcom/android/server/vibrator/Vibration;->callerInfo:Lcom/android/server/vibrator/Vibration$CallerInfo;

    iget-object v0, v0, Lcom/android/server/vibrator/Vibration$CallerInfo;->reason:Ljava/lang/String;

    const-string v2, ";"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 308
    .local v0, "settingsInfo":[Ljava/lang/String;
    aget-object v2, v0, v1

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v1

    const-string v4, "TAG"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    aget-object v2, v0, v1

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    const/4 v5, 0x2

    if-ne v2, v5, :cond_4

    .line 309
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 310
    .local v2, "settingsMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    array-length v6, v0

    move v7, v1

    :goto_0
    const/4 v8, 0x1

    if-ge v7, v6, :cond_2

    aget-object v9, v0, v7

    .line 311
    .local v9, "settingInfo":Ljava/lang/String;
    invoke-virtual {v9, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 312
    .local v10, "setting":[Ljava/lang/String;
    array-length v11, v10

    if-ne v11, v5, :cond_1

    .line 313
    aget-object v11, v10, v1

    aget-object v8, v10, v8

    invoke-virtual {v2, v11, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 310
    .end local v9    # "settingInfo":Ljava/lang/String;
    .end local v10    # "setting":[Ljava/lang/String;
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 315
    :cond_2
    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_3
    goto :goto_1

    :sswitch_0
    const-string v4, "haptic_feedback_config_strength"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    move v3, v1

    goto :goto_2

    :sswitch_1
    const-string v4, "haptic_feedback_infinite_intensity"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    move v3, v8

    goto :goto_2

    :goto_1
    const/4 v3, -0x1

    :goto_2
    const-string v4, "intensity"

    const-string v5, "VibratorManagerServiceImpl"

    packed-switch v3, :pswitch_data_0

    .line 340
    return v1

    .line 330
    :pswitch_0
    :try_start_0
    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    .line 331
    .local v1, "intensity":F
    iget-object v3, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mHalExt:Lvendor/hardware/vibratorfeature/IVibratorExt;

    invoke-interface {v3, v1, v8}, Lvendor/hardware/vibratorfeature/IVibratorExt;->setAmplitudeExt(FI)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "intensity":F
    goto :goto_3

    .line 335
    :catch_0
    move-exception v1

    .line 336
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "skip setAmplitudeExt "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 332
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    .line 333
    .local v1, "e":Landroid/os/DeadObjectException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "fail to setAmplitudeExt, reset mHalExt "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    invoke-direct {p0}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->getHalExt()Lvendor/hardware/vibratorfeature/IVibratorExt;

    .line 337
    .end local v1    # "e":Landroid/os/DeadObjectException;
    :goto_3
    nop

    .line 338
    :goto_4
    return v8

    .line 318
    :pswitch_1
    :try_start_1
    const-string v1, "effectId"

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 319
    .local v1, "effectId":I
    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    .line 320
    .local v3, "intensity":F
    iget-object v4, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mHalExt:Lvendor/hardware/vibratorfeature/IVibratorExt;

    invoke-interface {v4, v1, v3}, Lvendor/hardware/vibratorfeature/IVibratorExt;->configStrengthForEffect(IF)V
    :try_end_1
    .catch Landroid/os/DeadObjectException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .end local v1    # "effectId":I
    .end local v3    # "intensity":F
    goto :goto_5

    .line 324
    :catch_2
    move-exception v1

    .line 325
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "skip configStrength "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 321
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v1

    .line 322
    .local v1, "e":Landroid/os/DeadObjectException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "fail to configStrength, reset mHalExt "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    invoke-direct {p0}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->getHalExt()Lvendor/hardware/vibratorfeature/IVibratorExt;

    .line 326
    .end local v1    # "e":Landroid/os/DeadObjectException;
    :goto_5
    nop

    .line 327
    :goto_6
    return v8

    .line 344
    .end local v0    # "settingsInfo":[Ljava/lang/String;
    .end local v2    # "settingsMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_4
    return v1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x605f1ae6 -> :sswitch_1
        -0x8bf9774 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public shouldIgnoreForDynamicEffect(Lcom/android/server/vibrator/Vibration;)Z
    .locals 8
    .param p1, "incomingVibration"    # Lcom/android/server/vibrator/Vibration;

    .line 387
    iget-object v0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mCurrentDynamicEffect:Lcom/android/server/vibrator/Vibration;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    .line 388
    invoke-virtual {p1}, Lcom/android/server/vibrator/Vibration;->getEffect()Landroid/os/CombinedVibration;

    move-result-object v0

    .line 389
    .local v0, "incoming":Landroid/os/CombinedVibration;
    iget-object v2, p1, Lcom/android/server/vibrator/Vibration;->callerInfo:Lcom/android/server/vibrator/Vibration$CallerInfo;

    iget-object v2, v2, Lcom/android/server/vibrator/Vibration$CallerInfo;->attrs:Landroid/os/VibrationAttributes;

    invoke-virtual {v2}, Landroid/os/VibrationAttributes;->getUsage()I

    move-result v2

    const/16 v3, 0x21

    const/4 v4, -0x2

    const-string v5, "VibratorManagerServiceImpl"

    if-eq v2, v3, :cond_2

    iget-object v2, p1, Lcom/android/server/vibrator/Vibration;->callerInfo:Lcom/android/server/vibrator/Vibration$CallerInfo;

    iget-object v2, v2, Lcom/android/server/vibrator/Vibration$CallerInfo;->attrs:Landroid/os/VibrationAttributes;

    invoke-virtual {v2}, Landroid/os/VibrationAttributes;->getUsage()I

    move-result v2

    const/16 v3, 0x11

    if-ne v2, v3, :cond_0

    goto :goto_0

    .line 394
    :cond_0
    instance-of v2, v0, Landroid/os/DynamicEffect;

    if-nez v2, :cond_3

    .line 395
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-object v6, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mCurrentDynamicEffect:Lcom/android/server/vibrator/Vibration;

    iget-object v6, v6, Lcom/android/server/vibrator/Vibration;->callerInfo:Lcom/android/server/vibrator/Vibration$CallerInfo;

    iget-wide v6, v6, Lcom/android/server/vibrator/Vibration$CallerInfo;->startUptimeMillis:J

    sub-long/2addr v2, v6

    const-wide/16 v6, 0xbb8

    cmp-long v2, v2, v6

    if-gez v2, :cond_1

    .line 396
    const-string v1, "Ignoring incoming vibration in favor of current Dynamic Effect"

    invoke-static {v5, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    const/4 v1, 0x1

    return v1

    .line 399
    :cond_1
    const-string v2, "cancel current dynamiceffect"

    invoke-static {v5, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 400
    invoke-virtual {p0, v4}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->cancelDynamicEffectIfItIs(I)Z

    goto :goto_1

    .line 390
    :cond_2
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "stop DynamicEffect in favor of current Effect\'s attribute"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/android/server/vibrator/Vibration;->callerInfo:Lcom/android/server/vibrator/Vibration$CallerInfo;

    iget-object v3, v3, Lcom/android/server/vibrator/Vibration$CallerInfo;->attrs:Landroid/os/VibrationAttributes;

    invoke-virtual {v3}, Landroid/os/VibrationAttributes;->getUsage()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    invoke-virtual {p0, v4}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->cancelDynamicEffectIfItIs(I)Z

    .line 392
    return v1

    .line 404
    .end local v0    # "incoming":Landroid/os/CombinedVibration;
    :cond_3
    :goto_1
    return v1
.end method

.method public shouldIgnoredForRingtoneOrMIUI(Lcom/android/server/vibrator/Vibration$CallerInfo;)Lcom/android/server/vibrator/Vibration$EndInfo;
    .locals 3
    .param p1, "callerInfo"    # Lcom/android/server/vibrator/Vibration$CallerInfo;

    .line 238
    sget-boolean v0, Landroid/os/Build;->IS_MIUI:Z

    if-eqz v0, :cond_0

    .line 239
    invoke-static {}, Lcom/android/server/vibrator/VibratorManagerServiceStub;->getInstance()Lcom/android/server/vibrator/VibratorManagerServiceStub;

    move-result-object v0

    iget-object v1, p1, Lcom/android/server/vibrator/Vibration$CallerInfo;->attrs:Landroid/os/VibrationAttributes;

    iget-object v2, p1, Lcom/android/server/vibrator/Vibration$CallerInfo;->opPkg:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/android/server/vibrator/VibratorManagerServiceStub;->shouldIgnoredVibration(Landroid/os/VibrationAttributes;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 240
    const-string v0, "VibratorManagerServiceImpl"

    const-string v1, "Vibrate ignored, not vibrating for ringtones or notify for MIUI"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    new-instance v0, Lcom/android/server/vibrator/Vibration$EndInfo;

    sget-object v1, Lcom/android/server/vibrator/Vibration$Status;->IGNORED_RINGTONE_OR_NOTIFY_MIUI:Lcom/android/server/vibrator/Vibration$Status;

    invoke-direct {v0, v1}, Lcom/android/server/vibrator/Vibration$EndInfo;-><init>(Lcom/android/server/vibrator/Vibration$Status;)V

    return-object v0

    .line 244
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public shouldIgnoredVibration(Landroid/os/VibrationAttributes;Ljava/lang/String;)Z
    .locals 4
    .param p1, "attrs"    # Landroid/os/VibrationAttributes;
    .param p2, "opPkg"    # Ljava/lang/String;

    .line 195
    invoke-virtual {p1}, Landroid/os/VibrationAttributes;->getUsage()I

    move-result v0

    const/4 v1, 0x1

    const/16 v2, 0x21

    if-eq v0, v2, :cond_0

    .line 196
    invoke-virtual {p1}, Landroid/os/VibrationAttributes;->getUsage()I

    move-result v0

    const/16 v3, 0x31

    if-ne v0, v3, :cond_1

    :cond_0
    nop

    .line 197
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/os/VibrationAttributes;->isFlagSet(I)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mContext:Landroid/content/Context;

    .line 198
    invoke-static {v0}, Lmiui/util/AudioManagerHelper;->isVibrateEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 199
    return v1

    .line 204
    :cond_1
    sget-boolean v0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mIgnoreVibrationWhenCamera:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mCameraUnAvailableSets:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 205
    invoke-virtual {p1}, Landroid/os/VibrationAttributes;->getUsage()I

    move-result v0

    const/16 v3, 0x11

    if-eq v0, v3, :cond_2

    .line 206
    invoke-virtual {p1}, Landroid/os/VibrationAttributes;->getUsage()I

    move-result v0

    if-ne v0, v2, :cond_4

    .line 207
    :cond_2
    iget-object v0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mCameraUnAvailableSets:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mCameraUnAvailableSets:Ljava/util/Set;

    const-string v2, "1"

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 208
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ignore incoming vibration in favor of camera in device  "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "VibratorManagerServiceImpl"

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    return v1

    .line 213
    :cond_4
    invoke-virtual {p1}, Landroid/os/VibrationAttributes;->getUsage()I

    move-result v0

    const/16 v1, 0x12

    const/4 v2, 0x0

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mContext:Landroid/content/Context;

    .line 214
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "haptic_feedback_enabled"

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->VIBRATOR_IGNORE_LIST:Ljava/util/List;

    .line 215
    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 216
    return v2

    .line 218
    :cond_5
    return v2
.end method

.method public startVibrationLockedInjector(Lcom/android/server/vibrator/Vibration;)Lcom/android/server/vibrator/Vibration$Status;
    .locals 10
    .param p1, "vib"    # Lcom/android/server/vibrator/Vibration;

    .line 249
    const/4 v0, 0x0

    .line 250
    .local v0, "injectorInfo":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "attrs = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/android/server/vibrator/Vibration;->callerInfo:Lcom/android/server/vibrator/Vibration$CallerInfo;

    iget-object v2, v2, Lcom/android/server/vibrator/Vibration$CallerInfo;->attrs:Landroid/os/VibrationAttributes;

    invoke-virtual {v2}, Landroid/os/VibrationAttributes;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", attrs.usage = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/android/server/vibrator/Vibration;->callerInfo:Lcom/android/server/vibrator/Vibration$CallerInfo;

    iget-object v2, v2, Lcom/android/server/vibrator/Vibration$CallerInfo;->attrs:Landroid/os/VibrationAttributes;

    invoke-virtual {v2}, Landroid/os/VibrationAttributes;->getUsage()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "VibratorManagerServiceImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    :try_start_0
    iget-object v1, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mHalExt:Lvendor/hardware/vibratorfeature/IVibratorExt;

    iget-object v3, p1, Lcom/android/server/vibrator/Vibration;->callerInfo:Lcom/android/server/vibrator/Vibration$CallerInfo;

    iget-object v3, v3, Lcom/android/server/vibrator/Vibration$CallerInfo;->attrs:Landroid/os/VibrationAttributes;

    invoke-virtual {v3}, Landroid/os/VibrationAttributes;->getUsage()I

    move-result v3

    invoke-interface {v1, v3}, Lvendor/hardware/vibratorfeature/IVibratorExt;->setUsageExt(I)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 256
    :catch_0
    move-exception v1

    .line 257
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "skip"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 253
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    .line 254
    .local v1, "e":Landroid/os/DeadObjectException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "fail"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    invoke-direct {p0}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->getHalExt()Lvendor/hardware/vibratorfeature/IVibratorExt;

    .line 258
    .end local v1    # "e":Landroid/os/DeadObjectException;
    :goto_0
    nop

    .line 259
    :goto_1
    sget v1, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->hapticFeedbackDisable:I

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    .line 260
    const-string v0, "hapticFeedbackDisable"

    goto :goto_2

    .line 261
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->playDynamicEffectIfItIs(Lcom/android/server/vibrator/Vibration;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 262
    const-string v0, "playDynamicEffect"

    goto :goto_2

    .line 263
    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->processTaggedReasonIfItHas(Lcom/android/server/vibrator/Vibration;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 264
    const-string v0, "processTaggedReason"

    .line 265
    :cond_2
    :goto_2
    const-string v1, "playDynamicEffect"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 266
    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    sget-object v1, Lcom/android/server/vibrator/Vibration$Status;->RUNNING:Lcom/android/server/vibrator/Vibration$Status;

    return-object v1

    .line 269
    :cond_3
    const-string v1, "hapticFeedbackDisable"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 270
    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    sget-object v1, Lcom/android/server/vibrator/Vibration$Status;->FINISHED:Lcom/android/server/vibrator/Vibration$Status;

    return-object v1

    .line 273
    :cond_4
    sget-boolean v1, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mSupportVibrationOneTrack:Z

    if-eqz v1, :cond_8

    .line 274
    invoke-virtual {p1}, Lcom/android/server/vibrator/Vibration;->getEffect()Landroid/os/CombinedVibration;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 275
    .local v1, "effect":Ljava/lang/String;
    iget-object v2, p1, Lcom/android/server/vibrator/Vibration;->callerInfo:Lcom/android/server/vibrator/Vibration$CallerInfo;

    iget-object v2, v2, Lcom/android/server/vibrator/Vibration$CallerInfo;->opPkg:Ljava/lang/String;

    .line 276
    .local v2, "opPkg":Ljava/lang/String;
    sget-object v4, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->pattern:Ljava/util/regex/Pattern;

    invoke-virtual {v4, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 277
    .local v4, "matcher":Ljava/util/regex/Matcher;
    const-string v5, ""

    .line 278
    .local v5, "effectID":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 279
    invoke-virtual {v4, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    .line 281
    :cond_5
    sget-object v6, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->lastEffectID:Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    sget-object v6, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->lastPackageName:Ljava/lang/String;

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_6

    goto :goto_3

    .line 287
    :cond_6
    sget-object v6, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->lastEffectID:Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    sget-object v6, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->lastPackageName:Ljava/lang/String;

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 288
    sget v6, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->vibrationCount:I

    add-int/2addr v6, v3

    sput v6, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->vibrationCount:I

    goto :goto_4

    .line 282
    :cond_7
    :goto_3
    new-instance v6, Lcom/android/server/MQSThread;

    sget-object v7, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->lastPackageName:Ljava/lang/String;

    sget-object v8, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->lastEffectID:Ljava/lang/String;

    sget v9, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->vibrationCount:I

    invoke-direct {v6, v7, v8, v9}, Lcom/android/server/MQSThread;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v6, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mMQSThread:Lcom/android/server/MQSThread;

    .line 283
    iget-object v6, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mMQSThread:Lcom/android/server/MQSThread;

    invoke-virtual {v6}, Lcom/android/server/MQSThread;->start()V

    .line 284
    sput v3, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->vibrationCount:I

    .line 285
    sput-object v5, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->lastEffectID:Ljava/lang/String;

    .line 286
    sput-object v2, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->lastPackageName:Ljava/lang/String;

    .line 291
    .end local v1    # "effect":Ljava/lang/String;
    .end local v2    # "opPkg":Ljava/lang/String;
    .end local v4    # "matcher":Ljava/util/regex/Matcher;
    .end local v5    # "effectID":Ljava/lang/String;
    :cond_8
    :goto_4
    const/4 v1, 0x0

    return-object v1
.end method

.method public startVibrationLockedInjector(Landroid/os/ExternalVibration;)Ljava/lang/String;
    .locals 2
    .param p1, "vib"    # Landroid/os/ExternalVibration;

    .line 295
    sget v0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->hapticFeedbackDisable:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 296
    const-string v0, "VibratorManagerServiceImpl"

    const-string v1, "hapticFeedbackDisable"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    return-object v1

    .line 299
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public weakenVibrationIfNecessary(JI)J
    .locals 3
    .param p1, "time"    # J
    .param p3, "uid"    # I

    .line 225
    iget-boolean v0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mIncall:Z

    if-eqz v0, :cond_2

    invoke-static {p3}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->isLinearOrZLinear:Z

    if-eqz v0, :cond_0

    sget-wide v1, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->VIBRATION_THRESHOLD_IN_CALL_LINEAR:J

    goto :goto_0

    :cond_0
    sget-wide v1, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->VIBRATION_THRESHOLD_IN_CALL_MOTOR:J

    :goto_0
    cmp-long v1, p1, v1

    if-lez v1, :cond_2

    .line 226
    if-eqz v0, :cond_1

    sget-wide v0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->VIBRATION_THRESHOLD_IN_CALL_LINEAR:J

    goto :goto_1

    :cond_1
    sget-wide v0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->VIBRATION_THRESHOLD_IN_CALL_MOTOR:J

    :goto_1
    move-wide p1, v0

    goto :goto_2

    .line 228
    :cond_2
    const-wide/16 v0, 0x2710

    cmp-long v0, p1, v0

    if-lez v0, :cond_3

    .line 229
    const-wide/16 p1, 0x3e8

    .line 232
    :cond_3
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "weakenVibrationIfNecessary time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "VibratorManagerServiceImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    return-wide p1
.end method
