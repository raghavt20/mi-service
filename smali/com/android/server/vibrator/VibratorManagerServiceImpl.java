public class com.android.server.vibrator.VibratorManagerServiceImpl extends com.android.server.vibrator.VibratorManagerServiceStub {
	 /* .source "VibratorManagerServiceImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.vibrator.VibratorManagerServiceStub$$" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/android/server/vibrator/VibratorManagerServiceImpl$SettingsObserver; */
/* } */
} // .end annotation
/* # static fields */
private static final java.lang.String DEVICE;
private static final Long DYNAMIC_EFFECT_IGNORE_TIMEOUT;
private static final java.lang.String HAPTIC_EFFECT_CONFIG_STRENGTH;
private static final java.lang.String HAPTIC_FEEDBACK_DISABLE;
private static final java.lang.String HAPTIC_FEEDBACK_INFINITE_INTENSITY;
private static final Long MAX_VIBRATOR_TIMEOUT;
private static final Long PERFECT_VIBRATOR_TIMEOUT;
private static final java.lang.String TAG;
private static final Integer USAGE_DYNAMICEFFECT;
private static Long VIBRATION_THRESHOLD_IN_CALL_LINEAR;
private static Long VIBRATION_THRESHOLD_IN_CALL_MOTOR;
private static Integer hapticFeedbackDisable;
private static java.lang.String lastEffectID;
private static java.lang.String lastPackageName;
private static final Boolean mIgnoreVibrationWhenCamera;
private static final Boolean mSupportVibrationOneTrack;
private static final java.util.regex.Pattern pattern;
private static Integer vibrationCount;
/* # instance fields */
private final java.util.List VIBRATOR_IGNORE_LIST;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean isLinearOrZLinear;
private final android.hardware.camera2.CameraManager$AvailabilityCallback mAvailabilityCallback;
private android.hardware.camera2.CameraManager mCameraManager;
private java.util.Set mCameraUnAvailableSets;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.content.Context mContext;
private com.android.server.vibrator.Vibration mCurrentDynamicEffect;
private android.hardware.vibrator.IVibrator mHal;
private vendor.hardware.vibratorfeature.IVibratorExt mHalExt;
private android.net.Uri mHapticFeedbackDisableUri;
private android.net.Uri mHapticFeedbackInfiniteIntensityUri;
private Boolean mIncall;
private android.telephony.PhoneStateListener mListener;
private volatile com.android.server.MQSThread mMQSThread;
private com.android.server.vibrator.VibratorManagerServiceImpl$SettingsObserver mSettingObserver;
private android.os.Vibrator mVibrator;
/* # direct methods */
static java.util.Set -$$Nest$fgetmCameraUnAvailableSets ( com.android.server.vibrator.VibratorManagerServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCameraUnAvailableSets;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.vibrator.VibratorManagerServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static android.hardware.vibrator.IVibrator -$$Nest$fgetmHal ( com.android.server.vibrator.VibratorManagerServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHal;
} // .end method
static vendor.hardware.vibratorfeature.IVibratorExt -$$Nest$fgetmHalExt ( com.android.server.vibrator.VibratorManagerServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHalExt;
} // .end method
static android.net.Uri -$$Nest$fgetmHapticFeedbackDisableUri ( com.android.server.vibrator.VibratorManagerServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHapticFeedbackDisableUri;
} // .end method
static android.net.Uri -$$Nest$fgetmHapticFeedbackInfiniteIntensityUri ( com.android.server.vibrator.VibratorManagerServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHapticFeedbackInfiniteIntensityUri;
} // .end method
static android.os.Vibrator -$$Nest$fgetmVibrator ( com.android.server.vibrator.VibratorManagerServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mVibrator;
} // .end method
static void -$$Nest$fputmIncall ( com.android.server.vibrator.VibratorManagerServiceImpl p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mIncall:Z */
return;
} // .end method
static void -$$Nest$fputmVibrator ( com.android.server.vibrator.VibratorManagerServiceImpl p0, android.os.Vibrator p1 ) { //bridge//synthethic
/* .locals 0 */
this.mVibrator = p1;
return;
} // .end method
static vendor.hardware.vibratorfeature.IVibratorExt -$$Nest$mgetHalExt ( com.android.server.vibrator.VibratorManagerServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->getHalExt()Lvendor/hardware/vibratorfeature/IVibratorExt; */
} // .end method
static Integer -$$Nest$sfgethapticFeedbackDisable ( ) { //bridge//synthethic
/* .locals 1 */
} // .end method
static void -$$Nest$sfputhapticFeedbackDisable ( Integer p0 ) { //bridge//synthethic
/* .locals 0 */
return;
} // .end method
static com.android.server.vibrator.VibratorManagerServiceImpl ( ) {
/* .locals 2 */
/* .line 47 */
/* const-wide/16 v0, 0x1e */
/* sput-wide v0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->VIBRATION_THRESHOLD_IN_CALL_LINEAR:J */
/* .line 49 */
/* const-wide/16 v0, 0x64 */
/* sput-wide v0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->VIBRATION_THRESHOLD_IN_CALL_MOTOR:J */
/* .line 84 */
final String v0 = ""; // const-string v0, ""
/* .line 86 */
/* .line 88 */
int v0 = 1; // const/4 v0, 0x1
/* .line 90 */
final String v0 = "effect=(\\d+)"; // const-string v0, "effect=(\\d+)"
java.util.regex.Pattern .compile ( v0 );
/* .line 100 */
v0 = miui.os.Build.DEVICE;
(( java.lang.String ) v0 ).toLowerCase ( ); // invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
/* .line 102 */
/* const-string/jumbo v0, "sys.haptic.ignoreWhenCamera" */
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.vibrator.VibratorManagerServiceImpl.mIgnoreVibrationWhenCamera = (v0!= 0);
/* .line 104 */
/* const-string/jumbo v0, "sys.haptic.onetrack" */
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.vibrator.VibratorManagerServiceImpl.mSupportVibrationOneTrack = (v0!= 0);
/* .line 131 */
return;
} // .end method
public com.android.server.vibrator.VibratorManagerServiceImpl ( ) {
/* .locals 1 */
/* .line 46 */
/* invoke-direct {p0}, Lcom/android/server/vibrator/VibratorManagerServiceStub;-><init>()V */
/* .line 70 */
int v0 = 0; // const/4 v0, 0x0
this.mHal = v0;
/* .line 72 */
this.mHalExt = v0;
/* .line 107 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mCameraUnAvailableSets = v0;
/* .line 109 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->isLinearOrZLinear:Z */
/* .line 112 */
/* new-instance v0, Lcom/android/server/vibrator/VibratorManagerServiceImpl$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/vibrator/VibratorManagerServiceImpl$1;-><init>(Lcom/android/server/vibrator/VibratorManagerServiceImpl;)V */
this.mAvailabilityCallback = v0;
/* .line 128 */
final String v0 = "com.android.systemui"; // const-string v0, "com.android.systemui"
/* filled-new-array {v0}, [Ljava/lang/String; */
java.util.Arrays .asList ( v0 );
this.VIBRATOR_IGNORE_LIST = v0;
return;
} // .end method
private android.hardware.vibrator.IVibrator getHal ( ) {
/* .locals 5 */
/* .line 409 */
final String v0 = "VibratorManagerServiceImpl"; // const-string v0, "VibratorManagerServiceImpl"
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
/* iget-boolean v2, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->isLinearOrZLinear:Z */
/* if-nez v2, :cond_0 */
/* .line 410 */
/* const-string/jumbo v2, "skip vibrator hal" */
android.util.Slog .d ( v0,v2 );
/* .line 411 */
/* .line 413 */
} // :cond_0
/* const-string/jumbo v2, "waiting for vibratorfeature hal begin" */
android.util.Slog .d ( v0,v2 );
/* .line 414 */
final String v2 = "android.hardware.vibrator.IVibrator/vibratorfeature"; // const-string v2, "android.hardware.vibrator.IVibrator/vibratorfeature"
android.os.ServiceManager .waitForService ( v2 );
android.hardware.vibrator.IVibrator$Stub .asInterface ( v2 );
this.mHal = v2;
/* .line 415 */
/* const-string/jumbo v2, "waiting for vibratorfeature hal end" */
android.util.Slog .d ( v0,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 420 */
/* .line 417 */
/* :catch_0 */
/* move-exception v2 */
/* .line 418 */
/* .local v2, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "fail to get vibrator hal server "; // const-string v4, "fail to get vibrator hal server "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v3 );
/* .line 419 */
this.mHal = v1;
/* .line 421 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_0
v0 = this.mHal;
} // .end method
private vendor.hardware.vibratorfeature.IVibratorExt getHalExt ( ) {
/* .locals 5 */
/* .line 425 */
final String v0 = "VibratorManagerServiceImpl"; // const-string v0, "VibratorManagerServiceImpl"
/* invoke-direct {p0}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->getHal()Landroid/hardware/vibrator/IVibrator; */
/* .line 427 */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
/* iget-boolean v2, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->isLinearOrZLinear:Z */
/* if-nez v2, :cond_0 */
/* .line 428 */
/* const-string/jumbo v2, "skip vibrator halExt" */
android.util.Slog .d ( v0,v2 );
/* .line 429 */
/* .line 431 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->getHal()Landroid/hardware/vibrator/IVibrator; */
vendor.hardware.vibratorfeature.IVibratorExt$Stub .asInterface ( v2 );
this.mHalExt = v2;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 436 */
/* .line 433 */
/* :catch_0 */
/* move-exception v2 */
/* .line 434 */
/* .local v2, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "fail to get VibratorExt server "; // const-string v4, "fail to get VibratorExt server "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v3 );
/* .line 435 */
this.mHalExt = v1;
/* .line 437 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_0
v0 = this.mHalExt;
} // .end method
private void listenForCallState ( ) {
/* .locals 3 */
/* .line 175 */
/* new-instance v0, Lcom/android/server/vibrator/VibratorManagerServiceImpl$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/vibrator/VibratorManagerServiceImpl$2;-><init>(Lcom/android/server/vibrator/VibratorManagerServiceImpl;)V */
this.mListener = v0;
/* .line 181 */
v0 = this.mContext;
android.telephony.TelephonyManager .from ( v0 );
v1 = this.mListener;
/* const/16 v2, 0x20 */
(( android.telephony.TelephonyManager ) v0 ).listen ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V
/* .line 182 */
return;
} // .end method
private void registerSettingsObserver ( android.net.Uri p0 ) {
/* .locals 4 */
/* .param p1, "settingUri" # Landroid/net/Uri; */
/* .line 169 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v1 = this.mSettingObserver;
int v2 = -1; // const/4 v2, -0x1
int v3 = 1; // const/4 v3, 0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( p1, v3, v1, v2 ); // invoke-virtual {v0, p1, v3, v1, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 172 */
return;
} // .end method
/* # virtual methods */
public android.os.CombinedVibration calculateVibrateForMiui ( android.os.CombinedVibration p0, android.os.VibrationAttributes p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "effect" # Landroid/os/CombinedVibration; */
/* .param p2, "attrs" # Landroid/os/VibrationAttributes; */
/* .param p3, "opPkg" # Ljava/lang/String; */
/* .line 186 */
/* if-nez p2, :cond_0 */
/* new-instance v0, Landroid/os/VibrationAttributes$Builder; */
/* invoke-direct {v0}, Landroid/os/VibrationAttributes$Builder;-><init>()V */
(( android.os.VibrationAttributes$Builder ) v0 ).build ( ); // invoke-virtual {v0}, Landroid/os/VibrationAttributes$Builder;->build()Landroid/os/VibrationAttributes;
} // :cond_0
/* move-object v0, p2 */
} // :goto_0
v0 = (( com.android.server.vibrator.VibratorManagerServiceImpl ) p0 ).shouldIgnoredVibration ( v0, p3 ); // invoke-virtual {p0, v0, p3}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->shouldIgnoredVibration(Landroid/os/VibrationAttributes;Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 187 */
int v0 = 0; // const/4 v0, 0x0
/* .line 189 */
} // :cond_1
} // .end method
public Boolean cancelDynamicEffectIfItIs ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "usageFilter" # I */
/* .line 369 */
final String v0 = "VibratorManagerServiceImpl"; // const-string v0, "VibratorManagerServiceImpl"
int v1 = 0; // const/4 v1, 0x0
/* .line 370 */
/* .local v1, "success":Z */
int v2 = -2; // const/4 v2, -0x2
/* if-ne p1, v2, :cond_0 */
v2 = this.mCurrentDynamicEffect;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 372 */
try { // :try_start_0
v2 = this.mHal;
/* .line 373 */
int v2 = 0; // const/4 v2, 0x0
this.mCurrentDynamicEffect = v2;
/* :try_end_0 */
/* .catch Landroid/os/DeadObjectException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 374 */
int v1 = 1; // const/4 v1, 0x1
/* .line 380 */
} // :goto_0
/* .line 378 */
/* :catch_0 */
/* move-exception v2 */
/* .line 379 */
/* .local v2, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "skip cancel dynamicEffect " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v3 );
/* .line 375 */
} // .end local v2 # "e":Ljava/lang/Exception;
/* :catch_1 */
/* move-exception v2 */
/* .line 376 */
/* .local v2, "e":Landroid/os/DeadObjectException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "fail to cancel dynamicEffect, reset mHalExt "; // const-string v4, "fail to cancel dynamicEffect, reset mHalExt "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v3 );
/* .line 377 */
/* invoke-direct {p0}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->getHalExt()Lvendor/hardware/vibratorfeature/IVibratorExt; */
} // .end local v2 # "e":Landroid/os/DeadObjectException;
/* .line 382 */
} // :cond_0
} // :goto_1
} // .end method
public void init ( android.content.Context p0 ) {
/* .locals 7 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 135 */
this.mContext = p1;
/* .line 136 */
/* const-string/jumbo v0, "sys.haptic.motor" */
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
final String v3 = "linear"; // const-string v3, "linear"
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v3 = 0; // const/4 v3, 0x0
/* if-nez v2, :cond_1 */
android.os.SystemProperties .get ( v0,v1 );
/* const-string/jumbo v2, "zlinear" */
v0 = (( java.lang.String ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
/* move v0, v3 */
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
/* iput-boolean v0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->isLinearOrZLinear:Z */
/* .line 137 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "isLinearOrZLinear is "; // const-string v2, "isLinearOrZLinear is "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->isLinearOrZLinear:Z */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "VibratorManagerServiceImpl"; // const-string v2, "VibratorManagerServiceImpl"
android.util.Slog .w ( v2,v0 );
/* .line 138 */
/* invoke-direct {p0}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->listenForCallState()V */
/* .line 139 */
/* sget-boolean v0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mIgnoreVibrationWhenCamera:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 140 */
final String v0 = "camera"; // const-string v0, "camera"
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/camera2/CameraManager; */
this.mCameraManager = v0;
/* .line 141 */
v4 = this.mAvailabilityCallback;
int v5 = 0; // const/4 v5, 0x0
(( android.hardware.camera2.CameraManager ) v0 ).registerAvailabilityCallback ( v4, v5 ); // invoke-virtual {v0, v4, v5}, Landroid/hardware/camera2/CameraManager;->registerAvailabilityCallback(Landroid/hardware/camera2/CameraManager$AvailabilityCallback;Landroid/os/Handler;)V
/* .line 143 */
} // :cond_2
/* new-instance v0, Lcom/android/server/vibrator/VibratorManagerServiceImpl$SettingsObserver; */
/* new-instance v4, Landroid/os/Handler; */
android.os.Looper .myLooper ( );
/* invoke-direct {v4, v5}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* invoke-direct {v0, p0, v4}, Lcom/android/server/vibrator/VibratorManagerServiceImpl$SettingsObserver;-><init>(Lcom/android/server/vibrator/VibratorManagerServiceImpl;Landroid/os/Handler;)V */
this.mSettingObserver = v0;
/* .line 144 */
final String v0 = "haptic_feedback_infinite_intensity"; // const-string v0, "haptic_feedback_infinite_intensity"
android.provider.Settings$System .getUriFor ( v0 );
this.mHapticFeedbackInfiniteIntensityUri = v4;
/* .line 145 */
/* invoke-direct {p0, v4}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->registerSettingsObserver(Landroid/net/Uri;)V */
/* .line 146 */
/* invoke-direct {p0}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->getHalExt()Lvendor/hardware/vibratorfeature/IVibratorExt; */
/* .line 148 */
try { // :try_start_0
/* const-string/jumbo v4, "sys.haptic.slide_version" */
android.os.SystemProperties .get ( v4,v1 );
final String v4 = "2.0"; // const-string v4, "2.0"
v1 = (( java.lang.String ) v1 ).equals ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* const/high16 v1, 0x3f800000 # 1.0f */
} // :cond_3
/* const/high16 v1, 0x3fc00000 # 1.5f */
/* .line 149 */
/* .local v1, "amplitudeDefault":F */
} // :goto_2
v4 = this.mHalExt;
v5 = this.mContext;
/* .line 150 */
(( android.content.Context ) v5 ).getContentResolver ( ); // invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v6 = -2; // const/4 v6, -0x2
v0 = android.provider.Settings$System .getFloatForUser ( v5,v0,v1,v6 );
/* .line 149 */
/* :try_end_0 */
/* .catch Landroid/os/DeadObjectException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
} // .end local v1 # "amplitudeDefault":F
/* .line 155 */
/* :catch_0 */
/* move-exception v0 */
/* .line 156 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "skip init amplitude ext " */
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v1 );
/* .line 152 */
} // .end local v0 # "e":Ljava/lang/Exception;
/* :catch_1 */
/* move-exception v0 */
/* .line 153 */
/* .local v0, "e":Landroid/os/DeadObjectException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "fail to init amplitude ext, reset mHalExt "; // const-string v4, "fail to init amplitude ext, reset mHalExt "
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v1 );
/* .line 154 */
/* invoke-direct {p0}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->getHalExt()Lvendor/hardware/vibratorfeature/IVibratorExt; */
/* .line 157 */
} // .end local v0 # "e":Landroid/os/DeadObjectException;
} // :goto_3
/* nop */
/* .line 161 */
} // :goto_4
v0 = this.mContext;
/* .line 162 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 161 */
final String v1 = "haptic_feedback_disable"; // const-string v1, "haptic_feedback_disable"
android.provider.Settings$System .putIntForUser ( v0,v1,v3,v3 );
/* .line 164 */
android.provider.Settings$System .getUriFor ( v1 );
this.mHapticFeedbackDisableUri = v0;
/* .line 165 */
/* invoke-direct {p0, v0}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->registerSettingsObserver(Landroid/net/Uri;)V */
/* .line 166 */
return;
} // .end method
public Boolean playDynamicEffectIfItIs ( com.android.server.vibrator.Vibration p0 ) {
/* .locals 8 */
/* .param p1, "vib" # Lcom/android/server/vibrator/Vibration; */
/* .line 349 */
final String v0 = "VibratorManagerServiceImpl"; // const-string v0, "VibratorManagerServiceImpl"
(( com.android.server.vibrator.Vibration ) p1 ).getEffect ( ); // invoke-virtual {p1}, Lcom/android/server/vibrator/Vibration;->getEffect()Landroid/os/CombinedVibration;
/* instance-of v1, v1, Landroid/os/DynamicEffect; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 350 */
this.mCurrentDynamicEffect = p1;
/* .line 351 */
(( com.android.server.vibrator.Vibration ) p1 ).getEffect ( ); // invoke-virtual {p1}, Lcom/android/server/vibrator/Vibration;->getEffect()Landroid/os/CombinedVibration;
/* check-cast v1, Landroid/os/DynamicEffect; */
/* .line 353 */
/* .local v1, "d":Landroid/os/DynamicEffect; */
try { // :try_start_0
v2 = this.mHalExt;
(( android.os.DynamicEffect ) v1 ).encapsulate ( ); // invoke-virtual {v1}, Landroid/os/DynamicEffect;->encapsulate()[I
/* iget v4, v1, Landroid/os/DynamicEffect;->mLoop:I */
/* iget v5, v1, Landroid/os/DynamicEffect;->mInterval:I */
/* int-to-long v5, v5 */
int v7 = -1; // const/4 v7, -0x1
/* invoke-interface/range {v2 ..v7}, Lvendor/hardware/vibratorfeature/IVibratorExt;->play([IIJI)V */
/* .line 354 */
/* new-instance v2, Lcom/android/server/MQSThread; */
v3 = this.callerInfo;
v3 = this.opPkg;
/* invoke-direct {v2, v3}, Lcom/android/server/MQSThread;-><init>(Ljava/lang/String;)V */
this.mMQSThread = v2;
/* .line 355 */
v2 = this.mMQSThread;
(( com.android.server.MQSThread ) v2 ).start ( ); // invoke-virtual {v2}, Lcom/android/server/MQSThread;->start()V
/* :try_end_0 */
/* .catch Landroid/os/DeadObjectException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 359 */
/* :catch_0 */
/* move-exception v2 */
/* .line 360 */
/* .local v2, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "skip play dynamicEffect " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v3 );
/* .line 356 */
} // .end local v2 # "e":Ljava/lang/Exception;
/* :catch_1 */
/* move-exception v2 */
/* .line 357 */
/* .local v2, "e":Landroid/os/DeadObjectException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "fail to play dynamicEffect, reset mHalExt "; // const-string v4, "fail to play dynamicEffect, reset mHalExt "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v3 );
/* .line 358 */
/* invoke-direct {p0}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->getHalExt()Lvendor/hardware/vibratorfeature/IVibratorExt; */
/* .line 361 */
} // .end local v2 # "e":Landroid/os/DeadObjectException;
} // :goto_0
/* nop */
/* .line 362 */
} // :goto_1
int v0 = 1; // const/4 v0, 0x1
/* .line 364 */
} // .end local v1 # "d":Landroid/os/DynamicEffect;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean processTaggedReasonIfItHas ( com.android.server.vibrator.Vibration p0 ) {
/* .locals 12 */
/* .param p1, "vib" # Lcom/android/server/vibrator/Vibration; */
/* .line 304 */
v0 = this.callerInfo;
v0 = this.reason;
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 305 */
/* .line 307 */
} // :cond_0
v0 = this.callerInfo;
v0 = this.reason;
final String v2 = ";"; // const-string v2, ";"
(( java.lang.String ) v0 ).split ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 308 */
/* .local v0, "settingsInfo":[Ljava/lang/String; */
/* aget-object v2, v0, v1 */
final String v3 = "="; // const-string v3, "="
(( java.lang.String ) v2 ).split ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* aget-object v2, v2, v1 */
final String v4 = "TAG"; // const-string v4, "TAG"
v2 = (( java.lang.String ) v2 ).equals ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* aget-object v2, v0, v1 */
(( java.lang.String ) v2 ).split ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* array-length v2, v2 */
int v5 = 2; // const/4 v5, 0x2
/* if-ne v2, v5, :cond_4 */
/* .line 309 */
/* new-instance v2, Ljava/util/HashMap; */
/* invoke-direct {v2}, Ljava/util/HashMap;-><init>()V */
/* .line 310 */
/* .local v2, "settingsMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;" */
/* array-length v6, v0 */
/* move v7, v1 */
} // :goto_0
int v8 = 1; // const/4 v8, 0x1
/* if-ge v7, v6, :cond_2 */
/* aget-object v9, v0, v7 */
/* .line 311 */
/* .local v9, "settingInfo":Ljava/lang/String; */
(( java.lang.String ) v9 ).split ( v3 ); // invoke-virtual {v9, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 312 */
/* .local v10, "setting":[Ljava/lang/String; */
/* array-length v11, v10 */
/* if-ne v11, v5, :cond_1 */
/* .line 313 */
/* aget-object v11, v10, v1 */
/* aget-object v8, v10, v8 */
(( java.util.HashMap ) v2 ).put ( v11, v8 ); // invoke-virtual {v2, v11, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 310 */
} // .end local v9 # "settingInfo":Ljava/lang/String;
} // .end local v10 # "setting":[Ljava/lang/String;
} // :cond_1
/* add-int/lit8 v7, v7, 0x1 */
/* .line 315 */
} // :cond_2
(( java.util.HashMap ) v2 ).get ( v4 ); // invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/String; */
v4 = (( java.lang.String ) v3 ).hashCode ( ); // invoke-virtual {v3}, Ljava/lang/String;->hashCode()I
/* sparse-switch v4, :sswitch_data_0 */
} // :cond_3
/* :sswitch_0 */
final String v4 = "haptic_feedback_config_strength"; // const-string v4, "haptic_feedback_config_strength"
v3 = (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_3
/* move v3, v1 */
/* :sswitch_1 */
final String v4 = "haptic_feedback_infinite_intensity"; // const-string v4, "haptic_feedback_infinite_intensity"
v3 = (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_3
/* move v3, v8 */
} // :goto_1
int v3 = -1; // const/4 v3, -0x1
} // :goto_2
final String v4 = "intensity"; // const-string v4, "intensity"
final String v5 = "VibratorManagerServiceImpl"; // const-string v5, "VibratorManagerServiceImpl"
/* packed-switch v3, :pswitch_data_0 */
/* .line 340 */
/* .line 330 */
/* :pswitch_0 */
try { // :try_start_0
(( java.util.HashMap ) v2 ).get ( v4 ); // invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/String; */
v1 = java.lang.Float .parseFloat ( v1 );
/* .line 331 */
/* .local v1, "intensity":F */
v3 = this.mHalExt;
/* :try_end_0 */
/* .catch Landroid/os/DeadObjectException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
} // .end local v1 # "intensity":F
/* .line 335 */
/* :catch_0 */
/* move-exception v1 */
/* .line 336 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "skip setAmplitudeExt " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v5,v3 );
/* .line 332 */
} // .end local v1 # "e":Ljava/lang/Exception;
/* :catch_1 */
/* move-exception v1 */
/* .line 333 */
/* .local v1, "e":Landroid/os/DeadObjectException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "fail to setAmplitudeExt, reset mHalExt "; // const-string v4, "fail to setAmplitudeExt, reset mHalExt "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v5,v3 );
/* .line 334 */
/* invoke-direct {p0}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->getHalExt()Lvendor/hardware/vibratorfeature/IVibratorExt; */
/* .line 337 */
} // .end local v1 # "e":Landroid/os/DeadObjectException;
} // :goto_3
/* nop */
/* .line 338 */
} // :goto_4
/* .line 318 */
/* :pswitch_1 */
try { // :try_start_1
final String v1 = "effectId"; // const-string v1, "effectId"
(( java.util.HashMap ) v2 ).get ( v1 ); // invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/String; */
v1 = java.lang.Integer .parseInt ( v1 );
/* .line 319 */
/* .local v1, "effectId":I */
(( java.util.HashMap ) v2 ).get ( v4 ); // invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/String; */
v3 = java.lang.Float .parseFloat ( v3 );
/* .line 320 */
/* .local v3, "intensity":F */
v4 = this.mHalExt;
/* :try_end_1 */
/* .catch Landroid/os/DeadObjectException; {:try_start_1 ..:try_end_1} :catch_3 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_2 */
} // .end local v1 # "effectId":I
} // .end local v3 # "intensity":F
/* .line 324 */
/* :catch_2 */
/* move-exception v1 */
/* .line 325 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "skip configStrength " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v5,v3 );
/* .line 321 */
} // .end local v1 # "e":Ljava/lang/Exception;
/* :catch_3 */
/* move-exception v1 */
/* .line 322 */
/* .local v1, "e":Landroid/os/DeadObjectException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "fail to configStrength, reset mHalExt "; // const-string v4, "fail to configStrength, reset mHalExt "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v5,v3 );
/* .line 323 */
/* invoke-direct {p0}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->getHalExt()Lvendor/hardware/vibratorfeature/IVibratorExt; */
/* .line 326 */
} // .end local v1 # "e":Landroid/os/DeadObjectException;
} // :goto_5
/* nop */
/* .line 327 */
} // :goto_6
/* .line 344 */
} // .end local v0 # "settingsInfo":[Ljava/lang/String;
} // .end local v2 # "settingsMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
} // :cond_4
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x605f1ae6 -> :sswitch_1 */
/* -0x8bf9774 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public Boolean shouldIgnoreForDynamicEffect ( com.android.server.vibrator.Vibration p0 ) {
/* .locals 8 */
/* .param p1, "incomingVibration" # Lcom/android/server/vibrator/Vibration; */
/* .line 387 */
v0 = this.mCurrentDynamicEffect;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 388 */
(( com.android.server.vibrator.Vibration ) p1 ).getEffect ( ); // invoke-virtual {p1}, Lcom/android/server/vibrator/Vibration;->getEffect()Landroid/os/CombinedVibration;
/* .line 389 */
/* .local v0, "incoming":Landroid/os/CombinedVibration; */
v2 = this.callerInfo;
v2 = this.attrs;
v2 = (( android.os.VibrationAttributes ) v2 ).getUsage ( ); // invoke-virtual {v2}, Landroid/os/VibrationAttributes;->getUsage()I
/* const/16 v3, 0x21 */
int v4 = -2; // const/4 v4, -0x2
final String v5 = "VibratorManagerServiceImpl"; // const-string v5, "VibratorManagerServiceImpl"
/* if-eq v2, v3, :cond_2 */
v2 = this.callerInfo;
v2 = this.attrs;
v2 = (( android.os.VibrationAttributes ) v2 ).getUsage ( ); // invoke-virtual {v2}, Landroid/os/VibrationAttributes;->getUsage()I
/* const/16 v3, 0x11 */
/* if-ne v2, v3, :cond_0 */
/* .line 394 */
} // :cond_0
/* instance-of v2, v0, Landroid/os/DynamicEffect; */
/* if-nez v2, :cond_3 */
/* .line 395 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
v6 = this.mCurrentDynamicEffect;
v6 = this.callerInfo;
/* iget-wide v6, v6, Lcom/android/server/vibrator/Vibration$CallerInfo;->startUptimeMillis:J */
/* sub-long/2addr v2, v6 */
/* const-wide/16 v6, 0xbb8 */
/* cmp-long v2, v2, v6 */
/* if-gez v2, :cond_1 */
/* .line 396 */
final String v1 = "Ignoring incoming vibration in favor of current Dynamic Effect"; // const-string v1, "Ignoring incoming vibration in favor of current Dynamic Effect"
android.util.Slog .d ( v5,v1 );
/* .line 397 */
int v1 = 1; // const/4 v1, 0x1
/* .line 399 */
} // :cond_1
final String v2 = "cancel current dynamiceffect"; // const-string v2, "cancel current dynamiceffect"
android.util.Slog .d ( v5,v2 );
/* .line 400 */
(( com.android.server.vibrator.VibratorManagerServiceImpl ) p0 ).cancelDynamicEffectIfItIs ( v4 ); // invoke-virtual {p0, v4}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->cancelDynamicEffectIfItIs(I)Z
/* .line 390 */
} // :cond_2
} // :goto_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "stop DynamicEffect in favor of current Effect\'s attribute" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.callerInfo;
v3 = this.attrs;
v3 = (( android.os.VibrationAttributes ) v3 ).getUsage ( ); // invoke-virtual {v3}, Landroid/os/VibrationAttributes;->getUsage()I
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v2 );
/* .line 391 */
(( com.android.server.vibrator.VibratorManagerServiceImpl ) p0 ).cancelDynamicEffectIfItIs ( v4 ); // invoke-virtual {p0, v4}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->cancelDynamicEffectIfItIs(I)Z
/* .line 392 */
/* .line 404 */
} // .end local v0 # "incoming":Landroid/os/CombinedVibration;
} // :cond_3
} // :goto_1
} // .end method
public com.android.server.vibrator.Vibration$EndInfo shouldIgnoredForRingtoneOrMIUI ( com.android.server.vibrator.Vibration$CallerInfo p0 ) {
/* .locals 3 */
/* .param p1, "callerInfo" # Lcom/android/server/vibrator/Vibration$CallerInfo; */
/* .line 238 */
/* sget-boolean v0, Landroid/os/Build;->IS_MIUI:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 239 */
com.android.server.vibrator.VibratorManagerServiceStub .getInstance ( );
v1 = this.attrs;
v2 = this.opPkg;
v0 = (( com.android.server.vibrator.VibratorManagerServiceStub ) v0 ).shouldIgnoredVibration ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/vibrator/VibratorManagerServiceStub;->shouldIgnoredVibration(Landroid/os/VibrationAttributes;Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 240 */
final String v0 = "VibratorManagerServiceImpl"; // const-string v0, "VibratorManagerServiceImpl"
final String v1 = "Vibrate ignored, not vibrating for ringtones or notify for MIUI"; // const-string v1, "Vibrate ignored, not vibrating for ringtones or notify for MIUI"
android.util.Slog .e ( v0,v1 );
/* .line 241 */
/* new-instance v0, Lcom/android/server/vibrator/Vibration$EndInfo; */
v1 = com.android.server.vibrator.Vibration$Status.IGNORED_RINGTONE_OR_NOTIFY_MIUI;
/* invoke-direct {v0, v1}, Lcom/android/server/vibrator/Vibration$EndInfo;-><init>(Lcom/android/server/vibrator/Vibration$Status;)V */
/* .line 244 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean shouldIgnoredVibration ( android.os.VibrationAttributes p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "attrs" # Landroid/os/VibrationAttributes; */
/* .param p2, "opPkg" # Ljava/lang/String; */
/* .line 195 */
v0 = (( android.os.VibrationAttributes ) p1 ).getUsage ( ); // invoke-virtual {p1}, Landroid/os/VibrationAttributes;->getUsage()I
int v1 = 1; // const/4 v1, 0x1
/* const/16 v2, 0x21 */
/* if-eq v0, v2, :cond_0 */
/* .line 196 */
v0 = (( android.os.VibrationAttributes ) p1 ).getUsage ( ); // invoke-virtual {p1}, Landroid/os/VibrationAttributes;->getUsage()I
/* const/16 v3, 0x31 */
/* if-ne v0, v3, :cond_1 */
} // :cond_0
/* nop */
/* .line 197 */
int v0 = 2; // const/4 v0, 0x2
v0 = (( android.os.VibrationAttributes ) p1 ).isFlagSet ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/VibrationAttributes;->isFlagSet(I)Z
/* if-nez v0, :cond_1 */
v0 = this.mContext;
/* .line 198 */
v0 = miui.util.AudioManagerHelper .isVibrateEnabled ( v0 );
/* if-nez v0, :cond_1 */
/* .line 199 */
/* .line 204 */
} // :cond_1
/* sget-boolean v0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mIgnoreVibrationWhenCamera:Z */
if ( v0 != null) { // if-eqz v0, :cond_4
v0 = v0 = this.mCameraUnAvailableSets;
/* if-nez v0, :cond_4 */
/* .line 205 */
v0 = (( android.os.VibrationAttributes ) p1 ).getUsage ( ); // invoke-virtual {p1}, Landroid/os/VibrationAttributes;->getUsage()I
/* const/16 v3, 0x11 */
/* if-eq v0, v3, :cond_2 */
/* .line 206 */
v0 = (( android.os.VibrationAttributes ) p1 ).getUsage ( ); // invoke-virtual {p1}, Landroid/os/VibrationAttributes;->getUsage()I
/* if-ne v0, v2, :cond_4 */
/* .line 207 */
} // :cond_2
v0 = v0 = this.mCameraUnAvailableSets;
/* if-ne v0, v1, :cond_3 */
v0 = this.mCameraUnAvailableSets;
v0 = final String v2 = "1"; // const-string v2, "1"
/* if-nez v0, :cond_4 */
/* .line 208 */
} // :cond_3
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "ignore incoming vibration in favor of camera in device "; // const-string v2, "ignore incoming vibration in favor of camera in device "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = miui.os.Build.DEVICE;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "VibratorManagerServiceImpl"; // const-string v2, "VibratorManagerServiceImpl"
android.util.Slog .w ( v2,v0 );
/* .line 209 */
/* .line 213 */
} // :cond_4
v0 = (( android.os.VibrationAttributes ) p1 ).getUsage ( ); // invoke-virtual {p1}, Landroid/os/VibrationAttributes;->getUsage()I
/* const/16 v1, 0x12 */
int v2 = 0; // const/4 v2, 0x0
/* if-ne v0, v1, :cond_5 */
v0 = this.mContext;
/* .line 214 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "haptic_feedback_enabled"; // const-string v1, "haptic_feedback_enabled"
int v3 = -2; // const/4 v3, -0x2
v0 = android.provider.Settings$System .getIntForUser ( v0,v1,v2,v3 );
/* if-nez v0, :cond_5 */
v0 = this.VIBRATOR_IGNORE_LIST;
/* .line 215 */
/* .line 216 */
/* .line 218 */
} // :cond_5
} // .end method
public com.android.server.vibrator.Vibration$Status startVibrationLockedInjector ( com.android.server.vibrator.Vibration p0 ) {
/* .locals 10 */
/* .param p1, "vib" # Lcom/android/server/vibrator/Vibration; */
/* .line 249 */
int v0 = 0; // const/4 v0, 0x0
/* .line 250 */
/* .local v0, "injectorInfo":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "attrs = "; // const-string v2, "attrs = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.callerInfo;
v2 = this.attrs;
(( android.os.VibrationAttributes ) v2 ).toString ( ); // invoke-virtual {v2}, Landroid/os/VibrationAttributes;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", attrs.usage = "; // const-string v2, ", attrs.usage = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.callerInfo;
v2 = this.attrs;
v2 = (( android.os.VibrationAttributes ) v2 ).getUsage ( ); // invoke-virtual {v2}, Landroid/os/VibrationAttributes;->getUsage()I
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "VibratorManagerServiceImpl"; // const-string v2, "VibratorManagerServiceImpl"
android.util.Slog .d ( v2,v1 );
/* .line 252 */
try { // :try_start_0
v1 = this.mHalExt;
v3 = this.callerInfo;
v3 = this.attrs;
v3 = (( android.os.VibrationAttributes ) v3 ).getUsage ( ); // invoke-virtual {v3}, Landroid/os/VibrationAttributes;->getUsage()I
/* :try_end_0 */
/* .catch Landroid/os/DeadObjectException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 256 */
/* :catch_0 */
/* move-exception v1 */
/* .line 257 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "skip" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* .line 253 */
} // .end local v1 # "e":Ljava/lang/Exception;
/* :catch_1 */
/* move-exception v1 */
/* .line 254 */
/* .local v1, "e":Landroid/os/DeadObjectException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "fail"; // const-string v4, "fail"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* .line 255 */
/* invoke-direct {p0}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->getHalExt()Lvendor/hardware/vibratorfeature/IVibratorExt; */
/* .line 258 */
} // .end local v1 # "e":Landroid/os/DeadObjectException;
} // :goto_0
/* nop */
/* .line 259 */
} // :goto_1
int v3 = 1; // const/4 v3, 0x1
/* if-ne v1, v3, :cond_0 */
/* .line 260 */
final String v0 = "hapticFeedbackDisable"; // const-string v0, "hapticFeedbackDisable"
/* .line 261 */
} // :cond_0
v1 = (( com.android.server.vibrator.VibratorManagerServiceImpl ) p0 ).playDynamicEffectIfItIs ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->playDynamicEffectIfItIs(Lcom/android/server/vibrator/Vibration;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 262 */
final String v0 = "playDynamicEffect"; // const-string v0, "playDynamicEffect"
/* .line 263 */
} // :cond_1
v1 = (( com.android.server.vibrator.VibratorManagerServiceImpl ) p0 ).processTaggedReasonIfItHas ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->processTaggedReasonIfItHas(Lcom/android/server/vibrator/Vibration;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 264 */
final String v0 = "processTaggedReason"; // const-string v0, "processTaggedReason"
/* .line 265 */
} // :cond_2
} // :goto_2
final String v1 = "playDynamicEffect"; // const-string v1, "playDynamicEffect"
v4 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 266 */
android.util.Slog .d ( v2,v1 );
/* .line 267 */
v1 = com.android.server.vibrator.Vibration$Status.RUNNING;
/* .line 269 */
} // :cond_3
final String v1 = "hapticFeedbackDisable"; // const-string v1, "hapticFeedbackDisable"
v4 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 270 */
android.util.Slog .d ( v2,v1 );
/* .line 271 */
v1 = com.android.server.vibrator.Vibration$Status.FINISHED;
/* .line 273 */
} // :cond_4
/* sget-boolean v1, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mSupportVibrationOneTrack:Z */
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 274 */
(( com.android.server.vibrator.Vibration ) p1 ).getEffect ( ); // invoke-virtual {p1}, Lcom/android/server/vibrator/Vibration;->getEffect()Landroid/os/CombinedVibration;
(( java.lang.Object ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
/* .line 275 */
/* .local v1, "effect":Ljava/lang/String; */
v2 = this.callerInfo;
v2 = this.opPkg;
/* .line 276 */
/* .local v2, "opPkg":Ljava/lang/String; */
v4 = com.android.server.vibrator.VibratorManagerServiceImpl.pattern;
(( java.util.regex.Pattern ) v4 ).matcher ( v1 ); // invoke-virtual {v4, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
/* .line 277 */
/* .local v4, "matcher":Ljava/util/regex/Matcher; */
final String v5 = ""; // const-string v5, ""
/* .line 278 */
/* .local v5, "effectID":Ljava/lang/String; */
v6 = (( java.util.regex.Matcher ) v4 ).find ( ); // invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z
if ( v6 != null) { // if-eqz v6, :cond_5
/* .line 279 */
(( java.util.regex.Matcher ) v4 ).group ( v3 ); // invoke-virtual {v4, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
/* .line 281 */
} // :cond_5
v6 = com.android.server.vibrator.VibratorManagerServiceImpl.lastEffectID;
v6 = (( java.lang.String ) v6 ).equals ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_7
v6 = com.android.server.vibrator.VibratorManagerServiceImpl.lastPackageName;
v6 = (( java.lang.String ) v6 ).equals ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v6, :cond_6 */
/* .line 287 */
} // :cond_6
v6 = com.android.server.vibrator.VibratorManagerServiceImpl.lastEffectID;
v6 = (( java.lang.String ) v6 ).equals ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_8
v6 = com.android.server.vibrator.VibratorManagerServiceImpl.lastPackageName;
v6 = (( java.lang.String ) v6 ).equals ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_8
/* .line 288 */
/* add-int/2addr v6, v3 */
/* .line 282 */
} // :cond_7
} // :goto_3
/* new-instance v6, Lcom/android/server/MQSThread; */
v7 = com.android.server.vibrator.VibratorManagerServiceImpl.lastPackageName;
v8 = com.android.server.vibrator.VibratorManagerServiceImpl.lastEffectID;
/* invoke-direct {v6, v7, v8, v9}, Lcom/android/server/MQSThread;-><init>(Ljava/lang/String;Ljava/lang/String;I)V */
this.mMQSThread = v6;
/* .line 283 */
v6 = this.mMQSThread;
(( com.android.server.MQSThread ) v6 ).start ( ); // invoke-virtual {v6}, Lcom/android/server/MQSThread;->start()V
/* .line 284 */
/* .line 285 */
/* .line 286 */
/* .line 291 */
} // .end local v1 # "effect":Ljava/lang/String;
} // .end local v2 # "opPkg":Ljava/lang/String;
} // .end local v4 # "matcher":Ljava/util/regex/Matcher;
} // .end local v5 # "effectID":Ljava/lang/String;
} // :cond_8
} // :goto_4
int v1 = 0; // const/4 v1, 0x0
} // .end method
public java.lang.String startVibrationLockedInjector ( android.os.ExternalVibration p0 ) {
/* .locals 2 */
/* .param p1, "vib" # Landroid/os/ExternalVibration; */
/* .line 295 */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* .line 296 */
final String v0 = "VibratorManagerServiceImpl"; // const-string v0, "VibratorManagerServiceImpl"
final String v1 = "hapticFeedbackDisable"; // const-string v1, "hapticFeedbackDisable"
android.util.Slog .d ( v0,v1 );
/* .line 297 */
/* .line 299 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Long weakenVibrationIfNecessary ( Long p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "time" # J */
/* .param p3, "uid" # I */
/* .line 225 */
/* iget-boolean v0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->mIncall:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = android.os.UserHandle .isApp ( p3 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget-boolean v0, p0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->isLinearOrZLinear:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* sget-wide v1, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->VIBRATION_THRESHOLD_IN_CALL_LINEAR:J */
} // :cond_0
/* sget-wide v1, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->VIBRATION_THRESHOLD_IN_CALL_MOTOR:J */
} // :goto_0
/* cmp-long v1, p1, v1 */
/* if-lez v1, :cond_2 */
/* .line 226 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* sget-wide v0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->VIBRATION_THRESHOLD_IN_CALL_LINEAR:J */
} // :cond_1
/* sget-wide v0, Lcom/android/server/vibrator/VibratorManagerServiceImpl;->VIBRATION_THRESHOLD_IN_CALL_MOTOR:J */
} // :goto_1
/* move-wide p1, v0 */
/* .line 228 */
} // :cond_2
/* const-wide/16 v0, 0x2710 */
/* cmp-long v0, p1, v0 */
/* if-lez v0, :cond_3 */
/* .line 229 */
/* const-wide/16 p1, 0x3e8 */
/* .line 232 */
} // :cond_3
} // :goto_2
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "weakenVibrationIfNecessary time" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "VibratorManagerServiceImpl"; // const-string v1, "VibratorManagerServiceImpl"
android.util.Slog .d ( v1,v0 );
/* .line 233 */
/* return-wide p1 */
} // .end method
