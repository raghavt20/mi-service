class inal extends android.database.ContentObserver {
	 /* .source "VibratorManagerServiceImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/vibrator/VibratorManagerServiceImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x12 */
/* name = "SettingsObserver" */
} // .end annotation
/* # instance fields */
final com.android.server.vibrator.VibratorManagerServiceImpl this$0; //synthetic
/* # direct methods */
 inal ( ) {
/* .locals 0 */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 442 */
this.this$0 = p1;
/* .line 443 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 444 */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 6 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 448 */
final String v0 = "VibratorManagerServiceImpl"; // const-string v0, "VibratorManagerServiceImpl"
v1 = this.this$0;
com.android.server.vibrator.VibratorManagerServiceImpl .-$$Nest$fgetmVibrator ( v1 );
/* if-nez v1, :cond_0 */
/* .line 449 */
v1 = this.this$0;
com.android.server.vibrator.VibratorManagerServiceImpl .-$$Nest$fgetmContext ( v1 );
/* const-string/jumbo v3, "vibrator" */
(( android.content.Context ) v2 ).getSystemService ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v2, Landroid/os/Vibrator; */
com.android.server.vibrator.VibratorManagerServiceImpl .-$$Nest$fputmVibrator ( v1,v2 );
/* .line 452 */
} // :cond_0
try { // :try_start_0
v1 = this.this$0;
com.android.server.vibrator.VibratorManagerServiceImpl .-$$Nest$fgetmHapticFeedbackInfiniteIntensityUri ( v1 );
v1 = (( android.net.Uri ) p2 ).equals ( v1 ); // invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
int v2 = -2; // const/4 v2, -0x2
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 453 */
	 v1 = this.this$0;
	 com.android.server.vibrator.VibratorManagerServiceImpl .-$$Nest$fgetmHalExt ( v1 );
	 v3 = this.this$0;
	 com.android.server.vibrator.VibratorManagerServiceImpl .-$$Nest$fgetmContext ( v3 );
	 /* .line 454 */
	 (( android.content.Context ) v3 ).getContentResolver ( ); // invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 final String v4 = "haptic_feedback_infinite_intensity"; // const-string v4, "haptic_feedback_infinite_intensity"
	 /* const/high16 v5, 0x3f800000 # 1.0f */
	 v2 = 	 android.provider.Settings$System .getFloatForUser ( v3,v4,v5,v2 );
	 /* .line 453 */
	 int v3 = 0; // const/4 v3, 0x0
	 /* goto/16 :goto_0 */
	 /* .line 456 */
} // :cond_1
v1 = this.this$0;
com.android.server.vibrator.VibratorManagerServiceImpl .-$$Nest$fgetmHapticFeedbackDisableUri ( v1 );
v1 = (( android.net.Uri ) p2 ).equals ( v1 ); // invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
	 /* .line 457 */
	 v1 = this.this$0;
	 com.android.server.vibrator.VibratorManagerServiceImpl .-$$Nest$fgetmContext ( v1 );
	 (( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 final String v3 = "haptic_feedback_disable"; // const-string v3, "haptic_feedback_disable"
	 v1 = 	 android.provider.Settings$System .getIntForUser ( v1,v3,v2 );
	 com.android.server.vibrator.VibratorManagerServiceImpl .-$$Nest$sfputhapticFeedbackDisable ( v1 );
	 /* .line 459 */
	 v1 = 	 com.android.server.vibrator.VibratorManagerServiceImpl .-$$Nest$sfgethapticFeedbackDisable ( );
	 int v2 = 1; // const/4 v2, 0x1
	 /* if-ne v1, v2, :cond_2 */
	 /* .line 460 */
	 v1 = this.this$0;
	 com.android.server.vibrator.VibratorManagerServiceImpl .-$$Nest$fgetmHal ( v1 );
	 /* .line 461 */
	 v1 = this.this$0;
	 com.android.server.vibrator.VibratorManagerServiceImpl .-$$Nest$fgetmHalExt ( v1 );
	 int v2 = 0; // const/4 v2, 0x0
	 int v3 = 2; // const/4 v3, 0x2
	 /* .line 463 */
	 v1 = this.this$0;
	 com.android.server.vibrator.VibratorManagerServiceImpl .-$$Nest$fgetmVibrator ( v1 );
	 /* const/16 v2, 0x21 */
	 (( android.os.Vibrator ) v1 ).cancel ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Vibrator;->cancel(I)V
	 /* .line 464 */
	 /* const-string/jumbo v1, "start disable haptic feedback" */
	 android.util.Slog .d ( v0,v1 );
	 /* :try_end_0 */
	 /* .catch Landroid/os/DeadObjectException; {:try_start_0 ..:try_end_0} :catch_1 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 470 */
	 /* :catch_0 */
	 /* move-exception v1 */
	 /* .line 471 */
	 /* .local v1, "e":Ljava/lang/Exception; */
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 /* const-string/jumbo v3, "skip setAmplitudeExt with flag " */
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .e ( v0,v2 );
	 /* .line 467 */
} // .end local v1 # "e":Ljava/lang/Exception;
/* :catch_1 */
/* move-exception v1 */
/* .line 468 */
/* .local v1, "e":Landroid/os/DeadObjectException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "fail to setAmplitudeExt, reset mHalExt "; // const-string v3, "fail to setAmplitudeExt, reset mHalExt "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v2 );
/* .line 469 */
v0 = this.this$0;
com.android.server.vibrator.VibratorManagerServiceImpl .-$$Nest$mgetHalExt ( v0 );
/* .line 472 */
} // .end local v1 # "e":Landroid/os/DeadObjectException;
} // :cond_2
} // :goto_0
/* nop */
/* .line 473 */
} // :goto_1
return;
} // .end method
