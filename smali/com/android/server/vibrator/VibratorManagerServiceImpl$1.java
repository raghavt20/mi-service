class com.android.server.vibrator.VibratorManagerServiceImpl$1 extends android.hardware.camera2.CameraManager$AvailabilityCallback {
	 /* .source "VibratorManagerServiceImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/vibrator/VibratorManagerServiceImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.vibrator.VibratorManagerServiceImpl this$0; //synthetic
/* # direct methods */
 com.android.server.vibrator.VibratorManagerServiceImpl$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/vibrator/VibratorManagerServiceImpl; */
/* .line 112 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/hardware/camera2/CameraManager$AvailabilityCallback;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onCameraAvailable ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "cameraId" # Ljava/lang/String; */
/* .line 115 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "onCameraAvailable with id"; // const-string v1, "onCameraAvailable with id"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "VibratorManagerServiceImpl"; // const-string v1, "VibratorManagerServiceImpl"
android.util.Slog .w ( v1,v0 );
/* .line 116 */
v0 = this.this$0;
v0 = com.android.server.vibrator.VibratorManagerServiceImpl .-$$Nest$fgetmCameraUnAvailableSets ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 117 */
	 final String v0 = "remove from mCameraUnAvailableSet"; // const-string v0, "remove from mCameraUnAvailableSet"
	 android.util.Slog .w ( v1,v0 );
	 /* .line 119 */
} // :cond_0
return;
} // .end method
public void onCameraUnavailable ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "cameraId" # Ljava/lang/String; */
/* .line 123 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "onCameraUnavailable with id"; // const-string v1, "onCameraUnavailable with id"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "VibratorManagerServiceImpl"; // const-string v1, "VibratorManagerServiceImpl"
android.util.Slog .w ( v1,v0 );
/* .line 124 */
v0 = this.this$0;
com.android.server.vibrator.VibratorManagerServiceImpl .-$$Nest$fgetmCameraUnAvailableSets ( v0 );
/* .line 125 */
return;
} // .end method
