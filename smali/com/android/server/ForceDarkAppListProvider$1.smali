.class Lcom/android/server/ForceDarkAppListProvider$1;
.super Landroid/database/ContentObserver;
.source "ForceDarkAppListProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/ForceDarkAppListProvider;->registerDataObserver(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/ForceDarkAppListProvider;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/android/server/ForceDarkAppListProvider;Landroid/os/Handler;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/ForceDarkAppListProvider;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 202
    iput-object p1, p0, Lcom/android/server/ForceDarkAppListProvider$1;->this$0:Lcom/android/server/ForceDarkAppListProvider;

    iput-object p3, p0, Lcom/android/server/ForceDarkAppListProvider$1;->val$context:Landroid/content/Context;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2
    .param p1, "selfChange"    # Z

    .line 205
    const-class v0, Lcom/android/server/ForceDarkAppListProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "forceDarkAppListCouldData onChange--"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    iget-object v0, p0, Lcom/android/server/ForceDarkAppListProvider$1;->this$0:Lcom/android/server/ForceDarkAppListProvider;

    iget-object v1, p0, Lcom/android/server/ForceDarkAppListProvider$1;->val$context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/server/ForceDarkAppListProvider;->updateCloudForceDarkAppList(Landroid/content/Context;)V

    .line 207
    return-void
.end method
