.class Lcom/android/server/MiuiBatteryServiceImpl$9;
.super Landroid/content/BroadcastReceiver;
.source "MiuiBatteryServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/MiuiBatteryServiceImpl;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/MiuiBatteryServiceImpl;


# direct methods
.method constructor <init>(Lcom/android/server/MiuiBatteryServiceImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/MiuiBatteryServiceImpl;

    .line 336
    iput-object p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$9;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 339
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 340
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/16 v2, 0xa

    const/4 v3, 0x0

    if-eqz v1, :cond_2

    .line 341
    const-string v1, "android.bluetooth.adapter.extra.STATE"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 342
    .local v1, "value":I
    const/16 v4, 0xf

    if-eq v1, v4, :cond_0

    if-ne v1, v2, :cond_1

    .line 343
    :cond_0
    iget-object v4, p0, Lcom/android/server/MiuiBatteryServiceImpl$9;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v4, v3}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fputBtConnectedCount(Lcom/android/server/MiuiBatteryServiceImpl;I)V

    .line 344
    iget-object v4, p0, Lcom/android/server/MiuiBatteryServiceImpl$9;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v4}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryServiceImpl;)Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(IZ)V

    .line 346
    .end local v1    # "value":I
    :cond_1
    goto :goto_0

    :cond_2
    const-string v1, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 347
    const-string v1, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 348
    :cond_3
    const-string v1, "android.bluetooth.profile.extra.STATE"

    const/4 v4, -0x1

    invoke-virtual {p2, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 349
    .local v1, "state":I
    const-string v5, "android.bluetooth.profile.extra.PREVIOUS_STATE"

    invoke-virtual {p2, v5, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 350
    .local v4, "preState":I
    const/4 v5, 0x2

    const/4 v6, 0x1

    if-ne v1, v5, :cond_4

    .line 351
    iget-object v3, p0, Lcom/android/server/MiuiBatteryServiceImpl$9;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetBtConnectedCount(Lcom/android/server/MiuiBatteryServiceImpl;)I

    move-result v5

    add-int/2addr v5, v6

    invoke-static {v3, v5}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fputBtConnectedCount(Lcom/android/server/MiuiBatteryServiceImpl;I)V

    .line 352
    iget-object v3, p0, Lcom/android/server/MiuiBatteryServiceImpl$9;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryServiceImpl;)Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    move-result-object v3

    invoke-virtual {v3, v2, v6}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(IZ)V

    goto :goto_0

    .line 353
    :cond_4
    if-nez v1, :cond_5

    if-eq v4, v6, :cond_5

    .line 355
    iget-object v5, p0, Lcom/android/server/MiuiBatteryServiceImpl$9;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetBtConnectedCount(Lcom/android/server/MiuiBatteryServiceImpl;)I

    move-result v7

    sub-int/2addr v7, v6

    invoke-static {v5, v7}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fputBtConnectedCount(Lcom/android/server/MiuiBatteryServiceImpl;I)V

    .line 356
    iget-object v5, p0, Lcom/android/server/MiuiBatteryServiceImpl$9;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetBtConnectedCount(Lcom/android/server/MiuiBatteryServiceImpl;)I

    move-result v5

    if-gtz v5, :cond_5

    .line 357
    iget-object v5, p0, Lcom/android/server/MiuiBatteryServiceImpl$9;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v5, v3}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fputBtConnectedCount(Lcom/android/server/MiuiBatteryServiceImpl;I)V

    .line 358
    iget-object v5, p0, Lcom/android/server/MiuiBatteryServiceImpl$9;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryServiceImpl;)Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(IZ)V

    .line 362
    .end local v1    # "state":I
    .end local v4    # "preState":I
    :cond_5
    :goto_0
    return-void
.end method
