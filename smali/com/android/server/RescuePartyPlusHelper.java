public class com.android.server.RescuePartyPlusHelper {
	 /* .source "RescuePartyPlusHelper.java" */
	 /* # static fields */
	 private static final java.lang.String CLOUD_CONTROL_FILE;
	 private static final java.lang.String CONFIG_RESET_PROCESS;
	 private static final java.util.Set CORE_PACKAGE;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Set<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private static final java.lang.String DEVELOPMENT_PROP;
private static final java.lang.String PRODUCT_PROP;
private static final java.lang.String PROP_BOOTCOMPLETE;
private static final java.lang.String PROP_DISABLE_AUTORESTART_APP_PREFIX;
private static final java.lang.String PROP_READY_SHOW_RESET_CONFIG_UI;
private static final java.lang.String RESCUEPARTY_DEBUG_PROC;
private static final java.lang.String RESCUEPARTY_LAST_RESET_CONFIG_PROP;
private static final java.lang.String RESCUEPARTY_PLUS_DISABLE_PROP;
private static final java.lang.String RESCUEPARTY_PLUS_ENABLE_PROP;
private static final java.lang.String RESCUEPARTY_TEMP_MITIGATION_COUNT_PROP;
private static final java.util.Set RESET_CONFIG_DEFAULT_SET;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
static final java.lang.String TAG;
private static final java.lang.String THEME_DATA;
private static final java.lang.String THEME_MAGIC_DATA;
private static final java.util.Set TOP_UI_PACKAGE;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.Set UI_PACKAGE;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.android.server.RescuePartyPlusHelper ( ) {
/* .locals 1 */
/* .line 50 */
/* new-instance v0, Lcom/android/server/RescuePartyPlusHelper$1; */
/* invoke-direct {v0}, Lcom/android/server/RescuePartyPlusHelper$1;-><init>()V */
/* .line 60 */
/* new-instance v0, Lcom/android/server/RescuePartyPlusHelper$2; */
/* invoke-direct {v0}, Lcom/android/server/RescuePartyPlusHelper$2;-><init>()V */
/* .line 64 */
/* new-instance v0, Lcom/android/server/RescuePartyPlusHelper$3; */
/* invoke-direct {v0}, Lcom/android/server/RescuePartyPlusHelper$3;-><init>()V */
/* .line 68 */
/* new-instance v0, Lcom/android/server/RescuePartyPlusHelper$4; */
/* invoke-direct {v0}, Lcom/android/server/RescuePartyPlusHelper$4;-><init>()V */
return;
} // .end method
public com.android.server.RescuePartyPlusHelper ( ) {
/* .locals 0 */
/* .line 24 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
static Boolean checkBootCompletedStatus ( ) {
/* .locals 2 */
/* .line 111 */
/* const-string/jumbo v0, "sys.boot_completed" */
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
} // .end method
static Boolean checkDisableRescuePartyPlus ( ) {
/* .locals 3 */
/* .line 146 */
final String v0 = "persist.sys.rescuepartyplus.disable"; // const-string v0, "persist.sys.rescuepartyplus.disable"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
final String v2 = "RescuePartyPlus"; // const-string v2, "RescuePartyPlus"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 147 */
final String v0 = "RescueParty Plus is disable!"; // const-string v0, "RescueParty Plus is disable!"
android.util.Slog .w ( v2,v0 );
/* .line 148 */
int v0 = 1; // const/4 v0, 0x1
/* .line 149 */
} // :cond_0
final String v0 = "persist.sys.rescuepartyplus.enable"; // const-string v0, "persist.sys.rescuepartyplus.enable"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 150 */
final String v0 = "This device support and enable RescuePartyPlus! (Via cloud control)"; // const-string v0, "This device support and enable RescuePartyPlus! (Via cloud control)"
android.util.Slog .w ( v2,v0 );
/* .line 151 */
/* .line 154 */
} // :cond_1
} // .end method
static Boolean checkPackageIsCore ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .line 116 */
/* if-nez p0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
} // :cond_0
v0 = v0 = com.android.server.RescuePartyPlusHelper.CORE_PACKAGE;
} // :goto_0
} // .end method
static Boolean checkPackageIsTOPUI ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .line 124 */
/* if-nez p0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
} // :cond_0
v0 = v0 = com.android.server.RescuePartyPlusHelper.TOP_UI_PACKAGE;
} // :goto_0
} // .end method
static Boolean checkPackageIsUI ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .line 120 */
/* if-nez p0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
} // :cond_0
v0 = v0 = com.android.server.RescuePartyPlusHelper.UI_PACKAGE;
} // :goto_0
} // .end method
static Boolean delete ( java.io.File p0, java.util.Set p1 ) {
/* .locals 6 */
/* .param p0, "deleteFile" # Ljava/io/File; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/io/File;", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 171 */
/* .local p1, "saveDir":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
int v0 = 1; // const/4 v0, 0x1
/* .line 172 */
/* .local v0, "result":Z */
v1 = (( java.io.File ) p0 ).isFile ( ); // invoke-virtual {p0}, Ljava/io/File;->isFile()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 173 */
v1 = (( java.io.File ) p0 ).delete ( ); // invoke-virtual {p0}, Ljava/io/File;->delete()Z
/* and-int/2addr v0, v1 */
/* .line 174 */
} // :cond_0
v1 = (( java.io.File ) p0 ).isDirectory ( ); // invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 175 */
(( java.io.File ) p0 ).listFiles ( ); // invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;
/* .line 176 */
/* .local v1, "files":[Ljava/io/File; */
/* if-nez v1, :cond_1 */
/* .line 177 */
/* .line 179 */
} // :cond_1
/* array-length v2, v1 */
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
/* if-ge v3, v2, :cond_4 */
/* aget-object v4, v1, v3 */
/* .line 180 */
/* .local v4, "deleteInFile":Ljava/io/File; */
v5 = (( java.io.File ) v4 ).isFile ( ); // invoke-virtual {v4}, Ljava/io/File;->isFile()Z
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 181 */
v5 = (( java.io.File ) v4 ).delete ( ); // invoke-virtual {v4}, Ljava/io/File;->delete()Z
/* and-int/2addr v0, v5 */
/* .line 182 */
} // :cond_2
v5 = (( java.io.File ) v4 ).isDirectory ( ); // invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 183 */
v5 = com.android.server.RescuePartyPlusHelper .delete ( v4,p1 );
/* and-int/2addr v0, v5 */
/* .line 179 */
} // .end local v4 # "deleteInFile":Ljava/io/File;
} // :cond_3
} // :goto_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 187 */
} // :cond_4
v2 = (( java.io.File ) p0 ).getAbsolutePath ( ); // invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
/* if-nez v2, :cond_5 */
/* .line 188 */
(( java.io.File ) p0 ).delete ( ); // invoke-virtual {p0}, Ljava/io/File;->delete()Z
/* .line 191 */
} // .end local v1 # "files":[Ljava/io/File;
} // :cond_5
} // :goto_2
} // .end method
static void disableAppRestart ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .line 129 */
v0 = android.text.TextUtils .isEmpty ( p0 );
final String v1 = "RescuePartyPlus"; // const-string v1, "RescuePartyPlus"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 130 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Disable app auto restart failed, because package is empty: "; // const-string v2, "Disable app auto restart failed, because package is empty: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v0 );
/* .line 131 */
return;
/* .line 133 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Disable app auto restart: "; // const-string v2, "Disable app auto restart: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v0 );
/* .line 134 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "sys.rescuepartyplus.disable_autorestart." */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "1"; // const-string v1, "1"
android.os.SystemProperties .set ( v0,v1 );
/* .line 135 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Finished rescue level DISABLE_APP for package "; // const-string v1, "Finished rescue level DISABLE_APP for package "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v1 = 3; // const/4 v1, 0x3
com.android.server.pm.PackageManagerServiceUtils .logCriticalInfo ( v1,v0 );
/* .line 136 */
return;
} // .end method
static Boolean enableDebugStatus ( ) {
/* .locals 2 */
/* .line 140 */
final String v0 = "persist.sys.rescuepartyplus.debug"; // const-string v0, "persist.sys.rescuepartyplus.debug"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* if-nez v0, :cond_0 */
/* .line 141 */
final String v0 = "ro.mi.development"; // const-string v0, "ro.mi.development"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
/* .line 140 */
} // :cond_1
} // .end method
static Boolean getConfigResetProcessStatus ( ) {
/* .locals 2 */
/* .line 86 */
/* const-string/jumbo v0, "sys.rescuepartyplus.config_reset_process" */
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
} // .end method
static Boolean getLastResetConfigStatus ( ) {
/* .locals 2 */
/* .line 94 */
final String v0 = "persist.sys.rescuepartyplus.last_reset_config"; // const-string v0, "persist.sys.rescuepartyplus.last_reset_config"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
} // .end method
static java.lang.String getLauncherPackageName ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 160 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "android.intent.action.MAIN"; // const-string v1, "android.intent.action.MAIN"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 161 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "android.intent.category.HOME"; // const-string v1, "android.intent.category.HOME"
(( android.content.Intent ) v0 ).addCategory ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;
/* .line 162 */
(( android.content.Context ) p0 ).getPackageManager ( ); // invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
int v2 = 0; // const/4 v2, 0x0
(( android.content.pm.PackageManager ) v1 ).resolveActivity ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;
/* .line 163 */
/* .local v1, "res":Landroid/content/pm/ResolveInfo; */
if ( v1 != null) { // if-eqz v1, :cond_1
v2 = this.activityInfo;
if ( v2 != null) { // if-eqz v2, :cond_1
v2 = this.activityInfo;
v2 = this.packageName;
final String v3 = "android"; // const-string v3, "android"
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 166 */
} // :cond_0
v2 = this.activityInfo;
v2 = this.packageName;
/* .line 164 */
} // :cond_1
} // :goto_0
int v2 = 0; // const/4 v2, 0x0
} // .end method
static Integer getMitigationTempCount ( ) {
/* .locals 2 */
/* .line 78 */
/* const-string/jumbo v0, "sys.rescuepartyplus.temp_mitigation_count" */
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getInt ( v0,v1 );
} // .end method
private static java.lang.String getProductInfo ( ) {
/* .locals 2 */
/* .line 74 */
final String v0 = "ro.build.product"; // const-string v0, "ro.build.product"
final String v1 = "None"; // const-string v1, "None"
android.os.SystemProperties .get ( v0,v1 );
} // .end method
static Boolean getShowResetConfigUIStatus ( ) {
/* .locals 2 */
/* .line 102 */
/* const-string/jumbo v0, "sys.rescuepartyplus.ready_show_reset_config_ui" */
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
} // .end method
static Boolean resetTheme ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p0, "failedPackage" # Ljava/lang/String; */
/* .line 196 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Preparing to reset theme: "; // const-string v1, "Preparing to reset theme: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "RescuePartyPlus"; // const-string v1, "RescuePartyPlus"
android.util.Slog .w ( v1,v0 );
/* .line 197 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 198 */
/* .local v0, "saveDir":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;" */
int v2 = 1; // const/4 v2, 0x1
/* .line 199 */
/* .local v2, "result":Z */
final String v3 = "/data/system/theme"; // const-string v3, "/data/system/theme"
(( java.util.HashSet ) v0 ).add ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 200 */
final String v4 = "/data/system/theme_magic"; // const-string v4, "/data/system/theme_magic"
(( java.util.HashSet ) v0 ).add ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 201 */
/* new-instance v5, Ljava/io/File; */
/* invoke-direct {v5, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
v3 = com.android.server.RescuePartyPlusHelper .delete ( v5,v0 );
/* if-nez v3, :cond_0 */
/* .line 202 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Delete theme files failed: "; // const-string v5, "Delete theme files failed: "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p0 ); // invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v3 );
/* .line 203 */
int v2 = 0; // const/4 v2, 0x0
/* .line 205 */
} // :cond_0
/* new-instance v3, Ljava/io/File; */
/* invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
v3 = com.android.server.RescuePartyPlusHelper .delete ( v3,v0 );
/* if-nez v3, :cond_1 */
/* .line 206 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Delete magic theme files failed: "; // const-string v4, "Delete magic theme files failed: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p0 ); // invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v3 );
/* .line 207 */
int v2 = 0; // const/4 v2, 0x0
/* .line 209 */
} // :cond_1
} // .end method
static void setConfigResetProcessStatus ( Boolean p0 ) {
/* .locals 2 */
/* .param p0, "status" # Z */
/* .line 90 */
if ( p0 != null) { // if-eqz p0, :cond_0
final String v0 = "1"; // const-string v0, "1"
} // :cond_0
final String v0 = "0"; // const-string v0, "0"
} // :goto_0
/* const-string/jumbo v1, "sys.rescuepartyplus.config_reset_process" */
android.os.SystemProperties .set ( v1,v0 );
/* .line 91 */
return;
} // .end method
static void setLastResetConfigStatus ( Boolean p0 ) {
/* .locals 2 */
/* .param p0, "status" # Z */
/* .line 98 */
if ( p0 != null) { // if-eqz p0, :cond_0
final String v0 = "1"; // const-string v0, "1"
} // :cond_0
final String v0 = "0"; // const-string v0, "0"
} // :goto_0
final String v1 = "persist.sys.rescuepartyplus.last_reset_config"; // const-string v1, "persist.sys.rescuepartyplus.last_reset_config"
android.os.SystemProperties .set ( v1,v0 );
/* .line 99 */
return;
} // .end method
static void setMitigationTempCount ( Integer p0 ) {
/* .locals 2 */
/* .param p0, "mitigationTempCount" # I */
/* .line 82 */
/* const-string/jumbo v0, "sys.rescuepartyplus.temp_mitigation_count" */
java.lang.String .valueOf ( p0 );
android.os.SystemProperties .set ( v0,v1 );
/* .line 83 */
return;
} // .end method
static void setShowResetConfigUIStatus ( Boolean p0 ) {
/* .locals 2 */
/* .param p0, "status" # Z */
/* .line 106 */
if ( p0 != null) { // if-eqz p0, :cond_0
final String v0 = "1"; // const-string v0, "1"
} // :cond_0
final String v0 = "0"; // const-string v0, "0"
} // :goto_0
/* const-string/jumbo v1, "sys.rescuepartyplus.ready_show_reset_config_ui" */
android.os.SystemProperties .set ( v1,v0 );
/* .line 107 */
return;
} // .end method
static java.util.Set tryGetCloudControlOrDefaultData ( ) {
/* .locals 15 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 214 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/data/mqsas/cloud/rescuePartyPlusCloudControl.json"; // const-string v1, "/data/mqsas/cloud/rescuePartyPlusCloudControl.json"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 215 */
/* .local v0, "cloudControlFile":Ljava/io/File; */
/* new-instance v1, Ljava/util/HashSet; */
/* invoke-direct {v1}, Ljava/util/HashSet;-><init>()V */
/* .line 216 */
/* .local v1, "cloudControlData":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;" */
v2 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 217 */
final String v2 = "Use rescueparty plus cloud control data!"; // const-string v2, "Use rescueparty plus cloud control data!"
final String v3 = "RescuePartyPlus"; // const-string v3, "RescuePartyPlus"
android.util.Slog .w ( v3,v2 );
/* .line 218 */
try { // :try_start_0
/* new-instance v2, Ljava/io/FileReader; */
/* invoke-direct {v2, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 219 */
/* .local v2, "fileReader":Ljava/io/FileReader; */
try { // :try_start_1
/* new-instance v4, Ljava/io/BufferedReader; */
/* invoke-direct {v4, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_2 */
/* .line 220 */
/* .local v4, "bufferedReader":Ljava/io/BufferedReader; */
try { // :try_start_2
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 221 */
/* .local v5, "stringBuilder":Ljava/lang/StringBuilder; */
final String v6 = ""; // const-string v6, ""
/* .line 222 */
/* .local v6, "str":Ljava/lang/String; */
} // :goto_0
(( java.io.BufferedReader ) v4 ).readLine ( ); // invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v6, v7 */
if ( v7 != null) { // if-eqz v7, :cond_0
/* .line 223 */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 226 */
} // :cond_0
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 227 */
/* .local v7, "originData":Ljava/lang/String; */
v8 = (( java.lang.String ) v7 ).isEmpty ( ); // invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z
/* if-nez v8, :cond_3 */
/* .line 228 */
/* new-instance v8, Lorg/json/JSONObject; */
/* invoke-direct {v8, v7}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* .line 229 */
/* .local v8, "controlJSONObject":Lorg/json/JSONObject; */
final String v9 = "controlData"; // const-string v9, "controlData"
(( org.json.JSONObject ) v8 ).optString ( v9 ); // invoke-virtual {v8, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 230 */
/* .local v9, "controlData":Ljava/lang/String; */
final String v10 = ";"; // const-string v10, ";"
(( java.lang.String ) v9 ).split ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 231 */
/* .local v10, "deleteFiles":[Ljava/lang/String; */
/* array-length v11, v10 */
int v12 = 0; // const/4 v12, 0x0
} // :goto_1
/* if-ge v12, v11, :cond_2 */
/* aget-object v13, v10, v12 */
/* .line 232 */
/* .local v13, "filePath":Ljava/lang/String; */
v14 = (( java.lang.String ) v13 ).isEmpty ( ); // invoke-virtual {v13}, Ljava/lang/String;->isEmpty()Z
/* if-nez v14, :cond_1 */
/* .line 233 */
(( java.util.HashSet ) v1 ).add ( v13 ); // invoke-virtual {v1, v13}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 231 */
} // .end local v13 # "filePath":Ljava/lang/String;
} // :cond_1
/* add-int/lit8 v12, v12, 0x1 */
/* .line 236 */
} // .end local v8 # "controlJSONObject":Lorg/json/JSONObject;
} // .end local v9 # "controlData":Ljava/lang/String;
} // .end local v10 # "deleteFiles":[Ljava/lang/String;
} // :cond_2
/* .line 237 */
} // :cond_3
final String v8 = "Cloud control data is empty!"; // const-string v8, "Cloud control data is empty!"
android.util.Slog .e ( v3,v8 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 239 */
} // .end local v5 # "stringBuilder":Ljava/lang/StringBuilder;
} // .end local v6 # "str":Ljava/lang/String;
} // .end local v7 # "originData":Ljava/lang/String;
} // :goto_2
try { // :try_start_3
(( java.io.BufferedReader ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_2 */
} // .end local v4 # "bufferedReader":Ljava/io/BufferedReader;
try { // :try_start_4
(( java.io.FileReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileReader;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_1 */
/* .catch Lorg/json/JSONException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 218 */
/* .restart local v4 # "bufferedReader":Ljava/io/BufferedReader; */
/* :catchall_0 */
/* move-exception v5 */
try { // :try_start_5
(( java.io.BufferedReader ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* :catchall_1 */
/* move-exception v6 */
try { // :try_start_6
(( java.lang.Throwable ) v5 ).addSuppressed ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "cloudControlFile":Ljava/io/File;
} // .end local v1 # "cloudControlData":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
} // .end local v2 # "fileReader":Ljava/io/FileReader;
} // :goto_3
/* throw v5 */
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_2 */
} // .end local v4 # "bufferedReader":Ljava/io/BufferedReader;
/* .restart local v0 # "cloudControlFile":Ljava/io/File; */
/* .restart local v1 # "cloudControlData":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;" */
/* .restart local v2 # "fileReader":Ljava/io/FileReader; */
/* :catchall_2 */
/* move-exception v4 */
try { // :try_start_7
(( java.io.FileReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileReader;->close()V
/* :try_end_7 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_3 */
/* :catchall_3 */
/* move-exception v5 */
try { // :try_start_8
(( java.lang.Throwable ) v4 ).addSuppressed ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "cloudControlFile":Ljava/io/File;
} // .end local v1 # "cloudControlData":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
} // :goto_4
/* throw v4 */
/* :try_end_8 */
/* .catch Ljava/io/IOException; {:try_start_8 ..:try_end_8} :catch_1 */
/* .catch Lorg/json/JSONException; {:try_start_8 ..:try_end_8} :catch_0 */
/* .line 241 */
} // .end local v2 # "fileReader":Ljava/io/FileReader;
/* .restart local v0 # "cloudControlFile":Ljava/io/File; */
/* .restart local v1 # "cloudControlData":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;" */
/* :catch_0 */
/* move-exception v2 */
/* .line 242 */
/* .local v2, "e":Lorg/json/JSONException; */
final String v4 = "Parse cloud control data failed!"; // const-string v4, "Parse cloud control data failed!"
android.util.Slog .e ( v3,v4,v2 );
} // .end local v2 # "e":Lorg/json/JSONException;
/* .line 239 */
/* :catch_1 */
/* move-exception v2 */
/* .line 240 */
/* .local v2, "e":Ljava/io/IOException; */
final String v4 = "Read cloud control data failed!"; // const-string v4, "Read cloud control data failed!"
android.util.Slog .e ( v3,v4,v2 );
/* .line 243 */
} // .end local v2 # "e":Ljava/io/IOException;
} // :goto_5
/* .line 245 */
} // :cond_4
v2 = com.android.server.RescuePartyPlusHelper.RESET_CONFIG_DEFAULT_SET;
(( java.util.HashSet ) v1 ).addAll ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z
/* .line 248 */
} // :goto_6
} // .end method
