public class com.android.server.ExtendMImpl extends android.app.job.JobService implements com.android.server.ExtendMStub {
	 /* .source "ExtendMImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.ExtendMStub$$" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/android/server/ExtendMImpl$MyHandler;, */
/* Lcom/android/server/ExtendMImpl$ExtendMRecord; */
/* } */
} // .end annotation
/* # static fields */
private static final java.lang.String BDEV_SYS;
private static final java.lang.String BROADCASTS_ACTION_MARK;
private static final java.lang.String CLOUD_DM_OPT_PROP;
private static final java.lang.String CLOUD_EXT_MEM_PROP;
private static final java.lang.String CLOUD_MFZ_PROP;
private static final Long DELAY_DURATION;
private static final Long DELAY_TIME;
private static final java.lang.String EXTM_SETTINGS_PROP;
private static final java.lang.String EXTM_UUID;
private static final Long FLUSH_DURATION;
private static final Integer FLUSH_HIGH_LEVEL;
private static final Long FLUSH_INTERVAL_LIMIT;
private static final Integer FLUSH_LEVEL;
private static final Integer FLUSH_LOW_LEVEL;
private static final Integer FLUSH_MEDIUM_LEVEL;
private static final Integer FLUSH_RWTHRESHOLD_PART;
private static final Integer FLUSH_ZRAM_USEDRATE;
private static final Boolean LOG_VERBOSE;
private static final Long MARK_DURATION;
private static final java.lang.String MEMINFO_MEM_TOTAL;
private static final java.lang.String MEMINFO_SWAP_FREE;
private static final java.lang.String MEMINFO_SWAP_TOTAL;
private static final Boolean MEMORY_FREEZE_ENABLE;
private static final java.lang.String MEM_CONF_FILE_PATH;
private static final java.lang.String MFZ_ENABLE_SYS;
private static final Boolean MIUI_DM_OPT_ENABLE;
private static final Integer MM_STATS_MAX_FILE_SIZE;
private static final java.lang.String MM_STATS_SYS;
private static final Integer MSG_DO_FLUSH_HIGH;
private static final Integer MSG_DO_FLUSH_LOW;
private static final Integer MSG_DO_FLUSH_MEDIUM;
private static final Integer MSG_DO_MARK;
private static final Integer MSG_START_EXTM;
private static final Integer MSG_STOP_FLUSH;
private static final java.lang.String OLD_COUNT_SYS;
private static final Integer PER_FLUSH_QUOTA_HIGH;
private static final Integer PER_FLUSH_QUOTA_LOW;
private static final Integer PER_FLUSH_QUOTA_MEDIUM;
private static final Integer REPORTMEMP_EXTM_LIMIT;
private static final Integer RESET_WB_LIMIT_JOB_ID;
private static final Integer STATE_NOT_RUNNING;
private static final Integer STATE_WAIT_FOR_MARK;
private static final Integer STATE_WAIT_FOR_RESUME_FLUSH;
private static final java.lang.String TAG;
private static final java.lang.String TAG_AM;
private static final Long THIRTY_SECONDS;
private static final Long THREE_MINUTES;
private static final java.lang.String VERSION;
private static final java.lang.String WB_LIMIT_ENABLE_SYS;
private static final java.lang.String WB_LIMIT_SYS;
private static final Integer WB_STATS_MAX_FILE_SIZE;
private static final java.lang.String WB_STATS_SYS;
private static final java.lang.String WB_SYS_LIMIT_PAGES;
private static java.util.ArrayList extmGear;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Float;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final android.content.ComponentName sExtendM;
private static com.android.server.ExtendMImpl$ExtendMRecord sExtendMRecord;
private static java.util.ArrayList sMemTags;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static com.android.server.ExtendMImpl sSelf;
/* # instance fields */
private android.os.IBinder binder;
android.os.IBinder$DeathRecipient death;
private Boolean dmoptLocalCloudEnable;
private Integer flush_level_adjust;
private Integer flush_number_adjust;
private Integer flush_per_quota;
private Boolean hasParseMemConfSuccessful;
private Boolean isFlushFinished;
private Boolean isInitSuccess;
private Integer kernelSupportCheckCnt;
private Long lastFlushTime;
private Long lastReportMPtime;
private android.content.Context mContext;
private Integer mCurState;
private Long mFlushDiffTime;
private com.android.server.ExtendMImpl$MyHandler mHandler;
private Integer mLastOldPages;
private Boolean mLocalCloudEnable;
private Long mMarkDiffTime;
private Long mMarkStartTime;
private java.util.Map mMemInfo;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.content.BroadcastReceiver mReceiver;
private Long mScreenOnTime;
private android.os.HandlerThread mThread;
private Long mTotalFlushed;
private Long mTotalMarked;
private volatile android.os.IVold mVold;
private Boolean mfzLocalCloudEnable;
private Integer prev_bd_read;
private Integer prev_bd_write;
private Integer reportMemPressureCnt;
/* # direct methods */
static Boolean -$$Nest$fgetisFlushFinished ( com.android.server.ExtendMImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/ExtendMImpl;->isFlushFinished:Z */
} // .end method
static Long -$$Nest$fgetlastFlushTime ( com.android.server.ExtendMImpl p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/ExtendMImpl;->lastFlushTime:J */
/* return-wide v0 */
} // .end method
static Integer -$$Nest$fgetmCurState ( com.android.server.ExtendMImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/ExtendMImpl;->mCurState:I */
} // .end method
static Long -$$Nest$fgetmFlushDiffTime ( com.android.server.ExtendMImpl p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/ExtendMImpl;->mFlushDiffTime:J */
/* return-wide v0 */
} // .end method
static com.android.server.ExtendMImpl$MyHandler -$$Nest$fgetmHandler ( com.android.server.ExtendMImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static Long -$$Nest$fgetmMarkDiffTime ( com.android.server.ExtendMImpl p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/ExtendMImpl;->mMarkDiffTime:J */
/* return-wide v0 */
} // .end method
static Long -$$Nest$fgetmMarkStartTime ( com.android.server.ExtendMImpl p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/ExtendMImpl;->mMarkStartTime:J */
/* return-wide v0 */
} // .end method
static java.util.Map -$$Nest$fgetmMemInfo ( com.android.server.ExtendMImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMemInfo;
} // .end method
static Long -$$Nest$fgetmScreenOnTime ( com.android.server.ExtendMImpl p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/ExtendMImpl;->mScreenOnTime:J */
/* return-wide v0 */
} // .end method
static Integer -$$Nest$fgetprev_bd_write ( com.android.server.ExtendMImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/ExtendMImpl;->prev_bd_write:I */
} // .end method
static void -$$Nest$fputisFlushFinished ( com.android.server.ExtendMImpl p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/ExtendMImpl;->isFlushFinished:Z */
return;
} // .end method
static void -$$Nest$fputlastFlushTime ( com.android.server.ExtendMImpl p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-wide p1, p0, Lcom/android/server/ExtendMImpl;->lastFlushTime:J */
return;
} // .end method
static void -$$Nest$fputmFlushDiffTime ( com.android.server.ExtendMImpl p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-wide p1, p0, Lcom/android/server/ExtendMImpl;->mFlushDiffTime:J */
return;
} // .end method
static void -$$Nest$fputmMarkDiffTime ( com.android.server.ExtendMImpl p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-wide p1, p0, Lcom/android/server/ExtendMImpl;->mMarkDiffTime:J */
return;
} // .end method
static void -$$Nest$fputmMarkStartTime ( com.android.server.ExtendMImpl p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-wide p1, p0, Lcom/android/server/ExtendMImpl;->mMarkStartTime:J */
return;
} // .end method
static void -$$Nest$fputmScreenOnTime ( com.android.server.ExtendMImpl p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-wide p1, p0, Lcom/android/server/ExtendMImpl;->mScreenOnTime:J */
return;
} // .end method
static void -$$Nest$fputmVold ( com.android.server.ExtendMImpl p0, android.os.IVold p1 ) { //bridge//synthethic
/* .locals 0 */
this.mVold = p1;
return;
} // .end method
static void -$$Nest$mParseMfzSettings ( com.android.server.ExtendMImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->ParseMfzSettings()V */
return;
} // .end method
static void -$$Nest$mParsedmoptSettings ( com.android.server.ExtendMImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->ParsedmoptSettings()V */
return;
} // .end method
static void -$$Nest$mSetMfzEnable ( com.android.server.ExtendMImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->SetMfzEnable()V */
return;
} // .end method
static void -$$Nest$mSetdmoptEnable ( com.android.server.ExtendMImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->SetdmoptEnable()V */
return;
} // .end method
static void -$$Nest$mconnect ( com.android.server.ExtendMImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->connect()V */
return;
} // .end method
static Integer -$$Nest$mgetQuotaLeft ( com.android.server.ExtendMImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->getQuotaLeft()I */
} // .end method
static void -$$Nest$mreadMemInfo ( com.android.server.ExtendMImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->readMemInfo()V */
return;
} // .end method
static void -$$Nest$mrefreshExtmSettings ( com.android.server.ExtendMImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->refreshExtmSettings()V */
return;
} // .end method
static void -$$Nest$mregisterAlarmClock ( com.android.server.ExtendMImpl p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/ExtendMImpl;->registerAlarmClock(J)V */
return;
} // .end method
static Boolean -$$Nest$mrunFlush ( com.android.server.ExtendMImpl p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/ExtendMImpl;->runFlush(I)Z */
} // .end method
static void -$$Nest$mshowAndSaveFlushedStat ( com.android.server.ExtendMImpl p0, Integer p1, Long p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/ExtendMImpl;->showAndSaveFlushedStat(IJ)V */
return;
} // .end method
static void -$$Nest$mshowTotalStat ( com.android.server.ExtendMImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->showTotalStat()V */
return;
} // .end method
static void -$$Nest$mstartExtM ( com.android.server.ExtendMImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->startExtM()V */
return;
} // .end method
static void -$$Nest$mstopFlush ( com.android.server.ExtendMImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->stopFlush()V */
return;
} // .end method
static Boolean -$$Nest$mtryToFlushPages ( com.android.server.ExtendMImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->tryToFlushPages()Z */
} // .end method
static Boolean -$$Nest$mtryToMarkPages ( com.android.server.ExtendMImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->tryToMarkPages()Z */
} // .end method
static void -$$Nest$mtryToStopFlush ( com.android.server.ExtendMImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->tryToStopFlush()V */
return;
} // .end method
static void -$$Nest$munregisterAlarmClock ( com.android.server.ExtendMImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->unregisterAlarmClock()V */
return;
} // .end method
static void -$$Nest$mupdateState ( com.android.server.ExtendMImpl p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/ExtendMImpl;->updateState(I)V */
return;
} // .end method
static Long -$$Nest$sfgetFLUSH_DURATION ( ) { //bridge//synthethic
/* .locals 2 */
/* sget-wide v0, Lcom/android/server/ExtendMImpl;->FLUSH_DURATION:J */
/* return-wide v0 */
} // .end method
static Integer -$$Nest$sfgetFLUSH_HIGH_LEVEL ( ) { //bridge//synthethic
/* .locals 1 */
} // .end method
static Integer -$$Nest$sfgetFLUSH_LOW_LEVEL ( ) { //bridge//synthethic
/* .locals 1 */
} // .end method
static Integer -$$Nest$sfgetFLUSH_MEDIUM_LEVEL ( ) { //bridge//synthethic
/* .locals 1 */
} // .end method
static Boolean -$$Nest$sfgetLOG_VERBOSE ( ) { //bridge//synthethic
/* .locals 1 */
/* sget-boolean v0, Lcom/android/server/ExtendMImpl;->LOG_VERBOSE:Z */
} // .end method
static Long -$$Nest$sfgetMARK_DURATION ( ) { //bridge//synthethic
/* .locals 2 */
/* sget-wide v0, Lcom/android/server/ExtendMImpl;->MARK_DURATION:J */
/* return-wide v0 */
} // .end method
static com.android.server.ExtendMImpl ( ) {
/* .locals 6 */
/* .line 77 */
final String v0 = "persist.miui.extm.debug_enable"; // const-string v0, "persist.miui.extm.debug_enable"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.ExtendMImpl.LOG_VERBOSE = (v0!= 0);
/* .line 80 */
final String v0 = "persist.miui.extm.enable"; // const-string v0, "persist.miui.extm.enable"
final String v2 = "0"; // const-string v2, "0"
android.os.SystemProperties .get ( v0,v2 );
/* .line 81 */
final String v0 = "persist.sys.mfz.enable"; // const-string v0, "persist.sys.mfz.enable"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.ExtendMImpl.MEMORY_FREEZE_ENABLE = (v0!= 0);
/* .line 82 */
final String v0 = "persist.miui.extm.dm_opt.enable"; // const-string v0, "persist.miui.extm.dm_opt.enable"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.ExtendMImpl.MIUI_DM_OPT_ENABLE = (v0!= 0);
/* .line 87 */
/* new-instance v0, Landroid/content/ComponentName; */
/* .line 88 */
/* const-class v1, Lcom/android/server/ExtendMImpl; */
(( java.lang.Class ) v1 ).getName ( ); // invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;
final String v2 = "android"; // const-string v2, "android"
/* invoke-direct {v0, v2, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 124 */
final String v0 = "persist.lm.em.flush_level"; // const-string v0, "persist.lm.em.flush_level"
int v1 = 5; // const/4 v1, 0x5
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* .line 126 */
final String v0 = "persist.lm.em.mark_duration"; // const-string v0, "persist.lm.em.mark_duration"
/* const-wide/32 v2, 0x1d4c0 */
android.os.SystemProperties .getLong ( v0,v2,v3 );
/* move-result-wide v2 */
/* sput-wide v2, Lcom/android/server/ExtendMImpl;->MARK_DURATION:J */
/* .line 128 */
final String v0 = "persist.lm.em.flush_duration"; // const-string v0, "persist.lm.em.flush_duration"
/* const-wide/32 v2, 0x927c0 */
android.os.SystemProperties .getLong ( v0,v2,v3 );
/* move-result-wide v2 */
/* sput-wide v2, Lcom/android/server/ExtendMImpl;->FLUSH_DURATION:J */
/* .line 130 */
final String v0 = "persist.lm.em.delay_duration"; // const-string v0, "persist.lm.em.delay_duration"
/* const-wide/16 v2, 0x7530 */
android.os.SystemProperties .getLong ( v0,v2,v3 );
/* move-result-wide v4 */
/* sput-wide v4, Lcom/android/server/ExtendMImpl;->DELAY_DURATION:J */
/* .line 132 */
final String v0 = "persist.lm.em.flush_interval_limit"; // const-string v0, "persist.lm.em.flush_interval_limit"
android.os.SystemProperties .getLong ( v0,v2,v3 );
/* move-result-wide v2 */
/* sput-wide v2, Lcom/android/server/ExtendMImpl;->FLUSH_INTERVAL_LIMIT:J */
/* .line 134 */
final String v0 = "persist.lm.em.per_flush_quota_low"; // const-string v0, "persist.lm.em.per_flush_quota_low"
/* const/16 v2, 0x1388 */
v0 = android.os.SystemProperties .getInt ( v0,v2 );
/* .line 136 */
final String v0 = "persist.lm.em.per_flush_quota_medium"; // const-string v0, "persist.lm.em.per_flush_quota_medium"
/* const/16 v2, 0xbb8 */
v0 = android.os.SystemProperties .getInt ( v0,v2 );
/* .line 138 */
final String v0 = "persist.lm.em.per_flush_quota_high"; // const-string v0, "persist.lm.em.per_flush_quota_high"
/* const/16 v2, 0x7d0 */
v0 = android.os.SystemProperties .getInt ( v0,v2 );
/* .line 140 */
final String v0 = "persist.lm.em.flush_zram_usedrate"; // const-string v0, "persist.lm.em.flush_zram_usedrate"
/* const/16 v2, 0x32 */
v0 = android.os.SystemProperties .getInt ( v0,v2 );
/* .line 142 */
final String v0 = "persist.lm.em.flush_rwthreshold_part"; // const-string v0, "persist.lm.em.flush_rwthreshold_part"
int v2 = 3; // const/4 v2, 0x3
v0 = android.os.SystemProperties .getInt ( v0,v2 );
/* .line 162 */
final String v0 = "persist.em.low_flush_level"; // const-string v0, "persist.em.low_flush_level"
/* const/16 v2, 0xa */
v0 = android.os.SystemProperties .getInt ( v0,v2 );
/* .line 164 */
final String v0 = "persist.em.medium_flush_level"; // const-string v0, "persist.em.medium_flush_level"
/* const/16 v2, 0x8 */
v0 = android.os.SystemProperties .getInt ( v0,v2 );
/* .line 166 */
final String v0 = "persist.em.high_flush_level"; // const-string v0, "persist.em.high_flush_level"
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* .line 177 */
final String v0 = "persist.miui.extm.memory_conf_file"; // const-string v0, "persist.miui.extm.memory_conf_file"
final String v1 = "/system_ext/etc/perfinit_bdsize_zram.conf"; // const-string v1, "/system_ext/etc/perfinit_bdsize_zram.conf"
android.os.SystemProperties .get ( v0,v1 );
/* .line 179 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 182 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 184 */
final String v1 = "SwapTotal"; // const-string v1, "SwapTotal"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 185 */
v0 = com.android.server.ExtendMImpl.sMemTags;
final String v1 = "SwapFree"; // const-string v1, "SwapFree"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 186 */
v0 = com.android.server.ExtendMImpl.sMemTags;
final String v1 = "MemTotal"; // const-string v1, "MemTotal"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 187 */
return;
} // .end method
public com.android.server.ExtendMImpl ( ) {
/* .locals 5 */
/* .line 189 */
/* invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V */
/* .line 108 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/ExtendMImpl;->mCurState:I */
/* .line 109 */
/* iput-boolean v0, p0, Lcom/android/server/ExtendMImpl;->mLocalCloudEnable:Z */
/* .line 110 */
/* iput-boolean v0, p0, Lcom/android/server/ExtendMImpl;->mfzLocalCloudEnable:Z */
/* .line 111 */
/* iput-boolean v0, p0, Lcom/android/server/ExtendMImpl;->dmoptLocalCloudEnable:Z */
/* .line 112 */
/* iput v0, p0, Lcom/android/server/ExtendMImpl;->mLastOldPages:I */
/* .line 113 */
/* const-wide/16 v1, 0x0 */
/* iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->mTotalMarked:J */
/* .line 114 */
/* iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->mTotalFlushed:J */
/* .line 115 */
/* iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->lastFlushTime:J */
/* .line 116 */
/* const/16 v3, 0xa */
/* iput v3, p0, Lcom/android/server/ExtendMImpl;->flush_level_adjust:I */
/* .line 117 */
/* iput v0, p0, Lcom/android/server/ExtendMImpl;->flush_number_adjust:I */
/* .line 118 */
/* iput v0, p0, Lcom/android/server/ExtendMImpl;->flush_per_quota:I */
/* .line 119 */
/* iput v0, p0, Lcom/android/server/ExtendMImpl;->prev_bd_write:I */
/* .line 120 */
/* iput v0, p0, Lcom/android/server/ExtendMImpl;->prev_bd_read:I */
/* .line 123 */
/* new-instance v3, Ljava/util/HashMap; */
/* invoke-direct {v3}, Ljava/util/HashMap;-><init>()V */
this.mMemInfo = v3;
/* .line 149 */
/* iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->lastReportMPtime:J */
/* .line 150 */
/* iput v0, p0, Lcom/android/server/ExtendMImpl;->reportMemPressureCnt:I */
/* .line 151 */
/* iput v0, p0, Lcom/android/server/ExtendMImpl;->kernelSupportCheckCnt:I */
/* .line 169 */
/* new-instance v3, Landroid/os/HandlerThread; */
final String v4 = "ExtM"; // const-string v4, "ExtM"
/* invoke-direct {v3, v4}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mThread = v3;
/* .line 170 */
/* iput-boolean v0, p0, Lcom/android/server/ExtendMImpl;->isInitSuccess:Z */
/* .line 171 */
int v3 = 0; // const/4 v3, 0x0
this.mHandler = v3;
/* .line 180 */
/* iput-boolean v0, p0, Lcom/android/server/ExtendMImpl;->hasParseMemConfSuccessful:Z */
/* .line 941 */
/* iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->mScreenOnTime:J */
/* .line 942 */
/* iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->mMarkStartTime:J */
/* .line 943 */
/* iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->mMarkDiffTime:J */
/* .line 944 */
/* iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->mFlushDiffTime:J */
/* .line 946 */
/* new-instance v0, Lcom/android/server/ExtendMImpl$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/ExtendMImpl$1;-><init>(Lcom/android/server/ExtendMImpl;)V */
this.mReceiver = v0;
/* .line 1084 */
/* new-instance v0, Lcom/android/server/ExtendMImpl$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/ExtendMImpl$2;-><init>(Lcom/android/server/ExtendMImpl;)V */
this.death = v0;
/* .line 1173 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/ExtendMImpl;->isFlushFinished:Z */
/* .line 190 */
return;
} // .end method
public com.android.server.ExtendMImpl ( ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 192 */
/* invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V */
/* .line 108 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/ExtendMImpl;->mCurState:I */
/* .line 109 */
/* iput-boolean v0, p0, Lcom/android/server/ExtendMImpl;->mLocalCloudEnable:Z */
/* .line 110 */
/* iput-boolean v0, p0, Lcom/android/server/ExtendMImpl;->mfzLocalCloudEnable:Z */
/* .line 111 */
/* iput-boolean v0, p0, Lcom/android/server/ExtendMImpl;->dmoptLocalCloudEnable:Z */
/* .line 112 */
/* iput v0, p0, Lcom/android/server/ExtendMImpl;->mLastOldPages:I */
/* .line 113 */
/* const-wide/16 v1, 0x0 */
/* iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->mTotalMarked:J */
/* .line 114 */
/* iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->mTotalFlushed:J */
/* .line 115 */
/* iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->lastFlushTime:J */
/* .line 116 */
/* const/16 v3, 0xa */
/* iput v3, p0, Lcom/android/server/ExtendMImpl;->flush_level_adjust:I */
/* .line 117 */
/* iput v0, p0, Lcom/android/server/ExtendMImpl;->flush_number_adjust:I */
/* .line 118 */
/* iput v0, p0, Lcom/android/server/ExtendMImpl;->flush_per_quota:I */
/* .line 119 */
/* iput v0, p0, Lcom/android/server/ExtendMImpl;->prev_bd_write:I */
/* .line 120 */
/* iput v0, p0, Lcom/android/server/ExtendMImpl;->prev_bd_read:I */
/* .line 123 */
/* new-instance v3, Ljava/util/HashMap; */
/* invoke-direct {v3}, Ljava/util/HashMap;-><init>()V */
this.mMemInfo = v3;
/* .line 149 */
/* iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->lastReportMPtime:J */
/* .line 150 */
/* iput v0, p0, Lcom/android/server/ExtendMImpl;->reportMemPressureCnt:I */
/* .line 151 */
/* iput v0, p0, Lcom/android/server/ExtendMImpl;->kernelSupportCheckCnt:I */
/* .line 169 */
/* new-instance v3, Landroid/os/HandlerThread; */
final String v4 = "ExtM"; // const-string v4, "ExtM"
/* invoke-direct {v3, v4}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mThread = v3;
/* .line 170 */
/* iput-boolean v0, p0, Lcom/android/server/ExtendMImpl;->isInitSuccess:Z */
/* .line 171 */
int v3 = 0; // const/4 v3, 0x0
this.mHandler = v3;
/* .line 180 */
/* iput-boolean v0, p0, Lcom/android/server/ExtendMImpl;->hasParseMemConfSuccessful:Z */
/* .line 941 */
/* iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->mScreenOnTime:J */
/* .line 942 */
/* iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->mMarkStartTime:J */
/* .line 943 */
/* iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->mMarkDiffTime:J */
/* .line 944 */
/* iput-wide v1, p0, Lcom/android/server/ExtendMImpl;->mFlushDiffTime:J */
/* .line 946 */
/* new-instance v0, Lcom/android/server/ExtendMImpl$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/ExtendMImpl$1;-><init>(Lcom/android/server/ExtendMImpl;)V */
this.mReceiver = v0;
/* .line 1084 */
/* new-instance v0, Lcom/android/server/ExtendMImpl$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/ExtendMImpl$2;-><init>(Lcom/android/server/ExtendMImpl;)V */
this.death = v0;
/* .line 1173 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/ExtendMImpl;->isFlushFinished:Z */
/* .line 193 */
this.mContext = p1;
/* .line 194 */
return;
} // .end method
private void ParseMfzSettings ( ) {
/* .locals 4 */
/* .line 346 */
final String v0 = "Enter mfz cloud control"; // const-string v0, "Enter mfz cloud control"
final String v1 = "MFZ"; // const-string v1, "MFZ"
android.util.Slog .i ( v1,v0 );
/* .line 348 */
v0 = /* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->getMfzCloud()I */
/* .line 349 */
/* .local v0, "mfzCloudEnable":I */
/* if-ltz v0, :cond_2 */
int v2 = 1; // const/4 v2, 0x1
/* if-le v0, v2, :cond_0 */
/* .line 353 */
} // :cond_0
/* if-nez v0, :cond_1 */
/* .line 354 */
int v2 = 0; // const/4 v2, 0x0
/* iput-boolean v2, p0, Lcom/android/server/ExtendMImpl;->mfzLocalCloudEnable:Z */
/* .line 356 */
} // :cond_1
/* iput-boolean v2, p0, Lcom/android/server/ExtendMImpl;->mfzLocalCloudEnable:Z */
/* .line 358 */
} // :goto_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "CloudEnable: "; // const-string v3, "CloudEnable: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/android/server/ExtendMImpl;->mfzLocalCloudEnable:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = ", Exit mfz cloud control"; // const-string v3, ", Exit mfz cloud control"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 359 */
return;
/* .line 350 */
} // :cond_2
} // :goto_1
final String v2 = "Invalid cloud control enable"; // const-string v2, "Invalid cloud control enable"
android.util.Slog .i ( v1,v2 );
/* .line 351 */
return;
} // .end method
private void ParsedmoptSettings ( ) {
/* .locals 4 */
/* .line 363 */
final String v0 = "Enter dmopt cloud control"; // const-string v0, "Enter dmopt cloud control"
final String v1 = "ExtM"; // const-string v1, "ExtM"
android.util.Slog .i ( v1,v0 );
/* .line 365 */
v0 = /* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->getdmoptCloud()I */
/* .line 366 */
/* .local v0, "dmoptCloudEnable":I */
/* if-ltz v0, :cond_2 */
int v2 = 1; // const/4 v2, 0x1
/* if-le v0, v2, :cond_0 */
/* .line 370 */
} // :cond_0
/* if-nez v0, :cond_1 */
/* .line 371 */
int v2 = 0; // const/4 v2, 0x0
/* iput-boolean v2, p0, Lcom/android/server/ExtendMImpl;->dmoptLocalCloudEnable:Z */
/* .line 373 */
} // :cond_1
/* iput-boolean v2, p0, Lcom/android/server/ExtendMImpl;->dmoptLocalCloudEnable:Z */
/* .line 375 */
} // :goto_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "CloudEnable:"; // const-string v3, "CloudEnable:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/android/server/ExtendMImpl;->dmoptLocalCloudEnable:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = ",Exit dmopt cloud control"; // const-string v3, ",Exit dmopt cloud control"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 376 */
return;
/* .line 367 */
} // :cond_2
} // :goto_1
final String v2 = "Invalid cloud control enable"; // const-string v2, "Invalid cloud control enable"
android.util.Slog .i ( v1,v2 );
/* .line 368 */
return;
} // .end method
private void SetMfzEnable ( ) {
/* .locals 3 */
/* .line 762 */
try { // :try_start_0
v0 = /* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->getMfzCloud()I */
/* .line 763 */
/* .local v0, "MFZ_SYS_Control":I */
final String v1 = "persist.sys.mfz.enable"; // const-string v1, "persist.sys.mfz.enable"
v2 = (( com.android.server.ExtendMImpl ) p0 ).isMfzCloud ( ); // invoke-virtual {p0}, Lcom/android/server/ExtendMImpl;->isMfzCloud()Z
java.lang.String .valueOf ( v2 );
android.os.SystemProperties .set ( v1,v2 );
/* .line 764 */
/* new-instance v1, Ljava/io/File; */
final String v2 = "/sys/block/zram0/mfz_enable"; // const-string v2, "/sys/block/zram0/mfz_enable"
/* invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
java.lang.String .valueOf ( v0 );
android.os.FileUtils .stringToFile ( v1,v2 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 767 */
} // .end local v0 # "MFZ_SYS_Control":I
/* .line 765 */
/* :catch_0 */
/* move-exception v0 */
/* .line 766 */
/* .local v0, "e":Ljava/io/IOException; */
final String v1 = "MFZ"; // const-string v1, "MFZ"
final String v2 = "Failed to set mfz sys"; // const-string v2, "Failed to set mfz sys"
android.util.Slog .e ( v1,v2 );
/* .line 768 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_0
return;
} // .end method
private void SetdmoptEnable ( ) {
/* .locals 3 */
/* .line 772 */
try { // :try_start_0
/* sget-boolean v0, Lcom/android/server/ExtendMImpl;->MIUI_DM_OPT_ENABLE:Z */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* .line 773 */
final String v0 = "persist.miui.extm.dm_opt.enable"; // const-string v0, "persist.miui.extm.dm_opt.enable"
v1 = (( com.android.server.ExtendMImpl ) p0 ).isdmoptCloud ( ); // invoke-virtual {p0}, Lcom/android/server/ExtendMImpl;->isdmoptCloud()Z
java.lang.String .valueOf ( v1 );
android.os.SystemProperties .set ( v0,v1 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 777 */
} // :cond_0
/* .line 775 */
/* :catch_0 */
/* move-exception v0 */
/* .line 776 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "MFZ"; // const-string v1, "MFZ"
final String v2 = "Failed to set dmopt sys"; // const-string v2, "Failed to set dmopt sys"
android.util.Slog .e ( v1,v2 );
/* .line 778 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private changeArrayListToFloatArray ( java.util.ArrayList p0 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Float;", */
/* ">;)[F" */
/* } */
} // .end annotation
/* .line 657 */
/* .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;" */
v0 = (( java.util.ArrayList ) p1 ).size ( ); // invoke-virtual {p1}, Ljava/util/ArrayList;->size()I
/* new-array v0, v0, [F */
/* .line 658 */
/* .local v0, "ret":[F */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = (( java.util.ArrayList ) p1 ).size ( ); // invoke-virtual {p1}, Ljava/util/ArrayList;->size()I
/* if-ge v1, v2, :cond_0 */
/* .line 659 */
(( java.util.ArrayList ) p1 ).get ( v1 ); // invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
/* aput v2, v0, v1 */
/* .line 658 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 661 */
} // .end local v1 # "i":I
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "arrayList value is: "; // const-string v2, "arrayList value is: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .toString ( v0 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "ExtM"; // const-string v2, "ExtM"
android.util.Slog .d ( v2,v1 );
/* .line 662 */
} // .end method
private void checkTime ( Long p0, java.lang.String p1, Long p2 ) {
/* .locals 5 */
/* .param p1, "startTime" # J */
/* .param p3, "where" # Ljava/lang/String; */
/* .param p4, "threshold" # J */
/* .line 912 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 913 */
/* .local v0, "now":J */
/* sub-long v2, v0, p1 */
/* cmp-long v2, v2, p4 */
/* if-lez v2, :cond_0 */
/* .line 914 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Slow operation: "; // const-string v3, "Slow operation: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sub-long v3, v0, p1 */
(( java.lang.StringBuilder ) v2 ).append ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v3 = "ms so far, now at "; // const-string v3, "ms so far, now at "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p3 ); // invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "ExtM"; // const-string v3, "ExtM"
android.util.Slog .w ( v3,v2 );
/* .line 916 */
} // :cond_0
return;
} // .end method
private void connect ( ) {
/* .locals 3 */
/* .line 1094 */
/* const-string/jumbo v0, "vold" */
android.os.ServiceManager .getService ( v0 );
this.binder = v0;
/* .line 1095 */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mVold;
/* if-nez v0, :cond_0 */
/* .line 1097 */
try { // :try_start_0
v0 = this.binder;
v1 = this.death;
int v2 = 0; // const/4 v2, 0x0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1100 */
/* .line 1098 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1099 */
/* .local v0, "e":Landroid/os/RemoteException; */
int v1 = 0; // const/4 v1, 0x0
this.binder = v1;
/* .line 1102 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :cond_0
} // :goto_0
v0 = this.binder;
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mVold;
/* if-nez v0, :cond_1 */
/* .line 1103 */
final String v0 = "ExtM"; // const-string v0, "ExtM"
final String v1 = "init vold"; // const-string v1, "init vold"
android.util.Slog .i ( v0,v1 );
/* .line 1104 */
v0 = this.binder;
android.os.IVold$Stub .asInterface ( v0 );
this.mVold = v0;
/* .line 1106 */
} // :cond_1
return;
} // .end method
private void disconnect ( ) {
/* .locals 3 */
/* .line 1109 */
v0 = this.binder;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mVold;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1110 */
v0 = this.binder;
v1 = this.death;
int v2 = 0; // const/4 v2, 0x0
/* .line 1112 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
this.mVold = v0;
/* .line 1113 */
this.binder = v0;
/* .line 1114 */
return;
} // .end method
private void enableFlushQuota ( ) {
/* .locals 3 */
/* .line 745 */
try { // :try_start_0
/* new-instance v0, Ljava/io/File; */
final String v1 = "/sys/block/zram0/writeback_limit_enable"; // const-string v1, "/sys/block/zram0/writeback_limit_enable"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
final String v1 = "1"; // const-string v1, "1"
android.os.FileUtils .stringToFile ( v0,v1 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 748 */
/* .line 746 */
/* :catch_0 */
/* move-exception v0 */
/* .line 747 */
/* .local v0, "e":Ljava/io/IOException; */
final String v1 = "ExtM"; // const-string v1, "ExtM"
final String v2 = "Failed to enable flush quota "; // const-string v2, "Failed to enable flush quota "
android.util.Slog .e ( v1,v2 );
/* .line 749 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_0
return;
} // .end method
private void exit ( ) {
/* .locals 2 */
/* .line 317 */
v0 = this.mContext;
v1 = this.mReceiver;
(( android.content.Context ) v0 ).unregisterReceiver ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
/* .line 318 */
final String v0 = "exit: unregister screen on/off monitor"; // const-string v0, "exit: unregister screen on/off monitor"
final String v1 = "ExtM"; // const-string v1, "ExtM"
android.util.Slog .i ( v1,v0 );
/* .line 319 */
/* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->disconnect()V */
/* .line 320 */
final String v0 = "exit: disconnect vold"; // const-string v0, "exit: disconnect vold"
android.util.Slog .i ( v1,v0 );
/* .line 321 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/ExtendMImpl;->mCurState:I */
/* .line 322 */
/* iput-boolean v0, p0, Lcom/android/server/ExtendMImpl;->isInitSuccess:Z */
/* .line 323 */
return;
} // .end method
private Boolean flushIsAllowed ( ) {
/* .locals 7 */
/* .line 881 */
/* iget-wide v0, p0, Lcom/android/server/ExtendMImpl;->lastFlushTime:J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
int v1 = 0; // const/4 v1, 0x0
final String v2 = "ExtM"; // const-string v2, "ExtM"
if ( v0 != null) { // if-eqz v0, :cond_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v3 */
/* iget-wide v5, p0, Lcom/android/server/ExtendMImpl;->lastFlushTime:J */
/* sub-long/2addr v3, v5 */
/* sget-wide v5, Lcom/android/server/ExtendMImpl;->FLUSH_INTERVAL_LIMIT:J */
/* cmp-long v0, v3, v5 */
/* if-gez v0, :cond_0 */
/* .line 882 */
final String v0 = "runFlush interval less than FLUSH_INTERVAL_LIMIT, skip flush"; // const-string v0, "runFlush interval less than FLUSH_INTERVAL_LIMIT, skip flush"
android.util.Slog .i ( v2,v0 );
/* .line 883 */
/* .line 887 */
} // :cond_0
v3 = /* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->getZramUseRate()I */
/* if-le v0, v3, :cond_1 */
/* .line 888 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "zram userate is low, no need to flush: " */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = /* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->getZramUseRate()I */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v0 );
/* .line 889 */
/* .line 891 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // .end method
private Integer getCloudPercent ( ) {
/* .locals 4 */
/* .line 207 */
/* const/16 v0, 0x9 */
/* .line 209 */
/* .local v0, "percent":I */
try { // :try_start_0
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "cloud_extm_percent"; // const-string v2, "cloud_extm_percent"
int v3 = -2; // const/4 v3, -0x2
v1 = android.provider.Settings$System .getIntForUser ( v1,v2,v3 );
/* :try_end_0 */
/* .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v0, v1 */
/* .line 214 */
/* .line 211 */
/* :catch_0 */
/* move-exception v1 */
/* .line 212 */
/* .local v1, "e":Landroid/provider/Settings$SettingNotFoundException; */
final String v2 = "ExtM"; // const-string v2, "ExtM"
final String v3 = "Do not get local cloud percent, set as 9"; // const-string v3, "Do not get local cloud percent, set as 9"
android.util.Slog .i ( v2,v3 );
/* .line 213 */
/* const/16 v0, 0x9 */
/* .line 215 */
} // .end local v1 # "e":Landroid/provider/Settings$SettingNotFoundException;
} // :goto_0
} // .end method
private Integer getDataSizeGB ( ) {
/* .locals 9 */
/* .line 615 */
/* new-instance v0, Landroid/os/StatFs; */
final String v1 = "/data"; // const-string v1, "/data"
/* invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V */
/* .line 616 */
/* .local v0, "statFs":Landroid/os/StatFs; */
(( android.os.StatFs ) v0 ).getTotalBytes ( ); // invoke-virtual {v0}, Landroid/os/StatFs;->getTotalBytes()J
/* move-result-wide v1 */
/* const-wide/32 v3, 0x40000000 */
/* div-long/2addr v1, v3 */
/* .line 617 */
/* .local v1, "flashTotal":J */
/* const-wide/16 v3, 0x10 */
/* .line 618 */
/* .local v3, "gap":J */
/* const-wide/16 v5, 0x20 */
/* cmp-long v5, v1, v5 */
/* const-wide/16 v6, 0x40 */
/* if-lez v5, :cond_0 */
/* cmp-long v5, v1, v6 */
/* if-gtz v5, :cond_0 */
/* .line 619 */
/* const-wide/16 v3, 0x20 */
/* .line 620 */
} // :cond_0
/* cmp-long v5, v1, v6 */
/* const-wide/16 v6, 0x100 */
/* if-lez v5, :cond_1 */
/* cmp-long v5, v1, v6 */
/* if-gtz v5, :cond_1 */
/* .line 621 */
/* const-wide/16 v3, 0x40 */
/* .line 622 */
} // :cond_1
/* cmp-long v5, v1, v6 */
/* const-wide/16 v6, 0x400 */
/* if-lez v5, :cond_2 */
/* cmp-long v5, v1, v6 */
/* if-gtz v5, :cond_2 */
/* .line 623 */
/* const-wide/16 v3, 0x80 */
/* .line 624 */
} // :cond_2
/* cmp-long v5, v1, v6 */
/* if-lez v5, :cond_3 */
/* .line 625 */
/* const-wide/16 v3, 0x100 */
/* .line 627 */
} // :cond_3
} // :goto_0
/* div-long v5, v1, v3 */
/* const-wide/16 v7, 0x1 */
/* add-long/2addr v5, v7 */
/* mul-long/2addr v5, v3 */
/* long-to-int v5, v5 */
} // .end method
private getDefaultExtMGear ( Integer p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "memSize" # I */
/* .param p2, "dataSize" # I */
/* .line 643 */
int v0 = 3; // const/4 v0, 0x3
/* new-array v0, v0, [F */
/* .line 644 */
/* .local v0, "ret":[F */
/* const/16 v1, 0x8 */
int v2 = 2; // const/4 v2, 0x2
int v3 = 1; // const/4 v3, 0x1
int v4 = 0; // const/4 v4, 0x0
/* if-lt p1, v1, :cond_0 */
/* const/16 v1, 0x80 */
/* if-lt p2, v1, :cond_0 */
/* .line 645 */
/* const/high16 v1, 0x40800000 # 4.0f */
/* aput v1, v0, v4 */
/* .line 646 */
/* const/high16 v1, 0x40c00000 # 6.0f */
/* aput v1, v0, v3 */
/* .line 647 */
/* const/high16 v1, 0x41000000 # 8.0f */
/* aput v1, v0, v2 */
/* .line 649 */
} // :cond_0
/* const/high16 v1, 0x3f800000 # 1.0f */
/* aput v1, v0, v4 */
/* .line 650 */
/* const/high16 v1, 0x40000000 # 2.0f */
/* aput v1, v0, v3 */
/* .line 651 */
/* const/high16 v1, 0x40400000 # 3.0f */
/* aput v1, v0, v2 */
/* .line 653 */
} // :goto_0
} // .end method
private static com.android.server.ExtendMImpl$ExtendMRecord getExtendMRecordInstance ( ) {
/* .locals 1 */
/* .line 300 */
v0 = com.android.server.ExtendMImpl.sExtendMRecord;
/* if-nez v0, :cond_0 */
/* .line 301 */
/* new-instance v0, Lcom/android/server/ExtendMImpl$ExtendMRecord; */
/* invoke-direct {v0}, Lcom/android/server/ExtendMImpl$ExtendMRecord;-><init>()V */
/* .line 303 */
} // :cond_0
v0 = com.android.server.ExtendMImpl.sExtendMRecord;
} // .end method
private Integer getFlushedPages ( ) {
/* .locals 3 */
/* .line 469 */
try { // :try_start_0
/* new-instance v0, Ljava/io/File; */
final String v1 = "/sys/block/zram0/bd_stat"; // const-string v1, "/sys/block/zram0/bd_stat"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
final String v1 = ""; // const-string v1, ""
/* .line 470 */
/* const/16 v2, 0x80 */
android.os.FileUtils .readTextFile ( v0,v2,v1 );
/* .line 471 */
/* .local v0, "wbStats":Ljava/lang/String; */
(( java.lang.String ) v0 ).trim ( ); // invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;
final String v2 = "\\s+"; // const-string v2, "\\s+"
(( java.lang.String ) v1 ).split ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
int v2 = 2; // const/4 v2, 0x2
/* aget-object v1, v1, v2 */
/* const/16 v2, 0xa */
v1 = java.lang.Integer .parseInt ( v1,v2 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 472 */
} // .end local v0 # "wbStats":Ljava/lang/String;
/* :catch_0 */
/* move-exception v0 */
/* .line 473 */
/* .local v0, "e":Ljava/io/IOException; */
final String v1 = "ExtM"; // const-string v1, "ExtM"
final String v2 = "Failed to read flushed page count"; // const-string v2, "Failed to read flushed page count"
android.util.Slog .e ( v1,v2 );
/* .line 475 */
} // .end local v0 # "e":Ljava/io/IOException;
int v0 = -1; // const/4 v0, -0x1
} // .end method
public static com.android.server.ExtendMImpl getInstance ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 307 */
/* if-nez p0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 308 */
} // :cond_0
/* const-class v0, Lcom/android/server/ExtendMImpl; */
/* monitor-enter v0 */
/* .line 309 */
try { // :try_start_0
v1 = com.android.server.ExtendMImpl.sSelf;
/* if-nez v1, :cond_1 */
/* .line 310 */
/* new-instance v1, Lcom/android/server/ExtendMImpl; */
/* invoke-direct {v1, p0}, Lcom/android/server/ExtendMImpl;-><init>(Landroid/content/Context;)V */
/* .line 312 */
} // :cond_1
v1 = com.android.server.ExtendMImpl.sSelf;
/* monitor-exit v0 */
/* .line 313 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private Integer getMemSizeGB ( ) {
/* .locals 8 */
/* .line 631 */
/* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->readMemInfo()V */
/* .line 632 */
v0 = this.mMemInfo;
final String v1 = "MemTotal"; // const-string v1, "MemTotal"
/* check-cast v0, Ljava/lang/Long; */
(( java.lang.Long ) v0 ).longValue ( ); // invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
/* move-result-wide v0 */
/* const-wide/32 v2, 0x100000 */
/* div-long/2addr v0, v2 */
/* .line 633 */
/* .local v0, "memTotal":J */
/* const-wide/16 v2, 0x1 */
/* .line 634 */
/* .local v2, "gap":J */
/* const-wide/16 v4, 0x4 */
/* cmp-long v4, v0, v4 */
/* const-wide/16 v5, 0x8 */
/* if-lez v4, :cond_0 */
/* cmp-long v4, v0, v5 */
/* if-gtz v4, :cond_0 */
/* .line 635 */
/* const-wide/16 v2, 0x2 */
/* .line 636 */
} // :cond_0
/* cmp-long v4, v0, v5 */
/* if-lez v4, :cond_1 */
/* .line 637 */
/* const-wide/16 v2, 0x4 */
/* .line 639 */
} // :cond_1
} // :goto_0
/* div-long v4, v0, v2 */
/* const-wide/16 v6, 0x1 */
/* add-long/2addr v4, v6 */
/* mul-long/2addr v4, v2 */
/* long-to-int v4, v4 */
} // .end method
private Integer getMfzCloud ( ) {
/* .locals 4 */
/* .line 219 */
int v0 = 1; // const/4 v0, 0x1
/* .line 221 */
/* .local v0, "mfzcloud":I */
try { // :try_start_0
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "cloud_memFreeze_control"; // const-string v2, "cloud_memFreeze_control"
int v3 = -2; // const/4 v3, -0x2
v1 = android.provider.Settings$System .getIntForUser ( v1,v2,v3 );
/* :try_end_0 */
/* .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v0, v1 */
/* .line 226 */
/* .line 223 */
/* :catch_0 */
/* move-exception v1 */
/* .line 224 */
/* .local v1, "e":Landroid/provider/Settings$SettingNotFoundException; */
final String v2 = "MFZ"; // const-string v2, "MFZ"
final String v3 = "continue mfz cloud control"; // const-string v3, "continue mfz cloud control"
android.util.Slog .i ( v2,v3 );
/* .line 225 */
int v0 = 1; // const/4 v0, 0x1
/* .line 227 */
} // .end local v1 # "e":Landroid/provider/Settings$SettingNotFoundException;
} // :goto_0
} // .end method
private void getPagesNeedFlush ( Integer p0 ) {
/* .locals 9 */
/* .param p1, "flushLevel" # I */
/* .line 424 */
final String v0 = "ExtM"; // const-string v0, "ExtM"
int v1 = 0; // const/4 v1, 0x0
/* iput v1, p0, Lcom/android/server/ExtendMImpl;->flush_number_adjust:I */
/* .line 425 */
/* const/16 v2, 0xa */
/* iput v2, p0, Lcom/android/server/ExtendMImpl;->flush_level_adjust:I */
/* .line 427 */
v2 = /* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->isFlushthrashing()Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 428 */
/* .line 429 */
/* iput v2, p0, Lcom/android/server/ExtendMImpl;->flush_per_quota:I */
/* .line 433 */
} // :cond_0
try { // :try_start_0
/* new-instance v2, Ljava/io/File; */
final String v3 = "/sys/block/zram0/idle_stat"; // const-string v3, "/sys/block/zram0/idle_stat"
/* invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
final String v3 = ""; // const-string v3, ""
android.os.FileUtils .readTextFile ( v2,v1,v3 );
/* .line 434 */
/* .local v1, "idlePages":Ljava/lang/String; */
/* if-nez v1, :cond_1 */
return;
/* .line 435 */
} // :cond_1
final String v2 = "\\s+"; // const-string v2, "\\s+"
(( java.lang.String ) v1 ).split ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 436 */
/* .local v2, "pages":[Ljava/lang/String; */
int v3 = 0; // const/4 v3, 0x0
/* .line 437 */
/* .local v3, "pagesCount":I */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
int v5 = 0; // const/4 v5, 0x0
/* .line 438 */
/* .local v5, "tmpNum":I */
int v6 = 0; // const/4 v6, 0x0
/* .line 440 */
/* .local v6, "current_idle_pages":I */
/* array-length v7, v2 */
/* add-int/lit8 v7, v7, -0x1 */
} // .end local v4 # "i":I
/* .local v7, "i":I */
} // :goto_0
/* add-int/lit8 v4, p1, -0x1 */
/* if-lt v7, v4, :cond_2 */
/* if-ltz v7, :cond_2 */
/* .line 441 */
/* aget-object v4, v2, v7 */
v4 = java.lang.Integer .parseInt ( v4 );
/* add-int/2addr v6, v4 */
/* .line 440 */
/* add-int/lit8 v7, v7, -0x1 */
/* .line 444 */
} // :cond_2
/* array-length v4, v2 */
/* add-int/lit8 v4, v4, -0x1 */
} // .end local v7 # "i":I
/* .restart local v4 # "i":I */
} // :goto_1
/* add-int/lit8 v7, p1, -0x1 */
/* if-lt v4, v7, :cond_4 */
/* if-ltz v4, :cond_4 */
/* .line 445 */
/* aget-object v7, v2, v4 */
v7 = java.lang.Integer .parseInt ( v7 );
/* move v5, v7 */
/* .line 446 */
/* if-lez v5, :cond_3 */
/* .line 447 */
/* add-int/2addr v3, v5 */
/* .line 448 */
/* iget v7, p0, Lcom/android/server/ExtendMImpl;->flush_per_quota:I */
/* if-le v3, v7, :cond_3 */
/* .line 449 */
/* .line 444 */
} // :cond_3
/* add-int/lit8 v4, v4, -0x1 */
/* .line 453 */
} // :cond_4
} // :goto_2
/* add-int/lit8 v7, p1, -0x1 */
/* if-le v4, v7, :cond_5 */
/* .line 454 */
/* add-int/lit8 v7, v4, 0x1 */
/* iput v7, p0, Lcom/android/server/ExtendMImpl;->flush_level_adjust:I */
/* .line 456 */
} // :cond_5
/* iput p1, p0, Lcom/android/server/ExtendMImpl;->flush_level_adjust:I */
/* .line 458 */
} // :goto_3
/* iget v7, p0, Lcom/android/server/ExtendMImpl;->flush_per_quota:I */
/* if-le v3, v7, :cond_6 */
} // :cond_6
/* move v7, v3 */
} // :goto_4
/* iput v7, p0, Lcom/android/server/ExtendMImpl;->flush_number_adjust:I */
/* .line 460 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "can_flush_idle_pages:"; // const-string v8, "can_flush_idle_pages:"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v8 = " flush_number_adjust: "; // const-string v8, " flush_number_adjust: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v8, p0, Lcom/android/server/ExtendMImpl;->flush_number_adjust:I */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v7 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 463 */
/* nop */
} // .end local v1 # "idlePages":Ljava/lang/String;
} // .end local v2 # "pages":[Ljava/lang/String;
} // .end local v3 # "pagesCount":I
} // .end local v4 # "i":I
} // .end local v5 # "tmpNum":I
} // .end local v6 # "current_idle_pages":I
/* .line 461 */
/* :catch_0 */
/* move-exception v1 */
/* .line 462 */
/* .local v1, "e":Ljava/io/IOException; */
final String v2 = "Failed to get pages need to flush"; // const-string v2, "Failed to get pages need to flush"
android.util.Slog .e ( v0,v2 );
/* .line 464 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :goto_5
return;
} // .end method
private Integer getQuotaLeft ( ) {
/* .locals 3 */
/* .line 730 */
try { // :try_start_0
/* new-instance v0, Ljava/io/File; */
final String v1 = "/sys/block/zram0/writeback_limit"; // const-string v1, "/sys/block/zram0/writeback_limit"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
final String v1 = ""; // const-string v1, ""
/* .line 731 */
int v2 = 0; // const/4 v2, 0x0
android.os.FileUtils .readTextFile ( v0,v2,v1 );
/* .line 732 */
/* .local v0, "remainingPages":Ljava/lang/String; */
(( java.lang.String ) v0 ).trim ( ); // invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;
v1 = java.lang.Integer .parseInt ( v1 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 733 */
} // .end local v0 # "remainingPages":Ljava/lang/String;
/* :catch_0 */
/* move-exception v0 */
/* .line 734 */
/* .local v0, "e":Ljava/io/IOException; */
final String v1 = "ExtM"; // const-string v1, "ExtM"
final String v2 = "Failed to get remaining pages"; // const-string v2, "Failed to get remaining pages"
android.util.Slog .e ( v1,v2 );
/* .line 736 */
} // .end local v0 # "e":Ljava/io/IOException;
int v0 = -1; // const/4 v0, -0x1
} // .end method
private java.lang.String getReadBackRate ( ) {
/* .locals 11 */
/* .line 480 */
try { // :try_start_0
/* new-instance v0, Ljava/io/File; */
final String v1 = "/sys/block/zram0/bd_stat"; // const-string v1, "/sys/block/zram0/bd_stat"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
final String v1 = ""; // const-string v1, ""
/* .line 481 */
/* const/16 v2, 0x80 */
android.os.FileUtils .readTextFile ( v0,v2,v1 );
/* .line 482 */
/* .local v0, "wbStats":Ljava/lang/String; */
(( java.lang.String ) v0 ).trim ( ); // invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;
final String v2 = "\\s+"; // const-string v2, "\\s+"
(( java.lang.String ) v1 ).split ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 485 */
/* .local v1, "wbStat":[Ljava/lang/String; */
/* new-instance v2, Ljava/math/BigDecimal; */
int v3 = 1; // const/4 v3, 0x1
/* aget-object v4, v1, v3 */
/* invoke-direct {v2, v4}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V */
/* .line 486 */
/* .local v2, "b1":Ljava/math/BigDecimal; */
/* new-instance v4, Ljava/math/BigDecimal; */
int v5 = 2; // const/4 v5, 0x2
/* aget-object v6, v1, v5 */
/* invoke-direct {v4, v6}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V */
/* .line 487 */
/* .local v4, "b2":Ljava/math/BigDecimal; */
v6 = (( java.math.BigDecimal ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/math/BigDecimal;->intValue()I
/* if-nez v6, :cond_0 */
final String v3 = "Failed to get read back rate"; // const-string v3, "Failed to get read back rate"
/* .line 488 */
} // :cond_0
int v6 = 4; // const/4 v6, 0x4
(( java.math.BigDecimal ) v2 ).divide ( v4, v5, v6 ); // invoke-virtual {v2, v4, v5, v6}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;II)Ljava/math/BigDecimal;
(( java.math.BigDecimal ) v5 ).doubleValue ( ); // invoke-virtual {v5}, Ljava/math/BigDecimal;->doubleValue()D
/* move-result-wide v5 */
java.lang.Double .valueOf ( v5,v6 );
/* .line 489 */
/* .local v5, "result":Ljava/lang/Double; */
final String v6 = "%.2f%%"; // const-string v6, "%.2f%%"
/* new-array v3, v3, [Ljava/lang/Object; */
(( java.lang.Double ) v5 ).doubleValue ( ); // invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D
/* move-result-wide v7 */
/* const-wide/high16 v9, 0x4059000000000000L # 100.0 */
/* mul-double/2addr v7, v9 */
java.lang.Double .valueOf ( v7,v8 );
int v8 = 0; // const/4 v8, 0x0
/* aput-object v7, v3, v8 */
java.lang.String .format ( v6,v3 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 490 */
} // .end local v0 # "wbStats":Ljava/lang/String;
} // .end local v1 # "wbStat":[Ljava/lang/String;
} // .end local v2 # "b1":Ljava/math/BigDecimal;
} // .end local v4 # "b2":Ljava/math/BigDecimal;
} // .end local v5 # "result":Ljava/lang/Double;
/* :catch_0 */
/* move-exception v0 */
/* .line 491 */
/* .local v0, "e":Ljava/io/IOException; */
final String v1 = "ExtM"; // const-string v1, "ExtM"
final String v2 = "Failed to read flushed page count"; // const-string v2, "Failed to read flushed page count"
android.util.Slog .e ( v1,v2 );
/* .line 493 */
} // .end local v0 # "e":Ljava/io/IOException;
final String v0 = " calculate read back rate error"; // const-string v0, " calculate read back rate error"
} // .end method
private Integer getRefinedUUID ( ) {
/* .locals 5 */
/* .line 382 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 383 */
/* .local v0, "cr":Landroid/content/ContentResolver; */
final String v1 = "extm_uuid"; // const-string v1, "extm_uuid"
android.provider.Settings$Global .getString ( v0,v1 );
/* .line 384 */
/* .local v2, "uuid":Ljava/lang/String; */
v3 = android.text.TextUtils .isEmpty ( v2 );
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 385 */
java.util.UUID .randomUUID ( );
(( java.util.UUID ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;
/* .line 386 */
android.provider.Settings$Global .putString ( v0,v1,v2 );
/* .line 389 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 390 */
/* .local v1, "sum":I */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
v4 = (( java.lang.String ) v2 ).length ( ); // invoke-virtual {v2}, Ljava/lang/String;->length()I
/* if-ge v3, v4, :cond_1 */
/* .line 391 */
v4 = (( java.lang.String ) v2 ).charAt ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C
/* add-int/lit8 v4, v4, -0x30 */
/* add-int/2addr v1, v4 */
/* .line 390 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 393 */
} // .end local v3 # "i":I
} // :cond_1
/* rem-int/lit8 v3, v1, 0xa */
/* add-int/lit8 v3, v3, 0xa */
/* rem-int/lit8 v3, v3, 0xa */
} // .end method
private synchronized Integer getZramUseRate ( ) {
/* .locals 9 */
/* monitor-enter p0 */
/* .line 569 */
int v0 = 0; // const/4 v0, 0x0
/* .line 570 */
/* .local v0, "zramUsedRate":I */
/* const-wide/16 v1, 0x0 */
/* .line 571 */
/* .local v1, "swapTotal":J */
/* const-wide/16 v3, 0x0 */
/* .line 573 */
/* .local v3, "swapFree":J */
try { // :try_start_0
/* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->readMemInfo()V */
/* .line 575 */
v5 = this.mMemInfo;
int v6 = 0; // const/4 v6, 0x0
if ( v5 != null) { // if-eqz v5, :cond_3
final String v7 = "SwapTotal"; // const-string v7, "SwapTotal"
if ( v5 != null) { // if-eqz v5, :cond_3
v5 = this.mMemInfo;
final String v7 = "SwapFree"; // const-string v7, "SwapFree"
/* if-nez v5, :cond_0 */
/* .line 578 */
} // :cond_0
v5 = this.mMemInfo;
final String v7 = "SwapTotal"; // const-string v7, "SwapTotal"
/* check-cast v5, Ljava/lang/Long; */
(( java.lang.Long ) v5 ).longValue ( ); // invoke-virtual {v5}, Ljava/lang/Long;->longValue()J
/* move-result-wide v7 */
/* move-wide v1, v7 */
/* .line 579 */
v5 = this.mMemInfo;
final String v7 = "SwapFree"; // const-string v7, "SwapFree"
/* check-cast v5, Ljava/lang/Long; */
(( java.lang.Long ) v5 ).longValue ( ); // invoke-virtual {v5}, Ljava/lang/Long;->longValue()J
/* move-result-wide v7 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move-wide v3, v7 */
/* .line 582 */
/* const-wide/16 v7, 0x0 */
/* cmp-long v5, v1, v7 */
/* if-nez v5, :cond_1 */
/* .line 583 */
/* monitor-exit p0 */
/* .line 585 */
} // :cond_1
/* sub-long v5, v1, v3 */
/* const-wide/16 v7, 0x64 */
/* mul-long/2addr v5, v7 */
try { // :try_start_1
/* div-long/2addr v5, v1 */
/* long-to-int v0, v5 */
/* .line 586 */
/* sget-boolean v5, Lcom/android/server/ExtendMImpl;->LOG_VERBOSE:Z */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 587 */
final String v5 = "ExtM"; // const-string v5, "ExtM"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "getZramUseRate zramUsedRate:"; // const-string v7, "getZramUseRate zramUsedRate:"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v6 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 588 */
} // .end local p0 # "this":Lcom/android/server/ExtendMImpl;
} // :cond_2
/* monitor-exit p0 */
/* .line 576 */
} // :cond_3
} // :goto_0
/* monitor-exit p0 */
/* .line 568 */
} // .end local v0 # "zramUsedRate":I
} // .end local v1 # "swapTotal":J
} // .end local v3 # "swapFree":J
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
private Integer getdmoptCloud ( ) {
/* .locals 4 */
/* .line 231 */
int v0 = 1; // const/4 v0, 0x1
/* .line 233 */
/* .local v0, "dmoptcloud":I */
try { // :try_start_0
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "cloud_dm_opt_enable"; // const-string v2, "cloud_dm_opt_enable"
int v3 = -2; // const/4 v3, -0x2
v1 = android.provider.Settings$System .getIntForUser ( v1,v2,v3 );
/* :try_end_0 */
/* .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v0, v1 */
/* .line 238 */
/* .line 235 */
/* :catch_0 */
/* move-exception v1 */
/* .line 236 */
/* .local v1, "e":Landroid/provider/Settings$SettingNotFoundException; */
final String v2 = "ExtM"; // const-string v2, "ExtM"
final String v3 = "continue dmopt cloud control"; // const-string v3, "continue dmopt cloud control"
android.util.Slog .i ( v2,v3 );
/* .line 237 */
int v0 = 1; // const/4 v0, 0x1
/* .line 239 */
} // .end local v1 # "e":Landroid/provider/Settings$SettingNotFoundException;
} // :goto_0
} // .end method
private void init ( ) {
/* .locals 5 */
/* .line 280 */
/* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->connect()V */
/* .line 281 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/ExtendMImpl;->mMarkStartTime:J */
/* .line 282 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/ExtendMImpl;->mScreenOnTime:J */
/* .line 283 */
com.android.server.ExtendMImpl .getExtendMRecordInstance ( );
/* .line 285 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 286 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.SCREEN_ON"; // const-string v1, "android.intent.action.SCREEN_ON"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 287 */
final String v1 = "android.intent.action.SCREEN_OFF"; // const-string v1, "android.intent.action.SCREEN_OFF"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 288 */
final String v1 = "miui.extm.action.mark"; // const-string v1, "miui.extm.action.mark"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 289 */
v1 = this.mContext;
v2 = this.mReceiver;
(( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 290 */
final String v1 = "ExtM"; // const-string v1, "ExtM"
final String v2 = "init: register mark monitor"; // const-string v2, "init: register mark monitor"
android.util.Slog .i ( v1,v2 );
/* .line 291 */
/* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->enableFlushQuota()V */
/* .line 293 */
v1 = this.mContext;
final String v2 = "jobscheduler"; // const-string v2, "jobscheduler"
(( android.content.Context ) v1 ).getSystemService ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/app/job/JobScheduler; */
/* .line 294 */
/* .local v1, "js":Landroid/app/job/JobScheduler; */
/* new-instance v2, Landroid/app/job/JobInfo$Builder; */
/* const v3, 0x82f4 */
v4 = com.android.server.ExtendMImpl.sExtendM;
/* invoke-direct {v2, v3, v4}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V */
(( android.app.job.JobInfo$Builder ) v2 ).build ( ); // invoke-virtual {v2}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;
(( android.app.job.JobScheduler ) v1 ).schedule ( v2 ); // invoke-virtual {v1, v2}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I
/* .line 295 */
int v2 = 1; // const/4 v2, 0x1
/* iput v2, p0, Lcom/android/server/ExtendMImpl;->mCurState:I */
/* .line 296 */
/* iput-boolean v2, p0, Lcom/android/server/ExtendMImpl;->isInitSuccess:Z */
/* .line 297 */
return;
} // .end method
private Boolean isFlushablePagesExist ( Integer p0, Integer p1 ) {
/* .locals 7 */
/* .param p1, "flush_level" # I */
/* .param p2, "flush_count_least" # I */
/* .line 857 */
final String v0 = "ExtM"; // const-string v0, "ExtM"
int v2 = 0; // const/4 v2, 0x0
/* if-lt p1, v1, :cond_4 */
/* if-le p1, v1, :cond_0 */
/* .line 860 */
} // :cond_0
try { // :try_start_0
/* new-instance v1, Ljava/io/File; */
final String v3 = "/sys/block/zram0/idle_stat"; // const-string v3, "/sys/block/zram0/idle_stat"
/* invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
final String v3 = ""; // const-string v3, ""
android.os.FileUtils .readTextFile ( v1,v2,v3 );
/* .line 861 */
/* .local v1, "idlePages":Ljava/lang/String; */
/* if-nez v1, :cond_1 */
/* .line 862 */
/* .line 864 */
} // :cond_1
final String v3 = "\\s+"; // const-string v3, "\\s+"
(( java.lang.String ) v1 ).split ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 865 */
/* .local v3, "pages":[Ljava/lang/String; */
int v4 = 0; // const/4 v4, 0x0
/* .line 866 */
/* .local v4, "pagesCount":I */
/* add-int/lit8 v5, p1, -0x1 */
/* .local v5, "i":I */
} // :goto_0
/* array-length v6, v3 */
/* if-ge v5, v6, :cond_2 */
/* .line 867 */
/* aget-object v6, v3, v5 */
v6 = java.lang.Integer .parseInt ( v6 );
/* add-int/2addr v4, v6 */
/* .line 866 */
/* add-int/lit8 v5, v5, 0x1 */
/* .line 869 */
} // .end local v5 # "i":I
} // :cond_2
/* if-ge v4, p2, :cond_3 */
/* .line 870 */
/* const-string/jumbo v5, "the number of flushable pages is relatively few" */
android.util.Slog .i ( v0,v5 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 871 */
/* .line 875 */
} // .end local v1 # "idlePages":Ljava/lang/String;
} // .end local v3 # "pages":[Ljava/lang/String;
} // .end local v4 # "pagesCount":I
} // :cond_3
/* .line 873 */
/* :catch_0 */
/* move-exception v1 */
/* .line 874 */
/* .local v1, "e":Ljava/io/IOException; */
final String v2 = "Failed to get pages need to flush"; // const-string v2, "Failed to get pages need to flush"
android.util.Slog .e ( v0,v2 );
/* .line 876 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :goto_1
int v0 = 1; // const/4 v0, 0x1
/* .line 858 */
} // :cond_4
} // :goto_2
} // .end method
private Boolean isFlushthrashing ( ) {
/* .locals 9 */
/* .line 531 */
final String v0 = "ExtM"; // const-string v0, "ExtM"
int v1 = 0; // const/4 v1, 0x0
/* .line 532 */
/* .local v1, "cur_bd_write":I */
int v2 = 0; // const/4 v2, 0x0
/* .line 533 */
/* .local v2, "cur_bd_read":I */
int v3 = 0; // const/4 v3, 0x0
/* .line 534 */
/* .local v3, "bdRWRate":I */
int v4 = 0; // const/4 v4, 0x0
/* .line 536 */
/* .local v4, "bThrashing":Z */
try { // :try_start_0
/* new-instance v5, Ljava/io/File; */
final String v6 = "/sys/block/zram0/bd_stat"; // const-string v6, "/sys/block/zram0/bd_stat"
/* invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
final String v6 = ""; // const-string v6, ""
/* .line 537 */
/* const/16 v7, 0x80 */
android.os.FileUtils .readTextFile ( v5,v7,v6 );
/* .line 538 */
/* .local v5, "wbStats":Ljava/lang/String; */
(( java.lang.String ) v5 ).trim ( ); // invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;
final String v7 = "\\s+"; // const-string v7, "\\s+"
(( java.lang.String ) v6 ).split ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 540 */
/* .local v6, "wbStat":[Ljava/lang/String; */
int v7 = 1; // const/4 v7, 0x1
/* aget-object v7, v6, v7 */
v7 = java.lang.Integer .parseInt ( v7 );
/* move v2, v7 */
/* .line 541 */
int v7 = 2; // const/4 v7, 0x2
/* aget-object v7, v6, v7 */
v7 = java.lang.Integer .parseInt ( v7 );
/* move v1, v7 */
/* .line 542 */
if ( v1 != null) { // if-eqz v1, :cond_2
/* if-nez v2, :cond_0 */
/* .line 546 */
} // :cond_0
/* iget v7, p0, Lcom/android/server/ExtendMImpl;->prev_bd_write:I */
/* sub-int v8, v1, v7 */
/* if-lez v8, :cond_1 */
/* .line 547 */
/* iget v8, p0, Lcom/android/server/ExtendMImpl;->prev_bd_read:I */
/* sub-int v8, v2, v8 */
/* mul-int/lit8 v8, v8, 0x64 */
/* sub-int v7, v1, v7 */
/* div-int/2addr v8, v7 */
/* move v3, v8 */
/* .line 548 */
/* if-le v3, v7, :cond_1 */
/* .line 549 */
final String v7 = "ExtM flush exist thrashing"; // const-string v7, "ExtM flush exist thrashing"
android.util.Slog .e ( v0,v7 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 550 */
int v0 = 1; // const/4 v0, 0x1
/* move v4, v0 */
/* .line 561 */
} // .end local v5 # "wbStats":Ljava/lang/String;
} // .end local v6 # "wbStat":[Ljava/lang/String;
} // :cond_1
/* .line 543 */
/* .restart local v5 # "wbStats":Ljava/lang/String; */
/* .restart local v6 # "wbStat":[Ljava/lang/String; */
} // :cond_2
} // :goto_0
/* .line 559 */
} // .end local v5 # "wbStats":Ljava/lang/String;
} // .end local v6 # "wbStat":[Ljava/lang/String;
/* :catch_0 */
/* move-exception v5 */
/* .line 560 */
/* .local v5, "e":Ljava/io/IOException; */
final String v6 = "Failed to read bd_stat"; // const-string v6, "Failed to read bd_stat"
android.util.Slog .e ( v0,v6 );
/* .line 562 */
} // .end local v5 # "e":Ljava/io/IOException;
} // :goto_1
/* iput v2, p0, Lcom/android/server/ExtendMImpl;->prev_bd_read:I */
/* .line 563 */
/* iput v1, p0, Lcom/android/server/ExtendMImpl;->prev_bd_write:I */
/* .line 565 */
} // .end method
private Boolean isKernelSupported ( ) {
/* .locals 3 */
/* .line 256 */
try { // :try_start_0
/* new-instance v0, Ljava/io/File; */
final String v1 = "/sys/block/zram0/backing_dev"; // const-string v1, "/sys/block/zram0/backing_dev"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
final String v1 = ""; // const-string v1, ""
/* const/16 v2, 0x80 */
android.os.FileUtils .readTextFile ( v0,v2,v1 );
/* .line 257 */
/* .local v0, "backingDev":Ljava/lang/String; */
final String v1 = "none"; // const-string v1, "none"
(( java.lang.String ) v0 ).trim ( ); // invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* if-nez v1, :cond_0 */
/* .line 258 */
int v1 = 1; // const/4 v1, 0x1
/* .line 262 */
} // .end local v0 # "backingDev":Ljava/lang/String;
} // :cond_0
/* .line 260 */
/* :catch_0 */
/* move-exception v0 */
/* .line 261 */
/* .local v0, "e":Ljava/io/IOException; */
final String v1 = "ExtM"; // const-string v1, "ExtM"
final String v2 = "Failed to read kernel info"; // const-string v2, "Failed to read kernel info"
android.util.Slog .w ( v1,v2 );
/* .line 263 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean isMfzSupported ( ) {
/* .locals 4 */
/* .line 267 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/sys/block/zram0/mfz_enable"; // const-string v1, "/sys/block/zram0/mfz_enable"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 268 */
/* .local v0, "MFZ_sys":Ljava/io/File; */
int v1 = 1; // const/4 v1, 0x1
/* .line 269 */
/* .local v1, "MfzSup":Z */
v2 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
final String v3 = "MFZ"; // const-string v3, "MFZ"
/* if-nez v2, :cond_0 */
/* .line 270 */
int v1 = 0; // const/4 v1, 0x0
/* .line 271 */
final String v2 = "Do not supported MFZ"; // const-string v2, "Do not supported MFZ"
android.util.Slog .d ( v3,v2 );
/* .line 273 */
} // :cond_0
final String v2 = "Supported MFZ"; // const-string v2, "Supported MFZ"
android.util.Slog .d ( v3,v2 );
/* .line 275 */
} // :goto_0
} // .end method
private void markPagesAsNew ( ) {
/* .locals 0 */
/* .line 741 */
return;
} // .end method
private Long pagesToMb ( Long p0 ) {
/* .locals 2 */
/* .param p1, "pageCnt" # J */
/* .line 420 */
/* const-wide/16 v0, 0x100 */
/* div-long v0, p1, v0 */
/* return-wide v0 */
} // .end method
private void parseExtMemCloudControl ( ) {
/* .locals 6 */
/* .line 397 */
v0 = /* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->getCloudPercent()I */
/* .line 399 */
/* .local v0, "cloudPercent":I */
int v1 = 0; // const/4 v1, 0x0
final String v2 = "ExtM"; // const-string v2, "ExtM"
/* if-ltz v0, :cond_3 */
/* const/16 v3, 0xa */
/* if-lt v0, v3, :cond_0 */
/* .line 404 */
} // :cond_0
v3 = /* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->getRefinedUUID()I */
/* .line 405 */
/* .local v3, "thres":I */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "ExtMemCloudControl value is "; // const-string v5, "ExtMemCloudControl value is "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = ", thres is "; // const-string v5, ", thres is "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v4 );
/* .line 411 */
/* const/16 v2, 0x9 */
/* if-eq v0, v2, :cond_2 */
/* if-le v0, v3, :cond_1 */
/* .line 414 */
} // :cond_1
/* iput-boolean v1, p0, Lcom/android/server/ExtendMImpl;->mLocalCloudEnable:Z */
/* .line 412 */
} // :cond_2
} // :goto_0
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/ExtendMImpl;->mLocalCloudEnable:Z */
/* .line 416 */
} // :goto_1
return;
/* .line 400 */
} // .end local v3 # "thres":I
} // :cond_3
} // :goto_2
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Invalid clound control value: "; // const-string v4, "Invalid clound control value: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ", must be in the range [0,9]"; // const-string v4, ", must be in the range [0,9]"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v3 );
/* .line 401 */
/* iput-boolean v1, p0, Lcom/android/server/ExtendMImpl;->mLocalCloudEnable:Z */
/* .line 402 */
return;
} // .end method
private synchronized void readMemInfo ( ) {
/* .locals 11 */
/* monitor-enter p0 */
/* .line 497 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
this.mMemInfo = v0;
/* .line 498 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 499 */
/* .local v0, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;" */
android.os.StrictMode .allowThreadDiskReads ( );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_6 */
/* .line 501 */
/* .local v1, "oldPolicy":Landroid/os/StrictMode$ThreadPolicy; */
try { // :try_start_1
/* new-instance v2, Ljava/io/FileReader; */
final String v3 = "/proc/meminfo"; // const-string v3, "/proc/meminfo"
/* invoke-direct {v2, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_2 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_4 */
/* .line 503 */
/* .local v2, "fileReader":Ljava/io/FileReader; */
try { // :try_start_2
/* new-instance v3, Ljava/io/BufferedReader; */
/* invoke-direct {v3, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_3 */
/* .line 505 */
/* .local v3, "reader":Ljava/io/BufferedReader; */
int v4 = 0; // const/4 v4, 0x0
/* .line 506 */
/* .local v4, "line":Ljava/lang/String; */
} // :goto_0
try { // :try_start_3
(( java.io.BufferedReader ) v3 ).readLine ( ); // invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_2 */
/* move-object v4, v5 */
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 508 */
try { // :try_start_4
final String v5 = ":"; // const-string v5, ":"
(( java.lang.String ) v4 ).split ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 509 */
/* .local v5, "items":[Ljava/lang/String; */
v6 = com.android.server.ExtendMImpl.sMemTags;
int v7 = 0; // const/4 v7, 0x0
/* aget-object v8, v5, v7 */
(( java.lang.String ) v8 ).trim ( ); // invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;
v6 = (( java.util.ArrayList ) v6 ).contains ( v8 ); // invoke-virtual {v6, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 510 */
/* aget-object v6, v5, v7 */
(( java.lang.String ) v6 ).trim ( ); // invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;
int v8 = 1; // const/4 v8, 0x1
/* aget-object v9, v5, v8 */
/* aget-object v8, v5, v8 */
final String v10 = "k"; // const-string v10, "k"
v8 = (( java.lang.String ) v8 ).indexOf ( v10 ); // invoke-virtual {v8, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
(( java.lang.String ) v9 ).substring ( v7, v8 ); // invoke-virtual {v9, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;
(( java.lang.String ) v7 ).trim ( ); // invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;
java.lang.Long .valueOf ( v7 );
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 514 */
} // .end local v5 # "items":[Ljava/lang/String;
} // :cond_0
/* .line 518 */
} // .end local v4 # "line":Ljava/lang/String;
/* :catchall_0 */
/* move-exception v4 */
/* .line 512 */
/* .restart local v4 # "line":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v5 */
/* .line 518 */
/* .local v5, "e":Ljava/lang/Exception; */
try { // :try_start_5
(( java.io.BufferedReader ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* .line 521 */
try { // :try_start_6
(( java.io.FileReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileReader;->close()V
/* :try_end_6 */
/* .catch Ljava/lang/Exception; {:try_start_6 ..:try_end_6} :catch_1 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_5 */
/* .line 526 */
try { // :try_start_7
android.os.StrictMode .setThreadPolicy ( v1 );
/* :try_end_7 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_6 */
/* .line 513 */
/* monitor-exit p0 */
return;
/* .line 516 */
} // .end local v5 # "e":Ljava/lang/Exception;
} // .end local p0 # "this":Lcom/android/server/ExtendMImpl;
} // :cond_1
try { // :try_start_8
this.mMemInfo = v0;
/* :try_end_8 */
/* .catchall {:try_start_8 ..:try_end_8} :catchall_2 */
/* .line 518 */
} // .end local v4 # "line":Ljava/lang/String;
try { // :try_start_9
(( java.io.BufferedReader ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
/* :try_end_9 */
/* .catchall {:try_start_9 ..:try_end_9} :catchall_1 */
/* .line 519 */
/* nop */
/* .line 521 */
} // .end local v3 # "reader":Ljava/io/BufferedReader;
try { // :try_start_a
(( java.io.FileReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileReader;->close()V
/* :try_end_a */
/* .catch Ljava/lang/Exception; {:try_start_a ..:try_end_a} :catch_1 */
/* .catchall {:try_start_a ..:try_end_a} :catchall_5 */
/* .line 522 */
/* nop */
/* .line 526 */
} // .end local v2 # "fileReader":Ljava/io/FileReader;
try { // :try_start_b
android.os.StrictMode .setThreadPolicy ( v1 );
/* :try_end_b */
/* .catchall {:try_start_b ..:try_end_b} :catchall_6 */
/* .line 527 */
/* .line 523 */
/* :catch_1 */
/* move-exception v2 */
/* .line 521 */
/* .restart local v2 # "fileReader":Ljava/io/FileReader; */
/* :catchall_1 */
/* move-exception v3 */
/* .line 518 */
/* .restart local v3 # "reader":Ljava/io/BufferedReader; */
/* :catchall_2 */
/* move-exception v4 */
} // :goto_1
try { // :try_start_c
(( java.io.BufferedReader ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
/* .line 519 */
/* nop */
} // .end local v0 # "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
} // .end local v1 # "oldPolicy":Landroid/os/StrictMode$ThreadPolicy;
} // .end local v2 # "fileReader":Ljava/io/FileReader;
/* throw v4 */
/* :try_end_c */
/* .catchall {:try_start_c ..:try_end_c} :catchall_3 */
/* .line 521 */
} // .end local v3 # "reader":Ljava/io/BufferedReader;
/* .restart local v0 # "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;" */
/* .restart local v1 # "oldPolicy":Landroid/os/StrictMode$ThreadPolicy; */
/* .restart local v2 # "fileReader":Ljava/io/FileReader; */
/* .restart local p0 # "this":Lcom/android/server/ExtendMImpl; */
/* :catchall_3 */
/* move-exception v3 */
} // :goto_2
try { // :try_start_d
(( java.io.FileReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileReader;->close()V
/* .line 522 */
/* nop */
} // .end local v0 # "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
} // .end local v1 # "oldPolicy":Landroid/os/StrictMode$ThreadPolicy;
} // .end local p0 # "this":Lcom/android/server/ExtendMImpl;
/* throw v3 */
/* :try_end_d */
/* .catch Ljava/lang/Exception; {:try_start_d ..:try_end_d} :catch_2 */
/* .catchall {:try_start_d ..:try_end_d} :catchall_4 */
/* .line 526 */
} // .end local v2 # "fileReader":Ljava/io/FileReader;
/* .restart local v0 # "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;" */
/* .restart local v1 # "oldPolicy":Landroid/os/StrictMode$ThreadPolicy; */
/* .restart local p0 # "this":Lcom/android/server/ExtendMImpl; */
/* :catchall_4 */
/* move-exception v2 */
/* .line 523 */
/* :catch_2 */
/* move-exception v2 */
/* .line 524 */
/* .local v2, "e":Ljava/lang/Exception; */
} // :goto_3
try { // :try_start_e
final String v3 = "ExtM"; // const-string v3, "ExtM"
final String v4 = "Cannot readMemInfo from /proc/meminfo"; // const-string v4, "Cannot readMemInfo from /proc/meminfo"
android.util.Slog .e ( v3,v4,v2 );
/* :try_end_e */
/* .catchall {:try_start_e ..:try_end_e} :catchall_5 */
/* .line 526 */
} // .end local v2 # "e":Ljava/lang/Exception;
try { // :try_start_f
android.os.StrictMode .setThreadPolicy ( v1 );
/* :try_end_f */
/* .catchall {:try_start_f ..:try_end_f} :catchall_6 */
/* .line 527 */
/* nop */
/* .line 528 */
} // :goto_4
/* monitor-exit p0 */
return;
/* .line 526 */
} // .end local p0 # "this":Lcom/android/server/ExtendMImpl;
/* :catchall_5 */
/* move-exception v2 */
} // :goto_5
try { // :try_start_10
android.os.StrictMode .setThreadPolicy ( v1 );
/* .line 527 */
/* throw v2 */
/* :try_end_10 */
/* .catchall {:try_start_10 ..:try_end_10} :catchall_6 */
/* .line 496 */
} // .end local v0 # "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
} // .end local v1 # "oldPolicy":Landroid/os/StrictMode$ThreadPolicy;
/* :catchall_6 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
private java.lang.String readerJsonFromFile ( java.io.File p0 ) {
/* .locals 8 */
/* .param p1, "file" # Ljava/io/File; */
/* .line 592 */
int v0 = 0; // const/4 v0, 0x0
/* .line 593 */
/* .local v0, "jsonStr":Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* if-nez p1, :cond_0 */
/* .line 594 */
/* .line 597 */
} // :cond_0
try { // :try_start_0
/* new-instance v2, Ljava/io/FileReader; */
/* invoke-direct {v2, p1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V */
/* .line 598 */
/* .local v2, "fileReader":Ljava/io/FileReader; */
/* new-instance v3, Ljava/io/InputStreamReader; */
/* new-instance v4, Ljava/io/FileInputStream; */
/* invoke-direct {v4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
final String v5 = "Utf-8"; // const-string v5, "Utf-8"
/* invoke-direct {v3, v4, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V */
/* .line 599 */
/* .local v3, "reader":Ljava/io/Reader; */
int v4 = 0; // const/4 v4, 0x0
/* .line 600 */
/* .local v4, "ch":I */
/* new-instance v5, Ljava/lang/StringBuffer; */
/* invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V */
/* .line 601 */
/* .local v5, "sb":Ljava/lang/StringBuffer; */
} // :goto_0
v6 = (( java.io.Reader ) v3 ).read ( ); // invoke-virtual {v3}, Ljava/io/Reader;->read()I
/* move v4, v6 */
int v7 = -1; // const/4 v7, -0x1
/* if-eq v6, v7, :cond_1 */
/* .line 602 */
/* int-to-char v6, v4 */
(( java.lang.StringBuffer ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
/* .line 604 */
} // :cond_1
(( java.io.FileReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileReader;->close()V
/* .line 605 */
(( java.io.Reader ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/Reader;->close()V
/* .line 606 */
(( java.lang.StringBuffer ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v0, v1 */
/* .line 610 */
} // .end local v2 # "fileReader":Ljava/io/FileReader;
} // .end local v3 # "reader":Ljava/io/Reader;
} // .end local v4 # "ch":I
} // .end local v5 # "sb":Ljava/lang/StringBuffer;
/* nop */
/* .line 611 */
/* .line 607 */
/* :catch_0 */
/* move-exception v2 */
/* .line 608 */
/* .local v2, "e":Ljava/io/IOException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "file read error: "; // const-string v4, "file read error: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "ExtM"; // const-string v4, "ExtM"
android.util.Slog .e ( v4,v3 );
/* .line 609 */
} // .end method
private void refreshExtmSettings ( ) {
/* .locals 2 */
/* .line 326 */
final String v0 = "Enter cloud control"; // const-string v0, "Enter cloud control"
final String v1 = "ExtM"; // const-string v1, "ExtM"
android.util.Slog .i ( v1,v0 );
/* .line 328 */
/* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->parseExtMemCloudControl()V */
/* .line 329 */
v0 = (( com.android.server.ExtendMImpl ) p0 ).isCloudAllowed ( ); // invoke-virtual {p0}, Lcom/android/server/ExtendMImpl;->isCloudAllowed()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 330 */
/* iget v0, p0, Lcom/android/server/ExtendMImpl;->mCurState:I */
/* if-nez v0, :cond_0 */
/* .line 331 */
/* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->init()V */
/* .line 333 */
} // :cond_0
final String v0 = "Already running"; // const-string v0, "Already running"
android.util.Slog .i ( v1,v0 );
/* .line 336 */
} // :cond_1
/* iget v0, p0, Lcom/android/server/ExtendMImpl;->mCurState:I */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 337 */
/* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->exit()V */
/* .line 339 */
} // :cond_2
final String v0 = "Already stopped"; // const-string v0, "Already stopped"
android.util.Slog .i ( v1,v0 );
/* .line 342 */
} // :goto_0
final String v0 = "Exit cloud control"; // const-string v0, "Exit cloud control"
android.util.Slog .i ( v1,v0 );
/* .line 343 */
return;
} // .end method
private void registerAlarmClock ( Long p0 ) {
/* .locals 5 */
/* .param p1, "starttime" # J */
/* .line 919 */
v0 = this.mContext;
final String v1 = "alarm"; // const-string v1, "alarm"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/AlarmManager; */
/* .line 920 */
/* .local v0, "alarmManager":Landroid/app/AlarmManager; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 921 */
/* new-instance v1, Landroid/content/Intent; */
/* invoke-direct {v1}, Landroid/content/Intent;-><init>()V */
/* .line 922 */
/* .local v1, "intent":Landroid/content/Intent; */
final String v2 = "miui.extm.action.mark"; // const-string v2, "miui.extm.action.mark"
(( android.content.Intent ) v1 ).setAction ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 923 */
v3 = this.mContext;
/* .line 924 */
v2 = (( java.lang.String ) v2 ).hashCode ( ); // invoke-virtual {v2}, Ljava/lang/String;->hashCode()I
/* .line 923 */
/* const/high16 v4, 0x4000000 */
android.app.PendingIntent .getBroadcast ( v3,v2,v1,v4 );
/* .line 925 */
/* .local v2, "pendingIntent":Landroid/app/PendingIntent; */
(( android.app.AlarmManager ) v0 ).cancel ( v2 ); // invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V
/* .line 926 */
int v3 = 0; // const/4 v3, 0x0
(( android.app.AlarmManager ) v0 ).setExactAndAllowWhileIdle ( v3, p1, p2, v2 ); // invoke-virtual {v0, v3, p1, p2, v2}, Landroid/app/AlarmManager;->setExactAndAllowWhileIdle(IJLandroid/app/PendingIntent;)V
/* .line 928 */
} // .end local v1 # "intent":Landroid/content/Intent;
} // .end local v2 # "pendingIntent":Landroid/app/PendingIntent;
} // :cond_0
return;
} // .end method
private Boolean reportLowMemPressureLimit ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "pressureState" # I */
/* .line 895 */
int v0 = 1; // const/4 v0, 0x1
/* if-eq p1, v0, :cond_0 */
/* .line 896 */
/* .line 898 */
} // :cond_0
java.lang.System .currentTimeMillis ( );
/* move-result-wide v1 */
/* iget-wide v3, p0, Lcom/android/server/ExtendMImpl;->lastReportMPtime:J */
/* sub-long/2addr v1, v3 */
/* const-wide/16 v3, 0x7530 */
/* cmp-long v1, v1, v3 */
int v2 = 0; // const/4 v2, 0x0
/* if-ltz v1, :cond_1 */
/* .line 899 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
/* iput-wide v3, p0, Lcom/android/server/ExtendMImpl;->lastReportMPtime:J */
/* .line 900 */
/* iput v2, p0, Lcom/android/server/ExtendMImpl;->reportMemPressureCnt:I */
/* .line 902 */
} // :cond_1
/* iget v1, p0, Lcom/android/server/ExtendMImpl;->reportMemPressureCnt:I */
int v3 = 3; // const/4 v3, 0x3
/* if-lt v1, v3, :cond_2 */
/* .line 903 */
/* .line 906 */
} // :cond_2
/* add-int/2addr v1, v0 */
/* iput v1, p0, Lcom/android/server/ExtendMImpl;->reportMemPressureCnt:I */
/* .line 907 */
} // .end method
private static void resetNextFlushQuota ( android.content.Context p0 ) {
/* .locals 7 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 781 */
com.android.server.ExtendMImpl .tomorrowMidnight ( );
/* .line 782 */
/* .local v0, "calendar":Ljava/util/Calendar; */
(( java.util.Calendar ) v0 ).getTimeInMillis ( ); // invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J
/* move-result-wide v1 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
/* sub-long/2addr v1, v3 */
/* .line 783 */
/* .local v1, "timeToMidnight":J */
final String v3 = "jobscheduler"; // const-string v3, "jobscheduler"
(( android.content.Context ) p0 ).getSystemService ( v3 ); // invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v3, Landroid/app/job/JobScheduler; */
/* .line 784 */
/* .local v3, "js":Landroid/app/job/JobScheduler; */
/* new-instance v4, Landroid/app/job/JobInfo$Builder; */
/* const v5, 0x82f4 */
v6 = com.android.server.ExtendMImpl.sExtendM;
/* invoke-direct {v4, v5, v6}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V */
/* .line 785 */
(( android.app.job.JobInfo$Builder ) v4 ).setMinimumLatency ( v1, v2 ); // invoke-virtual {v4, v1, v2}, Landroid/app/job/JobInfo$Builder;->setMinimumLatency(J)Landroid/app/job/JobInfo$Builder;
/* .line 786 */
(( android.app.job.JobInfo$Builder ) v4 ).build ( ); // invoke-virtual {v4}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;
/* .line 784 */
(( android.app.job.JobScheduler ) v3 ).schedule ( v4 ); // invoke-virtual {v3, v4}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I
/* .line 787 */
return;
} // .end method
private Boolean runFlush ( Integer p0 ) {
/* .locals 9 */
/* .param p1, "flushLevel" # I */
/* .line 1177 */
/* invoke-direct {p0, p1}, Lcom/android/server/ExtendMImpl;->getPagesNeedFlush(I)V */
/* .line 1178 */
/* iget v0, p0, Lcom/android/server/ExtendMImpl;->flush_number_adjust:I */
int v1 = 0; // const/4 v1, 0x0
final String v2 = "ExtM"; // const-string v2, "ExtM"
/* if-gtz v0, :cond_0 */
/* .line 1179 */
final String v0 = "no pages need to flush"; // const-string v0, "no pages need to flush"
android.util.Slog .w ( v2,v0 );
/* .line 1180 */
/* .line 1183 */
} // :cond_0
final String v3 = " pages "; // const-string v3, " pages "
final String v4 = " Need Flush: "; // const-string v4, " Need Flush: "
final String v5 = "MFZ"; // const-string v5, "MFZ"
/* if-ne p1, v0, :cond_1 */
/* .line 1184 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Report MEM_PRESSURE_LOW! Flush Level is: "; // const-string v6, "Report MEM_PRESSURE_LOW! Flush Level is: "
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/android/server/ExtendMImpl;->flush_number_adjust:I */
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v0 );
/* .line 1185 */
} // :cond_1
/* if-ne p1, v0, :cond_2 */
/* .line 1186 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Report MEM_PRESSURE_MEDIUM! Flush Level is: "; // const-string v6, "Report MEM_PRESSURE_MEDIUM! Flush Level is: "
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/android/server/ExtendMImpl;->flush_number_adjust:I */
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v0 );
/* .line 1188 */
} // :cond_2
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Report MEM_PRESSURE_HIGH! Flush Level is: "; // const-string v6, "Report MEM_PRESSURE_HIGH! Flush Level is: "
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/android/server/ExtendMImpl;->flush_number_adjust:I */
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v0 );
/* .line 1191 */
} // :goto_0
try { // :try_start_0
/* new-instance v0, Ljava/io/File; */
final String v3 = "/sys/block/zram0/idle_stat"; // const-string v3, "/sys/block/zram0/idle_stat"
/* invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
final String v3 = ""; // const-string v3, ""
/* .line 1192 */
/* const/16 v4, 0x80 */
android.os.FileUtils .readTextFile ( v0,v4,v3 );
/* .line 1193 */
/* .local v0, "before_idle_stats":Ljava/lang/String; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "before Flush idle_stats: "; // const-string v4, "before Flush idle_stats: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v3 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1196 */
/* nop */
} // .end local v0 # "before_idle_stats":Ljava/lang/String;
/* .line 1194 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1195 */
/* .local v0, "e":Ljava/io/IOException; */
final String v3 = "before Flush idle_stats: error"; // const-string v3, "before Flush idle_stats: error"
android.util.Slog .d ( v5,v3 );
/* .line 1198 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_1
/* iput-boolean v1, p0, Lcom/android/server/ExtendMImpl;->isFlushFinished:Z */
/* .line 1199 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Flush thread start , "; // const-string v3, "Flush thread start , "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/ExtendMImpl;->flush_number_adjust:I */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " pages need to flush"; // const-string v3, " pages need to flush"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v2,v0 );
/* .line 1201 */
int v0 = 1; // const/4 v0, 0x1
try { // :try_start_1
/* const-class v3, Landroid/os/IVold; */
final String v4 = "runExtMFlush"; // const-string v4, "runExtMFlush"
int v5 = 3; // const/4 v5, 0x3
/* new-array v6, v5, [Ljava/lang/Class; */
v7 = java.lang.Integer.TYPE;
/* aput-object v7, v6, v1 */
v7 = java.lang.Integer.TYPE;
/* aput-object v7, v6, v0 */
/* const-class v7, Landroid/os/IVoldTaskListener; */
int v8 = 2; // const/4 v8, 0x2
/* aput-object v7, v6, v8 */
(( java.lang.Class ) v3 ).getDeclaredMethod ( v4, v6 ); // invoke-virtual {v3, v4, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 1202 */
/* .local v3, "method":Ljava/lang/reflect/Method; */
v4 = this.mVold;
/* new-array v5, v5, [Ljava/lang/Object; */
/* iget v6, p0, Lcom/android/server/ExtendMImpl;->flush_number_adjust:I */
java.lang.Integer .valueOf ( v6 );
/* aput-object v6, v5, v1 */
/* iget v1, p0, Lcom/android/server/ExtendMImpl;->flush_level_adjust:I */
java.lang.Integer .valueOf ( v1 );
/* aput-object v1, v5, v0 */
/* new-instance v1, Lcom/android/server/ExtendMImpl$6; */
/* invoke-direct {v1, p0}, Lcom/android/server/ExtendMImpl$6;-><init>(Lcom/android/server/ExtendMImpl;)V */
/* aput-object v1, v5, v8 */
(( java.lang.reflect.Method ) v3 ).invoke ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 1243 */
/* nop */
} // .end local v3 # "method":Ljava/lang/reflect/Method;
/* .line 1240 */
/* :catch_1 */
/* move-exception v1 */
/* .line 1241 */
/* .local v1, "e":Ljava/lang/Exception; */
android.util.Slog .wtf ( v2,v1 );
/* .line 1242 */
final String v3 = "Failed to runExtmFlushPages"; // const-string v3, "Failed to runExtmFlushPages"
android.util.Slog .e ( v2,v3 );
/* .line 1244 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_2
} // .end method
private Boolean runMark ( ) {
/* .locals 7 */
/* .line 1248 */
final String v0 = "Mark thread start "; // const-string v0, "Mark thread start "
final String v1 = "ExtM"; // const-string v1, "ExtM"
android.util.Slog .w ( v1,v0 );
/* .line 1250 */
int v0 = 1; // const/4 v0, 0x1
try { // :try_start_0
/* const-class v2, Landroid/os/IVold; */
final String v3 = "runExtMMark"; // const-string v3, "runExtMMark"
/* new-array v4, v0, [Ljava/lang/Class; */
/* const-class v5, Landroid/os/IVoldTaskListener; */
int v6 = 0; // const/4 v6, 0x0
/* aput-object v5, v4, v6 */
(( java.lang.Class ) v2 ).getDeclaredMethod ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 1251 */
/* .local v2, "method":Ljava/lang/reflect/Method; */
v3 = this.mVold;
/* new-array v4, v0, [Ljava/lang/Object; */
/* new-instance v5, Lcom/android/server/ExtendMImpl$7; */
/* invoke-direct {v5, p0}, Lcom/android/server/ExtendMImpl$7;-><init>(Lcom/android/server/ExtendMImpl;)V */
/* aput-object v5, v4, v6 */
(( java.lang.reflect.Method ) v2 ).invoke ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1264 */
/* nop */
} // .end local v2 # "method":Ljava/lang/reflect/Method;
/* .line 1261 */
/* :catch_0 */
/* move-exception v2 */
/* .line 1262 */
/* .local v2, "e":Ljava/lang/Exception; */
android.util.Slog .wtf ( v1,v2 );
/* .line 1263 */
final String v3 = "Failed to runExtmMarkPages"; // const-string v3, "Failed to runExtmMarkPages"
android.util.Slog .e ( v1,v3 );
/* .line 1265 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_0
} // .end method
private void saveFlushData ( Integer p0, java.lang.String p1, Long p2 ) {
/* .locals 10 */
/* .param p1, "writeCount" # I */
/* .param p2, "mReadBackRate" # Ljava/lang/String; */
/* .param p3, "startTime" # J */
/* .line 1307 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 1308 */
/* .local v0, "now":J */
/* sub-long v8, v0, p3 */
/* .line 1309 */
/* .local v8, "costTime":J */
v2 = com.android.server.ExtendMImpl.sExtendMRecord;
/* int-to-long v3, p1 */
/* invoke-direct {p0, v3, v4}, Lcom/android/server/ExtendMImpl;->pagesToMb(J)J */
/* move-result-wide v3 */
/* long-to-int v4, v3 */
/* move v3, p1 */
/* move-object v5, p2 */
/* move-wide v6, v8 */
/* invoke-virtual/range {v2 ..v7}, Lcom/android/server/ExtendMImpl$ExtendMRecord;->setFlushRecord(IILjava/lang/String;J)V */
/* .line 1310 */
return;
} // .end method
private void setFlushQuota ( ) {
/* .locals 3 */
/* .line 753 */
try { // :try_start_0
final String v0 = "persist.miui.extm.daily_flush_count"; // const-string v0, "persist.miui.extm.daily_flush_count"
android.os.SystemProperties .get ( v0 );
/* .line 754 */
/* .local v0, "WriteBackLimit":Ljava/lang/String; */
/* new-instance v1, Ljava/io/File; */
final String v2 = "/sys/block/zram0/writeback_limit"; // const-string v2, "/sys/block/zram0/writeback_limit"
/* invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
android.os.FileUtils .stringToFile ( v1,v0 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 757 */
} // .end local v0 # "WriteBackLimit":Ljava/lang/String;
/* .line 755 */
/* :catch_0 */
/* move-exception v0 */
/* .line 756 */
/* .local v0, "e":Ljava/io/IOException; */
final String v1 = "ExtM"; // const-string v1, "ExtM"
final String v2 = "Failed to set flush quota"; // const-string v2, "Failed to set flush quota"
android.util.Slog .e ( v1,v2 );
/* .line 758 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_0
return;
} // .end method
private void showAndSaveFlushedStat ( Integer p0, Long p1 ) {
/* .locals 8 */
/* .param p1, "prevCount" # I */
/* .param p2, "startTime" # J */
/* .line 790 */
int v0 = -1; // const/4 v0, -0x1
final String v1 = "ExtM"; // const-string v1, "ExtM"
/* if-eq p1, v0, :cond_0 */
/* .line 791 */
v0 = /* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->getFlushedPages()I */
/* sub-int/2addr v0, p1 */
/* .line 792 */
/* .local v0, "writeCount":I */
/* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->getReadBackRate()Ljava/lang/String; */
/* .line 793 */
/* .local v2, "mReadBackRate":Ljava/lang/String; */
/* invoke-direct {p0, v0, v2, p2, p3}, Lcom/android/server/ExtendMImpl;->saveFlushData(ILjava/lang/String;J)V */
/* .line 794 */
/* iget-wide v3, p0, Lcom/android/server/ExtendMImpl;->mTotalFlushed:J */
/* int-to-long v5, v0 */
/* add-long/2addr v3, v5 */
/* iput-wide v3, p0, Lcom/android/server/ExtendMImpl;->mTotalFlushed:J */
/* .line 795 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = "("; // const-string v4, "("
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* int-to-long v4, v0 */
/* invoke-direct {p0, v4, v5}, Lcom/android/server/ExtendMImpl;->pagesToMb(J)J */
/* move-result-wide v4 */
(( java.lang.StringBuilder ) v3 ).append ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v4 = " m) pages flushed"; // const-string v4, " m) pages flushed"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v3 );
/* .line 797 */
} // .end local v0 # "writeCount":I
} // .end local v2 # "mReadBackRate":Ljava/lang/String;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/ExtendMImpl;->mLastOldPages:I */
/* .line 798 */
final String v5 = "flush pages"; // const-string v5, "flush pages"
/* const-wide/16 v6, 0x3e8 */
/* move-object v2, p0 */
/* move-wide v3, p2 */
/* invoke-direct/range {v2 ..v7}, Lcom/android/server/ExtendMImpl;->checkTime(JLjava/lang/String;J)V */
/* .line 799 */
final String v0 = "After flushing"; // const-string v0, "After flushing"
android.util.Slog .i ( v1,v0 );
/* .line 800 */
/* sget-boolean v0, Lcom/android/server/ExtendMImpl;->LOG_VERBOSE:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 801 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Quota left "; // const-string v2, "Quota left "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = /* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->getQuotaLeft()I */
/* int-to-long v2, v2 */
/* invoke-direct {p0, v2, v3}, Lcom/android/server/ExtendMImpl;->pagesToMb(J)J */
/* move-result-wide v2 */
(( java.lang.StringBuilder ) v0 ).append ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = "m"; // const-string v2, "m"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v0 );
/* .line 803 */
} // :cond_1
return;
} // .end method
private void showTotalStat ( ) {
/* .locals 6 */
/* .line 806 */
final String v0 = ""; // const-string v0, ""
final String v1 = "Total stat:"; // const-string v1, "Total stat:"
final String v2 = "ExtM"; // const-string v2, "ExtM"
android.util.Slog .i ( v2,v1 );
/* .line 807 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " total marked "; // const-string v3, " total marked "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v3, p0, Lcom/android/server/ExtendMImpl;->mTotalMarked:J */
(( java.lang.StringBuilder ) v1 ).append ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v3 = "("; // const-string v3, "("
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v4, p0, Lcom/android/server/ExtendMImpl;->mTotalMarked:J */
/* invoke-direct {p0, v4, v5}, Lcom/android/server/ExtendMImpl;->pagesToMb(J)J */
/* move-result-wide v4 */
(( java.lang.StringBuilder ) v1 ).append ( v4, v5 ); // invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v4 = " m), total flushed "; // const-string v4, " m), total flushed "
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v4, p0, Lcom/android/server/ExtendMImpl;->mTotalFlushed:J */
(( java.lang.StringBuilder ) v1 ).append ( v4, v5 ); // invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v3, p0, Lcom/android/server/ExtendMImpl;->mTotalFlushed:J */
/* .line 808 */
/* invoke-direct {p0, v3, v4}, Lcom/android/server/ExtendMImpl;->pagesToMb(J)J */
/* move-result-wide v3 */
(( java.lang.StringBuilder ) v1 ).append ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v3 = " m)"; // const-string v3, " m)"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 807 */
android.util.Slog .i ( v2,v1 );
/* .line 810 */
/* const/16 v1, 0x80 */
try { // :try_start_0
/* new-instance v3, Ljava/io/File; */
final String v4 = "/sys/block/zram0/bd_stat"; // const-string v4, "/sys/block/zram0/bd_stat"
/* invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 811 */
android.os.FileUtils .readTextFile ( v3,v1,v0 );
/* .line 812 */
/* .local v3, "wbStats":Ljava/lang/String; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " wb stat: "; // const-string v5, " wb stat: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v4 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 815 */
/* nop */
} // .end local v3 # "wbStats":Ljava/lang/String;
/* .line 813 */
/* :catch_0 */
/* move-exception v3 */
/* .line 814 */
/* .local v3, "e":Ljava/io/IOException; */
final String v4 = " wb stat: error"; // const-string v4, " wb stat: error"
android.util.Slog .i ( v2,v4 );
/* .line 817 */
} // .end local v3 # "e":Ljava/io/IOException;
} // :goto_0
try { // :try_start_1
/* new-instance v3, Ljava/io/File; */
final String v4 = "/sys/block/zram0/mm_stat"; // const-string v4, "/sys/block/zram0/mm_stat"
/* invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 818 */
android.os.FileUtils .readTextFile ( v3,v1,v0 );
/* .line 819 */
/* .local v0, "mm_stats":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " mm stat: "; // const-string v3, " mm stat: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v1 );
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 822 */
/* nop */
} // .end local v0 # "mm_stats":Ljava/lang/String;
/* .line 820 */
/* :catch_1 */
/* move-exception v0 */
/* .line 821 */
/* .local v0, "e":Ljava/io/IOException; */
final String v1 = " mm stat: error"; // const-string v1, " mm stat: error"
android.util.Slog .i ( v2,v1 );
/* .line 823 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_1
return;
} // .end method
private void startExtM ( ) {
/* .locals 7 */
/* .line 1117 */
v0 = /* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->isKernelSupported()Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1118 */
v0 = com.android.server.ExtendMImpl.EXTM_SETTINGS_PROP;
final String v1 = "1"; // const-string v1, "1"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 1120 */
/* new-instance v0, Lcom/android/server/ExtendMImpl$3; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/android/server/ExtendMImpl$3;-><init>(Lcom/android/server/ExtendMImpl;Landroid/os/Handler;)V */
/* .line 1127 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1128 */
final String v3 = "cloud_extm_percent"; // const-string v3, "cloud_extm_percent"
android.provider.Settings$System .getUriFor ( v3 );
/* .line 1127 */
int v4 = 0; // const/4 v4, 0x0
int v5 = -2; // const/4 v5, -0x2
(( android.content.ContentResolver ) v2 ).registerContentObserver ( v3, v4, v0, v5 ); // invoke-virtual {v2, v3, v4, v0, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 1130 */
/* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->parseExtMemCloudControl()V */
/* .line 1131 */
v2 = (( com.android.server.ExtendMImpl ) p0 ).isCloudAllowed ( ); // invoke-virtual {p0}, Lcom/android/server/ExtendMImpl;->isCloudAllowed()Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1132 */
/* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->init()V */
/* .line 1134 */
} // :cond_0
v2 = /* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->isMfzSupported()Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1136 */
/* new-instance v2, Lcom/android/server/ExtendMImpl$4; */
/* invoke-direct {v2, p0, v1}, Lcom/android/server/ExtendMImpl$4;-><init>(Lcom/android/server/ExtendMImpl;Landroid/os/Handler;)V */
/* .line 1144 */
/* .local v2, "mfzobsever":Landroid/database/ContentObserver; */
v3 = this.mContext;
(( android.content.Context ) v3 ).getContentResolver ( ); // invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1145 */
final String v6 = "cloud_memFreeze_control"; // const-string v6, "cloud_memFreeze_control"
android.provider.Settings$System .getUriFor ( v6 );
/* .line 1144 */
(( android.content.ContentResolver ) v3 ).registerContentObserver ( v6, v4, v2, v5 ); // invoke-virtual {v3, v6, v4, v2, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 1147 */
/* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->ParseMfzSettings()V */
/* .line 1148 */
v3 = (( com.android.server.ExtendMImpl ) p0 ).isMfzCloud ( ); // invoke-virtual {p0}, Lcom/android/server/ExtendMImpl;->isMfzCloud()Z
/* if-nez v3, :cond_1 */
/* .line 1149 */
/* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->SetMfzEnable()V */
/* .line 1152 */
} // .end local v2 # "mfzobsever":Landroid/database/ContentObserver;
} // :cond_1
/* new-instance v2, Lcom/android/server/ExtendMImpl$5; */
/* invoke-direct {v2, p0, v1}, Lcom/android/server/ExtendMImpl$5;-><init>(Lcom/android/server/ExtendMImpl;Landroid/os/Handler;)V */
/* move-object v1, v2 */
/* .line 1159 */
/* .local v1, "dmoptobsever":Landroid/database/ContentObserver; */
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1160 */
final String v3 = "cloud_dm_opt_enable"; // const-string v3, "cloud_dm_opt_enable"
android.provider.Settings$System .getUriFor ( v3 );
/* .line 1159 */
(( android.content.ContentResolver ) v2 ).registerContentObserver ( v3, v4, v1, v5 ); // invoke-virtual {v2, v3, v4, v1, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 1162 */
} // .end local v0 # "observer":Landroid/database/ContentObserver;
} // .end local v1 # "dmoptobsever":Landroid/database/ContentObserver;
/* .line 1164 */
} // :cond_2
/* iget v0, p0, Lcom/android/server/ExtendMImpl;->kernelSupportCheckCnt:I */
/* const/16 v1, 0x1e */
/* if-ge v0, v1, :cond_3 */
/* .line 1165 */
/* add-int/lit8 v0, v0, 0x1 */
/* iput v0, p0, Lcom/android/server/ExtendMImpl;->kernelSupportCheckCnt:I */
/* .line 1166 */
v0 = this.mHandler;
int v1 = 5; // const/4 v1, 0x5
/* const-wide/16 v2, 0x3e8 */
(( com.android.server.ExtendMImpl$MyHandler ) v0 ).sendEmptyMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/ExtendMImpl$MyHandler;->sendEmptyMessageDelayed(IJ)Z
/* .line 1168 */
} // :cond_3
final String v0 = "ExtM"; // const-string v0, "ExtM"
/* const-string/jumbo v1, "the number of times has reached the upper limit, skip" */
android.util.Slog .d ( v0,v1 );
/* .line 1171 */
} // :cond_4
} // :goto_0
return;
} // .end method
private void stopFlush ( ) {
/* .locals 7 */
/* .line 1269 */
final String v0 = "-----------------------"; // const-string v0, "-----------------------"
final String v1 = "ExtM"; // const-string v1, "ExtM"
android.util.Slog .d ( v1,v0 );
/* .line 1271 */
try { // :try_start_0
/* const-class v0, Landroid/os/IVold; */
/* const-string/jumbo v2, "stopExtMFlush" */
int v3 = 1; // const/4 v3, 0x1
/* new-array v4, v3, [Ljava/lang/Class; */
/* const-class v5, Landroid/os/IVoldTaskListener; */
int v6 = 0; // const/4 v6, 0x0
/* aput-object v5, v4, v6 */
(( java.lang.Class ) v0 ).getDeclaredMethod ( v2, v4 ); // invoke-virtual {v0, v2, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 1272 */
/* .local v0, "method":Ljava/lang/reflect/Method; */
v2 = this.mVold;
/* new-array v3, v3, [Ljava/lang/Object; */
/* new-instance v4, Lcom/android/server/ExtendMImpl$8; */
/* invoke-direct {v4, p0}, Lcom/android/server/ExtendMImpl$8;-><init>(Lcom/android/server/ExtendMImpl;)V */
/* aput-object v4, v3, v6 */
(( java.lang.reflect.Method ) v0 ).invoke ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1292 */
/* nop */
} // .end local v0 # "method":Ljava/lang/reflect/Method;
/* .line 1289 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1290 */
/* .local v0, "e":Ljava/lang/Exception; */
android.util.Slog .wtf ( v1,v0 );
/* .line 1291 */
final String v2 = "Failed to stopExtmFlushPages"; // const-string v2, "Failed to stopExtmFlushPages"
android.util.Slog .e ( v1,v2 );
/* .line 1293 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private static java.util.Calendar tomorrowMidnight ( ) {
/* .locals 3 */
/* .line 1296 */
java.util.Calendar .getInstance ( );
/* .line 1297 */
/* .local v0, "calendar":Ljava/util/Calendar; */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v1 */
(( java.util.Calendar ) v0 ).setTimeInMillis ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V
/* .line 1298 */
/* const/16 v1, 0xb */
int v2 = 2; // const/4 v2, 0x2
(( java.util.Calendar ) v0 ).set ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V
/* .line 1299 */
/* const/16 v1, 0xc */
int v2 = 0; // const/4 v2, 0x0
(( java.util.Calendar ) v0 ).set ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V
/* .line 1300 */
/* const/16 v1, 0xd */
(( java.util.Calendar ) v0 ).set ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V
/* .line 1301 */
/* const/16 v1, 0xe */
(( java.util.Calendar ) v0 ).set ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V
/* .line 1302 */
int v1 = 5; // const/4 v1, 0x5
int v2 = 1; // const/4 v2, 0x1
(( java.util.Calendar ) v0 ).add ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V
/* .line 1303 */
} // .end method
private Boolean tryToFlushPages ( ) {
/* .locals 2 */
/* .line 839 */
v0 = /* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->getQuotaLeft()I */
/* if-gtz v0, :cond_1 */
/* .line 840 */
/* sget-boolean v0, Lcom/android/server/ExtendMImpl;->LOG_VERBOSE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
final String v0 = "ExtM"; // const-string v0, "ExtM"
final String v1 = "Total flush pages over limit, exit"; // const-string v1, "Total flush pages over limit, exit"
android.util.Slog .d ( v0,v1 );
/* .line 841 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 843 */
} // :cond_1
/* invoke-direct {p0, v0}, Lcom/android/server/ExtendMImpl;->runFlush(I)Z */
/* .line 844 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
private Boolean tryToMarkPages ( ) {
/* .locals 9 */
/* .line 826 */
v0 = /* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->getQuotaLeft()I */
final String v1 = "ExtM"; // const-string v1, "ExtM"
/* if-gtz v0, :cond_1 */
/* .line 827 */
/* sget-boolean v0, Lcom/android/server/ExtendMImpl;->LOG_VERBOSE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
final String v0 = "Total flush pages over limit, exit"; // const-string v0, "Total flush pages over limit, exit"
android.util.Slog .d ( v1,v0 );
/* .line 828 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 830 */
} // :cond_1
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v7 */
/* .line 832 */
/* .local v7, "startTime":J */
final String v0 = "Start marking ..."; // const-string v0, "Start marking ..."
android.util.Slog .d ( v1,v0 );
/* .line 833 */
/* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->runMark()Z */
/* .line 834 */
final String v4 = "mark pages"; // const-string v4, "mark pages"
/* const-wide/16 v5, 0x64 */
/* move-object v1, p0 */
/* move-wide v2, v7 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/ExtendMImpl;->checkTime(JLjava/lang/String;J)V */
/* .line 835 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
private void tryToStopFlush ( ) {
/* .locals 0 */
/* .line 848 */
/* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->stopFlush()V */
/* .line 849 */
return;
} // .end method
private void unregisterAlarmClock ( ) {
/* .locals 5 */
/* .line 931 */
v0 = this.mContext;
final String v1 = "alarm"; // const-string v1, "alarm"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/AlarmManager; */
/* .line 932 */
/* .local v0, "alarmManager":Landroid/app/AlarmManager; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 933 */
/* new-instance v1, Landroid/content/Intent; */
/* invoke-direct {v1}, Landroid/content/Intent;-><init>()V */
/* .line 934 */
/* .local v1, "intent":Landroid/content/Intent; */
final String v2 = "miui.extm.action.mark"; // const-string v2, "miui.extm.action.mark"
(( android.content.Intent ) v1 ).setAction ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 935 */
v3 = this.mContext;
/* .line 936 */
v2 = (( java.lang.String ) v2 ).hashCode ( ); // invoke-virtual {v2}, Ljava/lang/String;->hashCode()I
/* .line 935 */
/* const/high16 v4, 0x4000000 */
android.app.PendingIntent .getBroadcast ( v3,v2,v1,v4 );
/* .line 937 */
/* .local v2, "pendingIntent":Landroid/app/PendingIntent; */
(( android.app.AlarmManager ) v0 ).cancel ( v2 ); // invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V
/* .line 939 */
} // .end local v1 # "intent":Landroid/content/Intent;
} // .end local v2 # "pendingIntent":Landroid/app/PendingIntent;
} // :cond_0
return;
} // .end method
private void updateState ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "newState" # I */
/* .line 852 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "State update : "; // const-string v1, "State update : "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/ExtendMImpl;->mCurState:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " -> "; // const-string v1, " -> "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "ExtM"; // const-string v1, "ExtM"
android.util.Slog .i ( v1,v0 );
/* .line 853 */
/* iput p1, p0, Lcom/android/server/ExtendMImpl;->mCurState:I */
/* .line 854 */
return;
} // .end method
/* # virtual methods */
public void dump ( com.android.internal.util.IndentingPrintWriter p0 ) {
/* .locals 4 */
/* .param p1, "pw" # Lcom/android/internal/util/IndentingPrintWriter; */
/* .line 1315 */
try { // :try_start_0
v0 = com.android.server.ExtendMImpl.sExtendMRecord;
(( com.android.server.ExtendMImpl$ExtendMRecord ) v0 ).getSingleRecord ( ); // invoke-virtual {v0}, Lcom/android/server/ExtendMImpl$ExtendMRecord;->getSingleRecord()Ljava/util/LinkedList;
/* .line 1316 */
/* .local v0, "records":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;" */
v1 = com.android.server.ExtendMImpl.sExtendMRecord;
(( com.android.server.ExtendMImpl$ExtendMRecord ) v1 ).getDailyRecords ( ); // invoke-virtual {v1}, Lcom/android/server/ExtendMImpl$ExtendMRecord;->getDailyRecords()Ljava/util/LinkedList;
/* .line 1317 */
/* .local v1, "dailyRecords":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;" */
final String v2 = "ExtM details:"; // const-string v2, "ExtM details:"
(( com.android.internal.util.IndentingPrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V
/* .line 1318 */
(( com.android.internal.util.IndentingPrintWriter ) p1 ).increaseIndent ( ); // invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()Lcom/android/internal/util/IndentingPrintWriter;
/* .line 1319 */
(( java.util.LinkedList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_0
/* check-cast v3, Ljava/lang/String; */
/* .line 1320 */
/* .local v3, "record":Ljava/lang/String; */
(( com.android.internal.util.IndentingPrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V
/* .line 1321 */
} // .end local v3 # "record":Ljava/lang/String;
/* .line 1322 */
} // :cond_0
(( com.android.internal.util.IndentingPrintWriter ) p1 ).decreaseIndent ( ); // invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->decreaseIndent()Lcom/android/internal/util/IndentingPrintWriter;
/* .line 1323 */
(( com.android.internal.util.IndentingPrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->println()V
/* .line 1324 */
final String v2 = "ExtM daily details:"; // const-string v2, "ExtM daily details:"
(( com.android.internal.util.IndentingPrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V
/* .line 1325 */
(( com.android.internal.util.IndentingPrintWriter ) p1 ).increaseIndent ( ); // invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()Lcom/android/internal/util/IndentingPrintWriter;
/* .line 1326 */
(( java.util.LinkedList ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;
v3 = } // :goto_1
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Ljava/lang/String; */
/* .line 1327 */
/* .local v3, "dailyRecord":Ljava/lang/String; */
(( com.android.internal.util.IndentingPrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V
/* .line 1328 */
} // .end local v3 # "dailyRecord":Ljava/lang/String;
/* .line 1329 */
} // :cond_1
(( com.android.internal.util.IndentingPrintWriter ) p1 ).decreaseIndent ( ); // invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->decreaseIndent()Lcom/android/internal/util/IndentingPrintWriter;
/* .line 1330 */
(( com.android.internal.util.IndentingPrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->println()V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1333 */
} // .end local v0 # "records":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;"
} // .end local v1 # "dailyRecords":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;"
/* .line 1331 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1332 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "ExtM not init, nothing to dump"; // const-string v1, "ExtM not init, nothing to dump"
(( com.android.internal.util.IndentingPrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V
/* .line 1334 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_2
return;
} // .end method
public getExtMGear ( ) {
/* .locals 20 */
/* .line 666 */
/* move-object/from16 v1, p0 */
v0 = com.android.server.ExtendMImpl.extmGear;
v0 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
final String v2 = "ExtM"; // const-string v2, "ExtM"
/* if-lez v0, :cond_0 */
/* iget-boolean v0, v1, Lcom/android/server/ExtendMImpl;->hasParseMemConfSuccessful:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 667 */
final String v0 = "extmGear exist"; // const-string v0, "extmGear exist"
android.util.Slog .d ( v2,v0 );
/* .line 668 */
v0 = com.android.server.ExtendMImpl.extmGear;
/* invoke-direct {v1, v0}, Lcom/android/server/ExtendMImpl;->changeArrayListToFloatArray(Ljava/util/ArrayList;)[F */
/* .line 671 */
} // :cond_0
v3 = /* invoke-direct/range {p0 ..p0}, Lcom/android/server/ExtendMImpl;->getDataSizeGB()I */
/* .line 672 */
/* .local v3, "flashSizeGB":I */
v4 = /* invoke-direct/range {p0 ..p0}, Lcom/android/server/ExtendMImpl;->getMemSizeGB()I */
/* .line 673 */
/* .local v4, "memSizeGB":I */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "flashSize(GB): "; // const-string v5, "flashSize(GB): "
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = ", memSize(GB):"; // const-string v5, ", memSize(GB):"
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* .line 675 */
/* new-instance v0, Ljava/io/File; */
v5 = com.android.server.ExtendMImpl.MEM_CONF_FILE_PATH;
/* invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* move-object v5, v0 */
/* .line 676 */
/* .local v5, "jsonFile":Ljava/io/File; */
/* invoke-direct {v1, v5}, Lcom/android/server/ExtendMImpl;->readerJsonFromFile(Ljava/io/File;)Ljava/lang/String; */
/* .line 677 */
/* .local v6, "jsonData":Ljava/lang/String; */
/* if-nez v6, :cond_1 */
/* .line 678 */
final String v0 = "No json data."; // const-string v0, "No json data."
android.util.Slog .e ( v2,v0 );
/* .line 679 */
/* invoke-direct {v1, v4, v3}, Lcom/android/server/ExtendMImpl;->getDefaultExtMGear(II)[F */
/* .line 682 */
} // :cond_1
try { // :try_start_0
v0 = com.android.server.ExtendMImpl.extmGear;
(( java.util.ArrayList ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
/* .line 683 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0, v6}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* move-object v7, v0 */
/* .line 685 */
/* .local v7, "parse":Lorg/json/JSONObject; */
final String v0 = "auto_zram"; // const-string v0, "auto_zram"
(( org.json.JSONObject ) v7 ).getJSONArray ( v0 ); // invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* move-object v8, v0 */
/* .line 686 */
/* .local v8, "autoZramItems":Lorg/json/JSONArray; */
int v0 = 0; // const/4 v0, 0x0
/* move v9, v0 */
/* .local v9, "i":I */
} // :goto_0
v0 = (( org.json.JSONArray ) v8 ).length ( ); // invoke-virtual {v8}, Lorg/json/JSONArray;->length()I
/* if-ge v9, v0, :cond_6 */
/* .line 687 */
(( org.json.JSONArray ) v8 ).get ( v9 ); // invoke-virtual {v8, v9}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Lorg/json/JSONObject; */
/* move-object v10, v0 */
/* .line 688 */
/* .local v10, "autoZramItem":Lorg/json/JSONObject; */
final String v0 = "auto_zram_flash"; // const-string v0, "auto_zram_flash"
(( org.json.JSONObject ) v10 ).getJSONArray ( v0 ); // invoke-virtual {v10, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* move-object v11, v0 */
/* .line 689 */
/* .local v11, "autoZramFlashItems":Lorg/json/JSONArray; */
int v0 = 0; // const/4 v0, 0x0
/* .line 691 */
/* .local v0, "getFlashSize":Z */
int v12 = 0; // const/4 v12, 0x0
/* .local v12, "count":I */
} // :goto_1
v13 = (( org.json.JSONArray ) v11 ).length ( ); // invoke-virtual {v11}, Lorg/json/JSONArray;->length()I
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_4 */
/* if-ge v12, v13, :cond_3 */
/* .line 692 */
try { // :try_start_1
v13 = (( org.json.JSONArray ) v11 ).getInt ( v12 ); // invoke-virtual {v11, v12}, Lorg/json/JSONArray;->getInt(I)I
/* :try_end_1 */
/* .catch Lorg/json/JSONException; {:try_start_1 ..:try_end_1} :catch_0 */
/* if-ne v13, v3, :cond_2 */
/* .line 693 */
int v0 = 1; // const/4 v0, 0x1
/* .line 694 */
/* move v12, v0 */
/* .line 691 */
} // :cond_2
/* add-int/lit8 v12, v12, 0x1 */
/* .line 713 */
} // .end local v0 # "getFlashSize":Z
} // .end local v7 # "parse":Lorg/json/JSONObject;
} // .end local v8 # "autoZramItems":Lorg/json/JSONArray;
} // .end local v9 # "i":I
} // .end local v10 # "autoZramItem":Lorg/json/JSONObject;
} // .end local v11 # "autoZramFlashItems":Lorg/json/JSONArray;
} // .end local v12 # "count":I
/* :catch_0 */
/* move-exception v0 */
/* move-object/from16 v18, v5 */
/* move-object/from16 v19, v6 */
/* goto/16 :goto_5 */
/* .line 691 */
/* .restart local v0 # "getFlashSize":Z */
/* .restart local v7 # "parse":Lorg/json/JSONObject; */
/* .restart local v8 # "autoZramItems":Lorg/json/JSONArray; */
/* .restart local v9 # "i":I */
/* .restart local v10 # "autoZramItem":Lorg/json/JSONObject; */
/* .restart local v11 # "autoZramFlashItems":Lorg/json/JSONArray; */
/* .restart local v12 # "count":I */
} // :cond_3
/* move v12, v0 */
/* .line 698 */
} // .end local v0 # "getFlashSize":Z
/* .local v12, "getFlashSize":Z */
} // :goto_2
if ( v12 != null) { // if-eqz v12, :cond_5
/* .line 699 */
try { // :try_start_2
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v13 = "auto_zram_ram_"; // const-string v13, "auto_zram_ram_"
(( java.lang.StringBuilder ) v0 ).append ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v13 = "G"; // const-string v13, "G"
(( java.lang.StringBuilder ) v0 ).append ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object v13, v0 */
/* .line 700 */
/* .local v13, "autoZramRamTag":Ljava/lang/String; */
(( org.json.JSONObject ) v10 ).getJSONObject ( v13 ); // invoke-virtual {v10, v13}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* move-object v14, v0 */
/* .line 701 */
/* .local v14, "autoZramRamTagItems":Lorg/json/JSONObject; */
(( org.json.JSONObject ) v14 ).keySet ( ); // invoke-virtual {v14}, Lorg/json/JSONObject;->keySet()Ljava/util/Set;
/* move-object v15, v0 */
/* .line 702 */
/* .local v15, "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
} // :goto_3
v0 = /* invoke-interface/range {v16 ..v16}, Ljava/util/Iterator;->hasNext()Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* invoke-interface/range {v16 ..v16}, Ljava/util/Iterator;->next()Ljava/lang/Object; */
/* check-cast v0, Ljava/lang/String; */
/* :try_end_2 */
/* .catch Lorg/json/JSONException; {:try_start_2 ..:try_end_2} :catch_4 */
/* move-object/from16 v17, v0 */
/* .line 705 */
/* .local v17, "key":Ljava/lang/String; */
try { // :try_start_3
v0 = /* invoke-static/range {v17 ..v17}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F */
/* :try_end_3 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_3 ..:try_end_3} :catch_3 */
/* .catch Lorg/json/JSONException; {:try_start_3 ..:try_end_3} :catch_4 */
/* .line 708 */
/* .local v0, "value":F */
/* nop */
/* .line 709 */
/* move-object/from16 v18, v5 */
} // .end local v5 # "jsonFile":Ljava/io/File;
/* .local v18, "jsonFile":Ljava/io/File; */
try { // :try_start_4
v5 = com.android.server.ExtendMImpl.extmGear;
/* :try_end_4 */
/* .catch Lorg/json/JSONException; {:try_start_4 ..:try_end_4} :catch_2 */
/* move-object/from16 v19, v6 */
} // .end local v6 # "jsonData":Ljava/lang/String;
/* .local v19, "jsonData":Ljava/lang/String; */
try { // :try_start_5
java.lang.Float .valueOf ( v0 );
(( java.util.ArrayList ) v5 ).add ( v6 ); // invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* :try_end_5 */
/* .catch Lorg/json/JSONException; {:try_start_5 ..:try_end_5} :catch_1 */
/* .line 710 */
/* move-object/from16 v5, v18 */
/* move-object/from16 v6, v19 */
} // .end local v0 # "value":F
} // .end local v17 # "key":Ljava/lang/String;
/* .line 713 */
} // .end local v7 # "parse":Lorg/json/JSONObject;
} // .end local v8 # "autoZramItems":Lorg/json/JSONArray;
} // .end local v9 # "i":I
} // .end local v10 # "autoZramItem":Lorg/json/JSONObject;
} // .end local v11 # "autoZramFlashItems":Lorg/json/JSONArray;
} // .end local v12 # "getFlashSize":Z
} // .end local v13 # "autoZramRamTag":Ljava/lang/String;
} // .end local v14 # "autoZramRamTagItems":Lorg/json/JSONObject;
} // .end local v15 # "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
/* :catch_1 */
/* move-exception v0 */
} // .end local v19 # "jsonData":Ljava/lang/String;
/* .restart local v6 # "jsonData":Ljava/lang/String; */
/* :catch_2 */
/* move-exception v0 */
/* move-object/from16 v19, v6 */
} // .end local v6 # "jsonData":Ljava/lang/String;
/* .restart local v19 # "jsonData":Ljava/lang/String; */
/* .line 706 */
} // .end local v18 # "jsonFile":Ljava/io/File;
} // .end local v19 # "jsonData":Ljava/lang/String;
/* .restart local v5 # "jsonFile":Ljava/io/File; */
/* .restart local v6 # "jsonData":Ljava/lang/String; */
/* .restart local v7 # "parse":Lorg/json/JSONObject; */
/* .restart local v8 # "autoZramItems":Lorg/json/JSONArray; */
/* .restart local v9 # "i":I */
/* .restart local v10 # "autoZramItem":Lorg/json/JSONObject; */
/* .restart local v11 # "autoZramFlashItems":Lorg/json/JSONArray; */
/* .restart local v12 # "getFlashSize":Z */
/* .restart local v13 # "autoZramRamTag":Ljava/lang/String; */
/* .restart local v14 # "autoZramRamTagItems":Lorg/json/JSONObject; */
/* .restart local v15 # "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* .restart local v17 # "key":Ljava/lang/String; */
/* :catch_3 */
/* move-exception v0 */
/* move-object/from16 v18, v5 */
/* move-object/from16 v19, v6 */
/* move-object v5, v0 */
} // .end local v5 # "jsonFile":Ljava/io/File;
} // .end local v6 # "jsonData":Ljava/lang/String;
/* .restart local v18 # "jsonFile":Ljava/io/File; */
/* .restart local v19 # "jsonData":Ljava/lang/String; */
/* move-object v0, v5 */
/* .line 707 */
/* .local v0, "expected":Ljava/lang/NumberFormatException; */
/* move-object/from16 v5, v18 */
/* move-object/from16 v6, v19 */
/* .line 702 */
} // .end local v0 # "expected":Ljava/lang/NumberFormatException;
} // .end local v17 # "key":Ljava/lang/String;
} // .end local v18 # "jsonFile":Ljava/io/File;
} // .end local v19 # "jsonData":Ljava/lang/String;
/* .restart local v5 # "jsonFile":Ljava/io/File; */
/* .restart local v6 # "jsonData":Ljava/lang/String; */
} // :cond_4
/* move-object/from16 v18, v5 */
/* move-object/from16 v19, v6 */
} // .end local v5 # "jsonFile":Ljava/io/File;
} // .end local v6 # "jsonData":Ljava/lang/String;
/* .restart local v18 # "jsonFile":Ljava/io/File; */
/* .restart local v19 # "jsonData":Ljava/lang/String; */
/* .line 698 */
} // .end local v13 # "autoZramRamTag":Ljava/lang/String;
} // .end local v14 # "autoZramRamTagItems":Lorg/json/JSONObject;
} // .end local v15 # "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
} // .end local v18 # "jsonFile":Ljava/io/File;
} // .end local v19 # "jsonData":Ljava/lang/String;
/* .restart local v5 # "jsonFile":Ljava/io/File; */
/* .restart local v6 # "jsonData":Ljava/lang/String; */
} // :cond_5
/* move-object/from16 v18, v5 */
/* move-object/from16 v19, v6 */
/* .line 686 */
} // .end local v5 # "jsonFile":Ljava/io/File;
} // .end local v6 # "jsonData":Ljava/lang/String;
} // .end local v10 # "autoZramItem":Lorg/json/JSONObject;
} // .end local v11 # "autoZramFlashItems":Lorg/json/JSONArray;
} // .end local v12 # "getFlashSize":Z
/* .restart local v18 # "jsonFile":Ljava/io/File; */
/* .restart local v19 # "jsonData":Ljava/lang/String; */
} // :goto_4
/* add-int/lit8 v9, v9, 0x1 */
/* move-object/from16 v5, v18 */
/* move-object/from16 v6, v19 */
/* goto/16 :goto_0 */
} // .end local v18 # "jsonFile":Ljava/io/File;
} // .end local v19 # "jsonData":Ljava/lang/String;
/* .restart local v5 # "jsonFile":Ljava/io/File; */
/* .restart local v6 # "jsonData":Ljava/lang/String; */
} // :cond_6
/* move-object/from16 v18, v5 */
/* move-object/from16 v19, v6 */
/* .line 717 */
} // .end local v5 # "jsonFile":Ljava/io/File;
} // .end local v6 # "jsonData":Ljava/lang/String;
} // .end local v7 # "parse":Lorg/json/JSONObject;
} // .end local v8 # "autoZramItems":Lorg/json/JSONArray;
} // .end local v9 # "i":I
/* .restart local v18 # "jsonFile":Ljava/io/File; */
/* .restart local v19 # "jsonData":Ljava/lang/String; */
/* nop */
/* .line 718 */
v0 = com.android.server.ExtendMImpl.extmGear;
v0 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* if-gtz v0, :cond_7 */
/* .line 719 */
/* invoke-direct {v1, v4, v3}, Lcom/android/server/ExtendMImpl;->getDefaultExtMGear(II)[F */
/* .line 722 */
} // :cond_7
v0 = com.android.server.ExtendMImpl.extmGear;
/* invoke-direct {v1, v0}, Lcom/android/server/ExtendMImpl;->changeArrayListToFloatArray(Ljava/util/ArrayList;)[F */
/* .line 723 */
/* .local v0, "ret":[F */
int v5 = 1; // const/4 v5, 0x1
/* iput-boolean v5, v1, Lcom/android/server/ExtendMImpl;->hasParseMemConfSuccessful:Z */
/* .line 724 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "read extm gear success and gear is: "; // const-string v6, "read extm gear success and gear is: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .toString ( v0 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v5 );
/* .line 725 */
/* .line 713 */
} // .end local v0 # "ret":[F
} // .end local v18 # "jsonFile":Ljava/io/File;
} // .end local v19 # "jsonData":Ljava/lang/String;
/* .restart local v5 # "jsonFile":Ljava/io/File; */
/* .restart local v6 # "jsonData":Ljava/lang/String; */
/* :catch_4 */
/* move-exception v0 */
/* move-object/from16 v18, v5 */
/* move-object/from16 v19, v6 */
/* .line 714 */
} // .end local v5 # "jsonFile":Ljava/io/File;
} // .end local v6 # "jsonData":Ljava/lang/String;
/* .local v0, "e":Lorg/json/JSONException; */
/* .restart local v18 # "jsonFile":Ljava/io/File; */
/* .restart local v19 # "jsonData":Ljava/lang/String; */
} // :goto_5
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Json parse error: "; // const-string v6, "Json parse error: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v5 );
/* .line 716 */
/* invoke-direct {v1, v4, v3}, Lcom/android/server/ExtendMImpl;->getDefaultExtMGear(II)[F */
} // .end method
public Boolean isCloudAllowed ( ) {
/* .locals 1 */
/* .line 243 */
/* iget-boolean v0, p0, Lcom/android/server/ExtendMImpl;->mLocalCloudEnable:Z */
} // .end method
public Boolean isMfzCloud ( ) {
/* .locals 1 */
/* .line 247 */
/* iget-boolean v0, p0, Lcom/android/server/ExtendMImpl;->mfzLocalCloudEnable:Z */
} // .end method
public Boolean isdmoptCloud ( ) {
/* .locals 1 */
/* .line 251 */
/* iget-boolean v0, p0, Lcom/android/server/ExtendMImpl;->dmoptLocalCloudEnable:Z */
} // .end method
public Boolean onStartJob ( android.app.job.JobParameters p0 ) {
/* .locals 7 */
/* .param p1, "params" # Landroid/app/job/JobParameters; */
/* .line 1066 */
v0 = (( android.app.job.JobParameters ) p1 ).getJobId ( ); // invoke-virtual {p1}, Landroid/app/job/JobParameters;->getJobId()I
/* const v1, 0x82f4 */
int v2 = 0; // const/4 v2, 0x0
/* if-ne v0, v1, :cond_0 */
/* .line 1068 */
v0 = com.android.server.ExtendMImpl.sExtendMRecord;
/* iget-wide v3, p0, Lcom/android/server/ExtendMImpl;->mTotalMarked:J */
/* iget-wide v5, p0, Lcom/android/server/ExtendMImpl;->mTotalFlushed:J */
(( com.android.server.ExtendMImpl$ExtendMRecord ) v0 ).setDailyRecord ( v3, v4, v5, v6 ); // invoke-virtual {v0, v3, v4, v5, v6}, Lcom/android/server/ExtendMImpl$ExtendMRecord;->setDailyRecord(JJ)V
/* .line 1069 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/ExtendMImpl;->mTotalMarked:J */
/* .line 1070 */
/* iput-wide v0, p0, Lcom/android/server/ExtendMImpl;->mTotalFlushed:J */
/* .line 1071 */
/* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->setFlushQuota()V */
/* .line 1072 */
com.android.server.ExtendMImpl .resetNextFlushQuota ( p0 );
/* .line 1073 */
(( com.android.server.ExtendMImpl ) p0 ).jobFinished ( p1, v2 ); // invoke-virtual {p0, p1, v2}, Lcom/android/server/ExtendMImpl;->jobFinished(Landroid/app/job/JobParameters;Z)V
/* .line 1074 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1076 */
} // :cond_0
} // .end method
public Boolean onStopJob ( android.app.job.JobParameters p0 ) {
/* .locals 1 */
/* .param p1, "params" # Landroid/app/job/JobParameters; */
/* .line 1081 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void reportMemPressure ( Integer p0 ) {
/* .locals 8 */
/* .param p1, "pressureState" # I */
/* .line 1013 */
final String v0 = "ExtM"; // const-string v0, "ExtM"
int v1 = 0; // const/4 v1, 0x0
/* .line 1014 */
/* .local v1, "flush_level":I */
int v2 = 0; // const/4 v2, 0x0
/* .line 1015 */
/* .local v2, "flush_count_least":I */
int v3 = 0; // const/4 v3, 0x0
/* .line 1018 */
/* .local v3, "msg_do_flush_level":I */
/* iget-boolean v4, p0, Lcom/android/server/ExtendMImpl;->isInitSuccess:Z */
if ( v4 != null) { // if-eqz v4, :cond_5
int v4 = 1; // const/4 v4, 0x1
/* if-ge p1, v4, :cond_0 */
/* goto/16 :goto_3 */
/* .line 1023 */
} // :cond_0
v4 = /* invoke-direct {p0, p1}, Lcom/android/server/ExtendMImpl;->reportLowMemPressureLimit(I)Z */
if ( v4 != null) { // if-eqz v4, :cond_4
v4 = /* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->flushIsAllowed()Z */
/* if-nez v4, :cond_1 */
/* .line 1027 */
} // :cond_1
/* packed-switch p1, :pswitch_data_0 */
/* .line 1044 */
return;
/* .line 1039 */
/* :pswitch_0 */
/* .line 1040 */
/* iput v4, p0, Lcom/android/server/ExtendMImpl;->flush_per_quota:I */
/* .line 1041 */
int v3 = 3; // const/4 v3, 0x3
/* .line 1042 */
/* .line 1034 */
/* :pswitch_1 */
/* .line 1035 */
/* iput v4, p0, Lcom/android/server/ExtendMImpl;->flush_per_quota:I */
/* .line 1036 */
int v3 = 2; // const/4 v3, 0x2
/* .line 1037 */
/* .line 1029 */
/* :pswitch_2 */
/* .line 1030 */
/* iput v4, p0, Lcom/android/server/ExtendMImpl;->flush_per_quota:I */
/* .line 1031 */
int v3 = 1; // const/4 v3, 0x1
/* .line 1032 */
/* nop */
/* .line 1047 */
} // :goto_0
/* iget v4, p0, Lcom/android/server/ExtendMImpl;->flush_per_quota:I */
/* div-int/lit8 v4, v4, 0x2 */
/* .line 1048 */
} // .end local v2 # "flush_count_least":I
/* .local v4, "flush_count_least":I */
v2 = /* invoke-direct {p0, v1, v4}, Lcom/android/server/ExtendMImpl;->isFlushablePagesExist(II)Z */
/* if-nez v2, :cond_2 */
/* .line 1049 */
return;
/* .line 1052 */
} // :cond_2
/* iget-boolean v2, p0, Lcom/android/server/ExtendMImpl;->isFlushFinished:Z */
if ( v2 != null) { // if-eqz v2, :cond_3
v2 = /* invoke-direct {p0}, Lcom/android/server/ExtendMImpl;->getQuotaLeft()I */
/* if-lez v2, :cond_3 */
/* .line 1054 */
try { // :try_start_0
v2 = this.mHandler;
int v5 = 4; // const/4 v5, 0x4
(( com.android.server.ExtendMImpl$MyHandler ) v2 ).removeMessages ( v5 ); // invoke-virtual {v2, v5}, Lcom/android/server/ExtendMImpl$MyHandler;->removeMessages(I)V
/* .line 1055 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "reportMemPressure: "; // const-string v6, "reportMemPressure: "
(( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = ", start flush"; // const-string v6, ", start flush"
(( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v2 );
/* .line 1056 */
v2 = this.mHandler;
(( com.android.server.ExtendMImpl$MyHandler ) v2 ).sendEmptyMessage ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/ExtendMImpl$MyHandler;->sendEmptyMessage(I)Z
/* .line 1057 */
v2 = this.mHandler;
/* sget-wide v6, Lcom/android/server/ExtendMImpl;->DELAY_DURATION:J */
(( com.android.server.ExtendMImpl$MyHandler ) v2 ).sendEmptyMessageDelayed ( v5, v6, v7 ); // invoke-virtual {v2, v5, v6, v7}, Lcom/android/server/ExtendMImpl$MyHandler;->sendEmptyMessageDelayed(IJ)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1060 */
/* .line 1058 */
/* :catch_0 */
/* move-exception v2 */
/* .line 1059 */
/* .local v2, "e":Ljava/lang/Exception; */
final String v5 = "reportMemPressure error "; // const-string v5, "reportMemPressure error "
android.util.Slog .i ( v0,v5 );
/* .line 1062 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :cond_3
} // :goto_1
return;
/* .line 1024 */
} // .end local v4 # "flush_count_least":I
/* .local v2, "flush_count_least":I */
} // :cond_4
} // :goto_2
return;
/* .line 1019 */
} // :cond_5
} // :goto_3
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void start ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 198 */
final String v0 = "ExtM"; // const-string v0, "ExtM"
final String v1 = "Extm Version 3.0"; // const-string v1, "Extm Version 3.0"
android.util.Slog .i ( v0,v1 );
/* .line 199 */
com.android.server.ExtendMImpl .getInstance ( p1 );
/* .line 200 */
v0 = this.mContext;
/* if-nez v0, :cond_0 */
this.mContext = p1;
/* .line 201 */
} // :cond_0
v0 = this.mThread;
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 202 */
/* new-instance v0, Lcom/android/server/ExtendMImpl$MyHandler; */
v1 = this.mThread;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/ExtendMImpl$MyHandler;-><init>(Lcom/android/server/ExtendMImpl;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 203 */
int v1 = 5; // const/4 v1, 0x5
/* const-wide/16 v2, 0x3e8 */
(( com.android.server.ExtendMImpl$MyHandler ) v0 ).sendEmptyMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/ExtendMImpl$MyHandler;->sendEmptyMessageDelayed(IJ)Z
/* .line 204 */
return;
} // .end method
