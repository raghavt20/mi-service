.class Lcom/android/server/MiuiBatteryIntelligence$1;
.super Landroid/content/BroadcastReceiver;
.source "MiuiBatteryIntelligence.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MiuiBatteryIntelligence;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/MiuiBatteryIntelligence;


# direct methods
.method constructor <init>(Lcom/android/server/MiuiBatteryIntelligence;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/MiuiBatteryIntelligence;

    .line 87
    iput-object p1, p0, Lcom/android/server/MiuiBatteryIntelligence$1;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 90
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 91
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-wide/16 v4, 0x0

    if-eqz v1, :cond_2

    .line 92
    const-string v1, "plugged"

    const/4 v6, -0x1

    invoke-virtual {p2, v1, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 93
    .local v1, "plugType":I
    if-eqz v1, :cond_0

    move v2, v3

    .line 94
    .local v2, "plugged":Z
    :cond_0
    iget-object v6, p0, Lcom/android/server/MiuiBatteryIntelligence$1;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    invoke-static {v6}, Lcom/android/server/MiuiBatteryIntelligence;->-$$Nest$fgetmPlugged(Lcom/android/server/MiuiBatteryIntelligence;)Z

    move-result v6

    if-eq v2, v6, :cond_1

    .line 95
    iget-object v6, p0, Lcom/android/server/MiuiBatteryIntelligence$1;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    invoke-static {v6, v2}, Lcom/android/server/MiuiBatteryIntelligence;->-$$Nest$fputmPlugged(Lcom/android/server/MiuiBatteryIntelligence;Z)V

    .line 96
    iget-object v6, p0, Lcom/android/server/MiuiBatteryIntelligence$1;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    invoke-static {v6}, Lcom/android/server/MiuiBatteryIntelligence;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryIntelligence;)Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;

    move-result-object v6

    invoke-virtual {v6, v3, v4, v5}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->sendMessageDelayed(IJ)V

    .line 98
    .end local v1    # "plugType":I
    .end local v2    # "plugged":Z
    :cond_1
    goto :goto_1

    :cond_2
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 99
    iget-object v1, p0, Lcom/android/server/MiuiBatteryIntelligence$1;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryIntelligence;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryIntelligence;)Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2, v4, v5}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->sendMessageDelayed(IJ)V

    goto :goto_1

    .line 100
    :cond_3
    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 101
    const-string/jumbo v1, "wifi_state"

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 102
    .local v1, "wifiState":I
    iget-object v2, p0, Lcom/android/server/MiuiBatteryIntelligence$1;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryIntelligence;->-$$Nest$fgetmWifiState(Lcom/android/server/MiuiBatteryIntelligence;)I

    move-result v2

    if-eq v1, v2, :cond_5

    .line 103
    iget-object v2, p0, Lcom/android/server/MiuiBatteryIntelligence$1;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    invoke-static {v2, v1}, Lcom/android/server/MiuiBatteryIntelligence;->-$$Nest$fputmWifiState(Lcom/android/server/MiuiBatteryIntelligence;I)V

    .line 104
    if-ne v1, v3, :cond_5

    iget-object v2, p0, Lcom/android/server/MiuiBatteryIntelligence$1;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryIntelligence;->-$$Nest$fgetmPlugged(Lcom/android/server/MiuiBatteryIntelligence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 105
    iget-object v2, p0, Lcom/android/server/MiuiBatteryIntelligence$1;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryIntelligence;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryIntelligence;)Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3, v4, v5}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->sendMessageDelayed(IJ)V

    goto :goto_0

    .line 108
    .end local v1    # "wifiState":I
    :cond_4
    const-string v1, "miui.intent.action.NEED_SCAN_WIFI"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 109
    iget-object v1, p0, Lcom/android/server/MiuiBatteryIntelligence$1;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryIntelligence;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryIntelligence;)Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2, v4, v5}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->sendMessageDelayed(IJ)V

    goto :goto_1

    .line 108
    :cond_5
    :goto_0
    nop

    .line 111
    :goto_1
    return-void
.end method
