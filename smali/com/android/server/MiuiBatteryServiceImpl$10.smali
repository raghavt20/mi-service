.class Lcom/android/server/MiuiBatteryServiceImpl$10;
.super Landroid/content/BroadcastReceiver;
.source "MiuiBatteryServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/MiuiBatteryServiceImpl;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/MiuiBatteryServiceImpl;


# direct methods
.method constructor <init>(Lcom/android/server/MiuiBatteryServiceImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/MiuiBatteryServiceImpl;

    .line 373
    iput-object p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$10;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 376
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 377
    .local v0, "action":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$10;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryServiceImpl;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "action = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiBatteryServiceImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    :cond_0
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-wide/16 v2, 0x0

    if-nez v1, :cond_2

    .line 379
    const-string v1, "miui.intent.action.UPDATE_BATTERY_DATA"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    .line 381
    :cond_1
    const-string v1, "miui.intent.action.ADJUST_VOLTAGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 382
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$10;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryServiceImpl;)Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    move-result-object v1

    const/16 v4, 0x14

    invoke-virtual {v1, v4, p2, v2, v3}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessageDelayed(ILjava/lang/Object;J)V

    goto :goto_1

    .line 380
    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$10;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryServiceImpl;)Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    move-result-object v1

    const/16 v4, 0xf

    invoke-virtual {v1, v4, v2, v3}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessageDelayed(IJ)V

    .line 384
    :cond_3
    :goto_1
    return-void
.end method
