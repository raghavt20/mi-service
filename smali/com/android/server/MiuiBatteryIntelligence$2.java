class com.android.server.MiuiBatteryIntelligence$2 extends android.database.ContentObserver {
	 /* .source "MiuiBatteryIntelligence.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/MiuiBatteryIntelligence;->registerCloudControlObserver(Landroid/content/ContentResolver;Ljava/lang/String;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.MiuiBatteryIntelligence this$0; //synthetic
final android.content.ContentResolver val$contentResolver; //synthetic
final java.lang.String val$moduleName; //synthetic
/* # direct methods */
 com.android.server.MiuiBatteryIntelligence$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/MiuiBatteryIntelligence; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 180 */
this.this$0 = p1;
this.val$contentResolver = p3;
this.val$moduleName = p4;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "selfChange" # Z */
/* .line 183 */
/* invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V */
/* .line 184 */
final String v0 = "MiuiBatteryIntelligence"; // const-string v0, "MiuiBatteryIntelligence"
final String v1 = "cloud data has update"; // const-string v1, "cloud data has update"
android.util.Slog .i ( v0,v1 );
/* .line 185 */
v0 = this.this$0;
v1 = this.val$contentResolver;
v2 = this.val$moduleName;
com.android.server.MiuiBatteryIntelligence .-$$Nest$mreadLocalCloudControlData ( v0,v1,v2 );
/* .line 186 */
return;
} // .end method
