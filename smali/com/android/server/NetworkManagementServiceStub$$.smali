.class public final Lcom/android/server/NetworkManagementServiceStub$$;
.super Ljava/lang/Object;
.source "NetworkManagementServiceStub$$.java"

# interfaces
.implements Lcom/miui/base/MiuiStubRegistry$ImplProviderManifest;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final collectImplProviders(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/base/MiuiStubRegistry$ImplProvider<",
            "*>;>;)V"
        }
    .end annotation

    .line 21
    .local p1, "outProviders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/base/MiuiStubRegistry$ImplProvider<*>;>;"
    new-instance v0, Lcom/android/server/net/NetworkPolicyManagerServiceImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/net/NetworkPolicyManagerServiceImpl$Provider;-><init>()V

    const-string v1, "com.android.server.net.NetworkPolicyManagerServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    new-instance v0, Lcom/android/server/net/NetworkManagermentServiceImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/net/NetworkManagermentServiceImpl$Provider;-><init>()V

    const-string v1, "com.android.server.net.NetworkManagementServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    new-instance v0, Lcom/android/server/net/NetworkTimeUpdateServiceImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/net/NetworkTimeUpdateServiceImpl$Provider;-><init>()V

    const-string v1, "com.android.server.net.NetworkTimeUpdateServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    return-void
.end method
