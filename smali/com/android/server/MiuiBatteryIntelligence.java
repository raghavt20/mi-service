public class com.android.server.MiuiBatteryIntelligence {
	 /* .source "MiuiBatteryIntelligence.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;, */
	 /* Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String ACTION_NEED_SCAN_WIFI;
private static final java.lang.String CHARGE_CLOUD_MODULE_NAME;
private static final Boolean DEBUG;
public static final Integer FUNCTION_LOW_BATTERY_FAST_CHARGE;
public static final Integer FUNCTION_NAVIGATION_CHARGE;
public static final Integer FUNCTION_OUT_DOOR_CHARGE;
private static volatile com.android.server.MiuiBatteryIntelligence INSTANCE;
private static final Long INTERVAL;
private static final java.lang.String MIUI_SECURITYCENTER_APP;
private static final java.lang.String NAVIGATION_APP_WHILTE_LIST;
private static final java.lang.String PERMISSON_MIUIBATTERY_BROADCAST;
private static final android.net.Uri URI_CLOUD_ALL_DATA_NOTIFY;
/* # instance fields */
private java.lang.String MAP_APP;
private final java.lang.String TAG;
public android.content.Context mContext;
private com.android.server.MiuiBatteryIntelligence$BatteryInelligenceHandler mHandler;
private java.util.ArrayList mMapApplist;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private miui.util.IMiCharge mMiCharge;
private Boolean mPlugged;
private android.content.BroadcastReceiver mStateChangedReceiver;
private Integer mWifiState;
public Integer mfunctions;
/* # direct methods */
static com.android.server.MiuiBatteryIntelligence$BatteryInelligenceHandler -$$Nest$fgetmHandler ( com.android.server.MiuiBatteryIntelligence p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static java.util.ArrayList -$$Nest$fgetmMapApplist ( com.android.server.MiuiBatteryIntelligence p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMapApplist;
} // .end method
static miui.util.IMiCharge -$$Nest$fgetmMiCharge ( com.android.server.MiuiBatteryIntelligence p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMiCharge;
} // .end method
static Boolean -$$Nest$fgetmPlugged ( com.android.server.MiuiBatteryIntelligence p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/MiuiBatteryIntelligence;->mPlugged:Z */
} // .end method
static Integer -$$Nest$fgetmWifiState ( com.android.server.MiuiBatteryIntelligence p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/MiuiBatteryIntelligence;->mWifiState:I */
} // .end method
static void -$$Nest$fputmPlugged ( com.android.server.MiuiBatteryIntelligence p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/MiuiBatteryIntelligence;->mPlugged:Z */
return;
} // .end method
static void -$$Nest$fputmWifiState ( com.android.server.MiuiBatteryIntelligence p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/MiuiBatteryIntelligence;->mWifiState:I */
return;
} // .end method
static void -$$Nest$mreadLocalCloudControlData ( com.android.server.MiuiBatteryIntelligence p0, android.content.ContentResolver p1, java.lang.String p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/MiuiBatteryIntelligence;->readLocalCloudControlData(Landroid/content/ContentResolver;Ljava/lang/String;)V */
return;
} // .end method
static Boolean -$$Nest$sfgetDEBUG ( ) { //bridge//synthethic
/* .locals 1 */
/* sget-boolean v0, Lcom/android/server/MiuiBatteryIntelligence;->DEBUG:Z */
} // .end method
static com.android.server.MiuiBatteryIntelligence ( ) {
/* .locals 2 */
/* .line 54 */
final String v0 = "persist.sys.debug_stats"; // const-string v0, "persist.sys.debug_stats"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.MiuiBatteryIntelligence.DEBUG = (v0!= 0);
/* .line 60 */
int v0 = 0; // const/4 v0, 0x0
/* .line 76 */
/* nop */
/* .line 77 */
final String v0 = "content://com.android.settings.cloud.CloudSettings/cloud_all_data/notify"; // const-string v0, "content://com.android.settings.cloud.CloudSettings/cloud_all_data/notify"
android.net.Uri .parse ( v0 );
/* .line 76 */
return;
} // .end method
private com.android.server.MiuiBatteryIntelligence ( ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 125 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 53 */
final String v0 = "MiuiBatteryIntelligence"; // const-string v0, "MiuiBatteryIntelligence"
this.TAG = v0;
/* .line 73 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mMapApplist = v0;
/* .line 79 */
miui.util.IMiCharge .getInstance ( );
this.mMiCharge = v0;
/* .line 81 */
final String v0 = "com.tencent.map"; // const-string v0, "com.tencent.map"
final String v1 = "com.baidu.BaiduMap"; // const-string v1, "com.baidu.BaiduMap"
final String v2 = "com.autonavi.minimap"; // const-string v2, "com.autonavi.minimap"
/* filled-new-array {v2, v0, v1}, [Ljava/lang/String; */
this.MAP_APP = v0;
/* .line 87 */
/* new-instance v0, Lcom/android/server/MiuiBatteryIntelligence$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/MiuiBatteryIntelligence$1;-><init>(Lcom/android/server/MiuiBatteryIntelligence;)V */
this.mStateChangedReceiver = v0;
/* .line 126 */
/* invoke-direct {p0, p1}, Lcom/android/server/MiuiBatteryIntelligence;->init(Landroid/content/Context;)V */
/* .line 127 */
return;
} // .end method
public static com.android.server.MiuiBatteryIntelligence getInstance ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 115 */
v0 = com.android.server.MiuiBatteryIntelligence.INSTANCE;
/* if-nez v0, :cond_1 */
/* .line 116 */
/* const-class v0, Lcom/android/server/MiuiBatteryIntelligence; */
/* monitor-enter v0 */
/* .line 117 */
try { // :try_start_0
v1 = com.android.server.MiuiBatteryIntelligence.INSTANCE;
/* if-nez v1, :cond_0 */
/* .line 118 */
/* new-instance v1, Lcom/android/server/MiuiBatteryIntelligence; */
/* invoke-direct {v1, p0}, Lcom/android/server/MiuiBatteryIntelligence;-><init>(Landroid/content/Context;)V */
/* .line 120 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 122 */
} // :cond_1
} // :goto_0
v0 = com.android.server.MiuiBatteryIntelligence.INSTANCE;
} // .end method
private void init ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 130 */
this.mContext = p1;
/* .line 131 */
final String v0 = "persist.vendor.smartchg"; // const-string v0, "persist.vendor.smartchg"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* iput v0, p0, Lcom/android/server/MiuiBatteryIntelligence;->mfunctions:I */
/* .line 132 */
/* new-instance v0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler; */
com.android.server.MiuiBgThread .get ( );
(( com.android.server.MiuiBgThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Lcom/android/server/MiuiBgThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;-><init>(Lcom/android/server/MiuiBatteryIntelligence;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 133 */
v0 = this.MAP_APP;
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryIntelligence;->initMapList([Ljava/lang/String;)V */
/* .line 134 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "NavigationWhiteList"; // const-string v1, "NavigationWhiteList"
/* invoke-direct {p0, v0, v1}, Lcom/android/server/MiuiBatteryIntelligence;->registerCloudControlObserver(Landroid/content/ContentResolver;Ljava/lang/String;)V */
/* .line 135 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence;->registerChangeStateReceiver()V */
/* .line 136 */
return;
} // .end method
private void initMapList ( java.lang.String[] p0 ) {
/* .locals 3 */
/* .param p1, "maplist" # [Ljava/lang/String; */
/* .line 151 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
/* array-length v1, p1 */
/* if-ge v0, v1, :cond_0 */
/* .line 152 */
v1 = this.mMapApplist;
/* aget-object v2, p1, v0 */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 151 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 154 */
} // .end local v0 # "i":I
} // :cond_0
return;
} // .end method
private void readLocalCloudControlData ( android.content.ContentResolver p0, java.lang.String p1 ) {
/* .locals 6 */
/* .param p1, "contentResolver" # Landroid/content/ContentResolver; */
/* .param p2, "moduleName" # Ljava/lang/String; */
/* .line 158 */
v0 = android.text.TextUtils .isEmpty ( p2 );
final String v1 = "MiuiBatteryIntelligence"; // const-string v1, "MiuiBatteryIntelligence"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 159 */
final String v0 = "moduleName can not be null"; // const-string v0, "moduleName can not be null"
android.util.Slog .e ( v1,v0 );
/* .line 160 */
return;
/* .line 163 */
} // :cond_0
try { // :try_start_0
final String v0 = "NavigationWhiteList"; // const-string v0, "NavigationWhiteList"
int v2 = 0; // const/4 v2, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( p1,p2,v0,v2 );
/* .line 164 */
/* .local v0, "data":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "on cloud read and data = "; // const-string v3, "on cloud read and data = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 165 */
v2 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v2, :cond_2 */
/* .line 166 */
/* new-instance v2, Lorg/json/JSONArray; */
/* invoke-direct {v2, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V */
/* .line 167 */
/* .local v2, "apps":Lorg/json/JSONArray; */
v3 = this.mMapApplist;
(( java.util.ArrayList ) v3 ).clear ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V
/* .line 168 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
v4 = (( org.json.JSONArray ) v2 ).length ( ); // invoke-virtual {v2}, Lorg/json/JSONArray;->length()I
/* if-ge v3, v4, :cond_1 */
/* .line 169 */
v4 = this.mMapApplist;
(( org.json.JSONArray ) v2 ).getString ( v3 ); // invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
(( java.util.ArrayList ) v4 ).add ( v5 ); // invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 168 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 171 */
} // .end local v3 # "i":I
} // :cond_1
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "cloud data for listNavigation "; // const-string v4, "cloud data for listNavigation "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mMapApplist;
(( java.util.ArrayList ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/util/ArrayList;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 175 */
} // .end local v0 # "data":Ljava/lang/String;
} // .end local v2 # "apps":Lorg/json/JSONArray;
} // :cond_2
/* .line 173 */
/* :catch_0 */
/* move-exception v0 */
/* .line 174 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v2 = "Exception when readLocalCloudControlData :"; // const-string v2, "Exception when readLocalCloudControlData :"
android.util.Slog .e ( v1,v2,v0 );
/* .line 177 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
private void registerChangeStateReceiver ( ) {
/* .locals 3 */
/* .line 139 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 140 */
/* .local v0, "i":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.BATTERY_CHANGED"; // const-string v1, "android.intent.action.BATTERY_CHANGED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 141 */
final String v1 = "android.intent.action.BOOT_COMPLETED"; // const-string v1, "android.intent.action.BOOT_COMPLETED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 142 */
v1 = (( com.android.server.MiuiBatteryIntelligence ) p0 ).isSupportOutDoorCharge ( ); // invoke-virtual {p0}, Lcom/android/server/MiuiBatteryIntelligence;->isSupportOutDoorCharge()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 143 */
final String v1 = "android.net.conn.CONNECTIVITY_CHANGE"; // const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 144 */
final String v1 = "android.net.wifi.WIFI_STATE_CHANGED"; // const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 145 */
final String v1 = "miui.intent.action.NEED_SCAN_WIFI"; // const-string v1, "miui.intent.action.NEED_SCAN_WIFI"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 147 */
} // :cond_0
v1 = this.mContext;
v2 = this.mStateChangedReceiver;
(( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 148 */
return;
} // .end method
private void registerCloudControlObserver ( android.content.ContentResolver p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "contentResolver" # Landroid/content/ContentResolver; */
/* .param p2, "moduleName" # Ljava/lang/String; */
/* .line 180 */
v0 = com.android.server.MiuiBatteryIntelligence.URI_CLOUD_ALL_DATA_NOTIFY;
/* new-instance v1, Lcom/android/server/MiuiBatteryIntelligence$2; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v1, p0, v2, p1, p2}, Lcom/android/server/MiuiBatteryIntelligence$2;-><init>(Lcom/android/server/MiuiBatteryIntelligence;Landroid/os/Handler;Landroid/content/ContentResolver;Ljava/lang/String;)V */
int v2 = 1; // const/4 v2, 0x1
(( android.content.ContentResolver ) p1 ).registerContentObserver ( v0, v2, v1 ); // invoke-virtual {p1, v0, v2, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 188 */
return;
} // .end method
/* # virtual methods */
public Boolean isSupportLowBatteryFastCharge ( ) {
/* .locals 1 */
/* .line 199 */
/* iget v0, p0, Lcom/android/server/MiuiBatteryIntelligence;->mfunctions:I */
/* and-int/lit8 v0, v0, 0x8 */
/* if-lez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isSupportNavigationCharge ( ) {
/* .locals 1 */
/* .line 191 */
/* iget v0, p0, Lcom/android/server/MiuiBatteryIntelligence;->mfunctions:I */
/* and-int/lit8 v0, v0, 0x2 */
/* if-lez v0, :cond_0 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isSupportOutDoorCharge ( ) {
/* .locals 1 */
/* .line 195 */
/* iget v0, p0, Lcom/android/server/MiuiBatteryIntelligence;->mfunctions:I */
/* and-int/lit8 v0, v0, 0x4 */
/* if-lez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
