.class Lcom/android/server/MiuiBatteryAuthentic$1;
.super Landroid/content/BroadcastReceiver;
.source "MiuiBatteryAuthentic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/MiuiBatteryAuthentic;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/MiuiBatteryAuthentic;


# direct methods
.method constructor <init>(Lcom/android/server/MiuiBatteryAuthentic;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/MiuiBatteryAuthentic;

    .line 100
    iput-object p1, p0, Lcom/android/server/MiuiBatteryAuthentic$1;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 103
    const-string v0, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 104
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.provision.action.PROVISION_COMPLETE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic$1;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    iget-boolean v0, v0, Lcom/android/server/MiuiBatteryAuthentic;->mIsVerified:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic$1;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryAuthentic;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryAuthentic;)Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;

    move-result-object v0

    .line 106
    invoke-virtual {v0, p1}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic$1;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryAuthentic;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryAuthentic;)Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;

    move-result-object v0

    .line 107
    invoke-virtual {v0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->isDeviceProvisioned()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 108
    iget-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic$1;->this$0:Lcom/android/server/MiuiBatteryAuthentic;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryAuthentic;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryAuthentic;)Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;

    move-result-object v0

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->sendMessageDelayed(IJ)V

    .line 110
    :cond_1
    return-void
.end method
