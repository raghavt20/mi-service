class com.android.server.MiuiBatteryStatsService$BatteryTempInfo {
	 /* .source "MiuiBatteryStatsService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/MiuiBatteryStatsService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "BatteryTempInfo" */
} // .end annotation
/* # instance fields */
private Integer averageTemp;
private java.lang.String condition;
private Integer dataCount;
private Integer maxTemp;
private Integer minTemp;
final com.android.server.MiuiBatteryStatsService this$0; //synthetic
private Integer totalTemp;
/* # direct methods */
public com.android.server.MiuiBatteryStatsService$BatteryTempInfo ( ) {
/* .locals 2 */
/* .param p1, "this$0" # Lcom/android/server/MiuiBatteryStatsService; */
/* .param p2, "condition" # Ljava/lang/String; */
/* .line 1778 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 1779 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->totalTemp:I */
/* .line 1780 */
/* iput v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->dataCount:I */
/* .line 1781 */
/* const/16 v1, -0x12c */
/* iput v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->maxTemp:I */
/* .line 1782 */
/* const/16 v1, 0x258 */
/* iput v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->minTemp:I */
/* .line 1783 */
/* iput v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->averageTemp:I */
/* .line 1784 */
this.condition = p2;
/* .line 1785 */
return;
} // .end method
/* # virtual methods */
public Integer getAverageTemp ( ) {
/* .locals 1 */
/* .line 1824 */
/* iget v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->averageTemp:I */
} // .end method
public java.lang.String getCondition ( ) {
/* .locals 1 */
/* .line 1831 */
v0 = this.condition;
} // .end method
public Integer getDataCount ( ) {
/* .locals 1 */
/* .line 1803 */
/* iget v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->dataCount:I */
} // .end method
public Integer getMaxTemp ( ) {
/* .locals 1 */
/* .line 1810 */
/* iget v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->maxTemp:I */
} // .end method
public Integer getMinTemp ( ) {
/* .locals 1 */
/* .line 1817 */
/* iget v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->minTemp:I */
} // .end method
public Integer getTotalTemp ( ) {
/* .locals 1 */
/* .line 1796 */
/* iget v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->totalTemp:I */
} // .end method
public void reset ( ) {
/* .locals 2 */
/* .line 1788 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->totalTemp:I */
/* .line 1789 */
/* iput v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->dataCount:I */
/* .line 1790 */
/* const/16 v1, -0x12c */
/* iput v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->maxTemp:I */
/* .line 1791 */
/* const/16 v1, 0x258 */
/* iput v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->minTemp:I */
/* .line 1792 */
/* iput v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->averageTemp:I */
/* .line 1793 */
return;
} // .end method
public void setAverageTemp ( ) {
/* .locals 2 */
/* .line 1827 */
/* iget v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->totalTemp:I */
/* iget v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->dataCount:I */
/* div-int/2addr v0, v1 */
/* iput v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->averageTemp:I */
/* .line 1828 */
return;
} // .end method
public void setDataCount ( ) {
/* .locals 1 */
/* .line 1806 */
/* iget v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->dataCount:I */
/* add-int/lit8 v0, v0, 0x1 */
/* iput v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->dataCount:I */
/* .line 1807 */
return;
} // .end method
public void setMaxTemp ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "maxTemp" # I */
/* .line 1813 */
/* iput p1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->maxTemp:I */
/* .line 1814 */
return;
} // .end method
public void setMinTemp ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "minTemp" # I */
/* .line 1820 */
/* iput p1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->minTemp:I */
/* .line 1821 */
return;
} // .end method
public void setTotalTemp ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "temp" # I */
/* .line 1799 */
/* iget v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->totalTemp:I */
/* add-int/2addr v0, p1 */
/* iput v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->totalTemp:I */
/* .line 1800 */
return;
} // .end method
