.class public Lcom/android/server/ScoutHelper;
.super Ljava/lang/Object;
.source "ScoutHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/ScoutHelper$ScoutBinderInfo;,
        Lcom/android/server/ScoutHelper$ProcStatus;,
        Lcom/android/server/ScoutHelper$Action;
    }
.end annotation


# static fields
.field public static final ACTION_CAT:Ljava/lang/String; = "cat"

.field public static final ACTION_CP:Ljava/lang/String; = "cp"

.field public static final ACTION_DMABUF_DUMP:Ljava/lang/String; = "dmabuf_dump"

.field public static final ACTION_DUMPSYS:Ljava/lang/String; = "dumpsys"

.field public static final ACTION_LOGCAT:Ljava/lang/String; = "logcat"

.field public static final ACTION_MV:Ljava/lang/String; = "mv"

.field public static final ACTION_PS:Ljava/lang/String; = "ps"

.field public static final ACTION_TOP:Ljava/lang/String; = "top"

.field public static final BINDER_ASYNC:Ljava/lang/String; = "    pending async transaction"

.field public static final BINDER_ASYNC_GKI:Ljava/lang/String; = "MIUI    pending async transaction"

.field public static final BINDER_ASYNC_MIUI:Ljava/lang/String; = "pending async transaction"

.field public static final BINDER_CONTEXT:Ljava/lang/String; = "context"

.field public static final BINDER_DUR:Ljava/lang/String; = "duration:"

.field public static final BINDER_FS_PATH_GKI:Ljava/lang/String; = "/dev/binderfs/binder_logs/proc/"

.field public static final BINDER_FS_PATH_MIUI:Ljava/lang/String; = "/dev/binderfs/binder_logs/proc_transaction/"

.field public static final BINDER_FULL_KILL_PROC:Z

.field public static final BINDER_FULL_KILL_SCORE_ADJ_MIN:I = -0x320

.field public static final BINDER_INCOMING:Ljava/lang/String; = "    incoming transaction"

.field public static final BINDER_INCOMING_GKI:Ljava/lang/String; = "MIUI    incoming transaction"

.field public static final BINDER_INCOMING_MIUI:Ljava/lang/String; = "incoming transaction"

.field public static final BINDER_OUTGOING:Ljava/lang/String; = "    outgoing transaction"

.field public static final BINDER_OUTGOING_GKI:Ljava/lang/String; = "MIUI    outgoing transaction"

.field public static final BINDER_OUTGOING_MIUI:Ljava/lang/String; = "outgoing transaction"

.field public static final BINDER_PENDING:Ljava/lang/String; = "    pending transaction"

.field public static final BINDER_PENDING_MIUI:Ljava/lang/String; = "pending transaction"

.field public static final BINDER_WAITTIME_THRESHOLD:I = 0x2

.field public static final CALL_TYPE_APP:I = 0x1

.field public static final CALL_TYPE_SYSTEM:I = 0x0

.field private static CONSOLE_RAMOOPS_0_PATH:Ljava/lang/String; = null

.field private static CONSOLE_RAMOOPS_PATH:Ljava/lang/String; = null

.field private static final DEBUG:Z = false

.field public static final DEFAULT_RUN_COMMAND_TIMEOUT:I = 0x3c

.field public static final DISABLE_AOSP_ANR_TRACE_POLICY:Z

.field public static final ENABLED_SCOUT:Z

.field public static final ENABLED_SCOUT_DEBUG:Z

.field public static final FILE_DIR_MQSAS:Ljava/lang/String; = "/data/mqsas/"

.field public static final FILE_DIR_SCOUT:Ljava/lang/String; = "scout"

.field public static final FILE_DIR_STABILITY:Ljava/lang/String; = "/data/miuilog/stability"

.field public static final IS_INTERNATIONAL_BUILD:Z

.field public static final JAVA_PROCESS:I = 0x1

.field private static final JAVA_SHELL_PROCESS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final MONKEY_BLOCK_THRESHOLD:I = 0x14

.field private static final MONKEY_PROCESS:Ljava/lang/String; = "com.android.commands.monkey"

.field private static final MQSASD:Ljava/lang/String; = "miui.mqsas.IMQSNative"

.field public static final MQS_PSTORE_DIR:Ljava/lang/String; = "/data/mqsas/temp/pstore/"

.field public static final NATIVE_PROCESS:I = 0x2

.field public static final OOM_SCORE_ADJ_MAX:I = 0x3e8

.field public static final OOM_SCORE_ADJ_MIN:I = -0x3e8

.field public static final OOM_SCORE_ADJ_SHELL:I = -0x3b6

.field public static final PANIC_ANR_D_THREAD:Z

.field public static final PANIC_D_THREAD:Z

.field private static PROC_VERSION:Ljava/lang/String; = null

.field public static final PS_MODE_FULL:I = 0x1

.field public static final PS_MODE_NORMAL:I = 0x0

.field private static final REGEX_PATTERN:Ljava/lang/String; = "\\bfrom((?:\\s+)?\\d+):((?:\\s+)?\\d+)\\s+to((?:\\s+)?\\d+):((?:\\s+)?\\d+)+.*code:((?:\\s+)?\\d+).*duration:((?:\\s+)?(\\d+(?:\\.\\d+)?))\\b"

.field private static final REGEX_PATTERN_LINUX_VERSION:Ljava/lang/String; = "\\bLinux version\\s+(\\d+\\.\\d+)."

.field private static final REGEX_PATTERN_NEW:Ljava/lang/String; = "\\bfrom((?:\\s+)?\\d+):((?:\\s+)?\\d+)\\s+to((?:\\s+)?\\d+):((?:\\s+)?\\d+)+.*code((?:\\s+)?\\d+)+.*elapsed((?:\\s+)?\\d+)ms+.*"

.field public static final SCOUT_BINDER_GKI:Z

.field public static final SHELL_PROCESS:I = 0x3

.field private static final STATUS_KEYS:[Ljava/lang/String;

.field public static final SYSRQ_ANR_D_THREAD:Z

.field public static final SYSTEM_ADJ:I = -0x384

.field private static final TAG:Ljava/lang/String; = "ScoutHelper"

.field public static final UNKNOW_PROCESS:I

.field public static linuxVersion:D

.field private static mDaemon:Lmiui/mqsas/IMQSNative;

.field private static final sSkipProcs:[Ljava/lang/String;

.field private static supportNewBinderLog:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 50
    const-string v0, "ScoutHelper"

    .line 51
    const-string v1, "persist.sys.miui_scout_enable"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/android/server/ScoutHelper;->ENABLED_SCOUT:Z

    .line 52
    nop

    .line 53
    const-string v1, "persist.sys.miui_scout_debug"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/android/server/ScoutHelper;->ENABLED_SCOUT_DEBUG:Z

    .line 54
    nop

    .line 55
    const-string v1, "persist.sys.miui_scout_binder_full_kill_process"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/android/server/ScoutHelper;->BINDER_FULL_KILL_PROC:Z

    .line 56
    nop

    .line 57
    const-string v1, "persist.sys.panicOnWatchdog_D_state"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/android/server/ScoutHelper;->PANIC_D_THREAD:Z

    .line 58
    nop

    .line 59
    const-string v1, "persist.sys.sysrqOnAnr_D_state"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    const-string v3, "persist.sys.panicOnAnr_D_state"

    const/4 v4, 0x1

    if-nez v1, :cond_1

    .line 60
    invoke-static {v3, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    move v1, v2

    goto :goto_1

    :cond_1
    :goto_0
    move v1, v4

    :goto_1
    sput-boolean v1, Lcom/android/server/ScoutHelper;->SYSRQ_ANR_D_THREAD:Z

    .line 61
    nop

    .line 62
    invoke-static {v3, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/android/server/ScoutHelper;->PANIC_ANR_D_THREAD:Z

    .line 63
    nop

    .line 64
    const-string v1, "persist.sys.scout_binder_gki"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/android/server/ScoutHelper;->SCOUT_BINDER_GKI:Z

    .line 65
    nop

    .line 66
    const-string v1, "persist.sys.disable_aosp_anr_policy"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/android/server/ScoutHelper;->DISABLE_AOSP_ANR_TRACE_POLICY:Z

    .line 68
    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    sput-boolean v1, Lcom/android/server/ScoutHelper;->IS_INTERNATIONAL_BUILD:Z

    .line 93
    const-string v1, "/sys/fs/pstore/console-ramoops"

    sput-object v1, Lcom/android/server/ScoutHelper;->CONSOLE_RAMOOPS_PATH:Ljava/lang/String;

    .line 94
    const-string v1, "/sys/fs/pstore/console-ramoops-0"

    sput-object v1, Lcom/android/server/ScoutHelper;->CONSOLE_RAMOOPS_0_PATH:Ljava/lang/String;

    .line 95
    const-string v1, "proc/version"

    sput-object v1, Lcom/android/server/ScoutHelper;->PROC_VERSION:Ljava/lang/String;

    .line 136
    const-wide/16 v5, 0x0

    sput-wide v5, Lcom/android/server/ScoutHelper;->linuxVersion:D

    .line 138
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/android/server/ScoutHelper;->supportNewBinderLog:Ljava/lang/Boolean;

    .line 143
    const/4 v1, 0x0

    .line 144
    .local v1, "cmdLine":Ljava/lang/String;
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    sget-object v5, Lcom/android/server/ScoutHelper;->PROC_VERSION:Ljava/lang/String;

    invoke-direct {v3, v5}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 145
    .local v2, "versionReader":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    move-object v1, v3

    .line 146
    const-string v3, "\\bLinux version\\s+(\\d+\\.\\d+)."

    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v3

    .line 147
    .local v3, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v3, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    .line 148
    .local v5, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v5}, Ljava/util/regex/Matcher;->find()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 149
    invoke-virtual {v5, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    sput-wide v6, Lcom/android/server/ScoutHelper;->linuxVersion:D

    .line 151
    const-wide v8, 0x4018666666666666L    # 6.1

    cmpl-double v6, v6, v8

    if-ltz v6, :cond_2

    .line 152
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    sput-object v4, Lcom/android/server/ScoutHelper;->supportNewBinderLog:Ljava/lang/Boolean;

    .line 154
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "The current Linux version is "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-wide v6, Lcom/android/server/ScoutHelper;->linuxVersion:D

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " supportNewBinderLog = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v6, Lcom/android/server/ScoutHelper;->supportNewBinderLog:Ljava/lang/Boolean;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 157
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Parsing Linux version failed. info : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 159
    .end local v3    # "pattern":Ljava/util/regex/Pattern;
    .end local v5    # "matcher":Ljava/util/regex/Matcher;
    :goto_2
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 161
    .end local v2    # "versionReader":Ljava/io/BufferedReader;
    goto :goto_4

    .line 144
    .restart local v2    # "versionReader":Ljava/io/BufferedReader;
    :catchall_0
    move-exception v3

    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_3

    :catchall_1
    move-exception v4

    :try_start_4
    invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v1    # "cmdLine":Ljava/lang/String;
    :goto_3
    throw v3
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 159
    .end local v2    # "versionReader":Ljava/io/BufferedReader;
    .restart local v1    # "cmdLine":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 160
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Read PROC_VERSION Error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    .end local v1    # "cmdLine":Ljava/lang/String;
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_4
    const-string v0, "com.android.commands.monkey"

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    .line 165
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/android/server/ScoutHelper;->JAVA_SHELL_PROCESS:Ljava/util/List;

    .line 695
    const-string v0, "com.android.networkstack.process"

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ScoutHelper;->sSkipProcs:[Ljava/lang/String;

    .line 985
    const-string v0, "TracerPid:"

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ScoutHelper;->STATUS_KEYS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static CheckDState(Ljava/lang/String;I)Ljava/lang/Boolean;
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "pid"    # I

    .line 320
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/android/server/ScoutHelper;->CheckDState(Ljava/lang/String;ILcom/android/server/ScoutHelper$ScoutBinderInfo;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static CheckDState(Ljava/lang/String;ILcom/android/server/ScoutHelper$ScoutBinderInfo;)Ljava/lang/Boolean;
    .locals 35
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "pid"    # I
    .param p2, "scoutInfo"    # Lcom/android/server/ScoutHelper$ScoutBinderInfo;

    .line 324
    move-object/from16 v1, p0

    move/from16 v2, p1

    move-object/from16 v3, p2

    const-string v4, "/"

    const-string v0, "/proc/"

    const/4 v5, 0x0

    .line 325
    .local v5, "result":Z
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 327
    .local v6, "procInfo":Ljava/lang/StringBuilder;
    :try_start_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/task"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 328
    .local v7, "taskPath":Ljava/lang/String;
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 329
    .local v8, "taskDir":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v9
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_b

    .line 330
    .local v9, "threadDirs":[Ljava/io/File;
    const-string v10, "("

    const-string v11, "; tracerPid="

    const-string v13, "\n"

    const-string v14, ") have "

    const-string v15, "Pid("

    const-string v12, "T"

    move/from16 v16, v5

    .end local v5    # "result":Z
    .local v16, "result":Z
    const-string v5, "Z"

    move-object/from16 v17, v8

    .end local v8    # "taskDir":Ljava/io/File;
    .local v17, "taskDir":Ljava/io/File;
    const-string v8, "D"

    const-string v3, "\\s+"

    move-object/from16 v18, v6

    .end local v6    # "procInfo":Ljava/lang/StringBuilder;
    .local v18, "procInfo":Ljava/lang/StringBuilder;
    const-string v6, "/stat"

    move-object/from16 v20, v13

    const-string v13, "/stack"

    const/16 v21, 0x0

    move-object/from16 v22, v13

    const-string v13, ")"

    const-string/jumbo v1, "t"

    move-object/from16 v23, v10

    if-eqz v9, :cond_7

    .line 331
    :try_start_1
    array-length v10, v9

    move-object/from16 v24, v11

    move/from16 v11, v21

    :goto_0
    if-ge v11, v10, :cond_6

    aget-object v0, v9, v11

    move-object/from16 v25, v0

    .line 332
    .local v25, "threadDir":Ljava/io/File;
    new-instance v0, Ljava/io/BufferedReader;

    move-object/from16 v26, v9

    .end local v9    # "threadDirs":[Ljava/io/File;
    .local v26, "threadDirs":[Ljava/io/File;
    new-instance v9, Ljava/io/FileReader;

    move/from16 v27, v10

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 333
    move/from16 v28, v11

    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v9}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6

    move-object v9, v0

    .line 334
    .local v9, "threadStat":Ljava/io/BufferedReader;
    :try_start_2
    invoke-virtual {v9}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    move-object v10, v0

    .line 335
    .local v10, "statInfo":Ljava/lang/String;
    const/16 v11, 0x28

    invoke-virtual {v10, v11}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 336
    move-object/from16 v29, v6

    const/16 v11, 0x29

    invoke-virtual {v10, v11}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    .line 335
    invoke-virtual {v10, v0, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    .line 337
    .local v6, "threadName":Ljava/lang/String;
    invoke-virtual {v10, v11}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    invoke-virtual {v10, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 338
    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v21

    move-object v11, v0

    .line 339
    .local v11, "threadState":Ljava/lang/String;
    invoke-virtual {v11, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v11, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 340
    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    move-object v6, v1

    move-object/from16 v34, v4

    move-object/from16 v30, v20

    move-object/from16 v11, v22

    move-object/from16 v1, p0

    goto/16 :goto_7

    .line 341
    :cond_1
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v30, v0

    .line 342
    .local v30, "logInfo":Ljava/lang/StringBuilder;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object/from16 v31, v10

    .end local v10    # "statInfo":Ljava/lang/String;
    .local v31, "statInfo":Ljava/lang/String;
    const-string v10, " state thread(tid:"

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 343
    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v10, " name:"

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 342
    move-object/from16 v10, v30

    .end local v30    # "logInfo":Ljava/lang/StringBuilder;
    .local v10, "logInfo":Ljava/lang/StringBuilder;
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 344
    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 345
    invoke-static/range {p1 .. p1}, Lcom/android/server/ScoutHelper;->getProcStatus(I)Lcom/android/server/ScoutHelper$ProcStatus;

    move-result-object v0

    .line 346
    .local v0, "procStatus":Lcom/android/server/ScoutHelper$ProcStatus;
    if-eqz v0, :cond_3

    .line 347
    move-object/from16 v30, v1

    iget v1, v0, Lcom/android/server/ScoutHelper$ProcStatus;->tracerPid:I

    .line 348
    .local v1, "tracerPid":I
    move-object/from16 v32, v6

    const/4 v6, -0x1

    .end local v6    # "threadName":Ljava/lang/String;
    .local v32, "threadName":Ljava/lang/String;
    if-le v1, v6, :cond_2

    .line 349
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v33, v11

    move-object/from16 v11, v24

    .end local v11    # "threadState":Ljava/lang/String;
    .local v33, "threadState":Ljava/lang/String;
    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v24, v11

    move-object/from16 v11, v23

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 350
    move-object/from16 v23, v0

    .end local v0    # "procStatus":Lcom/android/server/ScoutHelper$ProcStatus;
    .local v23, "procStatus":Lcom/android/server/ScoutHelper$ProcStatus;
    invoke-static {v1}, Lcom/android/server/ScoutHelper;->getProcessCmdline(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 349
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 348
    .end local v23    # "procStatus":Lcom/android/server/ScoutHelper$ProcStatus;
    .end local v33    # "threadState":Ljava/lang/String;
    .restart local v0    # "procStatus":Lcom/android/server/ScoutHelper$ProcStatus;
    .restart local v11    # "threadState":Ljava/lang/String;
    :cond_2
    move-object/from16 v33, v11

    move-object/from16 v11, v23

    move-object/from16 v23, v0

    .end local v0    # "procStatus":Lcom/android/server/ScoutHelper$ProcStatus;
    .end local v11    # "threadState":Ljava/lang/String;
    .restart local v23    # "procStatus":Lcom/android/server/ScoutHelper$ProcStatus;
    .restart local v33    # "threadState":Ljava/lang/String;
    goto :goto_2

    .line 346
    .end local v1    # "tracerPid":I
    .end local v23    # "procStatus":Lcom/android/server/ScoutHelper$ProcStatus;
    .end local v32    # "threadName":Ljava/lang/String;
    .end local v33    # "threadState":Ljava/lang/String;
    .restart local v0    # "procStatus":Lcom/android/server/ScoutHelper$ProcStatus;
    .restart local v6    # "threadName":Ljava/lang/String;
    .restart local v11    # "threadState":Ljava/lang/String;
    :cond_3
    move-object/from16 v30, v1

    move-object/from16 v32, v6

    move-object/from16 v33, v11

    move-object/from16 v11, v23

    move-object/from16 v23, v0

    .end local v0    # "procStatus":Lcom/android/server/ScoutHelper$ProcStatus;
    .end local v6    # "threadName":Ljava/lang/String;
    .end local v11    # "threadState":Ljava/lang/String;
    .restart local v23    # "procStatus":Lcom/android/server/ScoutHelper$ProcStatus;
    .restart local v32    # "threadName":Ljava/lang/String;
    .restart local v33    # "threadState":Ljava/lang/String;
    goto :goto_2

    .line 344
    .end local v23    # "procStatus":Lcom/android/server/ScoutHelper$ProcStatus;
    .end local v32    # "threadName":Ljava/lang/String;
    .end local v33    # "threadState":Ljava/lang/String;
    .restart local v6    # "threadName":Ljava/lang/String;
    .restart local v11    # "threadState":Ljava/lang/String;
    :cond_4
    move-object/from16 v30, v1

    move-object/from16 v32, v6

    move-object/from16 v33, v11

    move-object/from16 v11, v23

    .line 354
    .end local v6    # "threadName":Ljava/lang/String;
    .end local v11    # "threadState":Ljava/lang/String;
    .restart local v32    # "threadName":Ljava/lang/String;
    .restart local v33    # "threadState":Ljava/lang/String;
    :goto_2
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    move-object/from16 v1, p0

    move-object/from16 v6, v30

    :try_start_3
    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    move-object/from16 v23, v11

    move-object/from16 v11, v20

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 356
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move-object/from16 v20, v10

    move-object/from16 v10, v18

    .end local v18    # "procInfo":Ljava/lang/StringBuilder;
    .local v10, "procInfo":Ljava/lang/StringBuilder;
    .local v20, "logInfo":Ljava/lang/StringBuilder;
    :try_start_4
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 357
    :try_start_5
    new-instance v0, Ljava/io/BufferedReader;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-object/from16 v18, v10

    .end local v10    # "procInfo":Ljava/lang/StringBuilder;
    .restart local v18    # "procInfo":Ljava/lang/StringBuilder;
    :try_start_6
    new-instance v10, Ljava/io/FileReader;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    move-object/from16 v30, v11

    :try_start_7
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 358
    move-object/from16 v34, v4

    :try_start_8
    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    move-object/from16 v11, v22

    :try_start_9
    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v10, v4}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    move-object v4, v0

    .line 359
    .local v4, "threadStack":Ljava/io/BufferedReader;
    const/4 v0, 0x0

    .line 360
    .local v0, "stackLineInfo":Ljava/lang/String;
    :goto_3
    :try_start_a
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v10

    move-object v0, v10

    if-eqz v10, :cond_5

    .line 361
    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto :goto_3

    .line 363
    .end local v0    # "stackLineInfo":Ljava/lang/String;
    :cond_5
    :try_start_b
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    .line 364
    .end local v4    # "threadStack":Ljava/io/BufferedReader;
    goto :goto_6

    .line 357
    .restart local v4    # "threadStack":Ljava/io/BufferedReader;
    :catchall_0
    move-exception v0

    move-object v10, v0

    :try_start_c
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    move-object/from16 v22, v4

    goto :goto_4

    :catchall_1
    move-exception v0

    move-object/from16 v22, v4

    move-object v4, v0

    .end local v4    # "threadStack":Ljava/io/BufferedReader;
    .local v22, "threadStack":Ljava/io/BufferedReader;
    :try_start_d
    invoke-virtual {v10, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v7    # "taskPath":Ljava/lang/String;
    .end local v9    # "threadStat":Ljava/io/BufferedReader;
    .end local v16    # "result":Z
    .end local v17    # "taskDir":Ljava/io/File;
    .end local v18    # "procInfo":Ljava/lang/StringBuilder;
    .end local v20    # "logInfo":Ljava/lang/StringBuilder;
    .end local v25    # "threadDir":Ljava/io/File;
    .end local v26    # "threadDirs":[Ljava/io/File;
    .end local v31    # "statInfo":Ljava/lang/String;
    .end local v32    # "threadName":Ljava/lang/String;
    .end local v33    # "threadState":Ljava/lang/String;
    .end local p0    # "tag":Ljava/lang/String;
    .end local p1    # "pid":I
    .end local p2    # "scoutInfo":Lcom/android/server/ScoutHelper$ScoutBinderInfo;
    :goto_4
    throw v10
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_0
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    .line 363
    .end local v22    # "threadStack":Ljava/io/BufferedReader;
    .restart local v7    # "taskPath":Ljava/lang/String;
    .restart local v9    # "threadStat":Ljava/io/BufferedReader;
    .restart local v16    # "result":Z
    .restart local v17    # "taskDir":Ljava/io/File;
    .restart local v18    # "procInfo":Ljava/lang/StringBuilder;
    .restart local v20    # "logInfo":Ljava/lang/StringBuilder;
    .restart local v25    # "threadDir":Ljava/io/File;
    .restart local v26    # "threadDirs":[Ljava/io/File;
    .restart local v31    # "statInfo":Ljava/lang/String;
    .restart local v32    # "threadName":Ljava/lang/String;
    .restart local v33    # "threadState":Ljava/lang/String;
    .restart local p0    # "tag":Ljava/lang/String;
    .restart local p1    # "pid":I
    .restart local p2    # "scoutInfo":Lcom/android/server/ScoutHelper$ScoutBinderInfo;
    :catch_0
    move-exception v0

    goto :goto_6

    :catch_1
    move-exception v0

    goto :goto_5

    :catch_2
    move-exception v0

    move-object/from16 v34, v4

    goto :goto_5

    :catch_3
    move-exception v0

    move-object/from16 v34, v4

    move-object/from16 v30, v11

    :goto_5
    move-object/from16 v11, v22

    goto :goto_6

    .end local v18    # "procInfo":Ljava/lang/StringBuilder;
    .restart local v10    # "procInfo":Ljava/lang/StringBuilder;
    :catch_4
    move-exception v0

    move-object/from16 v34, v4

    move-object/from16 v18, v10

    move-object/from16 v30, v11

    move-object/from16 v11, v22

    .line 365
    .end local v10    # "procInfo":Ljava/lang/StringBuilder;
    .restart local v18    # "procInfo":Ljava/lang/StringBuilder;
    :goto_6
    const/4 v0, 0x1

    move/from16 v16, v0

    .line 367
    .end local v20    # "logInfo":Ljava/lang/StringBuilder;
    .end local v31    # "statInfo":Ljava/lang/String;
    .end local v32    # "threadName":Ljava/lang/String;
    .end local v33    # "threadState":Ljava/lang/String;
    :goto_7
    :try_start_e
    invoke-virtual {v9}, Ljava/io/BufferedReader;->close()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_5

    .line 331
    .end local v9    # "threadStat":Ljava/io/BufferedReader;
    .end local v25    # "threadDir":Ljava/io/File;
    add-int/lit8 v0, v28, 0x1

    move-object v1, v6

    move-object/from16 v22, v11

    move-object/from16 v9, v26

    move/from16 v10, v27

    move-object/from16 v6, v29

    move-object/from16 v20, v30

    move-object/from16 v4, v34

    move v11, v0

    goto/16 :goto_0

    .line 332
    .end local v18    # "procInfo":Ljava/lang/StringBuilder;
    .restart local v9    # "threadStat":Ljava/io/BufferedReader;
    .restart local v10    # "procInfo":Ljava/lang/StringBuilder;
    .restart local v25    # "threadDir":Ljava/io/File;
    :catchall_2
    move-exception v0

    move-object/from16 v18, v10

    move-object v3, v0

    .end local v10    # "procInfo":Ljava/lang/StringBuilder;
    .restart local v18    # "procInfo":Ljava/lang/StringBuilder;
    goto :goto_9

    :catchall_3
    move-exception v0

    goto :goto_8

    :catchall_4
    move-exception v0

    move-object/from16 v1, p0

    :goto_8
    move-object v3, v0

    :goto_9
    :try_start_f
    invoke-virtual {v9}, Ljava/io/BufferedReader;->close()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_5

    goto :goto_a

    :catchall_5
    move-exception v0

    move-object v4, v0

    :try_start_10
    invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v16    # "result":Z
    .end local v18    # "procInfo":Ljava/lang/StringBuilder;
    .end local p0    # "tag":Ljava/lang/String;
    .end local p1    # "pid":I
    .end local p2    # "scoutInfo":Lcom/android/server/ScoutHelper$ScoutBinderInfo;
    :goto_a
    throw v3
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_5

    .line 408
    .end local v7    # "taskPath":Ljava/lang/String;
    .end local v9    # "threadStat":Ljava/io/BufferedReader;
    .end local v17    # "taskDir":Ljava/io/File;
    .end local v25    # "threadDir":Ljava/io/File;
    .end local v26    # "threadDirs":[Ljava/io/File;
    .restart local v16    # "result":Z
    .restart local v18    # "procInfo":Ljava/lang/StringBuilder;
    .restart local p0    # "tag":Ljava/lang/String;
    .restart local p1    # "pid":I
    .restart local p2    # "scoutInfo":Lcom/android/server/ScoutHelper$ScoutBinderInfo;
    :catch_5
    move-exception v0

    goto :goto_b

    .line 331
    .restart local v7    # "taskPath":Ljava/lang/String;
    .local v9, "threadDirs":[Ljava/io/File;
    .restart local v17    # "taskDir":Ljava/io/File;
    :cond_6
    move-object/from16 v1, p0

    move-object/from16 v26, v9

    .end local v9    # "threadDirs":[Ljava/io/File;
    .restart local v26    # "threadDirs":[Ljava/io/File;
    move-object/from16 v6, v18

    goto/16 :goto_11

    .line 408
    .end local v7    # "taskPath":Ljava/lang/String;
    .end local v17    # "taskDir":Ljava/io/File;
    .end local v26    # "threadDirs":[Ljava/io/File;
    :catch_6
    move-exception v0

    move-object/from16 v1, p0

    :goto_b
    move/from16 v5, v16

    move-object/from16 v6, v18

    goto/16 :goto_14

    .line 370
    .restart local v7    # "taskPath":Ljava/lang/String;
    .restart local v9    # "threadDirs":[Ljava/io/File;
    .restart local v17    # "taskDir":Ljava/io/File;
    :cond_7
    move-object/from16 v29, v6

    move-object/from16 v26, v9

    move-object/from16 v24, v11

    move-object/from16 v30, v20

    move-object/from16 v11, v22

    move-object v6, v1

    move-object/from16 v1, p0

    .end local v9    # "threadDirs":[Ljava/io/File;
    .restart local v26    # "threadDirs":[Ljava/io/File;
    :try_start_11
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 371
    .local v4, "stackPath":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object/from16 v9, v29

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v9, v0

    .line 372
    .local v9, "statPath":Ljava/lang/String;
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v10, Ljava/io/FileReader;

    invoke-direct {v10, v9}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_a

    move-object v10, v0

    .line 373
    .local v10, "procStat":Ljava/io/BufferedReader;
    :try_start_12
    invoke-virtual {v10}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_c

    move-object v11, v0

    .line 374
    .local v11, "statInfo":Ljava/lang/String;
    move-object/from16 v20, v9

    const/16 v9, 0x28

    .end local v9    # "statPath":Ljava/lang/String;
    .local v20, "statPath":Ljava/lang/String;
    :try_start_13
    invoke-virtual {v11, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v0
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_b

    add-int/lit8 v0, v0, 0x1

    .line 375
    move-object/from16 v19, v10

    const/16 v9, 0x29

    .end local v10    # "procStat":Ljava/io/BufferedReader;
    .local v19, "procStat":Ljava/io/BufferedReader;
    :try_start_14
    invoke-virtual {v11, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v10

    .line 374
    invoke-virtual {v11, v0, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    move-object v10, v0

    .line 376
    .local v10, "procName":Ljava/lang/String;
    invoke-virtual {v11, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    invoke-virtual {v11, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 377
    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v21

    move-object v3, v0

    .line 378
    .local v3, "procState":Ljava/lang/String;
    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_a

    if-nez v0, :cond_9

    :try_start_15
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    invoke-virtual {v3, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 379
    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_6

    if-eqz v0, :cond_8

    goto :goto_c

    :cond_8
    move/from16 v5, v16

    move-object/from16 v6, v18

    goto/16 :goto_10

    .line 372
    .end local v3    # "procState":Ljava/lang/String;
    .end local v10    # "procName":Ljava/lang/String;
    .end local v11    # "statInfo":Ljava/lang/String;
    :catchall_6
    move-exception v0

    move-object v3, v0

    move-object/from16 v6, v18

    goto/16 :goto_12

    .line 380
    .restart local v3    # "procState":Ljava/lang/String;
    .restart local v10    # "procName":Ljava/lang/String;
    .restart local v11    # "statInfo":Ljava/lang/String;
    :cond_9
    :goto_c
    :try_start_16
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    move-object v5, v0

    .line 381
    .local v5, "logInfo":Ljava/lang/StringBuilder;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, " state in main thread ( name:"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 383
    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_a

    if-eqz v0, :cond_a

    .line 384
    :try_start_17
    invoke-static/range {p1 .. p1}, Lcom/android/server/ScoutHelper;->getProcStatus(I)Lcom/android/server/ScoutHelper$ProcStatus;

    move-result-object v0

    .line 385
    .local v0, "procStatus":Lcom/android/server/ScoutHelper$ProcStatus;
    if-eqz v0, :cond_a

    .line 386
    iget v6, v0, Lcom/android/server/ScoutHelper$ProcStatus;->tracerPid:I

    .line 387
    .local v6, "tracerPid":I
    const/4 v8, -0x1

    if-le v6, v8, :cond_a

    .line 388
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v9, v24

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v9, v23

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 389
    invoke-static {v6}, Lcom/android/server/ScoutHelper;->getProcessCmdline(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 388
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_6

    .line 393
    .end local v0    # "procStatus":Lcom/android/server/ScoutHelper$ProcStatus;
    .end local v6    # "tracerPid":I
    :cond_a
    :try_start_18
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    move-object/from16 v6, v30

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 395
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_a

    move-object/from16 v6, v18

    .end local v18    # "procInfo":Ljava/lang/StringBuilder;
    .local v6, "procInfo":Ljava/lang/StringBuilder;
    :try_start_19
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_9

    .line 396
    :try_start_1a
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v8, Ljava/io/FileReader;

    invoke-direct {v8, v4}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_7
    .catchall {:try_start_1a .. :try_end_1a} :catchall_9

    move-object v8, v0

    .line 397
    .local v8, "procStack":Ljava/io/BufferedReader;
    const/4 v0, 0x0

    .line 398
    .local v0, "stackLineInfo":Ljava/lang/String;
    :goto_d
    :try_start_1b
    invoke-virtual {v8}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v9

    move-object v0, v9

    if-eqz v9, :cond_b

    .line 399
    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_7

    goto :goto_d

    .line 401
    .end local v0    # "stackLineInfo":Ljava/lang/String;
    :cond_b
    :try_start_1c
    invoke-virtual {v8}, Ljava/io/BufferedReader;->close()V
    :try_end_1c
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1c} :catch_7
    .catchall {:try_start_1c .. :try_end_1c} :catchall_9

    .line 403
    .end local v8    # "procStack":Ljava/io/BufferedReader;
    goto :goto_f

    .line 396
    .restart local v8    # "procStack":Ljava/io/BufferedReader;
    :catchall_7
    move-exception v0

    move-object v9, v0

    :try_start_1d
    invoke-virtual {v8}, Ljava/io/BufferedReader;->close()V
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_8

    goto :goto_e

    :catchall_8
    move-exception v0

    move-object v12, v0

    :try_start_1e
    invoke-virtual {v9, v12}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v3    # "procState":Ljava/lang/String;
    .end local v4    # "stackPath":Ljava/lang/String;
    .end local v5    # "logInfo":Ljava/lang/StringBuilder;
    .end local v6    # "procInfo":Ljava/lang/StringBuilder;
    .end local v7    # "taskPath":Ljava/lang/String;
    .end local v10    # "procName":Ljava/lang/String;
    .end local v11    # "statInfo":Ljava/lang/String;
    .end local v16    # "result":Z
    .end local v17    # "taskDir":Ljava/io/File;
    .end local v19    # "procStat":Ljava/io/BufferedReader;
    .end local v20    # "statPath":Ljava/lang/String;
    .end local v26    # "threadDirs":[Ljava/io/File;
    .end local p0    # "tag":Ljava/lang/String;
    .end local p1    # "pid":I
    .end local p2    # "scoutInfo":Lcom/android/server/ScoutHelper$ScoutBinderInfo;
    :goto_e
    throw v9
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_1e} :catch_7
    .catchall {:try_start_1e .. :try_end_1e} :catchall_9

    .line 401
    .end local v8    # "procStack":Ljava/io/BufferedReader;
    .restart local v3    # "procState":Ljava/lang/String;
    .restart local v4    # "stackPath":Ljava/lang/String;
    .restart local v5    # "logInfo":Ljava/lang/StringBuilder;
    .restart local v6    # "procInfo":Ljava/lang/StringBuilder;
    .restart local v7    # "taskPath":Ljava/lang/String;
    .restart local v10    # "procName":Ljava/lang/String;
    .restart local v11    # "statInfo":Ljava/lang/String;
    .restart local v16    # "result":Z
    .restart local v17    # "taskDir":Ljava/io/File;
    .restart local v19    # "procStat":Ljava/io/BufferedReader;
    .restart local v20    # "statPath":Ljava/lang/String;
    .restart local v26    # "threadDirs":[Ljava/io/File;
    .restart local p0    # "tag":Ljava/lang/String;
    .restart local p1    # "pid":I
    .restart local p2    # "scoutInfo":Lcom/android/server/ScoutHelper$ScoutBinderInfo;
    :catch_7
    move-exception v0

    .line 402
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1f
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Failed to read "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v1, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_9

    .line 404
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_f
    const/4 v0, 0x1

    move v5, v0

    .line 406
    .end local v3    # "procState":Ljava/lang/String;
    .end local v10    # "procName":Ljava/lang/String;
    .end local v11    # "statInfo":Ljava/lang/String;
    .end local v16    # "result":Z
    .local v5, "result":Z
    :goto_10
    :try_start_20
    invoke-virtual/range {v19 .. v19}, Ljava/io/BufferedReader;->close()V
    :try_end_20
    .catch Ljava/lang/Exception; {:try_start_20 .. :try_end_20} :catch_8

    move/from16 v16, v5

    .line 410
    .end local v4    # "stackPath":Ljava/lang/String;
    .end local v5    # "result":Z
    .end local v7    # "taskPath":Ljava/lang/String;
    .end local v17    # "taskDir":Ljava/io/File;
    .end local v19    # "procStat":Ljava/io/BufferedReader;
    .end local v20    # "statPath":Ljava/lang/String;
    .end local v26    # "threadDirs":[Ljava/io/File;
    .restart local v16    # "result":Z
    :goto_11
    goto :goto_15

    .line 408
    .end local v16    # "result":Z
    .restart local v5    # "result":Z
    :catch_8
    move-exception v0

    goto :goto_14

    .line 372
    .end local v5    # "result":Z
    .restart local v4    # "stackPath":Ljava/lang/String;
    .restart local v7    # "taskPath":Ljava/lang/String;
    .restart local v16    # "result":Z
    .restart local v17    # "taskDir":Ljava/io/File;
    .restart local v19    # "procStat":Ljava/io/BufferedReader;
    .restart local v20    # "statPath":Ljava/lang/String;
    .restart local v26    # "threadDirs":[Ljava/io/File;
    :catchall_9
    move-exception v0

    move-object v3, v0

    goto :goto_12

    .end local v6    # "procInfo":Ljava/lang/StringBuilder;
    .restart local v18    # "procInfo":Ljava/lang/StringBuilder;
    :catchall_a
    move-exception v0

    move-object/from16 v6, v18

    move-object v3, v0

    .end local v18    # "procInfo":Ljava/lang/StringBuilder;
    .restart local v6    # "procInfo":Ljava/lang/StringBuilder;
    goto :goto_12

    .end local v6    # "procInfo":Ljava/lang/StringBuilder;
    .end local v19    # "procStat":Ljava/io/BufferedReader;
    .local v10, "procStat":Ljava/io/BufferedReader;
    .restart local v18    # "procInfo":Ljava/lang/StringBuilder;
    :catchall_b
    move-exception v0

    move-object/from16 v19, v10

    move-object/from16 v6, v18

    move-object v3, v0

    .end local v10    # "procStat":Ljava/io/BufferedReader;
    .end local v18    # "procInfo":Ljava/lang/StringBuilder;
    .restart local v6    # "procInfo":Ljava/lang/StringBuilder;
    .restart local v19    # "procStat":Ljava/io/BufferedReader;
    goto :goto_12

    .end local v6    # "procInfo":Ljava/lang/StringBuilder;
    .end local v19    # "procStat":Ljava/io/BufferedReader;
    .end local v20    # "statPath":Ljava/lang/String;
    .restart local v9    # "statPath":Ljava/lang/String;
    .restart local v10    # "procStat":Ljava/io/BufferedReader;
    .restart local v18    # "procInfo":Ljava/lang/StringBuilder;
    :catchall_c
    move-exception v0

    move-object/from16 v20, v9

    move-object/from16 v19, v10

    move-object/from16 v6, v18

    move-object v3, v0

    .end local v9    # "statPath":Ljava/lang/String;
    .end local v10    # "procStat":Ljava/io/BufferedReader;
    .end local v18    # "procInfo":Ljava/lang/StringBuilder;
    .restart local v6    # "procInfo":Ljava/lang/StringBuilder;
    .restart local v19    # "procStat":Ljava/io/BufferedReader;
    .restart local v20    # "statPath":Ljava/lang/String;
    :goto_12
    :try_start_21
    invoke-virtual/range {v19 .. v19}, Ljava/io/BufferedReader;->close()V
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_d

    goto :goto_13

    :catchall_d
    move-exception v0

    move-object v5, v0

    :try_start_22
    invoke-virtual {v3, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v6    # "procInfo":Ljava/lang/StringBuilder;
    .end local v16    # "result":Z
    .end local p0    # "tag":Ljava/lang/String;
    .end local p1    # "pid":I
    .end local p2    # "scoutInfo":Lcom/android/server/ScoutHelper$ScoutBinderInfo;
    :goto_13
    throw v3
    :try_end_22
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_22} :catch_9

    .line 408
    .end local v4    # "stackPath":Ljava/lang/String;
    .end local v7    # "taskPath":Ljava/lang/String;
    .end local v17    # "taskDir":Ljava/io/File;
    .end local v19    # "procStat":Ljava/io/BufferedReader;
    .end local v20    # "statPath":Ljava/lang/String;
    .end local v26    # "threadDirs":[Ljava/io/File;
    .restart local v6    # "procInfo":Ljava/lang/StringBuilder;
    .restart local v16    # "result":Z
    .restart local p0    # "tag":Ljava/lang/String;
    .restart local p1    # "pid":I
    .restart local p2    # "scoutInfo":Lcom/android/server/ScoutHelper$ScoutBinderInfo;
    :catch_9
    move-exception v0

    move/from16 v5, v16

    goto :goto_14

    .end local v6    # "procInfo":Ljava/lang/StringBuilder;
    .restart local v18    # "procInfo":Ljava/lang/StringBuilder;
    :catch_a
    move-exception v0

    move-object/from16 v6, v18

    move/from16 v5, v16

    .end local v18    # "procInfo":Ljava/lang/StringBuilder;
    .restart local v6    # "procInfo":Ljava/lang/StringBuilder;
    goto :goto_14

    .end local v16    # "result":Z
    .restart local v5    # "result":Z
    :catch_b
    move-exception v0

    move/from16 v16, v5

    .line 409
    .restart local v0    # "e":Ljava/lang/Exception;
    :goto_14
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to read thread stat: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    move/from16 v16, v5

    .line 411
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v5    # "result":Z
    .restart local v16    # "result":Z
    :goto_15
    move-object/from16 v3, p2

    if-eqz v3, :cond_c

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-eqz v0, :cond_c

    .line 412
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->setProcInfo(Ljava/lang/String;)V

    .line 414
    :cond_c
    invoke-static/range {v16 .. v16}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static addPidtoList(Ljava/lang/String;ILjava/util/ArrayList;Ljava/util/ArrayList;)Z
    .locals 5
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "pid"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    .line 437
    .local p2, "javaProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .local p3, "nativeProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-static {p0, p1}, Lcom/android/server/ScoutHelper;->getOomAdjOfPid(Ljava/lang/String;I)I

    move-result v0

    .line 438
    .local v0, "adj":I
    invoke-static {v0}, Lcom/android/server/ScoutHelper;->checkIsJavaOrNativeProcess(I)I

    move-result v1

    .line 439
    .local v1, "isJavaOrNativeProcess":I
    const/4 v2, 0x0

    if-nez v1, :cond_0

    return v2

    .line 440
    :cond_0
    const/4 v3, 0x1

    if-ne v1, v3, :cond_1

    .line 441
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 442
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 443
    return v3

    .line 444
    :cond_1
    const/4 v4, 0x2

    if-ne v1, v4, :cond_2

    .line 445
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 446
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 447
    return v3

    .line 449
    :cond_2
    return v2
.end method

.method public static captureLog(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;ZIZLjava/lang/String;Ljava/util/List;)V
    .locals 15
    .param p0, "type"    # Ljava/lang/String;
    .param p1, "headline"    # Ljava/lang/String;
    .param p4, "offline"    # Z
    .param p5, "id"    # I
    .param p6, "upload"    # Z
    .param p7, "where"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;ZIZ",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 834
    .local p2, "actions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "params":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p8, "includeFiles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Lcom/android/server/ScoutHelper;->getmDaemon()Lmiui/mqsas/IMQSNative;

    move-result-object v12

    .line 835
    .local v12, "mClient":Lmiui/mqsas/IMQSNative;
    const-string v13, "ScoutHelper"

    if-nez v12, :cond_0

    .line 836
    const-string v0, "CaptureLog no mqsasd!"

    invoke-static {v13, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 837
    return-void

    .line 840
    :cond_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_3

    .line 844
    :cond_1
    if-nez p7, :cond_2

    .line 845
    const-string v0, ""

    .line 846
    .end local p7    # "where":Ljava/lang/String;
    .local v0, "where":Ljava/lang/String;
    const-string v1, "CaptureLog where is null!"

    invoke-static {v13, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v14, v0

    goto :goto_0

    .line 844
    .end local v0    # "where":Ljava/lang/String;
    .restart local p7    # "where":Ljava/lang/String;
    :cond_2
    move-object/from16 v14, p7

    .line 849
    .end local p7    # "where":Ljava/lang/String;
    .local v14, "where":Ljava/lang/String;
    :goto_0
    const/4 v11, 0x0

    move-object v1, v12

    move-object v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    move-object v9, v14

    move-object/from16 v10, p8

    :try_start_0
    invoke-interface/range {v1 .. v11}, Lmiui/mqsas/IMQSNative;->captureLog(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;ZIZLjava/lang/String;Ljava/util/List;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 854
    :goto_1
    goto :goto_2

    .line 852
    :catch_0
    move-exception v0

    move-object v1, v0

    move-object v0, v1

    .line 853
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "CaptureLog failed! unknown error"

    invoke-static {v13, v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 850
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    move-object v1, v0

    move-object v0, v1

    .line 851
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "CaptureLog failed!"

    invoke-static {v13, v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .end local v0    # "e":Landroid/os/RemoteException;
    goto :goto_1

    .line 855
    :goto_2
    return-void

    .line 841
    .end local v14    # "where":Ljava/lang/String;
    .restart local p7    # "where":Ljava/lang/String;
    :cond_3
    :goto_3
    const-string v0, "CaptureLog type or headline is null!"

    invoke-static {v13, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 842
    return-void
.end method

.method public static checkAsyncBinderCallPidList(IILcom/android/server/ScoutHelper$ScoutBinderInfo;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
    .locals 28
    .param p0, "anrFromPid"    # I
    .param p1, "anrToPid"    # I
    .param p2, "info"    # Lcom/android/server/ScoutHelper$ScoutBinderInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/android/server/ScoutHelper$ScoutBinderInfo;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    .line 455
    .local p3, "javaProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .local p4, "nativeProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    move/from16 v1, p0

    move/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    const-string v6, "):"

    const/4 v7, 0x0

    .line 456
    .local v7, "isBlockSystem":Z
    invoke-virtual/range {p2 .. p2}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->getTag()Ljava/lang/String;

    move-result-object v8

    .line 457
    .local v8, "tag":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Check async binder info with Broadcast ANR  Pid="

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, " SystemPid="

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    invoke-static {v8, v2}, Lcom/android/server/ScoutHelper;->getBinderLogFile(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v9

    .line 460
    .local v9, "fileBinderReader":Ljava/io/File;
    if-nez v9, :cond_0

    .line 461
    return v7

    .line 463
    :cond_0
    :try_start_0
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v10, Ljava/io/FileReader;

    invoke-direct {v10, v9}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_a

    move-object v10, v0

    .line 464
    .local v10, "BinderReader":Ljava/io/BufferedReader;
    const/4 v0, 0x0

    .line 465
    .local v0, "binderTransactionInfo":Ljava/lang/String;
    const/4 v11, 0x0

    .line 466
    .local v11, "fromPid":I
    :goto_0
    :try_start_1
    invoke-virtual {v10}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v12

    move-object v13, v12

    .end local v0    # "binderTransactionInfo":Ljava/lang/String;
    .local v13, "binderTransactionInfo":Ljava/lang/String;
    if-eqz v12, :cond_9

    .line 467
    const-string v0, "MIUI    pending async transaction"

    invoke-virtual {v13, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_6

    if-nez v0, :cond_1

    :try_start_2
    const-string v0, "pending async transaction"

    .line 468
    invoke-virtual {v13, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/android/server/ScoutHelper;->supportNewBinderLog:Ljava/lang/Boolean;

    .line 469
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "    pending async transaction"

    invoke-virtual {v13, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v0, :cond_3

    goto :goto_1

    .line 463
    .end local v11    # "fromPid":I
    .end local v13    # "binderTransactionInfo":Ljava/lang/String;
    :catchall_0
    move-exception v0

    move-object v6, v0

    move-object/from16 v18, v9

    move-object/from16 v23, v10

    goto/16 :goto_7

    .line 471
    .restart local v11    # "fromPid":I
    .restart local v13    # "binderTransactionInfo":Ljava/lang/String;
    :cond_1
    :goto_1
    :try_start_3
    const-string v0, "\\bfrom((?:\\s+)?\\d+):((?:\\s+)?\\d+)\\s+to((?:\\s+)?\\d+):((?:\\s+)?\\d+)+.*code:((?:\\s+)?\\d+).*duration:((?:\\s+)?(\\d+(?:\\.\\d+)?))\\b"

    .line 472
    .local v0, "binderRegexPattern":Ljava/lang/String;
    sget-object v12, Lcom/android/server/ScoutHelper;->supportNewBinderLog:Ljava/lang/Boolean;

    invoke-virtual {v12}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_6

    if-eqz v12, :cond_2

    :try_start_4
    const-string v12, "\\bfrom((?:\\s+)?\\d+):((?:\\s+)?\\d+)\\s+to((?:\\s+)?\\d+):((?:\\s+)?\\d+)+.*code((?:\\s+)?\\d+)+.*elapsed((?:\\s+)?\\d+)ms+.*"
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-object v0, v12

    goto :goto_2

    .line 508
    .end local v0    # "binderRegexPattern":Ljava/lang/String;
    :catch_0
    move-exception v0

    move-object/from16 v18, v9

    move-object/from16 v23, v10

    move-object/from16 v21, v13

    goto/16 :goto_6

    .line 473
    .restart local v0    # "binderRegexPattern":Ljava/lang/String;
    :cond_2
    :goto_2
    :try_start_5
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v12

    .line 474
    .local v12, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v12, v13}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v14

    .line 475
    .local v14, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v14}, Ljava/util/regex/Matcher;->find()Z

    move-result v15
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_7
    .catchall {:try_start_5 .. :try_end_5} :catchall_6

    move-object/from16 v16, v0

    .end local v0    # "binderRegexPattern":Ljava/lang/String;
    .local v16, "binderRegexPattern":Ljava/lang/String;
    const-string v0, "("

    if-nez v15, :cond_4

    .line 476
    :try_start_6
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v15, ") regex match failed"

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 477
    nop

    .line 466
    .end local v12    # "pattern":Ljava/util/regex/Pattern;
    .end local v14    # "matcher":Ljava/util/regex/Matcher;
    .end local v16    # "binderRegexPattern":Ljava/lang/String;
    :cond_3
    move-object v0, v13

    goto :goto_0

    .line 479
    .restart local v12    # "pattern":Ljava/util/regex/Pattern;
    .restart local v14    # "matcher":Ljava/util/regex/Matcher;
    .restart local v16    # "binderRegexPattern":Ljava/lang/String;
    :cond_4
    const/4 v15, 0x1

    :try_start_7
    invoke-virtual {v14, v15}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    move v11, v15

    .line 480
    const/4 v15, 0x2

    invoke-virtual {v14, v15}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_6

    .line 481
    .local v15, "fromTid":I
    move/from16 v17, v7

    .end local v7    # "isBlockSystem":Z
    .local v17, "isBlockSystem":Z
    const/4 v7, 0x3

    :try_start_8
    invoke-virtual {v14, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_6
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    .line 482
    .local v7, "toPid":I
    move-object/from16 v18, v9

    .end local v9    # "fileBinderReader":Ljava/io/File;
    .local v18, "fileBinderReader":Ljava/io/File;
    const/4 v9, 0x4

    :try_start_9
    invoke-virtual {v14, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 483
    .local v9, "toTid":I
    move-object/from16 v19, v12

    .end local v12    # "pattern":Ljava/util/regex/Pattern;
    .local v19, "pattern":Ljava/util/regex/Pattern;
    const/4 v12, 0x5

    invoke-virtual {v14, v12}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    .line 484
    .local v12, "code":I
    const-wide/16 v20, 0x0

    .line 485
    .local v20, "waitTime":D
    sget-object v22, Lcom/android/server/ScoutHelper;->supportNewBinderLog:Ljava/lang/Boolean;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v22
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_5
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    move-object/from16 v23, v10

    .end local v10    # "BinderReader":Ljava/io/BufferedReader;
    .local v23, "BinderReader":Ljava/io/BufferedReader;
    const/4 v10, 0x6

    if-eqz v22, :cond_5

    .line 486
    :try_start_a
    invoke-virtual {v14, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v10

    .line 487
    .local v10, "elapsed":Ljava/lang/Double;
    invoke-virtual {v10}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v24
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    const-wide v26, 0x408f400000000000L    # 1000.0

    div-double v24, v24, v26

    .line 488
    .end local v10    # "elapsed":Ljava/lang/Double;
    .end local v20    # "waitTime":D
    .local v24, "waitTime":D
    move-object v10, v13

    move-object/from16 v20, v14

    move-wide/from16 v13, v24

    goto :goto_3

    .line 508
    .end local v7    # "toPid":I
    .end local v9    # "toTid":I
    .end local v12    # "code":I
    .end local v14    # "matcher":Ljava/util/regex/Matcher;
    .end local v15    # "fromTid":I
    .end local v16    # "binderRegexPattern":Ljava/lang/String;
    .end local v19    # "pattern":Ljava/util/regex/Pattern;
    .end local v24    # "waitTime":D
    :catch_1
    move-exception v0

    move-object/from16 v21, v13

    move/from16 v7, v17

    goto/16 :goto_6

    .line 489
    .restart local v7    # "toPid":I
    .restart local v9    # "toTid":I
    .restart local v12    # "code":I
    .restart local v14    # "matcher":Ljava/util/regex/Matcher;
    .restart local v15    # "fromTid":I
    .restart local v16    # "binderRegexPattern":Ljava/lang/String;
    .restart local v19    # "pattern":Ljava/util/regex/Pattern;
    .restart local v20    # "waitTime":D
    :cond_5
    :try_start_b
    invoke-virtual {v14, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v24
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_4
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .end local v20    # "waitTime":D
    .restart local v24    # "waitTime":D
    move-object v10, v13

    move-object/from16 v20, v14

    move-wide/from16 v13, v24

    .line 491
    .end local v14    # "matcher":Ljava/util/regex/Matcher;
    .end local v24    # "waitTime":D
    .local v10, "binderTransactionInfo":Ljava/lang/String;
    .local v13, "waitTime":D
    .local v20, "matcher":Ljava/util/regex/Matcher;
    :goto_3
    move-object/from16 v21, v10

    .end local v10    # "binderTransactionInfo":Ljava/lang/String;
    .local v21, "binderTransactionInfo":Ljava/lang/String;
    :try_start_c
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    :try_start_d
    const-string v4, "pending async transaction from "

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 492
    invoke-static {v11}, Lcom/android/server/ScoutHelper;->getProcessCmdline(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 493
    invoke-static {v11, v15}, Lcom/android/server/ScoutHelper;->getProcessComm(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v10, ") to "

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 494
    invoke-static {v7}, Lcom/android/server/ScoutHelper;->getProcessCmdline(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 495
    invoke-static {v7, v9}, Lcom/android/server/ScoutHelper;->getProcessComm(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ") elapsed: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v13, v14}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " s code: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 498
    .local v0, "binderLog":Ljava/lang/String;
    if-ne v11, v1, :cond_7

    if-ne v7, v2, :cond_7

    .line 499
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Binder Info:"

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v8, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    invoke-virtual {v3, v0}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->addBinderTransInfo(Ljava/lang/String;)V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_2
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 501
    const-wide/high16 v24, 0x4000000000000000L    # 2.0

    cmpl-double v4, v13, v24

    if-lez v4, :cond_6

    if-ne v11, v15, :cond_6

    .line 502
    const/4 v4, 0x1

    move/from16 v17, v4

    .line 504
    :cond_6
    move-object/from16 v4, p3

    :try_start_e
    invoke-static {v8, v7, v4, v5}, Lcom/android/server/ScoutHelper;->addPidtoList(Ljava/lang/String;ILjava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 505
    invoke-static {v7, v3, v4, v5}, Lcom/android/server/ScoutHelper;->checkBinderCallPidList(ILcom/android/server/ScoutHelper$ScoutBinderInfo;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_3
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    goto :goto_4

    .line 498
    :cond_7
    move-object/from16 v4, p3

    .line 510
    .end local v0    # "binderLog":Ljava/lang/String;
    .end local v7    # "toPid":I
    .end local v9    # "toTid":I
    .end local v12    # "code":I
    .end local v13    # "waitTime":D
    .end local v15    # "fromTid":I
    .end local v16    # "binderRegexPattern":Ljava/lang/String;
    .end local v19    # "pattern":Ljava/util/regex/Pattern;
    .end local v20    # "matcher":Ljava/util/regex/Matcher;
    :cond_8
    :goto_4
    move/from16 v7, v17

    .end local v17    # "isBlockSystem":Z
    .local v7, "isBlockSystem":Z
    move-object/from16 v9, v18

    move-object/from16 v0, v21

    move-object/from16 v10, v23

    goto/16 :goto_0

    .line 463
    .end local v7    # "isBlockSystem":Z
    .end local v11    # "fromPid":I
    .end local v21    # "binderTransactionInfo":Ljava/lang/String;
    .restart local v17    # "isBlockSystem":Z
    :catchall_1
    move-exception v0

    move-object/from16 v4, p3

    goto :goto_5

    .line 508
    .restart local v11    # "fromPid":I
    .restart local v21    # "binderTransactionInfo":Ljava/lang/String;
    :catch_2
    move-exception v0

    move-object/from16 v4, p3

    move/from16 v7, v17

    goto :goto_6

    :catch_3
    move-exception v0

    move/from16 v7, v17

    goto :goto_6

    .line 463
    .end local v11    # "fromPid":I
    .end local v21    # "binderTransactionInfo":Ljava/lang/String;
    :catchall_2
    move-exception v0

    :goto_5
    move-object v6, v0

    move/from16 v7, v17

    goto/16 :goto_7

    .line 508
    .restart local v11    # "fromPid":I
    .local v13, "binderTransactionInfo":Ljava/lang/String;
    :catch_4
    move-exception v0

    move-object/from16 v21, v13

    move/from16 v7, v17

    .end local v13    # "binderTransactionInfo":Ljava/lang/String;
    .restart local v21    # "binderTransactionInfo":Ljava/lang/String;
    goto :goto_6

    .line 463
    .end local v11    # "fromPid":I
    .end local v21    # "binderTransactionInfo":Ljava/lang/String;
    .end local v23    # "BinderReader":Ljava/io/BufferedReader;
    .local v10, "BinderReader":Ljava/io/BufferedReader;
    :catchall_3
    move-exception v0

    move-object/from16 v23, v10

    move-object v6, v0

    move/from16 v7, v17

    .end local v10    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v23    # "BinderReader":Ljava/io/BufferedReader;
    goto/16 :goto_7

    .line 508
    .end local v23    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v10    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v11    # "fromPid":I
    .restart local v13    # "binderTransactionInfo":Ljava/lang/String;
    :catch_5
    move-exception v0

    move-object/from16 v23, v10

    move-object/from16 v21, v13

    move/from16 v7, v17

    .end local v10    # "BinderReader":Ljava/io/BufferedReader;
    .end local v13    # "binderTransactionInfo":Ljava/lang/String;
    .restart local v21    # "binderTransactionInfo":Ljava/lang/String;
    .restart local v23    # "BinderReader":Ljava/io/BufferedReader;
    goto :goto_6

    .line 463
    .end local v11    # "fromPid":I
    .end local v18    # "fileBinderReader":Ljava/io/File;
    .end local v21    # "binderTransactionInfo":Ljava/lang/String;
    .end local v23    # "BinderReader":Ljava/io/BufferedReader;
    .local v9, "fileBinderReader":Ljava/io/File;
    .restart local v10    # "BinderReader":Ljava/io/BufferedReader;
    :catchall_4
    move-exception v0

    move-object/from16 v18, v9

    move-object/from16 v23, v10

    move-object v6, v0

    move/from16 v7, v17

    .end local v9    # "fileBinderReader":Ljava/io/File;
    .end local v10    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v18    # "fileBinderReader":Ljava/io/File;
    .restart local v23    # "BinderReader":Ljava/io/BufferedReader;
    goto :goto_7

    .line 508
    .end local v18    # "fileBinderReader":Ljava/io/File;
    .end local v23    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v9    # "fileBinderReader":Ljava/io/File;
    .restart local v10    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v11    # "fromPid":I
    .restart local v13    # "binderTransactionInfo":Ljava/lang/String;
    :catch_6
    move-exception v0

    move-object/from16 v18, v9

    move-object/from16 v23, v10

    move-object/from16 v21, v13

    move/from16 v7, v17

    .end local v9    # "fileBinderReader":Ljava/io/File;
    .end local v10    # "BinderReader":Ljava/io/BufferedReader;
    .end local v13    # "binderTransactionInfo":Ljava/lang/String;
    .restart local v18    # "fileBinderReader":Ljava/io/File;
    .restart local v21    # "binderTransactionInfo":Ljava/lang/String;
    .restart local v23    # "BinderReader":Ljava/io/BufferedReader;
    goto :goto_6

    .end local v17    # "isBlockSystem":Z
    .end local v18    # "fileBinderReader":Ljava/io/File;
    .end local v21    # "binderTransactionInfo":Ljava/lang/String;
    .end local v23    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v7    # "isBlockSystem":Z
    .restart local v9    # "fileBinderReader":Ljava/io/File;
    .restart local v10    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v13    # "binderTransactionInfo":Ljava/lang/String;
    :catch_7
    move-exception v0

    move/from16 v17, v7

    move-object/from16 v18, v9

    move-object/from16 v23, v10

    move-object/from16 v21, v13

    .line 509
    .end local v9    # "fileBinderReader":Ljava/io/File;
    .end local v10    # "BinderReader":Ljava/io/BufferedReader;
    .end local v13    # "binderTransactionInfo":Ljava/lang/String;
    .local v0, "e":Ljava/lang/Exception;
    .restart local v18    # "fileBinderReader":Ljava/io/File;
    .restart local v21    # "binderTransactionInfo":Ljava/lang/String;
    .restart local v23    # "BinderReader":Ljava/io/BufferedReader;
    :goto_6
    :try_start_f
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "regular match error, String:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v10, v21

    .end local v21    # "binderTransactionInfo":Ljava/lang/String;
    .local v10, "binderTransactionInfo":Ljava/lang/String;
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_5

    .line 510
    move-object v0, v10

    move-object/from16 v9, v18

    move-object/from16 v10, v23

    .end local v0    # "e":Ljava/lang/Exception;
    goto/16 :goto_0

    .line 463
    .end local v10    # "binderTransactionInfo":Ljava/lang/String;
    .end local v11    # "fromPid":I
    :catchall_5
    move-exception v0

    move-object v6, v0

    goto :goto_7

    .line 466
    .end local v18    # "fileBinderReader":Ljava/io/File;
    .end local v23    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v9    # "fileBinderReader":Ljava/io/File;
    .local v10, "BinderReader":Ljava/io/BufferedReader;
    .restart local v11    # "fromPid":I
    .restart local v13    # "binderTransactionInfo":Ljava/lang/String;
    :cond_9
    move/from16 v17, v7

    move-object/from16 v18, v9

    move-object/from16 v23, v10

    move-object v10, v13

    .line 513
    .end local v7    # "isBlockSystem":Z
    .end local v9    # "fileBinderReader":Ljava/io/File;
    .end local v10    # "BinderReader":Ljava/io/BufferedReader;
    .end local v11    # "fromPid":I
    .end local v13    # "binderTransactionInfo":Ljava/lang/String;
    .restart local v17    # "isBlockSystem":Z
    .restart local v18    # "fileBinderReader":Ljava/io/File;
    .restart local v23    # "BinderReader":Ljava/io/BufferedReader;
    :try_start_10
    invoke-virtual/range {v23 .. v23}, Ljava/io/BufferedReader;->close()V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_8

    .line 516
    .end local v23    # "BinderReader":Ljava/io/BufferedReader;
    move/from16 v7, v17

    goto :goto_a

    .line 513
    :catch_8
    move-exception v0

    move/from16 v7, v17

    goto :goto_9

    .line 463
    .end local v17    # "isBlockSystem":Z
    .end local v18    # "fileBinderReader":Ljava/io/File;
    .restart local v7    # "isBlockSystem":Z
    .restart local v9    # "fileBinderReader":Ljava/io/File;
    .restart local v10    # "BinderReader":Ljava/io/BufferedReader;
    :catchall_6
    move-exception v0

    move/from16 v17, v7

    move-object/from16 v18, v9

    move-object/from16 v23, v10

    move-object v6, v0

    .end local v9    # "fileBinderReader":Ljava/io/File;
    .end local v10    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v18    # "fileBinderReader":Ljava/io/File;
    .restart local v23    # "BinderReader":Ljava/io/BufferedReader;
    :goto_7
    :try_start_11
    invoke-virtual/range {v23 .. v23}, Ljava/io/BufferedReader;->close()V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_7

    goto :goto_8

    :catchall_7
    move-exception v0

    move-object v9, v0

    :try_start_12
    invoke-virtual {v6, v9}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v7    # "isBlockSystem":Z
    .end local v8    # "tag":Ljava/lang/String;
    .end local v18    # "fileBinderReader":Ljava/io/File;
    .end local p0    # "anrFromPid":I
    .end local p1    # "anrToPid":I
    .end local p2    # "info":Lcom/android/server/ScoutHelper$ScoutBinderInfo;
    .end local p3    # "javaProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local p4    # "nativeProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :goto_8
    throw v6
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_9

    .line 513
    .end local v23    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v7    # "isBlockSystem":Z
    .restart local v8    # "tag":Ljava/lang/String;
    .restart local v18    # "fileBinderReader":Ljava/io/File;
    .restart local p0    # "anrFromPid":I
    .restart local p1    # "anrToPid":I
    .restart local p2    # "info":Lcom/android/server/ScoutHelper$ScoutBinderInfo;
    .restart local p3    # "javaProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local p4    # "nativeProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :catch_9
    move-exception v0

    goto :goto_9

    .end local v18    # "fileBinderReader":Ljava/io/File;
    .restart local v9    # "fileBinderReader":Ljava/io/File;
    :catch_a
    move-exception v0

    move-object/from16 v18, v9

    .line 514
    .end local v9    # "fileBinderReader":Ljava/io/File;
    .restart local v0    # "e":Ljava/lang/Exception;
    .restart local v18    # "fileBinderReader":Ljava/io/File;
    :goto_9
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 515
    const-string v6, "Read binder Proc async transaction Error:"

    invoke-static {v8, v6, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 517
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_a
    return v7
.end method

.method public static checkBinderCallPidList(ILcom/android/server/ScoutHelper$ScoutBinderInfo;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
    .locals 38
    .param p0, "Pid"    # I
    .param p1, "info"    # Lcom/android/server/ScoutHelper$ScoutBinderInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/android/server/ScoutHelper$ScoutBinderInfo;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    .line 522
    .local p2, "javaProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .local p3, "nativeProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    move/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    const-string v5, "):"

    const/4 v6, 0x0

    .line 523
    .local v6, "isBlockSystem":Z
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->getTag()Ljava/lang/String;

    move-result-object v7

    .line 524
    .local v7, "tag":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->getCallType()I

    move-result v8

    .line 525
    .local v8, "callType":I
    const/4 v0, 0x0

    const/4 v9, 0x1

    if-ne v8, v9, :cond_0

    sget-boolean v10, Lcom/android/server/ScoutHelper;->SYSRQ_ANR_D_THREAD:Z

    if-eqz v10, :cond_0

    move v10, v9

    goto :goto_0

    :cond_0
    move v10, v0

    .line 526
    .local v10, "isCheckAppDThread":Z
    :goto_0
    if-nez v8, :cond_1

    move v0, v9

    :cond_1
    move v11, v0

    .line 527
    .local v11, "isCheckSystemDThread":Z
    if-nez v10, :cond_2

    if-eqz v11, :cond_3

    :cond_2
    if-eqz v2, :cond_3

    .line 528
    invoke-static {v7, v1, v2}, Lcom/android/server/ScoutHelper;->CheckDState(Ljava/lang/String;ILcom/android/server/ScoutHelper$ScoutBinderInfo;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 529
    invoke-virtual {v2, v9}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->setDThreadState(Z)V

    .line 531
    :cond_3
    invoke-static {v7, v1}, Lcom/android/server/ScoutHelper;->getBinderLogFile(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v12

    .line 532
    .local v12, "fileBinderReader":Ljava/io/File;
    if-nez v12, :cond_4

    .line 533
    return v6

    .line 536
    :cond_4
    :try_start_0
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v13, Ljava/io/FileReader;

    invoke-direct {v13, v12}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v13}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_d

    move-object v13, v0

    .line 537
    .local v13, "BinderReader":Ljava/io/BufferedReader;
    const/4 v0, 0x0

    .line 538
    .local v0, "binderTransactionInfo":Ljava/lang/String;
    const/4 v14, 0x0

    .line 539
    .local v14, "fromPid":I
    :goto_1
    :try_start_1
    invoke-virtual {v13}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v16, v15

    .end local v0    # "binderTransactionInfo":Ljava/lang/String;
    .local v16, "binderTransactionInfo":Ljava/lang/String;
    if-eqz v15, :cond_14

    .line 540
    const-string v0, "MIUI    outgoing transaction"

    move-object/from16 v15, v16

    .end local v16    # "binderTransactionInfo":Ljava/lang/String;
    .local v15, "binderTransactionInfo":Ljava/lang/String;
    invoke-virtual {v15, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_9

    if-nez v0, :cond_6

    :try_start_2
    const-string v0, "outgoing transaction"

    .line 541
    invoke-virtual {v15, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/android/server/ScoutHelper;->supportNewBinderLog:Ljava/lang/Boolean;

    .line 542
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "    outgoing transaction"

    invoke-virtual {v15, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v0, :cond_5

    goto :goto_2

    :cond_5
    move/from16 v17, v6

    goto/16 :goto_4

    .line 536
    .end local v14    # "fromPid":I
    .end local v15    # "binderTransactionInfo":Ljava/lang/String;
    :catchall_0
    move-exception v0

    move-object v1, v0

    move/from16 v18, v8

    move/from16 v28, v10

    move/from16 v25, v11

    move-object/from16 v21, v12

    move-object/from16 v23, v13

    goto/16 :goto_a

    .line 544
    .restart local v14    # "fromPid":I
    .restart local v15    # "binderTransactionInfo":Ljava/lang/String;
    :cond_6
    :goto_2
    :try_start_3
    const-string v0, "\\bfrom((?:\\s+)?\\d+):((?:\\s+)?\\d+)\\s+to((?:\\s+)?\\d+):((?:\\s+)?\\d+)+.*code:((?:\\s+)?\\d+).*duration:((?:\\s+)?(\\d+(?:\\.\\d+)?))\\b"

    .line 545
    .local v0, "binderRegexPattern":Ljava/lang/String;
    sget-object v16, Lcom/android/server/ScoutHelper;->supportNewBinderLog:Ljava/lang/Boolean;

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v16
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_a
    .catchall {:try_start_3 .. :try_end_3} :catchall_9

    if-eqz v16, :cond_7

    :try_start_4
    const-string v16, "\\bfrom((?:\\s+)?\\d+):((?:\\s+)?\\d+)\\s+to((?:\\s+)?\\d+):((?:\\s+)?\\d+)+.*code((?:\\s+)?\\d+)+.*elapsed((?:\\s+)?\\d+)ms+.*"
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-object/from16 v0, v16

    goto :goto_3

    .line 614
    .end local v0    # "binderRegexPattern":Ljava/lang/String;
    :catch_0
    move-exception v0

    move-object/from16 v30, v5

    move/from16 v18, v8

    move/from16 v28, v10

    move/from16 v25, v11

    move-object/from16 v21, v12

    move-object/from16 v23, v13

    move-object/from16 v32, v15

    goto/16 :goto_9

    .line 546
    .restart local v0    # "binderRegexPattern":Ljava/lang/String;
    :cond_7
    :goto_3
    :try_start_5
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v16

    move-object/from16 v17, v16

    .line 547
    .local v17, "pattern":Ljava/util/regex/Pattern;
    move-object/from16 v9, v17

    .end local v17    # "pattern":Ljava/util/regex/Pattern;
    .local v9, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v9, v15}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v17

    move-object/from16 v18, v17

    .line 548
    .local v18, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual/range {v18 .. v18}, Ljava/util/regex/Matcher;->find()Z

    move-result v17
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_a
    .catchall {:try_start_5 .. :try_end_5} :catchall_9

    move-object/from16 v19, v0

    .end local v0    # "binderRegexPattern":Ljava/lang/String;
    .local v19, "binderRegexPattern":Ljava/lang/String;
    const-string v0, "("

    if-nez v17, :cond_8

    .line 549
    move/from16 v17, v6

    .end local v6    # "isBlockSystem":Z
    .local v17, "isBlockSystem":Z
    :try_start_6
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, ") regex match failed"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 550
    nop

    .line 539
    .end local v9    # "pattern":Ljava/util/regex/Pattern;
    .end local v17    # "isBlockSystem":Z
    .end local v18    # "matcher":Ljava/util/regex/Matcher;
    .end local v19    # "binderRegexPattern":Ljava/lang/String;
    .restart local v6    # "isBlockSystem":Z
    :goto_4
    move-object v0, v15

    move/from16 v6, v17

    const/4 v9, 0x1

    .end local v6    # "isBlockSystem":Z
    .restart local v17    # "isBlockSystem":Z
    goto/16 :goto_1

    .line 536
    .end local v14    # "fromPid":I
    .end local v15    # "binderTransactionInfo":Ljava/lang/String;
    :catchall_1
    move-exception v0

    move-object v1, v0

    move/from16 v18, v8

    move/from16 v28, v10

    move/from16 v25, v11

    move-object/from16 v21, v12

    move-object/from16 v23, v13

    move/from16 v6, v17

    goto/16 :goto_a

    .line 614
    .restart local v14    # "fromPid":I
    .restart local v15    # "binderTransactionInfo":Ljava/lang/String;
    :catch_1
    move-exception v0

    move-object/from16 v30, v5

    move/from16 v18, v8

    move/from16 v28, v10

    move/from16 v25, v11

    move-object/from16 v21, v12

    move-object/from16 v23, v13

    move-object/from16 v32, v15

    move/from16 v6, v17

    goto/16 :goto_9

    .line 552
    .end local v17    # "isBlockSystem":Z
    .restart local v6    # "isBlockSystem":Z
    .restart local v9    # "pattern":Ljava/util/regex/Pattern;
    .restart local v18    # "matcher":Ljava/util/regex/Matcher;
    .restart local v19    # "binderRegexPattern":Ljava/lang/String;
    :cond_8
    move/from16 v17, v6

    .end local v6    # "isBlockSystem":Z
    .restart local v17    # "isBlockSystem":Z
    move-object/from16 v6, v18

    move/from16 v18, v8

    const/4 v8, 0x1

    .end local v8    # "callType":I
    .local v6, "matcher":Ljava/util/regex/Matcher;
    .local v18, "callType":I
    :try_start_7
    invoke-virtual {v6, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    move v14, v8

    .line 553
    const/4 v8, 0x2

    invoke-virtual {v6, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    move/from16 v21, v20

    .line 554
    .local v21, "fromTid":I
    const/4 v8, 0x3

    invoke-virtual {v6, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v22

    move/from16 v23, v22

    .line 555
    .local v23, "toPid":I
    const/4 v8, 0x4

    invoke-virtual {v6, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 556
    .local v8, "toTid":I
    move-object/from16 v24, v9

    .end local v9    # "pattern":Ljava/util/regex/Pattern;
    .local v24, "pattern":Ljava/util/regex/Pattern;
    const/4 v9, 0x5

    invoke-virtual {v6, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 557
    .local v9, "code":I
    const-wide/16 v25, 0x0

    .line 558
    .local v25, "waitTime":D
    sget-object v27, Lcom/android/server/ScoutHelper;->supportNewBinderLog:Ljava/lang/Boolean;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v27
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_9
    .catchall {:try_start_7 .. :try_end_7} :catchall_7

    move/from16 v28, v10

    .end local v10    # "isCheckAppDThread":Z
    .local v28, "isCheckAppDThread":Z
    const/4 v10, 0x6

    if-eqz v27, :cond_9

    .line 559
    :try_start_8
    invoke-virtual {v6, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v10

    .line 560
    .local v10, "elapsed":Ljava/lang/Double;
    invoke-virtual {v10}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v29
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    const-wide v31, 0x408f400000000000L    # 1000.0

    div-double v29, v29, v31

    .line 561
    .end local v10    # "elapsed":Ljava/lang/Double;
    .end local v25    # "waitTime":D
    .local v29, "waitTime":D
    move/from16 v25, v11

    move-wide/from16 v10, v29

    goto :goto_5

    .line 536
    .end local v6    # "matcher":Ljava/util/regex/Matcher;
    .end local v8    # "toTid":I
    .end local v9    # "code":I
    .end local v14    # "fromPid":I
    .end local v15    # "binderTransactionInfo":Ljava/lang/String;
    .end local v19    # "binderRegexPattern":Ljava/lang/String;
    .end local v21    # "fromTid":I
    .end local v23    # "toPid":I
    .end local v24    # "pattern":Ljava/util/regex/Pattern;
    .end local v29    # "waitTime":D
    :catchall_2
    move-exception v0

    move-object v1, v0

    move/from16 v25, v11

    move-object/from16 v21, v12

    move-object/from16 v23, v13

    move/from16 v6, v17

    goto/16 :goto_a

    .line 614
    .restart local v14    # "fromPid":I
    .restart local v15    # "binderTransactionInfo":Ljava/lang/String;
    :catch_2
    move-exception v0

    move-object/from16 v30, v5

    move/from16 v25, v11

    move-object/from16 v21, v12

    move-object/from16 v23, v13

    move-object/from16 v32, v15

    move/from16 v6, v17

    goto/16 :goto_9

    .line 562
    .restart local v6    # "matcher":Ljava/util/regex/Matcher;
    .restart local v8    # "toTid":I
    .restart local v9    # "code":I
    .restart local v19    # "binderRegexPattern":Ljava/lang/String;
    .restart local v21    # "fromTid":I
    .restart local v23    # "toPid":I
    .restart local v24    # "pattern":Ljava/util/regex/Pattern;
    .restart local v25    # "waitTime":D
    :cond_9
    :try_start_9
    invoke-virtual {v6, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v29
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_8
    .catchall {:try_start_9 .. :try_end_9} :catchall_6

    .end local v25    # "waitTime":D
    .restart local v29    # "waitTime":D
    move/from16 v25, v11

    move-wide/from16 v10, v29

    .line 564
    .end local v11    # "isCheckSystemDThread":Z
    .end local v29    # "waitTime":D
    .local v10, "waitTime":D
    .local v25, "isCheckSystemDThread":Z
    :goto_5
    :try_start_a
    invoke-static {v14}, Lcom/android/server/ScoutHelper;->getProcessCmdline(I)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v27, v26

    .line 565
    .local v27, "fromProcessName":Ljava/lang/String;
    move-object/from16 v26, v6

    move/from16 v6, v21

    .end local v21    # "fromTid":I
    .local v6, "fromTid":I
    .local v26, "matcher":Ljava/util/regex/Matcher;
    invoke-static {v14, v6}, Lcom/android/server/ScoutHelper;->getProcessComm(II)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v29, v21

    .line 566
    .local v29, "fromThreadName":Ljava/lang/String;
    invoke-static/range {v23 .. v23}, Lcom/android/server/ScoutHelper;->getProcessCmdline(I)Ljava/lang/String;

    move-result-object v21
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_7
    .catchall {:try_start_a .. :try_end_a} :catchall_5

    move-object/from16 v30, v21

    .line 567
    .local v30, "toProcess":Ljava/lang/String;
    move-object/from16 v21, v12

    move/from16 v12, v23

    .end local v23    # "toPid":I
    .local v12, "toPid":I
    .local v21, "fileBinderReader":Ljava/io/File;
    :try_start_b
    invoke-static {v12, v8}, Lcom/android/server/ScoutHelper;->getProcessComm(II)Ljava/lang/String;

    move-result-object v23
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_6
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    move-object/from16 v31, v23

    .line 568
    .local v31, "toThreadName":Ljava/lang/String;
    move-object/from16 v23, v13

    .end local v13    # "BinderReader":Ljava/io/BufferedReader;
    .local v23, "BinderReader":Ljava/io/BufferedReader;
    :try_start_c
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_5
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    move-object/from16 v32, v15

    .end local v15    # "binderTransactionInfo":Ljava/lang/String;
    .local v32, "binderTransactionInfo":Ljava/lang/String;
    :try_start_d
    const-string v15, "outgoing transaction from "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v15, v27

    .end local v27    # "fromProcessName":Ljava/lang/String;
    .local v15, "fromProcessName":Ljava/lang/String;
    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v27, v15

    move-object/from16 v15, v29

    .end local v29    # "fromThreadName":Ljava/lang/String;
    .local v15, "fromThreadName":Ljava/lang/String;
    .restart local v27    # "fromProcessName":Ljava/lang/String;
    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v29, v15

    .end local v15    # "fromThreadName":Ljava/lang/String;
    .restart local v29    # "fromThreadName":Ljava/lang/String;
    const-string v15, ") to "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v15, v30

    .end local v30    # "toProcess":Ljava/lang/String;
    .local v15, "toProcess":Ljava/lang/String;
    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_4
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    move-object/from16 v30, v5

    move-object/from16 v5, v31

    .end local v31    # "toThreadName":Ljava/lang/String;
    .local v5, "toThreadName":Ljava/lang/String;
    :try_start_e
    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v31, v5

    .end local v5    # "toThreadName":Ljava/lang/String;
    .restart local v31    # "toThreadName":Ljava/lang/String;
    const-string v5, ") elapsed: "

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v13, " s code: "

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 575
    .local v5, "binderLog":Ljava/lang/String;
    if-ne v14, v1, :cond_12

    if-lez v12, :cond_12

    if-eq v12, v1, :cond_12

    .line 576
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Binder Info:"

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 577
    invoke-virtual {v2, v5}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->addBinderTransInfo(Ljava/lang/String;)V

    .line 578
    invoke-static {v7, v12}, Lcom/android/server/ScoutHelper;->getOomAdjOfPid(Ljava/lang/String;I)I

    move-result v1

    .line 579
    .local v1, "adj":I
    invoke-static {v1}, Lcom/android/server/ScoutHelper;->checkIsJavaOrNativeProcess(I)I

    move-result v13

    .line 580
    .local v13, "isJavaOrNativeProcess":I
    if-nez v13, :cond_a

    move/from16 v1, p0

    move/from16 v6, v17

    move/from16 v8, v18

    move-object/from16 v12, v21

    move-object/from16 v13, v23

    move/from16 v11, v25

    move/from16 v10, v28

    move-object/from16 v5, v30

    move-object/from16 v0, v32

    const/4 v9, 0x1

    goto/16 :goto_1

    .line 581
    :cond_a
    const-wide/high16 v33, 0x4000000000000000L    # 2.0

    move-object/from16 v35, v5

    const/4 v5, 0x1

    .end local v5    # "binderLog":Ljava/lang/String;
    .local v35, "binderLog":Ljava/lang/String;
    if-ne v13, v5, :cond_c

    .line 582
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_c

    .line 583
    const/16 v0, -0x384

    if-ne v1, v0, :cond_b

    if-ne v14, v6, :cond_b

    cmpl-double v0, v10, v33

    if-lez v0, :cond_b

    .line 585
    const/4 v0, 0x1

    move/from16 v17, v0

    .line 587
    :cond_b
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 588
    invoke-static {v12, v2, v3, v4}, Lcom/android/server/ScoutHelper;->checkBinderCallPidList(ILcom/android/server/ScoutHelper$ScoutBinderInfo;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_3
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    move/from16 v6, v17

    goto/16 :goto_8

    .line 589
    :cond_c
    const-string v5, ")"

    move/from16 v36, v1

    .end local v1    # "adj":I
    .local v36, "adj":I
    const-string v1, "check blocked in monkey, will kill pid: "

    move/from16 v37, v8

    const/4 v8, 0x2

    .end local v8    # "toTid":I
    .local v37, "toTid":I
    if-ne v13, v8, :cond_f

    .line 590
    :try_start_f
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_f

    .line 591
    if-ne v14, v6, :cond_d

    cmpl-double v8, v10, v33

    if-lez v8, :cond_d

    .line 592
    const/4 v8, 0x1

    move/from16 v17, v8

    .line 594
    :cond_d
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v8

    if-ne v8, v14, :cond_e

    invoke-static {v15, v10, v11}, Lcom/android/server/ScoutHelper;->checkIsMonkey(Ljava/lang/String;D)Z

    move-result v8

    if-eqz v8, :cond_e

    .line 595
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 596
    invoke-virtual {v2, v12}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->setMonkeyPid(I)V

    .line 598
    :cond_e
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 599
    invoke-static {v12, v2, v3, v4}, Lcom/android/server/ScoutHelper;->checkBinderCallPidList(ILcom/android/server/ScoutHelper$ScoutBinderInfo;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move/from16 v6, v17

    goto :goto_8

    .line 600
    :cond_f
    const/4 v8, 0x3

    if-ne v13, v8, :cond_13

    .line 601
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_13

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_13

    .line 602
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v8

    if-ne v8, v14, :cond_10

    invoke-static {v15, v10, v11}, Lcom/android/server/ScoutHelper;->checkIsMonkey(Ljava/lang/String;D)Z

    move-result v8

    if-eqz v8, :cond_10

    .line 603
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 604
    invoke-virtual {v2, v12}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->setMonkeyPid(I)V

    .line 606
    :cond_10
    sget-object v0, Lcom/android/server/ScoutHelper;->JAVA_SHELL_PROCESS:Ljava/util/List;

    invoke-interface {v0, v15}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 607
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 609
    :cond_11
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 611
    :goto_6
    invoke-static {v12, v2, v3, v4}, Lcom/android/server/ScoutHelper;->checkBinderCallPidList(ILcom/android/server/ScoutHelper$ScoutBinderInfo;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_3
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    goto :goto_7

    .line 575
    .end local v13    # "isJavaOrNativeProcess":I
    .end local v35    # "binderLog":Ljava/lang/String;
    .end local v36    # "adj":I
    .end local v37    # "toTid":I
    .restart local v5    # "binderLog":Ljava/lang/String;
    .restart local v8    # "toTid":I
    :cond_12
    move-object/from16 v35, v5

    move/from16 v37, v8

    .line 616
    .end local v5    # "binderLog":Ljava/lang/String;
    .end local v6    # "fromTid":I
    .end local v8    # "toTid":I
    .end local v9    # "code":I
    .end local v10    # "waitTime":D
    .end local v12    # "toPid":I
    .end local v15    # "toProcess":Ljava/lang/String;
    .end local v19    # "binderRegexPattern":Ljava/lang/String;
    .end local v24    # "pattern":Ljava/util/regex/Pattern;
    .end local v26    # "matcher":Ljava/util/regex/Matcher;
    .end local v27    # "fromProcessName":Ljava/lang/String;
    .end local v29    # "fromThreadName":Ljava/lang/String;
    .end local v31    # "toThreadName":Ljava/lang/String;
    :cond_13
    :goto_7
    move/from16 v6, v17

    .end local v17    # "isBlockSystem":Z
    .local v6, "isBlockSystem":Z
    :goto_8
    move/from16 v1, p0

    move/from16 v8, v18

    move-object/from16 v12, v21

    move-object/from16 v13, v23

    move/from16 v11, v25

    move/from16 v10, v28

    move-object/from16 v5, v30

    move-object/from16 v0, v32

    const/4 v9, 0x1

    goto/16 :goto_1

    .line 614
    .end local v6    # "isBlockSystem":Z
    .restart local v17    # "isBlockSystem":Z
    :catch_3
    move-exception v0

    move/from16 v6, v17

    goto/16 :goto_9

    :catch_4
    move-exception v0

    move-object/from16 v30, v5

    move/from16 v6, v17

    goto/16 :goto_9

    .line 536
    .end local v14    # "fromPid":I
    .end local v32    # "binderTransactionInfo":Ljava/lang/String;
    :catchall_3
    move-exception v0

    move-object v1, v0

    move/from16 v6, v17

    goto/16 :goto_a

    .line 614
    .restart local v14    # "fromPid":I
    .local v15, "binderTransactionInfo":Ljava/lang/String;
    :catch_5
    move-exception v0

    move-object/from16 v30, v5

    move-object/from16 v32, v15

    move/from16 v6, v17

    .end local v15    # "binderTransactionInfo":Ljava/lang/String;
    .restart local v32    # "binderTransactionInfo":Ljava/lang/String;
    goto/16 :goto_9

    .line 536
    .end local v14    # "fromPid":I
    .end local v23    # "BinderReader":Ljava/io/BufferedReader;
    .end local v32    # "binderTransactionInfo":Ljava/lang/String;
    .local v13, "BinderReader":Ljava/io/BufferedReader;
    :catchall_4
    move-exception v0

    move-object/from16 v23, v13

    move-object v1, v0

    move/from16 v6, v17

    .end local v13    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v23    # "BinderReader":Ljava/io/BufferedReader;
    goto/16 :goto_a

    .line 614
    .end local v23    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v13    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v14    # "fromPid":I
    .restart local v15    # "binderTransactionInfo":Ljava/lang/String;
    :catch_6
    move-exception v0

    move-object/from16 v30, v5

    move-object/from16 v23, v13

    move-object/from16 v32, v15

    move/from16 v6, v17

    .end local v13    # "BinderReader":Ljava/io/BufferedReader;
    .end local v15    # "binderTransactionInfo":Ljava/lang/String;
    .restart local v23    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v32    # "binderTransactionInfo":Ljava/lang/String;
    goto/16 :goto_9

    .line 536
    .end local v14    # "fromPid":I
    .end local v21    # "fileBinderReader":Ljava/io/File;
    .end local v23    # "BinderReader":Ljava/io/BufferedReader;
    .end local v32    # "binderTransactionInfo":Ljava/lang/String;
    .local v12, "fileBinderReader":Ljava/io/File;
    .restart local v13    # "BinderReader":Ljava/io/BufferedReader;
    :catchall_5
    move-exception v0

    move-object/from16 v21, v12

    move-object/from16 v23, v13

    move-object v1, v0

    move/from16 v6, v17

    .end local v12    # "fileBinderReader":Ljava/io/File;
    .end local v13    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v21    # "fileBinderReader":Ljava/io/File;
    .restart local v23    # "BinderReader":Ljava/io/BufferedReader;
    goto/16 :goto_a

    .line 614
    .end local v21    # "fileBinderReader":Ljava/io/File;
    .end local v23    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v12    # "fileBinderReader":Ljava/io/File;
    .restart local v13    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v14    # "fromPid":I
    .restart local v15    # "binderTransactionInfo":Ljava/lang/String;
    :catch_7
    move-exception v0

    move-object/from16 v30, v5

    move-object/from16 v21, v12

    move-object/from16 v23, v13

    move-object/from16 v32, v15

    move/from16 v6, v17

    .end local v12    # "fileBinderReader":Ljava/io/File;
    .end local v13    # "BinderReader":Ljava/io/BufferedReader;
    .end local v15    # "binderTransactionInfo":Ljava/lang/String;
    .restart local v21    # "fileBinderReader":Ljava/io/File;
    .restart local v23    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v32    # "binderTransactionInfo":Ljava/lang/String;
    goto :goto_9

    .line 536
    .end local v14    # "fromPid":I
    .end local v21    # "fileBinderReader":Ljava/io/File;
    .end local v23    # "BinderReader":Ljava/io/BufferedReader;
    .end local v25    # "isCheckSystemDThread":Z
    .end local v32    # "binderTransactionInfo":Ljava/lang/String;
    .restart local v11    # "isCheckSystemDThread":Z
    .restart local v12    # "fileBinderReader":Ljava/io/File;
    .restart local v13    # "BinderReader":Ljava/io/BufferedReader;
    :catchall_6
    move-exception v0

    move/from16 v25, v11

    move-object/from16 v21, v12

    move-object/from16 v23, v13

    move-object v1, v0

    move/from16 v6, v17

    .end local v11    # "isCheckSystemDThread":Z
    .end local v12    # "fileBinderReader":Ljava/io/File;
    .end local v13    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v21    # "fileBinderReader":Ljava/io/File;
    .restart local v23    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v25    # "isCheckSystemDThread":Z
    goto/16 :goto_a

    .line 614
    .end local v21    # "fileBinderReader":Ljava/io/File;
    .end local v23    # "BinderReader":Ljava/io/BufferedReader;
    .end local v25    # "isCheckSystemDThread":Z
    .restart local v11    # "isCheckSystemDThread":Z
    .restart local v12    # "fileBinderReader":Ljava/io/File;
    .restart local v13    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v14    # "fromPid":I
    .restart local v15    # "binderTransactionInfo":Ljava/lang/String;
    :catch_8
    move-exception v0

    move-object/from16 v30, v5

    move/from16 v25, v11

    move-object/from16 v21, v12

    move-object/from16 v23, v13

    move-object/from16 v32, v15

    move/from16 v6, v17

    .end local v11    # "isCheckSystemDThread":Z
    .end local v12    # "fileBinderReader":Ljava/io/File;
    .end local v13    # "BinderReader":Ljava/io/BufferedReader;
    .end local v15    # "binderTransactionInfo":Ljava/lang/String;
    .restart local v21    # "fileBinderReader":Ljava/io/File;
    .restart local v23    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v25    # "isCheckSystemDThread":Z
    .restart local v32    # "binderTransactionInfo":Ljava/lang/String;
    goto :goto_9

    .line 536
    .end local v14    # "fromPid":I
    .end local v21    # "fileBinderReader":Ljava/io/File;
    .end local v23    # "BinderReader":Ljava/io/BufferedReader;
    .end local v25    # "isCheckSystemDThread":Z
    .end local v28    # "isCheckAppDThread":Z
    .end local v32    # "binderTransactionInfo":Ljava/lang/String;
    .local v10, "isCheckAppDThread":Z
    .restart local v11    # "isCheckSystemDThread":Z
    .restart local v12    # "fileBinderReader":Ljava/io/File;
    .restart local v13    # "BinderReader":Ljava/io/BufferedReader;
    :catchall_7
    move-exception v0

    move/from16 v28, v10

    move/from16 v25, v11

    move-object/from16 v21, v12

    move-object/from16 v23, v13

    move-object v1, v0

    move/from16 v6, v17

    .end local v10    # "isCheckAppDThread":Z
    .end local v11    # "isCheckSystemDThread":Z
    .end local v12    # "fileBinderReader":Ljava/io/File;
    .end local v13    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v21    # "fileBinderReader":Ljava/io/File;
    .restart local v23    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v25    # "isCheckSystemDThread":Z
    .restart local v28    # "isCheckAppDThread":Z
    goto/16 :goto_a

    .line 614
    .end local v21    # "fileBinderReader":Ljava/io/File;
    .end local v23    # "BinderReader":Ljava/io/BufferedReader;
    .end local v25    # "isCheckSystemDThread":Z
    .end local v28    # "isCheckAppDThread":Z
    .restart local v10    # "isCheckAppDThread":Z
    .restart local v11    # "isCheckSystemDThread":Z
    .restart local v12    # "fileBinderReader":Ljava/io/File;
    .restart local v13    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v14    # "fromPid":I
    .restart local v15    # "binderTransactionInfo":Ljava/lang/String;
    :catch_9
    move-exception v0

    move-object/from16 v30, v5

    move/from16 v28, v10

    move/from16 v25, v11

    move-object/from16 v21, v12

    move-object/from16 v23, v13

    move-object/from16 v32, v15

    move/from16 v6, v17

    .end local v10    # "isCheckAppDThread":Z
    .end local v11    # "isCheckSystemDThread":Z
    .end local v12    # "fileBinderReader":Ljava/io/File;
    .end local v13    # "BinderReader":Ljava/io/BufferedReader;
    .end local v15    # "binderTransactionInfo":Ljava/lang/String;
    .restart local v21    # "fileBinderReader":Ljava/io/File;
    .restart local v23    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v25    # "isCheckSystemDThread":Z
    .restart local v28    # "isCheckAppDThread":Z
    .restart local v32    # "binderTransactionInfo":Ljava/lang/String;
    goto :goto_9

    .end local v17    # "isBlockSystem":Z
    .end local v18    # "callType":I
    .end local v21    # "fileBinderReader":Ljava/io/File;
    .end local v23    # "BinderReader":Ljava/io/BufferedReader;
    .end local v25    # "isCheckSystemDThread":Z
    .end local v28    # "isCheckAppDThread":Z
    .end local v32    # "binderTransactionInfo":Ljava/lang/String;
    .restart local v6    # "isBlockSystem":Z
    .local v8, "callType":I
    .restart local v10    # "isCheckAppDThread":Z
    .restart local v11    # "isCheckSystemDThread":Z
    .restart local v12    # "fileBinderReader":Ljava/io/File;
    .restart local v13    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v15    # "binderTransactionInfo":Ljava/lang/String;
    :catch_a
    move-exception v0

    move-object/from16 v30, v5

    move/from16 v17, v6

    move/from16 v18, v8

    move/from16 v28, v10

    move/from16 v25, v11

    move-object/from16 v21, v12

    move-object/from16 v23, v13

    move-object/from16 v32, v15

    .line 615
    .end local v8    # "callType":I
    .end local v10    # "isCheckAppDThread":Z
    .end local v11    # "isCheckSystemDThread":Z
    .end local v12    # "fileBinderReader":Ljava/io/File;
    .end local v13    # "BinderReader":Ljava/io/BufferedReader;
    .end local v15    # "binderTransactionInfo":Ljava/lang/String;
    .local v0, "e":Ljava/lang/Exception;
    .restart local v18    # "callType":I
    .restart local v21    # "fileBinderReader":Ljava/io/File;
    .restart local v23    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v25    # "isCheckSystemDThread":Z
    .restart local v28    # "isCheckAppDThread":Z
    .restart local v32    # "binderTransactionInfo":Ljava/lang/String;
    :goto_9
    :try_start_10
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "regular match error, String:"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v5, v32

    .end local v32    # "binderTransactionInfo":Ljava/lang/String;
    .local v5, "binderTransactionInfo":Ljava/lang/String;
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_8

    .line 616
    move/from16 v1, p0

    move-object v0, v5

    move/from16 v8, v18

    move-object/from16 v12, v21

    move-object/from16 v13, v23

    move/from16 v11, v25

    move/from16 v10, v28

    move-object/from16 v5, v30

    const/4 v9, 0x1

    .end local v0    # "e":Ljava/lang/Exception;
    goto/16 :goto_1

    .line 536
    .end local v5    # "binderTransactionInfo":Ljava/lang/String;
    .end local v14    # "fromPid":I
    :catchall_8
    move-exception v0

    move-object v1, v0

    goto :goto_a

    .line 539
    .end local v18    # "callType":I
    .end local v21    # "fileBinderReader":Ljava/io/File;
    .end local v23    # "BinderReader":Ljava/io/BufferedReader;
    .end local v25    # "isCheckSystemDThread":Z
    .end local v28    # "isCheckAppDThread":Z
    .restart local v8    # "callType":I
    .restart local v10    # "isCheckAppDThread":Z
    .restart local v11    # "isCheckSystemDThread":Z
    .restart local v12    # "fileBinderReader":Ljava/io/File;
    .restart local v13    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v14    # "fromPid":I
    .restart local v16    # "binderTransactionInfo":Ljava/lang/String;
    :cond_14
    move/from16 v17, v6

    move/from16 v18, v8

    move/from16 v28, v10

    move/from16 v25, v11

    move-object/from16 v21, v12

    move-object/from16 v23, v13

    move-object/from16 v5, v16

    .line 619
    .end local v6    # "isBlockSystem":Z
    .end local v8    # "callType":I
    .end local v10    # "isCheckAppDThread":Z
    .end local v11    # "isCheckSystemDThread":Z
    .end local v12    # "fileBinderReader":Ljava/io/File;
    .end local v13    # "BinderReader":Ljava/io/BufferedReader;
    .end local v14    # "fromPid":I
    .end local v16    # "binderTransactionInfo":Ljava/lang/String;
    .restart local v17    # "isBlockSystem":Z
    .restart local v18    # "callType":I
    .restart local v21    # "fileBinderReader":Ljava/io/File;
    .restart local v23    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v25    # "isCheckSystemDThread":Z
    .restart local v28    # "isCheckAppDThread":Z
    :try_start_11
    invoke-virtual/range {v23 .. v23}, Ljava/io/BufferedReader;->close()V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_b

    .line 622
    .end local v23    # "BinderReader":Ljava/io/BufferedReader;
    move/from16 v6, v17

    goto :goto_d

    .line 619
    :catch_b
    move-exception v0

    move/from16 v6, v17

    goto :goto_c

    .line 536
    .end local v17    # "isBlockSystem":Z
    .end local v18    # "callType":I
    .end local v21    # "fileBinderReader":Ljava/io/File;
    .end local v25    # "isCheckSystemDThread":Z
    .end local v28    # "isCheckAppDThread":Z
    .restart local v6    # "isBlockSystem":Z
    .restart local v8    # "callType":I
    .restart local v10    # "isCheckAppDThread":Z
    .restart local v11    # "isCheckSystemDThread":Z
    .restart local v12    # "fileBinderReader":Ljava/io/File;
    .restart local v13    # "BinderReader":Ljava/io/BufferedReader;
    :catchall_9
    move-exception v0

    move/from16 v17, v6

    move/from16 v18, v8

    move/from16 v28, v10

    move/from16 v25, v11

    move-object/from16 v21, v12

    move-object/from16 v23, v13

    move-object v1, v0

    .end local v8    # "callType":I
    .end local v10    # "isCheckAppDThread":Z
    .end local v11    # "isCheckSystemDThread":Z
    .end local v12    # "fileBinderReader":Ljava/io/File;
    .end local v13    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v18    # "callType":I
    .restart local v21    # "fileBinderReader":Ljava/io/File;
    .restart local v23    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v25    # "isCheckSystemDThread":Z
    .restart local v28    # "isCheckAppDThread":Z
    :goto_a
    :try_start_12
    invoke-virtual/range {v23 .. v23}, Ljava/io/BufferedReader;->close()V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_a

    goto :goto_b

    :catchall_a
    move-exception v0

    move-object v5, v0

    :try_start_13
    invoke-virtual {v1, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v6    # "isBlockSystem":Z
    .end local v7    # "tag":Ljava/lang/String;
    .end local v18    # "callType":I
    .end local v21    # "fileBinderReader":Ljava/io/File;
    .end local v25    # "isCheckSystemDThread":Z
    .end local v28    # "isCheckAppDThread":Z
    .end local p0    # "Pid":I
    .end local p1    # "info":Lcom/android/server/ScoutHelper$ScoutBinderInfo;
    .end local p2    # "javaProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local p3    # "nativeProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :goto_b
    throw v1
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_c

    .line 619
    .end local v23    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v6    # "isBlockSystem":Z
    .restart local v7    # "tag":Ljava/lang/String;
    .restart local v18    # "callType":I
    .restart local v21    # "fileBinderReader":Ljava/io/File;
    .restart local v25    # "isCheckSystemDThread":Z
    .restart local v28    # "isCheckAppDThread":Z
    .restart local p0    # "Pid":I
    .restart local p1    # "info":Lcom/android/server/ScoutHelper$ScoutBinderInfo;
    .restart local p2    # "javaProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local p3    # "nativeProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :catch_c
    move-exception v0

    goto :goto_c

    .end local v18    # "callType":I
    .end local v21    # "fileBinderReader":Ljava/io/File;
    .end local v25    # "isCheckSystemDThread":Z
    .end local v28    # "isCheckAppDThread":Z
    .restart local v8    # "callType":I
    .restart local v10    # "isCheckAppDThread":Z
    .restart local v11    # "isCheckSystemDThread":Z
    .restart local v12    # "fileBinderReader":Ljava/io/File;
    :catch_d
    move-exception v0

    move/from16 v18, v8

    move/from16 v28, v10

    move/from16 v25, v11

    move-object/from16 v21, v12

    .line 620
    .end local v8    # "callType":I
    .end local v10    # "isCheckAppDThread":Z
    .end local v11    # "isCheckSystemDThread":Z
    .end local v12    # "fileBinderReader":Ljava/io/File;
    .restart local v0    # "e":Ljava/lang/Exception;
    .restart local v18    # "callType":I
    .restart local v21    # "fileBinderReader":Ljava/io/File;
    .restart local v25    # "isCheckSystemDThread":Z
    .restart local v28    # "isCheckAppDThread":Z
    :goto_c
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 621
    const-string v1, "Read binder Proc transaction Error:"

    invoke-static {v7, v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 623
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_d
    return v6
.end method

.method public static checkBinderThreadFull(ILcom/android/server/ScoutHelper$ScoutBinderInfo;Ljava/util/TreeMap;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 25
    .param p0, "Pid"    # I
    .param p1, "info"    # Lcom/android/server/ScoutHelper$ScoutBinderInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/android/server/ScoutHelper$ScoutBinderInfo;",
            "Ljava/util/TreeMap<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 634
    .local p2, "inPidMap":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .local p3, "javaProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .local p4, "nativeProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    move-object/from16 v1, p2

    const-string v2, "):"

    invoke-virtual/range {p1 .. p1}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->getTag()Ljava/lang/String;

    move-result-object v3

    .line 635
    .local v3, "tag":Ljava/lang/String;
    move/from16 v4, p0

    invoke-static {v3, v4}, Lcom/android/server/ScoutHelper;->getBinderLogFile(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v5

    .line 636
    .local v5, "fileBinderReader":Ljava/io/File;
    if-nez v5, :cond_0

    .line 637
    return-void

    .line 639
    :cond_0
    :try_start_0
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/FileReader;

    invoke-direct {v6, v5}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_6

    move-object v6, v0

    .line 640
    .local v6, "BinderReader":Ljava/io/BufferedReader;
    const/4 v0, 0x0

    .line 641
    .local v0, "binderTransactionInfo":Ljava/lang/String;
    const/4 v7, 0x0

    .line 642
    .local v7, "fromPid":I
    :goto_0
    :try_start_1
    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v8

    move-object v9, v8

    .end local v0    # "binderTransactionInfo":Ljava/lang/String;
    .local v9, "binderTransactionInfo":Ljava/lang/String;
    if-eqz v8, :cond_9

    .line 643
    const-string v0, "MIUI    incoming transaction"

    invoke-virtual {v9, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    const-string v8, "incoming transaction"

    if-nez v0, :cond_1

    .line 644
    :try_start_2
    invoke-virtual {v9, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/android/server/ScoutHelper;->supportNewBinderLog:Ljava/lang/Boolean;

    .line 645
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "    incoming transaction"

    invoke-virtual {v9, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v0, :cond_4

    goto :goto_1

    .line 639
    .end local v7    # "fromPid":I
    .end local v9    # "binderTransactionInfo":Ljava/lang/String;
    :catchall_0
    move-exception v0

    move-object/from16 v10, p1

    move-object/from16 v13, p3

    move-object/from16 v1, p4

    move-object v2, v0

    move-object/from16 v17, v5

    goto/16 :goto_a

    .line 647
    .restart local v7    # "fromPid":I
    .restart local v9    # "binderTransactionInfo":Ljava/lang/String;
    :cond_1
    :goto_1
    :try_start_3
    const-string v0, "\\bfrom((?:\\s+)?\\d+):((?:\\s+)?\\d+)\\s+to((?:\\s+)?\\d+):((?:\\s+)?\\d+)+.*code:((?:\\s+)?\\d+).*duration:((?:\\s+)?(\\d+(?:\\.\\d+)?))\\b"

    .line 648
    .local v0, "binderRegexPattern":Ljava/lang/String;
    sget-object v10, Lcom/android/server/ScoutHelper;->supportNewBinderLog:Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    if-eqz v10, :cond_2

    :try_start_4
    const-string v10, "\\bfrom((?:\\s+)?\\d+):((?:\\s+)?\\d+)\\s+to((?:\\s+)?\\d+):((?:\\s+)?\\d+)+.*code((?:\\s+)?\\d+)+.*elapsed((?:\\s+)?\\d+)ms+.*"
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-object v0, v10

    goto :goto_2

    .line 684
    .end local v0    # "binderRegexPattern":Ljava/lang/String;
    :catch_0
    move-exception v0

    move-object/from16 v10, p1

    move-object/from16 v13, p3

    move-object/from16 v1, p4

    move-object/from16 v17, v5

    goto/16 :goto_8

    .line 649
    .restart local v0    # "binderRegexPattern":Ljava/lang/String;
    :cond_2
    :goto_2
    :try_start_5
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v10

    .line 650
    .local v10, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v10, v9}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v11

    .line 651
    .local v11, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v11}, Ljava/util/regex/Matcher;->find()Z

    move-result v12
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    const-string v13, "("

    if-nez v12, :cond_5

    .line 652
    :try_start_6
    invoke-virtual {v9, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 653
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v12, ") regex match failed"

    invoke-virtual {v8, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 655
    :cond_3
    nop

    .line 642
    .end local v0    # "binderRegexPattern":Ljava/lang/String;
    .end local v10    # "pattern":Ljava/util/regex/Pattern;
    .end local v11    # "matcher":Ljava/util/regex/Matcher;
    :cond_4
    move-object v0, v9

    goto :goto_0

    .line 657
    .restart local v0    # "binderRegexPattern":Ljava/lang/String;
    .restart local v10    # "pattern":Ljava/util/regex/Pattern;
    .restart local v11    # "matcher":Ljava/util/regex/Matcher;
    :cond_5
    const/4 v8, 0x1

    :try_start_7
    invoke-virtual {v11, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    move v7, v12

    .line 658
    const/4 v12, 0x2

    invoke-virtual {v11, v12}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    .line 659
    .local v12, "fromTid":I
    const/4 v14, 0x3

    invoke-virtual {v11, v14}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    .line 660
    .local v14, "toPid":I
    const/4 v15, 0x4

    invoke-virtual {v11, v15}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    .line 661
    .local v15, "toTid":I
    const/4 v8, 0x5

    invoke-virtual {v11, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 662
    .local v8, "code":I
    const-wide/16 v17, 0x0

    .line 663
    .local v17, "waitTime":D
    sget-object v19, Lcom/android/server/ScoutHelper;->supportNewBinderLog:Ljava/lang/Boolean;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v19
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    move-object/from16 v20, v0

    .end local v0    # "binderRegexPattern":Ljava/lang/String;
    .local v20, "binderRegexPattern":Ljava/lang/String;
    const/4 v0, 0x6

    if-eqz v19, :cond_6

    .line 664
    :try_start_8
    invoke-virtual {v11, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    .line 665
    .local v0, "elapsed":Ljava/lang/Double;
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v21
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    const-wide v23, 0x408f400000000000L    # 1000.0

    div-double v21, v21, v23

    .line 666
    .end local v0    # "elapsed":Ljava/lang/Double;
    .end local v17    # "waitTime":D
    .local v21, "waitTime":D
    move-object/from16 v17, v5

    move-wide/from16 v4, v21

    goto :goto_3

    .line 667
    .end local v21    # "waitTime":D
    .restart local v17    # "waitTime":D
    :cond_6
    :try_start_9
    invoke-virtual {v11, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v21
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    .end local v17    # "waitTime":D
    .restart local v21    # "waitTime":D
    move-object/from16 v17, v5

    move-wide/from16 v4, v21

    .line 669
    .end local v5    # "fileBinderReader":Ljava/io/File;
    .end local v21    # "waitTime":D
    .local v4, "waitTime":D
    .local v17, "fileBinderReader":Ljava/io/File;
    :goto_3
    :try_start_a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v18, v10

    .end local v10    # "pattern":Ljava/util/regex/Pattern;
    .local v18, "pattern":Ljava/util/regex/Pattern;
    const-string v10, "incoming transaction from "

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 670
    invoke-static {v7}, Lcom/android/server/ScoutHelper;->getProcessCmdline(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 671
    invoke-static {v7, v12}, Lcom/android/server/ScoutHelper;->getProcessComm(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v10, ") to "

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 672
    invoke-static {v14}, Lcom/android/server/ScoutHelper;->getProcessCmdline(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 673
    invoke-static {v14, v15}, Lcom/android/server/ScoutHelper;->getProcessComm(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v10, ") elapsed: "

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v10, " s code: "

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 676
    .local v0, "binderLog":Ljava/lang/String;
    move-object/from16 v10, p1

    :try_start_b
    invoke-virtual {v10, v0}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->addBinderTransInfo(Ljava/lang/String;)V

    .line 677
    const/4 v13, 0x1

    if-ge v7, v13, :cond_7

    move-object/from16 v13, p3

    move-object/from16 v1, p4

    goto :goto_5

    .line 678
    :cond_7
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v1, v13}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 679
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    move-object/from16 v19, v0

    .end local v0    # "binderLog":Ljava/lang/String;
    .local v19, "binderLog":Ljava/lang/String;
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v16, 0x1

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v13, v0}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v13, p3

    move-object/from16 v1, p4

    goto :goto_4

    .line 681
    .end local v19    # "binderLog":Ljava/lang/String;
    .restart local v0    # "binderLog":Ljava/lang/String;
    :cond_8
    move-object/from16 v19, v0

    .end local v0    # "binderLog":Ljava/lang/String;
    .restart local v19    # "binderLog":Ljava/lang/String;
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v13, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v1, v0, v13}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 682
    move-object/from16 v13, p3

    move-object/from16 v1, p4

    :try_start_c
    invoke-static {v3, v7, v13, v1}, Lcom/android/server/ScoutHelper;->addPidtoList(Ljava/lang/String;ILjava/util/ArrayList;Ljava/util/ArrayList;)Z
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    .line 686
    .end local v4    # "waitTime":D
    .end local v8    # "code":I
    .end local v11    # "matcher":Ljava/util/regex/Matcher;
    .end local v12    # "fromTid":I
    .end local v14    # "toPid":I
    .end local v15    # "toTid":I
    .end local v18    # "pattern":Ljava/util/regex/Pattern;
    .end local v19    # "binderLog":Ljava/lang/String;
    .end local v20    # "binderRegexPattern":Ljava/lang/String;
    :goto_4
    nop

    .line 642
    :goto_5
    move/from16 v4, p0

    move-object/from16 v1, p2

    move-object v0, v9

    move-object/from16 v5, v17

    goto/16 :goto_0

    .line 684
    :catch_1
    move-exception v0

    goto :goto_8

    .line 639
    .end local v7    # "fromPid":I
    .end local v9    # "binderTransactionInfo":Ljava/lang/String;
    :catchall_1
    move-exception v0

    goto :goto_6

    .line 684
    .restart local v7    # "fromPid":I
    .restart local v9    # "binderTransactionInfo":Ljava/lang/String;
    :catch_2
    move-exception v0

    goto :goto_7

    .line 639
    .end local v7    # "fromPid":I
    .end local v9    # "binderTransactionInfo":Ljava/lang/String;
    :catchall_2
    move-exception v0

    move-object/from16 v10, p1

    :goto_6
    move-object/from16 v13, p3

    move-object/from16 v1, p4

    goto :goto_9

    .line 684
    .restart local v7    # "fromPid":I
    .restart local v9    # "binderTransactionInfo":Ljava/lang/String;
    :catch_3
    move-exception v0

    move-object/from16 v10, p1

    :goto_7
    move-object/from16 v13, p3

    move-object/from16 v1, p4

    goto :goto_8

    .end local v17    # "fileBinderReader":Ljava/io/File;
    .restart local v5    # "fileBinderReader":Ljava/io/File;
    :catch_4
    move-exception v0

    move-object/from16 v10, p1

    move-object/from16 v13, p3

    move-object/from16 v1, p4

    move-object/from16 v17, v5

    .line 685
    .end local v5    # "fileBinderReader":Ljava/io/File;
    .local v0, "e":Ljava/lang/Exception;
    .restart local v17    # "fileBinderReader":Ljava/io/File;
    :goto_8
    :try_start_d
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "regular match error, String:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    .line 686
    move/from16 v4, p0

    move-object/from16 v1, p2

    move-object v0, v9

    move-object/from16 v5, v17

    .end local v0    # "e":Ljava/lang/Exception;
    goto/16 :goto_0

    .line 639
    .end local v7    # "fromPid":I
    .end local v9    # "binderTransactionInfo":Ljava/lang/String;
    :catchall_3
    move-exception v0

    :goto_9
    move-object v2, v0

    goto :goto_a

    .line 642
    .end local v17    # "fileBinderReader":Ljava/io/File;
    .restart local v5    # "fileBinderReader":Ljava/io/File;
    .restart local v7    # "fromPid":I
    .restart local v9    # "binderTransactionInfo":Ljava/lang/String;
    :cond_9
    move-object/from16 v10, p1

    move-object/from16 v13, p3

    move-object/from16 v1, p4

    move-object/from16 v17, v5

    .line 689
    .end local v5    # "fileBinderReader":Ljava/io/File;
    .end local v7    # "fromPid":I
    .end local v9    # "binderTransactionInfo":Ljava/lang/String;
    .restart local v17    # "fileBinderReader":Ljava/io/File;
    :try_start_e
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_5

    .line 692
    .end local v6    # "BinderReader":Ljava/io/BufferedReader;
    goto :goto_d

    .line 689
    :catch_5
    move-exception v0

    goto :goto_c

    .line 639
    .end local v17    # "fileBinderReader":Ljava/io/File;
    .restart local v5    # "fileBinderReader":Ljava/io/File;
    .restart local v6    # "BinderReader":Ljava/io/BufferedReader;
    :catchall_4
    move-exception v0

    move-object/from16 v10, p1

    move-object/from16 v13, p3

    move-object/from16 v1, p4

    move-object/from16 v17, v5

    move-object v2, v0

    .end local v5    # "fileBinderReader":Ljava/io/File;
    .restart local v17    # "fileBinderReader":Ljava/io/File;
    :goto_a
    :try_start_f
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_5

    goto :goto_b

    :catchall_5
    move-exception v0

    move-object v4, v0

    :try_start_10
    invoke-virtual {v2, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v3    # "tag":Ljava/lang/String;
    .end local v17    # "fileBinderReader":Ljava/io/File;
    .end local p0    # "Pid":I
    .end local p1    # "info":Lcom/android/server/ScoutHelper$ScoutBinderInfo;
    .end local p2    # "inPidMap":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .end local p3    # "javaProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local p4    # "nativeProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :goto_b
    throw v2
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_5

    .line 689
    .end local v6    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v3    # "tag":Ljava/lang/String;
    .restart local v5    # "fileBinderReader":Ljava/io/File;
    .restart local p0    # "Pid":I
    .restart local p1    # "info":Lcom/android/server/ScoutHelper$ScoutBinderInfo;
    .restart local p2    # "inPidMap":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .restart local p3    # "javaProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local p4    # "nativeProcess":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :catch_6
    move-exception v0

    move-object/from16 v10, p1

    move-object/from16 v13, p3

    move-object/from16 v1, p4

    move-object/from16 v17, v5

    .line 690
    .end local v5    # "fileBinderReader":Ljava/io/File;
    .restart local v0    # "e":Ljava/lang/Exception;
    .restart local v17    # "fileBinderReader":Ljava/io/File;
    :goto_c
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 691
    const-string v2, "Read binder Proc transaction Error:"

    invoke-static {v3, v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 693
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_d
    return-void
.end method

.method public static checkIsJavaOrNativeProcess(I)I
    .locals 1
    .param p0, "adj"    # I

    .line 761
    const/16 v0, -0x3e8

    if-ne p0, v0, :cond_0

    const/4 v0, 0x2

    return v0

    .line 762
    :cond_0
    const/16 v0, -0x3b6

    if-ne p0, v0, :cond_1

    const/4 v0, 0x3

    return v0

    .line 763
    :cond_1
    const/16 v0, -0x384

    if-lt p0, v0, :cond_2

    const/16 v0, 0x3e8

    if-gt p0, v0, :cond_2

    const/4 v0, 0x1

    return v0

    .line 764
    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method public static checkIsMonkey(Ljava/lang/String;D)Z
    .locals 2
    .param p0, "processName"    # Ljava/lang/String;
    .param p1, "time"    # D

    .line 627
    const-string v0, "com.android.commands.monkey"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/high16 v0, 0x4034000000000000L    # 20.0

    cmpl-double v0, p1, v0

    if-lez v0, :cond_0

    .line 628
    invoke-static {}, Lmiui/mqsas/scout/ScoutUtils;->isLibraryTest()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 627
    :goto_0
    return v0
.end method

.method public static copyRamoopsFileToMqs()V
    .locals 8

    .line 885
    const-string v0, "ScoutHelper"

    new-instance v1, Ljava/io/File;

    sget-object v2, Lcom/android/server/ScoutHelper;->CONSOLE_RAMOOPS_PATH:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 886
    .local v1, "ramoopsFile":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    sget-object v3, Lcom/android/server/ScoutHelper;->CONSOLE_RAMOOPS_0_PATH:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 887
    .local v2, "ramoops_0File":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 888
    return-void

    .line 890
    :cond_0
    const-string/jumbo v3, "sys.system_server.start_count"

    const/4 v4, 0x1

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v4, :cond_2

    .line 891
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    move-object v3, v1

    goto :goto_0

    :cond_1
    move-object v3, v2

    .line 892
    .local v3, "lastKmsgFile":Ljava/io/File;
    :goto_0
    :try_start_0
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-static {}, Lcom/android/server/ScoutHelper;->getMqsRamoopsFile()Ljava/io/File;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 893
    .local v4, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 894
    .local v5, "ramoopsInput":Ljava/io/FileInputStream;
    :try_start_2
    invoke-static {v5, v4}, Landroid/os/FileUtils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 895
    :try_start_3
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .end local v5    # "ramoopsInput":Ljava/io/FileInputStream;
    :try_start_4
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_3

    .line 892
    .restart local v5    # "ramoopsInput":Ljava/io/FileInputStream;
    :catchall_0
    move-exception v6

    :try_start_5
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v7

    :try_start_6
    invoke-virtual {v6, v7}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v1    # "ramoopsFile":Ljava/io/File;
    .end local v2    # "ramoops_0File":Ljava/io/File;
    .end local v3    # "lastKmsgFile":Ljava/io/File;
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    :goto_1
    throw v6
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .end local v5    # "ramoopsInput":Ljava/io/FileInputStream;
    .restart local v1    # "ramoopsFile":Ljava/io/File;
    .restart local v2    # "ramoops_0File":Ljava/io/File;
    .restart local v3    # "lastKmsgFile":Ljava/io/File;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v5

    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    goto :goto_2

    :catchall_3
    move-exception v6

    :try_start_8
    invoke-virtual {v5, v6}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v1    # "ramoopsFile":Ljava/io/File;
    .end local v2    # "ramoops_0File":Ljava/io/File;
    .end local v3    # "lastKmsgFile":Ljava/io/File;
    :goto_2
    throw v5
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0

    .line 898
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "ramoopsFile":Ljava/io/File;
    .restart local v2    # "ramoops_0File":Ljava/io/File;
    .restart local v3    # "lastKmsgFile":Ljava/io/File;
    :catch_0
    move-exception v4

    .line 899
    .local v4, "e":Ljava/lang/Exception;
    const-string v5, "UNKown Exception: copyRamoopsFileToMqs fail"

    invoke-static {v0, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 900
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    .line 895
    .end local v4    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v4

    .line 896
    .local v4, "e":Ljava/io/IOException;
    const-string v5, "IOException: copyRamoopsFileToMqs fail"

    invoke-static {v0, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 897
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    .line 901
    .end local v4    # "e":Ljava/io/IOException;
    :goto_3
    nop

    .line 903
    .end local v3    # "lastKmsgFile":Ljava/io/File;
    :cond_2
    :goto_4
    return-void
.end method

.method public static doSysRqInterface(C)V
    .locals 3
    .param p0, "c"    # C

    .line 794
    :try_start_0
    new-instance v0, Ljava/io/FileWriter;

    const-string v1, "/proc/sysrq-trigger"

    invoke-direct {v0, v1}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 795
    .local v0, "sysrq_trigger":Ljava/io/FileWriter;
    :try_start_1
    invoke-virtual {v0, p0}, Ljava/io/FileWriter;->write(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 796
    :try_start_2
    invoke-virtual {v0}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 798
    .end local v0    # "sysrq_trigger":Ljava/io/FileWriter;
    goto :goto_1

    .line 794
    .restart local v0    # "sysrq_trigger":Ljava/io/FileWriter;
    :catchall_0
    move-exception v1

    :try_start_3
    invoke-virtual {v0}, Ljava/io/FileWriter;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v2

    :try_start_4
    invoke-virtual {v1, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "c":C
    :goto_0
    throw v1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 796
    .end local v0    # "sysrq_trigger":Ljava/io/FileWriter;
    .restart local p0    # "c":C
    :catch_0
    move-exception v0

    .line 797
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "ScoutHelper"

    const-string v2, "Failed to write to /proc/sysrq-trigger"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 799
    .end local v0    # "e":Ljava/io/IOException;
    :goto_1
    return-void
.end method

.method public static dumpOfflineLog(Ljava/lang/String;Lcom/android/server/ScoutHelper$Action;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 20
    .param p0, "reason"    # Ljava/lang/String;
    .param p1, "action"    # Lcom/android/server/ScoutHelper$Action;
    .param p2, "type"    # Ljava/lang/String;
    .param p3, "where"    # Ljava/lang/String;

    .line 858
    move-object/from16 v1, p0

    move-object/from16 v11, p2

    move-object/from16 v12, p3

    const-string v0, "_"

    const-string v13, ""

    .line 860
    .local v13, "zipPath":Ljava/lang/String;
    :try_start_0
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string/jumbo v3, "yyyy-MM-dd-HH-mm-ss-SSS"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    move-object v14, v2

    .line 862
    .local v14, "offlineLogDateFormat":Ljava/text/SimpleDateFormat;
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v14, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    move-object v15, v2

    .line 863
    .local v15, "formattedDate":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v10, v2

    .line 864
    .local v10, "fileDir":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v9, v2

    .line 866
    .local v9, "fileName":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v16, v2

    .line 867
    .local v16, "offlineLogDir":Ljava/io/File;
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    move-result v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v3, "ScoutHelper"

    if-nez v2, :cond_0

    .line 868
    :try_start_1
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-nez v2, :cond_0

    .line 869
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot create "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 870
    return-object v13

    .line 873
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dumpOfflineLog reason:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " action="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v8, p1

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " type="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " where="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 875
    invoke-static/range {p1 .. p1}, Lcom/android/server/ScoutHelper$Action;->-$$Nest$fgetactions(Lcom/android/server/ScoutHelper$Action;)Ljava/util/List;

    move-result-object v4

    invoke-static/range {p1 .. p1}, Lcom/android/server/ScoutHelper$Action;->-$$Nest$fgetparams(Lcom/android/server/ScoutHelper$Action;)Ljava/util/List;

    move-result-object v5

    const/4 v6, 0x1

    const/4 v7, 0x1

    const/16 v17, 0x0

    invoke-static/range {p1 .. p1}, Lcom/android/server/ScoutHelper$Action;->-$$Nest$fgetincludeFiles(Lcom/android/server/ScoutHelper$Action;)Ljava/util/List;

    move-result-object v18

    move-object/from16 v2, p2

    move-object v3, v9

    move/from16 v8, v17

    move-object/from16 v19, v9

    .end local v9    # "fileName":Ljava/lang/String;
    .local v19, "fileName":Ljava/lang/String;
    move-object v9, v10

    move-object/from16 v17, v10

    .end local v10    # "fileDir":Ljava/lang/String;
    .local v17, "fileDir":Ljava/lang/String;
    move-object/from16 v10, v18

    invoke-static/range {v2 .. v10}, Lcom/android/server/ScoutHelper;->captureLog(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;ZIZLjava/lang/String;Ljava/util/List;)V

    .line 877
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object/from16 v2, v19

    .end local v19    # "fileName":Ljava/lang/String;
    .local v2, "fileName":Ljava/lang/String;
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "_0.zip"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-object v13, v0

    .line 880
    .end local v2    # "fileName":Ljava/lang/String;
    .end local v14    # "offlineLogDateFormat":Ljava/text/SimpleDateFormat;
    .end local v15    # "formattedDate":Ljava/lang/String;
    .end local v16    # "offlineLogDir":Ljava/io/File;
    .end local v17    # "fileDir":Ljava/lang/String;
    goto :goto_0

    .line 878
    :catch_0
    move-exception v0

    .line 879
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 881
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-object v13
.end method

.method public static dumpUithreadPeriodHistoryMessage(JI)V
    .locals 6
    .param p0, "anrTime"    # J
    .param p2, "duration"    # I

    .line 936
    const-string v0, "MIUIScout ANR"

    const/4 v1, 0x0

    .line 938
    .local v1, "monitor":Landroid/os/perfdebug/MessageMonitor;
    :try_start_0
    invoke-static {}, Lcom/android/server/UiThread;->get()Lcom/android/server/UiThread;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/UiThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Looper;->getMessageMonitor()Landroid/os/perfdebug/MessageMonitor;

    move-result-object v2

    move-object v1, v2

    .line 940
    if-eqz v1, :cond_1

    .line 941
    invoke-virtual {v1, p0, p1, p2}, Landroid/os/perfdebug/MessageMonitor;->getHistoryMsgInfoStringInPeriod(JI)Ljava/util/List;

    move-result-object v2

    .line 942
    .local v2, "historymsg":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 943
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "get period history msg from android.ui:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 942
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .end local v3    # "i":I
    :cond_0
    goto :goto_1

    .line 946
    .end local v2    # "historymsg":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    const-string v2, "Can\'t dumpPeriodHistoryMessage because of null MessageMonitor"

    invoke-static {v0, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 950
    :goto_1
    goto :goto_2

    .line 948
    :catch_0
    move-exception v2

    .line 949
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "AnrScout failed to get period history msg"

    invoke-static {v0, v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 951
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_2
    invoke-static {}, Lcom/android/server/UiThread;->get()Lcom/android/server/UiThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/UiThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Landroid/os/Looper;->printLoopInfo(I)V

    .line 952
    return-void
.end method

.method public static getBinderLogFile(Ljava/lang/String;I)Ljava/io/File;
    .locals 5
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "pid"    # I

    .line 422
    const/4 v0, 0x0

    .line 423
    .local v0, "fileBinderLog":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/dev/binderfs/binder_logs/proc_transaction/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 424
    .local v1, "fileBinderMIUI":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/dev/binderfs/binder_logs/proc/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 425
    .local v2, "fileBinderGKI":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 426
    move-object v0, v1

    goto :goto_0

    .line 427
    :cond_0
    invoke-static {}, Lcom/android/server/ScoutHelper;->supportGKI()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 428
    move-object v0, v2

    goto :goto_0

    .line 430
    :cond_1
    const-string v3, "gki binder logfs or miui binder logfs are not exist"

    invoke-static {p0, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    :goto_0
    return-object v0
.end method

.method public static getMiuiStackTraceByTid(I)[Ljava/lang/StackTraceElement;
    .locals 8
    .param p0, "tid"    # I

    .line 920
    const/4 v0, 0x0

    .line 923
    .local v0, "st":[Ljava/lang/StackTraceElement;
    const/4 v1, 0x0

    :try_start_0
    const-string v2, "dalvik.system.VMStack"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 924
    .local v2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v3, "getMiuiStackTraceByTid"

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/Class;

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v7, 0x0

    aput-object v6, v5, v7

    invoke-virtual {v2, v3, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 925
    .local v3, "method":Ljava/lang/reflect/Method;
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 926
    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v3, v1, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/StackTraceElement;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v4

    .line 930
    .end local v2    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v3    # "method":Ljava/lang/reflect/Method;
    nop

    .line 931
    sget-boolean v1, Lcom/android/server/ScoutHelper;->ENABLED_SCOUT_DEBUG:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getMiuiStackTraceByTid tid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ScoutHelper"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 932
    :cond_0
    return-object v0

    .line 927
    :catch_0
    move-exception v2

    .line 928
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 929
    return-object v1
.end method

.method private static getMqsRamoopsFile()Ljava/io/File;
    .locals 4

    .line 906
    new-instance v0, Ljava/io/File;

    const-string v1, "/data/mqsas/temp/pstore/"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 908
    .local v0, "mqsFsDir":Ljava/io/File;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_0

    .line 909
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 910
    const/16 v1, 0x1fc

    const/4 v2, -0x1

    invoke-static {v0, v1, v2, v2}, Landroid/os/FileUtils;->setPermissions(Ljava/io/File;III)I

    .line 912
    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    const-string v3, "console-ramoops"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 913
    :catch_0
    move-exception v1

    .line 914
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 916
    .end local v1    # "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    return-object v1
.end method

.method public static getOomAdjOfPid(Ljava/lang/String;I)I
    .locals 5
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "Pid"    # I

    .line 768
    const/16 v0, -0x3e8

    .line 769
    .local v0, "adj":I
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/FileReader;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/proc/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 770
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/oom_score_adj"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 771
    .local v1, "adjReader":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v2

    .line 772
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 774
    .end local v1    # "adjReader":Ljava/io/BufferedReader;
    goto :goto_1

    .line 769
    .restart local v1    # "adjReader":Ljava/io/BufferedReader;
    :catchall_0
    move-exception v2

    :try_start_3
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v3

    :try_start_4
    invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "adj":I
    .end local p0    # "tag":Ljava/lang/String;
    .end local p1    # "Pid":I
    :goto_0
    throw v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 772
    .end local v1    # "adjReader":Ljava/io/BufferedReader;
    .restart local v0    # "adj":I
    .restart local p0    # "tag":Ljava/lang/String;
    .restart local p1    # "Pid":I
    :catch_0
    move-exception v1

    .line 773
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "Check is java or native process Error:"

    invoke-static {p0, v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 775
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return v0
.end method

.method public static getProcStatus(I)Lcom/android/server/ScoutHelper$ProcStatus;
    .locals 7
    .param p0, "Pid"    # I

    .line 990
    sget-object v0, Lcom/android/server/ScoutHelper;->STATUS_KEYS:[Ljava/lang/String;

    array-length v1, v0

    new-array v1, v1, [J

    .line 991
    .local v1, "output":[J
    const/4 v2, 0x0

    const-wide/16 v3, -0x1

    aput-wide v3, v1, v2

    .line 992
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "/proc/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/status"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v0, v1}, Landroid/os/Process;->readProcLines(Ljava/lang/String;[Ljava/lang/String;[J)V

    .line 993
    aget-wide v5, v1, v2

    cmp-long v0, v5, v3

    if-nez v0, :cond_0

    .line 995
    const/4 v0, 0x0

    return-object v0

    .line 997
    :cond_0
    new-instance v0, Lcom/android/server/ScoutHelper$ProcStatus;

    invoke-direct {v0}, Lcom/android/server/ScoutHelper$ProcStatus;-><init>()V

    .line 998
    .local v0, "procStatus":Lcom/android/server/ScoutHelper$ProcStatus;
    aget-wide v2, v1, v2

    long-to-int v2, v2

    iput v2, v0, Lcom/android/server/ScoutHelper$ProcStatus;->tracerPid:I

    .line 999
    return-object v0
.end method

.method public static getProcessCmdline(I)Ljava/lang/String;
    .locals 5
    .param p0, "pid"    # I

    .line 967
    const-string/jumbo v0, "unknow"

    .line 968
    .local v0, "cmdline":Ljava/lang/String;
    if-nez p0, :cond_0

    return-object v0

    .line 969
    :cond_0
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/FileReader;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "proc/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/cmdline"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 970
    .local v1, "cmdlineReader":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v2

    .line 971
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 973
    .end local v1    # "cmdlineReader":Ljava/io/BufferedReader;
    goto :goto_1

    .line 969
    .restart local v1    # "cmdlineReader":Ljava/io/BufferedReader;
    :catchall_0
    move-exception v2

    :try_start_3
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v3

    :try_start_4
    invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "cmdline":Ljava/lang/String;
    .end local p0    # "pid":I
    :goto_0
    throw v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 971
    .end local v1    # "cmdlineReader":Ljava/io/BufferedReader;
    .restart local v0    # "cmdline":Ljava/lang/String;
    .restart local p0    # "pid":I
    :catch_0
    move-exception v1

    .line 972
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Read process("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") cmdline Error"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ScoutHelper"

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 974
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return-object v0
.end method

.method public static getProcessComm(II)Ljava/lang/String;
    .locals 5
    .param p0, "pid"    # I
    .param p1, "tid"    # I

    .line 955
    const-string/jumbo v0, "unknow"

    .line 956
    .local v0, "comm":Ljava/lang/String;
    if-nez p1, :cond_0

    return-object v0

    .line 957
    :cond_0
    if-ne p0, p1, :cond_1

    invoke-static {p0}, Lcom/android/server/ScoutHelper;->getProcessCmdline(I)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 958
    :cond_1
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/FileReader;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "proc/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/comm"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 959
    .local v1, "commReader":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v2

    .line 960
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 962
    .end local v1    # "commReader":Ljava/io/BufferedReader;
    goto :goto_1

    .line 958
    .restart local v1    # "commReader":Ljava/io/BufferedReader;
    :catchall_0
    move-exception v2

    :try_start_3
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v3

    :try_start_4
    invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "comm":Ljava/lang/String;
    .end local p0    # "pid":I
    .end local p1    # "tid":I
    :goto_0
    throw v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 960
    .end local v1    # "commReader":Ljava/io/BufferedReader;
    .restart local v0    # "comm":Ljava/lang/String;
    .restart local p0    # "pid":I
    .restart local p1    # "tid":I
    :catch_0
    move-exception v1

    .line 961
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Read process("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") comm Error"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ScoutHelper"

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 963
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return-object v0
.end method

.method public static getPsInfo(I)Ljava/lang/String;
    .locals 11
    .param p0, "mode"    # I

    .line 1003
    const-string v0, ""

    const-string v1, "close file stream error"

    const-string v2, "ScoutHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1005
    .local v3, "psInfo":Ljava/lang/StringBuilder;
    packed-switch p0, :pswitch_data_0

    .line 1011
    const-string v4, " -A"

    .local v4, "args":Ljava/lang/String;
    goto :goto_0

    .line 1007
    .end local v4    # "args":Ljava/lang/String;
    :pswitch_0
    const-string v4, " -AT"

    .line 1008
    .restart local v4    # "args":Ljava/lang/String;
    nop

    .line 1013
    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ps"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1014
    .local v5, "command":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1016
    .local v6, "reader":Ljava/io/BufferedReader;
    const/4 v7, 0x0

    .line 1018
    .local v7, "in":Ljava/io/InputStreamReader;
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v8
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7

    .line 1022
    .local v8, "process":Ljava/lang/Process;
    nop

    .line 1024
    :try_start_1
    new-instance v9, Ljava/io/InputStreamReader;

    invoke-virtual {v8}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object v7, v9

    .line 1025
    new-instance v9, Ljava/io/BufferedReader;

    invoke-direct {v9, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object v6, v9

    .line 1027
    :goto_1
    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v9

    move-object v10, v9

    .local v10, "line":Ljava/lang/String;
    if-eqz v9, :cond_0

    .line 1028
    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1029
    const/16 v9, 0xa

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1035
    .end local v10    # "line":Ljava/lang/String;
    :cond_0
    :try_start_2
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 1038
    goto :goto_2

    .line 1036
    :catch_0
    move-exception v9

    .line 1037
    .local v9, "e":Ljava/io/IOException;
    invoke-static {v2, v1, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1040
    .end local v9    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_3
    invoke-virtual {v7}, Ljava/io/InputStreamReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_6

    .line 1041
    :catch_1
    move-exception v9

    goto :goto_5

    .line 1034
    :catchall_0
    move-exception v0

    goto :goto_8

    .line 1031
    :catch_2
    move-exception v9

    .line 1032
    .restart local v9    # "e":Ljava/io/IOException;
    :try_start_4
    const-string v10, "io error when read stream"

    invoke-static {v2, v10, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1035
    .end local v9    # "e":Ljava/io/IOException;
    if-eqz v6, :cond_1

    :try_start_5
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_3

    .line 1036
    :catch_3
    move-exception v9

    .line 1037
    .restart local v9    # "e":Ljava/io/IOException;
    invoke-static {v2, v1, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 1038
    .end local v9    # "e":Ljava/io/IOException;
    :cond_1
    :goto_3
    nop

    .line 1040
    :goto_4
    if-eqz v7, :cond_2

    :try_start_6
    invoke-virtual {v7}, Ljava/io/InputStreamReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_6

    .line 1041
    :catch_4
    move-exception v9

    .line 1042
    .restart local v9    # "e":Ljava/io/IOException;
    :goto_5
    invoke-static {v2, v1, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_7

    .line 1043
    .end local v9    # "e":Ljava/io/IOException;
    :cond_2
    :goto_6
    nop

    .line 1044
    :goto_7
    invoke-virtual {v8}, Ljava/lang/Process;->destroy()V

    .line 1045
    nop

    .line 1046
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_3
    return-object v0

    .line 1035
    :goto_8
    if-eqz v6, :cond_4

    :try_start_7
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    goto :goto_9

    .line 1036
    :catch_5
    move-exception v9

    .line 1037
    .restart local v9    # "e":Ljava/io/IOException;
    invoke-static {v2, v1, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_a

    .line 1038
    .end local v9    # "e":Ljava/io/IOException;
    :cond_4
    :goto_9
    nop

    .line 1040
    :goto_a
    if-eqz v7, :cond_5

    :try_start_8
    invoke-virtual {v7}, Ljava/io/InputStreamReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    goto :goto_b

    .line 1041
    :catch_6
    move-exception v9

    .line 1042
    .restart local v9    # "e":Ljava/io/IOException;
    invoke-static {v2, v1, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_c

    .line 1043
    .end local v9    # "e":Ljava/io/IOException;
    :cond_5
    :goto_b
    nop

    .line 1044
    :goto_c
    invoke-virtual {v8}, Ljava/lang/Process;->destroy()V

    .line 1045
    throw v0

    .line 1019
    .end local v8    # "process":Ljava/lang/Process;
    :catch_7
    move-exception v1

    .line 1020
    .local v1, "e":Ljava/io/IOException;
    const-string v8, "can\'t exec the cmd "

    invoke-static {v2, v8, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1021
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private static getmDaemon()Lmiui/mqsas/IMQSNative;
    .locals 2

    .line 169
    sget-object v0, Lcom/android/server/ScoutHelper;->mDaemon:Lmiui/mqsas/IMQSNative;

    if-nez v0, :cond_0

    .line 170
    const-string v0, "miui.mqsas.IMQSNative"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lmiui/mqsas/IMQSNative$Stub;->asInterface(Landroid/os/IBinder;)Lmiui/mqsas/IMQSNative;

    move-result-object v0

    sput-object v0, Lcom/android/server/ScoutHelper;->mDaemon:Lmiui/mqsas/IMQSNative;

    .line 171
    if-nez v0, :cond_0

    .line 172
    const-string v0, "ScoutHelper"

    const-string v1, "mqsasd not available!"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    :cond_0
    sget-object v0, Lcom/android/server/ScoutHelper;->mDaemon:Lmiui/mqsas/IMQSNative;

    return-object v0
.end method

.method public static isDebugpolicyed(Ljava/lang/String;)Z
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;

    .line 783
    const-string v0, "ro.boot.dp"

    const-string/jumbo v1, "unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 784
    .local v0, "dp":Ljava/lang/String;
    const-string v1, "0xB"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 788
    :cond_0
    const-string/jumbo v1, "this device didn\'t flash Debugpolicy"

    invoke-static {p0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 789
    const/4 v1, 0x0

    return v1

    .line 785
    :cond_1
    :goto_0
    const-string/jumbo v1, "this device has falshed Debugpolicy"

    invoke-static {p0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 786
    const/4 v1, 0x1

    return v1
.end method

.method public static isEnabelPanicDThread(Ljava/lang/String;)Z
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;

    .line 779
    sget-boolean v0, Lcom/android/server/ScoutHelper;->PANIC_D_THREAD:Z

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/android/server/ScoutHelper;->isDebugpolicyed(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static printfProcBinderInfo(ILjava/lang/String;)V
    .locals 5
    .param p0, "Pid"    # I
    .param p1, "tag"    # Ljava/lang/String;

    .line 731
    const-string v0, "ScoutHelper"

    invoke-static {v0, p0}, Lcom/android/server/ScoutHelper;->getBinderLogFile(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    .line 732
    .local v1, "fileBinderReader":Ljava/io/File;
    if-nez v1, :cond_0

    .line 733
    return-void

    .line 735
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Pid "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Binder Info:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 736
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, v1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 737
    .local v2, "BinderReader":Ljava/io/BufferedReader;
    const/4 v3, 0x0

    .line 738
    .local v3, "binderTransactionInfo":Ljava/lang/String;
    :cond_1
    :goto_0
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    if-eqz v4, :cond_5

    .line 739
    sget-object v4, Lcom/android/server/ScoutHelper;->supportNewBinderLog:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 740
    const-string v4, "    outgoing transaction"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "    incoming transaction"

    .line 741
    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "    pending async transaction"

    .line 742
    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "    pending transaction"

    .line 743
    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 744
    :cond_2
    invoke-static {p1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 746
    :cond_3
    sget-boolean v4, Lcom/android/server/ScoutHelper;->SCOUT_BINDER_GKI:Z

    if-eqz v4, :cond_4

    .line 747
    const-string v4, "MIUI"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 748
    invoke-static {p1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 751
    :cond_4
    invoke-static {p1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 754
    .end local v3    # "binderTransactionInfo":Ljava/lang/String;
    :cond_5
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 757
    .end local v2    # "BinderReader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 736
    .restart local v2    # "BinderReader":Ljava/io/BufferedReader;
    :catchall_0
    move-exception v3

    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v4

    :try_start_4
    invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v1    # "fileBinderReader":Ljava/io/File;
    .end local p0    # "Pid":I
    .end local p1    # "tag":Ljava/lang/String;
    :goto_1
    throw v3
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 754
    .end local v2    # "BinderReader":Ljava/io/BufferedReader;
    .restart local v1    # "fileBinderReader":Ljava/io/File;
    .restart local p0    # "Pid":I
    .restart local p1    # "tag":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 755
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 756
    const-string v3, "Read binder Proc transaction Error:"

    invoke-static {v0, v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 758
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_2
    return-void
.end method

.method public static resumeBinderThreadFull(Ljava/lang/String;Ljava/util/TreeMap;)Ljava/lang/String;
    .locals 11
    .param p0, "tag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/TreeMap<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 700
    .local p1, "inPidMap":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    sget-boolean v0, Lcom/android/server/ScoutHelper;->ENABLED_SCOUT_DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Debug: resumeBinderFull"

    invoke-static {p0, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 701
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 703
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;>;"
    new-instance v1, Lcom/android/server/ScoutHelper$1;

    invoke-direct {v1}, Lcom/android/server/ScoutHelper$1;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 709
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 710
    .local v1, "minfo":Ljava/lang/StringBuilder;
    const-string v2, "Incoming Binder Procsss Info:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 711
    const/4 v2, 0x0

    .line 712
    .local v2, "isKilled":Z
    sget-object v3, Lcom/android/server/ScoutHelper;->sSkipProcs:[Ljava/lang/String;

    invoke-static {v3}, Landroid/os/Process;->getPidsForCommands([Ljava/lang/String;)[I

    move-result-object v3

    .line 713
    .local v3, "skipPids":[I
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 714
    .local v5, "mapping":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 715
    .local v6, "inPid":I
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 716
    .local v7, "count":I
    invoke-static {p0, v6}, Lcom/android/server/ScoutHelper;->getOomAdjOfPid(Ljava/lang/String;I)I

    move-result v8

    .line 717
    .local v8, "oomAdj":I
    sget-boolean v9, Lcom/android/server/ScoutHelper;->BINDER_FULL_KILL_PROC:Z

    if-eqz v9, :cond_1

    const/16 v9, -0x320

    if-lt v8, v9, :cond_1

    if-nez v2, :cond_1

    if-lez v6, :cond_1

    .line 718
    invoke-static {v3, v6}, Lcom/android/internal/util/ArrayUtils;->contains([II)Z

    move-result v9

    if-nez v9, :cond_1

    .line 719
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Pid("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ") adj("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ") is Killed, Because it use "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " binder thread of System_server"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {p0, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 721
    invoke-static {v6}, Landroid/os/Process;->killProcess(I)V

    .line 722
    const/4 v2, 0x1

    .line 724
    :cond_1
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "\nPid "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "(adj : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ") count "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 725
    .end local v5    # "mapping":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .end local v6    # "inPid":I
    .end local v7    # "count":I
    .end local v8    # "oomAdj":I
    goto/16 :goto_0

    .line 726
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 727
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static runCommand(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6
    .param p0, "action"    # Ljava/lang/String;
    .param p1, "params"    # Ljava/lang/String;
    .param p2, "timeout"    # I

    .line 802
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "runCommand action "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " params "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ScoutHelper"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 803
    invoke-static {}, Lcom/android/server/ScoutHelper;->getmDaemon()Lmiui/mqsas/IMQSNative;

    move-result-object v0

    .line 804
    .local v0, "mClient":Lmiui/mqsas/IMQSNative;
    if-nez v0, :cond_0

    .line 805
    const-string v2, "runCommand no mqsasd!"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 806
    return-void

    .line 808
    :cond_0
    if-eqz p0, :cond_4

    if-nez p1, :cond_1

    goto :goto_1

    .line 813
    :cond_1
    const/16 v2, 0x3c

    if-ge p2, v2, :cond_2

    .line 814
    const/16 v2, 0x3c

    .local v2, "cmd_timeout":I
    goto :goto_0

    .line 816
    .end local v2    # "cmd_timeout":I
    :cond_2
    move v2, p2

    .line 820
    .restart local v2    # "cmd_timeout":I
    :goto_0
    :try_start_0
    invoke-interface {v0, p0, p1, v2}, Lmiui/mqsas/IMQSNative;->runCommand(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v3

    .line 821
    .local v3, "result":I
    if-gez v3, :cond_3

    .line 822
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "runCommanxd Fail result = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 824
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "runCommanxd result = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 829
    nop

    .line 830
    return-void

    .line 825
    .end local v3    # "result":I
    :catch_0
    move-exception v3

    .line 826
    .local v3, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "runCommanxd Exception "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 827
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 828
    return-void

    .line 809
    .end local v2    # "cmd_timeout":I
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_4
    :goto_1
    const-string v2, "runCommand Wrong parameters!"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 810
    return-void
.end method

.method private static supportGKI()Ljava/lang/Boolean;
    .locals 1

    .line 418
    sget-object v0, Lcom/android/server/ScoutHelper;->supportNewBinderLog:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/android/server/ScoutHelper;->SCOUT_BINDER_GKI:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
