class com.android.server.MiuiRestoreManagerService$BackupDataTask implements java.lang.Runnable {
	 /* .source "MiuiRestoreManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/MiuiRestoreManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "BackupDataTask" */
} // .end annotation
/* # instance fields */
private miui.app.backup.ITaskCommonCallback callback;
private java.lang.String domains;
private java.lang.String excludePaths;
private android.os.ParcelFileDescriptor outFd;
private java.lang.String packageName;
private java.lang.String parentPaths;
private java.lang.String paths;
final com.android.server.MiuiRestoreManagerService this$0; //synthetic
private Integer type;
/* # direct methods */
public com.android.server.MiuiRestoreManagerService$BackupDataTask ( ) {
/* .locals 0 */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "paths" # [Ljava/lang/String; */
/* .param p4, "domains" # [Ljava/lang/String; */
/* .param p5, "parentPaths" # [Ljava/lang/String; */
/* .param p6, "excludePaths" # [Ljava/lang/String; */
/* .param p7, "outFd" # Landroid/os/ParcelFileDescriptor; */
/* .param p8, "type" # I */
/* .param p9, "callback" # Lmiui/app/backup/ITaskCommonCallback; */
/* .line 683 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 684 */
this.packageName = p2;
/* .line 685 */
this.paths = p3;
/* .line 686 */
this.domains = p4;
/* .line 687 */
this.parentPaths = p5;
/* .line 688 */
this.excludePaths = p6;
/* .line 689 */
this.outFd = p7;
/* .line 690 */
/* iput p8, p0, Lcom/android/server/MiuiRestoreManagerService$BackupDataTask;->type:I */
/* .line 691 */
this.callback = p9;
/* .line 692 */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 15 */
/* .line 696 */
final String v0 = "error when write EOF "; // const-string v0, "error when write EOF "
final String v1 = "error when notify onTaskEnd "; // const-string v1, "error when notify onTaskEnd "
final String v2 = "MiuiRestoreManagerService"; // const-string v2, "MiuiRestoreManagerService"
int v3 = 0; // const/4 v3, 0x0
/* .line 697 */
/* .local v3, "errorCode":I */
int v4 = 0; // const/4 v4, 0x0
/* .line 699 */
/* .local v4, "exception":Ljava/lang/String; */
int v5 = 4; // const/4 v5, 0x4
int v6 = 0; // const/4 v6, 0x0
try { // :try_start_0
	 v7 = this.this$0;
	 com.android.server.MiuiRestoreManagerService .-$$Nest$fgetmInstaller ( v7 );
	 v9 = this.packageName;
	 v10 = this.paths;
	 v11 = this.domains;
	 v12 = this.parentPaths;
	 v13 = this.excludePaths;
	 v14 = this.outFd;
	 v7 = 	 /* invoke-virtual/range {v8 ..v14}, Lcom/android/server/pm/Installer;->backupAppData(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)I */
	 /* :try_end_0 */
	 /* .catch Lcom/android/server/pm/Installer$InstallerException; {:try_start_0 ..:try_end_0} :catch_2 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* move v3, v7 */
	 /* .line 706 */
	 try { // :try_start_1
		 v7 = this.callback;
		 /* :try_end_1 */
		 /* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
		 /* .line 709 */
		 /* .line 707 */
		 /* :catch_0 */
		 /* move-exception v6 */
		 /* .line 708 */
		 /* .local v6, "e":Ljava/lang/Exception; */
		 android.util.Slog .e ( v2,v1,v6 );
		 /* .line 710 */
	 } // .end local v6 # "e":Ljava/lang/Exception;
} // :goto_0
/* new-instance v1, Ljava/io/FileOutputStream; */
v6 = this.outFd;
(( android.os.ParcelFileDescriptor ) v6 ).getFileDescriptor ( ); // invoke-virtual {v6}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;
/* invoke-direct {v1, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V */
/* .line 711 */
/* .local v1, "out":Ljava/io/FileOutputStream; */
/* new-array v5, v5, [B */
/* .line 713 */
/* .local v5, "buf":[B */
try { // :try_start_2
	 (( java.io.FileOutputStream ) v1 ).write ( v5 ); // invoke-virtual {v1, v5}, Ljava/io/FileOutputStream;->write([B)V
	 /* .line 714 */
	 (( java.io.FileOutputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
	 /* :try_end_2 */
	 /* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_1 */
	 /* .line 715 */
	 /* :catch_1 */
	 /* move-exception v6 */
	 /* .line 705 */
} // .end local v1 # "out":Ljava/io/FileOutputStream;
} // .end local v5 # "buf":[B
/* :catchall_0 */
/* move-exception v7 */
/* .line 700 */
/* :catch_2 */
/* move-exception v7 */
/* .line 701 */
/* .local v7, "e":Lcom/android/server/pm/Installer$InstallerException; */
int v3 = 1; // const/4 v3, 0x1
/* .line 702 */
try { // :try_start_3
(( com.android.server.pm.Installer$InstallerException ) v7 ).getMessage ( ); // invoke-virtual {v7}, Lcom/android/server/pm/Installer$InstallerException;->getMessage()Ljava/lang/String;
/* move-object v4, v8 */
/* .line 703 */
final String v8 = "backupData "; // const-string v8, "backupData "
android.util.Slog .w ( v2,v8,v7 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 706 */
} // .end local v7 # "e":Lcom/android/server/pm/Installer$InstallerException;
try { // :try_start_4
v7 = this.callback;
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_3 */
/* .line 709 */
/* .line 707 */
/* :catch_3 */
/* move-exception v6 */
/* .line 708 */
/* .restart local v6 # "e":Ljava/lang/Exception; */
android.util.Slog .e ( v2,v1,v6 );
/* .line 710 */
} // .end local v6 # "e":Ljava/lang/Exception;
} // :goto_1
/* new-instance v1, Ljava/io/FileOutputStream; */
v6 = this.outFd;
(( android.os.ParcelFileDescriptor ) v6 ).getFileDescriptor ( ); // invoke-virtual {v6}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;
/* invoke-direct {v1, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V */
/* .line 711 */
/* .restart local v1 # "out":Ljava/io/FileOutputStream; */
/* new-array v5, v5, [B */
/* .line 713 */
/* .restart local v5 # "buf":[B */
try { // :try_start_5
(( java.io.FileOutputStream ) v1 ).write ( v5 ); // invoke-virtual {v1, v5}, Ljava/io/FileOutputStream;->write([B)V
/* .line 714 */
(( java.io.FileOutputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
/* :try_end_5 */
/* .catch Ljava/io/IOException; {:try_start_5 ..:try_end_5} :catch_4 */
/* .line 717 */
} // :goto_2
/* .line 715 */
/* :catch_4 */
/* move-exception v6 */
/* .line 716 */
/* .local v6, "e":Ljava/io/IOException; */
} // :goto_3
android.util.Slog .e ( v2,v0,v6 );
/* .line 719 */
} // .end local v1 # "out":Ljava/io/FileOutputStream;
} // .end local v5 # "buf":[B
} // .end local v6 # "e":Ljava/io/IOException;
} // :goto_4
/* nop */
/* .line 720 */
return;
/* .line 706 */
} // :goto_5
try { // :try_start_6
v8 = this.callback;
/* :try_end_6 */
/* .catch Ljava/lang/Exception; {:try_start_6 ..:try_end_6} :catch_5 */
/* .line 709 */
/* .line 707 */
/* :catch_5 */
/* move-exception v6 */
/* .line 708 */
/* .local v6, "e":Ljava/lang/Exception; */
android.util.Slog .e ( v2,v1,v6 );
/* .line 710 */
} // .end local v6 # "e":Ljava/lang/Exception;
} // :goto_6
/* new-instance v1, Ljava/io/FileOutputStream; */
v6 = this.outFd;
(( android.os.ParcelFileDescriptor ) v6 ).getFileDescriptor ( ); // invoke-virtual {v6}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;
/* invoke-direct {v1, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V */
/* .line 711 */
/* .restart local v1 # "out":Ljava/io/FileOutputStream; */
/* new-array v5, v5, [B */
/* .line 713 */
/* .restart local v5 # "buf":[B */
try { // :try_start_7
(( java.io.FileOutputStream ) v1 ).write ( v5 ); // invoke-virtual {v1, v5}, Ljava/io/FileOutputStream;->write([B)V
/* .line 714 */
(( java.io.FileOutputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
/* :try_end_7 */
/* .catch Ljava/io/IOException; {:try_start_7 ..:try_end_7} :catch_6 */
/* .line 717 */
/* .line 715 */
/* :catch_6 */
/* move-exception v6 */
/* .line 716 */
/* .local v6, "e":Ljava/io/IOException; */
android.util.Slog .e ( v2,v0,v6 );
/* .line 719 */
} // .end local v1 # "out":Ljava/io/FileOutputStream;
} // .end local v5 # "buf":[B
} // .end local v6 # "e":Ljava/io/IOException;
} // :goto_7
/* throw v7 */
} // .end method
