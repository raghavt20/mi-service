.class public Lcom/android/server/ForceDarkAppConfigProvider;
.super Ljava/lang/Object;
.source "ForceDarkAppConfigProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/ForceDarkAppConfigProvider$ForceDarkAppConfigChangeListener;
    }
.end annotation


# static fields
.field private static final CONFIG_VERSION:Ljava/lang/String; = "configVersion"

.field private static final DEBUG:Z = true

.field private static final FORCE_DARK_APP_AMEND_NAME:Ljava/lang/String; = "force_dark_app_config_amend"

.field private static final FORCE_DARK_APP_CONFIG_FILE_PATH:Ljava/lang/String; = "system_ext/etc/forcedarkconfig/"

.field private static final FORCE_DARK_APP_CONFIG_FILE_VERSION_PATH:Ljava/lang/String; = "system_ext/etc/forcedarkconfig/ForceDarkAppConfigVersion.json"

.field private static final FORCE_DARK_APP_MODULE_NAME:Ljava/lang/String; = "force_dark_app_config"

.field private static final TAG:Ljava/lang/String;

.field private static volatile sCloudDataHelper:Lcom/android/server/ForceDarkAppConfigProvider;


# instance fields
.field private debugDisableCloud:Z

.field private mAmendForceDarkAppConfig:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mAppConfigChangeListener:Lcom/android/server/ForceDarkAppConfigProvider$ForceDarkAppConfigChangeListener;

.field private mCloudConfigVersion:I

.field private mCloudForceDarkAppConfig:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mLocalConfigVersion:I

.field private mLocalForceDarkAppConfig:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$sfgetTAG()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/server/ForceDarkAppConfigProvider;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 33
    const-class v0, Lcom/android/server/ForceDarkAppConfigProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ForceDarkAppConfigProvider;->TAG:Ljava/lang/String;

    .line 45
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/ForceDarkAppConfigProvider;->sCloudDataHelper:Lcom/android/server/ForceDarkAppConfigProvider;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mCloudConfigVersion:I

    .line 43
    iput v0, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mLocalConfigVersion:I

    .line 48
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mLocalForceDarkAppConfig:Ljava/util/HashMap;

    .line 50
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mCloudForceDarkAppConfig:Ljava/util/HashMap;

    .line 52
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mAmendForceDarkAppConfig:Ljava/util/HashMap;

    .line 58
    const-string/jumbo v0, "sys.forcedark.disable_cloud"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    move v1, v2

    :cond_0
    iput-boolean v1, p0, Lcom/android/server/ForceDarkAppConfigProvider;->debugDisableCloud:Z

    .line 59
    return-void
.end method

.method private getAmendAppConfig(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 186
    iget-object v0, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mAmendForceDarkAppConfig:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mAmendForceDarkAppConfig:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0

    .line 189
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private getCloudAppConfig(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 179
    iget-object v0, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mCloudForceDarkAppConfig:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mCloudForceDarkAppConfig:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0

    .line 182
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public static getInstance()Lcom/android/server/ForceDarkAppConfigProvider;
    .locals 2

    .line 62
    sget-object v0, Lcom/android/server/ForceDarkAppConfigProvider;->sCloudDataHelper:Lcom/android/server/ForceDarkAppConfigProvider;

    if-nez v0, :cond_1

    .line 63
    const-class v0, Lcom/android/server/ForceDarkAppConfigProvider;

    monitor-enter v0

    .line 64
    :try_start_0
    sget-object v1, Lcom/android/server/ForceDarkAppConfigProvider;->sCloudDataHelper:Lcom/android/server/ForceDarkAppConfigProvider;

    if-nez v1, :cond_0

    .line 65
    new-instance v1, Lcom/android/server/ForceDarkAppConfigProvider;

    invoke-direct {v1}, Lcom/android/server/ForceDarkAppConfigProvider;-><init>()V

    sput-object v1, Lcom/android/server/ForceDarkAppConfigProvider;->sCloudDataHelper:Lcom/android/server/ForceDarkAppConfigProvider;

    .line 67
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 69
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/ForceDarkAppConfigProvider;->sCloudDataHelper:Lcom/android/server/ForceDarkAppConfigProvider;

    return-object v0
.end method

.method private getLocalAppConfig(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .line 193
    const-string v0, "../"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 194
    return-object v1

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mLocalForceDarkAppConfig:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 197
    iget-object v0, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mLocalForceDarkAppConfig:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0

    .line 199
    :cond_1
    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "system_ext/etc/forcedarkconfig/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 200
    .local v0, "localFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 201
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 202
    .local v2, "inputStream":Ljava/io/FileInputStream;
    :try_start_1
    invoke-direct {p0, v2}, Lcom/android/server/ForceDarkAppConfigProvider;->readFully(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v3

    .line 203
    .local v3, "jsonString":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mLocalForceDarkAppConfig:Ljava/util/HashMap;

    invoke-virtual {v4, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 204
    nop

    .line 205
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 204
    return-object v3

    .line 201
    .end local v3    # "jsonString":Ljava/lang/String;
    :catchall_0
    move-exception v3

    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v4

    :try_start_4
    invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "localFile":Ljava/io/File;
    .end local p0    # "this":Lcom/android/server/ForceDarkAppConfigProvider;
    .end local p1    # "packageName":Ljava/lang/String;
    :goto_0
    throw v3
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 205
    .end local v2    # "inputStream":Ljava/io/FileInputStream;
    .restart local v0    # "localFile":Ljava/io/File;
    .restart local p0    # "this":Lcom/android/server/ForceDarkAppConfigProvider;
    .restart local p1    # "packageName":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 206
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 209
    .end local v2    # "e":Ljava/io/IOException;
    :cond_2
    return-object v1
.end method

.method private getLocalAppConfigVersion()V
    .locals 6

    .line 214
    const-string v0, "configVersion"

    new-instance v1, Ljava/io/File;

    const-string/jumbo v2, "system_ext/etc/forcedarkconfig/ForceDarkAppConfigVersion.json"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 215
    .local v1, "versionFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 216
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v3

    .line 217
    .local v2, "inputStream":Ljava/io/FileInputStream;
    :try_start_1
    invoke-direct {p0, v2}, Lcom/android/server/ForceDarkAppConfigProvider;->readFully(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v3

    .line 218
    .local v3, "jsonString":Ljava/lang/String;
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 219
    .local v4, "configVersionJson":Lorg/json/JSONObject;
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 220
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mLocalConfigVersion:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 222
    .end local v3    # "jsonString":Ljava/lang/String;
    .end local v4    # "configVersionJson":Lorg/json/JSONObject;
    :cond_0
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    .line 224
    .end local v2    # "inputStream":Ljava/io/FileInputStream;
    goto :goto_1

    .line 216
    .restart local v2    # "inputStream":Ljava/io/FileInputStream;
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v3

    :try_start_4
    invoke-virtual {v0, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v1    # "versionFile":Ljava/io/File;
    .end local p0    # "this":Lcom/android/server/ForceDarkAppConfigProvider;
    :goto_0
    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0

    .line 222
    .end local v2    # "inputStream":Ljava/io/FileInputStream;
    .restart local v1    # "versionFile":Ljava/io/File;
    .restart local p0    # "this":Lcom/android/server/ForceDarkAppConfigProvider;
    :catch_0
    move-exception v0

    .line 223
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 226
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_1
    return-void
.end method

.method private readFully(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 4
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 230
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 231
    .local v0, "baos":Ljava/io/OutputStream;
    const/16 v1, 0x400

    new-array v1, v1, [B

    .line 232
    .local v1, "buffer":[B
    invoke-virtual {p1, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    .line 233
    .local v2, "read":I
    :goto_0
    if-ltz v2, :cond_0

    .line 234
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 235
    invoke-virtual {p1, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    goto :goto_0

    .line 237
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private registerDataObserver(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 141
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 142
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/android/server/ForceDarkAppConfigProvider$1;

    .line 143
    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v3

    invoke-direct {v2, p0, v3, p1}, Lcom/android/server/ForceDarkAppConfigProvider$1;-><init>(Lcom/android/server/ForceDarkAppConfigProvider;Landroid/os/Handler;Landroid/content/Context;)V

    .line 141
    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 150
    return-void
.end method


# virtual methods
.method public getForceDarkAppConfig(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .line 157
    const/4 v0, 0x0

    .line 158
    .local v0, "configJson":Ljava/lang/String;
    iget v1, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mCloudConfigVersion:I

    iget v2, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mLocalConfigVersion:I

    if-lt v1, v2, :cond_0

    .line 159
    invoke-direct {p0, p1}, Lcom/android/server/ForceDarkAppConfigProvider;->getCloudAppConfig(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 161
    :cond_0
    if-nez v0, :cond_1

    .line 162
    invoke-direct {p0, p1}, Lcom/android/server/ForceDarkAppConfigProvider;->getLocalAppConfig(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 164
    :cond_1
    sget-object v1, Lcom/android/server/ForceDarkAppConfigProvider;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cloudConfigVersion: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mCloudConfigVersion:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   localConfigVersion:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mLocalConfigVersion:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    invoke-direct {p0, p1}, Lcom/android/server/ForceDarkAppConfigProvider;->getAmendAppConfig(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 166
    .local v1, "amendJson":Ljava/lang/String;
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 168
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 169
    .local v2, "jsonObject":Lorg/json/JSONObject;
    const-string v3, "amend"

    invoke-virtual {v2, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 170
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v3

    .line 171
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    :catch_0
    move-exception v2

    .line 172
    .local v2, "e":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    .line 175
    .end local v2    # "e":Lorg/json/JSONException;
    :cond_2
    return-object v0
.end method

.method public onBootPhase(ILandroid/content/Context;)V
    .locals 0
    .param p1, "phase"    # I
    .param p2, "context"    # Landroid/content/Context;

    .line 82
    invoke-virtual {p0, p2}, Lcom/android/server/ForceDarkAppConfigProvider;->updateLocalForceDarkAppConfig(Landroid/content/Context;)V

    .line 83
    invoke-direct {p0, p2}, Lcom/android/server/ForceDarkAppConfigProvider;->registerDataObserver(Landroid/content/Context;)V

    .line 84
    invoke-direct {p0}, Lcom/android/server/ForceDarkAppConfigProvider;->getLocalAppConfigVersion()V

    .line 85
    return-void
.end method

.method public setAppConfigChangeListener(Lcom/android/server/ForceDarkAppConfigProvider$ForceDarkAppConfigChangeListener;)V
    .locals 0
    .param p1, "listChangeListener"    # Lcom/android/server/ForceDarkAppConfigProvider$ForceDarkAppConfigChangeListener;

    .line 78
    iput-object p1, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mAppConfigChangeListener:Lcom/android/server/ForceDarkAppConfigProvider$ForceDarkAppConfigChangeListener;

    .line 79
    return-void
.end method

.method public updateLocalForceDarkAppConfig(Landroid/content/Context;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;

    .line 89
    const-string v0, "configVersion"

    const-string v1, "force_dark_app_config_amend"

    const-string v2, "force_dark_app_config"

    iget-boolean v3, p0, Lcom/android/server/ForceDarkAppConfigProvider;->debugDisableCloud:Z

    if-eqz v3, :cond_0

    .line 90
    iget-object v0, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mCloudForceDarkAppConfig:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 91
    sget-object v0, Lcom/android/server/ForceDarkAppConfigProvider;->TAG:Ljava/lang/String;

    const-string v1, "getCloudForceDarkConfig empty for disableCloud"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    return-void

    .line 96
    :cond_0
    nop

    .line 97
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 96
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v3, v2, v2, v5, v4}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    .local v3, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    const-string v6, "packageName"

    if-eqz v3, :cond_2

    :try_start_1
    invoke-virtual {v3}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 100
    sget-object v7, Lcom/android/server/ForceDarkAppConfigProvider;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getCloudConfig, force_dark_app_config: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    invoke-virtual {v3}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v7

    .line 102
    .local v7, "jsonAll":Lorg/json/JSONObject;
    invoke-virtual {v7, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 103
    iget-object v8, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mCloudForceDarkAppConfig:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/util/HashMap;->clear()V

    .line 104
    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 105
    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mCloudConfigVersion:I

    .line 107
    :cond_1
    invoke-virtual {v7, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 108
    .local v0, "appConfigArray":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v2, v8, :cond_2

    .line 109
    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    .line 110
    .local v8, "appConfig":Lorg/json/JSONObject;
    invoke-virtual {v8, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 111
    .local v9, "packageName":Ljava/lang/String;
    iget-object v10, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mCloudForceDarkAppConfig:Ljava/util/HashMap;

    invoke-virtual {v8}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v9, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    nop

    .end local v8    # "appConfig":Lorg/json/JSONObject;
    .end local v9    # "packageName":Ljava/lang/String;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 115
    .end local v0    # "appConfigArray":Lorg/json/JSONArray;
    .end local v2    # "i":I
    .end local v7    # "jsonAll":Lorg/json/JSONObject;
    :cond_2
    nop

    .line 116
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 115
    invoke-static {v0, v1, v1, v5, v4}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v0

    .line 118
    .end local v3    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 119
    sget-object v2, Lcom/android/server/ForceDarkAppConfigProvider;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getCloudConfig, force_dark_app_config_amend: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v2

    .line 121
    .local v2, "jsonAll":Lorg/json/JSONObject;
    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 122
    iget-object v3, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mAmendForceDarkAppConfig:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    .line 123
    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 124
    .local v1, "appConfigArray":Lorg/json/JSONArray;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v3, v4, :cond_3

    .line 125
    invoke-virtual {v1, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 126
    .local v4, "appConfig":Lorg/json/JSONObject;
    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 127
    .local v5, "packageName":Ljava/lang/String;
    iget-object v7, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mAmendForceDarkAppConfig:Ljava/util/HashMap;

    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v5, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    nop

    .end local v4    # "appConfig":Lorg/json/JSONObject;
    .end local v5    # "packageName":Ljava/lang/String;
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 132
    .end local v1    # "appConfigArray":Lorg/json/JSONArray;
    .end local v2    # "jsonAll":Lorg/json/JSONObject;
    .end local v3    # "i":I
    :cond_3
    iget-object v1, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mAppConfigChangeListener:Lcom/android/server/ForceDarkAppConfigProvider$ForceDarkAppConfigChangeListener;

    if-eqz v1, :cond_4

    .line 133
    invoke-interface {v1}, Lcom/android/server/ForceDarkAppConfigProvider$ForceDarkAppConfigChangeListener;->onChange()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 137
    .end local v0    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    :cond_4
    goto :goto_2

    .line 135
    :catch_0
    move-exception v0

    .line 136
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/android/server/ForceDarkAppConfigProvider;->TAG:Ljava/lang/String;

    const-string v2, "exception when getCloudForceDarkConfig: "

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 138
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_2
    return-void
.end method
