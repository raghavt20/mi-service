.class Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;
.super Landroid/os/Handler;
.source "MiuiBatteryIntelligence.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MiuiBatteryIntelligence;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BatteryInelligenceHandler"
.end annotation


# static fields
.field public static final CLOSE_LOW_BATTERY_FAST_CHARGE_FUNCTION:Ljava/lang/String; = "8"

.field public static final KEY_LOW_BATTERY_FAST_CHARGE:Ljava/lang/String; = "pc_low_battery_fast_charge"

.field static final MSG_BOOT_COMPLETED:I = 0x2

.field static final MSG_DELAY_JUDGMEMNT:I = 0x5

.field static final MSG_NEED_SCAN_WIFI:I = 0x3

.field static final MSG_PLUGGED_CHANGED:I = 0x1

.field static final MSG_WIFI_STATE_CHANGE:I = 0x4

.field public static final SMART_CHARGER_NODE:Ljava/lang/String; = "smart_chg"

.field public static final START_LOW_BATTERY_FAST_CHARGE_FUNCTION:Ljava/lang/String; = "9"

.field public static final START_OUT_DOOR_CHARGE_FUNCTION:Ljava/lang/String; = "5"

.field public static final STOP_OUT_DOOR_CHARGE_FUNCTION:Ljava/lang/String; = "4"


# instance fields
.field private mAlarmManager:Landroid/app/AlarmManager;

.field private mBatteryNotificationListerner:Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;

.field private final mContentResolver:Landroid/content/ContentResolver;

.field private mIsOutDoor:Z

.field private mNeedScanWifi:Landroid/app/PendingIntent;

.field private mSavedApInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/net/wifi/WifiConfiguration;",
            ">;"
        }
    .end annotation
.end field

.field private mSettingsObserver:Landroid/database/ContentObserver;

.field private mWifiManager:Landroid/net/wifi/WifiManager;

.field final synthetic this$0:Lcom/android/server/MiuiBatteryIntelligence;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContentResolver(Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;)Landroid/content/ContentResolver;
    .locals 0

    iget-object p0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mContentResolver:Landroid/content/ContentResolver;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$msetSmartChargeFunction(Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->setSmartChargeFunction(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lcom/android/server/MiuiBatteryIntelligence;Landroid/os/Looper;)V
    .locals 2
    .param p2, "looper"    # Landroid/os/Looper;

    .line 249
    iput-object p1, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    .line 250
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 220
    new-instance v0, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;-><init>(Lcom/android/server/MiuiBatteryIntelligence;Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService-IA;)V

    iput-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mBatteryNotificationListerner:Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;

    .line 231
    new-instance v0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler$1;

    invoke-static {p1}, Lcom/android/server/MiuiBatteryIntelligence;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryIntelligence;)Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler$1;-><init>(Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mSettingsObserver:Landroid/database/ContentObserver;

    .line 251
    iget-object v0, p1, Lcom/android/server/MiuiBatteryIntelligence;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mContentResolver:Landroid/content/ContentResolver;

    .line 252
    iget-object v0, p1, Lcom/android/server/MiuiBatteryIntelligence;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 253
    iget-object v0, p1, Lcom/android/server/MiuiBatteryIntelligence;->mContext:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mAlarmManager:Landroid/app/AlarmManager;

    .line 254
    invoke-virtual {p1}, Lcom/android/server/MiuiBatteryIntelligence;->isSupportLowBatteryFastCharge()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 255
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->registerSettingsObserver()V

    .line 257
    :cond_0
    return-void
.end method

.method private cancelScanAlarm()V
    .locals 2

    .line 433
    iget-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mNeedScanWifi:Landroid/app/PendingIntent;

    if-eqz v0, :cond_0

    .line 434
    iget-object v1, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mAlarmManager:Landroid/app/AlarmManager;

    invoke-virtual {v1, v0}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 435
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mNeedScanWifi:Landroid/app/PendingIntent;

    .line 436
    const-string v0, "MiuiBatteryIntelligence"

    const-string v1, "cancel alarm"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    :cond_0
    return-void
.end method

.method private changeListenerStatus()V
    .locals 6

    .line 307
    iget-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryIntelligence;->-$$Nest$fgetmPlugged(Lcom/android/server/MiuiBatteryIntelligence;)Z

    move-result v0

    const-string v1, "MiuiBatteryIntelligence"

    if-eqz v0, :cond_0

    .line 309
    :try_start_0
    iget-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mBatteryNotificationListerner:Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;

    iget-object v2, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    iget-object v2, v2, Lcom/android/server/MiuiBatteryIntelligence;->mContext:Landroid/content/Context;

    new-instance v3, Landroid/content/ComponentName;

    iget-object v4, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    iget-object v4, v4, Lcom/android/server/MiuiBatteryIntelligence;->mContext:Landroid/content/Context;

    .line 310
    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    const/4 v4, -0x1

    invoke-virtual {v0, v2, v3, v4}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->registerAsSystemService(Landroid/content/Context;Landroid/content/ComponentName;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 311
    :catch_0
    move-exception v0

    .line 312
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "Cannot register listener"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    goto :goto_1

    .line 316
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mBatteryNotificationListerner:Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;

    invoke-virtual {v0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->clearStartAppList()V

    .line 317
    iget-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mBatteryNotificationListerner:Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;

    invoke-virtual {v0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryNotificationListernerService;->unregisterAsSystemService()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 320
    goto :goto_1

    .line 318
    :catch_1
    move-exception v0

    .line 319
    .restart local v0    # "e":Landroid/os/RemoteException;
    const-string v2, "Cannot unregister listener"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_1
    return-void
.end method

.method private changeOutDoorState()V
    .locals 3

    .line 325
    iget-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryIntelligence;->-$$Nest$fgetmPlugged(Lcom/android/server/MiuiBatteryIntelligence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 326
    const/4 v0, 0x5

    const-wide/16 v1, 0x3e8

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->sendMessageDelayed(IJ)V

    goto :goto_0

    .line 328
    :cond_0
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->turnOffMonitorIfNeeded()V

    .line 330
    :goto_0
    return-void
.end method

.method private compareSavedList(Ljava/util/List;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/net/wifi/ScanResult;",
            ">;)Z"
        }
    .end annotation

    .line 407
    .local p1, "scanResults":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/ScanResult;

    .line 408
    .local v1, "scanResult":Landroid/net/wifi/ScanResult;
    iget-object v2, v1, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    .line 409
    .local v2, "ssid":Ljava/lang/String;
    invoke-static {}, Lcom/android/server/MiuiBatteryIntelligence;->-$$Nest$sfgetDEBUG()Z

    move-result v3

    const-string v4, "MiuiBatteryIntelligence"

    if-eqz v3, :cond_0

    .line 410
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "scanResult ssid = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    :cond_0
    iget-object v3, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mSavedApInfo:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/net/wifi/WifiConfiguration;

    .line 414
    .local v5, "wifiConfiguration":Landroid/net/wifi/WifiConfiguration;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v7, v5, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 415
    const-string v0, "already has saved"

    invoke-static {v4, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    const/4 v0, 0x1

    return v0

    .line 418
    .end local v5    # "wifiConfiguration":Landroid/net/wifi/WifiConfiguration;
    :cond_1
    goto :goto_1

    .line 419
    .end local v1    # "scanResult":Landroid/net/wifi/ScanResult;
    .end local v2    # "ssid":Ljava/lang/String;
    :cond_2
    goto :goto_0

    .line 420
    :cond_3
    const/4 v0, 0x0

    return v0
.end method

.method private confrimOutDoorScene()V
    .locals 2

    .line 424
    iget-boolean v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mIsOutDoor:Z

    if-eqz v0, :cond_0

    .line 425
    return-void

    .line 427
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mIsOutDoor:Z

    .line 428
    const-string v0, "MiuiBatteryIntelligence"

    const-string v1, "Now we get outDoorScene"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    const-string v0, "5"

    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->setSmartChargeFunction(Ljava/lang/String;)V

    .line 430
    return-void
.end method

.method private getSavedApInfo()V
    .locals 1

    .line 445
    iget-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mSavedApInfo:Ljava/util/List;

    .line 446
    return-void
.end method

.method private handlerWifiChangedDisable()V
    .locals 0

    .line 333
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->cancelScanAlarm()V

    .line 334
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->confrimOutDoorScene()V

    .line 335
    return-void
.end method

.method private outDoorJudgment()V
    .locals 4

    .line 361
    iget-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryIntelligence;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryIntelligence;)Lmiui/util/IMiCharge;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/util/IMiCharge;->getBatteryChargeType()Ljava/lang/String;

    move-result-object v0

    .line 362
    .local v0, "chargeType":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TYPE = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiBatteryIntelligence"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    const-string v1, "DCP"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 364
    return-void

    .line 366
    :cond_0
    iget-object v1, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryIntelligence;->-$$Nest$fgetmWifiState(Lcom/android/server/MiuiBatteryIntelligence;)I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_1

    .line 367
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->confrimOutDoorScene()V

    goto :goto_0

    .line 368
    :cond_1
    iget-object v1, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryIntelligence;->-$$Nest$fgetmWifiState(Lcom/android/server/MiuiBatteryIntelligence;)I

    move-result v1

    const/4 v3, 0x3

    if-ne v1, v3, :cond_3

    .line 369
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->getSavedApInfo()V

    .line 370
    iget-object v1, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mSavedApInfo:Ljava/util/List;

    if-nez v1, :cond_2

    .line 371
    const-string/jumbo v1, "there is no saved AP info"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->confrimOutDoorScene()V

    .line 373
    return-void

    .line 375
    :cond_2
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->scanConfirmSavaStatus()V

    .line 378
    :cond_3
    :goto_0
    return-void
.end method

.method private registerSettingsObserver()V
    .locals 4

    .line 260
    const-string v0, "pc_low_battery_fast_charge"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 261
    .local v0, "u":Landroid/net/Uri;
    iget-object v1, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mSettingsObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 262
    return-void
.end method

.method private resetLowBatteryFastChargeState()V
    .locals 4

    .line 338
    iget-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mContentResolver:Landroid/content/ContentResolver;

    const-string v1, "pc_low_battery_fast_charge"

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 339
    .local v0, "state":I
    const-string v1, "MiuiBatteryIntelligence"

    if-ne v0, v2, :cond_0

    .line 340
    const-string v2, "Failed to get settings"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    return-void

    .line 343
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Boot Completed reset low-Battery-Charge State = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 345
    const-string v1, "9"

    invoke-direct {p0, v1}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->setSmartChargeFunction(Ljava/lang/String;)V

    goto :goto_0

    .line 346
    :cond_1
    if-nez v0, :cond_2

    .line 347
    const-string v1, "8"

    invoke-direct {p0, v1}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->setSmartChargeFunction(Ljava/lang/String;)V

    .line 349
    :cond_2
    :goto_0
    return-void
.end method

.method private sacnApList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/net/wifi/ScanResult;",
            ">;"
        }
    .end annotation

    .line 441
    iget-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private scanConfirmSavaStatus()V
    .locals 8

    .line 390
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->sacnApList()Ljava/util/List;

    move-result-object v0

    .line 391
    .local v0, "scanResults":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    const-string v1, "MiuiBatteryIntelligence"

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mSavedApInfo:Ljava/util/List;

    if-nez v2, :cond_0

    goto :goto_1

    .line 395
    :cond_0
    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->compareSavedList(Ljava/util/List;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 396
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->confrimOutDoorScene()V

    goto :goto_0

    .line 398
    :cond_1
    new-instance v2, Landroid/content/Intent;

    const-string v3, "miui.intent.action.NEED_SCAN_WIFI"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 399
    .local v2, "needScan":Landroid/content/Intent;
    iget-object v3, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    iget-object v3, v3, Lcom/android/server/MiuiBatteryIntelligence;->mContext:Landroid/content/Context;

    const/4 v4, 0x0

    const/high16 v5, 0x4000000

    invoke-static {v3, v4, v2, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    iput-object v3, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mNeedScanWifi:Landroid/app/PendingIntent;

    .line 400
    iget-object v3, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mAlarmManager:Landroid/app/AlarmManager;

    .line 401
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    const-wide/32 v6, 0x493e0

    add-long/2addr v4, v6

    iget-object v6, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mNeedScanWifi:Landroid/app/PendingIntent;

    .line 400
    const/4 v7, 0x2

    invoke-virtual {v3, v7, v4, v5, v6}, Landroid/app/AlarmManager;->setExactAndAllowWhileIdle(IJLandroid/app/PendingIntent;)V

    .line 402
    const-string v3, "has saved AP,wait 5 mins"

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    .end local v2    # "needScan":Landroid/content/Intent;
    :goto_0
    return-void

    .line 392
    :cond_2
    :goto_1
    const-string v2, "Faild to get current AP list"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    return-void
.end method

.method private setSmartChargeFunction(Ljava/lang/String;)V
    .locals 3
    .param p1, "function"    # Ljava/lang/String;

    .line 353
    const-string v0, "MiuiBatteryIntelligence"

    :try_start_0
    iget-object v1, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryIntelligence;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryIntelligence;)Lmiui/util/IMiCharge;

    move-result-object v1

    const-string/jumbo v2, "smart_chg"

    invoke-virtual {v1, v2, p1}, Lmiui/util/IMiCharge;->setMiChargePath(Ljava/lang/String;Ljava/lang/String;)Z

    .line 354
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "set smart charge = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 357
    goto :goto_0

    .line 355
    :catch_0
    move-exception v1

    .line 356
    .local v1, "ee":Ljava/lang/Exception;
    const-string v2, "failed to set function"

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    .end local v1    # "ee":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private turnOffMonitorIfNeeded()V
    .locals 1

    .line 381
    iget-boolean v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mIsOutDoor:Z

    if-eqz v0, :cond_0

    .line 382
    const-string v0, "4"

    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->setSmartChargeFunction(Ljava/lang/String;)V

    .line 384
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->mIsOutDoor:Z

    .line 385
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->removeMessages(I)V

    .line 386
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->cancelScanAlarm()V

    .line 387
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .line 272
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 302
    const-string v0, "MiuiBatteryIntelligence"

    const-string v1, "no msg need to handle"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 299
    :pswitch_0
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->outDoorJudgment()V

    .line 300
    goto :goto_0

    .line 293
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryIntelligence;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryIntelligence;)Lmiui/util/IMiCharge;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/util/IMiCharge;->getBatteryChargeType()Ljava/lang/String;

    move-result-object v0

    .line 294
    .local v0, "type":Ljava/lang/String;
    const-string v1, "DCP"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 295
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->handlerWifiChangedDisable()V

    goto :goto_0

    .line 290
    .end local v0    # "type":Ljava/lang/String;
    :pswitch_2
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->scanConfirmSavaStatus()V

    .line 291
    goto :goto_0

    .line 282
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    invoke-virtual {v0}, Lcom/android/server/MiuiBatteryIntelligence;->isSupportNavigationCharge()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    iget-object v1, v0, Lcom/android/server/MiuiBatteryIntelligence;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "NavigationWhiteList"

    invoke-static {v0, v1, v2}, Lcom/android/server/MiuiBatteryIntelligence;->-$$Nest$mreadLocalCloudControlData(Lcom/android/server/MiuiBatteryIntelligence;Landroid/content/ContentResolver;Ljava/lang/String;)V

    .line 285
    :cond_0
    iget-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    invoke-virtual {v0}, Lcom/android/server/MiuiBatteryIntelligence;->isSupportLowBatteryFastCharge()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 286
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->resetLowBatteryFastChargeState()V

    goto :goto_0

    .line 274
    :pswitch_4
    iget-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    invoke-virtual {v0}, Lcom/android/server/MiuiBatteryIntelligence;->isSupportNavigationCharge()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 275
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->changeListenerStatus()V

    .line 277
    :cond_1
    iget-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    invoke-virtual {v0}, Lcom/android/server/MiuiBatteryIntelligence;->isSupportOutDoorCharge()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 278
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->changeOutDoorState()V

    .line 304
    :cond_2
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public sendMessageDelayed(IJ)V
    .locals 1
    .param p1, "what"    # I
    .param p2, "delayMillis"    # J

    .line 265
    invoke-virtual {p0, p1}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->removeMessages(I)V

    .line 266
    invoke-static {p0, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 267
    .local v0, "m":Landroid/os/Message;
    invoke-virtual {p0, v0, p2, p3}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 268
    return-void
.end method
