class com.android.server.ScoutSystemMonitor$ScreenStateReceiver extends android.content.BroadcastReceiver {
	 /* .source "ScoutSystemMonitor.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/ScoutSystemMonitor; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "ScreenStateReceiver" */
} // .end annotation
/* # instance fields */
final com.android.server.ScoutSystemMonitor this$0; //synthetic
/* # direct methods */
 com.android.server.ScoutSystemMonitor$ScreenStateReceiver ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/ScoutSystemMonitor; */
/* .line 280 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 283 */
int v0 = 0; // const/4 v0, 0x0
/* .line 284 */
/* .local v0, "screenOn":Z */
v1 = this.this$0;
com.android.server.ScoutSystemMonitor .-$$Nest$fgetmiuiFboService ( v1 );
/* if-nez v1, :cond_0 */
/* .line 285 */
v1 = this.this$0;
/* const-class v2, Lcom/miui/app/MiuiFboServiceInternal; */
com.android.server.LocalServices .getService ( v2 );
/* check-cast v2, Lcom/miui/app/MiuiFboServiceInternal; */
com.android.server.ScoutSystemMonitor .-$$Nest$fputmiuiFboService ( v1,v2 );
/* .line 288 */
} // :cond_0
try { // :try_start_0
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v2 = (( java.lang.String ) v1 ).hashCode ( ); // invoke-virtual {v1}, Ljava/lang/String;->hashCode()I
/* sparse-switch v2, :sswitch_data_0 */
} // :cond_1
/* :sswitch_0 */
final String v2 = "android.intent.action.SCREEN_ON"; // const-string v2, "android.intent.action.SCREEN_ON"
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
int v1 = 0; // const/4 v1, 0x0
/* :sswitch_1 */
final String v2 = "android.intent.action.SCREEN_OFF"; // const-string v2, "android.intent.action.SCREEN_OFF"
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
	 int v1 = 1; // const/4 v1, 0x1
} // :goto_0
int v1 = -1; // const/4 v1, -0x1
} // :goto_1
/* packed-switch v1, :pswitch_data_0 */
/* .line 298 */
int v0 = 0; // const/4 v0, 0x0
/* .line 290 */
/* :pswitch_0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 291 */
v1 = this.this$0;
v1 = com.android.server.ScoutSystemMonitor .-$$Nest$fgetmiuiFboService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_2
v1 = this.this$0;
v1 = com.android.server.ScoutSystemMonitor .-$$Nest$fgetmiuiFboService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_2
	 /* .line 292 */
	 v1 = this.this$0;
	 com.android.server.ScoutSystemMonitor .-$$Nest$fgetmiuiFboService ( v1 );
	 /* const-string/jumbo v2, "stopDueToScreen" */
	 int v3 = 5; // const/4 v3, 0x5
	 /* const-wide/16 v4, 0x0 */
	 /* .line 301 */
} // :cond_2
} // :goto_2
v1 = this.this$0;
com.android.server.ScoutSystemMonitor .-$$Nest$fgetmiuiFboService ( v1 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 304 */
/* .line 302 */
/* :catch_0 */
/* move-exception v1 */
/* .line 303 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "ScoutSystemMonitor"; // const-string v2, "ScoutSystemMonitor"
final String v3 = "brodacastReceiver exp "; // const-string v3, "brodacastReceiver exp "
android.util.Slog .w ( v2,v3,v1 );
/* .line 306 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_3
v1 = this.this$0;
(( com.android.server.ScoutSystemMonitor ) v1 ).updateScreenState ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/ScoutSystemMonitor;->updateScreenState(Z)V
/* .line 307 */
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x7ed8ea7f -> :sswitch_1 */
/* -0x56ac2893 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
