class com.android.server.ForceDarkUiModeModeManager$1 implements com.android.server.ForceDarkAppConfigProvider$ForceDarkAppConfigChangeListener {
	 /* .source "ForceDarkUiModeModeManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/ForceDarkUiModeModeManager;->initForceDarkAppConfig()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.ForceDarkUiModeModeManager this$0; //synthetic
/* # direct methods */
 com.android.server.ForceDarkUiModeModeManager$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/ForceDarkUiModeModeManager; */
/* .line 263 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onChange ( ) {
/* .locals 3 */
/* .line 266 */
com.android.server.ForceDarkUiModeModeManager .-$$Nest$sfgetTAG ( );
final String v1 = "onForceDarkConfigChanged"; // const-string v1, "onForceDarkConfigChanged"
android.util.Log .i ( v0,v1 );
/* .line 268 */
android.view.ForceDarkHelperStub .getInstance ( );
v1 = this.this$0;
com.android.server.ForceDarkUiModeModeManager .-$$Nest$fgetmUiModeManagerService ( v1 );
/* .line 269 */
(( com.android.server.UiModeManagerService ) v2 ).getContext ( ); // invoke-virtual {v2}, Lcom/android/server/UiModeManagerService;->getContext()Landroid/content/Context;
(( android.content.Context ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
com.android.server.ForceDarkUiModeModeManager .-$$Nest$mgetForceDarkAppConfig ( v1,v2 );
/* .line 268 */
(( android.view.ForceDarkHelperStub ) v0 ).onForceDarkConfigChanged ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/ForceDarkHelperStub;->onForceDarkConfigChanged(Ljava/lang/String;)V
/* .line 270 */
return;
} // .end method
