.class Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;
.super Ljava/lang/Object;
.source "MiuiBatteryStatsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MiuiBatteryStatsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BatteryTempInfo"
.end annotation


# instance fields
.field private averageTemp:I

.field private condition:Ljava/lang/String;

.field private dataCount:I

.field private maxTemp:I

.field private minTemp:I

.field final synthetic this$0:Lcom/android/server/MiuiBatteryStatsService;

.field private totalTemp:I


# direct methods
.method public constructor <init>(Lcom/android/server/MiuiBatteryStatsService;Ljava/lang/String;)V
    .locals 2
    .param p1, "this$0"    # Lcom/android/server/MiuiBatteryStatsService;
    .param p2, "condition"    # Ljava/lang/String;

    .line 1778
    iput-object p1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1779
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->totalTemp:I

    .line 1780
    iput v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->dataCount:I

    .line 1781
    const/16 v1, -0x12c

    iput v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->maxTemp:I

    .line 1782
    const/16 v1, 0x258

    iput v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->minTemp:I

    .line 1783
    iput v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->averageTemp:I

    .line 1784
    iput-object p2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->condition:Ljava/lang/String;

    .line 1785
    return-void
.end method


# virtual methods
.method public getAverageTemp()I
    .locals 1

    .line 1824
    iget v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->averageTemp:I

    return v0
.end method

.method public getCondition()Ljava/lang/String;
    .locals 1

    .line 1831
    iget-object v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->condition:Ljava/lang/String;

    return-object v0
.end method

.method public getDataCount()I
    .locals 1

    .line 1803
    iget v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->dataCount:I

    return v0
.end method

.method public getMaxTemp()I
    .locals 1

    .line 1810
    iget v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->maxTemp:I

    return v0
.end method

.method public getMinTemp()I
    .locals 1

    .line 1817
    iget v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->minTemp:I

    return v0
.end method

.method public getTotalTemp()I
    .locals 1

    .line 1796
    iget v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->totalTemp:I

    return v0
.end method

.method public reset()V
    .locals 2

    .line 1788
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->totalTemp:I

    .line 1789
    iput v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->dataCount:I

    .line 1790
    const/16 v1, -0x12c

    iput v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->maxTemp:I

    .line 1791
    const/16 v1, 0x258

    iput v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->minTemp:I

    .line 1792
    iput v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->averageTemp:I

    .line 1793
    return-void
.end method

.method public setAverageTemp()V
    .locals 2

    .line 1827
    iget v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->totalTemp:I

    iget v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->dataCount:I

    div-int/2addr v0, v1

    iput v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->averageTemp:I

    .line 1828
    return-void
.end method

.method public setDataCount()V
    .locals 1

    .line 1806
    iget v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->dataCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->dataCount:I

    .line 1807
    return-void
.end method

.method public setMaxTemp(I)V
    .locals 0
    .param p1, "maxTemp"    # I

    .line 1813
    iput p1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->maxTemp:I

    .line 1814
    return-void
.end method

.method public setMinTemp(I)V
    .locals 0
    .param p1, "minTemp"    # I

    .line 1820
    iput p1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->minTemp:I

    .line 1821
    return-void
.end method

.method public setTotalTemp(I)V
    .locals 1
    .param p1, "temp"    # I

    .line 1799
    iget v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->totalTemp:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->totalTemp:I

    .line 1800
    return-void
.end method
