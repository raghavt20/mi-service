class com.android.server.BootKeeperStubImpl$BootKeeperOneTrack {
	 /* .source "BootKeeperStubImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/BootKeeperStubImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "BootKeeperOneTrack" */
} // .end annotation
/* # static fields */
private static final java.lang.String EXTRA_APP_ID;
private static final java.lang.String EXTRA_PACKAGE_NAME;
private static final Integer FLAG_NON_ANONYMOUS;
private static final Integer FLAG_NOT_LIMITED_BY_USER_EXPERIENCE_PLAN;
private static final java.lang.String INTENT_ACTION_ONETRACK;
private static final java.lang.String INTENT_PACKAGE_ONETRACK;
private static final java.lang.String RELEASE_SAPCE_ARG;
private static final java.lang.String RELEASE_SPACE_EVENT_NAME;
private static final Integer REPORT_LATENCY;
private static final java.lang.String TAG;
/* # direct methods */
private com.android.server.BootKeeperStubImpl$BootKeeperOneTrack ( ) {
/* .locals 0 */
/* .line 264 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 265 */
return;
} // .end method
static void lambda$reportReleaseSpaceOneTrack$0 ( android.content.Context p0, Boolean p1 ) { //synthethic
/* .locals 5 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "trigger" # Z */
/* .line 270 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "reportReleaseSpaceOneTrack context: "; // const-string v1, "reportReleaseSpaceOneTrack context: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "BootKeeperOneTrack"; // const-string v1, "BootKeeperOneTrack"
android.util.Slog .w ( v1,v0 );
/* .line 271 */
if ( p0 != null) { // if-eqz p0, :cond_1
	 /* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 274 */
	 } // :cond_0
	 /* new-instance v0, Landroid/content/Intent; */
	 final String v2 = "onetrack.action.TRACK_EVENT"; // const-string v2, "onetrack.action.TRACK_EVENT"
	 /* invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
	 /* .line 275 */
	 /* .local v0, "intent":Landroid/content/Intent; */
	 final String v2 = "com.miui.analytics"; // const-string v2, "com.miui.analytics"
	 (( android.content.Intent ) v0 ).setPackage ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
	 /* .line 276 */
	 final String v2 = "APP_ID"; // const-string v2, "APP_ID"
	 final String v3 = "31000401440"; // const-string v3, "31000401440"
	 (( android.content.Intent ) v0 ).putExtra ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
	 /* .line 277 */
	 final String v2 = "EVENT_NAME"; // const-string v2, "EVENT_NAME"
	 final String v3 = "release_space"; // const-string v3, "release_space"
	 (( android.content.Intent ) v0 ).putExtra ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
	 /* .line 278 */
	 final String v2 = "PACKAGE"; // const-string v2, "PACKAGE"
	 final String v4 = "com.android.server"; // const-string v4, "com.android.server"
	 (( android.content.Intent ) v0 ).putExtra ( v2, v4 ); // invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
	 /* .line 279 */
	 (( android.content.Intent ) v0 ).putExtra ( v3, p1 ); // invoke-virtual {v0, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
	 /* .line 280 */
	 int v2 = 3; // const/4 v2, 0x3
	 (( android.content.Intent ) v0 ).setFlags ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
	 /* .line 282 */
	 try { // :try_start_0
		 v2 = android.os.UserHandle.CURRENT;
		 (( android.content.Context ) p0 ).startServiceAsUser ( v0, v2 ); // invoke-virtual {p0, v0, v2}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
		 /* :try_end_0 */
		 /* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_1 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 285 */
		 /* :catch_0 */
		 /* move-exception v2 */
		 /* .line 286 */
		 /* .local v2, "e":Ljava/lang/Exception; */
		 final String v3 = "Unable to start service."; // const-string v3, "Unable to start service."
		 android.util.Slog .w ( v1,v3 );
		 /* .line 283 */
	 } // .end local v2 # "e":Ljava/lang/Exception;
	 /* :catch_1 */
	 /* move-exception v2 */
	 /* .line 284 */
	 /* .local v2, "e":Ljava/lang/IllegalStateException; */
	 final String v3 = "Failed to upload BootKeeper release space event."; // const-string v3, "Failed to upload BootKeeper release space event."
	 android.util.Slog .w ( v1,v3 );
	 /* .line 287 */
} // .end local v2 # "e":Ljava/lang/IllegalStateException;
} // :goto_0
/* nop */
/* .line 288 */
} // :goto_1
return;
/* .line 272 */
} // .end local v0 # "intent":Landroid/content/Intent;
} // :cond_1
} // :goto_2
return;
} // .end method
public static void reportReleaseSpaceOneTrack ( android.content.Context p0, Boolean p1 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "trigger" # Z */
/* .line 269 */
com.android.server.MiuiBgThread .getHandler ( );
/* new-instance v1, Lcom/android/server/BootKeeperStubImpl$BootKeeperOneTrack$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/BootKeeperStubImpl$BootKeeperOneTrack$$ExternalSyntheticLambda0;-><init>(Landroid/content/Context;Z)V */
/* const-wide/32 v2, 0xa4cb80 */
(( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 289 */
return;
} // .end method
