.class public Lcom/android/server/sensors/SensorServiceImpl;
.super Lcom/android/server/sensors/SensorServiceStub;
.source "SensorServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/sensors/SensorServiceImpl$DeviceStateListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SensorServiceImpl"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mSensorService:Lcom/android/server/sensors/SensorService;


# direct methods
.method static bridge synthetic -$$Nest$fgetmSensorService(Lcom/android/server/sensors/SensorServiceImpl;)Lcom/android/server/sensors/SensorService;
    .locals 0

    iget-object p0, p0, Lcom/android/server/sensors/SensorServiceImpl;->mSensorService:Lcom/android/server/sensors/SensorService;

    return-object p0
.end method

.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/android/server/sensors/SensorServiceStub;-><init>()V

    return-void
.end method


# virtual methods
.method public initialize(Landroid/content/Context;Lcom/android/server/sensors/SensorService;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "sensorService"    # Lcom/android/server/sensors/SensorService;

    .line 22
    iput-object p1, p0, Lcom/android/server/sensors/SensorServiceImpl;->mContext:Landroid/content/Context;

    .line 23
    iput-object p2, p0, Lcom/android/server/sensors/SensorServiceImpl;->mSensorService:Lcom/android/server/sensors/SensorService;

    .line 24
    return-void
.end method

.method public registerDeviceStateCallback()V
    .locals 5

    .line 28
    invoke-static {}, Lmiui/util/MiuiMultiDisplayTypeInfo;->isFoldDeviceInside()Z

    move-result v0

    if-nez v0, :cond_1

    .line 29
    invoke-static {}, Lmiui/util/MiuiMultiDisplayTypeInfo;->isFlipDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 30
    .local v0, "isFoldOrFlipDevice":Z
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isFoldOrFlipDevice: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SensorServiceImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    if-eqz v0, :cond_2

    .line 32
    iget-object v1, p0, Lcom/android/server/sensors/SensorServiceImpl;->mContext:Landroid/content/Context;

    const-class v2, Landroid/hardware/devicestate/DeviceStateManager;

    .line 33
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/devicestate/DeviceStateManager;

    .line 34
    .local v1, "deviceStateManager":Landroid/hardware/devicestate/DeviceStateManager;
    new-instance v2, Landroid/os/HandlerExecutor;

    .line 35
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/HandlerExecutor;-><init>(Landroid/os/Handler;)V

    new-instance v3, Lcom/android/server/sensors/SensorServiceImpl$DeviceStateListener;

    iget-object v4, p0, Lcom/android/server/sensors/SensorServiceImpl;->mContext:Landroid/content/Context;

    invoke-direct {v3, p0, v4}, Lcom/android/server/sensors/SensorServiceImpl$DeviceStateListener;-><init>(Lcom/android/server/sensors/SensorServiceImpl;Landroid/content/Context;)V

    .line 34
    invoke-virtual {v1, v2, v3}, Landroid/hardware/devicestate/DeviceStateManager;->registerCallback(Ljava/util/concurrent/Executor;Landroid/hardware/devicestate/DeviceStateManager$DeviceStateCallback;)V

    .line 38
    .end local v1    # "deviceStateManager":Landroid/hardware/devicestate/DeviceStateManager;
    :cond_2
    return-void
.end method
