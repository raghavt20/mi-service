class com.android.server.sensors.SensorServiceImpl$DeviceStateListener implements android.hardware.devicestate.DeviceStateManager$DeviceStateCallback {
	 /* .source "SensorServiceImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/sensors/SensorServiceImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "DeviceStateListener" */
} // .end annotation
/* # instance fields */
private final Integer REVERT_DEVICE_ACCELERATION;
private final Integer REVERT_DEVICE_DEVICE_ORIENT;
private final Integer REVERT_DEVICE_NO_SENSOR;
private final mDeviceOrientSensorReversedFoldedDeviceStates;
private final mReversedFoldedDeviceStates;
final com.android.server.sensors.SensorServiceImpl this$0; //synthetic
/* # direct methods */
public com.android.server.sensors.SensorServiceImpl$DeviceStateListener ( ) {
/* .locals 2 */
/* .param p1, "this$0" # Lcom/android/server/sensors/SensorServiceImpl; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 49 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 44 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/sensors/SensorServiceImpl$DeviceStateListener;->REVERT_DEVICE_NO_SENSOR:I */
/* .line 45 */
int v0 = 1; // const/4 v0, 0x1
/* iput v0, p0, Lcom/android/server/sensors/SensorServiceImpl$DeviceStateListener;->REVERT_DEVICE_DEVICE_ORIENT:I */
/* .line 46 */
int v0 = 2; // const/4 v0, 0x2
/* iput v0, p0, Lcom/android/server/sensors/SensorServiceImpl$DeviceStateListener;->REVERT_DEVICE_ACCELERATION:I */
/* .line 50 */
(( android.content.Context ) p2 ).getResources ( ); // invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x107003c */
(( android.content.res.Resources ) v0 ).getIntArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I
this.mDeviceOrientSensorReversedFoldedDeviceStates = v0;
/* .line 52 */
(( android.content.Context ) p2 ).getResources ( ); // invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x10700a4 */
(( android.content.res.Resources ) v0 ).getIntArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I
this.mReversedFoldedDeviceStates = v0;
/* .line 54 */
return;
} // .end method
/* # virtual methods */
public void onStateChanged ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "deviceState" # I */
/* .line 58 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "deviceState: "; // const-string v1, "deviceState: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SensorServiceImpl"; // const-string v1, "SensorServiceImpl"
android.util.Slog .i ( v1,v0 );
/* .line 59 */
int v0 = 0; // const/4 v0, 0x0
/* .line 60 */
/* .local v0, "reversedFlag":I */
v1 = this.mDeviceOrientSensorReversedFoldedDeviceStates;
v1 = com.android.internal.util.ArrayUtils .contains ( v1,p1 );
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 62 */
	 int v1 = 1; // const/4 v1, 0x1
} // :cond_0
/* move v1, v2 */
} // :goto_0
/* or-int/2addr v0, v1 */
/* .line 63 */
v1 = this.mReversedFoldedDeviceStates;
v1 = com.android.internal.util.ArrayUtils .contains ( v1,p1 );
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 64 */
int v2 = 2; // const/4 v2, 0x2
} // :cond_1
/* nop */
} // :goto_1
/* or-int/2addr v0, v2 */
/* .line 65 */
v1 = this.this$0;
com.android.server.sensors.SensorServiceImpl .-$$Nest$fgetmSensorService ( v1 );
(( com.android.server.sensors.SensorService ) v1 ).callReversedStateNative ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/sensors/SensorService;->callReversedStateNative(I)V
/* .line 66 */
v1 = this.this$0;
com.android.server.sensors.SensorServiceImpl .-$$Nest$fgetmSensorService ( v1 );
(( com.android.server.sensors.SensorService ) v1 ).callDeviceStateNative ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/sensors/SensorService;->callDeviceStateNative(I)V
/* .line 67 */
return;
} // .end method
