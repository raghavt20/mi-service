.class Lcom/android/server/sensors/SensorServiceImpl$DeviceStateListener;
.super Ljava/lang/Object;
.source "SensorServiceImpl.java"

# interfaces
.implements Landroid/hardware/devicestate/DeviceStateManager$DeviceStateCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/sensors/SensorServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DeviceStateListener"
.end annotation


# instance fields
.field private final REVERT_DEVICE_ACCELERATION:I

.field private final REVERT_DEVICE_DEVICE_ORIENT:I

.field private final REVERT_DEVICE_NO_SENSOR:I

.field private final mDeviceOrientSensorReversedFoldedDeviceStates:[I

.field private final mReversedFoldedDeviceStates:[I

.field final synthetic this$0:Lcom/android/server/sensors/SensorServiceImpl;


# direct methods
.method public constructor <init>(Lcom/android/server/sensors/SensorServiceImpl;Landroid/content/Context;)V
    .locals 2
    .param p1, "this$0"    # Lcom/android/server/sensors/SensorServiceImpl;
    .param p2, "context"    # Landroid/content/Context;

    .line 49
    iput-object p1, p0, Lcom/android/server/sensors/SensorServiceImpl$DeviceStateListener;->this$0:Lcom/android/server/sensors/SensorServiceImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/sensors/SensorServiceImpl$DeviceStateListener;->REVERT_DEVICE_NO_SENSOR:I

    .line 45
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/server/sensors/SensorServiceImpl$DeviceStateListener;->REVERT_DEVICE_DEVICE_ORIENT:I

    .line 46
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/server/sensors/SensorServiceImpl$DeviceStateListener;->REVERT_DEVICE_ACCELERATION:I

    .line 50
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x107003c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/sensors/SensorServiceImpl$DeviceStateListener;->mDeviceOrientSensorReversedFoldedDeviceStates:[I

    .line 52
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x10700a4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/sensors/SensorServiceImpl$DeviceStateListener;->mReversedFoldedDeviceStates:[I

    .line 54
    return-void
.end method


# virtual methods
.method public onStateChanged(I)V
    .locals 3
    .param p1, "deviceState"    # I

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "deviceState: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SensorServiceImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    const/4 v0, 0x0

    .line 60
    .local v0, "reversedFlag":I
    iget-object v1, p0, Lcom/android/server/sensors/SensorServiceImpl$DeviceStateListener;->mDeviceOrientSensorReversedFoldedDeviceStates:[I

    invoke-static {v1, p1}, Lcom/android/internal/util/ArrayUtils;->contains([II)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 62
    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_0
    or-int/2addr v0, v1

    .line 63
    iget-object v1, p0, Lcom/android/server/sensors/SensorServiceImpl$DeviceStateListener;->mReversedFoldedDeviceStates:[I

    invoke-static {v1, p1}, Lcom/android/internal/util/ArrayUtils;->contains([II)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 64
    const/4 v2, 0x2

    goto :goto_1

    :cond_1
    nop

    :goto_1
    or-int/2addr v0, v2

    .line 65
    iget-object v1, p0, Lcom/android/server/sensors/SensorServiceImpl$DeviceStateListener;->this$0:Lcom/android/server/sensors/SensorServiceImpl;

    invoke-static {v1}, Lcom/android/server/sensors/SensorServiceImpl;->-$$Nest$fgetmSensorService(Lcom/android/server/sensors/SensorServiceImpl;)Lcom/android/server/sensors/SensorService;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/sensors/SensorService;->callReversedStateNative(I)V

    .line 66
    iget-object v1, p0, Lcom/android/server/sensors/SensorServiceImpl$DeviceStateListener;->this$0:Lcom/android/server/sensors/SensorServiceImpl;

    invoke-static {v1}, Lcom/android/server/sensors/SensorServiceImpl;->-$$Nest$fgetmSensorService(Lcom/android/server/sensors/SensorServiceImpl;)Lcom/android/server/sensors/SensorService;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/server/sensors/SensorService;->callDeviceStateNative(I)V

    .line 67
    return-void
.end method
