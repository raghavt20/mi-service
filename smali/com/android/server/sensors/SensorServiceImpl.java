public class com.android.server.sensors.SensorServiceImpl extends com.android.server.sensors.SensorServiceStub {
	 /* .source "SensorServiceImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/sensors/SensorServiceImpl$DeviceStateListener; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
/* # instance fields */
private android.content.Context mContext;
private com.android.server.sensors.SensorService mSensorService;
/* # direct methods */
static com.android.server.sensors.SensorService -$$Nest$fgetmSensorService ( com.android.server.sensors.SensorServiceImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mSensorService;
} // .end method
public com.android.server.sensors.SensorServiceImpl ( ) {
	 /* .locals 0 */
	 /* .line 15 */
	 /* invoke-direct {p0}, Lcom/android/server/sensors/SensorServiceStub;-><init>()V */
	 return;
} // .end method
/* # virtual methods */
public void initialize ( android.content.Context p0, com.android.server.sensors.SensorService p1 ) {
	 /* .locals 0 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .param p2, "sensorService" # Lcom/android/server/sensors/SensorService; */
	 /* .line 22 */
	 this.mContext = p1;
	 /* .line 23 */
	 this.mSensorService = p2;
	 /* .line 24 */
	 return;
} // .end method
public void registerDeviceStateCallback ( ) {
	 /* .locals 5 */
	 /* .line 28 */
	 v0 = 	 miui.util.MiuiMultiDisplayTypeInfo .isFoldDeviceInside ( );
	 /* if-nez v0, :cond_1 */
	 /* .line 29 */
	 v0 = 	 miui.util.MiuiMultiDisplayTypeInfo .isFlipDevice ( );
	 if ( v0 != null) { // if-eqz v0, :cond_0
	 } // :cond_0
	 int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 30 */
/* .local v0, "isFoldOrFlipDevice":Z */
} // :goto_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "isFoldOrFlipDevice: "; // const-string v2, "isFoldOrFlipDevice: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "SensorServiceImpl"; // const-string v2, "SensorServiceImpl"
android.util.Slog .i ( v2,v1 );
/* .line 31 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 32 */
v1 = this.mContext;
/* const-class v2, Landroid/hardware/devicestate/DeviceStateManager; */
/* .line 33 */
(( android.content.Context ) v1 ).getSystemService ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v1, Landroid/hardware/devicestate/DeviceStateManager; */
/* .line 34 */
/* .local v1, "deviceStateManager":Landroid/hardware/devicestate/DeviceStateManager; */
/* new-instance v2, Landroid/os/HandlerExecutor; */
/* .line 35 */
com.android.internal.os.BackgroundThread .getHandler ( );
/* invoke-direct {v2, v3}, Landroid/os/HandlerExecutor;-><init>(Landroid/os/Handler;)V */
/* new-instance v3, Lcom/android/server/sensors/SensorServiceImpl$DeviceStateListener; */
v4 = this.mContext;
/* invoke-direct {v3, p0, v4}, Lcom/android/server/sensors/SensorServiceImpl$DeviceStateListener;-><init>(Lcom/android/server/sensors/SensorServiceImpl;Landroid/content/Context;)V */
/* .line 34 */
(( android.hardware.devicestate.DeviceStateManager ) v1 ).registerCallback ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/hardware/devicestate/DeviceStateManager;->registerCallback(Ljava/util/concurrent/Executor;Landroid/hardware/devicestate/DeviceStateManager$DeviceStateCallback;)V
/* .line 38 */
} // .end local v1 # "deviceStateManager":Landroid/hardware/devicestate/DeviceStateManager;
} // :cond_2
return;
} // .end method
