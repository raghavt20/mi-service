.class public Lcom/android/server/ForceDarkAppListManager;
.super Ljava/lang/Object;
.source "ForceDarkAppListManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/ForceDarkAppListManager$AppChangedReceiver;,
        Lcom/android/server/ForceDarkAppListManager$LocaleChangedReceiver;
    }
.end annotation


# instance fields
.field private mAppListCacheRef:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference<",
            "Ljava/util/ArrayList<",
            "Landroid/content/pm/PackageInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mDarkModeAppData:Lcom/miui/darkmode/DarkModeAppData;

.field private mForceDarkAppListProvider:Lcom/android/server/ForceDarkAppListProvider;

.field private mForceDarkAppSettings:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/android/server/DarkModeAppSettingsInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mForceDarkUiModeModeManager:Lcom/android/server/ForceDarkUiModeModeManager;

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field private mSecurityManagerService:Lmiui/security/SecurityManagerInternal;


# direct methods
.method static bridge synthetic -$$Nest$fgetmForceDarkAppListProvider(Lcom/android/server/ForceDarkAppListManager;)Lcom/android/server/ForceDarkAppListProvider;
    .locals 0

    iget-object p0, p0, Lcom/android/server/ForceDarkAppListManager;->mForceDarkAppListProvider:Lcom/android/server/ForceDarkAppListProvider;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmForceDarkUiModeModeManager(Lcom/android/server/ForceDarkAppListManager;)Lcom/android/server/ForceDarkUiModeModeManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/ForceDarkAppListManager;->mForceDarkUiModeModeManager:Lcom/android/server/ForceDarkUiModeModeManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmForceDarkAppSettings(Lcom/android/server/ForceDarkAppListManager;Ljava/util/HashMap;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/ForceDarkAppListManager;->mForceDarkAppSettings:Ljava/util/HashMap;

    return-void
.end method

.method static bridge synthetic -$$Nest$mclearCache(Lcom/android/server/ForceDarkAppListManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/ForceDarkAppListManager;->clearCache()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/server/ForceDarkUiModeModeManager;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "forceDarkUiModeModeManager"    # Lcom/android/server/ForceDarkUiModeModeManager;

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/lang/ref/SoftReference;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/server/ForceDarkAppListManager;->mAppListCacheRef:Ljava/lang/ref/SoftReference;

    .line 51
    new-instance v0, Lcom/miui/darkmode/DarkModeAppData;

    invoke-direct {v0}, Lcom/miui/darkmode/DarkModeAppData;-><init>()V

    iput-object v0, p0, Lcom/android/server/ForceDarkAppListManager;->mDarkModeAppData:Lcom/miui/darkmode/DarkModeAppData;

    .line 55
    invoke-static {}, Lcom/android/server/ForceDarkAppListProvider;->getInstance()Lcom/android/server/ForceDarkAppListProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ForceDarkAppListManager;->mForceDarkAppListProvider:Lcom/android/server/ForceDarkAppListProvider;

    .line 56
    invoke-virtual {v0}, Lcom/android/server/ForceDarkAppListProvider;->getForceDarkAppSettings()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ForceDarkAppListManager;->mForceDarkAppSettings:Ljava/util/HashMap;

    .line 57
    iget-object v0, p0, Lcom/android/server/ForceDarkAppListManager;->mForceDarkAppListProvider:Lcom/android/server/ForceDarkAppListProvider;

    new-instance v1, Lcom/android/server/ForceDarkAppListManager$1;

    invoke-direct {v1, p0}, Lcom/android/server/ForceDarkAppListManager$1;-><init>(Lcom/android/server/ForceDarkAppListManager;)V

    invoke-virtual {v0, v1}, Lcom/android/server/ForceDarkAppListProvider;->setAppListChangeListener(Lcom/android/server/ForceDarkAppListProvider$ForceDarkAppListChangeListener;)V

    .line 65
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ForceDarkAppListManager;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 66
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 67
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 68
    const-string v1, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 69
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 70
    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 71
    new-instance v1, Lcom/android/server/ForceDarkAppListManager$AppChangedReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/server/ForceDarkAppListManager$AppChangedReceiver;-><init>(Lcom/android/server/ForceDarkAppListManager;Lcom/android/server/ForceDarkAppListManager$AppChangedReceiver-IA;)V

    const/4 v3, 0x2

    invoke-virtual {p1, v1, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    .line 73
    new-instance v1, Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.LOCALE_CHANGED"

    invoke-direct {v1, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    .line 74
    new-instance v1, Lcom/android/server/ForceDarkAppListManager$LocaleChangedReceiver;

    invoke-direct {v1, p0, v2}, Lcom/android/server/ForceDarkAppListManager$LocaleChangedReceiver;-><init>(Lcom/android/server/ForceDarkAppListManager;Lcom/android/server/ForceDarkAppListManager$LocaleChangedReceiver-IA;)V

    invoke-virtual {p1, v1, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    .line 75
    iput-object p1, p0, Lcom/android/server/ForceDarkAppListManager;->mContext:Landroid/content/Context;

    .line 76
    iput-object p2, p0, Lcom/android/server/ForceDarkAppListManager;->mForceDarkUiModeModeManager:Lcom/android/server/ForceDarkUiModeModeManager;

    .line 77
    return-void
.end method

.method private clearCache()V
    .locals 3

    .line 199
    iget-object v0, p0, Lcom/android/server/ForceDarkAppListManager;->mDarkModeAppData:Lcom/miui/darkmode/DarkModeAppData;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/miui/darkmode/DarkModeAppData;->setCreateTime(J)V

    .line 200
    iget-object v0, p0, Lcom/android/server/ForceDarkAppListManager;->mAppListCacheRef:Ljava/lang/ref/SoftReference;

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->clear()V

    .line 201
    return-void
.end method

.method private getAppInfo(Lcom/android/server/DarkModeAppSettingsInfo;Landroid/content/pm/ApplicationInfo;I)Lcom/miui/darkmode/DarkModeAppDetailInfo;
    .locals 4
    .param p1, "darkModeAppSettingsInfo"    # Lcom/android/server/DarkModeAppSettingsInfo;
    .param p2, "applicationInfo"    # Landroid/content/pm/ApplicationInfo;
    .param p3, "userId"    # I

    .line 189
    iget-object v0, p0, Lcom/android/server/ForceDarkAppListManager;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {p2, v0}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 190
    .local v0, "label":Ljava/lang/String;
    new-instance v1, Lcom/miui/darkmode/DarkModeAppDetailInfo;

    invoke-direct {v1}, Lcom/miui/darkmode/DarkModeAppDetailInfo;-><init>()V

    .line 191
    .local v1, "appInfo":Lcom/miui/darkmode/DarkModeAppDetailInfo;
    invoke-virtual {v1, v0}, Lcom/miui/darkmode/DarkModeAppDetailInfo;->setLabel(Ljava/lang/String;)V

    .line 192
    iget-object v2, p2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/miui/darkmode/DarkModeAppDetailInfo;->setPkgName(Ljava/lang/String;)V

    .line 193
    iget-object v2, p0, Lcom/android/server/ForceDarkAppListManager;->mSecurityManagerService:Lmiui/security/SecurityManagerInternal;

    iget-object v3, p2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3, p3}, Lmiui/security/SecurityManagerInternal;->getAppDarkModeForUser(Ljava/lang/String;I)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/miui/darkmode/DarkModeAppDetailInfo;->setEnabled(Z)V

    .line 194
    invoke-virtual {p1}, Lcom/android/server/DarkModeAppSettingsInfo;->getAdaptStat()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/miui/darkmode/DarkModeAppDetailInfo;->setAdaptStatus(I)V

    .line 195
    return-object v1
.end method

.method private getInstalledPkgList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/content/pm/PackageInfo;",
            ">;"
        }
    .end annotation

    .line 210
    iget-object v0, p0, Lcom/android/server/ForceDarkAppListManager;->mAppListCacheRef:Ljava/lang/ref/SoftReference;

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 211
    .local v0, "appList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/PackageInfo;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 212
    return-object v0

    .line 214
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object v0, v1

    .line 215
    iget-object v1, p0, Lcom/android/server/ForceDarkAppListManager;->mPackageManager:Landroid/content/pm/PackageManager;

    const/16 v2, 0x40

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 216
    new-instance v1, Ljava/lang/ref/SoftReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/android/server/ForceDarkAppListManager;->mAppListCacheRef:Ljava/lang/ref/SoftReference;

    .line 217
    return-object v0
.end method

.method private shouldShowInSettings(Landroid/content/pm/ApplicationInfo;)Z
    .locals 2
    .param p1, "info"    # Landroid/content/pm/ApplicationInfo;

    .line 268
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/pm/ApplicationInfo;->isSystemApp()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p1, Landroid/content/pm/ApplicationInfo;->uid:I

    const/16 v1, 0x2710

    if-le v0, v1, :cond_0

    iget-object v0, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 269
    const-string v1, "com.miui."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-string v1, "com.xiaomi."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 270
    const-string v1, "com.mi."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-string v1, "com.google."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 268
    :goto_0
    return v0
.end method


# virtual methods
.method public getAllForceDarkMapForSplashScreen()Ljava/util/HashMap;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 226
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 228
    .local v0, "result":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v1, p0, Lcom/android/server/ForceDarkAppListManager;->mForceDarkAppSettings:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 229
    .local v2, "settingsInfo":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/DarkModeAppSettingsInfo;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/DarkModeAppSettingsInfo;

    invoke-virtual {v3}, Lcom/android/server/DarkModeAppSettingsInfo;->getForceDarkSplashScreen()I

    move-result v3

    .line 230
    .local v3, "forceDarkSplashScreenValue":I
    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    if-eqz v3, :cond_0

    .line 232
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    .end local v2    # "settingsInfo":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/DarkModeAppSettingsInfo;>;"
    .end local v3    # "forceDarkSplashScreenValue":I
    :cond_0
    goto :goto_0

    .line 235
    :cond_1
    return-object v0
.end method

.method public getAppDarkModeEnable(Ljava/lang/String;I)Z
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 106
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 107
    return v1

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/android/server/ForceDarkAppListManager;->mForceDarkAppSettings:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/DarkModeAppSettingsInfo;

    .line 110
    .local v0, "darkModeAppSettingsInfo":Lcom/android/server/DarkModeAppSettingsInfo;
    if-eqz v0, :cond_2

    .line 111
    invoke-virtual {v0}, Lcom/android/server/DarkModeAppSettingsInfo;->isShowInSettings()Z

    move-result v2

    if-nez v2, :cond_2

    .line 112
    invoke-virtual {v0}, Lcom/android/server/DarkModeAppSettingsInfo;->getOverrideEnableValue()I

    move-result v2

    .line 113
    .local v2, "overrideEnableValue":I
    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 114
    return v3

    .line 115
    :cond_1
    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 116
    return v1

    .line 120
    .end local v2    # "overrideEnableValue":I
    :cond_2
    iget-object v1, p0, Lcom/android/server/ForceDarkAppListManager;->mSecurityManagerService:Lmiui/security/SecurityManagerInternal;

    invoke-virtual {v1, p1, p2}, Lmiui/security/SecurityManagerInternal;->getAppDarkModeForUser(Ljava/lang/String;I)Z

    move-result v1

    return v1
.end method

.method public getAppForceDarkOrigin(Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .line 124
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 125
    return v1

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/android/server/ForceDarkAppListManager;->mForceDarkAppSettings:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/DarkModeAppSettingsInfo;

    .line 129
    .local v0, "darkModeAppSettingsInfo":Lcom/android/server/DarkModeAppSettingsInfo;
    if-eqz v0, :cond_1

    .line 130
    invoke-virtual {v0}, Lcom/android/server/DarkModeAppSettingsInfo;->isForceDarkOrigin()Z

    move-result v1

    return v1

    .line 133
    :cond_1
    iget-object v2, p0, Lcom/android/server/ForceDarkAppListManager;->mDarkModeAppData:Lcom/miui/darkmode/DarkModeAppData;

    invoke-virtual {v2, p1}, Lcom/miui/darkmode/DarkModeAppData;->getInfoWithPackageName(Ljava/lang/String;)Lcom/miui/darkmode/DarkModeAppDetailInfo;

    move-result-object v2

    if-nez v2, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public getDarkModeAppList(JI)Lcom/miui/darkmode/DarkModeAppData;
    .locals 7
    .param p1, "appLastUpdateTime"    # J
    .param p3, "userId"    # I

    .line 160
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/android/server/ForceDarkAppListManager;->mDarkModeAppData:Lcom/miui/darkmode/DarkModeAppData;

    invoke-virtual {v0}, Lcom/miui/darkmode/DarkModeAppData;->getCreateTime()J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 162
    const/4 v0, 0x0

    return-object v0

    .line 165
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 167
    .local v0, "darkModeAppDetailInfos":Ljava/util/List;, "Ljava/util/List<Lcom/miui/darkmode/DarkModeAppDetailInfo;>;"
    invoke-direct {p0}, Lcom/android/server/ForceDarkAppListManager;->getInstalledPkgList()Ljava/util/List;

    move-result-object v1

    .line 168
    .local v1, "installedPkgList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/PackageInfo;

    .line 170
    .local v3, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-object v4, p0, Lcom/android/server/ForceDarkAppListManager;->mForceDarkAppSettings:Ljava/util/HashMap;

    iget-object v5, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/DarkModeAppSettingsInfo;

    .line 171
    .local v4, "darkModeAppSettingsInfo":Lcom/android/server/DarkModeAppSettingsInfo;
    if-eqz v4, :cond_1

    .line 172
    invoke-virtual {v4}, Lcom/android/server/DarkModeAppSettingsInfo;->isShowInSettings()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 173
    iget-object v5, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 174
    invoke-direct {p0, v4, v5, p3}, Lcom/android/server/ForceDarkAppListManager;->getAppInfo(Lcom/android/server/DarkModeAppSettingsInfo;Landroid/content/pm/ApplicationInfo;I)Lcom/miui/darkmode/DarkModeAppDetailInfo;

    move-result-object v5

    .line 175
    .local v5, "darkModeAppDetailInfo":Lcom/miui/darkmode/DarkModeAppDetailInfo;
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    .end local v5    # "darkModeAppDetailInfo":Lcom/miui/darkmode/DarkModeAppDetailInfo;
    goto :goto_1

    .line 177
    :cond_1
    sget-boolean v5, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v5, :cond_2

    iget-object v5, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-direct {p0, v5}, Lcom/android/server/ForceDarkAppListManager;->shouldShowInSettings(Landroid/content/pm/ApplicationInfo;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 178
    new-instance v5, Lcom/android/server/DarkModeAppSettingsInfo;

    invoke-direct {v5}, Lcom/android/server/DarkModeAppSettingsInfo;-><init>()V

    iget-object v6, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 179
    invoke-direct {p0, v5, v6, p3}, Lcom/android/server/ForceDarkAppListManager;->getAppInfo(Lcom/android/server/DarkModeAppSettingsInfo;Landroid/content/pm/ApplicationInfo;I)Lcom/miui/darkmode/DarkModeAppDetailInfo;

    move-result-object v5

    .line 180
    .restart local v5    # "darkModeAppDetailInfo":Lcom/miui/darkmode/DarkModeAppDetailInfo;
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 182
    .end local v3    # "packageInfo":Landroid/content/pm/PackageInfo;
    .end local v4    # "darkModeAppSettingsInfo":Lcom/android/server/DarkModeAppSettingsInfo;
    .end local v5    # "darkModeAppDetailInfo":Lcom/miui/darkmode/DarkModeAppDetailInfo;
    :cond_2
    :goto_1
    goto :goto_0

    .line 183
    :cond_3
    iget-object v2, p0, Lcom/android/server/ForceDarkAppListManager;->mDarkModeAppData:Lcom/miui/darkmode/DarkModeAppData;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/miui/darkmode/DarkModeAppData;->setCreateTime(J)V

    .line 184
    iget-object v2, p0, Lcom/android/server/ForceDarkAppListManager;->mDarkModeAppData:Lcom/miui/darkmode/DarkModeAppData;

    invoke-virtual {v2, v0}, Lcom/miui/darkmode/DarkModeAppData;->setDarkModeAppDetailInfoList(Ljava/util/List;)V

    .line 185
    iget-object v2, p0, Lcom/android/server/ForceDarkAppListManager;->mDarkModeAppData:Lcom/miui/darkmode/DarkModeAppData;

    return-object v2
.end method

.method public getInterceptRelaunchType(Ljava/lang/String;I)I
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 137
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x2

    if-eqz v0, :cond_0

    .line 138
    return v1

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/android/server/ForceDarkAppListManager;->mForceDarkAppSettings:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/DarkModeAppSettingsInfo;

    .line 142
    .local v0, "settingsInfo":Lcom/android/server/DarkModeAppSettingsInfo;
    if-nez v0, :cond_1

    .line 143
    return v1

    .line 146
    :cond_1
    invoke-virtual {v0}, Lcom/android/server/DarkModeAppSettingsInfo;->getInterceptRelaunch()I

    move-result v2

    .line 147
    .local v2, "interceptRelaunchType":I
    invoke-virtual {v0}, Lcom/android/server/DarkModeAppSettingsInfo;->getInterceptRelaunch()I

    move-result v3

    if-eqz v3, :cond_2

    .line 148
    return v2

    .line 152
    :cond_2
    invoke-virtual {v0}, Lcom/android/server/DarkModeAppSettingsInfo;->isShowInSettings()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/server/ForceDarkAppListManager;->mSecurityManagerService:Lmiui/security/SecurityManagerInternal;

    invoke-virtual {v3, p1, p2}, Lmiui/security/SecurityManagerInternal;->getAppDarkModeForUser(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 153
    const/4 v1, 0x1

    return v1

    .line 156
    :cond_3
    return v1
.end method

.method public onBootPhase(ILandroid/content/Context;)V
    .locals 1
    .param p1, "phase"    # I
    .param p2, "context"    # Landroid/content/Context;

    .line 81
    const-class v0, Lmiui/security/SecurityManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/security/SecurityManagerInternal;

    iput-object v0, p0, Lcom/android/server/ForceDarkAppListManager;->mSecurityManagerService:Lmiui/security/SecurityManagerInternal;

    .line 82
    iget-object v0, p0, Lcom/android/server/ForceDarkAppListManager;->mForceDarkAppListProvider:Lcom/android/server/ForceDarkAppListProvider;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/ForceDarkAppListProvider;->onBootPhase(ILandroid/content/Context;)V

    .line 83
    return-void
.end method

.method public setAppDarkModeEnable(Ljava/lang/String;ZI)V
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "enable"    # Z
    .param p3, "userId"    # I

    .line 86
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    return-void

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/android/server/ForceDarkAppListManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "last_app_dark_mode_pkg"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 92
    iget-object v0, p0, Lcom/android/server/ForceDarkAppListManager;->mSecurityManagerService:Lmiui/security/SecurityManagerInternal;

    invoke-virtual {v0, p1, p2, p3}, Lmiui/security/SecurityManagerInternal;->setAppDarkModeForUser(Ljava/lang/String;ZI)V

    .line 93
    iget-object v0, p0, Lcom/android/server/ForceDarkAppListManager;->mDarkModeAppData:Lcom/miui/darkmode/DarkModeAppData;

    if-eqz v0, :cond_2

    .line 94
    invoke-virtual {v0}, Lcom/miui/darkmode/DarkModeAppData;->getDarkModeAppDetailInfoList()Ljava/util/List;

    move-result-object v0

    .line 95
    .local v0, "darkModeAppDetailInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/darkmode/DarkModeAppDetailInfo;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 96
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/darkmode/DarkModeAppDetailInfo;

    invoke-virtual {v2}, Lcom/miui/darkmode/DarkModeAppDetailInfo;->getPkgName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 97
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/darkmode/DarkModeAppDetailInfo;

    invoke-virtual {v2, p2}, Lcom/miui/darkmode/DarkModeAppDetailInfo;->setEnabled(Z)V

    .line 98
    iget-object v2, p0, Lcom/android/server/ForceDarkAppListManager;->mDarkModeAppData:Lcom/miui/darkmode/DarkModeAppData;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/miui/darkmode/DarkModeAppData;->setCreateTime(J)V

    .line 99
    goto :goto_1

    .line 95
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 103
    .end local v0    # "darkModeAppDetailInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/darkmode/DarkModeAppDetailInfo;>;"
    .end local v1    # "i":I
    :cond_2
    :goto_1
    return-void
.end method
