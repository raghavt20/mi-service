.class public final Lcom/android/server/AppOpsServiceStubImpl$Callback;
.super Ljava/lang/Object;
.source "AppOpsServiceStubImpl.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/AppOpsServiceStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "Callback"
.end annotation


# instance fields
.field final mCallback:Landroid/os/IBinder;

.field volatile mUnLink:Z

.field final mUserId:I

.field final synthetic this$0:Lcom/android/server/AppOpsServiceStubImpl;


# direct methods
.method public constructor <init>(Lcom/android/server/AppOpsServiceStubImpl;Landroid/os/IBinder;I)V
    .locals 3
    .param p1, "this$0"    # Lcom/android/server/AppOpsServiceStubImpl;
    .param p2, "callback"    # Landroid/os/IBinder;
    .param p3, "userId"    # I

    .line 390
    const-string v0, "AppOpsServiceStubImpl"

    iput-object p1, p0, Lcom/android/server/AppOpsServiceStubImpl$Callback;->this$0:Lcom/android/server/AppOpsServiceStubImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 391
    iput-object p2, p0, Lcom/android/server/AppOpsServiceStubImpl$Callback;->mCallback:Landroid/os/IBinder;

    .line 392
    iput p3, p0, Lcom/android/server/AppOpsServiceStubImpl$Callback;->mUserId:I

    .line 394
    const/4 v1, 0x0

    :try_start_0
    invoke-interface {p2, p0, v1}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    .line 395
    const-string v1, "linkToDeath"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 398
    goto :goto_0

    .line 396
    :catch_0
    move-exception v1

    .line 397
    .local v1, "e":Landroid/os/RemoteException;
    const-string v2, "linkToDeath failed!"

    invoke-static {v0, v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 399
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method


# virtual methods
.method public binderDied()V
    .locals 2

    .line 415
    invoke-virtual {p0}, Lcom/android/server/AppOpsServiceStubImpl$Callback;->unlinkToDeath()V

    .line 416
    iget-object v0, p0, Lcom/android/server/AppOpsServiceStubImpl$Callback;->this$0:Lcom/android/server/AppOpsServiceStubImpl;

    iget v1, p0, Lcom/android/server/AppOpsServiceStubImpl$Callback;->mUserId:I

    invoke-static {v0, v1}, Lcom/android/server/AppOpsServiceStubImpl;->-$$Nest$mstartService(Lcom/android/server/AppOpsServiceStubImpl;I)V

    .line 417
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "binderDied mUserId : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/AppOpsServiceStubImpl$Callback;->mUserId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AppOpsServiceStubImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 418
    return-void
.end method

.method public unlinkToDeath()V
    .locals 3

    .line 402
    iget-boolean v0, p0, Lcom/android/server/AppOpsServiceStubImpl$Callback;->mUnLink:Z

    if-eqz v0, :cond_0

    .line 403
    return-void

    .line 406
    :cond_0
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/android/server/AppOpsServiceStubImpl$Callback;->mUnLink:Z

    .line 407
    iget-object v0, p0, Lcom/android/server/AppOpsServiceStubImpl$Callback;->mCallback:Landroid/os/IBinder;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 410
    goto :goto_0

    .line 408
    :catch_0
    move-exception v0

    .line 409
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "AppOpsServiceStubImpl"

    const-string/jumbo v2, "unlinkToDeath failed!"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 411
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method
