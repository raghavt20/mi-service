public class com.android.server.MiuiRestoreManagerService extends miui.app.backup.IMiuiRestoreManager$Stub {
	 /* .source "MiuiRestoreManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;, */
	 /* Lcom/android/server/MiuiRestoreManagerService$BackupDataTask;, */
	 /* Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;, */
	 /* Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;, */
	 /* Lcom/android/server/MiuiRestoreManagerService$GetDataFileInfoTask;, */
	 /* Lcom/android/server/MiuiRestoreManagerService$ListDataDirTask;, */
	 /* Lcom/android/server/MiuiRestoreManagerService$FileChangedException;, */
	 /* Lcom/android/server/MiuiRestoreManagerService$BadSeInfoException;, */
	 /* Lcom/android/server/MiuiRestoreManagerService$QueryAppInfoErrorException;, */
	 /* Lcom/android/server/MiuiRestoreManagerService$BadFileDescriptorException;, */
	 /* Lcom/android/server/MiuiRestoreManagerService$Lifecycle; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer DEFAULT_THREAD_COUNT;
private static final Integer ERROR;
private static final Integer ERROR_CODE_BAD_SEINFO;
private static final Integer ERROR_CODE_INVOKE_FAIL;
private static final Integer ERROR_CODE_NO_APP_INFO;
private static final Integer ERROR_FILE_CHANGED;
private static final Integer ERROR_NONE;
private static final Integer FLAG_MOVE_COPY_MODE;
private static final Integer FLAG_MOVE_RENAME_MODE;
private static final Integer FLAG_MOVE_WITH_LOCK;
private static final java.lang.String PACKAGE_BACKUP;
private static final java.lang.String PACKAGE_CLOUD_BACKUP;
private static final java.lang.String PACKAGE_HUANJI;
public static final java.lang.String SERVICE_NAME;
private static final java.lang.String TAG;
/* # instance fields */
private final java.util.Map mAppDataRootPathMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.content.Context mContext;
private com.android.server.pm.Installer mInstaller;
private java.util.concurrent.Executor mInstallerExecutor;
private java.util.concurrent.Executor mListDataExecutor;
private final android.os.RemoteCallbackList mRestoreObservers;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/os/RemoteCallbackList<", */
/* "Lmiui/app/backup/IRestoreListener;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.concurrent.Executor mTransferExecutor;
/* # direct methods */
static com.android.server.pm.Installer -$$Nest$fgetmInstaller ( com.android.server.MiuiRestoreManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mInstaller;
} // .end method
static void -$$Nest$mnotifyMoveTaskEnd ( com.android.server.MiuiRestoreManagerService p0, java.lang.String p1, Integer p2, Integer p3 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/MiuiRestoreManagerService;->notifyMoveTaskEnd(Ljava/lang/String;II)V */
return;
} // .end method
public com.android.server.MiuiRestoreManagerService ( ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "installer" # Lcom/android/server/pm/Installer; */
/* .line 99 */
/* invoke-direct {p0}, Lmiui/app/backup/IMiuiRestoreManager$Stub;-><init>()V */
/* .line 109 */
/* new-instance v0, Landroid/os/RemoteCallbackList; */
/* invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V */
this.mRestoreObservers = v0;
/* .line 100 */
final String v0 = "MiuiRestoreManagerService"; // const-string v0, "MiuiRestoreManagerService"
final String v1 = "init MiuiRestoreManagerService"; // const-string v1, "init MiuiRestoreManagerService"
android.util.Slog .d ( v0,v1 );
/* .line 101 */
this.mContext = p1;
/* .line 102 */
this.mInstaller = p2;
/* .line 103 */
int v0 = 3; // const/4 v0, 0x3
java.util.concurrent.Executors .newFixedThreadPool ( v0 );
this.mInstallerExecutor = v0;
/* .line 104 */
java.util.concurrent.Executors .newSingleThreadExecutor ( );
this.mListDataExecutor = v0;
/* .line 105 */
int v0 = 5; // const/4 v0, 0x5
java.util.concurrent.Executors .newFixedThreadPool ( v0 );
this.mTransferExecutor = v0;
/* .line 106 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mAppDataRootPathMap = v0;
/* .line 107 */
return;
} // .end method
private Boolean checkIsLegalPackageAndCallingUid ( Integer p0, java.lang.String p1 ) {
/* .locals 7 */
/* .param p1, "callingUid" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 372 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
android.app.AppGlobals .getPackageManager ( );
/* .line 373 */
/* .local v1, "packages":[Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 374 */
/* array-length v2, v1 */
/* move v3, v0 */
} // :goto_0
/* if-ge v3, v2, :cond_1 */
/* aget-object v4, v1, v3 */
/* .line 375 */
/* .local v4, "pkgName":Ljava/lang/String; */
v5 = android.text.TextUtils .equals ( p2,v4 );
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 376 */
android.app.AppGlobals .getPackageManager ( );
v5 = v6 = android.os.Process .myUid ( );
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* if-nez v5, :cond_0 */
/* .line 377 */
int v0 = 1; // const/4 v0, 0x1
/* .line 374 */
} // .end local v4 # "pkgName":Ljava/lang/String;
} // :cond_0
/* add-int/lit8 v3, v3, 0x1 */
/* .line 383 */
} // .end local v1 # "packages":[Ljava/lang/String;
} // :cond_1
/* .line 381 */
/* :catch_0 */
/* move-exception v1 */
/* .line 382 */
/* .local v1, "e":Landroid/os/RemoteException; */
final String v2 = "MiuiRestoreManagerService"; // const-string v2, "MiuiRestoreManagerService"
final String v3 = "get package info fail "; // const-string v3, "get package info fail "
android.util.Slog .e ( v2,v3,v1 );
/* .line 385 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_1
} // .end method
private com.android.server.MiuiRestoreManagerService$AppDataRootPath getAppDataRootPath ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "deviceUid" # I */
/* .line 181 */
v0 = this.mAppDataRootPathMap;
java.lang.Integer .valueOf ( p1 );
/* check-cast v0, Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath; */
/* .line 182 */
/* .local v0, "rootPath":Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath; */
/* if-nez v0, :cond_0 */
/* .line 183 */
com.android.server.MiuiRestoreManagerService .getExternalAppDataRootPathOrNull ( p1 );
/* .line 184 */
/* .local v1, "external":Ljava/lang/String; */
v2 = this.mContext;
com.android.server.MiuiRestoreManagerService .getInternalAppDataRootPathOrNull ( v2,p1 );
/* .line 185 */
/* .local v2, "internal":Ljava/lang/String; */
/* new-instance v3, Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath; */
/* invoke-direct {v3, v1, v2}, Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* move-object v0, v3 */
/* .line 186 */
v3 = this.mAppDataRootPathMap;
java.lang.Integer .valueOf ( p1 );
/* .line 187 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "app data root\uff1a"; // const-string v4, "app data root\uff1a"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "MiuiRestoreManagerService"; // const-string v4, "MiuiRestoreManagerService"
android.util.Slog .w ( v4,v3 );
/* .line 189 */
} // .end local v1 # "external":Ljava/lang/String;
} // .end local v2 # "internal":Ljava/lang/String;
} // :cond_0
} // .end method
private static java.lang.String getExternalAppDataRootPathOrNull ( Integer p0 ) {
/* .locals 4 */
/* .param p0, "deviceUid" # I */
/* .line 193 */
/* new-instance v0, Landroid/os/Environment$UserEnvironment; */
/* invoke-direct {v0, p0}, Landroid/os/Environment$UserEnvironment;-><init>(I)V */
/* .line 194 */
/* .local v0, "environment":Landroid/os/Environment$UserEnvironment; */
final String v1 = "com.miui.cloudbackup"; // const-string v1, "com.miui.cloudbackup"
(( android.os.Environment$UserEnvironment ) v0 ).buildExternalStorageAppDataDirs ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Environment$UserEnvironment;->buildExternalStorageAppDataDirs(Ljava/lang/String;)[Ljava/io/File;
/* .line 195 */
/* .local v1, "files":[Ljava/io/File; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* array-length v2, v1 */
/* if-lez v2, :cond_0 */
/* .line 196 */
int v2 = 0; // const/4 v2, 0x0
/* aget-object v2, v1, v2 */
(( java.io.File ) v2 ).getParentFile ( ); // invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;
/* .line 197 */
/* .local v2, "appDataParentFile":Ljava/io/File; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 198 */
(( java.io.File ) v2 ).getPath ( ); // invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;
/* .line 201 */
} // .end local v2 # "appDataParentFile":Ljava/io/File;
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // .end method
private static java.lang.String getInternalAppDataRootPathOrNull ( android.content.Context p0, Integer p1 ) {
/* .locals 3 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "deviceUid" # I */
/* .line 206 */
try { // :try_start_0
final String v0 = "com.miui.cloudbackup"; // const-string v0, "com.miui.cloudbackup"
/* .line 209 */
android.os.UserHandle .of ( p1 );
/* .line 206 */
int v2 = 2; // const/4 v2, 0x2
(( android.content.Context ) p0 ).createPackageContextAsUser ( v0, v2, v1 ); // invoke-virtual {p0, v0, v2, v1}, Landroid/content/Context;->createPackageContextAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)Landroid/content/Context;
/* .line 210 */
(( android.content.Context ) v0 ).getDataDir ( ); // invoke-virtual {v0}, Landroid/content/Context;->getDataDir()Ljava/io/File;
/* .line 211 */
/* .local v0, "appDataFile":Ljava/io/File; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 212 */
(( java.io.File ) v0 ).getParentFile ( ); // invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;
/* .line 213 */
/* .local v1, "appDataParentFile":Ljava/io/File; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 214 */
(( java.io.File ) v1 ).getPath ( ); // invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;
/* :try_end_0 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 219 */
} // .end local v0 # "appDataFile":Ljava/io/File;
} // .end local v1 # "appDataParentFile":Ljava/io/File;
} // :cond_0
/* .line 217 */
/* :catch_0 */
/* move-exception v0 */
/* .line 218 */
/* .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
final String v1 = "MiuiRestoreManagerService"; // const-string v1, "MiuiRestoreManagerService"
final String v2 = "package name not find"; // const-string v2, "package name not find"
android.util.Slog .w ( v1,v2 );
/* .line 220 */
} // .end local v0 # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean moveSdcardData ( java.lang.String p0, java.lang.String p1, java.lang.String p2, Integer p3, Integer p4 ) {
/* .locals 14 */
/* .param p1, "sourcePath" # Ljava/lang/String; */
/* .param p2, "destPath" # Ljava/lang/String; */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .param p4, "userId" # I */
/* .param p5, "flag" # I */
/* .line 156 */
/* move-object v11, p0 */
v12 = this.mInstallerExecutor;
/* new-instance v13, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask; */
v2 = this.mInstaller;
int v6 = 0; // const/4 v6, 0x0
final String v8 = ""; // const-string v8, ""
int v10 = 0; // const/4 v10, 0x0
/* move-object v0, v13 */
/* move-object v1, p0 */
/* move-object v3, p1 */
/* move-object/from16 v4, p2 */
/* move-object/from16 v5, p3 */
/* move/from16 v7, p4 */
/* move/from16 v9, p5 */
/* invoke-direct/range {v0 ..v10}, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;-><init>(Lcom/android/server/MiuiRestoreManagerService;Lcom/android/server/pm/Installer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;ILcom/android/server/MiuiRestoreManagerService$MoveDataTask-IA;)V */
/* .line 158 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
private void notifyMoveTaskEnd ( java.lang.String p0, Integer p1, Integer p2 ) {
/* .locals 6 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .param p3, "errorCode" # I */
/* .line 399 */
v0 = this.mRestoreObservers;
/* monitor-enter v0 */
/* .line 400 */
try { // :try_start_0
v1 = this.mRestoreObservers;
v1 = (( android.os.RemoteCallbackList ) v1 ).beginBroadcast ( ); // invoke-virtual {v1}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 401 */
/* .local v1, "cnt":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* if-ge v2, v1, :cond_0 */
/* .line 403 */
try { // :try_start_1
v3 = this.mRestoreObservers;
(( android.os.RemoteCallbackList ) v3 ).getBroadcastItem ( v2 ); // invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;
/* check-cast v3, Lmiui/app/backup/IRestoreListener; */
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 406 */
/* .line 404 */
/* :catch_0 */
/* move-exception v3 */
/* .line 405 */
/* .local v3, "e":Landroid/os/RemoteException; */
try { // :try_start_2
final String v4 = "MiuiRestoreManagerService"; // const-string v4, "MiuiRestoreManagerService"
final String v5 = "RemoteException error in notifyMoveTaskEnd "; // const-string v5, "RemoteException error in notifyMoveTaskEnd "
android.util.Slog .e ( v4,v5,v3 );
/* .line 401 */
} // .end local v3 # "e":Landroid/os/RemoteException;
} // :goto_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 408 */
} // .end local v2 # "i":I
} // :cond_0
v2 = this.mRestoreObservers;
(( android.os.RemoteCallbackList ) v2 ).finishBroadcast ( ); // invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
/* .line 409 */
} // .end local v1 # "cnt":I
/* monitor-exit v0 */
/* .line 410 */
return;
/* .line 409 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
/* # virtual methods */
public Integer backupAppData ( java.lang.String p0, java.lang.String[] p1, java.lang.String[] p2, java.lang.String[] p3, java.lang.String[] p4, android.os.ParcelFileDescriptor p5, Integer p6, miui.app.backup.ITaskCommonCallback p7 ) {
/* .locals 14 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "paths" # [Ljava/lang/String; */
/* .param p3, "domains" # [Ljava/lang/String; */
/* .param p4, "parentPaths" # [Ljava/lang/String; */
/* .param p5, "excludePaths" # [Ljava/lang/String; */
/* .param p6, "outFd" # Landroid/os/ParcelFileDescriptor; */
/* .param p7, "type" # I */
/* .param p8, "callback" # Lmiui/app/backup/ITaskCommonCallback; */
/* .line 164 */
v0 = android.os.Binder .getCallingUid ( );
/* const/16 v1, 0x17d4 */
int v2 = 1; // const/4 v2, 0x1
final String v3 = "MiuiRestoreManagerService"; // const-string v3, "MiuiRestoreManagerService"
/* if-eq v0, v1, :cond_0 */
/* .line 165 */
final String v0 = "caller is not backup uid"; // const-string v0, "caller is not backup uid"
android.util.Slog .w ( v3,v0 );
/* .line 166 */
/* .line 168 */
} // :cond_0
if ( p2 != null) { // if-eqz p2, :cond_2
/* if-nez p3, :cond_1 */
/* move-object v0, p0 */
/* .line 172 */
} // :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "call backupData:paths="; // const-string v1, "call backupData:paths="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* invoke-static/range {p2 ..p2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String; */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v0 );
/* .line 174 */
/* move-object v0, p0 */
v1 = this.mInstallerExecutor;
/* new-instance v2, Lcom/android/server/MiuiRestoreManagerService$BackupDataTask; */
/* move-object v4, v2 */
/* move-object v5, p0 */
/* move-object v6, p1 */
/* move-object/from16 v7, p2 */
/* move-object/from16 v8, p3 */
/* move-object/from16 v9, p4 */
/* move-object/from16 v10, p5 */
/* move-object/from16 v11, p6 */
/* move/from16 v12, p7 */
/* move-object/from16 v13, p8 */
/* invoke-direct/range {v4 ..v13}, Lcom/android/server/MiuiRestoreManagerService$BackupDataTask;-><init>(Lcom/android/server/MiuiRestoreManagerService;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Landroid/os/ParcelFileDescriptor;ILmiui/app/backup/ITaskCommonCallback;)V */
/* .line 176 */
int v1 = 0; // const/4 v1, 0x0
/* .line 168 */
} // :cond_2
/* move-object v0, p0 */
/* .line 169 */
} // :goto_0
final String v1 = "path or domains is null"; // const-string v1, "path or domains is null"
android.util.Slog .w ( v3,v1 );
/* .line 170 */
} // .end method
public Boolean getDataFileInfo ( java.lang.String p0, miui.app.backup.IGetFileInfoCallback p1 ) {
/* .locals 10 */
/* .param p1, "path" # Ljava/lang/String; */
/* .param p2, "callback" # Lmiui/app/backup/IGetFileInfoCallback; */
/* .line 280 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 281 */
/* .local v0, "callingUid":I */
v1 = android.os.Binder .getCallingPid ( );
/* .line 282 */
/* .local v1, "callingPid":I */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "call get data file info, path= "; // const-string v3, "call get data file info, path= "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ", uid= "; // const-string v3, ", uid= "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", pid= "; // const-string v3, ", pid= "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiRestoreManagerService"; // const-string v3, "MiuiRestoreManagerService"
android.util.Slog .d ( v3,v2 );
/* .line 283 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v4 */
/* .line 285 */
/* .local v4, "token":J */
try { // :try_start_0
final String v2 = "com.miui.cloudbackup"; // const-string v2, "com.miui.cloudbackup"
v2 = /* invoke-direct {p0, v0, v2}, Lcom/android/server/MiuiRestoreManagerService;->checkIsLegalPackageAndCallingUid(ILjava/lang/String;)Z */
int v6 = 0; // const/4 v6, 0x0
/* if-nez v2, :cond_0 */
final String v2 = "com.miui.backup"; // const-string v2, "com.miui.backup"
/* .line 286 */
v2 = /* invoke-direct {p0, v0, v2}, Lcom/android/server/MiuiRestoreManagerService;->checkIsLegalPackageAndCallingUid(ILjava/lang/String;)Z */
/* if-nez v2, :cond_0 */
final String v2 = "com.miui.huanji"; // const-string v2, "com.miui.huanji"
/* .line 287 */
v2 = /* invoke-direct {p0, v0, v2}, Lcom/android/server/MiuiRestoreManagerService;->checkIsLegalPackageAndCallingUid(ILjava/lang/String;)Z */
/* if-nez v2, :cond_0 */
/* .line 288 */
final String v2 = "not cloud backup or huanji"; // const-string v2, "not cloud backup or huanji"
android.util.Slog .w ( v3,v2 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 289 */
/* nop */
/* .line 318 */
android.os.Binder .restoreCallingIdentity ( v4,v5 );
/* .line 289 */
/* .line 292 */
} // :cond_0
try { // :try_start_1
v2 = android.text.TextUtils .isEmpty ( p1 );
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 293 */
final String v2 = "invalid params"; // const-string v2, "invalid params"
android.util.Slog .w ( v3,v2 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 294 */
/* nop */
/* .line 318 */
android.os.Binder .restoreCallingIdentity ( v4,v5 );
/* .line 294 */
/* .line 297 */
} // :cond_1
try { // :try_start_2
v2 = android.os.UserHandle .getUserId ( v0 );
/* invoke-direct {p0, v2}, Lcom/android/server/MiuiRestoreManagerService;->getAppDataRootPath(I)Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath; */
/* .line 298 */
/* .local v2, "appDataRootPath":Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath; */
v7 = this.external;
/* if-nez v7, :cond_2 */
v7 = this.internal;
/* if-nez v7, :cond_2 */
/* .line 299 */
final String v7 = "null root path"; // const-string v7, "null root path"
android.util.Slog .w ( v3,v7 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 300 */
/* nop */
/* .line 318 */
android.os.Binder .restoreCallingIdentity ( v4,v5 );
/* .line 300 */
/* .line 303 */
} // :cond_2
int v7 = 0; // const/4 v7, 0x0
/* .line 304 */
/* .local v7, "verify":Z */
int v8 = 1; // const/4 v8, 0x1
/* if-nez v7, :cond_4 */
try { // :try_start_3
v9 = this.external;
if ( v9 != null) { // if-eqz v9, :cond_3
v9 = this.external;
v9 = (( java.lang.String ) p1 ).startsWith ( v9 ); // invoke-virtual {p1, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v9 != null) { // if-eqz v9, :cond_3
} // :cond_3
/* move v9, v6 */
} // :cond_4
} // :goto_0
/* move v9, v8 */
} // :goto_1
/* move v7, v9 */
/* .line 305 */
/* if-nez v7, :cond_6 */
v9 = this.internal;
if ( v9 != null) { // if-eqz v9, :cond_5
v9 = this.internal;
v9 = (( java.lang.String ) p1 ).startsWith ( v9 ); // invoke-virtual {p1, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v9 != null) { // if-eqz v9, :cond_5
} // :cond_5
/* move v9, v6 */
} // :cond_6
} // :goto_2
/* move v9, v8 */
} // :goto_3
/* move v7, v9 */
/* .line 306 */
/* if-nez v7, :cond_7 */
/* .line 307 */
final String v8 = "illegal path"; // const-string v8, "illegal path"
android.util.Slog .w ( v3,v8 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 308 */
/* nop */
/* .line 318 */
android.os.Binder .restoreCallingIdentity ( v4,v5 );
/* .line 308 */
/* .line 311 */
} // :cond_7
/* if-nez p2, :cond_8 */
/* .line 312 */
try { // :try_start_4
final String v8 = "no callback"; // const-string v8, "no callback"
android.util.Slog .w ( v3,v8 );
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 313 */
/* nop */
/* .line 318 */
android.os.Binder .restoreCallingIdentity ( v4,v5 );
/* .line 313 */
/* .line 316 */
} // :cond_8
try { // :try_start_5
v3 = this.mListDataExecutor;
/* new-instance v6, Lcom/android/server/MiuiRestoreManagerService$GetDataFileInfoTask; */
/* invoke-direct {v6, p0, p1, p2}, Lcom/android/server/MiuiRestoreManagerService$GetDataFileInfoTask;-><init>(Lcom/android/server/MiuiRestoreManagerService;Ljava/lang/String;Lmiui/app/backup/IGetFileInfoCallback;)V */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_0 */
/* .line 318 */
} // .end local v2 # "appDataRootPath":Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;
} // .end local v7 # "verify":Z
android.os.Binder .restoreCallingIdentity ( v4,v5 );
/* .line 319 */
/* nop */
/* .line 321 */
/* .line 318 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v4,v5 );
/* .line 319 */
/* throw v2 */
} // .end method
public Boolean listDataDir ( java.lang.String p0, Long p1, Integer p2, miui.app.backup.IListDirCallback p3 ) {
/* .locals 20 */
/* .param p1, "path" # Ljava/lang/String; */
/* .param p2, "start" # J */
/* .param p4, "maxCount" # I */
/* .param p5, "callback" # Lmiui/app/backup/IListDirCallback; */
/* .line 326 */
/* move-object/from16 v8, p0 */
/* move-object/from16 v9, p1 */
/* move-wide/from16 v10, p2 */
/* move/from16 v12, p4 */
v13 = android.os.Binder .getCallingUid ( );
/* .line 327 */
/* .local v13, "callingUid":I */
v14 = android.os.Binder .getCallingPid ( );
/* .line 328 */
/* .local v14, "callingPid":I */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "call list data dir, path= "; // const-string v1, "call list data dir, path= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", start= "; // const-string v1, ", start= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v10, v11 ); // invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = ", maxCount= "; // const-string v1, ", maxCount= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v12 ); // invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", uid= "; // const-string v1, ", uid= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", pid= "; // const-string v1, ", pid= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v14 ); // invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiRestoreManagerService"; // const-string v1, "MiuiRestoreManagerService"
android.util.Slog .d ( v1,v0 );
/* .line 330 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v15 */
/* .line 332 */
/* .local v15, "token":J */
try { // :try_start_0
final String v0 = "com.miui.cloudbackup"; // const-string v0, "com.miui.cloudbackup"
v0 = /* invoke-direct {v8, v13, v0}, Lcom/android/server/MiuiRestoreManagerService;->checkIsLegalPackageAndCallingUid(ILjava/lang/String;)Z */
int v2 = 0; // const/4 v2, 0x0
/* if-nez v0, :cond_0 */
final String v0 = "com.miui.backup"; // const-string v0, "com.miui.backup"
/* .line 333 */
v0 = /* invoke-direct {v8, v13, v0}, Lcom/android/server/MiuiRestoreManagerService;->checkIsLegalPackageAndCallingUid(ILjava/lang/String;)Z */
/* if-nez v0, :cond_0 */
final String v0 = "com.miui.huanji"; // const-string v0, "com.miui.huanji"
/* .line 334 */
v0 = /* invoke-direct {v8, v13, v0}, Lcom/android/server/MiuiRestoreManagerService;->checkIsLegalPackageAndCallingUid(ILjava/lang/String;)Z */
/* if-nez v0, :cond_0 */
/* .line 335 */
final String v0 = "not cloud backup or huanji"; // const-string v0, "not cloud backup or huanji"
android.util.Slog .w ( v1,v0 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 336 */
/* nop */
/* .line 364 */
/* invoke-static/range {v15 ..v16}, Landroid/os/Binder;->restoreCallingIdentity(J)V */
/* .line 336 */
/* .line 338 */
} // :cond_0
try { // :try_start_1
v0 = /* invoke-static/range {p1 ..p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z */
/* if-nez v0, :cond_9 */
/* const-wide/16 v3, 0x0 */
/* cmp-long v0, v10, v3 */
/* if-ltz v0, :cond_9 */
/* if-gez v12, :cond_1 */
/* goto/16 :goto_4 */
/* .line 343 */
} // :cond_1
v0 = android.os.UserHandle .getUserId ( v13 );
/* invoke-direct {v8, v0}, Lcom/android/server/MiuiRestoreManagerService;->getAppDataRootPath(I)Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath; */
/* .line 344 */
/* .local v0, "appDataRootPath":Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath; */
v3 = this.external;
/* if-nez v3, :cond_2 */
v3 = this.internal;
/* if-nez v3, :cond_2 */
/* .line 345 */
final String v3 = "null root path"; // const-string v3, "null root path"
android.util.Slog .w ( v1,v3 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 346 */
/* nop */
/* .line 364 */
/* invoke-static/range {v15 ..v16}, Landroid/os/Binder;->restoreCallingIdentity(J)V */
/* .line 346 */
/* .line 349 */
} // :cond_2
int v3 = 0; // const/4 v3, 0x0
/* .line 350 */
/* .local v3, "verify":Z */
/* const/16 v17, 0x1 */
/* if-nez v3, :cond_4 */
try { // :try_start_2
v4 = this.external;
if ( v4 != null) { // if-eqz v4, :cond_3
v4 = this.external;
v4 = (( java.lang.String ) v9 ).startsWith ( v4 ); // invoke-virtual {v9, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v4 != null) { // if-eqz v4, :cond_3
} // :cond_3
/* move v4, v2 */
} // :cond_4
} // :goto_0
/* move/from16 v4, v17 */
} // :goto_1
/* move v3, v4 */
/* .line 351 */
/* if-nez v3, :cond_6 */
v4 = this.internal;
if ( v4 != null) { // if-eqz v4, :cond_5
v4 = this.internal;
v4 = (( java.lang.String ) v9 ).startsWith ( v4 ); // invoke-virtual {v9, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v4 != null) { // if-eqz v4, :cond_5
} // :cond_5
/* move v4, v2 */
} // :cond_6
} // :goto_2
/* move/from16 v4, v17 */
} // :goto_3
/* move/from16 v18, v4 */
/* .line 352 */
} // .end local v3 # "verify":Z
/* .local v18, "verify":Z */
/* if-nez v18, :cond_7 */
/* .line 353 */
final String v3 = "illegal path"; // const-string v3, "illegal path"
android.util.Slog .w ( v1,v3 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 354 */
/* nop */
/* .line 364 */
/* invoke-static/range {v15 ..v16}, Landroid/os/Binder;->restoreCallingIdentity(J)V */
/* .line 354 */
/* .line 357 */
} // :cond_7
/* if-nez p5, :cond_8 */
/* .line 358 */
try { // :try_start_3
final String v3 = "no callback"; // const-string v3, "no callback"
android.util.Slog .w ( v1,v3 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 359 */
/* nop */
/* .line 364 */
/* invoke-static/range {v15 ..v16}, Landroid/os/Binder;->restoreCallingIdentity(J)V */
/* .line 359 */
/* .line 362 */
} // :cond_8
try { // :try_start_4
v7 = this.mListDataExecutor;
/* new-instance v6, Lcom/android/server/MiuiRestoreManagerService$ListDataDirTask; */
/* move-object v1, v6 */
/* move-object/from16 v2, p0 */
/* move-object/from16 v3, p1 */
/* move-wide/from16 v4, p2 */
/* move-object/from16 v19, v0 */
/* move-object v0, v6 */
} // .end local v0 # "appDataRootPath":Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;
/* .local v19, "appDataRootPath":Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath; */
/* move/from16 v6, p4 */
/* move-object v8, v7 */
/* move-object/from16 v7, p5 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/MiuiRestoreManagerService$ListDataDirTask;-><init>(Lcom/android/server/MiuiRestoreManagerService;Ljava/lang/String;JILmiui/app/backup/IListDirCallback;)V */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 364 */
} // .end local v18 # "verify":Z
} // .end local v19 # "appDataRootPath":Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;
/* invoke-static/range {v15 ..v16}, Landroid/os/Binder;->restoreCallingIdentity(J)V */
/* .line 365 */
/* nop */
/* .line 367 */
/* .line 339 */
} // :cond_9
} // :goto_4
try { // :try_start_5
final String v0 = "invalid params"; // const-string v0, "invalid params"
android.util.Slog .w ( v1,v0 );
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_0 */
/* .line 340 */
/* nop */
/* .line 364 */
/* invoke-static/range {v15 ..v16}, Landroid/os/Binder;->restoreCallingIdentity(J)V */
/* .line 340 */
/* .line 364 */
/* :catchall_0 */
/* move-exception v0 */
/* invoke-static/range {v15 ..v16}, Landroid/os/Binder;->restoreCallingIdentity(J)V */
/* .line 365 */
/* throw v0 */
} // .end method
public Boolean moveData ( java.lang.String p0, java.lang.String p1, java.lang.String p2, Integer p3, Boolean p4, Integer p5 ) {
/* .locals 21 */
/* .param p1, "sourcePath" # Ljava/lang/String; */
/* .param p2, "destPath" # Ljava/lang/String; */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .param p4, "userId" # I */
/* .param p5, "isSdcardData" # Z */
/* .param p6, "flag" # I */
/* .line 114 */
/* move-object/from16 v12, p0 */
/* move-object/from16 v13, p3 */
/* move/from16 v14, p4 */
v0 = android.os.Binder .getCallingUid ( );
/* const/16 v1, 0x17d4 */
int v2 = 0; // const/4 v2, 0x0
final String v3 = "MiuiRestoreManagerService"; // const-string v3, "MiuiRestoreManagerService"
/* if-eq v0, v1, :cond_0 */
/* .line 115 */
final String v0 = "caller is not backup uid"; // const-string v0, "caller is not backup uid"
android.util.Slog .w ( v3,v0 );
/* .line 116 */
/* .line 118 */
} // :cond_0
v0 = /* invoke-static/range {p1 ..p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z */
/* if-nez v0, :cond_6 */
v0 = /* invoke-static/range {p2 ..p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z */
/* if-nez v0, :cond_6 */
v0 = /* invoke-static/range {p3 ..p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* move-object/from16 v15, p2 */
/* goto/16 :goto_2 */
/* .line 122 */
} // :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "call move:dp="; // const-string v1, "call move:dp="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v15, p2 */
(( java.lang.StringBuilder ) v0 ).append ( v15 ); // invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ",pkg="; // const-string v1, ",pkg="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ",userId="; // const-string v1, ",userId="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v14 ); // invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v0 );
/* .line 124 */
if ( p5 != null) { // if-eqz p5, :cond_2
/* .line 125 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
/* move-object/from16 v3, p2 */
/* move-object/from16 v4, p3 */
/* move/from16 v5, p4 */
/* move/from16 v6, p6 */
v0 = /* invoke-direct/range {v1 ..v6}, Lcom/android/server/MiuiRestoreManagerService;->moveSdcardData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Z */
/* .line 127 */
} // :cond_2
/* move-object/from16 v11, p1 */
v0 = (( java.lang.String ) v11 ).contains ( v13 ); // invoke-virtual {v11, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_5
v0 = /* invoke-virtual/range {p2 ..p3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z */
/* if-nez v0, :cond_3 */
/* .line 131 */
} // :cond_3
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v16 */
/* .line 134 */
/* .local v16, "token":J */
try { // :try_start_0
android.app.AppGlobals .getPackageManager ( );
/* .line 135 */
/* const-wide/16 v4, 0x400 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 143 */
/* .local v0, "info":Landroid/content/pm/ApplicationInfo; */
/* invoke-static/range {v16 ..v17}, Landroid/os/Binder;->restoreCallingIdentity(J)V */
/* .line 144 */
/* nop */
/* .line 145 */
/* if-nez v0, :cond_4 */
/* .line 146 */
final String v1 = "package application info is null"; // const-string v1, "package application info is null"
android.util.Slog .e ( v3,v1 );
/* .line 147 */
/* .line 149 */
} // :cond_4
v10 = this.mInstallerExecutor;
/* new-instance v9, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask; */
v3 = this.mInstaller;
/* iget v7, v0, Landroid/content/pm/ApplicationInfo;->uid:I */
v8 = this.seInfo;
/* const/16 v18, 0x0 */
/* move-object v1, v9 */
/* move-object/from16 v2, p0 */
/* move-object/from16 v4, p1 */
/* move-object/from16 v5, p2 */
/* move-object/from16 v6, p3 */
/* move-object/from16 v19, v8 */
/* move/from16 v8, p4 */
/* move-object/from16 v20, v9 */
/* move-object/from16 v9, v19 */
/* move-object/from16 v19, v0 */
/* move-object v0, v10 */
} // .end local v0 # "info":Landroid/content/pm/ApplicationInfo;
/* .local v19, "info":Landroid/content/pm/ApplicationInfo; */
/* move/from16 v10, p6 */
/* move-object/from16 v11, v18 */
/* invoke-direct/range {v1 ..v11}, Lcom/android/server/MiuiRestoreManagerService$MoveDataTask;-><init>(Lcom/android/server/MiuiRestoreManagerService;Lcom/android/server/pm/Installer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;ILcom/android/server/MiuiRestoreManagerService$MoveDataTask-IA;)V */
/* move-object/from16 v1, v20 */
/* .line 151 */
int v0 = 1; // const/4 v0, 0x1
/* .line 143 */
} // .end local v19 # "info":Landroid/content/pm/ApplicationInfo;
/* :catchall_0 */
/* move-exception v0 */
/* .line 139 */
/* :catch_0 */
/* move-exception v0 */
/* .line 140 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_1
final String v1 = "get package application info fail "; // const-string v1, "get package application info fail "
android.util.Slog .e ( v3,v1,v0 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 141 */
/* nop */
/* .line 143 */
/* invoke-static/range {v16 ..v17}, Landroid/os/Binder;->restoreCallingIdentity(J)V */
/* .line 141 */
/* .line 143 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
/* invoke-static/range {v16 ..v17}, Landroid/os/Binder;->restoreCallingIdentity(J)V */
/* .line 144 */
/* throw v0 */
/* .line 128 */
} // .end local v16 # "token":J
} // :cond_5
} // :goto_1
/* const-string/jumbo v0, "the path does not match the package name" */
android.util.Slog .d ( v3,v0 );
/* .line 129 */
/* .line 118 */
} // :cond_6
/* move-object/from16 v15, p2 */
/* .line 119 */
} // :goto_2
final String v0 = "path or packageName is null"; // const-string v0, "path or packageName is null"
android.util.Slog .w ( v3,v0 );
/* .line 120 */
} // .end method
public void registerRestoreListener ( miui.app.backup.IRestoreListener p0 ) {
/* .locals 1 */
/* .param p1, "listener" # Lmiui/app/backup/IRestoreListener; */
/* .line 390 */
v0 = this.mRestoreObservers;
(( android.os.RemoteCallbackList ) v0 ).register ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z
/* .line 391 */
return;
} // .end method
public Boolean transferData ( java.lang.String p0, java.lang.String p1, java.lang.String p2, java.lang.String p3, Integer p4, Boolean p5, Integer p6, Boolean p7, miui.app.backup.ITransferDataCallback p8 ) {
/* .locals 22 */
/* .param p1, "src" # Ljava/lang/String; */
/* .param p2, "targetBase" # Ljava/lang/String; */
/* .param p3, "targetRelative" # Ljava/lang/String; */
/* .param p4, "targetPkgName" # Ljava/lang/String; */
/* .param p5, "targetMode" # I */
/* .param p6, "copy" # Z */
/* .param p7, "userId" # I */
/* .param p8, "isExternal" # Z */
/* .param p9, "callback" # Lmiui/app/backup/ITransferDataCallback; */
/* .line 234 */
/* move-object/from16 v12, p0 */
/* move-object/from16 v13, p1 */
/* move-object/from16 v14, p2 */
/* move/from16 v15, p8 */
v11 = android.os.Binder .getCallingUid ( );
/* .line 235 */
/* .local v11, "callingUid":I */
v10 = android.os.Binder .getCallingPid ( );
/* .line 236 */
/* .local v10, "callingPid":I */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "call transfer data, src= "; // const-string v1, "call transfer data, src= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", targetBase= "; // const-string v1, ", targetBase= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v14 ); // invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", targetRelative= "; // const-string v1, ", targetRelative= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v9, p3 */
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", targetPkgName= "; // const-string v1, ", targetPkgName= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v8, p4 */
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", targetMode= "; // const-string v1, ", targetMode= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move/from16 v7, p5 */
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", copy= "; // const-string v1, ", copy= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move/from16 v6, p6 */
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ", userId= "; // const-string v1, ", userId= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move/from16 v5, p7 */
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", isExternal= "; // const-string v1, ", isExternal= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v15 ); // invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ", uid= "; // const-string v1, ", uid= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v11 ); // invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", pid= "; // const-string v1, ", pid= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiRestoreManagerService"; // const-string v1, "MiuiRestoreManagerService"
android.util.Slog .d ( v1,v0 );
/* .line 239 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v16 */
/* .line 241 */
/* .local v16, "token":J */
try { // :try_start_0
final String v0 = "com.miui.cloudbackup"; // const-string v0, "com.miui.cloudbackup"
v0 = /* invoke-direct {v12, v11, v0}, Lcom/android/server/MiuiRestoreManagerService;->checkIsLegalPackageAndCallingUid(ILjava/lang/String;)Z */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_2 */
int v2 = 0; // const/4 v2, 0x0
/* if-nez v0, :cond_0 */
/* .line 242 */
try { // :try_start_1
final String v0 = "not cloud backup"; // const-string v0, "not cloud backup"
android.util.Slog .w ( v1,v0 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 243 */
/* nop */
/* .line 272 */
/* invoke-static/range {v16 ..v17}, Landroid/os/Binder;->restoreCallingIdentity(J)V */
/* .line 243 */
/* .line 272 */
/* :catchall_0 */
/* move-exception v0 */
/* move/from16 v20, v10 */
/* move/from16 v21, v11 */
/* goto/16 :goto_3 */
/* .line 246 */
} // :cond_0
try { // :try_start_2
v0 = /* invoke-static/range {p1 ..p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z */
/* if-nez v0, :cond_8 */
v0 = /* invoke-static/range {p2 ..p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z */
/* if-nez v0, :cond_8 */
/* .line 247 */
v0 = /* invoke-static/range {p3 ..p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z */
/* if-nez v0, :cond_7 */
v0 = /* invoke-static/range {p4 ..p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* move/from16 v20, v10 */
/* move/from16 v21, v11 */
/* goto/16 :goto_2 */
/* .line 252 */
} // :cond_1
v0 = android.os.UserHandle .getUserId ( v11 );
/* invoke-direct {v12, v0}, Lcom/android/server/MiuiRestoreManagerService;->getAppDataRootPath(I)Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath; */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_2 */
/* .line 253 */
/* .local v0, "appDataRootPath":Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath; */
if ( v15 != null) { // if-eqz v15, :cond_2
try { // :try_start_3
v3 = this.external;
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
} // :cond_2
try { // :try_start_4
v3 = this.internal;
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_2 */
} // :goto_0
/* move-object v4, v3 */
/* .line 254 */
/* .local v4, "rootPath":Ljava/lang/String; */
/* if-nez v4, :cond_3 */
/* .line 255 */
try { // :try_start_5
final String v3 = "null root path"; // const-string v3, "null root path"
android.util.Slog .w ( v1,v3 );
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_0 */
/* .line 256 */
/* nop */
/* .line 272 */
/* invoke-static/range {v16 ..v17}, Landroid/os/Binder;->restoreCallingIdentity(J)V */
/* .line 256 */
/* .line 259 */
} // :cond_3
try { // :try_start_6
v3 = (( java.lang.String ) v13 ).startsWith ( v4 ); // invoke-virtual {v13, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v3 != null) { // if-eqz v3, :cond_6
v3 = (( java.lang.String ) v14 ).startsWith ( v4 ); // invoke-virtual {v14, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_2 */
/* if-nez v3, :cond_4 */
/* move-object/from16 v18, v0 */
/* move-object/from16 v19, v4 */
/* move/from16 v20, v10 */
/* move/from16 v21, v11 */
/* .line 264 */
} // :cond_4
/* if-nez p9, :cond_5 */
/* .line 265 */
try { // :try_start_7
final String v3 = "no callback"; // const-string v3, "no callback"
android.util.Slog .w ( v1,v3 );
/* :try_end_7 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_0 */
/* .line 266 */
/* nop */
/* .line 272 */
/* invoke-static/range {v16 ..v17}, Landroid/os/Binder;->restoreCallingIdentity(J)V */
/* .line 266 */
/* .line 269 */
} // :cond_5
try { // :try_start_8
v3 = this.mTransferExecutor;
/* new-instance v2, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask; */
/* :try_end_8 */
/* .catchall {:try_start_8 ..:try_end_8} :catchall_2 */
/* move-object v1, v2 */
/* move-object/from16 v18, v0 */
/* move-object v0, v2 */
} // .end local v0 # "appDataRootPath":Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;
/* .local v18, "appDataRootPath":Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath; */
/* move-object/from16 v2, p0 */
/* move-object v12, v3 */
/* move-object/from16 v3, p1 */
/* move-object/from16 v19, v4 */
} // .end local v4 # "rootPath":Ljava/lang/String;
/* .local v19, "rootPath":Ljava/lang/String; */
/* move-object/from16 v4, p2 */
/* move-object/from16 v5, p3 */
/* move-object/from16 v6, p4 */
/* move/from16 v7, p5 */
/* move/from16 v8, p6 */
/* move/from16 v9, p7 */
/* move/from16 v20, v10 */
} // .end local v10 # "callingPid":I
/* .local v20, "callingPid":I */
/* move/from16 v10, p8 */
/* move/from16 v21, v11 */
} // .end local v11 # "callingUid":I
/* .local v21, "callingUid":I */
/* move-object/from16 v11, p9 */
try { // :try_start_9
/* invoke-direct/range {v1 ..v11}, Lcom/android/server/MiuiRestoreManagerService$TransferDataTask;-><init>(Lcom/android/server/MiuiRestoreManagerService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIZLmiui/app/backup/ITransferDataCallback;)V */
/* :try_end_9 */
/* .catchall {:try_start_9 ..:try_end_9} :catchall_1 */
/* .line 272 */
} // .end local v18 # "appDataRootPath":Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;
} // .end local v19 # "rootPath":Ljava/lang/String;
/* invoke-static/range {v16 ..v17}, Landroid/os/Binder;->restoreCallingIdentity(J)V */
/* .line 273 */
/* nop */
/* .line 275 */
int v0 = 1; // const/4 v0, 0x1
/* .line 259 */
} // .end local v20 # "callingPid":I
} // .end local v21 # "callingUid":I
/* .restart local v0 # "appDataRootPath":Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath; */
/* .restart local v4 # "rootPath":Ljava/lang/String; */
/* .restart local v10 # "callingPid":I */
/* .restart local v11 # "callingUid":I */
} // :cond_6
/* move-object/from16 v18, v0 */
/* move-object/from16 v19, v4 */
/* move/from16 v20, v10 */
/* move/from16 v21, v11 */
/* .line 260 */
} // .end local v0 # "appDataRootPath":Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;
} // .end local v4 # "rootPath":Ljava/lang/String;
} // .end local v10 # "callingPid":I
} // .end local v11 # "callingUid":I
/* .restart local v18 # "appDataRootPath":Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath; */
/* .restart local v19 # "rootPath":Ljava/lang/String; */
/* .restart local v20 # "callingPid":I */
/* .restart local v21 # "callingUid":I */
} // :goto_1
try { // :try_start_a
final String v0 = "illegal path"; // const-string v0, "illegal path"
android.util.Slog .w ( v1,v0 );
/* :try_end_a */
/* .catchall {:try_start_a ..:try_end_a} :catchall_1 */
/* .line 261 */
/* nop */
/* .line 272 */
/* invoke-static/range {v16 ..v17}, Landroid/os/Binder;->restoreCallingIdentity(J)V */
/* .line 261 */
/* .line 247 */
} // .end local v18 # "appDataRootPath":Lcom/android/server/MiuiRestoreManagerService$AppDataRootPath;
} // .end local v19 # "rootPath":Ljava/lang/String;
} // .end local v20 # "callingPid":I
} // .end local v21 # "callingUid":I
/* .restart local v10 # "callingPid":I */
/* .restart local v11 # "callingUid":I */
} // :cond_7
/* move/from16 v20, v10 */
/* move/from16 v21, v11 */
} // .end local v10 # "callingPid":I
} // .end local v11 # "callingUid":I
/* .restart local v20 # "callingPid":I */
/* .restart local v21 # "callingUid":I */
/* .line 246 */
} // .end local v20 # "callingPid":I
} // .end local v21 # "callingUid":I
/* .restart local v10 # "callingPid":I */
/* .restart local v11 # "callingUid":I */
} // :cond_8
/* move/from16 v20, v10 */
/* move/from16 v21, v11 */
/* .line 248 */
} // .end local v10 # "callingPid":I
} // .end local v11 # "callingUid":I
/* .restart local v20 # "callingPid":I */
/* .restart local v21 # "callingUid":I */
} // :goto_2
try { // :try_start_b
final String v0 = "invalid params"; // const-string v0, "invalid params"
android.util.Slog .w ( v1,v0 );
/* :try_end_b */
/* .catchall {:try_start_b ..:try_end_b} :catchall_1 */
/* .line 249 */
/* nop */
/* .line 272 */
/* invoke-static/range {v16 ..v17}, Landroid/os/Binder;->restoreCallingIdentity(J)V */
/* .line 249 */
/* .line 272 */
/* :catchall_1 */
/* move-exception v0 */
} // .end local v20 # "callingPid":I
} // .end local v21 # "callingUid":I
/* .restart local v10 # "callingPid":I */
/* .restart local v11 # "callingUid":I */
/* :catchall_2 */
/* move-exception v0 */
/* move/from16 v20, v10 */
/* move/from16 v21, v11 */
} // .end local v10 # "callingPid":I
} // .end local v11 # "callingUid":I
/* .restart local v20 # "callingPid":I */
/* .restart local v21 # "callingUid":I */
} // :goto_3
/* invoke-static/range {v16 ..v17}, Landroid/os/Binder;->restoreCallingIdentity(J)V */
/* .line 273 */
/* throw v0 */
} // .end method
public void unregisterRestoreListener ( miui.app.backup.IRestoreListener p0 ) {
/* .locals 1 */
/* .param p1, "listener" # Lmiui/app/backup/IRestoreListener; */
/* .line 395 */
v0 = this.mRestoreObservers;
(( android.os.RemoteCallbackList ) v0 ).unregister ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z
/* .line 396 */
return;
} // .end method
