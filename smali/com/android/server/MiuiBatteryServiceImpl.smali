.class public Lcom/android/server/MiuiBatteryServiceImpl;
.super Lcom/android/server/MiuiBatteryServiceStub;
.source "MiuiBatteryServiceImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.MiuiBatteryServiceStub$$"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;,
        Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;
    }
.end annotation


# static fields
.field private static final AES_DECRYPTED_TEXT:Ljava/lang/String; = "0123456789abcd"

.field private static final AES_KEY:Ljava/lang/String; = "123456789abcdefa"

.field private static final HANDLE_PRODUCT_ID:I = 0x5083

.field private static final HANDLE_VENDOR_ID:I = 0x2717

.field public static final HIGH_TEMP_STOP_CHARGE_STATE:I = 0x5

.field private static final KEY_BATTERY_TEMP_ALLOW_KILL:Ljava/lang/String; = "allowed_kill_battery_temp_threshhold"

.field public static final KEY_FAST_CHARGE_ENABLED:Ljava/lang/String; = "key_fast_charge_enabled"

.field private static final KEY_SETTING_TEMP_STATE:Ljava/lang/String; = "thermal_temp_state_value"

.field private static final SHUTDOWN_TMEIOUT:I = 0x493e0

.field private static final TEST_APP_PACKAGENAME_KDDI:[Ljava/lang/String;

.field private static final TEST_APP_PACKAGENAME_SB:[Ljava/lang/String;

.field private static final WAKELOCK_ACQUIRE:I = 0x57e40

.field private static isSystemReady:Z


# instance fields
.field private BtConnectedCount:I

.field private final DEBUG:Z

.field public final SUPPORT_COUNTRY:[Ljava/lang/String;

.field private final TAG:Ljava/lang/String;

.field private isDisCharging:Z

.field private volatile mA2dp:Landroid/bluetooth/BluetoothA2dp;

.field private mBatteryTempThreshholdObserver:Landroid/database/ContentObserver;

.field private mBluetoothStateReceiver:Landroid/content/BroadcastReceiver;

.field private mBtConnectState:I

.field private mChargingStateReceiver:Landroid/content/BroadcastReceiver;

.field private mContext:Landroid/content/Context;

.field mFastChargeObserver:Landroid/database/ContentObserver;

.field private mHandler:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

.field private volatile mHeadset:Landroid/bluetooth/BluetoothHeadset;

.field private mIsSatisfyTempLevelCondition:Z

.field private mIsSatisfyTempSocCondition:Z

.field private mIsSatisfyTimeRegionCondition:Z

.field private mIsStopCharge:Z

.field private mIsTestMode:Z

.field private mLastPhoneBatteryLevel:I

.field private mMiCharge:Lmiui/util/IMiCharge;

.field private final mProfileServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

.field private mSupportHighTempStopCharge:Z

.field private mSupportSB:Z

.field private mSupportWirelessCharge:Z

.field private mTempStateObserver:Landroid/database/ContentObserver;

.field private mUsbStateReceiver:Landroid/content/BroadcastReceiver;

.field private miuiFboService:Lcom/miui/app/MiuiFboServiceInternal;

.field private pm:Landroid/os/PowerManager;

.field private shutDownRnuable:Ljava/lang/Runnable;

.field private wakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static bridge synthetic -$$Nest$fgetBtConnectedCount(Lcom/android/server/MiuiBatteryServiceImpl;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->BtConnectedCount:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryServiceImpl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->DEBUG:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetisDisCharging(Lcom/android/server/MiuiBatteryServiceImpl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->isDisCharging:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmBtConnectState(Lcom/android/server/MiuiBatteryServiceImpl;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mBtConnectState:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/MiuiBatteryServiceImpl;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryServiceImpl;)Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mHandler:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsSatisfyTempLevelCondition(Lcom/android/server/MiuiBatteryServiceImpl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsSatisfyTempLevelCondition:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsSatisfyTempSocCondition(Lcom/android/server/MiuiBatteryServiceImpl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsSatisfyTempSocCondition:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsSatisfyTimeRegionCondition(Lcom/android/server/MiuiBatteryServiceImpl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsSatisfyTimeRegionCondition:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsStopCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsStopCharge:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsTestMode(Lcom/android/server/MiuiBatteryServiceImpl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsTestMode:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastPhoneBatteryLevel(Lcom/android/server/MiuiBatteryServiceImpl;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mLastPhoneBatteryLevel:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Lmiui/util/IMiCharge;
    .locals 0

    iget-object p0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mMiCharge:Lmiui/util/IMiCharge;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSupportWirelessCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mSupportWirelessCharge:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetpm(Lcom/android/server/MiuiBatteryServiceImpl;)Landroid/os/PowerManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->pm:Landroid/os/PowerManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetshutDownRnuable(Lcom/android/server/MiuiBatteryServiceImpl;)Ljava/lang/Runnable;
    .locals 0

    iget-object p0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->shutDownRnuable:Ljava/lang/Runnable;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetwakeLock(Lcom/android/server/MiuiBatteryServiceImpl;)Landroid/os/PowerManager$WakeLock;
    .locals 0

    iget-object p0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->wakeLock:Landroid/os/PowerManager$WakeLock;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputBtConnectedCount(Lcom/android/server/MiuiBatteryServiceImpl;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->BtConnectedCount:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputisDisCharging(Lcom/android/server/MiuiBatteryServiceImpl;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->isDisCharging:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmA2dp(Lcom/android/server/MiuiBatteryServiceImpl;Landroid/bluetooth/BluetoothA2dp;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mA2dp:Landroid/bluetooth/BluetoothA2dp;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmBtConnectState(Lcom/android/server/MiuiBatteryServiceImpl;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mBtConnectState:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmHeadset(Lcom/android/server/MiuiBatteryServiceImpl;Landroid/bluetooth/BluetoothHeadset;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mHeadset:Landroid/bluetooth/BluetoothHeadset;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsSatisfyTempLevelCondition(Lcom/android/server/MiuiBatteryServiceImpl;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsSatisfyTempLevelCondition:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsSatisfyTempSocCondition(Lcom/android/server/MiuiBatteryServiceImpl;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsSatisfyTempSocCondition:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsSatisfyTimeRegionCondition(Lcom/android/server/MiuiBatteryServiceImpl;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsSatisfyTimeRegionCondition:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsStopCharge(Lcom/android/server/MiuiBatteryServiceImpl;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsStopCharge:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastPhoneBatteryLevel(Lcom/android/server/MiuiBatteryServiceImpl;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mLastPhoneBatteryLevel:I

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 97
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/server/MiuiBatteryServiceImpl;->isSystemReady:Z

    .line 115
    const-string v0, "com.kddi.hirakuto"

    const-string v1, "com.kddi.hirakuto.debug"

    filled-new-array {v0, v1}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/MiuiBatteryServiceImpl;->TEST_APP_PACKAGENAME_KDDI:[Ljava/lang/String;

    .line 119
    const-string v0, "com.xiaomi.mihomemanager"

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/MiuiBatteryServiceImpl;->TEST_APP_PACKAGENAME_SB:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    .line 100
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceStub;-><init>()V

    .line 71
    const-string v0, "MiuiBatteryServiceImpl"

    iput-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->TAG:Ljava/lang/String;

    .line 72
    const-string v0, "persist.sys.debug_impl"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->DEBUG:Z

    .line 83
    iput-boolean v1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsStopCharge:Z

    .line 84
    iput-boolean v1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsSatisfyTempSocCondition:Z

    .line 85
    iput-boolean v1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsSatisfyTimeRegionCondition:Z

    .line 86
    iput-boolean v1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsSatisfyTempLevelCondition:Z

    .line 87
    iput-boolean v1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsTestMode:Z

    .line 89
    iput v1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->BtConnectedCount:I

    .line 96
    invoke-static {}, Lmiui/util/IMiCharge;->getInstance()Lmiui/util/IMiCharge;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mMiCharge:Lmiui/util/IMiCharge;

    .line 106
    const-string v2, "IT"

    const-string v3, "FR"

    const-string v4, "ES"

    const-string v5, "DE"

    const-string v6, "PL"

    const-string v7, "GB"

    filled-new-array/range {v2 .. v7}, [Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->SUPPORT_COUNTRY:[Ljava/lang/String;

    .line 129
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mLastPhoneBatteryLevel:I

    .line 132
    iput-boolean v1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->isDisCharging:Z

    .line 133
    iput v1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mBtConnectState:I

    .line 136
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->pm:Landroid/os/PowerManager;

    .line 137
    iput-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->wakeLock:Landroid/os/PowerManager$WakeLock;

    .line 139
    new-instance v0, Lcom/android/server/MiuiBatteryServiceImpl$1;

    invoke-direct {v0, p0}, Lcom/android/server/MiuiBatteryServiceImpl$1;-><init>(Lcom/android/server/MiuiBatteryServiceImpl;)V

    iput-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mChargingStateReceiver:Landroid/content/BroadcastReceiver;

    .line 164
    new-instance v0, Lcom/android/server/MiuiBatteryServiceImpl$2;

    invoke-direct {v0, p0}, Lcom/android/server/MiuiBatteryServiceImpl$2;-><init>(Lcom/android/server/MiuiBatteryServiceImpl;)V

    iput-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mBluetoothStateReceiver:Landroid/content/BroadcastReceiver;

    .line 183
    new-instance v0, Lcom/android/server/MiuiBatteryServiceImpl$3;

    invoke-direct {v0, p0}, Lcom/android/server/MiuiBatteryServiceImpl$3;-><init>(Lcom/android/server/MiuiBatteryServiceImpl;)V

    iput-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->shutDownRnuable:Ljava/lang/Runnable;

    .line 203
    new-instance v0, Lcom/android/server/MiuiBatteryServiceImpl$4;

    invoke-direct {v0, p0}, Lcom/android/server/MiuiBatteryServiceImpl$4;-><init>(Lcom/android/server/MiuiBatteryServiceImpl;)V

    iput-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mUsbStateReceiver:Landroid/content/BroadcastReceiver;

    .line 444
    new-instance v0, Lcom/android/server/MiuiBatteryServiceImpl$11;

    invoke-direct {v0, p0}, Lcom/android/server/MiuiBatteryServiceImpl$11;-><init>(Lcom/android/server/MiuiBatteryServiceImpl;)V

    iput-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mProfileServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    .line 101
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mMiCharge:Lmiui/util/IMiCharge;

    invoke-virtual {v0}, Lmiui/util/IMiCharge;->isWirelessChargingSupported()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mSupportWirelessCharge:Z

    .line 102
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mMiCharge:Lmiui/util/IMiCharge;

    const-string/jumbo v2, "smart_batt"

    invoke-virtual {v0, v2}, Lmiui/util/IMiCharge;->isFunctionSupported(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mSupportSB:Z

    .line 103
    const-string v0, "persist.vendor.high_temp_stop_charge"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mSupportHighTempStopCharge:Z

    .line 104
    return-void
.end method

.method private initBtConnectCount()I
    .locals 6

    .line 478
    const/4 v0, 0x0

    .line 479
    .local v0, "a2dpCount":I
    const/4 v1, 0x0

    .line 481
    .local v1, "headsetCount":I
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    .line 482
    .local v2, "btAdapter":Landroid/bluetooth/BluetoothAdapter;
    if-eqz v2, :cond_0

    .line 483
    iget-object v3, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mProfileServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    const/4 v5, 0x2

    invoke-virtual {v2, v3, v4, v5}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    .line 484
    iget-object v3, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mProfileServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    .line 486
    :cond_0
    iget-object v3, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mA2dp:Landroid/bluetooth/BluetoothA2dp;

    if-eqz v3, :cond_1

    .line 487
    iget-object v3, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mA2dp:Landroid/bluetooth/BluetoothA2dp;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothA2dp;->getConnectedDevices()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    .line 489
    :cond_1
    iget-object v3, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mHeadset:Landroid/bluetooth/BluetoothHeadset;

    if-eqz v3, :cond_2

    .line 490
    iget-object v3, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mHeadset:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothHeadset;->getConnectedDevices()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    .line 493
    :cond_2
    add-int v3, v0, v1

    return v3
.end method

.method private isTestMUTForJapan([Ljava/lang/String;)Z
    .locals 6
    .param p1, "appPackage"    # [Ljava/lang/String;

    .line 506
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v0

    .line 507
    .local v0, "packages":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 508
    .local v2, "appPackagesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 509
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/PackageInfo;

    .line 510
    .local v4, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-object v5, v4, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-interface {v2, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 511
    const-string v1, "MiuiBatteryServiceImpl"

    const-string v5, "TEST MODE"

    invoke-static {v1, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 512
    const/4 v1, 0x1

    return v1

    .line 508
    .end local v4    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 515
    .end local v3    # "i":I
    :cond_1
    return v1
.end method

.method private registerBluetoothStateBroadcastReceiver()V
    .locals 4

    .line 196
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 197
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.bluetooth.device.action.ACL_CONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 198
    const-string v1, "android.bluetooth.device.action.ACL_DISCONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 199
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mBluetoothStateReceiver:Landroid/content/BroadcastReceiver;

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    .line 200
    return-void
.end method

.method private registerChargingStateBroadcastReceiver()V
    .locals 4

    .line 159
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 160
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 161
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mChargingStateReceiver:Landroid/content/BroadcastReceiver;

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    .line 162
    return-void
.end method

.method private registerUsbStateBroadcastReceiver()V
    .locals 4

    .line 497
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 498
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 499
    const-string v1, "android.hardware.usb.action.USB_DEVICE_ATTACHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 500
    const-string v1, "android.hardware.usb.action.USB_DEVICE_DETACHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 501
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 502
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mUsbStateReceiver:Landroid/content/BroadcastReceiver;

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    .line 503
    return-void
.end method


# virtual methods
.method public init(Landroid/content/Context;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;

    .line 240
    iput-object p1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mContext:Landroid/content/Context;

    .line 241
    new-instance v0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {}, Lcom/android/server/MiuiFgThread;->get()Lcom/android/server/MiuiFgThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/MiuiFgThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;-><init>(Lcom/android/server/MiuiBatteryServiceImpl;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mHandler:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    .line 242
    const-class v0, Lcom/miui/app/MiuiFboServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/app/MiuiFboServiceInternal;

    iput-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->miuiFboService:Lcom/miui/app/MiuiFboServiceInternal;

    .line 243
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/MiuiBatteryServiceImpl;->isSystemReady:Z

    .line 244
    new-instance v1, Lcom/android/server/MiuiBatteryServiceImpl$5;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-direct {v1, p0, v2}, Lcom/android/server/MiuiBatteryServiceImpl$5;-><init>(Lcom/android/server/MiuiBatteryServiceImpl;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mFastChargeObserver:Landroid/database/ContentObserver;

    .line 258
    new-instance v1, Lcom/android/server/MiuiBatteryServiceImpl$6;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-direct {v1, p0, v2}, Lcom/android/server/MiuiBatteryServiceImpl$6;-><init>(Lcom/android/server/MiuiBatteryServiceImpl;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mTempStateObserver:Landroid/database/ContentObserver;

    .line 266
    new-instance v1, Lcom/android/server/MiuiBatteryServiceImpl$7;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-direct {v1, p0, v2}, Lcom/android/server/MiuiBatteryServiceImpl$7;-><init>(Lcom/android/server/MiuiBatteryServiceImpl;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mBatteryTempThreshholdObserver:Landroid/database/ContentObserver;

    .line 274
    new-instance v1, Lcom/android/server/MiuiBatteryServiceImpl$8;

    invoke-direct {v1, p0}, Lcom/android/server/MiuiBatteryServiceImpl$8;-><init>(Lcom/android/server/MiuiBatteryServiceImpl;)V

    .line 297
    .local v1, "stateChangedReceiver":Landroid/content/BroadcastReceiver;
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 298
    .local v2, "intentfilter":Landroid/content/IntentFilter;
    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 299
    const-string v3, "ro.product.device"

    const-string v4, ""

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "yudi"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 300
    const-string v5, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 301
    const-string v5, "android.intent.action.USER_PRESENT"

    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 303
    :cond_0
    iget-object v5, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 305
    iget-object v5, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mMiCharge:Lmiui/util/IMiCharge;

    invoke-virtual {v5}, Lmiui/util/IMiCharge;->getBatteryChargeType()Ljava/lang/String;

    move-result-object v5

    .line 306
    .local v5, "chargeType":Ljava/lang/String;
    const/4 v6, 0x0

    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_1

    .line 307
    const-string v7, "persist.vendor.charge.oneTrack"

    invoke-static {v7, v6}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    if-nez v7, :cond_1

    .line 308
    invoke-static {p1}, Lcom/android/server/MiuiBatteryStatsService;->getInstance(Landroid/content/Context;)Lcom/android/server/MiuiBatteryStatsService;

    .line 311
    :cond_1
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "mona"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 312
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "thor"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    :cond_2
    sget-boolean v7, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v7, :cond_3

    .line 314
    invoke-static {p1}, Lcom/android/server/MiuiBatteryAuthentic;->getInstance(Landroid/content/Context;)Lcom/android/server/MiuiBatteryAuthentic;

    .line 317
    :cond_3
    const-string v7, "ro.miui.customized.region"

    invoke-static {v7, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "jp_kd"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 318
    sget-object v8, Lcom/android/server/MiuiBatteryServiceImpl;->TEST_APP_PACKAGENAME_KDDI:[Ljava/lang/String;

    invoke-direct {p0, v8}, Lcom/android/server/MiuiBatteryServiceImpl;->isTestMUTForJapan([Ljava/lang/String;)Z

    move-result v8

    iput-boolean v8, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsTestMode:Z

    .line 321
    :cond_4
    invoke-static {v7, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "jp_sb"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 322
    sget-object v7, Lcom/android/server/MiuiBatteryServiceImpl;->TEST_APP_PACKAGENAME_SB:[Ljava/lang/String;

    invoke-direct {p0, v7}, Lcom/android/server/MiuiBatteryServiceImpl;->isTestMUTForJapan([Ljava/lang/String;)Z

    move-result v7

    iput-boolean v7, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mIsTestMode:Z

    .line 325
    :cond_5
    const-string v7, "persist.vendor.smartchg"

    invoke-static {v7, v6}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v7

    if-lez v7, :cond_6

    .line 326
    invoke-static {p1}, Lcom/android/server/MiuiBatteryIntelligence;->getInstance(Landroid/content/Context;)Lcom/android/server/MiuiBatteryIntelligence;

    .line 329
    :cond_6
    const-string v7, "ro.product.mod_device"

    invoke-static {v7, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "star"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    const/4 v9, 0x2

    if-nez v8, :cond_7

    .line 330
    invoke-static {v7, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "mars"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 331
    :cond_7
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl;->initBtConnectCount()I

    move-result v7

    iput v7, p0, Lcom/android/server/MiuiBatteryServiceImpl;->BtConnectedCount:I

    .line 332
    if-lez v7, :cond_8

    .line 333
    iget-object v7, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mHandler:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    const/16 v8, 0xa

    const-wide/16 v10, 0x3e8

    invoke-virtual {v7, v8, v0, v10, v11}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessageDelayed(IZJ)V

    .line 336
    :cond_8
    new-instance v7, Lcom/android/server/MiuiBatteryServiceImpl$9;

    invoke-direct {v7, p0}, Lcom/android/server/MiuiBatteryServiceImpl$9;-><init>(Lcom/android/server/MiuiBatteryServiceImpl;)V

    .line 365
    .local v7, "bluetoothReceiver":Landroid/content/BroadcastReceiver;
    new-instance v8, Landroid/content/IntentFilter;

    invoke-direct {v8}, Landroid/content/IntentFilter;-><init>()V

    .line 366
    .local v8, "filter":Landroid/content/IntentFilter;
    const-string v10, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v8, v10}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 367
    const-string v10, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v8, v10}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 368
    const-string v10, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v8, v10}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 369
    iget-object v10, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v10, v7, v8, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    .line 372
    .end local v7    # "bluetoothReceiver":Landroid/content/BroadcastReceiver;
    .end local v8    # "filter":Landroid/content/IntentFilter;
    :cond_9
    iget-boolean v7, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mSupportSB:Z

    if-eqz v7, :cond_a

    .line 373
    new-instance v7, Lcom/android/server/MiuiBatteryServiceImpl$10;

    invoke-direct {v7, p0}, Lcom/android/server/MiuiBatteryServiceImpl$10;-><init>(Lcom/android/server/MiuiBatteryServiceImpl;)V

    .line 386
    .local v7, "updateBattVolIntentReceiver":Landroid/content/BroadcastReceiver;
    new-instance v8, Landroid/content/IntentFilter;

    const-string v10, "miui.intent.action.UPDATE_BATTERY_DATA"

    invoke-direct {v8, v10}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 387
    .restart local v8    # "filter":Landroid/content/IntentFilter;
    const-string v10, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v8, v10}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 388
    const-string v10, "miui.intent.action.ADJUST_VOLTAGE"

    invoke-virtual {v8, v10}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 389
    iget-object v10, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v10, v7, v8, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    .line 392
    .end local v7    # "updateBattVolIntentReceiver":Landroid/content/BroadcastReceiver;
    .end local v8    # "filter":Landroid/content/IntentFilter;
    :cond_a
    iget-boolean v7, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mSupportHighTempStopCharge:Z

    if-eqz v7, :cond_b

    .line 393
    iget-object v7, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string/jumbo v8, "thermal_temp_state_value"

    invoke-static {v8}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mTempStateObserver:Landroid/database/ContentObserver;

    invoke-virtual {v7, v8, v6, v9}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 395
    iget-object v7, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "allowed_kill_battery_temp_threshhold"

    invoke-static {v8}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mBatteryTempThreshholdObserver:Landroid/database/ContentObserver;

    invoke-virtual {v7, v8, v6, v9}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 400
    :cond_b
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "aurora"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 401
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl;->registerUsbStateBroadcastReceiver()V

    .line 404
    :cond_c
    iget-object v7, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    .line 405
    const-string v8, "key_fast_charge_enabled"

    invoke-static {v8}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mFastChargeObserver:Landroid/database/ContentObserver;

    .line 404
    invoke-virtual {v7, v8, v6, v9}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 408
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "zhuque"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 409
    iget-object v3, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mContext:Landroid/content/Context;

    const-string v4, "power"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/PowerManager;

    iput-object v3, p0, Lcom/android/server/MiuiBatteryServiceImpl;->pm:Landroid/os/PowerManager;

    .line 410
    const-string v4, "Screenoffshutdown"

    invoke-virtual {v3, v0, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->wakeLock:Landroid/os/PowerManager$WakeLock;

    .line 411
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl;->registerChargingStateBroadcastReceiver()V

    .line 412
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl;->registerBluetoothStateBroadcastReceiver()V

    .line 413
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->wakeLock:Landroid/os/PowerManager$WakeLock;

    const-wide/32 v3, 0x57e40

    invoke-virtual {v0, v3, v4}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 414
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->mHandler:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    iget-object v3, p0, Lcom/android/server/MiuiBatteryServiceImpl;->shutDownRnuable:Ljava/lang/Runnable;

    const-wide/32 v6, 0x493e0

    invoke-virtual {v0, v3, v6, v7}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 417
    :cond_d
    return-void
.end method

.method public setBatteryStatusWithFbo(III)V
    .locals 5
    .param p1, "batteryStatus"    # I
    .param p2, "batteryLevel"    # I
    .param p3, "batteryTemperature"    # I

    .line 421
    sget-boolean v0, Lcom/android/server/MiuiBatteryServiceImpl;->isSystemReady:Z

    if-nez v0, :cond_0

    .line 422
    return-void

    .line 424
    :cond_0
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->miuiFboService:Lcom/miui/app/MiuiFboServiceInternal;

    invoke-interface {v0, p1, p2, p3}, Lcom/miui/app/MiuiFboServiceInternal;->setBatteryInfos(III)V

    .line 425
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->miuiFboService:Lcom/miui/app/MiuiFboServiceInternal;

    invoke-interface {v0}, Lcom/miui/app/MiuiFboServiceInternal;->getGlobalSwitch()Z

    move-result v0

    const-wide/16 v1, 0x0

    if-eqz v0, :cond_1

    const/16 v0, 0x1f4

    if-lt p3, v0, :cond_1

    .line 426
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->miuiFboService:Lcom/miui/app/MiuiFboServiceInternal;

    const-string/jumbo v3, "stop"

    const/4 v4, 0x3

    invoke-interface {v0, v3, v4, v1, v2}, Lcom/miui/app/MiuiFboServiceInternal;->deliverMessage(Ljava/lang/String;IJ)V

    .line 428
    return-void

    .line 430
    :cond_1
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->miuiFboService:Lcom/miui/app/MiuiFboServiceInternal;

    invoke-interface {v0}, Lcom/miui/app/MiuiFboServiceInternal;->getNativeIsRunning()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->miuiFboService:Lcom/miui/app/MiuiFboServiceInternal;

    invoke-interface {v0}, Lcom/miui/app/MiuiFboServiceInternal;->getGlobalSwitch()Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x1c2

    if-le p3, v0, :cond_2

    .line 432
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->miuiFboService:Lcom/miui/app/MiuiFboServiceInternal;

    const-string/jumbo v3, "stopDueTobatteryTemperature"

    const/4 v4, 0x6

    invoke-interface {v0, v3, v4, v1, v2}, Lcom/miui/app/MiuiFboServiceInternal;->deliverMessage(Ljava/lang/String;IJ)V

    .line 434
    return-void

    .line 436
    :cond_2
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->miuiFboService:Lcom/miui/app/MiuiFboServiceInternal;

    invoke-interface {v0}, Lcom/miui/app/MiuiFboServiceInternal;->getNativeIsRunning()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->miuiFboService:Lcom/miui/app/MiuiFboServiceInternal;

    invoke-interface {v0}, Lcom/miui/app/MiuiFboServiceInternal;->getGlobalSwitch()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->miuiFboService:Lcom/miui/app/MiuiFboServiceInternal;

    .line 437
    invoke-interface {v0}, Lcom/miui/app/MiuiFboServiceInternal;->getDueToScreenWait()Z

    move-result v0

    if-nez v0, :cond_3

    const/16 v0, 0x190

    if-ge p3, v0, :cond_3

    .line 438
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl;->miuiFboService:Lcom/miui/app/MiuiFboServiceInternal;

    const-string v3, "continue"

    const/4 v4, 0x2

    invoke-interface {v0, v3, v4, v1, v2}, Lcom/miui/app/MiuiFboServiceInternal;->deliverMessage(Ljava/lang/String;IJ)V

    .line 440
    return-void

    .line 442
    :cond_3
    return-void
.end method
