class com.android.server.MiuiBatteryStatsService$BatteryStatsHandler extends android.os.Handler {
	 /* .source "MiuiBatteryStatsService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/MiuiBatteryStatsService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "BatteryStatsHandler" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler$BatteryUEventObserver; */
/* } */
} // .end annotation
/* # static fields */
private static final java.lang.String BATTERY_LPD_EVENT;
private static final java.lang.String BATTERY_LPD_INFOMATION;
private static final java.lang.String CC_SHORT_VBUS_EVENT;
private static final java.lang.String LPD_DM_RES;
private static final java.lang.String LPD_DP_RES;
private static final java.lang.String LPD_SBU1_RES;
private static final java.lang.String LPD_SBU2_RES;
public static final Integer MSG_HANDLE_LPD_INFOMATION;
public static final Integer MSG_HANDLE_NOT_FULLY_CHARGED;
public static final Integer MSG_HANDLE_SLOW_CHARGE;
public static final Integer MSG_HANDLE_SREIES_DELTA_VOLTAGE;
public static final Integer MSG_INTERMITTENT_CHARGE;
public static final Integer MSG_NOT_FULLY_CHARGED_ORDER_TIMER;
public static final Integer MSG_RESTORE_LAST_STATE;
public static final Integer MSG_SAVE_BATT_INFO;
public static final Integer MSG_SEND_BATTERY_EXCEPTION_TIME;
public static final Integer MSG_SEND_BATTERY_TEMP;
public static final Integer MSG_SLOW_CHARGE;
public static final Integer MSG_TEMP_CONTROL_VOLTAGE;
public static final Integer MSG_UPDATE_BATTERY_HEALTH;
public static final Integer MSG_UPDATE_BATTERY_TEMP;
public static final Integer MSG_UPDATE_CHARGE;
public static final Integer MSG_UPDATE_CHARGE_ACTION;
public static final Integer MSG_UPDATE_LPD_COUNT;
public static final Integer MSG_UPDATE_POWER_OFF_DELAY;
public static final Integer MSG_UPDATE_USB_FUNCTION;
public static final Integer MSG_UPDATE_VBUS_DISALE;
public static final Integer MSG_UPDATE_WIRELESS_REVERSE_CHARGE;
public static final Integer MSG_WIRELESS_COMPOSITE;
private static final java.lang.String POEER_50;
private static final java.lang.String POWER_0;
private static final java.lang.String POWER_10;
private static final java.lang.String POWER_15;
private static final java.lang.String POWER_18;
private static final java.lang.String POWER_20;
private static final java.lang.String POWER_22_5;
private static final java.lang.String POWER_27;
private static final java.lang.String POWER_2_5;
private static final java.lang.String POWER_30;
private static final java.lang.String POWER_5;
private static final java.lang.String POWER_7_5;
private static final java.lang.String REVERSE_CHG_MODE_EVENT;
private static final java.lang.String VBUS_DISABLE_EVENT;
public static final Integer WIRELESS_OTG;
public static final Integer WIRELESS_REVERSE_OTG;
public static final Integer WIRELESS_REVERSE_WIRED;
/* # instance fields */
private java.lang.String BATTER_HEALTH_PARAMS;
private java.lang.String CHARGE_PARAMS;
private java.lang.String SLOW_CHARGE_TYPE;
private java.lang.String TEMP_COLLECTED_SCENE;
private java.lang.String TX_ADAPT_POWER;
private java.lang.String WIRELESS_COMPOSITE_TYPE;
private java.lang.String WIRELESS_REVERSE_CHARGE_PARAMS;
private android.util.ArrayMap mBatteryTemps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.HashMap mChargePowerHashMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.lang.String mChargeType;
private Integer mDeltaV;
private Boolean mIsDeltaVAssigned;
private Integer mLastOpenStatus;
public Integer mLastShortStatus;
private Integer mLastVbusDisable;
private java.util.List mNotUsbFunction;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.lang.String mTxAdapter;
private final android.os.UEventObserver mUEventObserver;
final com.android.server.MiuiBatteryStatsService this$0; //synthetic
/* # direct methods */
static Integer -$$Nest$fgetmLastOpenStatus ( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mLastOpenStatus:I */
} // .end method
static Integer -$$Nest$fgetmLastVbusDisable ( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mLastVbusDisable:I */
} // .end method
static void -$$Nest$fputmLastOpenStatus ( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mLastOpenStatus:I */
return;
} // .end method
static void -$$Nest$fputmLastVbusDisable ( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mLastVbusDisable:I */
return;
} // .end method
static java.lang.String -$$Nest$mformatTime ( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->formatTime(J)Ljava/lang/String; */
} // .end method
static void -$$Nest$mhandleNonlinearChangeOfCapacity ( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleNonlinearChangeOfCapacity(I)V */
return;
} // .end method
static void -$$Nest$msendAdjustVolBroadcast ( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendAdjustVolBroadcast()V */
return;
} // .end method
public com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ( ) {
/* .locals 19 */
/* .param p1, "this$0" # Lcom/android/server/MiuiBatteryStatsService; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 491 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
this.this$0 = v1;
/* .line 492 */
/* move-object/from16 v2, p2 */
/* invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 400 */
final String v3 = "host_connected"; // const-string v3, "host_connected"
final String v4 = "connected"; // const-string v4, "connected"
final String v5 = "configured"; // const-string v5, "configured"
/* const-string/jumbo v6, "unlocked" */
/* filled-new-array {v5, v6, v3, v4}, [Ljava/lang/String; */
java.util.Arrays .asList ( v3 );
this.mNotUsbFunction = v3;
/* .line 402 */
int v3 = 0; // const/4 v3, 0x0
this.mChargeType = v3;
/* .line 403 */
this.mTxAdapter = v3;
/* .line 405 */
int v4 = 0; // const/4 v4, 0x0
/* iput v4, v0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mDeltaV:I */
/* .line 406 */
/* iput-boolean v4, v0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mIsDeltaVAssigned:Z */
/* .line 414 */
/* new-instance v4, Ljava/util/HashMap; */
/* invoke-direct {v4}, Ljava/util/HashMap;-><init>()V */
this.mChargePowerHashMap = v4;
/* .line 415 */
/* new-instance v4, Landroid/util/ArrayMap; */
/* invoke-direct {v4}, Landroid/util/ArrayMap;-><init>()V */
this.mBatteryTemps = v4;
/* .line 418 */
final String v5 = "2.5"; // const-string v5, "2.5"
final String v6 = "5"; // const-string v6, "5"
final String v7 = "5"; // const-string v7, "5"
final String v8 = "0"; // const-string v8, "0"
final String v9 = "5"; // const-string v9, "5"
final String v10 = "10"; // const-string v10, "10"
final String v11 = "10"; // const-string v11, "10"
final String v12 = "10"; // const-string v12, "10"
final String v13 = "20"; // const-string v13, "20"
final String v14 = "20"; // const-string v14, "20"
final String v15 = "20"; // const-string v15, "20"
final String v16 = "30"; // const-string v16, "30"
final String v17 = "30"; // const-string v17, "30"
final String v18 = "50"; // const-string v18, "50"
/* filled-new-array/range {v5 ..v18}, [Ljava/lang/String; */
this.TX_ADAPT_POWER = v4;
/* .line 435 */
final String v4 = "charger_full"; // const-string v4, "charger_full"
final String v5 = "battery_authentic"; // const-string v5, "battery_authentic"
final String v6 = "cyclecount"; // const-string v6, "cyclecount"
/* const-string/jumbo v7, "soh" */
/* filled-new-array {v6, v7, v4, v5}, [Ljava/lang/String; */
this.BATTER_HEALTH_PARAMS = v4;
/* .line 442 */
final String v5 = "charger_type"; // const-string v5, "charger_type"
/* const-string/jumbo v6, "usb_voltage" */
/* const-string/jumbo v7, "usb_current" */
final String v8 = "charge_power"; // const-string v8, "charge_power"
/* const-string/jumbo v9, "tx_adapter" */
/* const-string/jumbo v10, "tx_uuid" */
final String v11 = "pd_authentication"; // const-string v11, "pd_authentication"
final String v12 = "pd_apdoMax"; // const-string v12, "pd_apdoMax"
/* const-string/jumbo v13, "vbat" */
final String v14 = "capacity"; // const-string v14, "capacity"
final String v15 = "ibat"; // const-string v15, "ibat"
final String v16 = "Tbat"; // const-string v16, "Tbat"
final String v17 = "resistance"; // const-string v17, "resistance"
/* const-string/jumbo v18, "thermal_level" */
/* filled-new-array/range {v5 ..v18}, [Ljava/lang/String; */
this.CHARGE_PARAMS = v4;
/* .line 459 */
/* const-string/jumbo v4, "wireless_reverse_enable" */
/* filled-new-array {v4}, [Ljava/lang/String; */
this.WIRELESS_REVERSE_CHARGE_PARAMS = v4;
/* .line 463 */
/* const-string/jumbo v5, "wiredOpenFFC" */
/* const-string/jumbo v6, "wirelessOpenFFC" */
/* const-string/jumbo v7, "wiredNotOpenFFC" */
/* const-string/jumbo v8, "wirelessNotOpenFFC" */
final String v9 = "releaseCharge"; // const-string v9, "releaseCharge"
final String v10 = "reverseWirelessCharge"; // const-string v10, "reverseWirelessCharge"
/* filled-new-array/range {v5 ..v10}, [Ljava/lang/String; */
this.TEMP_COLLECTED_SCENE = v4;
/* .line 472 */
/* const-string/jumbo v4, "wiredNormalSlowCharge" */
/* const-string/jumbo v5, "wiredSlowCharge" */
/* const-string/jumbo v6, "wiredRecognizedUsbFloat" */
/* filled-new-array {v6, v4, v5}, [Ljava/lang/String; */
this.SLOW_CHARGE_TYPE = v4;
/* .line 478 */
/* const-string/jumbo v4, "wireless_reverse_otg" */
/* const-string/jumbo v5, "wireless_reverse_wired" */
/* const-string/jumbo v6, "wireless_otg" */
/* filled-new-array {v6, v4, v5}, [Ljava/lang/String; */
this.WIRELESS_COMPOSITE_TYPE = v4;
/* .line 485 */
int v4 = -1; // const/4 v4, -0x1
/* iput v4, v0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mLastVbusDisable:I */
/* .line 494 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->initWiredChargePower()V */
/* .line 495 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->initBatteryTemp()V */
/* .line 496 */
/* new-instance v4, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler$BatteryUEventObserver; */
/* invoke-direct {v4, v0, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler$BatteryUEventObserver;-><init>(Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler$BatteryUEventObserver-IA;)V */
this.mUEventObserver = v4;
/* .line 497 */
final String v3 = "POWER_SUPPLY_REVERSE_CHG_MODE"; // const-string v3, "POWER_SUPPLY_REVERSE_CHG_MODE"
(( android.os.UEventObserver ) v4 ).startObserving ( v3 ); // invoke-virtual {v4, v3}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V
/* .line 498 */
final String v3 = "POWER_SUPPLY_VBUS_DISABLE"; // const-string v3, "POWER_SUPPLY_VBUS_DISABLE"
(( android.os.UEventObserver ) v4 ).startObserving ( v3 ); // invoke-virtual {v4, v3}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V
/* .line 499 */
final String v3 = "POWER_SUPPLY_CC_SHORT_VBUS"; // const-string v3, "POWER_SUPPLY_CC_SHORT_VBUS"
(( android.os.UEventObserver ) v4 ).startObserving ( v3 ); // invoke-virtual {v4, v3}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V
/* .line 500 */
final String v3 = "POWER_SUPPLY_LPD_INFOMATION"; // const-string v3, "POWER_SUPPLY_LPD_INFOMATION"
(( android.os.UEventObserver ) v4 ).startObserving ( v3 ); // invoke-virtual {v4, v3}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V
/* .line 501 */
final String v3 = "POWER_SUPPLY_MOISTURE_DET_STS"; // const-string v3, "POWER_SUPPLY_MOISTURE_DET_STS"
(( android.os.UEventObserver ) v4 ).startObserving ( v3 ); // invoke-virtual {v4, v3}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V
/* .line 502 */
/* invoke-static/range {p1 ..p1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge; */
/* const-string/jumbo v4, "smart_batt" */
v3 = (( miui.util.IMiCharge ) v3 ).isFunctionSupported ( v4 ); // invoke-virtual {v3, v4}, Lmiui/util/IMiCharge;->isFunctionSupported(Ljava/lang/String;)Z
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmSupportedSB ( v1,v3 );
/* .line 503 */
/* invoke-static/range {p1 ..p1}, Lcom/android/server/MiuiBatteryStatsService;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryStatsService;)Lmiui/util/IMiCharge; */
final String v4 = "cell1_volt"; // const-string v4, "cell1_volt"
v3 = (( miui.util.IMiCharge ) v3 ).isFunctionSupported ( v4 ); // invoke-virtual {v3, v4}, Lmiui/util/IMiCharge;->isFunctionSupported(Ljava/lang/String;)Z
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmSupportedCellVolt ( v1,v3 );
/* .line 504 */
return;
} // .end method
private java.lang.String formatTime ( Long p0 ) {
/* .locals 3 */
/* .param p1, "timestamp" # J */
/* .line 601 */
/* new-instance v0, Ljava/text/SimpleDateFormat; */
/* const-string/jumbo v1, "yyyy-MM-dd HH:mm:ss" */
/* invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V */
/* .line 602 */
/* .local v0, "sdf":Ljava/text/SimpleDateFormat; */
/* const-wide/16 v1, 0x0 */
/* cmp-long v1, p1, v1 */
/* if-lez v1, :cond_0 */
/* .line 603 */
/* new-instance v1, Ljava/util/Date; */
/* invoke-direct {v1, p1, p2}, Ljava/util/Date;-><init>(J)V */
(( java.text.SimpleDateFormat ) v0 ).format ( v1 ); // invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
/* .line 605 */
} // :cond_0
final String v1 = "None"; // const-string v1, "None"
} // .end method
private java.lang.String getBatteryChargeType ( ) {
/* .locals 1 */
/* .line 540 */
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v0 );
(( miui.util.IMiCharge ) v0 ).getBatteryChargeType ( ); // invoke-virtual {v0}, Lmiui/util/IMiCharge;->getBatteryChargeType()Ljava/lang/String;
} // .end method
private Integer getChargingPowerMax ( ) {
/* .locals 2 */
/* .line 524 */
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v0 );
(( miui.util.IMiCharge ) v0 ).getChargingPowerMax ( ); // invoke-virtual {v0}, Lmiui/util/IMiCharge;->getChargingPowerMax()Ljava/lang/String;
/* .line 525 */
/* .local v0, "powerMax":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( java.lang.String ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/String;->length()I
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 526 */
v1 = (( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).parseInt ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I
/* .line 528 */
} // :cond_0
int v1 = -1; // const/4 v1, -0x1
} // .end method
private Integer getPdAuthentication ( ) {
/* .locals 2 */
/* .line 532 */
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v0 );
(( miui.util.IMiCharge ) v0 ).getPdAuthentication ( ); // invoke-virtual {v0}, Lmiui/util/IMiCharge;->getPdAuthentication()Ljava/lang/String;
/* .line 533 */
/* .local v0, "pdAuth":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( java.lang.String ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/String;->length()I
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 534 */
v1 = (( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).parseInt ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I
/* .line 536 */
} // :cond_0
int v1 = -1; // const/4 v1, -0x1
} // .end method
private java.lang.String getTimeHours ( Long p0 ) {
/* .locals 14 */
/* .param p1, "totalMilliSeconds" # J */
/* .line 609 */
/* const-wide/16 v0, 0x0 */
/* cmp-long v0, p1, v0 */
/* if-lez v0, :cond_0 */
/* .line 610 */
/* const-wide/16 v0, 0x3e8 */
/* div-long v0, p1, v0 */
/* .line 611 */
/* .local v0, "totalSeconds":J */
/* const-wide/16 v2, 0x3c */
/* rem-long v4, v0, v2 */
/* .line 612 */
/* .local v4, "currentSecond":J */
/* div-long v6, v0, v2 */
/* .line 613 */
/* .local v6, "totalMinutes":J */
/* rem-long v8, v6, v2 */
/* .line 614 */
/* .local v8, "currentMinute":J */
/* div-long v2, v6, v2 */
/* .line 615 */
/* .local v2, "totalHour":J */
/* const-wide/16 v10, 0x18 */
/* rem-long v10, v2, v10 */
/* .line 616 */
/* .local v10, "currentHour":J */
/* new-instance v12, Ljava/lang/StringBuilder; */
/* invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v12 ).append ( v10, v11 ); // invoke-virtual {v12, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v13 = "h"; // const-string v13, "h"
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).append ( v8, v9 ); // invoke-virtual {v12, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v13 = "min"; // const-string v13, "min"
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).append ( v4, v5 ); // invoke-virtual {v12, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v13 = "s"; // const-string v13, "s"
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).toString ( ); // invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 618 */
} // .end local v0 # "totalSeconds":J
} // .end local v2 # "totalHour":J
} // .end local v4 # "currentSecond":J
} // .end local v6 # "totalMinutes":J
} // .end local v8 # "currentMinute":J
} // .end local v10 # "currentHour":J
} // :cond_0
final String v0 = "None"; // const-string v0, "None"
} // .end method
private void handleBatteryHealth ( ) {
/* .locals 10 */
/* .line 830 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 832 */
/* .local v0, "params":Landroid/os/Bundle; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = this.BATTER_HEALTH_PARAMS;
/* array-length v3, v2 */
int v4 = 0; // const/4 v4, 0x0
final String v5 = "MiuiBatteryStatsService"; // const-string v5, "MiuiBatteryStatsService"
/* if-ge v1, v3, :cond_3 */
/* .line 834 */
try { // :try_start_0
/* aget-object v2, v2, v1 */
v3 = (( java.lang.String ) v2 ).hashCode ( ); // invoke-virtual {v2}, Ljava/lang/String;->hashCode()I
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
final String v6 = "charger_full"; // const-string v6, "charger_full"
final String v7 = "battery_authentic"; // const-string v7, "battery_authentic"
/* const-string/jumbo v8, "soh" */
final String v9 = "cyclecount"; // const-string v9, "cyclecount"
/* sparse-switch v3, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
try { // :try_start_1
v2 = (( java.lang.String ) v2 ).equals ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
int v4 = 2; // const/4 v4, 0x2
/* :sswitch_1 */
v2 = (( java.lang.String ) v2 ).equals ( v7 ); // invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
int v4 = 3; // const/4 v4, 0x3
/* :sswitch_2 */
v2 = (( java.lang.String ) v2 ).equals ( v8 ); // invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
int v4 = 1; // const/4 v4, 0x1
/* :sswitch_3 */
v2 = (( java.lang.String ) v2 ).equals ( v9 ); // invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
} // :goto_1
int v4 = -1; // const/4 v4, -0x1
} // :goto_2
/* packed-switch v4, :pswitch_data_0 */
/* .line 855 */
/* .line 851 */
/* :pswitch_0 */
v2 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v2 );
(( miui.util.IMiCharge ) v2 ).getBatteryAuthentic ( ); // invoke-virtual {v2}, Lmiui/util/IMiCharge;->getBatteryAuthentic()Ljava/lang/String;
/* .line 852 */
/* .local v2, "value":Ljava/lang/String; */
(( android.os.Bundle ) v0 ).putString ( v7, v2 ); // invoke-virtual {v0, v7, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 853 */
/* .line 847 */
} // .end local v2 # "value":Ljava/lang/String;
/* :pswitch_1 */
v2 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v2 );
(( miui.util.IMiCharge ) v2 ).getBatteryChargeFull ( ); // invoke-virtual {v2}, Lmiui/util/IMiCharge;->getBatteryChargeFull()Ljava/lang/String;
/* .line 848 */
/* .restart local v2 # "value":Ljava/lang/String; */
(( android.os.Bundle ) v0 ).putString ( v6, v2 ); // invoke-virtual {v0, v6, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 849 */
/* .line 840 */
} // .end local v2 # "value":Ljava/lang/String;
/* :pswitch_2 */
v2 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v2 );
(( miui.util.IMiCharge ) v2 ).getBatterySoh ( ); // invoke-virtual {v2}, Lmiui/util/IMiCharge;->getBatterySoh()Ljava/lang/String;
/* .line 841 */
/* .restart local v2 # "value":Ljava/lang/String; */
if ( v2 != null) { // if-eqz v2, :cond_1
v3 = (( java.lang.String ) v2 ).length ( ); // invoke-virtual {v2}, Ljava/lang/String;->length()I
/* if-nez v3, :cond_2 */
/* .line 842 */
} // :cond_1
final String v3 = "-1"; // const-string v3, "-1"
/* move-object v2, v3 */
/* .line 844 */
} // :cond_2
(( android.os.Bundle ) v0 ).putString ( v8, v2 ); // invoke-virtual {v0, v8, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 845 */
/* .line 836 */
} // .end local v2 # "value":Ljava/lang/String;
/* :pswitch_3 */
v2 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v2 );
(( miui.util.IMiCharge ) v2 ).getBatteryCycleCount ( ); // invoke-virtual {v2}, Lmiui/util/IMiCharge;->getBatteryCycleCount()Ljava/lang/String;
/* .line 837 */
/* .restart local v2 # "value":Ljava/lang/String; */
(( android.os.Bundle ) v0 ).putString ( v9, v2 ); // invoke-virtual {v0, v9, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 838 */
/* .line 855 */
} // .end local v2 # "value":Ljava/lang/String;
} // :goto_3
final String v2 = "nothing to handle battery health"; // const-string v2, "nothing to handle battery health"
android.util.Slog .d ( v5,v2 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 859 */
} // :goto_4
/* .line 857 */
/* :catch_0 */
/* move-exception v2 */
/* .line 858 */
/* .local v2, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "read file about battery health error "; // const-string v4, "read file about battery health error "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v5,v3 );
/* .line 832 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_5
/* add-int/lit8 v1, v1, 0x1 */
/* goto/16 :goto_0 */
/* .line 861 */
} // .end local v1 # "i":I
} // :cond_3
final String v1 = "cc_short_vbus"; // const-string v1, "cc_short_vbus"
/* iget v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mLastShortStatus:I */
(( android.os.Bundle ) v0 ).putInt ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 862 */
/* iput v4, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mLastShortStatus:I */
/* .line 863 */
v1 = this.this$0;
v1 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 864 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "BATTER_HEALTH_PARAMS params = "; // const-string v2, "BATTER_HEALTH_PARAMS params = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v1 );
/* .line 866 */
} // :cond_4
final String v1 = "31000000094"; // const-string v1, "31000000094"
final String v2 = "battery_health"; // const-string v2, "battery_health"
/* invoke-direct {p0, v0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 867 */
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v1 );
final String v2 = "lpd_update_en"; // const-string v2, "lpd_update_en"
final String v3 = "1"; // const-string v3, "1"
(( miui.util.IMiCharge ) v1 ).setMiChargePath ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lmiui/util/IMiCharge;->setMiChargePath(Ljava/lang/String;Ljava/lang/String;)Z
/* .line 868 */
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x440aa817 -> :sswitch_3 */
/* 0x1bd8c -> :sswitch_2 */
/* 0x4ec3aeeb -> :sswitch_1 */
/* 0x7f4a9df0 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void handleBatteryTemp ( android.content.Intent p0 ) {
/* .locals 12 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 1133 */
final String v0 = "plugged"; // const-string v0, "plugged"
int v1 = -1; // const/4 v1, -0x1
v0 = (( android.content.Intent ) p1 ).getIntExtra ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 1134 */
/* .local v0, "currentPlugType":I */
/* const-string/jumbo v1, "status" */
int v2 = 1; // const/4 v2, 0x1
v1 = (( android.content.Intent ) p1 ).getIntExtra ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 1135 */
/* .local v1, "currentChargeStatus":I */
/* const-string/jumbo v3, "temperature" */
int v4 = 0; // const/4 v4, 0x0
v3 = (( android.content.Intent ) p1 ).getIntExtra ( v3, v4 ); // invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 1137 */
/* .local v3, "currentTemp":I */
int v5 = 0; // const/4 v5, 0x0
/* .line 1138 */
/* .local v5, "wirelessReverseValue":I */
int v6 = 0; // const/4 v6, 0x0
/* .line 1140 */
/* .local v6, "fastChargeModeValue":I */
v7 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v7 );
(( miui.util.IMiCharge ) v7 ).getWirelessReverseStatus ( ); // invoke-virtual {v7}, Lmiui/util/IMiCharge;->getWirelessReverseStatus()Ljava/lang/String;
/* .line 1141 */
/* .local v7, "wirelessReverseStatus":Ljava/lang/String; */
if ( v7 != null) { // if-eqz v7, :cond_0
v8 = (( java.lang.String ) v7 ).length ( ); // invoke-virtual {v7}, Ljava/lang/String;->length()I
if ( v8 != null) { // if-eqz v8, :cond_0
/* .line 1142 */
v5 = (( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).parseInt ( v7 ); // invoke-virtual {p0, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I
/* .line 1144 */
} // :cond_0
v8 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v8 );
(( miui.util.IMiCharge ) v8 ).getFastChargeModeStatus ( ); // invoke-virtual {v8}, Lmiui/util/IMiCharge;->getFastChargeModeStatus()Ljava/lang/String;
/* .line 1145 */
/* .local v8, "fastChargeStatus":Ljava/lang/String; */
if ( v8 != null) { // if-eqz v8, :cond_1
v9 = (( java.lang.String ) v8 ).length ( ); // invoke-virtual {v8}, Ljava/lang/String;->length()I
if ( v9 != null) { // if-eqz v9, :cond_1
/* .line 1146 */
v6 = (( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).parseInt ( v8 ); // invoke-virtual {p0, v8}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I
/* .line 1148 */
} // :cond_1
v9 = this.this$0;
v9 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v9 );
if ( v9 != null) { // if-eqz v9, :cond_2
/* .line 1149 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "currentPlugType = "; // const-string v10, "currentPlugType = "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v0 ); // invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = " currentChargeStatus = "; // const-string v10, " currentChargeStatus = "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v1 ); // invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = " currentTemp = "; // const-string v10, " currentTemp = "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v3 ); // invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = " wirelessReverseStatus = "; // const-string v10, " wirelessReverseStatus = "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v5 ); // invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = " fastChargeStatus = "; // const-string v10, " fastChargeStatus = "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v6 ); // invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v10 = "MiuiBatteryStatsService"; // const-string v10, "MiuiBatteryStatsService"
android.util.Slog .d ( v10,v9 );
/* .line 1154 */
} // :cond_2
/* if-lez v0, :cond_4 */
/* .line 1155 */
v9 = this.this$0;
v9 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmChargeMaxTemp ( v9 );
/* if-ge v9, v3, :cond_3 */
/* .line 1156 */
v9 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmChargeMaxTemp ( v9,v3 );
/* .line 1158 */
} // :cond_3
v9 = this.this$0;
v9 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmChargeMinTemp ( v9 );
/* if-le v9, v3, :cond_4 */
/* .line 1159 */
v9 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmChargeMinTemp ( v9,v3 );
/* .line 1163 */
} // :cond_4
int v9 = 2; // const/4 v9, 0x2
/* if-ne v1, v9, :cond_6 */
/* if-eq v0, v2, :cond_5 */
/* if-ne v0, v9, :cond_6 */
} // :cond_5
/* if-ne v6, v2, :cond_6 */
/* .line 1166 */
v10 = this.mBatteryTemps;
v11 = this.TEMP_COLLECTED_SCENE;
/* aget-object v4, v11, v4 */
(( android.util.ArrayMap ) v10 ).get ( v4 ); // invoke-virtual {v10, v4}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v4, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo; */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).calculateTemp ( v4, v3 ); // invoke-virtual {p0, v4, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->calculateTemp(Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;I)V
/* .line 1169 */
} // :cond_6
int v4 = 4; // const/4 v4, 0x4
/* if-ne v1, v9, :cond_7 */
/* if-ne v0, v4, :cond_7 */
/* if-ne v6, v2, :cond_7 */
/* .line 1172 */
v10 = this.mBatteryTemps;
v11 = this.TEMP_COLLECTED_SCENE;
/* aget-object v11, v11, v2 */
(( android.util.ArrayMap ) v10 ).get ( v11 ); // invoke-virtual {v10, v11}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v10, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo; */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).calculateTemp ( v10, v3 ); // invoke-virtual {p0, v10, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->calculateTemp(Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;I)V
/* .line 1175 */
} // :cond_7
/* if-ne v1, v9, :cond_9 */
/* if-eq v0, v2, :cond_8 */
/* if-ne v0, v9, :cond_9 */
} // :cond_8
/* if-nez v6, :cond_9 */
/* .line 1178 */
v10 = this.mBatteryTemps;
v11 = this.TEMP_COLLECTED_SCENE;
/* aget-object v11, v11, v9 */
(( android.util.ArrayMap ) v10 ).get ( v11 ); // invoke-virtual {v10, v11}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v10, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo; */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).calculateTemp ( v10, v3 ); // invoke-virtual {p0, v10, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->calculateTemp(Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;I)V
/* .line 1181 */
} // :cond_9
int v10 = 3; // const/4 v10, 0x3
/* if-ne v1, v9, :cond_a */
/* if-ne v0, v4, :cond_a */
/* if-nez v6, :cond_a */
/* .line 1184 */
v9 = this.mBatteryTemps;
v11 = this.TEMP_COLLECTED_SCENE;
/* aget-object v11, v11, v10 */
(( android.util.ArrayMap ) v9 ).get ( v11 ); // invoke-virtual {v9, v11}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v9, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo; */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).calculateTemp ( v9, v3 ); // invoke-virtual {p0, v9, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->calculateTemp(Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;I)V
/* .line 1187 */
} // :cond_a
/* if-nez v5, :cond_b */
/* if-ne v1, v10, :cond_b */
/* .line 1188 */
v9 = this.mBatteryTemps;
v10 = this.TEMP_COLLECTED_SCENE;
/* aget-object v4, v10, v4 */
(( android.util.ArrayMap ) v9 ).get ( v4 ); // invoke-virtual {v9, v4}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v4, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo; */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).calculateTemp ( v4, v3 ); // invoke-virtual {p0, v4, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->calculateTemp(Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;I)V
/* .line 1191 */
} // :cond_b
/* if-ne v5, v2, :cond_c */
/* .line 1192 */
v2 = this.mBatteryTemps;
v4 = this.TEMP_COLLECTED_SCENE;
int v9 = 5; // const/4 v9, 0x5
/* aget-object v4, v4, v9 */
(( android.util.ArrayMap ) v2 ).get ( v4 ); // invoke-virtual {v2, v4}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo; */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).calculateTemp ( v2, v3 ); // invoke-virtual {p0, v2, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->calculateTemp(Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;I)V
/* .line 1194 */
} // :cond_c
return;
} // .end method
private void handleCharge ( ) {
/* .locals 25 */
/* .line 871 */
/* move-object/from16 v1, p0 */
final String v2 = "WirelessCharging"; // const-string v2, "WirelessCharging"
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* move-object v3, v0 */
/* .line 873 */
/* .local v3, "params":Landroid/os/Bundle; */
final String v0 = "00.00.00.00"; // const-string v0, "00.00.00.00"
/* .line 874 */
/* .local v0, "txUuid":Ljava/lang/String; */
int v4 = 0; // const/4 v4, 0x0
/* .line 876 */
/* .local v4, "txAdapterValue":I */
v5 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v5 );
(( miui.util.IMiCharge ) v5 ).getBatteryChargeType ( ); // invoke-virtual {v5}, Lmiui/util/IMiCharge;->getBatteryChargeType()Ljava/lang/String;
/* .line 877 */
/* .local v5, "chargeType":Ljava/lang/String; */
v6 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v6 );
(( miui.util.IMiCharge ) v6 ).getTxAdapt ( ); // invoke-virtual {v6}, Lmiui/util/IMiCharge;->getTxAdapt()Ljava/lang/String;
/* .line 878 */
/* .local v6, "txAdapter":Ljava/lang/String; */
if ( v6 != null) { // if-eqz v6, :cond_0
v7 = (( java.lang.String ) v6 ).length ( ); // invoke-virtual {v6}, Ljava/lang/String;->length()I
if ( v7 != null) { // if-eqz v7, :cond_0
/* .line 879 */
v4 = (( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) v1 ).parseInt ( v6 ); // invoke-virtual {v1, v6}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I
/* .line 881 */
} // :cond_0
final String v6 = "-1"; // const-string v6, "-1"
/* .line 883 */
} // :goto_0
this.mTxAdapter = v6;
/* .line 884 */
v7 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v7 );
/* const-string/jumbo v8, "wireless_tx_uuid" */
v7 = (( miui.util.IMiCharge ) v7 ).isFunctionSupported ( v8 ); // invoke-virtual {v7, v8}, Lmiui/util/IMiCharge;->isFunctionSupported(Ljava/lang/String;)Z
if ( v7 != null) { // if-eqz v7, :cond_1
/* .line 885 */
v7 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v7 );
(( miui.util.IMiCharge ) v7 ).getMiChargePath ( v8 ); // invoke-virtual {v7, v8}, Lmiui/util/IMiCharge;->getMiChargePath(Ljava/lang/String;)Ljava/lang/String;
/* move-object v7, v0 */
/* .line 884 */
} // :cond_1
/* move-object v7, v0 */
/* .line 888 */
} // .end local v0 # "txUuid":Ljava/lang/String;
/* .local v7, "txUuid":Ljava/lang/String; */
} // :goto_1
int v0 = 0; // const/4 v0, 0x0
/* move v8, v0 */
/* .local v8, "i":I */
} // :goto_2
v0 = this.CHARGE_PARAMS;
/* array-length v9, v0 */
final String v10 = "MiuiBatteryStatsService"; // const-string v10, "MiuiBatteryStatsService"
/* if-ge v8, v9, :cond_15 */
/* .line 890 */
try { // :try_start_0
/* aget-object v0, v0, v8 */
v9 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_b */
/* const-string/jumbo v12, "tx_adapter" */
final String v13 = "resistance"; // const-string v13, "resistance"
/* const-string/jumbo v14, "usb_current" */
/* const-string/jumbo v15, "usb_voltage" */
/* const-string/jumbo v11, "thermal_level" */
/* move/from16 v16, v8 */
} // .end local v8 # "i":I
/* .local v16, "i":I */
final String v8 = "pd_apdoMax"; // const-string v8, "pd_apdoMax"
/* move-object/from16 v17, v10 */
/* const-string/jumbo v10, "vbat" */
/* move-object/from16 v18, v2 */
final String v2 = "ibat"; // const-string v2, "ibat"
/* move-object/from16 v19, v5 */
} // .end local v5 # "chargeType":Ljava/lang/String;
/* .local v19, "chargeType":Ljava/lang/String; */
final String v5 = "Tbat"; // const-string v5, "Tbat"
/* move/from16 v20, v4 */
} // .end local v4 # "txAdapterValue":I
/* .local v20, "txAdapterValue":I */
final String v4 = "capacity"; // const-string v4, "capacity"
/* move-object/from16 v21, v6 */
} // .end local v6 # "txAdapter":Ljava/lang/String;
/* .local v21, "txAdapter":Ljava/lang/String; */
final String v6 = "pd_authentication"; // const-string v6, "pd_authentication"
/* move-object/from16 v22, v7 */
} // .end local v7 # "txUuid":Ljava/lang/String;
/* .local v22, "txUuid":Ljava/lang/String; */
/* const-string/jumbo v7, "tx_uuid" */
/* move-object/from16 v23, v3 */
} // .end local v3 # "params":Landroid/os/Bundle;
/* .local v23, "params":Landroid/os/Bundle; */
final String v3 = "charger_type"; // const-string v3, "charger_type"
final String v1 = "charge_power"; // const-string v1, "charge_power"
/* move-object/from16 v24, v7 */
int v7 = 1; // const/4 v7, 0x1
/* sparse-switch v9, :sswitch_data_0 */
/* move-object/from16 v9, v24 */
/* goto/16 :goto_4 */
/* :sswitch_0 */
try { // :try_start_1
v0 = (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
int v0 = 0; // const/4 v0, 0x0
/* goto/16 :goto_3 */
/* :sswitch_1 */
v0 = (( java.lang.String ) v0 ).equals ( v12 ); // invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
int v0 = 4; // const/4 v0, 0x4
/* goto/16 :goto_3 */
/* :sswitch_2 */
v0 = (( java.lang.String ) v0 ).equals ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* const/16 v0, 0xc */
/* :sswitch_3 */
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
int v0 = 3; // const/4 v0, 0x3
/* :sswitch_4 */
v0 = (( java.lang.String ) v0 ).equals ( v14 ); // invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* move-object/from16 v9, v24 */
int v0 = 2; // const/4 v0, 0x2
/* goto/16 :goto_5 */
/* :sswitch_5 */
v0 = (( java.lang.String ) v0 ).equals ( v15 ); // invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* move v0, v7 */
/* :sswitch_6 */
v0 = (( java.lang.String ) v0 ).equals ( v11 ); // invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* const/16 v0, 0xd */
/* :sswitch_7 */
v0 = (( java.lang.String ) v0 ).equals ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
int v0 = 7; // const/4 v0, 0x7
/* :sswitch_8 */
v0 = (( java.lang.String ) v0 ).equals ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* const/16 v0, 0x8 */
/* :sswitch_9 */
v0 = (( java.lang.String ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* const/16 v0, 0xa */
/* :sswitch_a */
v0 = (( java.lang.String ) v0 ).equals ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* const/16 v0, 0xb */
/* :sswitch_b */
v0 = (( java.lang.String ) v0 ).equals ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* const/16 v0, 0x9 */
/* :sswitch_c */
v0 = (( java.lang.String ) v0 ).equals ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
int v0 = 6; // const/4 v0, 0x6
} // :goto_3
/* move-object/from16 v9, v24 */
} // :cond_2
/* move-object/from16 v9, v24 */
/* :sswitch_d */
/* move-object/from16 v9, v24 */
v0 = (( java.lang.String ) v0 ).equals ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
if ( v0 != null) { // if-eqz v0, :cond_3
int v0 = 5; // const/4 v0, 0x5
/* .line 986 */
/* :catch_0 */
/* move-exception v0 */
/* move-object/from16 v4, p0 */
/* move-object/from16 v3, v17 */
/* move-object/from16 v5, v18 */
/* move-object/from16 v9, v19 */
/* move/from16 v8, v20 */
/* move-object/from16 v6, v21 */
/* move-object/from16 v2, v22 */
/* move-object/from16 v1, v23 */
/* goto/16 :goto_b */
/* .line 890 */
} // :cond_3
} // :goto_4
int v0 = -1; // const/4 v0, -0x1
} // :goto_5
final String v24 = "-1"; // const-string v24, "-1"
/* packed-switch v0, :pswitch_data_0 */
/* .line 984 */
/* move-object/from16 v4, p0 */
/* move-object/from16 v5, v18 */
/* move-object/from16 v9, v19 */
/* move/from16 v8, v20 */
/* move-object/from16 v6, v21 */
/* move-object/from16 v2, v22 */
/* move-object/from16 v1, v23 */
} // .end local v19 # "chargeType":Ljava/lang/String;
} // .end local v20 # "txAdapterValue":I
} // .end local v21 # "txAdapter":Ljava/lang/String;
} // .end local v22 # "txUuid":Ljava/lang/String;
} // .end local v23 # "params":Landroid/os/Bundle;
/* .local v1, "params":Landroid/os/Bundle; */
/* .local v2, "txUuid":Ljava/lang/String; */
/* .restart local v6 # "txAdapter":Ljava/lang/String; */
/* .local v8, "txAdapterValue":I */
/* .local v9, "chargeType":Ljava/lang/String; */
/* goto/16 :goto_9 */
/* .line 980 */
} // .end local v1 # "params":Landroid/os/Bundle;
} // .end local v2 # "txUuid":Ljava/lang/String;
} // .end local v6 # "txAdapter":Ljava/lang/String;
} // .end local v8 # "txAdapterValue":I
} // .end local v9 # "chargeType":Ljava/lang/String;
/* .restart local v19 # "chargeType":Ljava/lang/String; */
/* .restart local v20 # "txAdapterValue":I */
/* .restart local v21 # "txAdapter":Ljava/lang/String; */
/* .restart local v22 # "txUuid":Ljava/lang/String; */
/* .restart local v23 # "params":Landroid/os/Bundle; */
/* :pswitch_0 */
/* move-object/from16 v3, p0 */
try { // :try_start_2
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v0 );
(( miui.util.IMiCharge ) v0 ).getBatteryThermaLevel ( ); // invoke-virtual {v0}, Lmiui/util/IMiCharge;->getBatteryThermaLevel()Ljava/lang/String;
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_1 */
/* .line 981 */
/* .local v0, "value":Ljava/lang/String; */
/* move-object/from16 v15, v23 */
} // .end local v23 # "params":Landroid/os/Bundle;
/* .local v15, "params":Landroid/os/Bundle; */
try { // :try_start_3
(( android.os.Bundle ) v15 ).putString ( v11, v0 ); // invoke-virtual {v15, v11, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 982 */
/* move-object v4, v3 */
/* move-object v1, v15 */
/* move-object/from16 v5, v18 */
/* move-object/from16 v9, v19 */
/* move/from16 v8, v20 */
/* move-object/from16 v6, v21 */
/* move-object/from16 v2, v22 */
/* goto/16 :goto_a */
/* .line 986 */
} // .end local v0 # "value":Ljava/lang/String;
} // .end local v15 # "params":Landroid/os/Bundle;
/* .restart local v23 # "params":Landroid/os/Bundle; */
/* :catch_1 */
/* move-exception v0 */
/* move-object v4, v3 */
/* move-object/from16 v3, v17 */
/* move-object/from16 v5, v18 */
/* move-object/from16 v9, v19 */
/* move/from16 v8, v20 */
/* move-object/from16 v6, v21 */
/* move-object/from16 v2, v22 */
/* move-object/from16 v1, v23 */
} // .end local v23 # "params":Landroid/os/Bundle;
/* .restart local v15 # "params":Landroid/os/Bundle; */
/* goto/16 :goto_b */
/* .line 973 */
} // .end local v15 # "params":Landroid/os/Bundle;
/* .restart local v23 # "params":Landroid/os/Bundle; */
/* :pswitch_1 */
/* move-object/from16 v3, p0 */
/* move-object/from16 v15, v23 */
} // .end local v23 # "params":Landroid/os/Bundle;
/* .restart local v15 # "params":Landroid/os/Bundle; */
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v0 );
(( miui.util.IMiCharge ) v0 ).getBatteryResistance ( ); // invoke-virtual {v0}, Lmiui/util/IMiCharge;->getBatteryResistance()Ljava/lang/String;
/* .line 974 */
/* .restart local v0 # "value":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_4
v1 = (( java.lang.String ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/String;->length()I
/* if-nez v1, :cond_5 */
/* .line 975 */
} // :cond_4
/* move-object/from16 v0, v24 */
/* .line 977 */
} // :cond_5
(( android.os.Bundle ) v15 ).putString ( v13, v0 ); // invoke-virtual {v15, v13, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 978 */
/* move-object v4, v3 */
/* move-object v1, v15 */
/* move-object/from16 v5, v18 */
/* move-object/from16 v9, v19 */
/* move/from16 v8, v20 */
/* move-object/from16 v6, v21 */
/* move-object/from16 v2, v22 */
/* goto/16 :goto_a */
/* .line 969 */
} // .end local v0 # "value":Ljava/lang/String;
} // .end local v15 # "params":Landroid/os/Bundle;
/* .restart local v23 # "params":Landroid/os/Bundle; */
/* :pswitch_2 */
/* move-object/from16 v3, p0 */
/* move-object/from16 v15, v23 */
} // .end local v23 # "params":Landroid/os/Bundle;
/* .restart local v15 # "params":Landroid/os/Bundle; */
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v0 );
(( miui.util.IMiCharge ) v0 ).getBatteryTbat ( ); // invoke-virtual {v0}, Lmiui/util/IMiCharge;->getBatteryTbat()Ljava/lang/String;
/* .line 970 */
/* .restart local v0 # "value":Ljava/lang/String; */
(( android.os.Bundle ) v15 ).putString ( v5, v0 ); // invoke-virtual {v15, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 971 */
/* move-object v4, v3 */
/* move-object v1, v15 */
/* move-object/from16 v5, v18 */
/* move-object/from16 v9, v19 */
/* move/from16 v8, v20 */
/* move-object/from16 v6, v21 */
/* move-object/from16 v2, v22 */
/* goto/16 :goto_a */
/* .line 965 */
} // .end local v0 # "value":Ljava/lang/String;
} // .end local v15 # "params":Landroid/os/Bundle;
/* .restart local v23 # "params":Landroid/os/Bundle; */
/* :pswitch_3 */
/* move-object/from16 v3, p0 */
/* move-object/from16 v15, v23 */
} // .end local v23 # "params":Landroid/os/Bundle;
/* .restart local v15 # "params":Landroid/os/Bundle; */
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v0 );
(( miui.util.IMiCharge ) v0 ).getBatteryIbat ( ); // invoke-virtual {v0}, Lmiui/util/IMiCharge;->getBatteryIbat()Ljava/lang/String;
/* .line 966 */
/* .restart local v0 # "value":Ljava/lang/String; */
(( android.os.Bundle ) v15 ).putString ( v2, v0 ); // invoke-virtual {v15, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 967 */
/* move-object v4, v3 */
/* move-object v1, v15 */
/* move-object/from16 v5, v18 */
/* move-object/from16 v9, v19 */
/* move/from16 v8, v20 */
/* move-object/from16 v6, v21 */
/* move-object/from16 v2, v22 */
/* goto/16 :goto_a */
/* .line 961 */
} // .end local v0 # "value":Ljava/lang/String;
} // .end local v15 # "params":Landroid/os/Bundle;
/* .restart local v23 # "params":Landroid/os/Bundle; */
/* :pswitch_4 */
/* move-object/from16 v3, p0 */
/* move-object/from16 v15, v23 */
} // .end local v23 # "params":Landroid/os/Bundle;
/* .restart local v15 # "params":Landroid/os/Bundle; */
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v0 );
(( miui.util.IMiCharge ) v0 ).getBatteryCapacity ( ); // invoke-virtual {v0}, Lmiui/util/IMiCharge;->getBatteryCapacity()Ljava/lang/String;
/* .line 962 */
/* .restart local v0 # "value":Ljava/lang/String; */
(( android.os.Bundle ) v15 ).putString ( v4, v0 ); // invoke-virtual {v15, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 963 */
/* move-object v4, v3 */
/* move-object v1, v15 */
/* move-object/from16 v5, v18 */
/* move-object/from16 v9, v19 */
/* move/from16 v8, v20 */
/* move-object/from16 v6, v21 */
/* move-object/from16 v2, v22 */
/* goto/16 :goto_a */
/* .line 957 */
} // .end local v0 # "value":Ljava/lang/String;
} // .end local v15 # "params":Landroid/os/Bundle;
/* .restart local v23 # "params":Landroid/os/Bundle; */
/* :pswitch_5 */
/* move-object/from16 v3, p0 */
/* move-object/from16 v15, v23 */
} // .end local v23 # "params":Landroid/os/Bundle;
/* .restart local v15 # "params":Landroid/os/Bundle; */
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v0 );
(( miui.util.IMiCharge ) v0 ).getBatteryVbat ( ); // invoke-virtual {v0}, Lmiui/util/IMiCharge;->getBatteryVbat()Ljava/lang/String;
/* .line 958 */
/* .restart local v0 # "value":Ljava/lang/String; */
(( android.os.Bundle ) v15 ).putString ( v10, v0 ); // invoke-virtual {v15, v10, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 959 */
/* move-object v4, v3 */
/* move-object v1, v15 */
/* move-object/from16 v5, v18 */
/* move-object/from16 v9, v19 */
/* move/from16 v8, v20 */
/* move-object/from16 v6, v21 */
/* move-object/from16 v2, v22 */
/* goto/16 :goto_a */
/* .line 950 */
} // .end local v0 # "value":Ljava/lang/String;
} // .end local v15 # "params":Landroid/os/Bundle;
/* .restart local v23 # "params":Landroid/os/Bundle; */
/* :pswitch_6 */
/* move-object/from16 v3, p0 */
/* move-object/from16 v15, v23 */
} // .end local v23 # "params":Landroid/os/Bundle;
/* .restart local v15 # "params":Landroid/os/Bundle; */
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v0 );
(( miui.util.IMiCharge ) v0 ).getPdApdoMax ( ); // invoke-virtual {v0}, Lmiui/util/IMiCharge;->getPdApdoMax()Ljava/lang/String;
/* .line 951 */
/* .restart local v0 # "value":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_6
v1 = (( java.lang.String ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/String;->length()I
/* if-nez v1, :cond_7 */
/* .line 952 */
} // :cond_6
/* move-object/from16 v0, v24 */
/* .line 954 */
} // :cond_7
(( android.os.Bundle ) v15 ).putString ( v8, v0 ); // invoke-virtual {v15, v8, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 955 */
/* move-object v4, v3 */
/* move-object v1, v15 */
/* move-object/from16 v5, v18 */
/* move-object/from16 v9, v19 */
/* move/from16 v8, v20 */
/* move-object/from16 v6, v21 */
/* move-object/from16 v2, v22 */
/* goto/16 :goto_a */
/* .line 943 */
} // .end local v0 # "value":Ljava/lang/String;
} // .end local v15 # "params":Landroid/os/Bundle;
/* .restart local v23 # "params":Landroid/os/Bundle; */
/* :pswitch_7 */
/* move-object/from16 v3, p0 */
/* move-object/from16 v15, v23 */
} // .end local v23 # "params":Landroid/os/Bundle;
/* .restart local v15 # "params":Landroid/os/Bundle; */
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v0 );
(( miui.util.IMiCharge ) v0 ).getPdAuthentication ( ); // invoke-virtual {v0}, Lmiui/util/IMiCharge;->getPdAuthentication()Ljava/lang/String;
/* .line 944 */
/* .restart local v0 # "value":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_8
v1 = (( java.lang.String ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/String;->length()I
/* if-nez v1, :cond_9 */
/* .line 945 */
} // :cond_8
/* move-object/from16 v0, v24 */
/* .line 947 */
} // :cond_9
(( android.os.Bundle ) v15 ).putString ( v6, v0 ); // invoke-virtual {v15, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_2 */
/* .line 948 */
/* move-object v4, v3 */
/* move-object v1, v15 */
/* move-object/from16 v5, v18 */
/* move-object/from16 v9, v19 */
/* move/from16 v8, v20 */
/* move-object/from16 v6, v21 */
/* move-object/from16 v2, v22 */
/* goto/16 :goto_a */
/* .line 986 */
} // .end local v0 # "value":Ljava/lang/String;
/* :catch_2 */
/* move-exception v0 */
/* move-object v4, v3 */
/* move-object v1, v15 */
/* move-object/from16 v3, v17 */
/* move-object/from16 v5, v18 */
/* move-object/from16 v9, v19 */
/* move/from16 v8, v20 */
/* move-object/from16 v6, v21 */
/* move-object/from16 v2, v22 */
/* goto/16 :goto_b */
/* .line 940 */
} // .end local v15 # "params":Landroid/os/Bundle;
/* .restart local v23 # "params":Landroid/os/Bundle; */
/* :pswitch_8 */
/* move-object/from16 v3, p0 */
/* move-object/from16 v15, v23 */
} // .end local v23 # "params":Landroid/os/Bundle;
/* .restart local v15 # "params":Landroid/os/Bundle; */
/* move-object/from16 v2, v22 */
} // .end local v22 # "txUuid":Ljava/lang/String;
/* .restart local v2 # "txUuid":Ljava/lang/String; */
try { // :try_start_4
(( android.os.Bundle ) v15 ).putString ( v9, v2 ); // invoke-virtual {v15, v9, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_3 */
/* .line 941 */
/* move-object v4, v3 */
/* move-object v1, v15 */
/* move-object/from16 v5, v18 */
/* move-object/from16 v9, v19 */
/* move/from16 v8, v20 */
/* move-object/from16 v6, v21 */
/* goto/16 :goto_a */
/* .line 986 */
/* :catch_3 */
/* move-exception v0 */
/* move-object v4, v3 */
/* move-object v1, v15 */
/* move-object/from16 v3, v17 */
/* move-object/from16 v5, v18 */
/* move-object/from16 v9, v19 */
/* move/from16 v8, v20 */
/* move-object/from16 v6, v21 */
/* goto/16 :goto_b */
/* .line 937 */
} // .end local v2 # "txUuid":Ljava/lang/String;
} // .end local v15 # "params":Landroid/os/Bundle;
/* .restart local v22 # "txUuid":Ljava/lang/String; */
/* .restart local v23 # "params":Landroid/os/Bundle; */
/* :pswitch_9 */
/* move-object/from16 v3, p0 */
/* move-object/from16 v2, v22 */
/* move-object/from16 v15, v23 */
} // .end local v22 # "txUuid":Ljava/lang/String;
} // .end local v23 # "params":Landroid/os/Bundle;
/* .restart local v2 # "txUuid":Ljava/lang/String; */
/* .restart local v15 # "params":Landroid/os/Bundle; */
/* move-object/from16 v6, v21 */
} // .end local v21 # "txAdapter":Ljava/lang/String;
/* .restart local v6 # "txAdapter":Ljava/lang/String; */
try { // :try_start_5
(( android.os.Bundle ) v15 ).putString ( v12, v6 ); // invoke-virtual {v15, v12, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* :try_end_5 */
/* .catch Ljava/lang/Exception; {:try_start_5 ..:try_end_5} :catch_4 */
/* .line 938 */
/* move-object v4, v3 */
/* move-object v1, v15 */
/* move-object/from16 v5, v18 */
/* move-object/from16 v9, v19 */
/* move/from16 v8, v20 */
/* goto/16 :goto_a */
/* .line 986 */
/* :catch_4 */
/* move-exception v0 */
/* move-object v4, v3 */
/* move-object v1, v15 */
/* move-object/from16 v3, v17 */
/* move-object/from16 v5, v18 */
/* move-object/from16 v9, v19 */
/* move/from16 v8, v20 */
/* goto/16 :goto_b */
/* .line 909 */
} // .end local v2 # "txUuid":Ljava/lang/String;
} // .end local v6 # "txAdapter":Ljava/lang/String;
} // .end local v15 # "params":Landroid/os/Bundle;
/* .restart local v21 # "txAdapter":Ljava/lang/String; */
/* .restart local v22 # "txUuid":Ljava/lang/String; */
/* .restart local v23 # "params":Landroid/os/Bundle; */
/* :pswitch_a */
/* move-object/from16 v3, p0 */
/* move-object/from16 v6, v21 */
/* move-object/from16 v2, v22 */
/* move-object/from16 v15, v23 */
} // .end local v21 # "txAdapter":Ljava/lang/String;
} // .end local v22 # "txUuid":Ljava/lang/String;
} // .end local v23 # "params":Landroid/os/Bundle;
/* .restart local v2 # "txUuid":Ljava/lang/String; */
/* .restart local v6 # "txAdapter":Ljava/lang/String; */
/* .restart local v15 # "params":Landroid/os/Bundle; */
final String v0 = "0"; // const-string v0, "0"
/* move-object v4, v0 */
/* .line 910 */
/* .local v4, "chargePower":Ljava/lang/String; */
/* if-lez v20, :cond_a */
/* const/16 v5, 0xf */
/* move/from16 v8, v20 */
} // .end local v20 # "txAdapterValue":I
/* .restart local v8 # "txAdapterValue":I */
/* if-ge v8, v5, :cond_b */
/* .line 911 */
try { // :try_start_6
v0 = this.TX_ADAPT_POWER;
/* add-int/lit8 v5, v8, -0x1 */
/* aget-object v0, v0, v5 */
/* .line 912 */
} // .end local v4 # "chargePower":Ljava/lang/String;
/* .local v0, "chargePower":Ljava/lang/String; */
(( android.os.Bundle ) v15 ).putString ( v1, v0 ); // invoke-virtual {v15, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* :try_end_6 */
/* .catch Ljava/lang/Exception; {:try_start_6 ..:try_end_6} :catch_5 */
/* .line 913 */
/* move-object v4, v3 */
/* move-object v1, v15 */
/* move-object/from16 v5, v18 */
/* move-object/from16 v9, v19 */
/* goto/16 :goto_a */
/* .line 986 */
} // .end local v0 # "chargePower":Ljava/lang/String;
/* :catch_5 */
/* move-exception v0 */
/* move-object v4, v3 */
/* move-object v1, v15 */
/* move-object/from16 v3, v17 */
/* move-object/from16 v5, v18 */
/* move-object/from16 v9, v19 */
/* goto/16 :goto_b */
/* .line 910 */
} // .end local v8 # "txAdapterValue":I
/* .restart local v4 # "chargePower":Ljava/lang/String; */
/* .restart local v20 # "txAdapterValue":I */
} // :cond_a
/* move/from16 v8, v20 */
/* .line 916 */
} // .end local v20 # "txAdapterValue":I
/* .restart local v8 # "txAdapterValue":I */
} // :cond_b
try { // :try_start_7
final String v5 = "Unknown"; // const-string v5, "Unknown"
/* :try_end_7 */
/* .catch Ljava/lang/Exception; {:try_start_7 ..:try_end_7} :catch_6 */
/* move-object/from16 v9, v19 */
} // .end local v19 # "chargeType":Ljava/lang/String;
/* .restart local v9 # "chargeType":Ljava/lang/String; */
try { // :try_start_8
v5 = (( java.lang.String ) v5 ).equals ( v9 ); // invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_c
/* .line 917 */
/* nop */
} // .end local v4 # "chargePower":Ljava/lang/String;
/* .restart local v0 # "chargePower":Ljava/lang/String; */
/* .line 918 */
} // .end local v0 # "chargePower":Ljava/lang/String;
/* .restart local v4 # "chargePower":Ljava/lang/String; */
} // :cond_c
final String v0 = "USB_HVDCP_3"; // const-string v0, "USB_HVDCP_3"
v0 = (( java.lang.String ) v0 ).equals ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_f
/* .line 919 */
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v0 );
(( miui.util.IMiCharge ) v0 ).getQuickChargeType ( ); // invoke-virtual {v0}, Lmiui/util/IMiCharge;->getQuickChargeType()Ljava/lang/String;
/* .line 920 */
/* .local v0, "value":Ljava/lang/String; */
v5 = (( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) v3 ).parseInt ( v0 ); // invoke-virtual {v3, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I
/* .line 921 */
/* .local v5, "valueInteger":I */
int v10 = 2; // const/4 v10, 0x2
/* if-ne v5, v10, :cond_d */
/* .line 922 */
final String v7 = "27"; // const-string v7, "27"
/* move-object v4, v7 */
/* .line 923 */
} // :cond_d
/* if-ne v5, v7, :cond_e */
/* .line 924 */
final String v7 = "18"; // const-string v7, "18"
/* move-object v4, v7 */
/* .line 926 */
} // .end local v5 # "valueInteger":I
} // :cond_e
} // :goto_6
/* move-object v0, v4 */
} // .end local v0 # "value":Ljava/lang/String;
} // :cond_f
final String v0 = "USB_PD"; // const-string v0, "USB_PD"
v0 = (( java.lang.String ) v0 ).equals ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_11 */
final String v0 = "PD_PPS"; // const-string v0, "PD_PPS"
v0 = (( java.lang.String ) v0 ).equals ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_10
/* .line 929 */
} // :cond_10
v0 = this.mChargePowerHashMap;
(( java.util.HashMap ) v0 ).get ( v9 ); // invoke-virtual {v0, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/String; */
} // .end local v4 # "chargePower":Ljava/lang/String;
/* .local v0, "chargePower":Ljava/lang/String; */
/* .line 927 */
} // .end local v0 # "chargePower":Ljava/lang/String;
/* .restart local v4 # "chargePower":Ljava/lang/String; */
} // :cond_11
} // :goto_7
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v0 );
(( miui.util.IMiCharge ) v0 ).getPdApdoMax ( ); // invoke-virtual {v0}, Lmiui/util/IMiCharge;->getPdApdoMax()Ljava/lang/String;
/* .line 931 */
} // .end local v4 # "chargePower":Ljava/lang/String;
/* .restart local v0 # "chargePower":Ljava/lang/String; */
} // :goto_8
if ( v0 != null) { // if-eqz v0, :cond_12
v4 = (( java.lang.String ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/String;->length()I
/* if-nez v4, :cond_13 */
/* .line 932 */
} // :cond_12
/* move-object/from16 v0, v24 */
/* .line 934 */
} // :cond_13
(( android.os.Bundle ) v15 ).putString ( v1, v0 ); // invoke-virtual {v15, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 935 */
/* move-object v4, v3 */
/* move-object v1, v15 */
/* move-object/from16 v5, v18 */
/* goto/16 :goto_a */
/* .line 986 */
} // .end local v0 # "chargePower":Ljava/lang/String;
} // .end local v9 # "chargeType":Ljava/lang/String;
/* .restart local v19 # "chargeType":Ljava/lang/String; */
/* :catch_6 */
/* move-exception v0 */
/* move-object/from16 v9, v19 */
/* move-object v4, v3 */
/* move-object v1, v15 */
/* move-object/from16 v3, v17 */
/* move-object/from16 v5, v18 */
} // .end local v19 # "chargeType":Ljava/lang/String;
/* .restart local v9 # "chargeType":Ljava/lang/String; */
/* goto/16 :goto_b */
/* .line 905 */
} // .end local v2 # "txUuid":Ljava/lang/String;
} // .end local v6 # "txAdapter":Ljava/lang/String;
} // .end local v8 # "txAdapterValue":I
} // .end local v9 # "chargeType":Ljava/lang/String;
} // .end local v15 # "params":Landroid/os/Bundle;
/* .restart local v19 # "chargeType":Ljava/lang/String; */
/* .restart local v20 # "txAdapterValue":I */
/* .restart local v21 # "txAdapter":Ljava/lang/String; */
/* .restart local v22 # "txUuid":Ljava/lang/String; */
/* .restart local v23 # "params":Landroid/os/Bundle; */
/* :pswitch_b */
/* move-object/from16 v3, p0 */
/* move-object/from16 v9, v19 */
/* move/from16 v8, v20 */
/* move-object/from16 v6, v21 */
/* move-object/from16 v2, v22 */
/* move-object/from16 v15, v23 */
} // .end local v19 # "chargeType":Ljava/lang/String;
} // .end local v20 # "txAdapterValue":I
} // .end local v21 # "txAdapter":Ljava/lang/String;
} // .end local v22 # "txUuid":Ljava/lang/String;
} // .end local v23 # "params":Landroid/os/Bundle;
/* .restart local v2 # "txUuid":Ljava/lang/String; */
/* .restart local v6 # "txAdapter":Ljava/lang/String; */
/* .restart local v8 # "txAdapterValue":I */
/* .restart local v9 # "chargeType":Ljava/lang/String; */
/* .restart local v15 # "params":Landroid/os/Bundle; */
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v0 );
(( miui.util.IMiCharge ) v0 ).getUsbCurrent ( ); // invoke-virtual {v0}, Lmiui/util/IMiCharge;->getUsbCurrent()Ljava/lang/String;
/* .line 906 */
/* .local v0, "value":Ljava/lang/String; */
(( android.os.Bundle ) v15 ).putString ( v14, v0 ); // invoke-virtual {v15, v14, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* :try_end_8 */
/* .catch Ljava/lang/Exception; {:try_start_8 ..:try_end_8} :catch_7 */
/* .line 907 */
/* move-object v4, v3 */
/* move-object v1, v15 */
/* move-object/from16 v5, v18 */
/* goto/16 :goto_a */
/* .line 986 */
} // .end local v0 # "value":Ljava/lang/String;
/* :catch_7 */
/* move-exception v0 */
/* move-object v4, v3 */
/* move-object v1, v15 */
/* move-object/from16 v3, v17 */
/* move-object/from16 v5, v18 */
/* goto/16 :goto_b */
/* .line 901 */
} // .end local v2 # "txUuid":Ljava/lang/String;
} // .end local v6 # "txAdapter":Ljava/lang/String;
} // .end local v8 # "txAdapterValue":I
} // .end local v9 # "chargeType":Ljava/lang/String;
} // .end local v15 # "params":Landroid/os/Bundle;
/* .restart local v19 # "chargeType":Ljava/lang/String; */
/* .restart local v20 # "txAdapterValue":I */
/* .restart local v21 # "txAdapter":Ljava/lang/String; */
/* .restart local v22 # "txUuid":Ljava/lang/String; */
/* .restart local v23 # "params":Landroid/os/Bundle; */
/* :pswitch_c */
/* move-object/from16 v3, p0 */
/* move-object/from16 v9, v19 */
/* move/from16 v8, v20 */
/* move-object/from16 v6, v21 */
/* move-object/from16 v2, v22 */
/* move-object/from16 v1, v23 */
} // .end local v19 # "chargeType":Ljava/lang/String;
} // .end local v20 # "txAdapterValue":I
} // .end local v21 # "txAdapter":Ljava/lang/String;
} // .end local v22 # "txUuid":Ljava/lang/String;
} // .end local v23 # "params":Landroid/os/Bundle;
/* .restart local v1 # "params":Landroid/os/Bundle; */
/* .restart local v2 # "txUuid":Ljava/lang/String; */
/* .restart local v6 # "txAdapter":Ljava/lang/String; */
/* .restart local v8 # "txAdapterValue":I */
/* .restart local v9 # "chargeType":Ljava/lang/String; */
try { // :try_start_9
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v0 );
(( miui.util.IMiCharge ) v0 ).getUsbVoltage ( ); // invoke-virtual {v0}, Lmiui/util/IMiCharge;->getUsbVoltage()Ljava/lang/String;
/* .line 902 */
/* .restart local v0 # "value":Ljava/lang/String; */
(( android.os.Bundle ) v1 ).putString ( v15, v0 ); // invoke-virtual {v1, v15, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* :try_end_9 */
/* .catch Ljava/lang/Exception; {:try_start_9 ..:try_end_9} :catch_8 */
/* .line 903 */
/* move-object v4, v3 */
/* move-object/from16 v5, v18 */
/* .line 986 */
} // .end local v0 # "value":Ljava/lang/String;
/* :catch_8 */
/* move-exception v0 */
/* move-object v4, v3 */
/* move-object/from16 v3, v17 */
/* move-object/from16 v5, v18 */
/* .line 892 */
} // .end local v1 # "params":Landroid/os/Bundle;
} // .end local v2 # "txUuid":Ljava/lang/String;
} // .end local v6 # "txAdapter":Ljava/lang/String;
} // .end local v8 # "txAdapterValue":I
} // .end local v9 # "chargeType":Ljava/lang/String;
/* .restart local v19 # "chargeType":Ljava/lang/String; */
/* .restart local v20 # "txAdapterValue":I */
/* .restart local v21 # "txAdapter":Ljava/lang/String; */
/* .restart local v22 # "txUuid":Ljava/lang/String; */
/* .restart local v23 # "params":Landroid/os/Bundle; */
/* :pswitch_d */
/* move-object/from16 v4, p0 */
/* move-object/from16 v9, v19 */
/* move/from16 v8, v20 */
/* move-object/from16 v6, v21 */
/* move-object/from16 v2, v22 */
/* move-object/from16 v1, v23 */
} // .end local v19 # "chargeType":Ljava/lang/String;
} // .end local v20 # "txAdapterValue":I
} // .end local v21 # "txAdapter":Ljava/lang/String;
} // .end local v22 # "txUuid":Ljava/lang/String;
} // .end local v23 # "params":Landroid/os/Bundle;
/* .restart local v1 # "params":Landroid/os/Bundle; */
/* .restart local v2 # "txUuid":Ljava/lang/String; */
/* .restart local v6 # "txAdapter":Ljava/lang/String; */
/* .restart local v8 # "txAdapterValue":I */
/* .restart local v9 # "chargeType":Ljava/lang/String; */
/* if-lez v8, :cond_14 */
/* .line 893 */
/* move-object/from16 v5, v18 */
try { // :try_start_a
this.mChargeType = v5;
/* .line 894 */
(( android.os.Bundle ) v1 ).putString ( v3, v5 ); // invoke-virtual {v1, v3, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 896 */
} // :cond_14
/* move-object/from16 v5, v18 */
this.mChargeType = v9;
/* .line 897 */
(( android.os.Bundle ) v1 ).putString ( v3, v9 ); // invoke-virtual {v1, v3, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 899 */
/* .line 986 */
/* :catch_9 */
/* move-exception v0 */
/* move-object/from16 v3, v17 */
/* .line 984 */
} // :goto_9
final String v0 = "no chagre params to handle"; // const-string v0, "no chagre params to handle"
/* :try_end_a */
/* .catch Ljava/lang/Exception; {:try_start_a ..:try_end_a} :catch_9 */
/* move-object/from16 v3, v17 */
try { // :try_start_b
android.util.Slog .d ( v3,v0 );
/* :try_end_b */
/* .catch Ljava/lang/Exception; {:try_start_b ..:try_end_b} :catch_a */
/* .line 988 */
} // :goto_a
/* .line 986 */
/* :catch_a */
/* move-exception v0 */
} // .end local v1 # "params":Landroid/os/Bundle;
} // .end local v2 # "txUuid":Ljava/lang/String;
} // .end local v9 # "chargeType":Ljava/lang/String;
} // .end local v16 # "i":I
/* .restart local v3 # "params":Landroid/os/Bundle; */
/* .local v4, "txAdapterValue":I */
/* .local v5, "chargeType":Ljava/lang/String; */
/* .restart local v7 # "txUuid":Ljava/lang/String; */
/* .local v8, "i":I */
/* :catch_b */
/* move-exception v0 */
/* move-object v9, v5 */
/* move/from16 v16, v8 */
/* move-object v5, v2 */
/* move v8, v4 */
/* move-object v2, v7 */
/* move-object v4, v1 */
/* move-object v1, v3 */
/* move-object v3, v10 */
/* .line 987 */
} // .end local v3 # "params":Landroid/os/Bundle;
} // .end local v4 # "txAdapterValue":I
} // .end local v5 # "chargeType":Ljava/lang/String;
} // .end local v7 # "txUuid":Ljava/lang/String;
/* .local v0, "e":Ljava/lang/Exception; */
/* .restart local v1 # "params":Landroid/os/Bundle; */
/* .restart local v2 # "txUuid":Ljava/lang/String; */
/* .local v8, "txAdapterValue":I */
/* .restart local v9 # "chargeType":Ljava/lang/String; */
/* .restart local v16 # "i":I */
} // :goto_b
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "read charge file error "; // const-string v10, "read charge file error "
(( java.lang.StringBuilder ) v7 ).append ( v10 ); // invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v7 );
/* .line 888 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_c
/* add-int/lit8 v0, v16, 0x1 */
/* move-object v3, v1 */
/* move-object v7, v2 */
/* move-object v1, v4 */
/* move-object v2, v5 */
/* move v4, v8 */
/* move-object v5, v9 */
/* move v8, v0 */
} // .end local v16 # "i":I
/* .local v0, "i":I */
/* goto/16 :goto_2 */
} // .end local v0 # "i":I
} // .end local v1 # "params":Landroid/os/Bundle;
} // .end local v2 # "txUuid":Ljava/lang/String;
} // .end local v9 # "chargeType":Ljava/lang/String;
/* .restart local v3 # "params":Landroid/os/Bundle; */
/* .restart local v4 # "txAdapterValue":I */
/* .restart local v5 # "chargeType":Ljava/lang/String; */
/* .restart local v7 # "txUuid":Ljava/lang/String; */
/* .local v8, "i":I */
} // :cond_15
/* move-object v9, v5 */
/* move-object v2, v7 */
/* move/from16 v16, v8 */
/* move v8, v4 */
/* move-object v4, v1 */
/* move-object v1, v3 */
/* move-object v3, v10 */
/* .line 990 */
} // .end local v3 # "params":Landroid/os/Bundle;
} // .end local v4 # "txAdapterValue":I
} // .end local v5 # "chargeType":Ljava/lang/String;
} // .end local v7 # "txUuid":Ljava/lang/String;
/* .restart local v1 # "params":Landroid/os/Bundle; */
/* .restart local v2 # "txUuid":Ljava/lang/String; */
/* .local v8, "txAdapterValue":I */
/* .restart local v9 # "chargeType":Ljava/lang/String; */
v0 = this.this$0;
v0 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_16
/* .line 991 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Charge params = "; // const-string v5, "Charge params = "
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v0 );
/* .line 993 */
} // :cond_16
final String v0 = "31000000094"; // const-string v0, "31000000094"
final String v3 = "charge"; // const-string v3, "charge"
/* invoke-direct {v4, v1, v0, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 994 */
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x3575ef6a -> :sswitch_d */
/* -0x269fe3dd -> :sswitch_c */
/* -0x40aeb46 -> :sswitch_b */
/* 0x27ab41 -> :sswitch_a */
/* 0x31370c -> :sswitch_9 */
/* 0x371fdf -> :sswitch_8 */
/* 0x277c8abf -> :sswitch_7 */
/* 0x314d48dc -> :sswitch_6 */
/* 0x3df703a3 -> :sswitch_5 */
/* 0x5b7165be -> :sswitch_4 */
/* 0x5fd237ba -> :sswitch_3 */
/* 0x6f175839 -> :sswitch_2 */
/* 0x79579494 -> :sswitch_1 */
/* 0x7f510a9b -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_d */
/* :pswitch_c */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void handleChargeAction ( ) {
/* .locals 8 */
/* .line 1020 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 1021 */
/* .local v0, "params":Landroid/os/Bundle; */
v1 = this.mChargeType;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1022 */
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmChargeStartTime ( v1 );
/* move-result-wide v1 */
/* invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->formatTime(J)Ljava/lang/String; */
final String v2 = "charge_start_time"; // const-string v2, "charge_start_time"
(( android.os.Bundle ) v0 ).putString ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1023 */
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmChargeEndTime ( v1 );
/* move-result-wide v1 */
/* invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->formatTime(J)Ljava/lang/String; */
final String v2 = "charge_end_time"; // const-string v2, "charge_end_time"
(( android.os.Bundle ) v0 ).putString ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1024 */
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmFullChargeStartTime ( v1 );
/* move-result-wide v1 */
/* invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->formatTime(J)Ljava/lang/String; */
final String v2 = "full_charge_start_time"; // const-string v2, "full_charge_start_time"
(( android.os.Bundle ) v0 ).putString ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1025 */
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmFullChargeEndTime ( v1 );
/* move-result-wide v1 */
/* invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->formatTime(J)Ljava/lang/String; */
final String v2 = "full_charge_end_time"; // const-string v2, "full_charge_end_time"
(( android.os.Bundle ) v0 ).putString ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1026 */
v1 = this.this$0;
v1 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmChargeStartCapacity ( v1 );
final String v2 = "charge_start_capacity"; // const-string v2, "charge_start_capacity"
(( android.os.Bundle ) v0 ).putInt ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 1027 */
v1 = this.this$0;
v1 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmChargeEndCapacity ( v1 );
final String v2 = "charge_end_capacity"; // const-string v2, "charge_end_capacity"
(( android.os.Bundle ) v0 ).putInt ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 1028 */
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmChargeEndTime ( v1 );
/* move-result-wide v1 */
v3 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmChargeStartTime ( v3 );
/* move-result-wide v3 */
/* sub-long/2addr v1, v3 */
/* .line 1029 */
/* invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->getTimeHours(J)Ljava/lang/String; */
/* .line 1028 */
final String v2 = "charge_total_time"; // const-string v2, "charge_total_time"
(( android.os.Bundle ) v0 ).putString ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1030 */
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmFullChargeEndTime ( v1 );
/* move-result-wide v1 */
v3 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmFullChargeStartTime ( v3 );
/* move-result-wide v3 */
/* sub-long/2addr v1, v3 */
/* .line 1031 */
/* invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->getTimeHours(J)Ljava/lang/String; */
/* .line 1030 */
final String v2 = "full_charge_total_time"; // const-string v2, "full_charge_total_time"
(( android.os.Bundle ) v0 ).putString ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1032 */
final String v1 = "charger_type"; // const-string v1, "charger_type"
v2 = this.mChargeType;
(( android.os.Bundle ) v0 ).putString ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1033 */
/* const-string/jumbo v1, "tx_adapter" */
v2 = this.mTxAdapter;
(( android.os.Bundle ) v0 ).putString ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1034 */
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmAppUsageStats ( v1 );
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmContext ( v1 );
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmChargeStartTime ( v1 );
/* move-result-wide v4 */
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmChargeEndTime ( v1 );
/* move-result-wide v6 */
/* .line 1035 */
/* invoke-virtual/range {v2 ..v7}, Lcom/android/server/MiuiAppUsageStats;->getTop3Apps(Landroid/content/Context;JJ)Ljava/util/ArrayList; */
/* .line 1034 */
/* const-string/jumbo v2, "top3_app" */
(( android.os.Bundle ) v0 ).putStringArrayList ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V
/* .line 1036 */
v1 = this.this$0;
v1 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmChargeMaxTemp ( v1 );
final String v2 = "charge_battery_max_temp"; // const-string v2, "charge_battery_max_temp"
(( android.os.Bundle ) v0 ).putInt ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 1037 */
v1 = this.this$0;
v1 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmChargeMinTemp ( v1 );
final String v2 = "charge_battery_min_temp"; // const-string v2, "charge_battery_min_temp"
(( android.os.Bundle ) v0 ).putInt ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 1038 */
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmScreenOnTime ( v1 );
/* move-result-wide v1 */
/* invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->getTimeHours(J)Ljava/lang/String; */
final String v2 = "screen_on_time"; // const-string v2, "screen_on_time"
(( android.os.Bundle ) v0 ).putString ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1039 */
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmChargeEndTime ( v1 );
/* move-result-wide v1 */
v3 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmChargeStartTime ( v3 );
/* move-result-wide v3 */
/* sub-long/2addr v1, v3 */
v3 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmScreenOnTime ( v3 );
/* move-result-wide v3 */
/* sub-long/2addr v1, v3 */
/* .line 1040 */
/* invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->getTimeHours(J)Ljava/lang/String; */
/* .line 1039 */
final String v2 = "screen_off_time"; // const-string v2, "screen_off_time"
(( android.os.Bundle ) v0 ).putString ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1042 */
v1 = this.this$0;
v1 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1043 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Charge Action params = "; // const-string v2, "Charge Action params = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiBatteryStatsService"; // const-string v2, "MiuiBatteryStatsService"
android.util.Slog .d ( v2,v1 );
/* .line 1045 */
} // :cond_0
final String v1 = "31000000094"; // const-string v1, "31000000094"
final String v2 = "charge_action"; // const-string v2, "charge_action"
/* invoke-direct {p0, v0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 1046 */
int v1 = 0; // const/4 v1, 0x0
this.mChargeType = v1;
/* .line 1047 */
v1 = this.this$0;
/* const-wide/16 v2, 0x0 */
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmFullChargeStartTime ( v1,v2,v3 );
/* .line 1048 */
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmFullChargeEndTime ( v1,v2,v3 );
/* .line 1049 */
v1 = this.this$0;
int v4 = 0; // const/4 v4, 0x0
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmChargeMaxTemp ( v1,v4 );
/* .line 1050 */
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmChargeMinTemp ( v1,v4 );
/* .line 1051 */
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmScreenOnTime ( v1,v2,v3 );
/* .line 1053 */
} // :cond_1
return;
} // .end method
private void handleCheckSoc ( ) {
/* .locals 6 */
/* .line 1385 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 1386 */
/* .local v0, "params":Landroid/os/Bundle; */
int v1 = 0; // const/4 v1, 0x0
/* .line 1387 */
/* .local v1, "socValue":I */
v2 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v2 );
(( miui.util.IMiCharge ) v2 ).getBatteryCapacity ( ); // invoke-virtual {v2}, Lmiui/util/IMiCharge;->getBatteryCapacity()Ljava/lang/String;
/* .line 1388 */
/* .local v2, "soc":Ljava/lang/String; */
v3 = android.text.TextUtils .isEmpty ( v2 );
/* if-nez v3, :cond_0 */
/* .line 1389 */
v1 = (( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).parseInt ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I
/* .line 1391 */
} // :cond_0
/* const/16 v3, 0x64 */
final String v4 = "not_fully_charged"; // const-string v4, "not_fully_charged"
/* if-eq v1, v3, :cond_1 */
/* .line 1392 */
final String v3 = "notFullyCharged"; // const-string v3, "notFullyCharged"
(( android.os.Bundle ) v0 ).putString ( v4, v3 ); // invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1394 */
} // :cond_1
v3 = this.this$0;
v3 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 1395 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "NOT_FULLY_CHARGED_PARAMS params = "; // const-string v5, "NOT_FULLY_CHARGED_PARAMS params = "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "MiuiBatteryStatsService"; // const-string v5, "MiuiBatteryStatsService"
android.util.Slog .d ( v5,v3 );
/* .line 1397 */
} // :cond_2
final String v3 = "31000000094"; // const-string v3, "31000000094"
/* invoke-direct {p0, v0, v3, v4}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 1398 */
return;
} // .end method
private void handleIntermittentCharge ( ) {
/* .locals 5 */
/* .line 1281 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 1282 */
/* .local v0, "params":Landroid/os/Bundle; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "mDischargingCount = "; // const-string v2, "mDischargingCount = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
v2 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmDischargingCount ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiBatteryStatsService"; // const-string v2, "MiuiBatteryStatsService"
android.util.Slog .d ( v2,v1 );
/* .line 1283 */
v1 = this.this$0;
v1 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmDischargingCount ( v1 );
int v3 = 3; // const/4 v3, 0x3
/* if-lt v1, v3, :cond_1 */
/* .line 1284 */
final String v1 = "intermittentCharge"; // const-string v1, "intermittentCharge"
final String v3 = "intermittent_charge"; // const-string v3, "intermittent_charge"
(( android.os.Bundle ) v0 ).putString ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1285 */
v1 = this.this$0;
v1 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1286 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Intermittent Charge params = "; // const-string v4, "Intermittent Charge params = "
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v1 );
/* .line 1288 */
} // :cond_0
final String v1 = "31000000094"; // const-string v1, "31000000094"
/* invoke-direct {p0, v0, v1, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 1290 */
} // :cond_1
v1 = this.this$0;
int v2 = 0; // const/4 v2, 0x0
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmDischargingCount ( v1,v2 );
/* .line 1291 */
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmIsHandleIntermittentCharge ( v1,v2 );
/* .line 1292 */
return;
} // .end method
private void handleLPDInfomation ( java.lang.String p0 ) {
/* .locals 13 */
/* .param p1, "lpdInfomation" # Ljava/lang/String; */
/* .line 1333 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 1334 */
/* .local v0, "params":Landroid/os/Bundle; */
int v1 = 0; // const/4 v1, 0x0
/* .line 1335 */
/* .local v1, "dp":I */
int v2 = 0; // const/4 v2, 0x0
/* .line 1336 */
/* .local v2, "dm":I */
int v3 = 0; // const/4 v3, 0x0
/* .line 1337 */
/* .local v3, "sbu1":I */
int v4 = 0; // const/4 v4, 0x0
/* .line 1338 */
/* .local v4, "sbu2":I */
int v5 = 0; // const/4 v5, 0x0
/* .line 1339 */
/* .local v5, "lines":[Ljava/lang/String; */
v6 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v6, :cond_0 */
/* .line 1340 */
final String v6 = "\n"; // const-string v6, "\n"
(( java.lang.String ) p1 ).split ( v6 ); // invoke-virtual {p1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1342 */
} // :cond_0
final String v6 = "MiuiBatteryStatsService"; // const-string v6, "MiuiBatteryStatsService"
if ( v5 != null) { // if-eqz v5, :cond_9
/* array-length v7, v5 */
/* if-nez v7, :cond_1 */
/* goto/16 :goto_2 */
/* .line 1346 */
} // :cond_1
v7 = com.android.internal.util.ArrayUtils .size ( v5 );
/* add-int/lit8 v7, v7, -0x1 */
/* .local v7, "i":I */
} // :goto_0
final String v8 = "LPD_SBU2_RES="; // const-string v8, "LPD_SBU2_RES="
final String v9 = "LPD_SBU1_RES="; // const-string v9, "LPD_SBU1_RES="
final String v10 = "LPD_DM_RES="; // const-string v10, "LPD_DM_RES="
final String v11 = "LPD_DP_RES="; // const-string v11, "LPD_DP_RES="
/* if-ltz v7, :cond_7 */
/* .line 1347 */
/* aget-object v12, v5, v7 */
v12 = android.text.TextUtils .isEmpty ( v12 );
if ( v12 != null) { // if-eqz v12, :cond_2
/* .line 1348 */
/* .line 1350 */
} // :cond_2
/* aget-object v12, v5, v7 */
v12 = (( java.lang.String ) v12 ).startsWith ( v11 ); // invoke-virtual {v12, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v12 != null) { // if-eqz v12, :cond_3
/* .line 1351 */
/* aget-object v8, v5, v7 */
v9 = (( java.lang.String ) v11 ).length ( ); // invoke-virtual {v11}, Ljava/lang/String;->length()I
(( java.lang.String ) v8 ).substring ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;
v1 = (( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).parseInt ( v8 ); // invoke-virtual {p0, v8}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I
/* .line 1352 */
final String v8 = "lpd_dp_res"; // const-string v8, "lpd_dp_res"
(( android.os.Bundle ) v0 ).putInt ( v8, v1 ); // invoke-virtual {v0, v8, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 1353 */
/* .line 1355 */
} // :cond_3
/* aget-object v11, v5, v7 */
v11 = (( java.lang.String ) v11 ).startsWith ( v10 ); // invoke-virtual {v11, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v11 != null) { // if-eqz v11, :cond_4
/* .line 1356 */
/* aget-object v8, v5, v7 */
v9 = (( java.lang.String ) v10 ).length ( ); // invoke-virtual {v10}, Ljava/lang/String;->length()I
(( java.lang.String ) v8 ).substring ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;
v2 = (( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).parseInt ( v8 ); // invoke-virtual {p0, v8}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I
/* .line 1357 */
final String v8 = "lpd_dm_res"; // const-string v8, "lpd_dm_res"
(( android.os.Bundle ) v0 ).putInt ( v8, v2 ); // invoke-virtual {v0, v8, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 1358 */
/* .line 1360 */
} // :cond_4
/* aget-object v10, v5, v7 */
v10 = (( java.lang.String ) v10 ).startsWith ( v9 ); // invoke-virtual {v10, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v10 != null) { // if-eqz v10, :cond_5
/* .line 1361 */
/* aget-object v8, v5, v7 */
v9 = (( java.lang.String ) v9 ).length ( ); // invoke-virtual {v9}, Ljava/lang/String;->length()I
(( java.lang.String ) v8 ).substring ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;
v3 = (( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).parseInt ( v8 ); // invoke-virtual {p0, v8}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I
/* .line 1362 */
final String v8 = "lpd_sbu1_res"; // const-string v8, "lpd_sbu1_res"
(( android.os.Bundle ) v0 ).putInt ( v8, v3 ); // invoke-virtual {v0, v8, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 1363 */
/* .line 1365 */
} // :cond_5
/* aget-object v9, v5, v7 */
v9 = (( java.lang.String ) v9 ).startsWith ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v9 != null) { // if-eqz v9, :cond_6
/* .line 1366 */
/* aget-object v9, v5, v7 */
v8 = (( java.lang.String ) v8 ).length ( ); // invoke-virtual {v8}, Ljava/lang/String;->length()I
(( java.lang.String ) v9 ).substring ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;
v4 = (( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).parseInt ( v8 ); // invoke-virtual {p0, v8}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I
/* .line 1367 */
final String v8 = "lpd_sbu2_res"; // const-string v8, "lpd_sbu2_res"
(( android.os.Bundle ) v0 ).putInt ( v8, v4 ); // invoke-virtual {v0, v8, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 1346 */
} // :cond_6
} // :goto_1
/* add-int/lit8 v7, v7, -0x1 */
/* goto/16 :goto_0 */
/* .line 1371 */
} // .end local v7 # "i":I
} // :cond_7
v7 = this.this$0;
v7 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v7 );
if ( v7 != null) { // if-eqz v7, :cond_8
/* .line 1372 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v11 ); // invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v1 ); // invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v10 ); // invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v2 ); // invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v3 ); // invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v6,v7 );
/* .line 1374 */
} // :cond_8
final String v6 = "31000000094"; // const-string v6, "31000000094"
final String v7 = "battery_lpd_infomation"; // const-string v7, "battery_lpd_infomation"
/* invoke-direct {p0, v0, v6, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 1375 */
return;
/* .line 1343 */
} // :cond_9
} // :goto_2
final String v7 = "LpdInfomation has no data"; // const-string v7, "LpdInfomation has no data"
android.util.Slog .e ( v6,v7 );
/* .line 1344 */
return;
} // .end method
private void handleLpdCountReport ( ) {
/* .locals 3 */
/* .line 1378 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 1379 */
/* .local v0, "params":Landroid/os/Bundle; */
v1 = this.this$0;
v1 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmLpdCount ( v1 );
final String v2 = "lpd_count"; // const-string v2, "lpd_count"
(( android.os.Bundle ) v0 ).putInt ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 1380 */
final String v1 = "31000000094"; // const-string v1, "31000000094"
/* invoke-direct {p0, v0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 1381 */
v1 = this.this$0;
int v2 = 0; // const/4 v2, 0x0
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmLpdCount ( v1,v2 );
/* .line 1382 */
return;
} // .end method
private void handleNonlinearChangeOfCapacity ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "currentSoc" # I */
/* .line 1295 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 1296 */
/* .local v0, "params":Landroid/os/Bundle; */
v1 = this.this$0;
v1 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmLastSoc ( v1 );
/* sub-int v1, p1, v1 */
/* .line 1297 */
/* .local v1, "capacityChangeValue":I */
v2 = this.this$0;
v2 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v2 );
final String v3 = "MiuiBatteryStatsService"; // const-string v3, "MiuiBatteryStatsService"
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1298 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "currentSoc = "; // const-string v4, "currentSoc = "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " mLastSoc = "; // const-string v4, " mLastSoc = "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.this$0;
v4 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmLastSoc ( v4 );
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " capacityChangeValue = "; // const-string v4, " capacityChangeValue = "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v2 );
/* .line 1299 */
} // :cond_0
v2 = java.lang.Math .abs ( v1 );
int v4 = 1; // const/4 v4, 0x1
/* if-le v2, v4, :cond_2 */
/* .line 1300 */
final String v2 = "capacity_change_value"; // const-string v2, "capacity_change_value"
(( android.os.Bundle ) v0 ).putInt ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 1301 */
v2 = this.this$0;
v2 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1302 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Nonlinear Change Of Capacity params = "; // const-string v4, "Nonlinear Change Of Capacity params = "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v2 );
/* .line 1304 */
} // :cond_1
final String v2 = "31000000094"; // const-string v2, "31000000094"
final String v3 = "nonlinear_change_of_capacity"; // const-string v3, "nonlinear_change_of_capacity"
/* invoke-direct {p0, v0, v2, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 1306 */
} // :cond_2
return;
} // .end method
private void handlePowerOff ( Boolean p0 ) {
/* .locals 6 */
/* .param p1, "shutdown_start" # Z */
/* .line 1056 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 1057 */
/* .local v0, "params":Landroid/os/Bundle; */
/* const-string/jumbo v1, "shutdown_delay" */
/* if-nez p1, :cond_0 */
/* .line 1058 */
int v2 = 1; // const/4 v2, 0x1
(( android.os.Bundle ) v0 ).putInt ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 1059 */
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v1 );
(( miui.util.IMiCharge ) v1 ).getBatteryVbat ( ); // invoke-virtual {v1}, Lmiui/util/IMiCharge;->getBatteryVbat()Ljava/lang/String;
/* const-string/jumbo v3, "vbat" */
(( android.os.Bundle ) v0 ).putString ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1060 */
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v1 );
(( miui.util.IMiCharge ) v1 ).getBatteryTbat ( ); // invoke-virtual {v1}, Lmiui/util/IMiCharge;->getBatteryTbat()Ljava/lang/String;
final String v3 = "Tbat"; // const-string v3, "Tbat"
(( android.os.Bundle ) v0 ).putString ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1061 */
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v1 );
(( miui.util.IMiCharge ) v1 ).getBatteryIbat ( ); // invoke-virtual {v1}, Lmiui/util/IMiCharge;->getBatteryIbat()Ljava/lang/String;
final String v3 = "ibat"; // const-string v3, "ibat"
(( android.os.Bundle ) v0 ).putString ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1062 */
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v1 );
int v3 = 5; // const/4 v3, 0x5
/* const-wide/32 v4, 0x88b8 */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) v1 ).sendMessageDelayed ( v3, v2, v4, v5 ); // invoke-virtual {v1, v3, v2, v4, v5}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IZJ)V
/* .line 1064 */
} // :cond_0
int v2 = 2; // const/4 v2, 0x2
(( android.os.Bundle ) v0 ).putInt ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 1066 */
} // :goto_0
v1 = this.this$0;
v1 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1067 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Power Off Action params = "; // const-string v2, "Power Off Action params = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiBatteryStatsService"; // const-string v2, "MiuiBatteryStatsService"
android.util.Slog .d ( v2,v1 );
/* .line 1069 */
} // :cond_1
final String v1 = "31000000094"; // const-string v1, "31000000094"
final String v2 = "power_off"; // const-string v2, "power_off"
/* invoke-direct {p0, v0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 1070 */
return;
} // .end method
private void handleSlowCharge ( ) {
/* .locals 15 */
/* .line 1233 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 1234 */
/* .local v0, "params":Landroid/os/Bundle; */
int v1 = 0; // const/4 v1, 0x0
/* .line 1235 */
/* .local v1, "battTempValue":I */
int v2 = 0; // const/4 v2, 0x0
/* .line 1236 */
/* .local v2, "thermalLevelValue":I */
int v3 = 0; // const/4 v3, 0x0
/* .line 1237 */
/* .local v3, "quickChargeTypeValue":I */
int v4 = 0; // const/4 v4, 0x0
/* .line 1238 */
/* .local v4, "chargeCurrentSocValue":I */
v5 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v5 );
(( miui.util.IMiCharge ) v5 ).getBatteryCapacity ( ); // invoke-virtual {v5}, Lmiui/util/IMiCharge;->getBatteryCapacity()Ljava/lang/String;
/* .line 1239 */
/* .local v5, "chargeCurrentSoc":Ljava/lang/String; */
v6 = android.text.TextUtils .isEmpty ( v5 );
/* if-nez v6, :cond_0 */
/* .line 1240 */
v4 = (( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).parseInt ( v5 ); // invoke-virtual {p0, v5}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I
/* .line 1242 */
} // :cond_0
v6 = this.this$0;
v6 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v6 );
final String v7 = "MiuiBatteryStatsService"; // const-string v7, "MiuiBatteryStatsService"
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 1243 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "chargeCurrentSoc = "; // const-string v8, "chargeCurrentSoc = "
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v8 = ", chargeStartSoc = "; // const-string v8, ", chargeStartSoc = "
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v8 = this.this$0;
v8 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmChargeStartCapacity ( v8 );
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v7,v6 );
/* .line 1245 */
} // :cond_1
final String v6 = "USB_FLOAT"; // const-string v6, "USB_FLOAT"
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->getBatteryChargeType()Ljava/lang/String; */
v6 = (( java.lang.String ) v6 ).equals ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* const-string/jumbo v8, "slow_charge_type" */
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 1246 */
v6 = this.SLOW_CHARGE_TYPE;
int v9 = 0; // const/4 v9, 0x0
/* aget-object v6, v6, v9 */
(( android.os.Bundle ) v0 ).putString ( v8, v6 ); // invoke-virtual {v0, v8, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* goto/16 :goto_0 */
/* .line 1247 */
} // :cond_2
v6 = this.this$0;
v6 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmChargeStartCapacity ( v6 );
/* sub-int v6, v4, v6 */
int v9 = 3; // const/4 v9, 0x3
/* if-ge v6, v9, :cond_9 */
/* .line 1248 */
v6 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v6 );
(( miui.util.IMiCharge ) v6 ).getPdAuthentication ( ); // invoke-virtual {v6}, Lmiui/util/IMiCharge;->getPdAuthentication()Ljava/lang/String;
/* .line 1249 */
/* .local v6, "pdVerified":Ljava/lang/String; */
v10 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v10 );
(( miui.util.IMiCharge ) v10 ).getQuickChargeType ( ); // invoke-virtual {v10}, Lmiui/util/IMiCharge;->getQuickChargeType()Ljava/lang/String;
/* .line 1250 */
/* .local v10, "quickChargeType":Ljava/lang/String; */
v11 = android.text.TextUtils .isEmpty ( v10 );
/* if-nez v11, :cond_3 */
/* .line 1251 */
v3 = (( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).parseInt ( v10 ); // invoke-virtual {p0, v10}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I
/* .line 1253 */
} // :cond_3
v11 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v11 );
(( miui.util.IMiCharge ) v11 ).getBatteryThermaLevel ( ); // invoke-virtual {v11}, Lmiui/util/IMiCharge;->getBatteryThermaLevel()Ljava/lang/String;
/* .line 1254 */
/* .local v11, "thermalLevel":Ljava/lang/String; */
v12 = android.text.TextUtils .isEmpty ( v11 );
/* if-nez v12, :cond_4 */
/* .line 1255 */
v2 = (( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).parseInt ( v11 ); // invoke-virtual {p0, v11}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I
/* .line 1257 */
} // :cond_4
v12 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v12 );
(( miui.util.IMiCharge ) v12 ).getBatteryTbat ( ); // invoke-virtual {v12}, Lmiui/util/IMiCharge;->getBatteryTbat()Ljava/lang/String;
/* .line 1258 */
/* .local v12, "battTemp":Ljava/lang/String; */
v13 = android.text.TextUtils .isEmpty ( v12 );
/* if-nez v13, :cond_5 */
/* .line 1259 */
v1 = (( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).parseInt ( v12 ); // invoke-virtual {p0, v12}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I
/* .line 1261 */
} // :cond_5
v13 = this.this$0;
v13 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v13 );
if ( v13 != null) { // if-eqz v13, :cond_6
/* .line 1262 */
/* new-instance v13, Ljava/lang/StringBuilder; */
/* invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V */
final String v14 = "pdVerified = "; // const-string v14, "pdVerified = "
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v6 ); // invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v14 = ", quickChargeTypeValue = "; // const-string v14, ", quickChargeTypeValue = "
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v3 ); // invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v14 = ", thermalLevelValue = "; // const-string v14, ", thermalLevelValue = "
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v2 ); // invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v14 = ", battTempValue = "; // const-string v14, ", battTempValue = "
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v1 ); // invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).toString ( ); // invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v7,v13 );
/* .line 1265 */
} // :cond_6
final String v13 = "1"; // const-string v13, "1"
v13 = (( java.lang.String ) v13 ).equals ( v6 ); // invoke-virtual {v13, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* const/16 v14, 0x1d6 */
if ( v13 != null) { // if-eqz v13, :cond_8
/* if-gt v1, v14, :cond_7 */
/* const/16 v13, 0x9 */
/* if-le v2, v13, :cond_8 */
/* .line 1266 */
} // :cond_7
v9 = this.SLOW_CHARGE_TYPE;
int v13 = 1; // const/4 v13, 0x1
/* aget-object v9, v9, v13 */
(( android.os.Bundle ) v0 ).putString ( v8, v9 ); // invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1267 */
} // :cond_8
/* if-lt v3, v9, :cond_9 */
/* const/16 v9, 0x50 */
/* if-ge v4, v9, :cond_9 */
/* const/16 v9, 0x8 */
/* if-ge v2, v9, :cond_9 */
/* if-ge v1, v14, :cond_9 */
/* .line 1268 */
v9 = this.SLOW_CHARGE_TYPE;
int v13 = 2; // const/4 v13, 0x2
/* aget-object v9, v9, v13 */
(( android.os.Bundle ) v0 ).putString ( v8, v9 ); // invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1272 */
} // .end local v6 # "pdVerified":Ljava/lang/String;
} // .end local v10 # "quickChargeType":Ljava/lang/String;
} // .end local v11 # "thermalLevel":Ljava/lang/String;
} // .end local v12 # "battTemp":Ljava/lang/String;
} // :cond_9
} // :goto_0
v6 = (( android.os.Bundle ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/os/Bundle;->size()I
if ( v6 != null) { // if-eqz v6, :cond_b
/* .line 1273 */
v6 = this.this$0;
v6 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v6 );
if ( v6 != null) { // if-eqz v6, :cond_a
/* .line 1274 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Slow Charge params = "; // const-string v8, "Slow Charge params = "
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v7,v6 );
/* .line 1276 */
} // :cond_a
final String v6 = "31000000094"; // const-string v6, "31000000094"
/* const-string/jumbo v7, "slow_charge" */
/* invoke-direct {p0, v0, v6, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 1278 */
} // :cond_b
return;
} // .end method
private void handleSreiesDeltaVoltage ( android.content.Intent p0 ) {
/* .locals 10 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 1082 */
/* const-string/jumbo v0, "temperature" */
int v1 = 0; // const/4 v1, 0x0
v0 = (( android.content.Intent ) p1 ).getIntExtra ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 1083 */
/* .local v0, "temp":I */
final String v2 = "level"; // const-string v2, "level"
int v3 = -1; // const/4 v3, -0x1
v2 = (( android.content.Intent ) p1 ).getIntExtra ( v2, v3 ); // invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 1084 */
/* .local v2, "soc":I */
/* new-instance v3, Landroid/os/Bundle; */
/* invoke-direct {v3}, Landroid/os/Bundle;-><init>()V */
/* .line 1085 */
/* .local v3, "params":Landroid/os/Bundle; */
int v4 = 0; // const/4 v4, 0x0
/* .line 1086 */
/* .local v4, "currentV":I */
/* const/16 v5, 0xc8 */
final String v6 = "MiuiBatteryStatsService"; // const-string v6, "MiuiBatteryStatsService"
/* if-lt v0, v5, :cond_3 */
/* const/16 v5, 0x15e */
/* if-gt v0, v5, :cond_3 */
/* const/16 v5, 0x23 */
/* if-lt v2, v5, :cond_3 */
/* const/16 v5, 0x50 */
/* if-gt v2, v5, :cond_3 */
/* .line 1087 */
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v1 );
(( miui.util.IMiCharge ) v1 ).getBatteryIbat ( ); // invoke-virtual {v1}, Lmiui/util/IMiCharge;->getBatteryIbat()Ljava/lang/String;
v1 = (( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).parseInt ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I
/* .line 1088 */
/* .local v1, "ibat":I */
v5 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v5 );
final String v7 = "cell1_volt"; // const-string v7, "cell1_volt"
(( miui.util.IMiCharge ) v5 ).getMiChargePath ( v7 ); // invoke-virtual {v5, v7}, Lmiui/util/IMiCharge;->getMiChargePath(Ljava/lang/String;)Ljava/lang/String;
v5 = (( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).parseInt ( v5 ); // invoke-virtual {p0, v5}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I
/* .line 1089 */
/* .local v5, "vCell1":I */
v7 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v7 );
final String v8 = "cell2_volt"; // const-string v8, "cell2_volt"
(( miui.util.IMiCharge ) v7 ).getMiChargePath ( v8 ); // invoke-virtual {v7, v8}, Lmiui/util/IMiCharge;->getMiChargePath(Ljava/lang/String;)Ljava/lang/String;
v7 = (( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).parseInt ( v7 ); // invoke-virtual {p0, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I
/* .line 1090 */
/* .local v7, "vCell2":I */
/* const v8, 0x186a0 */
/* if-gt v1, v8, :cond_2 */
/* const/16 v8, 0xf0a */
/* if-lt v5, v8, :cond_2 */
/* const/16 v9, 0x1004 */
/* if-gt v5, v9, :cond_2 */
/* if-lt v7, v8, :cond_2 */
/* if-gt v7, v9, :cond_2 */
/* .line 1091 */
/* sub-int v8, v5, v7 */
v4 = java.lang.Math .abs ( v8 );
/* .line 1092 */
/* iget v8, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mDeltaV:I */
/* if-le v4, v8, :cond_0 */
/* .line 1093 */
/* iput v4, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mDeltaV:I */
/* .line 1094 */
} // :cond_0
v8 = this.this$0;
v8 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v8 );
if ( v8 != null) { // if-eqz v8, :cond_1
/* .line 1095 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "cell1_volt = "; // const-string v9, "cell1_volt = "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v5 ); // invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = ", cell2_volt = "; // const-string v9, ", cell2_volt = "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = ", mDeltaV = "; // const-string v9, ", mDeltaV = "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v9, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mDeltaV:I */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v6,v8 );
/* .line 1097 */
} // :cond_1
int v6 = 1; // const/4 v6, 0x1
/* iput-boolean v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mIsDeltaVAssigned:Z */
/* .line 1099 */
} // .end local v1 # "ibat":I
} // .end local v5 # "vCell1":I
} // .end local v7 # "vCell2":I
} // :cond_2
} // :cond_3
/* iget-boolean v5, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mIsDeltaVAssigned:Z */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 1100 */
/* iget v5, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mDeltaV:I */
/* const-string/jumbo v7, "series_delta_voltage" */
(( android.os.Bundle ) v3 ).putInt ( v7, v5 ); // invoke-virtual {v3, v7, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 1101 */
v5 = this.this$0;
v5 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v5 );
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 1102 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v8, "series delta voltage params = " */
(( java.lang.StringBuilder ) v5 ).append ( v8 ); // invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v6,v5 );
/* .line 1104 */
} // :cond_4
final String v5 = "31000000094"; // const-string v5, "31000000094"
/* invoke-direct {p0, v3, v5, v7}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 1105 */
/* iput v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mDeltaV:I */
/* .line 1106 */
/* iput-boolean v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mIsDeltaVAssigned:Z */
/* .line 1108 */
} // :goto_0
return;
} // .end method
private void handleUsbFunction ( android.content.Intent p0 ) {
/* .locals 8 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 1111 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 1112 */
/* .local v0, "params":Landroid/os/Bundle; */
/* const-string/jumbo v1, "unlocked" */
int v2 = 0; // const/4 v2, 0x0
v1 = (( android.content.Intent ) p1 ).getBooleanExtra ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z
/* .line 1113 */
/* .local v1, "dataUnlock":Z */
final String v2 = "data_unlock"; // const-string v2, "data_unlock"
(( android.os.Bundle ) v0 ).putInt ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 1115 */
v2 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v2 );
v2 = (( miui.util.IMiCharge ) v2 ).isUSB32 ( ); // invoke-virtual {v2}, Lmiui/util/IMiCharge;->isUSB32()Z
final String v3 = "USB32"; // const-string v3, "USB32"
(( android.os.Bundle ) v0 ).putInt ( v3, v2 ); // invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 1117 */
(( android.content.Intent ) p1 ).getExtras ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;
(( android.os.Bundle ) v2 ).keySet ( ); // invoke-virtual {v2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;
/* .line 1118 */
/* .local v2, "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
final String v3 = ""; // const-string v3, ""
/* .line 1119 */
/* .local v3, "usb_function":Ljava/lang/String; */
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_2
/* check-cast v5, Ljava/lang/String; */
/* .line 1120 */
/* .local v5, "key":Ljava/lang/String; */
v6 = v6 = this.mNotUsbFunction;
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 1121 */
} // :cond_0
v6 = android.text.TextUtils .isEmpty ( v3 );
/* if-nez v6, :cond_1 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = ","; // const-string v7, ","
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1122 */
} // :cond_1
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1123 */
} // .end local v5 # "key":Ljava/lang/String;
/* .line 1124 */
} // :cond_2
v4 = android.text.TextUtils .isEmpty ( v3 );
if ( v4 != null) { // if-eqz v4, :cond_3
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "none"; // const-string v5, "none"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1125 */
} // :cond_3
/* const-string/jumbo v4, "usb_function" */
(( android.os.Bundle ) v0 ).putString ( v4, v3 ); // invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1126 */
v4 = this.this$0;
v4 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v4 );
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 1127 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Usb function params = "; // const-string v5, "Usb function params = "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "MiuiBatteryStatsService"; // const-string v5, "MiuiBatteryStatsService"
android.util.Slog .d ( v5,v4 );
/* .line 1129 */
} // :cond_4
final String v4 = "31000000092"; // const-string v4, "31000000092"
/* const-string/jumbo v5, "usb_deivce" */
/* invoke-direct {p0, v0, v4, v5}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 1130 */
return;
} // .end method
private void handleVbusDisable ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "vbusDisable" # I */
/* .line 1073 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 1074 */
/* .local v0, "params":Landroid/os/Bundle; */
/* const-string/jumbo v1, "vbus_disable" */
(( android.os.Bundle ) v0 ).putInt ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 1075 */
final String v2 = "31000000094"; // const-string v2, "31000000094"
/* invoke-direct {p0, v0, v2, v1}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 1076 */
v1 = this.this$0;
v1 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1077 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "VBUS params = "; // const-string v2, "VBUS params = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiBatteryStatsService"; // const-string v2, "MiuiBatteryStatsService"
android.util.Slog .d ( v2,v1 );
/* .line 1079 */
} // :cond_0
return;
} // .end method
private void handleWirelessComposite ( ) {
/* .locals 5 */
/* .line 1309 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 1310 */
/* .local v0, "params":Landroid/os/Bundle; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "mPlugType = "; // const-string v2, "mPlugType = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
v2 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmPlugType ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " mLastOpenStatus = "; // const-string v2, " mLastOpenStatus = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mLastOpenStatus:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " mOtgConnected = "; // const-string v2, " mOtgConnected = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
v2 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmOtgConnected ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiBatteryStatsService"; // const-string v2, "MiuiBatteryStatsService"
android.util.Slog .d ( v2,v1 );
/* .line 1312 */
v1 = this.this$0;
v1 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmPlugType ( v1 );
/* const-string/jumbo v3, "wireless_composite" */
int v4 = 4; // const/4 v4, 0x4
/* if-ne v1, v4, :cond_0 */
v1 = this.this$0;
v1 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmOtgConnected ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1313 */
v1 = this.WIRELESS_COMPOSITE_TYPE;
int v4 = 0; // const/4 v4, 0x0
/* aget-object v1, v1, v4 */
(( android.os.Bundle ) v0 ).putString ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1316 */
} // :cond_0
/* iget v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mLastOpenStatus:I */
/* if-lez v1, :cond_1 */
v1 = this.this$0;
v1 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmOtgConnected ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1317 */
v1 = this.WIRELESS_COMPOSITE_TYPE;
int v4 = 1; // const/4 v4, 0x1
/* aget-object v1, v1, v4 */
(( android.os.Bundle ) v0 ).putString ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1320 */
} // :cond_1
/* iget v1, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->mLastOpenStatus:I */
/* if-lez v1, :cond_2 */
v1 = this.this$0;
v1 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmPlugType ( v1 );
/* if-lez v1, :cond_2 */
v1 = this.this$0;
v1 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmPlugType ( v1 );
/* if-eq v1, v4, :cond_2 */
/* .line 1321 */
v1 = this.WIRELESS_COMPOSITE_TYPE;
int v4 = 2; // const/4 v4, 0x2
/* aget-object v1, v1, v4 */
(( android.os.Bundle ) v0 ).putString ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1324 */
} // :cond_2
} // :goto_0
v1 = (( android.os.Bundle ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/os/Bundle;->size()I
/* if-lez v1, :cond_4 */
/* .line 1325 */
v1 = this.this$0;
v1 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 1326 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "wireless composite = " */
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v1 );
/* .line 1328 */
} // :cond_3
final String v1 = "31000000094"; // const-string v1, "31000000094"
/* invoke-direct {p0, v0, v1, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 1330 */
} // :cond_4
return;
} // .end method
private void handleWirelessReverseCharge ( ) {
/* .locals 6 */
/* .line 997 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 999 */
/* .local v0, "params":Landroid/os/Bundle; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = this.WIRELESS_REVERSE_CHARGE_PARAMS;
/* array-length v3, v2 */
final String v4 = "MiuiBatteryStatsService"; // const-string v4, "MiuiBatteryStatsService"
/* if-ge v1, v3, :cond_1 */
/* .line 1001 */
try { // :try_start_0
/* aget-object v2, v2, v1 */
v3 = (( java.lang.String ) v2 ).hashCode ( ); // invoke-virtual {v2}, Ljava/lang/String;->hashCode()I
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* const-string/jumbo v5, "wireless_reverse_enable" */
/* packed-switch v3, :pswitch_data_0 */
} // :cond_0
/* :pswitch_0 */
try { // :try_start_1
v2 = (( java.lang.String ) v2 ).equals ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
int v2 = 0; // const/4 v2, 0x0
} // :goto_1
int v2 = -1; // const/4 v2, -0x1
} // :goto_2
/* packed-switch v2, :pswitch_data_1 */
/* .line 1007 */
/* .line 1003 */
/* :pswitch_1 */
v2 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v2 );
(( miui.util.IMiCharge ) v2 ).getWirelessReverseStatus ( ); // invoke-virtual {v2}, Lmiui/util/IMiCharge;->getWirelessReverseStatus()Ljava/lang/String;
/* .line 1004 */
/* .local v2, "value":Ljava/lang/String; */
(( android.os.Bundle ) v0 ).putString ( v5, v2 ); // invoke-virtual {v0, v5, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1005 */
/* .line 1007 */
} // .end local v2 # "value":Ljava/lang/String;
} // :goto_3
final String v2 = "no wireless reverse charge params to handle"; // const-string v2, "no wireless reverse charge params to handle"
android.util.Slog .d ( v4,v2 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 1011 */
} // :goto_4
/* .line 1009 */
/* :catch_0 */
/* move-exception v2 */
/* .line 1010 */
/* .local v2, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "read wireless reverse charge file error "; // const-string v5, "read wireless reverse charge file error "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v4,v3 );
/* .line 999 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_5
/* add-int/lit8 v1, v1, 0x1 */
/* .line 1013 */
} // .end local v1 # "i":I
} // :cond_1
v1 = this.this$0;
v1 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1014 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Wireless Reverse Charge params = "; // const-string v2, "Wireless Reverse Charge params = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v1 );
/* .line 1016 */
} // :cond_2
final String v1 = "31000000094"; // const-string v1, "31000000094"
/* const-string/jumbo v2, "wireless_reverse_charge" */
/* invoke-direct {p0, v0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 1017 */
return;
/* :pswitch_data_0 */
/* .packed-switch -0x708fdbbf */
/* :pswitch_0 */
} // .end packed-switch
/* :pswitch_data_1 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
private void initBatteryTemp ( ) {
/* .locals 4 */
/* .line 517 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = this.TEMP_COLLECTED_SCENE;
/* array-length v2, v1 */
/* if-ge v0, v2, :cond_0 */
/* .line 518 */
/* new-instance v2, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo; */
v3 = this.this$0;
/* aget-object v1, v1, v0 */
/* invoke-direct {v2, v3, v1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;-><init>(Lcom/android/server/MiuiBatteryStatsService;Ljava/lang/String;)V */
/* move-object v1, v2 */
/* .line 519 */
/* .local v1, "batteryTempInfo":Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo; */
v2 = this.mBatteryTemps;
v3 = this.TEMP_COLLECTED_SCENE;
/* aget-object v3, v3, v0 */
(( android.util.ArrayMap ) v2 ).put ( v3, v1 ); // invoke-virtual {v2, v3, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 517 */
} // .end local v1 # "batteryTempInfo":Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;
/* add-int/lit8 v0, v0, 0x1 */
/* .line 521 */
} // .end local v0 # "i":I
} // :cond_0
return;
} // .end method
private void initWiredChargePower ( ) {
/* .locals 3 */
/* .line 507 */
v0 = this.mChargePowerHashMap;
final String v1 = "USB"; // const-string v1, "USB"
final String v2 = "2.5"; // const-string v2, "2.5"
(( java.util.HashMap ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 508 */
v0 = this.mChargePowerHashMap;
final String v1 = "OCP"; // const-string v1, "OCP"
(( java.util.HashMap ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 509 */
v0 = this.mChargePowerHashMap;
final String v1 = "USB_DCP"; // const-string v1, "USB_DCP"
final String v2 = "7.5"; // const-string v2, "7.5"
(( java.util.HashMap ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 510 */
v0 = this.mChargePowerHashMap;
final String v1 = "USB_CDP"; // const-string v1, "USB_CDP"
(( java.util.HashMap ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 511 */
v0 = this.mChargePowerHashMap;
final String v1 = "USB_FLOAT"; // const-string v1, "USB_FLOAT"
final String v2 = "5"; // const-string v2, "5"
(( java.util.HashMap ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 512 */
v0 = this.mChargePowerHashMap;
final String v1 = "USB_HVDCP"; // const-string v1, "USB_HVDCP"
final String v2 = "15"; // const-string v2, "15"
(( java.util.HashMap ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 513 */
v0 = this.mChargePowerHashMap;
final String v1 = "USB_HVDCP_3P5"; // const-string v1, "USB_HVDCP_3P5"
final String v2 = "22.5"; // const-string v2, "22.5"
(( java.util.HashMap ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 514 */
return;
} // .end method
private void orderCheckSocTimer ( ) {
/* .locals 7 */
/* .line 584 */
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmAlarmManager ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmPendingIntentCheckSoc ( v0 );
/* if-nez v0, :cond_0 */
/* .line 587 */
} // :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->getPdAuthentication()I */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_2 */
v0 = this.this$0;
v0 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmIsOrderedCheckSocTimer ( v0 );
/* if-nez v0, :cond_2 */
/* .line 588 */
v0 = /* invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->getChargingPowerMax()I */
int v2 = 2; // const/4 v2, 0x2
/* const/16 v3, 0x21 */
/* if-ne v0, v3, :cond_1 */
/* .line 589 */
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmIsOrderedCheckSocTimer ( v0,v1 );
/* .line 590 */
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmAlarmManager ( v0 );
/* .line 591 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v3 */
/* const-wide/32 v5, 0x6ddd00 */
/* add-long/2addr v3, v5 */
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmPendingIntentCheckSoc ( v1 );
/* .line 590 */
(( android.app.AlarmManager ) v0 ).setExactAndAllowWhileIdle ( v2, v3, v4, v1 ); // invoke-virtual {v0, v2, v3, v4, v1}, Landroid/app/AlarmManager;->setExactAndAllowWhileIdle(IJLandroid/app/PendingIntent;)V
/* .line 592 */
} // :cond_1
v0 = /* invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->getChargingPowerMax()I */
/* if-le v0, v3, :cond_2 */
/* .line 593 */
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fputmIsOrderedCheckSocTimer ( v0,v1 );
/* .line 594 */
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmAlarmManager ( v0 );
/* .line 595 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v3 */
/* const-wide/32 v5, 0x36ee80 */
/* add-long/2addr v3, v5 */
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmPendingIntentCheckSoc ( v1 );
/* .line 594 */
(( android.app.AlarmManager ) v0 ).setExactAndAllowWhileIdle ( v2, v3, v4, v1 ); // invoke-virtual {v0, v2, v3, v4, v1}, Landroid/app/AlarmManager;->setExactAndAllowWhileIdle(IJLandroid/app/PendingIntent;)V
/* .line 598 */
} // :cond_2
} // :goto_0
return;
/* .line 585 */
} // :cond_3
} // :goto_1
return;
} // .end method
private void sendAdjustVolBroadcast ( ) {
/* .locals 3 */
/* .line 693 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "miui.intent.action.ADJUST_VOLTAGE"; // const-string v1, "miui.intent.action.ADJUST_VOLTAGE"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 694 */
/* .local v0, "intent":Landroid/content/Intent; */
/* const/high16 v1, 0x40000000 # 2.0f */
(( android.content.Intent ) v0 ).addFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 695 */
final String v1 = "miui.intent.extra.ADJUST_VOLTAGE_TS"; // const-string v1, "miui.intent.extra.ADJUST_VOLTAGE_TS"
v2 = com.android.server.MiuiBatteryStatsService .-$$Nest$sfgetmIsSatisfyTempSocCondition ( );
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 696 */
final String v1 = "miui.intent.extra.ADJUST_VOLTAGE_TL"; // const-string v1, "miui.intent.extra.ADJUST_VOLTAGE_TL"
v2 = com.android.server.MiuiBatteryStatsService .-$$Nest$sfgetmIsSatisfyTempLevelCondition ( );
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 697 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "send broadcast adjust , mIsSatisfyTempSocCondition = " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = com.android.server.MiuiBatteryStatsService .-$$Nest$sfgetmIsSatisfyTempSocCondition ( );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = " , mIsSatisfyTempLevelCondition = "; // const-string v2, " , mIsSatisfyTempLevelCondition = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = com.android.server.MiuiBatteryStatsService .-$$Nest$sfgetmIsSatisfyTempLevelCondition ( );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiBatteryStatsService"; // const-string v2, "MiuiBatteryStatsService"
android.util.Slog .i ( v2,v1 );
/* .line 700 */
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmContext ( v1 );
v2 = android.os.UserHandle.ALL;
(( android.content.Context ) v1 ).sendBroadcastAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 701 */
return;
} // .end method
private void sendBatteryTempData ( ) {
/* .locals 5 */
/* .line 1197 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = this.TEMP_COLLECTED_SCENE;
/* array-length v2, v1 */
/* if-ge v0, v2, :cond_3 */
/* .line 1198 */
v2 = this.mBatteryTemps;
/* aget-object v1, v1, v0 */
(( android.util.ArrayMap ) v2 ).get ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo; */
/* .line 1199 */
/* .local v1, "batteryTempInfo":Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo; */
v2 = this.TEMP_COLLECTED_SCENE;
/* aget-object v2, v2, v0 */
(( com.android.server.MiuiBatteryStatsService$BatteryTempInfo ) v1 ).getCondition ( ); // invoke-virtual {v1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->getCondition()Ljava/lang/String;
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_0 */
return;
/* .line 1200 */
} // :cond_0
v2 = (( com.android.server.MiuiBatteryStatsService$BatteryTempInfo ) v1 ).getDataCount ( ); // invoke-virtual {v1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->getDataCount()I
/* if-nez v2, :cond_1 */
/* .line 1201 */
} // :cond_1
(( com.android.server.MiuiBatteryStatsService$BatteryTempInfo ) v1 ).setAverageTemp ( ); // invoke-virtual {v1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->setAverageTemp()V
/* .line 1202 */
/* new-instance v2, Landroid/os/Bundle; */
/* invoke-direct {v2}, Landroid/os/Bundle;-><init>()V */
/* .line 1203 */
/* .local v2, "params":Landroid/os/Bundle; */
final String v3 = "battery_charge_discharge_state"; // const-string v3, "battery_charge_discharge_state"
(( com.android.server.MiuiBatteryStatsService$BatteryTempInfo ) v1 ).getCondition ( ); // invoke-virtual {v1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->getCondition()Ljava/lang/String;
(( android.os.Bundle ) v2 ).putString ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1204 */
final String v3 = "max_temp"; // const-string v3, "max_temp"
v4 = (( com.android.server.MiuiBatteryStatsService$BatteryTempInfo ) v1 ).getMaxTemp ( ); // invoke-virtual {v1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->getMaxTemp()I
(( android.os.Bundle ) v2 ).putInt ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 1205 */
final String v3 = "min_temp"; // const-string v3, "min_temp"
v4 = (( com.android.server.MiuiBatteryStatsService$BatteryTempInfo ) v1 ).getMinTemp ( ); // invoke-virtual {v1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->getMinTemp()I
(( android.os.Bundle ) v2 ).putInt ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 1206 */
final String v3 = "ave_temp"; // const-string v3, "ave_temp"
v4 = (( com.android.server.MiuiBatteryStatsService$BatteryTempInfo ) v1 ).getAverageTemp ( ); // invoke-virtual {v1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->getAverageTemp()I
(( android.os.Bundle ) v2 ).putInt ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 1207 */
final String v3 = "31000000094"; // const-string v3, "31000000094"
final String v4 = "battery_temp"; // const-string v4, "battery_temp"
/* invoke-direct {p0, v2, v3, v4}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 1208 */
(( com.android.server.MiuiBatteryStatsService$BatteryTempInfo ) v1 ).reset ( ); // invoke-virtual {v1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->reset()V
/* .line 1209 */
v3 = this.this$0;
v3 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 1210 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
v4 = this.TEMP_COLLECTED_SCENE;
/* aget-object v4, v4, v0 */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " params = "; // const-string v4, " params = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "MiuiBatteryStatsService"; // const-string v4, "MiuiBatteryStatsService"
android.util.Slog .d ( v4,v3 );
/* .line 1197 */
} // .end local v1 # "batteryTempInfo":Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;
} // .end local v2 # "params":Landroid/os/Bundle;
} // :cond_2
} // :goto_1
/* add-int/lit8 v0, v0, 0x1 */
/* goto/16 :goto_0 */
/* .line 1213 */
} // .end local v0 # "i":I
} // :cond_3
return;
} // .end method
private void sendBatteryTempVoltageTimeData ( com.android.server.MiuiBatteryStatsService$BatteryTempVoltageTimeInfo p0 ) {
/* .locals 3 */
/* .param p1, "info" # Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo; */
/* .line 1216 */
com.android.server.MiuiBatteryStatsService$BatteryTempVoltageTimeInfo .-$$Nest$mstopTime ( p1 );
/* .line 1217 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 1218 */
/* .local v0, "params":Landroid/os/Bundle; */
(( com.android.server.MiuiBatteryStatsService$BatteryTempVoltageTimeInfo ) p1 ).getHighTempTotalTime ( ); // invoke-virtual {p1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->getHighTempTotalTime()J
/* move-result-wide v1 */
/* invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->getTimeHours(J)Ljava/lang/String; */
final String v2 = "high_temp_time"; // const-string v2, "high_temp_time"
(( android.os.Bundle ) v0 ).putString ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1219 */
(( com.android.server.MiuiBatteryStatsService$BatteryTempVoltageTimeInfo ) p1 ).getHighVoltageTotalTime ( ); // invoke-virtual {p1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->getHighVoltageTotalTime()J
/* move-result-wide v1 */
/* invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->getTimeHours(J)Ljava/lang/String; */
final String v2 = "high_voltage_time"; // const-string v2, "high_voltage_time"
(( android.os.Bundle ) v0 ).putString ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1220 */
(( com.android.server.MiuiBatteryStatsService$BatteryTempVoltageTimeInfo ) p1 ).getHighTempVoltageTotalTime ( ); // invoke-virtual {p1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->getHighTempVoltageTotalTime()J
/* move-result-wide v1 */
/* invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->getTimeHours(J)Ljava/lang/String; */
final String v2 = "high_temp_voltage_time"; // const-string v2, "high_temp_voltage_time"
(( android.os.Bundle ) v0 ).putString ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1221 */
(( com.android.server.MiuiBatteryStatsService$BatteryTempVoltageTimeInfo ) p1 ).getFullChargeTotalTime ( ); // invoke-virtual {p1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->getFullChargeTotalTime()J
/* move-result-wide v1 */
/* invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->getTimeHours(J)Ljava/lang/String; */
final String v2 = "full_charge_total_time"; // const-string v2, "full_charge_total_time"
(( android.os.Bundle ) v0 ).putString ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1222 */
final String v1 = "charge_status"; // const-string v1, "charge_status"
(( com.android.server.MiuiBatteryStatsService$BatteryTempVoltageTimeInfo ) p1 ).getCondition ( ); // invoke-virtual {p1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->getCondition()Ljava/lang/String;
(( android.os.Bundle ) v0 ).putString ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1224 */
final String v1 = "31000000094"; // const-string v1, "31000000094"
final String v2 = "battery_high_temp_voltage"; // const-string v2, "battery_high_temp_voltage"
/* invoke-direct {p0, v0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendOneTrackInfo(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 1226 */
v1 = this.this$0;
v1 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1227 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( com.android.server.MiuiBatteryStatsService$BatteryTempVoltageTimeInfo ) p1 ).getCondition ( ); // invoke-virtual {p1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;->getCondition()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " params = "; // const-string v2, " params = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiBatteryStatsService"; // const-string v2, "MiuiBatteryStatsService"
android.util.Slog .d ( v2,v1 );
/* .line 1229 */
} // :cond_0
com.android.server.MiuiBatteryStatsService$BatteryTempVoltageTimeInfo .-$$Nest$mclearTime ( p1 );
/* .line 1230 */
return;
} // .end method
private void sendOneTrackInfo ( android.os.Bundle p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 4 */
/* .param p1, "params" # Landroid/os/Bundle; */
/* .param p2, "id" # Ljava/lang/String; */
/* .param p3, "event" # Ljava/lang/String; */
/* .line 675 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
return;
/* .line 676 */
} // :cond_0
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "onetrack.action.TRACK_EVENT"; // const-string v1, "onetrack.action.TRACK_EVENT"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 677 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.miui.analytics"; // const-string v1, "com.miui.analytics"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 678 */
final String v1 = "APP_ID"; // const-string v1, "APP_ID"
(( android.content.Intent ) v0 ).putExtra ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 679 */
final String v1 = "EVENT_NAME"; // const-string v1, "EVENT_NAME"
(( android.content.Intent ) v0 ).putExtra ( v1, p3 ); // invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 680 */
final String v1 = "PACKAGE"; // const-string v1, "PACKAGE"
final String v2 = "Android"; // const-string v2, "Android"
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 681 */
(( android.content.Intent ) v0 ).putExtras ( p1 ); // invoke-virtual {v0, p1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
/* .line 682 */
/* sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v1, :cond_1 */
/* .line 683 */
int v1 = 2; // const/4 v1, 0x2
(( android.content.Intent ) v0 ).setFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 686 */
} // :cond_1
try { // :try_start_0
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmContext ( v1 );
v2 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v1 ).startServiceAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
/* :try_end_0 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 689 */
/* .line 687 */
/* :catch_0 */
/* move-exception v1 */
/* .line 688 */
/* .local v1, "e":Ljava/lang/IllegalStateException; */
final String v2 = "MiuiBatteryStatsService"; // const-string v2, "MiuiBatteryStatsService"
final String v3 = "Start one-Track service failed"; // const-string v3, "Start one-Track service failed"
android.util.Slog .e ( v2,v3,v1 );
/* .line 690 */
} // .end local v1 # "e":Ljava/lang/IllegalStateException;
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void calculateTemp ( com.android.server.MiuiBatteryStatsService$BatteryTempInfo p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "t" # Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo; */
/* .param p2, "temp" # I */
/* .line 622 */
v0 = (( com.android.server.MiuiBatteryStatsService$BatteryTempInfo ) p1 ).getMaxTemp ( ); // invoke-virtual {p1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->getMaxTemp()I
/* if-ge v0, p2, :cond_0 */
/* .line 623 */
(( com.android.server.MiuiBatteryStatsService$BatteryTempInfo ) p1 ).setMaxTemp ( p2 ); // invoke-virtual {p1, p2}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->setMaxTemp(I)V
/* .line 626 */
} // :cond_0
v0 = (( com.android.server.MiuiBatteryStatsService$BatteryTempInfo ) p1 ).getMinTemp ( ); // invoke-virtual {p1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->getMinTemp()I
/* if-le v0, p2, :cond_1 */
/* .line 627 */
(( com.android.server.MiuiBatteryStatsService$BatteryTempInfo ) p1 ).setMinTemp ( p2 ); // invoke-virtual {p1, p2}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->setMinTemp(I)V
/* .line 630 */
} // :cond_1
(( com.android.server.MiuiBatteryStatsService$BatteryTempInfo ) p1 ).setTotalTemp ( p2 ); // invoke-virtual {p1, p2}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->setTotalTemp(I)V
/* .line 631 */
(( com.android.server.MiuiBatteryStatsService$BatteryTempInfo ) p1 ).setDataCount ( ); // invoke-virtual {p1}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempInfo;->setDataCount()V
/* .line 632 */
return;
} // .end method
public void handleMessage ( android.os.Message p0 ) {
/* .locals 3 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 749 */
/* iget v0, p1, Landroid/os/Message;->what:I */
final String v1 = "lpd_update_en"; // const-string v1, "lpd_update_en"
/* packed-switch v0, :pswitch_data_0 */
/* .line 825 */
final String v0 = "MiuiBatteryStatsService"; // const-string v0, "MiuiBatteryStatsService"
final String v1 = "no message to handle"; // const-string v1, "no message to handle"
android.util.Slog .d ( v0,v1 );
/* goto/16 :goto_1 */
/* .line 822 */
/* :pswitch_0 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleCheckSoc()V */
/* .line 823 */
/* goto/16 :goto_1 */
/* .line 817 */
/* :pswitch_1 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleLpdCountReport()V */
/* .line 819 */
/* :pswitch_2 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->orderCheckSocTimer()V */
/* .line 820 */
/* goto/16 :goto_1 */
/* .line 812 */
/* :pswitch_3 */
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v0 );
final String v2 = "0"; // const-string v2, "0"
(( miui.util.IMiCharge ) v0 ).setMiChargePath ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lmiui/util/IMiCharge;->setMiChargePath(Ljava/lang/String;Ljava/lang/String;)Z
/* .line 813 */
v0 = this.obj;
/* check-cast v0, Ljava/lang/String; */
/* .line 814 */
/* .local v0, "lpdInfomation":Ljava/lang/String; */
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleLPDInfomation(Ljava/lang/String;)V */
/* .line 815 */
/* goto/16 :goto_1 */
/* .line 809 */
} // .end local v0 # "lpdInfomation":Ljava/lang/String;
/* :pswitch_4 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleWirelessComposite()V */
/* .line 810 */
/* goto/16 :goto_1 */
/* .line 806 */
/* :pswitch_5 */
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmBatteryTempLevel ( v0 );
com.android.server.MiuiBatteryStatsService$BatteryTempLevelInfo .-$$Nest$mcycleCheck ( v0 );
/* .line 807 */
/* goto/16 :goto_1 */
/* .line 802 */
/* :pswitch_6 */
v0 = this.obj;
/* check-cast v0, Landroid/content/Intent; */
/* .line 803 */
/* .local v0, "k":Landroid/content/Intent; */
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleSreiesDeltaVoltage(Landroid/content/Intent;)V */
/* .line 804 */
/* goto/16 :goto_1 */
/* .line 799 */
} // .end local v0 # "k":Landroid/content/Intent;
/* :pswitch_7 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleIntermittentCharge()V */
/* .line 800 */
/* goto/16 :goto_1 */
/* .line 796 */
/* :pswitch_8 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleSlowCharge()V */
/* .line 797 */
/* goto/16 :goto_1 */
/* .line 793 */
/* :pswitch_9 */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).limitTimeForSlowCharge ( ); // invoke-virtual {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->limitTimeForSlowCharge()V
/* .line 794 */
/* goto/16 :goto_1 */
/* .line 790 */
/* :pswitch_a */
/* iget v0, p1, Landroid/os/Message;->arg1:I */
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleVbusDisable(I)V */
/* .line 791 */
/* goto/16 :goto_1 */
/* .line 785 */
/* :pswitch_b */
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v0 );
final String v2 = "1"; // const-string v2, "1"
(( miui.util.IMiCharge ) v0 ).setMiChargePath ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lmiui/util/IMiCharge;->setMiChargePath(Ljava/lang/String;Ljava/lang/String;)Z
/* .line 786 */
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmBatteryTempSocTime ( v0 );
com.android.server.MiuiBatteryStatsService$BatteryTempSocTimeInfo .-$$Nest$mreadDataFromFile ( v0 );
/* .line 787 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendAdjustVolBroadcast()V */
/* .line 788 */
/* .line 782 */
/* :pswitch_c */
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmBatteryTempSocTime ( v0 );
com.android.server.MiuiBatteryStatsService$BatteryTempSocTimeInfo .-$$Nest$mwriteDataToFile ( v0 );
/* .line 783 */
/* .line 778 */
/* :pswitch_d */
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmBatteryInfoNormal ( v0 );
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendBatteryTempVoltageTimeData(Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;)V */
/* .line 779 */
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmBatteryInfoFull ( v0 );
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendBatteryTempVoltageTimeData(Lcom/android/server/MiuiBatteryStatsService$BatteryTempVoltageTimeInfo;)V */
/* .line 780 */
/* .line 775 */
/* :pswitch_e */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendBatteryTempData()V */
/* .line 776 */
/* .line 771 */
/* :pswitch_f */
v0 = this.obj;
/* check-cast v0, Landroid/content/Intent; */
/* .line 772 */
/* .local v0, "j":Landroid/content/Intent; */
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleBatteryTemp(Landroid/content/Intent;)V */
/* .line 773 */
/* .line 767 */
} // .end local v0 # "j":Landroid/content/Intent;
/* :pswitch_10 */
/* iget v0, p1, Landroid/os/Message;->arg1:I */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* move v0, v1 */
/* .line 768 */
/* .local v0, "value":Z */
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handlePowerOff(Z)V */
/* .line 769 */
/* .line 764 */
} // .end local v0 # "value":Z
/* :pswitch_11 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleChargeAction()V */
/* .line 765 */
/* .line 760 */
/* :pswitch_12 */
v0 = this.obj;
/* check-cast v0, Landroid/content/Intent; */
/* .line 761 */
/* .local v0, "i":Landroid/content/Intent; */
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleUsbFunction(Landroid/content/Intent;)V */
/* .line 762 */
/* .line 757 */
} // .end local v0 # "i":Landroid/content/Intent;
/* :pswitch_13 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleWirelessReverseCharge()V */
/* .line 758 */
/* .line 754 */
/* :pswitch_14 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleCharge()V */
/* .line 755 */
/* .line 751 */
/* :pswitch_15 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleBatteryHealth()V */
/* .line 752 */
/* nop */
/* .line 827 */
} // :goto_1
return;
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_15 */
/* :pswitch_14 */
/* :pswitch_13 */
/* :pswitch_12 */
/* :pswitch_11 */
/* :pswitch_10 */
/* :pswitch_f */
/* :pswitch_e */
/* :pswitch_d */
/* :pswitch_c */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_1 */
/* :pswitch_2 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void limitTimeForSlowCharge ( ) {
/* .locals 7 */
/* .line 570 */
final String v0 = "USB_FLOAT"; // const-string v0, "USB_FLOAT"
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->getBatteryChargeType()Ljava/lang/String; */
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 571 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->handleSlowCharge()V */
/* .line 573 */
} // :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->getChargingPowerMax()I */
/* const/16 v1, 0x60 */
int v2 = 2; // const/4 v2, 0x2
/* if-ge v0, v1, :cond_1 */
/* .line 574 */
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmAlarmManager ( v0 );
/* .line 575 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v3 */
/* const-wide/32 v5, 0x927c0 */
/* add-long/2addr v3, v5 */
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmPendingIntentLimitTime ( v1 );
/* .line 574 */
(( android.app.AlarmManager ) v0 ).setExactAndAllowWhileIdle ( v2, v3, v4, v1 ); // invoke-virtual {v0, v2, v3, v4, v1}, Landroid/app/AlarmManager;->setExactAndAllowWhileIdle(IJLandroid/app/PendingIntent;)V
/* .line 577 */
} // :cond_1
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmAlarmManager ( v0 );
/* .line 578 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v3 */
/* const-wide/32 v5, 0x493e0 */
/* add-long/2addr v3, v5 */
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmPendingIntentLimitTime ( v1 );
/* .line 577 */
(( android.app.AlarmManager ) v0 ).setExactAndAllowWhileIdle ( v2, v3, v4, v1 ); // invoke-virtual {v0, v2, v3, v4, v1}, Landroid/app/AlarmManager;->setExactAndAllowWhileIdle(IJLandroid/app/PendingIntent;)V
/* .line 581 */
} // :goto_0
return;
} // .end method
public Integer parseInt ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "argument" # Ljava/lang/String; */
/* .line 663 */
final String v0 = "MiuiBatteryStatsService"; // const-string v0, "MiuiBatteryStatsService"
int v1 = -1; // const/4 v1, -0x1
try { // :try_start_0
v2 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v2, :cond_0 */
/* .line 664 */
v0 = java.lang.Integer .parseInt ( p1 );
/* .line 666 */
} // :cond_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "argument = "; // const-string v3, "argument = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 667 */
/* .line 668 */
/* :catch_0 */
/* move-exception v2 */
/* .line 669 */
/* .local v2, "e":Ljava/lang/NumberFormatException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Invalid integer argument "; // const-string v4, "Invalid integer argument "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v3 );
/* .line 670 */
} // .end method
public void sendMessage ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "what" # I */
/* .param p2, "arg1" # I */
/* .line 635 */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).removeMessages ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->removeMessages(I)V
/* .line 636 */
android.os.Message .obtain ( p0,p1 );
/* .line 637 */
/* .local v0, "m":Landroid/os/Message; */
/* iput p2, v0, Landroid/os/Message;->arg1:I */
/* .line 638 */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).sendMessage ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 639 */
return;
} // .end method
public void sendMessageDelayed ( Integer p0, Long p1 ) {
/* .locals 1 */
/* .param p1, "what" # I */
/* .param p2, "delayMillis" # J */
/* .line 656 */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).removeMessages ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->removeMessages(I)V
/* .line 657 */
android.os.Message .obtain ( p0,p1 );
/* .line 658 */
/* .local v0, "m":Landroid/os/Message; */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).sendMessageDelayed ( v0, p2, p3 ); // invoke-virtual {p0, v0, p2, p3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 659 */
return;
} // .end method
public void sendMessageDelayed ( Integer p0, java.lang.Object p1, Long p2 ) {
/* .locals 1 */
/* .param p1, "what" # I */
/* .param p2, "arg" # Ljava/lang/Object; */
/* .param p3, "delayMillis" # J */
/* .line 642 */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).removeMessages ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->removeMessages(I)V
/* .line 643 */
android.os.Message .obtain ( p0,p1 );
/* .line 644 */
/* .local v0, "m":Landroid/os/Message; */
this.obj = p2;
/* .line 645 */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).sendMessageDelayed ( v0, p3, p4 ); // invoke-virtual {p0, v0, p3, p4}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 646 */
return;
} // .end method
public void sendMessageDelayed ( Integer p0, Boolean p1, Long p2 ) {
/* .locals 1 */
/* .param p1, "what" # I */
/* .param p2, "arg" # Z */
/* .param p3, "delayMillis" # J */
/* .line 649 */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).removeMessages ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->removeMessages(I)V
/* .line 650 */
android.os.Message .obtain ( p0,p1 );
/* .line 651 */
/* .local v0, "m":Landroid/os/Message; */
/* iput p2, v0, Landroid/os/Message;->arg1:I */
/* .line 652 */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).sendMessageDelayed ( v0, p3, p4 ); // invoke-virtual {p0, v0, p3, p4}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 653 */
return;
} // .end method
public void switchTimer ( Boolean p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "plugged" # Z */
/* .param p2, "soc" # I */
/* .line 553 */
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmAlarmManager ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmPendingIntentCycleCheck ( v0 );
/* if-nez v0, :cond_0 */
/* .line 556 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 557 */
/* const/16 v0, 0x5a */
/* if-ge p2, v0, :cond_2 */
/* .line 559 */
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmAlarmManager ( v0 );
/* .line 560 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v1 */
/* const-wide/16 v3, 0x7530 */
/* add-long/2addr v1, v3 */
v3 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmPendingIntentCycleCheck ( v3 );
/* .line 559 */
int v4 = 2; // const/4 v4, 0x2
(( android.app.AlarmManager ) v0 ).setExactAndAllowWhileIdle ( v4, v1, v2, v3 ); // invoke-virtual {v0, v4, v1, v2, v3}, Landroid/app/AlarmManager;->setExactAndAllowWhileIdle(IJLandroid/app/PendingIntent;)V
/* .line 564 */
} // :cond_1
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmAlarmManager ( v0 );
v1 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmPendingIntentCycleCheck ( v1 );
(( android.app.AlarmManager ) v0 ).cancel ( v1 ); // invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V
/* .line 565 */
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmBatteryTempLevel ( v0 );
com.android.server.MiuiBatteryStatsService$BatteryTempLevelInfo .-$$Nest$mdataReset ( v0 );
/* .line 567 */
} // :cond_2
} // :goto_0
return;
/* .line 554 */
} // :cond_3
} // :goto_1
return;
} // .end method
public void updateChargeInfo ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "plugged" # Z */
/* .line 544 */
int v0 = 1; // const/4 v0, 0x1
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 545 */
/* const-wide/32 v1, 0x493e0 */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).sendMessageDelayed ( v0, v1, v2 ); // invoke-virtual {p0, v0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V
/* .line 547 */
} // :cond_0
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).removeMessages ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->removeMessages(I)V
/* .line 548 */
int v0 = 4; // const/4 v0, 0x4
/* const-wide/16 v1, 0x0 */
(( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) p0 ).sendMessageDelayed ( v0, v1, v2 ); // invoke-virtual {p0, v0, v1, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->sendMessageDelayed(IJ)V
/* .line 550 */
} // :goto_0
return;
} // .end method
