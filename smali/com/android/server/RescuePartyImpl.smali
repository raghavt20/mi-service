.class public Lcom/android/server/RescuePartyImpl;
.super Lcom/android/server/RescuePartyStub;
.source "RescuePartyImpl.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/android/server/RescuePartyStub;-><init>()V

    return-void
.end method

.method static synthetic lambda$maybeDoResetConfig$0(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .line 81
    :try_start_0
    const-class v0, Landroid/os/PowerManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 82
    .local v0, "pm":Landroid/os/PowerManager;
    if-eqz v0, :cond_0

    .line 83
    const-string v1, "RescueParty"

    invoke-virtual {v0, v1}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87
    .end local v0    # "pm":Landroid/os/PowerManager;
    :cond_0
    goto :goto_0

    .line 85
    :catchall_0
    move-exception v0

    .line 86
    .local v0, "t":Ljava/lang/Throwable;
    const-string v1, "RescuePartyPlus"

    const-string v2, "do config reset failed!"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 88
    .end local v0    # "t":Ljava/lang/Throwable;
    :goto_0
    return-void
.end method


# virtual methods
.method public isLauncher(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packageName"    # Ljava/lang/String;

    .line 24
    invoke-static {}, Lcom/android/server/RescuePartyPlusHelper;->checkDisableRescuePartyPlus()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 25
    :cond_0
    if-eqz p2, :cond_1

    invoke-static {p1}, Lcom/android/server/RescuePartyPlusHelper;->getLauncherPackageName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 26
    const/4 v0, 0x1

    return v0

    .line 28
    :cond_1
    return v1
.end method

.method public maybeDoResetConfig(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "failedPackage"    # Ljava/lang/String;

    .line 33
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 34
    const-string/jumbo v0, "sys.rescue_party_failed_package"

    invoke-static {v0, p2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    :cond_0
    invoke-static {}, Lcom/android/server/RescuePartyPlusHelper;->checkDisableRescuePartyPlus()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    return v1

    .line 40
    :cond_1
    invoke-static {}, Lcom/android/server/RescuePartyPlusHelper;->getMitigationTempCount()I

    move-result v0

    .line 43
    .local v0, "mitigationCount":I
    invoke-static {}, Lcom/android/server/RescuePartyPlusHelper;->getConfigResetProcessStatus()Z

    move-result v2

    const/4 v3, 0x1

    const-string v4, "RescuePartyPlus"

    if-eqz v2, :cond_2

    .line 44
    const-string v1, "Config Reset in progress!"

    invoke-static {v4, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    const-string/jumbo v1, "sys.powerctl"

    const-string v2, "reboot,RescueParty"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    return v3

    .line 50
    :cond_2
    const/4 v2, 0x5

    if-eq v0, v2, :cond_3

    .line 51
    return v1

    .line 55
    :cond_3
    invoke-static {v3}, Lcom/android/server/RescuePartyPlusHelper;->setLastResetConfigStatus(Z)V

    .line 56
    invoke-static {v3}, Lcom/android/server/RescuePartyPlusHelper;->setConfigResetProcessStatus(Z)V

    .line 57
    const-string v1, "Start Config Reset!"

    invoke-static {v4, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Finished rescue level CONFIG_RESET for package "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {v2, v1}, Lcom/android/server/pm/PackageManagerServiceUtils;->logCriticalInfo(ILjava/lang/String;)V

    .line 61
    invoke-static {}, Lcom/android/server/RescuePartyPlusHelper;->tryGetCloudControlOrDefaultData()Ljava/util/Set;

    move-result-object v1

    .line 64
    .local v1, "deleteFileSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 65
    .local v6, "filename":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Preparing to delete files: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/android/server/pm/PackageManagerServiceUtils;->logCriticalInfo(ILjava/lang/String;)V

    .line 67
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 68
    .local v7, "file":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 69
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    .line 71
    .end local v6    # "filename":Ljava/lang/String;
    .end local v7    # "file":Ljava/io/File;
    :cond_4
    goto :goto_0

    .line 74
    :cond_5
    invoke-static {p2}, Lcom/android/server/RescuePartyPlusHelper;->resetTheme(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 75
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Reset theme failed: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    :cond_6
    new-instance v2, Lcom/android/server/RescuePartyImpl$$ExternalSyntheticLambda0;

    invoke-direct {v2, p1}, Lcom/android/server/RescuePartyImpl$$ExternalSyntheticLambda0;-><init>(Landroid/content/Context;)V

    .line 89
    .local v2, "runnable":Ljava/lang/Runnable;
    new-instance v4, Ljava/lang/Thread;

    invoke-direct {v4, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 90
    .local v4, "thread":Ljava/lang/Thread;
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    .line 91
    return v3
.end method
