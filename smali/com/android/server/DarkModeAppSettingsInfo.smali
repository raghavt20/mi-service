.class public Lcom/android/server/DarkModeAppSettingsInfo;
.super Ljava/lang/Object;
.source "DarkModeAppSettingsInfo.java"


# static fields
.field public static final OVERRIDE_ENABLE_CLOSE:I = 0x2

.field public static final OVERRIDE_ENABLE_NO:I = 0x0

.field public static final OVERRIDE_ENABLE_OPEN:I = 0x1


# instance fields
.field private adaptStat:I

.field private defaultEnable:Z

.field private forceDarkOrigin:Z

.field private forceDarkSplashScreen:I

.field private interceptRelaunch:I

.field private overrideEnableValue:I

.field private packageName:Ljava/lang/String;

.field private showInSettings:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAdaptStat()I
    .locals 1

    .line 56
    iget v0, p0, Lcom/android/server/DarkModeAppSettingsInfo;->adaptStat:I

    return v0
.end method

.method public getForceDarkSplashScreen()I
    .locals 1

    .line 80
    iget v0, p0, Lcom/android/server/DarkModeAppSettingsInfo;->forceDarkSplashScreen:I

    return v0
.end method

.method public getInterceptRelaunch()I
    .locals 1

    .line 88
    iget v0, p0, Lcom/android/server/DarkModeAppSettingsInfo;->interceptRelaunch:I

    return v0
.end method

.method public getOverrideEnableValue()I
    .locals 1

    .line 64
    iget v0, p0, Lcom/android/server/DarkModeAppSettingsInfo;->overrideEnableValue:I

    return v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/android/server/DarkModeAppSettingsInfo;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method public isDefaultEnable()Z
    .locals 1

    .line 48
    iget-boolean v0, p0, Lcom/android/server/DarkModeAppSettingsInfo;->defaultEnable:Z

    return v0
.end method

.method public isForceDarkOrigin()Z
    .locals 1

    .line 72
    iget-boolean v0, p0, Lcom/android/server/DarkModeAppSettingsInfo;->forceDarkOrigin:Z

    return v0
.end method

.method public isShowInSettings()Z
    .locals 1

    .line 40
    iget-boolean v0, p0, Lcom/android/server/DarkModeAppSettingsInfo;->showInSettings:Z

    return v0
.end method

.method public setAdaptStat(I)V
    .locals 0
    .param p1, "adaptStat"    # I

    .line 60
    iput p1, p0, Lcom/android/server/DarkModeAppSettingsInfo;->adaptStat:I

    .line 61
    return-void
.end method

.method public setDefaultEnable(Z)V
    .locals 0
    .param p1, "defaultEnable"    # Z

    .line 52
    iput-boolean p1, p0, Lcom/android/server/DarkModeAppSettingsInfo;->defaultEnable:Z

    .line 53
    return-void
.end method

.method public setForceDarkOrigin(Z)V
    .locals 0
    .param p1, "forceDarkOrigin"    # Z

    .line 76
    iput-boolean p1, p0, Lcom/android/server/DarkModeAppSettingsInfo;->forceDarkOrigin:Z

    .line 77
    return-void
.end method

.method public setForceDarkSplashScreen(I)V
    .locals 0
    .param p1, "forceDarkSplashScreen"    # I

    .line 84
    iput p1, p0, Lcom/android/server/DarkModeAppSettingsInfo;->forceDarkSplashScreen:I

    .line 85
    return-void
.end method

.method public setInterceptRelaunch(I)V
    .locals 0
    .param p1, "interceptRelaunch"    # I

    .line 92
    iput p1, p0, Lcom/android/server/DarkModeAppSettingsInfo;->interceptRelaunch:I

    .line 93
    return-void
.end method

.method public setOverrideEnableValue(I)V
    .locals 0
    .param p1, "overrideEnableValue"    # I

    .line 68
    iput p1, p0, Lcom/android/server/DarkModeAppSettingsInfo;->overrideEnableValue:I

    .line 69
    return-void
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .line 36
    iput-object p1, p0, Lcom/android/server/DarkModeAppSettingsInfo;->packageName:Ljava/lang/String;

    .line 37
    return-void
.end method

.method public setShowInSettings(Z)V
    .locals 0
    .param p1, "showInSettings"    # Z

    .line 44
    iput-boolean p1, p0, Lcom/android/server/DarkModeAppSettingsInfo;->showInSettings:Z

    .line 45
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DarkModeAppSettingsInfo{packageName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/DarkModeAppSettingsInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", showInSettings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/DarkModeAppSettingsInfo;->showInSettings:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", defaultEnable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/DarkModeAppSettingsInfo;->defaultEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", overrideEnableValue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/DarkModeAppSettingsInfo;->overrideEnableValue:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", forceDarkOrigin="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/DarkModeAppSettingsInfo;->forceDarkOrigin:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", adaptStat="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/DarkModeAppSettingsInfo;->adaptStat:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", forceDarkSplashScreen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/DarkModeAppSettingsInfo;->forceDarkSplashScreen:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", interceptRelaunch="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/DarkModeAppSettingsInfo;->interceptRelaunch:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
