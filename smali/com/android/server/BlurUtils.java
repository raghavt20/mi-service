public class com.android.server.BlurUtils {
	 /* .source "BlurUtils.java" */
	 /* # direct methods */
	 public com.android.server.BlurUtils ( ) {
		 /* .locals 0 */
		 /* .line 15 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static android.graphics.Bitmap addBlackBoard ( android.graphics.Bitmap p0, Integer p1 ) {
		 /* .locals 5 */
		 /* .param p0, "bmp" # Landroid/graphics/Bitmap; */
		 /* .param p1, "color" # I */
		 /* .line 17 */
		 /* new-instance v0, Landroid/graphics/Canvas; */
		 /* invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V */
		 /* .line 18 */
		 /* .local v0, "canvas":Landroid/graphics/Canvas; */
		 /* new-instance v1, Landroid/graphics/Paint; */
		 /* invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V */
		 /* .line 19 */
		 /* .local v1, "paint":Landroid/graphics/Paint; */
		 v2 = 		 (( android.graphics.Bitmap ) p0 ).getWidth ( ); // invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I
		 v3 = 		 (( android.graphics.Bitmap ) p0 ).getHeight ( ); // invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I
		 v4 = android.graphics.Bitmap$Config.ARGB_8888;
		 android.graphics.Bitmap .createBitmap ( v2,v3,v4 );
		 /* .line 20 */
		 /* .local v2, "newBitmap":Landroid/graphics/Bitmap; */
		 (( android.graphics.Canvas ) v0 ).setBitmap ( v2 ); // invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V
		 /* .line 21 */
		 int v3 = 0; // const/4 v3, 0x0
		 (( android.graphics.Canvas ) v0 ).drawBitmap ( p0, v3, v3, v1 ); // invoke-virtual {v0, p0, v3, v3, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
		 /* .line 22 */
		 (( android.graphics.Canvas ) v0 ).drawColor ( p1 ); // invoke-virtual {v0, p1}, Landroid/graphics/Canvas;->drawColor(I)V
		 /* .line 23 */
	 } // .end method
	 public static android.graphics.Bitmap blurImage ( android.content.Context p0, android.graphics.Bitmap p1, Float p2 ) {
		 /* .locals 9 */
		 /* .param p0, "context" # Landroid/content/Context; */
		 /* .param p1, "input" # Landroid/graphics/Bitmap; */
		 /* .param p2, "radius" # F */
		 /* .line 27 */
		 v0 = 		 (( android.graphics.Bitmap ) p1 ).getWidth ( ); // invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I
		 /* div-int/lit8 v0, v0, 0x4 */
		 v1 = 		 (( android.graphics.Bitmap ) p1 ).getHeight ( ); // invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I
		 /* div-int/lit8 v1, v1, 0x4 */
		 int v2 = 0; // const/4 v2, 0x0
		 android.graphics.Bitmap .createScaledBitmap ( p1,v0,v1,v2 );
		 /* .line 28 */
		 /* .local v0, "tempInput":Landroid/graphics/Bitmap; */
		 (( android.graphics.Bitmap ) v0 ).getConfig ( ); // invoke-virtual {v0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;
		 int v3 = 1; // const/4 v3, 0x1
		 (( android.graphics.Bitmap ) v0 ).copy ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;
		 /* .line 29 */
		 /* .local v1, "result":Landroid/graphics/Bitmap; */
		 android.renderscript.RenderScript .create ( p0 );
		 /* .line 30 */
		 /* .local v4, "rsScript":Landroid/renderscript/RenderScript; */
		 /* if-nez v4, :cond_0 */
		 /* .line 31 */
		 int v2 = 0; // const/4 v2, 0x0
		 /* .line 34 */
	 } // :cond_0
	 v5 = android.renderscript.Allocation$MipmapControl.MIPMAP_NONE;
	 android.renderscript.Allocation .createFromBitmap ( v4,v0,v5,v3 );
	 /* .line 35 */
	 /* .local v3, "alloc":Landroid/renderscript/Allocation; */
	 (( android.renderscript.Allocation ) v3 ).getType ( ); // invoke-virtual {v3}, Landroid/renderscript/Allocation;->getType()Landroid/renderscript/Type;
	 android.renderscript.Allocation .createTyped ( v4,v5 );
	 /* .line 36 */
	 /* .local v5, "outAlloc":Landroid/renderscript/Allocation; */
	 android.renderscript.Element .U8_4 ( v4 );
	 android.renderscript.ScriptIntrinsicBlur .create ( v4,v6 );
	 /* .line 37 */
	 /* .local v6, "blur":Landroid/renderscript/ScriptIntrinsicBlur; */
	 (( android.renderscript.ScriptIntrinsicBlur ) v6 ).setRadius ( p2 ); // invoke-virtual {v6, p2}, Landroid/renderscript/ScriptIntrinsicBlur;->setRadius(F)V
	 /* .line 38 */
	 (( android.renderscript.ScriptIntrinsicBlur ) v6 ).setInput ( v3 ); // invoke-virtual {v6, v3}, Landroid/renderscript/ScriptIntrinsicBlur;->setInput(Landroid/renderscript/Allocation;)V
	 /* .line 39 */
	 (( android.renderscript.ScriptIntrinsicBlur ) v6 ).forEach ( v5 ); // invoke-virtual {v6, v5}, Landroid/renderscript/ScriptIntrinsicBlur;->forEach(Landroid/renderscript/Allocation;)V
	 /* .line 40 */
	 (( android.renderscript.Allocation ) v5 ).copyTo ( v1 ); // invoke-virtual {v5, v1}, Landroid/renderscript/Allocation;->copyTo(Landroid/graphics/Bitmap;)V
	 /* .line 41 */
	 (( android.renderscript.RenderScript ) v4 ).destroy ( ); // invoke-virtual {v4}, Landroid/renderscript/RenderScript;->destroy()V
	 /* .line 42 */
	 v7 = 	 (( android.graphics.Bitmap ) p1 ).getWidth ( ); // invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I
	 v8 = 	 (( android.graphics.Bitmap ) p1 ).getHeight ( ); // invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I
	 android.graphics.Bitmap .createScaledBitmap ( v1,v7,v8,v2 );
} // .end method
public static android.graphics.Bitmap stackBlur ( android.graphics.Bitmap p0, Integer p1 ) {
	 /* .locals 44 */
	 /* .param p0, "sentBitmap" # Landroid/graphics/Bitmap; */
	 /* .param p1, "radius" # I */
	 /* .line 46 */
	 /* move/from16 v0, p1 */
	 /* invoke-virtual/range {p0 ..p0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config; */
	 int v2 = 1; // const/4 v2, 0x1
	 /* move-object/from16 v3, p0 */
	 (( android.graphics.Bitmap ) v3 ).copy ( v1, v2 ); // invoke-virtual {v3, v1, v2}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;
	 /* .line 48 */
	 /* .local v1, "bitmap":Landroid/graphics/Bitmap; */
	 /* if-ge v0, v2, :cond_0 */
	 /* .line 49 */
	 int v2 = 0; // const/4 v2, 0x0
	 /* .line 52 */
} // :cond_0
v12 = (( android.graphics.Bitmap ) v1 ).getWidth ( ); // invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I
/* .line 53 */
/* .local v12, "w":I */
v13 = (( android.graphics.Bitmap ) v1 ).getHeight ( ); // invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I
/* .line 55 */
/* .local v13, "h":I */
/* mul-int v4, v12, v13 */
/* new-array v14, v4, [I */
/* .line 56 */
/* .local v14, "pix":[I */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v12 ); // invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v15 = " "; // const-string v15, " "
(( java.lang.StringBuilder ) v4 ).append ( v15 ); // invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v13 ); // invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v15 ); // invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v5, v14 */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v11 = "pix"; // const-string v11, "pix"
android.util.Log .e ( v11,v4 );
/* .line 57 */
int v6 = 0; // const/4 v6, 0x0
int v8 = 0; // const/4 v8, 0x0
int v9 = 0; // const/4 v9, 0x0
/* move-object v4, v1 */
/* move-object v5, v14 */
/* move v7, v12 */
/* move v10, v12 */
/* move-object/from16 v16, v11 */
/* move v11, v13 */
/* invoke-virtual/range {v4 ..v11}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V */
/* .line 59 */
/* add-int/lit8 v11, v12, -0x1 */
/* .line 60 */
/* .local v11, "wm":I */
/* add-int/lit8 v10, v13, -0x1 */
/* .line 61 */
/* .local v10, "hm":I */
/* mul-int v9, v12, v13 */
/* .line 62 */
/* .local v9, "wh":I */
/* add-int v4, v0, v0 */
/* add-int/lit8 v8, v4, 0x1 */
/* .line 64 */
/* .local v8, "div":I */
/* new-array v7, v9, [I */
/* .line 65 */
/* .local v7, "r":[I */
/* new-array v6, v9, [I */
/* .line 66 */
/* .local v6, "g":[I */
/* new-array v5, v9, [I */
/* .line 68 */
/* .local v5, "b":[I */
v4 = java.lang.Math .max ( v12,v13 );
/* new-array v4, v4, [I */
/* .line 70 */
/* .local v4, "vmin":[I */
/* add-int/lit8 v17, v8, 0x1 */
/* shr-int/lit8 v17, v17, 0x1 */
/* .line 71 */
/* .local v17, "divsum":I */
/* move/from16 v18, v9 */
} // .end local v9 # "wh":I
/* .local v18, "wh":I */
/* mul-int v9, v17, v17 */
/* .line 72 */
} // .end local v17 # "divsum":I
/* .local v9, "divsum":I */
/* mul-int/lit16 v2, v9, 0x100 */
/* new-array v2, v2, [I */
/* .line 73 */
/* .local v2, "dv":[I */
/* const/16 v19, 0x0 */
/* move/from16 v3, v19 */
/* .local v3, "i":I */
} // :goto_0
/* move-object/from16 v19, v1 */
} // .end local v1 # "bitmap":Landroid/graphics/Bitmap;
/* .local v19, "bitmap":Landroid/graphics/Bitmap; */
/* mul-int/lit16 v1, v9, 0x100 */
/* if-ge v3, v1, :cond_1 */
/* .line 74 */
/* div-int v1, v3, v9 */
/* aput v1, v2, v3 */
/* .line 73 */
/* add-int/lit8 v3, v3, 0x1 */
/* move-object/from16 v1, v19 */
/* .line 77 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
/* move/from16 v20, v1 */
/* .local v20, "yi":I */
/* move/from16 v21, v1 */
/* .line 79 */
/* .local v21, "yw":I */
int v1 = 3; // const/4 v1, 0x3
/* filled-new-array {v8, v1}, [I */
/* move/from16 v23, v3 */
} // .end local v3 # "i":I
/* .local v23, "i":I */
v3 = java.lang.Integer.TYPE;
java.lang.reflect.Array .newInstance ( v3,v1 );
/* check-cast v1, [[I */
/* .line 84 */
/* .local v1, "stack":[[I */
/* add-int/lit8 v3, v0, 0x1 */
/* .line 88 */
/* .local v3, "r1":I */
/* const/16 v24, 0x0 */
/* move/from16 v43, v24 */
/* move/from16 v24, v9 */
/* move/from16 v9, v43 */
/* .local v9, "y":I */
/* .local v24, "divsum":I */
} // :goto_1
/* const/16 v25, 0x2 */
/* if-ge v9, v13, :cond_6 */
/* .line 89 */
/* const/16 v26, 0x0 */
/* move/from16 v22, v26 */
/* .local v22, "bsum":I */
/* move/from16 v27, v26 */
/* .local v27, "gsum":I */
/* move/from16 v28, v26 */
/* .local v28, "rsum":I */
/* move/from16 v29, v26 */
/* .local v29, "boutsum":I */
/* move/from16 v30, v26 */
/* .local v30, "goutsum":I */
/* move/from16 v31, v26 */
/* .local v31, "routsum":I */
/* move/from16 v32, v26 */
/* .local v32, "binsum":I */
/* move/from16 v33, v26 */
/* .local v33, "ginsum":I */
/* move/from16 v34, v26 */
/* .line 90 */
/* .local v34, "rinsum":I */
/* move-object/from16 v35, v15 */
/* neg-int v15, v0 */
/* move/from16 v23, v27 */
/* move/from16 v27, v13 */
/* move v13, v15 */
/* move/from16 v15, v22 */
} // .end local v22 # "bsum":I
/* .local v13, "i":I */
/* .local v15, "bsum":I */
/* .local v23, "gsum":I */
/* .local v27, "h":I */
} // :goto_2
/* const v36, 0xff00 */
/* const/high16 v37, 0xff0000 */
/* if-gt v13, v0, :cond_3 */
/* .line 91 */
/* move/from16 v38, v10 */
/* move/from16 v10, v26 */
/* move-object/from16 v26, v4 */
} // .end local v4 # "vmin":[I
} // .end local v10 # "hm":I
/* .local v26, "vmin":[I */
/* .local v38, "hm":I */
v4 = java.lang.Math .max ( v13,v10 );
v4 = java.lang.Math .min ( v11,v4 );
/* add-int v4, v20, v4 */
/* aget v4, v14, v4 */
/* .line 92 */
/* .local v4, "p":I */
/* add-int v22, v13, v0 */
/* aget-object v39, v1, v22 */
/* .line 93 */
/* .local v39, "sir":[I */
/* and-int v22, v4, v37 */
/* shr-int/lit8 v22, v22, 0x10 */
/* aput v22, v39, v10 */
/* .line 94 */
/* and-int v10, v4, v36 */
/* shr-int/lit8 v10, v10, 0x8 */
/* const/16 v17, 0x1 */
/* aput v10, v39, v17 */
/* .line 95 */
/* and-int/lit16 v10, v4, 0xff */
/* aput v10, v39, v25 */
/* .line 96 */
v10 = java.lang.Math .abs ( v13 );
/* sub-int v10, v3, v10 */
/* .line 97 */
/* .local v10, "rbs":I */
/* const/16 v22, 0x0 */
/* aget v36, v39, v22 */
/* mul-int v36, v36, v10 */
/* add-int v28, v28, v36 */
/* .line 98 */
/* const/16 v17, 0x1 */
/* aget v36, v39, v17 */
/* mul-int v36, v36, v10 */
/* add-int v23, v23, v36 */
/* .line 99 */
/* aget v36, v39, v25 */
/* mul-int v36, v36, v10 */
/* add-int v15, v15, v36 */
/* .line 100 */
/* if-lez v13, :cond_2 */
/* .line 101 */
/* const/16 v22, 0x0 */
/* aget v36, v39, v22 */
/* add-int v34, v34, v36 */
/* .line 102 */
/* const/16 v17, 0x1 */
/* aget v36, v39, v17 */
/* add-int v33, v33, v36 */
/* .line 103 */
/* aget v36, v39, v25 */
/* add-int v32, v32, v36 */
/* .line 105 */
} // :cond_2
/* const/16 v17, 0x1 */
/* const/16 v22, 0x0 */
/* aget v36, v39, v22 */
/* add-int v31, v31, v36 */
/* .line 106 */
/* aget v36, v39, v17 */
/* add-int v30, v30, v36 */
/* .line 107 */
/* aget v36, v39, v25 */
/* add-int v29, v29, v36 */
/* .line 90 */
} // :goto_3
/* add-int/lit8 v13, v13, 0x1 */
/* move-object/from16 v4, v26 */
/* move/from16 v10, v38 */
/* const/16 v26, 0x0 */
/* .line 110 */
} // .end local v26 # "vmin":[I
} // .end local v38 # "hm":I
} // .end local v39 # "sir":[I
/* .local v4, "vmin":[I */
/* .local v10, "hm":I */
} // :cond_3
/* move-object/from16 v26, v4 */
/* move/from16 v38, v10 */
} // .end local v4 # "vmin":[I
} // .end local v10 # "hm":I
/* .restart local v26 # "vmin":[I */
/* .restart local v38 # "hm":I */
/* move/from16 v4, p1 */
/* .line 112 */
/* .local v4, "stackpointer":I */
int v10 = 0; // const/4 v10, 0x0
/* .local v10, "x":I */
} // :goto_4
/* if-ge v10, v12, :cond_5 */
/* .line 113 */
/* aget v39, v2, v28 */
/* aput v39, v7, v20 */
/* .line 114 */
/* aget v39, v2, v23 */
/* aput v39, v6, v20 */
/* .line 115 */
/* aget v39, v2, v15 */
/* aput v39, v5, v20 */
/* .line 117 */
/* sub-int v28, v28, v31 */
/* .line 118 */
/* sub-int v23, v23, v30 */
/* .line 119 */
/* sub-int v15, v15, v29 */
/* .line 121 */
/* sub-int v39, v4, v0 */
/* add-int v39, v39, v8 */
/* .line 122 */
/* .local v39, "stackstart":I */
/* rem-int v40, v39, v8 */
/* aget-object v40, v1, v40 */
/* .line 124 */
/* .local v40, "sir":[I */
/* const/16 v22, 0x0 */
/* aget v41, v40, v22 */
/* sub-int v31, v31, v41 */
/* .line 125 */
/* const/16 v17, 0x1 */
/* aget v41, v40, v17 */
/* sub-int v30, v30, v41 */
/* .line 126 */
/* aget v41, v40, v25 */
/* sub-int v29, v29, v41 */
/* .line 128 */
/* if-nez v9, :cond_4 */
/* .line 129 */
/* add-int v41, v10, v0 */
/* move/from16 v42, v13 */
} // .end local v13 # "i":I
/* .local v42, "i":I */
/* add-int/lit8 v13, v41, 0x1 */
v13 = java.lang.Math .min ( v13,v11 );
/* aput v13, v26, v10 */
/* .line 128 */
} // .end local v42 # "i":I
/* .restart local v13 # "i":I */
} // :cond_4
/* move/from16 v42, v13 */
/* .line 131 */
} // .end local v13 # "i":I
/* .restart local v42 # "i":I */
} // :goto_5
/* aget v13, v26, v10 */
/* add-int v13, v21, v13 */
/* aget v13, v14, v13 */
/* .line 133 */
/* .local v13, "p":I */
/* and-int v41, v13, v37 */
/* shr-int/lit8 v41, v41, 0x10 */
/* const/16 v22, 0x0 */
/* aput v41, v40, v22 */
/* .line 134 */
/* and-int v41, v13, v36 */
/* shr-int/lit8 v41, v41, 0x8 */
/* const/16 v17, 0x1 */
/* aput v41, v40, v17 */
/* .line 135 */
/* move/from16 v41, v11 */
} // .end local v11 # "wm":I
/* .local v41, "wm":I */
/* and-int/lit16 v11, v13, 0xff */
/* aput v11, v40, v25 */
/* .line 137 */
/* aget v11, v40, v22 */
/* add-int v34, v34, v11 */
/* .line 138 */
/* aget v11, v40, v17 */
/* add-int v33, v33, v11 */
/* .line 139 */
/* aget v11, v40, v25 */
/* add-int v32, v32, v11 */
/* .line 141 */
/* add-int v28, v28, v34 */
/* .line 142 */
/* add-int v23, v23, v33 */
/* .line 143 */
/* add-int v15, v15, v32 */
/* .line 145 */
/* add-int/lit8 v11, v4, 0x1 */
/* rem-int v4, v11, v8 */
/* .line 146 */
/* rem-int v11, v4, v8 */
/* aget-object v11, v1, v11 */
/* .line 148 */
} // .end local v40 # "sir":[I
/* .local v11, "sir":[I */
/* const/16 v22, 0x0 */
/* aget v40, v11, v22 */
/* add-int v31, v31, v40 */
/* .line 149 */
/* const/16 v17, 0x1 */
/* aget v40, v11, v17 */
/* add-int v30, v30, v40 */
/* .line 150 */
/* aget v40, v11, v25 */
/* add-int v29, v29, v40 */
/* .line 152 */
/* aget v40, v11, v22 */
/* sub-int v34, v34, v40 */
/* .line 153 */
/* aget v40, v11, v17 */
/* sub-int v33, v33, v40 */
/* .line 154 */
/* aget v40, v11, v25 */
/* sub-int v32, v32, v40 */
/* .line 156 */
/* add-int/lit8 v20, v20, 0x1 */
/* .line 112 */
/* add-int/lit8 v10, v10, 0x1 */
/* move/from16 v11, v41 */
/* move/from16 v13, v42 */
/* goto/16 :goto_4 */
/* .line 158 */
} // .end local v39 # "stackstart":I
} // .end local v41 # "wm":I
} // .end local v42 # "i":I
/* .local v11, "wm":I */
/* .local v13, "i":I */
} // :cond_5
/* move/from16 v41, v11 */
/* move/from16 v42, v13 */
} // .end local v11 # "wm":I
} // .end local v13 # "i":I
/* .restart local v41 # "wm":I */
/* .restart local v42 # "i":I */
/* add-int v21, v21, v12 */
/* .line 88 */
/* add-int/lit8 v9, v9, 0x1 */
/* move-object/from16 v4, v26 */
/* move/from16 v13, v27 */
/* move-object/from16 v15, v35 */
/* move/from16 v10, v38 */
/* move/from16 v23, v42 */
/* goto/16 :goto_1 */
/* .line 161 */
} // .end local v15 # "bsum":I
} // .end local v26 # "vmin":[I
} // .end local v27 # "h":I
} // .end local v28 # "rsum":I
} // .end local v29 # "boutsum":I
} // .end local v30 # "goutsum":I
} // .end local v31 # "routsum":I
} // .end local v32 # "binsum":I
} // .end local v33 # "ginsum":I
} // .end local v34 # "rinsum":I
} // .end local v38 # "hm":I
} // .end local v41 # "wm":I
} // .end local v42 # "i":I
/* .local v4, "vmin":[I */
/* .local v10, "hm":I */
/* .restart local v11 # "wm":I */
/* .local v13, "h":I */
/* .local v23, "i":I */
} // :cond_6
/* move-object/from16 v26, v4 */
/* move/from16 v38, v10 */
/* move/from16 v41, v11 */
/* move/from16 v27, v13 */
/* move-object/from16 v35, v15 */
} // .end local v4 # "vmin":[I
} // .end local v10 # "hm":I
} // .end local v11 # "wm":I
} // .end local v13 # "h":I
/* .restart local v26 # "vmin":[I */
/* .restart local v27 # "h":I */
/* .restart local v38 # "hm":I */
/* .restart local v41 # "wm":I */
int v4 = 0; // const/4 v4, 0x0
/* move v13, v4 */
/* move v15, v9 */
} // .end local v9 # "y":I
/* .local v13, "x":I */
/* .local v15, "y":I */
} // :goto_6
/* if-ge v13, v12, :cond_c */
/* .line 162 */
int v4 = 0; // const/4 v4, 0x0
/* move v9, v4 */
/* .local v9, "bsum":I */
/* move v10, v4 */
/* .local v10, "gsum":I */
/* move v11, v4 */
/* .local v11, "rsum":I */
/* move/from16 v22, v4 */
/* .local v22, "boutsum":I */
/* move/from16 v28, v4 */
/* .local v28, "goutsum":I */
/* move/from16 v29, v4 */
/* .local v29, "routsum":I */
/* move/from16 v30, v4 */
/* .local v30, "binsum":I */
/* move/from16 v31, v4 */
/* .local v31, "ginsum":I */
/* move/from16 v32, v4 */
/* .line 163 */
/* .local v32, "rinsum":I */
/* neg-int v4, v0 */
/* mul-int/2addr v4, v12 */
/* .line 164 */
/* .local v4, "yp":I */
/* move/from16 v34, v4 */
} // .end local v4 # "yp":I
/* .local v34, "yp":I */
/* neg-int v4, v0 */
/* move/from16 v23, v22 */
/* move/from16 v43, v15 */
/* move v15, v4 */
/* move/from16 v4, v34 */
/* move/from16 v34, v32 */
/* move/from16 v32, v31 */
/* move/from16 v31, v30 */
/* move/from16 v30, v29 */
/* move/from16 v29, v28 */
/* move/from16 v28, v43 */
} // .end local v22 # "boutsum":I
/* .restart local v4 # "yp":I */
/* .local v15, "i":I */
/* .local v23, "boutsum":I */
/* .local v28, "y":I */
/* .local v29, "goutsum":I */
/* .local v30, "routsum":I */
/* .local v31, "binsum":I */
/* .local v32, "ginsum":I */
/* .local v34, "rinsum":I */
} // :goto_7
/* if-gt v15, v0, :cond_9 */
/* .line 165 */
/* move/from16 v36, v8 */
int v8 = 0; // const/4 v8, 0x0
} // .end local v8 # "div":I
/* .local v36, "div":I */
v22 = java.lang.Math .max ( v8,v4 );
/* add-int v20, v22, v13 */
/* .line 167 */
/* add-int v22, v15, v0 */
/* aget-object v33, v1, v22 */
/* .line 169 */
/* .local v33, "sir":[I */
/* aget v22, v7, v20 */
/* aput v22, v33, v8 */
/* .line 170 */
/* aget v8, v6, v20 */
/* const/16 v17, 0x1 */
/* aput v8, v33, v17 */
/* .line 171 */
/* aget v8, v5, v20 */
/* aput v8, v33, v25 */
/* .line 173 */
v8 = java.lang.Math .abs ( v15 );
/* sub-int v8, v3, v8 */
/* .line 175 */
/* .local v8, "rbs":I */
/* aget v37, v7, v20 */
/* mul-int v37, v37, v8 */
/* add-int v11, v11, v37 */
/* .line 176 */
/* aget v37, v6, v20 */
/* mul-int v37, v37, v8 */
/* add-int v10, v10, v37 */
/* .line 177 */
/* aget v37, v5, v20 */
/* mul-int v37, v37, v8 */
/* add-int v9, v9, v37 */
/* .line 179 */
/* if-lez v15, :cond_7 */
/* .line 180 */
/* const/16 v22, 0x0 */
/* aget v37, v33, v22 */
/* add-int v34, v34, v37 */
/* .line 181 */
/* const/16 v17, 0x1 */
/* aget v37, v33, v17 */
/* add-int v32, v32, v37 */
/* .line 182 */
/* aget v37, v33, v25 */
/* add-int v31, v31, v37 */
/* .line 184 */
} // :cond_7
/* const/16 v17, 0x1 */
/* const/16 v22, 0x0 */
/* aget v37, v33, v22 */
/* add-int v30, v30, v37 */
/* .line 185 */
/* aget v37, v33, v17 */
/* add-int v29, v29, v37 */
/* .line 186 */
/* aget v37, v33, v25 */
/* add-int v23, v23, v37 */
/* .line 189 */
} // :goto_8
/* move/from16 v37, v9 */
/* move/from16 v9, v38 */
} // .end local v38 # "hm":I
/* .local v9, "hm":I */
/* .local v37, "bsum":I */
/* if-ge v15, v9, :cond_8 */
/* .line 190 */
/* add-int/2addr v4, v12 */
/* .line 164 */
} // :cond_8
/* add-int/lit8 v15, v15, 0x1 */
/* move/from16 v38, v9 */
/* move/from16 v8, v36 */
/* move/from16 v9, v37 */
/* .line 193 */
} // .end local v33 # "sir":[I
} // .end local v36 # "div":I
} // .end local v37 # "bsum":I
/* .local v8, "div":I */
/* .local v9, "bsum":I */
/* .restart local v38 # "hm":I */
} // :cond_9
/* move/from16 v36, v8 */
/* move/from16 v37, v9 */
/* move/from16 v9, v38 */
} // .end local v8 # "div":I
} // .end local v38 # "hm":I
/* .local v9, "hm":I */
/* .restart local v36 # "div":I */
/* .restart local v37 # "bsum":I */
/* move v8, v13 */
/* .line 194 */
} // .end local v20 # "yi":I
/* .local v8, "yi":I */
/* move/from16 v20, p1 */
/* .line 195 */
/* .local v20, "stackpointer":I */
/* const/16 v28, 0x0 */
/* move/from16 v43, v23 */
/* move/from16 v23, v8 */
/* move/from16 v8, v28 */
/* move/from16 v28, v43 */
/* .local v8, "y":I */
/* .local v23, "yi":I */
/* .local v28, "boutsum":I */
} // :goto_9
/* move/from16 v33, v15 */
/* move/from16 v15, v27 */
} // .end local v27 # "h":I
/* .local v15, "h":I */
/* .local v33, "i":I */
/* if-ge v8, v15, :cond_b */
/* .line 197 */
/* const/high16 v27, -0x1000000 */
/* aget v38, v14, v23 */
/* and-int v27, v38, v27 */
/* aget v38, v2, v11 */
/* shl-int/lit8 v38, v38, 0x10 */
/* or-int v27, v27, v38 */
/* aget v38, v2, v10 */
/* shl-int/lit8 v38, v38, 0x8 */
/* or-int v27, v27, v38 */
/* aget v38, v2, v37 */
/* or-int v27, v27, v38 */
/* aput v27, v14, v23 */
/* .line 199 */
/* sub-int v11, v11, v30 */
/* .line 200 */
/* sub-int v10, v10, v29 */
/* .line 201 */
/* sub-int v37, v37, v28 */
/* .line 203 */
/* sub-int v27, v20, v0 */
/* add-int v27, v27, v36 */
/* .line 204 */
/* .local v27, "stackstart":I */
/* rem-int v38, v27, v36 */
/* aget-object v38, v1, v38 */
/* .line 206 */
/* .local v38, "sir":[I */
/* const/16 v22, 0x0 */
/* aget v39, v38, v22 */
/* sub-int v30, v30, v39 */
/* .line 207 */
/* const/16 v17, 0x1 */
/* aget v39, v38, v17 */
/* sub-int v29, v29, v39 */
/* .line 208 */
/* aget v39, v38, v25 */
/* sub-int v28, v28, v39 */
/* .line 210 */
/* if-nez v13, :cond_a */
/* .line 211 */
/* add-int v0, v8, v3 */
v0 = java.lang.Math .min ( v0,v9 );
/* mul-int/2addr v0, v12 */
/* aput v0, v26, v8 */
/* .line 213 */
} // :cond_a
/* aget v0, v26, v8 */
/* add-int/2addr v0, v13 */
/* .line 215 */
/* .local v0, "p":I */
/* aget v39, v7, v0 */
/* const/16 v22, 0x0 */
/* aput v39, v38, v22 */
/* .line 216 */
/* aget v39, v6, v0 */
/* const/16 v17, 0x1 */
/* aput v39, v38, v17 */
/* .line 217 */
/* aget v39, v5, v0 */
/* aput v39, v38, v25 */
/* .line 219 */
/* aget v39, v38, v22 */
/* add-int v34, v34, v39 */
/* .line 220 */
/* aget v39, v38, v17 */
/* add-int v32, v32, v39 */
/* .line 221 */
/* aget v39, v38, v25 */
/* add-int v31, v31, v39 */
/* .line 223 */
/* add-int v11, v11, v34 */
/* .line 224 */
/* add-int v10, v10, v32 */
/* .line 225 */
/* add-int v37, v37, v31 */
/* .line 227 */
/* add-int/lit8 v39, v20, 0x1 */
/* rem-int v20, v39, v36 */
/* .line 228 */
/* aget-object v38, v1, v20 */
/* .line 230 */
/* const/16 v22, 0x0 */
/* aget v39, v38, v22 */
/* add-int v30, v30, v39 */
/* .line 231 */
/* const/16 v17, 0x1 */
/* aget v39, v38, v17 */
/* add-int v29, v29, v39 */
/* .line 232 */
/* aget v39, v38, v25 */
/* add-int v28, v28, v39 */
/* .line 234 */
/* aget v39, v38, v22 */
/* sub-int v34, v34, v39 */
/* .line 235 */
/* aget v39, v38, v17 */
/* sub-int v32, v32, v39 */
/* .line 236 */
/* aget v39, v38, v25 */
/* sub-int v31, v31, v39 */
/* .line 238 */
/* add-int v23, v23, v12 */
/* .line 195 */
/* add-int/lit8 v8, v8, 0x1 */
/* move/from16 v0, p1 */
/* move/from16 v27, v15 */
/* move/from16 v15, v33 */
/* goto/16 :goto_9 */
/* .line 161 */
} // .end local v0 # "p":I
} // .end local v27 # "stackstart":I
} // .end local v38 # "sir":[I
} // :cond_b
/* const/16 v17, 0x1 */
/* const/16 v22, 0x0 */
/* add-int/lit8 v13, v13, 0x1 */
/* move/from16 v0, p1 */
/* move/from16 v38, v9 */
/* move/from16 v27, v15 */
/* move/from16 v20, v23 */
/* move/from16 v23, v33 */
/* move v15, v8 */
/* move/from16 v8, v36 */
/* goto/16 :goto_6 */
/* .line 242 */
} // .end local v4 # "yp":I
} // .end local v9 # "hm":I
} // .end local v10 # "gsum":I
} // .end local v11 # "rsum":I
} // .end local v28 # "boutsum":I
} // .end local v29 # "goutsum":I
} // .end local v30 # "routsum":I
} // .end local v31 # "binsum":I
} // .end local v32 # "ginsum":I
} // .end local v33 # "i":I
} // .end local v34 # "rinsum":I
} // .end local v36 # "div":I
} // .end local v37 # "bsum":I
/* .local v8, "div":I */
/* .local v15, "y":I */
/* .local v20, "yi":I */
/* .local v23, "i":I */
/* .local v27, "h":I */
/* .local v38, "hm":I */
} // :cond_c
/* move/from16 v36, v8 */
/* move/from16 v28, v15 */
/* move/from16 v15, v27 */
/* move/from16 v9, v38 */
} // .end local v8 # "div":I
} // .end local v27 # "h":I
} // .end local v38 # "hm":I
/* .restart local v9 # "hm":I */
/* .local v15, "h":I */
/* .local v28, "y":I */
/* .restart local v36 # "div":I */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v12 ); // invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* move-object/from16 v4, v35 */
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v15 ); // invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v4, v14 */
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object/from16 v4, v16 */
android.util.Log .e ( v4,v0 );
/* .line 243 */
int v0 = 0; // const/4 v0, 0x0
int v8 = 0; // const/4 v8, 0x0
int v10 = 0; // const/4 v10, 0x0
/* move-object/from16 v16, v26 */
} // .end local v26 # "vmin":[I
/* .local v16, "vmin":[I */
/* move-object/from16 v4, v19 */
/* move-object/from16 v17, v5 */
} // .end local v5 # "b":[I
/* .local v17, "b":[I */
/* move-object v5, v14 */
/* move-object/from16 v22, v6 */
} // .end local v6 # "g":[I
/* .local v22, "g":[I */
/* move v6, v0 */
/* move-object v0, v7 */
} // .end local v7 # "r":[I
/* .local v0, "r":[I */
/* move v7, v12 */
/* move/from16 v25, v36 */
} // .end local v36 # "div":I
/* .local v25, "div":I */
/* move v11, v9 */
} // .end local v9 # "hm":I
/* .local v11, "hm":I */
/* move v9, v10 */
/* move/from16 v26, v11 */
} // .end local v11 # "hm":I
/* .local v26, "hm":I */
/* move v10, v12 */
/* move/from16 v27, v41 */
} // .end local v41 # "wm":I
/* .local v27, "wm":I */
/* move v11, v15 */
/* invoke-virtual/range {v4 ..v11}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V */
/* .line 244 */
} // .end method
