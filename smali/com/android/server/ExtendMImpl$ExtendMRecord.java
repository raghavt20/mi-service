class com.android.server.ExtendMImpl$ExtendMRecord {
	 /* .source "ExtendMImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/ExtendMImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = "ExtendMRecord" */
} // .end annotation
/* # static fields */
private static final java.lang.String FILE_PATH;
/* # instance fields */
private Integer MAX_DAILY_RECORD_COUNT;
private Integer MAX_RECORD_COUNT;
public java.util.LinkedList dailyRecords;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/LinkedList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.lang.String mDailyRecordData;
private Integer mFlushCount;
private Integer mMarkCount;
private Double mReadBackRateTotal;
private java.lang.String mRecordData;
private java.lang.String mTimeStamp;
public java.util.LinkedList records;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/LinkedList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
 com.android.server.ExtendMImpl$ExtendMRecord ( ) {
/* .locals 2 */
/* .line 1372 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 1373 */
/* const/16 v0, 0x12c */
/* iput v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->MAX_RECORD_COUNT:I */
/* .line 1374 */
int v0 = 7; // const/4 v0, 0x7
/* iput v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->MAX_DAILY_RECORD_COUNT:I */
/* .line 1378 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mMarkCount:I */
/* .line 1379 */
/* iput v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mFlushCount:I */
/* .line 1380 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mReadBackRateTotal:D */
/* .line 1382 */
/* new-instance v0, Ljava/util/LinkedList; */
/* invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V */
this.records = v0;
/* .line 1384 */
/* new-instance v0, Ljava/util/LinkedList; */
/* invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V */
this.dailyRecords = v0;
return;
} // .end method
/* # virtual methods */
public java.util.LinkedList getDailyRecords ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/LinkedList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1392 */
v0 = this.dailyRecords;
} // .end method
public java.util.LinkedList getSingleRecord ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/LinkedList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1388 */
v0 = this.records;
} // .end method
public void setDailyRecord ( Long p0, Long p1 ) {
/* .locals 6 */
/* .param p1, "totalMarked" # J */
/* .param p3, "totalFlushed" # J */
/* .line 1408 */
java.util.Calendar .getInstance ( );
/* .line 1409 */
/* .local v0, "calendar":Ljava/util/Calendar; */
int v1 = 5; // const/4 v1, 0x5
int v2 = -1; // const/4 v2, -0x1
(( java.util.Calendar ) v0 ).add ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V
/* .line 1410 */
(( java.util.Calendar ) v0 ).getTime ( ); // invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;
/* .line 1411 */
/* .local v1, "date":Ljava/util/Date; */
/* iget-wide v2, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mReadBackRateTotal:D */
/* iget v4, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mFlushCount:I */
/* int-to-double v4, v4 */
/* div-double/2addr v2, v4 */
/* double-to-int v2, v2 */
/* .line 1412 */
/* .local v2, "avgRBRate":I */
int v3 = 0; // const/4 v3, 0x0
/* .line 1415 */
/* .local v3, "obj":Lorg/json/JSONObject; */
try { // :try_start_0
/* new-instance v4, Lorg/json/JSONObject; */
/* invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V */
/* move-object v3, v4 */
/* .line 1416 */
final String v4 = "date"; // const-string v4, "date"
(( org.json.JSONObject ) v3 ).put ( v4, v1 ); // invoke-virtual {v3, v4, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 1417 */
final String v4 = "markCount"; // const-string v4, "markCount"
/* iget v5, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mMarkCount:I */
(( org.json.JSONObject ) v3 ).put ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 1418 */
final String v4 = "markTotal"; // const-string v4, "markTotal"
(( org.json.JSONObject ) v3 ).put ( v4, p1, p2 ); // invoke-virtual {v3, v4, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
/* .line 1419 */
final String v4 = "flushCount"; // const-string v4, "flushCount"
/* iget v5, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mFlushCount:I */
(( org.json.JSONObject ) v3 ).put ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 1420 */
final String v4 = "flushTotal"; // const-string v4, "flushTotal"
(( org.json.JSONObject ) v3 ).put ( v4, p3, p4 ); // invoke-virtual {v3, v4, p3, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
/* .line 1421 */
final String v4 = "readBackRate"; // const-string v4, "readBackRate"
(( org.json.JSONObject ) v3 ).put ( v4, v2 ); // invoke-virtual {v3, v4, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1424 */
/* .line 1422 */
/* :catch_0 */
/* move-exception v4 */
/* .line 1423 */
/* .local v4, "ignored":Lorg/json/JSONException; */
(( org.json.JSONException ) v4 ).printStackTrace ( ); // invoke-virtual {v4}, Lorg/json/JSONException;->printStackTrace()V
/* .line 1425 */
} // .end local v4 # "ignored":Lorg/json/JSONException;
} // :goto_0
/* if-nez v3, :cond_0 */
return;
/* .line 1426 */
} // :cond_0
(( org.json.JSONObject ) v3 ).toString ( ); // invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
this.mDailyRecordData = v4;
/* .line 1427 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
v5 = this.mDailyRecordData;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "\n"; // const-string v5, "\n"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "/data/mqsas/extm/dailyrecord.txt"; // const-string v5, "/data/mqsas/extm/dailyrecord.txt"
(( com.android.server.ExtendMImpl$ExtendMRecord ) p0 ).writeToFile ( v4, v5 ); // invoke-virtual {p0, v4, v5}, Lcom/android/server/ExtendMImpl$ExtendMRecord;->writeToFile(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1428 */
v4 = com.android.server.ExtendMImpl .-$$Nest$sfgetLOG_VERBOSE ( );
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 1429 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "daily record data : "; // const-string v5, "daily record data : "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.mDailyRecordData;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "ExtM"; // const-string v5, "ExtM"
android.util.Slog .d ( v5,v4 );
/* .line 1431 */
} // :cond_1
v4 = this.mDailyRecordData;
(( com.android.server.ExtendMImpl$ExtendMRecord ) p0 ).updateDailyRecord ( v4 ); // invoke-virtual {p0, v4}, Lcom/android/server/ExtendMImpl$ExtendMRecord;->updateDailyRecord(Ljava/lang/String;)V
/* .line 1432 */
return;
} // .end method
public void setFlushRecord ( Integer p0, Integer p1, java.lang.String p2, Long p3 ) {
/* .locals 4 */
/* .param p1, "writeCount" # I */
/* .param p2, "writeTotal" # I */
/* .param p3, "readBackRate" # Ljava/lang/String; */
/* .param p4, "costTime" # J */
/* .line 1396 */
/* iget v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mFlushCount:I */
/* add-int/lit8 v0, v0, 0x1 */
/* iput v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mFlushCount:I */
/* .line 1397 */
/* iget-wide v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mReadBackRateTotal:D */
java.lang.Double .parseDouble ( p3 );
/* move-result-wide v2 */
/* add-double/2addr v0, v2 */
/* iput-wide v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mReadBackRateTotal:D */
/* .line 1398 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Flushed finished: "; // const-string v1, "Flushed finished: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "("; // const-string v1, "("
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " m) flushed, and read back rate is "; // const-string v1, " m) flushed, and read back rate is "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " , flush costs "; // const-string v1, " , flush costs "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4, p5 ); // invoke-virtual {v0, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = " ms"; // const-string v1, " ms"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1401 */
/* .local v0, "recordData":Ljava/lang/String; */
/* new-instance v1, Ljava/text/SimpleDateFormat; */
/* const-string/jumbo v2, "yyyy/MM/dd HH:mm:ss" */
java.util.Locale .getDefault ( );
/* invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V */
/* new-instance v2, Ljava/util/Date; */
/* invoke-direct {v2}, Ljava/util/Date;-><init>()V */
(( java.text.SimpleDateFormat ) v1 ).format ( v2 ); // invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
this.mTimeStamp = v1;
/* .line 1402 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
v2 = this.mTimeStamp;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ": "; // const-string v2, ": "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
this.mRecordData = v1;
/* .line 1403 */
(( com.android.server.ExtendMImpl$ExtendMRecord ) p0 ).updateSingleRecord ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/ExtendMImpl$ExtendMRecord;->updateSingleRecord(Ljava/lang/String;)V
/* .line 1404 */
return;
} // .end method
public void updateDailyRecord ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "dailyRecord" # Ljava/lang/String; */
/* .line 1444 */
v0 = this.dailyRecords;
v0 = (( java.util.LinkedList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->size()I
/* iget v1, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->MAX_DAILY_RECORD_COUNT:I */
/* if-ge v0, v1, :cond_0 */
/* .line 1445 */
v0 = this.dailyRecords;
(( java.util.LinkedList ) v0 ).addLast ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V
/* .line 1447 */
} // :cond_0
v0 = this.dailyRecords;
(( java.util.LinkedList ) v0 ).removeFirst ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;
/* .line 1448 */
v0 = this.dailyRecords;
(( java.util.LinkedList ) v0 ).addLast ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V
/* .line 1451 */
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mMarkCount:I */
/* .line 1452 */
/* iput v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mFlushCount:I */
/* .line 1453 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->mReadBackRateTotal:D */
/* .line 1454 */
return;
} // .end method
public void updateSingleRecord ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "recordData" # Ljava/lang/String; */
/* .line 1435 */
v0 = this.records;
v0 = (( java.util.LinkedList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->size()I
/* iget v1, p0, Lcom/android/server/ExtendMImpl$ExtendMRecord;->MAX_RECORD_COUNT:I */
/* if-ge v0, v1, :cond_0 */
/* .line 1436 */
v0 = this.records;
(( java.util.LinkedList ) v0 ).addLast ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V
/* .line 1438 */
} // :cond_0
v0 = this.records;
(( java.util.LinkedList ) v0 ).removeFirst ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;
/* .line 1439 */
v0 = this.records;
(( java.util.LinkedList ) v0 ).addLast ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V
/* .line 1441 */
} // :goto_0
return;
} // .end method
public void writeToFile ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "line" # Ljava/lang/String; */
/* .param p2, "path" # Ljava/lang/String; */
/* .line 1457 */
final String v0 = "ExtM"; // const-string v0, "ExtM"
final String v1 = " write daily extm usage to /data/mqsas/extm/dailyrecord.txt"; // const-string v1, " write daily extm usage to /data/mqsas/extm/dailyrecord.txt"
android.util.Slog .w ( v0,v1 );
/* .line 1458 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1459 */
/* .local v0, "fos":Ljava/io/FileOutputStream; */
/* new-instance v1, Ljava/io/File; */
/* invoke-direct {v1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 1460 */
/* .local v1, "file":Ljava/io/File; */
v2 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
/* if-nez v2, :cond_0 */
/* .line 1462 */
try { // :try_start_0
(( java.io.File ) v1 ).getParentFile ( ); // invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;
(( java.io.File ) v2 ).mkdir ( ); // invoke-virtual {v2}, Ljava/io/File;->mkdir()Z
/* .line 1463 */
(( java.io.File ) v1 ).createNewFile ( ); // invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1466 */
/* .line 1464 */
/* :catch_0 */
/* move-exception v2 */
/* .line 1465 */
/* .local v2, "e":Ljava/io/IOException; */
(( java.io.IOException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
/* .line 1469 */
} // .end local v2 # "e":Ljava/io/IOException;
} // :cond_0
} // :goto_0
try { // :try_start_1
/* new-instance v2, Ljava/io/FileOutputStream; */
int v3 = 1; // const/4 v3, 0x1
/* invoke-direct {v2, v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V */
/* move-object v0, v2 */
/* .line 1470 */
(( java.lang.String ) p1 ).getBytes ( ); // invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B
/* .line 1471 */
/* .local v2, "bytes":[B */
(( java.io.FileOutputStream ) v0 ).write ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/FileOutputStream;->write([B)V
/* .line 1472 */
(( java.io.FileOutputStream ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_2 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1476 */
} // .end local v2 # "bytes":[B
/* nop */
/* .line 1478 */
try { // :try_start_2
(( java.io.FileOutputStream ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_1 */
/* .line 1481 */
} // :goto_1
/* .line 1479 */
/* :catch_1 */
/* move-exception v2 */
/* .line 1480 */
/* .local v2, "e2":Ljava/lang/Exception; */
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
} // .end local v2 # "e2":Ljava/lang/Exception;
/* .line 1476 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 1473 */
/* :catch_2 */
/* move-exception v2 */
/* .line 1474 */
/* .local v2, "e1":Ljava/lang/Exception; */
try { // :try_start_3
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 1476 */
} // .end local v2 # "e1":Ljava/lang/Exception;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1478 */
try { // :try_start_4
(( java.io.FileOutputStream ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_1 */
/* .line 1484 */
} // :cond_1
} // :goto_2
return;
/* .line 1476 */
} // :goto_3
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1478 */
try { // :try_start_5
(( java.io.FileOutputStream ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
/* :try_end_5 */
/* .catch Ljava/lang/Exception; {:try_start_5 ..:try_end_5} :catch_3 */
/* .line 1481 */
/* .line 1479 */
/* :catch_3 */
/* move-exception v3 */
/* .line 1480 */
/* .local v3, "e2":Ljava/lang/Exception; */
(( java.lang.Exception ) v3 ).printStackTrace ( ); // invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
/* .line 1483 */
} // .end local v3 # "e2":Ljava/lang/Exception;
} // :cond_2
} // :goto_4
/* throw v2 */
} // .end method
