public class com.android.server.ProcHunterImpl extends com.android.server.ProcHunterStub {
	 /* .source "ProcHunterImpl.java" */
	 /* # static fields */
	 private static final java.lang.String SYSPROP_ENABLE_PROCHUNTER;
	 private static final java.lang.String TAG;
	 /* # direct methods */
	 static com.android.server.ProcHunterImpl ( ) {
		 /* .locals 3 */
		 /* .line 17 */
		 final String v0 = "ProcHunter"; // const-string v0, "ProcHunter"
		 try { // :try_start_0
			 final String v1 = "persist.sys.debug.enable_prochunter"; // const-string v1, "persist.sys.debug.enable_prochunter"
			 int v2 = 0; // const/4 v2, 0x0
			 v1 = 			 android.os.SystemProperties .getBoolean ( v1,v2 );
			 if ( v1 != null) { // if-eqz v1, :cond_0
				 /* .line 18 */
				 final String v1 = "Load libprochunter"; // const-string v1, "Load libprochunter"
				 android.util.Log .i ( v0,v1 );
				 /* .line 20 */
				 final String v1 = "procdaemon"; // const-string v1, "procdaemon"
				 java.lang.System .loadLibrary ( v1 );
				 /* :try_end_0 */
				 /* .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 ..:try_end_0} :catch_0 */
				 /* .line 24 */
			 } // :cond_0
			 /* .line 22 */
			 /* :catch_0 */
			 /* move-exception v1 */
			 /* .line 23 */
			 /* .local v1, "e":Ljava/lang/UnsatisfiedLinkError; */
			 final String v2 = "can\'t loadLibrary libprochunter"; // const-string v2, "can\'t loadLibrary libprochunter"
			 android.util.Log .w ( v0,v2,v1 );
			 /* .line 25 */
		 } // .end local v1 # "e":Ljava/lang/UnsatisfiedLinkError;
	 } // :goto_0
	 return;
} // .end method
public com.android.server.ProcHunterImpl ( ) {
	 /* .locals 0 */
	 /* .line 10 */
	 /* invoke-direct {p0}, Lcom/android/server/ProcHunterStub;-><init>()V */
	 return;
} // .end method
private static native void nNotifyScreenState ( Boolean p0 ) {
} // .end method
/* # virtual methods */
public void notifyScreenState ( Boolean p0 ) {
	 /* .locals 0 */
	 /* .param p1, "state" # Z */
	 /* .line 34 */
	 com.android.server.ProcHunterImpl .nNotifyScreenState ( p1 );
	 /* .line 35 */
	 return;
} // .end method
public void start ( ) {
	 /* .locals 2 */
	 /* .line 29 */
	 final String v0 = "ProcHunter"; // const-string v0, "ProcHunter"
	 final String v1 = "Here we start the ProcHunter..."; // const-string v1, "Here we start the ProcHunter..."
	 android.util.Log .i ( v0,v1 );
	 /* .line 30 */
	 return;
} // .end method
