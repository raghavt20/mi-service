class com.android.server.MiuiInputFilter$H extends android.os.Handler {
	 /* .source "MiuiInputFilter.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/MiuiInputFilter; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "H" */
} // .end annotation
/* # static fields */
public static final Integer MSG_DOUBLE_CLICK_DELAY;
/* # instance fields */
final com.android.server.MiuiInputFilter this$0; //synthetic
/* # direct methods */
public com.android.server.MiuiInputFilter$H ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 84 */
this.this$0 = p1;
/* .line 85 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 86 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 1 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 90 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 92 */
/* :pswitch_0 */
v0 = this.this$0;
(( com.android.server.MiuiInputFilter ) v0 ).flushPending ( ); // invoke-virtual {v0}, Lcom/android/server/MiuiInputFilter;->flushPending()V
/* .line 95 */
} // :goto_0
/* invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V */
/* .line 96 */
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
