.class public Lcom/android/server/DarkModeOneTrackHelper;
.super Ljava/lang/Object;
.source "DarkModeOneTrackHelper.java"


# static fields
.field private static final APP_ID:Ljava/lang/String; = "31000000485"

.field private static final DEVICE_REGION:Ljava/lang/String;

.field private static final EVENT_NAME:Ljava/lang/String; = "EVENT_NAME"

.field public static final EVENT_NAME_AUTO_SWITCH:Ljava/lang/String; = "auto_switch"

.field public static final EVENT_NAME_SETTING:Ljava/lang/String; = "setting"

.field public static final EVENT_NAME_STATUS:Ljava/lang/String; = "status"

.field public static final EVENT_NAME_SUGGEST:Ljava/lang/String; = "darkModeSuggest"

.field private static final ONETRACK_PACKAGE_NAME:Ljava/lang/String; = "com.miui.analytics"

.field private static final ONE_TRACK_ACTION:Ljava/lang/String; = "onetrack.action.TRACK_EVENT"

.field private static final PARAM_KEY_APP_LIST:Ljava/lang/String; = "app_list"

.field private static final PARAM_KEY_APP_NAME:Ljava/lang/String; = "app_name"

.field private static final PARAM_KEY_APP_PKG:Ljava/lang/String; = "app_package_name"

.field private static final PARAM_KEY_BEGIN_TIME:Ljava/lang/String; = "begin_time"

.field private static final PARAM_KEY_CONTRAST:Ljava/lang/String; = "font_bgcolor_status"

.field private static final PARAM_KEY_DARK_MODE_STATUS:Ljava/lang/String; = "dark_status"

.field private static final PARAM_KEY_END_TIME:Ljava/lang/String; = "end_time"

.field private static final PARAM_KEY_SETTING_CHANNEL:Ljava/lang/String; = "setting_channel"

.field private static final PARAM_KEY_STATUS_AFTER_CLICK:Ljava/lang/String; = "after_click_status"

.field private static final PARAM_KEY_SUGGEST:Ljava/lang/String; = "dark_mode_mode_suggest"

.field private static final PARAM_KEY_SUGGEST_CLICK:Ljava/lang/String; = "dark_mode_suggest_enter_settings"

.field private static final PARAM_KEY_SUGGEST_ENABLE:Ljava/lang/String; = "dark_mode_suggest_enable"

.field private static final PARAM_KEY_SUGGEST_OPEN_IN_SETTING:Ljava/lang/String; = "open_dark_mode_from_settings"

.field private static final PARAM_KEY_SWITCH_WAY:Ljava/lang/String; = "switch_way"

.field private static final PARAM_KEY_TIME_MODE_PATTERN:Ljava/lang/String; = "dark_mode_timing_pattern"

.field private static final PARAM_KEY_TIME_MODE_STATUS:Ljava/lang/String; = "dark_mode_timing_status"

.field private static final PARAM_KEY_WALL_PAPER:Ljava/lang/String; = "wallpaper_status"

.field public static final PARAM_VALUE_APP_NAME:Ljava/lang/String; = "app_name"

.field public static final PARAM_VALUE_APP_PKG:Ljava/lang/String; = "app_package_name"

.field public static final PARAM_VALUE_APP_SWITCH_STATUS:Ljava/lang/String; = "app_switch_status"

.field public static final PARAM_VALUE_CHANNEL_CENTER:Ljava/lang/String; = "\u63a7\u5236\u4e2d\u5fc3"

.field public static final PARAM_VALUE_CHANNEL_NOTIFY:Ljava/lang/String; = "\u5f39\u7a97"

.field public static final PARAM_VALUE_CHANNEL_SETTING:Ljava/lang/String; = "\u8bbe\u7f6e"

.field public static final PARAM_VALUE_CLOSE:Ljava/lang/String; = "\u5173"

.field public static final PARAM_VALUE_CUSTOM:Ljava/lang/String; = "\u81ea\u5b9a\u4e49\u65f6\u95f4"

.field public static final PARAM_VALUE_OPEN:Ljava/lang/String; = "\u5f00"

.field public static final PARAM_VALUE_SWITCH_CLOSE:Ljava/lang/String; = "\u4ece\u6df1\u5230\u6d45"

.field public static final PARAM_VALUE_SWITCH_OPEN:Ljava/lang/String; = "\u4ece\u6d45\u5230\u6df1"

.field public static final PARAM_VALUE_TWILIGHT:Ljava/lang/String; = "\u65e5\u51fa\u65e5\u843d\u6a21\u5f0f"

.field private static final TAG:Ljava/lang/String; = "DarkModeOneTrackHelper"

.field private static final TIP:Ljava/lang/String; = "tip"

.field public static final TIP_APP_SETTING:Ljava/lang/String; = "577.3.3.1.23094"

.field public static final TIP_APP_STATUS:Ljava/lang/String; = "577.1.2.1.23090"

.field public static final TIP_AUTO_SWITCH:Ljava/lang/String; = "577.5.0.1.23096"

.field public static final TIP_CONTRAST_SETTING:Ljava/lang/String; = "577.3.2.1.23093"

.field public static final TIP_DARK_MODE_SETTING:Ljava/lang/String; = "577.4.0.1.23106"

.field public static final TIP_DARK_MODE_STATUS:Ljava/lang/String; = "577.1.1.1.23089"

.field public static final TIP_SUGGEST:Ljava/lang/String; = ""

.field public static final TIP_TIME_MODE_SETTING:Ljava/lang/String; = "577.2.0.1.23091"

.field public static final TIP_WALL_PAPER_SETTING:Ljava/lang/String; = "577.3.1.1.23092"

.field private static sRegions:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$smupdateDarkModeIntent(Landroid/content/Intent;Lcom/android/server/DarkModeEvent;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/android/server/DarkModeOneTrackHelper;->updateDarkModeIntent(Landroid/content/Intent;Lcom/android/server/DarkModeEvent;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 29
    const-string v0, "ro.miui.region"

    const-string v1, "CN"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/DarkModeOneTrackHelper;->DEVICE_REGION:Ljava/lang/String;

    .line 82
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/server/DarkModeOneTrackHelper;->sRegions:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static setDataDisableRegion(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 112
    .local p0, "regions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    sget-object v0, Lcom/android/server/DarkModeOneTrackHelper;->sRegions:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 113
    sget-object v0, Lcom/android/server/DarkModeOneTrackHelper;->sRegions:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 114
    return-void
.end method

.method private static updateAppSettingIntent(Landroid/content/Intent;Lcom/android/server/DarkModeStauesEvent;)V
    .locals 3
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "event"    # Lcom/android/server/DarkModeStauesEvent;

    .line 193
    const-string v0, "EVENT_NAME"

    const-string/jumbo v1, "setting"

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 194
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getTip()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "tip"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 195
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getAppPkg()Ljava/lang/String;

    move-result-object v1

    const-string v2, "app_package_name"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 196
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getAppName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "app_name"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 197
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getAppEnable()Ljava/lang/String;

    move-result-object v1

    const-string v2, "after_click_status"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 198
    return-void
.end method

.method private static updateAppStatusIntent(Landroid/content/Intent;Lcom/android/server/DarkModeStauesEvent;)V
    .locals 3
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "event"    # Lcom/android/server/DarkModeStauesEvent;

    .line 163
    const-string v0, "EVENT_NAME"

    const-string/jumbo v1, "status"

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 164
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getTip()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "tip"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 165
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getAppList()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    const-string v2, "app_list"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 166
    return-void
.end method

.method private static updateAutoSwitchIntent(Landroid/content/Intent;Lcom/android/server/DarkModeStauesEvent;)V
    .locals 3
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "event"    # Lcom/android/server/DarkModeStauesEvent;

    .line 208
    const-string v0, "EVENT_NAME"

    const-string v1, "auto_switch"

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 209
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getTip()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "tip"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 210
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getAutoSwitch()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "switch_way"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 211
    return-void
.end method

.method private static updateContrastSettingIntent(Landroid/content/Intent;Lcom/android/server/DarkModeStauesEvent;)V
    .locals 3
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "event"    # Lcom/android/server/DarkModeStauesEvent;

    .line 187
    const-string v0, "EVENT_NAME"

    const-string/jumbo v1, "setting"

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 188
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getTip()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "tip"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 189
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getContrastStatus()Ljava/lang/String;

    move-result-object v1

    const-string v2, "after_click_status"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 190
    return-void
.end method

.method private static updateDarkModeIntent(Landroid/content/Intent;Lcom/android/server/DarkModeEvent;)V
    .locals 2
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "event"    # Lcom/android/server/DarkModeEvent;

    .line 117
    invoke-interface {p1}, Lcom/android/server/DarkModeEvent;->getTip()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string v1, "577.5.0.1.23096"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x7

    goto :goto_1

    :sswitch_1
    const-string v1, "577.3.2.1.23093"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    goto :goto_1

    :sswitch_2
    const-string v1, "577.1.2.1.23090"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_3
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    goto :goto_1

    :sswitch_4
    const-string v1, "577.3.3.1.23094"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    goto :goto_1

    :sswitch_5
    const-string v1, "577.4.0.1.23106"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    goto :goto_1

    :sswitch_6
    const-string v1, "577.3.1.1.23092"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_7
    const-string v1, "577.2.0.1.23091"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_8
    const-string v1, "577.1.1.1.23089"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    goto :goto_2

    .line 143
    :pswitch_0
    move-object v0, p1

    check-cast v0, Lcom/android/server/DarkModeStauesEvent;

    invoke-static {p0, v0}, Lcom/android/server/DarkModeOneTrackHelper;->updateSuggestEventIntent(Landroid/content/Intent;Lcom/android/server/DarkModeStauesEvent;)V

    .line 144
    goto :goto_2

    .line 140
    :pswitch_1
    move-object v0, p1

    check-cast v0, Lcom/android/server/DarkModeStauesEvent;

    invoke-static {p0, v0}, Lcom/android/server/DarkModeOneTrackHelper;->updateAutoSwitchIntent(Landroid/content/Intent;Lcom/android/server/DarkModeStauesEvent;)V

    .line 141
    goto :goto_2

    .line 137
    :pswitch_2
    move-object v0, p1

    check-cast v0, Lcom/android/server/DarkModeStauesEvent;

    invoke-static {p0, v0}, Lcom/android/server/DarkModeOneTrackHelper;->updateDarkModeSettingIntent(Landroid/content/Intent;Lcom/android/server/DarkModeStauesEvent;)V

    .line 138
    goto :goto_2

    .line 134
    :pswitch_3
    move-object v0, p1

    check-cast v0, Lcom/android/server/DarkModeStauesEvent;

    invoke-static {p0, v0}, Lcom/android/server/DarkModeOneTrackHelper;->updateAppSettingIntent(Landroid/content/Intent;Lcom/android/server/DarkModeStauesEvent;)V

    .line 135
    goto :goto_2

    .line 131
    :pswitch_4
    move-object v0, p1

    check-cast v0, Lcom/android/server/DarkModeStauesEvent;

    invoke-static {p0, v0}, Lcom/android/server/DarkModeOneTrackHelper;->updateContrastSettingIntent(Landroid/content/Intent;Lcom/android/server/DarkModeStauesEvent;)V

    .line 132
    goto :goto_2

    .line 128
    :pswitch_5
    move-object v0, p1

    check-cast v0, Lcom/android/server/DarkModeStauesEvent;

    invoke-static {p0, v0}, Lcom/android/server/DarkModeOneTrackHelper;->updateWallPaperSettingIntent(Landroid/content/Intent;Lcom/android/server/DarkModeStauesEvent;)V

    .line 129
    goto :goto_2

    .line 125
    :pswitch_6
    move-object v0, p1

    check-cast v0, Lcom/android/server/DarkModeStauesEvent;

    invoke-static {p0, v0}, Lcom/android/server/DarkModeOneTrackHelper;->updateTimeModeSettingIntent(Landroid/content/Intent;Lcom/android/server/DarkModeStauesEvent;)V

    .line 126
    goto :goto_2

    .line 122
    :pswitch_7
    move-object v0, p1

    check-cast v0, Lcom/android/server/DarkModeStauesEvent;

    invoke-static {p0, v0}, Lcom/android/server/DarkModeOneTrackHelper;->updateAppStatusIntent(Landroid/content/Intent;Lcom/android/server/DarkModeStauesEvent;)V

    .line 123
    goto :goto_2

    .line 119
    :pswitch_8
    move-object v0, p1

    check-cast v0, Lcom/android/server/DarkModeStauesEvent;

    invoke-static {p0, v0}, Lcom/android/server/DarkModeOneTrackHelper;->updateDarkModeStatusIntent(Landroid/content/Intent;Lcom/android/server/DarkModeStauesEvent;)V

    .line 120
    nop

    .line 148
    :goto_2
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x5ce9f380 -> :sswitch_8
        -0x5c49afa9 -> :sswitch_7
        -0x33208de6 -> :sswitch_6
        -0x32804778 -> :sswitch_5
        -0xa97afe2 -> :sswitch_4
        0x0 -> :sswitch_3
        0x375a7b97 -> :sswitch_2
        0x6123e11c -> :sswitch_1
        0x6264689f -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static updateDarkModeSettingIntent(Landroid/content/Intent;Lcom/android/server/DarkModeStauesEvent;)V
    .locals 3
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "event"    # Lcom/android/server/DarkModeStauesEvent;

    .line 201
    const-string v0, "EVENT_NAME"

    const-string/jumbo v1, "setting"

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 202
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getTip()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "tip"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 203
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getDarkModeStatus()Ljava/lang/String;

    move-result-object v1

    const-string v2, "dark_status"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 204
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getSettingChannel()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "setting_channel"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 205
    return-void
.end method

.method private static updateDarkModeStatusIntent(Landroid/content/Intent;Lcom/android/server/DarkModeStauesEvent;)V
    .locals 3
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "event"    # Lcom/android/server/DarkModeStauesEvent;

    .line 151
    const-string v0, "EVENT_NAME"

    const-string/jumbo v1, "status"

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 152
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getTip()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "tip"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 153
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getDarkModeStatus()Ljava/lang/String;

    move-result-object v1

    const-string v2, "dark_status"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 154
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getTimeModeStatus()Ljava/lang/String;

    move-result-object v1

    const-string v2, "dark_mode_timing_status"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 155
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getTimeModePattern()Ljava/lang/String;

    move-result-object v1

    const-string v2, "dark_mode_timing_pattern"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 156
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getBeginTime()Ljava/lang/String;

    move-result-object v1

    const-string v2, "begin_time"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 157
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getEndTime()Ljava/lang/String;

    move-result-object v1

    const-string v2, "end_time"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 158
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getWallPaperStatus()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "wallpaper_status"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 159
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getContrastStatus()Ljava/lang/String;

    move-result-object v1

    const-string v2, "font_bgcolor_status"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 160
    return-void
.end method

.method private static updateSuggestEventIntent(Landroid/content/Intent;Lcom/android/server/DarkModeStauesEvent;)V
    .locals 2
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "event"    # Lcom/android/server/DarkModeStauesEvent;

    .line 214
    const-string v0, "EVENT_NAME"

    const-string v1, "darkModeSuggest"

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 215
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getSuggest()I

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    const-string v0, "dark_mode_mode_suggest"

    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getSuggest()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 218
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getSuggestEnable()I

    move-result v0

    if-eqz v0, :cond_1

    .line 219
    const-string v0, "dark_mode_suggest_enable"

    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getSuggestEnable()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 221
    :cond_1
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getSuggestClick()I

    move-result v0

    if-eqz v0, :cond_2

    .line 222
    const-string v0, "dark_mode_suggest_enter_settings"

    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getSuggestClick()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 224
    :cond_2
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getSuggestOpenInSetting()I

    move-result v0

    if-eqz v0, :cond_3

    .line 225
    const-string v0, "open_dark_mode_from_settings"

    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getSuggestOpenInSetting()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 227
    :cond_3
    return-void
.end method

.method private static updateTimeModeSettingIntent(Landroid/content/Intent;Lcom/android/server/DarkModeStauesEvent;)V
    .locals 3
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "event"    # Lcom/android/server/DarkModeStauesEvent;

    .line 169
    const-string v0, "EVENT_NAME"

    const-string/jumbo v1, "setting"

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 170
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getTip()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "tip"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 171
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getTimeModeStatus()Ljava/lang/String;

    move-result-object v1

    const-string v2, "dark_mode_timing_status"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 172
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getTimeModePattern()Ljava/lang/String;

    move-result-object v1

    const-string v2, "dark_mode_timing_pattern"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 173
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getBeginTime()Ljava/lang/String;

    move-result-object v1

    const-string v2, "begin_time"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 174
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getEndTime()Ljava/lang/String;

    move-result-object v1

    const-string v2, "end_time"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 175
    const-string/jumbo v0, "\u65e5\u51fa\u65e5\u843d\u6a21\u5f0f"

    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getTimeModePattern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176
    const-string/jumbo v0, "setting_channel"

    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getSettingChannel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 178
    :cond_0
    return-void
.end method

.method private static updateWallPaperSettingIntent(Landroid/content/Intent;Lcom/android/server/DarkModeStauesEvent;)V
    .locals 3
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "event"    # Lcom/android/server/DarkModeStauesEvent;

    .line 181
    const-string v0, "EVENT_NAME"

    const-string/jumbo v1, "setting"

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 182
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getTip()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "tip"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 183
    invoke-virtual {p1}, Lcom/android/server/DarkModeStauesEvent;->getWallPaperStatus()Ljava/lang/String;

    move-result-object v1

    const-string v2, "after_click_status"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 184
    return-void
.end method

.method public static uploadToOneTrack(Landroid/content/Context;Lcom/android/server/DarkModeEvent;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "event"    # Lcom/android/server/DarkModeEvent;

    .line 85
    sget-object v0, Lcom/android/server/DarkModeOneTrackHelper;->sRegions:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/server/DarkModeOneTrackHelper;->sRegions:Ljava/util/Set;

    sget-object v1, Lcom/android/server/DarkModeOneTrackHelper;->DEVICE_REGION:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "do not upload data in "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DarkModeOneTrackHelper"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    return-void

    .line 89
    :cond_0
    if-eqz p0, :cond_2

    if-eqz p1, :cond_2

    invoke-interface {p1}, Lcom/android/server/DarkModeEvent;->getEventName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Lcom/android/server/DarkModeEvent;->getTip()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    goto :goto_0

    .line 92
    :cond_1
    invoke-interface {p1}, Lcom/android/server/DarkModeEvent;->clone()Lcom/android/server/DarkModeEvent;

    move-result-object v0

    .line 93
    .local v0, "newEvent":Lcom/android/server/DarkModeEvent;
    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/android/server/DarkModeOneTrackHelper$1;

    invoke-direct {v2, p0, v0}, Lcom/android/server/DarkModeOneTrackHelper$1;-><init>(Landroid/content/Context;Lcom/android/server/DarkModeEvent;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 109
    return-void

    .line 90
    .end local v0    # "newEvent":Lcom/android/server/DarkModeEvent;
    :cond_2
    :goto_0
    return-void
.end method
