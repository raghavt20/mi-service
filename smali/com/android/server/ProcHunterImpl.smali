.class public Lcom/android/server/ProcHunterImpl;
.super Lcom/android/server/ProcHunterStub;
.source "ProcHunterImpl.java"


# static fields
.field private static final SYSPROP_ENABLE_PROCHUNTER:Ljava/lang/String; = "persist.sys.debug.enable_prochunter"

.field private static final TAG:Ljava/lang/String; = "ProcHunter"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 17
    const-string v0, "ProcHunter"

    :try_start_0
    const-string v1, "persist.sys.debug.enable_prochunter"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 18
    const-string v1, "Load libprochunter"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 20
    const-string v1, "procdaemon"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 24
    :cond_0
    goto :goto_0

    .line 22
    :catch_0
    move-exception v1

    .line 23
    .local v1, "e":Ljava/lang/UnsatisfiedLinkError;
    const-string v2, "can\'t loadLibrary libprochunter"

    invoke-static {v0, v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 25
    .end local v1    # "e":Ljava/lang/UnsatisfiedLinkError;
    :goto_0
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Lcom/android/server/ProcHunterStub;-><init>()V

    return-void
.end method

.method private static native nNotifyScreenState(Z)V
.end method


# virtual methods
.method public notifyScreenState(Z)V
    .locals 0
    .param p1, "state"    # Z

    .line 34
    invoke-static {p1}, Lcom/android/server/ProcHunterImpl;->nNotifyScreenState(Z)V

    .line 35
    return-void
.end method

.method public start()V
    .locals 2

    .line 29
    const-string v0, "ProcHunter"

    const-string v1, "Here we start the ProcHunter..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 30
    return-void
.end method
