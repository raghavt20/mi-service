.class final Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;
.super Landroid/os/UEventObserver;
.source "MiuiBatteryServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "BatteryUEventObserver"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;


# direct methods
.method private constructor <init>(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)V
    .locals 0

    .line 817
    iput-object p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-direct {p0}, Landroid/os/UEventObserver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;-><init>(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)V

    return-void
.end method


# virtual methods
.method public onUEvent(Landroid/os/UEventObserver$UEvent;)V
    .locals 6
    .param p1, "event"    # Landroid/os/UEventObserver$UEvent;

    .line 820
    const-string v0, "POWER_SUPPLY_TX_ADAPTER"

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    const-string v3, "MiuiBatteryServiceImpl"

    if-eqz v1, :cond_0

    .line 821
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 822
    .local v0, "wirelessTxType":I
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastWirelessTxType(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 823
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Wireless_tx_type = "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " mLastWireless_tx_type = "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v4}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastWirelessTxType(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 825
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fputmLastWirelessTxType(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;I)V

    .line 826
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-virtual {v1, v2, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(II)V

    .line 830
    .end local v0    # "wirelessTxType":I
    :cond_0
    const-string v0, "POWER_SUPPLY_HVDCP3_TYPE"

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 831
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 832
    .local v0, "hvdcpType":I
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastHvdcpType(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 833
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HVDCP type = "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " Last HVDCP type = "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v4}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastHvdcpType(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 835
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fputmLastHvdcpType(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;I)V

    .line 836
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    const/4 v4, 0x2

    invoke-virtual {v1, v4, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(II)V

    .line 840
    .end local v0    # "hvdcpType":I
    :cond_1
    const-string v0, "POWER_SUPPLY_QUICK_CHARGE_TYPE"

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x3

    if-eqz v1, :cond_2

    .line 841
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 842
    .local v0, "quickChargeType":I
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastQuickChargeType(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 843
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Quick Charge type = "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " Last Quick Charge type = "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v5, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v5}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastQuickChargeType(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 845
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fputmLastQuickChargeType(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;I)V

    .line 846
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-virtual {v1, v4, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(II)V

    .line 847
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1, v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fputmUpdateSocDecimal(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;Z)V

    .line 851
    .end local v0    # "quickChargeType":I
    :cond_2
    const-string v0, "POWER_SUPPLY_SOC_DECIMAL"

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastQuickChargeType(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I

    move-result v1

    if-lt v1, v4, :cond_3

    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmUpdateSocDecimal(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 853
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 854
    .local v0, "socDecimal":I
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    const-string v2, "POWER_SUPPLY_SOC_DECIMAL_RATE"

    invoke-virtual {p1, v2}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 855
    .local v1, "socDecimalRate":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "socDecimal = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " socDecimalRate = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 856
    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    const/4 v4, 0x4

    invoke-virtual {v2, v4, v0, v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(III)V

    .line 857
    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fputmUpdateSocDecimal(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;Z)V

    .line 860
    .end local v0    # "socDecimal":I
    .end local v1    # "socDecimalRate":I
    :cond_3
    const-string v0, "POWER_SUPPLY_REVERSE_CHG_STATE"

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 861
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 862
    .local v0, "closeReason":I
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    iget-object v1, v1, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmSupportWirelessCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastCloseReason(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I

    move-result v1

    if-eq v0, v1, :cond_4

    .line 863
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Wireless Reverse Charging Closed Reason  = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Last Wireless Reverse charging closed reason = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastCloseReason(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 865
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fputmLastCloseReason(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;I)V

    .line 866
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    const/4 v2, 0x5

    invoke-virtual {v1, v2, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(II)V

    .line 870
    .end local v0    # "closeReason":I
    :cond_4
    const-string v0, "POWER_SUPPLY_REVERSE_CHG_MODE"

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 871
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 872
    .local v0, "openStatus":I
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    iget-object v1, v1, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmSupportWirelessCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastOpenStatus(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I

    move-result v1

    if-eq v0, v1, :cond_5

    .line 873
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Wireless Reverse Charing status  = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Last Wireless Reverse Charing status = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastOpenStatus(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 875
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fputmLastOpenStatus(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;I)V

    .line 876
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    const/4 v2, 0x6

    invoke-virtual {v1, v2, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(II)V

    .line 880
    .end local v0    # "openStatus":I
    :cond_5
    const-string v0, "POWER_SUPPLY_SHUTDOWN_DELAY"

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 881
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 882
    .local v0, "shutdownDelay":I
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastShutdownDelay(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I

    move-result v1

    if-eq v0, v1, :cond_6

    .line 883
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "shutdown delay status  = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Last shutdown delay status = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastShutdownDelay(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 885
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fputmLastShutdownDelay(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;I)V

    .line 886
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(II)V

    .line 890
    .end local v0    # "shutdownDelay":I
    :cond_6
    const-string v0, "POWER_SUPPLY_WLS_FW_STATE"

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 891
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 892
    .local v0, "wirelessFwState":I
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastWirelessFwState(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I

    move-result v1

    if-eq v0, v1, :cond_7

    .line 893
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "wireless fw update status  = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Last wireless fw update status = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastWirelessFwState(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 895
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fputmLastWirelessFwState(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;I)V

    .line 896
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    const/16 v2, 0xb

    invoke-virtual {v1, v2, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(II)V

    .line 900
    .end local v0    # "wirelessFwState":I
    :cond_7
    const-string v0, "POWER_SUPPLY_REVERSE_PEN_CHG_STATE"

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 901
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 902
    .local v0, "penReverseChargeState":I
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastPenReverseChargeState(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I

    move-result v1

    if-eq v0, v1, :cond_8

    .line 903
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "current pen reverse charge state = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Last pen reverse charge state = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastPenReverseChargeState(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 905
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fputmLastPenReverseChargeState(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;I)V

    .line 906
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    const/16 v2, 0xc

    invoke-virtual {v1, v2, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(II)V

    .line 911
    .end local v0    # "penReverseChargeState":I
    :cond_8
    const-string v0, "POWER_SUPPLY_REVERSE_PEN_SOC"

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 912
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 913
    .local v0, "penReverseSoc":I
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastPenReverseSoc(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I

    move-result v1

    if-eq v0, v1, :cond_9

    .line 914
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "current pen reverse soc = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Last pen reverse soc = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastPenReverseSoc(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 916
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fputmLastPenReverseSoc(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;I)V

    .line 919
    .end local v0    # "penReverseSoc":I
    :cond_9
    const-string v0, "POWER_SUPPLY_PEN_MAC"

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_a

    .line 920
    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 921
    .local v0, "penReverseMac":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastPenReverseMac(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 922
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "current pen reverse mac = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Last pen reverse mac = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastPenReverseMac(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 924
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fputmLastPenReverseMac(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;Ljava/lang/String;)V

    .line 927
    .end local v0    # "penReverseMac":Ljava/lang/String;
    :cond_a
    const-string v0, "POWER_SUPPLY_PEN_PLACE_ERR"

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_b

    .line 928
    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 929
    .local v0, "penReversePlaceErr":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastPenReversePLaceErr(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 930
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "current pen place error = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Last pen place error = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastPenReversePLaceErr(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 932
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fputmLastPenReversePLaceErr(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;Ljava/lang/String;)V

    .line 937
    .end local v0    # "penReversePlaceErr":Ljava/lang/String;
    :cond_b
    const-string v0, "POWER_SUPPLY_CONNECTOR_TEMP"

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_c

    .line 938
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 939
    .local v0, "connectorTemp":I
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastConnectorTemp(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I

    move-result v1

    if-eq v0, v1, :cond_c

    .line 940
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "currenet connector temp = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Last currenet connector temp = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastConnectorTemp(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 942
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fputmLastConnectorTemp(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;I)V

    .line 943
    const/16 v1, 0x28a

    if-le v0, v1, :cond_c

    .line 944
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    const/16 v2, 0x10

    invoke-virtual {v1, v2, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(II)V

    .line 949
    .end local v0    # "connectorTemp":I
    :cond_c
    const-string v0, "POWER_SUPPLY_RX_OFFSET"

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_d

    .line 950
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 951
    .local v0, "rxOffset":I
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastRxOffset(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I

    move-result v1

    if-eq v0, v1, :cond_d

    .line 952
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "current rx offset = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " last rx offset = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastRxOffset(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 954
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fputmLastRxOffset(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;I)V

    .line 955
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    const/16 v2, 0x11

    invoke-virtual {v1, v2, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(II)V

    .line 959
    .end local v0    # "rxOffset":I
    :cond_d
    const-string v0, "POWER_SUPPLY_MOISTURE_DET_STS"

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_e

    .line 960
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 961
    .local v0, "moistureDet":I
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastMoistureDet(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I

    move-result v1

    if-eq v0, v1, :cond_e

    .line 962
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "current moistureDet = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " last moistureDet = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastMoistureDet(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 964
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fputmLastMoistureDet(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;I)V

    .line 965
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    const/16 v2, 0x15

    invoke-virtual {v1, v2, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(II)V

    .line 968
    .end local v0    # "moistureDet":I
    :cond_e
    const-string v0, "POWER_SUPPLY_CAR_APP_STATE"

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_f

    .line 969
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 970
    .local v0, "pogoConnectedState":I
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastPogoConnectedState(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I

    move-result v1

    if-eq v0, v1, :cond_f

    .line 971
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pogo charging connected sate = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " last pog charging connected state = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastPogoConnectedState(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 973
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fputmLastPogoConnectedState(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;I)V

    .line 974
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    const/16 v2, 0x16

    invoke-virtual {v1, v2, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(II)V

    .line 977
    .end local v0    # "pogoConnectedState":I
    :cond_f
    const-string v0, "POWER_SUPPLY_NAME"

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_11

    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmHandleInfo(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;

    move-result-object v1

    if-eqz v1, :cond_11

    .line 978
    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 980
    .local v0, "info":Ljava/lang/String;
    if-eqz v0, :cond_11

    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmHandleInfo(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;

    move-result-object v1

    iget-object v1, v1, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mHidName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 982
    const-string v1, "POWER_SUPPLY_CAPACITY"

    invoke-virtual {p1, v1}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_11

    .line 983
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    const-string v2, "POWER_SUPPLY_CAPACITY"

    invoke-virtual {p1, v2}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 984
    .local v1, "level":I
    const-string v2, "POWER_SUPPLY_STATUS"

    invoke-virtual {p1, v2}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 985
    .local v2, "state":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v4}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmHandleInfo(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;

    move-result-object v4

    iget v4, v4, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mBatteryLevel:I

    if-ne v4, v1, :cond_10

    if-eqz v2, :cond_11

    iget-object v4, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v4}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmHandleInfo(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;

    move-result-object v4

    iget-object v4, v4, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mBatteryStats:Ljava/lang/String;

    .line 986
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_11

    .line 987
    :cond_10
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handle battery changed, state = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " and level = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 988
    iget-object v4, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v4}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmHandleInfo(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->setBatteryLevel(I)V

    .line 989
    iget-object v4, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v4}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmHandleInfo(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->setBatteryStats(Ljava/lang/String;)V

    .line 990
    iget-object v4, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v4}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$msendHandleBatteryStatsChangeBroadcast(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)V

    .line 995
    .end local v0    # "info":Ljava/lang/String;
    .end local v1    # "level":I
    .end local v2    # "state":Ljava/lang/String;
    :cond_11
    const-string v0, "POWER_SUPPLY_LOW_INDUCTANCE_OFFSET"

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_12

    .line 996
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 997
    .local v0, "offsetState":I
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastOffsetState(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I

    move-result v1

    if-eq v0, v1, :cond_12

    .line 998
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Low Inductance offset state = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Last Low Inductance offset state = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastOffsetState(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1000
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fputmLastOffsetState(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;I)V

    .line 1001
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    const/16 v2, 0x1b

    invoke-virtual {v1, v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendEmptyMessage(I)Z

    .line 1004
    .end local v0    # "offsetState":I
    :cond_12
    const-string v0, "POWER_SUPPLY_NTC_ALARM"

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_13

    .line 1005
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1006
    .local v0, "ntcAlarm":I
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastNtcAlarm(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I

    move-result v1

    if-eq v0, v1, :cond_13

    .line 1007
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "current ntc alarm = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " last ntc alarm = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fgetmLastNtcAlarm(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1009
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    invoke-static {v1, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->-$$Nest$fputmLastNtcAlarm(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;I)V

    .line 1010
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;->this$1:Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    const/16 v2, 0x1c

    invoke-virtual {v1, v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendEmptyMessage(I)Z

    .line 1013
    .end local v0    # "ntcAlarm":I
    :cond_13
    return-void
.end method
