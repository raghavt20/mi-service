public class com.android.server.ScoutSystemMonitor extends com.android.server.ScoutStub {
	 /* .source "ScoutSystemMonitor.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;, */
	 /* Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;, */
	 /* Lcom/android/server/ScoutSystemMonitor$ScreenStateReceiver;, */
	 /* Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Integer SCOUT_COMPLETED;
public static final Integer SCOUT_FW_LEVEL_NORMAL;
public static final Integer SCOUT_MEM_CHECK_MSG;
public static final Integer SCOUT_MEM_CRITICAL_MSG;
public static final Integer SCOUT_MEM_DUMP_MSG;
public static final Integer SCOUT_OVERDUE;
public static final Long SCOUT_SYSTEM_IO_TIMEOUT;
public static final Long SCOUT_SYSTEM_TIMEOUT;
public static final Integer SCOUT_WAITED_HALF;
public static final Integer SCOUT_WAITING;
private static final java.lang.String TAG;
public static final java.util.ArrayList mScoutHandlerCheckers;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private android.content.Context mContext;
private com.android.server.ScoutSystemMonitor$ScoutHandlerChecker mScoutBinderMonitorChecker;
private com.android.server.ScoutSystemMonitor$ScoutHandlerChecker mScoutMonitorChecker;
private java.lang.Object mScoutSysLock;
private com.android.server.am.ActivityManagerService mService;
private android.os.HandlerThread mSysMonitorThread;
private android.os.HandlerThread mSysServiceMonitorThread;
private android.os.HandlerThread mSysWorkThread;
private volatile com.android.server.ScoutSystemMonitor$SystemWorkerHandler mSysWorkerHandler;
private android.app.AppScoutStateMachine mUiScoutStateMachine;
private com.miui.app.MiuiFboServiceInternal miuiFboService;
private Integer preScoutLevel;
private Integer scoutLevel;
/* # direct methods */
static java.lang.Object -$$Nest$fgetmScoutSysLock ( com.android.server.ScoutSystemMonitor p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mScoutSysLock;
} // .end method
static com.miui.app.MiuiFboServiceInternal -$$Nest$fgetmiuiFboService ( com.android.server.ScoutSystemMonitor p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.miuiFboService;
} // .end method
static void -$$Nest$fputmiuiFboService ( com.android.server.ScoutSystemMonitor p0, com.miui.app.MiuiFboServiceInternal p1 ) { //bridge//synthethic
/* .locals 0 */
this.miuiFboService = p1;
return;
} // .end method
static com.android.server.ScoutSystemMonitor ( ) {
/* .locals 1 */
/* .line 81 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
return;
} // .end method
public com.android.server.ScoutSystemMonitor ( ) {
/* .locals 2 */
/* .line 125 */
/* invoke-direct {p0}, Lcom/android/server/ScoutStub;-><init>()V */
/* .line 79 */
int v0 = 0; // const/4 v0, 0x0
this.mUiScoutStateMachine = v0;
/* .line 98 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/ScoutSystemMonitor;->scoutLevel:I */
/* .line 99 */
/* iput v0, p0, Lcom/android/server/ScoutSystemMonitor;->preScoutLevel:I */
/* .line 126 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "ScoutSystemMonitor"; // const-string v1, "ScoutSystemMonitor"
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mSysMonitorThread = v0;
/* .line 127 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "ScoutSystemWork"; // const-string v1, "ScoutSystemWork"
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mSysWorkThread = v0;
/* .line 128 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "ScoutSystemServiceMonitor"; // const-string v1, "ScoutSystemServiceMonitor"
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mSysServiceMonitorThread = v0;
/* .line 129 */
com.android.server.ScoutSystemMonitor .initNativeScout ( );
/* .line 130 */
com.android.server.ScoutSystemMonitor .registerThermalTempListener ( );
/* .line 131 */
return;
} // .end method
private void addScoutBinderMonitor ( com.android.server.Watchdog$Monitor p0 ) {
/* .locals 1 */
/* .param p1, "monitor" # Lcom/android/server/Watchdog$Monitor; */
/* .line 822 */
/* sget-boolean v0, Lcom/android/server/ScoutHelper;->ENABLED_SCOUT:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 823 */
} // :cond_0
/* monitor-enter p0 */
/* .line 824 */
try { // :try_start_0
v0 = this.mScoutBinderMonitorChecker;
(( com.android.server.ScoutSystemMonitor$ScoutHandlerChecker ) v0 ).addMonitorLocked ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->addMonitorLocked(Lcom/android/server/Watchdog$Monitor;)V
/* .line 825 */
/* monitor-exit p0 */
/* .line 826 */
return;
/* .line 825 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
public static java.lang.String describeScoutCheckersLocked ( java.util.List p0 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;", */
/* ">;)", */
/* "Ljava/lang/String;" */
/* } */
} // .end annotation
/* .line 862 */
/* .local p0, "checkers":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;>;" */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* const/16 v1, 0x80 */
/* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V */
/* .line 863 */
/* .local v0, "builder":Ljava/lang/StringBuilder; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
v2 = } // :goto_0
/* if-ge v1, v2, :cond_1 */
/* .line 864 */
v2 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v2, :cond_0 */
/* .line 865 */
final String v2 = ", "; // const-string v2, ", "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 867 */
} // :cond_0
/* check-cast v2, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker; */
(( com.android.server.ScoutSystemMonitor$ScoutHandlerChecker ) v2 ).describeBlockedStateLocked ( ); // invoke-virtual {v2}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->describeBlockedStateLocked()Ljava/lang/String;
/* .line 868 */
/* .local v2, "info":Ljava/lang/String; */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 863 */
} // .end local v2 # "info":Ljava/lang/String;
/* add-int/lit8 v1, v1, 0x1 */
/* .line 870 */
} // .end local v1 # "i":I
} // :cond_1
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public static Integer evaluateCheckerScoutCompletionLocked ( ) {
/* .locals 4 */
/* .line 838 */
int v0 = 0; // const/4 v0, 0x0
/* .line 839 */
/* .local v0, "state":I */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = com.android.server.ScoutSystemMonitor.mScoutHandlerCheckers;
v3 = (( java.util.ArrayList ) v2 ).size ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->size()I
/* if-ge v1, v3, :cond_0 */
/* .line 840 */
(( java.util.ArrayList ) v2 ).get ( v1 ); // invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker; */
/* .line 841 */
/* .local v2, "hc":Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker; */
v3 = (( com.android.server.ScoutSystemMonitor$ScoutHandlerChecker ) v2 ).getCompletionStateLocked ( ); // invoke-virtual {v2}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->getCompletionStateLocked()I
v0 = java.lang.Math .max ( v0,v3 );
/* .line 839 */
} // .end local v2 # "hc":Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;
/* add-int/lit8 v1, v1, 0x1 */
/* .line 843 */
} // .end local v1 # "i":I
} // :cond_0
} // .end method
public static com.android.server.ScoutSystemMonitor getInstance ( ) {
/* .locals 1 */
/* .line 105 */
com.android.server.ScoutStub .getInstance ( );
/* check-cast v0, Lcom/android/server/ScoutSystemMonitor; */
} // .end method
public static java.util.ArrayList getScoutBlockedCheckersLocked ( Boolean p0 ) {
/* .locals 7 */
/* .param p0, "isHalf" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(Z)", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 847 */
/* sget-boolean v0, Lcom/android/server/ScoutHelper;->ENABLED_SCOUT_DEBUG:Z */
/* .line 848 */
/* .local v0, "debug":Z */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 849 */
/* .local v1, "checkers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;>;" */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
v3 = com.android.server.ScoutSystemMonitor.mScoutHandlerCheckers;
v4 = (( java.util.ArrayList ) v3 ).size ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
/* if-ge v2, v4, :cond_4 */
/* .line 850 */
(( java.util.ArrayList ) v3 ).get ( v2 ); // invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker; */
/* .line 851 */
/* .local v3, "hc":Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker; */
v4 = (( com.android.server.ScoutSystemMonitor$ScoutHandlerChecker ) v3 ).isOverdueLocked ( ); // invoke-virtual {v3}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->isOverdueLocked()Z
final String v5 = "ScoutSystemMonitor"; // const-string v5, "ScoutSystemMonitor"
/* if-nez v4, :cond_1 */
if ( p0 != null) { // if-eqz p0, :cond_0
v4 = (( com.android.server.ScoutSystemMonitor$ScoutHandlerChecker ) v3 ).isHalfLocked ( ); // invoke-virtual {v3}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->isHalfLocked()Z
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 855 */
} // :cond_0
if ( v0 != null) { // if-eqz v0, :cond_3
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Debug: no Block getScoutCheckersLocked Block : "; // const-string v6, "Debug: no Block getScoutCheckersLocked Block : "
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.ScoutSystemMonitor$ScoutHandlerChecker ) v3 ).describeBlockedStateLocked ( ); // invoke-virtual {v3}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->describeBlockedStateLocked()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v4 );
/* .line 852 */
} // :cond_1
} // :goto_1
if ( v0 != null) { // if-eqz v0, :cond_2
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Debug: getScoutCheckersLocked Block : "; // const-string v6, "Debug: getScoutCheckersLocked Block : "
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.ScoutSystemMonitor$ScoutHandlerChecker ) v3 ).describeBlockedStateLocked ( ); // invoke-virtual {v3}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->describeBlockedStateLocked()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v4 );
/* .line 853 */
} // :cond_2
(( java.util.ArrayList ) v1 ).add ( v3 ); // invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 849 */
} // .end local v3 # "hc":Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;
} // :cond_3
} // :goto_2
/* add-int/lit8 v2, v2, 0x1 */
/* .line 858 */
} // .end local v2 # "i":I
} // :cond_4
} // .end method
private java.lang.String getScoutSystemDetails ( java.util.List p0 ) {
/* .locals 10 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;", */
/* ">;)", */
/* "Ljava/lang/String;" */
/* } */
} // .end annotation
/* .line 581 */
/* .local p1, "handlerCheckers":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;>;" */
v0 = miui.mqsas.scout.ScoutUtils .isMtbfTest ( );
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 582 */
/* .line 584 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_4
/* .line 585 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 586 */
/* .local v0, "details":Ljava/lang/StringBuilder; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
v2 = } // :goto_0
/* if-ge v1, v2, :cond_3 */
/* .line 587 */
/* check-cast v2, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker; */
/* .line 588 */
/* .local v2, "mCheck":Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker; */
v3 = (( com.android.server.ScoutSystemMonitor$ScoutHandlerChecker ) v2 ).getThreadTid ( ); // invoke-virtual {v2}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->getThreadTid()I
/* .line 590 */
/* .local v3, "tid":I */
/* sget-boolean v4, Lcom/android/server/ScoutHelper;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v4, :cond_1 */
/* if-lez v3, :cond_1 */
/* .line 591 */
com.android.server.ScoutHelper .getMiuiStackTraceByTid ( v3 );
/* .local v4, "st":[Ljava/lang/StackTraceElement; */
/* .line 593 */
} // .end local v4 # "st":[Ljava/lang/StackTraceElement;
} // :cond_1
(( com.android.server.ScoutSystemMonitor$ScoutHandlerChecker ) v2 ).getThread ( ); // invoke-virtual {v2}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->getThread()Ljava/lang/Thread;
(( java.lang.Thread ) v4 ).getStackTrace ( ); // invoke-virtual {v4}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;
/* .line 595 */
/* .restart local v4 # "st":[Ljava/lang/StackTraceElement; */
} // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 596 */
/* array-length v5, v4 */
int v6 = 0; // const/4 v6, 0x0
} // :goto_2
/* if-ge v6, v5, :cond_2 */
/* aget-object v7, v4, v6 */
/* .line 597 */
/* .local v7, "element":Ljava/lang/StackTraceElement; */
final String v8 = " at "; // const-string v8, " at "
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v9 = "\n"; // const-string v9, "\n"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 596 */
} // .end local v7 # "element":Ljava/lang/StackTraceElement;
/* add-int/lit8 v6, v6, 0x1 */
/* .line 600 */
} // :cond_2
final String v5 = "\n\n"; // const-string v5, "\n\n"
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 586 */
} // .end local v2 # "mCheck":Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;
} // .end local v3 # "tid":I
} // .end local v4 # "st":[Ljava/lang/StackTraceElement;
/* add-int/lit8 v1, v1, 0x1 */
/* .line 602 */
} // .end local v1 # "i":I
} // :cond_3
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 604 */
} // .end local v0 # "details":Ljava/lang/StringBuilder;
} // :cond_4
} // .end method
private java.lang.String getScoutSystemDetailsAsync ( java.util.List p0 ) {
/* .locals 8 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;", */
/* ">;)", */
/* "Ljava/lang/String;" */
/* } */
} // .end annotation
/* .line 527 */
/* .local p1, "handlerCheckers":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;>;" */
final String v0 = "getScoutSystemDetails shutdown exp "; // const-string v0, "getScoutSystemDetails shutdown exp "
final String v1 = "ScoutSystemMonitor"; // const-string v1, "ScoutSystemMonitor"
v2 = miui.mqsas.scout.ScoutUtils .isMtbfTest ( );
if ( v2 != null) { // if-eqz v2, :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 529 */
} // :cond_0
java.util.concurrent.Executors .newSingleThreadExecutor ( );
/* .line 530 */
/* .local v2, "executor":Ljava/util/concurrent/ExecutorService; */
/* new-instance v3, Ljava/util/concurrent/FutureTask; */
/* new-instance v4, Lcom/android/server/ScoutSystemMonitor$1; */
/* invoke-direct {v4, p0, p1}, Lcom/android/server/ScoutSystemMonitor$1;-><init>(Lcom/android/server/ScoutSystemMonitor;Ljava/util/List;)V */
/* invoke-direct {v3, v4}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V */
/* .line 555 */
/* .local v3, "futureTask":Ljava/util/concurrent/FutureTask;, "Ljava/util/concurrent/FutureTask<Ljava/lang/String;>;" */
/* .line 556 */
int v4 = 0; // const/4 v4, 0x0
/* .line 558 */
/* .local v4, "result":Ljava/lang/String; */
try { // :try_start_0
v5 = java.util.concurrent.TimeUnit.MILLISECONDS;
/* const-wide/16 v6, 0xbb8 */
(( java.util.concurrent.FutureTask ) v3 ).get ( v6, v7, v5 ); // invoke-virtual {v3, v6, v7, v5}, Ljava/util/concurrent/FutureTask;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
/* check-cast v5, Ljava/lang/String; */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move-object v4, v5 */
/* .line 559 */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 560 */
/* nop */
/* .line 571 */
try { // :try_start_1
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 574 */
/* .line 572 */
/* :catch_0 */
/* move-exception v5 */
/* .line 573 */
/* .local v5, "e":Ljava/lang/Exception; */
android.util.Slog .w ( v1,v0,v5 );
/* .line 560 */
} // .end local v5 # "e":Ljava/lang/Exception;
} // :goto_0
/* .line 571 */
} // :cond_1
try { // :try_start_2
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_1 */
/* .line 574 */
} // :goto_1
/* .line 572 */
/* :catch_1 */
/* move-exception v5 */
/* .line 573 */
/* .restart local v5 # "e":Ljava/lang/Exception; */
android.util.Slog .w ( v1,v0,v5 );
/* .line 575 */
} // .end local v5 # "e":Ljava/lang/Exception;
/* .line 570 */
/* :catchall_0 */
/* move-exception v5 */
/* .line 562 */
/* :catch_2 */
/* move-exception v5 */
/* .line 563 */
/* .restart local v5 # "e":Ljava/lang/Exception; */
try { // :try_start_3
final String v6 = "getScoutSystemDetails exp is "; // const-string v6, "getScoutSystemDetails exp is "
android.util.Slog .w ( v1,v6,v5 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 565 */
int v6 = 1; // const/4 v6, 0x1
try { // :try_start_4
(( java.util.concurrent.FutureTask ) v3 ).cancel ( v6 ); // invoke-virtual {v3, v6}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_3 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 568 */
/* .line 566 */
/* :catch_3 */
/* move-exception v6 */
/* .line 567 */
/* .local v6, "e1":Ljava/lang/Exception; */
try { // :try_start_5
final String v7 = "getScoutSystemDetails futureTask.cancel exp "; // const-string v7, "getScoutSystemDetails futureTask.cancel exp "
android.util.Slog .w ( v1,v7,v6 );
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_0 */
/* .line 571 */
} // .end local v5 # "e":Ljava/lang/Exception;
} // .end local v6 # "e1":Ljava/lang/Exception;
} // :goto_2
try { // :try_start_6
/* :try_end_6 */
/* .catch Ljava/lang/Exception; {:try_start_6 ..:try_end_6} :catch_1 */
/* .line 576 */
} // :goto_3
final String v0 = "getScoutSystemDetails fail"; // const-string v0, "getScoutSystemDetails fail"
android.util.Slog .w ( v1,v0 );
/* .line 577 */
/* .line 571 */
} // :goto_4
try { // :try_start_7
/* :try_end_7 */
/* .catch Ljava/lang/Exception; {:try_start_7 ..:try_end_7} :catch_4 */
/* .line 574 */
/* .line 572 */
/* :catch_4 */
/* move-exception v6 */
/* .line 573 */
/* .local v6, "e":Ljava/lang/Exception; */
android.util.Slog .w ( v1,v0,v6 );
/* .line 575 */
} // .end local v6 # "e":Ljava/lang/Exception;
} // :goto_5
/* throw v5 */
} // .end method
private static void initNativeScout ( ) {
/* .locals 3 */
/* .line 110 */
final String v0 = "ScoutSystemMonitor"; // const-string v0, "ScoutSystemMonitor"
try { // :try_start_0
final String v1 = "Load libscout"; // const-string v1, "Load libscout"
android.util.Slog .i ( v0,v1 );
/* .line 111 */
final String v1 = "scout"; // const-string v1, "scout"
java.lang.System .loadLibrary ( v1 );
/* :try_end_0 */
/* .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 114 */
/* .line 112 */
/* :catch_0 */
/* move-exception v1 */
/* .line 113 */
/* .local v1, "e":Ljava/lang/UnsatisfiedLinkError; */
final String v2 = "can\'t loadLibrary libscout"; // const-string v2, "can\'t loadLibrary libscout"
android.util.Slog .w ( v0,v2,v1 );
/* .line 115 */
} // .end local v1 # "e":Ljava/lang/UnsatisfiedLinkError;
} // :goto_0
return;
} // .end method
static void lambda$registerThermalTempListener$0 ( Integer p0, Long p1 ) { //synthethic
/* .locals 1 */
/* .param p0, "temp" # I */
/* .param p1, "timestamp" # J */
/* .line 121 */
miui.mqsas.sdk.MQSEventManagerDelegate .getInstance ( );
(( miui.mqsas.sdk.MQSEventManagerDelegate ) v0 ).reportThermalTempChange ( p0, p1, p2 ); // invoke-virtual {v0, p0, p1, p2}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportThermalTempChange(IJ)V
return;
} // .end method
static void lambda$registerThermalTempListener$1 ( Integer p0 ) { //synthethic
/* .locals 4 */
/* .param p0, "temp" # I */
/* .line 119 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* .line 120 */
/* .local v0, "timestamp":J */
com.android.server.MiuiBgThread .getHandler ( );
/* new-instance v3, Lcom/android/server/ScoutSystemMonitor$$ExternalSyntheticLambda1; */
/* invoke-direct {v3, p0, v0, v1}, Lcom/android/server/ScoutSystemMonitor$$ExternalSyntheticLambda1;-><init>(IJ)V */
(( android.os.Handler ) v2 ).post ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 122 */
return;
} // .end method
static void onFwScout ( Integer p0, java.io.File p1, com.android.server.ScoutSystemMonitor$ScoutSystemInfo p2, java.lang.String p3 ) {
/* .locals 3 */
/* .param p0, "type" # I */
/* .param p1, "trace" # Ljava/io/File; */
/* .param p2, "info" # Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo; */
/* .param p3, "mOtherMsg" # Ljava/lang/String; */
/* .line 934 */
v0 = android.os.Debug .isDebuggerConnected ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 935 */
return;
/* .line 941 */
} // :cond_0
/* new-instance v0, Lmiui/mqsas/sdk/event/SysScoutEvent; */
/* invoke-direct {v0}, Lmiui/mqsas/sdk/event/SysScoutEvent;-><init>()V */
/* .line 942 */
/* .local v0, "event":Lmiui/mqsas/sdk/event/SysScoutEvent; */
(( miui.mqsas.sdk.event.SysScoutEvent ) v0 ).setType ( p0 ); // invoke-virtual {v0, p0}, Lmiui/mqsas/sdk/event/SysScoutEvent;->setType(I)V
/* .line 943 */
v1 = android.os.Process .myPid ( );
(( miui.mqsas.sdk.event.SysScoutEvent ) v0 ).setPid ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/SysScoutEvent;->setPid(I)V
/* .line 944 */
/* const-string/jumbo v1, "system_server" */
(( miui.mqsas.sdk.event.SysScoutEvent ) v0 ).setProcessName ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/SysScoutEvent;->setProcessName(Ljava/lang/String;)V
/* .line 945 */
(( miui.mqsas.sdk.event.SysScoutEvent ) v0 ).setPackageName ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/SysScoutEvent;->setPackageName(Ljava/lang/String;)V
/* .line 946 */
(( com.android.server.ScoutSystemMonitor$ScoutSystemInfo ) p2 ).getTimeStamp ( ); // invoke-virtual {p2}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->getTimeStamp()J
/* move-result-wide v1 */
(( miui.mqsas.sdk.event.SysScoutEvent ) v0 ).setTimeStamp ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lmiui/mqsas/sdk/event/SysScoutEvent;->setTimeStamp(J)V
/* .line 947 */
int v1 = 1; // const/4 v1, 0x1
(( miui.mqsas.sdk.event.SysScoutEvent ) v0 ).setSystem ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/SysScoutEvent;->setSystem(Z)V
/* .line 948 */
(( com.android.server.ScoutSystemMonitor$ScoutSystemInfo ) p2 ).getDescribeInfo ( ); // invoke-virtual {p2}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->getDescribeInfo()Ljava/lang/String;
(( miui.mqsas.sdk.event.SysScoutEvent ) v0 ).setSummary ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/SysScoutEvent;->setSummary(Ljava/lang/String;)V
/* .line 949 */
(( com.android.server.ScoutSystemMonitor$ScoutSystemInfo ) p2 ).getDetails ( ); // invoke-virtual {p2}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->getDetails()Ljava/lang/String;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 950 */
(( com.android.server.ScoutSystemMonitor$ScoutSystemInfo ) p2 ).getDetails ( ); // invoke-virtual {p2}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->getDetails()Ljava/lang/String;
(( miui.mqsas.sdk.event.SysScoutEvent ) v0 ).setDetails ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/SysScoutEvent;->setDetails(Ljava/lang/String;)V
/* .line 952 */
} // :cond_1
(( com.android.server.ScoutSystemMonitor$ScoutSystemInfo ) p2 ).getDescribeInfo ( ); // invoke-virtual {p2}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->getDescribeInfo()Ljava/lang/String;
(( miui.mqsas.sdk.event.SysScoutEvent ) v0 ).setDetails ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/SysScoutEvent;->setDetails(Ljava/lang/String;)V
/* .line 954 */
} // :goto_0
(( miui.mqsas.sdk.event.SysScoutEvent ) v0 ).setOtherMsg ( p3 ); // invoke-virtual {v0, p3}, Lmiui/mqsas/sdk/event/SysScoutEvent;->setOtherMsg(Ljava/lang/String;)V
/* .line 955 */
v1 = (( com.android.server.ScoutSystemMonitor$ScoutSystemInfo ) p2 ).getScoutLevel ( ); // invoke-virtual {p2}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->getScoutLevel()I
(( miui.mqsas.sdk.event.SysScoutEvent ) v0 ).setScoutLevel ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/SysScoutEvent;->setScoutLevel(I)V
/* .line 956 */
v1 = (( com.android.server.ScoutSystemMonitor$ScoutSystemInfo ) p2 ).getPreScoutLevel ( ); // invoke-virtual {p2}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->getPreScoutLevel()I
(( miui.mqsas.sdk.event.SysScoutEvent ) v0 ).setPreScoutLevel ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/SysScoutEvent;->setPreScoutLevel(I)V
/* .line 957 */
if ( p1 != null) { // if-eqz p1, :cond_2
/* .line 958 */
(( java.io.File ) p1 ).getAbsolutePath ( ); // invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( miui.mqsas.sdk.event.SysScoutEvent ) v0 ).setLogName ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/SysScoutEvent;->setLogName(Ljava/lang/String;)V
/* .line 960 */
} // :cond_2
(( com.android.server.ScoutSystemMonitor$ScoutSystemInfo ) p2 ).getBinderTransInfo ( ); // invoke-virtual {p2}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->getBinderTransInfo()Ljava/lang/String;
(( miui.mqsas.sdk.event.SysScoutEvent ) v0 ).setBinderTransactionInfo ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/SysScoutEvent;->setBinderTransactionInfo(Ljava/lang/String;)V
/* .line 961 */
(( com.android.server.ScoutSystemMonitor$ScoutSystemInfo ) p2 ).getUuid ( ); // invoke-virtual {p2}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->getUuid()Ljava/lang/String;
(( miui.mqsas.sdk.event.SysScoutEvent ) v0 ).setUuid ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/SysScoutEvent;->setUuid(Ljava/lang/String;)V
/* .line 962 */
miui.mqsas.sdk.MQSEventManagerDelegate .getInstance ( );
(( miui.mqsas.sdk.MQSEventManagerDelegate ) v1 ).reportSysScoutEvent ( v0 ); // invoke-virtual {v1, v0}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportSysScoutEvent(Lmiui/mqsas/sdk/event/SysScoutEvent;)V
/* .line 963 */
return;
} // .end method
private void registerScreenStateReceiver ( ) {
/* .locals 3 */
/* .line 273 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 274 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.SCREEN_ON"; // const-string v1, "android.intent.action.SCREEN_ON"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 275 */
final String v1 = "android.intent.action.SCREEN_OFF"; // const-string v1, "android.intent.action.SCREEN_OFF"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 276 */
/* const/16 v1, 0x3e8 */
(( android.content.IntentFilter ) v0 ).setPriority ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->setPriority(I)V
/* .line 277 */
v1 = this.mContext;
/* new-instance v2, Lcom/android/server/ScoutSystemMonitor$ScreenStateReceiver; */
/* invoke-direct {v2, p0}, Lcom/android/server/ScoutSystemMonitor$ScreenStateReceiver;-><init>(Lcom/android/server/ScoutSystemMonitor;)V */
(( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 278 */
return;
} // .end method
private static void registerThermalTempListener ( ) {
/* .locals 2 */
/* .line 118 */
com.android.server.am.SystemPressureControllerStub .getInstance ( );
/* new-instance v1, Lcom/android/server/ScoutSystemMonitor$$ExternalSyntheticLambda0; */
/* invoke-direct {v1}, Lcom/android/server/ScoutSystemMonitor$$ExternalSyntheticLambda0;-><init>()V */
(( com.android.server.am.SystemPressureControllerStub ) v0 ).registerThermalTempListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/SystemPressureControllerStub;->registerThermalTempListener(Lcom/android/server/am/ThermalTempListener;)V
/* .line 123 */
return;
} // .end method
private void runSystemMonitor ( Long p0, Integer p1, Boolean p2, com.android.server.ScoutWatchdogInfo$ScoutId p3 ) {
/* .locals 30 */
/* .param p1, "timeout" # J */
/* .param p3, "count" # I */
/* .param p4, "waitedHalf" # Z */
/* .param p5, "scoutId" # Lcom/android/server/ScoutWatchdogInfo$ScoutId; */
/* .line 609 */
/* move-object/from16 v1, p0 */
/* move/from16 v2, p3 */
/* const-wide/16 v3, 0x2710 */
/* .line 610 */
/* .local v3, "waitTime":J */
/* const-wide/16 v5, 0x2710 */
/* cmp-long v0, p1, v5 */
/* if-gez v0, :cond_0 */
/* .line 611 */
/* move-wide/from16 v3, p1 */
/* move-wide v11, v3 */
/* .line 610 */
} // :cond_0
/* move-wide v11, v3 */
/* .line 614 */
} // .end local v3 # "waitTime":J
/* .local v11, "waitTime":J */
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_1
v3 = com.android.server.ScoutSystemMonitor.mScoutHandlerCheckers;
v4 = (( java.util.ArrayList ) v3 ).size ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
/* if-ge v0, v4, :cond_1 */
/* .line 615 */
(( java.util.ArrayList ) v3 ).get ( v0 ); // invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker; */
/* .line 616 */
/* .local v3, "hc":Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker; */
(( com.android.server.ScoutSystemMonitor$ScoutHandlerChecker ) v3 ).scheduleCheckLocked ( ); // invoke-virtual {v3}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->scheduleCheckLocked()V
/* .line 614 */
} // .end local v3 # "hc":Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;
/* add-int/lit8 v0, v0, 0x1 */
/* .line 619 */
} // .end local v0 # "i":I
} // :cond_1
/* move-wide v3, v11 */
/* .line 620 */
/* .local v3, "scoutTimeout":J */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v13 */
/* move-wide v9, v3 */
/* .line 621 */
} // .end local v3 # "scoutTimeout":J
/* .local v9, "scoutTimeout":J */
/* .local v13, "scoutStart":J */
} // :goto_2
/* const-wide/16 v3, 0x0 */
/* cmp-long v0, v9, v3 */
/* if-lez v0, :cond_2 */
/* .line 623 */
try { // :try_start_0
v0 = this.mScoutSysLock;
(( java.lang.Object ) v0 ).wait ( v9, v10 ); // invoke-virtual {v0, v9, v10}, Ljava/lang/Object;->wait(J)V
/* :try_end_0 */
/* .catch Ljava/lang/InterruptedException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 626 */
/* .line 624 */
/* :catch_0 */
/* move-exception v0 */
/* .line 625 */
/* .local v0, "ex":Ljava/lang/InterruptedException; */
final String v3 = "ScoutSystemMonitor"; // const-string v3, "ScoutSystemMonitor"
android.util.Slog .wtf ( v3,v0 );
/* .line 627 */
} // .end local v0 # "ex":Ljava/lang/InterruptedException;
} // :goto_3
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v3 */
/* sub-long/2addr v3, v13 */
/* sub-long v9, v11, v3 */
/* .line 631 */
} // :cond_2
v0 = com.android.server.ScoutSystemMonitor .evaluateCheckerScoutCompletionLocked ( );
/* .line 632 */
/* .local v0, "waitState":I */
/* invoke-virtual/range {p5 ..p5}, Lcom/android/server/ScoutWatchdogInfo$ScoutId;->getUuid()Ljava/lang/String; */
/* .line 633 */
/* .local v15, "uuid":Ljava/lang/String; */
/* iget v7, v1, Lcom/android/server/ScoutSystemMonitor;->scoutLevel:I */
/* iput v7, v1, Lcom/android/server/ScoutSystemMonitor;->preScoutLevel:I */
/* .line 634 */
int v3 = 3; // const/4 v3, 0x3
final String v8 = "ms"; // const-string v8, "ms"
final String v6 = " waittime : "; // const-string v6, " waittime : "
final String v5 = "; Scout Check count : "; // const-string v5, "; Scout Check count : "
final String v4 = " to "; // const-string v4, " to "
/* move-object/from16 v16, v8 */
final String v8 = "MIUIScout System"; // const-string v8, "MIUIScout System"
/* move-wide/from16 v17, v13 */
} // .end local v13 # "scoutStart":J
/* .local v17, "scoutStart":J */
int v13 = 1; // const/4 v13, 0x1
int v14 = 2; // const/4 v14, 0x2
/* if-eq v0, v3, :cond_7 */
/* if-ne v0, v14, :cond_3 */
/* if-ne v2, v14, :cond_3 */
/* move-wide/from16 v19, v9 */
/* move-object v10, v4 */
/* move-object v9, v5 */
/* move-object v4, v8 */
/* move-object/from16 v5, v16 */
/* move-object v8, v6 */
/* move-object/from16 v6, p5 */
/* goto/16 :goto_5 */
/* .line 663 */
} // :cond_3
int v14 = 0; // const/4 v14, 0x0
/* iput v14, v1, Lcom/android/server/ScoutSystemMonitor;->scoutLevel:I */
/* .line 664 */
/* if-le v7, v13, :cond_5 */
/* .line 665 */
/* new-instance v13, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo; */
final String v19 = ""; // const-string v19, ""
final String v20 = ""; // const-string v20, ""
/* const/16 v21, 0x0 */
/* move-object v3, v13 */
/* move-object/from16 v22, v4 */
/* move-object/from16 v4, v19 */
/* move-object/from16 v23, v5 */
/* move-object/from16 v5, v20 */
/* move-object/from16 v24, v6 */
/* move v6, v14 */
/* move-object/from16 v25, v8 */
/* move-object/from16 v14, v16 */
/* move-object/from16 v8, v21 */
/* move-wide/from16 v19, v9 */
} // .end local v9 # "scoutTimeout":J
/* .local v19, "scoutTimeout":J */
/* move/from16 v9, p4 */
/* move-object v10, v15 */
/* invoke-direct/range {v3 ..v10}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;-><init>(Ljava/lang/String;Ljava/lang/String;IILjava/util/ArrayList;ZLjava/lang/String;)V */
/* .line 666 */
/* .local v3, "info":Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo; */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v4 */
(( com.android.server.ScoutSystemMonitor$ScoutSystemInfo ) v3 ).setTimeStamp ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->setTimeStamp(J)V
/* .line 667 */
final String v4 = ""; // const-string v4, ""
int v5 = 0; // const/4 v5, 0x0
if ( p4 != null) { // if-eqz p4, :cond_4
/* .line 668 */
/* const/16 v6, 0x192 */
(( com.android.server.ScoutSystemMonitor$ScoutSystemInfo ) v3 ).setEvent ( v6 ); // invoke-virtual {v3, v6}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->setEvent(I)V
/* .line 669 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Enter FW_SCOUT_NORMALLY from Level "; // const-string v8, "Enter FW_SCOUT_NORMALLY from Level "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v8, v1, Lcom/android/server/ScoutSystemMonitor;->preScoutLevel:I */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* move-object/from16 v10, v22 */
(( java.lang.StringBuilder ) v7 ).append ( v10 ); // invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v8, v1, Lcom/android/server/ScoutSystemMonitor;->scoutLevel:I */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* move-object/from16 v9, v23 */
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v2 ); // invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* move-object/from16 v8, v24 */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v11, v12 ); // invoke-virtual {v7, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v14 ); // invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object/from16 v13, v25 */
android.util.Slog .d ( v13,v7 );
/* .line 672 */
com.android.server.ScoutSystemMonitor .onFwScout ( v6,v5,v3,v4 );
/* goto/16 :goto_4 */
/* .line 674 */
} // :cond_4
/* move-object/from16 v10, v22 */
/* move-object/from16 v9, v23 */
/* move-object/from16 v8, v24 */
/* move-object/from16 v13, v25 */
/* const/16 v6, 0x193 */
(( com.android.server.ScoutSystemMonitor$ScoutSystemInfo ) v3 ).setEvent ( v6 ); // invoke-virtual {v3, v6}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->setEvent(I)V
/* .line 675 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Enter FW_SCOUT_SLOW from Level "; // const-string v5, "Enter FW_SCOUT_SLOW from Level "
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v1, Lcom/android/server/ScoutSystemMonitor;->preScoutLevel:I */
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v10 ); // invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v1, Lcom/android/server/ScoutSystemMonitor;->scoutLevel:I */
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v9 ); // invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v8 ); // invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v11, v12 ); // invoke-virtual {v5, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v14 ); // invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v13,v5 );
/* .line 678 */
int v5 = 0; // const/4 v5, 0x0
com.android.server.ScoutSystemMonitor .onFwScout ( v6,v5,v3,v4 );
/* .line 680 */
} // .end local v3 # "info":Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;
} // .end local v19 # "scoutTimeout":J
/* .restart local v9 # "scoutTimeout":J */
} // :cond_5
/* move-object v13, v8 */
/* move-wide/from16 v19, v9 */
/* move-object/from16 v14, v16 */
/* move-object v10, v4 */
/* move-object v9, v5 */
/* move-object v8, v6 */
} // .end local v9 # "scoutTimeout":J
/* .restart local v19 # "scoutTimeout":J */
/* if-lez v7, :cond_6 */
/* .line 681 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "FW Resume from Level "; // const-string v4, "FW Resume from Level "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, v1, Lcom/android/server/ScoutSystemMonitor;->preScoutLevel:I */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v10 ); // invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, v1, Lcom/android/server/ScoutSystemMonitor;->scoutLevel:I */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v9 ); // invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v8 ); // invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v11, v12 ); // invoke-virtual {v3, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v14 ); // invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v13,v3 );
/* .line 685 */
} // :cond_6
} // :goto_4
java.util.UUID .randomUUID ( );
(( java.util.UUID ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;
/* .line 686 */
/* move-object/from16 v6, p5 */
(( com.android.server.ScoutWatchdogInfo$ScoutId ) v6 ).setUuid ( v15 ); // invoke-virtual {v6, v15}, Lcom/android/server/ScoutWatchdogInfo$ScoutId;->setUuid(Ljava/lang/String;)V
/* move/from16 v24, v0 */
/* move-wide v4, v11 */
/* goto/16 :goto_9 */
/* .line 634 */
} // .end local v19 # "scoutTimeout":J
/* .restart local v9 # "scoutTimeout":J */
} // :cond_7
/* move-wide/from16 v19, v9 */
/* move-object v10, v4 */
/* move-object v9, v5 */
/* move-object v4, v8 */
/* move-object/from16 v5, v16 */
/* move-object v8, v6 */
/* move-object/from16 v6, p5 */
/* .line 635 */
} // .end local v9 # "scoutTimeout":J
/* .restart local v19 # "scoutTimeout":J */
} // :goto_5
/* add-int/2addr v7, v13 */
/* iput v7, v1, Lcom/android/server/ScoutSystemMonitor;->scoutLevel:I */
/* .line 636 */
/* if-ne v2, v14, :cond_9 */
/* if-nez p4, :cond_9 */
/* if-gt v7, v14, :cond_8 */
} // :cond_8
/* move/from16 v24, v0 */
/* move-wide v4, v11 */
/* goto/16 :goto_9 */
} // :cond_9
} // :goto_6
/* if-ne v2, v14, :cond_a */
if ( p4 != null) { // if-eqz p4, :cond_a
int v3 = 5; // const/4 v3, 0x5
/* if-gt v7, v3, :cond_8 */
/* .line 638 */
} // :cond_a
/* if-ne v0, v14, :cond_b */
/* move v3, v13 */
} // :cond_b
int v3 = 0; // const/4 v3, 0x0
} // :goto_7
/* move v14, v3 */
/* .line 639 */
/* .local v14, "isHalf":Z */
com.android.server.ScoutSystemMonitor .getScoutBlockedCheckersLocked ( v14 );
/* .line 640 */
/* .local v3, "handlerChecke":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;>;" */
com.android.server.ScoutSystemMonitor .describeScoutCheckersLocked ( v3 );
/* .line 641 */
/* .local v13, "scoutDescribe":Ljava/lang/String; */
/* invoke-direct {v1, v3}, Lcom/android/server/ScoutSystemMonitor;->getScoutSystemDetailsAsync(Ljava/util/List;)Ljava/lang/String; */
/* .line 642 */
/* .local v21, "scoutDetails":Ljava/lang/String; */
/* new-instance v22, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo; */
/* iget v7, v1, Lcom/android/server/ScoutSystemMonitor;->scoutLevel:I */
/* move/from16 v24, v0 */
} // .end local v0 # "waitState":I
/* .local v24, "waitState":I */
/* iget v0, v1, Lcom/android/server/ScoutSystemMonitor;->preScoutLevel:I */
/* move-object/from16 v25, v3 */
} // .end local v3 # "handlerChecke":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;>;"
/* .local v25, "handlerChecke":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;>;" */
/* move-object/from16 v3, v22 */
/* move/from16 v26, v14 */
/* move-object v14, v4 */
} // .end local v14 # "isHalf":Z
/* .local v26, "isHalf":Z */
/* move-object v4, v13 */
/* move-object/from16 v27, v14 */
/* move-object v14, v5 */
/* move-object/from16 v5, v21 */
/* move v6, v7 */
/* move v7, v0 */
/* move-object v0, v8 */
/* move-object/from16 v8, v25 */
/* move-object/from16 v23, v14 */
/* move-object v14, v9 */
/* move/from16 v9, p4 */
/* move-wide/from16 v28, v11 */
/* move-object v11, v10 */
} // .end local v11 # "waitTime":J
/* .local v28, "waitTime":J */
/* move-object v10, v15 */
/* invoke-direct/range {v3 ..v10}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;-><init>(Ljava/lang/String;Ljava/lang/String;IILjava/util/ArrayList;ZLjava/lang/String;)V */
/* .line 645 */
/* .local v3, "info":Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo; */
final String v4 = "BinderThreadMonitor"; // const-string v4, "BinderThreadMonitor"
v4 = (( java.lang.String ) v13 ).contains ( v4 ); // invoke-virtual {v13, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v4 != null) { // if-eqz v4, :cond_c
/* .line 646 */
/* const/16 v4, 0x191 */
(( com.android.server.ScoutSystemMonitor$ScoutSystemInfo ) v3 ).setEvent ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->setEvent(I)V
/* .line 647 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Enter FW_SCOUT_BINDER_FULL from Level "; // const-string v5, "Enter FW_SCOUT_BINDER_FULL from Level "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v5, v1, Lcom/android/server/ScoutSystemMonitor;->preScoutLevel:I */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v11 ); // invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v5, v1, Lcom/android/server/ScoutSystemMonitor;->scoutLevel:I */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v14 ); // invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-wide/from16 v4, v28 */
} // .end local v28 # "waitTime":J
/* .local v4, "waitTime":J */
(( java.lang.StringBuilder ) v0 ).append ( v4, v5 ); // invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
/* move-object/from16 v6, v23 */
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object/from16 v7, v27 */
android.util.Slog .d ( v7,v0 );
/* .line 650 */
v0 = this.mSysWorkerHandler;
int v6 = 1; // const/4 v6, 0x1
(( com.android.server.ScoutSystemMonitor$SystemWorkerHandler ) v0 ).obtainMessage ( v6 ); // invoke-virtual {v0, v6}, Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;->obtainMessage(I)Landroid/os/Message;
/* .local v0, "msg":Landroid/os/Message; */
/* .line 652 */
} // .end local v0 # "msg":Landroid/os/Message;
} // .end local v4 # "waitTime":J
/* .restart local v28 # "waitTime":J */
} // :cond_c
/* move-object/from16 v6, v23 */
/* move-object/from16 v7, v27 */
/* move-wide/from16 v4, v28 */
} // .end local v28 # "waitTime":J
/* .restart local v4 # "waitTime":J */
/* const/16 v8, 0x190 */
(( com.android.server.ScoutSystemMonitor$ScoutSystemInfo ) v3 ).setEvent ( v8 ); // invoke-virtual {v3, v8}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->setEvent(I)V
/* .line 653 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "Enter FW_SCOUT_HANG from Level "; // const-string v9, "Enter FW_SCOUT_HANG from Level "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v9, v1, Lcom/android/server/ScoutSystemMonitor;->preScoutLevel:I */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v11 ); // invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v9, v1, Lcom/android/server/ScoutSystemMonitor;->scoutLevel:I */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v14 ); // invoke-virtual {v8, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v2 ); // invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v0 ); // invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4, v5 ); // invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v7,v0 );
/* .line 656 */
v0 = this.mSysWorkerHandler;
int v6 = 0; // const/4 v6, 0x0
(( com.android.server.ScoutSystemMonitor$SystemWorkerHandler ) v0 ).obtainMessage ( v6 ); // invoke-virtual {v0, v6}, Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;->obtainMessage(I)Landroid/os/Message;
/* .line 658 */
/* .restart local v0 # "msg":Landroid/os/Message; */
} // :goto_8
java.lang.System .currentTimeMillis ( );
/* move-result-wide v6 */
(( com.android.server.ScoutSystemMonitor$ScoutSystemInfo ) v3 ).setTimeStamp ( v6, v7 ); // invoke-virtual {v3, v6, v7}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->setTimeStamp(J)V
/* .line 659 */
this.obj = v3;
/* .line 660 */
v6 = this.mSysWorkerHandler;
(( com.android.server.ScoutSystemMonitor$SystemWorkerHandler ) v6 ).sendMessage ( v0 ); // invoke-virtual {v6, v0}, Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 661 */
} // .end local v0 # "msg":Landroid/os/Message;
} // .end local v13 # "scoutDescribe":Ljava/lang/String;
} // .end local v21 # "scoutDetails":Ljava/lang/String;
} // .end local v25 # "handlerChecke":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;>;"
} // .end local v26 # "isHalf":Z
/* nop */
/* .line 688 */
} // .end local v3 # "info":Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;
} // :goto_9
return;
} // .end method
/* # virtual methods */
public void addScoutMonitor ( com.android.server.Watchdog$Monitor p0 ) {
/* .locals 1 */
/* .param p1, "monitor" # Lcom/android/server/Watchdog$Monitor; */
/* .line 815 */
/* sget-boolean v0, Lcom/android/server/ScoutHelper;->ENABLED_SCOUT:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 816 */
} // :cond_0
/* monitor-enter p0 */
/* .line 817 */
try { // :try_start_0
v0 = this.mScoutMonitorChecker;
(( com.android.server.ScoutSystemMonitor$ScoutHandlerChecker ) v0 ).addMonitorLocked ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->addMonitorLocked(Lcom/android/server/Watchdog$Monitor;)V
/* .line 818 */
/* monitor-exit p0 */
/* .line 819 */
return;
/* .line 818 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
public void addScoutThread ( android.os.Handler p0 ) {
/* .locals 8 */
/* .param p1, "thread" # Landroid/os/Handler; */
/* .line 830 */
/* sget-boolean v0, Lcom/android/server/ScoutHelper;->ENABLED_SCOUT:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 831 */
} // :cond_0
/* monitor-enter p0 */
/* .line 832 */
try { // :try_start_0
(( android.os.Handler ) p1 ).getLooper ( ); // invoke-virtual {p1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;
(( android.os.Looper ) v0 ).getThread ( ); // invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;
(( java.lang.Thread ) v0 ).getName ( ); // invoke-virtual {v0}, Ljava/lang/Thread;->getName()Ljava/lang/String;
/* .line 833 */
/* .local v4, "name":Ljava/lang/String; */
v0 = com.android.server.ScoutSystemMonitor.mScoutHandlerCheckers;
/* new-instance v7, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker; */
/* const-wide/16 v5, 0x2710 */
/* move-object v1, v7 */
/* move-object v2, p0 */
/* move-object v3, p1 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;-><init>(Lcom/android/server/ScoutSystemMonitor;Landroid/os/Handler;Ljava/lang/String;J)V */
(( java.util.ArrayList ) v0 ).add ( v7 ); // invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 834 */
/* nop */
} // .end local v4 # "name":Ljava/lang/String;
/* monitor-exit p0 */
/* .line 835 */
return;
/* .line 834 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
public void crashIfHasDThread ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "mHasDThread" # Z */
/* .line 173 */
if ( p1 != null) { // if-eqz p1, :cond_0
final String v0 = "ScoutSystemMonitor"; // const-string v0, "ScoutSystemMonitor"
v1 = com.android.server.ScoutHelper .isEnabelPanicDThread ( v0 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 174 */
/* const-string/jumbo v1, "trigger kernel crash: Has D state thread" */
android.util.Slog .e ( v0,v1 );
/* .line 176 */
/* const-wide/16 v0, 0xbb8 */
android.os.SystemClock .sleep ( v0,v1 );
/* .line 178 */
/* const/16 v0, 0x63 */
com.android.server.ScoutHelper .doSysRqInterface ( v0 );
/* .line 180 */
} // :cond_0
return;
} // .end method
public android.os.Handler getSystemWorkerHandler ( ) {
/* .locals 1 */
/* .line 269 */
v0 = this.mSysWorkerHandler;
} // .end method
public android.app.AppScoutStateMachine getUiScoutStateMachine ( ) {
/* .locals 1 */
/* .line 911 */
v0 = this.mUiScoutStateMachine;
} // .end method
public void init ( java.lang.Object p0 ) {
/* .locals 14 */
/* .param p1, "mLock" # Ljava/lang/Object; */
/* .line 134 */
this.mScoutSysLock = p1;
/* .line 135 */
v0 = this.mSysWorkThread;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 136 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 137 */
/* new-instance v0, Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler; */
v1 = this.mSysWorkThread;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;-><init>(Lcom/android/server/ScoutSystemMonitor;Landroid/os/Looper;)V */
this.mSysWorkerHandler = v0;
/* .line 139 */
} // :cond_0
v0 = this.mSysMonitorThread;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 140 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 141 */
/* new-instance v0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker; */
v1 = this.mSysMonitorThread;
(( android.os.HandlerThread ) v1 ).getThreadHandler ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getThreadHandler()Landroid/os/Handler;
final String v4 = "ScoutSystemMonitor thread"; // const-string v4, "ScoutSystemMonitor thread"
/* const-wide/16 v5, 0x2710 */
/* move-object v1, v0 */
/* move-object v2, p0 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;-><init>(Lcom/android/server/ScoutSystemMonitor;Landroid/os/Handler;Ljava/lang/String;J)V */
this.mScoutBinderMonitorChecker = v0;
/* .line 143 */
v1 = com.android.server.ScoutSystemMonitor.mScoutHandlerCheckers;
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 145 */
} // :cond_1
v0 = this.mSysServiceMonitorThread;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 146 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 147 */
/* new-instance v0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker; */
v1 = this.mSysServiceMonitorThread;
(( android.os.HandlerThread ) v1 ).getThreadHandler ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getThreadHandler()Landroid/os/Handler;
final String v4 = "ScoutSystemServiceMonitor thread"; // const-string v4, "ScoutSystemServiceMonitor thread"
/* const-wide/16 v5, 0x2710 */
/* move-object v1, v0 */
/* move-object v2, p0 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;-><init>(Lcom/android/server/ScoutSystemMonitor;Landroid/os/Handler;Ljava/lang/String;J)V */
this.mScoutMonitorChecker = v0;
/* .line 150 */
} // :cond_2
v0 = com.android.server.ScoutSystemMonitor.mScoutHandlerCheckers;
v1 = this.mScoutMonitorChecker;
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 151 */
/* new-instance v1, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker; */
/* new-instance v4, Landroid/os/Handler; */
android.os.Looper .getMainLooper ( );
/* invoke-direct {v4, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
final String v5 = "main thread"; // const-string v5, "main thread"
/* const-wide/16 v6, 0x2710 */
/* move-object v2, v1 */
/* move-object v3, p0 */
/* invoke-direct/range {v2 ..v7}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;-><init>(Lcom/android/server/ScoutSystemMonitor;Landroid/os/Handler;Ljava/lang/String;J)V */
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 153 */
/* new-instance v1, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker; */
com.android.server.UiThread .getHandler ( );
/* const-string/jumbo v11, "ui thread" */
/* const-wide/16 v12, 0x2710 */
/* move-object v8, v1 */
/* move-object v9, p0 */
/* invoke-direct/range {v8 ..v13}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;-><init>(Lcom/android/server/ScoutSystemMonitor;Landroid/os/Handler;Ljava/lang/String;J)V */
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 155 */
/* new-instance v1, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker; */
com.android.server.IoThread .getHandler ( );
final String v5 = "i/o thread"; // const-string v5, "i/o thread"
/* const-wide/16 v6, 0x7530 */
/* move-object v2, v1 */
/* invoke-direct/range {v2 ..v7}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;-><init>(Lcom/android/server/ScoutSystemMonitor;Landroid/os/Handler;Ljava/lang/String;J)V */
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 157 */
/* new-instance v1, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker; */
com.android.server.DisplayThread .getHandler ( );
final String v11 = "display thread"; // const-string v11, "display thread"
/* move-object v8, v1 */
/* invoke-direct/range {v8 ..v13}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;-><init>(Lcom/android/server/ScoutSystemMonitor;Landroid/os/Handler;Ljava/lang/String;J)V */
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 159 */
/* new-instance v1, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker; */
com.android.server.AnimationThread .getHandler ( );
final String v5 = "animation thread"; // const-string v5, "animation thread"
/* const-wide/16 v6, 0x2710 */
/* move-object v2, v1 */
/* invoke-direct/range {v2 ..v7}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;-><init>(Lcom/android/server/ScoutSystemMonitor;Landroid/os/Handler;Ljava/lang/String;J)V */
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 161 */
/* new-instance v1, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker; */
com.android.server.wm.SurfaceAnimationThread .getHandler ( );
/* const-string/jumbo v11, "surface animation thread" */
/* move-object v8, v1 */
/* invoke-direct/range {v8 ..v13}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;-><init>(Lcom/android/server/ScoutSystemMonitor;Landroid/os/Handler;Ljava/lang/String;J)V */
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 163 */
int v0 = 1; // const/4 v0, 0x1
(( com.android.server.ScoutSystemMonitor ) p0 ).updateScreenState ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/ScoutSystemMonitor;->updateScreenState(Z)V
/* .line 164 */
com.android.server.ScoutHelper .copyRamoopsFileToMqs ( );
/* .line 165 */
com.miui.server.stability.ScoutLibraryTestManager .getInstance ( );
(( com.miui.server.stability.ScoutLibraryTestManager ) v0 ).init ( ); // invoke-virtual {v0}, Lcom/miui/server/stability/ScoutLibraryTestManager;->init()V
/* .line 166 */
return;
} // .end method
public void pauseScoutWatchingCurrentThread ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .line 875 */
/* sget-boolean v0, Lcom/android/server/ScoutHelper;->ENABLED_SCOUT:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 876 */
} // :cond_0
/* monitor-enter p0 */
/* .line 877 */
try { // :try_start_0
v0 = com.android.server.ScoutSystemMonitor.mScoutHandlerCheckers;
(( java.util.ArrayList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_2
/* check-cast v1, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker; */
/* .line 878 */
/* .local v1, "hc":Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker; */
java.lang.Thread .currentThread ( );
(( com.android.server.ScoutSystemMonitor$ScoutHandlerChecker ) v1 ).getThread ( ); // invoke-virtual {v1}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->getThread()Ljava/lang/Thread;
v2 = (( java.lang.Object ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 879 */
(( com.android.server.ScoutSystemMonitor$ScoutHandlerChecker ) v1 ).pauseLocked ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->pauseLocked(Ljava/lang/String;)V
/* .line 881 */
} // .end local v1 # "hc":Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;
} // :cond_1
/* .line 882 */
} // :cond_2
/* monitor-exit p0 */
/* .line 883 */
return;
/* .line 882 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
public void reportRebootNullEventtoMqs ( java.lang.String p0, Integer p1, java.lang.String p2, java.lang.String p3 ) {
/* .locals 5 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .param p2, "processId" # I */
/* .param p3, "triggerWay" # Ljava/lang/String; */
/* .param p4, "intentName" # Ljava/lang/String; */
/* .line 243 */
v0 = (( com.android.server.ScoutSystemMonitor ) p0 ).shouldRebootReasonCheckNull ( ); // invoke-virtual {p0}, Lcom/android/server/ScoutSystemMonitor;->shouldRebootReasonCheckNull()Z
if ( v0 != null) { // if-eqz v0, :cond_2
int v0 = 0; // const/4 v0, 0x0
final String v1 = "debug.record.rebootnull"; // const-string v1, "debug.record.rebootnull"
v0 = android.os.SystemProperties .getBoolean ( v1,v0 );
/* if-nez v0, :cond_2 */
/* .line 244 */
int v0 = 0; // const/4 v0, 0x0
/* .line 245 */
/* .local v0, "details":Ljava/lang/String; */
final String v2 = "intent"; // const-string v2, "intent"
v2 = (( java.lang.String ) p3 ).contains ( v2 ); // invoke-virtual {p3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 246 */
final String v2 = "Shutdown intent checkpoint recorded intent=%s from package=%s"; // const-string v2, "Shutdown intent checkpoint recorded intent=%s from package=%s"
/* filled-new-array {p4, p1}, [Ljava/lang/Object; */
java.lang.String .format ( v2,v3 );
/* .line 247 */
/* const-string/jumbo v2, "true" */
android.os.SystemProperties .set ( v1,v2 );
/* .line 248 */
} // :cond_0
final String v1 = "binder"; // const-string v1, "binder"
v1 = (( java.lang.String ) p3 ).contains ( v1 ); // invoke-virtual {p3, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 249 */
com.android.server.am.ProcessUtils .getProcessNameByPid ( p2 );
/* .line 250 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Binder shutdown checkpoint recorded with package="; // const-string v2, "Binder shutdown checkpoint recorded with package="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 252 */
} // :cond_1
} // :goto_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " by "; // const-string v2, " by "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 253 */
/* .local v1, "caller":Ljava/lang/String; */
/* new-instance v2, Lmiui/mqsas/sdk/event/RebootNullEvent; */
/* invoke-direct {v2}, Lmiui/mqsas/sdk/event/RebootNullEvent;-><init>()V */
/* .line 254 */
/* .local v2, "event":Lmiui/mqsas/sdk/event/RebootNullEvent; */
(( miui.mqsas.sdk.event.RebootNullEvent ) v2 ).setProcessName ( p1 ); // invoke-virtual {v2, p1}, Lmiui/mqsas/sdk/event/RebootNullEvent;->setProcessName(Ljava/lang/String;)V
/* .line 255 */
(( miui.mqsas.sdk.event.RebootNullEvent ) v2 ).setPackageName ( p1 ); // invoke-virtual {v2, p1}, Lmiui/mqsas/sdk/event/RebootNullEvent;->setPackageName(Ljava/lang/String;)V
/* .line 256 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
(( miui.mqsas.sdk.event.RebootNullEvent ) v2 ).setTimeStamp ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lmiui/mqsas/sdk/event/RebootNullEvent;->setTimeStamp(J)V
/* .line 257 */
(( miui.mqsas.sdk.event.RebootNullEvent ) v2 ).setCaller ( v1 ); // invoke-virtual {v2, v1}, Lmiui/mqsas/sdk/event/RebootNullEvent;->setCaller(Ljava/lang/String;)V
/* .line 258 */
final String v3 = "reboot or shutdown with null reason"; // const-string v3, "reboot or shutdown with null reason"
(( miui.mqsas.sdk.event.RebootNullEvent ) v2 ).setSummary ( v3 ); // invoke-virtual {v2, v3}, Lmiui/mqsas/sdk/event/RebootNullEvent;->setSummary(Ljava/lang/String;)V
/* .line 259 */
(( miui.mqsas.sdk.event.RebootNullEvent ) v2 ).setDetails ( v0 ); // invoke-virtual {v2, v0}, Lmiui/mqsas/sdk/event/RebootNullEvent;->setDetails(Ljava/lang/String;)V
/* .line 260 */
miui.mqsas.sdk.MQSEventManagerDelegate .getInstance ( );
(( miui.mqsas.sdk.MQSEventManagerDelegate ) v3 ).reportRebootNullEvent ( v2 ); // invoke-virtual {v3, v2}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportRebootNullEvent(Lmiui/mqsas/sdk/event/RebootNullEvent;)V
/* .line 262 */
} // .end local v0 # "details":Ljava/lang/String;
} // .end local v1 # "caller":Ljava/lang/String;
} // .end local v2 # "event":Lmiui/mqsas/sdk/event/RebootNullEvent;
} // :cond_2
return;
} // .end method
public void resetClipProp ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 926 */
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_0 */
/* .line 927 */
final String v0 = "com.milink.service"; // const-string v0, "com.milink.service"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 928 */
final String v0 = "persist.sys.debug.app.clipdata"; // const-string v0, "persist.sys.debug.app.clipdata"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .set ( v0,v1 );
/* .line 930 */
} // :cond_0
return;
} // .end method
public void resumeScoutWatchingCurrentThread ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .line 887 */
/* sget-boolean v0, Lcom/android/server/ScoutHelper;->ENABLED_SCOUT:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 888 */
} // :cond_0
/* monitor-enter p0 */
/* .line 889 */
try { // :try_start_0
v0 = com.android.server.ScoutSystemMonitor.mScoutHandlerCheckers;
(( java.util.ArrayList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_2
/* check-cast v1, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker; */
/* .line 890 */
/* .local v1, "hc":Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker; */
java.lang.Thread .currentThread ( );
(( com.android.server.ScoutSystemMonitor$ScoutHandlerChecker ) v1 ).getThread ( ); // invoke-virtual {v1}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->getThread()Ljava/lang/Thread;
v2 = (( java.lang.Object ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 891 */
(( com.android.server.ScoutSystemMonitor$ScoutHandlerChecker ) v1 ).resumeLocked ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->resumeLocked(Ljava/lang/String;)V
/* .line 893 */
} // .end local v1 # "hc":Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;
} // :cond_1
/* .line 894 */
} // :cond_2
/* monitor-exit p0 */
/* .line 895 */
return;
/* .line 894 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
public void scoutSystemCheckBinderCallChain ( java.util.ArrayList p0, java.util.ArrayList p1, com.android.server.ScoutWatchdogInfo p2 ) {
/* .locals 8 */
/* .param p3, "watchdoginfo" # Lcom/android/server/ScoutWatchdogInfo; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;", */
/* "Lcom/android/server/ScoutWatchdogInfo;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 186 */
/* .local p1, "pids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* .local p2, "nativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* new-instance v0, Ljava/util/ArrayList; */
int v1 = 5; // const/4 v1, 0x5
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V */
/* .line 187 */
/* .local v0, "scoutJavaPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V */
/* move-object v1, v2 */
/* .line 188 */
/* .local v1, "scoutNativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* new-instance v2, Lcom/android/server/ScoutHelper$ScoutBinderInfo; */
/* .line 189 */
v3 = android.os.Process .myPid ( );
int v4 = 0; // const/4 v4, 0x0
final String v5 = "MIUIScout Watchdog"; // const-string v5, "MIUIScout Watchdog"
/* invoke-direct {v2, v3, v4, v5}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;-><init>(IILjava/lang/String;)V */
/* .line 191 */
/* .local v2, "scoutBinderInfo":Lcom/android/server/ScoutHelper$ScoutBinderInfo; */
v3 = android.os.Process .myPid ( );
java.lang.Integer .valueOf ( v3 );
(( java.util.ArrayList ) v0 ).add ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 192 */
v3 = android.os.Process .myPid ( );
com.android.server.ScoutHelper .checkBinderCallPidList ( v3,v2,v0,v1 );
/* .line 194 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( com.android.server.ScoutHelper$ScoutBinderInfo ) v2 ).getBinderTransInfo ( ); // invoke-virtual {v2}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->getBinderTransInfo()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "\n"; // const-string v4, "\n"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 195 */
(( com.android.server.ScoutHelper$ScoutBinderInfo ) v2 ).getProcInfo ( ); // invoke-virtual {v2}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->getProcInfo()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 194 */
(( com.android.server.ScoutWatchdogInfo ) p3 ).setBinderTransInfo ( v3 ); // invoke-virtual {p3, v3}, Lcom/android/server/ScoutWatchdogInfo;->setBinderTransInfo(Ljava/lang/String;)V
/* .line 196 */
v3 = (( com.android.server.ScoutHelper$ScoutBinderInfo ) v2 ).getDThreadState ( ); // invoke-virtual {v2}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->getDThreadState()Z
(( com.android.server.ScoutWatchdogInfo ) p3 ).setDThreadState ( v3 ); // invoke-virtual {p3, v3}, Lcom/android/server/ScoutWatchdogInfo;->setDThreadState(Z)V
/* .line 197 */
v3 = (( com.android.server.ScoutHelper$ScoutBinderInfo ) v2 ).getMonkeyPid ( ); // invoke-virtual {v2}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->getMonkeyPid()I
(( com.android.server.ScoutWatchdogInfo ) p3 ).setMonkeyPid ( v3 ); // invoke-virtual {p3, v3}, Lcom/android/server/ScoutWatchdogInfo;->setMonkeyPid(I)V
/* .line 199 */
v3 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
final String v4 = "Dump Trace: add java proc "; // const-string v4, "Dump Trace: add java proc "
final String v5 = "ScoutSystemMonitor"; // const-string v5, "ScoutSystemMonitor"
/* if-lez v3, :cond_1 */
/* .line 200 */
(( java.util.ArrayList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v6 = } // :goto_0
if ( v6 != null) { // if-eqz v6, :cond_1
/* check-cast v6, Ljava/lang/Integer; */
v6 = (( java.lang.Integer ) v6 ).intValue ( ); // invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I
/* .line 201 */
/* .local v6, "javaPid":I */
java.lang.Integer .valueOf ( v6 );
v7 = (( java.util.ArrayList ) p1 ).contains ( v7 ); // invoke-virtual {p1, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v7, :cond_0 */
/* .line 202 */
java.lang.Integer .valueOf ( v6 );
(( java.util.ArrayList ) p1 ).add ( v7 ); // invoke-virtual {p1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 203 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v7 );
/* .line 205 */
} // .end local v6 # "javaPid":I
} // :cond_0
/* .line 208 */
} // :cond_1
v3 = (( java.util.ArrayList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
/* if-lez v3, :cond_3 */
/* .line 209 */
(( java.util.ArrayList ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v6 = } // :goto_1
if ( v6 != null) { // if-eqz v6, :cond_3
/* check-cast v6, Ljava/lang/Integer; */
v6 = (( java.lang.Integer ) v6 ).intValue ( ); // invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I
/* .line 210 */
/* .local v6, "nativePid":I */
java.lang.Integer .valueOf ( v6 );
v7 = (( java.util.ArrayList ) p2 ).contains ( v7 ); // invoke-virtual {p2, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v7, :cond_2 */
/* .line 211 */
java.lang.Integer .valueOf ( v6 );
(( java.util.ArrayList ) p2 ).add ( v7 ); // invoke-virtual {p2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 212 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v7 );
/* .line 214 */
} // .end local v6 # "nativePid":I
} // :cond_2
/* .line 216 */
} // :cond_3
return;
} // .end method
public Boolean scoutSystemMonitorEnable ( ) {
/* .locals 1 */
/* .line 220 */
/* sget-boolean v0, Lcom/android/server/ScoutHelper;->ENABLED_SCOUT:Z */
} // .end method
public void scoutSystemMonitorInit ( com.android.server.Watchdog$Monitor p0, java.lang.Object p1 ) {
/* .locals 0 */
/* .param p1, "monitor" # Lcom/android/server/Watchdog$Monitor; */
/* .param p2, "mLock" # Ljava/lang/Object; */
/* .line 225 */
(( com.android.server.ScoutSystemMonitor ) p0 ).init ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/ScoutSystemMonitor;->init(Ljava/lang/Object;)V
/* .line 226 */
/* invoke-direct {p0, p1}, Lcom/android/server/ScoutSystemMonitor;->addScoutBinderMonitor(Lcom/android/server/Watchdog$Monitor;)V */
/* .line 227 */
return;
} // .end method
public void scoutSystemMonitorInitContext ( android.content.Context p0, com.android.server.am.ActivityManagerService p1 ) {
/* .locals 0 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "activity" # Lcom/android/server/am/ActivityManagerService; */
/* .line 236 */
this.mContext = p1;
/* .line 237 */
this.mService = p2;
/* .line 238 */
/* invoke-direct {p0}, Lcom/android/server/ScoutSystemMonitor;->registerScreenStateReceiver()V */
/* .line 239 */
return;
} // .end method
public void scoutSystemMonitorWork ( Long p0, Integer p1, Boolean p2, com.android.server.ScoutWatchdogInfo$ScoutId p3 ) {
/* .locals 0 */
/* .param p1, "timeout" # J */
/* .param p3, "count" # I */
/* .param p4, "waitedHalf" # Z */
/* .param p5, "scoutId" # Lcom/android/server/ScoutWatchdogInfo$ScoutId; */
/* .line 231 */
/* invoke-direct/range {p0 ..p5}, Lcom/android/server/ScoutSystemMonitor;->runSystemMonitor(JIZLcom/android/server/ScoutWatchdogInfo$ScoutId;)V */
/* .line 232 */
return;
} // .end method
public void setWorkMessage ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "msgId" # I */
/* .line 508 */
v0 = this.mSysWorkerHandler;
/* if-nez v0, :cond_0 */
return;
/* .line 510 */
} // :cond_0
/* packed-switch p1, :pswitch_data_0 */
/* .line 521 */
return;
/* .line 518 */
/* :pswitch_0 */
v0 = this.mSysWorkerHandler;
/* const/16 v1, 0xc */
(( com.android.server.ScoutSystemMonitor$SystemWorkerHandler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;->obtainMessage(I)Landroid/os/Message;
/* .line 519 */
/* .local v0, "msg":Landroid/os/Message; */
/* .line 515 */
} // .end local v0 # "msg":Landroid/os/Message;
/* :pswitch_1 */
v0 = this.mSysWorkerHandler;
/* const/16 v1, 0xb */
(( com.android.server.ScoutSystemMonitor$SystemWorkerHandler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;->obtainMessage(I)Landroid/os/Message;
/* .line 516 */
/* .restart local v0 # "msg":Landroid/os/Message; */
/* .line 512 */
} // .end local v0 # "msg":Landroid/os/Message;
/* :pswitch_2 */
v0 = this.mSysWorkerHandler;
/* const/16 v1, 0xa */
(( com.android.server.ScoutSystemMonitor$SystemWorkerHandler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;->obtainMessage(I)Landroid/os/Message;
/* .line 513 */
/* .restart local v0 # "msg":Landroid/os/Message; */
/* nop */
/* .line 523 */
} // :goto_0
v1 = this.mSysWorkerHandler;
(( com.android.server.ScoutSystemMonitor$SystemWorkerHandler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/ScoutSystemMonitor$SystemWorkerHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 524 */
return;
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public Boolean shouldRebootReasonCheckNull ( ) {
/* .locals 2 */
/* .line 265 */
final String v0 = "persist.sys.stability.rebootreason_check"; // const-string v0, "persist.sys.stability.rebootreason_check"
int v1 = 1; // const/4 v1, 0x1
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
} // .end method
public Boolean skipClipDataAppAnr ( Integer p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "pid" # I */
/* .param p2, "uid" # I */
/* .line 916 */
final String v0 = "persist.sys.debug.app.clipdata"; // const-string v0, "persist.sys.debug.app.clipdata"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
/* .line 917 */
/* .local v0, "clipProcess":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 918 */
int v1 = 0; // const/4 v1, 0x0
/* .line 920 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = "_"; // const-string v2, "_"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 921 */
/* .local v1, "currentAnrApp":Ljava/lang/String; */
v2 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
} // .end method
public void updateScreenState ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "screenOn" # Z */
/* .line 898 */
/* sget-boolean v0, Lcom/android/server/ScoutHelper;->ENABLED_SCOUT:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* sget-boolean v0, Lmiui/mqsas/scout/ScoutUtils;->REBOOT_COREDUMP:Z */
/* if-nez v0, :cond_3 */
/* sget-boolean v0, Lmiui/mqsas/scout/ScoutUtils;->MTBF_MIUI_TEST:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 901 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
v0 = this.mUiScoutStateMachine;
/* if-nez v0, :cond_1 */
/* .line 902 */
/* nop */
/* .line 903 */
com.android.server.UiThread .get ( );
int v1 = 1; // const/4 v1, 0x1
java.lang.Boolean .valueOf ( v1 );
/* .line 902 */
final String v2 = "UiThread"; // const-string v2, "UiThread"
android.app.AppScoutStateMachine .CreateAppScoutStateMachine ( v0,v2,v1 );
this.mUiScoutStateMachine = v0;
/* .line 904 */
} // :cond_1
/* if-nez p1, :cond_2 */
v0 = this.mUiScoutStateMachine;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 905 */
(( android.app.AppScoutStateMachine ) v0 ).quit ( ); // invoke-virtual {v0}, Landroid/app/AppScoutStateMachine;->quit()V
/* .line 906 */
int v0 = 0; // const/4 v0, 0x0
this.mUiScoutStateMachine = v0;
/* .line 908 */
} // :cond_2
} // :goto_0
return;
/* .line 899 */
} // :cond_3
} // :goto_1
return;
} // .end method
