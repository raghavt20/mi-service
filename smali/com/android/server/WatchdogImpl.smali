.class public Lcom/android/server/WatchdogImpl;
.super Lcom/android/server/WatchdogStub;
.source "WatchdogImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.WatchdogStub$$"
.end annotation


# static fields
.field private static final APP_ID:Ljava/lang/String; = "APP_ID"

.field private static final CHECK_LAYER_TIMEOUT:J = 0x249f0L

.field public static final DEBUG:Z = false

.field private static final EMPTY_BINDER:Ljava/lang/String; = "Here are no Binder-related exception messages available."

.field private static final EMPTY_MESSAGE:Ljava/lang/String; = ""

.field private static final EVENT_NAME:Ljava/lang/String; = "EVENT_NAME"

.field private static final EXTRA_APP_ID:Ljava/lang/String; = "31000401706"

.field private static final EXTRA_EVENT_NAME:Ljava/lang/String; = "native_hang"

.field private static final EXTRA_PACKAGE_NAME:Ljava/lang/String; = "android"

.field private static final FLAG_NON_ANONYMOUS:I = 0x2

.field private static final FLAG_NOT_LIMITED_BY_USER_EXPERIENCE_PLAN:I = 0x1

.field private static final GB:J = 0x40000000L

.field private static final HEAP_MONITOR_TAG:Ljava/lang/String; = "HeapUsage Monitor"

.field private static final HEAP_MONITOR_THRESHOLD:I

.field private static final INTENT_ACTION_ONETRACK:Ljava/lang/String; = "onetrack.action.TRACK_EVENT"

.field private static final INTENT_PACKAGE_ONETRACK:Ljava/lang/String; = "com.miui.analytics"

.field private static final INVAILD_FORMAT_BINDER:Ljava/lang/String; = "regex match failed"

.field private static final KB:J = 0x400L

.field private static final MAX_TRACES:I = 0x3

.field private static final MB:J = 0x100000L

.field private static final NATIVE_HANG_ENABLE:Ljava/lang/String; = "persist.sys.stability.nativehang.enable"

.field private static final NATIVE_HANG_PROCESS_NAME:Ljava/lang/String; = "sys.stability.nativehang.processname"

.field private static final NATIVE_HANG_THRESHOLD:I = 0x2

.field private static final OOM_CRASH_ON_WATCHDOG:Z

.field private static final PACKAGE:Ljava/lang/String; = "PACKAGE"

.field private static final PID:Ljava/lang/String; = "native_hang_pid"

.field private static final PROC_NAME:Ljava/lang/String; = "naitve_hang_packageName"

.field private static final PROP_PRESERVE_LAYER_LEAK_CRIME_SCENE:Ljava/lang/String; = "persist.sys.debug.preserve_scout_memory_leak_scene"

.field private static final REBOOT_MIUITEST:Ljava/lang/String; = "persist.reboot.miuitest"

.field private static final REGEX_PATTERN:Ljava/lang/String; = "\\bfrom((?:\\s+)?\\d+)\\(((?:\\s+)?\\S+)\\):.+to((?:\\s+)?\\d+)\\(((?:\\s+)?\\S+)\\):.+elapsed:((?:\\s+)?\\d*\\.?\\d*).\\b"

.field private static final SYSTEM_NATIVE_HANG_COUNT:Ljava/lang/String; = "sys.stability.nativehang.count"

.field private static final SYSTEM_SERVER:Ljava/lang/String; = "system_server"

.field private static final SYSTEM_SERVER_START_COUNT:Ljava/lang/String; = "sys.system_server.start_count"

.field private static final TAG:Ljava/lang/String; = "Watchdog"

.field private static final WATCHDOG_DIR:Ljava/lang/String; = "/data/miuilog/stability/scout/watchdog"

.field private static final WATCHDOG_THRESHOLD:I = 0x3c

.field private static final diableThreadList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static isHalfOom:Z

.field public static final nativeHangProcList:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mContext:Landroid/content/Context;

.field mPendingTime:I

.field mToPid:I

.field mToProcess:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 68
    nop

    .line 69
    const-string v0, "persist.sys.oom_crash_on_watchdog"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/WatchdogImpl;->OOM_CRASH_ON_WATCHDOG:Z

    .line 70
    nop

    .line 71
    const-string v0, "persist.sys.oom_crash_on_watchdog_size"

    const/16 v2, 0x1f4

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/server/WatchdogImpl;->HEAP_MONITOR_THRESHOLD:I

    .line 78
    sput-boolean v1, Lcom/android/server/WatchdogImpl;->isHalfOom:Z

    .line 80
    const-string v0, "PackageManager"

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/android/server/WatchdogImpl;->diableThreadList:Ljava/util/List;

    .line 108
    new-instance v0, Ljava/util/HashSet;

    const-string v1, "/system/bin/surfaceflinger"

    const-string v2, "/system/bin/keystore2/data/misc/keystore"

    filled-new-array {v1, v2}, [Ljava/lang/String;

    move-result-object v1

    .line 109
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/android/server/WatchdogImpl;->nativeHangProcList:Ljava/util/Set;

    .line 108
    return-void
.end method

.method constructor <init>()V
    .locals 1

    .line 114
    invoke-direct {p0}, Lcom/android/server/WatchdogStub;-><init>()V

    .line 449
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/WatchdogImpl;->mToProcess:Ljava/lang/String;

    .line 450
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/WatchdogImpl;->mPendingTime:I

    .line 451
    iput v0, p0, Lcom/android/server/WatchdogImpl;->mToPid:I

    .line 115
    invoke-static {}, Lmiui/mqsas/scout/ScoutUtils;->isLibraryTest()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    invoke-static {}, Lcom/android/server/WatchdogImpl;->scheduleLayerLeakCheck()V

    .line 117
    invoke-static {}, Landroid/view/SurfaceControlImpl;->enableRenovationForLibraryTests()V

    .line 119
    :cond_0
    return-void
.end method

.method private checkIsMiuiMtbf()Z
    .locals 2

    .line 433
    const-string v0, "persist.reboot.miuitest"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private checkLabTestStatus()Z
    .locals 1

    .line 437
    invoke-static {}, Lmiui/mqsas/scout/ScoutUtils;->isLibraryTest()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/server/WatchdogImpl;->checkIsMiuiMtbf()Z

    move-result v0

    if-nez v0, :cond_0

    .line 438
    const/4 v0, 0x0

    return v0

    .line 440
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method private checkNativeHang(Ljava/lang/String;)Z
    .locals 11
    .param p1, "binderTransInfo"    # Ljava/lang/String;

    .line 458
    invoke-direct {p0}, Lcom/android/server/WatchdogImpl;->checkNativeHangEnable()Z

    move-result v0

    const/4 v1, 0x0

    const-string v2, "MIUIScout Watchdog"

    if-nez v0, :cond_0

    .line 459
    const-string v0, "naitve hang feature disable."

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    return v1

    .line 463
    :cond_0
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "Here are no Binder-related exception messages available."

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 464
    const-string v0, "regex match failed"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto/16 :goto_1

    .line 469
    :cond_1
    :try_start_0
    const-string v0, "\\bfrom((?:\\s+)?\\d+)\\(((?:\\s+)?\\S+)\\):.+to((?:\\s+)?\\d+)\\(((?:\\s+)?\\S+)\\):.+elapsed:((?:\\s+)?\\d*\\.?\\d*).\\b"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 470
    .local v0, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 471
    .local v3, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-nez v4, :cond_2

    .line 472
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "nativeHang ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") regex match failed"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 473
    return v1

    .line 475
    :cond_2
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 476
    .local v5, "fromPid":I
    const/4 v6, 0x2

    invoke-virtual {v3, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 477
    .local v6, "fromProcess":Ljava/lang/String;
    const/4 v7, 0x3

    invoke-virtual {v3, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/android/server/WatchdogImpl;->mToPid:I

    .line 478
    const/4 v7, 0x4

    invoke-virtual {v3, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/android/server/WatchdogImpl;->mToProcess:Ljava/lang/String;

    .line 479
    const/4 v7, 0x5

    invoke-virtual {v3, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v7

    .line 480
    .local v7, "timeout":D
    invoke-static {v7, v8}, Ljava/lang/Math;->round(D)J

    move-result-wide v9

    long-to-int v9, v9

    iput v9, p0, Lcom/android/server/WatchdogImpl;->mPendingTime:I

    .line 482
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "fromPid="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", fromProcess="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", mToPid="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/android/server/WatchdogImpl;->mToPid:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", mToProcess="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/android/server/WatchdogImpl;->mToProcess:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", mPendingTime="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/android/server/WatchdogImpl;->mPendingTime:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v9

    if-ne v9, v5, :cond_4

    sget-object v9, Lcom/android/server/WatchdogImpl;->nativeHangProcList:Ljava/util/Set;

    iget-object v10, p0, Lcom/android/server/WatchdogImpl;->mToProcess:Ljava/lang/String;

    invoke-interface {v9, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    iget v9, p0, Lcom/android/server/WatchdogImpl;->mPendingTime:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v10, 0x3c

    if-ge v9, v10, :cond_3

    goto :goto_0

    .line 494
    .end local v0    # "pattern":Ljava/util/regex/Pattern;
    .end local v3    # "matcher":Ljava/util/regex/Matcher;
    .end local v5    # "fromPid":I
    .end local v6    # "fromProcess":Ljava/lang/String;
    .end local v7    # "timeout":D
    :cond_3
    nop

    .line 495
    const-string v0, "Occur native hang"

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    return v4

    .line 488
    .restart local v0    # "pattern":Ljava/util/regex/Pattern;
    .restart local v3    # "matcher":Ljava/util/regex/Matcher;
    .restart local v5    # "fromPid":I
    .restart local v6    # "fromProcess":Ljava/lang/String;
    .restart local v7    # "timeout":D
    :cond_4
    :goto_0
    return v1

    .line 491
    .end local v0    # "pattern":Ljava/util/regex/Pattern;
    .end local v3    # "matcher":Ljava/util/regex/Matcher;
    .end local v5    # "fromPid":I
    .end local v6    # "fromProcess":Ljava/lang/String;
    .end local v7    # "timeout":D
    :catch_0
    move-exception v0

    .line 492
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "checkNativeHang process Error: "

    invoke-static {v2, v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 493
    return v1

    .line 465
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_5
    :goto_1
    return v1
.end method

.method private checkNativeHangEnable()Z
    .locals 2

    .line 421
    const-string v0, "persist.sys.stability.nativehang.enable"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static getWatchdogDir()Ljava/io/File;
    .locals 3

    .line 291
    new-instance v0, Ljava/io/File;

    const-string v1, "/data/miuilog/stability/scout/watchdog"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 292
    .local v0, "tracesDir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-nez v1, :cond_0

    .line 293
    const/4 v1, 0x0

    return-object v1

    .line 295
    :cond_0
    const/16 v1, 0x1ed

    const/4 v2, -0x1

    invoke-static {v0, v1, v2, v2}, Landroid/os/FileUtils;->setPermissions(Ljava/io/File;III)I

    .line 296
    return-object v0
.end method

.method static synthetic lambda$reportEvent$1(Lmiui/mqsas/sdk/event/WatchdogEvent;)V
    .locals 1
    .param p0, "event"    # Lmiui/mqsas/sdk/event/WatchdogEvent;

    .line 359
    invoke-static {}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->getInstance()Lmiui/mqsas/sdk/MQSEventManagerDelegate;

    move-result-object v0

    invoke-virtual {v0, p0}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportWatchdogEvent(Lmiui/mqsas/sdk/event/WatchdogEvent;)Z

    return-void
.end method

.method static synthetic lambda$saveWatchdogTrace$0(Ljava/lang/String;Ljava/util/TreeSet;Ljava/io/File;)Z
    .locals 1
    .param p0, "prefix"    # Ljava/lang/String;
    .param p1, "existingTraces"    # Ljava/util/TreeSet;
    .param p2, "pathname"    # Ljava/io/File;

    .line 238
    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 239
    invoke-virtual {p1, p2}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 241
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public static prettySize(J)Ljava/lang/String;
    .locals 8
    .param p0, "byte_count"    # J

    .line 300
    const/4 v0, 0x4

    new-array v1, v0, [J

    fill-array-data v1, :array_0

    .line 307
    .local v1, "kUnitThresholds":[J
    new-array v0, v0, [J

    fill-array-data v0, :array_1

    .line 308
    .local v0, "kBytesPerUnit":[J
    const-string v2, "MB"

    const-string v3, "GB"

    const-string v4, "B"

    const-string v5, "KB"

    filled-new-array {v4, v5, v2, v3}, [Ljava/lang/String;

    move-result-object v2

    .line 309
    .local v2, "kUnitStrings":[Ljava/lang/String;
    const-string v3, ""

    .line 310
    .local v3, "negative_str":Ljava/lang/String;
    const-wide/16 v4, 0x0

    cmp-long v4, p0, v4

    if-gez v4, :cond_0

    .line 311
    const-string v3, "-"

    .line 312
    neg-long p0, p0

    .line 314
    :cond_0
    array-length v4, v1

    .line 315
    .local v4, "i":I
    :cond_1
    add-int/lit8 v4, v4, -0x1

    if-lez v4, :cond_2

    .line 316
    aget-wide v5, v1, v4

    cmp-long v5, p0, v5

    if-ltz v5, :cond_1

    .line 317
    nop

    .line 320
    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-wide v6, v0, v4

    div-long v6, p0, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v2, v4

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    :array_0
    .array-data 8
        0x0
        0x2800
        0xa00000
        0x280000000L
    .end array-data

    :array_1
    .array-data 8
        0x1
        0x400
        0x100000
        0x40000000
    .end array-data
.end method

.method private static reportEvent(ILjava/lang/String;Ljava/io/File;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p0, "type"    # I
    .param p1, "subject"    # Ljava/lang/String;
    .param p2, "trace"    # Ljava/io/File;
    .param p4, "mBinderInfo"    # Ljava/lang/String;
    .param p5, "mUuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/io/File;",
            "Ljava/util/List<",
            "Lcom/android/server/Watchdog$HandlerChecker;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 326
    .local p3, "handlerCheckers":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/Watchdog$HandlerChecker;>;"
    new-instance v0, Lmiui/mqsas/sdk/event/WatchdogEvent;

    invoke-direct {v0}, Lmiui/mqsas/sdk/event/WatchdogEvent;-><init>()V

    .line 327
    .local v0, "event":Lmiui/mqsas/sdk/event/WatchdogEvent;
    invoke-virtual {v0, p0}, Lmiui/mqsas/sdk/event/WatchdogEvent;->setType(I)V

    .line 328
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/WatchdogEvent;->setPid(I)V

    .line 329
    const-string/jumbo v1, "system_server"

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/WatchdogEvent;->setProcessName(Ljava/lang/String;)V

    .line 330
    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/WatchdogEvent;->setPackageName(Ljava/lang/String;)V

    .line 331
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lmiui/mqsas/sdk/event/WatchdogEvent;->setTimeStamp(J)V

    .line 332
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/WatchdogEvent;->setSystem(Z)V

    .line 333
    invoke-virtual {v0, p1}, Lmiui/mqsas/sdk/event/WatchdogEvent;->setSummary(Ljava/lang/String;)V

    .line 334
    invoke-virtual {v0, p1}, Lmiui/mqsas/sdk/event/WatchdogEvent;->setDetails(Ljava/lang/String;)V

    .line 335
    invoke-virtual {v0, p4}, Lmiui/mqsas/sdk/event/WatchdogEvent;->setBinderTransactionInfo(Ljava/lang/String;)V

    .line 336
    invoke-virtual {v0, p5}, Lmiui/mqsas/sdk/event/WatchdogEvent;->setUuid(Ljava/lang/String;)V

    .line 337
    const-string v2, "persist.sys.zygote.start_pid"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmiui/mqsas/sdk/event/WatchdogEvent;->setZygotePid(Ljava/lang/String;)V

    .line 338
    if-eqz p2, :cond_0

    .line 339
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmiui/mqsas/sdk/event/WatchdogEvent;->setLogName(Ljava/lang/String;)V

    .line 341
    :cond_0
    if-eqz p3, :cond_3

    .line 342
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 343
    .local v2, "details":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 344
    invoke-interface {p3, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/Watchdog$HandlerChecker;

    invoke-virtual {v4}, Lcom/android/server/Watchdog$HandlerChecker;->getThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    .line 345
    .local v4, "st":[Ljava/lang/StackTraceElement;
    array-length v5, v4

    const/4 v6, 0x0

    :goto_1
    if-ge v6, v5, :cond_1

    aget-object v7, v4, v6

    .line 346
    .local v7, "element":Ljava/lang/StackTraceElement;
    const-string v8, "    at "

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 345
    .end local v7    # "element":Ljava/lang/StackTraceElement;
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 348
    :cond_1
    const-string v5, "\n\n"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 343
    .end local v4    # "st":[Ljava/lang/StackTraceElement;
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 350
    .end local v3    # "i":I
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lmiui/mqsas/sdk/event/WatchdogEvent;->setDetails(Ljava/lang/String;)V

    .line 354
    .end local v2    # "details":Ljava/lang/StringBuilder;
    :cond_3
    const/4 v2, 0x2

    if-ne v2, p0, :cond_4

    .line 355
    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/WatchdogEvent;->setEnsureReport(Z)V

    .line 357
    :cond_4
    invoke-static {}, Lmiui/mqsas/scout/ScoutUtils;->isLibraryTest()Z

    move-result v1

    if-nez v1, :cond_5

    .line 358
    new-instance v1, Lcom/android/server/WatchdogImpl$$ExternalSyntheticLambda1;

    invoke-direct {v1, v0}, Lcom/android/server/WatchdogImpl$$ExternalSyntheticLambda1;-><init>(Lmiui/mqsas/sdk/event/WatchdogEvent;)V

    invoke-static {v1}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->runtimeWithTimeout(Ljava/lang/Runnable;)Z

    goto :goto_2

    .line 361
    :cond_5
    invoke-static {}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->getInstance()Lmiui/mqsas/sdk/MQSEventManagerDelegate;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportWatchdogEvent(Lmiui/mqsas/sdk/event/WatchdogEvent;)Z

    .line 363
    :goto_2
    return-void
.end method

.method private reportNativeHang(ILjava/lang/String;)V
    .locals 3
    .param p1, "pid"    # I
    .param p2, "packageName"    # Ljava/lang/String;

    .line 397
    iget-object v0, p0, Lcom/android/server/WatchdogImpl;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 398
    return-void

    .line 401
    :cond_0
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "onetrack.action.TRACK_EVENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 402
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.miui.analytics"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 403
    const-string v1, "APP_ID"

    const-string v2, "31000401706"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 404
    const-string v1, "EVENT_NAME"

    const-string v2, "native_hang"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 405
    const-string v1, "PACKAGE"

    const-string v2, "android"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 406
    const-string v1, "native_hang_pid"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 407
    const-string v1, "naitve_hang_packageName"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 408
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 409
    iget-object v1, p0, Lcom/android/server/WatchdogImpl;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 417
    nop

    .end local v0    # "intent":Landroid/content/Intent;
    goto :goto_0

    .line 414
    :catch_0
    move-exception v0

    .line 415
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 416
    const-string v1, "MIUIScout Watchdog"

    const-string v2, "Upload onetrack exception!"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 418
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private static saveWatchdogTrace(ZLjava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;
    .locals 16
    .param p0, "halfWatchdog"    # Z
    .param p1, "subject"    # Ljava/lang/String;
    .param p2, "binderTransInfo"    # Ljava/lang/String;
    .param p3, "file"    # Ljava/io/File;

    .line 229
    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-static {}, Lcom/android/server/WatchdogImpl;->getWatchdogDir()Ljava/io/File;

    move-result-object v3

    .line 230
    .local v3, "tracesDir":Ljava/io/File;
    const-string v4, "Watchdog"

    if-nez v3, :cond_0

    .line 231
    const-string v0, "Failed to get watchdog dir"

    invoke-static {v4, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    return-object v2

    .line 234
    :cond_0
    if-eqz p0, :cond_1

    const-string v0, "pre_watchdog_pid_"

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "watchdog_pid_"

    :goto_0
    move-object v5, v0

    .line 236
    .local v5, "prefix":Ljava/lang/String;
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    move-object v6, v0

    .line 237
    .local v6, "existingTraces":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/io/File;>;"
    new-instance v0, Lcom/android/server/WatchdogImpl$$ExternalSyntheticLambda0;

    invoke-direct {v0, v5, v6}, Lcom/android/server/WatchdogImpl$$ExternalSyntheticLambda0;-><init>(Ljava/lang/String;Ljava/util/TreeSet;)V

    invoke-virtual {v3, v0}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    .line 243
    invoke-virtual {v6}, Ljava/util/TreeSet;->size()I

    move-result v0

    const/4 v7, 0x3

    if-lt v0, v7, :cond_3

    .line 244
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    const/4 v7, 0x2

    if-ge v0, v7, :cond_2

    .line 245
    invoke-virtual {v6}, Ljava/util/TreeSet;->pollLast()Ljava/lang/Object;

    .line 244
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 247
    .end local v0    # "i":I
    :cond_2
    invoke-virtual {v6}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/io/File;

    .line 248
    .local v7, "trace":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    .line 249
    .end local v7    # "trace":Ljava/io/File;
    goto :goto_2

    .line 251
    :cond_3
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v7, "yyyy_MM_dd_HH_mm_ss"

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v7, v8}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    move-object v7, v0

    .line 252
    .local v7, "dateFormat":Ljava/text/SimpleDateFormat;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v8

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, "_"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v8, Ljava/util/Date;

    invoke-direct {v8}, Ljava/util/Date;-><init>()V

    invoke-virtual {v7, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 253
    .local v8, "fileName":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v9, v0

    .line 254
    .local v9, "watchdogFile":Ljava/io/File;
    const/4 v10, 0x0

    .line 255
    .local v10, "fout":Ljava/io/FileOutputStream;
    const/4 v11, 0x0

    .line 257
    .local v11, "fin":Ljava/io/FileInputStream;
    if-eqz v2, :cond_4

    :try_start_0
    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->length()J

    move-result-wide v12

    const-wide/16 v14, 0x0

    cmp-long v0, v12, v14

    if-lez v0, :cond_4

    .line 258
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    move-object v11, v0

    .line 260
    :cond_4
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    move-object v10, v0

    .line 263
    const/16 v0, 0xa

    if-nez v11, :cond_5

    .line 264
    const-string v12, "Subject"

    sget-object v13, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v12, v13}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/io/FileOutputStream;->write([B)V

    .line 265
    invoke-virtual {v10, v0}, Ljava/io/FileOutputStream;->write(I)V

    .line 266
    invoke-virtual {v10, v0}, Ljava/io/FileOutputStream;->write(I)V

    .line 267
    if-eqz v1, :cond_6

    .line 268
    sget-object v12, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v1, v12}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/io/FileOutputStream;->write([B)V

    .line 269
    invoke-virtual {v10, v0}, Ljava/io/FileOutputStream;->write(I)V

    .line 270
    invoke-virtual {v10, v0}, Ljava/io/FileOutputStream;->write(I)V

    goto :goto_3

    .line 273
    :cond_5
    const/4 v12, 0x0

    invoke-static {v11, v10, v12, v12, v12}, Landroid/os/FileUtils;->copyInternalUserspace(Ljava/io/InputStream;Ljava/io/OutputStream;Landroid/os/CancellationSignal;Ljava/util/concurrent/Executor;Landroid/os/FileUtils$ProgressListener;)J

    .line 275
    :cond_6
    :goto_3
    if-nez p0, :cond_7

    .line 276
    invoke-virtual {v10, v0}, Ljava/io/FileOutputStream;->write(I)V

    .line 277
    const-string v0, "------ ps info ------"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/io/FileOutputStream;->write([B)V

    .line 278
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/android/server/ScoutHelper;->getPsInfo(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 283
    :cond_7
    nop

    :goto_4
    invoke-static {v11}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 284
    invoke-static {v10}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 285
    goto :goto_5

    .line 283
    :catchall_0
    move-exception v0

    goto :goto_6

    .line 280
    :catch_0
    move-exception v0

    .line 281
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Failed to save watchdog trace: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v4, v12}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 283
    nop

    .end local v0    # "e":Ljava/lang/Exception;
    goto :goto_4

    .line 286
    :goto_5
    const/16 v0, 0x1a4

    const/4 v4, -0x1

    invoke-static {v9, v0, v4, v4}, Landroid/os/FileUtils;->setPermissions(Ljava/io/File;III)I

    .line 287
    return-object v9

    .line 283
    :goto_6
    invoke-static {v11}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 284
    invoke-static {v10}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 285
    throw v0
.end method

.method private static scheduleLayerLeakCheck()V
    .locals 4

    .line 122
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    .line 123
    .local v0, "h":Landroid/os/Handler;
    new-instance v1, Lcom/android/server/WatchdogImpl$1;

    invoke-direct {v1, v0}, Lcom/android/server/WatchdogImpl$1;-><init>(Landroid/os/Handler;)V

    const-wide/32 v2, 0x249f0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 136
    return-void
.end method

.method private setWatchdogCount(I)V
    .locals 2
    .param p1, "count"    # I

    .line 429
    const-string/jumbo v0, "sys.stability.nativehang.count"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    return-void
.end method


# virtual methods
.method public checkAndSaveStatus(ZLjava/lang/String;)Z
    .locals 7
    .param p1, "waitHalf"    # Z
    .param p2, "binderTransInfo"    # Ljava/lang/String;

    .line 501
    invoke-direct {p0}, Lcom/android/server/WatchdogImpl;->checkLabTestStatus()Z

    move-result v0

    const/4 v1, 0x0

    const-string v2, "MIUIScout Watchdog"

    if-nez v0, :cond_0

    .line 502
    const-string v0, "only checkNativeHang when MIUI MTBF& non-laboratory test scenarios"

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    return v1

    .line 507
    :cond_0
    if-eqz p1, :cond_1

    .line 508
    const-string v0, "only checkNativeHang when watchdog"

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 509
    return v1

    .line 512
    :cond_1
    invoke-direct {p0, p2}, Lcom/android/server/WatchdogImpl;->checkNativeHang(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 513
    const-string v0, "not native hang process"

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    return v1

    .line 517
    :cond_2
    invoke-virtual {p0}, Lcom/android/server/WatchdogImpl;->getWatchdogCount()I

    move-result v0

    .line 518
    .local v0, "oldCount":I
    const-string/jumbo v3, "sys.stability.nativehang.processname"

    const/4 v4, 0x1

    if-nez v0, :cond_3

    .line 519
    add-int/lit8 v1, v0, 0x1

    invoke-direct {p0, v1}, Lcom/android/server/WatchdogImpl;->setWatchdogCount(I)V

    .line 520
    iget-object v1, p0, Lcom/android/server/WatchdogImpl;->mToProcess:Ljava/lang/String;

    invoke-static {v3, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    return v4

    .line 524
    :cond_3
    if-ne v0, v4, :cond_4

    .line 525
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/WatchdogImpl;->mToProcess:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 526
    add-int/lit8 v1, v0, 0x1

    invoke-direct {p0, v1}, Lcom/android/server/WatchdogImpl;->setWatchdogCount(I)V

    .line 527
    iget v1, p0, Lcom/android/server/WatchdogImpl;->mToPid:I

    iget-object v3, p0, Lcom/android/server/WatchdogImpl;->mToProcess:Ljava/lang/String;

    invoke-direct {p0, v1, v3}, Lcom/android/server/WatchdogImpl;->reportNativeHang(ILjava/lang/String;)V

    .line 528
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Occur native hang, processName="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/WatchdogImpl;->mToProcess:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", mPendTime="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/android/server/WatchdogImpl;->mPendingTime:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "s."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 530
    const-string v1, "Native process hang reboot time larger than 2 time, reboot device!"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 531
    return v4

    .line 534
    :cond_4
    invoke-direct {p0, v1}, Lcom/android/server/WatchdogImpl;->setWatchdogCount(I)V

    .line 535
    const-string v2, "null"

    invoke-static {v3, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    return v1
.end method

.method public checkOOMState()V
    .locals 16

    .line 190
    const-string v1, "HeapUsage Monitor"

    sget-boolean v0, Lcom/android/server/WatchdogImpl;->OOM_CRASH_ON_WATCHDOG:Z

    if-nez v0, :cond_0

    .line 191
    return-void

    .line 195
    :cond_0
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    .line 196
    .local v0, "runtime":Ljava/lang/Runtime;
    invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v2

    .line 197
    .local v2, "dalvikMax":J
    invoke-virtual {v0}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v4

    .line 198
    .local v4, "dalvikFree":J
    sub-long v6, v2, v4

    .line 199
    .local v6, "allocHeapSpace":J
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "HeapAllocSize : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v6, v7}, Lcom/android/server/WatchdogImpl;->prettySize(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "; monitorSize : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget v9, Lcom/android/server/WatchdogImpl;->HEAP_MONITOR_THRESHOLD:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, "MB"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v1, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    int-to-long v10, v9

    const-wide/32 v12, 0x100000

    mul-long/2addr v10, v12

    cmp-long v8, v6, v10

    const/4 v10, 0x0

    if-ltz v8, :cond_3

    .line 202
    invoke-virtual {v0}, Ljava/lang/Runtime;->gc()V

    .line 203
    invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v14

    move-wide v2, v14

    .line 204
    invoke-virtual {v0}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v14

    move-wide v4, v14

    .line 205
    sub-long v6, v2, v4

    .line 206
    int-to-long v8, v9

    mul-long/2addr v8, v12

    cmp-long v8, v6, v8

    if-gez v8, :cond_1

    .line 207
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "After performing Gc, HeapAllocSize : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v6, v7}, Lcom/android/server/WatchdogImpl;->prettySize(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v1, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    sput-boolean v10, Lcom/android/server/WatchdogImpl;->isHalfOom:Z

    .line 209
    return-void

    .line 211
    :cond_1
    sget-boolean v8, Lcom/android/server/WatchdogImpl;->isHalfOom:Z

    if-nez v8, :cond_2

    .line 216
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Half Oom, Heap of System_Server has allocated "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v6, v7}, Lcom/android/server/WatchdogImpl;->prettySize(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v1, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    const/4 v8, 0x1

    sput-boolean v8, Lcom/android/server/WatchdogImpl;->isHalfOom:Z

    goto :goto_0

    .line 212
    :cond_2
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Heap of System_Server has allocated "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v6, v7}, Lcom/android/server/WatchdogImpl;->prettySize(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " , So trigger OutOfMemoryError crash"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 214
    .local v8, "msg":Ljava/lang/String;
    new-instance v9, Ljava/lang/OutOfMemoryError;

    invoke-direct {v9, v8}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    .end local p0    # "this":Lcom/android/server/WatchdogImpl;
    throw v9

    .line 220
    .end local v8    # "msg":Ljava/lang/String;
    .restart local p0    # "this":Lcom/android/server/WatchdogImpl;
    :cond_3
    sput-boolean v10, Lcom/android/server/WatchdogImpl;->isHalfOom:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 224
    .end local v0    # "runtime":Ljava/lang/Runtime;
    .end local v2    # "dalvikMax":J
    .end local v4    # "dalvikFree":J
    .end local v6    # "allocHeapSpace":J
    :goto_0
    goto :goto_1

    .line 222
    :catch_0
    move-exception v0

    .line 223
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "checkOOMState:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method

.method public getWatchdogCount()I
    .locals 2

    .line 425
    const-string/jumbo v0, "sys.stability.nativehang.count"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public init(Landroid/content/Context;Lcom/android/server/am/ActivityManagerService;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "activity"    # Lcom/android/server/am/ActivityManagerService;

    .line 393
    iput-object p1, p0, Lcom/android/server/WatchdogImpl;->mContext:Landroid/content/Context;

    .line 394
    return-void
.end method

.method public isNativeHang()Z
    .locals 2

    .line 541
    invoke-virtual {p0}, Lcom/android/server/WatchdogImpl;->getWatchdogCount()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public needRebootSystem()Z
    .locals 1

    .line 546
    invoke-virtual {p0}, Lcom/android/server/WatchdogImpl;->isNativeHang()Z

    move-result v0

    if-nez v0, :cond_0

    .line 547
    const/4 v0, 0x0

    return v0

    .line 550
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method onHalfWatchdog(Ljava/lang/String;Ljava/io/File;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "subject"    # Ljava/lang/String;
    .param p2, "trace"    # Ljava/io/File;
    .param p4, "binderTransInfo"    # Ljava/lang/String;
    .param p5, "mUuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/io/File;",
            "Ljava/util/List<",
            "Lcom/android/server/Watchdog$HandlerChecker;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 141
    .local p3, "blockedCheckers":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/Watchdog$HandlerChecker;>;"
    invoke-static {}, Landroid/os/Debug;->isDebuggerConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    return-void

    .line 147
    :cond_0
    const/4 v0, 0x1

    if-eqz p3, :cond_1

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v0, :cond_1

    .line 148
    const/4 v1, 0x0

    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/Watchdog$HandlerChecker;

    invoke-virtual {v1}, Lcom/android/server/Watchdog$HandlerChecker;->getName()Ljava/lang/String;

    move-result-object v1

    .line 149
    .local v1, "name":Ljava/lang/String;
    sget-object v2, Lcom/android/server/WatchdogImpl;->diableThreadList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    return-void

    .line 151
    .end local v1    # "name":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/android/server/ResourcePressureUtil;->currentPsiState()Ljava/lang/String;

    move-result-object v1

    .line 152
    .local v1, "psi":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Enter HALF_WATCHDOG \n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MIUIScout Watchdog"

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    invoke-static {}, Lcom/android/server/ScoutSystemMonitor;->getInstance()Lcom/android/server/ScoutSystemMonitor;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/android/server/ScoutSystemMonitor;->setWorkMessage(I)V

    .line 154
    invoke-static {v0, p1, p4, p2}, Lcom/android/server/WatchdogImpl;->saveWatchdogTrace(ZLjava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object p2

    .line 155
    const/16 v4, 0x180

    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    move-object v8, p4

    move-object v9, p5

    invoke-static/range {v4 .. v9}, Lcom/android/server/WatchdogImpl;->reportEvent(ILjava/lang/String;Ljava/io/File;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    return-void
.end method

.method onWatchdog(Ljava/lang/String;Ljava/io/File;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "subject"    # Ljava/lang/String;
    .param p2, "trace"    # Ljava/io/File;
    .param p4, "binderTransInfo"    # Ljava/lang/String;
    .param p5, "mUuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/io/File;",
            "Ljava/util/List<",
            "Lcom/android/server/Watchdog$HandlerChecker;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 161
    .local p3, "blockedCheckers":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/Watchdog$HandlerChecker;>;"
    invoke-static {}, Landroid/os/Debug;->isDebuggerConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    return-void

    .line 164
    :cond_0
    invoke-static {}, Lcom/android/server/ResourcePressureUtil;->currentPsiState()Ljava/lang/String;

    move-result-object v0

    .line 165
    .local v0, "psi":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Enter WATCHDOG \n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MIUIScout Watchdog"

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    invoke-static {}, Lcom/android/server/am/AppProfilerStub;->getInstance()Lcom/android/server/am/AppProfilerStub;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/android/server/am/AppProfilerStub;->checkMemoryPsi(Z)V

    .line 167
    if-eqz p2, :cond_1

    const/4 v1, 0x2

    goto :goto_0

    :cond_1
    const/16 v1, 0x181

    :goto_0
    move v2, v1

    .line 168
    .local v2, "eventType":I
    const/4 v1, 0x0

    invoke-static {v1, p1, p4, p2}, Lcom/android/server/WatchdogImpl;->saveWatchdogTrace(ZLjava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object p2

    .line 169
    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-static/range {v2 .. v7}, Lcom/android/server/WatchdogImpl;->reportEvent(ILjava/lang/String;Ljava/io/File;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    return-void
.end method

.method public retryDumpMyStacktrace(Ljava/lang/String;JJ)Z
    .locals 15
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "timeStartMs"    # J
    .param p4, "totalTimeoutMs"    # J

    .line 367
    const/16 v0, 0x7d0

    .line 368
    .local v0, "QUICK_FAILURE_MILLS":I
    const/16 v1, 0x1388

    .line 369
    .local v1, "MIN_TIMEOUT_MILLIS":I
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long v2, v2, p2

    .line 370
    .local v2, "duration":J
    move-wide/from16 v4, p4

    .line 371
    .local v4, "timeoutMs":J
    const/16 v6, 0x3e8

    .line 372
    .local v6, "SLEEP_INTERVAL_MILLS":I
    const/4 v7, 0x3

    move v9, v7

    move-wide v7, v4

    move-wide/from16 v4, p2

    .line 373
    .end local p2    # "timeStartMs":J
    .local v4, "timeStartMs":J
    .local v7, "timeoutMs":J
    .local v9, "retry":I
    :goto_0
    const-wide/16 v10, 0x7d0

    cmp-long v10, v2, v10

    const-string v11, ", prevDumpDuration="

    const-string v12, "Watchdog"

    if-gez v10, :cond_2

    add-int/lit8 v10, v9, -0x1

    .end local v9    # "retry":I
    .local v10, "retry":I
    if-lez v9, :cond_1

    const-wide/16 v13, 0x1388

    cmp-long v9, v7, v13

    if-ltz v9, :cond_1

    .line 374
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "retryDumpMyStacktrace: timeoutMs="

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v12, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    const-wide/16 v13, 0x3e8

    invoke-static {v13, v14}, Landroid/os/SystemClock;->sleep(J)V

    .line 376
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 377
    sub-long/2addr v7, v13

    .line 378
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v9

    div-long v13, v7, v13

    long-to-int v11, v13

    move-object/from16 v13, p1

    invoke-static {v9, v13, v11}, Landroid/os/Debug;->dumpJavaBacktraceToFileTimeout(ILjava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 380
    const-string v9, "retryDumpMyStacktrace succeeded"

    invoke-static {v12, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    const/4 v9, 0x1

    return v9

    .line 383
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v11

    sub-long/2addr v11, v4

    .line 384
    .local v11, "dumpDuration":J
    sub-long/2addr v7, v11

    .line 385
    .end local v11    # "dumpDuration":J
    move v9, v10

    goto :goto_0

    .line 373
    :cond_1
    move-object/from16 v13, p1

    .line 386
    move v9, v10

    goto :goto_1

    .line 373
    .end local v10    # "retry":I
    .restart local v9    # "retry":I
    :cond_2
    move-object/from16 v13, p1

    .line 386
    :goto_1
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "retryDumpMyStacktrace failed: timeoutMs="

    invoke-virtual {v10, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v12, v10}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    const/4 v10, 0x0

    return v10
.end method

.method public setWatchdogPropTimestamp(J)V
    .locals 3
    .param p1, "anrtime"    # J

    .line 176
    :try_start_0
    const-string/jumbo v0, "sys.service.watchdog.timestamp"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 179
    goto :goto_0

    .line 177
    :catch_0
    move-exception v0

    .line 178
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v1, "Watchdog"

    const-string v2, "Failed to set watchdog.timestamp property"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 180
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :goto_0
    return-void
.end method
