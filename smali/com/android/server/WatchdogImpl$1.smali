.class Lcom/android/server/WatchdogImpl$1;
.super Ljava/lang/Object;
.source "WatchdogImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/WatchdogImpl;->scheduleLayerLeakCheck()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic val$h:Landroid/os/Handler;


# direct methods
.method constructor <init>(Landroid/os/Handler;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 123
    iput-object p1, p0, Lcom/android/server/WatchdogImpl$1;->val$h:Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 126
    invoke-static {}, Landroid/view/SurfaceControl;->checkLayersLeaked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    invoke-static {}, Landroid/view/SurfaceControl;->getLeakedLayers()Ljava/lang/String;

    move-result-object v0

    .line 128
    .local v0, "leakedLayers":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Leaked layers: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Watchdog"

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    invoke-static {}, Landroid/view/SurfaceControlStub;->getInstance()Landroid/view/SurfaceControlStub;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/SurfaceControlStub;->reportOORException(Z)V

    .line 133
    .end local v0    # "leakedLayers":Ljava/lang/String;
    :cond_0
    iget-object v0, p0, Lcom/android/server/WatchdogImpl$1;->val$h:Landroid/os/Handler;

    const-wide/32 v1, 0x249f0

    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 134
    return-void
.end method
