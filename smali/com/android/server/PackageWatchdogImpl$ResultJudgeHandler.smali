.class Lcom/android/server/PackageWatchdogImpl$ResultJudgeHandler;
.super Landroid/os/Handler;
.source "PackageWatchdogImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/PackageWatchdogImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ResultJudgeHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/PackageWatchdogImpl;


# direct methods
.method public constructor <init>(Lcom/android/server/PackageWatchdogImpl;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 56
    iput-object p1, p0, Lcom/android/server/PackageWatchdogImpl$ResultJudgeHandler;->this$0:Lcom/android/server/PackageWatchdogImpl;

    .line 57
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 58
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .line 61
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;

    .line 62
    .local v0, "event":Lmiui/mqsas/sdk/event/GeneralExceptionEvent;
    invoke-static {}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->getInstance()Lmiui/mqsas/sdk/MQSEventManagerDelegate;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportGeneralException(Lmiui/mqsas/sdk/event/GeneralExceptionEvent;)Z

    .line 63
    iget-object v1, p0, Lcom/android/server/PackageWatchdogImpl$ResultJudgeHandler;->this$0:Lcom/android/server/PackageWatchdogImpl;

    iget-object v1, v1, Lcom/android/server/PackageWatchdogImpl;->rescuemap:Ljava/util/HashMap;

    invoke-virtual {v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->getPackageName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    iget v2, p1, Landroid/os/Message;->what:I

    .line 64
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 65
    iget-object v1, p0, Lcom/android/server/PackageWatchdogImpl$ResultJudgeHandler;->this$0:Lcom/android/server/PackageWatchdogImpl;

    iget-object v1, v1, Lcom/android/server/PackageWatchdogImpl;->rescuemap:Ljava/util/HashMap;

    invoke-virtual {v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->getPackageName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    iget v2, p1, Landroid/os/Message;->what:I

    .line 66
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    :cond_0
    return-void
.end method
