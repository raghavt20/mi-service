public class com.android.server.MQSThread extends java.lang.Thread {
	 /* .source "MQSThread.java" */
	 /* # static fields */
	 private static final java.lang.String DYNAMIC_DAILY_USE_FORMAT;
	 private static final java.lang.String TAG;
	 private static final java.lang.String VIBRATION_DAILY_USE_FORMAT;
	 /* # instance fields */
	 private final java.lang.String EffectID;
	 private final java.lang.String PackageName;
	 private final java.lang.String count;
	 private android.media.MiuiXlog mMiuiXlog;
	 /* # direct methods */
	 public com.android.server.MQSThread ( ) {
		 /* .locals 1 */
		 /* .param p1, "PKG" # Ljava/lang/String; */
		 /* .line 29 */
		 /* invoke-direct {p0}, Ljava/lang/Thread;-><init>()V */
		 /* .line 27 */
		 /* new-instance v0, Landroid/media/MiuiXlog; */
		 /* invoke-direct {v0}, Landroid/media/MiuiXlog;-><init>()V */
		 this.mMiuiXlog = v0;
		 /* .line 30 */
		 this.PackageName = p1;
		 /* .line 31 */
		 int v0 = 0; // const/4 v0, 0x0
		 this.EffectID = v0;
		 /* .line 32 */
		 this.count = v0;
		 /* .line 33 */
		 return;
	 } // .end method
	 public com.android.server.MQSThread ( ) {
		 /* .locals 1 */
		 /* .param p1, "PKG" # Ljava/lang/String; */
		 /* .param p2, "ID" # Ljava/lang/String; */
		 /* .param p3, "nums" # I */
		 /* .line 35 */
		 /* invoke-direct {p0}, Ljava/lang/Thread;-><init>()V */
		 /* .line 27 */
		 /* new-instance v0, Landroid/media/MiuiXlog; */
		 /* invoke-direct {v0}, Landroid/media/MiuiXlog;-><init>()V */
		 this.mMiuiXlog = v0;
		 /* .line 36 */
		 this.PackageName = p1;
		 /* .line 37 */
		 this.EffectID = p2;
		 /* .line 38 */
		 java.lang.Integer .toString ( p3 );
		 this.count = v0;
		 /* .line 39 */
		 return;
	 } // .end method
	 private Boolean isAllowedRegion ( ) {
		 /* .locals 3 */
		 /* .line 91 */
		 final String v0 = "ro.miui.region"; // const-string v0, "ro.miui.region"
		 final String v1 = ""; // const-string v1, ""
		 android.os.SystemProperties .get ( v0,v1 );
		 /* .line 92 */
		 /* .local v0, "region":Ljava/lang/String; */
		 /* new-instance v1, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
		 /* const-string/jumbo v2, "the region is :" */
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 final String v2 = "MQSThread.vibrator"; // const-string v2, "MQSThread.vibrator"
		 android.util.Slog .i ( v2,v1 );
		 /* .line 93 */
		 final String v1 = "CN"; // const-string v1, "CN"
		 v1 = 		 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 } // .end method
	 /* # virtual methods */
	 public void reportDynamicDailyUse ( ) {
		 /* .locals 4 */
		 /* .line 57 */
		 v0 = 		 /* invoke-direct {p0}, Lcom/android/server/MQSThread;->isAllowedRegion()Z */
		 /* if-nez v0, :cond_0 */
		 /* .line 58 */
		 return;
		 /* .line 60 */
	 } // :cond_0
	 final String v0 = "reportDynamicDailyUse start."; // const-string v0, "reportDynamicDailyUse start."
	 final String v1 = "MQSThread.vibrator"; // const-string v1, "MQSThread.vibrator"
	 android.util.Slog .i ( v1,v0 );
	 /* .line 63 */
	 v0 = this.PackageName;
	 /* filled-new-array {v0}, [Ljava/lang/Object; */
	 /* const-string/jumbo v2, "{\"name\":\"dynamic_dailyuse\",\"audio_event\":{\"dynamic_package_name\":\"%s\"},\"dgt\":\"null\",\"audio_ext\":\"null\" }" */
	 java.lang.String .format ( v2,v0 );
	 /* .line 65 */
	 /* .local v0, "result":Ljava/lang/String; */
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v3 = "reportDynamicDailyUse:"; // const-string v3, "reportDynamicDailyUse:"
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v1,v2 );
	 /* .line 67 */
	 try { // :try_start_0
		 v2 = this.mMiuiXlog;
		 (( android.media.MiuiXlog ) v2 ).miuiXlogSend ( v0 ); // invoke-virtual {v2, v0}, Landroid/media/MiuiXlog;->miuiXlogSend(Ljava/lang/String;)I
		 /* :try_end_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
		 /* .line 70 */
		 /* .line 68 */
		 /* :catchall_0 */
		 /* move-exception v2 */
		 /* .line 69 */
		 /* .local v2, "e":Ljava/lang/Throwable; */
		 final String v3 = "can not use miuiXlogSend!!!"; // const-string v3, "can not use miuiXlogSend!!!"
		 android.util.Slog .d ( v1,v3 );
		 /* .line 71 */
	 } // .end local v2 # "e":Ljava/lang/Throwable;
} // :goto_0
return;
} // .end method
public void reportVibrationDailyUse ( ) {
/* .locals 4 */
/* .line 74 */
v0 = /* invoke-direct {p0}, Lcom/android/server/MQSThread;->isAllowedRegion()Z */
/* if-nez v0, :cond_0 */
/* .line 75 */
return;
/* .line 77 */
} // :cond_0
final String v0 = "reportVibrationDailyUse start."; // const-string v0, "reportVibrationDailyUse start."
final String v1 = "MQSThread.vibrator"; // const-string v1, "MQSThread.vibrator"
android.util.Slog .i ( v1,v0 );
/* .line 80 */
v0 = this.PackageName;
v2 = this.EffectID;
v3 = this.count;
/* filled-new-array {v0, v2, v3}, [Ljava/lang/Object; */
/* const-string/jumbo v2, "{\"name\":\"vibration_dailyuse\",\"audio_event\":{\"vibration_package_name\":\"%s\",\"vibration_effect_id\":\"%s\",\"vibration_count\":\"%s\"},\"dgt\":\"null\",\"audio_ext\":\"null\" }" */
java.lang.String .format ( v2,v0 );
/* .line 82 */
/* .local v0, "result":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "reportVibrationDailyUse:"; // const-string v3, "reportVibrationDailyUse:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 84 */
try { // :try_start_0
v2 = this.mMiuiXlog;
(( android.media.MiuiXlog ) v2 ).miuiXlogSend ( v0 ); // invoke-virtual {v2, v0}, Landroid/media/MiuiXlog;->miuiXlogSend(Ljava/lang/String;)I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 87 */
/* .line 85 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 86 */
/* .local v2, "e":Ljava/lang/Throwable; */
final String v3 = "can not use miuiXlogSend!!!"; // const-string v3, "can not use miuiXlogSend!!!"
android.util.Slog .d ( v1,v3 );
/* .line 88 */
} // .end local v2 # "e":Ljava/lang/Throwable;
} // :goto_0
return;
} // .end method
public void run ( ) {
/* .locals 3 */
/* .line 43 */
int v0 = 0; // const/4 v0, 0x0
android.os.Process .setThreadPriority ( v0 );
/* .line 45 */
try { // :try_start_0
v0 = this.EffectID;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( java.lang.String ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z
/* if-nez v0, :cond_0 */
/* .line 46 */
(( com.android.server.MQSThread ) p0 ).reportVibrationDailyUse ( ); // invoke-virtual {p0}, Lcom/android/server/MQSThread;->reportVibrationDailyUse()V
/* .line 48 */
} // :cond_0
(( com.android.server.MQSThread ) p0 ).reportDynamicDailyUse ( ); // invoke-virtual {p0}, Lcom/android/server/MQSThread;->reportDynamicDailyUse()V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 53 */
} // :goto_0
/* .line 50 */
/* :catch_0 */
/* move-exception v0 */
/* .line 51 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 52 */
final String v1 = "MQSThread.vibrator"; // const-string v1, "MQSThread.vibrator"
final String v2 = "error for MQSThread"; // const-string v2, "error for MQSThread"
android.util.Log .e ( v1,v2 );
/* .line 54 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
