.class public final Lcom/android/server/PsiParser$Psi;
.super Ljava/lang/Object;
.source "PsiParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/PsiParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Psi"
.end annotation


# instance fields
.field public final full:Lcom/android/server/PsiParser$Prop;

.field public final some:Lcom/android/server/PsiParser$Prop;


# direct methods
.method private constructor <init>(Lcom/android/server/PsiParser$Prop;Lcom/android/server/PsiParser$Prop;)V
    .locals 0
    .param p1, "some"    # Lcom/android/server/PsiParser$Prop;
    .param p2, "full"    # Lcom/android/server/PsiParser$Prop;

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p1, p0, Lcom/android/server/PsiParser$Psi;->some:Lcom/android/server/PsiParser$Prop;

    .line 81
    iput-object p2, p0, Lcom/android/server/PsiParser$Psi;->full:Lcom/android/server/PsiParser$Prop;

    .line 82
    return-void
.end method

.method public static parseFromRawString(Ljava/lang/String;)Lcom/android/server/PsiParser$Psi;
    .locals 4
    .param p0, "raw"    # Ljava/lang/String;

    .line 84
    const-string v0, "\\R"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 85
    .local v0, "lines":[Ljava/lang/String;
    const/4 v1, 0x0

    aget-object v1, v0, v1

    invoke-static {v1}, Lcom/android/server/PsiParser$Prop;->parseFromLine(Ljava/lang/String;)Lcom/android/server/PsiParser$Prop;

    move-result-object v1

    .line 86
    .local v1, "some":Lcom/android/server/PsiParser$Prop;
    const/4 v2, 0x1

    aget-object v2, v0, v2

    invoke-static {v2}, Lcom/android/server/PsiParser$Prop;->parseFromLine(Ljava/lang/String;)Lcom/android/server/PsiParser$Prop;

    move-result-object v2

    .line 87
    .local v2, "full":Lcom/android/server/PsiParser$Prop;
    new-instance v3, Lcom/android/server/PsiParser$Psi;

    invoke-direct {v3, v1, v2}, Lcom/android/server/PsiParser$Psi;-><init>(Lcom/android/server/PsiParser$Prop;Lcom/android/server/PsiParser$Prop;)V

    return-object v3
.end method
